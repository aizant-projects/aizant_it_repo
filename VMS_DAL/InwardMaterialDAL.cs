﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Data.SqlTypes;
using VMS_BO;

namespace VMS_DAL
{
    public class InwardMaterialDAL : SQLHELPER
    {
        public int InsertInwardMaterial(MaterialBO objMaterialBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_INWardMaterial_Insert";
            SqlParameter[] arrParam = new SqlParameter[17];
            arrParam[0] = new SqlParameter("@MaterialType", objMaterialBO.MaterialType);
            arrParam[1] = new SqlParameter("@MaterialMasterID", objMaterialBO.MaterialMasterID);
            arrParam[2] = new SqlParameter("@DescriptionOfMaterial", objMaterialBO.DescriptionOfMaterial);
            arrParam[3] = new SqlParameter("@Quantity", objMaterialBO.Quantity);
            arrParam[4] = new SqlParameter("@Department", objMaterialBO.Department);
            arrParam[5] = new SqlParameter("@ReceivedBy", objMaterialBO.ReceivedBy);
            arrParam[6] = new SqlParameter("@VechileNo", objMaterialBO.VechileNo);
            arrParam[7] = new SqlParameter("@DCP_InvoiceNo", objMaterialBO.DCP_InvoiceNo);
            arrParam[8] = new SqlParameter("@AssessableValue", objMaterialBO.AssessableValue);
            arrParam[9] = new SqlParameter("@InDateTime", objMaterialBO.InDateTime);
            arrParam[10] = new SqlParameter("@Remarks", objMaterialBO.Remarks);
            arrParam[11] = new SqlParameter("@CreatedBy", objMaterialBO.CreatedBy);
            arrParam[12] = new SqlParameter("@CreatedDate", objMaterialBO.CreatedDate);
            arrParam[13] = new SqlParameter("@SupplierName", objMaterialBO.SupplierName);
            arrParam[14] = new SqlParameter("@UnitsID", objMaterialBO.UnitsID);
            arrParam[15] = new SqlParameter("@DeptName", objMaterialBO.DeptName);
            arrParam[16] = new SqlParameter("@EmpName", objMaterialBO.EmpName);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }


        //SearchFilter InwardMaterials Find Method.
        public DataTable SearchFilterInwardMaterial(MaterialBO objMaterialBO)
        {
            string strQuery = "VMS.AizantIT_SP_INWardMaterialsSearch_Select";
            SqlParameter[] arrParam = new SqlParameter[9];
            arrParam[0] = new SqlParameter("@MaterialType", objMaterialBO.MaterialType);
            arrParam[1] = new SqlParameter("@MaterialName", objMaterialBO.MaterialName);
            arrParam[2] = new SqlParameter("@Department", objMaterialBO.Department);
            arrParam[3] = new SqlParameter("@DCPInvoiceNo", objMaterialBO.DCP_InvoiceNo);
            arrParam[4] = new SqlParameter("@Suppliername", objMaterialBO.SupplierName);
            arrParam[5] = new SqlParameter("@OutDateTime", objMaterialBO.InDateTime);
            arrParam[6] = new SqlParameter("@OutDateTime1", objMaterialBO.OutDateTime);
            arrParam[7] = new SqlParameter("@ReceviedBy", objMaterialBO.ReceivedBy);
            arrParam[8] = new SqlParameter("@SNo", objMaterialBO.Sno);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public int Update_InwardMaterialEdit(MaterialBO objMaterialBO)
        {
            int c = 0;
            string strQuery = "VMS.AizantIT_SP_INWardMaterial_Update";
            SqlParameter[] arrParam = new SqlParameter[18];
            arrParam[0] = new SqlParameter("@tempid", objMaterialBO.Sno);
            arrParam[1] = new SqlParameter("@MaterialType", objMaterialBO.MaterialType);
            arrParam[2] = new SqlParameter("@MaterialName", objMaterialBO.MaterialMasterID);
            arrParam[3] = new SqlParameter("@Quantity", objMaterialBO.Quantity);
            arrParam[4] = new SqlParameter("@Department", objMaterialBO.Department);
            arrParam[5] = new SqlParameter("@ReceivedBy", objMaterialBO.ReceivedBy);
            arrParam[6] = new SqlParameter("@VechileNo", objMaterialBO.VechileNo);
            arrParam[7] = new SqlParameter("@DCP_InvoiceNo", objMaterialBO.DCP_InvoiceNo);
            arrParam[8] = new SqlParameter("@SupplierName", objMaterialBO.SupplierName);
            arrParam[9] = new SqlParameter("@LastChangedBy", objMaterialBO.LastChangedBy);
            arrParam[10] = new SqlParameter("@LastChangedDate", objMaterialBO.LastChangedDate);
            arrParam[11] = new SqlParameter("@LastChangedByComments", objMaterialBO.LastChangedComments);
            arrParam[12] = new SqlParameter("@DescriptionOfMaterial", objMaterialBO.DescriptionOfMaterial);
            arrParam[13] = new SqlParameter("@Remarks", objMaterialBO.Remarks);
            arrParam[14] = new SqlParameter("@UnitsID", objMaterialBO.UnitsID);
            //arrParam[15] = new SqlParameter("@InDateTime", objMaterialBO.InDateTime);
            arrParam[15] = new SqlParameter("@AssessableValue", objMaterialBO.AssessableValue);
            arrParam[16] = new SqlParameter("@DeptName", objMaterialBO.DeptName);
            arrParam[17] = new SqlParameter("@EmpName", objMaterialBO.EmpName);
            c = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c;
        }

        public DataTable BindDataInwardMaterials()
        {
            string strQuery = "VMS.AizantIT_SP_INWardMaterial_BindData";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }

        public DataTable InwardMateriallinkbuttonFilldata(MaterialBO objMaterialBO)
        {
            string strQuery = "VMS.AizantIT_SP_INWardMaterial_GridViewLinkbutton";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@tempid", objMaterialBO.Sno);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public DataTable BindMaterialType()
        {
            string strQuery = "VMS.AizantIT_SP_InwardMaterialType_Select";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }
        //Get Inward Material Register Jquery Datatable
        public DataTable GetInMaterialListDb(JQDatatableBO _objJQDataTableBO)
        {
            string strQuery = "[VMS].[AizantIT_SP_GetInwardMaterialList]";
            SqlParameter[] arrParam = new SqlParameter[7];
            arrParam[0] = new SqlParameter("@Mode", _objJQDataTableBO.iMode);
            arrParam[1] = new SqlParameter("@SNo", _objJQDataTableBO.pkid);
            arrParam[2] = new SqlParameter("@DisplayLength", _objJQDataTableBO.iDisplayLength);
            arrParam[3] = new SqlParameter("@DisplayStart", _objJQDataTableBO.iDisplayStart);
            arrParam[4] = new SqlParameter("@SortCol", _objJQDataTableBO.iSortCol);
            arrParam[5] = new SqlParameter("@SortDir", _objJQDataTableBO.sSortDir);
            arrParam[6] = new SqlParameter("@Search", _objJQDataTableBO.sSearch);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public DataTable LoadInwardMaterialNameMaster()
        {
            string strQuery = "[VMS].[AizantIT_SP_GetOutwardMaterialNameMaster]";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }
    }
}
