﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Data.SqlTypes;
using VMS_BO;

namespace VMS_DAL
{
    public class EmployeeMovementDAL:SQLHELPER
    {
        //Staff Movement DAL
        public int StaffMovement_Insert(SupplyRegisterBO objSupplyRegisterBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_StaffMovement_Insert";
            SqlParameter[] arrParam = new SqlParameter[6];
            arrParam[0] = new SqlParameter("@EmpID", objSupplyRegisterBO.EmployeeID);
            arrParam[1] = new SqlParameter("@Purpose", objSupplyRegisterBO.Purpose);
            arrParam[2] = new SqlParameter("@Remarks", objSupplyRegisterBO.Remarks);
            arrParam[3] = new SqlParameter("@OutDateTime", objSupplyRegisterBO.ReturnDateTime);
            arrParam[4] = new SqlParameter("@CreatedBy", objSupplyRegisterBO.CreatedBy);
            arrParam[5] = new SqlParameter("@CreatedDate", objSupplyRegisterBO.CreatedDate);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public DataTable SearchFilterStaff(SupplyRegisterBO objSupplyRegisterBO)
        {
            string strQuery = "VMS.AizantIT_SP_StaffMovement_Select";
            SqlParameter[] arrParam = new SqlParameter[4];
            arrParam[0] = new SqlParameter("@EmpID", objSupplyRegisterBO.EmployeeID);
            arrParam[1] = new SqlParameter("@Department", objSupplyRegisterBO.Department);
            arrParam[2] = new SqlParameter("@indatetime", objSupplyRegisterBO.ReturnDateTime);
            arrParam[3] = new SqlParameter("@indatetime1", objSupplyRegisterBO.InDateTime);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public int Update_StaffRegisterEdit(SupplyRegisterBO objSupplyRegisterBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_StaffMovement_Update";
            SqlParameter[] arrParam = new SqlParameter[7];
            arrParam[0] = new SqlParameter("@tempid", objSupplyRegisterBO.SNo);
            arrParam[1] = new SqlParameter("@EmpID", objSupplyRegisterBO.EmployeeID);
            arrParam[2] = new SqlParameter("@LastChangedByComments", objSupplyRegisterBO.LastChangedComments);
            //arrParam[2] = new SqlParameter("@OutDateTime", objSupplyRegisterBO.ReturnDateTime);
            arrParam[3] = new SqlParameter("@Remarks", objSupplyRegisterBO.Remarks);
            arrParam[4] = new SqlParameter("@Purpose", objSupplyRegisterBO.Purpose);
            arrParam[5] = new SqlParameter("@LastChangedBy", objSupplyRegisterBO.LastChangedBy);
            arrParam[6] = new SqlParameter("@LastChangedDate", objSupplyRegisterBO.LastChangedDate);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public DataTable BindDataStaffMovementRegister()
        {
            string strQuery = "VMS.AizantIT_SP_StaffMovement_BindData";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }

        public DataTable StaffRegisterlinkbuttonFilldata(SupplyRegisterBO objSupplyRegisterBO)
        {
            string strQuery = "VMS.AizantIT_SP_StaffMovement_GridViewLinkbutton";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@tempid", objSupplyRegisterBO.SNo);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public int Checkout(SupplyRegisterBO objSupplyRegisterBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_StaffMovement_CheckOut";
            SqlParameter[] arrParam = new SqlParameter[2];
            arrParam[0] = new SqlParameter("@tempid", objSupplyRegisterBO.SNo);
            arrParam[1] = new SqlParameter("@indatetime", objSupplyRegisterBO.InDateTime);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }


        //Holiday Register DAL
        public int HolidayRegister_Insert(SupplyRegisterBO objSupplyRegisterBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_HolidayRegister_Insert";
            SqlParameter[] arrParam = new SqlParameter[8];
            arrParam[0] = new SqlParameter("@EmpID", objSupplyRegisterBO.EmployeeID);
            arrParam[1] = new SqlParameter("@ApprovedBy", objSupplyRegisterBO.ApprovedBy);
            arrParam[2] = new SqlParameter("@ApprovedOn", objSupplyRegisterBO.ApprovedOn);
            arrParam[3] = new SqlParameter("@Purpose", objSupplyRegisterBO.Purpose);
            arrParam[4] = new SqlParameter("@Remarks", objSupplyRegisterBO.Remarks);
            arrParam[5] = new SqlParameter("@InDateTime", objSupplyRegisterBO.InDateTime);
            arrParam[6] = new SqlParameter("@CreatedBy", objSupplyRegisterBO.CreatedBy);
            arrParam[7] = new SqlParameter("@CreatedDate", objSupplyRegisterBO.CreatedDate);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public DataTable SearchFilterHoliday(SupplyRegisterBO objSupplyRegisterBO)
        {
            string strQuery = "VMS.AizantIT_SP_HolidayRegister_Select";
            SqlParameter[] arrParam = new SqlParameter[4];
            arrParam[0] = new SqlParameter("@EmpID", objSupplyRegisterBO.EmployeeID);
            arrParam[1] = new SqlParameter("@Department", objSupplyRegisterBO.Department);
            arrParam[2] = new SqlParameter("@indatetime", objSupplyRegisterBO.InDateTime);
            arrParam[3] = new SqlParameter("@indatetime1", objSupplyRegisterBO.ReturnDateTime);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public int Update_HolidayRegisterEdit(SupplyRegisterBO objSupplyRegisterBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_HolidayRegister_Update";
            SqlParameter[] arrParam = new SqlParameter[9];
            arrParam[0] = new SqlParameter("@tempid", objSupplyRegisterBO.SNo);
            arrParam[1] = new SqlParameter("@EmpID", objSupplyRegisterBO.EmployeeID);
            //arrParam[2] = new SqlParameter("@InDateTime", objSupplyRegisterBO.InDateTime);
            arrParam[2] = new SqlParameter("@Remarks", objSupplyRegisterBO.Remarks);
            arrParam[3] = new SqlParameter("@Purpose", objSupplyRegisterBO.Purpose);
            arrParam[4] = new SqlParameter("@LastChangedBy", objSupplyRegisterBO.LastChangedBy);
            arrParam[5] = new SqlParameter("@LastChangedDate", objSupplyRegisterBO.LastChangedDate);
            arrParam[6] = new SqlParameter("@LastChangedByComments", objSupplyRegisterBO.LastChangedComments);
            arrParam[7] = new SqlParameter("@ApprovedBy", objSupplyRegisterBO.ApprovedBy);
            arrParam[8] = new SqlParameter("@ApprovedOn", objSupplyRegisterBO.ApprovedOn);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public DataTable BindDataHolidayRegister()
        {
            string strQuery = "VMS.AizantIT_SP_HolidayRegister_BindData";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }

        public DataTable HolidayRegisterlinkbuttonFilldata(SupplyRegisterBO objSupplyRegisterBO)
        {
            string strQuery = "VMS.AizantIT_SP_HolidayRegister_GridViewLinkbutton";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@tempid", objSupplyRegisterBO.SNo);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public int CheckoutHoliday(SupplyRegisterBO objSupplyRegisterBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_Holiday_CheckOut";
            SqlParameter[] arrParam = new SqlParameter[2];
            arrParam[0] = new SqlParameter("@tempid", objSupplyRegisterBO.SNo);
            arrParam[1] = new SqlParameter("@OutDateTime", objSupplyRegisterBO.ReturnDateTime);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public DataTable BindEmpMovementType()
        {
            string strQuery = "VMS.AizantIT_SP_EmpMovementTypeMaster_Select";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }
    }
}
