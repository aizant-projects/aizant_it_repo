﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using VMS_BO;


namespace VMS_DAL
{
    public class VechileRegisterDAL : SQLHELPER
    {
        public int VechileReading_Insert(ServiceAndVechileBO objServiceAndVechileBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_VechileRegister_Insert";
            SqlParameter[] arrParam = new SqlParameter[10];
            arrParam[0] = new SqlParameter("@VechileID", objServiceAndVechileBO.VechileID);
            arrParam[1] = new SqlParameter("@FirstName", objServiceAndVechileBO.FirstName);
            arrParam[2] = new SqlParameter("@MobileNo", objServiceAndVechileBO.MobileNo);
            arrParam[3] = new SqlParameter("@PlaceVisited", objServiceAndVechileBO.PlaceVisited);
            arrParam[4] = new SqlParameter("@OutReading", objServiceAndVechileBO.OutReading);
            arrParam[5] = new SqlParameter("@OutDateTime", objServiceAndVechileBO.OutDateTime);
            arrParam[6] = new SqlParameter("@Remarks", objServiceAndVechileBO.Remarks);
            arrParam[7] = new SqlParameter("@CreatedBy", objServiceAndVechileBO.CreatedBy);
            arrParam[8] = new SqlParameter("@CreatedDate", objServiceAndVechileBO.CreatedDate);
            arrParam[9] = new SqlParameter("@LastName", objServiceAndVechileBO.LastName);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public int Update_VechileRegister(ServiceAndVechileBO objServiceAndVechileBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_VechileRegister_Update";
            SqlParameter[] arrParam = new SqlParameter[15];
            arrParam[0] = new SqlParameter("@VechileID", objServiceAndVechileBO.VechileID);
            arrParam[1] = new SqlParameter("@FirstName", objServiceAndVechileBO.FirstName);
            arrParam[2] = new SqlParameter("@MobileNo", objServiceAndVechileBO.MobileNo);
            arrParam[3] = new SqlParameter("@PlaceVisited", objServiceAndVechileBO.PlaceVisited);
            arrParam[4] = new SqlParameter("@OutReading", objServiceAndVechileBO.OutReading);
            arrParam[5] = new SqlParameter("@InReading", objServiceAndVechileBO.InReading);
            arrParam[6] = new SqlParameter("@DistanceKms", objServiceAndVechileBO.DistanceKMS);
            arrParam[7] = new SqlParameter("@TotalKiloMeters", objServiceAndVechileBO.TotalKiloMeters);
            arrParam[8] = new SqlParameter("@LastChangedBy", objServiceAndVechileBO.LastChangedBy);
            arrParam[9] = new SqlParameter("@LastChangedDate", objServiceAndVechileBO.LastChangedDate);
            arrParam[10] = new SqlParameter("@LastChangedByComments", objServiceAndVechileBO.LastChangedComments);
            arrParam[11] = new SqlParameter("@tempid", objServiceAndVechileBO.SNo);
            //arrParam[12] = new SqlParameter("@OutDateTime", objServiceAndVechileBO.OutDateTime);
            arrParam[12] = new SqlParameter("@Remarks", objServiceAndVechileBO.Remarks);
            arrParam[13] = new SqlParameter("@LastName", objServiceAndVechileBO.LastName);
            arrParam[14] = new SqlParameter("@InDateTime", objServiceAndVechileBO.InDateTime);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public DataTable SearchFilterVechileReading(ServiceAndVechileBO objServiceAndVechileBO)
        {
            string strQuery = "VMS.AizantIT_SP_VechileRegister_Select";
            SqlParameter[] arrParam = new SqlParameter[6];
            arrParam[0] = new SqlParameter("@vechileID", objServiceAndVechileBO.VechileID);
            arrParam[1] = new SqlParameter("@FirstName", objServiceAndVechileBO.FirstName);
            arrParam[2] = new SqlParameter("@placeVisited", objServiceAndVechileBO.PlaceVisited);
            arrParam[3] = new SqlParameter("@indatetime", objServiceAndVechileBO.InDateTime);
            arrParam[4] = new SqlParameter("@indatetime1", objServiceAndVechileBO.OutDateTime);
            arrParam[5] = new SqlParameter("@SNo", objServiceAndVechileBO.SNo);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        //New List in VechicleRegisterList
        public DataTable SearchFilterVechileReadingDetails(ServiceAndVechileBO objServiceAndVechileBO)
        {
            string strQuery = "VMS.AizantIT_SP_VechileRegister_Select";
            SqlParameter[] arrParam = new SqlParameter[6];
            arrParam[0] = new SqlParameter("@vechileID", objServiceAndVechileBO.VechileID);
            arrParam[1] = new SqlParameter("@FirstName", objServiceAndVechileBO.FirstName);
            arrParam[2] = new SqlParameter("@placeVisited", objServiceAndVechileBO.PlaceVisited);
            arrParam[3] = new SqlParameter("@indatetime", objServiceAndVechileBO.InDateTime);
            arrParam[4] = new SqlParameter("@indatetime1", objServiceAndVechileBO.OutDateTime);
            arrParam[5] = new SqlParameter("@SNo", objServiceAndVechileBO.SNo);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);

        }



        public DataTable BindDataVechile()
        {
            string strQuery = "VMS.AizantIT_SP_VechileRegister_BindData";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }

        public DataTable VechilelinkbuttonFilldata(ServiceAndVechileBO objServiceAndVechileBO)
        {
            string strQuery = "VMS.AizantIT_SP_VechileRegister_GridViewLinkbutton";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@tempid", objServiceAndVechileBO.SNo);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public DataTable LoadVechileNo()
        {
            string strQuery = "VMS.AizantIT_SP_VechileNoMaster_Select";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }

        public int CheckoutVechile(ServiceAndVechileBO objServiceAndVechileBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_Vechile_CheckOut";
            SqlParameter[] arrParam = new SqlParameter[2];
            arrParam[0] = new SqlParameter("@tempid", objServiceAndVechileBO.SNo);
            arrParam[1] = new SqlParameter("@InDateTime", objServiceAndVechileBO.InDateTime);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }


        public int AddVechlieNoMaster(ServiceAndVechileBO objServiceAndVechileBO)
        {
            string strQuery = "VMS.AizantIT_SP_VechileNoMaster_AddVechileNo";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@VechileNo", objServiceAndVechileBO.VechileNo);
            return Convert.ToInt32(cmmdExecuteScalar(strQuery, CommandType.StoredProcedure, arrParam));
        }

        public DataTable GetDriverNames(ServiceAndVechileBO objServiceAndVechileBO)
        {
            string strQuery = "VMS.AizantIT_GetDriverID";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }

        public int VechileNoifExistsorNot(string VechileNo)
        {
            string strQuery = "VMS.AizantIT_SP_VechileNo_IfExits";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@VechileNo", VechileNo);
            return Convert.ToInt32(cmmdExecuteScalar(strQuery, CommandType.StoredProcedure, arrParam));
        }

        public DataTable GetVechileDetails_ListDb(JQDatatableBO _objJQDataTableBO)
        {
            string strQuery = "[VMS].[AizantIT_SP_VechileRegister_Details]";
            SqlParameter[] arrParam = new SqlParameter[7];
            arrParam[0] = new SqlParameter("@Mode", _objJQDataTableBO.iMode);
            arrParam[1] = new SqlParameter("@SNo", _objJQDataTableBO.pkid);
            arrParam[2] = new SqlParameter("@DisplayLength", _objJQDataTableBO.iDisplayLength);
            arrParam[3] = new SqlParameter("@DisplayStart", _objJQDataTableBO.iDisplayStart);
            arrParam[4] = new SqlParameter("@SortCol", _objJQDataTableBO.iSortCol);
            arrParam[5] = new SqlParameter("@SortDir", _objJQDataTableBO.sSortDir);
            arrParam[6] = new SqlParameter("@Search", _objJQDataTableBO.sSearch);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }
    }
}

