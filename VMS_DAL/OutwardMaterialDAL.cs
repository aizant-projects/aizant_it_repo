﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Data.SqlTypes;
using VMS_BO;

namespace VMS_DAL
{
    public class OutwardMaterialDAL : SQLHELPER
    {
        public int InsertMaterial(MaterialBO objMaterialBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_OutWardMaterial_Insert";
            SqlParameter[] arrParam = new SqlParameter[15];
            arrParam[0] = new SqlParameter("@MaterialType", objMaterialBO.MaterialType);
            arrParam[1] = new SqlParameter("@MaterialMasterID", objMaterialBO.MaterialMasterID);
            arrParam[2] = new SqlParameter("@DescriptionOfMaterial", objMaterialBO.DescriptionOfMaterial);
            arrParam[3] = new SqlParameter("@Quantity", objMaterialBO.Quantity);
            arrParam[4] = new SqlParameter("@Department", objMaterialBO.Department);
            arrParam[5] = new SqlParameter("@GivenBy", objMaterialBO.GivenBy);
            arrParam[6] = new SqlParameter("@VechileNo", objMaterialBO.VechileNo);
            arrParam[7] = new SqlParameter("@GPorDCPNo", objMaterialBO.GPorDCPNo);
            arrParam[8] = new SqlParameter("@NameOfParty", objMaterialBO.NameOfParty);
            arrParam[9] = new SqlParameter("@OutDateTime", objMaterialBO.OutDateTime);
            arrParam[10] = new SqlParameter("@Remarks", objMaterialBO.Remarks);
            arrParam[11] = new SqlParameter("@CreatedBy", objMaterialBO.CreatedBy);
            arrParam[12] = new SqlParameter("@CreatedDate", objMaterialBO.CreatedDate);
            arrParam[13] = new SqlParameter("@UnitID", objMaterialBO.UnitsID);
            arrParam[14] = new SqlParameter("@ExpectedReturnDate", objMaterialBO.ExpectedReturnDate);
            //arrParam[15] = new SqlParameter("@LastChangedComments", objMaterialBO.LastChangedComments);
            c1 = Convert.ToInt32(cmmdExecuteScalar(strQuery, CommandType.StoredProcedure, arrParam));
            return c1;
        }


        //SearchFilter OutwardMaterials Find Method.
        public DataTable SearchFilter(MaterialBO objMaterialBO)
        {
            string strQuery = "VMS.AizantIT_SP_OutWardMaterialsSearch_Select";
            SqlParameter[] arrParam = new SqlParameter[11];
            arrParam[0] = new SqlParameter("@MaterialType", objMaterialBO.MaterialType);
            arrParam[1] = new SqlParameter("@MaterialMasterID", objMaterialBO.MaterialMasterID);
            //prepare Parameter to Store Procedure
            var pList = new SqlParameter("@MDepartment", SqlDbType.Structured);
            pList.TypeName = "dbo.AizantIT_DepartmentType";
            pList.Value = objMaterialBO.MultiDepartment;
            arrParam[2] = pList;
            arrParam[3] = new SqlParameter("@GPorDCPNo", objMaterialBO.GPorDCPNo);
            arrParam[4] = new SqlParameter("@NameoftheParty", objMaterialBO.NameOfParty);
            arrParam[5] = new SqlParameter("@OutDateTime", objMaterialBO.OutDateTime);
            arrParam[6] = new SqlParameter("@OutDateTime1", objMaterialBO.InDateTime);
            arrParam[7] = new SqlParameter("@OutMaterialID", objMaterialBO.OutMaterialID);
            arrParam[8] = new SqlParameter("@CreatedBy", objMaterialBO.CreatedBy);
            arrParam[9] = new SqlParameter("@Department", objMaterialBO.Department);
            arrParam[10] = new SqlParameter("@StatusMode", objMaterialBO.TransStatus); // here TransStatus is Status 
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public int Update_OutwardMaterialEdit(MaterialBO objMaterialBO)
        {
            int c = 0;
            string strQuery = "VMS.AizantIT_SP_OutWardMaterial_Update";
            SqlParameter[] arrParam = new SqlParameter[21];
            arrParam[0] = new SqlParameter("@OutMaterialID", objMaterialBO.OutMaterialID);
            arrParam[1] = new SqlParameter("@MaterialType", objMaterialBO.MaterialType);
            arrParam[2] = new SqlParameter("@MaterialMasterID", objMaterialBO.MaterialMasterID);
            arrParam[3] = new SqlParameter("@Quantity", objMaterialBO.Quantity);
            arrParam[4] = new SqlParameter("@Department", objMaterialBO.Department);
            arrParam[5] = new SqlParameter("@GivenBy", objMaterialBO.GivenBy);
            arrParam[6] = new SqlParameter("@VechileNo", objMaterialBO.VechileNo);
            arrParam[7] = new SqlParameter("@GPorDCPNo", objMaterialBO.GPorDCPNo);
            arrParam[8] = new SqlParameter("@NameOfParty", objMaterialBO.NameOfParty);
            arrParam[9] = new SqlParameter("@LastChangedBy", objMaterialBO.LastChangedBy);
            arrParam[10] = new SqlParameter("@LastChangedDate", objMaterialBO.LastChangedDate);
            arrParam[11] = new SqlParameter("@LastChangedByComments", objMaterialBO.LastChangedComments);
            arrParam[12] = new SqlParameter("@Return_Quantity", objMaterialBO.Return_Quantity);
            arrParam[13] = new SqlParameter("@DescriptionOfMaterial", objMaterialBO.DescriptionOfMaterial);
            arrParam[14] = new SqlParameter("@Remarks", objMaterialBO.Remarks);
            arrParam[15] = new SqlParameter("@UnitsID", objMaterialBO.UnitsID);
            arrParam[16] = new SqlParameter("@ExpectedReturnDate", objMaterialBO.ExpectedReturnDate);
            arrParam[17] = new SqlParameter("@ReceivedbyDeptID", objMaterialBO.ReceivedDepartment);
            arrParam[18] = new SqlParameter("@ReceivedByID", objMaterialBO.ReceivedBy);
            arrParam[19] = new SqlParameter("@UpdateMode", objMaterialBO.UpdateMode);
            arrParam[20] = new SqlParameter("@ActualReturnDate", objMaterialBO.ActualReturnDate);
            c = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c;
        }

        public DataTable OutwardMateriallinkbuttonFilldata(MaterialBO objMaterialBO)//need to remove
        {
            string strQuery = "VMS.AizantIT_SP_OutwardMaterial_GridViewLinkbutton";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@tempid", objMaterialBO.OutMaterialID);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public int CheckoutOutwardMaterial(MaterialBO objMaterialBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_Outward_CheckOut";
            SqlParameter[] arrParam = new SqlParameter[2];
            arrParam[0] = new SqlParameter("@OutMaterialID", objMaterialBO.OutMaterialID);
            arrParam[1] = new SqlParameter("@Material_ReturnDateAndTime", objMaterialBO.Material_ReturnDateAndTime);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public DataTable BindUnits()
        {
            string strQuery = "VMS.AizantIT_SP_Units_Select";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }

        public DataTable BindMaterialType()
        {
            string strQuery = "VMS.AizantIT_SP_OutwardMaterialTypeMaster_Select";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }

        public int MaterialRejectOrCancel(MaterialBO objMaterialBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_OutwardMaterialStatusReject";
            SqlParameter[] arrParam = new SqlParameter[4];
            arrParam[0] = new SqlParameter("@OutMaterialID", objMaterialBO.OutMaterialID);
            arrParam[1] = new SqlParameter("@LastChangedBy", objMaterialBO.LastChangedBy);
            arrParam[2] = new SqlParameter("@LastChangedDate", objMaterialBO.LastChangedDate);
            arrParam[3] = new SqlParameter("@LastChangedByComments", objMaterialBO.LastChangedComments);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public int MaterialApproval(MaterialBO objMaterialBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_OutwardMaterialStatusApprove";
            SqlParameter[] arrParam = new SqlParameter[4];
            arrParam[0] = new SqlParameter("@OutMaterialID", objMaterialBO.OutMaterialID);
            arrParam[1] = new SqlParameter("@LastChangedBy", objMaterialBO.LastChangedBy);
            arrParam[2] = new SqlParameter("@LastChangedDate", objMaterialBO.LastChangedDate);
            arrParam[3] = new SqlParameter("@LastChangedByComments", objMaterialBO.LastChangedComments);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public int MaterialStoreRejectOrCancel(MaterialBO objMaterialBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_OutwardMaterialStoreStatusReject";
            SqlParameter[] arrParam = new SqlParameter[4];
            arrParam[0] = new SqlParameter("@OutMaterialID", objMaterialBO.OutMaterialID);
            arrParam[1] = new SqlParameter("@LastChangedBy", objMaterialBO.LastChangedBy);
            arrParam[2] = new SqlParameter("@LastChangedDate", objMaterialBO.LastChangedDate);
            arrParam[3] = new SqlParameter("@LastChangedByComments", objMaterialBO.LastChangedComments);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public int MaterialStoreApproval(MaterialBO objMaterialBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_OutwardMaterialStoreStatusApprove";
            SqlParameter[] arrParam = new SqlParameter[4];
            arrParam[0] = new SqlParameter("@OutMaterialID", objMaterialBO.OutMaterialID);
            arrParam[1] = new SqlParameter("@LastChangedBy", objMaterialBO.LastChangedBy);
            arrParam[2] = new SqlParameter("@LastChangedDate", objMaterialBO.LastChangedDate);
            arrParam[3] = new SqlParameter("@LastChangedByComments", objMaterialBO.LastChangedComments);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public DataTable GetCardCount(MaterialBO objMaterialBO)//common to all cards
        {
            string strQuery = "VMS.AizantIT_SP_GetCardCount";
            SqlParameter[] arrParam = new SqlParameter[4];
            arrParam[0] = new SqlParameter("@EmpId", objMaterialBO.EmpID);
            arrParam[1] = new SqlParameter("@DeptId", objMaterialBO.DeptID);
            arrParam[2] = new SqlParameter("@TransStatus", objMaterialBO.TransStatus);
            arrParam[3] = new SqlParameter("@RoleType", objMaterialBO.RoleType);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public string AutoGenerateGP_DC_NO(MaterialBO objMaterialBO)
        {
            string strQuery = "Select [VMS].[AizantIT_GetOutMaterialAutoID]("+ objMaterialBO.DeptID+")";
            return cmmdExecuteScalar(strQuery, CommandType.Text, null);
        }

        //Get Outward Material Register Jquery Datatable
        public DataTable GetOutwardMaterialListDb(JQDatatableBO _objJQDataTableBO)
        {
            string strQuery = "[VMS].[AizantIT_SP_GetOutwardMaterialList]";
            SqlParameter[] arrParam = new SqlParameter[9];
            arrParam[0] = new SqlParameter("@Mode", _objJQDataTableBO.iMode);
            arrParam[1] = new SqlParameter("@OutMaterialID", _objJQDataTableBO.pkid);
            arrParam[2] = new SqlParameter("@DisplayLength", _objJQDataTableBO.iDisplayLength);
            arrParam[3] = new SqlParameter("@DisplayStart", _objJQDataTableBO.iDisplayStart);
            arrParam[4] = new SqlParameter("@SortCol", _objJQDataTableBO.iSortCol);
            arrParam[5] = new SqlParameter("@SortDir", _objJQDataTableBO.sSortDir);
            arrParam[6] = new SqlParameter("@Search", _objJQDataTableBO.sSearch);
            arrParam[7] = new SqlParameter("@EmpID", _objJQDataTableBO.EmpID);
            arrParam[8] = new SqlParameter("@Status", _objJQDataTableBO.Status);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        //Get Manager Outward Material Register Jquery Datatable
        public DataTable GetManagerOutwardMaterialListDb(JQDatatableBO objJQDataTable_BO)
        {
            string strQuery = "[VMS].[AizantIT_SP_GetManagerOutwardMaterialList]";
            SqlParameter[] arrParam = new SqlParameter[9];
            arrParam[0] = new SqlParameter("@Mode", objJQDataTable_BO.iMode);
            arrParam[1] = new SqlParameter("@OutMaterialID", objJQDataTable_BO.pkid);
            arrParam[2] = new SqlParameter("@DisplayLength", objJQDataTable_BO.iDisplayLength);
            arrParam[3] = new SqlParameter("@DisplayStart", objJQDataTable_BO.iDisplayStart);
            arrParam[4] = new SqlParameter("@SortCol", objJQDataTable_BO.iSortCol);
            arrParam[5] = new SqlParameter("@SortDir", objJQDataTable_BO.sSortDir);
            arrParam[6] = new SqlParameter("@Search", objJQDataTable_BO.sSearch);
            arrParam[7] = new SqlParameter("@EmpID", objJQDataTable_BO.EmpID);
            arrParam[8] = new SqlParameter("@Status", objJQDataTable_BO.Status);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        //Get Reverted Outward Material Register Jquery Datatable for user
        public DataTable GetRevertedOutwardMaterialListforUserDb(JQDatatableBO objJQDataTable_BO)
        {
            string strQuery = "[VMS].[AizantIT_SP_GetRevertedOutwardMaterialListforUser]";
            SqlParameter[] arrParam = new SqlParameter[9];
            arrParam[0] = new SqlParameter("@Mode", objJQDataTable_BO.iMode);
            arrParam[1] = new SqlParameter("@OutMaterialID", objJQDataTable_BO.pkid);
            arrParam[2] = new SqlParameter("@DisplayLength", objJQDataTable_BO.iDisplayLength);
            arrParam[3] = new SqlParameter("@DisplayStart", objJQDataTable_BO.iDisplayStart);
            arrParam[4] = new SqlParameter("@SortCol", objJQDataTable_BO.iSortCol);
            arrParam[5] = new SqlParameter("@SortDir", objJQDataTable_BO.sSortDir);
            arrParam[6] = new SqlParameter("@Search", objJQDataTable_BO.sSearch);
            arrParam[7] = new SqlParameter("@EmpID", objJQDataTable_BO.EmpID);
            arrParam[8] = new SqlParameter("@Status", objJQDataTable_BO.Status);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public int OutwardMaterialNameInsert(MaterialBO objmaterialBO)
        {
            string strQuery = "[VMS].[AizantIT_SP_OutwardMaterialName_Insert]";
            SqlParameter[] arrParam = new SqlParameter[3];
            arrParam[0] = new SqlParameter("@MaterialName", objmaterialBO.MaterialName);
            arrParam[1] = new SqlParameter("@CreatedBy", objmaterialBO.CreatedBy);
            arrParam[2] = new SqlParameter("@CreatedDate", objmaterialBO.CreatedDate);
            return Convert.ToInt32(cmmdExecuteScalar(strQuery, CommandType.StoredProcedure, arrParam));
        }

        public DataTable LoadOutwardMaterialNameMaster()
        {
            string strQuery = "[VMS].[AizantIT_SP_GetOutwardMaterialNameMaster]";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }

        public DataTable GetOutwardMaterialGridHistory(MaterialBO objMaterialBO)
        {
            string strQuery = "[VMS].[AizantIT_SP_GetOutwardMaterialHistory]";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@OutMaterialID", objMaterialBO.OutMaterialID);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        //To get Return Quantity Details for outward
        public DataTable GetOutwardMaterialReturnQuantityHistory(MaterialBO objMaterialBO)
        {
            string strQuery = "[VMS].[AizantIT_SP_GetOutwardMaterialReturnDetails]";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@OutMaterialID", objMaterialBO.OutMaterialID);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }
        // To Check Material Name Verifying Data Exits or Not in Material Outward.
        public int MaterialNameifExistsorNot(string MaterialName)
        {
            string strQuery = "VMS.AizantIT_SP_MaterialName_IfExits";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@MaterialName", MaterialName);
            return Convert.ToInt32(cmmdExecuteScalar(strQuery, CommandType.StoredProcedure, arrParam));
        }

        public DataTable GetOutMaterialIDFromNotification(MaterialBO objMaterialBO)
        {
            string strQuery = "VMS.AizantIT_SP_GetOutMaterialIDFromNotification";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@NotificationID", objMaterialBO.NotificationID);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }
        public DataTable GetRoleWiseDepartments(int empId, int RoleID)
        {
            string strQuery = "[VMS].[AizantIT_SP_GetRoleDepts]";
            SqlParameter[] arrParam = new SqlParameter[2];
            arrParam[0] = new SqlParameter("@EmpID", empId);
            arrParam[1] = new SqlParameter("@RoleID", RoleID);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }
        //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
        //public DataTable GetRoleWiseDepartments(int EmpID, int RoleID)
        //{
        //    try
        //    {
                
        //        con.Open();
        //        SqlCommand cmd = new SqlCommand("[VMS].[AizantIT_SP_GetRoleDepts]", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@EmpID", EmpID);
        //        cmd.Parameters.AddWithValue("@RoleID", RoleID);
        //        SqlDataAdapter da = new SqlDataAdapter(cmd);
        //        DataTable dt = new DataTable();
        //        da.Fill(dt);
        //        return dt;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        con.Close();
        //        con.Dispose();
        //    }
        //}

    }
}
