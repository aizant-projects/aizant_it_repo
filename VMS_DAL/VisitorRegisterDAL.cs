﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Drawing;
using VMS_BO;

namespace VMS_DAL
{
    public class VisitorRegisterDAL : SQLHELPER
    {
        public DataTable LoadddlIDProofType()
        {
            string strQuery = "VMS.AizantIT_SP_IDProof_Select";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }

        public string VisitorAutoGenerateID()
        {
            string strQuery = "Select [dbo].[udf_VMS_GetVisitorAutoID]()";
            return cmmdExecuteScalar(strQuery, CommandType.Text, null); 
        }

        public string AppointmentVisitorAutoGenerateID()
        {
            string strQuery = "Select [dbo].[VMS_GetVisitorAutoAppID]()";
            return cmmdExecuteScalar(strQuery, CommandType.Text, null);
        }

        public DataTable LoadDepartment()
        {
            string strQuery = "VMS.AizantIT_SP_Department_Select";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }


        public DataTable LoadPurposeofVisit()
        {
            string strQuery = "VMS.AizantIT_SP_Visitor_PurposeofVisit_Select";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null); 
        }

        public int Visitor_Insert(VMS_BO.VisitorRegisterBO objVisitor)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_Visitor_Insert";
            SqlParameter[] arrParam = new SqlParameter[30];
            arrParam[0] = new SqlParameter("@VisitorID", objVisitor.VisitorID);
            arrParam[1] = new SqlParameter("@FirstName", objVisitor.FirstName);
            arrParam[2] = new SqlParameter("@Lastname", objVisitor.LastName);
            arrParam[3] = new SqlParameter("@IDCardTypeID", objVisitor.IDCardType);
            arrParam[4] = new SqlParameter("@IDCardNo", objVisitor.IDCardNo);
            arrParam[5] = new SqlParameter("@MobileNo", objVisitor.MobileNo);
            arrParam[6] = new SqlParameter("@Address", objVisitor.Address);
            arrParam[7] = new SqlParameter("@CountryID", objVisitor.CountryID);
            arrParam[8] = new SqlParameter("@StateID", objVisitor.StateID);
            arrParam[9] = new SqlParameter("@CityID", objVisitor.CityID);
            arrParam[10] = new SqlParameter("@Email", objVisitor.Email);
            arrParam[11] = new SqlParameter("@Organization", objVisitor.Organization);
            arrParam[12] = new SqlParameter("@Purpose_of_VisitID", objVisitor.Purpose_of_VisitID);
            arrParam[13] = new SqlParameter("@Remarks", objVisitor.Remarks);
            //arrParam[14] = new SqlParameter("@Department", objVisitor.Department);
            arrParam[14] = new SqlParameter("@Whom_To_VisitID", objVisitor.Whom_To_VisitID);
            arrParam[15] = new SqlParameter("@InDateTime", objVisitor.InDateTime);
            arrParam[16] = new SqlParameter("@VisitorIDCardNo", objVisitor.VisitorIDCardNo);
            arrParam[17] = new SqlParameter("@AccessLocation", objVisitor.AccessLocation);
            arrParam[18] = new SqlParameter("@AccompanyPerson", objVisitor.AccompanyPerson);
            arrParam[19] = new SqlParameter("@RoomNo", objVisitor.RoomNo);
            arrParam[20] = new SqlParameter("@NoofVisitors", objVisitor.NoofVisitors);
            arrParam[21] = new SqlParameter("@VechileNo", objVisitor.VechileNo);
            arrParam[22] = new SqlParameter("@Belongings", objVisitor.Belongings); 
            arrParam[23] = new SqlParameter("@PlannedOutTime", objVisitor.PlannedOutTime);
            arrParam[24] = new SqlParameter("@CreatedBy", objVisitor.CreatedBy);
            arrParam[25] = new SqlParameter("@CreatedDate", objVisitor.CreatedDate);
            arrParam[26] = new SqlParameter("@Photo", objVisitor.Photo);
            arrParam[27] = new SqlParameter("@PhotoFileData", (objVisitor.PhotoFileData == null) ? new byte[0] : objVisitor.PhotoFileData);
           // arrParam[28] = new SqlParameter("@AppointmentID", objVisitor.AppointmentID);
            arrParam[28] = new SqlParameter("@DeptName", objVisitor.DeptName);
            arrParam[29] = new SqlParameter("@EmpName", objVisitor.EmpName);
            c1 = Convert.ToInt32(cmmdExecuteScalar(strQuery, CommandType.StoredProcedure, arrParam));
            return c1;
        }

        public DataTable SearchFilterVisitor(VMS_BO.VisitorRegisterBO objvisitor)
        {
            string strQuery = "VMS.AizantIT_SP_VisitorsSearch_Select";
            SqlParameter[] arrParam = new SqlParameter[10];
            arrParam[0] = new SqlParameter("@FirstName", objvisitor.FirstName);
            arrParam[1] = new SqlParameter("@Purpose_of_VisitID", objvisitor.Purpose_of_VisitID);
            arrParam[2] = new SqlParameter("@VisitorID", objvisitor.VisitorID);
            arrParam[3] = new SqlParameter("@VisitorMobile", objvisitor.MobileNo);
            arrParam[4] = new SqlParameter("@VisitorOrganization", objvisitor.Organization);
            arrParam[5] = new SqlParameter("@OutDateTime", objvisitor.InDateTime);
            arrParam[6] = new SqlParameter("@OutDateTime1", objvisitor.OutDateTime);
            arrParam[7] = new SqlParameter("@Department", objvisitor.DeptName);
            arrParam[8] = new SqlParameter("@IDProofNo", objvisitor.IDCardNo);
            arrParam[9] = new SqlParameter("@SNo", objvisitor.SNo);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public int Update_Visitor(VMS_BO.VisitorRegisterBO objvisitor)
        {
            int c = 0;
            string strQuery = "VMS.AizantIT_SP_Visitor_Update";
            SqlParameter[] arrParam = new SqlParameter[29];
            arrParam[0] = new SqlParameter("@tempid", objvisitor.SNo);
            arrParam[1] = new SqlParameter("@FirstName", objvisitor.FirstName);
            arrParam[2] = new SqlParameter("@Lastname", objvisitor.LastName);
            arrParam[3] = new SqlParameter("@MobileNo", objvisitor.MobileNo);
            arrParam[4] = new SqlParameter("@Email", objvisitor.Email);
            arrParam[5] = new SqlParameter("@Purpose_of_VisitID", objvisitor.Purpose_of_VisitID);
            //arrParam[6] = new SqlParameter("@Department", objvisitor.Department);
            arrParam[6] = new SqlParameter("@PlannedOutTime", objvisitor.PlannedOutTime);
            arrParam[7] = new SqlParameter("@Whom_To_VisitID", objvisitor.Whom_To_VisitID);
            arrParam[8] = new SqlParameter("@NoofVisitors", objvisitor.NoofVisitors);
            arrParam[9] = new SqlParameter("@VechileNo", objvisitor.VechileNo);
            arrParam[10] = new SqlParameter("@EditedBy", objvisitor.LastChangedBy);
            arrParam[11] = new SqlParameter("@EditedDateAndTime", objvisitor.LastChangedDate);
            arrParam[12] = new SqlParameter("@EditedByComments", objvisitor.LastChangedComments);
            arrParam[13] = new SqlParameter("@IDCardTypeID", objvisitor.IDCardType);
            arrParam[14] = new SqlParameter("@IDCardNo", objvisitor.IDCardNo);
            arrParam[15] = new SqlParameter("@Address", objvisitor.Address);
            arrParam[16] = new SqlParameter("@CountryID", objvisitor.CountryID);
            arrParam[17] = new SqlParameter("@StateID", objvisitor.StateID);
            arrParam[18] = new SqlParameter("@CityID", objvisitor.CityID);
            arrParam[19] = new SqlParameter("@Organization", objvisitor.Organization);
            arrParam[20] = new SqlParameter("@Remarks", objvisitor.Remarks);
            arrParam[21] = new SqlParameter("@VisitorID", objvisitor.VisitorID);
            //arrParam[21] = new SqlParameter("@InDateTime", objvisitor.InDateTime); 
            arrParam[22] = new SqlParameter("@VisitorIDCardNo", objvisitor.VisitorIDCardNo);
            arrParam[23] = new SqlParameter("@AccessLocation", objvisitor.AccessLocation);
            arrParam[24] = new SqlParameter("@AccompanyPerson", objvisitor.AccompanyPerson);
            arrParam[25] = new SqlParameter("@RoomNo", objvisitor.RoomNo);
            arrParam[26] = new SqlParameter("@Belongings", objvisitor.Belongings);
            arrParam[27] = new SqlParameter("@DeptName", objvisitor.DeptName);
            arrParam[28] = new SqlParameter("@EmpName", objvisitor.EmpName);
            c = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c;
        }

        public DataTable BindDataVisitor()
        {
            string strQuery = "VMS.AizantIT_SP_Visitor_BindData";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }

        public DataTable VisitorlinkbuttonFilldata(VMS_BO.VisitorRegisterBO objVisitor)
        {
            string strQuery = "VMS.AizantIT_SP_Visitor_GridViewLinkbutton";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@tempid", objVisitor.SNo);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public DataSet VisitorReports(string VistorID)
        {
            string strQuery = "VMS.AizantIT_SP_Visitor_GridViewLinkbutton";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@tempid", VistorID);
            return GetDataSet(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public int PurposeOfVisitUpdate(VMS_BO.VisitorRegisterBO objvisitor)
        {
            string strQuery = "VMS.AizantIT_SP_PurposeofVisit_Insert";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@Purpose_of_Visit", objvisitor.PurposeofVisitTxt);
            return Convert.ToInt32(cmmdExecuteScalar(strQuery, CommandType.StoredProcedure, arrParam));
        }

        public int IDProofTypeUpdate(VMS_BO.VisitorRegisterBO objvisitor)
        {
            string strQuery = "VMS.AizantIT_SP_VisitorIDProof_Insert";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@IDCardTypeName", objvisitor.IDProofTxt);
            return Convert.ToInt32(cmmdExecuteScalar(strQuery, CommandType.StoredProcedure, arrParam));
        }

        public int BookAppointment(VisitorAppointmentBookingsBO objVisitorAppointmentBookingBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_AppointmentBooking_Insert";
            SqlParameter[] arrParam = new SqlParameter[14];
            arrParam[0] = new SqlParameter("@Appointment_ID", objVisitorAppointmentBookingBO.Appointment_ID);
            arrParam[1] = new SqlParameter("@Appointment_NAME", objVisitorAppointmentBookingBO.Appointment_NAME);
            arrParam[2] = new SqlParameter("@Appointment_MOBILE_NO", objVisitorAppointmentBookingBO.Appointment_MOBILE_NO);
            arrParam[3] = new SqlParameter("@Appointment_EMAILID", objVisitorAppointmentBookingBO.Appointment_EMAILID);
            arrParam[4] = new SqlParameter("@Appointment_ORGANISATION", objVisitorAppointmentBookingBO.Appointment_ORGANISATION);
            arrParam[5] = new SqlParameter("@Appointment_DATE", objVisitorAppointmentBookingBO.Appointment_DATE);
            arrParam[6] = new SqlParameter("@Appointment_TIME", objVisitorAppointmentBookingBO.Appointment_TIME);
            arrParam[7] = new SqlParameter("@Appointment_DEPARTMENT_TOVISIT", objVisitorAppointmentBookingBO.Appointment_DEPARTMENT_TOVISIT);
            arrParam[8] = new SqlParameter("@Appointment_WHOMTO_VISIT", objVisitorAppointmentBookingBO.Appointment_WHOMTO_VISIT);
            arrParam[9] = new SqlParameter("@Appointment_PURPOSEOF_VISIT", objVisitorAppointmentBookingBO.Appointment_PURPOSEOF_VISIT);
            arrParam[10] = new SqlParameter("@Appointment_REMARKS", objVisitorAppointmentBookingBO.Appointment_REMARKS);
            arrParam[11] = new SqlParameter("@CreatedBy", objVisitorAppointmentBookingBO.CreatedBy);
            arrParam[12] = new SqlParameter("@CreatedDate", objVisitorAppointmentBookingBO.CreatedDate);
            arrParam[13] = new SqlParameter("@Appointment_LastName", objVisitorAppointmentBookingBO.Appointment_LastName);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public DataTable SearchFilterAppointment(VisitorAppointmentBookingsBO objVisitorAppointmentBookingBO)
        {
            string strQuery = "VMS.AizantIT_SP_AppointmentsSearch_Select";
            SqlParameter[] arrParam = new SqlParameter[9];
            arrParam[0] = new SqlParameter("@FirstName", objVisitorAppointmentBookingBO.Appointment_NAME);
            arrParam[1] = new SqlParameter("@Purpose_of_VisitID", objVisitorAppointmentBookingBO.Appointment_PURPOSEOF_VISIT);
            arrParam[2] = new SqlParameter("@Appointment_ID", objVisitorAppointmentBookingBO.Appointment_ID);
            arrParam[3] = new SqlParameter("@Appointment_MOBILE_NO", objVisitorAppointmentBookingBO.Appointment_MOBILE_NO);
            arrParam[4] = new SqlParameter("@Appointment_ORGANISATION", objVisitorAppointmentBookingBO.Appointment_ORGANISATION);
            arrParam[5] = new SqlParameter("@AppointmentDateTime", objVisitorAppointmentBookingBO.Appointment_DATE);
            arrParam[6] = new SqlParameter("@OutDateTime1", objVisitorAppointmentBookingBO.Appointment_DATE);//ask sir
            arrParam[7] = new SqlParameter("@Department", objVisitorAppointmentBookingBO.Appointment_DEPARTMENT_TOVISIT);
            arrParam[8] = new SqlParameter("@SNo", objVisitorAppointmentBookingBO.Appointment_SNO);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public int Update_Appointment(VisitorAppointmentBookingsBO objVisitorAppointmentBookingBO)
        {
            int c = 0;
            string strQuery = "VMS.AizantIT_SP_Appointment_Update";
            SqlParameter[] arrParam = new SqlParameter[15];
            arrParam[0] = new SqlParameter("@tempid", objVisitorAppointmentBookingBO.Appointment_SNO);
            arrParam[1] = new SqlParameter("@Appointment_NAME", objVisitorAppointmentBookingBO.Appointment_NAME);
            arrParam[2] = new SqlParameter("@Appointment_LastName", objVisitorAppointmentBookingBO.Appointment_LastName);
            arrParam[3] = new SqlParameter("@Appointment_MOBILE_NO", objVisitorAppointmentBookingBO.Appointment_MOBILE_NO);
            arrParam[4] = new SqlParameter("@Appointment_EMAILID", objVisitorAppointmentBookingBO.Appointment_EMAILID);
            arrParam[5] = new SqlParameter("@Appointment_PURPOSEOF_VISIT", objVisitorAppointmentBookingBO.Appointment_PURPOSEOF_VISIT);
            arrParam[6] = new SqlParameter("@Appointment_TIME", objVisitorAppointmentBookingBO.Appointment_TIME);
            arrParam[7] = new SqlParameter("@Appointment_WHOMTO_VISIT", objVisitorAppointmentBookingBO.Appointment_WHOMTO_VISIT);
            arrParam[8] = new SqlParameter("@Appointment_DATE", objVisitorAppointmentBookingBO.Appointment_DATE);
            arrParam[9] = new SqlParameter("@EditedBy", objVisitorAppointmentBookingBO.LastChangedBy);
            arrParam[10] = new SqlParameter("@EditedDateAndTime", objVisitorAppointmentBookingBO.LastChangedDate);
            arrParam[11] = new SqlParameter("@EditedByComments", objVisitorAppointmentBookingBO.LastChangedComments);
            arrParam[12] = new SqlParameter("@Appointment_ORGANISATION", objVisitorAppointmentBookingBO.Appointment_ORGANISATION);
            arrParam[13] = new SqlParameter("@Appointment_REMARKS", objVisitorAppointmentBookingBO.Appointment_REMARKS);
            arrParam[14] = new SqlParameter("@Appointment_ID", objVisitorAppointmentBookingBO.Appointment_ID);
            c = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c;
        }

        public DataTable GetAppointmentData(VMS_BO.VisitorAppointmentBookingsBO objVisitorAppointmentBookingBO)
        {
            string strQuery = "VMS.AizantIT_SP_Visitor_GetAppointmentData";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@tempid", objVisitorAppointmentBookingBO.Appointment_ID);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public DataTable GetAppointmentDataBind(VMS_BO.VisitorAppointmentBookingsBO objVisitorAppointmentBookingBO)
        {
            string strQuery = "VMS.AizantIT_SP_Visitor_GetAppointmentDataBind";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@GetDate", DateTime.Now.ToString("yyyyMMdd"));
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public int CheckoutVisitor(VisitorRegisterBO objvisitorRegisterBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_VisitorRegister_CheckOut";
            SqlParameter[] arrParam = new SqlParameter[2];
            arrParam[0] = new SqlParameter("@tempid", objvisitorRegisterBO.SNo);
            arrParam[1] = new SqlParameter("@outdatetime", objvisitorRegisterBO.OutDateTime);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public DataTable BindCountriesDAL()
        {
            string strQuery = "UMS.AizantIT_SP_GetCountries";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }

        public DataTable BindStatesDAL(int CountryID)
        {
            string strQuery = "UMS.AizantIT_SP_GetStates";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@CountryID", CountryID);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public DataTable BindCities(int StateID)
        {
            string strQuery = "VMS.AizantIT_SP_GetCities";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@StateID", StateID);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public DataTable GetEmpData(int DeptID)
        {
            string strQuery = "VMS.AizantIT_GetEMPID";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@DeptID", DeptID);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public int IDCardTypeifExistsorNot(string IDCardType)
        {
            string strQuery = "VMS.AizantIT_SP_IDCardTypeMaster_IfExits";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@IDCardTypeName", IDCardType);
            return Convert.ToInt32(cmmdExecuteScalar(strQuery, CommandType.StoredProcedure, arrParam));
        }

        public int PurposeofVisitifExistsorNot(string Purpose)
        {
            string strQuery = "VMS.AizantIT_SP_PurposeofVisit_IfExits";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@Purpose_of_Visit", Purpose);
            return Convert.ToInt32(cmmdExecuteScalar(strQuery, CommandType.StoredProcedure, arrParam));
        }

        public DataTable AutoComplete(string Name)
        {
            string strQuery = "VMS.AizantIT_SP_Visitor_GridViewLinkbutton";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@tempid", Name);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public int AppointmentCancel(VisitorAppointmentBookingsBO objVisitorAppointmentBookingBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_AppointmentStatusCancel";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@AppointmentID", objVisitorAppointmentBookingBO.Appointment_ID);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }
        #region Charts and Export
        public DataTable LoadChartNoofVisitors(VisitorRegisterBO objVisitorRegisterBO)
        {
            string strQuery = "[VMS].[AizantIT_SP_VisitorsGetDatatoChartNoofVisitors]";
            SqlParameter[] arrParam = new SqlParameter[7];
            arrParam[0] = new SqlParameter("@FromDate", objVisitorRegisterBO.InDateTime);
            arrParam[1] = new SqlParameter("@ToDate", objVisitorRegisterBO.OutDateTime);
            arrParam[2] = new SqlParameter("@Mode", objVisitorRegisterBO.Mode);
            arrParam[3] = new SqlParameter("@FromYear", objVisitorRegisterBO.FromYear);
            arrParam[4] = new SqlParameter("@Toyear", objVisitorRegisterBO.ToYear);
            arrParam[5] = new SqlParameter("@FromMonth", objVisitorRegisterBO.FromMonth);
            arrParam[6] = new SqlParameter("@ToMonth", objVisitorRegisterBO.ToMonth);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public DataTable LoadChartPurposeofVisit(VisitorRegisterBO objVisitorRegisterBO)
        {
            string strQuery = "[VMS].[AizantIT_SP_VisitorsGetDatatoChartPurpose]";
            SqlParameter[] arrParam = new SqlParameter[7];
            arrParam[0] = new SqlParameter("@FromDate", objVisitorRegisterBO.InDateTime);
            arrParam[1] = new SqlParameter("@ToDate", objVisitorRegisterBO.OutDateTime);
            arrParam[2] = new SqlParameter("@Mode", objVisitorRegisterBO.Mode);
            arrParam[3] = new SqlParameter("@FromYear", objVisitorRegisterBO.FromYear);
            arrParam[4] = new SqlParameter("@Toyear", objVisitorRegisterBO.ToYear);
            arrParam[5] = new SqlParameter("@FromMonth", objVisitorRegisterBO.FromMonth);
            arrParam[6] = new SqlParameter("@ToMonth", objVisitorRegisterBO.ToMonth);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }
        public DataTable LoadChartVehicle(VisitorRegisterBO objVisitorRegisterBO)
        {
            string strQuery = "[VMS].[AizantIT_SP_Getvehiclechart]";
            SqlParameter[] arrParam = new SqlParameter[7];
            arrParam[0] = new SqlParameter("@FromDate", objVisitorRegisterBO.InDateTime);
            arrParam[1] = new SqlParameter("@ToDate", objVisitorRegisterBO.OutDateTime);
            arrParam[2] = new SqlParameter("@Mode", objVisitorRegisterBO.Mode);
            arrParam[3] = new SqlParameter("@FromYear", objVisitorRegisterBO.FromYear);
            arrParam[4] = new SqlParameter("@Toyear", objVisitorRegisterBO.ToYear);
            arrParam[5] = new SqlParameter("@FromMonth", objVisitorRegisterBO.FromMonth);
            arrParam[6] = new SqlParameter("@ToMonth", objVisitorRegisterBO.ToMonth);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public DataTable GetExportData(VisitorRegisterBO objVisitorRegisterBO)
        {
            string strQuery = "[VMS].[AizantIT_SP_VisitorsExportReport]";
            SqlParameter[] arrParam = new SqlParameter[7];
            switch (objVisitorRegisterBO.SNo)           
            {
                case 1:
                     arrParam = new SqlParameter[10];
                    strQuery = "[VMS].[AizantIT_SP_VisitorsExportReport]";
                    arrParam[0] = new SqlParameter("@FromDate", objVisitorRegisterBO.InDateTime);
                    arrParam[1] = new SqlParameter("@ToDate", objVisitorRegisterBO.OutDateTime);
                    arrParam[2] = new SqlParameter("@Mode", objVisitorRegisterBO.Mode);
                    arrParam[3] = new SqlParameter("@FromYear", objVisitorRegisterBO.FromYear);
                    arrParam[4] = new SqlParameter("@Toyear", objVisitorRegisterBO.ToYear);
                    arrParam[5] = new SqlParameter("@FromMonth", objVisitorRegisterBO.FromMonth);
                    arrParam[6] = new SqlParameter("@ToMonth", objVisitorRegisterBO.ToMonth);
                    arrParam[7] = new SqlParameter("@Department", objVisitorRegisterBO.DeptName);
                    arrParam[8] = new SqlParameter("@PurposeofVisit", objVisitorRegisterBO.Purpose_of_VisitID);
                    arrParam[9] = new SqlParameter("@organization", objVisitorRegisterBO.Organization);
                    break;
                case 2:
                    arrParam = new SqlParameter[8];
                    strQuery = "[VMS].[AizantIT_SP_vehicleExportReport]";
                    arrParam[0] = new SqlParameter("@FromDate", objVisitorRegisterBO.InDateTime);
                    arrParam[1] = new SqlParameter("@ToDate", objVisitorRegisterBO.OutDateTime);
                    arrParam[2] = new SqlParameter("@Mode", objVisitorRegisterBO.Mode);
                    arrParam[3] = new SqlParameter("@FromYear", objVisitorRegisterBO.FromYear);
                    arrParam[4] = new SqlParameter("@Toyear", objVisitorRegisterBO.ToYear);
                    arrParam[5] = new SqlParameter("@FromMonth", objVisitorRegisterBO.FromMonth);
                    arrParam[6] = new SqlParameter("@ToMonth", objVisitorRegisterBO.ToMonth);
                    arrParam[7] = new SqlParameter("@VechileNo", objVisitorRegisterBO.DeptName);//Vechileno
                    break;
            }           
           
           
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }
        #endregion Charts and Export
        public DataTable TestMethod()
        {
            string strQuery = "VMS.AizantIT_SP_Visitor_BindData";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }

        //Get Visitor Details Datatable
         public DataTable GetVisitorDetails_ListDb(JQDatatableBO _objJQDataTableBO)
         {
            string strQuery = "[VMS].[AizantIT_SP_GetVisitorDetails]";
            SqlParameter[] arrParam = new SqlParameter[7];
            arrParam[0] = new SqlParameter("@Mode", _objJQDataTableBO.iMode);
            arrParam[1] = new SqlParameter("@SNo", _objJQDataTableBO.pkid);
            arrParam[2] = new SqlParameter("@DisplayLength", _objJQDataTableBO.iDisplayLength);
            arrParam[3] = new SqlParameter("@DisplayStart", _objJQDataTableBO.iDisplayStart);
            arrParam[4] = new SqlParameter("@SortCol", _objJQDataTableBO.iSortCol);
            arrParam[5] = new SqlParameter("@SortDir", _objJQDataTableBO.sSortDir);
            arrParam[6] = new SqlParameter("@Search", _objJQDataTableBO.sSearch);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
         }

        //Get Appointment Details Datatable
        public DataTable GetVisitorAppointmentDetails_ListDb(JQDatatableBO _objJQDataTableBO)
        {
            string strQuery = "[VMS].[AizantIT_SP_GetAppointmentsDetails]";
            SqlParameter[] arrParam = new SqlParameter[7];
            arrParam[0] = new SqlParameter("@Mode", _objJQDataTableBO.iMode);
            arrParam[1] = new SqlParameter("@Appointment_SNO", _objJQDataTableBO.pkid);
            arrParam[2] = new SqlParameter("@DisplayLength", _objJQDataTableBO.iDisplayLength);
            arrParam[3] = new SqlParameter("@DisplayStart", _objJQDataTableBO.iDisplayStart);
            arrParam[4] = new SqlParameter("@SortCol", _objJQDataTableBO.iSortCol);
            arrParam[5] = new SqlParameter("@SortDir", _objJQDataTableBO.sSortDir);
            arrParam[6] = new SqlParameter("@Search", _objJQDataTableBO.sSearch);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public DataTable GetVisitorChartDetails_ListDb(JQDatatableBO _objJQDataTableBO,string FromDate,string ToDate,int Mode,int FromYear,int ToYear,int FromMonth,int ToMonth,string purposeofvisit,string TimePeriod)
        {
            try
            {
                string strQuery = "[VMS].[AizantIT_SP_VisitorsGetListByChart]";
                SqlParameter[] arrParam = new SqlParameter[13];
                arrParam[0] = new SqlParameter("@DisplayLength", _objJQDataTableBO.iDisplayLength);
                arrParam[1] = new SqlParameter("@DisplayStart", _objJQDataTableBO.iDisplayStart);
                arrParam[2] = new SqlParameter("@SortCol", _objJQDataTableBO.iSortCol);
                arrParam[3] = new SqlParameter("@SortDir", _objJQDataTableBO.sSortDir);
                arrParam[4] = new SqlParameter("@FromDate", _objJQDataTableBO.sSearch);
                arrParam[5] = new SqlParameter("@ToDate", _objJQDataTableBO.sSearch);
                arrParam[6] = new SqlParameter("@Mode", _objJQDataTableBO.sSearch);
                arrParam[7] = new SqlParameter("@FromYear", _objJQDataTableBO.sSearch);
                arrParam[8] = new SqlParameter("@Toyear", _objJQDataTableBO.sSearch);
                arrParam[9] = new SqlParameter("@FromMonth", _objJQDataTableBO.sSearch);
                arrParam[10] = new SqlParameter("@ToMonth", _objJQDataTableBO.sSearch);
                arrParam[11] = new SqlParameter("@purposeofvisit", _objJQDataTableBO.sSearch);
                arrParam[12] = new SqlParameter("@TimePeriod", _objJQDataTableBO.sSearch);
                return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
    }
