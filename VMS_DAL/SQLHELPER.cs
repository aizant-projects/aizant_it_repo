﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Runtime.InteropServices;

namespace VMS_DAL
{
    public class SQLHELPER
    {
        #region Global variables

        int intTimeOut = 120;//Convert.ToInt32(ConfigurationManager.AppSettings["SqlConnTimeOut"].ToString());
        #endregion

        public int cmmdExecuteNonQuery(string ProcName, CommandType cmdTYpe, SqlParameter[] param)
        {
            //for execute non queryt the Table
            int result = 0;
            try
            {

                using (SqlConnection con = GetConnectionString())
                {
                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = ProcName;
                        cmd.CommandType = cmdTYpe;
                        if (param != null)
                        {
                            cmd.Parameters.AddRange(param);
                        }
                        if (con.State != ConnectionState.Open)
                            con.Open();
                        result = cmd.ExecuteNonQuery();
                        if (con.State != ConnectionState.Closed)
                            con.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //if (GetConnectionString().State != ConnectionState.Closed)
                //    GetConnectionString().Close();
            }
            return result;

        }
        public string cmmdExecuteScalar(string ProcName, CommandType cmdTYpe, SqlParameter[] param)
        {
            //for select the Table
            string result = string.Empty;
            try
            {
                using (SqlConnection con = GetConnectionString())
                {
                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = ProcName;
                        cmd.CommandType = cmdTYpe;
                        if (param != null)
                        {
                            cmd.Parameters.AddRange(param);
                        }
                        if (con.State != ConnectionState.Open)
                            con.Open();
                        result = Convert.ToString(cmd.ExecuteScalar());
                        if (con.State != ConnectionState.Closed)
                            con.Close();

                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //if (GetConnectionString().State != ConnectionState.Closed)
                //    GetConnectionString().Close();
            }


            return result;

        }

        public string SQLEscapeString(string usString)
        {

            if (usString == null)
            {

                return null;

            }

            // it escapes \r, \n, \x00, \x1a, \, ', and "

            return System.Text.RegularExpressions.Regex.Replace(usString, @"[\r\n\x00\x1a\\'""]", @"\$0");

        }


        public string SQLEscapeString(string usString, [Optional, DefaultParameterValue(false)] bool blnOptNumFlag)
        {
            //public string SQLEscapeString(string usString, [Optional, DefaultParameterValue(false)] bool blnOptNumFlag )
            if ((usString == "") && (blnOptNumFlag == true))
            {

                return "0";
            }


            if (usString == null)
            {

                return null;

            }

            // it escapes \r, \n, \x00, \x1a, \, ', and "

            return System.Text.RegularExpressions.Regex.Replace(usString, @"[\r\n\x00\x1a\\'""]", @"\$0");

        }
        public SqlConnection GetConnectionString()
        {
            string str = ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ConnectionString;
             SqlConnection conn = new SqlConnection(str);
            return conn;
        }

        public DataTable GetDataTable(string ProcName, CommandType cmdTYpe, SqlParameter[] param)
        {
            DataTable dt = null;
            using (SqlConnection con = GetConnectionString())
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = ProcName;
                    cmd.CommandType = cmdTYpe;
                    if (param != null)
                    {
                        cmd.Parameters.AddRange(param);
                    }
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        dt = new DataTable();
                        dt.Clear();
                        da.Fill(dt);
                    }
                }
                return dt;
            }
        }
        public DataSet GetDataSet(string ProcName, CommandType cmdTYpe, SqlParameter[] param)
        {
            DataSet ds = null;
            using (SqlConnection con = GetConnectionString())
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = ProcName;
                    cmd.CommandType = cmdTYpe;
                    if (param != null)
                    {
                        cmd.Parameters.AddRange(param);
                    }
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        ds = new DataSet();
                        ds.Clear();
                        da.Fill(ds);
                    }
                }
                return ds;
            }
        }
        private  void ExecuteSqlTransaction(string ProcName, CommandType cmdTYpe, SqlParameter[] param)
        {
            using (SqlConnection connection = GetConnectionString())
            {
                
                if (connection.State != ConnectionState.Open)
                    connection.Open();
              
                SqlCommand command = connection.CreateCommand();
                SqlTransaction transaction;
                command.CommandType = cmdTYpe;
                if (param != null)
                {
                    command.Parameters.AddRange(param);
                }
                // Start a local transaction.
                transaction = connection.BeginTransaction("CRUDTransaction");

                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                command.Connection = connection;
                command.Transaction = transaction;

                try
                {
                    command.CommandText =ProcName;
                    command.ExecuteNonQuery();
                   

                    // Attempt to commit the transaction.
                    transaction.Commit();
                   
                }
                catch (Exception ex)
                {
                    //Console.WriteLine("Commit Exception Type: {0}", ex.GetType());
                    //Console.WriteLine("  Message: {0}", ex.Message);

                    // Attempt to roll back the transaction. 
                    try
                    {
                        transaction.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        // This catch block will handle any errors that may have occurred 
                        // on the server that would cause the rollback to fail, such as 
                        // a closed connection.
                        //Console.WriteLine("Rollback Exception Type: {0}", ex2.GetType());
                        //Console.WriteLine("  Message: {0}", ex2.Message);
                    }
                }

                if (connection.State != ConnectionState.Closed)
                    connection.Close();
            }
        }
    }
}
