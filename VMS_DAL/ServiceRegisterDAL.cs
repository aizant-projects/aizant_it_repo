﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using VMS_BO;

namespace VMS_DAL
{
    public class ServiceRegisterDAL : SQLHELPER
    {
        public int ServiceRegister_Insert(ServiceAndVechileBO objServiceAndVechileBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_ServiceRegister_Insert";
            SqlParameter[] arrParam = new SqlParameter[12];
            arrParam[0] = new SqlParameter("@ServiceTypeID", objServiceAndVechileBO.ServiceType);
            arrParam[1] = new SqlParameter("@FirstName", objServiceAndVechileBO.FirstName);
            arrParam[2] = new SqlParameter("@MobileNO", objServiceAndVechileBO.MobileNo);
            arrParam[3] = new SqlParameter("@CommingFrom", objServiceAndVechileBO.CommingFrom);
            arrParam[4] = new SqlParameter("@VechileNo", objServiceAndVechileBO.VechileNo);
            arrParam[5] = new SqlParameter("@PurposeofVisit", objServiceAndVechileBO.Purpose_of_Visit);
            arrParam[6] = new SqlParameter("@WhomToVisitID", objServiceAndVechileBO.Whom_To_Visit);
            arrParam[7] = new SqlParameter("@Remarks", objServiceAndVechileBO.Remarks);
            arrParam[8] = new SqlParameter("@InDateTime", objServiceAndVechileBO.InDateTime);
            arrParam[9] = new SqlParameter("@CreatedBy", objServiceAndVechileBO.CreatedBy);
            arrParam[10] = new SqlParameter("@CreatedDate", objServiceAndVechileBO.CreatedDate);
            arrParam[11] = new SqlParameter("@LastName", objServiceAndVechileBO.LastName);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public DataTable SearchFilterService(ServiceAndVechileBO objServiceAndVechileBO)
        {
            string strQuery = "VMS.AizantIT_SP_ServiceRegister_Select";
            SqlParameter[] arrParam = new SqlParameter[5];
            arrParam[0] = new SqlParameter("@servicetypeID", objServiceAndVechileBO.ServiceType);
            arrParam[1] = new SqlParameter("@FirstName", objServiceAndVechileBO.FirstName);
            arrParam[2] = new SqlParameter("@indatetime", objServiceAndVechileBO.InDateTime);
            arrParam[3] = new SqlParameter("@indatetime1", objServiceAndVechileBO.OutDateTime);
            arrParam[4] = new SqlParameter("@Department", objServiceAndVechileBO.Department);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public int Update_ServiceRegisterEdit(ServiceAndVechileBO objServiceAndVechileBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_ServiceRegister_Update";
            SqlParameter[] arrParam = new SqlParameter[13];
            arrParam[0] = new SqlParameter("@tempid", objServiceAndVechileBO.SNo);
            arrParam[1] = new SqlParameter("@ServiceTypeID", objServiceAndVechileBO.ServiceType);
            arrParam[2] = new SqlParameter("@FirstName", objServiceAndVechileBO.FirstName);
            arrParam[3] = new SqlParameter("@MobileNO", objServiceAndVechileBO.MobileNo);
            arrParam[4] = new SqlParameter("@CommingFrom", objServiceAndVechileBO.CommingFrom);
            arrParam[5] = new SqlParameter("@VechileNo", objServiceAndVechileBO.VechileNo);
            arrParam[6] = new SqlParameter("@PurposeofVisit", objServiceAndVechileBO.Purpose_of_Visit);
            arrParam[7] = new SqlParameter("@WhomToVisitID", objServiceAndVechileBO.Whom_To_Visit);
            arrParam[8] = new SqlParameter("@LastChangedBy", objServiceAndVechileBO.LastChangedBy);
            arrParam[9] = new SqlParameter("@LastChangedDate", objServiceAndVechileBO.LastChangedDate);
            arrParam[10] = new SqlParameter("@LastChangedByComments", objServiceAndVechileBO.LastChangedComments);
            arrParam[11] = new SqlParameter("@LastName", objServiceAndVechileBO.LastName);
            arrParam[12] = new SqlParameter("@Remarks", objServiceAndVechileBO.Remarks);
            //arrParam[13] = new SqlParameter("@InDateTime", objServiceAndVechileBO.InDateTime);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public DataTable BindDataServiceRegister()
        {
            string strQuery = "VMS.AizantIT_SP_ServiceRegister_BindData";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }

        public DataTable ServiceRegisterlinkbuttonFilldata(ServiceAndVechileBO objServiceAndVechileBO)
        {
            string strQuery = "VMS.AizantIT_SP_ServiceRegister_GridViewLinkbutton";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@tempid", objServiceAndVechileBO.SNo);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public DataTable BindDataServiceType()
        {
            string strQuery = "VMS.AizantIT_SP_ServiceTypeMaster_Select";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }

        public int ServiceTypeMasterInsert(ServiceAndVechileBO objServiceAndVechileBO)
        {
            string strQuery = "VMS.AizantIT_SP_ServiceTypeMaster_Insert";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@ServiceTye", objServiceAndVechileBO.ServiceType);
            return Convert.ToInt32(cmmdExecuteScalar(strQuery, CommandType.StoredProcedure, arrParam));
        }

        public int CheckoutService(ServiceAndVechileBO objServiceAndVechileBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_Service_CheckOut";
            SqlParameter[] arrParam = new SqlParameter[2];
            arrParam[0] = new SqlParameter("@tempid", objServiceAndVechileBO.SNo);
            arrParam[1] = new SqlParameter("@OutDateTime", objServiceAndVechileBO.OutDateTime);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public int ServiceTypeifExistsorNot(string ServiceType)
        {
            string strQuery = "VMS.AizantIT_SP_ServiceTypeMaster_IfExits";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@ServiceTye", ServiceType);
            return Convert.ToInt32(cmmdExecuteScalar(strQuery, CommandType.StoredProcedure, arrParam));
        }
    }
}
