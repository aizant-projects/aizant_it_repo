﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Drawing;
using VMS_BO;

namespace VMS_DAL
{
    public class VisitorFacilitiesDAL : SQLHELPER
    {
        public int VisitorFacilities_Insert(VisitorFacilitiesBO objVisitorFacilitiesBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_VisitorFacilities_Insert";
            SqlParameter[] arrParam = new SqlParameter[20];
            arrParam[0] = new SqlParameter("@EmpID", objVisitorFacilitiesBO.EmpID);
            arrParam[1] = new SqlParameter("@CreatedDate", objVisitorFacilitiesBO.CreatedDate);
            arrParam[2] = new SqlParameter("@DateofRequest", objVisitorFacilitiesBO.DateofRequest);
            arrParam[3] = new SqlParameter("@ClientID", objVisitorFacilitiesBO.ClientID);
            arrParam[4] = new SqlParameter("@NoofVisitors", objVisitorFacilitiesBO.NoofVisitors);
            arrParam[5] = new SqlParameter("@DateofVisit", objVisitorFacilitiesBO.DateofVisit);
            arrParam[6] = new SqlParameter("@TravelArrangement", objVisitorFacilitiesBO.TravelArrangement);
            arrParam[7] = new SqlParameter("@T_ECost", objVisitorFacilitiesBO.T_ECost);
            arrParam[8] = new SqlParameter("@T_ACost", objVisitorFacilitiesBO.T_ACost);
            arrParam[9] = new SqlParameter("@MSnacks", objVisitorFacilitiesBO.MSnacks);
            arrParam[10] = new SqlParameter("@MS_ECost", objVisitorFacilitiesBO.MS_ECost);
            arrParam[11] = new SqlParameter("@MS_ACost", objVisitorFacilitiesBO.MS_ACost);
            arrParam[12] = new SqlParameter("@AFSnacks", objVisitorFacilitiesBO.AFSnacks);
            arrParam[13] = new SqlParameter("@AF_ECost", objVisitorFacilitiesBO.AF_ECost);
            arrParam[14] = new SqlParameter("@AF_ACost", objVisitorFacilitiesBO.AF_ACost);
            arrParam[15] = new SqlParameter("@ESnacks", objVisitorFacilitiesBO.ESnacks);
            arrParam[16] = new SqlParameter("@ES_ECost", objVisitorFacilitiesBO.ES_ECost);
            arrParam[17] = new SqlParameter("@ES_ACost", objVisitorFacilitiesBO.ES_ACost);
            arrParam[18] = new SqlParameter("@Remarks", objVisitorFacilitiesBO.Remarks);
            arrParam[19] = new SqlParameter("@DeptID", objVisitorFacilitiesBO.DeptID);
            c1 = Convert.ToInt32(cmmdExecuteScalar(strQuery, CommandType.StoredProcedure, arrParam));
            return c1;
        }

        public DataSet GetEmpDataToVisitorFacilities(VisitorFacilitiesBO objVisitorFacilitiesBO)
        {
            string strQuery = "VMS.AizantIT_SP_VFGetEmpData";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@EmpID", objVisitorFacilitiesBO.EmpID);
            return GetDataSet(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public DataTable SearchFilterVisitorFacilities(VisitorFacilitiesBO objVisitorFacilitiesBO)
        {
            string strQuery = "VMS.AizantIT_SP_VisitorFacilitiesList_Select";
            SqlParameter[] arrParam = new SqlParameter[6];
            arrParam[0] = new SqlParameter("@ClientName",objVisitorFacilitiesBO.ClientName);
            arrParam[1] = new SqlParameter("@FromDate",objVisitorFacilitiesBO.DateofRequest);
            arrParam[2] = new SqlParameter("@Todate",objVisitorFacilitiesBO.DateofVisit);
            arrParam[3] = new SqlParameter("@FacilityRequestID",objVisitorFacilitiesBO.FacilityRequestID);
            arrParam[4] = new SqlParameter("@EmpID", objVisitorFacilitiesBO.EmpID);
            arrParam[5] = new SqlParameter("@StatusMode", objVisitorFacilitiesBO.StatusID);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public int Update_VisitorFacilites(VisitorFacilitiesBO objVisitorFacilitiesBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_VisitorFacilities_Update";
            SqlParameter[] arrParam = new SqlParameter[21];
            arrParam[0] = new SqlParameter("@FacilityRequestID", objVisitorFacilitiesBO.FacilityRequestID);
            arrParam[1] = new SqlParameter("@LastChangedBy", objVisitorFacilitiesBO.LastChangedBy);
            arrParam[2] = new SqlParameter("@LastChangedDate", objVisitorFacilitiesBO.LastChangedDate);
            arrParam[3] = new SqlParameter("@ClientID", objVisitorFacilitiesBO.ClientID);
            arrParam[4] = new SqlParameter("@NoofVisitors", objVisitorFacilitiesBO.NoofVisitors);
            arrParam[5] = new SqlParameter("@DateofVisit", objVisitorFacilitiesBO.DateofVisit);
            arrParam[6] = new SqlParameter("@TravelArrangement", objVisitorFacilitiesBO.TravelArrangement);
            arrParam[7] = new SqlParameter("@T_ECost", objVisitorFacilitiesBO.T_ECost);
            arrParam[8] = new SqlParameter("@T_ACost", objVisitorFacilitiesBO.T_ACost);
            arrParam[9] = new SqlParameter("@MSnacks", objVisitorFacilitiesBO.MSnacks);
            arrParam[10] = new SqlParameter("@MS_ECost", objVisitorFacilitiesBO.MS_ECost);
            arrParam[11] = new SqlParameter("@MS_ACost", objVisitorFacilitiesBO.MS_ACost);
            arrParam[12] = new SqlParameter("@AFSnacks", objVisitorFacilitiesBO.AFSnacks);
            arrParam[13] = new SqlParameter("@AF_ECost", objVisitorFacilitiesBO.AF_ECost);
            arrParam[14] = new SqlParameter("@AF_ACost", objVisitorFacilitiesBO.AF_ACost);
            arrParam[15] = new SqlParameter("@ESnacks", objVisitorFacilitiesBO.ESnacks);
            arrParam[16] = new SqlParameter("@ES_ECost", objVisitorFacilitiesBO.ES_ECost);
            arrParam[17] = new SqlParameter("@ES_ACost", objVisitorFacilitiesBO.ES_ACost);
            arrParam[18] = new SqlParameter("@Remarks", objVisitorFacilitiesBO.Remarks);
            arrParam[19] = new SqlParameter("@Comments", objVisitorFacilitiesBO.Comments);
            arrParam[20] = new SqlParameter("@Mode", objVisitorFacilitiesBO.Mode);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public int VisitorFacilitiesRevert(VisitorFacilitiesBO objVisitorFacilitiesBO)//Revert Request
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_VisitorFacilitesStatusCancel";
            SqlParameter[] arrParam = new SqlParameter[4];
            arrParam[0] = new SqlParameter("@FacilityRequestID", objVisitorFacilitiesBO.FacilityRequestID);
            arrParam[1] = new SqlParameter("@LastChangedBy", objVisitorFacilitiesBO.LastChangedBy);
            arrParam[2] = new SqlParameter("@LastChangedDate", objVisitorFacilitiesBO.LastChangedDate);
            arrParam[3] = new SqlParameter("@Comments", objVisitorFacilitiesBO.Comments);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public int VisitorFacilitiesApproval(VisitorFacilitiesBO objVisitorFacilitiesBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_VisitorFacilitesStatusApprove";
            SqlParameter[] arrParam = new SqlParameter[4];
            arrParam[0] = new SqlParameter("@FacilityRequestID", objVisitorFacilitiesBO.FacilityRequestID);
            arrParam[1] = new SqlParameter("@LastChangedBy", objVisitorFacilitiesBO.LastChangedBy);
            arrParam[2] = new SqlParameter("@LastChangedDate", objVisitorFacilitiesBO.LastChangedDate);
            arrParam[3] = new SqlParameter("@Comments", objVisitorFacilitiesBO.Comments);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public int VisitorFacilitiesRejectOrCancel(VisitorFacilitiesBO objVisitorFacilitiesBO)//Reject Request
        {
            int c1 = 0;
            string strQuery = "[VMS].[AizantIT_SP_VisitorFacilitesReject]";
            SqlParameter[] arrParam = new SqlParameter[4];
            arrParam[0] = new SqlParameter("@FacilityRequestID", objVisitorFacilitiesBO.FacilityRequestID);
            arrParam[1] = new SqlParameter("@LastChangedBy", objVisitorFacilitiesBO.LastChangedBy);
            arrParam[2] = new SqlParameter("@LastChangedDate", objVisitorFacilitiesBO.LastChangedDate);
            arrParam[3] = new SqlParameter("@Comments", objVisitorFacilitiesBO.Comments);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        //Get Visitor Facilities Details Datatable for HR or Approver
        public DataTable GetVisitorFacilitiesDetails_ListDb(JQDatatableBO _objJQDataTableBO)
        {
            string strQuery = "[VMS].[AizantIT_SP_GetVisitorFacilitiesRequestDetails]";
            SqlParameter[] arrParam = new SqlParameter[9];
            arrParam[0] = new SqlParameter("@Mode", _objJQDataTableBO.iMode);
            arrParam[1] = new SqlParameter("@FacilityRequestID", _objJQDataTableBO.pkid);
            arrParam[2] = new SqlParameter("@DisplayLength", _objJQDataTableBO.iDisplayLength);
            arrParam[3] = new SqlParameter("@DisplayStart", _objJQDataTableBO.iDisplayStart);
            arrParam[4] = new SqlParameter("@SortCol", _objJQDataTableBO.iSortCol);
            arrParam[5] = new SqlParameter("@SortDir", _objJQDataTableBO.sSortDir);
            arrParam[6] = new SqlParameter("@Search", _objJQDataTableBO.sSearch);
            arrParam[7] = new SqlParameter("@EmpID", _objJQDataTableBO.EmpID);
            arrParam[8] = new SqlParameter("@ApproveStatus", _objJQDataTableBO.Status);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

       
        //To insert ClientMaster in VisitorFacilities
        public int MasterClientNameInsert(VisitorFacilitiesBO objVisitorFacilitiesBO)
        {
            string strQuery = "[VMS].[AizantIT_SP_VisitorClientMaster_Insert]";
            SqlParameter[] arrParam = new SqlParameter[3];
            arrParam[0] = new SqlParameter("@ClientName", objVisitorFacilitiesBO.ClientName);
            arrParam[1] = new SqlParameter("@CreatedBy", objVisitorFacilitiesBO.CreatedBy);
            arrParam[2] = new SqlParameter("@CreatedDate", objVisitorFacilitiesBO.CreatedDate);
            return Convert.ToInt32(cmmdExecuteScalar(strQuery, CommandType.StoredProcedure, arrParam));
        }
        public DataTable LoadVisitorClientNameMaster()
        {
            string strQuery = "[VMS].[AizantIT_SP_GetClientNameMaster]";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }

        public DataTable GetVisitorFacilitiesGridHistory(VisitorFacilitiesBO objFacilitiesBO)
        {
            string strQuery = "[VMS].[AizantIT_SP_GetVisitorFacilitiesHistory]";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@FacilityRequestID", objFacilitiesBO.FacilityRequestID);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }
        // To Check Visitor/Client Name Verifying Data Exits or Not in VisitorFacilities
        public int VisitorClientNameifExistsorNot(string @ClientName)
        {
            string strQuery = "VMS.AizantIT_SP_NameOfVisitorClient_IfExits";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@ClientName", @ClientName);
            return Convert.ToInt32(cmmdExecuteScalar(strQuery, CommandType.StoredProcedure, arrParam));
        }

        public DataTable GetFacilityIDFromNotification(VisitorFacilitiesBO objFacilitiesBO)
        {
            string strQuery = "VMS.AizantIT_SP_GetFacilityIDFromNotification";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@NotificationID", objFacilitiesBO.NotificationID);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }
    }
}
