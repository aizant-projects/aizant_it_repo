﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using VMS_BO;

namespace VMS_DAL
{
    public class SupplyRegisterDAL : SQLHELPER
    {
        //Snacks Supply DAL

        public int Snacks_Insert(SupplyRegisterBO objSupplyRegisterBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_SnacksIssue_Insert";
            SqlParameter[] arrParam = new SqlParameter[7];
            arrParam[0] = new SqlParameter("@EmpID", objSupplyRegisterBO.EmployeeID);
            arrParam[1] = new SqlParameter("@ItemCost", objSupplyRegisterBO.ItemCost);
            arrParam[2] = new SqlParameter("@CreatedDate", objSupplyRegisterBO.CreatedDate);
            arrParam[3] = new SqlParameter("@IssuedItems", objSupplyRegisterBO.IssuedItems);
            arrParam[4] = new SqlParameter("@Remarks", objSupplyRegisterBO.Remarks);
            arrParam[5] = new SqlParameter("@IssuedOn", objSupplyRegisterBO.InDateTime);
            arrParam[6] = new SqlParameter("@CreatedBy", objSupplyRegisterBO.CreatedBy);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public DataTable SearchFilterSnacksSupply(SupplyRegisterBO objSupplyRegisterBO)
        {
            string strQuery = "VMS.AizantIT_SP_Snacks_Select";
            SqlParameter[] arrParam = new SqlParameter[4];
            arrParam[0] = new SqlParameter("@Department", objSupplyRegisterBO.Department);
            arrParam[1] = new SqlParameter("@EmpID", objSupplyRegisterBO.EmployeeID);
            arrParam[2] = new SqlParameter("@indatetime", objSupplyRegisterBO.InDateTime);
            arrParam[3] = new SqlParameter("@indatetime1", objSupplyRegisterBO.ReturnDateTime);
            //arrParam[2] = new SqlParameter("@employeename", objSupplyRegisterBO.EmployeeName);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public int Update_SnacksSupplyRegisterEdit(SupplyRegisterBO objSupplyRegisterBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_SnacksIssue_Update";
            SqlParameter[] arrParam = new SqlParameter[8];
            arrParam[0] = new SqlParameter("@tempid", objSupplyRegisterBO.SNo);
            arrParam[1] = new SqlParameter("@EmpID", objSupplyRegisterBO.EmployeeID);
            arrParam[2] = new SqlParameter("@ItemCost", objSupplyRegisterBO.ItemCost);
            arrParam[3] = new SqlParameter("@Remarks", objSupplyRegisterBO.Remarks);
            arrParam[4] = new SqlParameter("@IssuedItems", objSupplyRegisterBO.IssuedItems);
            arrParam[5] = new SqlParameter("@LastChangedBy", objSupplyRegisterBO.LastChangedBy);
            arrParam[6] = new SqlParameter("@LastChangedDate", objSupplyRegisterBO.LastChangedDate);
            arrParam[7] = new SqlParameter("@LastChangedByComments", objSupplyRegisterBO.LastChangedComments);
            //arrParam[8] = new SqlParameter("@IssuedOn", objSupplyRegisterBO.InDateTime);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public DataTable BindDataSnacksSupplyRegister()
        {
            string strQuery = "VMS.AizantIT_SP_SnaksIssue_BindData";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }

        public DataTable SnacksSupplyRegisterlinkbuttonFilldata(SupplyRegisterBO objSupplyRegisterBO)
        {
            string strQuery = "VMS.AizantIT_SP_SnaksIssue_GridViewLinkbutton";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@tempid", objSupplyRegisterBO.SNo);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }


        //Key Supply DAL

        public int Key_Insert(SupplyRegisterBO objSupplyRegisterBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_KeyIssue_Insert";
            SqlParameter[] arrParam = new SqlParameter[8];
            arrParam[0] = new SqlParameter("@EmpID", objSupplyRegisterBO.EmployeeID);
            arrParam[1] = new SqlParameter("@CreatedDate", objSupplyRegisterBO.CreatedDate);
            arrParam[2] = new SqlParameter("@KeyDeptID", objSupplyRegisterBO.Department);
            arrParam[3] = new SqlParameter("@IssuedOn", objSupplyRegisterBO.InDateTime);
            arrParam[4] = new SqlParameter("@Remarks", objSupplyRegisterBO.Remarks);
            arrParam[5] = new SqlParameter("@KeyDeptArea", objSupplyRegisterBO.DepartmentArea);
            arrParam[6] = new SqlParameter("@NoofKeys", objSupplyRegisterBO.NoofKeys);
            arrParam[7] = new SqlParameter("@CreatedBy", objSupplyRegisterBO.CreatedBy);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }


        public DataTable SearchFilterKeySupply(SupplyRegisterBO objSupplyRegisterBO)
        {
            string strQuery = "VMS.AizantIT_SP_KeyIssue_Select";
            SqlParameter[] arrParam = new SqlParameter[4];
            arrParam[0] = new SqlParameter("@Department", objSupplyRegisterBO.Department);
            arrParam[1] = new SqlParameter("@EmpID", objSupplyRegisterBO.EmployeeID);
            arrParam[2] = new SqlParameter("@indatetime", objSupplyRegisterBO.InDateTime);
            arrParam[3] = new SqlParameter("@indatetime1", objSupplyRegisterBO.ReturnDateTime);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public int Update_KeySupplyRegisterEdit(SupplyRegisterBO objSupplyRegisterBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_KeyIssue_Update";
            SqlParameter[] arrParam = new SqlParameter[9];
            arrParam[0] = new SqlParameter("@tempid", objSupplyRegisterBO.SNo);
            arrParam[1] = new SqlParameter("@EmpID", objSupplyRegisterBO.EmployeeID);
            arrParam[2] = new SqlParameter("@Remarks", objSupplyRegisterBO.Remarks);
            //arrParam[2] = new SqlParameter("@IssuedOn", objSupplyRegisterBO.InDateTime);
            arrParam[3] = new SqlParameter("@Department", objSupplyRegisterBO.Department);
            arrParam[4] = new SqlParameter("@DepartmentArea", objSupplyRegisterBO.DepartmentArea);
            arrParam[5] = new SqlParameter("@NoofKeys", objSupplyRegisterBO.NoofKeys);
            arrParam[6] = new SqlParameter("@LastChangedBy", objSupplyRegisterBO.LastChangedBy);
            arrParam[7] = new SqlParameter("@LastChangedDate", objSupplyRegisterBO.LastChangedDate);
            arrParam[8] = new SqlParameter("@LastChangedByComments", objSupplyRegisterBO.LastChangedComments);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public DataTable BindDataKeySupplyRegister()
        {
            string strQuery = "VMS.AizantIT_SP_KeyIssue_BindData";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }

        public DataTable KeySupplyRegisterlinkbuttonFilldata(SupplyRegisterBO objSupplyRegisterBO)
        {
            string strQuery = "VMS.AizantIT_SP_KeyIssue_GridViewLinkbutton";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@tempid", objSupplyRegisterBO.SNo);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public int CheckoutOutwardKey(SupplyRegisterBO objSupplyRegisterBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_KeySupply_CheckOut";
            SqlParameter[] arrParam = new SqlParameter[2];
            arrParam[0] = new SqlParameter("@tempid", objSupplyRegisterBO.SNo);
            arrParam[1] = new SqlParameter("@ReturnDateTime", objSupplyRegisterBO.ReturnDateTime);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }


        //AccessCard Supply DAL

        public int Access_Insert(SupplyRegisterBO objSupplyRegisterBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_AccessCardIssue_Insert";
            SqlParameter[] arrParam = new SqlParameter[8];
            arrParam[0] = new SqlParameter("@EmpID", objSupplyRegisterBO.EmployeeID);
            arrParam[1] = new SqlParameter("@CreatedDate", objSupplyRegisterBO.CreatedDate);
            arrParam[2] = new SqlParameter("@Department", objSupplyRegisterBO.Department);
            arrParam[3] = new SqlParameter("@IssuedOn", objSupplyRegisterBO.InDateTime);
            arrParam[4] = new SqlParameter("@Remarks", objSupplyRegisterBO.Remarks);
            arrParam[5] = new SqlParameter("@MobileNo", objSupplyRegisterBO.MobileNo);
            arrParam[6] = new SqlParameter("@AccessCardNo", objSupplyRegisterBO.AccessCardNo);
            arrParam[7] = new SqlParameter("@CreatedBy", objSupplyRegisterBO.CreatedBy);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }


        public DataTable SearchFilterAccessCardSupply(SupplyRegisterBO objSupplyRegisterBO)
        {
            string strQuery = "VMS.AizantIT_SP_AccessCardIssue_Select";
            SqlParameter[] arrParam = new SqlParameter[4];
            arrParam[0] = new SqlParameter("@department", objSupplyRegisterBO.Department);
            arrParam[1] = new SqlParameter("@EmpID", objSupplyRegisterBO.EmployeeID);
            arrParam[2] = new SqlParameter("@indatetime", objSupplyRegisterBO.InDateTime);
            arrParam[3] = new SqlParameter("@indatetime1", objSupplyRegisterBO.ReturnDateTime);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public int Update_AccessCardSupplyRegisterEdit(SupplyRegisterBO objSupplyRegisterBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_AccessCardIssue_Update";
            SqlParameter[] arrParam = new SqlParameter[9];
            arrParam[0] = new SqlParameter("@tempid", objSupplyRegisterBO.SNo);
            arrParam[1] = new SqlParameter("@EmpID", objSupplyRegisterBO.EmployeeID);
            arrParam[2] = new SqlParameter("@Remarks", objSupplyRegisterBO.Remarks);
            //arrParam[2] = new SqlParameter("@IssuedOn", objSupplyRegisterBO.InDateTime);
            arrParam[3] = new SqlParameter("@Department", objSupplyRegisterBO.Department);
            arrParam[4] = new SqlParameter("@MobileNo", objSupplyRegisterBO.MobileNo);
            arrParam[5] = new SqlParameter("@AccessCardNo", objSupplyRegisterBO.AccessCardNo);
            arrParam[6] = new SqlParameter("@LastChangedBy", objSupplyRegisterBO.LastChangedBy);
            arrParam[7] = new SqlParameter("@LastChangedDate", objSupplyRegisterBO.LastChangedDate);
            arrParam[8] = new SqlParameter("@LastChangedByComments", objSupplyRegisterBO.LastChangedComments);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public DataTable BindDataAccessCardSupplyRegister()
        {
            string strQuery = "VMS.AizantIT_SP_AccessCardIssue_BindData";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }

        public DataTable AccessCardSupplyRegisterlinkbuttonFilldata(SupplyRegisterBO objSupplyRegisterBO)
        {
            string strQuery = "VMS.AizantIT_SP_AccessCardIssue_GridViewLinkbutton";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@tempid", objSupplyRegisterBO.SNo);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }

        public int CheckoutOutwardAccess(SupplyRegisterBO objSupplyRegisterBO)
        {
            int c1 = 0;
            string strQuery = "VMS.AizantIT_SP_AccesscardSupply_CheckOut";
            SqlParameter[] arrParam = new SqlParameter[2];
            arrParam[0] = new SqlParameter("@tempid", objSupplyRegisterBO.SNo);
            arrParam[1] = new SqlParameter("@ReturnDateTime", objSupplyRegisterBO.ReturnDateTime);
            c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            return c1;
        }

        public DataTable BindSupplyType()
        {
            string strQuery = "VMS.AizantIT_SP_SupplyTypeMaster_Select";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }
    }
}
