﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Data.SqlTypes;
using VMS_BO;

namespace VMS_DAL
{
    public class VMSRolePrivilegesDAL : SQLHELPER
    {
        public DataTable GetPrivileIDAndNames()
        {
            string strQuery = "VMS.AizantIT_SP_GetPrivileIDAndNames";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }

        public DataTable GetRoles()
        {
            string strQuery = "VMS.AizantIT_SP_GetRoles";
            return GetDataTable(strQuery, CommandType.StoredProcedure, null);
        }

        public int InsertRolePrivileges(List<RolePrivilegesBO> lstRolePrivilegesBO)
        {
            int c1 = 0;
            int c = 0;

            string strQuery1 = "VMS.AizantIT_SP_DeleteRolePrivileges";
            SqlParameter[] arrParam1 = new SqlParameter[1];
            arrParam1[0] = new SqlParameter("@RoleID", lstRolePrivilegesBO[0].RoleID);
            c = cmmdExecuteNonQuery(strQuery1, CommandType.StoredProcedure, arrParam1);

            foreach (RolePrivilegesBO objRolePrivilegesBO in lstRolePrivilegesBO)
            {
                string strQuery = "VMS.AizantIT_SP_InsertRolePrivileges";
                SqlParameter[] arrParam = new SqlParameter[2];
                arrParam[0] = new SqlParameter("@RoleID", objRolePrivilegesBO.RoleID);
                arrParam[1] = new SqlParameter("@PrivilegeID", objRolePrivilegesBO.PrivilegeID);
                c1 = cmmdExecuteNonQuery(strQuery, CommandType.StoredProcedure, arrParam);
            }
            return c1;
        }

        public DataTable GetRolePrivileges(RolePrivilegesBO objRolePrivilegesBO)
        {
            string strQuery = "VMS.AizantIT_SP_GetRolePrivileges";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@RoleID", objRolePrivilegesBO.RoleID);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }
        public DataTable GetMenuItems(RolePrivilegesBO objRolePrivilegesBO)
        {
            string strQuery = "[VMS].[AizantIT_SP_GetPageRolePrivileges]";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@RoleID", objRolePrivilegesBO.RoleID);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }
        public DataTable GetEmpRoleID(RolePrivilegesBO objRolePrivilegesBO)
        {
            string strQuery = "[VMS].[AizantIT_SP_GetUserRoles]";
            SqlParameter[] arrParam = new SqlParameter[1];
            arrParam[0] = new SqlParameter("@EmpID", objRolePrivilegesBO.RoleID);
            return GetDataTable(strQuery, CommandType.StoredProcedure, arrParam);
        }
    }
}
