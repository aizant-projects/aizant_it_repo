﻿using Aizant_API_Entities;
using AizantIT_PharmaApp.Areas.QMS.Models.QmsModel;
using AizantIT_PharmaApp.Common;
using DevExpress.Pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AizantIT_PharmaApp.QMSIncient
{
    public partial class IframeDocumentViewer : System.Web.UI.Page
    {
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //private AizantIT_QMS_EF db = new AizantIT_QMS_EF();
        private AizantIT_DevEntities db = new AizantIT_DevEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        if (Request.QueryString.Count > 0 && Request.QueryString.Count<=2)
                        {
                            string EventID = Request.QueryString[0];
                            string FID = Request.QueryString[1];

                            ToViewReferralDocument(FID, EventID);
                        }

                        if (Request.QueryString.Count > 0 && Request.QueryString.Count >2)
                        {
                            string EventID = Request.QueryString[0];
                            string FID = Request.QueryString[1];
                            int QualityID = Convert.ToInt32(Request.QueryString[2]);

                            if (EventID == "TRPT")
                            {
                                ViewTempReport(FID, QualityID);
                            }
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Uclose", "CloseBrowser();", true);
                    // Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("QMSIF_M1:" + strline + "  " + strMsg);
                string strError = "QMSIF_M1:" + strline + "  " + "Page loading failed";
                literalSop.Visible = true;
                Spreadsheet.Visible = false;
                RichEdit.Visible = false;
                binImage.Visible = false;
                literalSop.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        public void ToViewReferralDocument(string FID, string EventID)
        {
            DocumentViewerBO obj = new DocumentViewerBO();
            int ID = Convert.ToInt32(FID);
            string fname = string.Empty, FileExtn = string.Empty; byte[] filecontent = null;
            if (EventID == "1")//Incident
            {
                var DocFiles = (from a in db.AizantIT_Incident_IncidentAttachments
                                where a.UniquePKID == ID
                                select a).SingleOrDefault();
                if (System.IO.File.Exists(Server.MapPath("~\\" + DocFiles.FilePath)))
                {
                    filecontent = System.IO.File.ReadAllBytes(Server.MapPath("~\\" + DocFiles.FilePath));
                }
                    FileExtn = DocFiles.FileType;
                    fname = DocFiles.FileName;
            }
            else if (EventID == "2")//Capa Attachments
            {
                var CAPAverificationAttachment = (from C in db.AizantIT_CAPAVerificationFilesAttachements
                                where C.UniquePKID == ID
                                select C).SingleOrDefault();
                if (System.IO.File.Exists(Server.MapPath("~\\" + CAPAverificationAttachment.FilePath)))
                {
                    filecontent = System.IO.File.ReadAllBytes(Server.MapPath("~\\" + CAPAverificationAttachment.FilePath));
                }
                FileExtn = CAPAverificationAttachment.FileType;
                    fname = CAPAverificationAttachment.FileName;
            }
            else if (EventID == "4")//CCN HOD Attachments
            {
                var HodAssessCCN = (from C in db.AizantIT_HODAssessmentAttachmentFile
                                    where C.UniquePKID == ID
                                    select C).SingleOrDefault();
                if (System.IO.File.Exists(Server.MapPath("~\\" + HodAssessCCN.FilePath)))
                {
                    filecontent = System.IO.File.ReadAllBytes(Server.MapPath("~\\" + HodAssessCCN.FilePath));
                }
                    FileExtn = HodAssessCCN.FileType;
                    fname = HodAssessCCN.FileName;
            }
            else if (EventID == "5")//CCN CloseAttachment
            {
                var CCNClose = (from C in db.AizantIT_CCNCloseFileAttachments
                                    where C.UniquePKID == ID
                                    select C).SingleOrDefault();
                if (System.IO.File.Exists(Server.MapPath("~\\" + CCNClose.FilePath)))
                {
                    filecontent = System.IO.File.ReadAllBytes(Server.MapPath("~\\" + CCNClose.FilePath));
                }
                FileExtn = CCNClose.FileType;
                    fname = CCNClose.FileName;
              
            }
            else if (EventID == "6")//CAPA CompletedAttachments
            {
                var CAPAComplt = (from C in db.AizantIT_CapaCompltFileAttachments
                                    where C.UniquePKID == ID
                                    select C).SingleOrDefault();
                if (System.IO.File.Exists(Server.MapPath("~\\" + CAPAComplt.FilePath)))
                {
                    filecontent = System.IO.File.ReadAllBytes(Server.MapPath("~\\" + CAPAComplt.FilePath));
                }
                FileExtn = CAPAComplt.FileType;
                   fname = CAPAComplt.FileName;
            }
            else if (EventID == "7")//CCN Initiator View
            {
                var InitiatorCCN = (from C in db.AizantIT_CCNInitiatorFileAttachment
                                    where C.UniquePKID == ID
                                    select new { C.TemprefID, C.FileType, C.FileName,C.FilePath }).SingleOrDefault();

                if (System.IO.File.Exists(Server.MapPath("~\\" + InitiatorCCN.FilePath)))
                {
                    filecontent = System.IO.File.ReadAllBytes(Server.MapPath("~\\" + InitiatorCCN.FilePath));
                }
                FileExtn = InitiatorCCN.FileType;
                fname = InitiatorCCN.FileName;
            }
            else if (EventID == "8")//Incident Initiator Attachments
            {
                var DocFiles = (from C in db.AizantIT_Incident_InitiatorAttachments
                                where C.UniquePKID == ID
                                select C).SingleOrDefault();
                if (System.IO.File.Exists(Server.MapPath("~\\" + DocFiles.FilePath)))
                {
                    filecontent = System.IO.File.ReadAllBytes(Server.MapPath("~\\" + DocFiles.FilePath));
                }
                    FileExtn = DocFiles.FileType;
                    fname = DocFiles.FileName;
            }

            if (filecontent == null || filecontent.Length == 0)
            {
                literalSop.Visible = true;
                Spreadsheet.Visible = false;
                RichEdit.Visible = false;
                binImage.Visible = false;
                literalSop.Text = "<p style='color: red; '><b>File is not available</b></p>";
                return;
            }
            switch (FileExtn)
            {
                case ".jpeg":
                    binImage.Visible = true;
                    Spreadsheet.Visible = false;
                    RichEdit.Visible = false;
                    literalSop.Visible = false;
                    binImage.ContentBytes = filecontent;
                    break;
                case ".jpg":
                    binImage.Visible = true;
                    Spreadsheet.Visible = false;
                    RichEdit.Visible = false;
                    literalSop.Visible = false;
                    binImage.ContentBytes = filecontent;
                    break;
                case ".png":
                    binImage.Visible = true;
                    Spreadsheet.Visible = false;
                    RichEdit.Visible = false;
                    literalSop.Visible = false;
                    binImage.ContentBytes = filecontent;
                    break;
                case ".gif":
                    binImage.Visible = true;
                    Spreadsheet.Visible = false;
                    RichEdit.Visible = false;
                    literalSop.Visible = false;
                    binImage.ContentBytes = filecontent;
                    break;
                case ".bmp":
                    binImage.Visible = true;
                    Spreadsheet.Visible = false;
                    RichEdit.Visible = false;
                    literalSop.Visible = false;
                    binImage.ContentBytes = filecontent;
                    break;
                case ".pdf":
                    literalSop.Visible = true;
                    Spreadsheet.Visible = false;
                    RichEdit.Visible = false;
                    binImage.Visible = false;
                    filecontent = null;
                    literalSop.Text = " <embed src=\"" + ResolveUrl("~/ASPXHandlers/QMSPDFViewHandler.ashx") + "?EventID=" + EventID + "&DOCID=" + ID + " #toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" height=\"661px\" />";
                    break;
                case ".xls":
                    Spreadsheet.Visible = true;
                    RichEdit.Visible = false;
                    binImage.Visible = false;
                    literalSop.Visible = false;
                    Spreadsheet.Open(
                       Guid.NewGuid().ToString(),
                       DevExpress.Spreadsheet.DocumentFormat.Xls,
                       () =>
                       {
                           byte[] docBytes = filecontent;
                           return new MemoryStream(docBytes);
                       }
                       );
                    break;
                case ".xlsx":
                    Spreadsheet.Visible = true;
                    RichEdit.Visible = false;
                    binImage.Visible = false;
                    literalSop.Visible = false;
                    Spreadsheet.Open(
                        Guid.NewGuid().ToString(),
                        DevExpress.Spreadsheet.DocumentFormat.Xlsx,
                        () =>
                        {
                            byte[] docBytes = filecontent;
                            return new MemoryStream(docBytes);
                        }
                        );
                    break;
                case ".rtf":
                    RichEdit.Visible = true;
                    Spreadsheet.Visible = false;
                    binImage.Visible = false;
                    literalSop.Visible = false;
                    RichEdit.Open(
                       Guid.NewGuid().ToString(),
                       DevExpress.XtraRichEdit.DocumentFormat.Rtf,
                       () =>
                       {
                           byte[] docBytes = filecontent;
                           return new MemoryStream(docBytes);
                       }
                       );
                    break;
                case ".doc":
                    RichEdit.Visible = true;
                    Spreadsheet.Visible = false;
                    binImage.Visible = false;
                    literalSop.Visible = false;
                    RichEdit.Open(
                      Guid.NewGuid().ToString(),
                      DevExpress.XtraRichEdit.DocumentFormat.Doc,
                      () =>
                      {
                          byte[] docBytes = filecontent;
                          return new MemoryStream(docBytes);
                      }
                      );
                    break;
                case ".docx":
                    RichEdit.Visible = true;
                    Spreadsheet.Visible = false;
                    binImage.Visible = false;
                    literalSop.Visible = false;
                    RichEdit.Open(
                      Guid.NewGuid().ToString(),
                      DevExpress.XtraRichEdit.DocumentFormat.OpenXml,
                      () =>
                      {
                          byte[] docBytes = filecontent;
                          return new MemoryStream(docBytes);
                      }
                      );
                    break;
                case ".txt":
                    RichEdit.Visible = true;
                    Spreadsheet.Visible = false;
                    binImage.Visible = false;
                    literalSop.Visible = false;
                    RichEdit.Open(
                      Guid.NewGuid().ToString(),
                      DevExpress.XtraRichEdit.DocumentFormat.PlainText,
                      () =>
                      {
                          byte[] docBytes = filecontent;
                          return new MemoryStream(docBytes);
                      }
                      );
                    break;
            }
        }
        public void ViewTempReport(string ID,int QualityID)
        {
            literalSop.Visible = true;
            Spreadsheet.Visible = false;
            RichEdit.Visible = false;
            binImage.Visible = false;

            literalSop.Text = " <embed src=\"" + ResolveUrl("~/ASPXHandlers/QMSPDFViewHandler.ashx") + "?EventID=TRPT&DOCID=" + ID +"&QualityID="+QualityID+ "  #toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" height=\"661px\" />";
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}