﻿using AizantIT_PharmaApp.Common;
using AizantIT_PharmaApp.UserControls.TMS_UserControls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using QMS_BusinessLayer;
using QMS_BO;
using TMS_BusinessLayer;
using TMS_BusinessObjects;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;
using QMS_BO.QMS_Incident_BO;
using Aizant_API_Entities;
using AizantIT_PharmaApp.Areas.QMS.Controllers.Common;

namespace AizantIT_PharmaApp.QMSIncient
{
    public partial class TTSTraining : System.Web.UI.Page
    {

        UMS_BAL objUMS_BAL;
        TMS_BAL objTMS_BAL;
        QMS_BAL objQMS_BAL = new QMS_BAL();
        int AuthorID = 0, ReviewerID = 0;
        int SessionsCount = 0;
        bool IsTTS_DetailsInIt = false;
        private int key;

        public object Today { get; private set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    //commented on 18-01-2020
                    if (Request.QueryString["_ShowType"] == "4" || Request.QueryString["_ShowType"] == "10")
                    {
                        if (!QMSCommonActions.GetIsTMSHODRoleExist())
                        {
                            btnHodSubmit.Visible = false;
                            btnReset.Visible = false;
                            lblErrMsg.Visible = true;
                        }
                        //To get Incident Current status and Hod Name
                        AizantIT_DevEntities db = new AizantIT_DevEntities();
                        //For MainList Current Status

                        //end


                        int incidentID = Convert.ToInt32(Request.QueryString["IMID"]);
                        var getIncidentCurrentStatus = (from t1 in db.AizantIT_IncidentMaster
                                                        join t2 in db.AizantIT_EmpMaster on t1.HODEmpID equals t2.EmpID
                                                        where t1.IncidentMasterID == incidentID
                                                        select new { t1.CurrentStatus, AuthorName = (t2.FirstName + "" + t2.LastName) }).SingleOrDefault();
                        if (getIncidentCurrentStatus != null)
                        {
                            txtAuthor.Value = getIncidentCurrentStatus.AuthorName;
                            if (getIncidentCurrentStatus.CurrentStatus == 21)//after HQA approved Approver Name Displayed 
                            {
                                hdnIncidentCurrentStatus.Value = "21";
                            }
                            if (getIncidentCurrentStatus.CurrentStatus == 16)//after HQA approved Approver Name Displayed 
                            {
                                divApproverName.Visible = true;
                            }
                            else
                            {
                                divApproverName.Visible = false;
                            }
                        }
                    }
                    //end 19-01-2020
                    if (!IsPostBack)
                    {
                        ddlTargetType.Attributes.Add("disabled", "disabled");
                        int EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        hdnEmpID.Value = EmpID.ToString();
                        ViewState["QAApproveOrRevert"] = string.Empty;

                        //commented on 12-02-2020
                        //To get Incident Created Date
                        AizantIT_DevEntities db = new AizantIT_DevEntities();
                        int incidentID = Convert.ToInt32(Request.QueryString["IMID"]);
                        int Main_IncidentID = Convert.ToInt32(Request.QueryString["MainlstID"]);

                        //For getting TTS Due Date
                        DataTable dt = objQMS_BAL.GetIncident_TTS_DateBAL((incidentID == 0 ? Main_IncidentID : incidentID));
                        hdnMinDate.Value =Convert.ToDateTime(dt.Rows[0][0]).ToString("dd MMM yyyy");
                       
                        

                        //start commetned on 13-02-2020
                        //To get Incident TTS Due Date
                        var getTTSDueDate = (from t1 in db.AizantIT_QITTS_Master
                                             where t1.Incident_ID == (incidentID == 0 ? Main_IncidentID : incidentID)
                                             select t1.DueDate).SingleOrDefault();

                        hdnIncidentTTSDueDate.Value = Convert.ToDateTime(getTTSDueDate).ToString("dd MMM yyyy");
                        //end commented on 13-02-2020

                        //hdnMinDate.Value = DateTime.Now.Date.ToString("dd MMM yyyy");
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drTmsRoles = dtx.Select("ModuleID=" + (int)Aizant_Enums.AizantEnums.Modules.QMS);
                        if (drTmsRoles.Length > 0)
                        {
                            dtTemp = drTmsRoles.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)QMS_Roles.HOD).Length > 0 || dtTemp.Select("RoleID=" + (int)QMS_Roles.QA).Length > 0 || dtTemp.Select("RoleID=" + (int)QMS_Roles.HeadQA).Length > 0 || dtTemp.Select("RoleID=" + (int)QMS_Roles.Initiator).Length > 0 || dtTemp.Select("RoleID=" + (int)QMS_Roles.Investigator).Length > 0 || dtTemp.Select("RoleID=" + (int)QMS_Roles.AdminRole).Length > 0)
                            {
                                if (dtTemp.Select("RoleID=" + (int)QMS_Roles.HOD).Length > 0 || dtTemp.Select("RoleID=" + (int)QMS_Roles.HOD).Length > 0 || dtTemp.Select("RoleID=" + (int)QMS_Roles.HeadQA).Length > 0 || dtTemp.Select("RoleID=" + (int)QMS_Roles.Initiator).Length > 0 || dtTemp.Select("RoleID=" + (int)QMS_Roles.Investigator).Length > 0)
                                {
                                    hdnEmpIsHodOrQA.Value = "Y";
                                }
                                ddlTargetType.Enabled = false;
                                ddlTargetType.CssClass = "selectpicker drop_down";

                                InitializeThePage();



                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }

                        }
                        else
                        {

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Uclose", "CloseBrowser();", true);
                            // Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Uclose", "CloseBrowser();", true);
                    // Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                //HelpClass.custAlertMsg(this, this.GetType(), "TTS_M1:" + strline + "  " + strMsg, "error");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "custAlertMsg('" + strMsg + "','error');", true);
            }
        }

        #region BindData
        private void loadAuthorizeErrorMsg()
        {
            try
            {
                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                divAutorizedMsg.Visible = true;
                divMainContainer.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M2:" + strline + "  " + strMsg, "error");
            }
        }
        private void InitializeThePage()
        {
            try
            {
                if (Request.QueryString["Notification_ID"] != null)
                {
                    objUMS_BAL = new UMS_BAL();
                    //2 represents NotifyEmp had visited the page through notification link.
                    int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs); //2 represents NotifyEmp had visited the page through notification link.
                }
                EnableDisableButtons();
                InitializeDataTables();


                if (Request.QueryString["TTS_Type"] != null)
                {

                    lbTTSList.Visible = false;
                    Tab_TargetHistory.Visible = false;
                    divAuthorComments.Visible = true;
                    //ddlReviewerTTS.Visible = true;
                    divHodActionBtns.Visible = true;
                    ApplyGridStylesToShow4Columns();

                    if (Request.QueryString["TTS_Type"] == "Create")
                    {
                        if (Session["PreviousPage"] != null)
                        {
                            Session.Remove("PreviousPage");
                        }
                        bindDepartments(Convert.ToInt32(hdnEmpID.Value));
                        BindDocType();
                    }
                    else if (Request.QueryString["TTS_Type"] == "CreateList")
                    {
                        ddlDepartmentTTS.Enabled = false;
                        ddlDocumentType.Enabled = false;
                        ddlTrainingDocument.Enabled = false;
                        ddlTraineeDepartment.Enabled = true;

                        if (Session["PreviousPage"] != null)
                        {
                            Dictionary<string, int> dict = (Dictionary<string, int>)Session["PreviousPage"];
                            ddlDepartmentTTS.Items.Add(new ListItem(dict.ElementAt(0).Key, Convert.ToString(dict.ElementAt(0).Value)));
                            ddlDocumentType.Items.Add(new ListItem(Convert.ToString(dict.ElementAt(1).Key), Convert.ToString(dict.ElementAt(1).Value)));
                            ddlTargetType.Items.FindByValue((Convert.ToString(dict.ElementAt(2).Value))).Selected = true;
                            ddlTrainingDocument.Items.Add(new ListItem(Convert.ToString(dict.ElementAt(3).Key), Convert.ToString(dict.ElementAt(3).Value)));
                        }
                        if (ddlTargetType.SelectedValue == "1")
                        {

                            string month = DateTime.Now.Month.ToString();

                        }
                        //upMonth.Update();
                        BindDropDowns(ddlDepartmentTTS.SelectedValue, hdnEmpID.Value);//To Load Author and Approver
                        BindTraineeDepartment(); //Trainee department
                        ddlTargetType.Enabled = false;
                    }
                }
                if (Request.QueryString["IMID"] != null)
                {
                    divDueDate.Visible = true;
                    int IncidentID = Convert.ToInt32(Request.QueryString["IMID"]);
                    DataTable dtGetTTsID = objQMS_BAL.GetTTSIDBal(IncidentID);
                    if (dtGetTTsID.Rows.Count > 0)
                    {
                        hdnTTS_ID.Value = dtGetTTsID.Rows[0]["TTS_ID"].ToString();
                        GetTargetTS_Details(hdnTTS_ID.Value);
                    }
                    //DataTable dtGetIncidentQA = objQMS_BAL.GetQaNameBal(IncidentID);
                    //if (dtGetIncidentQA.Rows.Count > 0)
                    //{
                    //    txtReviewer.Value = dtGetIncidentQA.Rows[0]["IncidentQAName"].ToString();
                    //    hdnIncidentQAID.Value = dtGetIncidentQA.Rows[0]["QAEmpID"].ToString();
                    //}
                }

                if (Request.QueryString["MainlstID"] != null)//From Main List hide buttons
                {
                    //start commented by pradeep 09-04-2019
                    DisableDropDowns();
                    //end
                    //DIVViewDueDate.Visible = true;
                    divDueDate.Visible = true;
                    int IncidentID = Convert.ToInt32(Request.QueryString["MainlstID"]);
                    DataTable dtGetTTsID = objQMS_BAL.GetTTSIDBal(IncidentID);
                    if (dtGetTTsID.Rows.Count > 0)
                    {
                        hdnTTS_ID.Value = dtGetTTsID.Rows[0]["TTS_ID"].ToString();
                        GetTargetTS_Details(hdnTTS_ID.Value);
                        btnHodSubmit.Visible = false;
                        btnReset.Visible = false;
                        btnHodCancel.Visible = false;
                        //btnHodCancel.Text = "Close";
                    }
                    //DataTable dtGetIncidentQA = objQMS_BAL.GetQaNameBal(IncidentID);
                    //if (dtGetIncidentQA.Rows.Count > 0)
                    //{
                    //    txtReviewer.Value = dtGetIncidentQA.Rows[0]["IncidentQAName"].ToString();
                    //    hdnIncidentQAID.Value = dtGetIncidentQA.Rows[0]["QAEmpID"].ToString();
                    //}
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                strMsg = "TTS_M3:" + strline + "  " + strMsg;
                //HelpClass.custAlertMsg(this, this.GetType(),strMsg , "error");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "custAlertMsg('" + strMsg + "','error');", true);
            }
        }

        public void GetTargetTS_Details(string TTS_ID)
        {
            try
            {
                //code to load Target training details based on Status
                objTMS_BAL = new TMS_BAL();
                int CurrentHistoryStatus = 0, TargetCurrentStatus = 0;
                int EmpID = Convert.ToInt32(hdnEmpID.Value);
                DataSet dsTTS_Details = objQMS_BAL.GetTargetTrainingDetails(TTS_ID, Convert.ToInt32(hdnEmpID.Value),
                    out CurrentHistoryStatus, out TargetCurrentStatus);

                // Target TS Main Doc. Status
                hdnTTS_Status.Value = TargetCurrentStatus.ToString();

                // Target TS Current Status
                hdnTTS_CurrentStatus.Value = CurrentHistoryStatus.ToString();

                AuthorID = Convert.ToInt32(dsTTS_Details.Tables[0].Rows[0]["AuthorBy"]);
                ReviewerID = Convert.ToInt32(dsTTS_Details.Tables[0].Rows[0]["ReviewBy"]);

                txtReviewer.Value = dsTTS_Details.Tables[0].Rows[0]["ReviewerName"].ToString();
                txtAuthor.Value = dsTTS_Details.Tables[0].Rows[0]["AuthorName"].ToString();
                DateTime DueDate = (DateTime)dsTTS_Details.Tables[0].Rows[0]["DueDate"];
                txtDueDate.Value = DueDate.ToString("dd MMM yyyy");
                //txtDueDateMainList.Value = DueDate.ToString("dd MMM yyyy");
                BindAssignedTrainers(dsTTS_Details.Tables[1]);
                // Check Wheather logged in User is Trainer or not
                if (dsTTS_Details.Tables[1].Rows.Count > 0)
                {
                    DataTable dtAssignedTrainers = dsTTS_Details.Tables[1];
                    foreach (DataRow drTrainers in dtAssignedTrainers.Rows)
                    {
                        for (int x = 0; x < lbxTrainersTTS.Items.Count; x++)
                        {
                            if (lbxTrainersTTS.Items[x].Value == drTrainers["EmpID"].ToString())
                            {
                                lbxTrainersTTS.Items[x].Selected = true;
                                lbxTrainersTTS.Items[x].Attributes.Add("disabled", "disabled");
                                if (drTrainers["EmpID"].ToString() == hdnEmpID.Value)
                                {
                                    hdnIsAssignedTrainer.Value = "true";
                                }
                            }
                        }
                    }
                }
                //After HQA Approved Status 1 so Approver Name Displayed 
                if (Convert.ToInt32(dsTTS_Details.Tables[0].Rows[0]["Status"]) == 1)
                {
                    divApproverName.Visible = true;
                }
                else
                {
                    divApproverName.Visible = false;
                }
                //For HOD
                if (EmpID == AuthorID)
                {
                    // Represents Target TS Reverted By QA
                    if (CurrentHistoryStatus == 2 || CurrentHistoryStatus == 1 || CurrentHistoryStatus == 3)
                    {
                        btnHodSubmit.Text = "Update";
                        btnHodCancel.Visible = true;
                        divHodReset.Visible = false;
                        divHodActionBtns.Visible = true;
                        ddlTraineeDepartment.Enabled = true;

                        //start commented on 05-05-2020
                        //For getting Select Trainer drop down as in disable mode in Mainlist
                        if (Request.QueryString["MainlstID"] != null)
                        {
                            BindTTS_DetailsToView(dsTTS_Details);
                            ddlTraineeDepartment.Enabled = false;
                        }
                        else
                        {
                            BindTTS_DetailsToEdit(dsTTS_Details);
                        }
                        //end commented on 05-05-2020

                        //txtAuthorComments.Value = string.Empty;
                        //commented on 14-02-2020
                        //Comments
                        if (Request.QueryString["_ShowType"] == "12")
                        {
                            if (dsTTS_Details.Tables[3].Rows.Count > 0)
                            {
                                txtAuthorComments.Value = dsTTS_Details.Tables[3].Rows[0]["AuthorRecentComments"].ToString();
                            }
                        }
                        else
                        {
                            txtAuthorComments.Value = string.Empty;
                        }
                        //if (dsTTS_Details.Tables[3].Rows.Count > 0)
                        //{
                        //    txtAuthorComments.Value = dsTTS_Details.Tables[3].Rows[0]["AuthorRecentComments"].ToString();
                        //}
                        //end commented on 14-02-2020

                    }

                    else
                    {
                        DisableDropDowns();
                        BindTTS_DetailsToView(dsTTS_Details);
                        if (CurrentHistoryStatus == 4)
                        {
                            //commented on 14-02-2020
                            divAuthorComments.Visible = false;
                            //end commented on 14-02-2020
                            btnHodSubmit.Visible = false;
                            // btnHodCancel.Visible = false;
                            divHodReset.Visible = false;
                        }
                    }
                }
                //For QA
                else
                {
                    BindTTS_DetailsToView(dsTTS_Details);
                    DisableDropDowns();
                    txtAuthorComments.Disabled = true;
                    if (CurrentHistoryStatus == 1 || CurrentHistoryStatus == 3)
                    {
                        btnHodSubmit.Visible = false;
                        divHodReset.Visible = false;
                        txtAuthorComments.Disabled = true;
                        txtDueDate.Disabled = true;
                        btnHodCancel.Text = "Close";
                        //Comments
                        if (dsTTS_Details.Tables[3].Rows.Count > 0)
                        {
                            txtAuthorComments.Value = dsTTS_Details.Tables[3].Rows[0]["AuthorRecentComments"].ToString();
                        }
                    }
                    else
                    {
                        divAuthorComments.Visible = false;
                        divHodActionBtns.Visible = false;
                        if (CurrentHistoryStatus == 1)
                        {
                            btnHodSubmit.Visible = false;
                            btnHodCancel.Text = "Close";
                            divHodReset.Visible = false;
                            btnHodCancel.Visible = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                strMsg = "TTS_M3:" + strline + "  " + strMsg;
                //HelpClass.custAlertMsg(this, this.GetType(),strMsg , "error");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "custAlertMsg('" + strMsg + "','error');", true);
            }
        }

        private void BindTTS_DetailsToEdit(DataSet dsTTS_Details)
        {
            ApplyGridStylesToShow4Columns();
            // Bind MainDetails
            if (dsTTS_Details.Tables[0].Rows.Count > 0)
            {
                //BindAuthorAccessiableDept(dsTTS_Details.Tables[5]);
                if (ddlDepartmentTTS.Items.Count > 0)
                {
                    if (Request.QueryString["MainlstID"] != null)
                    {
                        ddlDepartmentTTS.Items.Insert(0, new ListItem(dsTTS_Details.Tables[0].Rows[0]["Department"].ToString(),
                      dsTTS_Details.Tables[0].Rows[0]["DeptID"].ToString()));
                    }
                    else
                    {
                        // start commented on 23 - 01 - 2020
                        if (ddlDepartmentTTS.Items.Contains(new ListItem(dsTTS_Details.Tables[0].Rows[0]["Department"].ToString(),
                        dsTTS_Details.Tables[0].Rows[0]["DeptID"].ToString())))
                        {
                            ddlDepartmentTTS.Items.FindByValue(dsTTS_Details.Tables[0].Rows[0]["DeptID"].ToString()).Selected = true;
                        }
                    }


                    //end commented on 23-01-2020
                }
                BindDocType(dsTTS_Details.Tables[6]);
                if (ddlDocumentType.Items.Count > 0)
                {
                    if (ddlDocumentType.Items.Contains(new ListItem(dsTTS_Details.Tables[0].Rows[0]["DocumentType"].ToString(),
                    dsTTS_Details.Tables[0].Rows[0]["DocumentTypeID"].ToString())))
                    {
                        ddlDocumentType.Items.FindByValue(dsTTS_Details.Tables[0].Rows[0]["DocumentTypeID"].ToString()).Selected = true;
                    }
                }

                BindSelectAssignedTrainers(dsTTS_Details.Tables[7], dsTTS_Details.Tables[1]);
                string TargetType = dsTTS_Details.Tables[0].Rows[0]["TargetType"].ToString();
                ddlTargetType.Items.FindByValue(TargetType).Selected = true;
                ddlTargetType.Enabled = true;
                //if (TargetType == "1")
                //{
                //    string month = dsTTS_Details.Tables[0].Rows[0]["Month"].ToString();
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "showalert", "showHideDivsByDocType('1','1');", true);
                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "showalert", "showHideDivsByDocType('1','0');", true);
                //}
                if (dsTTS_Details.Tables[10].Rows.Count > 0)
                {
                    BindDocuments(dsTTS_Details.Tables[10]);
                    if (ddlTrainingDocument.Items.Contains(new ListItem(dsTTS_Details.Tables[0].Rows[0]["Document"].ToString(),
                                                                              dsTTS_Details.Tables[0].Rows[0]["DocumentID"].ToString())))
                    {
                        ddlTrainingDocument.Items.FindByValue(dsTTS_Details.Tables[0].Rows[0]["DocumentID"].ToString()).Selected = true;
                    }
                }
                ddlTypeOfTraining.Items.FindByValue(dsTTS_Details.Tables[0].Rows[0]["TypeOfTraining"].ToString()).Selected = true;
                txtStatusTTS.Value = dsTTS_Details.Tables[0].Rows[0]["CurrentStatus"].ToString();
                DateTime DueDate = (DateTime)dsTTS_Details.Tables[0].Rows[0]["DueDate"];
                txtDueDate.Value = DueDate.ToString("dd MMM yyyy");
                BindTraineeDepartments(dsTTS_Details.Tables[4]);
                //Bind Trainees
                if (dsTTS_Details.Tables[2].Rows.Count > 0)
                {
                    DataTable dtAssignedTrainees = dsTTS_Details.Tables[2];
                    DataColumn dcStatus = new DataColumn("Status");
                    dtAssignedTrainees.Columns.Add(dcStatus);
                    foreach (DataRow dr in dtAssignedTrainees.Rows)
                    {
                        dr["Status"] = "N";
                    }
                    bindTrainees(dtAssignedTrainees, ddlTraineeDepartment.SelectedValue);
                }
            }
        }

        private void BindTTS_DetailsToView(DataSet dsTTS_Details)
        {
            if (!IsTTS_DetailsInIt)
            {
                ApplyGridStylesToShow3Columns();
                // Bind MainDetails
                if (dsTTS_Details.Tables[0].Rows.Count > 0)
                {
                    ddlDepartmentTTS.Items.Insert(0, new ListItem(dsTTS_Details.Tables[0].Rows[0]["Department"].ToString(),
                        dsTTS_Details.Tables[0].Rows[0]["DeptID"].ToString()));
                    ddlDocumentType.Items.Insert(0, new ListItem(dsTTS_Details.Tables[0].Rows[0]["DocumentType"].ToString(),
                        dsTTS_Details.Tables[0].Rows[0]["DocumentTypeID"].ToString()));

                    string TargetType = dsTTS_Details.Tables[0].Rows[0]["TargetType"].ToString();
                    ddlTargetType.Items.FindByValue(TargetType).Selected = true;

                    //if (TargetType == "1")
                    //{
                    //    string month = dsTTS_Details.Tables[0].Rows[0]["Month"].ToString();
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "showalert", "showHideDivsByDocType('1','1');", true);
                    //}
                    //else
                    //{
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "showalert", "showHideDivsByDocType('1','0');", true);
                    //}
                    ddlTrainingDocument.Items.Insert(0, new ListItem(dsTTS_Details.Tables[0].Rows[0]["Document"].ToString(),
                                                      dsTTS_Details.Tables[0].Rows[0]["DocumentID"].ToString()));
                    ddlTypeOfTraining.Items.FindByValue(dsTTS_Details.Tables[0].Rows[0]["TypeOfTraining"].ToString()).Selected = true;
                    txtStatusTTS.Value = dsTTS_Details.Tables[0].Rows[0]["CurrentStatus"].ToString();
                    DateTime DueDate = (DateTime)dsTTS_Details.Tables[0].Rows[0]["DueDate"];
                    txtDueDate.Value = DueDate.ToString("dd MMM yyyy");
                    BindTraineeDepartments(dsTTS_Details.Tables[4]);
                    //Bind Trainees
                    if (dsTTS_Details.Tables[2].Rows.Count > 0)
                    {
                        DataTable dtAssignedTrainees = dsTTS_Details.Tables[2];
                        DataColumn dcStatus = new DataColumn("Status");
                        dtAssignedTrainees.Columns.Add(dcStatus);
                        foreach (DataRow dr in dtAssignedTrainees.Rows)
                        {
                            dr["Status"] = "N";
                        }
                        bindTrainees(dtAssignedTrainees, ddlTraineeDepartment.SelectedValue);
                    }
                }
                ddlDepartmentTTS.Enabled = ddlDocumentType.Enabled = ddlTrainingDocument.Enabled = ddlTypeOfTraining.Enabled = false;
                txtDueDate.Attributes.Add("disabled", "disabled");
                //start commented by pradeep 
                //txtDueDateMainList.Disabled =true;
                //end

                IsTTS_DetailsInIt = true;
            }
        }

        private void BindTraineeDepartments(DataTable dtTraineeDept)
        {
            //Trainee department           
            ddlTraineeDepartment.DataSource = dtTraineeDept;
            ddlTraineeDepartment.DataTextField = "DepartmentName";
            ddlTraineeDepartment.DataValueField = "DeptID";
            ddlTraineeDepartment.DataBind();
            ddlTraineeDepartment.Items.Insert(0, new ListItem("-- All Departments --", "0"));
        }

        private void BindSelectAssignedTrainers(DataTable dtAllTrainers, DataTable dtAssignedTrainers)
        {
            lbxTrainersTTS.DataSource = dtAllTrainers;
            lbxTrainersTTS.DataTextField = "EmpName";
            lbxTrainersTTS.DataValueField = "EmpID";
            lbxTrainersTTS.DataBind();
            for (int t = 0; t < lbxTrainersTTS.Items.Count; t++)
            {
                string TrainerID = lbxTrainersTTS.Items[t].Value;
                if (dtAssignedTrainers.Select("EmpID='" + TrainerID + "'").Length > 0)
                {
                    lbxTrainersTTS.Items[t].Selected = true;
                }
            }
        }

        public void BindAssignedTrainers(DataTable dtTrainers)
        {
            try
            {
                lbxTrainersTTS.DataSource = dtTrainers;
                lbxTrainersTTS.DataTextField = "EmpName";
                lbxTrainersTTS.DataValueField = "EmpID";
                lbxTrainersTTS.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M15b:" + strline + "  " + strMsg, "error");
            }
        }

        private void ApplyGridStylesToShow4Columns()
        {
            gvTraineeSelector.Columns[0].Visible = true;
            gvTraineeSelected.Columns[0].Visible = true;
            gvTraineeSelector.Columns[0].HeaderStyle.CssClass = "col-2";
            gvTraineeSelector.Columns[0].ItemStyle.CssClass = "col-2";
            gvTraineeSelector.Columns[3].HeaderStyle.CssClass = "col-3";
            gvTraineeSelector.Columns[3].ItemStyle.CssClass = "col-3 textAlignLeft";
            gvTraineeSelector.Columns[4].HeaderStyle.CssClass = "col-3";
            gvTraineeSelector.Columns[4].ItemStyle.CssClass = "col-3";
            gvTraineeSelector.Columns[5].HeaderStyle.CssClass = "col-4";
            gvTraineeSelector.Columns[5].ItemStyle.CssClass = "col-4 textAlignLeft";

            gvTraineeSelected.Columns[0].HeaderStyle.CssClass = "col-2";
            gvTraineeSelected.Columns[0].ItemStyle.CssClass = "col-2";
            gvTraineeSelected.Columns[3].HeaderStyle.CssClass = "col-3";
            gvTraineeSelected.Columns[3].ItemStyle.CssClass = "col-3 textAlignLeft";
            gvTraineeSelected.Columns[4].HeaderStyle.CssClass = "col-3";
            gvTraineeSelected.Columns[4].ItemStyle.CssClass = "col-3";
            gvTraineeSelected.Columns[5].HeaderStyle.CssClass = "col-4";
            gvTraineeSelected.Columns[5].ItemStyle.CssClass = "col-4 textAlignLeft";
        }

        private void ApplyGridStylesToShow3Columns()
        {
            gvTraineeSelector.Columns[3].HeaderStyle.CssClass = "col-3";
            gvTraineeSelector.Columns[3].ItemStyle.CssClass = "col-3 textAlignLeft";
            gvTraineeSelector.Columns[4].HeaderStyle.CssClass = "col-3";
            gvTraineeSelector.Columns[4].ItemStyle.CssClass = "col-3";
            gvTraineeSelector.Columns[5].HeaderStyle.CssClass = "col-3";
            gvTraineeSelector.Columns[5].ItemStyle.CssClass = "col-3 textAlignLeft";
            gvTraineeSelector.Enabled = false;

            gvTraineeSelected.Columns[3].HeaderStyle.CssClass = "col-3";
            gvTraineeSelected.Columns[3].ItemStyle.CssClass = "col-3 textAlignLeft";
            gvTraineeSelected.Columns[4].HeaderStyle.CssClass = "col-3";
            gvTraineeSelected.Columns[4].ItemStyle.CssClass = "col-3";
            gvTraineeSelected.Columns[5].HeaderStyle.CssClass = "col-3";
            gvTraineeSelected.Columns[5].ItemStyle.CssClass = "col-3 textAlignLeft";
            gvTraineeSelected.Enabled = false;
        }

        private void bindTrainees(DataTable dtTrainees, string DeptID)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();

                if (Convert.ToInt32(ddlTargetType.SelectedValue) == 4) //added to load Trainees for General Type
                {
                    if (Convert.ToInt32(hdnTTS_CurrentStatus.Value) == 2 || Convert.ToInt32(hdnTTS_CurrentStatus.Value) == 1)
                    {
                        if (dtTrainees.Rows.Count == 0)
                        {
                            dtTrainees = objTMS_BAL.EditTraineesForGeneralTraining(DeptID, ddlTrainingDocument.SelectedValue, hdnTTS_ID.Value, null, 1);
                        }
                        else
                        {
                            dtTrainees = objTMS_BAL.EditTraineesForGeneralTraining(DeptID, ddlTrainingDocument.SelectedValue, hdnTTS_ID.Value, dtTrainees, 1);
                        }
                    }
                    else
                    {
                        if (dtTrainees.Rows.Count == 0)
                        {
                            dtTrainees = objTMS_BAL.GetDeptTraineesForGeneral(DeptID, ddlTrainingDocument.SelectedValue, null, 1);
                        }
                        else
                        {
                            dtTrainees = objTMS_BAL.GetDeptTraineesForGeneral(DeptID, ddlTrainingDocument.SelectedValue, dtTrainees, 1);
                        }
                    }
                }
                else
                {
                    if (dtTrainees.Rows.Count == 0)
                    {
                        dtTrainees = objTMS_BAL.GetDeptTrainees(DeptID);
                    }
                    else
                    {
                        dtTrainees = objTMS_BAL.GetDeptTrainees(DeptID, dtTrainees);
                    }
                }
                ViewState["Trainees"] = null;
                ViewState["Trainees"] = dtTrainees;
                DataView dvTrainees = new DataView(dtTrainees);
                dvTrainees.RowFilter = "Status='Y' and DeptID=" + DeptID;
                gvTraineeSelector.DataSource = dvTrainees.ToTable();
                gvTraineeSelector.DataBind();

                if (ddlTraineeDepartment.SelectedValue == "0")
                {
                    dvTrainees.RowFilter = "Status='N'";
                    gvTraineeSelected.DataSource = dvTrainees.ToTable();
                    gvTraineeSelected.DataBind();
                }
                else
                {
                    dvTrainees.RowFilter = "Status='N' and DeptID=" + DeptID;
                    gvTraineeSelected.DataSource = dvTrainees.ToTable();
                    gvTraineeSelected.DataBind();
                }
                EnableDisableButtons();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M20:" + strline + "  " + strMsg, "error");
            }
        }

        private void DisableDropDowns()
        {
            ddlDepartmentTTS.Enabled = false;
            ddlDocumentType.Enabled = false;
            ddlTargetType.Enabled = false;
            ddlTrainingDocument.Enabled = false;
            ddlTypeOfTraining.Enabled = false;
            txtDueDate.Attributes.Add("disabled", "disabled");
            //For Disable comments Text box
            txtAuthorComments.Disabled = true;

            //start commented by pradeep 09-04-2019
            //txtDueDateMainList.Disabled = true;
            //end

            //lbxTrainersTTS.Enabled = false;
            // ddlAuthorTTS.Enabled = false;
            //ddlReviewerTTS.Enabled = false;
            gvTraineeSelected.Enabled = false;
            gvTraineeSelector.Enabled = false;
            btnAddTrainees.Enabled = false;
            //lbxTrainersTTS.CssClass = "col-lg-12 selectpicker show-tick form-control padding-none testLbx";
            //lbxTrainersTTS.Attributes.Add("disabled", "disabled");
            btnAddTrainees.CssClass = "col-lg-4 offset-lg-4 btn_selection_box btnAddTrainers";
            btnAddTrainees.Attributes.Add("style", "cursor:not-allowed;");
            btnRemoveTrainees.Enabled = false;
            btnRemoveTrainees.CssClass = "col-lg-4 offset-lg-4  btn_selection_box btnAddTrainers";
            btnRemoveTrainees.Attributes.Add("style", "cursor:not-allowed;");
        }
        public void BindDocType()
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                DataTable dt = objTMS_BAL.DocumentType();
                ddlDocumentType.DataSource = dt;
                ddlDocumentType.DataValueField = "DocumentTypeID";
                ddlDocumentType.DataTextField = "DocumentType";
                ddlDocumentType.DataBind();
                //ddlDocumentType.Items.Insert(0, new ListItem("--Select--", "0"));
                //upDeptAndDocType.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindDocType(DataTable dt)
        {
            ddlDocumentType.DataSource = dt;
            ddlDocumentType.DataValueField = "DocumentTypeID";
            ddlDocumentType.DataTextField = "DocumentType";
            ddlDocumentType.DataBind();
        }

        public void BindAuthorAccessiableDept(DataTable dt)
        {
            ddlDepartmentTTS.DataSource = dt;
            ddlDepartmentTTS.DataTextField = "DepartmentName";
            ddlDepartmentTTS.DataValueField = "DeptID";
            ddlDepartmentTTS.DataBind();
            ddlDepartmentTTS.Items.Insert(0, new ListItem("- Select Department -", "0"));
        }
        public void bindDepartments(int EmpID)
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                int[] RoleIDs = new int[] { (int)TMS_UserRole.HOD_TMS };
                DataTable dt = objUMS_BAL.getRoleWiseDepts(EmpID, RoleIDs);
                ddlDepartmentTTS.Items.Clear();
                ddlDepartmentTTS.DataSource = dt;
                ddlDepartmentTTS.DataTextField = "DepartmentName";
                ddlDepartmentTTS.DataValueField = "DeptID";
                ddlDepartmentTTS.DataBind();
                ddlDepartmentTTS.Items.Insert(0, new ListItem("- Select Department -", "0"));

                BindTraineeDepartment(); //Trainee department
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M7:" + strline + "  " + strMsg, "error");
            }
        }

        public void BindTraineeDepartment()
        {
            try
            {
                //Trainee department
                objUMS_BAL = new UMS_BAL();
                DataTable dtTraineeDept = objUMS_BAL.BindDepartmentsBAL();
                ddlTraineeDepartment.DataSource = dtTraineeDept;
                ddlTraineeDepartment.DataTextField = "DepartmentName";
                ddlTraineeDepartment.DataValueField = "DeptID";
                ddlTraineeDepartment.DataBind();
                ddlTraineeDepartment.Items.Insert(0, new ListItem("-- All Departments --", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M:" + strline + "  " + strMsg, "error");
            }
        }

        public void ClearGV()
        {
            DataTable ds = new DataTable();
            ds = null;
            gvTraineeSelector.DataSource = ds;
            gvTraineeSelector.DataBind();
            gvTraineeSelected.DataSource = ds;
            gvTraineeSelected.DataBind();
        }
        protected void ddlTargetType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlTrainingDocument.Items.Clear();
                if (Convert.ToInt32(ddlTargetType.SelectedValue) == 0 || Convert.ToInt32(ddlTargetType.SelectedValue) > 0)
                {
                    ddlTraineeDepartment.Enabled = false;
                    ddlTraineeDepartment.SelectedIndex = 0;
                    ClearGV();
                }
                if (ddlTargetType.SelectedValue == "4")
                {
                    getEffectedDocsForGeneral();
                }
                else
                {
                    BindDocAndMonths(ddlTargetType.SelectedValue);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M8:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindDocAndMonths(string TargetType)
        {
            objTMS_BAL = new TMS_BAL();
            //ddlRefresherMonth.Items.Clear();
            ddlTrainingDocument.Items.Clear();
            // New SOP / Revision
            if (ddlTargetType.SelectedValue == "2" || ddlTargetType.SelectedValue == "3")
            {
                if (ddlDepartmentTTS.SelectedValue != "0" && ddlDocumentType.SelectedValue != "")
                {
                    // Get Documents 
                    DataTable dt = bindDocuments(Convert.ToInt32(ddlDepartmentTTS.SelectedValue),
                        Convert.ToInt32(ddlTargetType.SelectedValue), Convert.ToInt32(ddlDocumentType.SelectedValue));
                    if (dt.Rows.Count > 0)
                    {
                        BindDocuments(dt);
                    }
                }
                else if (ddlDepartmentTTS.SelectedValue != "0" && ddlDocumentType.SelectedValue == "")
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Document Type Not Exists to Get Documents", "warning");
                }
            }
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "showalert", "showHideDivsByTargetType('" + ddlTargetType.SelectedValue + "');", true);
        }
        private void BindDocuments(DataTable dt)
        {
            try
            {
                ddlTrainingDocument.DataSource = dt;
                ddlTrainingDocument.DataValueField = "DocumentID";
                ddlTrainingDocument.DataTextField = "DocumentName";
                ddlTrainingDocument.DataBind();
                ddlTrainingDocument.Items.Insert(0, new ListItem("-Select Document-", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M9:" + strline + "  " + strMsg, "error");
            }
        }
        public DataTable bindDocuments(int DeptID, int targetType, int DocumentType)
        {
            //try
            //{
            objTMS_BAL = new TMS_BAL();
            //DataTable dt = objTMS_BAL.getSOPsByTargetType(DeptID, targetType, DocumentType);
            DataTable dt = objTMS_BAL.getEffectedDocsByTargetType(DeptID, targetType, DocumentType);
            return dt;
        }
        protected void ddlDocumentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //ddlTargetType.SelectedValue = "0";
                //ddlTrainingDocument.Items.Clear();
                //if (ddlDocumentType.SelectedValue == "2")
                //{
                //    int deptID = Convert.ToInt32(ddlDepartmentTTS.SelectedValue);
                //    if (deptID > 0)
                //    {
                //        bindCourseDocs(deptID);
                //    }
                //}

                //upTargetType.Update();
                //upMonth.Update();
                //upTrainingDoc.Update();
                //if (ddlDocumentType.SelectedValue == "1")
                //{
                // ScriptManager.RegisterStartupScript(this, this.GetType(), "showalert", "showHideDivsByDocType('" + ddlDocumentType.SelectedValue + "','" + ddlTargetType.SelectedValue + "');", true);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "showalert", "showHideDivsByDocType('1','" + ddlTargetType.SelectedValue + "');", true);

                //  }
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "showalert", "showHideDivsByDocType('" + ddlDocumentType.SelectedValue + "','0');", true);
                //}
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M10:" + strline + "  " + strMsg, "error");
            }
        }


        public void getEffectedDocsForGeneral()
        {
            try
            {
                objTMS_BAL = new TMS_BAL();

                if (ddlDepartmentTTS.SelectedValue != "0" && ddlDocumentType.SelectedValue != "")
                {
                    DataTable dt = objTMS_BAL.getEffectedDocumentsForGeneral(Convert.ToInt32(ddlDepartmentTTS.SelectedValue),
                        Convert.ToInt32(ddlDocumentType.SelectedValue));
                    if (dt.Rows.Count > 0)
                    {
                        BindDocuments(dt);
                    }
                }
                else if (ddlDepartmentTTS.SelectedValue != "0" && ddlDocumentType.SelectedValue == "")
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Document Type Not Exists to Get Documents", "warning");
                }
                //upTrainingDoc.Update();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "showalert", "showHideDivsByTargetType('" + ddlTargetType.SelectedValue + "');", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M15:" + strline + "  " + strMsg, "error");
            }
        }
        protected void ddlDepartmentTTS_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //ddlDocumentType.SelectedIndex = 0;
                ddlTrainingDocument.Items.Clear();

                if (Convert.ToInt32(ddlDepartmentTTS.SelectedValue) == 0 || ddlDepartmentTTS.SelectedValue != "0")
                {
                    ddlTraineeDepartment.Enabled = false;
                    ddlTraineeDepartment.SelectedIndex = 0;
                    ClearGV();
                }
                if (ddlDepartmentTTS.SelectedValue != "0")
                {
                    if (ddlDocumentType.Items.Count == 0)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Document Type Not Exists to get details.", "error");
                    }
                    else
                    {
                        BindDropDowns(ddlDepartmentTTS.SelectedValue, hdnEmpID.Value);
                        getEffectedDocsForGeneral();
                    }
                }
                else
                {
                    lbxTrainersTTS.Items.Clear();
                    ddlReviewerTTS.Items.Clear();
                    ddlTargetType.Enabled = false;
                    ddlTargetType.CssClass = "selectpicker form-control";
                }

                ddlTargetType.SelectedValue = "4";

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "showalert", "showHideDivsByTargetType('0');", true);

                //upTargetType.Update();
                //upActionUsers.Update();
                //upTrainingDoc.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M16:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindDropDowns(string Dept_ID, string Author, int IsEmpActive = 0)
        {
            try
            {
                //ddlTargetType.Enabled = true;
                //ddlTargetType.CssClass = "selectpicker form-control";
                objTMS_BAL = new TMS_BAL();
                DataSet ds = objTMS_BAL.GetRoleWiseEmployees(Convert.ToInt32(Dept_ID));

                // Trainer 
                lbxTrainersTTS.DataSource = ds.Tables[0];
                lbxTrainersTTS.DataTextField = "EmpName";
                lbxTrainersTTS.DataValueField = "EmpID";
                lbxTrainersTTS.DataBind();

                //// Reviewer(QA)
                //DataView dvReviewer = new DataView(ds.Tables[2]);
                //dvReviewer.RowFilter = "EmpID<>" + Author;
                //ddlReviewerTTS.DataSource = dvReviewer.ToTable();
                //ddlReviewerTTS.DataTextField = "EmpName";
                //ddlReviewerTTS.DataValueField = "EmpID";
                //ddlReviewerTTS.DataBind();
                //ddlReviewerTTS.Items.Remove(hdnEmpID.Value);
                //ddlReviewerTTS.Items.Insert(0, new ListItem("- Select Approver -", "0"));
                //ddlReviewerTTS.Items.Insert(0,new ListItem(ds.Tables[2].Rows[0]["EmpName"].ToString(),ds.Tables[2].Rows[0]["EmpID"].ToString()));//Need To Verify
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M17:" + strline + "  " + strMsg, "error");
            }
        }

        //All Employee's in View Mode
        public void BindDropDownsAllEmps(string Dept_ID, string Author, int IsEmpActive = 0)
        {
            try
            {
                ddlTargetType.Enabled = true;
                ddlTargetType.CssClass = "selectpicker form-control drop_down";
                objTMS_BAL = new TMS_BAL();
                DataSet ds = objTMS_BAL.GetRoleWiseEmployees(Convert.ToInt32(Dept_ID), 1);

                // Trainer 
                lbxTrainersTTS.DataSource = ds.Tables[0];
                lbxTrainersTTS.DataTextField = "EmpName";
                lbxTrainersTTS.DataValueField = "EmpID";
                lbxTrainersTTS.DataBind();

            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M18:" + strline + "  " + strMsg, "error");
            }
        }

        #endregion

        #region Trainee List
        protected void ddlTraineeDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindTrainersGv(ddlTraineeDepartment.SelectedValue);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M19:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindTrainersGv(string DeptID)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                DataTable dtVSTrainees = ViewState["Trainees"] as DataTable;
                bindTrainees(dtVSTrainees, DeptID);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M20:" + strline + "  " + strMsg, "error");
            }
        }
        public void InitializeDataTables()
        {
            try
            {
                DataTable dtTrainers = new DataTable();
                dtTrainers.Columns.Add(new DataColumn("EmpID"));
                dtTrainers.Columns.Add(new DataColumn("EmpCode"));
                dtTrainers.Columns.Add(new DataColumn("EmpName"));
                dtTrainers.Columns.Add(new DataColumn("DeptID"));
                dtTrainers.Columns.Add(new DataColumn("DeptCode"));
                dtTrainers.Columns.Add(new DataColumn("Status"));
                ViewState["Trainees"] = dtTrainers;
                gvTraineeSelected.DataSource = new List<object>();
                gvTraineeSelected.DataBind();
                gvTraineeSelector.DataSource = new List<object>();
                gvTraineeSelector.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M21:" + strline + "  " + strMsg, "error");
            }
        }
        protected void ddlTrainingDocument_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                if (ddlTrainingDocument.SelectedValue != "0")
                {
                    ddlTraineeDepartment.SelectedIndex = 0;
                    ddlTraineeDepartment.Enabled = true;
                }
                else if (ddlTrainingDocument.SelectedIndex == 0)
                {
                    ddlTraineeDepartment.SelectedIndex = 0;
                    ddlTraineeDepartment.Enabled = false;
                    ClearGV();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M22:" + strline + "  " + strMsg, "error");
            }
        }



        protected void btnAddTrainees_Click(object sender, EventArgs e)
        {
            try
            {
                int TrainerCount = 0;
                foreach (GridViewRow gvrTrainees in gvTraineeSelector.Rows)
                {
                    if (((CheckBox)gvrTrainees.FindControl("cbTraineeGv")).Checked)
                    {
                        TrainerCount++;
                    }
                }
                if (TrainerCount == 0)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Select atleast one Trainee from available Trainees.", "error");
                }
                else
                {
                    DataTable dtTrainees = (DataTable)ViewState["Trainees"];
                    foreach (GridViewRow gvrTrainees in gvTraineeSelector.Rows)
                    {
                        if (((CheckBox)gvrTrainees.FindControl("cbTraineeGv")).Checked)
                        {
                            int EmpID = Convert.ToInt32(((Label)gvrTrainees.FindControl("lblEmpIDGv")).Text);
                            int DeptID = Convert.ToInt32(((Label)gvrTrainees.FindControl("lblEmpDeptIDGv")).Text);

                            foreach (DataRow drSelectedTrainees in dtTrainees.Rows)
                            {
                                if (Convert.ToInt32(drSelectedTrainees["EmpID"]) == EmpID && Convert.ToInt32(drSelectedTrainees["DeptID"]) == DeptID)
                                {
                                    drSelectedTrainees["Status"] = "N";
                                }
                            }
                        }
                    }
                    //ViewState["Trainees"] = dtTrainees;
                    bindTrainees(dtTrainees, ddlTraineeDepartment.SelectedValue);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M23:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnRemoveTrainees_Click(object sender, EventArgs e)
        {
            try
            {
                int TrainerCount = 0;
                foreach (GridViewRow gvrTrainees in gvTraineeSelected.Rows)
                {
                    if (((CheckBox)gvrTrainees.FindControl("cbTraineeGv")).Checked)
                    {
                        TrainerCount++;
                    }
                }
                if (TrainerCount == 0)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Select atleast one Trainee from Assigned Trainees.", "error");
                }
                else
                {
                    DataTable dtTrainees = (DataTable)ViewState["Trainees"];

                    foreach (GridViewRow gvrAssignedTrainees in gvTraineeSelected.Rows)
                    {
                        if (((CheckBox)gvrAssignedTrainees.FindControl("cbTraineeGv")).Checked)
                        {
                            int EmpID = Convert.ToInt32(((Label)gvrAssignedTrainees.FindControl("lblEmpIDGv")).Text);
                            int DeptID = Convert.ToInt32(((Label)gvrAssignedTrainees.FindControl("lblEmpDeptIDGv")).Text);

                            foreach (DataRow drSelectedTrainees in dtTrainees.Rows)
                            {
                                if (Convert.ToInt32(drSelectedTrainees["EmpID"]) == EmpID && Convert.ToInt32(drSelectedTrainees["DeptID"]) == DeptID)
                                {
                                    drSelectedTrainees["Status"] = "Y";
                                }
                            }
                        }
                    }
                    ViewState["Trainers"] = dtTrainees;
                    bindTrainees(dtTrainees, ddlTraineeDepartment.SelectedValue);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M24:" + strline + "  " + strMsg, "error");
            }
        }
        public void EnableDisableButtons()
        {
            try
            {
                if (hdnTTS_CurrentStatus.Value == "0" || hdnTTS_CurrentStatus.Value == "2" || hdnTTS_CurrentStatus.Value == "1" || hdnTTS_CurrentStatus.Value == "3") // 0 for creation, 2 reverted tts
                {
                    if (gvTraineeSelected.Rows.Count == 0)
                    {
                        btnRemoveTrainees.Enabled = false;
                        btnRemoveTrainees.CssClass = "col-lg-4 offset-lg-4  btn_selection_box ";
                        btnRemoveTrainees.Attributes.Add("style", "cursor:not-allowed;");
                    }
                    else
                    {
                        btnRemoveTrainees.Enabled = true;
                        btnRemoveTrainees.Style.Remove("cursor");
                    }
                    if (gvTraineeSelector.Rows.Count == 0)
                    {
                        btnAddTrainees.Enabled = false;
                        btnAddTrainees.CssClass = "col-lg-4 offset-lg-4  btn_selection_box btnAddTrainers";
                        btnAddTrainees.Attributes.Add("style", "cursor:not-allowed;");
                    }
                    else
                    {
                        btnAddTrainees.Enabled = true;
                        btnAddTrainees.Style.Remove("cursor");
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M25:" + strline + "  " + strMsg, "error");
            }
        }
        #endregion

        #region HOD Actions    
        protected void btnHodSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                int IncidentID = Convert.ToInt32(Request.QueryString["IMID"]);
                string DepartmentName = "";
                if (!QMSCommonActions.GetIsTMSHODRoleExist())
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", "custAlertMsg('You are not Authorised Training Coordinator to prepare Training Schedule.','error');", true);
                }
                else if (!QMSCommonActions.TMSHODAccesibleDeptExist(IncidentID, ref DepartmentName, Convert.ToInt32(hdnEmpID.Value), Convert.ToInt32(ddlDepartmentTTS.SelectedValue)))
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "You are not having Accessible " + DepartmentName + " Department for Training Coordinator role.", "error");
                }
                else
                {
                    //For HOD Create or Modify
                    if (hdnTTS_CurrentStatus.Value == "0" || hdnTTS_CurrentStatus.Value == "1" || hdnTTS_CurrentStatus.Value == "2" || hdnTTS_CurrentStatus.Value == "3")
                    {
                        SubmitByHOD(hdnTTS_CurrentStatus.Value);
                    }
                }



            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M37:" + strline + "  " + strMsg, "error");
            }
        }


        public void SubmitByHOD(string CurrentStatus)
        {
            try
            {
                DataTable dtViewStateTrainees = (DataTable)ViewState["Trainees"];
                DataTable dtAssignedTrainees = new DataTable();
                dtAssignedTrainees.TableName = "Trainees";
                dtAssignedTrainees.Columns.Add("EmpID");
                //converting view state of trainees to datatable
                foreach (DataRow dr in dtViewStateTrainees.Rows)
                {
                    if (dr["Status"].ToString() == "N")
                    {
                        DataRow drTrainee = dtAssignedTrainees.NewRow();
                        drTrainee["EmpID"] = dr["EmpID"];
                        dtAssignedTrainees.Rows.Add(drTrainee);
                    }
                }
                //converting listbox selected trainers to datatable
                DataTable dtAssignedTrainers = new DataTable();
                dtAssignedTrainers.TableName = "Trainers";
                dtAssignedTrainers.Columns.Add("EmpID");
                for (int x = 0; x < lbxTrainersTTS.Items.Count; x++)
                {
                    if (lbxTrainersTTS.Items[x].Selected.Equals(true))
                    {
                        DataRow dr = dtAssignedTrainers.NewRow();
                        dr["EmpID"] = lbxTrainersTTS.Items[x].Value;
                        dtAssignedTrainers.Rows.Add(dr);
                    }
                }
                DataSet dsTrainersAndTrainees = new DataSet();
                if (dtAssignedTrainers.Rows.Count == 0 || dtAssignedTrainees.Rows.Count == 0)
                {
                    ArrayList Mandatory = new ArrayList();
                    if (dtAssignedTrainers.Rows.Count == 0)
                    {
                        Mandatory.Add("Select atleast one Trainer");
                    }
                    if (dtAssignedTrainees.Rows.Count == 0)
                    {
                        Mandatory.Add("Select atleast one Trainee");
                    }
                    StringBuilder s = new StringBuilder();
                    foreach (string x in Mandatory)
                    {
                        s.Append(x);
                        s.Append(".");
                    }
                    HelpClass.custAlertMsg(this, this.GetType(), s.ToString(), "error");
                }
                else
                {
                    bool IsTraineeAsTrainerorApprover = false;
                    if (dtAssignedTrainees.Rows.Count > 0 && dtAssignedTrainers.Rows.Count > 0)
                    {
                        if (lbxTrainersTTS.Items.Count > 0)
                        {
                            for (int x = 0; x < lbxTrainersTTS.Items.Count; x++)
                            {
                                if (lbxTrainersTTS.Items[x].Selected.Equals(true))
                                {
                                    foreach (DataRow dr in dtAssignedTrainees.Rows)
                                    {
                                        if (dr["EmpID"].ToString() == lbxTrainersTTS.Items[x].Value)
                                        {
                                            IsTraineeAsTrainerorApprover = true;
                                        }
                                    }
                                }
                            }
                        }
                        dsTrainersAndTrainees.Tables.Add(dtAssignedTrainers);
                        dsTrainersAndTrainees.Tables.Add(dtAssignedTrainees);
                    }
                    if (!IsTraineeAsTrainerorApprover)
                    {
                        SubmitTS(CurrentStatus, dsTrainersAndTrainees, ddlDepartmentTTS.SelectedValue, ddlDocumentType.SelectedValue
                      , ddlTrainingDocument.SelectedValue, ddlTypeOfTraining.SelectedValue, hdnEmpID.Value, txtDueDate.Value,
                      /*hdnIncidentQAID.Value*/ 0, txtAuthorComments.Value, ddlTargetType.SelectedValue, "0");
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", "custAlertMsg('Trainee shouldn\\'t be Trainer or Approver.','info');", true);
                    }

                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M38:" + strline + "  " + strMsg, "error");
            }
        }

        public void SubmitTS(string CurrentStatus, DataSet dsTrainersAndTrainees, string Dept_ID,
            string Document_Type, string Document_ID, string TypeOFTraining, string AuthorBy, string DueDate, int ReviewBy = 0,
            string Remarks = "", string TargetType = "0", string MonthID = "0")
        {
            try
            {
                objQMS_BAL = new QMS_BAL();
                TTS_QIObjects objTTS = new TTS_QIObjects();
                objTTS.dsTrainersAndTrainees = dsTrainersAndTrainees;
                objTTS.Dept_ID = Convert.ToInt32(ddlDepartmentTTS.SelectedValue);
                objTTS.Document_Type = Convert.ToInt32(Document_Type);
                objTTS.Document_ID = Convert.ToInt32(Document_ID);
                objTTS.TypeOFTraining = Convert.ToChar(TypeOFTraining);
                objTTS.AuthorEmpId = Convert.ToInt32(AuthorBy);
                objTTS.Remarks = Remarks;
                objTTS.DueDate = DueDate;
                objTTS.IncidentID = Convert.ToInt32(Request.QueryString["IMID"]);

                if (MonthID == "")
                {
                    objTTS.MonthID = 0;
                }
                else
                {
                    objTTS.MonthID = Convert.ToInt32(MonthID);
                }
                if (TargetType == "")
                {
                    objTTS.TargetType = 0;
                }
                else
                {
                    objTTS.TargetType = Convert.ToInt32(TargetType);
                }
                if (CurrentStatus == "0")// HOD Creation
                {
                    //created Session for TTS Status
                    Session["Incident_TTS_Status"] = 1; //Create
                    //
                    objTTS.ReviewEmpId = Convert.ToInt32(ReviewBy);
                    objQMS_BAL.CreateTargetTraining(objTTS, out int Result);
                    if (Result == 1)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Training Schedule Created.", "success", "CloseModel()");
                    }
                    if (Result == 2)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Training Schedule Already Created.", "info");
                    }
                }
                if (CurrentStatus == "2" || CurrentStatus == "1" || CurrentStatus == "3")// HOD Modification
                {
                    //Created Session for TTS Status
                    Session["Incident_TTS_Status"] = 2; //Update
                    //
                    objTTS.TargetTS_ID = Convert.ToInt32(hdnTTS_ID.Value);
                    objQMS_BAL.UpdateTargetTraining(objTTS, out int Result);
                    if (Result == 1)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Modified Training Schedule.", "success", "CloseModel()");
                    }
                    else if (Result == 2)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "You are not a Authorized Person to Modified.", "warning");
                    }
                    else if (Result == 3)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Record Already Modified", "info");
                    }
                    else
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Internal Error Occured", "error");
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M39:" + strline + "  " + strMsg, "error");
            }
        }
        #endregion


        protected void lbTTSList_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdnPreviousPage.Value == "PL")//HOD
                {
                    Response.Redirect("/TMS/TargetTrainingSchedule/TTS_Pending.aspx?FilterType=2", false);
                }
                else if (hdnPreviousPage.Value == "AL")//QA
                {
                    Response.Redirect("/TMS/TargetTrainingSchedule/TTS_ApprovalList.aspx?FilterType=2", false);
                }
                else if (hdnPreviousPage.Value == "QA") //Trainer
                {
                    Response.Redirect("/TMS/TargetTrainingSchedule/TTS_ApprovedList.aspx", false);
                }
                else if (hdnPreviousPage.Value == "ML") //HOD or QA
                {
                    Response.Redirect("/TMS/Reports/TargetTS_List.aspx?FilterType=9", false);
                }
                else if (hdnPreviousPage.Value == "CL") //HOD
                {
                    Response.Redirect("/TMS/TargetTrainingSchedule/TTS_CreationList.aspx", false);
                }
                else
                {
                    Response.Redirect("/TMS/TargetTrainingSchedule/TTS_Master.aspx", false);
                }
                if (Session["PreviousPage"] != null)
                {
                    Session.Remove("PreviousPage");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M41:" + strline + "  " + strMsg, "error");
            }
        }

        //public string GetCreatedDate(int incidentID)
        //{
        //    //To get Incident Current status and Hod Name
        //    AizantIT_DevEntities db = new AizantIT_DevEntities();
        //    var getCreatedDate = (from t1 in db.AizantIT_IncidentMaster
        //                                    where t1.IncidentMasterID == incidentID
        //                                    select new { t1.CreatedDate }).SingleOrDefault();

        //    return getCreatedDate;
        //}
    }
}