﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TTSTraining.aspx.cs" Inherits="AizantIT_PharmaApp.QMSIncient.TTSTraining" %>

<%@ Register Src="~/UserControls/ElectronicSign.ascx" TagPrefix="uc1" TagName="ElectronicSign" %>
<%@ Register Src="~/UserControls/TMS_UserControls/Exam/ucExamResultSheet.ascx" TagPrefix="uc1" TagName="ucExamResultSheet" %>
<%@ Register Src="~/UserControls/TMS_UserControls/ucFeedback.ascx" TagPrefix="uc1" TagName="ucFeedback" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <style>
                .btnAddTrainers {
                    margin-bottom: 10px;
                }

                .tblFixHeight tbody {
                    max-height: 210px !important;
                }

                .leftAlign {
                    text-align: left !important;
                }

                .label_point {
                    color: #f84545;
                    font-size: 12pt;
                }

                .table-fixed {
                    border-width: 0px !important;
                    border-color: transparent !important;
                }

                    .table-fixed thead tr th {
                        border-right: none !important;
                    }
            </style>
            <link href="<%=ResolveUrl("~/AppCSS/header/header_styles.css")%>" rel="stylesheet" />
            <link href="<%=ResolveUrl("~/AppCSS/TMS/tms_style.css")%>" rel="stylesheet" />
            <link href="<%=ResolveUrl("~/AppCSS/QMS/css/QMS_Style.css")%>" rel="stylesheet" />
            <link href="<%=ResolveUrl("~/AppCSS/Bootstrap/css/bootstrap4.css")%>" rel="stylesheet" />
            <link href="<%=ResolveUrl("~/Content/font-awesome.css")%>" rel="stylesheet" />
            <link href="<%=ResolveUrl("~/AppCSS/src/richtext.min.css")%>" rel="stylesheet" />
            <link href="<%=ResolveUrl("~/Scripts/jquery-ui.css")%>" rel="stylesheet" />
            <link href="<%=ResolveUrl("~/AppCSS/CustAlerts/SingleCustAlert.css")%>" rel="stylesheet" />
            <link href="<%=ResolveUrl("~/AppCSS/Calender.css")%>" rel="stylesheet" />
            <link href="<%=ResolveUrl("~/Content/bootstrap-select.css")%>" rel="stylesheet" />
            <link href="<%=ResolveUrl("~/AppCSS/leftnav_styles/leftnav.css")%>" rel="stylesheet" />
            <link href="<%=ResolveUrl("~/Content/DataTables/css/dataTables.bootstrap4.min.css")%>" rel="stylesheet" />

            <script src="<%=ResolveUrl("~/AppScripts/GlobalScripts.js")%>"></script>
            <script src="<%=ResolveUrl("~/AppScripts/CookieFunc.min.js")%>"></script>
            <script src="<%=ResolveUrl("~/AppScripts/LogOutFunc.js")%>"></script>
            <script src="<%=ResolveUrl("~/Scripts/jquery-3.2.1.js")%>"></script>
            <script src="<%=ResolveUrl("~/Scripts/popper.js")%>"></script>
            <script src="<%=ResolveUrl("~/AppCSS/Bootstrap/scripts/bootstrap4.min.js")%>"></script>
            <script src="<%=ResolveUrl("~/AppCSS/src/jquery.richtext.js")%>"></script>
            <script src="<%=ResolveUrl("~/Scripts/moment.min.js")%>"></script>
            <script src="<%=ResolveUrl("~/Scripts/bootstrap-datetimepicker.min.js")%>"></script>
            <script src="<%=ResolveUrl("~/Scripts/select2-4.0.3/accordion.js")%>"></script>
            <script src="<%=ResolveUrl("~/Scripts/bootstrap-select.min.js")%>"></script>
            <script src="<%=ResolveUrl("~/AppScripts/LeftActivePage.js")%>"></script>
            <script src="<%=ResolveUrl("~/Scripts/datatables.js")%>"></script>
            <script src="<%=ResolveUrl("~/Scripts/DataTables/dataTables.bootstrap4.min.js")%>"></script>

            <%--<link href="<%=ResolveUrl("~/AppCSS/Bootstrap/css/bootstrap4.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/Content/font-awesome.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/AppCSS/Editor/site.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/AppCSS/src/richtext.min.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/Scripts/jquery-ui.css")%>" rel="stylesheet" />
        <script src="<%=ResolveUrl("~/Scripts/jquery-3.2.1.js")%>"></script>
        <script src="<%=ResolveUrl("~/AppCSS/Bootstrap/scripts/bootstrap4.min.js")%>"></script>
        <script src="<%=ResolveUrl("~/AppCSS/src/jquery.richtext.js")%>"></script>
        <script src="<%=ResolveUrl("~/Scripts/moment.min.js")%>"></script>
        <script src="<%=ResolveUrl("~/Scripts/bootstrap-datetimepicker.min.js")%>"></script>
        <script src="<%=ResolveUrl("~/Scripts/select2-4.0.3/accordion.js")%>"></script>
        <link href="<%=ResolveUrl("~/AppCSS/AppStyles.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/AppCSS/LeftNaviStyle.css")%>" rel="stylesheet" />
        <script src="<%=ResolveUrl("~/AppScripts/GlobalScripts.js")%>"></script>
        <script src="<%=ResolveUrl("~/AppScripts/CookieFunc.min.js")%>"></script>
        <script src="<%=ResolveUrl("~/AppScripts/LogOutFunc.min.js")%>"></script>
        <link href="<%=ResolveUrl("~/AppCSS/CustAlerts/custAlert.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/AppCSS/Main_style.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/AppCSS/Calender.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/AppCSS/UserLogin/UserLoginStyles.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/AppCSS/MyProfileStyles.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/Content/bootstrap-select.css")%>" rel="stylesheet" />
        <script src="<%=ResolveUrl("~/Scripts/bootstrap-select.min.js")%>"></script>

             <link href="<%=ResolveUrl("~/AppCSS/TMS/tms_style.css")%>" rel="stylesheet" />
     <link href="<%=ResolveUrl("~/AppCSS/UMS/UMS_Styles.css")%>" rel="stylesheet" />

      <link href="<%=ResolveUrl("~/Scripts/DataTables-1.10.16/media/css/jquery.dataTables.min.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/Scripts/DataTables-1.10.16/extensions/FixedHeader/css/fixedHeader.dataTables.min.css")%>" rel="stylesheet" />

        <script src="<%=ResolveUrl("~/Scripts/DataTables-1.10.16/media/js/jquery.dataTables.min.js")%>"></script>
        <script src="<%=ResolveUrl("~/Scripts/DataTables-1.10.16/extensions/FixedHeader/js/dataTables.fixedHeader.min.js")%>"></script>--%>


            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <script src="<%= ResolveUrl("~/AppScripts/TableCheckBoxSelection.min.js")%>"></script>
            <asp:HiddenField ID="hdnTTS_ID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnTrainingSessionID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnPreviousPage" runat="server" Value="0" />
            <asp:HiddenField ID="hdnTTS_CurrentStatus" runat="server" Value="0" />
            <asp:HiddenField ID="hdnRecentSessionID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnTrainee" runat="server" Value="0" />
            <asp:HiddenField ID="hdnTargetExamID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnEmpIsHodOrQA" runat="server" Value="N" />
            <asp:HiddenField ID="hdnIsAssignedTrainer" runat="server" Value="false" />
            <asp:HiddenField ID="hdnModeOfTraining" runat="server" Value="1" />
            <asp:HiddenField ID="hdnIncidentQAID" runat="server" Value="0" />
            

            <asp:UpdatePanel UpdateMode="Always" runat="server" ID="uphdnRecentSessionStatus">
                <ContentTemplate>
                    <asp:HiddenField ID="hdnRecentSessionStatus" runat="server" Value="0" />
                    <asp:HiddenField ID="hdnTTS_OnlineSessionID" runat="server" Value="0" />
                    <asp:HiddenField ID="hdnDocID" runat="server" Value="0" />
                    <asp:HiddenField ID="hdnEmpFeedbackID" runat="server" Value="0" />
                    <asp:HiddenField ID="hdnExamType" runat="server" Value="0" />
                    <asp:HiddenField ID="hdnIncidentCurrentStatus" runat="server" Value="0" />
                    <asp:HiddenField ID="hdnIncidentTTSDueDate" runat="server" Value="0"/>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:HiddenField ID="hdnMinDate" runat="server" />
            <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnDueDate" runat="server" Value="0" />
            <asp:HiddenField ID="hdnTraineeIds" runat="server" Value="" />
            <asp:HiddenField ID="hdnTTS_Status" runat="server" />
            <div class="col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
                <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
            </div>
            <div id="divMainContainer" runat="server">
                <div class="form-group col-lg-6 float-right padding-none">
                    <div class="col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                        <asp:LinkButton ID="lbTTSList" class="float-right btn btn-dashboard" OnClick="lbTTSList_Click" runat="server">
                    Back To List</asp:LinkButton>
                    </div>
                </div>
                <div class="col-lg-12 col-lg-12 col-sm-12 col-12 col-md-12 col-12 col-md-12 padding-none dms_outer_border float-left">
                    <div class="accordion qms_accordion" id="divHearderCollapse">
                        <div class="card mb-0">

                            <div class="card-header header_col panel-heading_title  accordion-toggle" href="#divHearderBody" data-toggle="collapse">
                                <a class="card-title">Target Training Schedule</a>
                            </div>
                            <div id="divHearderBody" class=" show" data-parent="#divHearderCollapse">
                                <div class="card-body acc_boby float-left grid_panel_hod">

                                    <div class="col-lg-12 col-sm-12 col-12 col-md-12 padding-none top">
                                        <%--<asp:UpdatePanel ID="upDeptAndDocType" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>--%>
                                        <div class="form-group col-lg-3  col-12 col-md-3 col-sm-12 float-left">
                                            <div class="col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                                <label>Select Department<span class="mandatoryStar">*</span></label>
                                                <asp:DropDownList ID="ddlDepartmentTTS" runat="server" CssClass="col-12 padding-none  selectpicker  drop_down"
                                                    data-live-search="true" data-size="7" OnSelectedIndexChanged="ddlDepartmentTTS_SelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-2 col-12 col-md-2 col-sm-12 float-left">
                                            <div class="col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                                <label>Document Type</label>
                                                <asp:DropDownList ID="ddlDocumentType" runat="server" CssClass=" selectpicker drop_down col-12 padding-none"
                                                    OnSelectedIndexChanged="ddlTargetType_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <%-- </ContentTemplate>
                                </asp:UpdatePanel>--%>
                                        <%-- <asp:UpdatePanel ID="upTargetType" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>--%>
                                        <div class="form-group col-lg-3  col-12 col-md-3 col-sm-12 TargetTypeDiv float-left" id="divTargetType">
                                            <div class="col-lg-12 col-sm-12 col-12 col-md-12 padding-none" style="cursor: no-drop">
                                                <label>Target Training Type<span class="mandatoryStar">*</span></label>
                                                <asp:DropDownList ID="ddlTargetType" runat="server" CssClass=" selectpicker drop_down col-12 padding-none" AutoPostBack="true">
                                                    <%-- <asp:ListItem Text="- Select Target Training Type -" Value="0"></asp:ListItem>--%>
                                                    <%-- <asp:ListItem Text="Refresher" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="New Document" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Revision" Value="3"></asp:ListItem>--%>
                                                    <asp:ListItem Text="General" Value="4"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <%--   </ContentTemplate>
                                </asp:UpdatePanel>--%>
                                        <%-- <asp:UpdatePanel ID="upMonth" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>--%>

                                        <%--     </ContentTemplate>
                                </asp:UpdatePanel>--%>
                                        <%--<asp:UpdatePanel ID="upTrainingDoc" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>--%>
                                        <div class="form-group col-lg-7 col-12 col-md-7 col-sm-12 float-left" id="divDocuments">
                                            <label>Select Document<span class="mandatoryStar">*</span></label>
                                            <asp:DropDownList ID="ddlTrainingDocument" runat="server" CssClass="col-lg-12 col-sm-12 col-12 col-md-12  selectpicker drop_down show-tick padding-none" data-live-search="true" data-size="5" OnSelectedIndexChanged="ddlTrainingDocument_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                        <%--  </ContentTemplate>
                                </asp:UpdatePanel>--%>
                                        <%--<asp:UpdatePanel ID="upTypeOfTrainingAndStatus" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>--%>
                                        <div class="form-group col-lg-3  col-12 col-md-3 col-sm-12  float-left" id="divTypeOfTraining">
                                            <label>Type of Training</label>
                                            <asp:DropDownList ID="ddlTypeOfTraining" runat="server" CssClass="col-12 padding-none  selectpicker drop_down ">
                                                <asp:ListItem Text="Internal" Value="I"></asp:ListItem>
                                                <asp:ListItem Text="External" Value="E"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group col-lg-2 col-12 col-md-2 col-sm-12 float-left">
                                            <label id="lblStatus" runat="server">Status</label>
                                            <input type="text" runat="server" class="form-control" disabled="disabled" id="txtStatusTTS" value="Not Approved" />
                                        </div>
                                        <%--     </ContentTemplate>
                                </asp:UpdatePanel>--%>
                                        <%--  <asp:UpdatePanel ID="upActionUsers" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>--%>
                                        <div class="form-group col-lg-3 col-md-3 col-sm-12 col-12 float-left ">
                                            <label>Select Trainer<span class="mandatoryStar">*</span></label>
                                            <asp:ListBox ID="lbxTrainersTTS" runat="server"
                                                CssClass="col-lg-12 col-sm-12 col-12 col-md-12  selectpicker drop_down show-tick  padding-none testLbx"
                                                title="Select Trainer" SelectionMode="Multiple" data-live-search="true" data-size="5"></asp:ListBox>
                                        </div>
                                        <div class="form-group col-lg-3  col-12 col-md-3 col-sm-12 float-left" runat="server" id="divAuthorName">
                                            <div class="col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                                <label>Author</label>
                                                <%-- <asp:DropDownList ID="ddlAuthorTTS" CssClass="selectpicker form-control" data-live-search="true" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlAuthorTTS_SelectedIndexChanged">
                                                </asp:DropDownList>--%>
                                                <input type='text' runat="server" id="txtAuthor" class="form-control" placeholder="Author" readonly="readonly" style="cursor: not-allowed;" />
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-3  col-12 col-md-3 col-sm-12 float-left" runat="server" id="divApproverName">
                                            <div class="col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                                <label>Approver<span class="mandatoryStar">*</span></label>
                                                <asp:DropDownList ID="ddlReviewerTTS" Visible="false" CssClass=" selectpicker drop_down form-control" data-live-search="true" data-size="6" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                                <input type='text' runat="server" id="txtReviewer" class="form-control" placeholder="Reviewer" readonly="readonly" style="cursor: not-allowed;" />
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-3 padding-none float-left" id="divDueDate" runat="server" visible="false">
                                            <label for="lblDueDate" class=" col-lg-12 col-sm-12 col-12 col-md-12 control-label custom_label_answer">Due Date<span class="mandatoryStar">*</span></label>
                                            <div class="col-lg-12 col-sm-12 col-12 col-md-12 ">
                                                <input type='text' autocomplete="off" runat="server" id="txtDueDate" class="form-control" placeholder="DD MMM YYYY" onpaste="return false" />
                                            </div>
                                        </div>
                                        <%--    </ContentTemplate>
                                </asp:UpdatePanel>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-sm-12 col-12 col-md-12 float-left padding-none">
                        <ul class="nav nav-tabs top tab_grid">
                            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#TraineeList">Trainee List</a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Schedule" runat="server" id="Tab_TrainingSession" visible="false">Training Session</a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#History" runat="server" id="Tab_TargetHistory">History</a></li>
                        </ul>

                        <div class="tab-content">
                            <div id="TraineeList" class="tab-pane fade show active">
                                <div class="col-lg-12 col-sm-12 col-12 col-md-12 padding-none grid_panel_hod top float-left" id="gridSubmit">
                                    <div class="col-lg-12 col-sm-12 col-12 col-md-12 ">
                                        <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                    <ContentTemplate>--%>
                                        <div class="form-group col-lg-4 col-12 col-md-4 col-sm-12 padding-none float-left ">
                                            <label>Trainee Department</label>
                                            <asp:DropDownList ID="ddlTraineeDepartment" runat="server" data-live-search="true" Enabled="false" data-size="7" AutoPostBack="true"
                                                CssClass="col-lg-12 col-sm-12 col-12 col-md-12  selectpicker drop_down padding-none" OnSelectedIndexChanged="ddlTraineeDepartment_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-lg-12 col-sm-12 col-12 col-md-12 padding-none float-left" id="divTraineeSelection">
                                            <div class="col-lg-5 col-5 col-md-5 col-sm-5 padding-none panel_selection float-left">
                                                <div class="col-12 panel-heading_title float-left" style="padding: 3px;">
                                                    Select Trainee
                                                     
                                                    <%--<span data-toggle="tooltip" data-placement="bottom" style="color: #15afc2; border: 1px solid #fff; background: #fff; padding: 3px; border-radius: 4px; margin-left: 2px;" class="float-right glyphicon glyphicon-info-sign"
                                                        title="When Target Type is 'General', if the Trainee has already been Assigned with the same Training, the same would not be shown in the below list."></span>--%>
                                                    <input autocomplete="off" name="txtTerm" class="gridSearchBox float-right" type="search" oninput="GridViewSearch(this, '<%=gvTraineeSelector.ClientID %>')" placeholder="Search Select Trainees" />
                                                </div>
                                                <div class="col-lg-12 col-sm-12 col-12 col-md-12 " style="border: 1px solid #07889a; overflow: auto; min-height: 270px">
                                                    <asp:GridView ID="gvTraineeSelector" runat="server" AutoGenerateColumns="False"
                                                        CssClass="table table-fixed tblFixHeight fixGridHead top AspGrid"
                                                        EmptyDataText="No Records" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                                        <RowStyle CssClass="col-12 float-left gvRowStyle padding-none" />
                                                        <Columns>
                                                            <asp:TemplateField Visible="false">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox runat="server" onclick="chkHeaderSelect(this);" ID="chkHeaderElement" CssClass="chkHeaderElement"></asp:CheckBox>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox runat="server" onclick="chkRowSelect(this);" ID="cbTraineeGv" CssClass="chkRowElement"></asp:CheckBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="EmpID" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" Text='<%# Bind("EmpID") %>' ID="lblEmpIDGv"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="DeptID" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" Text='<%# Bind("DeptID") %>' ID="lblEmpDeptIDGv"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Emp. Code">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox runat="server" Text='<%# Bind("EmpCode") %>' ID="TextBox2"></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" Text='<%# Bind("EmpCode") %>' ID="lblEmpCodeGv"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Dept. Code">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox runat="server" Text='<%# Bind("DeptCode") %>' ID="txtDeptCode"></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" Text='<%# Bind("DeptCode") %>' ID="lblDeptCodeGv"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Trainee Name">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox runat="server" Text='<%# Bind("EmpName") %>' ID="TextBox3"></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" Text='<%# Bind("EmpName") %>' ID="lblTraineeGv"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-2 col-md-2 col-sm-2 float-left" style="margin-top: 8em;">
                                                <div class="col-lg-12 col-sm-12 col-12 col-md-12">
                                                    <asp:Button ID="btnAddTrainees" CssClass="col-lg-4 offset-lg-4 btn_selection_box btnAddTrainers" Style="cursor: not-allowed;" runat="server" Text=">" OnClick="btnAddTrainees_Click" />
                                                    <asp:Button ID="btnRemoveTrainees" CssClass="col-lg-4 offset-lg-4 btn_selection_box" runat="server" Style="cursor: not-allowed;" Text="<" OnClick="btnRemoveTrainees_Click" />
                                                </div>
                                            </div>
                                            <div class="col-lg-5 col-5 col-md-5 col-sm-5 panel_selection padding-none float-left">
                                                <div class="col-12 panel-heading_title float-left" style="padding: 3px;">
                                                    Selected Trainee
                                                    <input autocomplete="off" name="txtTerm" type="search" class="gridSearchBox float-right" oninput="GridViewSearch(this, '<%=gvTraineeSelected.ClientID %>')" placeholder="Search Selected Trainees">
                                                </div>
                                                <div class="col-lg-12 col-sm-12 col-12 col-md-12 float-left" style="border: 1px solid #07889a; overflow: auto; min-height: 270px">
                                                    <asp:GridView ID="gvTraineeSelected" runat="server" AutoGenerateColumns="False"
                                                        CssClass="table table-fixed tblFixHeight fixHead top AspGrid"
                                                        EmptyDataText="No Records" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                                        <RowStyle CssClass="col-12 col-lg-12 col-md-12 col-sm-12 gvRowStyle padding-none" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Select" Visible="false">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox runat="server" onclick="chkHeaderSelect(this);" ID="chkHeaderElement" CssClass="chkHeaderElement"></asp:CheckBox>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox runat="server" onclick="chkRowSelect(this);" ID="cbTraineeGv" CssClass="chkRowElement"></asp:CheckBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="EmpID" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" Text='<%# Bind("EmpID") %>' ID="lblEmpIDGv"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="DeptID" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" Text='<%# Bind("DeptID") %>' ID="lblEmpDeptIDGv"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Emp. Code">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox runat="server" Text='<%# Bind("EmpCode") %>' ID="TextBox2"></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" Text='<%# Bind("EmpCode") %>' ID="lblEmpCodeGv"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Dept. Code">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox runat="server" Text='<%# Bind("DeptCode") %>' ID="txtDeptCode"></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" Text='<%# Bind("DeptCode") %>' ID="lblDeptCodeGv"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Trainee Name">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox runat="server" Text='<%# Bind("EmpName") %>' ID="TextBox3"></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" Text='<%# Bind("EmpName") %>' ID="lblTraineeGv"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <%--        </ContentTemplate>
                                </asp:UpdatePanel>--%>
                                    </div>
                                    <div class="col-lg-12 col-sm-12 col-12 col-md-12 float-left">
                                        <div class="form-group col-lg-12 col-sm-12 col-12 col-md-12 padding-none" runat="server" visible="false" id="divAuthorComments">
                                            <label for="inputPassword3" class=" col-sm-12 control-label col-form-label  padding-none custom_label_answer">Comments</label>
                                            <div class="col-sm-12 col-12 col-md-12 col-lg-12 padding-none">
                                                <textarea class="form-control" maxlength="250" runat="server" rows="5" id="txtAuthorComments"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div id="divTTS_ActionBtns">
                                <div id="divHodActionBtns" runat="server" class="form-group float-right col-lg-12 col-sm-12 col-12 col-md-12 padding-none" visible="false">
                                    <asp:Button ID="btnHodCancel" runat="server" Visible="false" Text="Cancel" CssClass="float-right btn btn-cancel_popup" OnClientClick="CloseModel();" />
                                    <asp:Label ID="lblErrMsg" runat="server" Visible="false" CssClass="col-11 float-left text-center label_point" Text="You are Not authorized Training Coordinator to Prepare Training Schedule."></asp:Label>
                                    <div id="divHodReset" runat="server">
                                        <a href="#" id="btnReset" runat="server" class="float-right btn btn-revert_popup" onclick="TTS_Created_Success();">Reset</a>
                                    </div>
                                    <asp:Button ID="btnHodSubmit" runat="server" Text="Create" OnClick="btnHodSubmit_Click" OnClientClick="return TargetTrainingValidations();" CssClass="btn btn-signup_popup float-right" />
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>






            <!---Error popup--->
            <div id="ModalError" class="modal ModalDanger error_popup" role="dialog" style="">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content modalMainContent">
                        <div class="modal-header ModalHeaderPart">
                            <div class="error_icon float-left">
                                <%--                                <img src="<%=ResolveUrl("~/Images/CustomAlerts/error.png")%>" class="float-left" style="width: 35px" alt="Error" />--%>
                                <h4 class="modal-title float-left popup_title">Error</h4>
                            </div>

                        </div>
                        <div class="modal-body bottom" style="padding: 12px 0px">
                            <div class="col-lg-12 padding-none" style="padding: 10px; border-bottom: 1px solid #f95a5a;">

                                <p id="msgError" class="col-lg-12" style="padding: 10px 22px; max-height: 300px; overflow-x: auto; font-family: arial; color: #f95a5a; font-weight: bold;" runat="server"></p>
                            </div>
                        </div>
                        <div class="modal-footer ">
                            <button type="button" id="btnErrorOk" class="btn btn-cancel_popup" style="margin-top: 10px !important" data-dismiss="modal">Ok</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Error Popup-->

            <!-- Success content-->
            <div id="ModalSuccess" class="modal ModalSucess success_popup" role="dialog" style="z-index: 9999">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content modalMainContent ">
                        <div class="modal-header ModalHeaderPart">
                            <div class="success_icon float-left">

                                <h4 class="modal-title popup_title float-left">Success</h4>
                            </div>

                        </div>
                        <div class="modal-body" style="padding: 11px 0px;">
                            <div class="col-lg-12 padding-none" style="padding: 10px; border-bottom: 1px solid #3ab16a;">

                                <p id="msgSuccess" class="col-lg-12" style="padding: 0px 22px; max-height: 128px; overflow-x: auto;" runat="server"></p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnSuccessOk" class="btn-success_popup" data-dismiss="modal" style="margin-top: 10px !important" runat="server">Ok</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Success content-->

            <!-- Information content-->
            <div id="ModalInfo" class="modal info_popup ModalConfirm" role="dialog" style="z-index: 9999">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content modalMainContent">
                        <div class="modal-header ModalHeaderPart">
                            <div class="success_icon float-left">

                                <h4 class="modal-title popup_title float-left">Information</h4>
                            </div>

                        </div>
                        <div class="modal-body">
                            <div class="col-lg-12">


                                <p id="msgInfo" class="col-lg-12" style="padding: 0px 22px; max-height: 128px; overflow-x: auto;" runat="server"></p>

                            </div>
                        </div>
                        <div class="modal-footer ModalFooterPart">
                            <button type="button" id="btnInfoOk" class="btn-info_popup" data-dismiss="modal" style="margin-top: 10px !important" runat="server">Ok</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Information content-->

            <!-- Warning content-->
            <div id="ModalWarning" class="modal warning_popup" role="dialog" style="z-index: 9999">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="success_icon float-left">
                                <img src="<%=ResolveUrl("~/Images/CustomAlerts/warning.png")%>" class="float-left" style="width: 35px" alt="Warning" />
                                <h4 class="modal-title popup_title float-left">Warning</h4>
                            </div>

                        </div>
                        <div class="modal-body" style="padding: 0px;">
                            <div class="col-lg-12" style="padding: 0px; border-bottom: 1px solid #f65d21;">

                                <p id="msgWarning" class="col-lg-12" runat="server" style="padding: 10px 22px; max-height: 128px; overflow-x: auto; font-family: arial; color: #f65d21; font-weight: bold;"></p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnWarningOk" class="btnWarningOk" data-dismiss="modal" style="margin-top: 10px !important">Ok</button>
                        </div>
                    </div>

                </div>
            </div>
            <!-- End Warning content-->


            <uc1:ElectronicSign runat="server" ID="ElectronicSign" />
            <uc1:ucExamResultSheet runat="server" ID="ucExamResultSheet" />
            <uc1:ucFeedback runat="server" ID="ucFeedback" />




            <!--To Show Custom Alert Messages-->
            <script>
                function custAlertMsg(message, alertType, okEvent) {
                    var okEvent = okEvent || false;

                    if (!okEvent) {
                        okEvent = '';
                    }
                    if (alertType == 'error') {
                        document.getElementById("msgError").innerHTML = message;
                        //$("#ModalError").modal();
                        $('#ModalError').modal({ backdrop: 'static', keyboard: false });
                        $('#btnErrorOk').attr('onclick', okEvent);
                        $('#btnErrorOk').focus();
                        if (okEvent != '') {
                            // document.getElementById('btnErrorOk').setAttribute('onclick', okEvent)

                        }
                    }
                    else if (alertType == 'success') {
                        document.getElementById("msgSuccess").innerHTML = message;
                        //$("#ModalSuccess").modal();
                        $('#ModalSuccess').modal({ backdrop: 'static', keyboard: false });
                        $('#btnSuccessOk').attr('onclick', okEvent);
                        $('#btnSuccessOk').focus();
                        if (okEvent != '') {
                            // document.getElementById('btnSuccessOk').addEventListener("click", okfunc);
                            //document.getElementById('btnSuccessOk').setAttribute('onclick', okEvent)
                        }
                    }
                    else if (alertType == 'warning') {
                        document.getElementById("msgWarning").innerHTML = message;
                        //  $("#ModalWarning").modal();
                        $('#ModalWarning').modal({ backdrop: 'static', keyboard: false });
                        $('#btnWarningOk').attr('onclick', okEvent);
                        $('#btnWarningOk').focus();
                    }
                    else if (alertType == 'info') {
                        document.getElementById("msgInfo").innerHTML = message;
                        //  $("#ModalInfo").modal();
                        $('#ModalInfo').modal({ backdrop: 'static', keyboard: false });
                        $('#btnInfoOk').attr('onclick', okEvent);
                        $('#btnInfoOk').focus();
                        if (okEvent != '') {
                            // document.getElementById('btnInfoOk').setAttribute('onclick', okEvent)

                        }
                    }
                    else if (alertType == 'confirm') {
                        document.getElementById("msgConfirm").innerHTML = message;
                        $('#ModalConfirm').modal({ backdrop: 'static', keyboard: false });
                        $('#btnCnfYes').focus();
                    }
                }
            </script>


            <script>
                function closemodalSchedule() {
                    $('#modelScheduleNew').modal('hide');
                }

                function lblWarningText() {
                    $('#lblWarningMsg').text("Questionnaire Doesn't Exit ");
                    $('#divWarningMsg').show();
                    $('#divCheckbox').show();
                }

            </script>

            <!--script of Due date-->
            <script>
                $(function () {
                    loadDatePickers();
                });
                $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
                })

                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_endRequest(function () {
                    loadDatePickers();
                });

                function loadDatePickers()
                {
                    //var tts_Status = $('#<%=hdnTTS_CurrentStatus.ClientID%>').val();
                    //if (tts_Status == "0" || tts_Status == "2" || tts_Status == "1" || tts_Status == "3") {
                    //var Inc_CStatus = 

                    //commented on 13-02-2020
                    var DueDate = '<%=hdnIncidentTTSDueDate.Value%>';
                    //end commented on 13-02-2020

                    var ServerDateNow = '<%=hdnMinDate.Value%>';
                    if (('<%=hdnIncidentCurrentStatus.Value%>')!=21)
                    {
                        $('#txtDueDate').val(DueDate);
                    }
                    //$('#txtDueDate').datetimepicker({
                    //    useCurrent: false,
                    //    format: 'DD MMM YYYY',
                    //    minDate: new Date(new Date(ServerDateNow).getFullYear(), new Date(ServerDateNow).getMonth(), new Date(ServerDateNow).getDate(), 00, 00, 00),
                    //});

          
                    $('#<%=txtDueDate.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        minDate: new Date(ServerDateNow),
                       // useCurrent: true,
                        });
                }
                //}

               <%-- $(function () {
                    loadDatePickers();
                });

                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_endRequest(function () {
                    loadDatePickers();
                });

                function loadDatePickers() {
                    var tts_Status = $('#<%=hdnTTS_CurrentStatus.ClientID%>').val();
                    if (tts_Status == "0" || tts_Status == "2") {
                        $('#<%=txtDueDate.ClientID%>').datetimepicker({
                            format: 'DD MMM YYYY',
                            minDate: new Date('<%=hdnMinDate.Value%>'),
                    useCurrent: true,
                        });
                    }
                }--%>

</script>

            <!--Validating TTS-->
            <script>
                function TargetTrainingValidations() {
                    var Department = document.getElementById("<%=ddlDepartmentTTS.ClientID%>");
                    var DocumentType = document.getElementById("<%=ddlDocumentType.ClientID%>");
                    var Document = document.getElementById("<%=ddlTrainingDocument.ClientID%>");
                    var Training = document.getElementById("<%=ddlTypeOfTraining.ClientID%>");
                    var Trainer = document.getElementById("<%=lbxTrainersTTS.ClientID%>");
           <%-- var Reviewer = document.getElementById("<%=ddlReviewerTTS.ClientID%>");--%>
          <%--  var Month = document.getElementById("<%=ddlRefresherMonth.ClientID%>");--%>
                    var TargetType = document.getElementById("<%=ddlTargetType.ClientID%>");
                    var DueDate = document.getElementById("<%=txtDueDate.ClientID%>");
                errors = [];
                if (Department.value == 0) {
                    errors.push("Select Department.");
                }
                //if (DocumentType.value == 0) {
                //    errors.push("Select DocumentType");
                //}
                if (TargetType.value == 0) {
                    errors.push("Select Target Type.");
                }
                if (TargetType.value == 1) {
                    if (Month.value == 0) {
                        errors.push("Select Month.");
                    }
                }
                if (Document.value == 0) {
                    errors.push("Select Document.");
                }
                if (Training.value == 0) {
                    errors.push("Select Training.");
                }
                if (Trainer.value == 0) {
                    errors.push("Select Trainer.");
                }
                //if (Reviewer.value == 0) {
                //    errors.push("Select Approver");
                //}
                if (DueDate.value == "") {
                    errors.push("Select Due Date.");
                }
                if (errors.length > 0) {
                    custAlertMsg(errors.join("<br/>"), "error");
                    return false;
                }
                else {
                    return true;
                }
                }
            </script>




            <!--for gridview check box selection when we click on row-->
            <script>
                $(document).ready(function () {
                    $('.gvRowStyle').click(function (event) {
                        if (event.target.type !== 'checkbox') {
                            $(':checkbox', this).trigger('click');
                        }
                    });
                });
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_endRequest(function () {
                    $('.gvRowStyle').click(function (event) {
                        if (event.target.type !== 'checkbox') {
                            $(':checkbox', this).trigger('click');
                        }
                    });
                });
            </script>


            <!--Redirecting the Page-->
            <script>
                function TTS_Created_Success() {
          <%--  window.open("<%=ResolveUrl("~/QMSIncient/TTSTraining.aspx?TTS_Type=Create")%>", "_self");--%>
                    window.location.href = window.location.href;
                }
                function CloseModel() {
                    window.parent.closeModal();
                }
                function gotoApprovalList() {
                    window.open("<%=ResolveUrl("~/TMS/TargetTrainingSchedule/TTS_ApprovalList.aspx?FilterType=2")%>", "_self");
                }

            </script>

            <!--For Trainee ASP Grid Search-->
            <script>      
                function GridViewSearch(phrase, _id) {
                    var words = phrase.value.toLowerCase().split(" ");
                    var table = document.getElementById(_id);
                    var ele;
                    for (var r = 1; r < table.rows.length; r++) {
                        ele = table.rows[r].innerHTML.replace(/<[^>]+>/g, "");
                        var displayStyle = 'none';
                        for (var i = 0; i < words.length; i++) {
                            if (ele.toLowerCase().indexOf(words[i]) >= 0)
                                displayStyle = '';
                            else {
                                displayStyle = 'none';
                                break;
                            }
                        }
                        table.rows[r].style.display = displayStyle;
                    }
                }
            </script>



            <!--Fix up GridView to support THEAD tags -->
            <script type="text/javascript">        
                $(function () {
                    fixGvHeader();
                    addGridThead();
                });

                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_endRequest(function () {
                    fixGvHeader();
                    addGridThead();
                });

                function fixGvHeader() {
                    var tbl = document.getElementsByClassName("fixGridHead");
                    // Fix up GridView to support THEAD tags 
                    if ($(".fixGridHead").find("thead").length == 0) {
                        $(".fixGridHead tbody").before("<thead><tr></tr></thead>");
                        $(".fixGridHead thead tr").append($(".fixGridHead th"));
                        var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                        if (rows.length == 0) {
                            $(".fixGridHead tbody tr:first").remove();
                        }
                    }
                }
                function addGridThead() {
                    var tbl = document.getElementsByClassName("fixHead");
                    // Fix up GridView to support THEAD tags
                    if ($(".fixHead").find("thead").length == 0) {
                        $(".fixHead tbody").before("<thead><tr></tr></thead>");
                        $(".fixHead thead tr").append($(".fixHead th"));
                        var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                        if (rows.length == 0) {
                            $(".fixHead tbody tr:first").remove();
                        }
                    }
                }
            </script>

            <script>
                $(document).ready(function () {
                    $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
                        var anchor = $(e.target).attr('href');
                        if (anchor == "#History") {
                            loadTTS_ActionHistory();
                        }
                    });
                });
                function CloseBrowser() {

                    window.close();

                }
            </script>
        </div>
    </form>
</body>
</html>
