﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Optimization;
using System.Web.SessionState;
namespace AizantIT_PharmaApp
{
    public class Global : HttpApplication
    {
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        void Application_Start(object sender, EventArgs e)
        {
            DevExpress.XtraReports.Web.WebDocumentViewer.Native.WebDocumentViewerBootstrapper.SessionState = System.Web.SessionState.SessionStateBehavior.Disabled;
            // Initialize log4net.
            log4net.Config.XmlConfigurator.Configure();
            DevExtremeBundleConfig.RegisterBundles(BundleTable.Bundles);
            // Code that runs on application startup
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            DevExpress.XtraReports.Web.ASPxWebDocumentViewer.StaticInitialize();
        }
        protected void Application_Error(object sender, EventArgs e)
        {
            // Exception exception = Server.GetLastError();
            // Aizant_log.Error("APP000:" + exception.Message);
            // Server.Transfer("GenericErrorPage.html", true);
            //var serverError = Server.GetLastError() as HttpException;

            //if (serverError != null)
            //{
            //    if (serverError.GetHttpCode() == 404)
            //    {
            //        Server.ClearError();
            //        Server.Transfer("/404error.html");
            //    }
            //}
        }
        protected void Application_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            if (Context.Handler is IRequiresSessionState || Context.Handler is IReadOnlySessionState)
            {
                //HttpContext context = HttpContext.Current;
                // Your Methods
            }
        }
    }
}