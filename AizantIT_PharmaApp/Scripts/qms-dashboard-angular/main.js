(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "+E+c":
/*!**************************************!*\
  !*** ./src/app/models/dashboards.ts ***!
  \**************************************/
/*! exports provided: QualityEventIDs, QualityEvents, QualityEventStatus, ChangeCategoriesListValues, ChangeClassificationListValues, CCNClassificationStatusCodes, CCNCategoryStatusCodes, IncidentClassificationListValues, IncidentClassificationStatusCodes, QualityEventIds, MetricsListValues, ShowTypeList, TypeOfActionNames, TypeOfActionStatusCodes, TrimmedDepartmentNames */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QualityEventIDs", function() { return QualityEventIDs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QualityEvents", function() { return QualityEvents; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QualityEventStatus", function() { return QualityEventStatus; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangeCategoriesListValues", function() { return ChangeCategoriesListValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangeClassificationListValues", function() { return ChangeClassificationListValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CCNClassificationStatusCodes", function() { return CCNClassificationStatusCodes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CCNCategoryStatusCodes", function() { return CCNCategoryStatusCodes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IncidentClassificationListValues", function() { return IncidentClassificationListValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IncidentClassificationStatusCodes", function() { return IncidentClassificationStatusCodes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QualityEventIds", function() { return QualityEventIds; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MetricsListValues", function() { return MetricsListValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShowTypeList", function() { return ShowTypeList; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TypeOfActionNames", function() { return TypeOfActionNames; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TypeOfActionStatusCodes", function() { return TypeOfActionStatusCodes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrimmedDepartmentNames", function() { return TrimmedDepartmentNames; });
const QualityEventIDs = {
    IncidentReport: 1,
    ChangeControlNote: 2,
    Errata: 3,
    NoteToFile: 4
};
const QualityEvents = {
    ChangeControlNote: 'Change Control Note',
    IncidentReport: 'Incident Report',
    Errata: 'Errata',
    NoteToFile: 'Note To File'
};
const QualityEventStatus = {
    Approved: 'Approved',
    Closed: 'Closed',
    OverDue: 'OverDue',
    Pending: 'Pending',
    Rejected: 'Rejected',
    OverdueLessThanOneWk: "< 1wk",
    OverdueOneWkToOneMn: "1wk-1mn",
    OverdueOneMnToThreeMn: "1mn-3mn",
    OverdueThreeMnToSixMn: "3mn-6mn",
    OverdueGreaterThanSixMn: ">6mn"
};
const ChangeCategoriesListValues = {
    permanent: "Permanent",
    temporary: "Temporary"
};
const ChangeClassificationListValues = {
    major: "Major",
    minor: "Minor",
    unclassified: "Unclassified"
};
const CCNClassificationStatusCodes = {
    Major: 1,
    Minor: 2,
    Unclassified: 7,
    All: 0
};
const CCNCategoryStatusCodes = {
    Permanent: 3,
    Temporary: 4,
    Both: 0
};
const IncidentClassificationListValues = {
    qualityImpacting: "Quality Impacting",
    qualityNonImpacting: "Quality Non-Impacting",
    unclassified: "Unclassified"
};
const IncidentClassificationStatusCodes = {
    QualityImpacting: 5,
    QualityNonImpacting: 6,
    Unclassified: 8,
    All: 0
};
const QualityEventIds = {
    IncidentReport: 1,
    CCN: 2,
    Errata: 3,
    NoteToFile: 4
};
const MetricsListValues = {
    CountMetricID: 0,
    countMetricName: "Count",
    TATMetricID: 1,
    tatMetricName: "Turn Around Time"
};
const ShowTypeList = {
    OverallList: 1,
    Approved: 2,
    Pending: 3,
    Closed: 4,
    Rejected: 5,
    OverallOverdueList: 6,
    OverdueLessThanOneWk: 7,
    OverdueOneWkToOneMn: 8,
    OverdueOneMnToThreeMn: 9,
    OverdueThreeMnToSixMn: 10,
    OverdueGreaterThanSixMn: 11
};
const TypeOfActionNames = {
    preventive: 'Preventive',
    corrective: 'Corrective',
    correctiveAndPreventive: 'Corrective & Preventive'
};
const TypeOfActionStatusCodes = {
    Preventive: 1,
    Corrective: 2,
    CorrectiveAndPreventive: 3
};
const TrimmedDepartmentNames = {
    "Analytical Development": 'AD',
    "Business Development": 'BD',
    "Clinical Pharmacology": 'CP',
    "Engineering": 'Engg',
    "Executive Managements": 'EM',
    "Human Resources": 'HR',
    "Production": 'Prod.',
    "Quality Assurance": 'QA',
    "Quality Control": 'QC',
    "Regulatory Affairs": 'Reg. Aff',
    "Warehouse": 'WH'
};


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\sk058\Documents\Tasks\AGA_API_Main_Repo_17_may_2021\AizantIT_PharmaApp\qms-dashboard-angular\src\main.ts */"zUnb");


/***/ }),

/***/ "0SbY":
/*!***************************************************************************************************!*\
  !*** ./src/app/dashboards/department-performance-chart/department-performance-chart.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: DepartmentPerformanceChartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DepartmentPerformanceChartComponent", function() { return DepartmentPerformanceChartComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _models_dashboards__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/dashboards */ "+E+c");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "wd/R");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var flatpickr_dist_plugins_monthSelect__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! flatpickr/dist/plugins/monthSelect */ "xyPl");
/* harmony import */ var flatpickr_dist_plugins_monthSelect__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(flatpickr_dist_plugins_monthSelect__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _list_view_dept_performance_list_view_dept_performance_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../list-view-dept-performance/list-view-dept-performance.component */ "MiXW");
/* harmony import */ var _services_dashboards_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/dashboards.service */ "PGxC");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "s7LF");
/* harmony import */ var _services_server_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/server.service */ "H9vK");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-bootstrap/modal */ "LqlI");
/* harmony import */ var ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng-multiselect-dropdown */ "UPO+");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common */ "SVse");
/* harmony import */ var ng2_flatpickr__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ng2-flatpickr */ "A/TE");
/* harmony import */ var devextreme_angular__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! devextreme-angular */ "hYZE");
/* harmony import */ var devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! devextreme-angular/ui/nested */ "Q08N");















const _c0 = function () { return { standalone: true }; };
function DepartmentPerformanceChartComponent_div_14_Template(rf, ctx) { if (rf & 1) {
    const _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ng-multiselect-dropdown", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function DepartmentPerformanceChartComponent_div_14_Template_ng_multiselect_dropdown_ngModelChange_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r9); const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r8.selectedChangeCategory = $event; })("onSelect", function DepartmentPerformanceChartComponent_div_14_Template_ng_multiselect_dropdown_onSelect_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r9); const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r10.onDeptPerformanceDataChartFilterChange($event); })("onDeSelect", function DepartmentPerformanceChartComponent_div_14_Template_ng_multiselect_dropdown_onDeSelect_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r9); const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r11.onDeptPerformanceDataChartFilterChange($event); })("onSelectAll", function DepartmentPerformanceChartComponent_div_14_Template_ng_multiselect_dropdown_onSelectAll_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r9); const ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r12.onSelectAllChangeCategories($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", "Change Category")("settings", ctx_r0.changeCategoryDropdownSettings)("data", ctx_r0.changeCategoriesList)("ngModel", ctx_r0.selectedChangeCategory)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](5, _c0));
} }
function DepartmentPerformanceChartComponent_div_15_Template(rf, ctx) { if (rf & 1) {
    const _r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ng-multiselect-dropdown", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function DepartmentPerformanceChartComponent_div_15_Template_ng_multiselect_dropdown_ngModelChange_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r14); const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r13.selectedChangeClassification = $event; })("onSelect", function DepartmentPerformanceChartComponent_div_15_Template_ng_multiselect_dropdown_onSelect_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r14); const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r15.onDeptPerformanceDataChartFilterChange($event); })("onDeSelect", function DepartmentPerformanceChartComponent_div_15_Template_ng_multiselect_dropdown_onDeSelect_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r14); const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r16.onDeptPerformanceDataChartFilterChange($event); })("onSelectAll", function DepartmentPerformanceChartComponent_div_15_Template_ng_multiselect_dropdown_onSelectAll_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r14); const ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r17.onSelectAllChangeClassifications($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", "Change Classification")("settings", ctx_r1.changeClassificationDropdownSettings)("data", ctx_r1.changeClassificationList)("ngModel", ctx_r1.selectedChangeClassification)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](5, _c0));
} }
function DepartmentPerformanceChartComponent_div_16_Template(rf, ctx) { if (rf & 1) {
    const _r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ng-multiselect-dropdown", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function DepartmentPerformanceChartComponent_div_16_Template_ng_multiselect_dropdown_ngModelChange_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r19); const ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r18.selectedIncidentCategory = $event; })("onSelect", function DepartmentPerformanceChartComponent_div_16_Template_ng_multiselect_dropdown_onSelect_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r19); const ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r20.onDeptPerformanceDataChartFilterChange($event); })("onDeSelect", function DepartmentPerformanceChartComponent_div_16_Template_ng_multiselect_dropdown_onDeSelect_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r19); const ctx_r21 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r21.onDeptPerformanceDataChartFilterChange($event); })("onSelectAll", function DepartmentPerformanceChartComponent_div_16_Template_ng_multiselect_dropdown_onSelectAll_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r19); const ctx_r22 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r22.onSelectAllIncidentCategories($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("settings", ctx_r2.incidentCategoryDropdownSettings)("data", ctx_r2.incidentCategoriesList)("ngModel", ctx_r2.selectedIncidentCategory)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](4, _c0));
} }
function DepartmentPerformanceChartComponent_div_17_Template(rf, ctx) { if (rf & 1) {
    const _r24 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ng-multiselect-dropdown", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function DepartmentPerformanceChartComponent_div_17_Template_ng_multiselect_dropdown_ngModelChange_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r24); const ctx_r23 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r23.selectedIncidentClassification = $event; })("onSelect", function DepartmentPerformanceChartComponent_div_17_Template_ng_multiselect_dropdown_onSelect_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r24); const ctx_r25 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r25.onDeptPerformanceDataChartFilterChange($event); })("onDeSelect", function DepartmentPerformanceChartComponent_div_17_Template_ng_multiselect_dropdown_onDeSelect_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r24); const ctx_r26 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r26.onDeptPerformanceDataChartFilterChange($event); })("onSelectAll", function DepartmentPerformanceChartComponent_div_17_Template_ng_multiselect_dropdown_onSelectAll_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r24); const ctx_r27 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r27.onSelectAllIncidentClassification($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("settings", ctx_r3.incidentClassificationDropdownSettings)("data", ctx_r3.incidentClassificationList)("ngModel", ctx_r3.selectedIncidentClassification)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](4, _c0));
} }
function DepartmentPerformanceChartComponent_div_26_Template(rf, ctx) { if (rf & 1) {
    const _r29 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "button", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DepartmentPerformanceChartComponent_div_26_Template_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r29); const ctx_r28 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r28.onShowListView(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "List View");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function DepartmentPerformanceChartComponent_dxi_strip_33_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "dxi-strip", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "dxo-label", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "dxo-font", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("endValue", ctx_r5.lowerLimit);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("color", ctx_r5.lowerLimitColor);
} }
function DepartmentPerformanceChartComponent_dxi_strip_34_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "dxi-strip", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "dxo-label", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "dxo-font", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("startValue", ctx_r6.upperLimit);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("color", ctx_r6.upperLimitColor);
} }
const _c1 = function () { return { color: "#ff7c7c" }; };
function DepartmentPerformanceChartComponent_dxi_constant_line_40_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "dxi-constant-line", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "dxo-label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("width", 2)("value", ctx_r7.currentOrgAvg);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("font", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](3, _c1));
} }
const _c2 = function () { return { color: "#8c8cff" }; };
class DepartmentPerformanceChartComponent {
    constructor(dashboardService, fb, serverService, modalService) {
        this.dashboardService = dashboardService;
        this.fb = fb;
        this.serverService = serverService;
        this.modalService = modalService;
        this.departmentsList = [];
        this.selectedDepartments = [];
        this.departmentDropdownSettings = {};
        this.eventsDropdownSettings = {};
        this.qualityEventsList = [];
        this.selectedQualityEvents = [];
        this.changeCategoriesList = [];
        this.selectedChangeCategory = [];
        this.changeCategoryDropdownSettings = {};
        this.changeClassificationList = [];
        this.selectedChangeClassification = [];
        this.changeClassificationDropdownSettings = {};
        this.incidentCategoriesList = [];
        this.selectedIncidentCategory = [];
        this.incidentCategoryDropdownSettings = {};
        this.incidentClassificationList = [];
        this.selectedIncidentClassification = [];
        this.incidentClassificationDropdownSettings = {};
        this.metricSelectionDropdownSettings = {};
        this.metricSelectionList = [];
        this.selectedMetric = [];
        this.isLimitsShown = false;
        this.isAveragesShown = false;
        this.fromDateOptions = {
            // defaultDate: '2021-05-15',
            // minDate:'2021-05-15',
            dateFormat: "d-M-Y",
            plugins: [
                flatpickr_dist_plugins_monthSelect__WEBPACK_IMPORTED_MODULE_3___default()({
                    shorthand: true,
                    dateFormat: "M Y",
                    altFormat: "M Y",
                    theme: "light" // defaults to "light"
                })
            ]
        };
        this.toDateOptions = {
            // defaultDate: '2021-05-15',
            // minDate:'2021-05-15',
            dateFormat: "d-M-Y",
            plugins: [
                flatpickr_dist_plugins_monthSelect__WEBPACK_IMPORTED_MODULE_3___default()({
                    shorthand: true,
                    dateFormat: "M Y",
                    altFormat: "M Y",
                    theme: "light" // defaults to "light"
                })
            ]
        };
        this.currentQualityEventId = _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventIds"];
        this.selectedDeptIds = [];
        this.initialVisitCount = 0;
        this.currentDeptAvg = 0;
        this.currentOrgAvg = 0;
        this.upperLimit = 0;
        this.lowerLimit = 0;
        this.upperLimitColor = "#ff9b52";
        this.lowerLimitColor = "#6199e6";
        this.currentMetricId = _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["MetricsListValues"].CountMetricID;
        this.CCNRecords = [];
        this.incidentRecords = [];
        this.errrataRecords = [];
        this.noteToFileRecords = [];
        this.customizePoint = (arg) => {
            if (arg.value > this.upperLimit) {
                return { color: this.upperLimitColor };
            }
            else if (arg.value < this.lowerLimit) {
                return { color: this.lowerLimitColor };
            }
        };
        this.customizeLabel = (arg) => {
            if (arg.value > this.upperLimit) {
                return this.getLabelsSettings(this.upperLimitColor);
            }
            else if (arg.value < this.lowerLimit) {
                return this.getLabelsSettings(this.lowerLimitColor);
            }
        };
        //returns department ID from Dept name
        this.getDeptIdFromDeptName = (deptName) => {
            for (let i = 0; i < this.departmentsList.length; i++) {
                if (this.departmentsList[i].DepartmentName === deptName) {
                    return this.departmentsList[i].Department_ID;
                }
            }
        };
        //returns qualityEvent ID from qualityEvent name
        this.getQualityEventIdFromQualityEventName = (qualityEventName) => {
            for (let i = 0; i < this.qualityEventsList.length; i++) {
                if (this.qualityEventsList[i].QualityEvent_TypeName === qualityEventName) {
                    return this.qualityEventsList[i].QualityEvent_TypeID;
                }
            }
        };
        //converts fetched date into DB compatible date format
        this.getRequiredDateFormat = (selectedDate) => {
            var tempDate = new Date(selectedDate);
            let sampleDate = moment__WEBPACK_IMPORTED_MODULE_2__(tempDate.toISOString());
            return sampleDate;
        };
        this.isListViewShown = false;
        this.setChangeCategoryAndClassificationDropdown();
        this.setIncidentCategoryAndClassificationDropdown();
        this.filtersForm = this.fb.group({
            fromDate: [''],
            toDate: ['']
        });
        // this loads dept,quality events list sequentially and fetches data from db
        this.loadInitialDeptPerformanceData();
        this.setMetricDropdown();
    }
    ngOnInit() {
        this.departmentDropdownSettings = {
            singleSelection: true,
            idField: 'Department_ID',
            textField: 'DepartmentName',
            allowSearchFilter: true,
            closeDropDownOnSelection: true
        };
        this.eventsDropdownSettings = {
            singleSelection: true,
            idField: 'QualityEvent_TypeID',
            textField: 'QualityEvent_TypeName',
            closeDropDownOnSelection: true,
            allowSearchFilter: true
        };
        this.CCNRecords = [{
                Month: 'June',
                CCN_No: "1",
                ChangeClassification: "Major",
                CreatedBy: "Sandeep",
                CreatedDate: "12 Jun 2020",
                DepartmentName: "Quality Assurance",
                DueDate: "15 Jan 2021",
                CompletedDate: "20 May 2021",
                TAT: 24,
            },
            {
                Month: 'June',
                CCN_No: "2",
                ChangeClassification: "Minor",
                CreatedBy: "Mandeep",
                CreatedDate: "12 Jan 2020",
                DepartmentName: "Quality Control",
                DueDate: "15 Jun 2021",
                CompletedDate: "20 Apr 2021",
                TAT: 20,
            },
            {
                Month: 'June',
                CCN_No: "5",
                ChangeClassification: "Major",
                CreatedBy: "Mandeep",
                CreatedDate: "01 Jan 2020",
                DepartmentName: "Quality Control",
                DueDate: "15 Jun 2021",
                CompletedDate: "20 Apr 2021",
                TAT: 20,
            },
            {
                Month: 'July',
                CCN_No: "3",
                ChangeClassification: "Major",
                CreatedBy: "Jaideep",
                CreatedDate: "15 Jun 2020",
                DepartmentName: "Quality Assurance",
                DueDate: "15 Apr 2021",
                CompletedDate: "20 Dec 2021",
                TAT: 14,
            },
            {
                Month: 'July',
                CCN_No: "4",
                ChangeClassification: "Minor",
                CreatedBy: "Singh",
                CreatedDate: "10 Jun 2021",
                DepartmentName: "Quality Assurance",
                DueDate: "15 Jan 2022",
                CompletedDate: "20 May 2021",
                TAT: 4,
            }];
    }
    ngAfterViewInit() {
        let flatpickrToDateElem = document.getElementById('toDate');
        let inptElem = flatpickrToDateElem.getElementsByTagName('input')[0];
        inptElem.setAttribute("disabled", "true");
    }
    setMetricDropdown() {
        this.metricSelectionDropdownSettings = {
            singleSelection: true,
            idField: 'metricId',
            textField: 'metricName',
            closeDropDownOnSelection: true,
        };
        this.metricSelectionList = [
            { metricId: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["MetricsListValues"].CountMetricID, metricName: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["MetricsListValues"].countMetricName },
            { metricId: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["MetricsListValues"].TATMetricID, metricName: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["MetricsListValues"].tatMetricName }
        ];
        this.selectedMetric = [{
                metricId: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["MetricsListValues"].CountMetricID, metricName: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["MetricsListValues"].countMetricName
            }];
    }
    onMetricSelectionChange(event) {
        this.currentMetricId = event.metricId;
        this.getDeptPerformanceDataFromFilters();
    }
    getLabelsSettings(backgroundColor) {
        return {
            visible: true,
            backgroundColor: backgroundColor,
            customizeText: this.customizeText
        };
    }
    customizeText(arg) {
        return arg.valueText;
    }
    setChangeCategoryAndClassificationDropdown() {
        this.changeCategoryDropdownSettings = {
            singleSelection: false,
            idField: 'changeCategoryID',
            textField: 'changeCategoryName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: false,
            closeDropDownOnSelection: true
        };
        this.changeClassificationDropdownSettings = {
            singleSelection: true,
            idField: 'changeClassificationID',
            textField: 'changeClassificationName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: false,
            closeDropDownOnSelection: true
        };
        this.changeCategoriesList = [
            { changeCategoryID: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["CCNCategoryStatusCodes"].Permanent, changeCategoryName: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["ChangeCategoriesListValues"].permanent },
            { changeCategoryID: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["CCNCategoryStatusCodes"].Temporary, changeCategoryName: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["ChangeCategoriesListValues"].temporary }
        ];
        this.selectedChangeCategory = this.changeCategoriesList;
        this.changeClassificationList = [
            { changeClassificationID: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["CCNClassificationStatusCodes"].Major, changeClassificationName: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["ChangeClassificationListValues"].major },
            { changeClassificationID: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["CCNClassificationStatusCodes"].Minor, changeClassificationName: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["ChangeClassificationListValues"].minor }
        ];
        this.selectedChangeClassification = this.changeClassificationList;
        console.log("change classific", this.selectedChangeClassification);
    }
    setIncidentCategoryAndClassificationDropdown() {
        this.incidentCategoryDropdownSettings = {
            singleSelection: false,
            idField: 'TypeofCategoryID',
            textField: 'TypeofIncident',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            closeDropDownOnSelection: false
        };
        this.serverService.getIncidentCategoryList().subscribe((res) => {
            this.incidentCategoriesList = res.data;
            this.selectedIncidentCategory = this.incidentCategoriesList;
        }, (err) => {
            console.error(err);
        });
        this.incidentClassificationDropdownSettings = {
            singleSelection: false,
            idField: 'incidentClassificationID',
            textField: 'incidentClassificationName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: false,
            closeDropDownOnSelection: false
        };
        this.incidentClassificationList = [
            {
                "incidentClassificationID": _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["IncidentClassificationStatusCodes"].QualityImpacting,
                "incidentClassificationName": 'Quality Impacting'
            },
            {
                "incidentClassificationID": _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["IncidentClassificationStatusCodes"].QualityNonImpacting,
                "incidentClassificationName": 'Quality Non Impacting'
            }
        ];
        this.selectedIncidentClassification = this.incidentClassificationList;
    }
    loadInitialDeptPerformanceData() {
        this.initialVisitCount = 0;
        this.getDepartmentsList(); // this method also contains getQualityEventsList()
    }
    //fetches departmentsList from DB and sorts it alphabetically
    getDepartmentsList() {
        // let roleAndEventID={
        //   roleID:2,
        //   eventId:3
        // }
        this.serverService.getAllDepartmentsList().subscribe((resultantDepartments) => {
            this.departmentsList = resultantDepartments.DeptFevent_list;
            this.departmentsList = this.departmentsList.sort(function (x, y) {
                if (x.DepartmentName < y.DepartmentName)
                    return -1;
                if (x.DepartmentName > y.DepartmentName)
                    return 1;
                return 0;
            });
            this.selectedDepartments = [{
                    Department_ID: this.departmentsList[0].Department_ID,
                    DepartmentName: this.departmentsList[0].DepartmentName
                }];
            // The below method also contains getDeptPerformanceDataFromFilters() 
            //method which fetches data from db with given filters
            this.getQualityEventsList();
        }, (err) => {
            console.log(err);
        });
    }
    //fetches qualityEventsList from DB and sorts it alphabetically
    //also sets Initial Date values and calls getDeptPerformanceDataFromFilters Method sequentially.
    getQualityEventsList() {
        this.serverService.getQualityEventsList().subscribe((resultantQualityEvents) => {
            this.qualityEventsList = resultantQualityEvents.sort(function (x, y) {
                if (x.QualityEvent_TypeName < y.QualityEvent_TypeName)
                    return -1;
                if (x.QualityEvent_TypeName > y.QualityEvent_TypeName)
                    return 1;
                return 0;
            });
            this.selectedQualityEvents = [{
                    QualityEvent_TypeID: this.qualityEventsList[0].QualityEvent_TypeID,
                    QualityEvent_TypeName: this.qualityEventsList[0].QualityEvent_TypeName
                }];
            this.setInitialDateValues();
            this.getDeptPerformanceDataFromFilters();
        }, (err) => {
            console.log(err);
        });
    }
    setInitialDateValues() {
        let sampleDay = parseInt(moment__WEBPACK_IMPORTED_MODULE_2__().format('DD'));
        let prevSampleDay = sampleDay - 1;
        let firstOfMonth = 0 + (sampleDay - prevSampleDay).toString();
        this.fromDateValue = moment__WEBPACK_IMPORTED_MODULE_2__().date(parseInt(firstOfMonth)).subtract(1, 'years').format('MMM YYYY');
        let fromDateISOString = moment__WEBPACK_IMPORTED_MODULE_2__().date(parseInt(firstOfMonth)).subtract(1, 'years').toISOString();
        this.selectedFromDate = moment__WEBPACK_IMPORTED_MODULE_2__().date(parseInt(firstOfMonth)).subtract(1, 'years').format('DD-MMM-YYYY');
        this.setToDate(fromDateISOString);
    }
    setToDate(fromDateISOString) {
        var tempToDate = new Date(fromDateISOString);
        this.toDateValue = moment__WEBPACK_IMPORTED_MODULE_2__(tempToDate.toISOString()).add(1, 'years').subtract(1, 'days').format('MMM YYYY');
        let mon = this.toDateValue.substr(0, 3);
        if (mon === moment__WEBPACK_IMPORTED_MODULE_2__().month(11).format('MMM')) {
            //adding 1 year to fromdate -> subtracting 1 day-> results in last day of previous month -> subtracting 1 year if the todate month is dec
            this.selectedToDate = moment__WEBPACK_IMPORTED_MODULE_2__(tempToDate).add(1, 'years').subtract(1, 'days').subtract(1, 'years').format('DD-MMM-YYYY');
        }
        else {
            this.selectedToDate = moment__WEBPACK_IMPORTED_MODULE_2__(tempToDate).add(1, 'years').subtract(1, 'days').format('DD-MMM-YYYY');
        }
    }
    getDeptNameFromDeptID(deptId) {
        for (let i = 0; i < this.departmentsList.length; i++) {
            if (this.departmentsList[i].Department_ID === deptId) {
                return this.departmentsList[i].DepartmentName;
            }
        }
    }
    getQualityEventNameFromQualityEventId(qualityEventID) {
        for (let i = 0; i < this.qualityEventsList.length; i++) {
            if (this.qualityEventsList[i].QualityEvent_TypeID === qualityEventID) {
                return this.qualityEventsList[i].QualityEvent_TypeName;
            }
        }
    }
    setFromAndToDates() {
        let changedFromDate = this.filtersForm.get('fromDate').value[0];
        let fromDateISOString = this.getRequiredDateFormat(changedFromDate);
        this.setToDate(fromDateISOString);
    }
    // getSelectedDeptIDs(){
    //   this.selectedDeptIds=[];
    //   for(let i=0;i<this.selectedDepartments.length;i++){
    //     this.selectedDeptIds.push(this.selectedDepartments[i].Department_ID);
    //   }
    // }
    // getSelectedQualityEventIDs(){
    //   this.selectedQualityEventIds=[];
    //   for(let i=0;i<this.selectedQualityEvents.length;i++){
    //     this.selectedQualityEventIds.push(this.selectedQualityEvents[i].QualityEvent_TypeID);
    //   }
    // }
    //Extracting Department IDs from departmentsList Object array
    getDepartmentListIds() {
        let departmentIds = [];
        for (let i = 0; i < this.departmentsList.length; i++) {
            departmentIds.push(this.departmentsList[i].Department_ID);
        }
        return departmentIds;
    }
    //Extracting Quality Event IDs from qualityEventsList Object array
    getQualityEventIds() {
        let qualityEventIds = [];
        for (let i = 0; i < this.qualityEventsList.length; i++) {
            qualityEventIds.push(this.qualityEventsList[i].QualityEvent_TypeID);
        }
        return qualityEventIds;
    }
    onDeptPerformanceDataChartFilterChange(event) {
        this.initialVisitCount++;
        if (this.initialVisitCount > 2) { // to avoid recurring triggering of change event from datepickers on initial load
            this.setFromAndToDates();
            this.getDeptPerformanceDataFromFilters();
        }
        else {
            return;
        }
    }
    getDeptPerformanceDataFromFilters() {
        this.deptPerformanceDataFilters = {
            DeptID: this.selectedDepartments[0].Department_ID,
            QualityEventType: this.selectedQualityEvents[0].QualityEvent_TypeID,
            CCNClassification: this.selectedChangeClassification[0].changeClassificationID,
            CCNCategory: (this.selectedChangeCategory.length === 0 || this.selectedChangeCategory.length > 1) ? _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["CCNCategoryStatusCodes"].Both : this.selectedChangeCategory[0].changeCategoryID,
            IncidentClassification: (this.selectedIncidentClassification.length === 0 || this.selectedIncidentClassification.length > 1) ? _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["IncidentClassificationStatusCodes"].All : this.selectedIncidentClassification[0].incidentClassificationID,
            IncidentCategory: 0,
            FromDate: this.selectedFromDate,
            ToDate: this.selectedToDate
        };
        if (this.currentMetricId === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["MetricsListValues"].CountMetricID) {
            this.isListViewShown = false;
            this.getCountMetricDataOfSelectedFilters();
        }
        else {
            this.isListViewShown = true;
            this.getTATMetricDataOfSelectedFilters();
        }
    }
    getCountMetricDataOfSelectedFilters() {
        this.serverService.getDeptPerformanceDataOfCountMetric(this.deptPerformanceDataFilters).subscribe((res) => {
            this.deptPerformanceData = res.Performance_List;
            this.setMonthsInASequence();
            this.setDeptAndOrgAverages();
            this.setUpperAndLowerLimits();
        }, (err) => {
            console.error(err);
        });
    }
    setMonthsInASequence() {
        var _a;
        let fromDateMonth = this.deptPerformanceDataFilters.FromDate.substr(3, 3);
        let fromDateYear = this.deptPerformanceDataFilters.FromDate.substr(7, 4);
        let toDateYear = this.deptPerformanceDataFilters.ToDate.substr(7, 4);
        console.log("fromdate year", fromDateYear);
        let seqArrayWithStartingMonth = [];
        let i = 0;
        let startingMonthIndexNumber;
        let otherHalfseqArray = [];
        for (i = 0; i < this.deptPerformanceData.length; i++) {
            if (((_a = this.deptPerformanceData[i]) === null || _a === void 0 ? void 0 : _a.Monthname) === fromDateMonth) {
                startingMonthIndexNumber = i;
                seqArrayWithStartingMonth.push(this.deptPerformanceData[i]);
                this.deptPerformanceData[i].MonthnameAndYear = this.deptPerformanceData[i].Monthname + ' - ' + fromDateYear;
                for (i = startingMonthIndexNumber + 1; i < this.deptPerformanceData.length; i++) {
                    seqArrayWithStartingMonth.push(this.deptPerformanceData[i]);
                    this.deptPerformanceData[i].MonthnameAndYear = this.deptPerformanceData[i].Monthname + ' - ' + fromDateYear;
                }
            }
            else {
                otherHalfseqArray.push(this.deptPerformanceData[i]);
                this.deptPerformanceData[i].MonthnameAndYear = this.deptPerformanceData[i].Monthname + ' - ' + toDateYear;
            }
        }
        let modifiedDeptPerformancedata = [...seqArrayWithStartingMonth, ...otherHalfseqArray];
        this.deptPerformanceData = modifiedDeptPerformancedata;
    }
    setDeptAndOrgAverages() {
        this.currentDeptAvg = this.deptPerformanceData[0].DeptAvg;
        this.currentOrgAvg = this.deptPerformanceData[0].OrgAvg;
    }
    setUpperAndLowerLimits() {
        let deptAvg = this.deptPerformanceData[0].DeptAvg;
        let sum = 0;
        for (let i = 0; i < this.deptPerformanceData.length; i++) {
            sum = sum + (Math.pow((this.deptPerformanceData[i].Count - deptAvg), 2));
        }
        let sd = Math.sqrt((sum / this.deptPerformanceData.length));
        this.upperLimit = deptAvg + sd;
        this.lowerLimit = (deptAvg - sd) > 0 ? (deptAvg - sd) : 0;
    }
    getTATMetricDataOfSelectedFilters() {
        this.serverService.getDeptPerformanceDataOfTATMetric(this.deptPerformanceDataFilters).subscribe((res) => {
            console.log("tat metric data", res);
            this.deptPerformanceData = res.Performance_List;
            this.setMonthsInASequence();
            this.setDeptAndOrgAverages();
            this.setUpperAndLowerLimits();
        }, (err) => {
            console.error(err);
        });
    }
    onSelectAllChangeCategories(items) {
        this.selectedChangeCategory = items;
        this.getDeptPerformanceDataFromFilters();
    }
    onSelectAllChangeClassifications(items) {
        this.selectedChangeClassification = items;
        this.getDeptPerformanceDataFromFilters();
    }
    onSelectAllIncidentCategories(items) {
        this.selectedIncidentCategory = items;
        this.getDeptPerformanceDataFromFilters();
    }
    onSelectAllIncidentClassification(items) {
        this.selectedIncidentClassification = items;
        this.getDeptPerformanceDataFromFilters();
    }
    onSelectAllQualityEvents(event) { }
    onSelectAllDepartments(event) { }
    onShowListView() {
        // this.getOverallListOfAppliedFilters();
        if (this.selectedQualityEvents[0].QualityEvent_TypeID === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventIDs"].ChangeControlNote) {
            this.getOverallCCNListOfAppliedFilters();
        }
        else if (this.selectedQualityEvents[0].QualityEvent_TypeID === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventIDs"].IncidentReport) {
            this.getOverallIncidentListOfAppliedFilters();
        }
        else if (this.selectedQualityEvents[0].QualityEvent_TypeID === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventIDs"].Errata) {
            this.getOverallErrataListOfAppliedFilters();
        }
        else {
            this.getOverallNTFListOfAppliedFilters();
        }
    }
    getUpperAndLowerLimitsForListView(qualityEventRecords) {
        var _a, _b, _c, _d;
        let sumOfAllTATs = 0;
        let earlierMonth = '';
        let count = 0;
        for (let i = 0; i < qualityEventRecords.length; i++) {
            qualityEventRecords[i].monthlyTAT = 0;
            qualityEventRecords[i].totalRecordsInMonth = 0;
        }
        if (qualityEventRecords.length !== 0) {
            for (let i = 0; i <= qualityEventRecords.length; i++) {
                //in the below code - calculation is being performed on the previous record, 
                //hence to perform on all records the index is made equal to the length of array
                if (i === 0) {
                    earlierMonth = qualityEventRecords[i].Month;
                    count = count + 1;
                    qualityEventRecords[i].totalRecordsInMonth = count;
                }
                else if (i === 1) {
                    if (((_a = qualityEventRecords[i]) === null || _a === void 0 ? void 0 : _a.Month) === earlierMonth) {
                        qualityEventRecords[i].monthlyTAT = qualityEventRecords[i - 1].TAT + qualityEventRecords[i].TAT;
                        // count++;
                        qualityEventRecords[i].totalRecordsInMonth = qualityEventRecords[i - 1].totalRecordsInMonth + 1;
                        earlierMonth = qualityEventRecords[i].Month;
                    }
                    else {
                        sumOfAllTATs = sumOfAllTATs + qualityEventRecords[i - 1].TAT;
                        earlierMonth = (_b = qualityEventRecords[i]) === null || _b === void 0 ? void 0 : _b.Month;
                        qualityEventRecords[i - 1].totalRecordsInMonth = 1;
                    }
                }
                else {
                    if (i !== qualityEventRecords.length) {
                        if (((_c = qualityEventRecords[i]) === null || _c === void 0 ? void 0 : _c.Month) === earlierMonth) {
                            if (qualityEventRecords[i - 1].monthlyTAT === 0) {
                                qualityEventRecords[i].monthlyTAT = qualityEventRecords[i - 1].TAT + qualityEventRecords[i].TAT;
                                // count++;
                                qualityEventRecords[i - 1].totalRecordsInMonth = 1;
                                qualityEventRecords[i].totalRecordsInMonth = qualityEventRecords[i - 1].totalRecordsInMonth + 1;
                                earlierMonth = qualityEventRecords[i].Month;
                            }
                            else {
                                qualityEventRecords[i].monthlyTAT = qualityEventRecords[i - 1].monthlyTAT + qualityEventRecords[i].TAT;
                                // count++;
                                qualityEventRecords[i].totalRecordsInMonth = qualityEventRecords[i - 1].totalRecordsInMonth + 1;
                                earlierMonth = qualityEventRecords[i].Month;
                            }
                        }
                        else {
                            if (qualityEventRecords[i - 1].monthlyTAT !== 0) {
                                sumOfAllTATs = sumOfAllTATs + (qualityEventRecords[i - 1].monthlyTAT / qualityEventRecords[i - 1].totalRecordsInMonth);
                                earlierMonth = qualityEventRecords[i].Month;
                                qualityEventRecords[i].totalRecordsInMonth = 1;
                            }
                            else {
                                sumOfAllTATs = sumOfAllTATs + qualityEventRecords[i - 1].TAT;
                                earlierMonth = (_d = qualityEventRecords[i]) === null || _d === void 0 ? void 0 : _d.Month;
                                qualityEventRecords[i].totalRecordsInMonth = 1;
                            }
                        }
                    }
                    else {
                        sumOfAllTATs = sumOfAllTATs + qualityEventRecords[i - 1].TAT;
                    }
                }
            }
            console.log("qualityEventRecords", qualityEventRecords);
            console.log("sum of all tats", sumOfAllTATs);
            let meanOfAllTATs = sumOfAllTATs / 12;
            let sum = 0;
            for (let i = 0; i < qualityEventRecords.length; i++) {
                sum = sum + (Math.pow((qualityEventRecords[i].TAT - meanOfAllTATs), 2));
            }
            let sd = Math.sqrt((sum / qualityEventRecords.length));
            let upperLimitForListView = meanOfAllTATs + sd;
            let lowerLimitForListView = (meanOfAllTATs - sd) > 0 ? (meanOfAllTATs - sd) : 0;
            return { upperLimitForListView: upperLimitForListView, lowerLimitForListView: lowerLimitForListView };
        }
        else {
            return { upperLimitForListView: 0, lowerLimitForListView: 0 };
        }
    }
    getOverallCCNListOfAppliedFilters() {
        this.ccnListFiltersForDeptPerformance = {
            DeptID: this.selectedDepartments[0].Department_ID,
            CCNClassification: this.selectedChangeClassification[0].changeClassificationID,
            CCNCategory: (this.selectedChangeCategory.length === 0 || this.selectedChangeCategory.length > 1) ? _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["CCNCategoryStatusCodes"].Both : this.selectedChangeCategory[0].changeCategoryID,
            FromDate: this.selectedFromDate,
            ToDate: this.selectedToDate
        };
        console.log("list filters for dept per", this.ccnListFiltersForDeptPerformance);
        this.serverService.getCCNListForDeptPerformance(this.ccnListFiltersForDeptPerformance).subscribe((res) => {
            console.log("ccn list for dept preformance", res.Cchart_List);
            this.CCNRecords = res.Cchart_List;
            for (let i = 0; i < this.CCNRecords.length; i++) {
                this.CCNRecords[i].TAT = parseInt(res.Cchart_List[i].TAT);
            }
            let limitsForListView = this.getUpperAndLowerLimitsForListView(this.CCNRecords);
            console.log("limits for list view", limitsForListView);
            const initialState = {
                currentDrillDownHeaderText: "Department Performance - CCN" + ' - ' + 'Overall List',
                ccnRecords: this.CCNRecords,
                qualityEvent: this.selectedQualityEvents[0].QualityEvent_TypeName,
                fileName: ' - ' + 'Overall List' + ' (' + this.ccnListFiltersForDeptPerformance.FromDate + ' to ' + this.ccnListFiltersForDeptPerformance.ToDate + ')',
                limits: limitsForListView,
                dates: { fromDate: this.selectedFromDate, toDate: this.selectedToDate }
            };
            this.bsModalRef = this.modalService.show(_list_view_dept_performance_list_view_dept_performance_component__WEBPACK_IMPORTED_MODULE_4__["ListViewDeptPerformanceComponent"], { initialState, class: 'gray modal-xl', backdrop: 'static', });
        }, (err) => {
            console.error(err);
        });
    }
    getOverallIncidentListOfAppliedFilters() {
        this.incidentListFilters = {
            DeptID: this.selectedDepartments[0].Department_ID,
            IncidentClassification: (this.selectedIncidentClassification.length === 0 || this.selectedIncidentClassification.length > 1) ? _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["IncidentClassificationStatusCodes"].All : this.selectedIncidentClassification[0].incidentClassificationID,
            IncidentCategory: 0,
            FromDate: this.selectedFromDate,
            ToDate: this.selectedToDate
        };
        this.serverService.getIncidentListForDeptPerformance(this.incidentListFilters).subscribe((res) => {
            console.log("Incident records for dept perf", res);
            this.incidentRecords = res.Pchart_List;
            for (let i = 0; i < this.incidentRecords.length; i++) {
                this.incidentRecords[i].TAT = parseInt(res.Pchart_List[i].TAT);
            }
            let limitsForListView = this.getUpperAndLowerLimitsForListView(this.incidentRecords);
            const initialState = {
                currentDrillDownHeaderText: "Department Performance - IR" + ' - ' + 'Overall List',
                incidentRecords: this.incidentRecords,
                qualityEvent: this.getQualityEventNameFromQualityEventId(this.selectedQualityEvents[0].QualityEvent_TypeID),
                fileName: ' - ' + 'Overall List' + ' (' + this.incidentListFilters.FromDate + ' to ' + this.incidentListFilters.ToDate + ')',
                limits: limitsForListView,
                dates: { fromDate: this.selectedFromDate, toDate: this.selectedToDate }
            };
            this.bsModalRef = this.modalService.show(_list_view_dept_performance_list_view_dept_performance_component__WEBPACK_IMPORTED_MODULE_4__["ListViewDeptPerformanceComponent"], { initialState, class: 'gray modal-xl', backdrop: 'static', });
        }, (err) => {
            console.error(err);
        });
    }
    getOverallErrataListOfAppliedFilters() {
        this.errataListFilters = {
            DeptID: this.selectedDepartments[0].Department_ID,
            FromDate: this.selectedFromDate,
            ToDate: this.selectedToDate
        };
        this.serverService.getErrataListForDeptPerformance(this.errataListFilters).subscribe((res) => {
            console.log("errata records fr dept perf", res);
            this.errrataRecords = res.Echart_List;
            for (let i = 0; i < this.errrataRecords.length; i++) {
                this.errrataRecords[i].TAT = parseInt(res.Echart_List[i].TAT);
            }
            let limitsForListView = this.getUpperAndLowerLimitsForListView(this.errrataRecords);
            const initialState = {
                currentDrillDownHeaderText: "Department Performance - Errata" + ' - ' + 'Overall List',
                errataRecords: this.errrataRecords,
                qualityEvent: this.getQualityEventNameFromQualityEventId(this.selectedQualityEvents[0].QualityEvent_TypeID),
                fileName: ' - ' + 'Overall List' + ' (' + this.errataListFilters.FromDate + ' to ' + this.errataListFilters.ToDate + ')',
                limits: limitsForListView,
                dates: { fromDate: this.selectedFromDate, toDate: this.selectedToDate }
            };
            this.bsModalRef = this.modalService.show(_list_view_dept_performance_list_view_dept_performance_component__WEBPACK_IMPORTED_MODULE_4__["ListViewDeptPerformanceComponent"], { initialState, class: 'gray modal-xl', backdrop: 'static', });
        }, (err) => {
            console.error(err);
        });
    }
    getOverallNTFListOfAppliedFilters() {
        this.noteToFileListFilters = {
            DeptID: this.selectedDepartments[0].Department_ID,
            FromDate: this.selectedFromDate,
            ToDate: this.selectedToDate
        };
        this.serverService.getNTFListForDeptPerformance(this.noteToFileListFilters).subscribe((res) => {
            console.log("NTF records for dept perf", res);
            this.noteToFileRecords = res.Nchart_List;
            for (let i = 0; i < this.noteToFileRecords.length; i++) {
                this.noteToFileRecords[i].TAT = parseInt(res.Nchart_List[i].TAT);
            }
            let limitsForListView = this.getUpperAndLowerLimitsForListView(this.noteToFileRecords);
            const initialState = {
                currentDrillDownHeaderText: "Department Performance - NTF" + ' - ' + 'Overall List',
                noteToFileRecords: this.noteToFileRecords,
                qualityEvent: this.getQualityEventNameFromQualityEventId(this.selectedQualityEvents[0].QualityEvent_TypeID),
                fileName: ' - ' + 'Overall List' + ' (' + this.noteToFileListFilters.FromDate + ' to ' + this.noteToFileListFilters.ToDate + ')',
                limits: limitsForListView,
                dates: { fromDate: this.selectedFromDate, toDate: this.selectedToDate }
            };
            this.bsModalRef = this.modalService.show(_list_view_dept_performance_list_view_dept_performance_component__WEBPACK_IMPORTED_MODULE_4__["ListViewDeptPerformanceComponent"], { initialState, class: 'gray modal-xl', backdrop: 'static', });
        }, (err) => {
            console.error(err);
        });
    }
}
DepartmentPerformanceChartComponent.ɵfac = function DepartmentPerformanceChartComponent_Factory(t) { return new (t || DepartmentPerformanceChartComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_dashboards_service__WEBPACK_IMPORTED_MODULE_5__["DashboardsService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_server_service__WEBPACK_IMPORTED_MODULE_7__["ServerService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_8__["BsModalService"])); };
DepartmentPerformanceChartComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: DepartmentPerformanceChartComponent, selectors: [["app-department-performance-chart"]], decls: 50, vars: 43, consts: [[1, "filter-bar"], [1, "row", "mt-3"], [1, "col-md-12"], [1, "col-sm-12", "padding-none", "dashboard_panel_full", "float-left"], [1, "col-lg-12", "col-12", "col-sm-12", "col-md-12", "col-12", "col-sm-12", "col-md-12", "dashboard_panel_header", "float-left"], [1, "col-lg-12", "col-12", "col-sm-12", "col-md-12", "col-12", "col-sm-12", "col-md-12", "float-left", "top", "bottom"], [1, "mb-2", 3, "formGroup"], [1, "row"], [1, "col-md-6"], [3, "placeholder", "settings", "data", "ngModel", "ngModelOptions", "ngModelChange", "onSelect", "onDeSelect"], [1, "row", "mt-2"], ["class", "col-md-6", 4, "ngIf"], [3, "settings", "data", "ngModel", "ngModelOptions", "ngModelChange", "onSelect", "onDeSelect"], ["id", "fromDate", "placeholder", "From date", "formControlName", "fromDate", 3, "setDate", "config", "change"], ["id", "toDate", "formControlName", "toDate", "placeholder", "To date", 3, "setDate", "config", "change"], ["class", "d-flex justify-content-end", 4, "ngIf"], ["id", "deptPerformanceChart", "title", "Department Performance (12M)", 3, "dataSource", "customizePoint", "customizeLabel"], ["argumentField", "MonthnameAndYear", "valueField", "Count", "type", "spline", "color", "#a3aaaa"], ["overlappingBehavior", "rotate", "rotationAngle", "45", "wordWrap", "breakWord"], [3, "allowDecimals"], [3, "customizeText"], ["color", "rgba(97,153,230,0.10)", 3, "endValue", 4, "ngIf"], ["color", "rgba(255,155,85,0.15)", 3, "startValue", 4, "ngIf"], ["weight", "500", "size", "14"], ["color", "#8c8cff", "dashStyle", "dash", 3, "width", "value"], ["text", "Dept Avg", 3, "font"], ["color", "#ff7c7c", "dashStyle", "dot", 3, "width", "value", 4, "ngIf"], ["verticalAlignment", "bottom", 3, "visible"], [3, "enabled"], [1, "d-flex", "flex-row", "justify-content-around"], [1, "dx-field"], [1, "dx-field-value"], ["text", "Show Limits", 3, "value", "valueChange"], ["text", "Show Org Average", 3, "value", "valueChange"], [3, "placeholder", "settings", "data", "ngModel", "ngModelOptions", "ngModelChange", "onSelect", "onDeSelect", "onSelectAll"], [3, "settings", "data", "ngModel", "ngModelOptions", "ngModelChange", "onSelect", "onDeSelect", "onSelectAll"], [1, "d-flex", "justify-content-end"], ["type", "button", 1, "btn", "btn-link", 3, "click"], ["color", "rgba(97,153,230,0.10)", 3, "endValue"], ["text", "LL", "verticalAlignment", "top"], [3, "color"], ["color", "rgba(255,155,85,0.15)", 3, "startValue"], ["text", "UL", "verticalAlignment", "bottom"], ["color", "#ff7c7c", "dashStyle", "dot", 3, "width", "value"], ["text", "Org Avg", "horizontalAlignment", "right", 3, "font"]], template: function DepartmentPerformanceChartComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Department Performance");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "form", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "ng-multiselect-dropdown", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function DepartmentPerformanceChartComponent_Template_ng_multiselect_dropdown_ngModelChange_10_listener($event) { return ctx.selectedDepartments = $event; })("onSelect", function DepartmentPerformanceChartComponent_Template_ng_multiselect_dropdown_onSelect_10_listener($event) { return ctx.onDeptPerformanceDataChartFilterChange($event); })("onDeSelect", function DepartmentPerformanceChartComponent_Template_ng_multiselect_dropdown_onDeSelect_10_listener($event) { return ctx.onDeptPerformanceDataChartFilterChange($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "ng-multiselect-dropdown", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function DepartmentPerformanceChartComponent_Template_ng_multiselect_dropdown_ngModelChange_12_listener($event) { return ctx.selectedQualityEvents = $event; })("onSelect", function DepartmentPerformanceChartComponent_Template_ng_multiselect_dropdown_onSelect_12_listener($event) { return ctx.onDeptPerformanceDataChartFilterChange($event); })("onDeSelect", function DepartmentPerformanceChartComponent_Template_ng_multiselect_dropdown_onDeSelect_12_listener($event) { return ctx.onDeptPerformanceDataChartFilterChange($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, DepartmentPerformanceChartComponent_div_14_Template, 2, 6, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](15, DepartmentPerformanceChartComponent_div_15_Template, 2, 6, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, DepartmentPerformanceChartComponent_div_16_Template, 2, 5, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, DepartmentPerformanceChartComponent_div_17_Template, 2, 5, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "ng-multiselect-dropdown", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function DepartmentPerformanceChartComponent_Template_ng_multiselect_dropdown_ngModelChange_20_listener($event) { return ctx.selectedMetric = $event; })("onSelect", function DepartmentPerformanceChartComponent_Template_ng_multiselect_dropdown_onSelect_20_listener($event) { return ctx.onMetricSelectionChange($event); })("onDeSelect", function DepartmentPerformanceChartComponent_Template_ng_multiselect_dropdown_onDeSelect_20_listener($event) { return ctx.onMetricSelectionChange($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "ng2-flatpickr", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function DepartmentPerformanceChartComponent_Template_ng2_flatpickr_change_23_listener($event) { return ctx.onDeptPerformanceDataChartFilterChange($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "ng2-flatpickr", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function DepartmentPerformanceChartComponent_Template_ng2_flatpickr_change_25_listener($event) { return ctx.onDeptPerformanceDataChartFilterChange($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](26, DepartmentPerformanceChartComponent_div_26_Template, 3, 0, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "dx-chart", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "dxi-series", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "dxo-argument-axis");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "dxo-label", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "dxi-value-axis", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "dxo-label", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](33, DepartmentPerformanceChartComponent_dxi_strip_33_Template, 3, 2, "dxi-strip", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](34, DepartmentPerformanceChartComponent_dxi_strip_34_Template, 3, 2, "dxi-strip", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "dxo-strip-style");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "dxo-label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "dxo-font", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "dxi-constant-line", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "dxo-label", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](40, DepartmentPerformanceChartComponent_dxi_constant_line_40_Template, 2, 4, "dxi-constant-line", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "dxo-legend", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](42, "dxo-export", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "dx-check-box", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("valueChange", function DepartmentPerformanceChartComponent_Template_dx_check_box_valueChange_46_listener($event) { return ctx.isLimitsShown = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "dx-check-box", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("valueChange", function DepartmentPerformanceChartComponent_Template_dx_check_box_valueChange_49_listener($event) { return ctx.isAveragesShown = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.filtersForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", "Select Department")("settings", ctx.departmentDropdownSettings)("data", ctx.departmentsList)("ngModel", ctx.selectedDepartments)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](39, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", "Select Quality Event")("settings", ctx.eventsDropdownSettings)("data", ctx.qualityEventsList)("ngModel", ctx.selectedQualityEvents)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](40, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.deptPerformanceDataFilters && ctx.deptPerformanceDataFilters.QualityEventType === ctx.currentQualityEventId.CCN);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.deptPerformanceDataFilters && ctx.deptPerformanceDataFilters.QualityEventType === ctx.currentQualityEventId.CCN);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.deptPerformanceDataFilters && ctx.deptPerformanceDataFilters.QualityEventType === ctx.currentQualityEventId.IncidentReport);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.deptPerformanceDataFilters && ctx.deptPerformanceDataFilters.QualityEventType === ctx.currentQualityEventId.IncidentReport);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("settings", ctx.metricSelectionDropdownSettings)("data", ctx.metricSelectionList)("ngModel", ctx.selectedMetric)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](41, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("setDate", ctx.fromDateValue)("config", ctx.fromDateOptions);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("setDate", ctx.toDateValue)("config", ctx.toDateOptions);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isListViewShown);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("dataSource", ctx.deptPerformanceData)("customizePoint", ctx.customizePoint)("customizeLabel", ctx.customizeLabel);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("allowDecimals", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("customizeText", ctx.customizeText);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isLimitsShown);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isLimitsShown);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("width", 2)("value", ctx.currentDeptAvg);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("font", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](42, _c2));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isAveragesShown);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("enabled", true);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.isLimitsShown);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.isAveragesShown);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormGroupDirective"], ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_9__["MultiSelectComponent"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_10__["NgIf"], ng2_flatpickr__WEBPACK_IMPORTED_MODULE_11__["Ng2FlatpickrComponent"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControlName"], devextreme_angular__WEBPACK_IMPORTED_MODULE_12__["DxChartComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_13__["DxiSeriesComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_13__["DxoArgumentAxisComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_13__["DxoLabelComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_13__["DxiValueAxisComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_13__["DxoStripStyleComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_13__["DxoFontComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_13__["DxiConstantLineComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_13__["DxoLegendComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_13__["DxoExportComponent"], devextreme_angular__WEBPACK_IMPORTED_MODULE_12__["DxCheckBoxComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_13__["DxiStripComponent"]], styles: [".dx-checkbox-container {\n  margin-left: 0.5rem !important;\n}\n  .dx-checkbox-container .dx-checkbox-icon {\n  border: 1px solid #adadad !important;\n  border-radius: 4px !important;\n}\n  .dx-checkbox-text {\n  white-space: inherit !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkcy9kZXBhcnRtZW50LXBlcmZvcm1hbmNlLWNoYXJ0L2RlcGFydG1lbnQtcGVyZm9ybWFuY2UtY2hhcnQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSw4QkFBQTtBQUNKO0FBQUk7RUFDSSxvQ0FBQTtFQUNBLDZCQUFBO0FBRVI7QUFFQTtFQUNJLCtCQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9kYXNoYm9hcmRzL2RlcGFydG1lbnQtcGVyZm9ybWFuY2UtY2hhcnQvZGVwYXJ0bWVudC1wZXJmb3JtYW5jZS1jaGFydC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjo6bmctZGVlcCAuZHgtY2hlY2tib3gtY29udGFpbmVye1xyXG4gICAgbWFyZ2luLWxlZnQ6MC41cmVtICFpbXBvcnRhbnQ7XHJcbiAgICAuZHgtY2hlY2tib3gtaWNvbntcclxuICAgICAgICBib3JkZXI6MXB4IHNvbGlkICNhZGFkYWQgIWltcG9ydGFudDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA0cHggIWltcG9ydGFudDtcclxuICAgIH1cclxufVxyXG5cclxuOjpuZy1kZWVwIC5keC1jaGVja2JveC10ZXh0e1xyXG4gICAgd2hpdGUtc3BhY2U6IGluaGVyaXQgIWltcG9ydGFudDtcclxufVxyXG5cclxuLy8gOjpuZy1kZWVwIC5uZzItZmxhdHBpY2tyLWlucHV0LWNvbnRhaW5lcntcclxuLy8gICAgIC50b0RhdGUtZGVwdC1wZXJme1xyXG4vLyAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNlOWVjZWYgIWltcG9ydGFudDtcclxuLy8gICAgIH1cclxuLy8gfVxyXG4iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DepartmentPerformanceChartComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-department-performance-chart',
                templateUrl: './department-performance-chart.component.html',
                styleUrls: ['./department-performance-chart.component.scss']
            }]
    }], function () { return [{ type: _services_dashboards_service__WEBPACK_IMPORTED_MODULE_5__["DashboardsService"] }, { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"] }, { type: _services_server_service__WEBPACK_IMPORTED_MODULE_7__["ServerService"] }, { type: ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_8__["BsModalService"] }]; }, null); })();


/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false,
    serverUrl: 'http://localhost:53973/api/Dashboard'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "BNDV":
/*!*************************************************!*\
  !*** ./src/app/dashboards/dashboards.module.ts ***!
  \*************************************************/
/*! exports provided: DashboardsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardsModule", function() { return DashboardsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "SVse");
/* harmony import */ var _dashboards_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dashboards.component */ "Hktm");
/* harmony import */ var _management_level_chart_management_level_chart_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./management-level-chart/management-level-chart.component */ "F19l");
/* harmony import */ var devextreme_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! devextreme-angular */ "hYZE");
/* harmony import */ var ng2_flatpickr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng2-flatpickr */ "A/TE");
/* harmony import */ var angular2_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular2-multiselect-dropdown */ "8Zpt");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "s7LF");
/* harmony import */ var ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng-multiselect-dropdown */ "UPO+");
/* harmony import */ var _department_performance_chart_department_performance_chart_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./department-performance-chart/department-performance-chart.component */ "0SbY");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! angular-datatables */ "oTcB");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-bootstrap/modal */ "LqlI");
/* harmony import */ var _list_view_list_view_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./list-view/list-view.component */ "Oj50");
/* harmony import */ var _list_view_dept_performance_list_view_dept_performance_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./list-view-dept-performance/list-view-dept-performance.component */ "MiXW");
/* harmony import */ var _capa_chart_capa_chart_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./capa-chart/capa-chart.component */ "sW5g");


















class DashboardsModule {
}
DashboardsModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: DashboardsModule });
DashboardsModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function DashboardsModule_Factory(t) { return new (t || DashboardsModule)(); }, providers: [
    // NgxSmartModalService
    ], imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            devextreme_angular__WEBPACK_IMPORTED_MODULE_4__["DxChartModule"],
            devextreme_angular__WEBPACK_IMPORTED_MODULE_4__["DxButtonModule"],
            ng2_flatpickr__WEBPACK_IMPORTED_MODULE_5__["Ng2FlatpickrModule"],
            angular2_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_6__["AngularMultiSelectModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
            devextreme_angular__WEBPACK_IMPORTED_MODULE_4__["DxPieChartModule"],
            devextreme_angular__WEBPACK_IMPORTED_MODULE_4__["DxCheckBoxModule"],
            ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_8__["NgMultiSelectDropDownModule"].forRoot(),
            // NgxSmartModalModule.forRoot(),
            angular_datatables__WEBPACK_IMPORTED_MODULE_10__["DataTablesModule"],
            ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_11__["ModalModule"].forRoot()
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](DashboardsModule, { declarations: [_dashboards_component__WEBPACK_IMPORTED_MODULE_2__["DashboardsComponent"], _management_level_chart_management_level_chart_component__WEBPACK_IMPORTED_MODULE_3__["ManagementLevelChartComponent"], _department_performance_chart_department_performance_chart_component__WEBPACK_IMPORTED_MODULE_9__["DepartmentPerformanceChartComponent"], _list_view_list_view_component__WEBPACK_IMPORTED_MODULE_12__["ListViewComponent"], _list_view_dept_performance_list_view_dept_performance_component__WEBPACK_IMPORTED_MODULE_13__["ListViewDeptPerformanceComponent"], _capa_chart_capa_chart_component__WEBPACK_IMPORTED_MODULE_14__["CapaChartComponent"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
        devextreme_angular__WEBPACK_IMPORTED_MODULE_4__["DxChartModule"],
        devextreme_angular__WEBPACK_IMPORTED_MODULE_4__["DxButtonModule"],
        ng2_flatpickr__WEBPACK_IMPORTED_MODULE_5__["Ng2FlatpickrModule"],
        angular2_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_6__["AngularMultiSelectModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
        devextreme_angular__WEBPACK_IMPORTED_MODULE_4__["DxPieChartModule"],
        devextreme_angular__WEBPACK_IMPORTED_MODULE_4__["DxCheckBoxModule"], ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_8__["NgMultiSelectDropDownModule"], 
        // NgxSmartModalModule.forRoot(),
        angular_datatables__WEBPACK_IMPORTED_MODULE_10__["DataTablesModule"], ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_11__["ModalModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DashboardsModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                declarations: [_dashboards_component__WEBPACK_IMPORTED_MODULE_2__["DashboardsComponent"], _management_level_chart_management_level_chart_component__WEBPACK_IMPORTED_MODULE_3__["ManagementLevelChartComponent"], _department_performance_chart_department_performance_chart_component__WEBPACK_IMPORTED_MODULE_9__["DepartmentPerformanceChartComponent"], _list_view_list_view_component__WEBPACK_IMPORTED_MODULE_12__["ListViewComponent"], _list_view_dept_performance_list_view_dept_performance_component__WEBPACK_IMPORTED_MODULE_13__["ListViewDeptPerformanceComponent"], _capa_chart_capa_chart_component__WEBPACK_IMPORTED_MODULE_14__["CapaChartComponent"]],
                imports: [
                    _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                    devextreme_angular__WEBPACK_IMPORTED_MODULE_4__["DxChartModule"],
                    devextreme_angular__WEBPACK_IMPORTED_MODULE_4__["DxButtonModule"],
                    ng2_flatpickr__WEBPACK_IMPORTED_MODULE_5__["Ng2FlatpickrModule"],
                    angular2_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_6__["AngularMultiSelectModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                    devextreme_angular__WEBPACK_IMPORTED_MODULE_4__["DxPieChartModule"],
                    devextreme_angular__WEBPACK_IMPORTED_MODULE_4__["DxCheckBoxModule"],
                    ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_8__["NgMultiSelectDropDownModule"].forRoot(),
                    // NgxSmartModalModule.forRoot(),
                    angular_datatables__WEBPACK_IMPORTED_MODULE_10__["DataTablesModule"],
                    ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_11__["ModalModule"].forRoot()
                ],
                providers: [
                // NgxSmartModalService
                ],
                entryComponents: [_list_view_list_view_component__WEBPACK_IMPORTED_MODULE_12__["ListViewComponent"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "F19l":
/*!***************************************************************************************!*\
  !*** ./src/app/dashboards/management-level-chart/management-level-chart.component.ts ***!
  \***************************************************************************************/
/*! exports provided: ManagementLevelChartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagementLevelChartComponent", function() { return ManagementLevelChartComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _models_dashboards__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/dashboards */ "+E+c");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "wd/R");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-datatables */ "oTcB");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var _list_view_list_view_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../list-view/list-view.component */ "Oj50");
/* harmony import */ var _services_dashboards_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/dashboards.service */ "PGxC");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "s7LF");
/* harmony import */ var _services_server_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/server.service */ "H9vK");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/modal */ "LqlI");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common */ "SVse");
/* harmony import */ var ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ng-multiselect-dropdown */ "UPO+");
/* harmony import */ var ng2_flatpickr__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ng2-flatpickr */ "A/TE");
/* harmony import */ var devextreme_angular__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! devextreme-angular */ "hYZE");
/* harmony import */ var devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! devextreme-angular/ui/nested */ "Q08N");
















const _c0 = ["fromDatePickrElement"];
function ManagementLevelChartComponent_a_5_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ManagementLevelChartComponent_a_5_Template_a_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.onPreviousLevelClick(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c1 = function () { return { standalone: true }; };
function ManagementLevelChartComponent_div_13_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ng-multiselect-dropdown", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function ManagementLevelChartComponent_div_13_div_1_Template_ng_multiselect_dropdown_ngModelChange_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r10); const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r9.selectedChangeCategory = $event; })("onSelect", function ManagementLevelChartComponent_div_13_div_1_Template_ng_multiselect_dropdown_onSelect_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r10); const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r11.getFilteredStatusChartData($event); })("onDeSelect", function ManagementLevelChartComponent_div_13_div_1_Template_ng_multiselect_dropdown_onDeSelect_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r10); const ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r12.getFilteredStatusChartData($event); })("onSelectAll", function ManagementLevelChartComponent_div_13_div_1_Template_ng_multiselect_dropdown_onSelectAll_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r10); const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r13.onSelectAllChangeCategories($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", "Change Category")("settings", ctx_r5.changeCategoryDropdownSettings)("data", ctx_r5.changeCategoriesList)("ngModel", ctx_r5.selectedChangeCategory)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](5, _c1));
} }
function ManagementLevelChartComponent_div_13_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ng-multiselect-dropdown", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function ManagementLevelChartComponent_div_13_div_2_Template_ng_multiselect_dropdown_ngModelChange_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r15); const ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r14.selectedChangeClassification = $event; })("onSelect", function ManagementLevelChartComponent_div_13_div_2_Template_ng_multiselect_dropdown_onSelect_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r15); const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r16.getFilteredStatusChartData($event); })("onDeSelect", function ManagementLevelChartComponent_div_13_div_2_Template_ng_multiselect_dropdown_onDeSelect_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r15); const ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r17.getFilteredStatusChartData($event); })("onSelectAll", function ManagementLevelChartComponent_div_13_div_2_Template_ng_multiselect_dropdown_onSelectAll_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r15); const ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r18.onSelectAllChangeClassifications($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", "Change Classification")("settings", ctx_r6.changeClassificationDropdownSettings)("data", ctx_r6.changeClassificationList)("ngModel", ctx_r6.selectedChangeClassification)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](5, _c1));
} }
function ManagementLevelChartComponent_div_13_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r20 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ng-multiselect-dropdown", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function ManagementLevelChartComponent_div_13_div_3_Template_ng_multiselect_dropdown_ngModelChange_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r20); const ctx_r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r19.selectedIncidentCategory = $event; })("onSelect", function ManagementLevelChartComponent_div_13_div_3_Template_ng_multiselect_dropdown_onSelect_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r20); const ctx_r21 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r21.getFilteredStatusChartData($event); })("onDeSelect", function ManagementLevelChartComponent_div_13_div_3_Template_ng_multiselect_dropdown_onDeSelect_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r20); const ctx_r22 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r22.getFilteredStatusChartData($event); })("onSelectAll", function ManagementLevelChartComponent_div_13_div_3_Template_ng_multiselect_dropdown_onSelectAll_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r20); const ctx_r23 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r23.onSelectAllIncidentCategories($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("settings", ctx_r7.incidentCategoryDropdownSettings)("data", ctx_r7.incidentCategoriesList)("ngModel", ctx_r7.selectedIncidentCategory)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](4, _c1));
} }
function ManagementLevelChartComponent_div_13_div_4_Template(rf, ctx) { if (rf & 1) {
    const _r25 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ng-multiselect-dropdown", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function ManagementLevelChartComponent_div_13_div_4_Template_ng_multiselect_dropdown_ngModelChange_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r25); const ctx_r24 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r24.selectedIncidentClassification = $event; })("onSelect", function ManagementLevelChartComponent_div_13_div_4_Template_ng_multiselect_dropdown_onSelect_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r25); const ctx_r26 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r26.getFilteredStatusChartData($event); })("onDeSelect", function ManagementLevelChartComponent_div_13_div_4_Template_ng_multiselect_dropdown_onDeSelect_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r25); const ctx_r27 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r27.getFilteredStatusChartData($event); })("onSelectAll", function ManagementLevelChartComponent_div_13_div_4_Template_ng_multiselect_dropdown_onSelectAll_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r25); const ctx_r28 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r28.onSelectAllIncidentClassification($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("settings", ctx_r8.incidentClassificationDropdownSettings)("data", ctx_r8.incidentClassificationList)("ngModel", ctx_r8.selectedIncidentClassification)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](4, _c1));
} }
function ManagementLevelChartComponent_div_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ManagementLevelChartComponent_div_13_div_1_Template, 2, 6, "div", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, ManagementLevelChartComponent_div_13_div_2_Template, 2, 6, "div", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, ManagementLevelChartComponent_div_13_div_3_Template, 2, 5, "div", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, ManagementLevelChartComponent_div_13_div_4_Template, 2, 5, "div", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r1.qualityEventDataOfOneDept.QualityEventType === ctx_r1.currentQualityEventId.CCN);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r1.qualityEventDataOfOneDept.QualityEventType === ctx_r1.currentQualityEventId.CCN);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r1.qualityEventDataOfOneDept.QualityEventType === ctx_r1.currentQualityEventId.IncidentReport);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r1.qualityEventDataOfOneDept.QualityEventType === ctx_r1.currentQualityEventId.IncidentReport);
} }
function ManagementLevelChartComponent_div_19_Template(rf, ctx) { if (rf & 1) {
    const _r30 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "button", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ManagementLevelChartComponent_div_19_Template_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r30); const ctx_r29 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r29.onShowOverallListView(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "List View");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c2 = function () { return { name: "frequency", position: "left", tickInterval: 5, title: "Overdues Count" }; };
const _c3 = function (a0) { return { customizeText: a0 }; };
const _c4 = function () { return { visible: false }; };
const _c5 = function (a4) { return { value: 80, color: "#fc3535", dashStyle: "dash", width: 2, label: a4 }; };
const _c6 = function (a0) { return [a0]; };
const _c7 = function (a4, a5) { return { name: "percentage", position: "right", showZero: true, title: "Cumulative Percentage", label: a4, constantLines: a5, valueMarginsEnabled: false }; };
const _c8 = function (a0, a1) { return [a0, a1]; };
class ManagementLevelChartComponent {
    constructor(dashboardService, fb, serverService, modalService, changeDetection, renderer) {
        this.dashboardService = dashboardService;
        this.fb = fb;
        this.serverService = serverService;
        this.modalService = modalService;
        this.changeDetection = changeDetection;
        this.renderer = renderer;
        this.events = [];
        this.dtOptions = {};
        this.dtTrigger = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Subject"]();
        this.statusChartDataSource = [];
        this.departmentsList = [];
        this.selectedDepartments = [];
        this.departmentDropdownSettings = {};
        this.eventsDropdownSettings = {};
        this.qualityEventsList = [];
        this.selectedQualityEvents = [];
        this.changeCategoriesList = [];
        this.selectedChangeCategory = [];
        this.changeCategoryDropdownSettings = {};
        this.changeClassificationList = [];
        this.selectedChangeClassification = [];
        this.changeClassificationDropdownSettings = {};
        this.selectedCCNClassificationIDs = [];
        this.incidentCategoriesList = [];
        this.selectedIncidentCategory = [];
        this.incidentCategoryDropdownSettings = {};
        this.incidentClassificationList = [];
        this.selectedIncidentClassification = [];
        this.incidentClassificationDropdownSettings = {};
        this.fromDateOptions = {};
        //   fromDateOptions: Partial<BaseOptions> = {
        //   dateFormat:"d-M-Y",
        //   onClose: this.closeFromDate.bind(this)
        // };
        this.toDateOptions = {
        // dateFormat:"d-M-Y",
        //     // onOpen:this.openTodate.bind(this)
        };
        this.subtitleForStackedBarChart = '';
        this.currentFileName = '';
        this.currentQualityEventId = _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventIds"];
        this.selectedDeptIds = [];
        this.overdueData = [];
        this.selectedQualityEventName = '';
        this.CCNRecords = [];
        this.incidentRecords = [];
        this.errrataRecords = [];
        this.noteToFileRecords = [];
        //returns department ID from Dept name
        this.getDeptIdFromDeptName = (deptName) => {
            for (let i = 0; i < this.departmentsList.length; i++) {
                if (this.departmentsList[i].DepartmentName === deptName) {
                    return this.departmentsList[i].Department_ID;
                }
            }
        };
        //returns qualityEvent ID from qualityEvent name
        this.getQualityEventIdFromQualityEventName = (qualityEventName) => {
            for (let i = 0; i < this.qualityEventsList.length; i++) {
                if (this.qualityEventsList[i].QualityEvent_TypeName === qualityEventName) {
                    return this.qualityEventsList[i].QualityEvent_TypeID;
                }
            }
        };
        //converts fetched date into DB compatible date format
        this.getRequiredDateFormat = (selectedDate) => {
            var tempDate = new Date(selectedDate);
            let sampleFromDate = moment__WEBPACK_IMPORTED_MODULE_2__(tempDate.toISOString());
            return sampleFromDate.format('DD-MMM-YYYY');
        };
        this.customizeTooltipForOverdue = (info) => {
            return {
                html: "<div><div class='tooltip-header'>" +
                    info.argumentText + "</div>" +
                    "<div class='tooltip-body'><div class='series-name'>" +
                    "<span class='top-series-name'>" + info.points[0].seriesName + "</span>" +
                    ": </div><div class='value-text'>" +
                    "<span class='top-series-value'>" + info.points[0].valueText + "</span>" +
                    "</div><div class='series-name'>" +
                    "<span class='bottom-series-name'>" + info.points[1].seriesName + "</span>" +
                    ": </div><div class='value-text'>" +
                    "<span class='bottom-series-value'>" + info.points[1].valueText + "</span>" +
                    "% </div></div></div>"
            };
        };
        this.customizeLabelText = (info) => {
            return info.valueText + "%";
        };
        this.overallStatuslevel = 0;
        this.setChangeCategoryAndClassificationDropdown();
        this.setIncidentCategoryAndClassificationDropdown();
        this.filtersForm = this.fb.group({
            fromDate: [''],
            toDate: ['']
        });
        this.getDepartmentsList();
        this.getQualityEventsList();
        this.fromDateOptions.dateFormat = 'd-M-Y';
        this.fromDateOptions.defaultDate = moment__WEBPACK_IMPORTED_MODULE_2__().subtract(1, 'years').format('DD-MMM-YYYY');
        this.toDateOptions.dateFormat = 'd-M-Y';
        this.toDateOptions.defaultDate = moment__WEBPACK_IMPORTED_MODULE_2__().format('DD-MMM-YYYY');
    }
    ngOnInit() {
        this.departmentDropdownSettings = {
            singleSelection: false,
            idField: 'Department_ID',
            textField: 'DepartmentName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
        };
        this.eventsDropdownSettings = {
            singleSelection: false,
            idField: 'QualityEvent_TypeID',
            textField: 'QualityEvent_TypeName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
        };
        // let fromDateElem = document.getElementById('fromDatePickrElement');
        // this.fromDateOptionsNew=flatpickr(fromDateElem,{
        //   dateFormat:'d-M-Y',
        //   defaultDate : moment().subtract(1,'years').format('DD-MMM-YYYY'),
        //   onClose: this.closeFromDate.bind(this)
        // });
        // let toDateElem = document.getElementById('toDatePickrElement');
        // this.toDateOptionsNew=flatpickr(toDateElem,{
        //   dateFormat:"d-M-Y",
        //   defaultDate: moment().format('DD-MMM-YYYY')
        // });
        // this.fromDateOptions={
        //     dateFormat:'d-M-Y',
        //   defaultDate : moment().subtract(1,'years').format('DD-MMM-YYYY'),
        //   onClose: this.closeFromDate.bind(this,this.toDateOptionsNew)
        // }
        this.initialFromDate = moment__WEBPACK_IMPORTED_MODULE_2__().subtract(1, 'years').format('DD-MMM-YYYY');
        this.initialToDate = moment__WEBPACK_IMPORTED_MODULE_2__().format('DD-MMM-YYYY');
        this.allDepartmentQualityEventsFilters = {
            DeptID: this.getDepartmentListIds(),
            QualityEventType: this.getQualityEventIds(),
            FromDate: this.initialFromDate,
            ToDate: this.initialToDate
        };
        this.previousDataFiltersForStackedBarChart = this.allDepartmentQualityEventsFilters;
        this.getDepartmentsQualityEventsDataFromFilters(this.allDepartmentQualityEventsFilters);
        this.subtitleForStackedBarChart = '( ' + this.allDepartmentQualityEventsFilters.FromDate + ' to ' + this.allDepartmentQualityEventsFilters.ToDate + ' )';
    }
    ngAfterViewInit() {
        let flatpickrInput = document.getElementsByClassName('ng2-flatpickr-input');
        for (let i = 0; i < flatpickrInput.length; i++) {
            this.renderer.addClass(flatpickrInput[i], 'form-control');
        }
    }
    setChangeCategoryAndClassificationDropdown() {
        this.changeCategoryDropdownSettings = {
            singleSelection: false,
            idField: 'changeCategoryID',
            textField: 'changeCategoryName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: false,
        };
        this.changeClassificationDropdownSettings = {
            singleSelection: false,
            idField: 'changeClassificationID',
            textField: 'changeClassificationName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: false,
        };
        this.changeCategoriesList = [
            { changeCategoryID: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["CCNCategoryStatusCodes"].Permanent, changeCategoryName: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["ChangeCategoriesListValues"].permanent },
            { changeCategoryID: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["CCNCategoryStatusCodes"].Temporary, changeCategoryName: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["ChangeCategoriesListValues"].temporary }
        ];
        this.selectedChangeCategory = this.changeCategoriesList;
        this.changeClassificationList = [
            { changeClassificationID: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["CCNClassificationStatusCodes"].Major, changeClassificationName: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["ChangeClassificationListValues"].major },
            { changeClassificationID: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["CCNClassificationStatusCodes"].Minor, changeClassificationName: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["ChangeClassificationListValues"].minor },
            { changeClassificationID: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["CCNClassificationStatusCodes"].Unclassified, changeClassificationName: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["ChangeClassificationListValues"].unclassified }
        ];
        this.selectedChangeClassification = this.changeClassificationList;
    }
    setIncidentCategoryAndClassificationDropdown() {
        this.incidentCategoryDropdownSettings = {
            singleSelection: false,
            idField: 'TypeofCategoryID',
            textField: 'TypeofIncident',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            closeDropDownOnSelection: false
        };
        this.serverService.getIncidentCategoryList().subscribe((res) => {
            this.incidentCategoriesList = res.data;
            this.selectedIncidentCategory = this.incidentCategoriesList;
        }, (err) => {
            console.error(err);
        });
        this.incidentClassificationDropdownSettings = {
            singleSelection: false,
            idField: 'incidentClassificationID',
            textField: 'incidentClassificationName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true
        };
        this.incidentClassificationList = [
            {
                "incidentClassificationID": _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["IncidentClassificationStatusCodes"].QualityImpacting,
                "incidentClassificationName": _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["IncidentClassificationListValues"].qualityImpacting
            },
            {
                "incidentClassificationID": _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["IncidentClassificationStatusCodes"].QualityNonImpacting,
                "incidentClassificationName": _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["IncidentClassificationListValues"].qualityNonImpacting
            },
            {
                "incidentClassificationID": _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["IncidentClassificationStatusCodes"].Unclassified,
                "incidentClassificationName": _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["IncidentClassificationListValues"].unclassified
            }
        ];
        this.selectedIncidentClassification = this.incidentClassificationList;
    }
    getColorPaletteForStatusChart() {
        return this.dashboardService.statusChartColorPalette;
    }
    getColorOFQE(QualityEventNumber) {
        return this.dashboardService.getColor(QualityEventNumber);
    }
    onPreviousLevelClick() {
        if (this.overallStatuslevel === 1) {
            this.overallStatuslevel--;
            this.getDepartmentsQualityEventsDataFromFilters(this.previousDataFiltersForStackedBarChart);
            this.setSelectedDepartmentsValues(this.previousDataFiltersForStackedBarChart.DeptID);
            this.setSelectedQualityEventValues(this.previousDataFiltersForStackedBarChart.QualityEventType, 'qualityEventsChart');
            this.setSelectedDateValues(this.previousDataFiltersForStackedBarChart.FromDate, this.previousDataFiltersForStackedBarChart.ToDate);
        }
        else if (this.overallStatuslevel == 2) {
            this.overallStatuslevel--;
            this.currentDrillDownHeaderText = this.prevCurrentDrillDownHeaderText;
            this.getFilteredQualityEventStatusChart(this.previousDataFiltersForStatusChart);
            this.setSelectedDepartmentsValues(this.previousDataFiltersForStatusChart.DeptID);
            this.setSelectedQualityEventValues(this.previousDataFiltersForStatusChart.QualityEventType, 'statusChart');
            this.setSelectedDateValues(this.previousDataFiltersForStatusChart.FromDate, this.previousDataFiltersForStatusChart.ToDate);
            this.setChangeCategoryAndClassficationValues(this.previousDataFiltersForStatusChart.CCNCategory, this.previousDataFiltersForStatusChart.CCNClassification);
            this.setIncidentCategoryAndClassificationValues(this.previousDataFiltersForStatusChart.IncidentCategory, this.previousDataFiltersForStatusChart.IncidentClassification);
        }
    }
    setSelectedDepartmentsValues(deptIds) {
        this.selectedDepartments = [];
        for (let i = 0; i < deptIds.length; i++) {
            this.selectedDepartments.push({
                Department_ID: deptIds[i],
                DepartmentName: this.getDeptNameFromDeptID(deptIds[i])
            });
        }
    }
    setSelectedQualityEventValues(qualityEventIds, source) {
        this.selectedQualityEvents = [];
        if (source === 'qualityEventsChart') {
            for (let i = 0; i < qualityEventIds.length; i++) {
                this.selectedQualityEvents.push({
                    QualityEvent_TypeID: qualityEventIds[i],
                    QualityEvent_TypeName: this.getQualityEventNameFromQualityEventId(qualityEventIds[i])
                });
            }
        }
        else if (source === 'statusChart') {
            this.selectedQualityEvents.push({
                QualityEvent_TypeID: qualityEventIds,
                QualityEvent_TypeName: this.getQualityEventNameFromQualityEventId(qualityEventIds)
            });
        }
    }
    setSelectedDateValues(fromDate, toDate) {
        this.fromDateValue = fromDate;
        this.toDateValue = toDate;
    }
    setChangeCategoryAndClassficationValues(changeCategoryId, changeClassificationId) {
        this.selectedChangeCategory = [];
        this.selectedChangeClassification = [];
        if (changeCategoryId === 0) {
            for (let i = 0; i < this.changeCategoriesList.length; i++) {
                this.selectedChangeCategory.push({
                    changeCategoryID: this.changeCategoriesList[i].changeCategoryID,
                    changeCategoryName: this.changeCategoriesList[i].changeCategoryName
                });
            }
        }
        else if (changeCategoryId !== 0) {
            this.selectedChangeCategory.push({
                changeCategoryID: changeCategoryId,
                changeCategoryName: this.getChangeCatNameFromChangeCatID(changeCategoryId)
            });
        }
        if (changeClassificationId === 0) {
            for (let i = 0; i < this.changeClassificationList.length; i++) {
                this.selectedChangeClassification.push({
                    changeClassificationID: this.changeClassificationList[i].changeClassificationID,
                    changeClassificationName: this.changeClassificationList[i].changeClassificationName
                });
            }
        }
        else if (changeClassificationId !== 0) {
            this.selectedChangeClassification.push({
                changeClassificationID: changeClassificationId,
                changeClassificationName: this.getChangeClassificationNameFromChangeClassificationID(changeClassificationId)
            });
        }
    }
    setIncidentCategoryAndClassificationValues(incidentCategoryId, incidentClassificationId) {
        this.selectedIncidentCategory = [];
        this.selectedIncidentClassification = [];
        if (incidentCategoryId === 0) {
            for (let i = 0; i < this.incidentCategoriesList.length; i++) {
                this.selectedIncidentCategory.push({
                    TypeofCategoryID: this.incidentCategoriesList[i].TypeofCategoryID,
                    TypeofIncident: this.incidentCategoriesList[i].TypeofIncident
                });
            }
        }
        else if (incidentCategoryId !== 0) {
            this.selectedIncidentCategory.push({
                TypeofCategoryID: incidentCategoryId,
                TypeofIncident: this.getIncidentCatNameFromIncidentCatID(incidentCategoryId)
            });
        }
        if (incidentClassificationId === 0) {
            for (let i = 0; i < this.incidentClassificationList.length; i++) {
                this.selectedIncidentClassification.push({
                    incidentClassificationID: this.incidentClassificationList[i].incidentClassificationID,
                    incidentClassificationName: this.incidentClassificationList[i].incidentClassificationName
                });
            }
        }
        else if (incidentClassificationId !== 0) {
            this.selectedIncidentClassification.push({
                incidentClassificationID: incidentClassificationId,
                incidentClassificationName: this.getIncidentClassificationNameFromIncidentClassificationID(incidentClassificationId)
            });
        }
    }
    getChangeCatNameFromChangeCatID(changeCategoryId) {
        for (let i = 0; i < this.changeCategoriesList.length; i++) {
            if (this.changeCategoriesList[i].changeCategoryID === changeCategoryId) {
                return this.changeCategoriesList[i].changeCategoryName;
            }
        }
    }
    getChangeClassificationNameFromChangeClassificationID(changeClassificationId) {
        for (let i = 0; i < this.changeClassificationList.length; i++) {
            if (this.changeClassificationList[i].changeClassificationID === changeClassificationId) {
                return this.changeClassificationList[i].changeClassificationName;
            }
        }
    }
    getIncidentCatNameFromIncidentCatID(incidentCategoryId) {
        for (let i = 0; i < this.incidentCategoriesList.length; i++) {
            if (this.incidentCategoriesList[i].TypeofCategoryID === incidentCategoryId) {
                return this.incidentCategoriesList[i].TypeofIncident;
            }
        }
    }
    getIncidentClassificationNameFromIncidentClassificationID(incidentclassificationId) {
        for (let i = 0; i < this.incidentClassificationList.length; i++) {
            if (this.incidentClassificationList[i].incidentClassificationID === incidentclassificationId) {
                return this.incidentClassificationList[i].incidentClassificationName;
            }
        }
    }
    //Extracting Department IDs from departmentsList Object array
    getDepartmentListIds() {
        let departmentIds = [];
        for (let i = 0; i < this.departmentsList.length; i++) {
            departmentIds.push(this.departmentsList[i].Department_ID);
        }
        return departmentIds;
    }
    //Extracting Quality Event IDs from qualityEventsList Object array
    getQualityEventIds() {
        let qualityEventIds = [];
        for (let i = 0; i < this.qualityEventsList.length; i++) {
            qualityEventIds.push(this.qualityEventsList[i].QualityEvent_TypeID);
        }
        return qualityEventIds;
    }
    //fetches chart data based on filters passed
    getDepartmentsQualityEventsDataFromFilters(allDepartmentQualityEventsFilters) {
        this.serverService.getDepartmentsQualityEventsData(allDepartmentQualityEventsFilters).subscribe((res) => {
            this.dataSource = res.Mchart_bind;
            for (let i = 0; i < this.dataSource.length; i++) {
                this.dataSource[i].DepartmentName = _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["TrimmedDepartmentNames"][this.dataSource[i].DepartmentName];
            }
            this.dataSource = this.dataSource.sort(function (x, y) {
                if (x.DepartmentName < y.DepartmentName)
                    return -1;
                if (x.DepartmentName > y.DepartmentName)
                    return 1;
                return 0;
            });
            // this.dataSource=this.dataSource;
        }, (err) => {
            console.error(err);
        });
    }
    //fetches departmentsList from DB and sorts it alphabetically
    getDepartmentsList() {
        // let roleAndEventID={
        //   roleID:2,
        //   eventId:3
        // }
        this.serverService.getAllDepartmentsList().subscribe((resultantDepartments) => {
            this.departmentsList = resultantDepartments.DeptFevent_list;
            this.departmentsList = this.departmentsList.sort(function (x, y) {
                if (x.DepartmentName < y.DepartmentName)
                    return -1;
                if (x.DepartmentName > y.DepartmentName)
                    return 1;
                return 0;
            });
            this.selectedDepartments = this.departmentsList;
            console.log("dept names", this.departmentsList);
            this.previousDataFiltersForStackedBarChart.DeptID = this.getDepartmentListIds();
        }, (err) => {
            console.error(err);
        });
    }
    //fetches qualityEventsList from DB and sorts it alphabetically
    getQualityEventsList() {
        this.serverService.getQualityEventsList().subscribe((resultantQualityEvents) => {
            this.qualityEventsList = resultantQualityEvents.sort(function (x, y) {
                if (x.QualityEvent_TypeName < y.QualityEvent_TypeName)
                    return -1;
                if (x.QualityEvent_TypeName > y.QualityEvent_TypeName)
                    return 1;
                return 0;
            });
            this.selectedQualityEvents = this.qualityEventsList;
            this.previousDataFiltersForStackedBarChart.QualityEventType = this.getQualityEventIds();
        }, (err) => {
            console.error(err);
        });
    }
    getDeptNameFromDeptID(deptId) {
        for (let i = 0; i < this.departmentsList.length; i++) {
            if (this.departmentsList[i].Department_ID === deptId) {
                return this.departmentsList[i].DepartmentName;
            }
        }
    }
    getQualityEventNameFromQualityEventId(qualityEventID) {
        for (let i = 0; i < this.qualityEventsList.length; i++) {
            if (this.qualityEventsList[i].QualityEvent_TypeID === qualityEventID) {
                return this.qualityEventsList[i].QualityEvent_TypeName;
            }
        }
    }
    setFromAndToDates() {
        this.selectedFromDate = this.filtersForm.get('fromDate').value[0];
        this.selectedToDate = this.filtersForm.get('toDate').value[0];
    }
    closeFromDate(otherArg, selectedDates, dateStr, instance) {
        console.log("selected dates", selectedDates);
        console.log("date str", dateStr);
        console.log("instance", instance);
        console.log("other arg", otherArg);
        otherArg.set('minDate', '01-Jul-2021');
        // this.toDateOptions.minDate='01-Jul-2021';
        // console.log('modified to date options',this.toDateOptions);
    }
    // openTodate(selectedDates, dateStr, instance){
    //   console.log("selected dates",selectedDates);
    //   console.log("to date str",dateStr);
    //   console.log("to date instance",instance);
    //   // instance.set('minDate','01-Jul-2021');
    //   // console.log("instance modified",instance);
    // }
    isDatesValid(fromDate, toDate) {
        // let requiredFormatOfFromDate:string = this.getRequiredDateFormat(fromDate);
        // this.toDateOptions.minDate='01-Jul-2021';
        // console.log('min to date',this.toDateOptions.minDate);
        // let requiredFormatOfToDate=this.getRequiredDateFormat(toDate);
        // return moment(requiredFormatOfFromDate).isBefore(requiredFormatOfToDate);
    }
    getSelectedDeptIDs() {
        this.selectedDeptIds = [];
        for (let i = 0; i < this.selectedDepartments.length; i++) {
            this.selectedDeptIds.push(this.selectedDepartments[i].Department_ID);
        }
    }
    getSelectedQualityEventIDs() {
        this.selectedQualityEventIds = [];
        for (let i = 0; i < this.selectedQualityEvents.length; i++) {
            this.selectedQualityEventIds.push(this.selectedQualityEvents[i].QualityEvent_TypeID);
        }
    }
    getSelectedCCNClassificationIDs() {
        this.selectedCCNClassificationIDs = [];
        for (let i = 0; i < this.selectedChangeClassification.length; i++) {
            this.selectedCCNClassificationIDs.push(this.selectedChangeClassification[i].changeClassificationID);
        }
    }
    //The following method executes on every filter change
    onDeptQualityEventsDataChartFilterChange(item) {
        this.dataSource = [];
        this.setFromAndToDates();
        this.isDatesValid(this.selectedFromDate, this.selectedToDate);
        if (this.overallStatuslevel === 0) {
            this.getSelectedDeptIDs();
            this.getSelectedQualityEventIDs();
            this.allDepartmentQualityEventsFilters = {
                DeptID: this.selectedDepartments.length === 0 ? [] : this.selectedDeptIds,
                QualityEventType: this.selectedQualityEvents.length === 0 ? [] : this.selectedQualityEventIds,
                FromDate: this.selectedFromDate !== undefined ? this.getRequiredDateFormat(this.selectedFromDate) : this.initialFromDate,
                ToDate: this.selectedToDate !== undefined ? this.getRequiredDateFormat(this.selectedToDate) : this.initialToDate
            };
            this.previousDataFiltersForStackedBarChart = this.allDepartmentQualityEventsFilters;
            this.subtitleForStackedBarChart = '( ' + this.allDepartmentQualityEventsFilters.FromDate + ' to ' + this.allDepartmentQualityEventsFilters.ToDate + ' )';
            this.getDepartmentsQualityEventsDataFromFilters(this.allDepartmentQualityEventsFilters);
        }
        else if (this.overallStatuslevel === 1) {
            this.getFilteredStatusChartData();
        }
        else if (this.overallStatuslevel === 2) {
            this.getFilteredOverdueData();
        }
    }
    onSelectAllDepartments(items) {
        this.selectedDepartments = items;
        this.onDeptQualityEventsDataChartFilterChange(items);
    }
    onSelectAllQualityEvents(items) {
        this.selectedQualityEvents = items;
        this.onDeptQualityEventsDataChartFilterChange(items);
    }
    onSelectAllChangeCategories(items) {
        this.selectedChangeCategory = items;
        this.getFilteredStatusChartData();
    }
    onSelectAllChangeClassifications(items) {
        this.selectedChangeClassification = items;
        this.getFilteredStatusChartData();
    }
    onSelectAllIncidentCategories(items) {
        this.selectedIncidentCategory = items;
        this.getFilteredStatusChartData();
    }
    onSelectAllIncidentClassification(items) {
        this.selectedIncidentClassification = items;
        this.getFilteredStatusChartData();
    }
    getUntrimmedDeptName(trimmedDeptName) {
        for (let i = 0; i < this.departmentsList.length; i++) {
            if (trimmedDeptName === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["TrimmedDepartmentNames"][this.departmentsList[i].DepartmentName]) {
                return this.departmentsList[i].DepartmentName;
            }
        }
    }
    //on Department's Quality event Stack is clicked on graph
    onDeptQualityEventPointClick(event) {
        this.setFromAndToDates();
        this.selectedDepartments = [];
        let unTrimmedDeptName = this.getUntrimmedDeptName(event.target.originalArgument);
        // event.target.originalArgument = event.target.originalArgument === 'Executive Management'? 'Executive Managements' : event.target.originalArgument;
        this.selectedDepartments.push({
            Department_ID: this.getDeptIdFromDeptName(unTrimmedDeptName),
            DepartmentName: unTrimmedDeptName
        });
        this.selectedQualityEvents = [];
        this.selectedQualityEvents.push({
            QualityEvent_TypeID: this.getQualityEventIdFromQualityEventName(event.target.series.name),
            QualityEvent_TypeName: event.target.series.name
        });
        this.selectedChangeCategory = this.changeCategoriesList;
        this.selectedChangeClassification = this.changeClassificationList;
        this.selectedIncidentCategory = this.incidentCategoriesList;
        this.selectedIncidentClassification = this.incidentClassificationList;
        this.prevCurrentDrillDownHeaderText = this.currentDrillDownHeaderText = event.target.series.name;
        this.selectedQualityEventName = event.target.series.name;
        this.getQualityEventDataOfOneDept(this.selectedDepartments[0].Department_ID, event.target.series.name);
    }
    getQualityEventDataOfOneDept(deptId, qualityEventName) {
        let numToArrConversion = [];
        numToArrConversion.push(deptId);
        this.qualityEventDataOfOneDept = {
            DeptID: numToArrConversion,
            QualityEventType: this.getQualityEventIdFromQualityEventName(qualityEventName),
            CCNClassification: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["CCNClassificationStatusCodes"].All,
            CCNCategory: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["CCNCategoryStatusCodes"].Both,
            IncidentClassification: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["IncidentClassificationStatusCodes"].All,
            IncidentCategory: 0,
            FromDate: this.selectedFromDate !== undefined ? this.getRequiredDateFormat(this.selectedFromDate) : this.initialFromDate,
            ToDate: this.selectedToDate !== undefined ? this.getRequiredDateFormat(this.selectedToDate) : this.initialToDate
        };
        this.selectedQualityEventIdForStatusChart = this.qualityEventDataOfOneDept.QualityEventType;
        this.previousDataFiltersForStatusChart = this.qualityEventDataOfOneDept;
        this.serverService.getQualityEventStatusOfOneDept(this.qualityEventDataOfOneDept).subscribe((res) => {
            this.statusListData = res.QeStatus_List;
            this.statusListData = this.excludeEmptyRecordsOfStatusChart(this.statusListData);
            this.overallStatuslevel++;
        }, (err) => {
            console.error(err);
        });
    }
    excludeEmptyRecordsOfStatusChart(statusListData) {
        let sampleListData = [];
        for (let i = 0; i < statusListData.length; i++) {
            if (statusListData[i].TotalRecords !== 0) {
                sampleListData.push(statusListData[i]);
            }
        }
        return sampleListData;
    }
    getFilteredStatusChartData() {
        if (this.overallStatuslevel === 1) {
            this.getSelectedDeptIDs();
            this.getSelectedCCNClassificationIDs();
            this.filteredQualityEventData = {
                DeptID: this.selectedDeptIds,
                QualityEventType: this.selectedQualityEventIdForStatusChart,
                // CCNClassification:(this.selectedChangeClassification.length===0) ? CCNClassificationStatusCodes.All: this.selectedChangeClassification[0].changeClassificationID,
                CCNClassification: ((this.selectedChangeClassification.length === 0) || (this.selectedChangeClassification.length > 1)) ? _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["CCNClassificationStatusCodes"].All : this.selectedChangeClassification[0].changeClassificationID,
                CCNCategory: (this.selectedChangeCategory.length === 0 || this.selectedChangeCategory.length > 1) ? _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["CCNCategoryStatusCodes"].Both : this.selectedChangeCategory[0].changeCategoryID,
                IncidentClassification: (this.selectedIncidentClassification.length === 0 || this.selectedIncidentClassification.length > 1) ? _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["IncidentClassificationStatusCodes"].All : this.selectedIncidentClassification[0].incidentClassificationID,
                IncidentCategory: 0,
                FromDate: this.selectedFromDate !== undefined ? this.getRequiredDateFormat(this.selectedFromDate) : this.initialFromDate,
                ToDate: this.selectedToDate !== undefined ? this.getRequiredDateFormat(this.selectedToDate) : this.initialToDate
            };
            this.previousDataFiltersForStatusChart = this.filteredQualityEventData;
            this.getFilteredQualityEventStatusChart(this.filteredQualityEventData);
        }
        else if (this.overallStatuslevel === 2) {
            this.getFilteredOverdueData();
        }
    }
    getFilteredQualityEventStatusChart(filteredQualityEventData) {
        this.serverService.getQualityEventStatusOfOneDept(filteredQualityEventData).subscribe((res) => {
            this.statusListData = res.QeStatus_List;
            this.statusListData = this.excludeEmptyRecordsOfStatusChart(this.statusListData);
        }, (err) => {
            console.error(err);
        });
    }
    onStatusChartPointClick(event) {
        if (event.target.argument === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventStatus"].OverDue) {
            this.setFromAndToDates();
            this.getSelectedDeptIDs();
            this.currentDrillDownHeaderText = event.target.argument + ' - ' + this.selectedQualityEventName;
            this.overallStatuslevel++;
            this.getFilteredOverdueData();
        }
        else if (this.selectedQualityEventIdForStatusChart === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventIDs"].ChangeControlNote) {
            this.getCCNStatusListOfAppliedFilters(event.target.argument);
        }
        else if (this.selectedQualityEventIdForStatusChart === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventIDs"].IncidentReport) {
            this.getIncidentStatusListOfAppliedFilters(event.target.argument);
        }
        else if (this.selectedQualityEventIdForStatusChart === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventIDs"].Errata) {
            this.getErrataStatusListOfAppliedFilters(event.target.argument);
        }
        else {
            this.getNTFStatusListOfAppliedFilters(event.target.argument);
        }
    }
    getFilteredOverdueData() {
        this.getSelectedDeptIDs();
        this.overdueDataFilters = {
            DeptID: this.selectedDeptIds,
            QualityEventType: this.selectedQualityEventIdForStatusChart,
            CCNClassification: ((this.selectedChangeClassification.length === 0) || (this.selectedChangeClassification.length > 1)) ? _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["CCNClassificationStatusCodes"].All : this.selectedChangeClassification[0].changeClassificationID,
            CCNCategory: (this.selectedChangeCategory.length === 0 || this.selectedChangeCategory.length > 1) ? _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["CCNCategoryStatusCodes"].Both : this.selectedChangeCategory[0].changeCategoryID,
            IncidentClassification: (this.selectedIncidentClassification.length === 0 || this.selectedIncidentClassification.length > 1) ? _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["IncidentClassificationStatusCodes"].All : this.selectedIncidentClassification[0].incidentClassificationID,
            IncidentCategory: 0,
            FromDate: this.selectedFromDate !== undefined ? this.getRequiredDateFormat(this.selectedFromDate) : this.initialFromDate,
            ToDate: this.selectedToDate !== undefined ? this.getRequiredDateFormat(this.selectedToDate) : this.initialToDate
        };
        this.serverService.getOverdueData(this.overdueDataFilters).subscribe((res) => {
            this.overdueData = this.dashboardService.getModifiedOverdueData(res.Overdue_List);
        }, (err) => {
            console.error(err);
        });
    }
    onStackedBarChartExporting(event) {
        event.fileName = "Total Quality Events of Depts " + this.subtitleForStackedBarChart;
    }
    customizeLabel(arg) {
        return arg.argumentText + ' : ' + arg.valueText;
    }
    customizeTooltip(arg) {
        return {
            text: arg.seriesName + ' : ' + arg.valueText
        };
    }
    onOverdueStatusChartPointClick(event) {
        if (this.selectedQualityEventIdForStatusChart === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventIDs"].ChangeControlNote) {
            this.getCCNStatusListOfAppliedFilters(event.target.argument);
        }
        else if (this.selectedQualityEventIdForStatusChart === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventIDs"].IncidentReport) {
            this.getIncidentStatusListOfAppliedFilters(event.target.argument);
        }
    }
    onShowOverallListView() {
        if (this.selectedQualityEventIdForStatusChart === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventIDs"].ChangeControlNote) {
            this.getOverallCCNListOfAppliedFilters();
        }
        else if (this.selectedQualityEventIdForStatusChart === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventIDs"].IncidentReport) {
            this.getOverallIncidentListOfAppliedFilters();
        }
        else if (this.selectedQualityEventIdForStatusChart === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventIDs"].Errata) {
            this.getOverallErrataListOfAppliedFilters();
        }
        else {
            this.getOverallNTFListOfAppliedFilters();
        }
    }
    getOverallCCNListOfAppliedFilters() {
        if (this.overallStatuslevel !== 2) {
            this.ccnListFilters = {
                DeptID: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.DeptID : this.filteredQualityEventData.DeptID,
                CCNClassification: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.CCNClassification : this.filteredQualityEventData.CCNClassification,
                CCNCategory: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.CCNCategory : this.filteredQualityEventData.CCNCategory,
                FromDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.FromDate : this.filteredQualityEventData.FromDate,
                ToDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.ToDate : this.filteredQualityEventData.ToDate,
                showtype: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["ShowTypeList"].OverallList
            };
        }
        else {
            this.ccnListFilters = {
                DeptID: this.overdueDataFilters.DeptID,
                CCNClassification: this.overdueDataFilters.CCNClassification,
                CCNCategory: this.overdueDataFilters.CCNCategory,
                FromDate: this.overdueDataFilters.FromDate,
                ToDate: this.overdueDataFilters.ToDate,
                showtype: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["ShowTypeList"].OverallOverdueList
            };
        }
        this.serverService.getCCNList(this.ccnListFilters).subscribe((res) => {
            this.CCNRecords = res.CCN_List;
            const initialState = {
                currentDrillDownHeaderText: this.currentDrillDownHeaderText + ' - ' + 'Overall List',
                ccnRecords: this.CCNRecords,
                qualityEvent: this.getQualityEventNameFromQualityEventId(this.selectedQualityEventIdForStatusChart),
                fileName: ' - ' + 'Overall List' + ' (' + this.ccnListFilters.FromDate + ' to ' + this.ccnListFilters.ToDate + ')',
                showType: this.ccnListFilters.showtype
            };
            this.bsModalRef = this.modalService.show(_list_view_list_view_component__WEBPACK_IMPORTED_MODULE_5__["ListViewComponent"], { initialState, class: 'gray modal-xl', backdrop: 'static', });
        }, (err) => {
            console.error(err);
        });
    }
    getShowTypeListID(statusName) {
        if (this.overallStatuslevel === 2) {
            if (statusName === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventStatus"].OverdueLessThanOneWk) {
                return _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["ShowTypeList"].OverdueLessThanOneWk;
            }
            else if (statusName === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventStatus"].OverdueOneWkToOneMn) {
                return _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["ShowTypeList"].OverdueOneWkToOneMn;
            }
            else if (statusName === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventStatus"].OverdueOneMnToThreeMn) {
                return _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["ShowTypeList"].OverdueOneMnToThreeMn;
            }
            else if (statusName === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventStatus"].OverdueThreeMnToSixMn) {
                return _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["ShowTypeList"].OverdueThreeMnToSixMn;
            }
            else if (statusName === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventStatus"].OverdueGreaterThanSixMn) {
                return _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["ShowTypeList"].OverdueGreaterThanSixMn;
            }
        }
        else {
            return _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["ShowTypeList"][statusName];
        }
    }
    getCCNStatusListOfAppliedFilters(CCNStatusName) {
        if (this.overallStatuslevel !== 2) {
            this.ccnListFilters = {
                DeptID: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.DeptID : this.filteredQualityEventData.DeptID,
                CCNClassification: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.CCNClassification : this.filteredQualityEventData.CCNClassification,
                CCNCategory: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.CCNCategory : this.filteredQualityEventData.CCNCategory,
                FromDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.FromDate : this.filteredQualityEventData.FromDate,
                ToDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.ToDate : this.filteredQualityEventData.ToDate,
                showtype: this.getShowTypeListID(CCNStatusName)
            };
        }
        else {
            this.ccnListFilters = {
                DeptID: this.overdueDataFilters.DeptID,
                CCNClassification: this.overdueDataFilters.CCNClassification,
                CCNCategory: this.overdueDataFilters.CCNCategory,
                FromDate: this.overdueDataFilters.FromDate,
                ToDate: this.overdueDataFilters.ToDate,
                showtype: this.getShowTypeListID(CCNStatusName)
            };
        }
        this.serverService.getCCNList(this.ccnListFilters).subscribe((res) => {
            this.CCNRecords = res.CCN_List;
            const initialState = {
                currentDrillDownHeaderText: this.currentDrillDownHeaderText + ' - ' + CCNStatusName,
                ccnRecords: this.CCNRecords,
                qualityEvent: this.getQualityEventNameFromQualityEventId(this.selectedQualityEventIdForStatusChart),
                fileName: ' - ' + CCNStatusName + ' (' + this.ccnListFilters.FromDate + ' to ' + this.ccnListFilters.ToDate + ')',
                showType: this.ccnListFilters.showtype
            };
            this.bsModalRef = this.modalService.show(_list_view_list_view_component__WEBPACK_IMPORTED_MODULE_5__["ListViewComponent"], { initialState, class: 'gray modal-xl', backdrop: 'static', });
        }, (err) => {
            console.error(err);
        });
    }
    getOverallIncidentListOfAppliedFilters() {
        if (this.overallStatuslevel !== 2) {
            this.incidentListFilters = {
                DeptID: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.DeptID : this.filteredQualityEventData.DeptID,
                IncidentClassification: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.IncidentClassification : this.filteredQualityEventData.IncidentClassification,
                IncidentCategory: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.IncidentCategory : this.filteredQualityEventData.IncidentCategory,
                FromDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.FromDate : this.filteredQualityEventData.FromDate,
                ToDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.ToDate : this.filteredQualityEventData.ToDate,
                showtype: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["ShowTypeList"].OverallList
            };
        }
        else {
            this.incidentListFilters = {
                DeptID: this.overdueDataFilters.DeptID,
                IncidentClassification: this.overdueDataFilters.IncidentClassification,
                IncidentCategory: this.overdueDataFilters.IncidentCategory,
                FromDate: this.overdueDataFilters.FromDate,
                ToDate: this.overdueDataFilters.ToDate,
                showtype: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["ShowTypeList"].OverallOverdueList
            };
        }
        this.serverService.getIncidentList(this.incidentListFilters).subscribe((res) => {
            this.incidentRecords = res.Incident_List;
            const initialState = {
                currentDrillDownHeaderText: this.currentDrillDownHeaderText + ' - ' + 'Overall List',
                incidentRecords: this.incidentRecords,
                qualityEvent: this.getQualityEventNameFromQualityEventId(this.selectedQualityEventIdForStatusChart),
                fileName: ' - ' + 'Overall List' + ' (' + this.incidentListFilters.FromDate + ' to ' + this.incidentListFilters.ToDate + ')',
                showType: this.incidentListFilters.showtype
            };
            this.bsModalRef = this.modalService.show(_list_view_list_view_component__WEBPACK_IMPORTED_MODULE_5__["ListViewComponent"], { initialState, class: 'gray modal-xl', backdrop: 'static', });
        }, (err) => {
            console.error(err);
        });
    }
    getIncidentStatusListOfAppliedFilters(incidentStatusName) {
        if (this.overallStatuslevel !== 2) {
            this.incidentListFilters = {
                DeptID: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.DeptID : this.filteredQualityEventData.DeptID,
                IncidentClassification: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.IncidentClassification : this.filteredQualityEventData.IncidentClassification,
                IncidentCategory: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.IncidentCategory : this.filteredQualityEventData.IncidentCategory,
                FromDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.FromDate : this.filteredQualityEventData.FromDate,
                ToDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.ToDate : this.filteredQualityEventData.ToDate,
                showtype: this.getShowTypeListID(incidentStatusName)
            };
        }
        else {
            this.incidentListFilters = {
                DeptID: this.overdueDataFilters.DeptID,
                IncidentClassification: this.overdueDataFilters.IncidentClassification,
                IncidentCategory: this.overdueDataFilters.IncidentCategory,
                FromDate: this.overdueDataFilters.FromDate,
                ToDate: this.overdueDataFilters.ToDate,
                showtype: this.getShowTypeListID(incidentStatusName)
            };
        }
        this.serverService.getIncidentList(this.incidentListFilters).subscribe((res) => {
            this.incidentRecords = res.Incident_List;
            const initialState = {
                currentDrillDownHeaderText: this.currentDrillDownHeaderText + ' - ' + incidentStatusName,
                incidentRecords: this.incidentRecords,
                qualityEvent: this.getQualityEventNameFromQualityEventId(this.selectedQualityEventIdForStatusChart),
                fileName: ' - ' + incidentStatusName + ' (' + this.incidentListFilters.FromDate + ' to ' + this.incidentListFilters.ToDate + ')',
                showType: this.incidentListFilters.showtype
            };
            this.bsModalRef = this.modalService.show(_list_view_list_view_component__WEBPACK_IMPORTED_MODULE_5__["ListViewComponent"], { initialState, class: 'gray modal-xl', backdrop: 'static', });
        }, (err) => {
            console.error(err);
        });
    }
    getOverallErrataListOfAppliedFilters() {
        this.errataListFilters = {
            DeptID: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.DeptID : this.filteredQualityEventData.DeptID,
            FromDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.FromDate : this.filteredQualityEventData.FromDate,
            ToDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.ToDate : this.filteredQualityEventData.ToDate,
            showtype: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["ShowTypeList"].OverallList
        };
        this.serverService.getErrataList(this.errataListFilters).subscribe((res) => {
            this.errrataRecords = res.Errata_List;
            const initialState = {
                currentDrillDownHeaderText: this.currentDrillDownHeaderText + ' - ' + 'Overall List',
                errataRecords: this.errrataRecords,
                qualityEvent: this.getQualityEventNameFromQualityEventId(this.selectedQualityEventIdForStatusChart),
                fileName: ' - ' + 'Overall List' + ' (' + this.errataListFilters.FromDate + ' to ' + this.errataListFilters.ToDate + ')',
                showType: this.errataListFilters.showtype
            };
            this.bsModalRef = this.modalService.show(_list_view_list_view_component__WEBPACK_IMPORTED_MODULE_5__["ListViewComponent"], { initialState, class: 'gray modal-xl', backdrop: 'static', });
        }, (err) => {
            console.error(err);
        });
    }
    getErrataStatusListOfAppliedFilters(errataStatusName) {
        this.errataListFilters = {
            DeptID: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.DeptID : this.filteredQualityEventData.DeptID,
            FromDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.FromDate : this.filteredQualityEventData.FromDate,
            ToDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.ToDate : this.filteredQualityEventData.ToDate,
            showtype: this.getShowTypeListID(errataStatusName)
        };
        this.serverService.getErrataList(this.errataListFilters).subscribe((res) => {
            this.errrataRecords = res.Errata_List;
            const initialState = {
                currentDrillDownHeaderText: this.currentDrillDownHeaderText + ' - ' + errataStatusName,
                errataRecords: this.errrataRecords,
                qualityEvent: this.getQualityEventNameFromQualityEventId(this.selectedQualityEventIdForStatusChart),
                fileName: ' - ' + errataStatusName + ' (' + this.errataListFilters.FromDate + ' to ' + this.errataListFilters.ToDate + ')',
                showType: this.errataListFilters.showtype
            };
            this.bsModalRef = this.modalService.show(_list_view_list_view_component__WEBPACK_IMPORTED_MODULE_5__["ListViewComponent"], { initialState, class: 'gray modal-xl', backdrop: 'static', });
        }, (err) => {
            console.error(err);
        });
    }
    getOverallNTFListOfAppliedFilters() {
        this.noteToFileListFilters = {
            DeptID: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.DeptID : this.filteredQualityEventData.DeptID,
            FromDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.FromDate : this.filteredQualityEventData.FromDate,
            ToDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.ToDate : this.filteredQualityEventData.ToDate,
            showtype: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["ShowTypeList"].OverallList
        };
        this.serverService.getNTFList(this.noteToFileListFilters).subscribe((res) => {
            this.noteToFileRecords = res.NTF_List;
            const initialState = {
                currentDrillDownHeaderText: this.currentDrillDownHeaderText + ' - ' + 'Overall List',
                noteToFileRecords: this.noteToFileRecords,
                qualityEvent: this.getQualityEventNameFromQualityEventId(this.selectedQualityEventIdForStatusChart),
                fileName: ' - ' + 'Overall List' + ' (' + this.noteToFileListFilters.FromDate + ' to ' + this.noteToFileListFilters.ToDate + ')',
                showType: this.noteToFileListFilters.showtype
            };
            this.bsModalRef = this.modalService.show(_list_view_list_view_component__WEBPACK_IMPORTED_MODULE_5__["ListViewComponent"], { initialState, class: 'gray modal-xl', backdrop: 'static', });
        }, (err) => {
            console.error(err);
        });
    }
    getNTFStatusListOfAppliedFilters(noteToFileStatusName) {
        this.noteToFileListFilters = {
            DeptID: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.DeptID : this.filteredQualityEventData.DeptID,
            FromDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.FromDate : this.filteredQualityEventData.FromDate,
            ToDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.ToDate : this.filteredQualityEventData.ToDate,
            showtype: this.getShowTypeListID(noteToFileStatusName)
        };
        this.serverService.getNTFList(this.noteToFileListFilters).subscribe((res) => {
            this.noteToFileRecords = res.NTF_List;
            const initialState = {
                currentDrillDownHeaderText: this.currentDrillDownHeaderText + ' - ' + noteToFileStatusName,
                noteToFileRecords: this.noteToFileRecords,
                qualityEvent: this.getQualityEventNameFromQualityEventId(this.selectedQualityEventIdForStatusChart),
                fileName: ' - ' + noteToFileStatusName + ' (' + this.noteToFileListFilters.FromDate + ' to ' + this.noteToFileListFilters.ToDate + ')',
                showType: this.noteToFileListFilters.showtype
            };
            this.bsModalRef = this.modalService.show(_list_view_list_view_component__WEBPACK_IMPORTED_MODULE_5__["ListViewComponent"], { initialState, class: 'gray modal-xl', backdrop: 'static', });
        }, (err) => {
            console.error(err);
        });
    }
    onCloseListView() {
        this.bsModalRef.hide();
    }
    // rerender(): void {
    //   this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
    //     // Destroy the table first
    //     dtInstance.destroy();
    //     // Call the dtTrigger to rerender again
    //     this.dtTrigger.next();
    //   });
    // }
    unsubscribe() {
        this.events.forEach((subscription) => {
            subscription.unsubscribe();
        });
        this.events = [];
    }
    ngOnDestroy() {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
    }
}
ManagementLevelChartComponent.ɵfac = function ManagementLevelChartComponent_Factory(t) { return new (t || ManagementLevelChartComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_dashboards_service__WEBPACK_IMPORTED_MODULE_6__["DashboardsService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_server_service__WEBPACK_IMPORTED_MODULE_8__["ServerService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_9__["BsModalService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"])); };
ManagementLevelChartComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ManagementLevelChartComponent, selectors: [["app-management-level-chart"]], viewQuery: function ManagementLevelChartComponent_Query(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](angular_datatables__WEBPACK_IMPORTED_MODULE_3__["DataTableDirective"], true);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, true);
    } if (rf & 2) {
        var _t;
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.datatableElement = _t.first);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.fromDatePickr = _t.first);
    } }, decls: 55, vars: 79, consts: [[1, "row", "mt-3"], [1, "col-md-12"], [1, "col-sm-12", "padding-none", "dashboard_panel_full", "float-left"], [1, "col-lg-12", "col-12", "col-sm-12", "col-md-12", "col-12", "col-sm-12", "col-md-12", "dashboard_panel_header", "float-left"], ["href", "javascript:void(0)", "title", "Back to List", "class", "upload_btn float-left", 3, "click", 4, "ngIf"], [1, "col-lg-12", "col-12", "col-sm-12", "col-md-12", "col-12", "col-sm-12", "col-md-12", "float-left", "top", "bottom"], [1, "mb-2", 3, "formGroup"], [1, "row"], [1, "col-md-6"], [3, "placeholder", "settings", "data", "ngModel", "ngModelOptions", "ngModelChange", "onSelect", "onDeSelect", "onSelectAll"], [3, "placeholder", "settings", "data", "ngModel", "ngModelOptions", "disabled", "ngModelChange", "onSelect", "onDeSelect", "onSelectAll"], ["class", "row mt-2", 4, "ngIf"], [1, "row", "mt-2"], ["placeholder", "From date", "formControlName", "fromDate", 3, "config", "setDate", "change"], ["id", "toDatePickrElement", "formControlName", "toDate", "placeholder", "To date", 3, "config", "setDate", "change"], ["class", "d-flex justify-content-end", 4, "ngIf"], ["id", "chart", 3, "dataSource", "onPointClick", "onExporting"], ["valueAxis", "none", "argumentAxis", "both", "panKey", "shift", 3, "dragToZoom", "allowMouseWheel"], ["position", "top", 3, "visible", "width"], [3, "height"], ["text", "Quality Events"], [3, "text"], ["valueField", "TotalRecordCCN", "name", "Change Control Note", 3, "color"], ["valueField", "TotalRecordIncident", "name", "Incident Report", 3, "color"], ["valueField", "TotalRecordsErrata", "name", "Errata", 3, "color"], ["valueField", "TotalRecordNTF", "name", "Note To File", 3, "color"], ["position", "left", 3, "allowDecimals"], ["text", "Total Count of QE"], ["argumentField", "DepartmentName", "type", "stackedBar", "barWidth", "50"], ["verticalAlignment", "bottom", "horizontalAlignment", "center", "itemTextPosition", "right"], [3, "enabled", "margin"], ["location", "edge", "icon", "export", 3, "enabled", "customizeTooltip"], ["id", "pie", "resolveLabelOverlapping", "shift", 3, "title", "palette", "dataSource", "onPointClick"], ["orientation", "horizontal", "itemTextPosition", "right", "horizontalAlignment", "center", "verticalAlignment", "bottom", 3, "visible", "columnCount"], ["argumentField", "EventName", "valueField", "TotalRecords"], ["position", "outside", "position", "columns", 3, "visible", "customizeText"], [3, "size"], [3, "visible", "width"], ["id", "overdueChart", "palette", "soft", 3, "dataSource", "valueAxis", "onPointClick"], ["type", "bar", "valueField", "TotalRecords", "axis", "frequency", "name", "Total Overdues", "color", "#f9a825"], ["type", "spline", "valueField", "cumulativePercent", "axis", "percentage", "name", "Cumulative percentage", "color", "#178b7e"], ["overlappingBehavior", "stagger"], [3, "enabled", "shared", "customizeTooltip"], ["argumentField", "EventName", "type", "bar", 3, "barPadding"], ["verticalAlignment", "bottom", "horizontalAlignment", "center"], ["href", "javascript:void(0)", "title", "Back to List", 1, "upload_btn", "float-left", 3, "click"], ["src", "/Images/PMS/market_backicon.png"], ["class", "col-md-6", 4, "ngIf"], [3, "settings", "data", "ngModel", "ngModelOptions", "ngModelChange", "onSelect", "onDeSelect", "onSelectAll"], [1, "d-flex", "justify-content-end"], ["type", "button", 1, "btn", "btn-link", 3, "click"]], template: function ManagementLevelChartComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Quality Events ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, ManagementLevelChartComponent_a_5_Template, 2, 0, "a", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "form", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "ng-multiselect-dropdown", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function ManagementLevelChartComponent_Template_ng_multiselect_dropdown_ngModelChange_10_listener($event) { return ctx.selectedDepartments = $event; })("onSelect", function ManagementLevelChartComponent_Template_ng_multiselect_dropdown_onSelect_10_listener($event) { return ctx.onDeptQualityEventsDataChartFilterChange($event); })("onDeSelect", function ManagementLevelChartComponent_Template_ng_multiselect_dropdown_onDeSelect_10_listener($event) { return ctx.onDeptQualityEventsDataChartFilterChange($event); })("onSelectAll", function ManagementLevelChartComponent_Template_ng_multiselect_dropdown_onSelectAll_10_listener($event) { return ctx.onSelectAllDepartments($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "ng-multiselect-dropdown", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function ManagementLevelChartComponent_Template_ng_multiselect_dropdown_ngModelChange_12_listener($event) { return ctx.selectedQualityEvents = $event; })("onSelect", function ManagementLevelChartComponent_Template_ng_multiselect_dropdown_onSelect_12_listener($event) { return ctx.onDeptQualityEventsDataChartFilterChange($event); })("onDeSelect", function ManagementLevelChartComponent_Template_ng_multiselect_dropdown_onDeSelect_12_listener($event) { return ctx.onDeptQualityEventsDataChartFilterChange($event); })("onSelectAll", function ManagementLevelChartComponent_Template_ng_multiselect_dropdown_onSelectAll_12_listener($event) { return ctx.onSelectAllQualityEvents($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, ManagementLevelChartComponent_div_13_Template, 5, 4, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "ng2-flatpickr", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function ManagementLevelChartComponent_Template_ng2_flatpickr_change_16_listener($event) { return ctx.onDeptQualityEventsDataChartFilterChange($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "ng2-flatpickr", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function ManagementLevelChartComponent_Template_ng2_flatpickr_change_18_listener($event) { return ctx.onDeptQualityEventsDataChartFilterChange($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](19, ManagementLevelChartComponent_div_19_Template, 3, 0, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "dx-chart", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("onPointClick", function ManagementLevelChartComponent_Template_dx_chart_onPointClick_20_listener($event) { return ctx.onDeptQualityEventPointClick($event); })("onExporting", function ManagementLevelChartComponent_Template_dx_chart_onExporting_20_listener($event) { return ctx.onStackedBarChartExporting($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "dxo-zoom-and-pan", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "dxo-scroll-bar", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "dxo-size", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "dxo-title", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "dxo-subtitle", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "dxi-series", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "dxi-series", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "dxi-series", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "dxi-series", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "dxi-value-axis", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "dxo-title", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "dxo-argument-axis");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "dxo-common-series-settings", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "dxo-legend", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "dxo-export", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "dxo-tooltip", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "dx-pie-chart", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("onPointClick", function ManagementLevelChartComponent_Template_dx_pie_chart_onPointClick_37_listener($event) { return ctx.onStatusChartPointClick($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "dxo-size", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "dxo-legend", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "dxo-export", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "dxi-series", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "dxo-label", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](43, "dxo-font", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](44, "dxo-connector", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "dx-chart", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("onPointClick", function ManagementLevelChartComponent_Template_dx_chart_onPointClick_45_listener($event) { return ctx.onOverdueStatusChartPointClick($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](46, "dxi-series", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "dxi-series", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "dxo-argument-axis");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](49, "dxo-label", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](50, "dxo-tooltip", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "dxo-common-series-settings", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](52, "dxo-legend", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](53, "dxo-export", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](54, "dxo-title", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.overallStatuslevel !== 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.filtersForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", "Select Department")("settings", ctx.departmentDropdownSettings)("data", ctx.departmentsList)("ngModel", ctx.selectedDepartments)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](63, _c1));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", "Select Quality Event")("settings", ctx.eventsDropdownSettings)("data", ctx.qualityEventsList)("ngModel", ctx.selectedQualityEvents)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](64, _c1))("disabled", ctx.overallStatuslevel !== 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.overallStatuslevel !== 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("config", ctx.fromDateOptions)("setDate", ctx.fromDateValue);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("config", ctx.toDateOptions)("setDate", ctx.toDateValue);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.overallStatuslevel !== 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](ctx.overallStatuslevel === 0 ? "" : "hideChart");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("dataSource", ctx.dataSource);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("dragToZoom", false)("allowMouseWheel", true);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", true)("width", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("height", 500);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("text", ctx.subtitleForStackedBarChart);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("color", ctx.getColorOFQE(1));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("color", ctx.getColorOFQE(3));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("color", ctx.getColorOFQE(5));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("color", ctx.getColorOFQE(7));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("allowDecimals", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("enabled", true)("margin", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("enabled", true)("customizeTooltip", ctx.customizeTooltip);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](ctx.overallStatuslevel === 1 ? "" : "hideChart");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("title", ctx.currentDrillDownHeaderText);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("palette", ctx.getColorPaletteForStatusChart())("dataSource", ctx.statusListData);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("height", 450);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", false)("columnCount", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("enabled", true)("margin", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", true)("customizeText", ctx.customizeLabel);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("size", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", true)("width", 0.5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](ctx.overallStatuslevel === 2 ? "" : "hideChart");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("dataSource", ctx.overdueData)("valueAxis", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](76, _c8, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](65, _c2), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](73, _c7, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](66, _c3, ctx.customizeLabelText), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](71, _c6, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](69, _c5, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](68, _c4))))));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("enabled", true)("shared", true)("customizeTooltip", ctx.customizeTooltipForOverdue);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("barPadding", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("enabled", true)("margin", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("text", ctx.currentDrillDownHeaderText);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_10__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormGroupDirective"], ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_11__["MultiSelectComponent"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["NgModel"], ng2_flatpickr__WEBPACK_IMPORTED_MODULE_12__["Ng2FlatpickrComponent"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormControlName"], devextreme_angular__WEBPACK_IMPORTED_MODULE_13__["DxChartComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_14__["DxoZoomAndPanComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_14__["DxoScrollBarComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_14__["DxoSizeComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_14__["DxoTitleComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_14__["DxoSubtitleComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_14__["DxiSeriesComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_14__["DxiValueAxisComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_14__["DxoArgumentAxisComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_14__["DxoCommonSeriesSettingsComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_14__["DxoLegendComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_14__["DxoExportComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_14__["DxoTooltipComponent"], devextreme_angular__WEBPACK_IMPORTED_MODULE_13__["DxPieChartComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_14__["DxoLabelComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_14__["DxoFontComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_14__["DxoConnectorComponent"]], styles: [".hideChart[_ngcontent-%COMP%] {\n  display: none;\n}\n\n.button-container[_ngcontent-%COMP%] {\n  text-align: center;\n  height: 40px;\n  position: absolute;\n  top: 7px;\n  left: 0px;\n}\n\n  .ng2-flatpickr-input-container .form-control[readonly]:not([disabled]) {\n  background-color: transparent !important;\n}\n\n  .tooltip-header {\n  margin-bottom: 5px;\n  font-size: 16px;\n  font-weight: 500;\n  padding-bottom: 5px;\n  border-bottom: 1px solid #c5c5c5;\n}\n\n  .tooltip-body {\n  width: 170px;\n}\n\n  .tooltip-body .series-name {\n  font-weight: normal;\n  opacity: 0.6;\n  display: inline-block;\n  line-height: 1.5;\n  padding-right: 10px;\n  width: 126px;\n}\n\n  .tooltip-body .value-text {\n  display: inline-block;\n  line-height: 1.5;\n  width: 30px;\n}\n\n  .dxc-val-title text {\n  font-size: 10pt !important;\n}\n\n  .multiselect-dropdown .dropdown-btn .selected-item {\n  font-size: 10pt !important;\n  margin-top: 4px !important;\n}\n\n  .dxc-title text {\n  font-family: seguisb !important;\n  font-size: 13pt !important;\n}\n\n.upload_btn[_ngcontent-%COMP%] {\n  border: 1px solid #094353;\n  border-radius: 4px;\n  background-color: #fff;\n}\n\n.upload_btn[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  padding: 5px;\n}\n\n.close[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  color: black;\n}\n\n.table[_ngcontent-%COMP%]   th[_ngcontent-%COMP%], .table[_ngcontent-%COMP%]   td[_ngcontent-%COMP%] {\n  padding: 10px;\n  vertical-align: top;\n  border-top: 1px solid #dee2e6;\n  font-size: 10pt !important;\n  text-align: center;\n  font-weight: normal;\n}\n\n.datatable_cust[_ngcontent-%COMP%]   tfoot[_ngcontent-%COMP%]   td[_ngcontent-%COMP%] {\n  border-right: 1px solid #c3c3c3;\n  border-bottom: 1px solid #c3c3c3;\n  background: #38b8ba;\n  color: #fff;\n  font-family: seguisb;\n  text-align: center;\n  font-size: 10pt;\n}\n\n.datatable_cust[_ngcontent-%COMP%]   tfoot[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\n  width: 100%;\n  font-size: 10pt !important;\n  padding: 2px 10px;\n  border-radius: 4px;\n  background: transparent;\n  color: #fff;\n  font-family: seguisb;\n  border: 1px solid #fff !important;\n}\n\ndiv.dataTables_wrapper[_ngcontent-%COMP%]   div.dataTables_paginate[_ngcontent-%COMP%]   ul.pagination[_ngcontent-%COMP%] {\n  margin: 2px 0;\n  white-space: nowrap;\n  justify-content: flex-end;\n  font-size: 10pt;\n}\n\n  .page-item.active .page-link {\n  z-index: 1;\n  color: #fff !important;\n  background-color: #38b8ba !important;\n  border-color: #38b8ba !important;\n}\n\n[_ngcontent-%COMP%]:ng-deep   button.dt-button[_ngcontent-%COMP%] {\n  background-color: transparent !important;\n  background: none !important;\n}\n\n  .square {\n  height: 20px;\n  width: 20px;\n  margin-left: auto;\n  margin-right: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkcy9tYW5hZ2VtZW50LWxldmVsLWNoYXJ0L21hbmFnZW1lbnQtbGV2ZWwtY2hhcnQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0FBQ0o7O0FBT0k7RUFDSSx3Q0FBQTtBQUpSOztBQVFBO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdDQUFBO0FBTEo7O0FBUUE7RUFDSSxZQUFBO0FBTEo7O0FBUUE7RUFDSSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0FBTEo7O0FBUUE7RUFDSSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtBQUxKOztBQVNJO0VBQ0ksMEJBQUE7QUFOUjs7QUFVQTtFQUNJLDBCQUFBO0VBQ0EsMEJBQUE7QUFQSjs7QUFXSTtFQUNFLCtCQUFBO0VBQ0EsMEJBQUE7QUFSTjs7QUFZQTtFQUNFLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxzQkFBQTtBQVRGOztBQVdBO0VBQ0UsWUFBQTtBQVJGOztBQVlJO0VBQ0ksWUFBQTtBQVRSOztBQWFBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsNkJBQUE7RUFDQSwwQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUFWSjs7QUFhQTtFQUNJLCtCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQVZKOztBQWFBO0VBQ0ksV0FBQTtFQUNBLDBCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHVCQUFBO0VBQ0EsV0FBQTtFQUNBLG9CQUFBO0VBQ0EsaUNBQUE7QUFWSjs7QUFhQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZUFBQTtBQVZKOztBQWFBO0VBQ0ksVUFBQTtFQUNBLHNCQUFBO0VBQ0Esb0NBQUE7RUFDQSxnQ0FBQTtBQVZKOztBQWFBO0VBQ0ksd0NBQUE7RUFDQSwyQkFBQTtBQVZKOztBQWFBO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBVkoiLCJmaWxlIjoic3JjL2FwcC9kYXNoYm9hcmRzL21hbmFnZW1lbnQtbGV2ZWwtY2hhcnQvbWFuYWdlbWVudC1sZXZlbC1jaGFydC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oaWRlQ2hhcnR7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG59XHJcblxyXG4uYnV0dG9uLWNvbnRhaW5lciB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDdweDtcclxuICAgIGxlZnQ6IDBweDtcclxufVxyXG5cclxuLy8gLnF1YWRyYW50e1xyXG4vLyAgICAgYm9yZGVyOjJweCBzb2xpZCBibGFjaztcclxuLy8gfVxyXG5cclxuOjpuZy1kZWVwIC5uZzItZmxhdHBpY2tyLWlucHV0LWNvbnRhaW5lcntcclxuICAgIC5mb3JtLWNvbnRyb2xbcmVhZG9ubHldOm5vdChbZGlzYWJsZWRdKXtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG59XHJcblxyXG46Om5nLWRlZXAgLnRvb2x0aXAtaGVhZGVyIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNjNWM1YzU7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAudG9vbHRpcC1ib2R5IHtcclxuICAgIHdpZHRoOiAxNzBweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC50b29sdGlwLWJvZHkgLnNlcmllcy1uYW1lIHtcclxuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICBvcGFjaXR5OiAwLjY7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBsaW5lLWhlaWdodDogMS41O1xyXG4gICAgcGFkZGluZy1yaWdodDogMTBweDtcclxuICAgIHdpZHRoOiAxMjZweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC50b29sdGlwLWJvZHkgLnZhbHVlLXRleHQge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgbGluZS1oZWlnaHQ6IDEuNTtcclxuICAgIHdpZHRoOiAzMHB4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLmR4Yy12YWwtdGl0bGV7XHJcbiAgICB0ZXh0e1xyXG4gICAgICAgIGZvbnQtc2l6ZToxMHB0ICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubXVsdGlzZWxlY3QtZHJvcGRvd24gLmRyb3Bkb3duLWJ0biAuc2VsZWN0ZWQtaXRlbXtcclxuICAgIGZvbnQtc2l6ZToxMHB0ICFpbXBvcnRhbnQ7XHJcbiAgICBtYXJnaW4tdG9wOiA0cHggIWltcG9ydGFudDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5keGMtdGl0bGV7XHJcbiAgICB0ZXh0e1xyXG4gICAgICBmb250LWZhbWlseTogc2VndWlzYiAhaW1wb3J0YW50O1xyXG4gICAgICBmb250LXNpemU6IDEzcHQgIWltcG9ydGFudDtcclxuICAgIH1cclxufVxyXG5cclxuLnVwbG9hZF9idG4ge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICMwOTQzNTM7XHJcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbn1cclxuLnVwbG9hZF9idG4gaW1nIHtcclxuICBwYWRkaW5nOiA1cHg7XHJcbn1cclxuXHJcbi5jbG9zZXtcclxuICAgIHNwYW57XHJcbiAgICAgICAgY29sb3I6YmxhY2s7XHJcbiAgICB9XHJcbn1cclxuXHJcbi50YWJsZSB0aCwgLnRhYmxlIHRkIHtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xyXG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNkZWUyZTY7XHJcbiAgICBmb250LXNpemU6IDEwcHQhaW1wb3J0YW50O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxufVxyXG5cclxuLmRhdGF0YWJsZV9jdXN0IHRmb290IHRkIHtcclxuICAgIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICNjM2MzYzM7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2MzYzNjMztcclxuICAgIGJhY2tncm91bmQ6ICMzOGI4YmE7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIGZvbnQtZmFtaWx5OiBzZWd1aXNiO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAxMHB0O1xyXG59XHJcblxyXG4uZGF0YXRhYmxlX2N1c3QgdGZvb3QgaW5wdXQge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBmb250LXNpemU6IDEwcHQhaW1wb3J0YW50O1xyXG4gICAgcGFkZGluZzogMnB4IDEwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgZm9udC1mYW1pbHk6IHNlZ3Vpc2I7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZmZmIWltcG9ydGFudDtcclxufVxyXG5cclxuZGl2LmRhdGFUYWJsZXNfd3JhcHBlciBkaXYuZGF0YVRhYmxlc19wYWdpbmF0ZSB1bC5wYWdpbmF0aW9uIHtcclxuICAgIG1hcmdpbjogMnB4IDA7XHJcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuICAgIGZvbnQtc2l6ZTogMTBwdDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5wYWdlLWl0ZW0uYWN0aXZlIC5wYWdlLWxpbmsge1xyXG4gICAgei1pbmRleDogMTtcclxuICAgIGNvbG9yOiAjZmZmIWltcG9ydGFudDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzOGI4YmEhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjMzhiOGJhICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbjpuZy1kZWVwIGJ1dHRvbi5kdC1idXR0b257XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xyXG4gICAgYmFja2dyb3VuZDogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLnNxdWFyZXtcclxuICAgIGhlaWdodDogMjBweDtcclxuICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6YXV0bztcclxuICAgIG1hcmdpbi1yaWdodDphdXRvXHJcbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ManagementLevelChartComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-management-level-chart',
                templateUrl: './management-level-chart.component.html',
                styleUrls: ['./management-level-chart.component.scss']
            }]
    }], function () { return [{ type: _services_dashboards_service__WEBPACK_IMPORTED_MODULE_6__["DashboardsService"] }, { type: _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"] }, { type: _services_server_service__WEBPACK_IMPORTED_MODULE_8__["ServerService"] }, { type: ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_9__["BsModalService"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"] }]; }, { datatableElement: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
            args: [angular_datatables__WEBPACK_IMPORTED_MODULE_3__["DataTableDirective"]]
        }], fromDatePickr: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
            args: ['fromDatePickrElement']
        }] }); })();


/***/ }),

/***/ "H9vK":
/*!********************************************!*\
  !*** ./src/app/services/server.service.ts ***!
  \********************************************/
/*! exports provided: ServerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServerService", function() { return ServerService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/environments/environment */ "AytR");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "IheW");




class ServerService {
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    getAllDepartmentsList() {
        return this.httpClient.get(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].serverUrl}/GetdepartmentList`);
    }
    getQualityEventsList() {
        return this.httpClient.get(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].serverUrl}/GetQualityEventList`);
    }
    getIncidentCategoryList() {
        return this.httpClient.get(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].serverUrl}/BindIncidentCategory`);
    }
    getDepartmentsQualityEventsData(departmentsQualityEventsForData) {
        return this.httpClient.post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].serverUrl}/ManagementChart`, departmentsQualityEventsForData);
    }
    getQualityEventStatusOfOneDept(qualityEventDataOfOneDept) {
        return this.httpClient.post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].serverUrl}/StatusChartForQualityEvents`, qualityEventDataOfOneDept);
    }
    getOverdueData(overdueDataFilters) {
        return this.httpClient.post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].serverUrl}/OverdueChart`, overdueDataFilters);
    }
    getDeptPerformanceDataOfCountMetric(deptPerformanceDataFilters) {
        return this.httpClient.post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].serverUrl}/GetPerformanceChartCount`, deptPerformanceDataFilters);
    }
    getDeptPerformanceDataOfTATMetric(deptPerformanceDataFilters) {
        return this.httpClient.post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].serverUrl}/GetPerformanceChartTAT`, deptPerformanceDataFilters);
    }
    getCCNList(ccnListFilters) {
        return this.httpClient.post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].serverUrl}/GetCCNList`, ccnListFilters);
    }
    getIncidentList(incidentListFilters) {
        return this.httpClient.post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].serverUrl}/GetIncidentList`, incidentListFilters);
    }
    getErrataList(errataListFilters) {
        return this.httpClient.post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].serverUrl}/GetErrataList`, errataListFilters);
    }
    getNTFList(noteToFileListFilters) {
        return this.httpClient.post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].serverUrl}/GetNTFList`, noteToFileListFilters);
    }
    getCCNListForDeptPerformance(ccnListFilters) {
        return this.httpClient.post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].serverUrl}/GetCCNListViewForPerformanceChart`, ccnListFilters);
    }
    getIncidentListForDeptPerformance(incidentListFilters) {
        return this.httpClient.post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].serverUrl}/GetIncidentListViewForPerformanceChart`, incidentListFilters);
    }
    getErrataListForDeptPerformance(errataListFilters) {
        return this.httpClient.post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].serverUrl}/GetErrataListViewForPerformanceChart`, errataListFilters);
    }
    getNTFListForDeptPerformance(noteToFileListFilters) {
        return this.httpClient.post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].serverUrl}/GetNTFListViewForPerformanceChart`, noteToFileListFilters);
    }
    getQualityEventsListForCAPA() {
        return this.httpClient.get(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].serverUrl}/GetQualityEventListforCAPAChart`);
    }
    getDeptQualityEventsDataForCAPA(departmentsQualityEventsFiltersForCAPA) {
        return this.httpClient.post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].serverUrl}/CapaBarChart`, departmentsQualityEventsFiltersForCAPA);
    }
    getQualityEventStatusOfOneDeptForCAPA(departmentsQualityEventStatusFiltersForCAPA) {
        return this.httpClient.post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].serverUrl}/StatusChartForCAPA`, departmentsQualityEventStatusFiltersForCAPA);
    }
}
ServerService.ɵfac = function ServerService_Factory(t) { return new (t || ServerService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"])); };
ServerService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: ServerService, factory: ServerService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ServerService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "Hktm":
/*!****************************************************!*\
  !*** ./src/app/dashboards/dashboards.component.ts ***!
  \****************************************************/
/*! exports provided: DashboardsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardsComponent", function() { return DashboardsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _management_level_chart_management_level_chart_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./management-level-chart/management-level-chart.component */ "F19l");
/* harmony import */ var _department_performance_chart_department_performance_chart_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./department-performance-chart/department-performance-chart.component */ "0SbY");
/* harmony import */ var _capa_chart_capa_chart_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./capa-chart/capa-chart.component */ "sW5g");





class DashboardsComponent {
    constructor() {
    }
    ngOnInit() {
    }
}
DashboardsComponent.ɵfac = function DashboardsComponent_Factory(t) { return new (t || DashboardsComponent)(); };
DashboardsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: DashboardsComponent, selectors: [["app-dashboards"]], decls: 9, vars: 0, consts: [[1, "container", "mt-3"], [1, "row"], [1, "col-md-6"]], template: function DashboardsComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "app-management-level-chart");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "app-department-performance-chart");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "app-capa-chart");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, directives: [_management_level_chart_management_level_chart_component__WEBPACK_IMPORTED_MODULE_1__["ManagementLevelChartComponent"], _department_performance_chart_department_performance_chart_component__WEBPACK_IMPORTED_MODULE_2__["DepartmentPerformanceChartComponent"], _capa_chart_capa_chart_component__WEBPACK_IMPORTED_MODULE_3__["CapaChartComponent"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZHMvZGFzaGJvYXJkcy5jb21wb25lbnQuc2NzcyJ9 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DashboardsComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-dashboards',
                templateUrl: './dashboards.component.html',
                styleUrls: ['./dashboards.component.scss']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "MiXW":
/*!***********************************************************************************************!*\
  !*** ./src/app/dashboards/list-view-dept-performance/list-view-dept-performance.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: ListViewDeptPerformanceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListViewDeptPerformanceComponent", function() { return ListViewDeptPerformanceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angular-datatables */ "oTcB");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var src_app_models_dashboards__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/models/dashboards */ "+E+c");
/* harmony import */ var src_app_services_dashboards_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/dashboards.service */ "PGxC");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/modal */ "LqlI");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "SVse");









function ListViewDeptPerformanceComponent_th_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const eachCol_r6 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](eachCol_r6.title);
} }
function ListViewDeptPerformanceComponent_tbody_11_tr_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const record_r8 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r8.CCN_No);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r8.DepartmentName);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r8.CreatedBy);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r8.MonthnameAndYear);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r8.CreatedDate);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r8.DueDate);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r8.CompletedDate);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r8.ChangeClassification);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r8.TAT);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r8.TAT);
} }
function ListViewDeptPerformanceComponent_tbody_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tbody");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ListViewDeptPerformanceComponent_tbody_11_tr_1_Template, 21, 10, "tr", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r1.records);
} }
function ListViewDeptPerformanceComponent_tbody_12_tr_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const record_r11 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r11.Incident_No);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r11.DepartmentName);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r11.TypeofIncident);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r11.MonthnameAndYear);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r11.Category);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r11.CreatedBy);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r11.DateOfOccurance);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r11.DateOfReport);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r11.DueDate);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r11.CompletedDate);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r11.TAT);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r11.TAT);
} }
function ListViewDeptPerformanceComponent_tbody_12_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tbody");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ListViewDeptPerformanceComponent_tbody_12_tr_1_Template, 25, 12, "tr", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r2.records);
} }
function ListViewDeptPerformanceComponent_tbody_13_tr_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const record_r13 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r13.Errata_No);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r13.DepartmentName);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r13.DocName);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r13.MonthnameAndYear);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r13.refdocno);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r13.CreatedBy);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r13.CreatedDate);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r13.CompletedDate);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r13.TAT);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r13.TAT);
} }
function ListViewDeptPerformanceComponent_tbody_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tbody");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ListViewDeptPerformanceComponent_tbody_13_tr_1_Template, 21, 10, "tr", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r3.records);
} }
function ListViewDeptPerformanceComponent_tbody_14_tr_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const record_r15 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r15.NTF_No);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r15.DepartmentName);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r15.DocName);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r15.MonthnameAndYear);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r15.refdocno);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r15.CreatedBy);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r15.CreatedDate);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r15.CompletedDate);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r15.TAT);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r15.TAT);
} }
function ListViewDeptPerformanceComponent_tbody_14_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tbody");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ListViewDeptPerformanceComponent_tbody_14_tr_1_Template, 21, 10, "tr", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r4.records);
} }
function ListViewDeptPerformanceComponent_td_17_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "input", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
class ListViewDeptPerformanceComponent {
    constructor(dashboardService, bsModalRef, modalService, changeDetection, renderer) {
        this.dashboardService = dashboardService;
        this.bsModalRef = bsModalRef;
        this.modalService = modalService;
        this.changeDetection = changeDetection;
        this.renderer = renderer;
        this.dtOptions = {};
        this.CCNdtOptions = {};
        this.incidentDtOptions = {};
        this.errataDtOptions = {};
        this.noteToFileDtOptions = {};
        this.qualityEventsNames = src_app_models_dashboards__WEBPACK_IMPORTED_MODULE_3__["QualityEvents"];
        this.events = [];
        this.ccnRecordsPerformance = {};
    }
    ngOnInit() {
        let limitsRecvd = this.limits;
        this.setCCNDtOptions(limitsRecvd);
        this.setIncidentDtOptions(limitsRecvd);
        this.setErrataDtOptions(limitsRecvd);
        this.setNTFDtOptions(limitsRecvd);
        var trimmedQualityEventName = '';
        if (this.qualityEvent === src_app_models_dashboards__WEBPACK_IMPORTED_MODULE_3__["QualityEvents"].ChangeControlNote) {
            trimmedQualityEventName = 'CCN';
            this.dtOptions = this.CCNdtOptions;
            this.records = this.ccnRecords;
        }
        else if (this.qualityEvent === src_app_models_dashboards__WEBPACK_IMPORTED_MODULE_3__["QualityEvents"].IncidentReport) {
            trimmedQualityEventName = 'IR';
            this.dtOptions = this.incidentDtOptions;
            this.records = this.incidentRecords;
        }
        else if (this.qualityEvent === src_app_models_dashboards__WEBPACK_IMPORTED_MODULE_3__["QualityEvents"].NoteToFile) {
            trimmedQualityEventName = 'NTF';
            this.dtOptions = this.noteToFileDtOptions;
            this.records = this.noteToFileRecords;
        }
        else {
            trimmedQualityEventName = 'Errata';
            this.dtOptions = this.errataDtOptions;
            this.records = this.errataRecords;
        }
        this.getMonthsInASequenceForListView(this.records);
        this.dtOptions.buttons[0].filename = "Dept. Performance - " + trimmedQualityEventName + this.fileName;
        // let excelButton = document.getElementsByClassName('buttons-html5');
        // console.log(excelButton);
        //     this.renderer.addClass(excelButton[0],'btn btn-outline');
    }
    ngAfterViewInit() {
        const _combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["combineLatest"])([this.modalService.onShow,
            this.modalService.onShown,
            this.modalService.onHide,
            this.modalService.onHidden]).subscribe(() => this.changeDetection.markForCheck());
        this.events.push(this.modalService.onShown.subscribe((reason) => {
            this.datatableElement.dtInstance.then((dtInstance) => {
                dtInstance.columns().every(function () {
                    const that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this['value']) {
                            that
                                .search(this['value'])
                                .draw();
                        }
                    });
                });
            });
        }));
        this.events.push(_combine);
    }
    setCCNDtOptions(limitsRecvd) {
        this.CCNdtOptions = {
            columns: [
                {
                    title: 'CCN Number',
                    data: 'ccnNumber'
                }, {
                    title: 'Department',
                    data: 'department'
                }, {
                    title: 'Created By',
                    data: 'createdBy'
                },
                {
                    title: 'Month',
                    data: 'month'
                },
                {
                    title: 'Created Date',
                    data: 'createdDate'
                },
                {
                    title: 'Due Date',
                    data: 'dueDate'
                },
                {
                    title: 'Completed Date',
                    data: 'completedDate'
                },
                {
                    title: 'Classification',
                    data: 'classification'
                },
                {
                    title: 'TAT',
                    data: 'turnAroundTime'
                },
                {
                    title: 'Performance',
                    data: 'performance'
                }
            ],
            order: [],
            rowGroup: {
                dataSrc: "month"
            },
            responsive: true,
            // pagination:true,
            scrollX: true,
            // Declare the use of the extension in the dom parameter
            dom: 'lBfrtip',
            columnDefs: [
                { "visible": false, "targets": 3 },
                {
                    "targets": 9,
                    "data": null,
                    "render": function (data, type, row, meta) {
                        if (data >= limitsRecvd.upperLimitForListView) {
                            if (type === 'display') {
                                return '<img src="/Images/QMS_Dashboard_Angular/Poor_45x45.png">';
                            }
                            else {
                                return 'Poor';
                            }
                        }
                        else if (data < limitsRecvd.upperLimitForListView) {
                            if (data < limitsRecvd.lowerLimitForListView) {
                                if (type === 'display') {
                                    return '<img src="/Images/QMS_Dashboard_Angular/Good_45x45.png">';
                                }
                                else {
                                    return 'Good';
                                }
                            }
                            else {
                                if (type === 'display') {
                                    return '<img src="/Images/QMS_Dashboard_Angular/Moderate_45x45.png">';
                                }
                                else {
                                    return 'Moderate';
                                }
                            }
                        }
                    }
                }
            ],
            // Configure the buttons
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: '<img src="/Images/QMS_Dashboard_Angular/excel.png">',
                    title: null,
                    exportOptions: {
                        // Any other settings used
                        grouped_array_index: 'month',
                        columns: ':visible',
                        format: {
                            body: function (data, row, column, node) {
                                if (column === 8) {
                                    if (data === '<img src="/Images/QMS_Dashboard_Angular/Poor_45x45.png">') {
                                        return 'Poor';
                                    }
                                    else if (data === '<img src="/Images/QMS_Dashboard_Angular/Moderate_45x45.png">') {
                                        return 'Moderate';
                                    }
                                    else if (data === '<img src="/Images/QMS_Dashboard_Angular/Good_45x45.png">') {
                                        return 'Good';
                                    }
                                    else
                                        return data;
                                }
                                else
                                    return data;
                            }
                        }
                    },
                    customize: function (xlsx) {
                        var sheet = xlsx.xl.worksheets['sheet1.xml'];
                        console.log(sheet);
                        // this will be used to determine addresses of cells being merged later (on export) in excel
                        let lettersAndIndexes = { 'A': 1, 'B': 2, 'C': 3, 'D': 4, 'E': 5, 'F': 6, 'G': 7, 'H': 8, 'I': 9, 'J': 10, 'K': 11, 'L': 12, 'M': 13, 'N': 14, 'O': 15, 'P': 16, 'Q': 17, 'R': 18, 'S': 19, 'T': 20, 'U': 21, 'V': 22, 'W': 23, 'X': 24, 'Y': 25, 'Z': 26 };
                        let indexesAndLetters = { 1: 'A', 2: 'B', 3: 'C', 4: 'D', 5: 'E', 6: 'F', 7: 'G', 8: 'H', 9: 'I', 10: 'J', 11: 'K', 12: 'L', 13: 'M', 14: 'N', 15: 'O', 16: 'P', 17: 'Q', 18: 'R', 19: 'S', 20: 'T', 21: 'U', 22: 'V', 23: 'W', 24: 'X', 25: 'Y', 26: 'Z' };
                        var cols = sheet.getElementsByTagName('col');
                        let numOfCols = cols.length;
                        let lastColName = getLetterCodeByNumIndex(numOfCols);
                        // let perfColName=getLetterCodeByNumIndex(numOfCols-1);
                        var rows = $('row', sheet);
                        console.log("rows", rows);
                        let totalRows = rows.length;
                        for (let i = 0; i < totalRows; i++) {
                            if (rows[i].childNodes.length === 1) {
                                let rowDet = getRowNodeHavingRowGroupHeader(rows[i]);
                                let refString = rowDet.startingCellNum + ':' + lastColName + rowDet.rowNum;
                                $(`c[r=${rowDet.startingCellNum}]`, sheet).attr('s', '7');
                                // $(`c[r=${rowDet.startingCellNum}]`,sheet).attr('s','51');
                                mergeFunction(refString);
                            }
                        }
                        // for updating performance column data
                        $(`row c[r^=${lastColName}]`, sheet).each(function () {
                            if ($('is t', this).text() === 'Poor') {
                                $(this).attr('s', '11');
                            }
                            else if ($('is t', this).text() === 'Good') {
                                $(this).attr('s', '16');
                            }
                            else if ($('is t', this).text() === 'Moderate') {
                                $(this).attr('s', '20');
                            }
                        });
                        // for(let i=0;i<totalRows;i++){
                        //   if(i!==0){
                        //     if(rows[i].childNodes.length>1){
                        //       let cellName=lastColName+i;
                        //       if((rows[i].lastChild.textContent)==='Poor'){
                        //         $(`row c[r=${cellName}] is t`,sheet).css('color','red');
                        //       }
                        //       else if((rows[i].lastChild.textContent)==='Good'){
                        //         $(`row c[r=${cellName}]`,sheet).css('color','green');
                        //       }
                        //       else if((rows[i].lastChild.textContent)==='Moderate'){
                        //         $(`row c[r=${cellName}]`,sheet).css('color','Yellow');
                        //       }
                        //     }
                        //   }
                        // }
                        console.log("after updating sheet", sheet);
                        function getLetterCodeByNumIndex(numIndex) {
                            let letterCode;
                            if (numIndex > 702) { // 3 symbols
                                let lastLetter, left, firstLetterNumber;
                                if (numIndex % 26 === 0) {
                                    lastLetter = indexesAndLetters[26];
                                    left = numIndex - 26;
                                }
                                else {
                                    lastLetter = indexesAndLetters[numIndex % 26];
                                    left = numIndex - (numIndex % 26);
                                }
                                if ((left / 676) % 1 === 0) {
                                    firstLetterNumber = Math.floor(left / 676) - 1;
                                }
                                else {
                                    firstLetterNumber = Math.floor(left / 676);
                                }
                                let firstLetter = indexesAndLetters[firstLetterNumber];
                                left = left - firstLetterNumber * 676;
                                let middleLetter = indexesAndLetters[(left / 26).toFixed(0)];
                                letterCode = firstLetter + middleLetter + lastLetter;
                            }
                            else if (numIndex > 26) { // 2 symbols
                                let lastLetter, left;
                                if (numIndex % 26 === 0) {
                                    lastLetter = indexesAndLetters[26];
                                    left = numIndex - 26;
                                }
                                else {
                                    lastLetter = indexesAndLetters[numIndex % 26];
                                    left = numIndex - (numIndex % 26);
                                }
                                let firstLetter = indexesAndLetters[(left / 26).toFixed(0)];
                                letterCode = firstLetter + lastLetter;
                            }
                            else { // 1 symbol
                                letterCode = indexesAndLetters[numIndex];
                            }
                            return letterCode;
                        }
                        function getNumIndexByLetterCode(letterCode) {
                            letterCode = letterCode.toUpperCase();
                            let numIndex;
                            switch (letterCode.length) {
                                case 1: {
                                    numIndex = lettersAndIndexes[letterCode];
                                    break;
                                }
                                case 2: {
                                    let [firstLetter, secondLetter] = letterCode.split('');
                                    numIndex = lettersAndIndexes[firstLetter] * 26 + lettersAndIndexes[secondLetter];
                                    break;
                                }
                                case 3: {
                                    let [firstLetter, secondLetter, thirdLetter] = letterCode.split('');
                                    numIndex = lettersAndIndexes[firstLetter] * 676 + lettersAndIndexes[secondLetter] * 26 + lettersAndIndexes[thirdLetter];
                                    break;
                                }
                                default:
                                    console.log(`unhandled letterCode.length: ${letterCode.length} ${letterCode}`);
                                    break;
                            }
                            return numIndex;
                        }
                        function _createNode(doc, nodeName, opts) {
                            var tempNode = doc.createElement(nodeName);
                            if (opts) {
                                if (opts.attr) {
                                    $(tempNode).attr(opts.attr);
                                }
                                if (opts.children) {
                                    $.each(opts.children, function (key, value) {
                                        tempNode.appendChild(value);
                                    });
                                }
                                if (opts.text !== null && opts.text !== undefined) {
                                    tempNode.appendChild(doc.createTextNode(opts.text));
                                }
                            }
                            return tempNode;
                        }
                        function mergeFunction(referenceString) {
                            // reference string format: 'B1:E1'
                            var mergeCells = $('mergeCells', sheet);
                            console.log("mergeCells", mergeCells);
                            if (!referenceString) {
                                alert('no reference string on merge function call');
                                return;
                            }
                            mergeCells[0].appendChild(_createNode(sheet, 'mergeCell', {
                                attr: {
                                    ref: referenceString
                                }
                            }));
                            mergeCells.attr('count', mergeCells.attr('count') + 1);
                        }
                        function getRowNodeHavingRowGroupHeader(currentRow) {
                            let rowDetails = {
                                startingCellNum: currentRow.lastChild.attributes[1].value,
                                rowNum: currentRow.attributes[0].value
                            };
                            return rowDetails;
                        }
                    },
                }
            ],
        };
    }
    setIncidentDtOptions(limitsRecvd) {
        this.incidentDtOptions = {
            columns: [{
                    title: 'Incident_No',
                    data: 'incidentNumber'
                }, {
                    title: 'Department',
                    data: 'department'
                },
                {
                    title: 'Type of Incident',
                    data: 'typeOfIncident'
                },
                {
                    title: 'Month',
                    data: 'month'
                },
                {
                    title: 'Category',
                    data: 'category'
                },
                {
                    title: 'Created By',
                    data: 'createdBy'
                },
                {
                    title: 'Date of Occurence',
                    data: 'dateOfOccurance'
                },
                {
                    title: 'Date of Report',
                    data: 'dateOfReport'
                },
                {
                    title: 'Due Date',
                    data: 'dueDate'
                },
                {
                    title: 'Completed Date',
                    data: 'completedDate',
                },
                {
                    title: 'TAT',
                    data: 'turnAroundTime'
                },
                {
                    title: 'Performance',
                    data: 'performance'
                }
            ],
            columnDefs: [
                { "visible": false, "targets": 3 },
                {
                    "targets": 11,
                    "data": null,
                    "render": function (data, type, row, meta) {
                        if (data >= limitsRecvd.upperLimitForListView) {
                            if (type === 'display') {
                                return '<img src="/Images/QMS_Dashboard_Angular/Poor_45x45.png">';
                            }
                            else {
                                return 'Poor';
                            }
                        }
                        else if (data < limitsRecvd.upperLimitForListView) {
                            if (data < limitsRecvd.lowerLimitForListView) {
                                if (type === 'display') {
                                    return '<img src="/Images/QMS_Dashboard_Angular/Good_45x45.png">';
                                }
                                else {
                                    return 'Good';
                                }
                            }
                            else {
                                if (type === 'display') {
                                    return '<img src="/Images/QMS_Dashboard_Angular/Moderate_45x45.png">';
                                }
                                else {
                                    return 'Moderate';
                                }
                            }
                        }
                    }
                }
            ],
            order: [],
            rowGroup: {
                dataSrc: "month"
            },
            responsive: true,
            scrollX: true,
            // Declare the use of the extension in the dom parameter
            dom: 'lBfrtip',
            // Configure the buttons
            buttons: [
                {
                    extend: 'excel',
                    text: '<img src="/Images/QMS_Dashboard_Angular/excel.png">',
                    title: null,
                    exportOptions: {
                        // Any other settings used
                        grouped_array_index: 'month',
                        columns: ':visible',
                        format: {
                            body: function (data, row, column, node) {
                                if (column === 10) {
                                    if (data === '<img src="/Images/QMS_Dashboard_Angular/Poor_45x45.png">') {
                                        return 'Poor';
                                    }
                                    else if (data === '<img src="/Images/QMS_Dashboard_Angular/Moderate_45x45.png">') {
                                        return 'Moderate';
                                    }
                                    else if (data === '<img src="/Images/QMS_Dashboard_Angular/Good_45x45.png">') {
                                        return 'Good';
                                    }
                                    else
                                        return data;
                                }
                                else
                                    return data;
                            }
                        }
                    },
                    customize: function (xlsx) {
                        var sheet = xlsx.xl.worksheets['sheet1.xml'];
                        console.log(sheet);
                        // this will be used to determine addresses of cells being merged later (on export) in excel
                        let lettersAndIndexes = { 'A': 1, 'B': 2, 'C': 3, 'D': 4, 'E': 5, 'F': 6, 'G': 7, 'H': 8, 'I': 9, 'J': 10, 'K': 11, 'L': 12, 'M': 13, 'N': 14, 'O': 15, 'P': 16, 'Q': 17, 'R': 18, 'S': 19, 'T': 20, 'U': 21, 'V': 22, 'W': 23, 'X': 24, 'Y': 25, 'Z': 26 };
                        let indexesAndLetters = { 1: 'A', 2: 'B', 3: 'C', 4: 'D', 5: 'E', 6: 'F', 7: 'G', 8: 'H', 9: 'I', 10: 'J', 11: 'K', 12: 'L', 13: 'M', 14: 'N', 15: 'O', 16: 'P', 17: 'Q', 18: 'R', 19: 'S', 20: 'T', 21: 'U', 22: 'V', 23: 'W', 24: 'X', 25: 'Y', 26: 'Z' };
                        var cols = sheet.getElementsByTagName('col');
                        let numOfCols = cols.length;
                        let lastColName = getLetterCodeByNumIndex(numOfCols);
                        var rows = $('row', sheet);
                        console.log("rows", rows);
                        let totalRows = rows.length;
                        for (let i = 0; i < totalRows; i++) {
                            if (rows[i].childNodes.length === 1) {
                                let rowDet = getRowNodeHavingRowGroupHeader(rows[i]);
                                let refString = rowDet.startingCellNum + ':' + lastColName + rowDet.rowNum;
                                $(`c[r=${rowDet.startingCellNum}]`, sheet).attr('s', '7');
                                // $(`c[r=${rowDet.startingCellNum}]`,sheet).attr('s','51');
                                mergeFunction(refString);
                            }
                        }
                        // for updating performance column data styles
                        $(`row c[r^=${lastColName}]`, sheet).each(function () {
                            if ($('is t', this).text() === 'Poor') {
                                $(this).attr('s', '11');
                            }
                            else if ($('is t', this).text() === 'Good') {
                                $(this).attr('s', '16');
                            }
                            else if ($('is t', this).text() === 'Moderate') {
                                $(this).attr('s', '20');
                            }
                        });
                        console.log("after updating sheet", sheet);
                        function getLetterCodeByNumIndex(numIndex) {
                            let letterCode;
                            if (numIndex > 702) { // 3 symbols
                                let lastLetter, left, firstLetterNumber;
                                if (numIndex % 26 === 0) {
                                    lastLetter = indexesAndLetters[26];
                                    left = numIndex - 26;
                                }
                                else {
                                    lastLetter = indexesAndLetters[numIndex % 26];
                                    left = numIndex - (numIndex % 26);
                                }
                                if ((left / 676) % 1 === 0) {
                                    firstLetterNumber = Math.floor(left / 676) - 1;
                                }
                                else {
                                    firstLetterNumber = Math.floor(left / 676);
                                }
                                let firstLetter = indexesAndLetters[firstLetterNumber];
                                left = left - firstLetterNumber * 676;
                                let middleLetter = indexesAndLetters[(left / 26).toFixed(0)];
                                letterCode = firstLetter + middleLetter + lastLetter;
                            }
                            else if (numIndex > 26) { // 2 symbols
                                let lastLetter, left;
                                if (numIndex % 26 === 0) {
                                    lastLetter = indexesAndLetters[26];
                                    left = numIndex - 26;
                                }
                                else {
                                    lastLetter = indexesAndLetters[numIndex % 26];
                                    left = numIndex - (numIndex % 26);
                                }
                                let firstLetter = indexesAndLetters[(left / 26).toFixed(0)];
                                letterCode = firstLetter + lastLetter;
                            }
                            else { // 1 symbol
                                letterCode = indexesAndLetters[numIndex];
                            }
                            return letterCode;
                        }
                        function getNumIndexByLetterCode(letterCode) {
                            letterCode = letterCode.toUpperCase();
                            let numIndex;
                            switch (letterCode.length) {
                                case 1: {
                                    numIndex = lettersAndIndexes[letterCode];
                                    break;
                                }
                                case 2: {
                                    let [firstLetter, secondLetter] = letterCode.split('');
                                    numIndex = lettersAndIndexes[firstLetter] * 26 + lettersAndIndexes[secondLetter];
                                    break;
                                }
                                case 3: {
                                    let [firstLetter, secondLetter, thirdLetter] = letterCode.split('');
                                    numIndex = lettersAndIndexes[firstLetter] * 676 + lettersAndIndexes[secondLetter] * 26 + lettersAndIndexes[thirdLetter];
                                    break;
                                }
                                default:
                                    console.log(`unhandled letterCode.length: ${letterCode.length} ${letterCode}`);
                                    break;
                            }
                            return numIndex;
                        }
                        function _createNode(doc, nodeName, opts) {
                            var tempNode = doc.createElement(nodeName);
                            if (opts) {
                                if (opts.attr) {
                                    $(tempNode).attr(opts.attr);
                                }
                                if (opts.children) {
                                    $.each(opts.children, function (key, value) {
                                        tempNode.appendChild(value);
                                    });
                                }
                                if (opts.text !== null && opts.text !== undefined) {
                                    tempNode.appendChild(doc.createTextNode(opts.text));
                                }
                            }
                            return tempNode;
                        }
                        function mergeFunction(referenceString) {
                            // reference string format: 'B1:E1'
                            var mergeCells = $('mergeCells', sheet);
                            console.log("mergeCells", mergeCells);
                            if (!referenceString) {
                                alert('no reference string on merge function call');
                                return;
                            }
                            mergeCells[0].appendChild(_createNode(sheet, 'mergeCell', {
                                attr: {
                                    ref: referenceString
                                }
                            }));
                            mergeCells.attr('count', mergeCells.attr('count') + 1);
                        }
                        function getRowNodeHavingRowGroupHeader(currentRow) {
                            let rowDetails = {
                                startingCellNum: currentRow.lastChild.attributes[1].value,
                                rowNum: currentRow.attributes[0].value
                            };
                            return rowDetails;
                        }
                    },
                }
            ],
        };
    }
    setErrataDtOptions(limitsRecvd) {
        this.errataDtOptions = {
            columns: [{
                    title: 'Errata Number',
                    data: 'errataNumber'
                }, {
                    title: 'Department',
                    data: 'department'
                },
                {
                    title: 'Document',
                    data: 'document'
                },
                {
                    title: 'Month',
                    data: 'month'
                },
                {
                    title: 'Ref. Doc No.',
                    data: 'refDocNum'
                }, {
                    title: 'Created By',
                    data: 'createdBy'
                },
                {
                    title: 'Created Date',
                    data: 'createdDate'
                },
                {
                    title: 'Completed Date',
                    data: 'completedDate',
                },
                {
                    title: 'TAT',
                    data: 'turnAroundTime'
                },
                {
                    title: 'Performance',
                    data: 'performance'
                }
            ],
            columnDefs: [
                { "visible": false, "targets": 3 },
                {
                    "targets": 9,
                    "data": null,
                    "render": function (data, type, row, meta) {
                        if (data >= limitsRecvd.upperLimitForListView) {
                            if (type === 'display') {
                                return '<img src="/Images/QMS_Dashboard_Angular/Poor_45x45.png">';
                            }
                            else {
                                return 'Poor';
                            }
                        }
                        else if (data < limitsRecvd.upperLimitForListView) {
                            if (data < limitsRecvd.lowerLimitForListView) {
                                if (type === 'display') {
                                    return '<img src="/Images/QMS_Dashboard_Angular/Good_45x45.png">';
                                }
                                else {
                                    return 'Good';
                                }
                            }
                            else {
                                if (type === 'display') {
                                    return '<img src="/Images/QMS_Dashboard_Angular/Moderate_45x45.png">';
                                }
                                else {
                                    return 'Moderate';
                                }
                            }
                        }
                    }
                }
            ],
            // order: [[3, 'asc']],
            order: [],
            rowGroup: {
                dataSrc: "month"
            },
            responsive: true,
            scrollX: true,
            // Declare the use of the extension in the dom parameter
            dom: 'lBfrtip',
            // Configure the buttons
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: '<img src="/Images/QMS_Dashboard_Angular/excel.png">',
                    title: null,
                    exportOptions: {
                        // Any other settings used
                        grouped_array_index: 'month',
                        columns: ':visible',
                        format: {
                            body: function (data, row, column, node) {
                                if (column === 8) {
                                    if (data === '<img src="/Images/QMS_Dashboard_Angular/Poor_45x45.png">') {
                                        return 'Poor';
                                    }
                                    else if (data === '<img src="/Images/QMS_Dashboard_Angular/Moderate_45x45.png">') {
                                        return 'Moderate';
                                    }
                                    else if (data === '<img src="/Images/QMS_Dashboard_Angular/Good_45x45.png">') {
                                        return 'Good';
                                    }
                                    else
                                        return data;
                                }
                                else
                                    return data;
                            }
                        }
                    },
                    customize: function (xlsx) {
                        var sheet = xlsx.xl.worksheets['sheet1.xml'];
                        console.log(sheet);
                        // this will be used to determine addresses of cells being merged later (on export) in excel
                        let lettersAndIndexes = { 'A': 1, 'B': 2, 'C': 3, 'D': 4, 'E': 5, 'F': 6, 'G': 7, 'H': 8, 'I': 9, 'J': 10, 'K': 11, 'L': 12, 'M': 13, 'N': 14, 'O': 15, 'P': 16, 'Q': 17, 'R': 18, 'S': 19, 'T': 20, 'U': 21, 'V': 22, 'W': 23, 'X': 24, 'Y': 25, 'Z': 26 };
                        let indexesAndLetters = { 1: 'A', 2: 'B', 3: 'C', 4: 'D', 5: 'E', 6: 'F', 7: 'G', 8: 'H', 9: 'I', 10: 'J', 11: 'K', 12: 'L', 13: 'M', 14: 'N', 15: 'O', 16: 'P', 17: 'Q', 18: 'R', 19: 'S', 20: 'T', 21: 'U', 22: 'V', 23: 'W', 24: 'X', 25: 'Y', 26: 'Z' };
                        var cols = sheet.getElementsByTagName('col');
                        let numOfCols = cols.length;
                        let lastColName = getLetterCodeByNumIndex(numOfCols);
                        // let perfColName=getLetterCodeByNumIndex(numOfCols-1);
                        var rows = $('row', sheet);
                        console.log("rows", rows);
                        let totalRows = rows.length;
                        for (let i = 0; i < totalRows; i++) {
                            if (rows[i].childNodes.length === 1) {
                                let rowDet = getRowNodeHavingRowGroupHeader(rows[i]);
                                let refString = rowDet.startingCellNum + ':' + lastColName + rowDet.rowNum;
                                $(`c[r=${rowDet.startingCellNum}]`, sheet).attr('s', '7');
                                // $(`c[r=${rowDet.startingCellNum}]`,sheet).attr('s','51');
                                mergeFunction(refString);
                            }
                        }
                        // for updating performance column data
                        $(`row c[r^=${lastColName}]`, sheet).each(function () {
                            if ($('is t', this).text() === 'Poor') {
                                $(this).attr('s', '11');
                            }
                            else if ($('is t', this).text() === 'Good') {
                                $(this).attr('s', '16');
                            }
                            else if ($('is t', this).text() === 'Moderate') {
                                $(this).attr('s', '20');
                            }
                        });
                        console.log("after updating sheet", sheet);
                        function getLetterCodeByNumIndex(numIndex) {
                            let letterCode;
                            if (numIndex > 702) { // 3 symbols
                                let lastLetter, left, firstLetterNumber;
                                if (numIndex % 26 === 0) {
                                    lastLetter = indexesAndLetters[26];
                                    left = numIndex - 26;
                                }
                                else {
                                    lastLetter = indexesAndLetters[numIndex % 26];
                                    left = numIndex - (numIndex % 26);
                                }
                                if ((left / 676) % 1 === 0) {
                                    firstLetterNumber = Math.floor(left / 676) - 1;
                                }
                                else {
                                    firstLetterNumber = Math.floor(left / 676);
                                }
                                let firstLetter = indexesAndLetters[firstLetterNumber];
                                left = left - firstLetterNumber * 676;
                                let middleLetter = indexesAndLetters[(left / 26).toFixed(0)];
                                letterCode = firstLetter + middleLetter + lastLetter;
                            }
                            else if (numIndex > 26) { // 2 symbols
                                let lastLetter, left;
                                if (numIndex % 26 === 0) {
                                    lastLetter = indexesAndLetters[26];
                                    left = numIndex - 26;
                                }
                                else {
                                    lastLetter = indexesAndLetters[numIndex % 26];
                                    left = numIndex - (numIndex % 26);
                                }
                                let firstLetter = indexesAndLetters[(left / 26).toFixed(0)];
                                letterCode = firstLetter + lastLetter;
                            }
                            else { // 1 symbol
                                letterCode = indexesAndLetters[numIndex];
                            }
                            return letterCode;
                        }
                        function getNumIndexByLetterCode(letterCode) {
                            letterCode = letterCode.toUpperCase();
                            let numIndex;
                            switch (letterCode.length) {
                                case 1: {
                                    numIndex = lettersAndIndexes[letterCode];
                                    break;
                                }
                                case 2: {
                                    let [firstLetter, secondLetter] = letterCode.split('');
                                    numIndex = lettersAndIndexes[firstLetter] * 26 + lettersAndIndexes[secondLetter];
                                    break;
                                }
                                case 3: {
                                    let [firstLetter, secondLetter, thirdLetter] = letterCode.split('');
                                    numIndex = lettersAndIndexes[firstLetter] * 676 + lettersAndIndexes[secondLetter] * 26 + lettersAndIndexes[thirdLetter];
                                    break;
                                }
                                default:
                                    console.log(`unhandled letterCode.length: ${letterCode.length} ${letterCode}`);
                                    break;
                            }
                            return numIndex;
                        }
                        function _createNode(doc, nodeName, opts) {
                            var tempNode = doc.createElement(nodeName);
                            if (opts) {
                                if (opts.attr) {
                                    $(tempNode).attr(opts.attr);
                                }
                                if (opts.children) {
                                    $.each(opts.children, function (key, value) {
                                        tempNode.appendChild(value);
                                    });
                                }
                                if (opts.text !== null && opts.text !== undefined) {
                                    tempNode.appendChild(doc.createTextNode(opts.text));
                                }
                            }
                            return tempNode;
                        }
                        function mergeFunction(referenceString) {
                            // reference string format: 'B1:E1'
                            var mergeCells = $('mergeCells', sheet);
                            console.log("mergeCells", mergeCells);
                            if (!referenceString) {
                                alert('no reference string on merge function call');
                                return;
                            }
                            mergeCells[0].appendChild(_createNode(sheet, 'mergeCell', {
                                attr: {
                                    ref: referenceString
                                }
                            }));
                            mergeCells.attr('count', mergeCells.attr('count') + 1);
                        }
                        function getRowNodeHavingRowGroupHeader(currentRow) {
                            let rowDetails = {
                                startingCellNum: currentRow.lastChild.attributes[1].value,
                                rowNum: currentRow.attributes[0].value
                            };
                            return rowDetails;
                        }
                    },
                }
            ],
        };
    }
    setNTFDtOptions(limitsRecvd) {
        this.noteToFileDtOptions = {
            columns: [{
                    title: 'NTF Number',
                    data: 'noteToFileNumber'
                }, {
                    title: 'Department',
                    data: 'department'
                },
                {
                    title: 'Document',
                    data: 'document'
                },
                {
                    title: 'Month',
                    data: 'month'
                },
                {
                    title: 'Ref. Doc No.',
                    data: 'refDocNum'
                }, {
                    title: 'Created By',
                    data: 'createdBy'
                },
                {
                    title: 'Created Date',
                    data: 'createdDate'
                },
                {
                    title: 'Completed Date',
                    data: 'completedDate',
                },
                {
                    title: 'TAT',
                    data: 'turnAroundTime'
                },
                {
                    title: 'Performance',
                    data: 'performance'
                }
            ],
            columnDefs: [
                { "visible": false, "targets": 3 },
                {
                    "targets": 9,
                    "data": null,
                    "render": function (data, type, row, meta) {
                        if (data >= limitsRecvd.upperLimitForListView) {
                            if (type === 'display') {
                                return '<img src="/Images/QMS_Dashboard_Angular/Poor_45x45.png">';
                            }
                            else {
                                return 'Poor';
                            }
                        }
                        else if (data < limitsRecvd.upperLimitForListView) {
                            if (data < limitsRecvd.lowerLimitForListView) {
                                if (type === 'display') {
                                    return '<img src="/Images/QMS_Dashboard_Angular/Good_45x45.png">';
                                }
                                else {
                                    return 'Good';
                                }
                            }
                            else {
                                if (type === 'display') {
                                    return '<img src="/Images/QMS_Dashboard_Angular/Moderate_45x45.png">';
                                }
                                else {
                                    return 'Moderate';
                                }
                            }
                        }
                    }
                }
            ],
            // order: [[3, 'asc']],
            order: [],
            rowGroup: {
                dataSrc: "month"
            },
            responsive: true,
            scrollX: true,
            // Declare the use of the extension in the dom parameter
            dom: 'lBfrtip',
            // Configure the buttons
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: '<img src="/Images/QMS_Dashboard_Angular/excel.png">',
                    title: null,
                    exportOptions: {
                        // Any other settings used
                        grouped_array_index: 'month',
                        columns: ':visible',
                        format: {
                            body: function (data, row, column, node) {
                                if (column === 8) {
                                    if (data === '<img src="/Images/QMS_Dashboard_Angular/Poor_45x45.png">') {
                                        return 'Poor';
                                    }
                                    else if (data === '<img src="/Images/QMS_Dashboard_Angular/Moderate_45x45.png">') {
                                        return 'Moderate';
                                    }
                                    else if (data === '<img src="/Images/QMS_Dashboard_Angular/Good_45x45.png">') {
                                        return 'Good';
                                    }
                                    else
                                        return data;
                                }
                                else
                                    return data;
                            }
                        }
                    },
                    customize: function (xlsx) {
                        var sheet = xlsx.xl.worksheets['sheet1.xml'];
                        console.log(sheet);
                        // this will be used to determine addresses of cells being merged later (on export) in excel
                        let lettersAndIndexes = { 'A': 1, 'B': 2, 'C': 3, 'D': 4, 'E': 5, 'F': 6, 'G': 7, 'H': 8, 'I': 9, 'J': 10, 'K': 11, 'L': 12, 'M': 13, 'N': 14, 'O': 15, 'P': 16, 'Q': 17, 'R': 18, 'S': 19, 'T': 20, 'U': 21, 'V': 22, 'W': 23, 'X': 24, 'Y': 25, 'Z': 26 };
                        let indexesAndLetters = { 1: 'A', 2: 'B', 3: 'C', 4: 'D', 5: 'E', 6: 'F', 7: 'G', 8: 'H', 9: 'I', 10: 'J', 11: 'K', 12: 'L', 13: 'M', 14: 'N', 15: 'O', 16: 'P', 17: 'Q', 18: 'R', 19: 'S', 20: 'T', 21: 'U', 22: 'V', 23: 'W', 24: 'X', 25: 'Y', 26: 'Z' };
                        var cols = sheet.getElementsByTagName('col');
                        let numOfCols = cols.length;
                        let lastColName = getLetterCodeByNumIndex(numOfCols);
                        var rows = $('row', sheet);
                        console.log("rows", rows);
                        let totalRows = rows.length;
                        for (let i = 0; i < totalRows; i++) {
                            if (rows[i].childNodes.length === 1) {
                                let rowDet = getRowNodeHavingRowGroupHeader(rows[i]);
                                let refString = rowDet.startingCellNum + ':' + lastColName + rowDet.rowNum;
                                $(`c[r=${rowDet.startingCellNum}]`, sheet).attr('s', '7');
                                // $(`c[r=${rowDet.startingCellNum}]`,sheet).attr('s','51');
                                mergeFunction(refString);
                            }
                        }
                        // for updating performance column data styles
                        $(`row c[r^=${lastColName}]`, sheet).each(function () {
                            if ($('is t', this).text() === 'Poor') {
                                $(this).attr('s', '11');
                            }
                            else if ($('is t', this).text() === 'Good') {
                                $(this).attr('s', '16');
                            }
                            else if ($('is t', this).text() === 'Moderate') {
                                $(this).attr('s', '20');
                            }
                        });
                        console.log("after updating sheet", sheet);
                        function getLetterCodeByNumIndex(numIndex) {
                            let letterCode;
                            if (numIndex > 702) { // 3 symbols
                                let lastLetter, left, firstLetterNumber;
                                if (numIndex % 26 === 0) {
                                    lastLetter = indexesAndLetters[26];
                                    left = numIndex - 26;
                                }
                                else {
                                    lastLetter = indexesAndLetters[numIndex % 26];
                                    left = numIndex - (numIndex % 26);
                                }
                                if ((left / 676) % 1 === 0) {
                                    firstLetterNumber = Math.floor(left / 676) - 1;
                                }
                                else {
                                    firstLetterNumber = Math.floor(left / 676);
                                }
                                let firstLetter = indexesAndLetters[firstLetterNumber];
                                left = left - firstLetterNumber * 676;
                                let middleLetter = indexesAndLetters[(left / 26).toFixed(0)];
                                letterCode = firstLetter + middleLetter + lastLetter;
                            }
                            else if (numIndex > 26) { // 2 symbols
                                let lastLetter, left;
                                if (numIndex % 26 === 0) {
                                    lastLetter = indexesAndLetters[26];
                                    left = numIndex - 26;
                                }
                                else {
                                    lastLetter = indexesAndLetters[numIndex % 26];
                                    left = numIndex - (numIndex % 26);
                                }
                                let firstLetter = indexesAndLetters[(left / 26).toFixed(0)];
                                letterCode = firstLetter + lastLetter;
                            }
                            else { // 1 symbol
                                letterCode = indexesAndLetters[numIndex];
                            }
                            return letterCode;
                        }
                        function getNumIndexByLetterCode(letterCode) {
                            letterCode = letterCode.toUpperCase();
                            let numIndex;
                            switch (letterCode.length) {
                                case 1: {
                                    numIndex = lettersAndIndexes[letterCode];
                                    break;
                                }
                                case 2: {
                                    let [firstLetter, secondLetter] = letterCode.split('');
                                    numIndex = lettersAndIndexes[firstLetter] * 26 + lettersAndIndexes[secondLetter];
                                    break;
                                }
                                case 3: {
                                    let [firstLetter, secondLetter, thirdLetter] = letterCode.split('');
                                    numIndex = lettersAndIndexes[firstLetter] * 676 + lettersAndIndexes[secondLetter] * 26 + lettersAndIndexes[thirdLetter];
                                    break;
                                }
                                default:
                                    console.log(`unhandled letterCode.length: ${letterCode.length} ${letterCode}`);
                                    break;
                            }
                            return numIndex;
                        }
                        function _createNode(doc, nodeName, opts) {
                            var tempNode = doc.createElement(nodeName);
                            if (opts) {
                                if (opts.attr) {
                                    $(tempNode).attr(opts.attr);
                                }
                                if (opts.children) {
                                    $.each(opts.children, function (key, value) {
                                        tempNode.appendChild(value);
                                    });
                                }
                                if (opts.text !== null && opts.text !== undefined) {
                                    tempNode.appendChild(doc.createTextNode(opts.text));
                                }
                            }
                            return tempNode;
                        }
                        function mergeFunction(referenceString) {
                            // reference string format: 'B1:E1'
                            var mergeCells = $('mergeCells', sheet);
                            console.log("mergeCells", mergeCells);
                            if (!referenceString) {
                                alert('no reference string on merge function call');
                                return;
                            }
                            mergeCells[0].appendChild(_createNode(sheet, 'mergeCell', {
                                attr: {
                                    ref: referenceString
                                }
                            }));
                            mergeCells.attr('count', mergeCells.attr('count') + 1);
                        }
                        function getRowNodeHavingRowGroupHeader(currentRow) {
                            let rowDetails = {
                                startingCellNum: currentRow.lastChild.attributes[1].value,
                                rowNum: currentRow.attributes[0].value
                            };
                            return rowDetails;
                        }
                    },
                }
            ],
        };
    }
    getMonthsInASequenceForListView(qualityEventRecords) {
        var _a;
        for (let i = 0; i < qualityEventRecords.length; i++) {
            qualityEventRecords[i].Month = this.dashboardService.getMonth(parseInt(qualityEventRecords[i].Month));
        }
        let fromDateMonth = this.dates.fromDate.substr(3, 3);
        let fromDateYear = this.dates.fromDate.substr(7, 4);
        let toDateYear = this.dates.toDate.substr(7, 4);
        let seqArrayWithStartingMonth = [];
        let i = 0;
        let startingMonthIndexNumber;
        let otherHalfseqArray = [];
        for (i = 0; i < qualityEventRecords.length; i++) {
            if (((_a = qualityEventRecords[i]) === null || _a === void 0 ? void 0 : _a.Month) === fromDateMonth) {
                startingMonthIndexNumber = i;
                seqArrayWithStartingMonth.push(qualityEventRecords[i]);
                qualityEventRecords[i].MonthnameAndYear = qualityEventRecords[i].Month + ' - ' + fromDateYear;
                for (i = startingMonthIndexNumber + 1; i < qualityEventRecords.length; i++) {
                    seqArrayWithStartingMonth.push(qualityEventRecords[i]);
                    qualityEventRecords[i].MonthnameAndYear = qualityEventRecords[i].Month + ' - ' + fromDateYear;
                }
            }
            else {
                otherHalfseqArray.push(qualityEventRecords[i]);
                qualityEventRecords[i].MonthnameAndYear = qualityEventRecords[i].Month + ' - ' + toDateYear;
            }
        }
        let modifiedDeptPerformancedataForList = [...seqArrayWithStartingMonth, ...otherHalfseqArray];
        qualityEventRecords = modifiedDeptPerformancedataForList;
        this.records = qualityEventRecords;
    }
    onCloseListView() {
        this.bsModalRef.hide();
    }
    ngOnDestroy() {
        this.events.forEach((subscription) => {
            subscription.unsubscribe();
        });
        this.events = [];
    }
}
ListViewDeptPerformanceComponent.ɵfac = function ListViewDeptPerformanceComponent_Factory(t) { return new (t || ListViewDeptPerformanceComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_dashboards_service__WEBPACK_IMPORTED_MODULE_4__["DashboardsService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_5__["BsModalRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_5__["BsModalService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"])); };
ListViewDeptPerformanceComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ListViewDeptPerformanceComponent, selectors: [["app-list-view-dept-performance"]], viewQuery: function ListViewDeptPerformanceComponent_Query(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](angular_datatables__WEBPACK_IMPORTED_MODULE_1__["DataTableDirective"], true);
    } if (rf & 2) {
        var _t;
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.datatableElement = _t.first);
    } }, decls: 18, vars: 8, consts: [[1, "modal-header"], ["id", "dialog-sizes-name1", 1, "modal-title", "pull-left"], ["type", "button", "aria-label", "Close", 1, "close", "pull-right", 3, "click"], ["aria-hidden", "true"], [1, "modal-body"], ["datatable", "", 1, "table", "datatable_cust", "table-striped", "table-bordered", "table-sm", "row-border", "hover", 3, "dtOptions"], [4, "ngFor", "ngForOf"], [4, "ngIf"], ["type", "text", "placeholder", "Search"]], template: function ListViewDeptPerformanceComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h4", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "button", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ListViewDeptPerformanceComponent_Template_button_click_3_listener() { return ctx.onCloseListView(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "span", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "\u00D7");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "table", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "thead");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, ListViewDeptPerformanceComponent_th_10_Template, 2, 1, "th", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, ListViewDeptPerformanceComponent_tbody_11_Template, 2, 1, "tbody", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, ListViewDeptPerformanceComponent_tbody_12_Template, 2, 1, "tbody", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, ListViewDeptPerformanceComponent_tbody_13_Template, 2, 1, "tbody", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, ListViewDeptPerformanceComponent_tbody_14_Template, 2, 1, "tbody", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "tfoot");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, ListViewDeptPerformanceComponent_td_17_Template, 2, 0, "td", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.currentDrillDownHeaderText);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("dtOptions", ctx.dtOptions);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.dtOptions.columns);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.qualityEvent === ctx.qualityEventsNames.ChangeControlNote);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.qualityEvent === ctx.qualityEventsNames.IncidentReport);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.qualityEvent === ctx.qualityEventsNames.Errata);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.qualityEvent === ctx.qualityEventsNames.NoteToFile);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.dtOptions.columns);
    } }, directives: [angular_datatables__WEBPACK_IMPORTED_MODULE_1__["DataTableDirective"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"]], styles: [".hideChart[_ngcontent-%COMP%] {\n  display: none;\n}\n\n.button-container[_ngcontent-%COMP%] {\n  text-align: center;\n  height: 40px;\n  position: absolute;\n  top: 7px;\n  left: 0px;\n}\n\n  .ng2-flatpickr-input-container .form-control[readonly]:not([disabled]) {\n  background-color: transparent !important;\n}\n\n  .tooltip-header {\n  margin-bottom: 5px;\n  font-size: 16px;\n  font-weight: 500;\n  padding-bottom: 5px;\n  border-bottom: 1px solid #c5c5c5;\n}\n\n  .tooltip-body {\n  width: 170px;\n}\n\n  .tooltip-body .series-name {\n  font-weight: normal;\n  opacity: 0.6;\n  display: inline-block;\n  line-height: 1.5;\n  padding-right: 10px;\n  width: 126px;\n}\n\n  .tooltip-body .value-text {\n  display: inline-block;\n  line-height: 1.5;\n  width: 30px;\n}\n\n  .dxc-val-title text {\n  font-size: 10pt !important;\n}\n\n  .multiselect-dropdown .dropdown-btn .selected-item {\n  font-size: 10pt !important;\n  margin-top: 4px !important;\n}\n\n  .dxc-title text {\n  font-family: seguisb !important;\n  font-size: 13pt !important;\n}\n\n.upload_btn[_ngcontent-%COMP%] {\n  border: 1px solid #094353;\n  border-radius: 4px;\n  background-color: #fff;\n}\n\n.upload_btn[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  padding: 5px;\n}\n\n.close[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  color: black;\n}\n\n.table[_ngcontent-%COMP%]   th[_ngcontent-%COMP%], .table[_ngcontent-%COMP%]   td[_ngcontent-%COMP%] {\n  padding: 10px;\n  vertical-align: top;\n  border-top: 1px solid #dee2e6;\n  font-size: 10pt !important;\n  text-align: center;\n  font-weight: normal;\n}\n\n.datatable_cust[_ngcontent-%COMP%]   tfoot[_ngcontent-%COMP%]   td[_ngcontent-%COMP%] {\n  border-right: 1px solid #c3c3c3;\n  border-bottom: 1px solid #c3c3c3;\n  background: #38b8ba;\n  color: #fff;\n  font-family: seguisb;\n  text-align: center;\n  font-size: 10pt;\n}\n\n.datatable_cust[_ngcontent-%COMP%]   tfoot[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\n  width: 100%;\n  font-size: 10pt !important;\n  padding: 2px 10px;\n  border-radius: 4px;\n  background: transparent;\n  color: #fff;\n  font-family: seguisb;\n  border: 1px solid #fff !important;\n}\n\ndiv.dataTables_wrapper[_ngcontent-%COMP%]   div.dataTables_paginate[_ngcontent-%COMP%]   ul.pagination[_ngcontent-%COMP%] {\n  margin: 2px 0;\n  white-space: nowrap;\n  justify-content: flex-end;\n  font-size: 10pt;\n}\n\n  .page-item.active .page-link {\n  z-index: 1;\n  color: #fff !important;\n  background-color: #38b8ba !important;\n  border-color: #38b8ba !important;\n}\n\n[_ngcontent-%COMP%]:ng-deep   button.dt-button[_ngcontent-%COMP%] {\n  background-color: transparent !important;\n  background: none !important;\n}\n\n  .square {\n  height: 20px;\n  width: 20px;\n  margin-left: auto;\n  margin-right: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkcy9tYW5hZ2VtZW50LWxldmVsLWNoYXJ0L21hbmFnZW1lbnQtbGV2ZWwtY2hhcnQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0FBQ0o7O0FBT0k7RUFDSSx3Q0FBQTtBQUpSOztBQVFBO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdDQUFBO0FBTEo7O0FBUUE7RUFDSSxZQUFBO0FBTEo7O0FBUUE7RUFDSSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0FBTEo7O0FBUUE7RUFDSSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtBQUxKOztBQVNJO0VBQ0ksMEJBQUE7QUFOUjs7QUFVQTtFQUNJLDBCQUFBO0VBQ0EsMEJBQUE7QUFQSjs7QUFXSTtFQUNFLCtCQUFBO0VBQ0EsMEJBQUE7QUFSTjs7QUFZQTtFQUNFLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxzQkFBQTtBQVRGOztBQVdBO0VBQ0UsWUFBQTtBQVJGOztBQVlJO0VBQ0ksWUFBQTtBQVRSOztBQWFBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsNkJBQUE7RUFDQSwwQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUFWSjs7QUFhQTtFQUNJLCtCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQVZKOztBQWFBO0VBQ0ksV0FBQTtFQUNBLDBCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHVCQUFBO0VBQ0EsV0FBQTtFQUNBLG9CQUFBO0VBQ0EsaUNBQUE7QUFWSjs7QUFhQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZUFBQTtBQVZKOztBQWFBO0VBQ0ksVUFBQTtFQUNBLHNCQUFBO0VBQ0Esb0NBQUE7RUFDQSxnQ0FBQTtBQVZKOztBQWFBO0VBQ0ksd0NBQUE7RUFDQSwyQkFBQTtBQVZKOztBQWFBO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBVkoiLCJmaWxlIjoic3JjL2FwcC9kYXNoYm9hcmRzL21hbmFnZW1lbnQtbGV2ZWwtY2hhcnQvbWFuYWdlbWVudC1sZXZlbC1jaGFydC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oaWRlQ2hhcnR7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG59XHJcblxyXG4uYnV0dG9uLWNvbnRhaW5lciB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDdweDtcclxuICAgIGxlZnQ6IDBweDtcclxufVxyXG5cclxuLy8gLnF1YWRyYW50e1xyXG4vLyAgICAgYm9yZGVyOjJweCBzb2xpZCBibGFjaztcclxuLy8gfVxyXG5cclxuOjpuZy1kZWVwIC5uZzItZmxhdHBpY2tyLWlucHV0LWNvbnRhaW5lcntcclxuICAgIC5mb3JtLWNvbnRyb2xbcmVhZG9ubHldOm5vdChbZGlzYWJsZWRdKXtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG59XHJcblxyXG46Om5nLWRlZXAgLnRvb2x0aXAtaGVhZGVyIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNjNWM1YzU7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAudG9vbHRpcC1ib2R5IHtcclxuICAgIHdpZHRoOiAxNzBweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC50b29sdGlwLWJvZHkgLnNlcmllcy1uYW1lIHtcclxuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICBvcGFjaXR5OiAwLjY7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBsaW5lLWhlaWdodDogMS41O1xyXG4gICAgcGFkZGluZy1yaWdodDogMTBweDtcclxuICAgIHdpZHRoOiAxMjZweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC50b29sdGlwLWJvZHkgLnZhbHVlLXRleHQge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgbGluZS1oZWlnaHQ6IDEuNTtcclxuICAgIHdpZHRoOiAzMHB4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLmR4Yy12YWwtdGl0bGV7XHJcbiAgICB0ZXh0e1xyXG4gICAgICAgIGZvbnQtc2l6ZToxMHB0ICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubXVsdGlzZWxlY3QtZHJvcGRvd24gLmRyb3Bkb3duLWJ0biAuc2VsZWN0ZWQtaXRlbXtcclxuICAgIGZvbnQtc2l6ZToxMHB0ICFpbXBvcnRhbnQ7XHJcbiAgICBtYXJnaW4tdG9wOiA0cHggIWltcG9ydGFudDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5keGMtdGl0bGV7XHJcbiAgICB0ZXh0e1xyXG4gICAgICBmb250LWZhbWlseTogc2VndWlzYiAhaW1wb3J0YW50O1xyXG4gICAgICBmb250LXNpemU6IDEzcHQgIWltcG9ydGFudDtcclxuICAgIH1cclxufVxyXG5cclxuLnVwbG9hZF9idG4ge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICMwOTQzNTM7XHJcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbn1cclxuLnVwbG9hZF9idG4gaW1nIHtcclxuICBwYWRkaW5nOiA1cHg7XHJcbn1cclxuXHJcbi5jbG9zZXtcclxuICAgIHNwYW57XHJcbiAgICAgICAgY29sb3I6YmxhY2s7XHJcbiAgICB9XHJcbn1cclxuXHJcbi50YWJsZSB0aCwgLnRhYmxlIHRkIHtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xyXG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNkZWUyZTY7XHJcbiAgICBmb250LXNpemU6IDEwcHQhaW1wb3J0YW50O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxufVxyXG5cclxuLmRhdGF0YWJsZV9jdXN0IHRmb290IHRkIHtcclxuICAgIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICNjM2MzYzM7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2MzYzNjMztcclxuICAgIGJhY2tncm91bmQ6ICMzOGI4YmE7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIGZvbnQtZmFtaWx5OiBzZWd1aXNiO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAxMHB0O1xyXG59XHJcblxyXG4uZGF0YXRhYmxlX2N1c3QgdGZvb3QgaW5wdXQge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBmb250LXNpemU6IDEwcHQhaW1wb3J0YW50O1xyXG4gICAgcGFkZGluZzogMnB4IDEwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgZm9udC1mYW1pbHk6IHNlZ3Vpc2I7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZmZmIWltcG9ydGFudDtcclxufVxyXG5cclxuZGl2LmRhdGFUYWJsZXNfd3JhcHBlciBkaXYuZGF0YVRhYmxlc19wYWdpbmF0ZSB1bC5wYWdpbmF0aW9uIHtcclxuICAgIG1hcmdpbjogMnB4IDA7XHJcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuICAgIGZvbnQtc2l6ZTogMTBwdDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5wYWdlLWl0ZW0uYWN0aXZlIC5wYWdlLWxpbmsge1xyXG4gICAgei1pbmRleDogMTtcclxuICAgIGNvbG9yOiAjZmZmIWltcG9ydGFudDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzOGI4YmEhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjMzhiOGJhICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbjpuZy1kZWVwIGJ1dHRvbi5kdC1idXR0b257XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xyXG4gICAgYmFja2dyb3VuZDogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLnNxdWFyZXtcclxuICAgIGhlaWdodDogMjBweDtcclxuICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6YXV0bztcclxuICAgIG1hcmdpbi1yaWdodDphdXRvXHJcbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ListViewDeptPerformanceComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-list-view-dept-performance',
                template: `
  <div class="modal-header">
       <h4 id="dialog-sizes-name1" class="modal-title pull-left">{{currentDrillDownHeaderText}}</h4>
       <button type="button" class="close pull-right" (click)="onCloseListView()" aria-label="Close">
         <span aria-hidden="true">&times;</span>
       </button>
     </div>
     <div class="modal-body">
   <table datatable [dtOptions]="dtOptions" class="table datatable_cust table-striped table-bordered table-sm row-border hover">
           <thead>
               <tr >
                 <th *ngFor="let eachCol of dtOptions.columns">{{eachCol.title}}</th>
               </tr>
               
             </thead>
             <tbody *ngIf="qualityEvent===qualityEventsNames.ChangeControlNote">
             
               <tr *ngFor="let record of records; let i=index">
                
                 <td>{{ record.CCN_No }}</td>
                 <td>{{ record.DepartmentName }}</td>
                 <td>{{ record.CreatedBy }}</td>
                 <td>{{ record.MonthnameAndYear }}</td>
                 <td>{{ record.CreatedDate }}</td>
                 <td>{{ record.DueDate }}</td>
                 <td>{{ record.CompletedDate }}</td>
                 <td>{{ record.ChangeClassification }}</td>
                 <td>{{ record.TAT }}</td>
                 <td>{{ record.TAT }}</td>
               </tr>
               
             </tbody>
             <tbody *ngIf="qualityEvent===qualityEventsNames.IncidentReport">
               <tr *ngFor="let record of records">
                 <td>{{ record.Incident_No }}</td>
                 <td>{{ record.DepartmentName }}</td>
                 <td>{{ record.TypeofIncident }}</td>
                 <td>{{ record.MonthnameAndYear }}</td>
                 <td>{{ record.Category }}</td>
                 <td>{{ record.CreatedBy }}</td>
                 <td>{{ record.DateOfOccurance }}</td>
                 <td>{{ record.DateOfReport }}</td>
                 <td>{{ record.DueDate }}</td>
                 <td>{{ record.CompletedDate }}</td>
                 <td>{{ record.TAT }}</td>
                 <td>{{ record.TAT }}</td>
               </tr>
             </tbody>
             <tbody *ngIf="qualityEvent===qualityEventsNames.Errata">
               <tr *ngFor="let record of records">
                 <td>{{ record.Errata_No }}</td>
                 <td>{{ record.DepartmentName }}</td>
                 <td>{{ record.DocName }}</td>
                 <td>{{ record.MonthnameAndYear }}</td>
                 <td>{{ record.refdocno }}</td>
                 <td>{{ record.CreatedBy }}</td>
                 <td>{{ record.CreatedDate }}</td>
                 <td>{{ record.CompletedDate }}</td>
                 <td>{{ record.TAT }}</td>
                 <td>{{ record.TAT }}</td>
               </tr>
             </tbody>
             <tbody *ngIf="qualityEvent===qualityEventsNames.NoteToFile">
               <tr *ngFor="let record of records">
                 <td>{{ record.NTF_No }}</td>
                 <td>{{ record.DepartmentName }}</td>
                 <td>{{ record.DocName }}</td>
                 <td>{{ record.MonthnameAndYear }}</td>
                 <td>{{ record.refdocno }}</td>
                 <td>{{ record.CreatedBy }}</td>
                 <td>{{ record.CreatedDate }}</td>
                 <td>{{ record.CompletedDate }}</td>
                 <td>{{ record.TAT }}</td>
                 <td>{{ record.TAT }}</td>
               </tr>
             </tbody>
             <tfoot>
             <tr >
             <td *ngFor="let eachC of dtOptions.columns"><input type="text" placeholder="Search"></td>
             </tr>
             </tfoot>
       </table>
     </div>`,
                styleUrls: ["../management-level-chart/management-level-chart.component.scss"
                ]
            }]
    }], function () { return [{ type: src_app_services_dashboards_service__WEBPACK_IMPORTED_MODULE_4__["DashboardsService"] }, { type: ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_5__["BsModalRef"] }, { type: ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_5__["BsModalService"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"] }]; }, { datatableElement: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
            args: [angular_datatables__WEBPACK_IMPORTED_MODULE_1__["DataTableDirective"]]
        }] }); })();


/***/ }),

/***/ "Oj50":
/*!*************************************************************!*\
  !*** ./src/app/dashboards/list-view/list-view.component.ts ***!
  \*************************************************************/
/*! exports provided: ListViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListViewComponent", function() { return ListViewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angular-datatables */ "oTcB");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var src_app_models_dashboards__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/models/dashboards */ "+E+c");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/modal */ "LqlI");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "SVse");








function ListViewComponent_th_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const eachCol_r6 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](eachCol_r6.title);
} }
function ListViewComponent_tbody_11_tr_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const record_r8 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r8.CCN_No);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r8.DepartmentName);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r8.CreatedBy);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r8.CreatedDate);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r8.DueDate);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r8.NumberofDueDays);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r8.ChangeClassification);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r8.StatusName);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r8.assignTo);
} }
function ListViewComponent_tbody_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tbody");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ListViewComponent_tbody_11_tr_1_Template, 19, 9, "tr", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r1.records);
} }
function ListViewComponent_tbody_12_tr_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const record_r10 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r10.Incident_No);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r10.DepartmentName);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r10.TypeofIncident);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r10.Category);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r10.CreatedBy);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r10.DateOfOccurance);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r10.DateOfReport);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r10.DueDate);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r10.NumberofDueDays);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r10.StatusName);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r10.assignTo);
} }
function ListViewComponent_tbody_12_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tbody");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ListViewComponent_tbody_12_tr_1_Template, 23, 11, "tr", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r2.records);
} }
function ListViewComponent_tbody_13_tr_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const record_r12 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r12.Errata_No);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r12.DepartmentName);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r12.Docname);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r12.referencedocno);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r12.CreatedBy);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r12.CreatedDate);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r12.StatusName);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r12.assignTo);
} }
function ListViewComponent_tbody_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tbody");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ListViewComponent_tbody_13_tr_1_Template, 17, 8, "tr", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r3.records);
} }
function ListViewComponent_tbody_14_tr_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const record_r14 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r14.NTF_No);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r14.DepartmentName);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r14.Docname);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r14.referencedocno);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r14.CreatedBy);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r14.CreatedDate);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r14.StatusName);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](record_r14.assignTo);
} }
function ListViewComponent_tbody_14_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tbody");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ListViewComponent_tbody_14_tr_1_Template, 17, 8, "tr", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r4.records);
} }
function ListViewComponent_td_17_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "input", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
class ListViewComponent {
    constructor(bsModalRef, modalService, changeDetection, renderer) {
        this.bsModalRef = bsModalRef;
        this.modalService = modalService;
        this.changeDetection = changeDetection;
        this.renderer = renderer;
        this.dtOptions = {};
        this.CCNdtOptions = {};
        this.incidentDtOptions = {};
        this.errataDtOptions = {};
        this.noteToFileDtOptions = {};
        this.qualityEventsNames = src_app_models_dashboards__WEBPACK_IMPORTED_MODULE_3__["QualityEvents"];
        this.events = [];
    }
    ngOnInit() {
        this.CCNdtOptions = {
            columns: [{
                    title: 'CCN Number',
                    data: 'ccnNumber'
                }, {
                    title: 'Department',
                    data: 'department'
                }, {
                    title: 'Created By',
                    data: 'createdBy'
                },
                {
                    title: 'Created Date',
                    data: 'createdDate'
                },
                {
                    title: 'Due Date',
                    data: 'dueDate'
                },
                {
                    title: 'No. of Due Days',
                    data: 'dueDays',
                },
                {
                    title: 'Classification',
                    data: 'classification'
                },
                {
                    title: 'Status',
                    data: 'status'
                },
                {
                    title: 'Assign To',
                    data: 'assignTo'
                }
            ],
            columnDefs: [
                { "visible": (this.showType > 5 && this.showType < 12), "targets": 5 }
            ],
            responsive: true,
            scrollX: true,
            // Declare the use of the extension in the dom parameter
            dom: 'lBfrtip',
            // Configure the buttons
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: '<img src="/Images/QMS_Dashboard_Angular/excel.png">',
                    title: null
                }
            ],
        };
        this.incidentDtOptions = {
            columns: [{
                    title: 'Incident_No',
                    data: 'incidentNumber'
                }, {
                    title: 'Department',
                    data: 'department'
                },
                {
                    title: 'Type of Incident',
                    data: 'typeOfIncident'
                },
                {
                    title: 'Category',
                    data: 'category'
                },
                {
                    title: 'Created By',
                    data: 'createdBy'
                },
                {
                    title: 'Date of Occurence',
                    data: 'dateOfOccurance'
                },
                {
                    title: 'Date of Report',
                    data: 'dateOfReport'
                },
                {
                    title: 'Due Date',
                    data: 'dueDate'
                },
                {
                    title: 'No. of Due Days',
                    data: 'dueDays',
                },
                {
                    title: 'Status',
                    data: 'status'
                },
                {
                    title: 'Assign To',
                    data: 'assignTo'
                }
            ],
            columnDefs: [
                { "visible": (this.showType > 5 && this.showType < 12), "targets": 8 }
            ],
            responsive: true,
            scrollX: true,
            // Declare the use of the extension in the dom parameter
            dom: 'lBfrtip',
            // Configure the buttons
            buttons: [
                {
                    extend: 'excel',
                    text: '<img src="/Images/QMS_Dashboard_Angular/excel.png">',
                    title: null
                }
            ],
        };
        this.errataDtOptions = {
            columns: [{
                    title: 'Errata Number',
                    data: 'errataNumber'
                }, {
                    title: 'Department',
                    data: 'department'
                },
                {
                    title: 'Document',
                    data: 'document'
                },
                {
                    title: 'Ref. Doc No.',
                    data: 'refDocNum'
                }, {
                    title: 'Created By',
                    data: 'createdBy'
                },
                {
                    title: 'Created Date',
                    data: 'createdDate'
                },
                {
                    title: 'Status',
                    data: 'status'
                },
                {
                    title: 'Assign To',
                    data: 'assignTo'
                }
            ],
            responsive: true,
            scrollX: true,
            // Declare the use of the extension in the dom parameter
            dom: 'lBfrtip',
            // Configure the buttons
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: '<img src="/Images/QMS_Dashboard_Angular/excel.png">',
                    title: null
                }
            ],
        };
        this.noteToFileDtOptions = {
            columns: [{
                    title: 'NTF Number',
                    data: 'noteToFileNumber'
                }, {
                    title: 'Department',
                    data: 'department'
                },
                {
                    title: 'Document',
                    data: 'document'
                },
                {
                    title: 'Ref. Doc No.',
                    data: 'refDocNum'
                }, {
                    title: 'Created By',
                    data: 'createdBy'
                },
                {
                    title: 'Created Date',
                    data: 'createdDate'
                },
                {
                    title: 'Status',
                    data: 'status'
                },
                {
                    title: 'Assign To',
                    data: 'assignTo'
                }
            ],
            responsive: true,
            scrollX: true,
            // Declare the use of the extension in the dom parameter
            dom: 'lBfrtip',
            // Configure the buttons
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: '<img src="/Images/QMS_Dashboard_Angular/excel.png">',
                    title: null
                }
            ],
        };
        var trimmedQualityEventName = '';
        if (this.qualityEvent === src_app_models_dashboards__WEBPACK_IMPORTED_MODULE_3__["QualityEvents"].ChangeControlNote) {
            trimmedQualityEventName = 'CCN';
            this.dtOptions = this.CCNdtOptions;
            this.records = this.ccnRecords;
        }
        else if (this.qualityEvent === src_app_models_dashboards__WEBPACK_IMPORTED_MODULE_3__["QualityEvents"].IncidentReport) {
            trimmedQualityEventName = 'IR';
            this.dtOptions = this.incidentDtOptions;
            this.records = this.incidentRecords;
        }
        else if (this.qualityEvent === src_app_models_dashboards__WEBPACK_IMPORTED_MODULE_3__["QualityEvents"].NoteToFile) {
            trimmedQualityEventName = 'NTF';
            this.dtOptions = this.noteToFileDtOptions;
            this.records = this.noteToFileRecords;
        }
        else {
            trimmedQualityEventName = 'Errata';
            this.dtOptions = this.errataDtOptions;
            this.records = this.errataRecords;
        }
        if (this.showType > 5 && this.showType < 12) {
            this.dtOptions.buttons[0].filename = 'OverDue' + ' - ' + trimmedQualityEventName + this.fileName;
        }
        else {
            this.dtOptions.buttons[0].filename = trimmedQualityEventName + this.fileName;
        }
        // let excelButton = document.getElementsByClassName('buttons-html5');
        // console.log(excelButton);
        //     this.renderer.addClass(excelButton[0],'btn btn-outline');
    }
    ngAfterViewInit() {
        const _combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["combineLatest"])([this.modalService.onShow,
            this.modalService.onShown,
            this.modalService.onHide,
            this.modalService.onHidden]).subscribe(() => this.changeDetection.markForCheck());
        this.events.push(this.modalService.onShown.subscribe((reason) => {
            this.datatableElement.dtInstance.then((dtInstance) => {
                dtInstance.columns().every(function () {
                    const that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this['value']) {
                            that
                                .search(this['value'])
                                .draw();
                        }
                    });
                });
            });
            // }
            // else{
            // this.dtTrigger.next();
            // }
        }));
        this.events.push(_combine);
    }
    onCloseListView() {
        this.bsModalRef.hide();
    }
    ngOnDestroy() {
        this.events.forEach((subscription) => {
            subscription.unsubscribe();
        });
        this.events = [];
    }
}
ListViewComponent.ɵfac = function ListViewComponent_Factory(t) { return new (t || ListViewComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_4__["BsModalRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_4__["BsModalService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"])); };
ListViewComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ListViewComponent, selectors: [["app-list-view"]], viewQuery: function ListViewComponent_Query(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](angular_datatables__WEBPACK_IMPORTED_MODULE_1__["DataTableDirective"], true);
    } if (rf & 2) {
        var _t;
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.datatableElement = _t.first);
    } }, decls: 18, vars: 8, consts: [[1, "modal-header"], ["id", "dialog-sizes-name1", 1, "modal-title", "pull-left"], ["type", "button", "aria-label", "Close", 1, "close", "pull-right", 3, "click"], ["aria-hidden", "true"], [1, "modal-body"], ["datatable", "", 1, "table", "datatable_cust", "table-striped", "table-bordered", "table-sm", "row-border", "hover", 3, "dtOptions"], [4, "ngFor", "ngForOf"], [4, "ngIf"], ["type", "text", "placeholder", "Search"]], template: function ListViewComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h4", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "button", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ListViewComponent_Template_button_click_3_listener() { return ctx.onCloseListView(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "span", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "\u00D7");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "table", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "thead");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, ListViewComponent_th_10_Template, 2, 1, "th", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, ListViewComponent_tbody_11_Template, 2, 1, "tbody", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, ListViewComponent_tbody_12_Template, 2, 1, "tbody", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, ListViewComponent_tbody_13_Template, 2, 1, "tbody", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, ListViewComponent_tbody_14_Template, 2, 1, "tbody", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "tfoot");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, ListViewComponent_td_17_Template, 2, 0, "td", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.currentDrillDownHeaderText);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("dtOptions", ctx.dtOptions);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.dtOptions.columns);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.qualityEvent === ctx.qualityEventsNames.ChangeControlNote);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.qualityEvent === ctx.qualityEventsNames.IncidentReport);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.qualityEvent === ctx.qualityEventsNames.Errata);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.qualityEvent === ctx.qualityEventsNames.NoteToFile);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.dtOptions.columns);
    } }, directives: [angular_datatables__WEBPACK_IMPORTED_MODULE_1__["DataTableDirective"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"]], styles: [".hideChart[_ngcontent-%COMP%] {\n  display: none;\n}\n\n.button-container[_ngcontent-%COMP%] {\n  text-align: center;\n  height: 40px;\n  position: absolute;\n  top: 7px;\n  left: 0px;\n}\n\n  .ng2-flatpickr-input-container .form-control[readonly]:not([disabled]) {\n  background-color: transparent !important;\n}\n\n  .tooltip-header {\n  margin-bottom: 5px;\n  font-size: 16px;\n  font-weight: 500;\n  padding-bottom: 5px;\n  border-bottom: 1px solid #c5c5c5;\n}\n\n  .tooltip-body {\n  width: 170px;\n}\n\n  .tooltip-body .series-name {\n  font-weight: normal;\n  opacity: 0.6;\n  display: inline-block;\n  line-height: 1.5;\n  padding-right: 10px;\n  width: 126px;\n}\n\n  .tooltip-body .value-text {\n  display: inline-block;\n  line-height: 1.5;\n  width: 30px;\n}\n\n  .dxc-val-title text {\n  font-size: 10pt !important;\n}\n\n  .multiselect-dropdown .dropdown-btn .selected-item {\n  font-size: 10pt !important;\n  margin-top: 4px !important;\n}\n\n  .dxc-title text {\n  font-family: seguisb !important;\n  font-size: 13pt !important;\n}\n\n.upload_btn[_ngcontent-%COMP%] {\n  border: 1px solid #094353;\n  border-radius: 4px;\n  background-color: #fff;\n}\n\n.upload_btn[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  padding: 5px;\n}\n\n.close[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  color: black;\n}\n\n.table[_ngcontent-%COMP%]   th[_ngcontent-%COMP%], .table[_ngcontent-%COMP%]   td[_ngcontent-%COMP%] {\n  padding: 10px;\n  vertical-align: top;\n  border-top: 1px solid #dee2e6;\n  font-size: 10pt !important;\n  text-align: center;\n  font-weight: normal;\n}\n\n.datatable_cust[_ngcontent-%COMP%]   tfoot[_ngcontent-%COMP%]   td[_ngcontent-%COMP%] {\n  border-right: 1px solid #c3c3c3;\n  border-bottom: 1px solid #c3c3c3;\n  background: #38b8ba;\n  color: #fff;\n  font-family: seguisb;\n  text-align: center;\n  font-size: 10pt;\n}\n\n.datatable_cust[_ngcontent-%COMP%]   tfoot[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\n  width: 100%;\n  font-size: 10pt !important;\n  padding: 2px 10px;\n  border-radius: 4px;\n  background: transparent;\n  color: #fff;\n  font-family: seguisb;\n  border: 1px solid #fff !important;\n}\n\ndiv.dataTables_wrapper[_ngcontent-%COMP%]   div.dataTables_paginate[_ngcontent-%COMP%]   ul.pagination[_ngcontent-%COMP%] {\n  margin: 2px 0;\n  white-space: nowrap;\n  justify-content: flex-end;\n  font-size: 10pt;\n}\n\n  .page-item.active .page-link {\n  z-index: 1;\n  color: #fff !important;\n  background-color: #38b8ba !important;\n  border-color: #38b8ba !important;\n}\n\n[_ngcontent-%COMP%]:ng-deep   button.dt-button[_ngcontent-%COMP%] {\n  background-color: transparent !important;\n  background: none !important;\n}\n\n  .square {\n  height: 20px;\n  width: 20px;\n  margin-left: auto;\n  margin-right: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkcy9tYW5hZ2VtZW50LWxldmVsLWNoYXJ0L21hbmFnZW1lbnQtbGV2ZWwtY2hhcnQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0FBQ0o7O0FBT0k7RUFDSSx3Q0FBQTtBQUpSOztBQVFBO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdDQUFBO0FBTEo7O0FBUUE7RUFDSSxZQUFBO0FBTEo7O0FBUUE7RUFDSSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0FBTEo7O0FBUUE7RUFDSSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtBQUxKOztBQVNJO0VBQ0ksMEJBQUE7QUFOUjs7QUFVQTtFQUNJLDBCQUFBO0VBQ0EsMEJBQUE7QUFQSjs7QUFXSTtFQUNFLCtCQUFBO0VBQ0EsMEJBQUE7QUFSTjs7QUFZQTtFQUNFLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxzQkFBQTtBQVRGOztBQVdBO0VBQ0UsWUFBQTtBQVJGOztBQVlJO0VBQ0ksWUFBQTtBQVRSOztBQWFBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsNkJBQUE7RUFDQSwwQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUFWSjs7QUFhQTtFQUNJLCtCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQVZKOztBQWFBO0VBQ0ksV0FBQTtFQUNBLDBCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHVCQUFBO0VBQ0EsV0FBQTtFQUNBLG9CQUFBO0VBQ0EsaUNBQUE7QUFWSjs7QUFhQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZUFBQTtBQVZKOztBQWFBO0VBQ0ksVUFBQTtFQUNBLHNCQUFBO0VBQ0Esb0NBQUE7RUFDQSxnQ0FBQTtBQVZKOztBQWFBO0VBQ0ksd0NBQUE7RUFDQSwyQkFBQTtBQVZKOztBQWFBO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBVkoiLCJmaWxlIjoic3JjL2FwcC9kYXNoYm9hcmRzL21hbmFnZW1lbnQtbGV2ZWwtY2hhcnQvbWFuYWdlbWVudC1sZXZlbC1jaGFydC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oaWRlQ2hhcnR7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG59XHJcblxyXG4uYnV0dG9uLWNvbnRhaW5lciB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDdweDtcclxuICAgIGxlZnQ6IDBweDtcclxufVxyXG5cclxuLy8gLnF1YWRyYW50e1xyXG4vLyAgICAgYm9yZGVyOjJweCBzb2xpZCBibGFjaztcclxuLy8gfVxyXG5cclxuOjpuZy1kZWVwIC5uZzItZmxhdHBpY2tyLWlucHV0LWNvbnRhaW5lcntcclxuICAgIC5mb3JtLWNvbnRyb2xbcmVhZG9ubHldOm5vdChbZGlzYWJsZWRdKXtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG59XHJcblxyXG46Om5nLWRlZXAgLnRvb2x0aXAtaGVhZGVyIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNjNWM1YzU7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAudG9vbHRpcC1ib2R5IHtcclxuICAgIHdpZHRoOiAxNzBweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC50b29sdGlwLWJvZHkgLnNlcmllcy1uYW1lIHtcclxuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICBvcGFjaXR5OiAwLjY7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBsaW5lLWhlaWdodDogMS41O1xyXG4gICAgcGFkZGluZy1yaWdodDogMTBweDtcclxuICAgIHdpZHRoOiAxMjZweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC50b29sdGlwLWJvZHkgLnZhbHVlLXRleHQge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgbGluZS1oZWlnaHQ6IDEuNTtcclxuICAgIHdpZHRoOiAzMHB4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLmR4Yy12YWwtdGl0bGV7XHJcbiAgICB0ZXh0e1xyXG4gICAgICAgIGZvbnQtc2l6ZToxMHB0ICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubXVsdGlzZWxlY3QtZHJvcGRvd24gLmRyb3Bkb3duLWJ0biAuc2VsZWN0ZWQtaXRlbXtcclxuICAgIGZvbnQtc2l6ZToxMHB0ICFpbXBvcnRhbnQ7XHJcbiAgICBtYXJnaW4tdG9wOiA0cHggIWltcG9ydGFudDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5keGMtdGl0bGV7XHJcbiAgICB0ZXh0e1xyXG4gICAgICBmb250LWZhbWlseTogc2VndWlzYiAhaW1wb3J0YW50O1xyXG4gICAgICBmb250LXNpemU6IDEzcHQgIWltcG9ydGFudDtcclxuICAgIH1cclxufVxyXG5cclxuLnVwbG9hZF9idG4ge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICMwOTQzNTM7XHJcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbn1cclxuLnVwbG9hZF9idG4gaW1nIHtcclxuICBwYWRkaW5nOiA1cHg7XHJcbn1cclxuXHJcbi5jbG9zZXtcclxuICAgIHNwYW57XHJcbiAgICAgICAgY29sb3I6YmxhY2s7XHJcbiAgICB9XHJcbn1cclxuXHJcbi50YWJsZSB0aCwgLnRhYmxlIHRkIHtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xyXG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNkZWUyZTY7XHJcbiAgICBmb250LXNpemU6IDEwcHQhaW1wb3J0YW50O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxufVxyXG5cclxuLmRhdGF0YWJsZV9jdXN0IHRmb290IHRkIHtcclxuICAgIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICNjM2MzYzM7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2MzYzNjMztcclxuICAgIGJhY2tncm91bmQ6ICMzOGI4YmE7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIGZvbnQtZmFtaWx5OiBzZWd1aXNiO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAxMHB0O1xyXG59XHJcblxyXG4uZGF0YXRhYmxlX2N1c3QgdGZvb3QgaW5wdXQge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBmb250LXNpemU6IDEwcHQhaW1wb3J0YW50O1xyXG4gICAgcGFkZGluZzogMnB4IDEwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgZm9udC1mYW1pbHk6IHNlZ3Vpc2I7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZmZmIWltcG9ydGFudDtcclxufVxyXG5cclxuZGl2LmRhdGFUYWJsZXNfd3JhcHBlciBkaXYuZGF0YVRhYmxlc19wYWdpbmF0ZSB1bC5wYWdpbmF0aW9uIHtcclxuICAgIG1hcmdpbjogMnB4IDA7XHJcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuICAgIGZvbnQtc2l6ZTogMTBwdDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5wYWdlLWl0ZW0uYWN0aXZlIC5wYWdlLWxpbmsge1xyXG4gICAgei1pbmRleDogMTtcclxuICAgIGNvbG9yOiAjZmZmIWltcG9ydGFudDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzOGI4YmEhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjMzhiOGJhICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbjpuZy1kZWVwIGJ1dHRvbi5kdC1idXR0b257XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xyXG4gICAgYmFja2dyb3VuZDogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLnNxdWFyZXtcclxuICAgIGhlaWdodDogMjBweDtcclxuICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6YXV0bztcclxuICAgIG1hcmdpbi1yaWdodDphdXRvXHJcbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ListViewComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-list-view',
                template: `
<div class="modal-header">
       <h4 id="dialog-sizes-name1" class="modal-title pull-left">{{currentDrillDownHeaderText}}</h4>
       <button type="button" class="close pull-right" (click)="onCloseListView()" aria-label="Close">
         <span aria-hidden="true">&times;</span>
       </button>
     </div>
     <div class="modal-body">
   <table datatable [dtOptions]="dtOptions" class="table datatable_cust table-striped table-bordered table-sm row-border hover">
           <thead>
               <tr >
                 <th *ngFor="let eachCol of dtOptions.columns">{{eachCol.title}}</th>
               </tr>
               
             </thead>
             <tbody *ngIf="qualityEvent===qualityEventsNames.ChangeControlNote">
               <tr *ngFor="let record of records">
                 <td>{{ record.CCN_No }}</td>
                 <td>{{ record.DepartmentName }}</td>
                 <td>{{ record.CreatedBy }}</td>
                 <td>{{ record.CreatedDate }}</td>
                 <td>{{ record.DueDate }}</td>
                 <td>{{ record.NumberofDueDays }}</td>
                 <td>{{ record.ChangeClassification }}</td>
                 <td>{{ record.StatusName }}</td>
                 <td>{{ record.assignTo }}</td>
               </tr>
             </tbody>
             <tbody *ngIf="qualityEvent===qualityEventsNames.IncidentReport">
               <tr *ngFor="let record of records">
                 <td>{{ record.Incident_No }}</td>
                 <td>{{ record.DepartmentName }}</td>
                 <td>{{ record.TypeofIncident }}</td>
                 <td>{{ record.Category }}</td>
                 <td>{{ record.CreatedBy }}</td>
                 <td>{{ record.DateOfOccurance }}</td>
                 <td>{{ record.DateOfReport }}</td>
                 <td>{{ record.DueDate }}</td>
                 <td>{{ record.NumberofDueDays }}</td>
                 <td>{{ record.StatusName }}</td>
                 <td>{{ record.assignTo }}</td>
               </tr>
             </tbody>
             <tbody *ngIf="qualityEvent===qualityEventsNames.Errata">
               <tr *ngFor="let record of records">
                 <td>{{ record.Errata_No }}</td>
                 <td>{{ record.DepartmentName }}</td>
                 <td>{{ record.Docname }}</td>
                 <td>{{ record.referencedocno }}</td>
                 <td>{{ record.CreatedBy }}</td>
                 <td>{{ record.CreatedDate }}</td>
                 <td>{{ record.StatusName }}</td>
                 <td>{{ record.assignTo }}</td>
               </tr>
             </tbody>
             <tbody *ngIf="qualityEvent===qualityEventsNames.NoteToFile">
               <tr *ngFor="let record of records">
                 <td>{{ record.NTF_No }}</td>
                 <td>{{ record.DepartmentName }}</td>
                 <td>{{ record.Docname }}</td>
                 <td>{{ record.referencedocno }}</td>
                 <td>{{ record.CreatedBy }}</td>
                 <td>{{ record.CreatedDate }}</td>
                 <td>{{ record.StatusName }}</td>
                 <td>{{ record.assignTo }}</td>
               </tr>
             </tbody>
             <tfoot>
             <tr >
             <td *ngFor="let eachC of dtOptions.columns"><input type="text" placeholder="Search"></td>
             </tr>
             </tfoot>
       </table>
     </div>`,
                styleUrls: ['../management-level-chart/management-level-chart.component.scss']
            }]
    }], function () { return [{ type: ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_4__["BsModalRef"] }, { type: ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_4__["BsModalService"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"] }]; }, { datatableElement: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
            args: [angular_datatables__WEBPACK_IMPORTED_MODULE_1__["DataTableDirective"]]
        }] }); })();


/***/ }),

/***/ "PGxC":
/*!************************************************!*\
  !*** ./src/app/services/dashboards.service.ts ***!
  \************************************************/
/*! exports provided: DashboardsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardsService", function() { return DashboardsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "8Y7J");


class DashboardsService {
    constructor() {
        this.statusChartColorPalette = ["#003f5c", "#665191", "#d45087", "#f95d6a"];
        this.colorPalette1 = ["#003f5c", "#2f4b7c", "#665191", "#a05195", "#d45087", "#f95d6a", "#ff7c43", "#ffa600"];
        this.colorPalette2 = ["#1565c0", "#f9a825", "#2e7d32", "#ef6c00", "#4e342e", "#37474f", "#178b7e", "#042347", "#424242", "#4d8d36"];
        this.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    }
    getColor(QualityEventID) {
        return this.colorPalette1[QualityEventID];
    }
    getMonth(monthNum) {
        return this.months[monthNum - 1];
    }
    getModifiedOverdueData(overdueData) {
        //   var localData = overdueData.sort(function (a, b) {
        //     return b.count - a.count;
        // });
        var localData = overdueData;
        var totalCount = localData.reduce(function (prevValue, item) {
            return prevValue + item.TotalRecords;
        }, 0), cumulativeCount = 0;
        return localData.map(function (item, index) {
            cumulativeCount += item.TotalRecords;
            return {
                EventName: item.EventName,
                TotalRecords: item.TotalRecords,
                cumulativePercent: Math.round(cumulativeCount * 100 / totalCount)
            };
        });
    }
}
DashboardsService.ɵfac = function DashboardsService_Factory(t) { return new (t || DashboardsService)(); };
DashboardsService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: DashboardsService, factory: DashboardsService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DashboardsService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "RnhZ":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "K/tc",
	"./af.js": "K/tc",
	"./ar": "jnO4",
	"./ar-dz": "o1bE",
	"./ar-dz.js": "o1bE",
	"./ar-kw": "Qj4J",
	"./ar-kw.js": "Qj4J",
	"./ar-ly": "HP3h",
	"./ar-ly.js": "HP3h",
	"./ar-ma": "CoRJ",
	"./ar-ma.js": "CoRJ",
	"./ar-sa": "gjCT",
	"./ar-sa.js": "gjCT",
	"./ar-tn": "bYM6",
	"./ar-tn.js": "bYM6",
	"./ar.js": "jnO4",
	"./az": "SFxW",
	"./az.js": "SFxW",
	"./be": "H8ED",
	"./be.js": "H8ED",
	"./bg": "hKrs",
	"./bg.js": "hKrs",
	"./bm": "p/rL",
	"./bm.js": "p/rL",
	"./bn": "kEOa",
	"./bn-bd": "loYQ",
	"./bn-bd.js": "loYQ",
	"./bn.js": "kEOa",
	"./bo": "0mo+",
	"./bo.js": "0mo+",
	"./br": "aIdf",
	"./br.js": "aIdf",
	"./bs": "JVSJ",
	"./bs.js": "JVSJ",
	"./ca": "1xZ4",
	"./ca.js": "1xZ4",
	"./cs": "PA2r",
	"./cs.js": "PA2r",
	"./cv": "A+xa",
	"./cv.js": "A+xa",
	"./cy": "l5ep",
	"./cy.js": "l5ep",
	"./da": "DxQv",
	"./da.js": "DxQv",
	"./de": "tGlX",
	"./de-at": "s+uk",
	"./de-at.js": "s+uk",
	"./de-ch": "u3GI",
	"./de-ch.js": "u3GI",
	"./de.js": "tGlX",
	"./dv": "WYrj",
	"./dv.js": "WYrj",
	"./el": "jUeY",
	"./el.js": "jUeY",
	"./en-au": "Dmvi",
	"./en-au.js": "Dmvi",
	"./en-ca": "OIYi",
	"./en-ca.js": "OIYi",
	"./en-gb": "Oaa7",
	"./en-gb.js": "Oaa7",
	"./en-ie": "4dOw",
	"./en-ie.js": "4dOw",
	"./en-il": "czMo",
	"./en-il.js": "czMo",
	"./en-in": "7C5Q",
	"./en-in.js": "7C5Q",
	"./en-nz": "b1Dy",
	"./en-nz.js": "b1Dy",
	"./en-sg": "t+mt",
	"./en-sg.js": "t+mt",
	"./eo": "Zduo",
	"./eo.js": "Zduo",
	"./es": "iYuL",
	"./es-do": "CjzT",
	"./es-do.js": "CjzT",
	"./es-mx": "tbfe",
	"./es-mx.js": "tbfe",
	"./es-us": "Vclq",
	"./es-us.js": "Vclq",
	"./es.js": "iYuL",
	"./et": "7BjC",
	"./et.js": "7BjC",
	"./eu": "D/JM",
	"./eu.js": "D/JM",
	"./fa": "jfSC",
	"./fa.js": "jfSC",
	"./fi": "gekB",
	"./fi.js": "gekB",
	"./fil": "1ppg",
	"./fil.js": "1ppg",
	"./fo": "ByF4",
	"./fo.js": "ByF4",
	"./fr": "nyYc",
	"./fr-ca": "2fjn",
	"./fr-ca.js": "2fjn",
	"./fr-ch": "Dkky",
	"./fr-ch.js": "Dkky",
	"./fr.js": "nyYc",
	"./fy": "cRix",
	"./fy.js": "cRix",
	"./ga": "USCx",
	"./ga.js": "USCx",
	"./gd": "9rRi",
	"./gd.js": "9rRi",
	"./gl": "iEDd",
	"./gl.js": "iEDd",
	"./gom-deva": "qvJo",
	"./gom-deva.js": "qvJo",
	"./gom-latn": "DKr+",
	"./gom-latn.js": "DKr+",
	"./gu": "4MV3",
	"./gu.js": "4MV3",
	"./he": "x6pH",
	"./he.js": "x6pH",
	"./hi": "3E1r",
	"./hi.js": "3E1r",
	"./hr": "S6ln",
	"./hr.js": "S6ln",
	"./hu": "WxRl",
	"./hu.js": "WxRl",
	"./hy-am": "1rYy",
	"./hy-am.js": "1rYy",
	"./id": "UDhR",
	"./id.js": "UDhR",
	"./is": "BVg3",
	"./is.js": "BVg3",
	"./it": "bpih",
	"./it-ch": "bxKX",
	"./it-ch.js": "bxKX",
	"./it.js": "bpih",
	"./ja": "B55N",
	"./ja.js": "B55N",
	"./jv": "tUCv",
	"./jv.js": "tUCv",
	"./ka": "IBtZ",
	"./ka.js": "IBtZ",
	"./kk": "bXm7",
	"./kk.js": "bXm7",
	"./km": "6B0Y",
	"./km.js": "6B0Y",
	"./kn": "PpIw",
	"./kn.js": "PpIw",
	"./ko": "Ivi+",
	"./ko.js": "Ivi+",
	"./ku": "JCF/",
	"./ku.js": "JCF/",
	"./ky": "lgnt",
	"./ky.js": "lgnt",
	"./lb": "RAwQ",
	"./lb.js": "RAwQ",
	"./lo": "sp3z",
	"./lo.js": "sp3z",
	"./lt": "JvlW",
	"./lt.js": "JvlW",
	"./lv": "uXwI",
	"./lv.js": "uXwI",
	"./me": "KTz0",
	"./me.js": "KTz0",
	"./mi": "aIsn",
	"./mi.js": "aIsn",
	"./mk": "aQkU",
	"./mk.js": "aQkU",
	"./ml": "AvvY",
	"./ml.js": "AvvY",
	"./mn": "lYtQ",
	"./mn.js": "lYtQ",
	"./mr": "Ob0Z",
	"./mr.js": "Ob0Z",
	"./ms": "6+QB",
	"./ms-my": "ZAMP",
	"./ms-my.js": "ZAMP",
	"./ms.js": "6+QB",
	"./mt": "G0Uy",
	"./mt.js": "G0Uy",
	"./my": "honF",
	"./my.js": "honF",
	"./nb": "bOMt",
	"./nb.js": "bOMt",
	"./ne": "OjkT",
	"./ne.js": "OjkT",
	"./nl": "+s0g",
	"./nl-be": "2ykv",
	"./nl-be.js": "2ykv",
	"./nl.js": "+s0g",
	"./nn": "uEye",
	"./nn.js": "uEye",
	"./oc-lnc": "Fnuy",
	"./oc-lnc.js": "Fnuy",
	"./pa-in": "8/+R",
	"./pa-in.js": "8/+R",
	"./pl": "jVdC",
	"./pl.js": "jVdC",
	"./pt": "8mBD",
	"./pt-br": "0tRk",
	"./pt-br.js": "0tRk",
	"./pt.js": "8mBD",
	"./ro": "lyxo",
	"./ro.js": "lyxo",
	"./ru": "lXzo",
	"./ru.js": "lXzo",
	"./sd": "Z4QM",
	"./sd.js": "Z4QM",
	"./se": "//9w",
	"./se.js": "//9w",
	"./si": "7aV9",
	"./si.js": "7aV9",
	"./sk": "e+ae",
	"./sk.js": "e+ae",
	"./sl": "gVVK",
	"./sl.js": "gVVK",
	"./sq": "yPMs",
	"./sq.js": "yPMs",
	"./sr": "zx6S",
	"./sr-cyrl": "E+lV",
	"./sr-cyrl.js": "E+lV",
	"./sr.js": "zx6S",
	"./ss": "Ur1D",
	"./ss.js": "Ur1D",
	"./sv": "X709",
	"./sv.js": "X709",
	"./sw": "dNwA",
	"./sw.js": "dNwA",
	"./ta": "PeUW",
	"./ta.js": "PeUW",
	"./te": "XLvN",
	"./te.js": "XLvN",
	"./tet": "V2x9",
	"./tet.js": "V2x9",
	"./tg": "Oxv6",
	"./tg.js": "Oxv6",
	"./th": "EOgW",
	"./th.js": "EOgW",
	"./tk": "Wv91",
	"./tk.js": "Wv91",
	"./tl-ph": "Dzi0",
	"./tl-ph.js": "Dzi0",
	"./tlh": "z3Vd",
	"./tlh.js": "z3Vd",
	"./tr": "DoHr",
	"./tr.js": "DoHr",
	"./tzl": "z1FC",
	"./tzl.js": "z1FC",
	"./tzm": "wQk9",
	"./tzm-latn": "tT3J",
	"./tzm-latn.js": "tT3J",
	"./tzm.js": "wQk9",
	"./ug-cn": "YRex",
	"./ug-cn.js": "YRex",
	"./uk": "raLr",
	"./uk.js": "raLr",
	"./ur": "UpQW",
	"./ur.js": "UpQW",
	"./uz": "Loxo",
	"./uz-latn": "AQ68",
	"./uz-latn.js": "AQ68",
	"./uz.js": "Loxo",
	"./vi": "KSF8",
	"./vi.js": "KSF8",
	"./x-pseudo": "/X5v",
	"./x-pseudo.js": "/X5v",
	"./yo": "fzPg",
	"./yo.js": "fzPg",
	"./zh-cn": "XDpg",
	"./zh-cn.js": "XDpg",
	"./zh-hk": "SatO",
	"./zh-hk.js": "SatO",
	"./zh-mo": "OmwH",
	"./zh-mo.js": "OmwH",
	"./zh-tw": "kOpN",
	"./zh-tw.js": "kOpN"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "RnhZ";

/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "iInd");



class AppComponent {
    constructor() {
        this.title = 'qms-dashboard-angular';
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 1, vars: 0, template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "router-outlet");
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-root',
                templateUrl: './app.component.html',
                styleUrls: ['./app.component.scss']
            }]
    }], null, null); })();


/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "cUpR");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-routing.module */ "vY5A");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "Sy1n");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "SVse");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "IheW");
/* harmony import */ var _dashboards_dashboards_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dashboards/dashboards.module */ "BNDV");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/platform-browser/animations */ "omvX");









class AppModule {
}
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({ factory: function AppModule_Factory(t) { return new (t || AppModule)(); }, providers: [
        {
            provide: _angular_common__WEBPACK_IMPORTED_MODULE_4__["APP_BASE_HREF"],
            useValue: '/QMS/DashBoard/QMS_Dashboard'
        },
    ], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"],
            _dashboards_dashboards_module__WEBPACK_IMPORTED_MODULE_6__["DashboardsModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__["BrowserAnimationsModule"],
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"],
        _dashboards_dashboards_module__WEBPACK_IMPORTED_MODULE_6__["DashboardsModule"],
        _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__["BrowserAnimationsModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
        args: [{
                declarations: [
                    _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                ],
                imports: [
                    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                    _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
                    _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"],
                    _dashboards_dashboards_module__WEBPACK_IMPORTED_MODULE_6__["DashboardsModule"],
                    _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__["BrowserAnimationsModule"],
                ],
                providers: [
                    {
                        provide: _angular_common__WEBPACK_IMPORTED_MODULE_4__["APP_BASE_HREF"],
                        useValue: '/QMS/DashBoard/QMS_Dashboard'
                    },
                ],
                bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "sW5g":
/*!***************************************************************!*\
  !*** ./src/app/dashboards/capa-chart/capa-chart.component.ts ***!
  \***************************************************************/
/*! exports provided: CapaChartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CapaChartComponent", function() { return CapaChartComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _models_dashboards__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/dashboards */ "+E+c");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "wd/R");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var src_app_services_dashboards_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/dashboards.service */ "PGxC");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "s7LF");
/* harmony import */ var src_app_services_server_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/server.service */ "H9vK");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/modal */ "LqlI");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "SVse");
/* harmony import */ var ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng-multiselect-dropdown */ "UPO+");
/* harmony import */ var ng2_flatpickr__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-flatpickr */ "A/TE");
/* harmony import */ var devextreme_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! devextreme-angular */ "hYZE");
/* harmony import */ var devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! devextreme-angular/ui/nested */ "Q08N");













function CapaChartComponent_a_5_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CapaChartComponent_a_5_Template_a_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.onPreviousLevelClick(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function () { return { standalone: true }; };
function CapaChartComponent_div_13_Template(rf, ctx) { if (rf & 1) {
    const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ng-multiselect-dropdown", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function CapaChartComponent_div_13_Template_ng_multiselect_dropdown_ngModelChange_2_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r5.selectedTypeOfAction = $event; })("onSelect", function CapaChartComponent_div_13_Template_ng_multiselect_dropdown_onSelect_2_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6); const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r7.getFilteredStatusChartData($event); })("onDeSelect", function CapaChartComponent_div_13_Template_ng_multiselect_dropdown_onDeSelect_2_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6); const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r8.getFilteredStatusChartData($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", "Type Of Action")("settings", ctx_r1.typeOfActionDropdownSettings)("data", ctx_r1.typeOfActionsList)("ngModel", ctx_r1.selectedTypeOfAction)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](5, _c0));
} }
function CapaChartComponent_div_19_Template(rf, ctx) { if (rf & 1) {
    const _r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "button", 51);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CapaChartComponent_div_19_Template_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r10); const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r9.onShowOverallCAPAListView(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "List View");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c1 = function () { return { name: "frequency", position: "left", tickInterval: 5, title: "Overdues Count" }; };
const _c2 = function (a0) { return { customizeText: a0 }; };
const _c3 = function () { return { visible: false }; };
const _c4 = function (a4) { return { value: 80, color: "#fc3535", dashStyle: "dash", width: 2, label: a4 }; };
const _c5 = function (a0) { return [a0]; };
const _c6 = function (a4, a5) { return { name: "percentage", position: "right", showZero: true, title: "Cumulative Percentage", label: a4, constantLines: a5, valueMarginsEnabled: false }; };
const _c7 = function (a0, a1) { return [a0, a1]; };
class CapaChartComponent {
    constructor(dashboardService, fb, serverService, modalService, renderer) {
        this.dashboardService = dashboardService;
        this.fb = fb;
        this.serverService = serverService;
        this.modalService = modalService;
        this.renderer = renderer;
        this.departmentsList = [];
        this.selectedDepartments = [];
        this.departmentDropdownSettings = {};
        this.eventsDropdownSettings = {};
        this.qualityEventsList = [];
        this.selectedQualityEvents = [];
        this.typeOfActionsList = [];
        this.selectedTypeOfAction = [];
        this.typeOfActionDropdownSettings = {};
        this.changeClassificationList = [];
        this.selectedChangeClassification = [];
        this.changeClassificationDropdownSettings = {};
        this.selectedCCNClassificationIDs = [];
        this.incidentCategoriesList = [];
        this.selectedIncidentCategory = [];
        this.incidentCategoryDropdownSettings = {};
        this.incidentClassificationList = [];
        this.selectedIncidentClassification = [];
        this.incidentClassificationDropdownSettings = {};
        this.fromDateOptions = {
            dateFormat: "d-M-Y"
        };
        this.toDateOptions = {
            dateFormat: "d-M-Y"
        };
        this.currentQualityEventId = _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventIds"];
        this.overallStatuslevel = 0;
        this.selectedDeptIds = [];
        this.overdueDataCAPA = [];
        //returns department ID from Dept name
        this.getDeptIdFromDeptName = (deptName) => {
            for (let i = 0; i < this.departmentsList.length; i++) {
                if (this.departmentsList[i].DepartmentName === deptName) {
                    return this.departmentsList[i].Department_ID;
                }
            }
        };
        //returns qualityEvent ID from qualityEvent name
        this.getQualityEventIdFromQualityEventName = (qualityEventName) => {
            for (let i = 0; i < this.qualityEventsList.length; i++) {
                if (this.qualityEventsList[i].QualityEvent_TypeName === qualityEventName) {
                    return this.qualityEventsList[i].QualityEvent_TypeID;
                }
            }
        };
        //converts fetched date into DB compatible date format
        this.getRequiredDateFormat = (selectedDate) => {
            var tempDate = new Date(selectedDate);
            let sampleFromDate = moment__WEBPACK_IMPORTED_MODULE_2__(tempDate.toISOString());
            return sampleFromDate.format('DD-MMM-YYYY');
        };
        this.customizeTooltipForOverdue = (info) => {
            return {
                html: "<div><div class='tooltip-header'>" +
                    info.argumentText + "</div>" +
                    "<div class='tooltip-body'><div class='series-name'>" +
                    "<span class='top-series-name'>" + info.points[0].seriesName + "</span>" +
                    ": </div><div class='value-text'>" +
                    "<span class='top-series-value'>" + info.points[0].valueText + "</span>" +
                    "</div><div class='series-name'>" +
                    "<span class='bottom-series-name'>" + info.points[1].seriesName + "</span>" +
                    ": </div><div class='value-text'>" +
                    "<span class='bottom-series-value'>" + info.points[1].valueText + "</span>" +
                    "% </div></div></div>"
            };
        };
        this.customizeLabelText = (info) => {
            return info.valueText + "%";
        };
        this.overallStatuslevel = 0;
        this.setTypeOfActionDropdown();
        this.CAPAfiltersForm = this.fb.group({
            fromDate: [''],
            toDate: ['']
        });
        this.getDepartmentsList();
        this.getQualityEventsListForCAPAChart();
        this.fromDateOptions.defaultDate = this.initialFromDate = moment__WEBPACK_IMPORTED_MODULE_2__().subtract(1, 'years').format('DD-MMM-YYYY');
        this.toDateOptions.defaultDate = this.initialToDate = moment__WEBPACK_IMPORTED_MODULE_2__().format('DD-MMM-YYYY');
        this.getAllDepartmentQualityEventsCAPADataOnInitialLoad();
        this.subtitleForStackedBarChart = '( ' + this.allDepartmentQualityEventsFiltersForCAPA.FromDate + ' to ' + this.allDepartmentQualityEventsFiltersForCAPA.ToDate + ' )';
    }
    ngOnInit() {
        this.departmentDropdownSettings = {
            singleSelection: false,
            idField: 'Department_ID',
            textField: 'DepartmentName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
        };
        this.eventsDropdownSettings = {
            singleSelection: true,
            idField: 'QualityEvent_TypeID',
            textField: 'QualityEvent_TypeName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
        };
    }
    setTypeOfActionDropdown() {
        this.typeOfActionDropdownSettings = {
            singleSelection: true,
            idField: 'typeOfActionID',
            textField: 'typeOfActionName',
            itemsShowLimit: 1,
            closeDropDownOnSelection: true
        };
        this.typeOfActionsList = [
            { typeOfActionId: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["TypeOfActionStatusCodes"].Corrective, typeOfActionName: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["TypeOfActionNames"].corrective },
            { typeOfActionId: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["TypeOfActionStatusCodes"].Preventive, typeOfActionName: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["TypeOfActionNames"].preventive },
            { typeOfActionId: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["TypeOfActionStatusCodes"].CorrectiveAndPreventive, typeOfActionName: _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["TypeOfActionNames"].correctiveAndPreventive }
        ];
        this.selectedTypeOfAction[0] = this.typeOfActionsList[2];
    }
    getAllDepartmentQualityEventsCAPADataOnInitialLoad() {
        this.allDepartmentQualityEventsFiltersForCAPA = {
            DeptID: this.getDepartmentListIds(),
            QualityEventType: this.getQualityEventIds(),
            FromDate: this.initialFromDate,
            ToDate: this.initialToDate
        };
        console.log("CAPA Chart filters", this.allDepartmentQualityEventsFiltersForCAPA);
        this.previousDataFiltersForStackedBarChart = this.allDepartmentQualityEventsFiltersForCAPA;
        this.getDepartmentsQualityEventsDataFromFiltersOfCAPA(this.allDepartmentQualityEventsFiltersForCAPA);
    }
    onSelectAllDepartments(items) {
        this.selectedDepartments = items;
        this.onDeptQualityEventsCAPADataChartFilterChange(items);
    }
    onSelectAllQualityEvents(items) {
        this.selectedQualityEvents = items;
        this.onDeptQualityEventsCAPADataChartFilterChange(items);
    }
    getColorOFQE(QualityEventNumber) {
        return this.dashboardService.getColor(QualityEventNumber);
    }
    getColorPaletteForStatusChart() {
        return this.dashboardService.colorPalette2;
    }
    //Extracting Department IDs from departmentsList Object array
    getDepartmentListIds() {
        let departmentIds = [];
        for (let i = 0; i < this.departmentsList.length; i++) {
            departmentIds.push(this.departmentsList[i].Department_ID);
        }
        return departmentIds;
    }
    //Extracting Quality Event IDs from qualityEventsList Object array
    getQualityEventIds() {
        let qualityEventIds = [];
        for (let i = 0; i < this.qualityEventsList.length; i++) {
            qualityEventIds.push(this.qualityEventsList[i].QualityEvent_TypeID);
        }
        return qualityEventIds;
    }
    //fetches chart data based on filters passed
    getDepartmentsQualityEventsDataFromFiltersOfCAPA(allDepartmentQualityEventsFiltersForCAPA) {
        this.serverService.getDeptQualityEventsDataForCAPA(allDepartmentQualityEventsFiltersForCAPA).subscribe((res) => {
            console.log("capa data", res);
            this.dataSource = res.CapaCount;
            for (let i = 0; i < this.dataSource.length; i++) {
                this.dataSource[i].DepartmentName = _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["TrimmedDepartmentNames"][this.dataSource[i].DepartmentName];
            }
            this.dataSource = this.dataSource.sort(function (x, y) {
                if (x.DepartmentName < y.DepartmentName)
                    return -1;
                if (x.DepartmentName > y.DepartmentName)
                    return 1;
                return 0;
            });
            // this.dataSource=this.dataSource;
            console.log("stacked bar data", this.dataSource);
        }, (err) => {
            console.error(err);
        });
    }
    //fetches departmentsList from DB and sorts it alphabetically
    getDepartmentsList() {
        // let roleAndEventID={
        //   roleID:2,
        //   eventId:3
        // }
        this.serverService.getAllDepartmentsList().subscribe((resultantDepartments) => {
            this.departmentsList = resultantDepartments.DeptFevent_list;
            this.departmentsList = this.departmentsList.sort(function (x, y) {
                if (x.DepartmentName < y.DepartmentName)
                    return -1;
                if (x.DepartmentName > y.DepartmentName)
                    return 1;
                return 0;
            });
            this.selectedDepartments = this.departmentsList;
            this.previousDataFiltersForStackedBarChart.DeptID = this.getDepartmentListIds();
        }, (err) => {
            console.log(err);
        });
    }
    //fetches qualityEventsList from DB and sorts it alphabetically
    getQualityEventsListForCAPAChart() {
        this.serverService.getQualityEventsListForCAPA().subscribe((resultantQualityEvents) => {
            this.qualityEventsList = resultantQualityEvents.sort(function (x, y) {
                if (x.QualityEvent_TypeName < y.QualityEvent_TypeName)
                    return -1;
                if (x.QualityEvent_TypeName > y.QualityEvent_TypeName)
                    return 1;
                return 0;
            });
            // this.selectedQualityEvents=this.qualityEventsList;
            this.selectedQualityEvents = [];
            this.previousDataFiltersForStackedBarChart.QualityEventType = this.getQualityEventIds();
        }, (err) => {
            console.error(err);
        });
    }
    getDeptNameFromDeptID(deptId) {
        for (let i = 0; i < this.departmentsList.length; i++) {
            if (this.departmentsList[i].Department_ID === deptId) {
                return this.departmentsList[i].DepartmentName;
            }
        }
    }
    getQualityEventNameFromQualityEventId(qualityEventID) {
        for (let i = 0; i < this.qualityEventsList.length; i++) {
            if (this.qualityEventsList[i].QualityEvent_TypeID === qualityEventID) {
                return this.qualityEventsList[i].QualityEvent_TypeName;
            }
        }
    }
    setFromAndToDates() {
        this.selectedFromDate = this.CAPAfiltersForm.get('fromDate').value[0];
        this.selectedToDate = this.CAPAfiltersForm.get('toDate').value[0];
    }
    getSelectedDeptIDs() {
        this.selectedDeptIds = [];
        for (let i = 0; i < this.selectedDepartments.length; i++) {
            this.selectedDeptIds.push(this.selectedDepartments[i].Department_ID);
        }
    }
    getSelectedQualityEventIDs() {
        this.selectedQualityEventIds = [];
        for (let i = 0; i < this.selectedQualityEvents.length; i++) {
            this.selectedQualityEventIds.push(this.selectedQualityEvents[i].QualityEvent_TypeID);
        }
    }
    getSelectedCCNClassificationIDs() {
        this.selectedCCNClassificationIDs = [];
        for (let i = 0; i < this.selectedChangeClassification.length; i++) {
            this.selectedCCNClassificationIDs.push(this.selectedChangeClassification[i].changeClassificationID);
        }
    }
    onDeptQualityEventsCAPADataChartFilterChange(event) {
        this.dataSource = [];
        this.setFromAndToDates();
        if (this.overallStatuslevel === 0) {
            this.getSelectedDeptIDs();
            this.getSelectedQualityEventIDs();
            this.allDepartmentQualityEventsFiltersForCAPA = {
                DeptID: this.selectedDepartments.length === 0 ? [] : this.selectedDeptIds,
                QualityEventType: ((this.selectedQualityEvents.length === 0) || (this.selectedQualityEvents.length === this.qualityEventsList.length)) ? [] : this.selectedQualityEventIds,
                FromDate: this.selectedFromDate !== undefined ? this.getRequiredDateFormat(this.selectedFromDate) : this.initialFromDate,
                ToDate: this.selectedToDate !== undefined ? this.getRequiredDateFormat(this.selectedToDate) : this.initialToDate
            };
            console.log("on filter change", this.allDepartmentQualityEventsFiltersForCAPA);
            this.previousDataFiltersForStackedBarChart = this.allDepartmentQualityEventsFiltersForCAPA;
            this.subtitleForStackedBarChart = '( ' + this.allDepartmentQualityEventsFiltersForCAPA.FromDate + ' to ' + this.allDepartmentQualityEventsFiltersForCAPA.ToDate + ' )';
            this.getDepartmentsQualityEventsDataFromFiltersOfCAPA(this.allDepartmentQualityEventsFiltersForCAPA);
        }
    }
    getUntrimmedDeptName(trimmedDeptName) {
        for (let i = 0; i < this.departmentsList.length; i++) {
            if (trimmedDeptName === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["TrimmedDepartmentNames"][this.departmentsList[i].DepartmentName]) {
                return this.departmentsList[i].DepartmentName;
            }
        }
    }
    onDeptQualityEventCAPAPointClick(event) {
        this.setFromAndToDates();
        this.selectedDepartments = [];
        let unTrimmedDeptName = this.getUntrimmedDeptName(event.target.originalArgument);
        // event.target.originalArgument = event.target.originalArgument === 'Executive Management'? 'Executive Managements' : event.target.originalArgument;
        this.selectedDepartments.push({
            Department_ID: this.getDeptIdFromDeptName(unTrimmedDeptName),
            DepartmentName: unTrimmedDeptName
        });
        this.selectedQualityEvents = [];
        this.selectedQualityEvents.push({
            QualityEvent_TypeID: this.getQualityEventIdFromQualityEventName(event.target.series.name),
            QualityEvent_TypeName: event.target.series.name
        });
        this.prevCurrentDrillDownHeaderText = this.currentDrillDownHeaderText = event.target.series.name;
        this.selectedQualityEventName = event.target.series.name;
        this.getQualityEventDataOfOneDept(this.selectedDepartments[0].Department_ID, event.target.series.name);
    }
    onStackedBarChartExporting(event) {
        event.fileName = "Total CAPA Count of QE of Depts " + this.subtitleForStackedBarChart;
    }
    customizeTooltip(arg) {
        return {
            text: arg.seriesName + ' : ' + arg.valueText
        };
    }
    getQualityEventDataOfOneDept(deptId, qualityEventName) {
        let numToArrConversion = [];
        numToArrConversion.push(deptId);
        this.qualityEventDataOfOneDept = {
            DeptID: numToArrConversion,
            QualityEventType: this.getQualityEventIdFromQualityEventName(qualityEventName),
            TypeOfAction: this.selectedTypeOfAction[0].typeOfActionId,
            FromDate: this.selectedFromDate !== undefined ? this.getRequiredDateFormat(this.selectedFromDate) : this.initialFromDate,
            ToDate: this.selectedToDate !== undefined ? this.getRequiredDateFormat(this.selectedToDate) : this.initialToDate
        };
        this.selectedQualityEventIdForStatusChart = this.qualityEventDataOfOneDept.QualityEventType;
        this.previousDataFiltersForStatusChart = this.qualityEventDataOfOneDept;
        this.serverService.getQualityEventStatusOfOneDeptForCAPA(this.qualityEventDataOfOneDept).subscribe((res) => {
            console.log("result of status chart capa", res);
            this.statusListData = res.QeStatus_List;
            this.statusListData = this.excludeEmptyRecordsOfStatusChart(this.statusListData);
            this.overallStatuslevel++;
        }, (err) => {
            console.error(err);
        });
    }
    excludeEmptyRecordsOfStatusChart(statusListData) {
        let sampleListData = [];
        for (let i = 0; i < statusListData.length; i++) {
            if (statusListData[i].TotalRecords !== 0) {
                sampleListData.push(statusListData[i]);
            }
        }
        return sampleListData;
    }
    customizeLabel(arg) {
        return arg.argumentText + ' : ' + arg.valueText;
    }
    onShowOverallCAPAListView() {
    }
    getFilteredStatusChartData() {
        if (this.overallStatuslevel === 1) {
            this.getSelectedDeptIDs();
            this.filteredQualityEventData = {
                DeptID: this.selectedDeptIds,
                QualityEventType: this.selectedQualityEventIdForStatusChart,
                TypeOfAction: this.selectedTypeOfAction[0].typeOfActionId,
                FromDate: this.selectedFromDate !== undefined ? this.getRequiredDateFormat(this.selectedFromDate) : this.initialFromDate,
                ToDate: this.selectedToDate !== undefined ? this.getRequiredDateFormat(this.selectedToDate) : this.initialToDate
            };
            this.previousDataFiltersForStatusChart = this.filteredQualityEventData;
            this.getFilteredQualityEventStatusChart(this.filteredQualityEventData);
        }
        else if (this.overallStatuslevel === 2) {
            // this.getFilteredOverdueData();
        }
    }
    getFilteredQualityEventStatusChart(filteredQualityEventData) {
        this.serverService.getQualityEventStatusOfOneDeptForCAPA(filteredQualityEventData).subscribe((res) => {
            this.statusListData = res.QeStatus_List;
            this.statusListData = this.excludeEmptyRecordsOfStatusChart(this.statusListData);
        }, (err) => {
            console.error(err);
        });
    }
    onPreviousLevelClick() {
        if (this.overallStatuslevel === 1) {
            this.overallStatuslevel--;
            this.getDepartmentsQualityEventsDataFromFiltersOfCAPA(this.previousDataFiltersForStackedBarChart);
            this.setSelectedDepartmentsValues(this.previousDataFiltersForStackedBarChart.DeptID);
            this.setSelectedQualityEventValues(this.previousDataFiltersForStackedBarChart.QualityEventType, 'qualityEventsChart');
            this.setSelectedDateValues(this.previousDataFiltersForStackedBarChart.FromDate, this.previousDataFiltersForStackedBarChart.ToDate);
        }
        else if (this.overallStatuslevel == 2) {
            this.overallStatuslevel--;
            this.currentDrillDownHeaderText = this.prevCurrentDrillDownHeaderText;
            this.getFilteredQualityEventStatusChart(this.previousDataFiltersForStatusChart);
            this.setSelectedDepartmentsValues(this.previousDataFiltersForStatusChart.DeptID);
            this.setSelectedQualityEventValues(this.previousDataFiltersForStatusChart.QualityEventType, 'statusChart');
            this.setSelectedDateValues(this.previousDataFiltersForStatusChart.FromDate, this.previousDataFiltersForStatusChart.ToDate);
        }
    }
    setSelectedDepartmentsValues(deptIds) {
        this.selectedDepartments = [];
        for (let i = 0; i < deptIds.length; i++) {
            this.selectedDepartments.push({
                Department_ID: deptIds[i],
                DepartmentName: this.getDeptNameFromDeptID(deptIds[i])
            });
        }
    }
    setSelectedQualityEventValues(qualityEventIds, source) {
        this.selectedQualityEvents = [];
        if (source === 'qualityEventsChart') {
            for (let i = 0; i < qualityEventIds.length; i++) {
                this.selectedQualityEvents.push({
                    QualityEvent_TypeID: qualityEventIds[i],
                    QualityEvent_TypeName: this.getQualityEventNameFromQualityEventId(qualityEventIds[i])
                });
            }
        }
        else if (source === 'statusChart') {
            this.selectedQualityEvents.push({
                QualityEvent_TypeID: qualityEventIds,
                QualityEvent_TypeName: this.getQualityEventNameFromQualityEventId(qualityEventIds)
            });
        }
    }
    setSelectedDateValues(fromDate, toDate) {
        this.fromDateValue = fromDate;
        this.toDateValue = toDate;
    }
    getChangeClassificationNameFromChangeClassificationID(changeClassificationId) {
        for (let i = 0; i < this.changeClassificationList.length; i++) {
            if (this.changeClassificationList[i].changeClassificationID === changeClassificationId) {
                return this.changeClassificationList[i].changeClassificationName;
            }
        }
    }
    getIncidentCatNameFromIncidentCatID(incidentCategoryId) {
        for (let i = 0; i < this.incidentCategoriesList.length; i++) {
            if (this.incidentCategoriesList[i].TypeofCategoryID === incidentCategoryId) {
                return this.incidentCategoriesList[i].TypeofIncident;
            }
        }
    }
    getIncidentClassificationNameFromIncidentClassificationID(incidentclassificationId) {
        for (let i = 0; i < this.incidentClassificationList.length; i++) {
            if (this.incidentClassificationList[i].incidentClassificationID === incidentclassificationId) {
                return this.incidentClassificationList[i].incidentClassificationName;
            }
        }
    }
    onStatusChartPointClick(event) {
        if ((event.target.argument).toLowerCase() === (_models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventStatus"].OverDue).toLowerCase()) {
            this.setFromAndToDates();
            this.getSelectedDeptIDs();
            this.currentDrillDownHeaderText = event.target.argument + ' - ' + this.selectedQualityEventName;
            this.overallStatuslevel++;
            this.getFilteredOverdueData();
        }
        else if (this.selectedQualityEventIdForStatusChart === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventIDs"].ChangeControlNote) {
            // this.getCCNStatusListOfAppliedFilters(event.target.argument);
        }
        else if (this.selectedQualityEventIdForStatusChart === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventIDs"].IncidentReport) {
            // this.getIncidentStatusListOfAppliedFilters(event.target.argument);
        }
        else if (this.selectedQualityEventIdForStatusChart === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventIDs"].Errata) {
            // this.getErrataStatusListOfAppliedFilters(event.target.argument);
        }
        else {
            // this.getNTFStatusListOfAppliedFilters(event.target.argument);
        }
    }
    getFilteredOverdueData() {
        this.getSelectedDeptIDs();
        this.overdueDataFilters = {
            DeptID: this.selectedDeptIds,
            QualityEventType: this.selectedQualityEventIdForStatusChart,
            TypeOfAction: this.selectedTypeOfAction[0].typeOfActionId,
            FromDate: this.selectedFromDate !== undefined ? this.getRequiredDateFormat(this.selectedFromDate) : this.initialFromDate,
            ToDate: this.selectedToDate !== undefined ? this.getRequiredDateFormat(this.selectedToDate) : this.initialToDate
        };
        this.serverService.getOverdueData(this.overdueDataFilters).subscribe((res) => {
            this.overdueDataCAPA = this.dashboardService.getModifiedOverdueData(res.Overdue_List);
        }, (err) => {
            console.error(err);
        });
    }
    onCAPAOverdueStatusChartPointClick(event) {
        if (this.selectedQualityEventIdForStatusChart === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventIDs"].ChangeControlNote) {
            // this.getCCNStatusListOfAppliedFilters(event.target.argument);
        }
        else if (this.selectedQualityEventIdForStatusChart === _models_dashboards__WEBPACK_IMPORTED_MODULE_1__["QualityEventIDs"].IncidentReport) {
            // this.getIncidentStatusListOfAppliedFilters(event.target.argument);
        }
    }
}
CapaChartComponent.ɵfac = function CapaChartComponent_Factory(t) { return new (t || CapaChartComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_dashboards_service__WEBPACK_IMPORTED_MODULE_3__["DashboardsService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_server_service__WEBPACK_IMPORTED_MODULE_5__["ServerService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__["BsModalService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"])); };
CapaChartComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: CapaChartComponent, selectors: [["app-capa-chart"]], decls: 57, vars: 80, consts: [[1, "row", "mt-3"], [1, "col-md-12"], [1, "col-sm-12", "padding-none", "dashboard_panel_full", "float-left"], [1, "col-lg-12", "col-12", "col-sm-12", "col-md-12", "col-12", "col-sm-12", "col-md-12", "dashboard_panel_header", "float-left"], ["href", "javascript:void(0)", "title", "Back to List", "class", "upload_btn float-left", 3, "click", 4, "ngIf"], [1, "col-lg-12", "col-12", "col-sm-12", "col-md-12", "col-12", "col-sm-12", "col-md-12", "float-left", "top", "bottom"], [1, "mb-2", 3, "formGroup"], [1, "row"], [1, "col-md-6"], [3, "placeholder", "settings", "data", "ngModel", "ngModelOptions", "ngModelChange", "onSelect", "onDeSelect", "onSelectAll"], [3, "placeholder", "settings", "data", "ngModel", "ngModelOptions", "disabled", "ngModelChange", "onSelect", "onDeSelect", "onSelectAll"], ["class", "row mt-2", 4, "ngIf"], [1, "row", "mt-2"], ["placeholder", "From date", "formControlName", "fromDate", 3, "setDate", "config", "change"], ["formControlName", "toDate", "placeholder", "To date", 3, "setDate", "config", "change"], ["class", "d-flex justify-content-end", 4, "ngIf"], ["id", "chart", 3, "dataSource", "onPointClick", "onExporting"], ["valueAxis", "none", "argumentAxis", "both", "panKey", "shift", 3, "dragToZoom", "allowMouseWheel"], ["position", "top", 3, "visible", "width"], [3, "height"], ["text", "CAPA Chart"], [3, "text"], ["valueField", "TotalRecordCCN", "name", "Change Control Note", 3, "color"], ["valueField", "TotalRecordIncident", "name", "Incident Report", 3, "color"], ["valueField", "TotalRecordNTF", "name", "Note To File", 3, "color"], ["valueField", "TotalRecordOOS", "name", "OOS", 3, "color"], ["valueField", "TotalRecordOOT", "name", "OOT", 3, "color"], ["valueField", "TotalRecordMC", "name", "MC", 3, "color"], ["position", "left"], ["text", "Total Count of CAPAs"], ["argumentField", "DepartmentName", "type", "stackedBar", "barWidth", "50"], ["verticalAlignment", "bottom", "horizontalAlignment", "center", "itemTextPosition", "right"], [3, "enabled", "margin"], ["location", "edge", "icon", "export", 3, "enabled", "customizeTooltip"], ["id", "pie", "resolveLabelOverlapping", "shift", 3, "title", "palette", "dataSource", "onPointClick"], ["orientation", "horizontal", "itemTextPosition", "right", "horizontalAlignment", "center", "verticalAlignment", "bottom", 3, "visible", "columnCount"], ["argumentField", "EventName", "valueField", "TotalRecords"], ["position", "outside", "position", "columns", 3, "visible", "customizeText"], [3, "size"], [3, "visible", "width"], ["id", "overdueChartCAPA", "palette", "soft", 3, "dataSource", "valueAxis", "onPointClick"], ["type", "bar", "valueField", "TotalRecords", "axis", "frequency", "name", "Total Overdues", "color", "#f9a825"], ["type", "spline", "valueField", "cumulativePercent", "axis", "percentage", "name", "Cumulative percentage", "color", "#178b7e"], ["overlappingBehavior", "stagger"], [3, "enabled", "shared", "customizeTooltip"], ["argumentField", "EventName", "type", "bar", 3, "barPadding"], ["verticalAlignment", "bottom", "horizontalAlignment", "center"], ["href", "javascript:void(0)", "title", "Back to List", 1, "upload_btn", "float-left", 3, "click"], ["src", "/Images/PMS/market_backicon.png"], [3, "placeholder", "settings", "data", "ngModel", "ngModelOptions", "ngModelChange", "onSelect", "onDeSelect"], [1, "d-flex", "justify-content-end"], ["type", "button", 1, "btn", "btn-link", 3, "click"]], template: function CapaChartComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "CAPA ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, CapaChartComponent_a_5_Template, 2, 0, "a", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "form", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "ng-multiselect-dropdown", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function CapaChartComponent_Template_ng_multiselect_dropdown_ngModelChange_10_listener($event) { return ctx.selectedDepartments = $event; })("onSelect", function CapaChartComponent_Template_ng_multiselect_dropdown_onSelect_10_listener($event) { return ctx.onDeptQualityEventsCAPADataChartFilterChange($event); })("onDeSelect", function CapaChartComponent_Template_ng_multiselect_dropdown_onDeSelect_10_listener($event) { return ctx.onDeptQualityEventsCAPADataChartFilterChange($event); })("onSelectAll", function CapaChartComponent_Template_ng_multiselect_dropdown_onSelectAll_10_listener($event) { return ctx.onSelectAllDepartments($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "ng-multiselect-dropdown", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function CapaChartComponent_Template_ng_multiselect_dropdown_ngModelChange_12_listener($event) { return ctx.selectedQualityEvents = $event; })("onSelect", function CapaChartComponent_Template_ng_multiselect_dropdown_onSelect_12_listener($event) { return ctx.onDeptQualityEventsCAPADataChartFilterChange($event); })("onDeSelect", function CapaChartComponent_Template_ng_multiselect_dropdown_onDeSelect_12_listener($event) { return ctx.onDeptQualityEventsCAPADataChartFilterChange($event); })("onSelectAll", function CapaChartComponent_Template_ng_multiselect_dropdown_onSelectAll_12_listener($event) { return ctx.onSelectAllQualityEvents($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, CapaChartComponent_div_13_Template, 3, 6, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "ng2-flatpickr", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function CapaChartComponent_Template_ng2_flatpickr_change_16_listener($event) { return ctx.onDeptQualityEventsCAPADataChartFilterChange($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "ng2-flatpickr", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function CapaChartComponent_Template_ng2_flatpickr_change_18_listener($event) { return ctx.onDeptQualityEventsCAPADataChartFilterChange($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](19, CapaChartComponent_div_19_Template, 3, 0, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "dx-chart", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("onPointClick", function CapaChartComponent_Template_dx_chart_onPointClick_20_listener($event) { return ctx.onDeptQualityEventCAPAPointClick($event); })("onExporting", function CapaChartComponent_Template_dx_chart_onExporting_20_listener($event) { return ctx.onStackedBarChartExporting($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "dxo-zoom-and-pan", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "dxo-scroll-bar", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "dxo-size", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "dxo-title", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "dxo-subtitle", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "dxi-series", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "dxi-series", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "dxi-series", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "dxi-series", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "dxi-series", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "dxi-series", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "dxi-value-axis", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "dxo-title", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "dxo-argument-axis");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "dxo-common-series-settings", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "dxo-legend", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "dxo-export", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "dxo-tooltip", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "dx-pie-chart", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("onPointClick", function CapaChartComponent_Template_dx_pie_chart_onPointClick_39_listener($event) { return ctx.onStatusChartPointClick($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "dxo-size", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "dxo-legend", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](42, "dxo-export", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "dxi-series", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "dxo-label", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](45, "dxo-font", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](46, "dxo-connector", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "dx-chart", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("onPointClick", function CapaChartComponent_Template_dx_chart_onPointClick_47_listener($event) { return ctx.onCAPAOverdueStatusChartPointClick($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](48, "dxi-series", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](49, "dxi-series", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "dxo-argument-axis");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "dxo-label", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](52, "dxo-tooltip", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](53, "dxo-common-series-settings", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](54, "dxo-legend", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](55, "dxo-export", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "dxo-title", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.overallStatuslevel !== 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.CAPAfiltersForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", "Select Department")("settings", ctx.departmentDropdownSettings)("data", ctx.departmentsList)("ngModel", ctx.selectedDepartments)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](64, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", "Select Quality Event")("settings", ctx.eventsDropdownSettings)("data", ctx.qualityEventsList)("ngModel", ctx.selectedQualityEvents)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](65, _c0))("disabled", ctx.overallStatuslevel !== 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.overallStatuslevel !== 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("setDate", ctx.fromDateValue)("config", ctx.fromDateOptions);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("setDate", ctx.toDateValue)("config", ctx.toDateOptions);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.overallStatuslevel !== 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](ctx.overallStatuslevel === 0 ? "" : "hideChart");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("dataSource", ctx.dataSource);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("dragToZoom", false)("allowMouseWheel", true);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", true)("width", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("height", 500);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("text", ctx.subtitleForStackedBarChart);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("color", ctx.getColorOFQE(1));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("color", ctx.getColorOFQE(3));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("color", ctx.getColorOFQE(5));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("color", ctx.getColorOFQE(7));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("color", ctx.getColorOFQE(9));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("color", ctx.getColorOFQE(11));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("enabled", true)("margin", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("enabled", true)("customizeTooltip", ctx.customizeTooltip);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](ctx.overallStatuslevel === 1 ? "" : "hideChart");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("title", ctx.currentDrillDownHeaderText);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("palette", ctx.getColorPaletteForStatusChart())("dataSource", ctx.statusListData);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("height", 450);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", false)("columnCount", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("enabled", true)("margin", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", true)("customizeText", ctx.customizeLabel);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("size", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", true)("width", 0.5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](ctx.overallStatuslevel === 2 ? "" : "hideChart");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("dataSource", ctx.overdueDataCAPA)("valueAxis", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](77, _c7, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](66, _c1), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](74, _c6, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](67, _c2, ctx.customizeLabelText), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](72, _c5, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](70, _c4, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](69, _c3))))));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("enabled", true)("shared", true)("customizeTooltip", ctx.customizeTooltipForOverdue);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("barPadding", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("enabled", true)("margin", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("text", ctx.currentDrillDownHeaderText);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroupDirective"], ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_8__["MultiSelectComponent"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgModel"], ng2_flatpickr__WEBPACK_IMPORTED_MODULE_9__["Ng2FlatpickrComponent"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControlName"], devextreme_angular__WEBPACK_IMPORTED_MODULE_10__["DxChartComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_11__["DxoZoomAndPanComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_11__["DxoScrollBarComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_11__["DxoSizeComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_11__["DxoTitleComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_11__["DxoSubtitleComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_11__["DxiSeriesComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_11__["DxiValueAxisComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_11__["DxoArgumentAxisComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_11__["DxoCommonSeriesSettingsComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_11__["DxoLegendComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_11__["DxoExportComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_11__["DxoTooltipComponent"], devextreme_angular__WEBPACK_IMPORTED_MODULE_10__["DxPieChartComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_11__["DxoLabelComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_11__["DxoFontComponent"], devextreme_angular_ui_nested__WEBPACK_IMPORTED_MODULE_11__["DxoConnectorComponent"]], styles: [".hideChart[_ngcontent-%COMP%] {\n  display: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkcy9jYXBhLWNoYXJ0L2NhcGEtY2hhcnQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9kYXNoYm9hcmRzL2NhcGEtY2hhcnQvY2FwYS1jaGFydC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oaWRlQ2hhcnR7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CapaChartComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-capa-chart',
                templateUrl: './capa-chart.component.html',
                styleUrls: ['./capa-chart.component.scss']
            }]
    }], function () { return [{ type: src_app_services_dashboards_service__WEBPACK_IMPORTED_MODULE_3__["DashboardsService"] }, { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] }, { type: src_app_services_server_service__WEBPACK_IMPORTED_MODULE_5__["ServerService"] }, { type: ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__["BsModalService"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"] }]; }, null); })();


/***/ }),

/***/ "vY5A":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var _dashboards_dashboards_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dashboards/dashboards.component */ "Hktm");





const routes = [
    { path: '', component: _dashboards_dashboards_component__WEBPACK_IMPORTED_MODULE_2__["DashboardsComponent"] },
];
class AppRoutingModule {
}
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ "AytR");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "cUpR");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ "zn8P":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "zn8P";

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map