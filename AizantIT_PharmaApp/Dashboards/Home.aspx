﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="AizantIT_PharmaApp.Dashboards.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">    
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">--%>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
  <link href="../AppCSS/landing/landing.css" rel="stylesheet" />
    <div class="bg_landing"></div>
    <div class="container-fluid">
        <ul class="CardRow1 landing_nav" id="divModules">
            <asp:HyperLink ID="hlUMS" NavigateUrl="~/UserManagement/DashboardUMS.aspx" runat="server" Visible="false">
               <li>
					<div class="icon_1 shadow_img col-centered" tabindex="1">
						<img src="../Images/Landing/icon3.png" alt="Administration" class="img_langing"/>
						<div class="module_name">Administration<br/>Setup</div>
					</div>
			    </li>
            </asp:HyperLink>
            <asp:HyperLink ID="hlDMS" NavigateUrl="~/DMS/DMSHomePage.aspx" runat="server" Visible="false">
               <li>
					<div class="icon_1 shadow_img col-centered" tabindex="2">
						<img src="../Images/Landing/icon4.png" alt="DMS" class="img_langing"/>
						<div class="module_name">Document Management System</div>
					</div>
			    </li>
            </asp:HyperLink>
            <asp:HyperLink ID="hlTMS" NavigateUrl="~/TMS/TmsDashboard.aspx" runat="server" Visible="false">
               <li>
					<div class="icon_1 shadow_img col-centered" tabindex="3">
						<img src="../Images/Landing/icon2.png" alt="TMS" class="img_langing"/>
						<div class="module_name">Training Management System</div>
					</div>
			    </li>
             </asp:HyperLink>
             <asp:HyperLink ID="hlVMS" NavigateUrl="~/VMS/VMS_HomePage.aspx" runat="server" Visible="false">
			    <li>
					<div class="icon_1 shadow_img col-centered" tabindex="4">
						<img src="../Images/Landing/icon1.png" alt="VMS" class="img_langing"/>
						<div class="module_name">Visitor <br/> Management System</div>
					</div>
			    </li>
            </asp:HyperLink>
             <asp:HyperLink ID="hlQMS" NavigateUrl="~/QMS/DashBoard/QMS_Dashboard" runat="server" Visible="false">
               <li>
					<div class="icon_1 shadow_img col-centered" tabindex="5">
						<img src="../Images/Landing/icon5.png" alt="QMS" class="img_langing"/>
						<div class="module_name">Quality<br /> Management System</div>
					</div>
			    </li>
            </asp:HyperLink>
            <%--<asp:HyperLink ID="hlPMS" NavigateUrl="~/PRODUCT/Product/_Product?showType=1" runat="server" Visible="false">
               <li>
					<div class="icon_1 shadow_img col-centered" tabindex="6">
						<img src="../Images/Landing/product_icon.png" alt="Product" class="img_langing"/>
						<div class="module_name">Product<br /> Search</div>
					</div>
			    </li>
            </asp:HyperLink>  --%>   
            <asp:HyperLink ID="hlRA" NavigateUrl="~/Regulatory/RegulatoryMain/RegulatoryDashBoard" runat="server" Visible="false">
               <li>
					<div class="icon_1 shadow_img col-centered" tabindex="7">
						<img src="../Images/Landing/regulatory_icon.png" alt="Regulatory" class="img_langing"/>
						<div class="module_name">Regulatory Affairs Database</div>
					</div>
			    </li>
            </asp:HyperLink>
            <%--  <asp:HyperLink ID="hlGlobal" NavigateUrl="~/UserManagement/DashboardUMS.aspx" runat="server" Visible="false">
               <li>
					<div class="icon_1 shadow_img col-centered" tabindex="8">
						<img src="../Images/Landing/icon3.png" alt="Smiley face" class="img_langing"/>
						<div class="module_name">Global Setup<br /> Management System</div>
					</div>
			    </li>
            </asp:HyperLink>--%>
        </ul>
 
    </div>
     
    <script>
        $(document).ready(function () {
            var selector = '.landing_links ul li a';

            $(selector).on('click', function () {
                $(selector).removeClass('active');
                $(this).addClass('active');
            });
        });
    </script>
    <script>
        function ModulesToShow(count) {
            var divModules = document.getElementById("divModules");
            if (count > 9 && count <= 18) {
                divModules.classList.remove("CardRow1");
                divModules.classList.add("CardRow2");
            }
            if (count > 18 && count <= 27) {
                divModules.classList.remove("CardRow1");
                divModules.classList.add("CardRow3");
            }
        }
    </script>
     <script>
            // Check browser support
            if (typeof (Storage) !== "undefined") {           
                // Retrieve
                console.log(localStorage.getItem("lastname"));
            } 
        </script>
</asp:Content>
