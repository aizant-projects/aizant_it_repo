﻿using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.Web.UI;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.Dashboards
{
    public partial class Home : System.Web.UI.Page
    {
        UMS_BAL UMBAL = new UMS_BAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["sesGMSRoleID"] = "0";// for vms role session is added
            if (HelpClass.IsUserAuthenticated())
            {
                if (!IsPostBack)
                {
                    try
                    {
                        if (Session["ShowPwdExpireMsg"] != null)
                        {
                            showExipreDate();
                            Session.Remove("ShowPwdExpireMsg");
                        }
                        DataTable dtUserModuleRoles = new DataTable();
                        dtUserModuleRoles = (Session["UserDetails"] as DataSet).Tables[1];
                        if (dtUserModuleRoles.Rows.Count > 0)
                        {
                            int ModuleCount = 0;
                            DataRow[] drUMS = dtUserModuleRoles.Select("ModuleID=1");
                            if (drUMS.Length > 0)
                            {
                                hlUMS.Visible = true;
                                ModuleCount += 1;
                                DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                                DataTable dtTemp = new DataTable();
                                DataRow[] drUMS_Roles = dtx.Select("ModuleID=" + (int)Modules.AdminSetup);
                                dtTemp = drUMS_Roles.CopyToDataTable();
                                if((drUMS_Roles.Length == 2) && (((dtTemp.Select("RoleID=" + (int)AdminSetup_UserRole.GlobalAdmin_UMS).Length > 0)) && ((dtTemp.Select("RoleID=" + (int)AdminSetup_UserRole.ProductAdmin).Length > 0))))
                                {
                                    hlUMS.NavigateUrl = "~/AdminSetup/BusinessDivisionAndSite/BusinessDivision";
                                }
                                else if ((drUMS_Roles.Length == 1) && ((dtTemp.Select("RoleID=" + (int)AdminSetup_UserRole.GlobalAdmin_UMS).Length > 0)))// Has only Global Admin Role
                                {
                                    hlUMS.NavigateUrl = "~/AdminSetup/BusinessDivisionAndSite/BusinessDivision";
                                }
                                else if ((drUMS_Roles.Length == 1) && ((dtTemp.Select("RoleID=" + (int)AdminSetup_UserRole.ProductAdmin).Length > 0)))
                                {
                                    hlUMS.NavigateUrl = "~/AdminSetup/Product/Product";
                                }
                            }
                            DataRow[] drDMS = dtUserModuleRoles.Select("ModuleID=2");
                            if (drDMS.Length > 0)
                            {
                                hlDMS.Visible = true;
                                ModuleCount += 1;
                            }
                            DataRow[] drTMS = dtUserModuleRoles.Select("ModuleID=3");
                            if (drTMS.Length > 0)
                            {
                                hlTMS.Visible = true;
                                ModuleCount += 1;
                            }
                            DataRow[] drQMS = dtUserModuleRoles.Select("ModuleID=4");
                            if (drQMS.Length > 0)
                            {
                                hlQMS.Visible = true;

                                //Checking weather User had only Audit Viewer and Admin role in QMS Module if has redirecting to main list
                                DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                                DataTable dtTemp = new DataTable();
                                DataRow[] drQMSRoles = dtx.Select("ModuleID=" + (int)Modules.QMS);
                                dtTemp = drQMSRoles.CopyToDataTable();
                                if (((drQMSRoles.Length == 2) && (dtTemp.Select("RoleID=" + (int)QMS_Audit_Roles.AuditViewer).Length > 0) && (dtTemp.Select("RoleID=" + (int)QMS_Audit_Roles.AuditAdmin).Length > 0))
                                    ||
                                    ((drQMSRoles.Length == 1) && ((dtTemp.Select("RoleID=" + (int)QMS_Audit_Roles.AuditAdmin).Length > 0)))
                                    )
                                {
                                    hlQMS.NavigateUrl = "~/QMS/AuditMaster/AuditMasterList";
                                }
                                else if (((drQMSRoles.Length == 1) && ((dtTemp.Select("RoleID=" + (int)QMS_Audit_Roles.AuditViewer).Length > 0))))// Has only Viewer Role
                                {
                                    hlQMS.NavigateUrl = "~/QMS/AuditMain/InitAuditReportList?listType=AuditReportMain";
                                }
                                ModuleCount += 1;
                            }
                            DataRow[] drVMS = dtUserModuleRoles.Select("ModuleID=6");
                            if (drVMS.Length > 0)
                            {
                                hlVMS.Visible = true;
                                ModuleCount += 1;
                            }
                            //DataRow[] drPMS = dtUserModuleRoles.Select("ModuleID=5");//
                            //if (drPMS.Length > 0)
                            //{
                            //    hlPMS.Visible = true;
                            //    ModuleCount += 1;
                            //}
                            DataRow[] drRA = dtUserModuleRoles.Select("ModuleID=7");
                            if (drRA.Length > 0)
                            {
                                hlRA.Visible = true;
                                DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                                DataTable dtTemp = new DataTable();
                                DataRow[] drRARoles = dtx.Select("ModuleID=" + (int)Modules.RA);
                                dtTemp = drRARoles.CopyToDataTable();
                                if (((drRARoles.Length == 1) && ((dtTemp.Select("RoleID=" + (int)RA_UserRole.RA_Admin).Length > 0))))// Has only Admin Role
                                {
                                    hlRA.NavigateUrl = "~/Regulatory/RegulatoryMaster/RegulatoryMasterList";
                                }
                                else
                                {
                                    hlRA.NavigateUrl = "~/Regulatory/RegulatoryMain/RegulatoryDashBoard";
                                }

                                ModuleCount += 1;
                            }
                            ScriptManager.RegisterStartupScript(Page, this.GetType(), "alert", "ModulesToShow('" + ModuleCount + "');", true);
                        }
                    }
                    catch (Exception ex)
                    {
                        string strline = HelpClass.LineNo(ex);
                        string strMsg = HelpClass.SQLEscapeString(ex.Message);
                        HelpClass.showMsg(this, this.GetType(), "HP01:" + strline + "  " + strMsg);
                    }
                }
            }
            else
            {
                Response.Redirect("~/UserLogin.aspx", false);
            }
        }

        private void showExipreDate()
        {
            int EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            try
            {
                DataSet ds = UMBAL.ShowExpireDate(EmpID, out int RemainingCount, out int Status);
                if (Status == 1)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Your password will expire in " + (RemainingCount == 1 ? RemainingCount + " day" : RemainingCount + " days") + ", Please reset your password.", "warning");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "HP02:" + strline + "  " + strMsg);
            }
        }

        private void JsMethod(string msg)
        {
            msg = msg.Replace("'", "");
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "alert", "alert('" + msg + "');", true);
        }
    }
}