﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using AizantIT_DMSBAL;


namespace AizantIT_PharmaApp.ASPXHandlers
{
    public class DocDetails : IHttpHandler
    {
        DocumentCreationBAL objDMS_Bal=new DocumentCreationBAL();

        public void ProcessRequest(HttpContext context)
        {
            //System.Diagnostics.Debugger.Break();
            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");

           
            string SopID = context.Request.QueryString["SopId"];
           // int SopVersion = int.Parse(context.Request.QueryString["VersionId"]);

            DataTable dtSop = objDMS_Bal.DMS_GetDocByID(SopID);
            if (dtSop.Rows.Count > 0)
            {
                DataRow dr = dtSop.Rows[0];
                //Response.Clear();
                context.Response.Buffer = true;
                context.Response.Charset = "";
                if (context.Request.QueryString["download"] == "1")
                {
                    context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + dr["FileName"].ToString());
                }
                context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                if (dr["FileType"].ToString() == "pdf")
                {
                    context.Response.ContentType = "application/pdf";
                }
                context.Response.BinaryWrite((byte[])dr["Content"]);
                context.Response.Flush();
                context.Response.End();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
