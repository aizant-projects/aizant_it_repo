﻿using AizantIT_PharmaApp.Areas.QMS.Models.Audit.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.ASPXHandlers
{
    /// <summary>
    /// Summary description for QMS_AuditPDF_Viewer
    /// </summary>
    public class QMS_AuditPDF_Viewer : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");

            if (context.Request.QueryString.Count > 0)
            {
                if (context.Request.QueryString["BaseID"] != null && context.Request.QueryString["BaseType"] != null)
                {
                    DAL_AuditReport objAuditDal = new DAL_AuditReport();
                    DataTable DtAttachment = objAuditDal.GetAttachmentContent(Convert.ToInt32(context.Request.QueryString["BaseType"]), Convert.ToInt32(context.Request.QueryString["BaseID"]));
                    if (DtAttachment.Rows.Count > 0)
                    {
                        context.Response.Buffer = true;
                        context.Response.Charset = "";
                        context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        context.Response.ContentType = "application/pdf";
                        byte[] PDF_Bytes= { };
                        if (System.IO.File.Exists(context.Server.MapPath("~\\"+DtAttachment.Rows[0]["FilePath"].ToString())))
                        {
                            PDF_Bytes= System.IO.File.ReadAllBytes(context.Server.MapPath("~\\" + DtAttachment.Rows[0]["FilePath"].ToString()));
                        }
                        //context.Response.BinaryWrite((byte[])DtAttachment.Rows[0]["Attachment"]);
                        context.Response.BinaryWrite(PDF_Bytes);
                        context.Response.Flush();
                        context.Response.End();
                    }
                }
            }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}