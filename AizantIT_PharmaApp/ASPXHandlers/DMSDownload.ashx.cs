﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.ASPXHandlers
{
    /// <summary>
    /// Summary description for DMSDownload
    /// </summary>
    public class DMSDownload : IHttpHandler
    {
       
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Write("Hello World");
            if (context.Session["DwnloadFilePath"] != null)
            {
                if (context.Session["DwnloadFilePath"].ToString().Trim().Length >20)
                {
                    System.IO.FileInfo file = new System.IO.FileInfo(context.Session["DwnloadFilePath"].ToString());
                    context.Response.Clear();
                    context.Response.Buffer = true;
                    context.Response.Charset = "";
                    context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    context.Response.ContentType = "application/pdf";
                    context.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                    //Response.AddHeader("Content-Length", file.Length.ToString());
                    context.Response.TransmitFile(file.FullName);
                    //context.Response.Flush();
                    //context.Response.End();
                }

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}