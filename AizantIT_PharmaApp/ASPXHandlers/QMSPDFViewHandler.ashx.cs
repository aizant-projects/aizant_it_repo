﻿using Aizant_API_Entities;
using AizantIT_PharmaApp.Areas.QMS.Models.QmsModel;
using AizantIT_PharmaApp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.ASPXHandlers
{
    /// <summary>
    /// Summary description for QMSPDFViewHandler
    /// </summary>
    public class QMSPDFViewHandler : IHttpHandler
    {
        //private AizantIT_QMS_EF db = new AizantIT_QMS_EF();
        private AizantIT_DevEntities db = new AizantIT_DevEntities();
        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.QueryString.Count > 0)
            {
                if (context.Request.QueryString["EventID"] != null)
                {
                    //Incident
                    if (context.Request.QueryString["EventID"] == "1")
                    {

                        if (context.Request.QueryString["DOCID"] != null)
                        {
                            int FileID = Convert.ToInt32(context.Request.QueryString["DOCID"]);
                            var DocFiles = (from a in db.AizantIT_Incident_IncidentAttachments
                                            where a.UniquePKID == FileID
                                            select a).SingleOrDefault();
                            context.Response.Buffer = true;
                            context.Response.Charset = "";
                            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            context.Response.ContentType = "application/pdf";
                            byte[] PDF_Bytes = { };
                            if (System.IO.File.Exists(context.Server.MapPath("~\\" + DocFiles.FilePath)))
                            {
                                PDF_Bytes = System.IO.File.ReadAllBytes(context.Server.MapPath("~\\" + DocFiles.FilePath));
                            }
                            context.Response.BinaryWrite(PDF_Bytes);
                            context.Response.Flush();
                            context.Response.End();
                        }
                    }

                    //CAPA 
                    else if (context.Request.QueryString["EventID"] == "2")
                    {
                        //Incident
                        if (context.Request.QueryString["DOCID"] != null)
                        {
                            int FileID = Convert.ToInt32(context.Request.QueryString["DOCID"]);
                            var DocFiles = (from a in db.AizantIT_CAPAVerificationFilesAttachements
                                            where a.UniquePKID == FileID
                                            select a).SingleOrDefault();
                                context.Response.Buffer = true;
                                context.Response.Charset = "";
                                context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                                context.Response.ContentType = "application/pdf";
                            byte[] PDF_Bytes = { };
                            if (System.IO.File.Exists(context.Server.MapPath("~\\" + DocFiles.FilePath)))
                            {
                                PDF_Bytes = System.IO.File.ReadAllBytes(context.Server.MapPath("~\\" + DocFiles.FilePath));
                            }
                            context.Response.BinaryWrite(PDF_Bytes);
                            context.Response.Flush();
                                context.Response.End();
                        }
                    }
                   else if (context.Request.QueryString["EventID"] == "TRPT")
                    {
                        if (context.Request.QueryString["DOCID"] != null)
                        {
                            int QualityEventID = Convert.ToInt16(context.Request.QueryString["QualityID"]);

                            if (QualityEventID == 10)  //Print
                            {
                                string strFilePath = DynamicFolder.CreateDynamicFolder(11);
                                string fileName = context.Request.QueryString["DOCID"];
                                strFilePath = strFilePath + "\\" + fileName;
                                context.Response.Clear();
                                context.Response.ContentType = "application/pdf";
                                context.Response.TransmitFile(strFilePath);
                            }

                            byte[] PDF_Bytes = { };

                            if (QualityEventID == 2)//CCN
                            {
                                int EventBaseID = Convert.ToInt32(context.Request.QueryString["DOCID"]);

                                var GetFileView = (from T1 in db.AizantIT_CCNFinalReports
                                                   where T1.CCNID == EventBaseID
                                                   select T1).SingleOrDefault();
                                context.Response.Buffer = true;
                                context.Response.Charset = "";
                                context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                                context.Response.ContentType = "application/pdf";

                                if (System.IO.File.Exists(context.Server.MapPath("~\\" + GetFileView.FinalReport)))
                                {
                                    PDF_Bytes = System.IO.File.ReadAllBytes(context.Server.MapPath("~\\" + GetFileView.FinalReport));
                                }
                                context.Response.BinaryWrite(PDF_Bytes);
                                context.Response.Flush();
                                context.Response.End();
                            }
                            else if (QualityEventID == 1)//Incident
                            {
                                int EventBaseID = Convert.ToInt32(context.Request.QueryString["DOCID"]);

                                var GetFileView = (from T1 in db.AizantIT_IncidentFinalReport
                                                   where T1.IncidentID == EventBaseID
                                                   select T1).SingleOrDefault();
                                context.Response.Buffer = true;
                                context.Response.Charset = "";
                                context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                                context.Response.ContentType = "application/pdf";

                                if (System.IO.File.Exists(context.Server.MapPath("~\\" + GetFileView.FinalReportPath)))
                                {
                                    PDF_Bytes = System.IO.File.ReadAllBytes(context.Server.MapPath("~\\" + GetFileView.FinalReportPath));
                                }
                                context.Response.BinaryWrite(PDF_Bytes);
                                context.Response.Flush();
                                context.Response.End();
                            }

                        }
                    }
                    //CCN Hod
                    else if (context.Request.QueryString["EventID"] == "4")
                    {

                        if (context.Request.QueryString["DOCID"] != null)
                        {
                            int FileID = Convert.ToInt32(context.Request.QueryString["DOCID"]);
                            var GetFileView = (from a in db.AizantIT_HODAssessmentAttachmentFile
                                               where a.UniquePKID == FileID
                                               select a).SingleOrDefault();

                            context.Response.Buffer = true;
                            context.Response.Charset = "";
                            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            context.Response.ContentType = "application/pdf";
                            byte[] PDF_Bytes = { };
                            if (System.IO.File.Exists(context.Server.MapPath("~\\" + GetFileView.FilePath)))
                            {
                                PDF_Bytes = System.IO.File.ReadAllBytes(context.Server.MapPath("~\\" + GetFileView.FilePath));
                            }
                            context.Response.BinaryWrite(PDF_Bytes);
                            context.Response.Flush();
                            context.Response.End();
                        }
                    }
                    //CCN Close
                    else if (context.Request.QueryString["EventID"] == "5")
                    {
                        if (context.Request.QueryString["DOCID"] != null)
                        {
                            int FileID = Convert.ToInt32(context.Request.QueryString["DOCID"]);
                            var GetFileView = (from a in db.AizantIT_CCNCloseFileAttachments
                                               where a.UniquePKID == FileID
                                               select a).SingleOrDefault();
                            //Response.Clear();
                            context.Response.Buffer = true;
                            context.Response.Charset = "";
                            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            context.Response.ContentType = "application/pdf";
                            byte[] PDF_Bytes = { };
                            if (System.IO.File.Exists(context.Server.MapPath("~\\" + GetFileView.FilePath)))
                            {
                                PDF_Bytes = System.IO.File.ReadAllBytes(context.Server.MapPath("~\\" + GetFileView.FilePath));
                            }
                            context.Response.BinaryWrite(PDF_Bytes);
                            context.Response.Flush();
                            context.Response.End();
                        }
                    }
                    //CAPA Complete
                    else if (context.Request.QueryString["EventID"] == "6")
                    {
                        if (context.Request.QueryString["DOCID"] != null)
                        {
                            int FileID = Convert.ToInt32(context.Request.QueryString["DOCID"]);
                            var GetFileView = (from a in db.AizantIT_CapaCompltFileAttachments
                                               where a.UniquePKID == FileID
                                               select a).SingleOrDefault();
                                context.Response.Buffer = true;
                                context.Response.Charset = "";
                                context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                                context.Response.ContentType = "application/pdf";
                                byte[] PDF_Bytes = { };
                                if (System.IO.File.Exists(context.Server.MapPath("~\\" + GetFileView.FilePath)))
                                {
                                    PDF_Bytes = System.IO.File.ReadAllBytes(context.Server.MapPath("~\\" + GetFileView.FilePath));
                                }
                            context.Response.BinaryWrite(PDF_Bytes);
                            context.Response.Flush();
                                context.Response.End();
                        }
                    }
                    //CCN Initiator File view
                    else if (context.Request.QueryString["EventID"] == "7")
                    {
                        if (context.Request.QueryString["DOCID"] != null)
                        {
                            int FileID = Convert.ToInt32(context.Request.QueryString["DOCID"]);
                            var GetFileView = (from a in db.AizantIT_CCNInitiatorFileAttachment
                                               where a.UniquePKID == FileID
                                               select a).SingleOrDefault();

                            context.Response.Buffer = true;
                            context.Response.Charset = "";
                            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            context.Response.ContentType = "application/pdf";
                            byte[] PDF_Bytes = { };
                            if (System.IO.File.Exists(context.Server.MapPath("~\\" + GetFileView.FilePath)))
                            {
                                PDF_Bytes = System.IO.File.ReadAllBytes(context.Server.MapPath("~\\" + GetFileView.FilePath));
                            }
                            context.Response.BinaryWrite(PDF_Bytes);
                            context.Response.Flush();
                            context.Response.End();
                        }
                    }
                    //Incident File view
                    else if (context.Request.QueryString["EventID"] == "8")//Incident
                    {
                        if (context.Request.QueryString["DOCID"] != null)
                        {
                            int FileID = Convert.ToInt32(context.Request.QueryString["DOCID"]);
                            var GetFileView = (from a in db.AizantIT_Incident_InitiatorAttachments
                                               where a.UniquePKID == FileID
                                               select a).SingleOrDefault();
                            context.Response.Buffer = true;
                            context.Response.Charset = "";
                            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            context.Response.ContentType = "application/pdf";
                            byte[] PDF_Bytes = { };
                            if (System.IO.File.Exists(context.Server.MapPath("~\\" + GetFileView.FilePath)))
                            {
                                PDF_Bytes = System.IO.File.ReadAllBytes(context.Server.MapPath("~\\" + GetFileView.FilePath));
                            }
                            context.Response.BinaryWrite(PDF_Bytes);
                            context.Response.Flush();
                            context.Response.End();
                        }
                    }
                }
            }
        }
        

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}