﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using AizantIT_DMSBAL;
namespace AizantIT_PharmaApp.ASPXHandlers
{
    /// <summary>
    /// Summary description for docdetaik
    /// </summary>
    public class docdetaik : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        public void ProcessRequest(HttpContext context)
        {
            //System.Diagnostics.Debugger.Break();
            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");
            string SopID = context.Request.QueryString["SopId"];
            // int SopVersion = int.Parse(context.Request.QueryString["VersionId"]);           
            if (context.Request.QueryString.Count == 1)
            {
                DataTable dtSop = DMS_Bal.DMS_GetDocByID(SopID);
                if (dtSop.Rows.Count > 0)
                {
                    DataRow dr = dtSop.Rows[0];
                    //Response.Clear();
                    context.Response.Buffer = true;
                    context.Response.Charset = "";
                    context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    if (dr["FileType"].ToString() == "pdf")
                    {
                        context.Response.ContentType = "application/pdf";
                    }
                    context.Response.BinaryWrite((byte[])dr["Content"]);
                    context.Response.Flush();
                    context.Response.End();
                }
            }
            if (context.Request.QueryString.Count == 2)
            {
                if (context.Request.QueryString["sessop"].Trim() == "true")
                {
                    if (HttpContext.Current.Session["ExistingFileContent"] != null)
                    {
                        context.Response.Buffer = true;
                        context.Response.Charset = "";
                        context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        context.Response.ContentType = "application/pdf";
                        context.Response.BinaryWrite(HttpContext.Current.Session["ExistingFileContent"] as byte[]);
                        context.Response.Flush();
                        context.Response.End();
                    }
                }
            }
            if (context.Request.QueryString.Count == 3)
            {
                if (context.Request.QueryString["ref"].Trim() == "1")
                {
                    if (HttpContext.Current.Session["TempRefFileContent"] != null)
                    {
                        context.Response.Buffer = true;
                        context.Response.Charset = "";
                        context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        context.Response.ContentType = "application/pdf";
                        context.Response.BinaryWrite(HttpContext.Current.Session["TempRefFileContent"] as byte[]);
                        context.Response.Flush();
                        context.Response.End();
                    }
                }
            }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
