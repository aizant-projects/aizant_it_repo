﻿using Aizant_API_Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.ASPXHandlers
{
    /// <summary>
    /// Summary description for RegulatoryPDFViewerHandler
    /// </summary>
    public class RegulatoryPDFViewerHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var dbRegulatory = new AizantIT_DevEntities();
            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");

            if (context.Request.QueryString.Count > 0)
            {
                if (context.Request.QueryString["AttachmentID"] != null )
                {
                    int AttachmentID = Convert.ToInt32(context.Request.QueryString["AttachmentID"]);
                    var defDetails = (from def in dbRegulatory.AizantIT_DeficiencyAttachment
                                      where def.DeficiencyAttachmentID == AttachmentID 
                                      select new
                                      {
                                          def.Attachment,
                                          def.FileExtention
                                      }).SingleOrDefault();
                    byte[] filecontent = (byte[])defDetails.Attachment;
                  
                        context.Response.Buffer = true;
                        context.Response.Charset = "";
                        context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        context.Response.ContentType = "application/pdf";
                        context.Response.BinaryWrite(filecontent);
                        context.Response.Flush();
                        context.Response.End();
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}