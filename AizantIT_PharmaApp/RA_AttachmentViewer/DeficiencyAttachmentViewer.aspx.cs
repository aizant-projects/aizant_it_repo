﻿using Aizant_API_Entities;
using AizantIT_PharmaApp.Areas.QMS.Models.Audit.DataLayer;
using AizantIT_PharmaApp.Areas.QMS.Models.QmsModel;
using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.IO;
using System.Linq;

namespace AizantIT_PharmaApp.RA_AttachmentViewer
{
    public partial class DeficiencyAttachmentViewer : System.Web.UI.Page
    {
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString.Count > 0)
                    {
                        string AttachmentID = Request.QueryString["AttachmentID"].ToString();
                        LoadAttachmentInIframe(Convert.ToInt32(AttachmentID));
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("QMS_AIF_M1:" + strline + "  " + strMsg);
                string strError = "QMS_AIF_M1:" + strline + "  " + "Page loading failed";
                literalPdf.Visible = true;
                Spreadsheet.Visible = false;
                RichEdit.Visible = false;
                binImage.Visible = false;
                literalPdf.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        public void LoadAttachmentInIframe(int AttachmentID)
        {
            try
            {
                var dbRegulatory = new AizantIT_DevEntities();
                DocumentViewerBO obj = new DocumentViewerBO();
                string fname = string.Empty, FileExtn = string.Empty; byte[] filecontent = null;
                // FileExtn = FileExtenssion;
                // fname = FileName;


                var defDetails = (from def in dbRegulatory.AizantIT_DeficiencyAttachment
                                  where def.DeficiencyAttachmentID == AttachmentID
                                  select new
                                  {
                                      def.Attachment,
                                      def.FileExtention
                                  }).SingleOrDefault();
                filecontent = (byte[])defDetails.Attachment;
                FileExtn = defDetails.FileExtention;

                if (filecontent == null || filecontent.Length == 0)
                {
                    literalPdf.Visible = true;
                    Spreadsheet.Visible = false;
                    RichEdit.Visible = false;
                    binImage.Visible = false;
                    literalPdf.Text = "<p style='color: red; '><b>File is not available</b></p>";
                    return;
                }
                binImage.Visible = false;
                Spreadsheet.Visible = false;
                RichEdit.Visible = false;
                literalPdf.Visible = false;
                string ShowFileIn = "pdf";
                if (FileExtn == "pdf")
                {
                    literalPdf.Visible = true;
                    filecontent = null;

                    literalPdf.Text = " <embed src=\"" + ResolveUrl("~/ASPXHandlers/RegulatoryPDFViewerHandler.ashx") + "?AttachmentID=" + AttachmentID + " #toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" height=\"661px\" />";
                }
                else if (FileExtn == "jpeg" || FileExtn == "jpg" || FileExtn == "png" || FileExtn == "gif" || FileExtn == "bmp")
                {
                    ShowFileIn = "Image";
                }
                else if (FileExtn == "xls" || FileExtn == "xlsx")
                {
                    ShowFileIn = "XLSheet";
                }
                else if (FileExtn == "rtf" || FileExtn == "doc" || FileExtn == "docx" || FileExtn == "txt")
                {
                    ShowFileIn = FileExtn;
                }

                switch (ShowFileIn)
                {
                    case "Image":
                        System.Drawing.Image image = System.Drawing.Image.FromStream(new System.IO.MemoryStream(filecontent));
                        int imgWidth = image.Width;
                        if (imgWidth > 500)
                        {
                            binImage.Width = System.Web.UI.WebControls.Unit.Percentage(100);
                        }
                        else
                        {
                            binImage.Width = System.Web.UI.WebControls.Unit.Percentage(50);
                        }
                        binImage.Visible = true;
                        binImage.ContentBytes = filecontent;
                        break;
                    case "pdf":
                        literalPdf.Visible = true;
                        filecontent = null;
                        literalPdf.Text = " <embed src=\"" + ResolveUrl("~/ASPXHandlers/RegulatoryPDFViewerHandler.ashx") + "?AttachmentID=" + AttachmentID + " #toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" height=\"661px\" />";

                        break;
                    case "XLSheet":
                        Spreadsheet.Visible = true;
                        Spreadsheet.Open(
                           Guid.NewGuid() + DateTime.Now.ToString("hhmmss").ToString(),
                           DevExpress.Spreadsheet.DocumentFormat.Xls,
                           () =>
                           {
                               byte[] docBytes = filecontent;
                               return new MemoryStream(docBytes);
                           }
                           );
                        break;
                    case "rtf":
                        RichEdit.Visible = true;
                        RichEdit.Open(
                           Guid.NewGuid() + DateTime.Now.ToString("hhmmss").ToString(),
                           DevExpress.XtraRichEdit.DocumentFormat.Rtf,
                           () =>
                           {
                               byte[] docBytes = filecontent;
                               return new MemoryStream(docBytes);
                           }
                           );
                        break;
                    case "doc":
                        RichEdit.Visible = true;
                        RichEdit.Open(
                          Guid.NewGuid() + DateTime.Now.ToString("hhmmss").ToString(),
                          DevExpress.XtraRichEdit.DocumentFormat.Doc,
                          () =>
                          {
                              byte[] docBytes = filecontent;
                              return new MemoryStream(docBytes);
                          }
                          );
                        break;
                    case "docx":
                        RichEdit.Visible = true;
                        RichEdit.Open(
                          Guid.NewGuid() + DateTime.Now.ToString("hhmmss").ToString(),
                          DevExpress.XtraRichEdit.DocumentFormat.OpenXml,
                          () =>
                          {
                              byte[] docBytes = filecontent;
                              return new MemoryStream(docBytes);
                          }
                          );
                        break;
                    case "txt":
                        RichEdit.Visible = true;
                        RichEdit.Open(
                          Guid.NewGuid() + DateTime.Now.ToString("hhmmss").ToString(),
                          DevExpress.XtraRichEdit.DocumentFormat.PlainText,
                          () =>
                          {
                              byte[] docBytes = filecontent;
                              return new MemoryStream(docBytes);
                          }
                          );
                        break;
                }

                //while (RichEdit.Loaded)
                //{
                //    int i = 0;
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
          
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}