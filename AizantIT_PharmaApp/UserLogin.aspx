﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserLogin.aspx.cs" Inherits="AizantIT_PharmaApp.UserLogin" EnableEventValidation="false" %>

<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Aizant Pharma Intelligence</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="<%# ResolveUrl("~/AppCSS/Bootstrap/css/bootstrap4.css")%>" rel="stylesheet" />
    <script src="<%# ResolveUrl("~/Scripts/jquery-3.2.1.min.js")%>"></script>
    <script src="<%# ResolveUrl("~/Scripts/moment.min.js")%>"></script>
    <script src="<%# ResolveUrl("~/Scripts/popper.js")%>"></script>
    <link href="<%# ResolveUrl("~/Content/font-awesome.min.css")%>" rel="stylesheet" />
    <script src="<%# ResolveUrl("~/AppCSS/Bootstrap/scripts/bootstrap4.min.js")%>"></script>
    <script src="<%# ResolveUrl("~/AppScripts/GlobalScripts.js")%>"></script>
    <link rel="shortcut icon" href="<%# ResolveUrl("~/favicon.ico")%>" />
    <link href="<%# ResolveUrl("~/AppCSS/login/login_api.css")%>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/AppCSS/Common/Progress.css")%>" rel="stylesheet" />
</head>
<body class="bg_login">
    <form id="fmUserLogin" runat="server" autocomplete="off">
        <script>
            window.onload = function () {
                document.cookie = "_inApp=; path=/";
                if (document.cookie.indexOf("_instance=true") === -1) {
                    document.cookie = "_instance=true";
                    window.onunload = function () {
                        document.cookie = "_instance=true;expires=Thu, 01-Jan-1900 00:00:01 GMT; path=/";
                    };
                }
                else {
                    var win = window.open("<%=ResolveUrl("~/InvalidPageAccess.html")%>", "_self"); win.close();
                }
            };
        </script>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <input type="text" style="display: none" />
        <input type="password" id="fakePwd" style="display: none" />
        <asp:HiddenField ID="hdnUserID" runat="server" />
        <asp:HiddenField ID="hdnPwd" runat="server" />
        <asp:HiddenField ID="hdnIsPost" runat="server" Value="N" />
        <div id="divhide">
            <div class="opacity_login col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12"></div>
            <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12  padding-none float-left">
                <div class=" col-lg-5 col-xl-3 col-md-5 col-sm-12 col-12 login_panel_left  float-left padding-none login-panel">
                    <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 logo float-left">
                        <img src="<%= ResolveUrl("~/Images/UserLogin/Aizant_Logo.png")%>" class="center-block img-fluid" alt="Aizant" />
                    </div>
                    <!-------lOGIN START-------->
                    <div id="signIn" runat="server">
                        <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 login_icon_bg float-left">
                            <div class="col-lg-4 col-md-4 col-xl-4 col-sm-4 col-4 offset-4  user_login " id="loginheadernm">
                                Login
                            </div>
                            <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 float-left sign_up_header" id="resetpwdnm" style="display: none">Change Password <a href="#" id="A1" runat="server" onserverclick="btnCancel_Click"><span id="signUpCancel" style="padding-top: 3px;" tabindex="-1" class="glyphicon glyphicon-remove float-right close_button"></span></a></div>
                        </div>
                        <div class="form-horizontal  padding-none col-12 float-left">
                            <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 login_details float-left top">
                                <div class="col-12 float-left padding-none" id="Logindiv">
                                    <asp:UpdatePanel ID="UpdateLogin" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group top">
                                                <label for="txtLoginUserName" class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 control-label custom_label_login ">Login ID<span class="smallredLogin">*</span></label>
                                                <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12">
                                                    <input type="text" onkeypress="return nospaces(event);" onkeydown="return RestrictEnterKey(event);"
                                                        autocomplete="off" class="form-control login_input" id="txtLoginUserName"
                                                        placeholder="Enter Login ID" runat="server" tabindex="1" maxlength="20" title="Login ID" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="txtLoginPassword" class="col-sm-12 control-label custom_label_login">Password<span class="smallredLogin">*</span></label>
                                                <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12">
                                                    <input type="password" onpaste="return false;" onkeypress="return nospaces(event)"
                                                        class="form-control login_input login_input_pwd " id="txtLoginPassword"
                                                        placeholder="Enter Password" runat="server" tabindex="2"
                                                        maxlength="20" title="Password" />
                                                </div>
                                            </div>
                                            <%-- <div class="form-group">
                                        <a href="#" class="col-sm-12">
                                            <div class="float-right forget" id="clickForget" title="Click if you Forget password.">Forgot Password?</div>
                                        </a>
                                    </div>--%>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <%--  <div class="form-group">
                                <div class="col-sm-12" >
                                    <%--<button type="button" class=" col-sm-6 float-left btn btn-signup" id="clickSignUp" tabindex="5" title="Click to Create SignUP">Sign Up</button>                                    
                                    <asp:Button ID="btnLogin" CssClass="col-sm-6 float-right btn btn-login" runat="server" Text="Log In" OnClientClick="return RequiredValidation();" OnClick="btnLogin_Click" TabIndex="3" ToolTip="Click to Login" />
                                </div>
                            </div>--%>
                                    <div class="form-group top">
                                        <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 float-left">
                                            <asp:Button ID="btnLogin" CssClass="col-sm-12 float-right btn btn-login" runat="server" Text="Sign In" OnClientClick="return RequiredValidation();" OnClick="btnLogin_Click" TabIndex="3" ToolTip="Click to Sign In" />
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 float-left">
                                            <a href="#">
                                                <div style="margin-top: 0px;" class="float-right forget" id="clickForget" title="Click here, if you Forgot Password?">Forgot Password?</div>
                                            </a>
                                            <a>
                                                <div style="margin-top: 0px; margin-right: 10px;" class="float-left forget_sign" id="clickSignUp" tabindex="5" title="Click here, To Register User Login">Register User Login</div>
                                            </a>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 float-left padding-none" id="pwdresetdiv" style="display: none;">
                                    <div class="form-group top">
                                        <label class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 control-label custom_label_login ">Login ID<span class="smallredLogin">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12">
                                            <input type="text" onpaste="return false;" onkeypress="return nospaces(event)"
                                                class="form-control login_input login_input_pwd " id="txtReset_LoginID"
                                                runat="server" disabled="disabled"
                                                maxlength="20" title="Login ID"
                                                placeholder="Enter Old Password" />
                                        </div>
                                    </div>
                                    <div class="form-group top">
                                        <label class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 control-label custom_label_login ">New Password<span class="smallredLogin">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12">
                                            <input type="password" onpaste="return false;" onkeypress="return nospaces(event)"
                                                class="form-control login_input login_input_pwd " id="txtReset_NewPassword"
                                                runat="server"
                                                maxlength="20" data-toggle="tooltip" data-placement="bottom" title=" Password must contain atleast 8 characters,one uppercase,one lowercase,one special character and one number."
                                                placeholder="Enter New Password" />
                                        </div>
                                    </div>
                                    <div class="form-group top">
                                        <label class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 control-label custom_label_login ">Confirm Password<span class="smallredLogin">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12">
                                            <input type="password" onpaste="return false;" onkeypress="return nospaces(event)"
                                                class="form-control login_input login_input_pwd " onkeydown="if (event.keyCode == 13) { return false; }" id="txtReset_ConfirmPassword"
                                                runat="server"
                                                maxlength="20" data-toggle="tooltip" data-placement="bottom" title="Password and Confirm Password should be same"
                                                placeholder="Enter Confirm Password" />
                                        </div>
                                    </div>
                                    <div class="col-12 form-group top text-right">
                                        <button type="button" class=" btn btn-signup_popup" id="btnResetSubmit">Submit</button>
                                        <button type="button" class=" btn btn-cancel_popup" id="btnResetcancel">Cancel</button>
                                    </div>
                                </div>
                                <div class="form-group col-12 text-center">
                                    <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 float-left" style="opacity: 0.6">
                                        <img src="<%= ResolveUrl("~/Images/UserLogin/Aizant_bg.png")%>" class="align-center img-fluid" width="29%" alt="Aizant" />
                                    </div>
                                </div>
                                <div class="form-group col-12 ">
                                    <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 float-left copyrights copy_right1">
                                        Your Copyright ©
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 float-left copyrights copy_right2">
                                        Your Copyright ©
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12">
                                        <asp:Label ID="lblLogin" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-------LOGIN-IN STOP-------->

                    <!-------SIGN-UP START-------->
                    <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 float-left padding-none" style="display: none;" id="signUp" runat="server">
                        <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 float-left sign_up_header">Register User Login <a href="#" id="btn_sinup_cancel" runat="server" onserverclick="btnCancel_Click"><span id="signUpCancel" style="padding-top: 3px;" class="glyphicon glyphicon-remove float-right close_button"></span></a></div>
                        <div class="form-horizontal col-12 float-left ">
                            <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 float-left sign_up_form padding-none top">
                                <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 padding-none float-left">
                                    <asp:UpdatePanel ID="UpdateSignUp" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group col-12 float-left">
                                                <label for="inputEmail3" class=" padding-none col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 control-label custom_label_answer">Login ID<span class="smallredLogin">*</span></label>
                                                <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 padding-none">
                                                    <input type="text" autocomplete="off" onkeydown="return RestrictEnterKey(event);" onkeypress="return nospaces(event);" class="form-control login_input_sign_up" name="name" id="txtLoginID" maxlength="20" runat="server" placeholder="Enter Login ID" tabindex="6" title="LoginID" />
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 float-left padding-none">
                                                <div class="form-group col-lg-6 col-md-6 col-xl-6 col-sm-6 col-6 float-left">
                                                    <label for="inputPassword3" class=" col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12padding-none control-label  custom_label_answer">Password<span class="smallredLogin">*</span></label>
                                                    <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 padding-none">
                                                        <input type="password" onkeydown="return RestrictEnterKey(event);" onpaste="return false;" onkeypress="return nospaces(event)" runat="server" class="form-control login_input_sign_up " data-toggle="tooltip" data-placement="bottom"
                                                            title="Password must contain atleast 8 characters,one uppercase,one lowercase,one special character and one number."
                                                            id="txtPassword" placeholder="Enter New Password" tabindex="7" maxlength="20" />
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-6 col-md-6 col-xl-6 col-sm-6 col-6  float-left">
                                                    <label for="inputPassword3" class="  padding-none col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 control-label  custom_label_answer">Confirm Password<span class="smallredLogin">*</span></label>
                                                    <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 padding-none">
                                                        <input type="password" onpaste="return false;" onkeydown="return RestrictEnterKey(event);" onkeypress="return nospaces(event)" runat="server" class="form-control login_input_sign_up"
                                                            data-toggle="tooltip" data-placement="bottom"
                                                            title="Password and Confirm Password Should be Same" id="txtConfirmPassword" placeholder="Re-Enter Password" tabindex="8" maxlength="20" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 float-left">
                                                <label for="inputPassword3" class=" col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 padding-none control-label  custom_label_answer">Security Question<span class="smallredLogin">*</span></label>
                                                <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 padding-none">

                                                    <textarea class="form-control" onkeydown="return RestrictEnterKey(event);" id="txtSecurityQuestion" onkeypress="return NoSpaceAllow(event);" rows="4" maxlength="200" cols="12" runat="server" placeholder="Enter Security Question ?" tabindex="9" onkeyup="characterCounter(txtSecurityQuestion, this.form.remCount, 200);" aria-multiline="true" title="Security Question"></textarea>
                                                    <div class="answer_count">
                                                        <input type="text" id="txtCount" name="remCount" size="3" maxlength="3" value="200" readonly="readonly" style="text-align: right; color: #38b8ba; font-family: seguisb; border: 0px;" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 float-left">
                                                <label for="inputPassword3" class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 padding-none control-label  custom_label_answer custom_label_answer_lbl">Answer<span class="smallredLogin">*</span></label>
                                                <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 padding-none">
                                                    <input type="text" autocomplete="off" onkeydown="return RestrictEnterKey(event);" onkeypress="return NoSpaceAllowAns(event);" class="form-control login_input_sign_up" runat="server" id="txtSecurityAnswer" placeholder="Enter Security Answer" tabindex="10" maxlength="20" title="Security Answer" />
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div class="form-group col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 float-left">
                                        <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 padding-none" style="margin-top: 12px;">
                                            <asp:UpdatePanel ID="upCreatebtnSignup" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Button ID="btn_cancelsignup" CssClass="float-right btn btn-cancel_popup" runat="server" Text="Cancel" OnClick="btnCancel_Click" TabIndex="11" ToolTip="Click to Cancel" />
                                                    <asp:Button ID="btnCreate" CssClass="float-right btn btn-signup_popup" Style="margin-right: 5px;" runat="server" Text="Create" OnClientClick="return SignUpValidate();" OnClick="btnCreate_Click" TabIndex="11" ToolTip="Click to Create SignUP" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                                <div class="Panel_Footer_Message_Label Model_Msg">
                                    <asp:UpdatePanel ID="UPlblSignupMessage" runat="server" UpdateMode="Always">
                                        <ContentTemplate>
                                            <asp:Label ID="lblSignupMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
                                            </div>
                                </div>
                            </div>
                        </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <!-------SIGN-UP END-------->

                                    <!-------FORGET PASSWORD START-------->
                                    <asp:UpdatePanel ID="uphdnField" runat="server">
                                        <ContentTemplate>
                                            <asp:HiddenField ID="hdnStep" runat="server" Value="0" />
                                            <asp:HiddenField ID="HFEmpID" runat="server" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                    <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 padding-none float-left" id="forgetPassword" runat="server" style="display: none;">
                                        <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 sign_up_header  float-left">Forgot Password <a href="#" id="btn_forgetPassword_Cancel" runat="server" onserverclick="btnCancel_Click"><span id="ForgetCancel" style="padding-top: 3px;" class="glyphicon glyphicon-remove float-right close_button"></span></a></div>
                                        <div class="form-horizontal col-12 float-left">
                                            <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 float-left sign_up_div padding-none">
                                                <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 float-left div_sign padding-none">
                                                    <div id="oneNext" runat="server">
                                                        <asp:UpdatePanel ID="UpdateForget" runat="server">
                                                            <ContentTemplate>
                                                                <div class="form-group col-12 float-left top">
                                                                    <label for="inputEmail3" class="padding-none col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 control-label custom_label_answer">Login ID<span class="smallredLogin">*</span></label>
                                                                    <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 padding-none">
                                                                        <input type="text" autocomplete="off" onkeydown="return RestrictEnterKey(event);" onkeypress="return nospaces(event)" class="form-control login_input_sign_up" id="txtLoginIDpwd" runat="server" placeholder="Enter Login ID" maxlength="20" title="Login ID" />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-12 float-left">
                                                                    <label for="inputEmail3" class=" padding-none col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 control-label custom_label_answer">Email<span class="smallredLogin">*</span></label>
                                                                    <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 padding-none">
                                                                        <input type="text" autocomplete="off" onkeydown="return RestrictEnterKey(event);" onkeypress="return nospaces(event)" class="form-control login_input_sign_up" id="txtEmailID" runat="server" placeholder="Email@domain.com" maxlength="100" title="Email" />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-12 float-left" id="hideButtonGroup" runat="server">
                                                                    <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 padding-none float-left text-right" style="margin-top: 12px;">
                                                                        <asp:Button ID="btn_Step1" runat="server" CssClass=" btn btn-signup_popup" OnClick="btn_LoginEmail_Click" OnClientClick=" return ForgetValidation();" Text="Next" ToolTip="Next Step" />

                                                                        <asp:Button ID="Button2" CssClass=" btn btn-cancel_popup" runat="server" Text="Cancel" OnClick="btnCancel_Click" ToolTip="Cancel" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <div id="twoNext" runat="server">
                                                        <asp:UpdatePanel ID="UpdateQuesAns" runat="server">
                                                            <ContentTemplate>
                                                                <div class="form-group col-12 float-left top">
                                                                    <label for="inputPassword3" class="padding-none col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 control-label  custom_label_answer">Security Question</label>
                                                                    <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 padding-none">
                                                                        <textarea class="form-control " onkeydown="return RestrictEnterKey(event);" id="txtSecurityQues" rows="3" runat="server" placeholder="Security Question" disabled="disabled" maxlength="200" oncopy="return false" title="Security Question"></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-12 float-left">
                                                                    <label for="inputPassword3" class="padding-none col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 control-label  custom_label_answer">Answer<span class="smallredLogin">*</span></label>
                                                                    <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 padding-none">
                                                                        <input type="text" autocomplete="off" onkeydown="return RestrictEnterKey(event);" class="form-control login_input_sign_up" onkeypress="return NoSpaceAllowUserAns(event);" id="txtUserAnswer" runat="server" placeholder="Enter Security Answer" maxlength="20" oncopy="return false" title="Security Answer" />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-12 float-left" id="hideTwoButtonGroup" runat="server">
                                                                    <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 padding-none text-right" style="margin-top: 12px;">
                                                                        <asp:Button ID="btn_Step2" runat="server" CssClass="steptwo  btn btn-signup_popup" OnClientClick="return SecurityValidation();" OnClick="btn_SecurityQues_Click" Text="Next" ToolTip="Next Step" />

                                                                        <asp:Button ID="Button1" CssClass=" btn btn-cancel_popup" runat="server" Text="Cancel" OnClick="btnCancel_Click" ToolTip="Cancel" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <div id="stepThree" runat="server">
                                                        <asp:UpdatePanel ID="UpdatePassword" runat="server">
                                                            <ContentTemplate>
                                                                <div class="form-group col-12 float-left top">
                                                                    <label class="padding-none col-sm-12 control-label  custom_label_answer">Password<span class="smallredLogin">*</span></label>
                                                                    <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 padding-none">
                                                                        <input type="password" onpaste="return false" onkeydown="return RestrictEnterKey(event);" onkeypress="return nospaces(event)" class="form-control login_input_sign_up " data-toggle="tooltip" data-placement="bottom" title=" Password must contain atleast 8 characters,one uppercase,one lowercase,one special character and one number."
                                                                            id="txtNewPwd" runat="server" placeholder="Enter New Password" maxlength="20" />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-12 float-left">
                                                                    <label class="padding-none  col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 control-label  custom_label_answer">Confirm Password<span class="smallredLogin">*</span></label>
                                                                    <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 padding-none">
                                                                        <input type="password" onpaste="return false" onkeydown="return RestrictEnterKey(event);" onkeypress="return nospaces(event)" class="form-control login_input_sign_up" runat="server" data-toggle="tooltip" data-placement="bottom" title="Password and Confirm Password should be same"
                                                                            id="txtConfirmPwd" placeholder="Re-Enter Password" maxlength="20" />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-12 float-left">
                                                                    <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 padding-none text-right" style="margin-top: 12px;">
                                                                        <a href="#">
                                                                            <asp:Button ID="btnCreatePassword" CssClass=" btn btn-signup_popup" runat="server" Text="Submit" OnClientClick="return confirmpasswordvalidation();" OnClick="btnCreatePassword_Click" ToolTip="Create Password" /></a>
                                                                        <a href="#">
                                                                            <asp:Button ID="btnCancel" CssClass=" btn btn-cancel_popup" runat="server" Text="Cancel" OnClick="btnCancel_Click" ToolTip="Cancel" /></a>
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <div class="form-group col-12 text-center">
                                                        <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 float-left" style="opacity: 0.6">
                                                            <img src="<%= ResolveUrl("~/Images/UserLogin/Aizant_bg.png")%>" class="center-block img-fluid" width="29%" alt="Aizant" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-12 float-left">
                                                        <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 float-left copyrights copy_right1">
                                                            Your Copyright ©
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 float-left copyrights copy_right2">
                                                            Your Copyright ©
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-------FORGET PASSWORD END-------->

                                    <asp:UpdatePanel ID="UPlblMsg" runat="server" UpdateMode="Always">
                                        <ContentTemplate>
                                            <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 col-12 alert_box" runat="server" visible="false" id="alert_Box">
                                                <p class="col-lg-10 col-md-10 col-xl-10 col-sm-10 col-10">
                                                    <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                                                </p>
                                                <span class="glyphicon glyphicon-remove float-right close_button col-lg-2 col-md-2 col-xl-2 col-sm-2 col-2" id="alert_Close"></span>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:Button ID="btnResetPWD" runat="server" Text="Button" Style="display: none;" OnClick="btnResetPWD_Click" />
        <!--------Logged In-------------->
        <div id="modalLoggedIn" class="modal login_popup" role="dialog" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-dialog-centered">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">

                        <div class="usersession_details">Your User Id: " <span class="HeaderuserID"></span>" Is Already Logged In.</div>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group padding-none">
                            <div class="modalSession">Do You Want To Close The Existing Session For "<span class="userID">"username"</span>" And Start a New Session?</div>
                            <div class="top"><span class="modalNote">Note :</span> <span class="modalHint">Closing a Session While Currently Working In It May Cause Data Problems.</span></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnReLogin" class=" btn-signup_popup" runat="server" Text="Close Session" OnClick="btnReLogin_Click" />
                        <a href="#" class=" btn-cancel_popup " data-dismiss="modal">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
        <!-------- End Logged In popup-------------->

        <!---Error popup--->
        <div id="ModalError" class="modal ModalDanger error_popup" role="dialog" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-dialog-centered">
                <!-- Modal content-->
                <div class="modal-content modalMainContent">
                    <div class="modal-header ModalHeaderPart ">
                        <div class="error_icon float-left">

                            <h4 class="modal-title float-left popup_title">Error</h4>
                        </div>

                    </div>
                    <div class="modal-body">
                        <div class="col-12">
                            <%-- <div style="position:absolute;right:0px;bottom:0px;top: 8px;"><img class="apilogo_small" src="Images/Landing/api_logo_small.png" /></div>--%>
                            <asp:UpdatePanel ID="upds" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <p id="msgError" class="col-lg-12" style="padding: 10px 12px; max-height: 300px; overflow-x: auto; font-family: arial; color: #f95a5a; font-weight: bold;" runat="server"></p>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="modal-footer ModalFooterPart">
                        <button type="button" id="btnErrorOk" class="btn-cancel_popup" data-dismiss="modal" style="border-top: 1px solid #f95a5a;" runat="server">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Error Popup-->

        <!-- Success content-->
        <div id="ModalSuccess" class="modal ModalSucess success_popup" role="dialog" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-dialog-centered">

                <!-- Modal content-->
                <div class="modal-content modalMainContent">
                    <div class="modal-header ModalHeaderPart" style="padding-top: 5px; padding-bottom: 5px;">
                        <div class="success_icon float-left">

                            <h4 class="modal-title popup_title float-left">Success</h4>
                        </div>

                    </div>
                    <div class="modal-body">
                        <div class="col-12" style="padding: 11px 0px;">
                            <%--                                                         <div style="position:absolute;right:0px;bottom:0px;top: 8px;"><img class="apilogo_small" src="Images/Landing/api_logo_small.png" /></div>--%>

                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <p id="msgSuccess" class="col-lg-12" style="padding: 10px 12px; max-height: 128px; overflow-x: auto;" runat="server"></p>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="modal-footer ModalFooterPart">
                        <button type="button" id="btnSuccessOk" class="btn-signup_popup" data-dismiss="modal" runat="server">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Success content-->

        <!-- Information content-->
        <div id="ModalInfo" class="modal info_popup ModalConfirm" role="dialog">
            <div class="modal-dialog modal-dialog-centered">
                <!-- Modal content-->
                <div class="modal-content modalMainContent">
                    <div class="modal-headerinfo ModalHeaderPart">
                        <div class="success_icon float-left">
                            <h4 class="modal-title popup_title float-left">Information</h4>
                        </div>

                    </div>
                    <div class="modal-body" style="padding: 0px">
                        <div class="col-12">
                            <%--                                                        <div style="position:absolute;right:0px;bottom:0px;top: 8px;"><img class="apilogo_small" src="Images/Landing/api_logo_small.png" /></div>--%>


                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <p id="msgInfo" class="col-lg-10" style="padding: 10px 12px; max-height: 128px; overflow-x: auto;" runat="server"></p>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="modal-footer ModalFooterPart">
                        <button type="button" id="btnInfoOk" class="btn-info_popup" data-dismiss="modal" runat="server">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Information content-->

        <!-- confirmation content-->
        <uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />
        <!-- End confirmation content-->

        <!-- Warning content-->
        <div id="ModalWarning" class="modal warning_popup ModalWarning" role="dialog" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-dialog-centered">
                <!-- Modal content-->
                <div class="modal-content modalMainContent">
                    <div class="modal-header ModalHeaderPart">
                        <div class="success_icon float-left">
                            <h4 class="modal-title popup_title float-left">Warning</h4>
                        </div>

                    </div>
                    <div class="modal-body" style="padding: 0px">
                        <div class="col-12">
                            <%--                                                         <div style="position:absolute;right:0px;bottom:0px;top: 8px;"><img class="apilogo_small" src="Images/Landing/api_logo_small.png" /></div>--%>

                            <p id="msgWarning" class="col-lg-12" runat="server" style="padding: 10px 12px; max-height: 128px; overflow-x: auto; font-family: arial; color: #b18100; font-weight: bold;"></p>
                        </div>
                    </div>
                    <div class="modal-footer ModalFooterPart">
                        <button type="button" id="btnWarningOk" class="btn-warning_popup" data-dismiss="modal" runat="server">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Warning content-->
        <div id="divPleaseWaitgif" class="PW_Modal" style="display: none">
            <div class="login_ProgressCenter">
                <img alt="Please Wait..." src='<%= ResolveUrl("~/Images/LodingGif/PW_Processing_new.gif")%>' />
            </div>
        </div>
        <div id="divPleaseWaitNoImg" class="PW_Modal_inner" style="display: none">
            <div class="PW_Center">
            </div>
        </div>
        <script>
            //$(function () {
            //    custAlertMsg('This Website is temporarily unavailable from 4pm to 6pm today due to system maintenance.', 'warning');
            //});
            function ShowPleaseWait(visible) {
                if (visible == 'show') {
                    $('#divPleaseWaitNoImg').show();
                }
                else {
                    $('#divPleaseWaitNoImg').hide();
                }
            };
            //$('#divPleaseWaitgif').show();
        </script>

        <!--on user server validation-->
        <script>           
            function ServerValidationOnLogin(MsgID, MsgTxt) {
                if (MsgTxt == "CallFunc") {
                    switch (MsgID) {
                        case 3:
                            $('#modalLoggedIn').modal('show');
                            break;
                        default:
                            custAlertMsg('Unknown, Contact Admin', 'warning');
                    }
                }
                else {
                    switch (MsgID) {
                        case 1:
                            custAlertMsg('Your profile is locked, Contact Admin', 'warning');
                            break;
                        case 2:
                            custAlertMsg('Your Password has Expired So Reset your Password', 'warning');
                            break;
                        case 4:
                            custAlertMsg('You have no Roles in Application', 'warning');
                            break;
                        case 5:
                            custAlertMsg('Invalid Password', 'warning', 'clearPassword');
                            break;
                        case 6:
                            custAlertMsg('In-Active user login', 'warning', 'clearPassword');
                            break;
                        case 7:
                            custAlertMsg('Your profile is locked, Contact Admin', 'error', 'clearPassword');
                            break;
                        case 8:
                            custAlertMsg('Invalid User Credentials', 'error', 'clearPassword');
                            break;
                        case 9:
                            custAlertMsg('Invalid Password You have only ' + MsgTxt + ' more attempts', 'warning', 'clearPassword');
                            break;
                        default:
                            custAlertMsg('Your profile is locked, Contact Admin', 'warning', 'clearPassword');
                            break;
                    }
                }
            }
        </script>

        <!--Close the alert when press Esc-->
        <script>
            $(document).keydown(function (event) {
                if (event.keyCode == 27) {
                    var btnwarningOk = document.getElementById("<%=btnWarningOk.ClientID %>");
                    var btninfo = document.getElementById("<%=btnInfoOk.ClientID %>");
                    var btnErrorOk = document.getElementById("<%=btnErrorOk.ClientID %>");
                    btnwarningOk.click();
                    btninfo.click();
                    btnErrorOk.click();

                }
            });
        </script>

        <script>
            function NoSpaceAllowUserAns(event) {
                var que = document.getElementById('txtUserAnswer').value.trim();
                if (event.keyCode == 32 && que.length == 0) {
                    return false;
                }
                else {
                    return true;
                }
            }
            //no spaces for first and last position in securityanswer
            function NoSpaceAllowAns(event) {
                var que = document.getElementById('txtSecurityAnswer').value.trim();
                if (event.keyCode == 32 && que.length == 0) {
                    return false;
                }
                else {
                    return true;
                }
            }
        </script>

        <script type="text/javascript">
            //no spaces for first and last position in securityquestion
            function NoSpaceAllow(evt) {
                var que = document.getElementById('txtSecurityQuestion').value.trim();
                if (evt.keyCode == 32 && que.length == 0) {
                    return false;
                }
                else {
                    return true;
                }
            }
        </script>

        <script type="text/javascript">
            $("#clickSignUp").click(function () {
                $("#forgetPassword").hide();
                $("#signIn").hide();
                $("#signUp").show();
                $("#onNext").hide();
                $("#txtLoginID").focus();
                $("#hideButtonGroup").hide();
            });

            $("#signUpCancel").click(function () {
                defaultDivWindows();
            });
            $("#ForgetCancel").click(function () {
                defaultDivWindows();
            });
            $("#cancelForget").click(function () {
                defaultDivWindows();
            });

            $("#clickForget").click(function () {
                $("#forgetPassword").show();
                $("#signIn").hide();
                $("#signUp").hide();
                $("#twoNext").hide();
                $("#stepThree").hide();
                $("#oneNext").show();
                $("#txtLoginIDpwd").focus();
            });

            $("#twoCancel").click(function () {
                $("#forgetPassword").hide();
                $("#signIn").show();
                $("#signUp").hide();
                $("#hideButtonGroup").hide();
                $("#onNext").hide();
                $("#twoNext").hide();
            });

        </script>

        <script>
            //Forget step 1 page back
            function step1back() {
                $("#forgetPassword").show();
                $("#signIn").hide();
                $("#signUp").hide();
                $("#twoNext").hide();
                $("#stepThree").hide();
                $("#oneNext").show();
                $("#txtLoginIDpwd").focus();
            }
        </script>

        <script>
            function step1() {
                $("#forgetPassword").show();
                $("#signIn").hide();
                $("#signUp").hide();
                $("#hideButtonGroup").hide();
                $("#oneNext").hide();
                $("#twoNext").show();
                $("#stepThree").hide();
            }
            //on success of loginId and Email
        </script>

        <script>
            function ClearAll() {
                var txtNewPwd = document.getElementById("<%=txtNewPwd.ClientID%>");
                var txtConfirmPwd = document.getElementById("<%=txtConfirmPwd.ClientID%>");
                txtNewPwd = '';
                txtConfirmPwd = '';
            }
            function step2() {
                $("#forgetPassword").show();
                $("#signIn").hide();
                $("#signUp").hide();
                $("#oneNext").hide();
                $("#twoNext").hide();
                $("#stepThree").show();
            }
            //on success of securityquestion and answer
        </script>

        <script>
            function defaultDivWindows() {
                $("#forgetPassword").hide();
                $("#signIn").show();
                $("#signUp").hide();
            }
        // On Successfully password changed
        </script>

        <script type="text/javascript">
            //Forgot Password Step3
            function FP_Step3() {
                defaultDivWindows();
            }
        </script>

        <script>

            function ShowPleaseWaitdisable() {
                ShowPleaseWait('hide');
            }

            //Required fields for Login Page
            function RequiredValidation() {
                var LoginID = document.getElementById("<%= txtLoginUserName.ClientID%>").value;
                var Password = document.getElementById("<%= txtLoginPassword.ClientID%>").value;
                errors = [];

                if (LoginID == "") {
                    errors.push("Enter Login ID");
                    document.getElementById("<%= txtLoginUserName.ClientID%>").focus();
                }
                if (Password == "") {
                    errors.push("Enter Password");
                    document.getElementById("<%= txtLoginPassword.ClientID%>").focus();
                }
                if (errors.length > 0) {
                    custAlertMsg(errors.join("<br/>"), "error");
                    return false;
                }
                else {
                    ShowPleaseWait('show');
                    $("#<%= hdnUserID.ClientID%>").val(LoginID);
                    $("#<%= hdnPwd.ClientID%>").val(Password);
                    // setTimeout(function () { $("#btnLogin").attr("disabled", true); }, 2000);//Login
                    return true;
                }
            }
        </script>

        <script type="text/javascript">
            //Required fields for signup page
            function SignUpValidate() {
                var LoginID = document.getElementById("<%= txtLoginID.ClientID%>").value;
                var txtPassword = document.getElementById("<%=txtPassword.ClientID%>").value;
                var txtConfirmPassword = document.getElementById("<%=txtConfirmPassword.ClientID%>").value;
                var SecurityQuestion = document.getElementById("<%=txtSecurityQuestion.ClientID%>").value;
                var SecurityAnswer = document.getElementById("<%=txtSecurityAnswer.ClientID%>").value;
                var undefChars = /[\ _]/;

                errors = [];
                if (LoginID !== "") {
                    if (LoginID.length < 4) {
                        errors.push("Login ID must contain atleast 4 characters");
                    }
                    if (undefChars.test(LoginID)) {
                        errors.push("Login ID contains undefined symbols");
                    }
                }
                if (LoginID == "") {
                    errors.push("Enter Login ID");
                }
                if (txtPassword != "") {
                    if (txtPassword.length < 8) {
                        errors.push("Password must contain Minimum 8 characters");
                    }
                    if (txtPassword.search(/[a-z]/) < 0) {
                        errors.push("Password must contain atleast one Lower Case");
                    }
                    if (txtPassword.search(/[A-Z]/) < 0) {
                        errors.push("Password must contain atleast one Upper Case");
                    }
                    if (txtPassword.search(/[0-9]/) < 0) {
                        errors.push("Password must contain atleast one Number");
                    }
                    if (txtPassword.search(/[@#&*]/) < 0) {
                        errors.push("Password must contain atleast one @,#,&,* character");
                    }
                }
                if (txtPassword == "") {
                    errors.push("Enter Password");
                }

                if (txtConfirmPassword != "") {
                    if (txtPassword != txtConfirmPassword) {
                        errors.push("Password and Confirm Password should match");
                    }
                }
                if (txtConfirmPassword == "") {
                    errors.push("Enter Confirm Password");
                }
                if (SecurityQuestion != "") {
                    if (SecurityQuestion.length < 4) {
                        errors.push("Security Question must contain atleast 4 Characters");
                    }
                }
                if (SecurityQuestion == "") {
                    errors.push("Enter Security Question");
                }
                if (SecurityAnswer != "") {
                    if (SecurityAnswer.length < 2) {
                        errors.push("Security Answer must contain atleast 2 characters");
                    }
                }
                if (SecurityAnswer == "") {
                    errors.push("Enter Answer");
                }
                if (errors.length > 0) {
                    custAlertMsg(errors.join("<br/>"), "error");
                    return false;
                }
                else {
                    ShowPleaseWait('show');
                    // setTimeout(function () { $("#btnCreate").attr("disabled", true); }, 3000);//Registration Sign in
                    return true;
                }
            }
        </script>

        <script type="text/javascript">
            //validations for forget page
            function ForgetValidation() {
                var LoginID = document.getElementById("<%= txtLoginIDpwd.ClientID%>").value;
                var Email = document.getElementById("<%= txtEmailID.ClientID%>").value;
                var expr = /^([\w-\.]+)((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                errors = [];
                if (LoginID == "") {
                    errors.push("Enter Login ID");
                }
                if (Email == "") {
                    errors.push("Enter Email");
                }
                if (Email != "") {
                    if (!IsValidEmail(Email)) {
                        errors.push("Invalid Email");
                    }
                }
                if (errors.length > 0) {
                    custAlertMsg(errors.join("<br/>"), "error");
                    return false;
                }
                else {
                    return true;
                }
            }
        </script>

        <!--To Show Custom Alert Messages-->
        <script>
            function custAlertMsg(message, alertType, okEvent) {
                var okEvent = okEvent || false;

                if (!okEvent) {
                    okEvent = '';
                }
                if (alertType == 'error') {
                    document.getElementById("msgError").innerHTML = message;
                    //$("#ModalError").modal();
                    $('#ModalError').modal({ backdrop: 'static', keyboard: false });
                    $('#btnErrorOk').attr('onclick', okEvent);
                    $('#btnErrorOk').focus();
                    if (okEvent != '') {
                        document.getElementById('btnErrorOk').setAttribute('onclick', okEvent)
                    }
                }
                else if (alertType == 'success') {
                    document.getElementById("msgSuccess").innerHTML = message;
                    //$("#ModalSuccess").modal();
                    $('#ModalSuccess').modal({ backdrop: 'static', keyboard: false });
                    $('#btnSuccessOk').attr('onclick', okEvent);
                    $('#btnSuccessOk').focus();
                    if (okEvent != '') {
                        // document.getElementById('btnSuccessOk').addEventListener("click", okfunc);
                        document.getElementById('btnSuccessOk').setAttribute('onclick', okEvent)
                    }
                }
                else if (alertType == 'warning') {
                    document.getElementById("msgWarning").innerHTML = message;
                    //  $("#ModalWarning").modal();
                    $('#ModalWarning').modal({ backdrop: 'static', keyboard: false });
                    $('#btnWarningOk').attr('onclick', okEvent);
                    $('#btnWarningOk').focus();
                    if (okEvent != '') {
                        document.getElementById('btnWarningOk').setAttribute('onclick', okEvent)
                    }
                }
                else if (alertType == 'info') {
                    document.getElementById("msgInfo").innerHTML = message;
                    //  $("#ModalInfo").modal();
                    $('#ModalInfo').modal({ backdrop: 'static', keyboard: false });
                    $('#btnInfoOk').attr('onclick', okEvent);
                    $('#btnInfoOk').focus();
                    if (okEvent != '') {
                        // document.getElementById('btnInfoOk').setAttribute('onclick', okEvent)
                    }
                }
                else if (alertType == 'confirm') {
                    document.getElementById("msgConfirm").innerHTML = message;
                    //$("#ModalConfirm").modal();
                    $('#ModalConfirm').modal({ backdrop: 'static', keyboard: false });
                    $('#ConfirmbtnYes').attr('onclick', okEvent);
                    $('#ConfirmbtnYes').focus();
                }
            }
        </script>

        <script type="text/javascript">
            //validations for security question and answer page
            function SecurityValidation() {
                var Securitryquestion = document.getElementById("<%= txtSecurityQues.ClientID%>").value;
                var Securityanswer = document.getElementById("<%= txtUserAnswer.ClientID%>").value;

                errors = [];
                if (Securitryquestion == "") {
                    errors.push("Enter Securitry Question");
                }
                if (Securityanswer == "") {
                    errors.push("Enter Answer");
                }
                if (errors.length > 0) {
                    custAlertMsg(errors.join("<br/>"), "error");
                    return false;
                }
                else {
                    $("#fakePwd").val("fakePwd");
                    $("#fakePwd").focus();
                    return true;
                }
            }
        </script>

        <script type="text/javascript">
            //validations for confirm password page
            function confirmpasswordvalidation() {
                var password = document.getElementById("<%= txtNewPwd.ClientID%>").value;
                var confirmpassword = document.getElementById("<%= txtConfirmPwd.ClientID%>").value;
                errors = [];
                if (password != "") {
                    if (password.length < 8) {
                        errors.push("Password must contain Minimum 8 characters");
                    }
                    if (password.search(/[a-z]/) < 0) {
                        errors.push("Password must contain atleast one Lower Case");
                    }
                    if (password.search(/[A-Z]/) < 0) {
                        errors.push("Password must contain atleast one Upper Case");
                    }
                    if (password.search(/[0-9]/) < 0) {
                        errors.push("Your Password must contain atleast one Number");
                    }
                    if (password.search(/[@#&*]/) < 0) {
                        errors.push("Password must contain atleast one @,#,&,* character");
                    }
                }
                if (password == "") {
                    errors.push("Enter password");
                }
                if (confirmpassword != "") {
                    if (password != confirmpassword) {
                        errors.push("Password and Confirm Password should match");
                    }
                }
                if (confirmpassword == "") {
                    errors.push("Enter confirm Password");
                }
                if (errors.length > 0) {
                    custAlertMsg(errors.join("<br/>"), "error");
                    return false;
                }
                else {
                    ShowPleaseWait('show');
                    // setTimeout(function () { $("#btnCreatePassword").attr("disabled", true); }, 3000);//Forget PWD
                    return true;
                }
            }

            function ShowResetPWD(ShowLoginID) {
                $("#txtReset_LoginID").val(ShowLoginID);
                $("#Logindiv,#loginheadernm").hide();
                $("#pwdresetdiv,#resetpwdnm").show();
            }
            function defaultDivResetPwdWindows() {
                $("#txtReset_LoginID").val(document.getElementById("<%= hdnUserID.ClientID%>").value);
                $("#Logindiv,#loginheadernm").hide();
                $("#pwdresetdiv,#resetpwdnm").show();
            }
            function NavigateDashboard(Url) {
                if (Url == 1)//VMS Security
                {
                    window.open("/VMS/Dashboard/SecurityLandingPage.aspx", "_self");
                } else {
                    window.open("/Dashboards/Home.aspx", "_self");
                }
            }
        </script>
        <script>
            //no spaces for first and last position in securityanswer
            function NoSpaceAllowAns(event) {
                var que = document.getElementById('txtSecurityAnswer').value.trim();
                if (event.keyCode == 32 && que.length == 0) {
                    return false;
                }
                else {
                    return true;
                }
            }
        </script>
        <script type="text/javascript">
            //no spaces for first and last position in securityquestion
            function NoSpaceAllow(evt) {
                var que = document.getElementById('txtSecurityQuestion').value.trim();
                if (evt.keyCode == 32 && que.length == 0) {
                    return false;
                }
                else {
                    return true;
                }
            }
        </script>
        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function (evt, args) {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
        <script>
            $(document).ready(function () {
                $(".copy_right1").text("Copyright © <%=DateTime.Now.Year.ToString() %> Aizant Global Analytics Pvt. Ltd,");
                $(".copy_right2").text(" All Rights Reserved Version-2.0.0");
                var userValue = $("#txtLoginUserName").val();
                $(".userID").html(userValue);
                $(".HeaderuserID").html(userValue);
            });

            function clearPassword() {
                document.getElementById("<%= txtLoginPassword.ClientID%>").value = '';
            }
        </script>
        <script>
            // reset Password
            $(function () {
                $("#btnResetcancel").on("click", function () {
                    $("#Logindiv,#loginheadernm").show();

                    $("#pwdresetdiv,#resetpwdnm").hide();
                })
            })
            $(function () {
                $("#btnResetSubmit").on("click", function () {
                    var ResetLoginID = document.getElementById("<%= txtReset_LoginID.ClientID%>").value;
                    var NewRStpassword = document.getElementById("<%= txtReset_NewPassword.ClientID%>").value;
                    var confirmRStpassword = document.getElementById("<%= txtReset_ConfirmPassword.ClientID%>").value;
                    errors = [];

                    if (NewRStpassword == "") {
                        errors.push("Enter new password");
                    }
                    if (confirmRStpassword == "") {
                        errors.push("Enter confirm password");
                    }
                    if (NewRStpassword != "") {

                        if (NewRStpassword.length < 8) {
                            errors.push("New Password must contain Minimum 8 characters");
                        }
                        if (NewRStpassword.search(/[a-z]/) < 0) {
                            errors.push("New Password must contain atleast one Lower Case");
                        }
                        if (NewRStpassword.search(/[A-Z]/) < 0) {
                            errors.push("New Password must contain atleast one Upper Case");
                        }
                        if (NewRStpassword.search(/[0-9]/) < 0) {
                            errors.push("New Password must contain atleast one Number");
                        }
                        if (NewRStpassword.search(/[@#&*]/) < 0) {
                            errors.push("New Password must contain atleast one @,#,&,* character");
                        }
                    }
                    if (confirmRStpassword != "") {

                        if (confirmRStpassword.length < 8) {
                            errors.push("Confirm Password must contain Minimum 8 characters");
                        }
                        if (confirmRStpassword.search(/[a-z]/) < 0) {
                            errors.push("Confirm Password must contain atleast one Lower Case");
                        }
                        if (confirmRStpassword.search(/[A-Z]/) < 0) {
                            errors.push("Confirm Password must contain atleast one Upper Case");
                        }
                        if (confirmRStpassword.search(/[0-9]/) < 0) {
                            errors.push("Confirm Password must contain atleast one Number");
                        }
                        if (confirmRStpassword.search(/[@#&*]/) < 0) {
                            errors.push("Confirm Password must contain atleast one @,#,&,* character");
                        }
                    }
                    if (confirmRStpassword != "") {
                        if (NewRStpassword != confirmRStpassword) {
                            errors.push("New Password and Confirm Password should match");
                        }
                    }
                    if (errors.length > 0) {
                        custAlertMsg(errors.join("<br/>"), "error");
                        return false;
                    }
                    else {
                        ShowPleaseWait('show'); //Change Password
                        var BTN_RST = document.getElementById("<%= btnResetPWD.ClientID%>");
                        BTN_RST.click();
                        //setTimeout(function () { $("#btnResetSubmit").attr("disabled", true); },3000);
                        //return true;
                    }
                })
            });
        </script>
    </form>
</body>
</html>
