﻿using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using UMS_BO;
using UMS_BusinessLayer;

namespace AizantIT_PharmaApp
{
    public partial class UserLogin : System.Web.UI.Page
    {
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    Page.Header.DataBind();
                    txtLoginUserName.Focus();
                    if (Session["UserDetails"] != null)
                    {
                        Response.Redirect("~/Common/LoggedIn.html", false);
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("UL_01:" + strline + "  " + strMsg);
                // ErrorMsg("UL00" + strline + "  " + strMsg);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('" + strMsg + "','error');", true);
            }
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            UMS_BAL UMBAL = new UMS_BAL();
            try
            {
                if (txtLoginID.Value.Trim() == "" || txtPassword.Value.Trim() == "" || txtConfirmPassword.Value.Trim() == "" || txtSecurityQuestion.Value.Trim() == "" || txtSecurityAnswer.Value.Trim() == "")
                {                   
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('All Fields  Represented with * Are Mandatory','error');", true);
                }
                else if (txtSecurityQuestion.Value.Trim().Length <= 3 || txtSecurityAnswer.Value.Trim().Length <= 1)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('Security Question and Answer should be atleast 4 and 2 Characters respectively','error');", true);                   
                }
                else
                {
                    string source = txtPassword.Value.Trim();
                    string result_cap = string.Concat(source.Where(c => c >= 'A' && c <= 'Z'));
                    string resul_small = string.Concat(source.Where(c => c >= 'a' && c <= 'z'));
                    string resul1_numeric = string.Concat(source.Where(c => c >= '0' && c <= '9'));
                    string resul1_spl = string.Concat(source.Where(c => c == '!' || c == '@' || c == '#' || c == '$' || c == '&' || c == '*'));
                    if (result_cap == "" || resul_small == "" || resul1_numeric == "" || (source.Length >= 8) == false)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('Password must contain at-least 8 characters,one uppercase,one lowercase,one uppercase and one number','error');", true);
                        
                    }
                    else if (txtPassword.Value.Trim() != txtConfirmPassword.Value.Trim())
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('Password and Confirm password Should match','error');", true);
                       
                    }
                    else
                    {
                        SigUPObjects UOB = new SigUPObjects();
                        UOB.UserLoginID = txtLoginID.Value.Trim();
                        UOB.ConfirmPassword = txtConfirmPassword.Value.Trim();
                        UOB.SecurityQuestion = txtSecurityQuestion.Value.Trim();
                        UOB.SecurityAnswer = txtSecurityAnswer.Value.Trim();
                        int Result = UMBAL.AddSignUpBAL(UOB, out int VerifyUserID,out int StartDateStatus);
                        if (StartDateStatus == 1)//StartDate Greater than today date 
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('User can\\'t sign up or Login Before Employee start date.','error');", true);
                        }
                        if (VerifyUserID == 1)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('Login ID Does not Exist','error');", true);
                            upCreatebtnSignup.Update();
                        }
                        else
                        {
                            if (VerifyUserID == 2)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('User Login Registration already Created','error');", true);
                            }
                            else
                            {
                                if (VerifyUserID == 0)
                                {
                                    txtLoginID.Value = txtSecurityQuestion.Value = txtSecurityAnswer.Value = string.Empty;
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('User Login Registration Created Successfully ','success',defaultDivWindows());", true);
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('Registering login ID Failed, please contact Admin!','error');", true);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("UL_03:" + strline + "  " + strMsg);
               
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('" + strMsg + "','error');", true);
            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", "ShowPleaseWaitdisable();", true);
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdnUserID.Value.Trim() == "" || hdnPwd.Value.Trim() == "")
                {
                    lblLogin.Text = "All Fields Are Mandatory";
                }
                else
                {
                    if (Session["UserDetails"] == null)
                    {
                        UserLoginMain();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('Your need to Log out the existing logged in User<br/> else close the browser and retry.','warning');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("UL_04:" + strline + "  " + strMsg);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('" + strMsg + "','error');", true);
            }
        }

        private void UserLoginMain()
        {
            string ClienT_ipAddress = Request.ServerVariables["REMOTE_ADDR"],
                    Server_IpAdress = Request.ServerVariables["LOCAL_ADDR"];
            UMS_BAL UMBAL = new UMS_BAL();
            SigUPObjects UOB = new SigUPObjects();
            UOB.UserLoginID = hdnUserID.Value.Trim();
            UOB.ConfirmPassword = hdnPwd.Value.Trim();
            string alertPop = DateTime.Now.ToString("hhmmssfff");
            DataSet ds = UMBAL.CheckLoginBAL(UOB, out int RemainingCount, out int Status, out char LoggedInStatus, 
                out int isAdminResetPWD);
         
            if (RemainingCount == 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), alertPop, "ServerValidationOnLogin(1,'other');", true);
            }
            else if (RemainingCount == -1)
            {
                DataTable dtLoginDetails = ds.Tables[0];
                if (dtLoginDetails.Rows.Count > 0)
                {
                    if (dtLoginDetails.Rows[0]["Status"].ToString() == "U")
                    {
                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            if (Status == 1)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), alertPop, "ServerValidationOnLogin(2,'other');", true);
                            }
                            else
                            {
                                if (LoggedInStatus == 'N')
                                {
                                    int RoleID = Convert.ToInt32(ds.Tables[1].Rows[0]["RoleID"].ToString());
                                    ViewState["EmpID"] = ds.Tables[0].Rows[0]["EmpID"].ToString();
                                    ViewState["RoleID"] = RoleID;
                                    ViewState["dsEmp"] = ds;
                                    if (isAdminResetPWD == 1)
                                    {
                                        // Show reset password
                                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), alertPop, "ShowResetPWD('" + hdnUserID.Value.Trim() + "');", true);
                                    }
                                    else
                                    {
                                        Session["ShowPwdExpireMsg"] = "Yes";

                                        //adding empid,SessionTimeOut to cookies
                                        string UserEmpID = UMBAL.Encrypt(ds.Tables[0].Rows[0]["EmpID"].ToString());
                                        HttpCookie CookieUserIn = new HttpCookie("CookieUserIn", UserEmpID);
                                        Response.Cookies.Add(CookieUserIn);
                                        HttpCookie CookieExpire = new HttpCookie("CookieExpire", ds.Tables[0].Rows[0]["SessionTimeOut"].ToString());
                                        Response.Cookies.Add(CookieExpire);
                                        Session.Timeout = Convert.ToInt32(ds.Tables[0].Rows[0]["SessionTimeOut"]);

                                        DataTable dtUserSession = new DataTable("UserDetails");
                                        dtUserSession.Columns.Add("ActiveUserSession", typeof(string));

                                        DataRow drUserSession = dtUserSession.NewRow();
                                        string UserSessionID = Session.SessionID;
                                        drUserSession["ActiveUserSession"] = UserSessionID;
                                        dtUserSession.Rows.Add(drUserSession);
                                        ds.Tables.Add(dtUserSession);
                                      
                                        // Last Login and Session Updated                                     
                                        int _logPkid = 0;
                                        UMBAL.UserSessionUpdate(UserSessionID, UOB.UserLoginID, Server_IpAdress, ClienT_ipAddress,out  _logPkid);

                                        DataTable dtUserLog = new DataTable("UserLogId");
                                        dtUserLog.Columns.Add("LogPkID", typeof(string));
                                        DataRow drUserLog = dtUserLog.NewRow();
                                        drUserLog["LogPkID"] = _logPkid;
                                        dtUserLog.Rows.Add(drUserLog);
                                        ds.Tables.Add(dtUserLog);

                                        Session["UserDetails"] = ds;
                                        // For VMS
                                        Session["sesGMSRoleID"] = "0";                                        
                                        
                                            if (RoleID == 22)//Security RoleID
                                            {
                                                Session["sesGMSRoleID"] = "22";
                                                Response.Redirect("~/VMS/Dashboard/SecurityLandingPage.aspx", false);
                                            }
                                            else
                                            {
                                            Response.Redirect("~/Dashboards/Home.aspx", false);
                                            //Response.Redirect("~/Home/Home", false);
                                        }
                                        
                                    }
                                    //else
                                    //{
                                    //    //txtReset_OldPassword.Value = hdnPwd.Value.Trim();
                                      
                                    //    // Show reset password
                                    //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), alertPop, "ShowResetPWD('" + hdnUserID.Value.Trim() + "');", true);
                                    //}
                                }
                                else
                                {                                   
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), alertPop, "ServerValidationOnLogin(3,'CallFunc');", true);
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), alertPop, "ServerValidationOnLogin(4,'other');", true);
                        }
                    }
                    else if (dtLoginDetails.Rows[0]["Status"].ToString() == "WP")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), alertPop, "ServerValidationOnLogin(5,'other');", true);
                    }
                    else if (dtLoginDetails.Rows[0]["Status"].ToString() == "IA")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), alertPop, "ServerValidationOnLogin(6,'other');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), alertPop, "ServerValidationOnLogin(7,'other');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), alertPop, "ServerValidationOnLogin(8,'other');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), alertPop, "ServerValidationOnLogin(9,'"+RemainingCount+"');", true);
            }
        }


        string LoginID = "";
        string EmailID = "";

        int EmpID = 0;
        string SecurityAnswer = "";
        string NewPassword = "";

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UserLogin.aspx");
        }
        protected void btn_LoginEmail_Click(object sender, EventArgs e)
        {
            try
            {
                UMS_BAL objUMS_BAL = new UMS_BAL();
                LoginID = txtLoginIDpwd.Value.Trim();
                EmailID = txtEmailID.Value.Trim();
                DataTable dt = objUMS_BAL.ForgotPassword(LoginID, EmailID, "Step1", EmpID, SecurityAnswer, NewPassword);
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    HFEmpID.Value = dr["EmpID"].ToString();
                    txtSecurityQues.Value = dr["Question"].ToString();
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "step1();", true);
                    
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('Invalid  LoginID Or Email','error');", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("UL_05:" + strline + "  " + strMsg);               
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('" + strMsg + "','error');", true);
            }
        }
        protected void btn_SecurityQues_Click(object sender, EventArgs e)
        {
            try
            {
                UMS_BAL objUMS_BAL = new UMS_BAL();
                EmpID = Convert.ToInt32(HFEmpID.Value);
                SecurityAnswer = txtUserAnswer.Value.Trim();
                DataTable dt2 = objUMS_BAL.ForgotPassword(LoginID, EmailID, "Step2", EmpID, SecurityAnswer, NewPassword);
                if (dt2.Rows.Count > 0)
                {
                    DataRow dr = dt2.Rows[0];
                    int Result = Convert.ToInt32(dr["Status"].ToString());
                    if (Result == 1)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "step2();", true);
                    }
                    else if (Result == 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('Not a Valid Answer','error');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("UL_06:" + strline + "  " + strMsg);               
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('" + strMsg + "','error');", true);
            }

        }
        protected void btn_Cancel_Click(object sender, EventArgs e)
        {
            signIn.Visible = false;
            forgetPassword.Style.Add("display", "None");
            forgetPassword.Visible = false;
        }
        protected void btnCreatePassword_Click(object sender, EventArgs e)
        {
            try
            {
                UMS_BAL objUMS_BAL = new UMS_BAL();
                EmpID = Convert.ToInt32(HFEmpID.Value);
                NewPassword = txtNewPwd.Value.Trim();
                DataTable dt3 = objUMS_BAL.ForgotPassword(LoginID, EmailID, "Step3", EmpID, SecurityAnswer, NewPassword);
                if (dt3.Rows.Count > 0)
                {
                    DataRow dr = dt3.Rows[0];
                    int Result = Convert.ToInt32(dr["Status"].ToString());
                    
                    if (Result ==5)//In-Active Employee
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", @"custAlertMsg('Not Allowed to Reset you\'re Password, Since you\'re In-Active Employee.','error');", true);
                    }
                    else if (Result == 4)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('New Password Should not be a Previous Passwords','warning',step2());", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('Password Changed Successfully','success',defaultDivWindows());", true);
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("UL_07:" + strline + "  " + strMsg);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('" + strMsg + "','error');", true);
            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", "ShowPleaseWaitdisable();", true);
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }

        protected void btnReLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdnUserID.Value.Trim() == "" || hdnPwd.Value.Trim() == "")
                {
                    lblLogin.Text = "All Fields Are Mandatory";
                }
                else
                {
                    UMS_BAL objUMS_BAL = new UMS_BAL();
                    objUMS_BAL.RemoveActiveUserSession(hdnUserID.Value.Trim(), hdnPwd.Value.Trim());
                    UserLoginMain();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("UL_04:" + strline + "  " + strMsg);                
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('" + strMsg + "','error');", true);
            }
        }

        protected void btnResetPWD_Click(object sender, EventArgs e)
        {
            try
            {
                string ClienT_ipAddress = Request.ServerVariables["REMOTE_ADDR"],
                    Server_IpAdress = Request.ServerVariables["LOCAL_ADDR"];
                UMS_BAL objUMS_BAL = new UMS_BAL();

                if (ViewState["RoleID"]!=null)
                {
                    DataTable dt3 = objUMS_BAL.MyprofileChangePassword(Convert.ToInt32(ViewState["EmpID"].ToString()), txtReset_NewPassword.Value.Trim(), "Password Reset by Logging in user himself, After Administrator changed Password.");
                    if (dt3.Rows.Count > 0)
                    {
                        if (Convert.ToInt32(dt3.Rows[0]["Status"].ToString()) == 4)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('New Password Should not be Previous 3 Passwords','error',defaultDivResetPwdWindows());", true);
                        }
                        else if (Convert.ToInt32(dt3.Rows[0]["Status"].ToString()) == 2 || Convert.ToInt32(dt3.Rows[0]["Status"].ToString()) == 3)
                        {
                            Session["ShowPwdExpireMsg"] = "Yes";
                            UMS_BAL UMBAL = new UMS_BAL();
                            DataSet ds =  (ViewState["dsEmp"]) as DataSet;
                            //adding empid,SessionTimeOut to cookies
                            string UserEmpID = UMBAL.Encrypt(ds.Tables[0].Rows[0]["EmpID"].ToString());
                            HttpCookie CookieUserIn = new HttpCookie("CookieUserIn", UserEmpID);
                            Response.Cookies.Add(CookieUserIn);
                            HttpCookie CookieExpire = new HttpCookie("CookieExpire", ds.Tables[0].Rows[0]["SessionTimeOut"].ToString());
                            Response.Cookies.Add(CookieExpire);
                            Session.Timeout = Convert.ToInt32(ds.Tables[0].Rows[0]["SessionTimeOut"]);

                            DataTable dtUserSession = new DataTable("UserDetails");
                            dtUserSession.Columns.Add("ActiveUserSession", typeof(string));

                            DataRow drUserSession = dtUserSession.NewRow();
                            string UserSessionID = Session.SessionID;
                            drUserSession["ActiveUserSession"] = UserSessionID;
                            dtUserSession.Rows.Add(drUserSession);
                            ds.Tables.Add(dtUserSession);

                          
                            // Last Login and Session Updated
                          
                           
                            int _logPkid = 0;
                            UMBAL.UserSessionUpdate(UserSessionID, hdnUserID.Value.Trim(), Server_IpAdress, ClienT_ipAddress, out _logPkid);

                            DataTable dtUserLog = new DataTable("UserLogId");
                            dtUserLog.Columns.Add("LogPkID", typeof(string));
                            DataRow drUserLog = dtUserLog.NewRow();
                            drUserLog["LogPkID"] = _logPkid;
                            dtUserLog.Rows.Add(drUserLog);
                            ds.Tables.Add(dtUserLog);

                            Session["UserDetails"] = ds;
                            // For VMS
                            Session["sesGMSRoleID"] = "0";
                           


                            if (ViewState["RoleID"].ToString() == "22")//Security RoleID
                            {
                                Session["sesGMSRoleID"] = "22";
                                HelpClass.custAlertMsgWithFunc(this, this.GetType(), " Password Reset Successfully", "success", "NavigateDashboard(1)");
                            }
                            else
                            {
                                HelpClass.custAlertMsgWithFunc(this, this.GetType(), " Password Reset Successfully", "success", "NavigateDashboard(2)");
                            }
                        }

                    } 
                      
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("UL_05:" + strline + "  " + strMsg);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('" + strMsg + "','error');", true);
            }
        }
    }
}