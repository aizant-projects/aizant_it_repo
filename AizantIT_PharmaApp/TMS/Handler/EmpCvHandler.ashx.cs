﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using TMS_BusinessLayer;

namespace AizantIT_PharmaApp.TMS.Handler
{
    /// <summary>
    /// Summary description for EmpCvHandler
    /// </summary>
    public class EmpCvHandler : IHttpHandler
    {
        TMS_BAL objTMS_Bal;
        public void ProcessRequest(HttpContext context)
        {
            objTMS_Bal = new TMS_BAL();
            int EmpID =Convert.ToInt32(context.Request.QueryString["EmpID"]);

            DataTable dtTrainingFile = objTMS_Bal.getEmployeeCvOrAc(EmpID,1);
            if (dtTrainingFile.Rows.Count > 0)
            {
                DataRow dr = dtTrainingFile.Rows[0];
                //Response.Clear();
                if (dr["DocumentContent"] != null)
                {
                    context.Response.Buffer = true;
                    context.Response.Charset = "";
                    //if (context.Request.QueryString["download"] == "1")
                    //{
                    //    context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + dr["FileName"].ToString());
                    //}
                    context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    //if (dr["FileType"].ToString() == "pdf")
                    //{

                    //}
                    context.Response.ContentType = "application/pdf";
                    context.Response.BinaryWrite((byte[])dr["DocumentContent"]);
                    context.Response.Flush();
                    context.Response.End();
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}