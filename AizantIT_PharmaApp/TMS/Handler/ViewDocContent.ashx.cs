﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using TMS_BusinessLayer;

namespace AizantIT_PharmaApp.TMS.Handler
{
    /// <summary>
    /// Summary description for ViewDocContent
    /// </summary>
    public class ViewDocContent : IHttpHandler
    {
        TMS_BAL objTMS_Bal;
        public void ProcessRequest(HttpContext context)
        {
            objTMS_Bal = new TMS_BAL();
            string DocID = context.Request.QueryString["DocID"];

            DataTable dtDocDetails = objTMS_Bal.ActiveDocumentContent(DocID);
            if (dtDocDetails.Rows.Count > 0)
            {
                DataRow dr = dtDocDetails.Rows[0];
                context.Response.Buffer = true;
                context.Response.Charset = "";
                context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                if (dr["FileType"].ToString() == "pdf")
                {
                    context.Response.ContentType = "application/pdf";
                }
                context.Response.BinaryWrite((byte[])dr["Content"]);
                context.Response.Flush();
                context.Response.End();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}