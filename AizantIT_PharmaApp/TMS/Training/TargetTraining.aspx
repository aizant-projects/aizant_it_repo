﻿<%@ Page Title="Target Training" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="TargetTraining.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Training.TargetTraining" %>

<%@ Register Src="~/UserControls/TMS_UserControls/Exam/ucExamination.ascx" TagPrefix="uc1" TagName="ucExamination" %>
<%@ Register Src="~/UserControls/TMS_UserControls/ucFeedback.ascx" TagPrefix="uc1" TagName="ucFeedback" %>
<%@ Register Src="~/UserControls/TMS_UserControls/ucViewDocFAQs.ascx" TagPrefix="uc1" TagName="ucViewDocFAQs" %>
<%@ Register Src="~/UserControls/TMS_UserControls/ucViewQuestions.ascx" TagPrefix="uc1" TagName="ucViewQuestions" %>
<%@ Register Src="~/UserControls/ElectronicSign.ascx" TagPrefix="uc1" TagName="ElectronicSign" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <!--ML Enable and Disable script starts -->
   <script src='<%=ResolveUrl("~/AppScripts/ML_FeaturesChecking.js")%>'></script>
        <script>
            GetMLFeatureChecking(3);//Passing Paramenter Module ID
        </script>
    <!--ML Enable and Disable script ends-->
    <style>
        #dvViewDociframe iframe {
            height: 500px !important;
        }
    </style>
    <asp:UpdatePanel ID="UpHdnFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnTTS_ID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnTrainingSessionID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnDocID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnTargetExamID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnDocVersionID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnIsTargetSession" runat="server" Value="Yes" />
            <asp:HiddenField ID="hdnIsFeedbacktoAsk" runat="server" Value="N" />
            <asp:HiddenField ID="hdnIsOnlineOrClassroom" runat="server" Value="ClassRoom" />
            <asp:HiddenField ID="hdnHours" runat="server" Value="0" />
            <asp:HiddenField ID="hdnMinutes" runat="server" Value="0" />
            <asp:HiddenField ID="hdnSeconds" runat="server" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAuthorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div class="col-12 padding-none" id="mainContainer" runat="server">
                <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 ">
                    <asp:Label ID="lblTitle" runat="server" Font-Bold="true" Text="Target Training"></asp:Label>
                </div>
                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12">
                    <ul class="nav nav-tabs tab_grid top">
                        <li class="nav-item  "><a data-toggle="tab" id="liClassroomTab" class="nav-link active ClassroomTab" href="#ClassroomTraining">Classroom Training</a></li>
                        <li class="nav-item "><a data-toggle="tab" id="liOnlineTab" class="nav-link alOnlineTab OnlineTab" href="#OnlineTraining">Online Training</a></li>
                    </ul>
                    <div class="tab-content ">
                        <div id="ClassroomTraining" class="tab-pane fade show active ClassroomTabBody">
                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_panel_hod padding-none" style="padding: 5px; border-top: none;">
                                <table id="tblTTS_TraineeSessionList" class="datatable_cust tblTTS_TraineeSessionListClass display top breakword" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th>TTS_ID</th>
                                            <th>TTS_ClassRoomTrainingSessionID</th>
                                            <th>TargetExamID</th>
                                            <th>Document_ID</th>
                                            <th>DocumentVersion_ID</th>
                                            <th>Document</th>
                                            <th>Dept Code</th>
                                            <th>Training Date</th>
                                            <th>From</th>
                                            <th>To</th>
                                            <th>Trainer</th>
                                            <th>Exam Type</th>
                                            <th>Remarks</th>
                                            <th>Type of Training</th>
                                            <th>Status</th>
                                            <th>EmpFeedbackID</th>
                                            <th>Trainer Note</th>
                                            <th>Read</th>
                                            <th>Feedback</th>
                                        </tr>
                                    </thead>
                                </table>
                                <div class="float-left col-lg-12">
                                    <span class="attendence_LogComplete_tms" style="padding: 2px 12px; margin-left: 4px"></span><b class="legends_grid_tms">Take Exam</b><span class="feedback_tms" style="margin-left: 5px; padding: 2px 12px !important"></span><b class="legends_grid_tms">Submit Feedback</b></span>
                                </div>
                            </div>
                        </div>
                        <div id="OnlineTraining" class="tab-pane fade OnlineTabBody">
                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_panel_hod padding-none" style="padding: 5px; border-top: none;">
                                <table id="tblTTS_TraineeOnlineSessionList" class="datatable_cust tblTTS_TraineeOnlineSessionListClass display breakword" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th>TTS_ID</th>
                                            <th>TTS_OnlineTrainingSessionID</th>
                                            <th>TargetExamID</th>
                                            <th>Document_ID</th>
                                            <th>Document_VersionID</th>
                                            <th>Document</th>
                                            <th>Dept Code</th>
                                            <th>Due Date</th>
                                            <th>Trainer</th>
                                            <th>Remarks</th>
                                            <th>Self Complete</th>
                                            <th>Status</th>
                                            <th>StatusID</th>
                                            <th>Trainer Note</th>
                                            <th>Read</th>
                                            <%-- <th>Take Exam</th>--%>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--modal popup to load Document Content--%>
    <div class="modal fade department " id="ReadDocModal" role="dialog">
        <div class="modal-dialog modal-lg" style="min-width: 90%;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header tmsModalHeader">
                    <h4 class="modal-title Panel_Title" style="color: aliceblue">Document</h4>
                    <button type="button" class="close " onclick="CloseDocumentModal();" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body tmsModalBody" style="max-height: 600px; max-height: 660px; overflow-y: auto">
                    <div class=" tmsModalContent">
                        <div class="float-left text-right col-lg-12 col-md-12 col-12 col-sm-12 padding-none">
                            <button type="button" id="btnAskQuestion" class="btn btn-signup_popup " data-toggle="modal" onclick="ShowAskQuestionDiv();">Ask a Question</button>
                            <button type="button" class="btn btn-signup_popup" onclick="openViewQuestionPopUp();">View Questions</button>
                            <button type="button" class="btn btn-revert_popup " data-toggle="modal" onclick="openFaqPopUp();">FAQs</button>
                            <div class="float-left" style="font-size: 21px; padding-top: 2px; padding-right: 20px; text-align: left"><span class="glyphicon glyphicon-time"></span>&nbsp;<span id="Timer"></span></div>
                            <!--Start TMS_ML (1/4) HTML Code-->
                            <button type="button" id="btnRelatedObservations" style="display: none" class="btn-signup_popup" data-toggle="modal">Related Observation<span class="relatedobs_count">0</span></button>
                            <div class=" col-lg-6 col-sm-6 col-6 col-md-6 related_obs_panel padding-none text-left float-left" id="draggablelist">
                                <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 ">
                                    <span class="col-3 padding-none" style="font-weight: bold;">Related Observations</span> <a href="#" id="relatedobsclose" class="close">&times;</a>
                                </div>
                                <div id="divRelatedDragList">
                                    <ul class="col-12 top float-left related_draglist">
                                        Loading...
                                    </ul>
                                </div>
                                <div class="footr_ml float-left col-lg-12  col-12 col-md-12 col-sm-12 ">
                                    <span class="internal_legend_ml"></span><span class="">Internal</span><span class="external_legend_ml"></span><span class="">External</span>
                                </div>
                            </div>
                            <!--End TMS_ML (1/4) HTML Code-->
                            <div id="divCreateQuestion" class="float-left col-lg-12" style="display: none">
                                <div class="form-group float-left col-12 float-left padding-none">
                                    <label for="inputEmail3" class=" padding-none float-left control-label custom_label_answer">Question</label>
                                    <div class=" padding-none float-left col-12 ">
                                        <textarea class="form-control" rows="5" runat="server" id="txtQuestion"></textarea>
                                    </div>
                                </div>
                                <div class="float-left col-lg-12 padding-none">
                                    <div class="float-right ">
                                        <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="upCreateQuestion">
                                            <ContentTemplate>
                                                <asp:Button ID="btnCreateQuestion" class="btn btn-signup_popup" OnClientClick="return CreateQuestionValidation();" runat="server" Text="Create" OnClick="btnCreateQuestion_Click" />
                                                <button type="button" class="btn btn-cancel_popup" onclick="ResetAskQuestion();">Cancel</button>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                            <div id="dvViewDociframe" class="traning_dept_select col-12 float-left">
                                <noscript>
                                    Please Enable Javascript.
                                </noscript>
                            </div>
                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12" style="text-align: center;">
                                <div id="divTakeExamBtn">
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <asp:Button ID="btnTakeExam" runat="server" CssClass="btn btn-signup_popup" Text="" OnClick="btnTakeExam_Click" OnClientClick="CloseDocumentModal();" title="" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--<%--modal popup for Remarks View--%>
    <div class="modal fade department" id="Remarks" role="dialog">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header tmsModalHeader">
                    <h4 class="modal-title Panel_Title" style="color: aliceblue">Trainer Notes</h4>
                    <button type="button" class="close " data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body tmsModalBody">
                    <div class="col-12 float-left padding-none tmsModalContent">
                        <div>
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="upRemarks">
                                <ContentTemplate>
                                    <div id="divRemarks" runat="server">
                                        <textarea class="form-control" id="txtRemarks" style="height: 100px" cols="12" runat="server" aria-multiline="true" disabled="disabled"></textarea>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <asp:Button ID="btnViewDocument" OnClick="btnViewDocument_Click" runat="server" Text="Button" />
                <%--<asp:Button ID="btnTakeExam" OnClick="btnTakeExam_Click" runat="server" Text="Button" />--%>
                <asp:Button ID="btnRemarks" OnClick="btnRemarks_Click" runat="server" Text="Note" />
                <asp:Button ID="btnAskFeedBack" runat="server" Text="Button" OnClick="btnAskFeedBack_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <uc1:ucViewQuestions runat="server" ID="ucViewQuestions" />
    <uc1:ucViewDocFAQs runat="server" ID="ucViewDocFAQs" />
    <uc1:ucExamination runat="server" ID="ucExamination" />
    <uc1:ucFeedback runat="server" ID="ucFeedback" />
    <uc1:ElectronicSign runat="server" ID="ElectronicSign" />

    <script>
        function openViewQuestionPopUp() {
            var TraineeID = $('#<%=hdnEmpID.ClientID%>').val();
            var TTS_ID = $('#<%=hdnTTS_ID.ClientID%>').val();
            ViewQuestionsByTrainee('2', TTS_ID, '0', TraineeID); //2 represents TTS basetype
        }
    </script>

    <script>
        function AskFeedBackOnTraining(AskFeedback) {
            if ($(<%=hdnIsOnlineOrClassroom.ClientID%>).val() == 'ClassRoom') {
                $('#<%=hdnIsFeedbacktoAsk.ClientID%>').val(AskFeedback);
                $("#<%=btnAskFeedBack.ClientID%>").click();
            }
            else {
                $('#<%=hdnIsFeedbacktoAsk.ClientID%>').val('N');
            }
        }
        function openFaqPopUp() {
            var DocID = $('#<%=hdnDocID.ClientID%>').val();
            OpenDocFAQPopup(DocID);
        }
        function OpenDocViewModal() {
            $('#ReadDocModal').modal({ backdrop: 'static', keyboard: false });
            OpenDocumentModal();
        }
        function OpenRemarks() {
            $('#Remarks').modal({ backdrop: 'static', keyboard: false });
        }
    </script>

    <!--TTS_TraineeSessionList jQuery DataTable For Classroom Training-->
    <script>
        var oTableTTS_TraineeSessionList;
        loadClassroomSessionDT();
        function loadClassroomSessionDT() {
            if (oTableTTS_TraineeSessionList != null) {
                oTableTTS_TraineeSessionList.destroy();
            }
            if (getUrlVars()["Tab"] != "Online") {
                oTableTTS_TraineeSessionList = $('#tblTTS_TraineeSessionList').DataTable({
                    columns: [
                        { 'data': 'TTS_ID' },
                        { 'data': 'TTS_ClassRoomTrainingSessionID' },
                        { 'data': 'TargetExamID' },
                        { 'data': 'DocumentID' },
                        { 'data': 'DocumentVersionID' },
                        { 'data': 'Document' },
                        { 'data': 'DeptCode' },
                        { 'data': 'TrainingDate' },
                        { 'data': 'StartTime' },
                        { 'data': 'EndTime' },
                        { 'data': 'Trainer' },
                        { 'data': 'Examtype' },
                        { 'data': 'Remarks' },
                        { 'data': 'TypeOfTraining' },
                        { 'data': 'StatusName' },
                        { 'data': 'EmpFeedbackID' },
                        {
                            "mData": null,
                            "bSortable": false,
                            "mRender": function (o) {
                                if (o.Remarks != "N/A") {
                                    return '<a class="attendence_LogComplete_tms" title="Remarks" href="#" onclick="ViewSessionRemarks(\'' + o.Remarks + '\');"></a>';
                                }
                                else {
                                    return '';
                                }
                            }
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "mRender": function (o) {
                                if (o.Examtype == "Written" && o.StatusName == "At Trainer Evaluation") {
                                    return '';
                                }
                                else {
                                    var eType = o.Examtype == "Written" ? "T" : "F";
                                    return '<a class="read_tms" title="Read"  href="#" onclick="ViewDocument(' + o.TTS_ID + ','
                                        + o.DocumentID + ',' + o.DocumentVersionID + ',' + o.TargetExamID + ','
                                        + o.TTS_ClassRoomTrainingSessionID + ',\'' + 'ClassRoom' + '\',\'' + eType + '\',\'' + 'false' + '\');"></a>';
                                }
                            }
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "mRender": function (o) {
                                if (o.Examtype == "Oral" || o.Examtype == "Practical") {
                                    if (o.StatusName == "At Trainer Evaluation" && o.EmpFeedbackID == 0) {
                                        return '<a class="feedback_tms" title="View FF" href="#" onclick="CreateTargetFeedback(' + o.TTS_ID + ');"></a>';
                                    }
                                    else {
                                        return '';
                                    }
                                }
                                else {
                                    return '';
                                }
                            }
                        }
                    ],
                    "order": [10, "desc"],
                    "scrollY": "360px",
                    "aoColumnDefs": [{ "targets": [0, 1, 2, 3, 4, 12, 15], "visible": false }, { className: "textAlignLeft", "targets": [5, 11, 13, 14] }],
                    "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TTS_ListService.asmx/TTS_Trainee_SessionList")%>",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData.push({ "name": "TraineeID", "value": <%=hdnEmpID.Value%> }, { "name": "DeptID", "value": getUrlVars()["DeptID"] },
                            { "name": "FilterType", "value": getUrlVars()["FilterType"] }, { "name": "TargetType", "value": getUrlVars()["TargetType"] },
                            { "name": "Year", "value": getUrlVars()["Year"] }, { "name": "Status", "value": getUrlVars()["Status"] });
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success": function (msg) {
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                UserSessionCheck();
                                $("#tblTTS_TraineeSessionList").show();
                                $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                                $(".tblTTS_TraineeSessionListClass").css({ "width": "100%" });
                            },
                            error: function (xhr, textStatus, error) {
                                if (typeof console == "object") {
                                    console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                                }
                            }
                        });
                    }
                });
            }
            else {
                oTableTTS_TraineeSessionList = $('#tblTTS_TraineeSessionList').DataTable({
                    "bServerSide": false,
                    "columnDefs": [{ "targets": [0, 1, 2, 3, 4, 12, 15], "visible": false }]
                }
                );
            }
        }

        function ReloadTheClassRoomDataTable() {
            if (oTableTTS_TraineeSessionList != null) {
                loadClassroomSessionDT();
            }
        }
    </script>

    <!--TTS_TraineeSessionList jQuery DataTable For Online Training-->
    <script>
        var oTableOnlineSessionList;
        function loadOnlineSessionDT() {
            if (oTableOnlineSessionList != null) {
                oTableOnlineSessionList.destroy();
            }
            oTableOnlineSessionList = $('#tblTTS_TraineeOnlineSessionList').DataTable({
                columns: [
                    { 'data': 'TTS_ID' },
                    { 'data': 'TTS_OnlineTrainingSessionID' },
                    { 'data': 'TargetExamID' },
                    { 'data': 'DocumentID' },
                    { 'data': 'DocumentVersionID' },
                    { 'data': 'Document' },
                    { 'data': 'DeptCode' },
                    { 'data': 'DueDate' },
                    { 'data': 'Trainer' },
                    { 'data': 'Remarks' },
                    { 'data': 'SelfComplete' },
                    { 'data': 'StatusName' },
                    { 'data': 'Status' },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            if (o.Remarks != "N/A") {
                                return '<a class="attendence_LogComplete_tms"  href="#" onclick="ViewSessionRemarks(\'' + o.Remarks + '\');"></a>';
                            }
                            else {
                                return '';
                            }
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            if (o.Status == "0" || o.Status == "1") {
                                return '<a class="read_tms"  href="#" title="Read" onclick="ViewDocument(' + o.TTS_ID + ',' + o.DocumentID + ',' + o.DocumentVersionID + ',' + o.TargetExamID + ',' + o.TTS_ClassRoomTrainingSessionID + ',\'' + 'Online' + '\',\'' + 'T' + '\',' + o.SelfComplete + ');"></a>';
                            }
                            else {
                                return 'Exam Qualified';
                            }
                        }
                    }
                ],
                "order": [[10, "desc"]],
                "scrollY": "360px",
                "aoColumnDefs": [{ "targets": [0, 1, 2, 3, 4, 9, 10, 12], "visible": false }, { className: "textAlignLeft", "targets": [5, 8, 11] }],
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TTS_ListService.asmx/TTS_Trainee_OnlineSessionList")%>",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "TraineeID", "value": <%=hdnEmpID.Value%> }, { "name": "DeptID", "value": getUrlVars()["DeptID"] },
                        { "name": "FilterType", "value": getUrlVars()["FilterType"] }, { "name": "TargetType", "value": getUrlVars()["TargetType"] },
                        { "name": "Year", "value": getUrlVars()["Year"] }, { "name": "Status", "value": getUrlVars()["Status"] });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            UserSessionCheck();
                            $("#tblTTS_TraineeOnlineSessionList").show();
                            $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                            $(".tblTTS_TraineeOnlineSessionListClass").css({ "width": "100%" });
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        }

        function ReloadTheOnlineDataTable() {
            loadOnlineSessionDT();
        }

    </script>

    <!--List Actions-->
    <script>
        var self_complete = "false";
        function ViewDocument(TTS_ID, DocumentID, DocumentVersionID, TargetExamID, TTS_TrainingSessionID, TypeOfTraining, ShowTakeExam, self_complete) {
            //Start TMS_ML (2/4) Code
            makeRelatedObservationsForDocumentToDefault();
            //End TMS_ML (2/4) Code
            $.ajax({
                type: "POST",
                url: "<%=ResolveUrl("~/TMS/WebServices/TTS_ListService.asmx/CheckTraineeActiveDocRead")%>",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ "DocID": DocumentID }),
                success: function (result) {
                    if (result.d != "0") {
                        custAlertMsg("You can read only one document at a time, <br/> if not kindly re-login.", "warning");
                    }
                    else {
                        $("#<%=hdnTTS_ID.ClientID%>").val(TTS_ID);
                        $("#<%=hdnDocID.ClientID%>").val(DocumentID);
                        $("#<%=hdnDocVersionID.ClientID%>").val(DocumentVersionID);
                        $("#<%=hdnTargetExamID.ClientID%>").val(TargetExamID);
                        $("#<%=hdnTrainingSessionID.ClientID%>").val(TTS_TrainingSessionID);
                        $("#<%=hdnIsOnlineOrClassroom.ClientID%>").val(TypeOfTraining);
                        $("#<%=btnViewDocument.ClientID%>").click();
                        UpdateReadDocID(DocumentID);
                        document.getElementById("Timer").innerHTML = "";
                        PdfViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>', 0, $('#<%=hdnDocVersionID.ClientID%>').val(), 0, 0, "#dvViewDociframe", "StartDocReadDuration");
                        if (ShowTakeExam == "F") {
                            $("#divTakeExamBtn").hide();
                        }
                        else {
                            $("#divTakeExamBtn").show();
                        }

                        function btnTakeExamChangeText() {
                            if (self_complete == true) {
                                $("#<%=btnTakeExam.ClientID%>").attr("title", "You would be able to submit after 10 minutes");
                                $("#<%=btnTakeExam.ClientID%>").val("Submit");
                            }
                            else {
                                $("#<%=btnTakeExam.ClientID%>").val("Take Exam");
                            }
                        }
                        $(function () {
                            btnTakeExamChangeText();
                        });
                        var prm = Sys.WebForms.PageRequestManager.getInstance();
                        prm.add_endRequest(function () {
                            btnTakeExamChangeText();
                        });
                    }
                },
                failure: function () {
                    custAlertMsg("Internal Server Error Occured.", "error");
                }
            });
        }

        function disableTakeExam() {
            $('#<%=btnTakeExam.ClientID %>').prop('disabled', true);
            $('#<%=btnTakeExam.ClientID %>').css('cursor', 'not-allowed');
            $('#<%=btnTakeExam.ClientID %>').attr("title", "You would be able to take an exam after 10 minutes");
            if (self_complete == true) {
                $("#<%=btnTakeExam.ClientID%>").attr("title", "You would be able to submit after 10 minutes");
            }
        }
        function enableTakeExam() {
            $('#<%=btnTakeExam.ClientID %>').prop('disabled', false);
            $('#<%=btnTakeExam.ClientID %>').css('cursor', 'pointer');
            $('#<%=btnTakeExam.ClientID %>').attr("title", "Click here to take exam");
            if (self_complete == true) {
                $("#<%=btnTakeExam.ClientID%>").attr("title", "Click here to Submit");
            }
        }

        function CloseDocumentModal() {
            UpdateReadDocID(0);
            ResetAskQuestion();
            CloseDocModal();
            loadOnlineSessionDT();
        }

        function UpdateReadDocID(DocumentID) {
            $.ajax({
                type: "POST",
                url: "<%=ResolveUrl("~/TMS/WebServices/TTS_ListService.asmx/UpdateTraineeDocRead")%>",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ "TraineeReadingDocID": DocumentID }),
                dataType: "json",
                success: function (result) {
                }
            });
        }

        function ViewRemarks(TTS_TrainingSessionID) {
            $("#<%=hdnTrainingSessionID.ClientID%>").val(TTS_TrainingSessionID);
            $("#<%=btnRemarks.ClientID%>").click();
        }
        function ViewSessionRemarks(Remarks) {
            $("#<%=txtRemarks.ClientID%>").val(Remarks);
            OpenRemarks();
        }
        function CreateTargetFeedback(TTS_ID) {
            $('#<%=hdnTTS_ID.ClientID%>').val(TTS_ID);
            $("#<%=btnAskFeedBack.ClientID%>").click();
            ClearFeedbackComments();
        }
    </script>

    <!--To Reload the Jquery based on the Active Tab-->
    <script>
        function ReloadSessionDatatables() {
            if ($('#liClassroomTab').hasClass('active')) {
                if (oTableTTS_TraineeSessionList != null) {
                    oTableTTS_TraineeSessionList.ajax.reload();
                }
            }
            if ($('#liOnlineTab').hasClass('active')) {
                if (oTableOnlineSessionList != null) {
                    oTableOnlineSessionList.ajax.reload();
                }
            }
        }
    </script>


    <!--To Active Tabs Based on the Dashboard Cards Click-->
    <script>
        function OnlineTabState() {
            $('.ClassroomTab').removeClass('active');
            $('.OnlineTab').addClass('active');
            $('.OnlineTabBody').addClass('active show');
            $('.ClassroomTabBody').removeClass('active show');
            loadOnlineSessionDT();
        }
        if (getUrlVars()["IsClassroomExist"] == "true") {
            OnlineTabState();
        }

        $('.alOnlineTab').click(function () {
            loadOnlineSessionDT();
        });

        function ClassroomTabState() {
            $('.OnlineTab').removeClass('active');
            $('.ClassroomTab').addClass('active');
            $('.OnlineTabBody').removeClass('active show');
            $('.ClassroomTabBody').addClass('active');
        }
    </script>

    <!---Create Question Validation and show Design on click--->
    <script>

        function ResetAskQuestion() {
            $('#divCreateQuestion').hide();
            $('#btnAskQuestion').show();
            document.getElementById("<%= txtQuestion.ClientID%>").value = '';
        }

        function clearQuestionText() {
            document.getElementById("<%= txtQuestion.ClientID%>").value = '';
        }

        function ShowAskQuestionDiv() {
            $('#divCreateQuestion').show();
            $('#btnAskQuestion').hide();
        }

        function CreateQuestionValidation() {
            var Question = document.getElementById("<%=txtQuestion.ClientID%>").value.trim();
            errors = [];
            if (Question == "") {
                errors.push("Enter Question");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }
    </script>

    <script>
        var readingTimer = 0;
        function DocReadDurationTimer(type) {
            if (type == "start") {
                var sec = parseInt(document.getElementById("<%=hdnSeconds.ClientID%>").value);
                var h = parseInt(document.getElementById("<%=hdnHours.ClientID%>").value);
                var min = parseInt(document.getElementById("<%=hdnMinutes.ClientID%>").value);
                readingTimer = setInterval(function () {
                    var seconds = sec + 1;
                    var minutes = min;
                    var hours = h;
                    SaveCurrentDuration(seconds, minutes, hours);
                    if (seconds >= 60) {
                        var minutes = min + 1;
                        seconds = 0;
                    }
                    if (minutes >= 60) {
                        var hours = h + 1;
                        minutes = 0;
                    }
                    sec = seconds;
                    min = minutes;
                    h = hours;

                    document.getElementById("Timer").innerHTML = hours + "h "
                        + minutes + "m " + seconds + "s ";
                }, 1000);
                $('#ReadDocModal').modal({ backdrop: 'static', keyboard: false });
            }
            else {
                $('#ReadDocModal').modal('hide');
                clearInterval(readingTimer);
                //SaveCurrentDuration();
            }
        }

        function OpenDocumentModal() {
            $('#<%=btnTakeExam.ClientID %>').prop('disabled', true);
            $('#<%=btnTakeExam.ClientID %>').css('cursor', 'not-allowed');
        }
        function StartDocReadDuration() {
            DocReadDurationTimer('start');
            //Start TMS_ML (3/4) Code ,//ML feature Checking
            if (ML_FeatureIDs.includes(7) == true) {
                GetRelatedObservationsForDocument($("#<%=hdnDocID.ClientID%>")[0].value);//TMS_ML
            }
            //End TMS_ML (3/4) Code
        }
        function CloseDocModal() {
            DocReadDurationTimer('stop');
        }

    </script>

    <script type="text/javascript">
        function SaveCurrentDuration(Seconds, Minutes, Hours) {
            UserSessionCheck();
            var TTS_ID = document.getElementById('<%=hdnTTS_ID.ClientID%>').value;
            var TraineeID = document.getElementById('<%=hdnEmpID.ClientID%>').value;
            var hours = Hours;
            var min = Minutes;
            var sec = Seconds;
            var objStr = { "TTS_ID": TTS_ID, "TraineeIDs": TraineeID, "Hours": hours, "Minutes": min, "Seconds": sec };
            var jsonData = JSON.stringify(objStr);
            $.ajax({
                type: "POST",
                url: "<%=ResolveUrl("~/TMS/WebServices/captureTime/captureSpentDuarationOnDoc.asmx/UpdateTraineeReadTimeOnDoc")%>",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function () {
                    var btnIsDisabled = $('#<%=btnTakeExam.ClientID %>').prop('disabled');
                     if ($(<%=hdnIsOnlineOrClassroom.ClientID%>).val() == 'ClassRoom') {
                         enableTakeExam();
                     }
                     else {
                         if (hours >= 1 || Minutes >= 10) {
                             enableTakeExam();
                         }
                         else {
                             disableTakeExam();
                         }
                     }
                 },
                 failure: function () {
                     custAlertMsg("Internal Server Error Occured. Document Read Duration not Recorded.", "error");
                 }
            });
        }


    </script>

    <script>      
        function openElectronicSignModal() {
            $('#usES_Modal').modal({ backdrop: 'static', keyboard: false });
            focusOnElecPwd();
            //ML feature Checking 
            if (ML_FeatureIDs.includes(7) == true) {
                $.get(AGA_ML_URL + "/Emp", { "Empid": $("#<%=hdnEmpID.ClientID%>").val() });
            }
        }
        // To Hide Electronic Sign Before Success/Error Message 
        function HideElectronicSignModal() {
            $('#usES_Modal').modal('hide');
            ReloadSessionDatatables();
            //ML feature Checking 
            if (ML_FeatureIDs.includes(7) == true) {
                $.get(AGA_ML_URL + "/Emp", { "Empid": $("#<%=hdnEmpID.ClientID%>").val() });
            }
        }
    </script>

    <script>
        $(document).ready(function () {
            $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
                var anchor = $(e.target).attr('href');
                if (anchor == "#ClassroomTraining") {
                    ReloadTheClassRoomDataTable();
                }
                else if (anchor == "#OnlineTraining") {
                    loadOnlineSessionDT();
                }
            })
        });
    </script>

    <script src='<%= Page.ResolveUrl("~/AppScripts/PDF_Viewer.min.js")%>'></script>
    <!--Start TMS_ML (4/4) Code-->
    <script src='<%= Page.ResolveUrl("~/Scripts/jquery-ui-1.12.1.min.js")%>'></script>
    <script src='<%= Page.ResolveUrl("~/AppScripts/TMS/TMS_ML_RelatedObservationsonDoc.js")%>'></script>
    <!--End TMS_ML (4/4) Code-->
</asp:Content>
