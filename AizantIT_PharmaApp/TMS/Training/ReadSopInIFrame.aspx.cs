﻿using AizantIT_PharmaApp.Common;
using System;
using System.Collections;
using System.Data;
using System.IO;
using TMS_BusinessLayer;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.Training
{
    public partial class ReadSopInIFrame : System.Web.UI.Page
    {
        TMS_BAL objTMS_Bal;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drTmsRoles = dtx.Select("ModuleID="+(int)Modules.TMS);
                        if (drTmsRoles.Length > 0)
                        {
                            dtTemp = drTmsRoles.CopyToDataTable();
                            if (dtTemp.Select("RoleID="+(int)TMS_UserRole.Trainee_TMS).Length > 0)
                            {
                                ToReadSopDocument();
                            }
                            else
                            {

                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                    else
                    {

                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                // HelpClass.showMsg(this, this.GetType(), "JRT1" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Doc-1:" + strline + "  " + strMsg, "error");
            }
        }

        private void ToReadSopDocument()
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                if (Session["Training"] != null)
                {
                    Hashtable ht = Session["Training"] as Hashtable;
                    string DocID = ht["DocID"].ToString();
                    string DocVersionID = ht["DocVersionID"].ToString();
                    string BasedOn = ht["BasedOn"].ToString();
                    if (BasedOn=="1")
                    {
                        if (ht["ReadPurpose"].ToString() == "Retrospective")
                        {
                            BasedOn = "2";
                        }
                    }
                    string BaseID = ht["BaseID"].ToString();
                   
                    DataTable dtDocData = objTMS_Bal.getDocDataToView(DocID,DocVersionID, BasedOn,BaseID);
                        if (dtDocData.Rows.Count > 0)
                        {
                            DataRow dr = dtDocData.Rows[0];
                            if (dr["FileType"].ToString() == "pdf")
                            {
                                divPdfViewer.Visible = true;
                                divDX_Viewer.Visible = false;
                                literalSop.Text = "<object data=\"" + ResolveUrl("~/TMS/Training/SopHandler.ashx") + "?DocID=" + DocID + "&DocVersionID=" + DocVersionID + "&BasedOn=" + BasedOn + "&BaseID=" + BaseID + "#toolbar=0&navpanes=0\" width=\"100%\" height=\"530px\" ></object>";
                            }
                            else
                            {
                                divPdfViewer.Visible = false;
                                divDX_Viewer.Visible = true;
                               
                                    ASPxRichEdit1.Open(
                                    Guid.NewGuid().ToString(),
                                    DevExpress.XtraRichEdit.DocumentFormat.Rtf,
                                    () =>
                                    {
                                        byte[] docBytes = (byte[])dtDocData.Rows[0]["Content"];
                                        return new MemoryStream(docBytes);
                                    });
                            }
                        }                      
                    if(Session["Training"] != null)
                    {
                        Session.Remove("Training");
                    }
                }
            }
            
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "Doc-2:" + strline + "  " + strMsg, "error");
            }
        }
    }
}