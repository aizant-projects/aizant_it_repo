﻿using AizantIT_PharmaApp.Common;
using System;
using System.Collections;
using System.Data;
using System.IO;
using TMS_BusinessLayer;

namespace AizantIT_PharmaApp.TMS.Training
{
    public partial class ReadDocInIFrame : System.Web.UI.Page
    {
        TMS_BAL objTMS_Bal;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                 ToReadDocument();               
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "Doc-1:" + strline + "  " + strMsg, "error");
            }
        }

        private void ToReadDocument()
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                if (Session["Training"] != null)
                {
                    Hashtable ht = Session["Training"] as Hashtable;
                    string DocID = ht["DocID"].ToString();

                    DataTable dtDocData = objTMS_Bal.ActiveDocumentContent(DocID);
                    if (dtDocData.Rows.Count > 0)
                    {
                        DataRow dr = dtDocData.Rows[0];
                        if (dr["FileType"].ToString() == "pdf")
                        {
                            divPdfViewer.Visible = true;
                            divDX_Viewer.Visible = false;
                            literalSop.Text = "<object data=\"" + ResolveUrl("~/TMS/Handler/ViewDocContent.ashx") + "?DocID=" + DocID + "#toolbar=0&navpanes=0\" width=\"100%\" height=\"530px\" ></object>";
                        }
                        else
                        {
                            divPdfViewer.Visible = false;
                            divDX_Viewer.Visible = true;

                            //DataTable dtContent = objTMS_Bal.getSopDataToView(DocID, DocVersionID);
                            //if (dtContent.Rows.Count > 0)
                            //{
                            ASPxRichEdit1.Open(
                            Guid.NewGuid().ToString(),
                            DevExpress.XtraRichEdit.DocumentFormat.Rtf,
                            () =>
                            {
                                byte[] docBytes = (byte[])dtDocData.Rows[0]["Content"];
                                return new MemoryStream(docBytes);
                            });
                        }
                    }

                    //}
                    if (Session["Training"] != null)
                    {
                        Session.Remove("Training");
                    }
                }
            }

            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "Doc-2:" + strline + "  " + strMsg, "error");
            }
        }
    }
}