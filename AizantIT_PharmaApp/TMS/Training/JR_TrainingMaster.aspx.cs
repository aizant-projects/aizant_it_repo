﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.Web.UI;
using TMS_BusinessLayer;
using TMS_BusinessObjects;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.Training
{
    public partial class JR_TrainingMaster : System.Web.UI.Page
    {
        TMS_BAL objTMS_Bal;
        UMS_BAL objUMS_BAL = new UMS_BAL();
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocObjects docObjects = new DocObjects();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=" + (int)Modules.TMS);
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainee_TMS).Length > 0) //For Trainee
                            {
                                if (Session["TrainingJrID"] != null)
                                {
                                    hdnJR_ID.Value = Session["TrainingJrID"].ToString();
                                    InitializeThePage();
                                }
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_T-1:" + strline + "  " + strMsg, "error");
            }
        }

        private void loadAuthorizeErrorMsg()
        {
            try
            {
                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                divAutorizedMsg.Visible = true;
                divMainContainer.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_T-2:" + strline + "  " + strMsg, "error");
            }

        }

        private void InitializeThePage()
        {
            try
            {
                if (Request.QueryString["Notification_ID"] != null)
                {
                    objUMS_BAL = new UMS_BAL();
                    int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs); //2 represents NotifyEmp had visited the page through notification link.
                }
                JR_TrainingView();
            }
            catch (Exception)
            {

                throw;
            }
        }
        private void EmptyData()
        {
            txtJR_AssignedBy.Text = "";
            txtJR_AssignedDate.Text = "";
            txtJR_Status.Text = "";
            txtTS_PreparedBy.Text = "";
            txtTS_PreparedDate.Text = "";
            txtJR_Version.Text = "";         
        }

        public void JR_TrainingView()
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                if (hdnJR_ID.Value != "")
                {
                    int JR_ID = Convert.ToInt32(hdnJR_ID.Value);
                    DataSet ds = objTMS_Bal.GetJrTrainingDetails(JR_ID);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        txtJR_List.Text = ds.Tables[0].Rows[0]["JR_Name"].ToString();
                        txtJR_AssignedBy.Text = ds.Tables[0].Rows[0]["JR_AssignedBy"].ToString();
                        txtJR_AssignedDate.Text = ds.Tables[0].Rows[0]["JR_AssignedDate"].ToString();
                        txtJR_Status.Text = ds.Tables[0].Rows[0]["JR_Status"].ToString();
                        txtTS_PreparedBy.Text = ds.Tables[0].Rows[0]["TS_PreparedBy"].ToString();
                        txtTS_PreparedDate.Text = ds.Tables[0].Rows[0]["TS_PreparedDate"].ToString();
                        txtJR_Version.Text = ds.Tables[0].Rows[0]["JR_Version"].ToString();
                        if (ds.Tables[0].Rows[0]["IsRetrospective"].ToString() == "True")
                        {
                            liRetrospectiveTab.Visible = true;
                            GetRetrospectiveJustificationData();
                            hdnIsRetrospective.Value = "Yes";
                        }
                        else
                        {
                            liRetrospectiveTab.Visible = false;
                        }
                    }
                    else
                    {
                        EmptyData();
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_T-3:" + strline + "  " + strMsg, "error");
            }
        }

        #region Retrospective
        private void GetRetrospectiveJustificationData()
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                DataTable dtJustNote = objTMS_Bal.GetRetrospectiveNoteData(Convert.ToInt32(hdnJR_ID.Value));
                if (dtJustNote.Rows.Count > 0)
                {
                    txtJustificationNote.InnerText = dtJustNote.Rows[0]["RQ_JustificationNote"].ToString();
                    if (txtJustificationNote.InnerText.Trim().Length > 0)
                    {
                        btnRetJustNoteSubmit.Visible = false;
                        txtJustificationNote.Disabled = true;
                        txtJustificationNote.Attributes.Add("style", "cursor:not-allowed");
                    }
                    else
                    {
                        btnRetJustNoteSubmit.Visible = true;
                    }
                    upRetSubmit.Update();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_T-4:" + strline + "  " + strMsg, "error");
            }
        }         
        #endregion

        protected void btnTakeExam_Click(object sender, EventArgs e)
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                string DocID = hdnDocID.Value;
                string JR_ID = hdnJR_ID.Value;
                DataTable dt = objTMS_Bal.GetActiveDocsBal(hdnDocID.Value, out int Result);
                if (Result == 0)
                {
                    HelpClass.CallJS_Func(this, this.GetType(), "ConfirmOralSubmission();");
                }
                else
                {
                    ucExamination.GetExamPaper(hdnJR_ID.Value, ((int)TmsBaseType.JR_Training).ToString(), hdnDocID.Value);
                    HelpClass.CallJS_Func(this, this.GetType(), "CloseReadDocModelAndCloseDocDetailsModel();");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_T-8:" + strline + "  " + strMsg, "error");
            }
        }
        
        protected void btnOpenDocumentOnRegular_Click(object sender, EventArgs e)
        {
            try
            {
                btnTakeExam.Text = "Take Exam";
                //btnTakeExam.Attributes.Add("title", "You would be able to take an exam after 10 min");
                divOralExamSubmittedMsg.Visible = false;
                divTakeExamBtn.Visible = true;
                objTMS_Bal = new TMS_BAL();

                int DocID = Convert.ToInt32(hdnDocID.Value);
                int DocVersionID = Convert.ToInt32(hdnDocVersionID.Value);
                string JR_ID = hdnJR_ID.Value;

                //For Timer Purpose we maintained the Document Details
                hdnDocID.Value = DocID.ToString();
                hdnDocVersionID.Value = DocID.ToString();

                //Get Document Read Details
                DataSet ds = objTMS_Bal.getDocInfo(JR_ID, DocID, DocVersionID, out int result);

                DataTable dtSopDurationandDetails = ds.Tables[0];
                if (dtSopDurationandDetails.Rows.Count > 0)
                {
                    DataRow dr = dtSopDurationandDetails.Rows[0];
                    hdnHours.Value = dr["Hours"].ToString();
                    hdnMinutes.Value = dr["Minutes"].ToString();
                    hdnSeconds.Value = dr["Seconds"].ToString();
                    hdnDocVersionID.Value = dr["DocVersionID"].ToString();                 
                }
                upBtnTakeExam.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "OpenPdfViewerContent();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_T-9:" + strline + "  " + strMsg, "error");
            }
        }
        
        protected void btnAskFeedBack_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdnIsFeedbacktoAsk.Value == "Y")
                {
                    ucFeedback.CreateFeedbackForm((int)TmsBaseType.JR_Training, Convert.ToInt32(hdnJR_ID.Value), 
                        Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_T-11:" + strline + "  " + strMsg, "error");
            }
        }

        protected void lblJR_TrainingList_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("/TMS/Training/JR_Training/JR_TrainingList.aspx?FilterType=2", false);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_T-12:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnCreateQuestion_Click(object sender, EventArgs e)
        {
            try
            {
                TraineeAskedQuestion();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_T-13:" + strline + "  " + strMsg, "error");
            }
        }
        public void TraineeAskedQuestion()
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                int TraineeID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                int DocID = Convert.ToInt32(hdnDocID.Value);
                int BaseID = Convert.ToInt32(hdnJR_ID.Value);
                objTMS_Bal.TraineeAskQuestion(TraineeID, txtQuestion.InnerText.ToString().Trim(), DocID, BaseID, 1, out int Result);
                if (Result == 1)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Question Inserted Successfully", "success", "clearQuestionText();");
                }
                else
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Question Already Exists", "error");
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", "HideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_T-14:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnSubmitJustificationNote_Click(object sender, EventArgs e)
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                EmpJrObjects objEmpJrObjects = new EmpJrObjects();
                objEmpJrObjects.JrID = Convert.ToInt32(hdnJR_ID.Value);
                objEmpJrObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                objEmpJrObjects.RQ_JustificationNote = txtJustificationNote.Value.Trim();
                objEmpJrObjects.CommentDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                int c = objTMS_Bal.RetJustificationNote_Insert(objEmpJrObjects, out int RetrospectiveStatus);
                string msgType = "", msg = "";
                switch (RetrospectiveStatus)
                {
                    case 1:
                        msg = "Retrospective submitted Successfully!";
                        msgType = "success";
                        hdnIsFeedbacktoAsk.Value = "Y";
                        txtJR_Status.Text = "JR at Trainer Evaluation";
                        break;
                    case 2:
                        msg = "Retrospective submitted Successfully!";
                        msgType = "success";
                        break;
                    case 3:
                        msg = "Retrospective is already submitted.";
                        msgType = "info";
                        break;
                    case 4:
                        msg = "There is no retrospective to this JR";
                        msgType = "info";
                        break;
                    case 5:
                        msg = "Retrospective submitted Successfully!";
                        msgType = "success";
                        hdnIsFeedbacktoAsk.Value = "Y";
                        txtJR_Status.Text = "JR at Training Coordinator Evaluation";
                        break;
                    default:
                        msg = "Unable to perform the action, Contact Admin.";
                        msgType = "info";
                        break;
                }

                HelpClass.custAlertMsgWithFunc(this, this.GetType(), msg, msgType, RetrospectiveStatus==1 || RetrospectiveStatus == 5 ? "AskFeedBackOnJR_Training()": "HideImg()");                
                GetRetrospectiveJustificationData();            
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_T-7:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnOralSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                string DocID = hdnDocID.Value;
                string JR_ID = hdnJR_ID.Value;
                DataTable dt = objTMS_Bal.JRDocToSubmitOral(DocID, JR_ID, out int Result);
                if (Result ==1)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Submitted for Oral Evaluation Successfully", "success", "AskFeedBackOnTraining(\"Y\")");
                    txtJR_Status.Text = "JR at Trainer Evaluation";
                }
                else if (Result == 2 || Result == 3)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Submitted for Oral Evaluation Successfully", "success", "ReloadRegularDocDataTable()");
                    txtJR_Status.Text = "JR at Trainer Evaluation";
                }
                else if (Result == 4)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "System error Occurred", "error");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_T-6:" + strline + "  " + strMsg, "error");
            }
        }
    }
}

