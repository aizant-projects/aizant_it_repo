﻿using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using TMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.Training
{
    public partial class TargetTrainerEvaluation : System.Web.UI.Page
    {
        TMS_BAL objTMS_Bal;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID="+(int)Modules.TMS);
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainer_TMS).Length > 0)
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                LoadUnAuthorizedMessage();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }               
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTE1:" + strline + "  " + strMsg, "error");
            }
        }
        private void LoadUnAuthorizedMessage()
        {
            try
            {
                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                divAutorizedMsg.Visible = true;
                mainContainer.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTE2:" + strline + "  " + strMsg, "error");
            }
        }

        public void InitializeThePage()
        {
            try
            {
                hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTE3:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnEvaluate_Click(object sender, EventArgs e)
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                int EvaluateStatus = 0, Opinion = 1, ExplanationStatus = 0;
                string Remarks = "";
                if (hdnEvaluateOn.Value.ToString() == "3")
                {
                    
                }
                else
                {
                    Opinion = rbtnSatisfactory.Checked == true ? 1 : 2;
                    Remarks = txtEvaluationComments.Text.Trim() == "" ? "N/A" : txtEvaluationComments.Text.Trim();
                }
                objTMS_Bal.EvaluateTargetTrainee(Convert.ToInt32(hdnTTS_TrainingSessionID.Value),
                    Convert.ToInt32(hdnTrainee.Value),
                    Convert.ToInt32(hdnEmpID.Value), out EvaluateStatus, Opinion, ExplanationStatus, Remarks);
                if (EvaluateStatus == 1)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Evaluated Trainee Record", "success", "ReloadTraineeSessions()");
                }
                else if (EvaluateStatus == 2)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Already Evaluated By Another Trainer", "info", "ReloadTraineeSessions()");
                }
                else if (EvaluateStatus == 3)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Trainee is in-active", "info", "ReloadTraineeSessions()");
                }
                else
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Evaluation Failed", "error", "ReloadTraineeSessions()");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTE5:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnViewExamAnswerSheet_Click(object sender, EventArgs e)
        {
            try
            {
                ucExamResultSheet.showExamResult(Convert.ToInt32(hdnTTS_TrainingSessionID.Value),Convert.ToInt32(hdnTTS_TargetExamID.Value),
                    hdnTrainee.Value,true, Convert.ToInt32(hdnEmpID.Value));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTE6:" + strline + "  " + strMsg, "error");
            }
        }      
    }
}