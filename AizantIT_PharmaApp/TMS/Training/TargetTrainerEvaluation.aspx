﻿<%@ Page Title="Target Evaluation" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="TargetTrainerEvaluation.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Training.TargetTrainerEvaluation" %>

<%@ Register Src="~/UserControls/TMS_UserControls/Exam/ucExamResultSheet.ascx" TagPrefix="uc1" TagName="ucExamResultSheet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <!--ML Enable and Disable script starts -->
   <script src='<%=ResolveUrl("~/AppScripts/ML_FeaturesChecking.js")%>'></script>
        <script>
            GetMLFeatureChecking(3);//Passing Paramenter Module ID
        </script>
    <!--ML Enable and Disable script ends-->
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnTTS_TrainingSessionID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnTTS_ID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnEvaluateOn" runat="server" Value="0" />
    <asp:HiddenField ID="hdnTrainee" runat="server" Value="0" />
    <asp:HiddenField ID="hdnTTS_TargetExamID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnDocID" runat="server" Value="0" />
    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div class="col-12 float-left padding-none" id="mainContainer" runat="server">
                <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 ">
                    <asp:Label ID="lblTitle" runat="server" Font-Bold="true" Text="Target Training Trainer Evaluation"></asp:Label>
                </div>
                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 bottom padding-none" style="overflow: auto; margin-top: 10px">
                    <table id="tblTTS_TrainerEvaluationList" class="datatable_cust tblTTS_TrainerEvalutionListClass breakword display" style="width: 100%">
                        <thead>
                            <tr>
                                <th>TTS_ID</th>
                                <th>TTSSession_ID</th>
                                <th>DocID</th>
                                <th>Document</th>
                                <th>Dept Code</th>
                                <th>Training Date</th>
                                <th>Start</th>
                                <th>End</th>
                                <th>Trainer</th>
                                <th>Type Of Training</th>
                                <th>Exam Type</th>
                                <th>View</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnEvaluate" runat="server" Text="" OnClick="btnEvaluate_Click" />
                <asp:Button ID="btnViewExamAnswerSheet" runat="server" Text="" OnClick="btnViewExamAnswerSheet_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <!--------Training Session Trainees Model---------->
    <div id="modelSessionTrainees" role="dialog" class="modal department fade">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Trainees List</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body float-left col-lg-12 col-sm-12 col-12 col-md-12">
                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                        <table id="tblSessionTrainees" class="datatable_cust display tblSessionTrainees breakword" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>Training Session ID</th>
                                    <th>Trainee ID</th>
                                    <th>EvaluationStatus</th>
                                    <th>TargetExam_ID</th>
                                    <th>Emp.Code</th>
                                    <th>Trainee Name</th>
                                    <th>Department</th>
                                    <th>Attempts</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!---- End---->

    <!--------Trainer Evaluation Opinion---------->
    <div id="modelTrainerEvaluationOpinion" role="dialog" class="modal department fade">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Trainer Opinion</h4>
                    <button type="button" class="close TabFocus" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body float-left col-lg-12 col-sm-12 col-12 col-md-12">
                    <div id="divOpinion">
                        <div class="padding-none col-sm-12 col-12 float-left col-lg-12 col-md-12 float-left" data-toggle="buttons">
                            <label for="inputPassword3" class=" float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none control-label">Opinion</label>
                            <label class="btn btn-primary_radio col-6 float-left col-lg-6 col-md-6 col-sm-6 active radio_check" id="lblRadioSatisfactory" runat="server">
                                <asp:RadioButton ID="rbtnSatisfactory" value="Yes" runat="server" GroupName="A" Text="Satisfactory" Checked="true" />
                            </label>
                            <label class="btn btn-primary_radio col-6 float-left col-lg-6 col-md-6 col-sm-6 radio_check" id="lblRadioNotSatisfactory" runat="server">
                                <asp:RadioButton ID="rbtnNotSatisfactory" value="No" runat="server" GroupName="A" Text="Not Satisfactory" />
                            </label>
                        </div>
                    </div>
                    <div class="form-group float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                        <label for="inputPassword3" class=" float-left col-lg-12 col-sm-12 col-12 col-md-12 control-label padding-none custom_label_answer">Comments</label>
                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                            <asp:TextBox ID="txtEvaluationComments" runat="server" TextMode="MultiLine" Rows="5" MaxLength="250" placeholder="Enter Comments" CssClass="form-control feed_text"></asp:TextBox>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="form-group float-left col-lg-12 col-sm-12 col-12 col-md-12 text-right padding-none">
                        <button type="button" class=" btn-cancel_popup  float-right" style="margin-left: 2px !important" data-dismiss="modal">Cancel</button>
                        <asp:UpdatePanel ID="upEvaluateOralPractical" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnEvaluateOralPractical" runat="server" CssClass=" btn-signup_popup float-right" Text="Evaluate" OnClick="btnEvaluate_Click" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!---- Trainer Evaluation Opinion ---->

    <uc1:ucExamResultSheet runat="server" ID="ucExamResultSheet"/>

    <!--View Training Session Trainees-->
    <script>   
        var ExamType = "";
        function ViewTrainingSessionTrainees(TTS_ID, TrainingSessionID, DocID, EvaluateOn) {
            $("#<%=hdnTTS_TrainingSessionID.ClientID%>").val(TrainingSessionID);
            $("#<%=hdnTTS_ID.ClientID%>").val(TTS_ID);
            $("#<%=hdnDocID.ClientID%>").val(DocID);

            if (EvaluateOn == "Oral") {
                $("#<%=hdnEvaluateOn.ClientID%>").val("1");
                ExamType = "O";
            }

            else if (EvaluateOn == "Practical") {
                $("#<%=hdnEvaluateOn.ClientID%>").val("2");
                ExamType = "P";
            }

            else {
                $("#<%=hdnEvaluateOn.ClientID%>").val("3");
                ExamType = "W";
            }

            $('#modelSessionTrainees').modal({ show: true, backdrop: 'static', keyboard: false });
        }

        $('#modelSessionTrainees').on('shown.bs.modal', function (e) {
            loadTraineeSessionsList(ExamType);
        });
    </script>

    <!---show Trainee List For Trainer to evaluate-->
    <script>
        var oTraineeSessions;
        function loadTraineeSessionsList(ExamType) {
            UserSessionCheck();
            if (oTraineeSessions != null) {
                oTraineeSessions.destroy();
            }
            var colShow = [];
            if (ExamType == "W") {
                colShow = [0, 1, 2, 3];
            }
            else {
                colShow = [0, 1, 2, 3, 7];
            }
            oTraineeSessions = $('#tblSessionTrainees').DataTable({
                columns: [
                    { 'data': 'TTS_TrainingSessionID' },
                    { 'data': 'TraineeID' },
                    { 'data': 'EvaluationStatus' },
                    { 'data': 'TargetExamID' },
                    { 'data': 'EmpCode' },
                    { 'data': 'Trainee' },
                    { 'data': 'DepartmentName' },
                    { 'data': 'Attempts' },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            switch (o.EvaluationStatus) {
                                case 0:
                                    if (o.Attempts == 0) {
                                        return 'Not Attempt';
                                    }
                                    else {
                                        return 'Not Qualified';
                                    }
                                case 1:
                                    return '<a class="OralEvaluation_tms" title="Evaluate" href="#" onclick="EvaluateTrainee(' + o.TTS_TrainingSessionID + ',' + o.TraineeID + ');"></a>';
                                case 2:
                                    return '<a class="OralEvaluation_tms" href="#" title="Evaluate" onclick="EvaluateTrainee(' + o.TTS_TrainingSessionID + ',' + o.TraineeID + ');"></a>';
                                case 3:
                                    return '<a class="AnswerSheet_tms"  href="#" title="Evaluate" onclick="EvaluateWrittenTrainee(' + o.TargetExamID + ',' + o.TTS_TrainingSessionID + ',' + o.TraineeID + '); "></a>';
                                case 5:
                                    return 'Evaluated';
                            }
                        }
                    }
                ],
                "scrollY": "47vh",
                "aoColumnDefs": [{ "targets": colShow, "visible": false },
                { className: "textAlignLeft", "targets": [4, 5, 6] }],
                "sAjaxSource": AizantIT_WebAPI_URL+"/api/TmsService/TargetTrainerEvaluationList",
                <%--"sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TTS_ListService.asmx/TrainingSessionTrainees")%>",--%>
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "TTS_TrainingSessionID", "value": $("#<%=hdnTTS_TrainingSessionID.ClientID%>").val() },
                        { "name": "EvaluateOn", "value": $("#<%=hdnEvaluateOn.ClientID%>").val() });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "headers": WebAPIRequestHeaderJson,
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = msg;
                            fnCallback(json);
                            $("#tblSessionTrainees").show(); 
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        }
    </script>

    <!--TTS_TrainerEvaluationList jQuery DataTable-->
    <script>
        function getUrlVars() {
            var vars = [], hash;
            var url = window.location.href;
            var hashes = url.slice(url.indexOf('?') + 1).split('&');
            if (url.indexOf("#") > 0) {
                var hashes = url.slice(url.indexOf('?') + 1, url.indexOf('#')).split('&');
            }

            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }

         $('#tblTTS_TrainerEvaluationList thead tr').clone(true).appendTo('#tblTTS_TrainerEvaluationList thead');
    $('#tblTTS_TrainerEvaluationList thead tr:eq(1) th').each(function (i) {
        if (i < 11) {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="Search " />');

            $('input', this).on('keyup change', function () {
                if (oTrainerEvaluationList.column(i).search() !== this.value) {
                    oTrainerEvaluationList
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
            if (i == 0 && i == 1 && i == 2)
            {
                $(this).html('');
            }
        }
        else {
            $(this).html('');
        }
        });

        var oTrainerEvaluationList = $('#tblTTS_TrainerEvaluationList').DataTable({
            columns: [
                { 'data': 'TTS_ID' },
                { 'data': 'TTS_TrainingSessionID' },
                { 'data': 'DocumentID' },
                { 'data': 'Document' },
                { 'data': 'DeptCode' },
                { 'data': 'TrainingDate' },
                { 'data': 'StartTime' },
                { 'data': 'EndTime' },
                { 'data': 'Trainer' },
                { 'data': 'TypeOfTraining' },
                { 'data': 'ExamType' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) {
                        return '<a class="view" title="View" href="#" onclick="ViewTrainingSessionTrainees(' + o.TTS_ID + ',' + o.TTS_TrainingSessionID + ',' + o.DocumentID + ',\'' + o.ExamType + '\');"></a>';
                    }
                }
            ],
            "orderCellsTop": true,
            "scrollY": "370px",
            "aoColumnDefs": [{ "targets": [0, 1, 2], "visible": false }, { className: "textAlignLeft", "targets": [3, 4, 8, 9, 10] }],
            "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TTS_ListService.asmx/TTS_Trainer_EvaluationList")%>",
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "TrainerID", "value": <%=hdnEmpID.Value%> }, { "name": "OnlyEvalutedRecords", "value": getUrlVars()["Evaluated"] });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        UserSessionCheck();
                        $("#tblTTS_TrainerEvaluationList").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        $(".tblTTS_TrainerEvalutionListClass").css({ "width": "100%" });
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });

    </script>

    <!--Action Buttons-->
    <script>
        function EvaluateTrainee(TrainingSessionID, TraineeID) {
            $("#<%=hdnTTS_TrainingSessionID.ClientID%>").val(TrainingSessionID);
            $("#<%=hdnTrainee.ClientID%>").val(TraineeID);
            $('#modelTrainerEvaluationOpinion').modal({ show: true, backdrop: 'static', keyboard: false });
            ClearTrainerOpinion();
        }

        function EvaluateWrittenTrainee(TargetExamID, TrainingSessionID, TraineeID) {
            $("#<%=hdnTTS_TrainingSessionID.ClientID%>").val(TrainingSessionID);
            $("#<%=hdnTTS_TargetExamID.ClientID%>").val(TargetExamID);
            $("#<%=hdnTrainee.ClientID%>").val(TraineeID);
            $("#<%=btnViewExamAnswerSheet.ClientID%>").click();
        }

        function ReloadTraineeSessions() {
            oTraineeSessions.ajax.reload();
            oTrainerEvaluationList.ajax.reload();
            $('#modelTrainerEvaluationOpinion').modal('hide');
            //ML feature checking 
            if (ML_FeatureIDs.includes(7) == true) {
                $.get(AGA_ML_URL + "/Emp", { "Empid": $("#<%=hdnTrainee.ClientID%>").val() });
            }
        }

        function ClearTrainerOpinion() {
            document.getElementById("<%= txtEvaluationComments.ClientID%>").value = '';
            $("#<%=lblRadioSatisfactory.ClientID%>").addClass("active");
            $("#<%=lblRadioNotSatisfactory.ClientID%>").removeClass("active");
        }

    </script>

</asp:Content>
