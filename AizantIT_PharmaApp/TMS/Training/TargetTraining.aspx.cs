﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using TMS_BusinessLayer;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.Training
{
    public partial class TargetTraining : System.Web.UI.Page
    {
        TMS_BAL objTMS_Bal;
        UMS_BAL objUMS_BAL;
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocObjects docObjects = new DocObjects();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ElectronicSign.buttonClick += new EventHandler(ElectronicSign_buttonClick);
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID="+(int)Modules.TMS);
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID="+(int)TMS_UserRole.Trainee_TMS).Length > 0)//only for Trainee role
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                LoadUnAuthorizedMessage();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }              
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TT1:" + strline + "  " + strMsg, "error");
            }
        }
        private void LoadUnAuthorizedMessage()
        {
            try
            {
                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                divAuthorizedMsg.Visible = true;
                mainContainer.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TT2:" + strline + "  " + strMsg, "error");
            }
        }

        public void InitializeThePage()
        {
            try
            {
                hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                if (Request.QueryString["Notification_ID"] != null)
                {
                    objUMS_BAL = new UMS_BAL();
                    int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs); //2 represents NotifyEmp had visited the page through notification link.
                }
                if (Request.QueryString["Tab"] != null)
                {
                    if (Request.QueryString["Tab"].ToString() == "Online")
                    {
                        //Code for loading the Second Tab(Online Training)
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", " OnlineTabState();", true);
                    }
                    else
                    {
                        //Code for Loading the first tab(Classroom Training)
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", " ClassroomTabState();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TT3:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnViewDocument_Click(object sender, EventArgs e)
        {
            try
            {
                Session["IsUserReadingDoc"] = true;
                GetReadDuration(hdnTTS_ID.Value, hdnEmpID.Value);
                AddDocDetailsToSession(hdnDocID.Value,hdnDocVersionID.Value);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "OpenDocViewModal();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TT4:" + strline + "  " + strMsg, "error");
            }
        }
        protected void ElectronicSign_buttonClick(object sender, EventArgs e)
        {
            try
            {
                bool b = ElectronicSign.IsPasswordValid;
                if (b == true)
                {
                    UpdateSelfCompleteStatus();
                }
                if (b == false)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Password is Incorrect.", "error", "focusOnElecPwd()");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TT5:" + strline + "  " + strMsg, "error");
            }
        }
        public void GetReadDuration(string TTS_ID,String EmpID)
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetReadDurationBal(TTS_ID, EmpID);
                if (dt.Rows.Count > 0)
                {
                    hdnHours.Value = dt.Rows[0]["Hours"].ToString();
                    hdnMinutes.Value = dt.Rows[0]["Minutes"].ToString();
                    hdnSeconds.Value = dt.Rows[0]["Seconds"].ToString();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TT6:" + strline + "  " + strMsg, "error");
            }
        }
        private void AddDocDetailsToSession(string DocID, string DocVersionID)
        {
            if (Convert.ToInt32(DocID) > 0 && Convert.ToInt32(DocVersionID) > 0)
            {
                Hashtable ht = new Hashtable();
                ht.Add("DocID", DocID.ToString());
                ht.Add("DocVersionID", DocVersionID.ToString());
                ht.Add("BasedOn", (int)TmsBaseType.Target_Training);
                ht.Add("BaseID", hdnTTS_ID.Value);
                Session["Training"] = ht;
            }
        }

        #region Exam
        protected void btnTakeExam_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(hdnTargetExamID.Value) != 0)
                {
                    ucExamination.GetExamPaper(hdnTargetExamID.Value, ((int)TmsBaseType.Target_Training).ToString(), hdnDocID.Value, hdnEmpID.Value);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "openElectronicSignModal();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TT7:" + strline + "  " + strMsg, "error");
            }
        }
        #endregion

        public void UpdateSelfCompleteStatus()
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                objTMS_Bal.ToGetTTS_StatusClose(hdnTTS_ID.Value, out int OutResult);
                if (OutResult==1)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "OpenTTSCloseValidation();", true);
                }
                else
                {
                    objTMS_Bal.UpdateSelfCompleteStatusBal(hdnTTS_ID.Value, hdnEmpID.Value, 2); //2 represents Self completed by Trainee.
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('Trainee Self completed Successfully','success',HideElectronicSignModal());", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TT8:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnRemarks_Click(object sender, EventArgs e)
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                string TrainingSessionID = hdnTrainingSessionID.Value;
                DataTable dt = objTMS_Bal.ViewRemarks(Convert.ToInt32(TrainingSessionID));
                if (dt.Rows.Count > 0)
                {
                    txtRemarks.Value = dt.Rows[0]["Remarks"].ToString();
                }
                upRemarks.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "OpenRemarks();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TT9:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnAskFeedBack_Click(object sender, EventArgs e)
        {
            try
            {
                ucFeedback.CreateFeedbackForm((int)TmsBaseType.Target_Training, Convert.ToInt32(hdnTTS_ID.Value), Convert.ToInt32(hdnEmpID.Value));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TT10:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnCreateQuestion_Click(object sender, EventArgs e)
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                objTMS_Bal.ToGetTTS_StatusClose(hdnTTS_ID.Value, out int OutResult);
                if (OutResult == 1)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "OpenTTSCloseValidation();", true);
                }
                else
                {
                 TraineeAskedQuestion();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TT11:" + strline + "  " + strMsg, "error");
            }
        }

        public void TraineeAskedQuestion()
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                int TraineeID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                int DocID = Convert.ToInt32(hdnDocID.Value);
                int BaseID = Convert.ToInt32(hdnTTS_ID.Value);
                objTMS_Bal.TraineeAskQuestion(TraineeID, txtQuestion.InnerText.ToString().Trim(), DocID, BaseID, 2, out int Result);
                if (Result == 1)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Question Inserted Successfully.", "success", "clearQuestionText();");
                }
                else
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Question Already Exists.", "error");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TT12:" + strline + "  " + strMsg, "error");
            }
        }
    }
}