﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using TMS_BusinessLayer;

namespace AizantIT_PharmaApp.TMS.Training
{
    /// <summary>
    /// Summary description for SopHandler
    /// </summary>
    public class SopHandler : IHttpHandler
    {
        TMS_BAL objTMS_Bal;

        public void ProcessRequest(HttpContext context)
        {
         
            objTMS_Bal = new TMS_BAL();
            string DocID = context.Request.QueryString["DocID"];
            string DocVersionID = context.Request.QueryString["DocVersionID"];
            string BasedOn = context.Request.QueryString["BasedOn"];
            string BaseID= context.Request.QueryString["BaseID"];
          
            DataTable dtDocDetails = objTMS_Bal.getDocDataToView(DocID, DocVersionID, BasedOn, BaseID);
            if (dtDocDetails.Rows.Count > 0)
            {
                DataRow dr = dtDocDetails.Rows[0];
                context.Response.Buffer = true;
                context.Response.Charset = "";               
                context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                if (dr["FileType"].ToString() == "pdf")
                {
                    context.Response.ContentType = "application/pdf";
                }
                context.Response.BinaryWrite((byte[])dr["Content"]);
                context.Response.Flush();
                context.Response.End();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}