﻿<%@ Page Title="JR TS Training Coordinator Evaluation" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" AutoEventWireup="true" CodeBehind="HodEvaluation.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Training.JR_Training.Evaluation.HodEvaluation" %>

<%@ Register Src="~/UserControls/TMS_UserControls/ucFeedback.ascx" TagPrefix="uc1" TagName="ucFeedback" %>
<%--<%@ Register Src="~/UserControls/TMS_UserControls/Exam/ucExamResultSheet.ascx" TagPrefix="uc1" TagName="ucExamResultSheet" %>--%>
<%@ Register Src="~/UserControls/TMS_UserControls/ucJrTrainedRecords.ascx" TagPrefix="uc1" TagName="ucJrTrainedRecords" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:HiddenField runat="server" ID="hdnHodEmpID" Value="0" />
    <asp:HiddenField runat="server" ID="hdnJR_ID" Value="0" />
    <asp:HiddenField runat="server" ID="hdnDocID" Value="0" />
     <asp:HiddenField runat="server" ID="hdnNotificationID" Value="0" />

    <asp:UpdatePanel ID="UpHdnFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField runat="server" ID="hdnEmpFeedbackID" Value="0" />
            <asp:HiddenField runat="server" ID="hdnJrID" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div class="col-12 padding-none" id="Div3" runat="server">
                <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 ">JR Training Coordinator Evaluation</div>
                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 top bottom padding-none">
                    <table id="tblJrEvaluateDetails" class="datatable_cust tblJrEvaluateDetailsClass display breakword" style="width: 100%">
                        <thead>
                            <tr>
                                <th>JR_ID</th>
                                <th>JR Name</th>
                                <th>Version</th>
                                <th>Trainee</th>
                                <th>Department</th>
                                <th>Assigned By</th>
                                <th>Assigned Date</th>
                                <th>EmpFeedbackID</th>
                                <th>Trained On</th>
                                <th>Feedback</th>
                                <th>Evaluate</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

     <!--------Hod Trained Document Details Modal---------->
    <div id="modalEmpTrainedDocs" role="dialog" class="modal department fade">
        <div class="modal-dialog" style="min-width:95%">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Trained Documents</h4>
                </div>
                <div class=" float-left col-lg-12 col-sm-12 col-12 col-md-12" style="background: #fff">
                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                        <table id="tblJR_EmpTrainedDocs" class="datatable_cust display tblJR_EmpTrainedDocsClass breakword" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>JrID</th>
                                    <th>DocID</th>
                                    <th>Document</th>
                                    <th>Doc.Version</th>
                                    <th>Department</th>
                                    <th>ReadDuration</th>
                                    <th>Attempts</th>
                                    <th>QualifiedDate</th>
                                    <th>MarksObtained</th>
                                    <th>EvaluatedTrainer</th>
                                    <th>View AS</th>
                                </tr>
                            </thead>
                        </table>
                        <div style="color: #098293; font-weight: bold">AS: Answer Sheet</div>
                    </div>
                </div>
                 <div class="modal-footer">
                </div>
            </div>

        </div>
    </div>
    <!---- End---->

  

    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnViewTrainedDocuments" runat="server" Text="viewDocs"/>
                <asp:Button ID="btnViewFeedback" runat="server" Text="viewFeedback" OnClick="btnViewFeedback_Click"/>
            </ContentTemplate>
         </asp:UpdatePanel>
         <asp:UpdatePanel ID="upBtnEvaluate" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnEvaluateJr" runat="server" Text="evaluateJr" OnClick="btnEvaluateJr_Click"/>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <uc1:ucFeedback runat="server" ID="ucFeedback" />
    <uc1:ucJrTrainedRecords runat="server" ID="ucJrTrainedRecords" />

    <script>
        function viewTrainingFeedback(EmpFeedbackID) {            
            $('#<%=hdnEmpFeedbackID.ClientID%>').val(EmpFeedbackID);
            $("#<%=btnViewFeedback.ClientID%>").click();
        }
        function viewTrainedDocuments(JR_ID) {
            GetRegularRecords(JR_ID, 0, true);
            GetRetrospectiveRecords(JR_ID, 0);
        }
        $('#modalEmpTrainedDocs').on('shown.bs.modal', function (e) {
            loadEmpTrainedDocuments();
        });
        function EvaluateJr(JrID) {
            $('#<%=hdnJrID.ClientID%>').val(JrID);
            $("#<%=btnEvaluateJr.ClientID%>").click();
        }
       
    </script>
    
    <!--JR Evaluation Main List jQuery DataTable-->
    <script>      
         $('#tblJrEvaluateDetails thead tr').clone(true).appendTo('#tblJrEvaluateDetails thead');
    $('#tblJrEvaluateDetails thead tr:eq(1) th').each(function (i) {
        if (i < 9) {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="Search " />');

            $('input', this).on('keyup change', function () {
                if (oTblJrEvaluateDetails.column(i).search() !== this.value) {
                    oTblJrEvaluateDetails
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
            if (i == 0 && i==7)
            {
                $(this).html('');
            }
        }
        else {
            $(this).html('');
        }
    });

        var oTblJrEvaluateDetails = $('#tblJrEvaluateDetails').DataTable({
            columns: [
                { 'data': 'JR_ID' },
                { 'data': 'JR_Name' },
                { 'data': 'JR_Version' },
                { 'data': 'TraineeName' },
                { 'data': 'TraineeDeptName' },
                { 'data': 'JR_AssignedBy' },
                { 'data': 'JR_AssignedDate' },
                { 'data': 'EmpFeedbackID' },
                {
                    "mData": null,
                    //"bSortable": false,
                    "mRender": function (o) { return '<a class="trainedrecords_tms" href="#" onclick="viewTrainedDocuments(' + o.JR_ID + ');"></a>'; }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) {
                        if (o.EmpFeedbackID != 0) {
                            return '<a class="feedback_tms" href="#" title="View Feedback" onclick="viewTrainingFeedback(' + o.EmpFeedbackID + ');"></a>';
                        }
                        else {
                            return '';
                        }
                    }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a class="ApproveDoc_tms" title="Evaluate" href="#" onclick="EvaluateJr(' + o.JR_ID + ');"></a>'; }
                }
            ],
            "fixedColumns": {
                "heightMatch": 'none'
            },
            "orderCellsTop": true,
            "scrollY": "47vh",
            "aoColumnDefs": [{ "targets": [0,7], "visible": false }, { className: "textAlignLeft", "targets": [1,3,4,5] }],
            "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/JrTraining/Evaluation/HodJrEvaluation.asmx/getHodJrEvaluationList")%>",
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "HodEmpID", "value": <%=hdnHodEmpID.Value%> }, { "name": "NotificationID", "value": <%=hdnNotificationID.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        UserSessionCheck();
                        $("#tblJrEvaluateDetails").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        $(".tblJrEvaluateDetailsClass").css({ "width": "100%" });
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }            
        });

        function reloadJrDetails() {
            oTblJrEvaluateDetails.ajax.reload();
        }
    </script>

    <!--View Trained Documents-->
    <script>
        var oTableJR_EmpTrainedDocs;
        function loadEmpTrainedDocuments() {
            if (oTableJR_EmpTrainedDocs != null) {
                oTableJR_EmpTrainedDocs.destroy();
            } 
            oTableJR_EmpTrainedDocs = $('#tblJR_EmpTrainedDocs').DataTable({
                columns: [
                    { 'data': 'JrID' },
                    { 'data': 'DocID' },
                    { 'data': 'Document' },
                    { 'data': 'DocVersion' },
                    { 'data': 'Department' },
                    { 'data': 'ReadDuration' },
                    { 'data': 'Attempts' },
                    { 'data': 'QualifiedDate' },
                    { 'data': 'MarksObtained' },
                    { 'data': 'EvaluatedTrainer' },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                                return '<a class="AnsSheet_tms"  href="#" onclick="viewAnswerSheet(' + o.DocID + ');"></a>';
                        }
                    }
                ],
                "fixedColumns": {
                    "heightMatch": 'none'
                },
                "order": [[7, "desc"]],
                "scrollY": "250px",
                "aoColumnDefs": [{ "targets": [0, 1], "visible": false }, { className: "textAlignLeft", "targets": [2, 4, 9] }, { "orderable": false, "targets": [0,1,3,5,8] }],
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/JrTraining/Evaluation/HodJrEvaluation.asmx/GetJR_EmpTrainedDocs")%>",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "JR_ID", "value": $('#<%=hdnJR_ID.ClientID%>').val() });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            UserSessionCheck();
                            $("#tblJR_EmpTrainedDocs").show();
                            $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                            $(".tblJR_EmpTrainedDocsClass").css({ "width": "100%" });
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
               
            });

        }
    </script>

</asp:Content>
