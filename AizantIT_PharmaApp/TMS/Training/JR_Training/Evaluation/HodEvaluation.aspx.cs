﻿using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using TMS_BusinessLayer;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.Training.JR_Training.Evaluation
{
    public partial class HodEvaluation : System.Web.UI.Page
    {
        TMS_BAL objTMS_Bal;
        UMS_BAL objUMS_BAL = new UMS_BAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    hdnHodEmpID.Value= (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                    if (!IsPostBack)
                    {
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID="+(int)Modules.TMS);
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0)
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }                  
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "HE01:" + strline + "  " + strMsg, "error");
            }
        }

        private void loadAuthorizeErrorMsg()
        {
            try
            {
                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                divAutorizedMsg.Visible = true;
                divMainContainer.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "HE02" + strline + "  " + strMsg, "error");
            }
        }
        private void InitializeThePage()
        {
            try
            {
                if (Request.QueryString["Notification_ID"] != null)
                {
                    objUMS_BAL = new UMS_BAL();
                    int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs); //2 represents NotifyEmp had visited the page through notification link.
                    hdnNotificationID.Value = Request.QueryString["Notification_ID"].ToString();

                }
            }
            catch (Exception)
            {

                throw;
            }

        }
        protected void btnViewFeedback_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdnEmpFeedbackID.Value != "0")
                {
                    ucFeedback.ViewFeedbackForm(Convert.ToInt32(hdnEmpFeedbackID.Value), (int)TmsBaseType.JR_Training);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "HE02" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnEvaluateJr_Click(object sender, EventArgs e)
        {
            objTMS_Bal = new TMS_BAL();
            int EvaluateResult=0;
            objTMS_Bal.EvalutedJrByHod(Convert.ToInt32(hdnJrID.Value), Convert.ToInt32(hdnHodEmpID.Value), out EvaluateResult);
            if (EvaluateResult==1)
            {
                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Evaluated Successfully", "success","reloadJrDetails()");
            }
            else if (EvaluateResult == 2)
            {
                HelpClass.custAlertMsg(this, this.GetType(), "Already Evaluated", "info");
            }
            else
            {
                HelpClass.custAlertMsg(this, this.GetType(), "Internal Error Occured", "error");
            }
        }

    }
}