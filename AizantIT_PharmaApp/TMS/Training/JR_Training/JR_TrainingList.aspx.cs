﻿using Aizant_Enums;
using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.Web.UI;
using TMS_BusinessLayer;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.Training.JR_Training
{
    public partial class JR_TrainingList : System.Web.UI.Page
    {
        TMS_BAL objTMS_BAL;
        UMS_BAL objUMS_BAL = new UMS_BAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID="+(int)AizantEnums.Modules.TMS);
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID="+(int)TMS_UserRole.Trainee_TMS).Length > 0) // Only for Trainee
                            {
                                InitializeThePage();
                                //divPrint.Visible = true;
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                               // divPrint.Visible = false;
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }                 
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_T-1:" + strline + "  " + strMsg, "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            try
            {
                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                divAutorizedMsg.Visible = true;
                divMainContainer.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TE-2:" + strline + "  " + strMsg, "error");
            }
        }
        private void InitializeThePage()
        {
            try
            {
                if (Request.QueryString["Notification_ID"] != null)
                {
                    objUMS_BAL = new UMS_BAL();
                    int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs); //2 represents NotifyEmp had visited the page through notification link.
                    hdnNotificationID.Value = Request.QueryString["Notification_ID"].ToString();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TE-3:" + strline + "  " + strMsg, "error");
            }
        }

        private void viewJR_Details()
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                if (hdnJR_ID.Value != "")
                {
                    int JR_ID = Convert.ToInt32(hdnJR_ID.Value);
                    hdnJR_ID.Value = JR_ID.ToString();
                    DataSet ds = objTMS_BAL.GetJR_Content(JR_ID);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        Literal1.Text = ds.Tables[0].Rows[0]["JR_Description"].ToString();
                        uptext.Update();
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "OpenModelViewJR();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JRTL_:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnViewJR_Details_Click(object sender, EventArgs e)
        {
            viewJR_Details();
        }

        protected void btnViewTraining_Click(object sender, EventArgs e)
        {
            if (Session["Training"] != null)
            {
                Session["Training"] = null;
            }
            Session["TrainingJrID"] = hdnJR_ID.Value;
            Response.Redirect("~/TMS/Training/JR_TrainingMaster.aspx", false);
        }

    }
}