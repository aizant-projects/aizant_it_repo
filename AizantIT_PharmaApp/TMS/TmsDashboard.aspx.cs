﻿using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.Web.UI.WebControls;
using TMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS
{
    public partial class TmsDashboard : System.Web.UI.Page
    {
        TMS_BAL objBAL_TMS;
        UMS_BusinessLayer.UMS_BAL objUMS_BAL;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        int EvaluationCount = 0;
                        int PendingCount = 0;
                        int RevertedCount = 0;
                        
                        int EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drTMS = dt.Select("ModuleID=" + (int)Modules.TMS);
                        if (drTMS.Length > 0)
                        {
                            dtTemp = drTMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0 || dtTemp.Select("RoleID=" + (int)TMS_UserRole.Admin_TMS).Length > 0 || dtTemp.Select("RoleID=" + (int)TMS_UserRole.QA_TMS).Length > 0
                                || dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainee_TMS).Length > 0 || dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainer_TMS).Length > 0 || dtTemp.Select("RoleID=" + (int)TMS_UserRole.Dept_Head).Length > 0)
                            {
                                hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                                BindYears();
                                if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0 || dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainer_TMS).Length > 0 || dtTemp.Select("RoleID=" + (int)TMS_UserRole.QA_TMS).Length > 0)
                                {
                                    BindDepartments(Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                                    hdnLoadCharts.Value = "true";
                                }
                                if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.Admin_TMS).Length > 0 && dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length == 0 && dtTemp.Select("RoleID=" + (int)TMS_UserRole.QA_TMS).Length == 0 && dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainee_TMS).Length == 0 && dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainer_TMS).Length == 0)
                                {
                                    Response.Redirect("~/TMS/ModifyActionEmployees.aspx", false);
                                }
                                if ((dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0 || dtTemp.Select("RoleID=" + (int)TMS_UserRole.Admin_TMS).Length > 0 || dtTemp.Select("RoleID=" + (int)TMS_UserRole.QA_TMS).Length > 0
                                || dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainer_TMS).Length > 0) && (dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainee_TMS).Length > 0))
                                {
                                    hdnToAccessRoles.Value = "1";
                                }
                                else if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0 || dtTemp.Select("RoleID=" + (int)TMS_UserRole.Admin_TMS).Length > 0 || dtTemp.Select("RoleID=" + (int)TMS_UserRole.QA_TMS).Length > 0
                                 || dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainer_TMS).Length > 0)
                                {
                                    hdnToAccessRoles.Value = "2";
                                }
                                else if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainee_TMS).Length > 0)
                                {
                                    BindAssignedDepartments(Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                                    hdnToAccessRoles.Value = "3";
                                }
                                #region TMS_ML
                                if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0)
                                {
                                    hdnTMS_TrainingCoordinator.Value = "3";//HOD
                                }
                                if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainee_TMS).Length > 0)
                                {
                                    hdnTMS_TrainingTrainee.Value = "6";//Trainee
                                }
                                //ML Feature is IsEnable 
                                API_BusinessLayer.API_BAL API_BAL = new API_BusinessLayer.API_BAL();
                                //Training Performance
                                if (Array.Exists(API_BAL.GetMLFeatureChecking(3), s => s.Equals(7)) == true)//3: TMS module ID,7:ML_Feature ID
                                {
                                    hdnMLTrainingPerformace_Enable.Value = "1";
                                } 
                                //Document Training Recommendations
                                if (Array.Exists(API_BAL.GetMLFeatureChecking(3), s => s.Equals(6)) == true)//3: TMS module ID,6:ML_Feature ID
                                {
                                    hdnTMS_DocumentTrainingRecommendations.Value ="1";
                                    //int DRCount= objBAL_TMS.GetDocumentRecommendedCount(EmpID);
                                    //hdnTMS_DocumentRecommendedCount.Value =Convert.ToString(DRCount);
                                }
                                #endregion

                                objBAL_TMS = new TMS_BAL();
                                DataSet ds = objBAL_TMS.GetDataTODashBoard(EmpID);
                                DataTable dtJR_Result = ds.Tables[0]; //Jr data
                                DataTable dtTTS_Result = ds.Tables[1];//TTS Data
                                DataTable dtQue_Result = ds.Tables[2];// Questionnaire
                                DataTable dtATC_Result = ds.Tables[3];//ATC Data
                                DataTable dtScheduledJRandTT_PendingList_Result = ds.Tables[4];
                                if (dtScheduledJRandTT_PendingList_Result.Rows.Count > 0)
                                {
                                    foreach (DataRow drScheduledJRandTT_PendingList_Result in dtScheduledJRandTT_PendingList_Result.Rows)
                                    {
                                        if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.Dept_Head).Length > 0) //for Dept Head role
                                        {
                                            divPendingCard.Visible = true;
                                            if (drScheduledJRandTT_PendingList_Result["CountFor"].ToString() == "ScheduledJRandTT_PendingList")
                                            {
                                                if (Convert.ToInt32(drScheduledJRandTT_PendingList_Result["Count"]) > 0)
                                                {
                                                    PendingCount += Convert.ToInt32(drScheduledJRandTT_PendingList_Result["Count"]);
                                                    hlScheduledJRandTT_PendingListForDeptHead.Visible = true;
                                                    hlScheduledJRandTT_PendingListForDeptHead.Text = string.Format("<div class='pull-left padding-none col-lg-11'>Scheduled JR and Target Training Pending</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drScheduledJRandTT_PendingList_Result["Count"].ToString());
                                                }
                                            }
                                        }
                                    }
                                }
                                if (dtJR_Result.Rows.Count > 0)
                                {
                                    foreach (DataRow drJR_Result in dtJR_Result.Rows)
                                    {
                                        if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0) //for HOD role
                                        {
                                            divEvaluationCard.Visible = true;
                                            divPendingCard.Visible = true;
                                            divRevertedCard.Visible = true;
                                            divATC_Card.Visible = true;

                                            if (drJR_Result["CountFor"].ToString() == "QaApprovedJR")
                                            {
                                                if (Convert.ToInt32(drJR_Result["Count"]) > 0)
                                                {
                                                    PendingCount += Convert.ToInt32(drJR_Result["Count"]);
                                                    hlJR_TS_PendingList.Visible = true;
                                                    hlJR_TS_PendingList.Text = string.Format("<div class='pull-left padding-none col-lg-11'>Assign Training Schedule to JR</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drJR_Result["Count"].ToString());
                                                }
                                            }
                                            if (drJR_Result["CountFor"].ToString() == "QaDeclinedJR")
                                            {
                                                if (Convert.ToInt32(drJR_Result["Count"]) > 0)
                                                {
                                                    RevertedCount += Convert.ToInt32(drJR_Result["Count"]);
                                                    hlJR_PendingList.Visible = true;
                                                    hlJR_PendingList.Text = string.Format("<div class='pull-left padding-none col-lg-11'>QA Reverted JR</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drJR_Result["Count"].ToString());
                                                }
                                            }
                                            if (drJR_Result["CountFor"].ToString() == "QaDeclinedTS")
                                            {
                                                if (Convert.ToInt32(drJR_Result["Count"]) > 0)
                                                {
                                                    RevertedCount += Convert.ToInt32(drJR_Result["Count"]);
                                                    hlJR_TS_Reverted.Visible = true;
                                                    hlJR_TS_Reverted.Text = string.Format("<div class='pull-left padding-none col-lg-11'>QA Reverted JR_TS</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drJR_Result["Count"].ToString());
                                                }
                                            }
                                            if (drJR_Result["CountFor"].ToString() == "HOD_Evaluation")
                                            {
                                                if (Convert.ToInt32(drJR_Result["Count"]) > 0)
                                                {
                                                    EvaluationCount += Convert.ToInt32(drJR_Result["Count"]);
                                                    hlJR_HodEvaluation.Visible = true;
                                                    hlJR_HodEvaluation.Text = string.Format("<div class='pull-left padding-none col-lg-11'>JR Training Coordinator Evaluation</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drJR_Result["Count"].ToString());
                                                }
                                            }
                                        }
                                        if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.QA_TMS).Length > 0) //for QA role
                                        {
                                            divPendingCard.Visible = true;
                                            divATC_Card.Visible = true;

                                            if (drJR_Result["CountFor"].ToString() == "TraineeAcceptedJR")
                                            {
                                                if (Convert.ToInt32(drJR_Result["Count"]) > 0)
                                                {
                                                    PendingCount += Convert.ToInt32(drJR_Result["Count"]);
                                                    hlJR_ReviewList.Visible = true;
                                                    hlJR_ReviewList.Text = string.Format("<div class='pull-left padding-none col-lg-11'>JR(s) for Approval</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drJR_Result["Count"].ToString());
                                                }
                                            }
                                            if (drJR_Result["CountFor"].ToString() == "AssignedTS")
                                            {
                                                if (Convert.ToInt32(drJR_Result["Count"]) > 0)
                                                {
                                                    PendingCount += Convert.ToInt32(drJR_Result["Count"]);
                                                    hlJR_TsReviewList.Visible = true;
                                                    hlJR_TsReviewList.Text = string.Format("<div class='pull-left padding-none col-lg-11'>JR Training Schedule(s) for Approval</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drJR_Result["Count"].ToString());
                                                }
                                            }
                                        }
                                        if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainer_TMS).Length > 0) //for Trainer role
                                        {
                                            divEvaluationCard.Visible = true;
                                            divRevertedCard.Visible = true;
                                            divPendingCard.Visible = true;

                                            if (drJR_Result["CountFor"].ToString() == "TrainerEvaluation")
                                            {
                                                if (Convert.ToInt32(drJR_Result["Count"]) > 0)
                                                {
                                                    EvaluationCount += Convert.ToInt32(drJR_Result["Count"]);
                                                    hlJR_TrainerEvaluation.Visible = true;
                                                    hlJR_TrainerEvaluation.Text = string.Format("<div class='pull-left padding-none col-lg-11'>JR Trainer Evaluation</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drJR_Result["Count"].ToString());
                                                }
                                            }
                                        }
                                        if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainee_TMS).Length > 0) //for Trainee role
                                        {
                                            divPendingCard.Visible = true;

                                            if (drJR_Result["CountFor"].ToString() == "AssignedJR")
                                            {
                                                if (Convert.ToInt32(drJR_Result["Count"]) > 0)
                                                {
                                                    PendingCount += Convert.ToInt32(drJR_Result["Count"]);
                                                    hlAcceptJR.Visible = true;
                                                    hlAcceptJR.Text = string.Format("<div class='pull-left padding-none col-lg-11'>Accept My JR</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drJR_Result["Count"].ToString());
                                                }
                                            }
                                            if (drJR_Result["CountFor"].ToString() == "JR_Training")
                                            {
                                                if (Convert.ToInt32(drJR_Result["Count"]) > 0)
                                                {
                                                    PendingCount += Convert.ToInt32(drJR_Result["Count"]);
                                                    hlJR_TrainingList.Visible = true;
                                                    hlJR_TrainingList.Text = string.Format("<div class='pull-left padding-none col-lg-11'>JR Document(s) to Complete</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drJR_Result["Count"].ToString());
                                                }
                                            }
                                        }
                                    }
                                }
                                if (dtTTS_Result.Rows.Count > 0)
                                {
                                    //DataRow drTTS_Result = dtTTS_Result.Rows[0];

                                    foreach (DataRow drTTS_Result in dtTTS_Result.Rows)
                                    {
                                        if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.QA_TMS).Length > 0) //for qa role
                                        {
                                            divPendingCard.Visible = true;
                                            divATC_Card.Visible = true;

                                            if (drTTS_Result["CountFor"].ToString() == "TTS_ApprovalPending")
                                            {
                                                if (Convert.ToInt32(drTTS_Result["Count"]) > 0)
                                                {
                                                    PendingCount += Convert.ToInt32(drTTS_Result["Count"]);
                                                    hlQA_TTS_Approval.Visible = true;
                                                    hlQA_TTS_Approval.Text = string.Format("<div class='pull-left padding-none col-lg-11'>Target Training(s) Approval</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drTTS_Result["Count"].ToString());
                                                }
                                            }
                                        }
                                        if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0) //for HOD role
                                        {
                                            divEvaluationCard.Visible = true;
                                            divPendingCard.Visible = true;
                                            divRevertedCard.Visible = true;
                                            divATC_Card.Visible = true;

                                            if (drTTS_Result["CountFor"].ToString() == "TTS_RefresherDocS")
                                            {
                                                if (Convert.ToInt32(drTTS_Result["Count"]) > 0)
                                                {
                                                    PendingCount += Convert.ToInt32(drTTS_Result["Count"]);
                                                    hlHOD_TTS_RefresherDocForThisMonth.Visible = true;
                                                    hlHOD_TTS_RefresherDocForThisMonth.Text = string.Format("<div class='pull-left padding-none col-lg-11'>Refresher Document(s) For This Month</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drTTS_Result["Count"].ToString());
                                                }
                                            }
                                            if (drTTS_Result["CountFor"].ToString() == "TTS_NewDocument_Pending")
                                            {
                                                if (Convert.ToInt32(drTTS_Result["Count"]) > 0)
                                                {
                                                    PendingCount += Convert.ToInt32(drTTS_Result["Count"]);
                                                    hlHOD_TTS_NewSOP_Pending.Visible = true;
                                                    hlHOD_TTS_NewSOP_Pending.Text = string.Format("<div class='pull-left padding-none col-lg-11'>New Document(s) For Target Training</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drTTS_Result["Count"].ToString());
                                                }
                                            }
                                            if (drTTS_Result["CountFor"].ToString() == "TTS_RevisionDocument_Pending")
                                            {
                                                if (Convert.ToInt32(drTTS_Result["Count"]) > 0)
                                                {
                                                    PendingCount += Convert.ToInt32(drTTS_Result["Count"]);
                                                    hlHOD_TTS_RevisionSOP_Pending.Visible = true;
                                                    hlHOD_TTS_RevisionSOP_Pending.Text = string.Format("<div class='pull-left padding-none col-lg-11'>Revision Document(s) for Target Training</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drTTS_Result["Count"].ToString());
                                                }
                                            }
                                            if (drTTS_Result["CountFor"].ToString() == "TTS_QA_Reverted")
                                            {
                                                if (Convert.ToInt32(drTTS_Result["Count"]) > 0)
                                                {
                                                    RevertedCount += Convert.ToInt32(drTTS_Result["Count"]);
                                                    hlQA_RevertedTTS.Visible = true;
                                                    hlQA_RevertedTTS.Text = string.Format("<div class='pull-left padding-none col-lg-11'>QA Reverted Target Training(s)</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drTTS_Result["Count"].ToString());
                                                }
                                            }
                                        }
                                        if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainee_TMS).Length > 0) //for Trainee role
                                        {
                                            if (drTTS_Result["CountFor"].ToString() == "TTS_ClassroomExamPending")
                                            {
                                                if (Convert.ToInt32(drTTS_Result["Count"]) > 0)
                                                {
                                                    PendingCount += Convert.ToInt32(drTTS_Result["Count"]);
                                                    hlTTS_ExamClassroom.Visible = true;
                                                    hlTTS_ExamClassroom.Text = string.Format("<div class='pull-left padding-none col-lg-11'>Target Training Exam(s) to Complete</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drTTS_Result["Count"].ToString());
                                                }
                                            }
                                            if (drTTS_Result["CountFor"].ToString() == "TTS_TraineeToAttendSessions")
                                            {
                                                if (Convert.ToInt32(drTTS_Result["Count"]) > 0)
                                                {
                                                    PendingCount += Convert.ToInt32(drTTS_Result["Count"]);
                                                    hlTTS_SessionClassroom.Visible = true;
                                                    hlTTS_SessionClassroom.Text = string.Format("<div class='pull-left padding-none col-lg-11'>Training Session(s) to Attend</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drTTS_Result["Count"].ToString());
                                                }
                                            }
                                            if (drTTS_Result["CountFor"].ToString() == "TTS_OnlineExamPending")
                                            {
                                                if (Convert.ToInt32(drTTS_Result["Count"]) > 0)
                                                {
                                                    PendingCount += Convert.ToInt32(drTTS_Result["Count"]);
                                                    hlOnlineExam.Visible = true;
                                                    hlOnlineExam.Text = string.Format("<div class='pull-left padding-none col-lg-11'>Online Target Exam(s) to be Complete</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drTTS_Result["Count"].ToString());
                                                }
                                            }
                                        }
                                        if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainer_TMS).Length > 0) //for Trainer role
                                        {
                                            divEvaluationCard.Visible = true;
                                            divRevertedCard.Visible = true;
                                            divPendingCard.Visible = true;

                                            if (drTTS_Result["CountFor"].ToString() == "TTS_TrainerEvaluation")
                                            {
                                                if (Convert.ToInt32(drTTS_Result["Count"]) > 0)
                                                {
                                                    EvaluationCount += Convert.ToInt32(drTTS_Result["Count"]);
                                                    hlTargetTrainerEvaluation.Visible = true;
                                                    hlTargetTrainerEvaluation.Text = string.Format("<div class='pull-left padding-none col-lg-11'>Target Trainer Evaluation</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drTTS_Result["Count"].ToString());
                                                }
                                            }
                                            if (drTTS_Result["CountFor"].ToString() == "TTS_Trainer_SessionCreationOrModification")
                                            {
                                                if (Convert.ToInt32(drTTS_Result["Count"]) > 0)
                                                {
                                                    PendingCount += Convert.ToInt32(drTTS_Result["Count"]);
                                                    hlTrainer_TTS_SessionCreationOrModification.Visible = true;
                                                    hlTrainer_TTS_SessionCreationOrModification.Text = string.Format("<div class='pull-left padding-none col-lg-11'>Target Training Session(s)</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drTTS_Result["Count"].ToString());
                                                }
                                            }
                                        }
                                    }
                                }
                                if (dtATC_Result.Rows.Count > 0)
                                {
                                    foreach (DataRow drATC_Result in dtATC_Result.Rows)
                                    {
                                        if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainer_TMS).Length > 0) //for Trainer role
                                        {
                                            divEvaluationCard.Visible = true;
                                            divRevertedCard.Visible = true;
                                            divPendingCard.Visible = true;

                                            if (drATC_Result["CountFor"].ToString() == "ATC_AuthorPending")
                                            {
                                                if (Convert.ToInt32(drATC_Result["Count"]) > 0)
                                                {
                                                    PendingCount += Convert.ToInt32(drATC_Result["Count"]);
                                                    hlPendingTrainingCalendar.Visible = true;
                                                    hlPendingTrainingCalendar.Text = string.Format("<div class='pull-left padding-none col-lg-11'>Annual Training Calendar(s) To Create</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drATC_Result["Count"].ToString());
                                                }
                                            }
                                            if (drATC_Result["CountFor"].ToString() == "ATC_QAorHODReverted")
                                            {
                                                if (Convert.ToInt32(drATC_Result["Count"]) > 0)
                                                {
                                                    RevertedCount += Convert.ToInt32(drATC_Result["Count"]);
                                                    hlTrainer_ATC_QAorHODReverted.Visible = true;
                                                    hlTrainer_ATC_QAorHODReverted.Text = string.Format("<div class='pull-left padding-none col-lg-11'>Reverted Annual Training Calendar(s)</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drATC_Result["Count"].ToString());
                                                }
                                            } 
                                        }
                                        if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0) //for HOD role
                                        { 
                                            divEvaluationCard.Visible = true;
                                            divPendingCard.Visible = true;
                                            divRevertedCard.Visible = true;
                                            divATC_Card.Visible = true;

                                            if (drATC_Result["CountFor"].ToString() == "ATC_HOD_Review")
                                            {
                                                if (Convert.ToInt32(drATC_Result["Count"]) > 0)
                                                {
                                                    PendingCount += Convert.ToInt32(drATC_Result["Count"]);
                                                    hlHOD_ATCApproval.Visible = true;
                                                    hlHOD_ATCApproval.Text = string.Format("<div class='pull-left padding-none col-lg-11'>Training Coordinator to Review Annual Training Calendar(s)</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drATC_Result["Count"].ToString());
                                                }
                                            }
                                        }
                                        if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.QA_TMS).Length > 0) //for QA role
                                        {
                                            divPendingCard.Visible = true;
                                            divATC_Card.Visible = true;

                                            if (drATC_Result["CountFor"].ToString() == "ATC_QA_Approve")
                                            {
                                                if (Convert.ToInt32(drATC_Result["Count"]) > 0)
                                                {
                                                    PendingCount += Convert.ToInt32(drATC_Result["Count"]);
                                                    hlQA_ATC_Approval.Visible = true;
                                                    hlQA_ATC_Approval.Text = string.Format("<div class='pull-left padding-none col-lg-11'>QA Approval Annual Training Calendar(s)</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drATC_Result["Count"].ToString());
                                                }
                                            }
                                        }
                                    }
                                }
                                if (dtQue_Result.Rows.Count > 0)
                                {
                                    //DataRow drQue_result = dtQue_Result.Rows[0];
                                    foreach (DataRow drQue_result in dtQue_Result.Rows)
                                    {
                                        if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0) //for HOD role
                                        {
                                            divEvaluationCard.Visible = true;
                                            divPendingCard.Visible = true;
                                            divRevertedCard.Visible = true;
                                            divATC_Card.Visible = true;
                                            if (drQue_result["CountFor"].ToString() == "QuestionnaireToApprove")
                                            {
                                                if (Convert.ToInt32(drQue_result["Count"]) > 0)
                                                {
                                                    PendingCount += Convert.ToInt32(drQue_result["Count"]);
                                                    hlHOD_Que_ToApproval.Visible = true;
                                                    hlHOD_Que_ToApproval.Text = string.Format("<div class='pull-left padding-none col-lg-11'>Questionnaire to Approve</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drQue_result["Count"].ToString());
                                                }
                                            }
                                        }
                                        if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainer_TMS).Length > 0) //for Trainer role
                                        {
                                            divEvaluationCard.Visible = true;
                                            divRevertedCard.Visible = true;
                                            divPendingCard.Visible = true;
                                            if (drQue_result["CountFor"].ToString() == "HODRevertedQuestionnaire")
                                            {
                                                if (Convert.ToInt32(drQue_result["Count"]) > 0)
                                                {
                                                    RevertedCount += Convert.ToInt32(drQue_result["Count"]);
                                                    hlDocQueMaster.Visible = true;
                                                    hlDocQueMaster.Text = string.Format("<div class='pull-left padding-none col-lg-11'>Training Coordinator Reverted Questionnaire</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drQue_result["Count"].ToString());
                                                }
                                            }
                                            if (drQue_result["CountFor"].ToString() == "TraineeQuestionsToTrainer")
                                            {
                                                if (Convert.ToInt32(drQue_result["Count"]) > 0)
                                                {
                                                    PendingCount += Convert.ToInt32(drQue_result["Count"]);
                                                    hlTraineeQuestionsToTrainer.Visible = true;
                                                    hlTraineeQuestionsToTrainer.Text = string.Format("<div class='pull-left padding-none col-lg-11'>UnAnswered Trainee Question(s)</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drQue_result["Count"].ToString());
                                                }
                                            }
                                        }
                                        if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainee_TMS).Length > 0) //for Trainee role
                                        {
                                            divPendingCard.Visible = true;
                                            if (drQue_result["CountFor"].ToString() == "TrainerAnswerstoTrainee")
                                            {
                                                if (Convert.ToInt32(drQue_result["Count"]) > 0)
                                                {
                                                    PendingCount += Convert.ToInt32(drQue_result["Count"]);
                                                    hlTrainerAnswersToTrainee.Visible = true;
                                                    hlTrainerAnswersToTrainee.Text = string.Format("<div class='pull-left padding-none col-lg-11'>Trainer Answered Question(s)</div><div class='pull-right padding-none col-lg-1 dashboard_panel_value'>{0}</div>", drQue_result["Count"].ToString());
                                                }
                                            }
                                        }
                                    }
                                }

                                lblEvaluationCount.Text = EvaluationCount.ToString();
                                lblPendingCount.Text = PendingCount.ToString();
                                lblRevertedCount.Text = RevertedCount.ToString();

                                if (PendingCount == 0)
                                {
                                    divPendingViewDetails.Visible = false;
                                    divPendingNoDetails.Visible = true;
                                }
                                if (EvaluationCount == 0)
                                {
                                    divEvaluationViewDetails.Visible = false;
                                    divEvaluationNoDetails.Visible = true;
                                }
                                if (RevertedCount == 0)
                                {
                                    divRevertedViewDetails.Visible = false;
                                    divRevertedNoDetails.Visible = true;
                                }
                            }
                            else
                            {
                                Response.Redirect("~/Dashboards/Home.aspx", false);
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TMS_DB1:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindDepartments(int EmpID)
        {
            try
            {
                objUMS_BAL = new UMS_BusinessLayer.UMS_BAL();
                int[] RoleIDs = new int[] { (int)TMS_UserRole.HOD_TMS, (int)TMS_UserRole.QA_TMS, (int)TMS_UserRole.Trainer_TMS };
                DataTable dt = objUMS_BAL.getRoleWiseDepts(EmpID, RoleIDs);
                ddlATC_Departments.Items.Clear();
                ddlATC_Departments.DataSource = dt;
                ddlATC_Departments.DataTextField = "DepartmentName";
                ddlATC_Departments.DataValueField = "DeptID";
                ddlATC_Departments.DataBind();
                ddlATC_Departments.Items.Insert(0, new ListItem("- Select Department -", "0"));

                ddlDepartmentATC.Items.Clear();
                ddlDepartmentATC.DataSource = dt;
                ddlDepartmentATC.DataTextField = "DepartmentName";
                ddlDepartmentATC.DataValueField = "DeptID";
                ddlDepartmentATC.DataBind();
                ddlDepartmentATC.Items.Insert(0, new ListItem(" All Departments ", "0"));

                ddlDepartmentCompleted.Items.Clear();
                ddlDepartmentCompleted.DataSource = dt;
                ddlDepartmentCompleted.DataTextField = "DepartmentName";
                ddlDepartmentCompleted.DataValueField = "DeptID";
                ddlDepartmentCompleted.DataBind();
                ddlDepartmentCompleted.Items.Insert(0, new ListItem(" All Departments ", "0"));

                ddlDepartmentOverDue.Items.Clear();
                ddlDepartmentOverDue.DataSource = dt;
                ddlDepartmentOverDue.DataTextField = "DepartmentName";
                ddlDepartmentOverDue.DataValueField = "DeptID";
                ddlDepartmentOverDue.DataBind();
                ddlDepartmentOverDue.Items.Insert(0, new ListItem(" All Departments ", "0"));

                ddlTrainingPerformanceDept.Items.Clear();
                ddlTrainingPerformanceDept.DataSource = dt;
                ddlTrainingPerformanceDept.DataTextField = "DepartmentName";
                ddlTrainingPerformanceDept.DataValueField = "DeptID";
                ddlTrainingPerformanceDept.DataBind();
                ddlTrainingPerformanceDept.Items.Insert(0, new ListItem(" All Departments ", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TMS_DB2:" + strline + "  " + strMsg, "error");
            }
        }

        public void BindAssignedDepartments(int EmpID)
        {
            try
            {
                objBAL_TMS = new TMS_BAL();
                DataTable dt = objBAL_TMS.BindAssignedDeptBAL(EmpID);

                ddlDepartmentCompleted.Items.Clear();
                ddlDepartmentCompleted.DataSource = dt;
                ddlDepartmentCompleted.DataTextField = "DepartmentName";
                ddlDepartmentCompleted.DataValueField = "DeptID";
                ddlDepartmentCompleted.DataBind();
                ddlDepartmentCompleted.Items.Insert(0, new ListItem(" All Departments ", "0"));

                ddlDepartmentOverDue.Items.Clear();
                ddlDepartmentOverDue.DataSource = dt;
                ddlDepartmentOverDue.DataTextField = "DepartmentName";
                ddlDepartmentOverDue.DataValueField = "DeptID";
                ddlDepartmentOverDue.DataBind();
                ddlDepartmentOverDue.Items.Insert(0, new ListItem(" All Departments ", "0"));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void BindYears()
        {
            try
            {
                BindJRandTargetCreationYears();//To bind JR and Target Creation Years
                BindATC_CreationYears();//To bind ATC Creation Years
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TMS_DB3:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindJRandTargetCreationYears()
        {
            try
            {
                objBAL_TMS = new TMS_BAL();
                DataTable dt = objBAL_TMS.GetCreationYearForJRandTargetBAL();
                if (dt.Rows[0]["Date"].ToString() != "")
                {
                    int year = Convert.ToInt32(dt.Rows[0]["Date"].ToString());
                    for (int i = year; i <= DateTime.Today.Year; i++)
                    {
                        ddlCompletedYear.Items.Add(new ListItem((i).ToString(), (i).ToString()));
                        if (DateTime.Now.Year == i)
                        {
                            ddlCompletedYear.SelectedValue = i.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TMS_DB4:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindATC_CreationYears()
        {
            try
            {
                objBAL_TMS = new TMS_BAL();
                DataSet ds = objBAL_TMS.GetCreationYearForATCBAL();
                if (ds.Tables[0].Rows[0]["MinDate"].ToString() != "")
                {
                    int Minyear = Convert.ToInt32(ds.Tables[0].Rows[0]["MinDate"].ToString());
                    int Maxyear = Convert.ToInt32(ds.Tables[1].Rows[0]["MaxDate"].ToString());
                    for (int i = Minyear; i <= Maxyear; i++)
                    {
                        ddlATCYear.Items.Add(new ListItem((i).ToString(), (i).ToString()));
                        if (DateTime.Now.Year == i)
                        {
                            ddlATCYear.SelectedValue = i.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TMS_DB5:" + strline + "  " + strMsg, "error");
            }
        }

    }
}