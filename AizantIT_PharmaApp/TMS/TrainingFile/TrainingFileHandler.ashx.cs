﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using TMS_BusinessLayer;

namespace AizantIT_PharmaApp.TMS.TrainingFile
{
    /// <summary>
    /// Summary description for TrainingFileHandler
    /// </summary>
    public class TrainingFileHandler : IHttpHandler
    {
        TMS_BAL objTMS_Bal;
        public void ProcessRequest(HttpContext context)
        {
            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");
            objTMS_Bal = new TMS_BAL();
            string JR_ID = context.Request.QueryString["JR_ID"];

            DataTable dtTrainingFile = objTMS_Bal.getTrainingFileDataToView(JR_ID);
            if (dtTrainingFile.Rows.Count > 0)
            {
                DataRow dr = dtTrainingFile.Rows[0];
                //Response.Clear();
                if (dr["Training_File"] != null)
                {
                    context.Response.Buffer = true;
                    context.Response.Charset = "";
                    //if (context.Request.QueryString["download"] == "1")
                    //{
                    //    context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + dr["FileName"].ToString());
                    //}
                    context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    //if (dr["FileType"].ToString() == "pdf")
                    //{

                    //}
                    context.Response.ContentType = "application/pdf";
                    context.Response.BinaryWrite((byte[])dr["Training_File"]);
                    context.Response.Flush();
                    context.Response.End();
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}