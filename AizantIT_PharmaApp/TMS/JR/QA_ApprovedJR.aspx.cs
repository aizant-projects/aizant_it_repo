﻿using System;
using System.Web.UI.WebControls;
using System.Data;
using AizantIT_PharmaApp.Common;
using TMS_BusinessLayer;
using System.Collections;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.JR
{
    public partial class QA_ApprovedJR1 : System.Web.UI.Page
    {
        TMS_BAL objBAL_TMS;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {

                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = (Session["UserDetails"] as DataSet).Tables[1].Select("ModuleID="+(int)Modules.TMS);
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0)
                            {
                                BindGrid();
                            }
                            else
                            {
                                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                                divAutorizedMsg.Visible = true;
                                mainContainer.Visible = false;
                            }

                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "QA_AJ-1:" + strline + "  " + strMsg, "error");
            }
        }
        //Need to review
        public void BindGrid()
        {
            try
            {
                objBAL_TMS = new TMS_BAL();
                DataTable dt = new DataTable();
                dt = objBAL_TMS.GetJRsToAccept(Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]),
                    (int)Aizant_Enums.AizantEnums.JR_Status.JR_Approved_By_QA);
                GVAssignTS.DataSource = dt;
                GVAssignTS.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "QA_AJ-2:" + strline + "  " + strMsg, "error");
            }
        }
        protected void lnkAssignTS_Click(object sender, EventArgs e)
        {
            try
            {
                // 
                int EmpID = Convert.ToInt32((sender as LinkButton).CommandArgument);
                GridViewRow row = (GridViewRow)((LinkButton)sender).NamingContainer;
                Label lblDepartment = row.FindControl("lblDepartment") as Label;
                Label lblJRID = row.FindControl("lblJR_ID") as Label;
                Label lblJRName = row.FindControl("lblJrName") as Label;
                Label lblDesignation = row.FindControl("lblDesignationName") as Label;

                Hashtable ht = new Hashtable();
                ht.Add("TraineeEmpID", EmpID);
                ht.Add("DeptID", Convert.ToInt32(lblDepartment.Text));
                ht.Add("JRID", Convert.ToInt32(lblJRID.Text));
                ht.Add("JRName", lblJRName.Text);
                ht.Add("DesignationName", lblDesignation.Text);
                Session["JRDetails"] = ht;
                if (Session["QA_Reverted_JR_ID"] != null)
                {
                    Session["QA_Reverted_JR_ID"] = null;
                }
                Response.Redirect("~/TMS/TS/AssignTS.aspx", false);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "QA_AJ-3:" + strline + "  " + strMsg, "error");
            }
        }
    }
}