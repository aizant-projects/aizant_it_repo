﻿<%@ Page Title="Approve Employee JR" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="ApproveEmployeeJR.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.JR.ApproveEmployeeJR1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12 grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div class="panel panel-default" id="mainContainer" runat="server">
                        <div class="grid_header float-left col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                <asp:Label ID="lblTitle" runat="server" Font-Bold="true">Approve JR</asp:Label>
                            </div>
           
            <div class="top float-left col-lg-12 col-xs-12 col-md-12 col-sm-12">
                <asp:GridView ID="gvEmployeeJR" EmptyDataText="No Records in Approve JR" OnRowCommand="gvEmployeeJR_RowCommand"
                    runat="server" AutoGenerateColumns="False" CssClass="table table-fixed fixHeadEmployeeJR AspGrid"
                    EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                     <RowStyle CssClass="col-xs-12 padding-none" />
                    <Columns>
                        <asp:TemplateField HeaderText="Line no" Visible="false">
                            <EditItemTemplate>
                                <asp:Label ID="lblEditLineno" runat="server" Text='<%# Bind("JR_ID") %>'></asp:Label>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblJrID" runat="server" Text='<%# Bind("JR_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="JR Name" SortExpression="JR_Name" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtJR_Name" runat="server" Text='<%# Bind("JR_Name") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblJR_Name" runat="server" Text='<%# Bind("JR_Name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="JR Version" SortExpression="JR_Version" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtJR_Version" runat="server" Text='<%# Bind("JR_Version") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblJR_Version" runat="server" Text='<%# Bind("JR_Version") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Emp Code" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                            <EditItemTemplate>
                                <asp:Label ID="lblEditEmpCode" runat="server" Text='<%# Bind("EmpCode") %>'></asp:Label>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblEmpCode" runat="server" Text='<%# Bind("EmpCode") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Trainee Name" SortExpression="EmpName" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEmpName" runat="server" Text='<%# Bind("EmpName") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("EmpName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Accepted Date" SortExpression="AcceptedDate" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTraineeAcceptedDate" runat="server" Text='<%# Bind("TraineeAcceptedDate") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblTraineeAcceptedDate" runat="server" Text='<%# Bind("TraineeAcceptedDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dept Code" SortExpression="DeptCode" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDeptCode" runat="server" Text='<%# Bind("DeptCode") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblDeptCode" runat="server" Text='<%# Bind("DeptCode") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Designation" SortExpression="DesignationName" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDesignationName" runat="server" Text='<%# Bind("DesignationName") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblDesignationName" runat="server" Text='<%# Bind("DesignationName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Assigned By" SortExpression="HODName" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtApprovedBy" runat="server" Text='<%# Bind("AssignedBy") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblApprovedBy" runat="server" Text='<%# Bind("AssignedBy") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Assigned Date" SortExpression="AssignedDate" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtAssignedDate" runat="server" Text='<%# Bind("AssignedDate") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAssignedDate" runat="server" Text='<%# Bind("AssignedDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="View" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkApprove" Font-Size="20px" ForeColor="Black" runat="server" CommandName="ApproveButton" CommandArgument='<%# Eval("JR_ID") %>' ToolTip="Click To View">
                                    <i class="view"></i>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="QA-Manager Action" HeaderStyle-CssClass="text-center" Visible="false">
                            <ItemTemplate>
                                <asp:Button ID="Button1" runat="server" Text="Submit" CssClass="btn TMS_Button" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>

     <script type="text/javascript">
        $(document).ready(function () {
            // Fix up GridView to support THEAD tags            
            fixGvHeader();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fixGvHeader();
        });

         function fixGvHeader() {
             var tbl = document.getElementsByClassName("fixHeadEmployeeJR");  
            // Fix up GridView to support THEAD tags     
            if ($(".fixHeadEmployeeJR").find("thead").length == 0) {
                $(".fixHeadEmployeeJR tbody").before("<thead><tr></tr></thead>");
                $(".fixHeadEmployeeJR thead tr").append($(".fixHeadEmployeeJR th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixHeadEmployeeJR tbody tr:first").remove();
                }               
            }
        }
  </script>
</asp:Content>
