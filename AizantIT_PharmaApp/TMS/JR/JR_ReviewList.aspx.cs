﻿using System;
using System.Data;
using AizantIT_PharmaApp.Common;
using System.Collections;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.JR
{
    public partial class JR_ReviewList : System.Web.UI.Page
    {
        UMS_BAL objUMS_BAL = new UMS_BAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID="+(int)Modules.TMS);
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.QA_TMS).Length > 0)
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                                divAutorizedMsg.Visible = true;
                                divMainContainer.Visible = false;
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }                    
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_RL-1:" + strline + "  " + strMsg, "error");
            }

        }

        private void InitializeThePage()
        {
            try
            {
                if (Request.QueryString["Notification_ID"] != null)
                {
                    objUMS_BAL = new UMS_BAL();
                    int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs); //2 represents NotifyEmp had visited the page through notification link.
                    hdnNotificationID.Value = Request.QueryString["Notification_ID"].ToString();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_RL-2:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnViewJR_Click(object sender, EventArgs e)
        {
            if (hdnJR_ID.Value != null)
            {
                int jrID = Convert.ToInt32(hdnJR_ID.Value);
                Hashtable ht = new Hashtable();
                ht.Add("ActionActor", "QA");
                ht.Add("JrID", hdnJR_ID.Value);
                Session["JR_ActionState"] = ht;
                Response.Redirect("~/TMS/JR/AssignJRtoEmp.aspx");
            }
        }
    }
}