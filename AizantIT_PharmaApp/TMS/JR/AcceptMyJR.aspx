﻿<%@ Page Title="Accept my JR" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="AcceptMyJR.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.JR.AcceptMyJR1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="mainContainer" runat="server">
            <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 ">
                <asp:Label ID="Label1" runat="server" Font-Bold="true">Accept My JR</asp:Label>
            </div>
            <div class="col-12   float-left bottom">
                <div class="col-12 padding-none  bottom float-left">
                    <asp:UpdatePanel ID="upGvAcceptMyJR" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView ID="GVAcceptJR" runat="server" OnRowCommand="GVAcceptJR_RowCommand" AutoGenerateColumns="False"
                                CssClass="table   col-12 float-left datatable_cust fixGridHead  AspGrid bottom"
                                EmptyDataText="No Records in Accept My JR" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle question_none">
                                <AlternatingRowStyle BackColor="White" />
                                <RowStyle CssClass="col-12 padding-none" />
                                <Columns>
                                    <asp:TemplateField HeaderText="JR ID" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblJR_ID" runat="server" Text='<%# Bind("JR_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="JR Name" SortExpression="JR_Name" ItemStyle-CssClass="textAlignLeft">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtJR_Name" runat="server" Text='<%# Bind("JR_Name") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblJR_Name" runat="server" Text='<%# Bind("JR_Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="JR Version" SortExpression="JR_Version">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtJR_Version" runat="server" Text='<%# Bind("JR_Version") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblJR_Version" runat="server" Text='<%# Bind("JR_Version") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Department" SortExpression="DepartmentName" ItemStyle-CssClass=" textAlignLeft">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtDepartmentName" runat="server" Text='<%# Bind("DepartmentName") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDepartmentName" runat="server" Text='<%# Bind("DepartmentName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Dept Code" SortExpression="DeptCode">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtDeptCode" runat="server" Text='<%# Bind("DeptCode") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDeptCode" runat="server" Text='<%# Bind("DeptCode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Designation" SortExpression="DesignationName" ItemStyle-CssClass="textAlignLeft">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtDesignationName" runat="server" Text='<%# Bind("DesignationName") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDesignationName" runat="server" Text='<%# Bind("DesignationName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Assigned By" SortExpression="HODName" ItemStyle-CssClass="textAlignLeft">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtHODName" runat="server" Text='<%# Bind("HODName") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblHODName" runat="server" Text='<%# Bind("HODName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Assigned Date" SortExpression="AssignedDate">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtHODAssignedDate" runat="server" Text='<%# Bind("HODAssignedDate") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblHODAssignedDate" runat="server" Text='<%# Bind("HODAssignedDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View JR">
                                        <EditItemTemplate>
                                            <asp:Label ID="lblEditJOB_DESCRIPTION" runat="server" Text='<%# Bind("JR_Name") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkViewJR" runat="server" CommandName="ViewButton" CommandArgument='<%# Eval("JR_ID") %>' ToolTip="Click To View JR" data-toggle="modal" data-target="#JRDescriptionModal">
                                                <asp:Image ID="Image1" runat="server" ImageUrl="/Images/TMS_Images/view_tms.png" />
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Associate  Action" Visible="false">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlchecking" ClientIDMode="Static" runat="server" CssClass="form-control">
                                                <asp:ListItem>Accept</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Accept JR">
                                        <ItemTemplate>
                                            <%--<asp:LinkButton ID="lnkApprove" CssClass="glyphicon glyphicon-check GV_Edit_Icon" runat="server" CommandName="ApproveButton" CommandArgument='<%# Eval("JR_ID") %>' ToolTip="Click To Accept"></asp:LinkButton>--%>
                                            <asp:LinkButton ID="lnkApprove" OnClientClick="ShowPleaseWait('show');" CssClass="AcceptMyJr_tms" runat="server" CommandName="ApproveButton" CommandArgument='<%# Eval("JR_ID") %>' ToolTip="Click To Accept"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <%--modal popup to load Jr Description--%>
            <div class="modal department fade" id="JRDescriptionModal" role="dialog" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-lg ">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header tmsModalHeader">

                            <h4 class="modal-title" style="color: aliceblue">JR Description</h4>
                            <button type="button" class="close " data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div class="row tmsModalContent">
                                <div class="col-12 ">
                                    <div style="border: groove; height: 400px; overflow: auto; border-width: 1px; padding-left: 20px">
                                        <asp:UpdatePanel ID="uptext" runat="server" UpdateMode="Conditional" style="margin: 10px">
                                            <ContentTemplate>
                                                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type='text/javascript'>
        function openModal() {
            $('#JRDescriptionModal').modal('show');
        }

        function hideImg() {
            ShowPleaseWait('hide');
        }
    </script>

    <script type="text/javascript">
            $(document).ready(function () {
                // Fix up GridView to support THEAD tags            
                fixGvHeader();
            });

            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_endRequest(function () {
                fixGvHeader();
            });

            function fixGvHeader() {
                var tbl = document.getElementsByClassName("fixHeadAcceptJR");
                // Fix up GridView to support THEAD tags     
                if ($(".fixHeadAcceptJR").find("thead").length == 0) {
                    $(".fixHeadAcceptJR tbody").before("<thead><tr></tr></thead>");
                    $(".fixHeadAcceptJR thead tr").append($(".fixHeadAcceptJR th"));
                    var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                    if (rows.length == 0) {
                        $(".fixHeadAcceptJR tbody tr:first").remove();
                    }
                }
            }
    </script>

</asp:Content>
