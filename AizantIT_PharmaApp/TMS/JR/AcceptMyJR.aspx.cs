﻿using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.Web.UI.WebControls;
using TMS_BusinessLayer;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.JR
{
    public partial class AcceptMyJR1 : System.Web.UI.Page
    {
        TMS_BAL objTMS_BAL;
        UMS_BAL objUMS_BAL = new UMS_BAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {

                        this.Title = "Accept My JR";
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID="+(int)Modules.TMS);
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID="+(int)TMS_UserRole.Trainee_TMS).Length > 0)
                            {
                                if (Request.QueryString["Notification_ID"] != null)
                                {
                                    objUMS_BAL = new UMS_BAL();
                                    int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs); //2 represents NotifyEmp had visited the page through notification link.
                                }
                                BindGV();
                            }
                            else
                            {
                                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                                divAutorizedMsg.Visible = true;
                                mainContainer.Visible = false;
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }                    
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AMJ-1:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindGV()
        {
            try
            {               
                objTMS_BAL = new TMS_BAL();
                DataTable dt = new DataTable();
                dt = objTMS_BAL.GetJRsToAccept(Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]), (int)Aizant_Enums.AizantEnums.JR_Status.JR_Assigned_to_Trainee);
                GVAcceptJR.DataSource = dt;
                GVAcceptJR.DataBind();
                if (dt.Rows.Count > 0)
                {
                    string text = dt.Rows[0]["JR_Description"].ToString();
                    Literal1.Text = text;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AMJ-2:" + strline + "  " + strMsg, "error");
            }
        }

        protected void GVAcceptJR_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                if (e.CommandName == "ApproveButton")
                {
                    int JRID = Convert.ToInt32(e.CommandArgument);
                    int s = objTMS_BAL.AcceptMyJR(JRID, Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]), (int)Aizant_Enums.AizantEnums.JR_Status.JR_Accepted_By_Trainee,out int ResultStatus);
                    if (ResultStatus == 1)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Accepted your JR.", "success", "hideImg();");
                    }
                    else if (ResultStatus == 2)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "JR is not in a Status of Accept.", "error", "hideImg();");
                    }
                    else if (ResultStatus == 3)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "You are not authorized person to action.", "error", "hideImg();");
                    }
                    else if (ResultStatus == 4)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), @"Can\'t Perform this Action due to you are Employee Status is InActive.", "error", "hideImg();");
                    }
                    else
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Fail to Accept your JR.", "error", "hideImg();");
                    }
                    BindGV();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AMJ-3:" + strline + "  " + strMsg, "error");
            }
        }
    }
}