﻿using Aizant_Enums;
using AizantIT_PharmaApp.Common;
using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BusinessLayer;
using TMS_BusinessObjects;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.JR
{
    public partial class AssignJRtoEmp1 : System.Web.UI.Page
    {
        UMS_BAL objUMS_BAL = new UMS_BAL();
        TMS_BAL objBal;
        int CurrentStatus, JrID = 0;
        string ActionActor;
        bool IsJrCreation = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ElectronicSign.buttonClick += new EventHandler(ElectronicSign_buttonClick);
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        var PreviousPage = "";
                        if (!Request.UrlReferrer.ToString().Contains("AssignJRtoEmp.aspx"))
                        {
                            PreviousPage = Request.UrlReferrer.ToString();
                            Session["TMS_BackToList"] = Request.UrlReferrer.ToString();
                        }
                        else
                        {
                            PreviousPage= Session["TMS_BackToList"].ToString();
                        }
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drTmsRoles = dtx.Select("ModuleID=" + (int)Modules.TMS);
                        if (drTmsRoles.Length > 0)
                        {
                            dtTemp = drTmsRoles.CopyToDataTable();
                            if (Request.QueryString["Create"] != null)
                            {
                                if (Request.QueryString["Create"].ToString() == "Y")
                                {
                                    if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0)
                                    {
                                        IsJrCreation = true;
                                        InitializeThePage();
                                    }
                                    else
                                    {
                                        loadAuthorizeErrorMsg();
                                    }
                                }
                            }
                            else
                            {
                                if (Session["JR_ActionState"] != null)
                                {
                                    Hashtable ht = Session["JR_ActionState"] as Hashtable;
                                    ActionActor = ht["ActionActor"].ToString();
                                    JrID = Convert.ToInt32(ht["JrID"]);
                                    hdnJrID.Value = JrID.ToString();
                                    if (ActionActor == "QA")
                                    {
                                        hlBackToList.Visible = true;
                                        //hlBackToList.NavigateUrl = "~/TMS/JR/JR_ReviewList.aspx?FilterType=2";
                                        hlBackToList.NavigateUrl = PreviousPage;
                                        if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.QA_TMS).Length > 0)
                                        {
                                            InitializeThePage();
                                        }
                                        else
                                        {
                                            loadAuthorizeErrorMsg();
                                        }
                                        //Session.Remove("JR_ActionState");
                                    }
                                    else
                                    {
                                        hlBackToList.Visible = true;
                                        // hlBackToList.NavigateUrl = "~/TMS/JR/JR_PendingList.aspx?FilterType=2";
                                        hlBackToList.NavigateUrl = PreviousPage;
                                        if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0)
                                        {
                                            InitializeThePage();
                                        }
                                        else
                                        {
                                            loadAuthorizeErrorMsg();
                                        }
                                    }
                                }
                                else
                                {
                                    loadAuthorizeErrorMsg();
                                }
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }
                    else
                    {
                        upJR_Description.Update();
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AJRT1:" + strline + "  " + strMsg, "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }

        private void InitializeThePage()
        {
            try
            {
                if (Request.QueryString["Notification_ID"] != null)
                {
                    objUMS_BAL = new UMS_BAL();
                    int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs); //2 represents NotifyEmp had visited the page through notification link.
                }
                if (IsJrCreation)
                {
                    BindDepartments(Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                    lblTitle.Text = "Assign JR to Employee";
                    divDecliendReason.Visible = true;
                    hdnJrTemplateChangeConfirm.Value = "Yes";
                    divHodCreation.Visible = true;
                    divDecliendReason.Visible = false;
                    divJR_DescriptionEditor.Visible = true;
                    divHodRemarks.Visible = true;
                    //txtAuthorName.Value = (Session["UserSignIn"] as DataSet).Tables[0].Rows[0]["Name"].ToString();
                }
                else
                {
                    if (JrID != 0)
                    {
                        divDecliendReason.Visible = true;
                        ddlDepartments.Enabled = false;
                        ddlEmployee.Enabled = false;
                        objBal = new TMS_BAL();

                        if (ActionActor != null)
                        {
                            int empID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            lblTitle.Text = "Employee JR";
                            DataSet ds = objBal.GetEmployeeJR(JrID.ToString());
                            DataTable dt = ds.Tables[0];
                            ddlDepartments.Items.Insert(0, new ListItem(dt.Rows[0]["DepartmentName"].ToString(), dt.Rows[0]["DeptID"].ToString()));
                            ddlEmployee.Items.Insert(0, new ListItem(dt.Rows[0]["Trainee"].ToString(), dt.Rows[0]["TraineeID"].ToString()));
                            BindJRTemplatesList(ds.Tables[1]);
                            ddlJrList.Items.FindByValue(dt.Rows[0]["JR_TemplateID"].ToString()).Selected = true;
                            hdnPreviousJRTemplateID.Value = dt.Rows[0]["JR_TemplateID"].ToString();
                            txtAuthorName.Value = dt.Rows[0]["HOD_EmpName"].ToString();
                            ddlReviewer.Items.Insert(0, new ListItem(dt.Rows[0]["QA_EmpName"].ToString(), dt.Rows[0]["QA_EmpID"].ToString()));
                            txtStatus.Text = dt.Rows[0]["JR_CurrentStatus"].ToString();
                            txtDesignation.Text = dt.Rows[0]["DesignationName"].ToString();
                            txtEmpCode.Text = dt.Rows[0]["EmpCode"].ToString();
                            txtEmpDOJ.Text = dt.Rows[0]["DOJ"].ToString();
                            CurrentStatus = Convert.ToInt32(dt.Rows[0]["CurrentStatusID"]);
                            string JrContent = dt.Rows[0]["JR_Description"].ToString();
                            txtJR_Name.Value = dt.Rows[0]["JR_Name"].ToString();
                            ShowJR_BasedOnStatusAndRole(ActionActor, CurrentStatus, JrContent);
                            if (empID == Convert.ToInt32(dt.Rows[0]["HOD_EmpID"]))
                            {
                                txtQaRemarks.Text = ds.Tables[2].Rows[0]["QA_LatestComments"].ToString();
                            }
                            else if (empID == Convert.ToInt32(dt.Rows[0]["QA_EmpID"]))
                            {
                                txtHodRemarks.Text = ds.Tables[2].Rows[0]["HOD_LatestComments"].ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AJRT2:" + strline + "  " + strMsg, "error");
            }
        }

        private void ShowJR_BasedOnStatusAndRole(string actionActor, int currentStatus, string JrContent)
        {
            try
            {
                ddlDepartments.Attributes.Add("disabled", "disabled");
                ddlEmployee.Attributes.Add("disabled", "disabled");
                ddlJrList.Attributes.Add("disabled", "disabled");
                ddlReviewer.Attributes.Add("disabled", "disabled");
                // ddlAuthor.Attributes.Add("disabled", "disabled");
                divHodRemarks.Visible = true;

                if (actionActor == "HOD")
                {
                    divReviewerDropdown.Visible = true;
                    divAuthorDropdown.Visible = false;
                    txtQaRemarks.Attributes.Add("disabled", "disabled");

                    if (currentStatus == 3)
                    {
                        divHodModifyJR.Visible = true;

                        divHodRemarks.Visible = true;
                        txtHodRemarks.Enabled = true;
                        divJR_DescriptionEditor.Visible = true;
                        //txtJR.InnerText = JrContent;
                        JrDescription.Html = JrContent;
                        ddlJrList.Attributes.Remove("disabled");
                    }
                    else
                    {
                        divHodRemarks.Visible = false;
                        //txtJR.Attributes.Add("disabled", "disabled");
                        //  ddlAuthor.Attributes.Add("disabled", "disabled");
                        txtJR_Name.Attributes.Add("disabled", "disabled");
                        txtEmpCode.Attributes.Add("disabled", "disabled");
                        txtDesignation.Attributes.Add("disabled", "disabled");
                        txtEmpDOJ.Attributes.Add("disabled", "disabled");
                        txtStatus.Attributes.Add("disabled", "disabled");
                        txtQaRemarks.Attributes.Add("disabled", "disabled");
                        txtHodRemarks.Attributes.Add("disabled", "disabled");

                        divJR_Description.Visible = true;
                        ltrJR_Description.Text = JrContent;
                    }
                }
                else //QA
                {

                    divReviewerDropdown.Visible = false;
                    divAuthorDropdown.Visible = true;
                    txtQaRemarks.Enabled = true;
                    divJR_Description.Visible = true;
                    ltrJR_Description.Text = JrContent;
                    divHodRemarks.Visible = true;

                    //txtJR.Attributes.Add("disabled", "disabled");
                    ddlReviewer.Attributes.Add("disabled", "disabled");
                    txtJR_Name.Attributes.Add("disabled", "disabled");
                    txtEmpCode.Attributes.Add("disabled", "disabled");
                    txtDesignation.Attributes.Add("disabled", "disabled");
                    txtEmpDOJ.Attributes.Add("disabled", "disabled");
                    txtHodRemarks.Attributes.Add("disabled", "disabled");

                    if (currentStatus == 2)
                    {
                        divQaModifyJR.Visible = true;
                    }
                    if (currentStatus == 1 || currentStatus == 3)
                    {
                        divDecliendReason.Visible = false;
                    }
                }
                upJrList.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AJRT3:" + strline + "  " + strMsg, "error");
            }

        }

        public void BindDepartments(int EmpID)
        {
            try
            {
                int[] RoleIDs = new int[] { (int)TMS_UserRole.HOD_TMS };
                DataTable dt = objUMS_BAL.getRoleWiseDepts(EmpID, RoleIDs);
                ddlDepartments.DataSource = dt;
                ddlDepartments.DataTextField = "DepartmentName";
                ddlDepartments.DataValueField = "DeptID";
                ddlDepartments.DataBind();
                ddlDepartments.Items.Insert(0, new ListItem("- Select Department -", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AJRtE5:" + strline + "  " + strMsg, "error");
            }
        }

        //added for electronic sign for QA
        protected void ElectronicSign_buttonClick(object sender, EventArgs e)
        {
            try
            {
                bool b = ElectronicSign.IsPasswordValid;
                if (b == true)
                {
                    if (hdnAction.Value == "Approve")
                    {
                        SubmitTheEmployeeJr("Approve");
                    }
                    else if (hdnAction.Value == "Revert")
                    {
                        SubmitTheEmployeeJr("Revert");
                    }
                    else if (hdnAction.Value == "Assign")
                    {
                        SubmitTheEmployeeJr("Assign");
                    }
                    else if (hdnAction.Value == "Modify")
                    {
                        SubmitTheEmployeeJr("Modify");
                    }
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "closeElectronicSignModal();", true);
                }
                if (b == false)
                {
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('');", true);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Password is Incorrect.", "error", "focusOnElecPwd()");
                }
                //Response.Write("Event fired in derived page.");
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Sign is Evaluated'); OpenApproveJRs();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AJRtE8:" + strline + "  " + strMsg, "error");
            }
        }
        //for Confirm alert
        private void SubmitTheEmployeeJr(string ActionType = "Approve")
        {
            try
            {
                objBal = new TMS_BAL();
                if (ddlDepartments.SelectedValue != "0" & ddlEmployee.SelectedValue != "0" & txtJR_Name.Value.Trim() != "")
                {
                    if (ActionType == "Approve" || ActionType == "Revert")
                    {


                        EmpJrObjects objEmpJrBO = new EmpJrObjects();
                        objEmpJrBO.DeptID = Convert.ToInt32(ddlDepartments.SelectedValue);
                        objEmpJrBO.EmpID = Convert.ToInt32(ddlEmployee.SelectedValue);
                        objEmpJrBO.Jr_Name = txtJR_Name.Value.Trim();
                        objEmpJrBO.Remarks = txtQaRemarks.Text.Trim();
                        objEmpJrBO.ApprovedOrDeclinedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        objEmpJrBO.JrID = Convert.ToInt32(hdnJrID.Value);
                        if (ActionType == "Approve")
                        {
                            ActionType = "Approved";
                            objEmpJrBO.CurrentStatus = (int)AizantEnums.JR_Status.JR_Approved_By_QA;
                            // int i=(int)AizantEnums.JR_Status.JR_Approved_By_QA;
                        }
                        else
                        {
                            ActionType = "Reverted";
                            objEmpJrBO.CurrentStatus = (int)AizantEnums.JR_Status.JR_Delined_By_QA;
                        }
                        //objEmpJrBO.JrID = Convert.ToInt32(Request.QueryString["JR_ID"]);
                        objBal.EmpJR_Operations("QA_Modify", objEmpJrBO, out int ResultStatus, "QA");
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "CloseESign();", true);
                        if (ResultStatus == 6)
                        {
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "JR " + ActionType + " Successfully !", "success", "OpenApproveJRs()");
                        }
                        else if (ResultStatus == 7)
                        {
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "You are not authorized Training Coordinator to do action.", "error", "OpenApproveJRs()");
                        }
                        else if (ResultStatus == 8)
                        {
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Action Already Performed on this JR.", "error", "OpenApproveJRs()");
                        }
                        else if (ResultStatus == 10)
                        {
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), @"Not allowed to Perform any action on this JR, Since Employee <span style=\'background-color:yellow;\'>" +txtEmpCode.Text.ToString()+" - " +ddlEmployee.SelectedItem.ToString()+"</span> Status is InActive. ", "error", "OpenApproveJRs()");
                        }
                        // }
                        // }
                    }
                    //else //for HOD modification
                    //{
                    else if (ActionType == "Modify")
                    {


                        EmpJrObjects objEmpJrBO = new EmpJrObjects();
                        objEmpJrBO.DeptID = Convert.ToInt32(ddlDepartments.SelectedValue);
                        objEmpJrBO.EmpID = Convert.ToInt32(ddlEmployee.SelectedValue);
                        objEmpJrBO.Jr_Name = txtJR_Name.Value.Trim();
                        objEmpJrBO.JR_TemplateID = Convert.ToInt32(ddlJrList.SelectedValue);
                        //objEmpJrBO.JrDescription = txtJR.InnerText;
                        objEmpJrBO.JrDescription = JrDescription.Html;
                        objEmpJrBO.Remarks = txtHodRemarks.Text;
                        objEmpJrBO.AssignedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        objEmpJrBO.CurrentStatus = (int)AizantEnums.JR_Status.JR_Assigned_to_Trainee;
                        objEmpJrBO.JrID = Convert.ToInt32(hdnJrID.Value);
                        objBal.EmpJR_Operations("HOD_Modify", objEmpJrBO, out int ResultStatus, "HOD");
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "CloseESign();", true);
                        if (ResultStatus == 3)
                        {
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "JR Modified Successfully !", "success", "OpenRevertedJRs()");
                            //upJR_Description.Update();
                        }
                        else if (ResultStatus == 4)
                        {
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "You are not authorized Training Coordinator to do action.", "error", "CloseESign()");
                        }
                        else if (ResultStatus == 5)
                        {
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Action Already Performed on this Trainee JR", "info", "OpenRevertedJRs()");
                        }
                        else if (ResultStatus == 10)
                        {
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), @"Not allowed to Perform any action on this JR, Since Employee <span style=\'background-color:yellow;\'>" + txtEmpCode.Text.ToString() + " - " +ddlEmployee.SelectedItem.ToString()+"</span> Status is InActive. ", "error", "OpenRevertedJRs()");
                        }
                    }
                    //else  //for creating new jr
                    //{
                    else if (ActionType == "Assign")
                    {
                        EmpJrObjects objEmpJrBO = new EmpJrObjects();
                        objEmpJrBO.DeptID = Convert.ToInt32(ddlDepartments.SelectedValue);
                        objEmpJrBO.EmpID = Convert.ToInt32(ddlEmployee.SelectedValue);
                        objEmpJrBO.Jr_Name = txtJR_Name.Value.Trim();
                        objEmpJrBO.JR_TemplateID = Convert.ToInt32(ddlJrList.SelectedValue);
                        //objEmpJrBO.JrDescription = txtJR.InnerText;
                        objEmpJrBO.JrDescription = JrDescription.Html;
                        objEmpJrBO.AssignedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        objEmpJrBO.Remarks = txtHodRemarks.Text.Trim();
                        objEmpJrBO.ApprovedOrDeclinedBy = Convert.ToInt32(ddlReviewer.SelectedValue);
                        objEmpJrBO.CurrentStatus = (int)AizantEnums.JR_Status.JR_Assigned_to_Trainee;
                        objBal.EmpJR_Operations("Insert", objEmpJrBO, out int ResultStatus, "HOD");
                        if (ResultStatus == 1)
                        {
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "JR Assigned to the Employee", "success", "ResetAssignJRtoEmpPage()");
                        }
                        else if (ResultStatus == 2)
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "JR already Assigned to this Trainee", "info");
                        }
                        else if (ResultStatus == 9)
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "You are not valid user to assign this JR.", "warning");
                        }
                        else if (ResultStatus == 10)
                        {
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), @"Not allowed to Perform any action on this JR, Since Employee <span style=\'background-color:yellow;\'>" + txtEmpCode.Text.ToString() + " - " + ddlEmployee.SelectedItem.ToString() + "</span> Status is InActive. ", "error", "ResetAssignJRtoEmpPage()");
                        }
                    }
                }
                else
                {
                    lblMsg.Text = "All fields represented with * is mandatory.";
                    HelpClass.custAlertMsg(this, this.GetType(), "All fields represented with * are mandatory.", "error");
                }
                upJR_Description.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AJRtE9:" + strline + "  " + strMsg, "error");
            }
        }
        public void Designation(int id)
        {
            objUMS_BAL = new UMS_BAL();
            try
            {
                DataTable dtEmpDetails = objUMS_BAL.getEmployeeDesignation(id);
                if (dtEmpDetails.Rows.Count > 0)
                {
                    txtDesignation.Text = dtEmpDetails.Rows[0]["DesignationName"].ToString();
                    txtEmpCode.Text = dtEmpDetails.Rows[0]["EmpCode"].ToString();
                    txtEmpDOJ.Text = dtEmpDetails.Rows[0]["StartDate"].ToString();
                }
                else
                {
                    txtDesignation.Text = txtEmpCode.Text = txtEmpDOJ.Text = string.Empty;
                }
                upEmpData.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AJRtE12:" + strline + "  " + strMsg, "error");

            }
        }
        protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                ddlReviewer.Enabled = true;
                if (ddlEmployee.SelectedValue != "0")
                {
                    string SelectedReviewer = ddlReviewer.SelectedValue;
                    Designation(Convert.ToInt32(ddlEmployee.SelectedValue));
                    DataTable dtQAs = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(ddlDepartments.SelectedValue), ((int)TMS_UserRole.QA_TMS), true, 2);
                    DataView dvQA = new DataView(dtQAs);
                    dvQA.RowFilter = "EmpID<>" + ddlEmployee.SelectedValue;
                    BindQAs(dvQA.ToTable());
                    if (ddlEmployee.SelectedValue != SelectedReviewer)
                    {
                        if (SelectedReviewer == "")
                        {
                            SelectedReviewer = "0";
                        }
                        ddlReviewer.Items.FindByValue(SelectedReviewer).Selected = true;
                    }
                }
                else
                {
                    ddlReviewer.Items.Clear();
                    ddlReviewer.Enabled = false;

                    txtDesignation.Text = "";
                }
                upJR_Description.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AJRtE13:" + strline + "  " + strMsg, "error");
            }
        }

        protected void ddlDepartments_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txtDesignation.Text = string.Empty;
                bindDropDowns(ddlDepartments.SelectedValue);
                hdnPreviousJRTemplateID.Value = "0";

                ddlReviewer.Items.Clear();
                ddlReviewer.Enabled = false;

                upJrList.Update();
                upJR_Description.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AJRT672:" + strline + "  " + strMsg, "error");
            }
        }

        private void bindDropDowns(string AccessibleDeptID)
        {
            try
            {
                objBal = new TMS_BAL();
                if (Convert.ToInt32(ddlDepartments.SelectedValue) > 0)
                {
                    DataSet dsEmpAndJRList = objBal.getHOD_ListAndQA_ListAndJR_ListByDeptID(AccessibleDeptID);
                    BindTrainees(dsEmpAndJRList.Tables[0]);
                    BindJRTemplatesList(dsEmpAndJRList.Tables[1]);
                }
                else
                {
                    ddlEmployee.Items.Clear();
                    ddlEmployee.Items.Insert(0, new ListItem("- Select Trainee -", "0"));
                    ddlJrList.Items.Clear();
                    ddlJrList.Items.Insert(0, new ListItem("-- Select JR Template --", "0"));
                    hdnPreviousJRTemplateID.Value = "0";
                }
                //ddlAuthor.Enabled = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AJRT696:" + strline + "  " + strMsg, "error");
            }
        }

        private void BindJRTemplatesList(DataTable dtJR_TemplateList)
        {
            try
            {
                ddlJrList.Items.Clear();
                ddlJrList.DataSource = dtJR_TemplateList;
                ddlJrList.DataValueField = "JR_TemplateID";
                ddlJrList.DataTextField = "JR_TemplateName";
                ddlJrList.DataBind();
                ddlJrList.Items.Insert(0, new ListItem("-- Select JR Template --", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AJRT735:" + strline + "  " + strMsg, "error");
            }
        }

        private void BindQAs(DataTable dtQAs)
        {
            try
            {
                ddlReviewer.Items.Clear();
                DataView dvQA = new DataView(dtQAs);
                dvQA.RowFilter = "EmpID<>" + Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                ddlReviewer.DataSource = dvQA.ToTable();
                ddlReviewer.DataValueField = "EmpID";
                ddlReviewer.DataTextField = "EmpName";
                ddlReviewer.DataBind();
                ddlReviewer.Items.Insert(0, new ListItem("-- Select Reviewer --", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AJRT756:" + strline + "  " + strMsg, "error");
            }
        }
        protected void ddlReviewer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                if (Convert.ToInt32(ddlReviewer.SelectedValue) > 0)
                {
                    string SelectedTrainee = ddlEmployee.SelectedValue;
                    if (ddlReviewer.SelectedValue == ddlEmployee.SelectedValue)
                    {
                        //DataTable dtemp = objBal.getEmpbyDefaultDeptIDandRoleID(ddlDepartments.SelectedValue, ((int)TMS_UserRole.Trainee_TMS).ToString());
                        DataTable dtemp = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(ddlDepartments.SelectedValue), ((int)TMS_UserRole.Trainee_TMS), true);
                        DataView dtEmployee = new DataView(dtemp);
                        dtEmployee.RowFilter = "EmpID<>" + Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        BindTrainees(dtEmployee.ToTable());
                    }
                    else
                    {
                        //DataTable dtemp = objBal.getEmpbyDefaultDeptIDandRoleID(ddlDepartments.SelectedValue, ((int)TMS_UserRole.Trainee_TMS).ToString());
                        DataTable dtemp = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(ddlDepartments.SelectedValue), ((int)TMS_UserRole.Trainee_TMS), true);
                        BindTrainees(dtemp);
                        ddlEmployee.Items.FindByValue(SelectedTrainee).Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AJRT786:" + strline + "  " + strMsg, "error");
            }
        }
        private void BindTrainees(DataTable dtActiveTrainees)
        {
            try
            {
                ddlEmployee.Items.Clear();
                DataView dvEmployee = new DataView(dtActiveTrainees);
                dvEmployee.RowFilter = "EmpID<>" + Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                ddlEmployee.DataSource = dvEmployee.ToTable();
                ddlEmployee.DataValueField = "EmpID";
                ddlEmployee.DataTextField = "EmpName";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("- Select Trainee -", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AJRT806:" + strline + "  " + strMsg, "error");
            }
        }

        private void bindJrAssignedEmployees(string DeptID)
        {
            try
            {
                objBal = new TMS_BAL();
                ddlEmployee.DataSource = objBal.getJrAssignedEmployeeList(DeptID);
                ddlEmployee.DataTextField = "Name";
                ddlEmployee.DataValueField = "EmpID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("- Select Trainee -", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AJRtE16:" + strline + "  " + strMsg, "error");
            }
        }
        private void bindJRList(string DeptID)
        {
            try
            {
                objBal = new TMS_BAL();
                ddlJrList.DataSource = objBal.JR_TemplateList(DeptID);
                ddlJrList.DataTextField = "JR_TemplateName";
                ddlJrList.DataValueField = "JR_TemplateID";
                ddlJrList.DataBind();
                ddlJrList.Items.Insert(0, new ListItem("-Select JR Name-", "0"));
                hdnPreviousJRTemplateID.Value = "0";
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AJRtE17:" + strline + "  " + strMsg, "error");
            }
        }
        protected void ddlJrList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(ddlJrList.SelectedValue) > 0)
                {
                    if (hdnJrTemplateChangeConfirm.Value == "No")
                    {
                        ddlJrList.SelectedValue = hdnPreviousJRTemplateID.Value;
                    }
                    else
                    {
                        bindJR_Description(ddlJrList.SelectedValue);
                        txtJR_Name.Value = ddlJrList.SelectedItem.Text;
                    }
                }
                else
                {
                    if (hdnJrTemplateChangeConfirm.Value == "No")
                    {
                        ddlJrList.SelectedValue = hdnPreviousJRTemplateID.Value;
                    }
                    else
                    {
                        bindJR_Description(ddlJrList.SelectedValue);
                        txtJR_Name.Value = "";
                    }
                }
                hdnPreviousJRTemplateID.Value = ddlJrList.SelectedValue;
                upJR_Description.Update();
                upJrList.Update();
                upJRName.Update();

            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AJRtE18:" + strline + "  " + strMsg, "error");
            }
        }

        private void bindJR_Description(string selectedValue)
        {
            try
            {
                if (selectedValue != "0")
                {
                    objBal = new TMS_BAL();
                    string Result = objBal.getJR_Description(ddlJrList.SelectedValue);
                    //txtJR.InnerText = Result;
                    JrDescription.Html = Result;
                }
                else
                {
                    //txtJR.InnerText = "";
                    JrDescription.Html = "";
                }
                //upJR_Description.Update();
                upJrList.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AJRtE21:" + strline + "  " + strMsg, "error");
            }
        }
    }
}