﻿<%@ Page Title="Assign JR to Employee" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" ValidateRequest="false" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="AssignJRtoEmp.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.JR.AssignJRtoEmp1" %>

<%@ Register Src="~/UserControls/ElectronicSign.ascx" TagPrefix="uc1" TagName="ElectronicSign" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HyperLink ID="hlBackToList" Visible="false" CssClass="btn btn-dashboard pull-right" runat="server">Back To List</asp:HyperLink>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <style>
        .fa-video-camera {
            visibility: hidden;
        }

        .hr_Line {
            margin-bottom: 10px;
            margin-top: 5px;
        }

        .literalStyle {
            padding: 30px;
            border: solid;
            border-color: #b5b4b4;
            border-width: 1px;
            /*height: 200px;*/
            min-height: 200px;
            max-height: 275px;
            overflow: auto
        }

        .richText .richText-help {
            display: none !important;
        }

        .dxpcLite .dxpc-mainDiv, .dxpcLite.dxpc-mainDiv, .dxdpLite .dxpc-mainDiv, .dxdpLite.dxpc-mainDiv {
            top: 0;
            left: 0;
            background-color: white;
            border: 1px solid #8B8B8B;
            height: 500px;
            overflow-y: scroll;
        }
        .dxeColorTableCellDiv:hover {
    border: 1px solid #808080 !important;
    width: 12px !important;
    height: 12px !important;
    font-size: 0 !important;
}
       
        /*.dxpcLite .dxpc-header, .dxdpLite .dxpc-header {
    color: #404040;
    background-color: #DCDCDC;
    border-bottom: 1px solid #C9C9C9;
    padding: 2px 2px 2px 12px;
    position: fixed;
    width: 32%;
    z-index: 999;
}*/
    </style>

    <asp:UpdatePanel ID="upHiddenFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnJrID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnAction" runat="server" Value="0" />
            <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnJrTemplateChangeConfirm" runat="server" Value="No" />
            <asp:HiddenField ID="hdnPreviousJRTemplateID" runat="server" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12 grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none" id="divMainContainer" runat="server">

            <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12 grid_header">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Label ID="lblTitle" runat="server"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="col-sm-12 padding-none float-left top">
                <div class="form-horizontal col-12 float-left">
                    <div class="float-left col-lg-4 col-sm-12 col-xs-12 col-md-4 padding-none">
                        <label class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12 control-label">Department<span class="mandatoryStar">*</span></label>
                        <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12">
                            <asp:UpdatePanel ID="upDepartment" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlDepartments" data-live-search="true" data-size="7"
                                        OnSelectedIndexChanged="ddlDepartments_SelectedIndexChanged" CssClass="selectpicker regulatory_dropdown_style form-control" runat="server" AutoPostBack="true" Width="100%" TabIndex="1">
                                    </asp:DropDownList>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlDepartments" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="float-left col-lg-4 col-sm-12 col-xs-12 col-md-4 padding-none">
                        <label class="control-label float-left col-lg-12 col-sm-12 col-xs-12 col-md-12">Trainee<span class="mandatoryStar">*</span></label>
                        <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12">
                            <asp:UpdatePanel ID="UpdatePanel10" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlEmployee" runat="server" data-live-search="true" data-size="7"
                                        CssClass="selectpicker regulatory_dropdown_style form-control" OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged" AutoPostBack="true" Width="100%" TabIndex="2">
                                    </asp:DropDownList>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlEmployee" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="float-left col-lg-4 col-sm-12 col-xs-12 col-md-4 padding-none" id="divJR_ddlField" runat="server">
                        <label class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12 control-label">JR Template</label>
                        <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12">
                            <asp:UpdatePanel ID="upJrList" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlJrList" data-live-search="true" data-size="7"
                                        CssClass="selectpicker regulatory_dropdown_style form-control"
                                        OnSelectedIndexChanged="ddlJrList_SelectedIndexChanged" onChange="Javascript:return OnJR_TemplateChange();" runat="server" AutoPostBack="true" Width="100%" TabIndex="3">
                                    </asp:DropDownList>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlJrList" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="float-left col-lg-4 col-sm-12 col-xs-12 col-md-4 padding-none" id="div1" runat="server">
                        <label class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12 control-label">JR Name<span class="mandatoryStar">*</span></label>
                        <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12">
                            <asp:UpdatePanel ID="upJRName" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <input type="text" class="form-control login_input_sign_up" name="JrName" runat="server" id="txtJR_Name" placeholder="Enter JR Name" maxlength="150" title="JR Name" tabindex="4" autocomplete="off" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="float-left col-lg-4 col-sm-12 col-xs-12 col-md-4 padding-none" id="divAuthorDropdown" runat="server" visible="false">
                        <label class="control-label float-left col-lg-12 col-sm-12 col-xs-12 col-md-12">Author<span class="mandatoryStar">*</span></label>
                        <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <input type="text" class="form-control login_input_sign_up" name="AuthorName" runat="server" id="txtAuthorName" placeholder="Author Name" title="Author Name" disabled="disabled" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlEmployee" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="float-left col-lg-4 col-sm-12 col-xs-12 col-md-4 padding-none" id="divReviewerDropdown" runat="server">
                        <label class="control-label float-left col-lg-12 col-sm-12 col-xs-12 col-md-12">Reviewer<span class="mandatoryStar">*</span></label>
                        <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlReviewer" runat="server"
                                        OnSelectedIndexChanged="ddlReviewer_SelectedIndexChanged"
                                        AutoPostBack="true" data-live-search="true" data-size="7"
                                        CssClass="selectpicker regulatory_dropdown_style form-control" TabIndex="5">
                                    </asp:DropDownList>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlReviewer" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="upEmpData" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class=" float-left col-lg-4 col-sm-12 col-xs-12 col-md-4 padding-none">
                                <label class="control-label float-left col-lg-12 col-sm-12 col-xs-12 col-md-12">Trainee Emp. Code</label>
                                <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12">
                                    <asp:TextBox ID="txtEmpCode" class="form-control login_input_sign_up" disabled="disabled" runat="server" placeholder="Employee Code"></asp:TextBox>
                                </div>
                            </div>
                            <div class=" float-left col-lg-4 col-sm-12 col-xs-12 col-md-4 padding-none">
                                <label class="control-label float-left col-lg-12 col-sm-12 col-xs-12 col-md-12">Designation</label>
                                <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12">
                                    <asp:TextBox ID="txtDesignation" class="form-control login_input_sign_up" disabled="disabled" runat="server" placeholder="Employee Designation"></asp:TextBox>
                                </div>
                            </div>
                            <div class=" float-left col-lg-4 col-sm-12 col-xs-12 col-md-4 padding-none">
                                <label class="control-label float-left col-lg-12 col-sm-12 col-xs-12 col-md-12">Trainee Date of Joining</label>
                                <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12">
                                    <asp:TextBox ID="txtEmpDOJ" class="form-control login_input_sign_up" disabled="disabled" runat="server" placeholder="DD MM YYYY"></asp:TextBox>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class=" float-left col-lg-4 col-sm-12 col-xs-12 col-md-4 padding-none" id="div2" runat="server">
                        <label class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12 control-label">Status</label>
                        <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12">
                            <asp:TextBox ID="txtStatus" CssClass="form-control login_input_sign_up" disabled="disabled" runat="server" placeholder="JR Status" ToolTip="JR Current Status"></asp:TextBox>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="upComments" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class=" float-left col-lg-4 col-sm-12 col-xs-12 col-md-6 padding-none" id="divHodRemarks" runat="server" visible="false">
                                <label class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12 control-label">Author Comments</label>
                                <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12">
                                    <asp:TextBox ID="txtHodRemarks" CssClass="form-control" TextMode="MultiLine" Rows="3" runat="server" onkeypress="return this.value.length<=250" placeholder="Author Comments" ToolTip="Enter Author Comments"></asp:TextBox>
                                </div>
                            </div>
                            <div class=" float-left col-lg-4 col-sm-12 col-xs-12 col-md-6 padding-none" id="divDecliendReason" runat="server" visible="false">
                                <label class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12 control-label">Reviewer Comments</label>
                                <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12">
                                    <asp:TextBox ID="txtQaRemarks" TextMode="MultiLine" Rows="3" CssClass="form-control" runat="server" onkeypress="return this.value.length<=250" ToolTip="Reviewer Comments" placeholder="Enter Reviewer Comments"></asp:TextBox>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

            <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12 ">
                <ul class="nav nav-tabs tab_grid">
                    <li class=" nav-item "><a data-toggle="tab" class="nav-link active" href="#ResponsibilitiesTab">Responsibilities</a></li>
                    <li class="nav-item "><a data-toggle="tab" class="nav-link " href="#JrHistoryTab">History</a></li>
                </ul>
                <div class="tab-content">
                    <div id="ResponsibilitiesTab" class="tab-pane fade show active">
                        <div id="row">
                            <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none top">
                                <div class="form-group padding-none">
                                    <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                                        <div id="divJR_DescriptionEditor" runat="server" visible="false">
                                            <asp:UpdatePanel ID="upJR_Description" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <%-- <textarea name="txtJR" id="txtJR" runat="server" class="JR_Content" style="height: 360px;" tabindex="6"></textarea>--%>

                                                    <dx:ASPxHtmlEditor ID="JrDescription" runat="server" Theme="Default" Width="100%" EnableTheming="True" ClientInstanceName="txtJrDescription" Height="476px">
                                                        <Toolbars>
                                                            <dx:HtmlEditorToolbar Name="StandardToolbar1">
                                                                <Items>
                                                                    <dx:ToolbarParagraphFormattingEdit AdaptivePriority="2" Width="120px">
                                                                        <Items>
                                                                            <dx:ToolbarListEditItem Text="Normal" Value="p" />
                                                                            <dx:ToolbarListEditItem Text="Heading  1" Value="h1" />
                                                                            <dx:ToolbarListEditItem Text="Heading  2" Value="h2" />
                                                                            <dx:ToolbarListEditItem Text="Heading  3" Value="h3" />
                                                                            <dx:ToolbarListEditItem Text="Heading  4" Value="h4" />
                                                                            <dx:ToolbarListEditItem Text="Heading  5" Value="h5" />
                                                                            <dx:ToolbarListEditItem Text="Heading  6" Value="h6" />
                                                                            <dx:ToolbarListEditItem Text="Address" Value="address" />
                                                                            <dx:ToolbarListEditItem Text="Normal (DIV)" Value="div" />
                                                                        </Items>
                                                                    </dx:ToolbarParagraphFormattingEdit>
                                                                    <dx:ToolbarFontNameEdit AdaptivePriority="2">
                                                                        <Items>
                                                                            <dx:ToolbarListEditItem Text="Times New Roman" Value="Times New Roman" />
                                                                            <dx:ToolbarListEditItem Text="Tahoma" Value="Tahoma" />
                                                                            <dx:ToolbarListEditItem Text="Verdana" Value="Verdana" />
                                                                            <dx:ToolbarListEditItem Text="Arial" Value="Arial" />
                                                                            <dx:ToolbarListEditItem Text="MS Sans Serif" Value="MS Sans Serif" />
                                                                            <dx:ToolbarListEditItem Text="Courier" Value="Courier" />
                                                                            <dx:ToolbarListEditItem Text="Segoe UI" Value="Segoe UI" />
                                                                        </Items>
                                                                    </dx:ToolbarFontNameEdit>
                                                                    <dx:ToolbarFontSizeEdit AdaptivePriority="2">
                                                                        <Items>
                                                                            <dx:ToolbarListEditItem Text="1 (8pt)" Value="1" />
                                                                            <dx:ToolbarListEditItem Text="2 (10pt)" Value="2" />
                                                                            <dx:ToolbarListEditItem Text="3 (12pt)" Value="3" />
                                                                            <dx:ToolbarListEditItem Text="4 (14pt)" Value="4" />
                                                                            <dx:ToolbarListEditItem Text="5 (18pt)" Value="5" />
                                                                            <dx:ToolbarListEditItem Text="6 (24pt)" Value="6" />
                                                                        </Items>
                                                                    </dx:ToolbarFontSizeEdit>
                                                                    <dx:ToolbarBoldButton AdaptivePriority="1" BeginGroup="True">
                                                                    </dx:ToolbarBoldButton>
                                                                    <dx:ToolbarItalicButton AdaptivePriority="1">
                                                                    </dx:ToolbarItalicButton>
                                                                    <dx:ToolbarUnderlineButton AdaptivePriority="1">
                                                                    </dx:ToolbarUnderlineButton>
                                                                    <dx:ToolbarStrikethroughButton AdaptivePriority="1">
                                                                    </dx:ToolbarStrikethroughButton>
                                                                    <dx:ToolbarJustifyLeftButton AdaptivePriority="1" BeginGroup="True">
                                                                    </dx:ToolbarJustifyLeftButton>
                                                                    <dx:ToolbarJustifyCenterButton AdaptivePriority="1">
                                                                    </dx:ToolbarJustifyCenterButton>
                                                                    <dx:ToolbarJustifyRightButton AdaptivePriority="1">
                                                                    </dx:ToolbarJustifyRightButton>
                                                                    <%--<dx:ToolbarBackColorButton AdaptivePriority="1" BeginGroup="True">
                                                                    </dx:ToolbarBackColorButton>--%>
                                                                    <dx:ToolbarFontColorButton AdaptivePriority="1">
                                                                    </dx:ToolbarFontColorButton>
                                                                    <dx:ToolbarCutButton AdaptivePriority="2">
                                                                    </dx:ToolbarCutButton>
                                                                    <dx:ToolbarCopyButton AdaptivePriority="2">
                                                                    </dx:ToolbarCopyButton>
                                                                    <dx:ToolbarPasteButton AdaptivePriority="2">
                                                                    </dx:ToolbarPasteButton>
                                                                    <dx:ToolbarPasteFromWordButton AdaptivePriority="2">
                                                                    </dx:ToolbarPasteFromWordButton>
                                                                    <dx:ToolbarUndoButton AdaptivePriority="1" BeginGroup="True">
                                                                    </dx:ToolbarUndoButton>
                                                                    <dx:ToolbarRedoButton AdaptivePriority="1">
                                                                    </dx:ToolbarRedoButton>
                                                                    <dx:ToolbarRemoveFormatButton AdaptivePriority="2" BeginGroup="True">
                                                                    </dx:ToolbarRemoveFormatButton>
                                                                    <dx:ToolbarSuperscriptButton AdaptivePriority="1" BeginGroup="True" Visible="False">
                                                                    </dx:ToolbarSuperscriptButton>
                                                                    <dx:ToolbarSubscriptButton AdaptivePriority="1" Visible="False">
                                                                    </dx:ToolbarSubscriptButton>
                                                                    <dx:ToolbarInsertOrderedListButton AdaptivePriority="1" BeginGroup="True">
                                                                    </dx:ToolbarInsertOrderedListButton>
                                                                    <dx:ToolbarInsertUnorderedListButton AdaptivePriority="1">
                                                                    </dx:ToolbarInsertUnorderedListButton>
                                                                    <dx:ToolbarIndentButton AdaptivePriority="2" BeginGroup="True">
                                                                    </dx:ToolbarIndentButton>
                                                                    <dx:ToolbarOutdentButton AdaptivePriority="2">
                                                                    </dx:ToolbarOutdentButton>
                                                                    <dx:ToolbarInsertLinkDialogButton AdaptivePriority="1" BeginGroup="True" Visible="False">
                                                                    </dx:ToolbarInsertLinkDialogButton>
                                                                    <dx:ToolbarUnlinkButton AdaptivePriority="1" Visible="False">
                                                                    </dx:ToolbarUnlinkButton>
                                                                    <dx:ToolbarInsertImageDialogButton AdaptivePriority="1" Visible="False">
                                                                    </dx:ToolbarInsertImageDialogButton>
                                                                    <dx:ToolbarTableOperationsDropDownButton AdaptivePriority="2" BeginGroup="True">
                                                                        <Items>
                                                                            <dx:ToolbarInsertTableDialogButton BeginGroup="True" Text="Insert Table..." ToolTip="Insert Table...">
                                                                            </dx:ToolbarInsertTableDialogButton>
                                                                            <dx:ToolbarTablePropertiesDialogButton BeginGroup="True">
                                                                            </dx:ToolbarTablePropertiesDialogButton>
                                                                            <dx:ToolbarTableRowPropertiesDialogButton>
                                                                            </dx:ToolbarTableRowPropertiesDialogButton>
                                                                            <dx:ToolbarTableColumnPropertiesDialogButton>
                                                                            </dx:ToolbarTableColumnPropertiesDialogButton>
                                                                            <dx:ToolbarTableCellPropertiesDialogButton>
                                                                            </dx:ToolbarTableCellPropertiesDialogButton>
                                                                            <dx:ToolbarInsertTableRowAboveButton BeginGroup="True">
                                                                            </dx:ToolbarInsertTableRowAboveButton>
                                                                            <dx:ToolbarInsertTableRowBelowButton>
                                                                            </dx:ToolbarInsertTableRowBelowButton>
                                                                            <dx:ToolbarInsertTableColumnToLeftButton>
                                                                            </dx:ToolbarInsertTableColumnToLeftButton>
                                                                            <dx:ToolbarInsertTableColumnToRightButton>
                                                                            </dx:ToolbarInsertTableColumnToRightButton>
                                                                            <dx:ToolbarSplitTableCellHorizontallyButton BeginGroup="True">
                                                                            </dx:ToolbarSplitTableCellHorizontallyButton>
                                                                            <dx:ToolbarSplitTableCellVerticallyButton>
                                                                            </dx:ToolbarSplitTableCellVerticallyButton>
                                                                            <dx:ToolbarMergeTableCellRightButton>
                                                                            </dx:ToolbarMergeTableCellRightButton>
                                                                            <dx:ToolbarMergeTableCellDownButton>
                                                                            </dx:ToolbarMergeTableCellDownButton>
                                                                            <dx:ToolbarDeleteTableButton BeginGroup="True">
                                                                            </dx:ToolbarDeleteTableButton>
                                                                            <dx:ToolbarDeleteTableRowButton>
                                                                            </dx:ToolbarDeleteTableRowButton>
                                                                            <dx:ToolbarDeleteTableColumnButton>
                                                                            </dx:ToolbarDeleteTableColumnButton>
                                                                        </Items>
                                                                    </dx:ToolbarTableOperationsDropDownButton>
                                                                    <dx:ToolbarFindAndReplaceDialogButton AdaptivePriority="2" BeginGroup="True">
                                                                    </dx:ToolbarFindAndReplaceDialogButton>
                                                                    <dx:ToolbarFullscreenButton AdaptivePriority="1" BeginGroup="True">
                                                                    </dx:ToolbarFullscreenButton>
                                                                </Items>
                                                            </dx:HtmlEditorToolbar>
                                                        </Toolbars>
                                                        <Settings AllowHtmlView="False">
                                                        </Settings>
                                                        <SettingsSpellChecker Culture="(Default)">
                                                        </SettingsSpellChecker>
                                                    </dx:ASPxHtmlEditor>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                            <br />
                                        </div>
                                        <div id="divJR_Description" runat="server" visible="false" class="literalStyle">
                                            <asp:Literal ID="ltrJR_Description" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="JrHistoryTab" class="tab-pane fade">
                        <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12 top padding-none">
                            <table id="tblJR_ActionHistory" class="datatable_cust tblJR_ActionHistoryClass display breakword" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>JR_HistoryID</th>
                                        <th>Action</th>
                                        <th>Action By</th>
                                        <th>Role</th>
                                        <th>Action Date</th>
                                        <th>Remarks</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12 top bottom">
                <asp:UpdatePanel ID="upFormActionBtns" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="pull-right">
                            <div id="divHodCreation" runat="server" visible="false">
                                <asp:Button ID="btnHodAssignJR" runat="server" Text="Submit" CssClass=" btn-signup_popup" TabIndex="7" OnClientClick="javascript:return SubmitMandatoryFields('Assign');" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass=" btn-revert_popup" OnClientClick="ResetAssignJRtoEmpPage();" TabIndex="8" />
                            </div>
                            <div id="divHodModifyJR" runat="server" visible="false">
                                <asp:Button ID="btnHodModifyJR" runat="server" Text="Update" CssClass=" btn-signup_popup"
                                    TabIndex="9" OnClientClick="javascript:return SubmitMandatoryFields('Modify');" />
                                <asp:Button ID="btnHodCancel3" runat="server" Text="Cancel" CssClass=" btn-cancel_popup" OnClientClick="RedirectToPendingList();" TabIndex="10" />
                            </div>
                            <div id="divQaModifyJR" runat="server" visible="false">
                                <asp:Button ID="btnQaApprovedJR" runat="server" Text="Approve" CssClass=" btn-signup_popup" TabIndex="11" OnClientClick="SubmitTheJR('Approve');" />
                                <asp:Button ID="btnQaRevertedJR" runat="server" Text="Revert" CssClass=" btn-revert_popup" TabIndex="12" OnClientClick="RevertMandatoryFields();" />
                                <asp:Button ID="btnQaCancel2" runat="server" Text="Cancel" Visible="false" CssClass=" btn-cancel_popup" TabIndex="13" OnClientClick="RedirectToReviewList();" />
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:Label ID="lblMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
            </div>
        </div>
    </div>

    <div id="modalJrContentChangeConfirm" class="modal confirmation_popup ModalConfirm" role="dialog">
        <asp:UpdatePanel ID="upJrContentChangeConfirm" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content modalMainContent">
                        <div class="modal-header ModalHeaderPart">
                            <div class="success_icon float-left">
                                <h4 class="modal-title float-left popup_title">Confirmation</h4>
                            </div>
                        </div>
                        <div class="modal-body" style="padding: 0px">
                            <div class="col-lg-12 padding-none ">
                                <p id="msgConfirm" class="col-lg-12 " style="padding: 8px; padding: 12px; max-height: 128px; overflow-x: auto;">JR Description will be lost if you change the Template, Do you want to continue?</p>
                            </div>
                        </div>
                        <div class="modal-footer ModalFooterPart">
                            <div class="confirmDialog" id="cnf">
                                <asp:UpdatePanel ID="upBtnContentChangeConfirm" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Button ID="btnCnfYes" runat="server" class="btn-signup_popup" Style="margin-top: 10px !important" Text="Yes" OnClientClick="ContentChangeConfirm('Y');" />
                                        <asp:Button ID="btnCnfNO" runat="server" Style="margin-left: 1px; margin-top: 10px !important" class="btn-cancel_popup" Text="No" OnClientClick="ContentChangeConfirm('N');" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <asp:Button ID="btnddlJrListSubmit" runat="server" OnClientClick="ContentChangeConfirm('Y');" Text="Button" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <%--added for electronic Sign--%>
    <uc1:ElectronicSign runat="server" ID="ElectronicSign" />
    <div id="jrDescriptionBody" style="display: none"></div>
    <!--JR Content Change Confirmation-->
    <script>
        function ContentChangeConfirm(AcceptChange) {
            if (AcceptChange == "Y") {
                $('#<%=hdnJrTemplateChangeConfirm.ClientID%>').val("Yes");
            }
            else {
                $('#<%=hdnJrTemplateChangeConfirm.ClientID%>').val("No");
            }
            $('#modalJrContentChangeConfirm').modal('hide');
        }
    </script>

    <script>
        function openElectronicSignModal() {
            ShowPleaseWait('hide');
            openUC_ElectronicSign();
        }
    </script>

    <script>
        function OnJR_TemplateChange() {
            <%--  var Description = document.getElementById('<%=txtJR.ClientID%>').value.trim();--%>

            var Description = txtJrDescription.GetHtml();
            if (Description == "") {
                $("#<%=btnddlJrListSubmit.ClientID%>").click();
            }
            else {
                $('#modalJrContentChangeConfirm').modal({ backdrop: 'static', keyboard: false });
            }
        }
    </script>

    <!--Navigations-->
    <script>            
        function RedirectToPendingList() {
            window.open("<%=ResolveUrl("~/TMS/JR/JR_PendingList.aspx?FilterType=2")%>", "_self");
        }
        function OpenRevertedJRs() {
            window.open("<%=ResolveUrl("~/TMS/JR/JR_PendingList.aspx?FilterType=2")%>", "_self");
        }
        function OpenApproveJRs() {
            window.open("<%=ResolveUrl("~/TMS/JR/JR_ReviewList.aspx?FilterType=2")%>", "_self");
        }
        function ResetAssignJRtoEmpPage() {
            window.open("<%=ResolveUrl("~/TMS/JR/AssignJRtoEmp.aspx?Create=Y")%>", "_self");
        }
        function CloseESign() {
            $('#usES_Modal').modal('hide');
        }
    </script>

    <!--JR Validation-->
    <script>
        function SubmitMandatoryFields(Type) {
            <%--   var Description = document.getElementById('<%=txtJR.ClientID%>').value;--%>
            $('#jrDescriptionBody').html(txtJrDescription.GetHtml());
            var Description = $('#jrDescriptionBody').text();
            //var Description = txtJrDescription.GetHtml().replace(/&nbsp;/gi, "");
            var JR_Name = document.getElementById('<%=txtJR_Name.ClientID%>').value.trim();
            errors = [];
            if (document.getElementById('<%=ddlDepartments.ClientID%>').value == 0) {
                errors.push("Select Department");
            }
            if (document.getElementById('<%=ddlEmployee.ClientID%>').value == 0) {
                errors.push("Select Trainee");
            }
            if (document.getElementById('<%=ddlReviewer.ClientID%>').value == 0) {
                errors.push("Select Reviewer");
            }
            if (JR_Name == '') {
                document.getElementById('<%=txtJR_Name.ClientID%>').focus();
                errors.push("Enter JR Name");
            }
            if (Description.trim() == "") {
                errors.push("Enter JR Description");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                ShowPleaseWait('show');
                SubmitTheJR(Type);
                return true;
            }
        }
        function RevertMandatoryFields() {
            var RevertReason = $("#<%=txtQaRemarks.ClientID%>").val().trim();
            if (RevertReason == '') {
                custAlertMsg("Enter Revert Reason in Reviewer Comments.", "error", "CursorInDeclineComments();");
                return false;
            }
            else {
                SubmitTheJR('Revert');
                return true;
            }
        }
        function CursorInDeclineComments() {
            $("#<%=txtQaRemarks.ClientID%>").focus();
        }
    </script>

    <script>
        function SubmitTheJR(Action) {
            $("#<%=hdnAction.ClientID%>").val(Action);
            openElectronicSignModal();
        }
    </script>

    <%--  <!--HTML Editor-->
    <script>
        $(document).ready(function () {
            $('.JR_Content').richText({

                // text formatting
                bold: true,
                italic: true,
                underline: true,

                // text alignment
                leftAlign: true,
                centerAlign: true,
                rightAlign: false,

                // lists
                ol: true,
                ul: false,

                // title
                heading: true,

                // colors
                fontColor: false,

                // uploads
                imageUpload: false,
                fileUpload: false,
                videoUpload: false,
                // link
                urls: false,

                // tables
                table: false,

                // code
                removeStyles: true,
                code: false,

                // colors
                colors: [],

                // dropdowns
                fileHTML: '',
                imageHTML: '',
            });
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(document).ready(function () {
                $('.JR_Content').richText({

                    // text formatting
                    bold: true,
                    italic: true,
                    underline: true,

                    // text alignment
                    leftAlign: true,
                    centerAlign: true,
                    rightAlign: false,

                    // lists
                    ol: true,
                    ul: false,

                    // title
                    heading: true,

                    // colors
                    fontColor: false,

                    // uploads
                    imageUpload: false,
                    fileUpload: false,
                    videoUpload: false,
                    // link
                    urls: false,

                    // tables
                    table: false,

                    // code
                    removeStyles: true,
                    code: false,

                    // colors
                    colors: [],


                    // dropdowns
                    fileHTML: '',
                    imageHTML: '',
                });
            });
        });
    </script>--%>

    <!--JR_Action  History jQuery DataTable-->
    <script>
        var oTableJR_ActionHistory;
        function loadJr_ActionHistory() {
            if (oTableJR_ActionHistory != null) {
                oTableJR_ActionHistory.destroy();
            }
            oTableJR_ActionHistory = $('#tblJR_ActionHistory').DataTable({
                columns: [
                    //{ 'data': 'JR_HistoryID' },//0
                    { 'data': 'ActionHistoryID' },//0
                    { 'data': 'ActionStatus' },//1
                    { 'data': 'ActionBy' },//2
                    { 'data': 'ActionRole' },//3
                    { 'data': 'ActionDate' },//4
                    { 'data': 'Remarks' }//5
                ],
                "order": [[0]],
                "scrollY": "250px",
                "aoColumnDefs": [{ "targets": [0], "visible": false }, { className: "textAlignLeft", "targets": [1, 2, 3, 5] },{"orderable": false, "targets": [1,2,3,4,5]}],
               <%-- "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/JR_ActionHistoryService.asmx/GetJR_ActionHistory")%>",--%>
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TMS_ActionHistoryService.asmx/GetTMS_ActionHistory")%>",
                "fnServerData": function (sSource, aoData, fnCallback) {
                 <%-- aoData.push({ "name": "JR_ID", "value": <%=hdnJrID.Value%> });--%>
                   aoData.push({ "name": "Base_ID", "value": <%=hdnJrID.Value%> },{ "name": "Table_ID", "value": "2"});
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            UserSessionCheck();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        }
        var elements = $('#tblJR_ActionHistory').find(':search,:text,:radio,:checkbox,select,textarea').filter(function () {
            return !this.readOnly &&
                !this.disabled &&
                $(this).parentsUntil('form', 'div').css('display') != "none";
        });
        elements.focus().select();

    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            // Fix up GridView to support THEAD tags            
            fixGvHeader();

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fixGvHeader();
        });

        function fixGvHeader() {
            var tbl = document.getElementsByClassName("fixHeadJR_Commentry");
            // Fix up GridView to support THEAD tags     
            if ($(".fixHeadJR_Commentry").find("thead").length == 0) {
                $(".fixHeadJR_Commentry tbody").before("<thead><tr></tr></thead>");
                $(".fixHeadJR_Commentry thead tr").append($(".fixHeadJR_Commentry th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixHeadJR_Commentry tbody tr:first").remove();
                }
            }
        }
    </script>

    <script>
        function RedirectToPendingList() {
            window.open("<%=ResolveUrl("~/TMS/JR/JR_PendingList.aspx?FilterType=2")%>", "_self");
        }

        function RedirectToReviewList() {
            window.open("<%=ResolveUrl("~/TMS/JR/JR_ReviewList.aspx?FilterType=2")%>", "_self");
        }
    </script>

    <script>
        $(document).ready(function () {
            $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
                var anchor = $(e.target).attr('href');
                if (anchor == "#JrHistoryTab") {
                    loadJr_ActionHistory();
                }
            })
        });
    </script>
</asp:Content>
