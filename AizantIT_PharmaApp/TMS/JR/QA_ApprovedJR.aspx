﻿<%@ Page Title="QA Approved JR" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="QA_ApprovedJR.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.JR.QA_ApprovedJR1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12  TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div class="panel panel-default" id="mainContainer" runat="server" >
                        <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 ">Assign TS</div>
            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 top">
                <asp:GridView ID="GVAssignTS"
                    runat="server" AutoGenerateColumns="False" CssClass="table table-fixed fixHeadAssignTS AspGrid"
                    EmptyDataText="No Records to Assign Training Schedule" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                    <AlternatingRowStyle BackColor="White" />
                    <RowStyle CssClass="col-12 padding-none" />
                    <Columns>
                        <asp:TemplateField HeaderText="JR ID" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblJR_ID" runat="server" Text='<%# Bind("JR_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Employee ID" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblEMPID" runat="server" Text='<%# Bind("EmpID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="JR Name" HeaderStyle-CssClass="col-1" ItemStyle-CssClass="col-1">
                            <ItemTemplate>
                                <asp:Label ID="lblJrName" runat="server" Text='<%# Bind("JR_Name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="JR Version" SortExpression="JR_Version" HeaderStyle-CssClass="col-1" ItemStyle-CssClass="col-1">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtJR_Version" runat="server" Text='<%# Bind("JR_Version") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblJR_Version" runat="server" Text='<%# Bind("JR_Version") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Emp Code" HeaderStyle-CssClass="col-1" ItemStyle-CssClass="col-1">
                            <EditItemTemplate>
                                <asp:Label ID="lblEditEmpCode" runat="server" Text='<%# Bind("EmpCode") %>'></asp:Label>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblEmpCode" runat="server" Text='<%# Bind("EmpCode") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Trainee Name" DataField="EmpName" ReadOnly="true" HeaderStyle-CssClass="col-1" ItemStyle-CssClass="col-1" />
                        <asp:TemplateField HeaderText="Department" HeaderStyle-CssClass="text-center" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblDepartment" runat="server" Text='<%# Bind("DeptID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dept Code" SortExpression="DeptCode" HeaderStyle-CssClass="col-1" ItemStyle-CssClass="col-1">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDeptCode" runat="server" Text='<%# Bind("DeptCode") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblDeptCode" runat="server" Text='<%# Bind("DeptCode") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Designation" HeaderStyle-CssClass="col-2" ItemStyle-CssClass="col-2">
                            <ItemTemplate>
                                <asp:Label ID="lblDesignationName" runat="server" Text='<%# Bind("DesignationName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Assigned By" HeaderStyle-CssClass="col-1" ItemStyle-CssClass="col-1">
                            <ItemTemplate>
                                <asp:Label ID="lblAssignedBy" runat="server" Text='<%# Bind("AssignedBy") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Approved By" HeaderStyle-CssClass="col-1" ItemStyle-CssClass="col-1">
                            <ItemTemplate>
                                <asp:Label ID="lblApprovedBy" runat="server" Text='<%# Bind("ApprovedBy") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Assigned Date" SortExpression="AssignedDate" HeaderStyle-CssClass="col-1" ItemStyle-CssClass="col-1">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtHODAssignedDate" runat="server" Text='<%# Bind("HODAssignedDate") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblHODAssignedDate" runat="server" Text='<%# Bind("HODAssignedDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Approved Date" SortExpression="ApprovedDate" HeaderStyle-CssClass="col-1" ItemStyle-CssClass="col-1">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtQAApprovedDate" runat="server" Text='<%# Bind("QAApprovedDate") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblQAApprovedDate" runat="server" Text='<%# Bind("QAApprovedDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Assign TS" HeaderStyle-CssClass="col-1" ItemStyle-CssClass="col-1">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkAssignTS" runat="server" CommandArgument='<%# Eval("EmpID") %>' OnClick="lnkAssignTS_Click" Font-Underline="true" Font-Bold="true">
                                    <asp:Image ID="Image1" Width="20px" ImageUrl="~/Images/TMS_Images/create_icon.png" runat="server" />
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>

                </asp:GridView>
            </div>
        </div>
    </div>

     <script type="text/javascript">
        $(document).ready(function () {
            // Fix up GridView to support THEAD tags            
            fixGvHeader();

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fixGvHeader();
        });

         function fixGvHeader() {
             var tbl = document.getElementsByClassName("fixHeadAssignTS");  
            // Fix up GridView to support THEAD tags     
            if ($(".fixHeadAssignTS").find("thead").length == 0) {
                $(".fixHeadAssignTS tbody").before("<thead><tr></tr></thead>");
                $(".fixHeadAssignTS thead tr").append($(".fixHeadAssignTS th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixHeadAssignTS tbody tr:first").remove();
                }               
            }
        }
  </script>

</asp:Content>
