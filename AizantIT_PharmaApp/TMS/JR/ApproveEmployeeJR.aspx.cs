﻿using System;
using System.Web.UI.WebControls;
using System.Data;
using AizantIT_PharmaApp.Common;
using TMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.JR
{
    public partial class ApproveEmployeeJR1 : System.Web.UI.Page
    {
        TMS_BAL objTMS_BAL;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {

                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID="+(int)Modules.TMS);
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID="+(int)TMS_UserRole.QA_TMS).Length > 0)
                            {
                                BindGrid();
                            }
                            else
                            {
                                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                                divAutorizedMsg.Visible = true;
                                mainContainer.Visible = false;
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AEJ-1:" + strline + "  " + strMsg, "error");
            }

        }
        public void BindGrid()
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                DataTable dt = new DataTable();
                dt = objTMS_BAL.GetJRsToAccept(Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]),
                    (int)Aizant_Enums.AizantEnums.JR_Status.JR_Accepted_By_Trainee);
                gvEmployeeJR.DataSource = dt;
                gvEmployeeJR.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AEJ-2:" + strline + "  " + strMsg, "error");
            }
        }
        protected void gvEmployeeJR_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                if (e.CommandName == "ApproveButton")
                {
                    int JRID = Convert.ToInt32(e.CommandArgument);
                    Response.Redirect("~/TMS/JR/AssignJRtoEmp.aspx?JR_ID=" + JRID + "&QA=Y", false);
                }
            }

            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AEJ-3:" + strline + "  " + strMsg, "error");
            }
        }
    }
}