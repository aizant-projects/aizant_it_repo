﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using AizantIT_PharmaApp.Common;
using TMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.JR
{
    public partial class RevertedJR : System.Web.UI.Page
    {
        TMS_BAL objTMS_Bal;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Cache.Get("CacheEmpInfo") != null && Request.Cookies.Get("CookieEmpID")!=null)
                {
                    if (!IsPostBack)
                    {

                        DataTable dtx = (Cache.Get("CacheEmpInfo") as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID="+(int)Modules.TMS);
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();

                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0 || dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainer_TMS).Length > 0)
                            {
                                ViewState["Role"] = 3;
                                InitializeThePage();
                            }
                            else
                            {
                                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                                divAutorizedMsg.Visible = true;
                                mainContainer.Visible = false;
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);

                        }
                    }
                    else
                    {
                        
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);


                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                // HelpClass.showMsg(this, this.GetType(), "QAPE1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "QA_RJ-1:" + strline + "  " + strMsg, "error");
            }
        }
        private void InitializeThePage()
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                //DataTable dt= objTMS_Bal.getQA_DeclinedEmpJRs((Session["UserSignIn"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                DataTable dt = objTMS_Bal.GetJRsToAccept(Convert.ToInt32(Request.Cookies.Get("CookieEmpID").Value), ((int)Aizant_Enums.AizantEnums.JR_Status.JR_Delined_By_QA));
                gvEmpJrDeclinedList.DataSource = dt;
                gvEmpJrDeclinedList.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                // HelpClass.showMsg(this, this.GetType(), "QAPE2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "QA_RJ-2:" + strline + "  " + strMsg, "error");
            }
        }
        protected void gvEmpJrDeclinedList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "JrEdit")
                {
                    int rowIndex = int.Parse(e.CommandArgument.ToString());
                    string val = gvEmpJrDeclinedList.DataKeys[rowIndex]["JR_ID"].ToString();
                    Response.Redirect("~/TMS/JR/AssignJRtoEmp.aspx?JR_ID=" + val);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                //HelpClass.showMsg(this, this.GetType(), "QAJR2:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "QA_RJ-3:" + strline + "  " + strMsg, "error");
            }
        }
    }
}