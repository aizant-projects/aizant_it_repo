﻿<%@ Page Title="Reverted JR" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="RevertedJR.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.JR.RevertedJR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 dms_outer_border">
        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div class="panel panel-default" id="mainContainer" runat="server">
             <div class="grid_header col-lg-12  col-xs-12 col-md-12 col-sm-12 "> <asp:Label ID="Label1" runat="server" Font-Bold="true">QA Reverted JR</asp:Label></div>
            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 top">
                <asp:UpdatePanel ID="upGvEmpJrDeclinedList" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:GridView ID="gvEmpJrDeclinedList" OnRowCommand="gvEmpJrDeclinedList_RowCommand" DataKeyNames="JR_ID"
                            EmptyDataText="No Records in Reverted JR"
                            runat="server" AutoGenerateColumns="False" CssClass="table table-fixed fixHeadEmpJrDeclinedList AspGrid"
                            EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                            <AlternatingRowStyle BackColor="White" />
                            <RowStyle CssClass="col-xs-12 padding-none" />
                            <Columns>
                                <asp:TemplateField HeaderText="JR ID" Visible="false">
                                    <EditItemTemplate>
                                        <asp:Label ID="lblEditJrID" runat="server" Text='<%# Bind("JR_ID") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblJrID" runat="server" Text='<%# Bind("JR_ID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="EmpID" Visible="false">
                                    <EditItemTemplate>
                                        <asp:Label ID="lblEditEmpID" runat="server" Text='<%# Bind("EmpID") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmpID" runat="server" Text='<%# Bind("EmpID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="JR Name" HeaderStyle-CssClass="col-xs-2" ItemStyle-CssClass="col-xs-2">
                                    <EditItemTemplate>
                                        <asp:Label ID="lblEditJrName" runat="server" Text='<%# Bind("JR_Name") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblJrName" runat="server" Text='<%# Bind("JR_Name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="JR Version" SortExpression="JR_Version" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtJR_Version" runat="server" Text='<%# Bind("JR_Version") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblJR_Version" runat="server" Text='<%# Bind("JR_Version") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Emp Code" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                    <EditItemTemplate>
                                        <asp:Label ID="lblEditEmpCode" runat="server" Text='<%# Bind("EmpCode") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmpCode" runat="server" Text='<%# Bind("EmpCode") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Trainee Name" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                    <EditItemTemplate>
                                        <asp:Label ID="lblEditEmpName" runat="server" Text='<%# Bind("EmpName") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("EmpName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Accepted Date" SortExpression="AcceptedDate" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtTraineeAcceptedDate" runat="server" Text='<%# Bind("TraineeAcceptedDate") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblTraineeAcceptedDate" runat="server" Text='<%# Bind("TraineeAcceptedDate") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:TemplateField HeaderText="Department">
                                <EditItemTemplate>
                                    <asp:Label ID="lblEditDepartmentName" runat="server" Text='<%# Bind("DepartmentName") %>'></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblDepartmentName" runat="server" Text='<%# Bind("DepartmentName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="Dept Code" SortExpression="DeptCode" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtDeptCode" runat="server" Text='<%# Bind("DeptCode") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDeptCode" runat="server" Text='<%# Bind("DeptCode") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Designation" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                    <EditItemTemplate>
                                        <asp:Label ID="lblEditDesignationName" runat="server" Text='<%# Bind("DesignationName") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDesignationName" runat="server" Text='<%# Bind("DesignationName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%-- <asp:TemplateField HeaderText="Assigned By">
                                <EditItemTemplate>
                                    <asp:Label ID="lblEditAssignedBy" runat="server" Text='<%# Bind("AssignedBy") %>'></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblAssignedBy" runat="server" Text='<%# Bind("AssignedBy") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="Assigned Date" SortExpression="AssignedDate" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtHODAssignedDate" runat="server" Text='<%# Bind("HODAssignedDate") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblHODAssignedDate" runat="server" Text='<%# Bind("HODAssignedDate") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Reverted By" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                    <EditItemTemplate>
                                        <asp:Label ID="lblEditDeclinedBy" runat="server" Text='<%# Bind("DeclinedBy") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDeclinedBy" runat="server" Text='<%# Bind("DeclinedBy") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Reverted Date" SortExpression="RevertedDate" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtQADeclinedDate" runat="server" Text='<%# Bind("QARevertedDate") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblQADeclinedDate" runat="server" Text='<%# Bind("QARevertedDate") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Edit" HeaderStyle-CssClass="col-xs-1" ItemStyle-CssClass="col-xs-1">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnGvEdit" title="Click to Edit" CssClass="glyphicon glyphicon-edit GV_Edit_Icon" runat="server" CommandName="JrEdit" CommandArgument="<%# Container.DataItemIndex %>"></asp:LinkButton>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

     <script type="text/javascript">
        $(document).ready(function () {
            // Fix up GridView to support THEAD tags            
            fixGvHeader();

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fixGvHeader();
        });

         function fixGvHeader() {
             var tbl = document.getElementsByClassName("fixHeadEmpJrDeclinedList");  
            // Fix up GridView to support THEAD tags     
            if ($(".fixHeadEmpJrDeclinedList").find("thead").length == 0) {
                $(".fixHeadEmpJrDeclinedList tbody").before("<thead><tr></tr></thead>");
                $(".fixHeadEmpJrDeclinedList thead tr").append($(".fixHeadEmpJrDeclinedList th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixHeadEmpJrDeclinedList tbody tr:first").remove();
                }               
            }
        }
  </script>

</asp:Content>
