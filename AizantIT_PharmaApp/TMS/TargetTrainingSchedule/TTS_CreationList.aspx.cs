﻿using AizantIT_PharmaApp.Common;
using System;
using System.Collections.Generic;
using System.Data;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.TargetTrainingSchedule
{
    public partial class TTS_CreationList : System.Web.UI.Page
    {
        UMS_BAL objUMS_BAL = new UMS_BAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=" + (int)Modules.TMS);
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();

                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0)
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                                divAutorizedMsg.Visible = true;
                                divMainContainer.Visible = false;
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTSList-1:" + strline + "  " + strMsg, "error");
            }
        }
        private void InitializeThePage()
        {
            try
            {
               
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTSList-2:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnViewTTS_CreationlList_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<string,int> dict = new Dictionary<string,int>();

                dict.Add(hdnDepartmentName.Value, Convert.ToInt32(hdnDeptID.Value));
                dict.Add(hdnDocType.Value,Convert.ToInt32(hdnDocumentTypeID.Value));
                dict.Add(hdnTargetType.Value,Convert.ToInt32(hdnTargetTypeID.Value));
                dict.Add(hdnDocument.Value,Convert.ToInt32(hdnDocumentID.Value));
                Session["PreviousPage"] = dict;
                Response.Redirect("~/TMS/TargetTrainingSchedule/TTS_Master.aspx?TTS_Type=CreateList", false);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTSList-3:" + strline + "  " + strMsg, "error");
            }

        }

        protected void lbTTSList_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["PreviousPage"] != null)
                {
                    Session.Remove("PreviousPage");
                }
                Response.Redirect("~/TMS/TargetTrainingSchedule/TTS_Master.aspx?TTS_Type=Create");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTSList-4:" + strline + "  " + strMsg, "error");
            }
        }
    }
}
