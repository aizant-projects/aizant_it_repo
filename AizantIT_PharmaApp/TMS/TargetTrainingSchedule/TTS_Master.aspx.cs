﻿using AizantIT_PharmaApp.Common;
using AizantIT_PharmaApp.UserControls.TMS_UserControls;
using API_BusinessLayer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BusinessLayer;
using TMS_BusinessObjects;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.TargetTrainingSchedule
{
    public partial class TTS_Master : System.Web.UI.Page
    {
        UMS_BAL objUMS_BAL;
        TMS_BAL objTMS_BAL;
        API_BAL objAPI_BAL = new API_BAL();
        int AuthorID = 0, ReviewerID = 0;
        int SessionsCount = 0;
        bool IsTTS_DetailsInIt = false;
        public object Today { get; private set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ElectronicSign.buttonClick += new EventHandler(ElectronicSign_buttonClick);
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        if (!Request.UrlReferrer.ToString().Contains("TTS_Master.aspx"))
                        {
                            hdnPreviousPage.Value = Request.UrlReferrer.ToString();
                            Session["TMS_BackToList"] = Request.UrlReferrer.ToString();
                        }
                        else
                        {
                            hdnPreviousPage.Value = Session["TMS_BackToList"].ToString();
                        }
                        int EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        hdnEmpID.Value = EmpID.ToString();
                        ViewState["QAApproveOrRevert"] = string.Empty; 
                        hdnMinDate.Value = DateTime.Now.Date.ToString("dd MMM yyyy");
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drTmsRoles = dtx.Select("ModuleID=" + (int)Aizant_Enums.AizantEnums.Modules.TMS);
                        if (drTmsRoles.Length > 0)
                        {
                            dtTemp = drTmsRoles.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0 || dtTemp.Select("RoleID=" + (int)TMS_UserRole.QA_TMS).Length > 0 || dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainer_TMS).Length > 0)
                            {
                                if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0 || dtTemp.Select("RoleID=" + (int)TMS_UserRole.QA_TMS).Length > 0)
                                {
                                    hdnEmpIsHodOrQA.Value = "Y";
                                }
                                ddlTargetType.Enabled = false;
                                ddlTargetType.CssClass = "selectpicker regulatory_dropdown_style  form-control";
                                InitializeThePage();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        } 
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                strMsg = "TTS_M1:" + strline + "  " + strMsg;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "custAlertMsg('" + strMsg + "','error');", true);
            } 
        }

        #region BindData
        private void loadAuthorizeErrorMsg()
        {
            try
            {
                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                divAutorizedMsg.Visible = true;
                divMainContainer.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M2:" + strline + "  " + strMsg, "error");
            }
        }
        private void InitializeThePage()
        {
            try
            {
                if (Request.QueryString["Notification_ID"] != null)
                {
                    objUMS_BAL = new UMS_BAL();
                    //2 represents NotifyEmp had visited the page through notification link.
                    int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs);
                       
                }
                EnableDisableButtons();
                InitializeDataTables();

                
                if (Request.QueryString["TTS_Type"] != null)
                {
                   
                    btnBackToList.Visible = false;
                    Tab_TargetHistory.Visible = false;
                    divAuthorComments.Visible = true;
                    divAuthorComments.Attributes.Add("class", "col-12 float-left padding-none");
                    ddlReviewerTTS.Visible = true;
                    divHodActionBtns.Visible = true;
                    ApplyGridStylesToShow4Columns();
                    
                    if (Request.QueryString["TTS_Type"] == "Create")
                    {
                        if (Session["PreviousPage"] != null)
                        {
                            Session.Remove("PreviousPage");
                        }
                        bindDepartments(Convert.ToInt32(hdnEmpID.Value));
                        BindDocType();
                    }
                    else if(Request.QueryString["TTS_Type"] == "CreateList")
                    {
                        ddlDepartmentTTS.Enabled = false;
                        ddlDocumentType.Enabled = false;
                        ddlTrainingDocument.Enabled = false;
                        ddlTraineeDepartment.Enabled = true;
                        ddlRefresherMonth.Enabled = false;  
                        if (Session["PreviousPage"] != null)
                        {
                            Dictionary<string,int> dict = (Dictionary<string,int>)Session["PreviousPage"];
                            ddlDepartmentTTS.Items.Add(new ListItem(dict.ElementAt(0).Key, Convert.ToString(dict.ElementAt(0).Value)));
                            ddlDocumentType.Items.Add(new ListItem(Convert.ToString(dict.ElementAt(1).Key), Convert.ToString(dict.ElementAt(1).Value)));
                            ddlTargetType.ClearSelection();
                            ddlTargetType.Items.FindByValue((Convert.ToString(dict.ElementAt(2).Value))).Selected = true;
                            ddlTrainingDocument.Items.Add(new ListItem(Convert.ToString(dict.ElementAt(3).Key), Convert.ToString(dict.ElementAt(3).Value)));
                        }
                        if (ddlTargetType.SelectedValue == "1")
                        {
                            divMonth.Visible = true;
                            string month= DateTime.Now.Month.ToString();
                            ddlRefresherMonth.Items.Insert(0, new ListItem(HelpClass.GetMonthName(month), month));
                        }
                        upMonth.Update();
                        BindDropDowns(ddlDepartmentTTS.SelectedValue,hdnEmpID.Value);//To Load Author and Approver
                        BindTraineeDepartment(); //Trainee department
                        GetPreviousTraineesForRevisionDoc();//PreviousTraineesForRevisionDoc
                        ddlTargetType.Enabled = false;
                    }
                }
                else  // On Not Create
                {
                    if (Session["PreviousPage"] != null)
                    {
                        Hashtable ht = Session["PreviousPage"] as Hashtable;
                        hdnTTS_ID.Value = ht["TTS_ID"].ToString();

                        //    //Bind TTS Based On the Status

                        //    // PL - Pending List
                        //    if (ht["Previous_PageValue"].ToString() == "PL") //from TTS_pending.aspx page for HOD
                        //    {
                        //        hdnPreviousPage.Value = "PL";
                        //    }
                        //    if (ht["Previous_PageValue"].ToString() == "QA")// from TTS_ApprovedList.aspx page for Trainer
                        //    {
                        //        hdnPreviousPage.Value = "QA";
                        //    }
                        //    if (ht["Previous_PageValue"].ToString() == "AL")//from TTS_ApprovalList.aspx page for QA
                        //    {
                        //        hdnPreviousPage.Value = "AL";
                        //    }
                        //    if (ht["Previous_PageValue"].ToString() == "ML")//from TargetTS_List.aspx page for QA or HOD
                        //    {
                        //        hdnPreviousPage.Value = "ML";
                        //    }
                            GetTargetTS_Details(hdnTTS_ID.Value);
                    }
                    else
                    {
                        //code for Create click
                        btnBackToList.Visible = false;  //for Create page Back to list link button hide
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                strMsg = "TTS_M3:" + strline + "  " + strMsg;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "custAlertMsg('" + strMsg + "','error');", true);
            }
        }

        public void GetTargetTS_Details(string TTS_ID)
        {
            try
            {
                //code to load Target training details based on Status
                objTMS_BAL = new TMS_BAL();
                int CurrentHistoryStatus = 0, TargetCurrentStatus = 0;
                int EmpID = Convert.ToInt32(hdnEmpID.Value);
                DataSet dsTTS_Details = objTMS_BAL.GetTargetTrainingDetails(TTS_ID, Convert.ToInt32(hdnEmpID.Value),
                    out CurrentHistoryStatus, out TargetCurrentStatus);

                // Target TS Main Doc. Status
                hdnTTS_Status.Value = TargetCurrentStatus.ToString();

                // Target TS Current Status
                hdnTTS_CurrentStatus.Value = CurrentHistoryStatus.ToString();

                AuthorID = Convert.ToInt32(dsTTS_Details.Tables[0].Rows[0]["AuthorBy"]);
                ReviewerID = Convert.ToInt32(dsTTS_Details.Tables[0].Rows[0]["ReviewBy"]);

                txtReviewer.Value = dsTTS_Details.Tables[0].Rows[0]["ReviewerName"].ToString();
                txtAuthor.Value = dsTTS_Details.Tables[0].Rows[0]["AuthorName"].ToString();
                DateTime DueDate = (DateTime)dsTTS_Details.Tables[0].Rows[0]["DueDate"];
                txtTTSDueDate.Value = DueDate.ToString("dd MMM yyyy");
                string TargetType = dsTTS_Details.Tables[0].Rows[0]["TargetType"].ToString();
                ddlTargetType.Items.FindByValue(TargetType).Selected = true;
                if (TargetType == "4")
                {
                    if (objAPI_BAL.IsModuleIdExists((int)Modules.QMS))
                    {
                        #region QMS Incident Training Check
                        DataTable dt = objTMS_BAL.GetIncidentInGeneralBal(Convert.ToInt32(hdnTTS_ID.Value));
                        if (dt.Rows.Count > 0)
                        {
                            divIncident.Visible = true;
                            txtIncident.Value = dt.Rows[0]["IRNumber"].ToString();
                        }
                        #endregion
                    }
                }

                BindAssignedTrainers(dsTTS_Details.Tables[1]);
                // Check Whether logged in User is Trainer or not
                if (dsTTS_Details.Tables[1].Rows.Count > 0)
                {
                    DataTable dtAssignedTrainers = dsTTS_Details.Tables[1];
                    foreach (DataRow drTrainers in dtAssignedTrainers.Rows)
                    {
                        for (int x = 0; x < lbxTrainersTTS.Items.Count; x++)
                        {
                            if (lbxTrainersTTS.Items[x].Value == drTrainers["EmpID"].ToString())
                            {
                                lbxTrainersTTS.Items[x].Selected = true;
                                lbxTrainersTTS.Items[x].Attributes.Add("disabled", "disabled");
                                if (drTrainers["EmpID"].ToString() == hdnEmpID.Value)
                                {
                                    hdnIsAssignedTrainer.Value = "true";
                                }
                            }
                        }
                    }

                    //For HOD
                    if (EmpID == AuthorID & hdnEmpIsHodOrQA.Value == "Y")
                    {
                        divCloseTargetTraining.Visible = Convert.ToBoolean(dsTTS_Details.Tables[0].Rows[0]["AllowClose"]);
                        //  txtCloseComments.Visible= Convert.ToBoolean(dsTTS_Details.Tables[0].Rows[0]["AllowClose"]);

                        divApproverName.Visible = true;
                        txtReviewer.Visible = true;
                        // Represents Target TS Reverted By QA
                        if (CurrentHistoryStatus == 2)
                        {
                            txtReviewer.Visible = false;
                            ddlReviewerTTS.Visible = true;
                            objUMS_BAL = new UMS_BAL();
                            DataTable dtReviewer = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(dsTTS_Details.Tables[0].Rows[0]["DeptID"]), (int)TMS_UserRole.QA_TMS, true);
                            BindReviewer(AuthorID.ToString(), dtReviewer);
                            if (ddlReviewerTTS.Items.Contains(new ListItem(dsTTS_Details.Tables[0].Rows[0]["ReviewerName"].ToString(), ReviewerID.ToString())))
                            {
                                ddlReviewerTTS.Items.FindByValue(ReviewerID.ToString()).Selected = true;
                            }
                            btnHodSubmit.Text = "Update";
                            btnHodSubmit.Attributes.Add("title", "Update");
                            btnHodCancel.Visible = true;
                            divHodReset.Visible = false;
                            divHodActionBtns.Visible = true;
                            ddlTraineeDepartment.Enabled = true;
                            BindTTS_DetailsToEdit(dsTTS_Details);
                            divAuthorComments.Visible = divReviewerComments.Visible = true;
                            txtAuthorComments.Value = string.Empty;
                            txtReviewerComments.Disabled = true;
                            //Comments
                            if (dsTTS_Details.Tables[3].Rows.Count > 0)
                            {
                                txtReviewerComments.Value = dsTTS_Details.Tables[3].Rows[0]["ReviewerRecentComments"].ToString();
                            }
                        }
                        else
                        {
                            DisableDropDowns();
                            BindTTS_DetailsToView(dsTTS_Details);
                        }
                    }
                    //For QA
                    else if (EmpID == ReviewerID)
                    {
                        BindTTS_DetailsToView(dsTTS_Details);
                        DisableDropDowns();
                        txtAuthorComments.Disabled = true;
                        divAuthorName.Visible = true;
                        divApproverName.Visible = false;
                        if (CurrentHistoryStatus == 1 || CurrentHistoryStatus == 3)
                        {
                            divQaActions.Visible = true;
                            txtReviewerComments.Value = string.Empty;
                            divAuthorComments.Visible = divReviewerComments.Visible = true;
                            txtReviewerComments.Value = string.Empty;
                            txtAuthorComments.Disabled = true;
                            divAuthorName.Visible = true;
                            divApproverName.Visible = false;
                            txtTTSDueDate.Disabled = true;
                            //Comments
                            if (dsTTS_Details.Tables[3].Rows.Count > 0)
                            {
                                txtAuthorComments.Value = dsTTS_Details.Tables[3].Rows[0]["AuthorRecentComments"].ToString();
                            }
                        }
                        else
                        {
                            divAuthorComments.Visible = false;
                            divReviewerComments.Visible = false;
                            divAuthorName.Visible = true;
                            divApproverName.Visible = false;
                        }
                    }
                    else
                    {
                        divApproverName.Visible = true;
                        txtReviewer.Visible = true;
                        DisableDropDowns();
                        BindTTS_DetailsToView(dsTTS_Details);
                    }

                    // Represents Target TS Approved by QA
                    if (CurrentHistoryStatus >= 4)
                    {
                        if (EmpID != AuthorID && EmpID != ReviewerID)
                        {
                            BindTTS_DetailsToView(dsTTS_Details);
                        }
                        Tab_TrainingSession.Visible = true;
                        if (dsTTS_Details.Tables[0].Rows[0]["ModeOfTraining"].ToString() == "2")
                        {
                            divOnlineGrid.Visible = true;
                            //BindOnlineSessionGrid(dsTTS_Details.Tables[11]);
                            BindOnlineSessionGrid(TTS_ID);
                            hdnModeOfTraining.Value = "2";
                        }
                        else
                        {
                            BindDataForTrainingSession();
                            divClassroomActionBtns.Visible = true;
                            divClassRoomGrid.Visible = true;
                        }
                        divAuthorComments.Visible = divReviewerComments.Visible = false;
                        divAuthorName.Visible = txtReviewer.Visible = true;
                        if (hdnIsAssignedTrainer.Value == "true")
                        {
                            BindTTS_DetailsToView(dsTTS_Details);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                strMsg = "TTS_M4:" + strline + "  " + strMsg;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "custAlertMsg('" + strMsg + "','error');", true);
            }
        }
        private void BindTTS_DetailsToEdit(DataSet dsTTS_Details)
        {
            try
            {
                ApplyGridStylesToShow4Columns();
                // Bind MainDetails
                if (dsTTS_Details.Tables[0].Rows.Count > 0)
                {
                    BindAuthorAccessiableDept(dsTTS_Details.Tables[5]);
                    if (ddlDepartmentTTS.Items.Count > 0)
                    {
                        if (ddlDepartmentTTS.Items.Contains(new ListItem(dsTTS_Details.Tables[0].Rows[0]["Department"].ToString(),
                        dsTTS_Details.Tables[0].Rows[0]["DeptID"].ToString())))
                        {
                            ddlDepartmentTTS.ClearSelection();
                            ddlDepartmentTTS.Items.FindByValue(dsTTS_Details.Tables[0].Rows[0]["DeptID"].ToString()).Selected = true;
                        }
                    }
                    BindDocType(dsTTS_Details.Tables[6]);
                    if (ddlDocumentType.Items.Count > 0)
                    {
                        if (ddlDocumentType.Items.Contains(new ListItem(dsTTS_Details.Tables[0].Rows[0]["DocumentType"].ToString(),
                        dsTTS_Details.Tables[0].Rows[0]["DocumentTypeID"].ToString())))
                        {
                            ddlDocumentType.ClearSelection();
                            ddlDocumentType.Items.FindByValue(dsTTS_Details.Tables[0].Rows[0]["DocumentTypeID"].ToString()).Selected = true;
                        }
                    }

                    BindSelectAssignedTrainers(dsTTS_Details.Tables[7], dsTTS_Details.Tables[1]);
                    string TargetType = dsTTS_Details.Tables[0].Rows[0]["TargetType"].ToString();
                    ddlTargetType.ClearSelection();
                    ddlTargetType.Items.FindByValue(TargetType).Selected = true;
                    ddlTargetType.Enabled = true;
                    if (TargetType == "1")
                    {
                        LoadMonth();
                        divMonth.Visible = true;
                        string month = dsTTS_Details.Tables[0].Rows[0]["Month"].ToString();
                        if (ddlRefresherMonth.Items.Contains(new ListItem(HelpClass.GetMonthName(month), month)))
                        {
                            ddlRefresherMonth.ClearSelection();
                            ddlRefresherMonth.Items.FindByValue(month).Selected = true;
                        }
                        ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "showHideDivsByDocType('1','1');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "showHideDivsByDocType('1','0');", true);
                    }
                    if (dsTTS_Details.Tables[10].Rows.Count > 0)
                    {
                        BindDocuments(dsTTS_Details.Tables[10]);
                        if (ddlTrainingDocument.Items.Contains(new ListItem(dsTTS_Details.Tables[0].Rows[0]["Document"].ToString(),
                                                                                  dsTTS_Details.Tables[0].Rows[0]["DocumentID"].ToString())))
                        {
                            ddlTrainingDocument.ClearSelection();
                            ddlTrainingDocument.Items.FindByValue(dsTTS_Details.Tables[0].Rows[0]["DocumentID"].ToString()).Selected = true;
                        }
                    }
                    ddlTypeOfTraining.ClearSelection();
                    ddlTypeOfTraining.Items.FindByValue(dsTTS_Details.Tables[0].Rows[0]["TypeOfTraining"].ToString()).Selected = true;
                    txtStatusTTS.Value = dsTTS_Details.Tables[0].Rows[0]["CurrentStatus"].ToString();
                    DateTime DueDate = (DateTime)dsTTS_Details.Tables[0].Rows[0]["DueDate"];
                    txtTTSDueDate.Value = DueDate.ToString("dd MMM yyyy");
                    BindTraineeDepartments(dsTTS_Details.Tables[4]);
                    //Bind Trainees
                    if (dsTTS_Details.Tables[2].Rows.Count > 0)
                    {
                        DataTable dtAssignedTrainees = dsTTS_Details.Tables[2];
                        DataColumn dcStatus = new DataColumn("Status");
                        dtAssignedTrainees.Columns.Add(dcStatus);
                        foreach (DataRow dr in dtAssignedTrainees.Rows)
                        {
                            dr["Status"] = "N";
                        }
                        bindTrainees(dtAssignedTrainees, ddlTraineeDepartment.SelectedValue);
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                strMsg = "TTS_M5:" + strline + "  " + strMsg;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "custAlertMsg('" + strMsg + "','error');", true);
            }            
        }

        private void BindTTS_DetailsToView(DataSet dsTTS_Details)
        {
            try
            {
                if (!IsTTS_DetailsInIt)
                {
                    ApplyGridStylesToShow3Columns();
                    // Bind MainDetails
                    if (dsTTS_Details.Tables[0].Rows.Count > 0)
                    {
                        ddlDepartmentTTS.Items.Insert(0, new ListItem(dsTTS_Details.Tables[0].Rows[0]["Department"].ToString(),
                            dsTTS_Details.Tables[0].Rows[0]["DeptID"].ToString()));
                        ddlDocumentType.Items.Insert(0, new ListItem(dsTTS_Details.Tables[0].Rows[0]["DocumentType"].ToString(),
                            dsTTS_Details.Tables[0].Rows[0]["DocumentTypeID"].ToString()));

                        string TargetType = dsTTS_Details.Tables[0].Rows[0]["TargetType"].ToString();
                        ddlTargetType.ClearSelection();
                        ddlTargetType.Items.FindByValue(TargetType).Selected = true;
                        if (hdnTTS_Status.Value == "4")
                        {
                            btnNewTrainingSession.Visible = false;
                        }
                        if (TargetType == "1")
                        {
                            divMonth.Visible = true;
                            string month = dsTTS_Details.Tables[0].Rows[0]["Month"].ToString();
                            ddlRefresherMonth.Items.Insert(0, new ListItem(HelpClass.GetMonthName(month), month));
                            ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "showHideDivsByDocType('1','1');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "showHideDivsByDocType('1','0');", true);
                        }
                        ddlTrainingDocument.Items.Insert(0, new ListItem(dsTTS_Details.Tables[0].Rows[0]["Document"].ToString(),
                                                          dsTTS_Details.Tables[0].Rows[0]["DocumentID"].ToString()));
                        ddlTypeOfTraining.ClearSelection();
                        ddlTypeOfTraining.Items.FindByValue(dsTTS_Details.Tables[0].Rows[0]["TypeOfTraining"].ToString()).Selected = true;
                        txtStatusTTS.Value = dsTTS_Details.Tables[0].Rows[0]["CurrentStatus"].ToString();
                        DateTime DueDate = (DateTime)dsTTS_Details.Tables[0].Rows[0]["DueDate"];
                        txtTTSDueDate.Value = DueDate.ToString("dd MMM yyyy");
                        BindTraineeDepartments(dsTTS_Details.Tables[4]);
                        //Bind Trainees
                        if (dsTTS_Details.Tables[2].Rows.Count > 0)
                        {
                            DataTable dtAssignedTrainees = dsTTS_Details.Tables[2];
                            DataColumn dcStatus = new DataColumn("Status");
                            dtAssignedTrainees.Columns.Add(dcStatus);
                            foreach (DataRow dr in dtAssignedTrainees.Rows)
                            {
                                dr["Status"] = "N";
                            }
                            bindTrainees(dtAssignedTrainees, ddlTraineeDepartment.SelectedValue);
                        }
                    }
                    ddlDepartmentTTS.Enabled = ddlDocumentType.Enabled = ddlRefresherMonth.Enabled = ddlTrainingDocument.Enabled = ddlTypeOfTraining.Enabled = false;
                    txtTTSDueDate.Attributes.Add("disabled", "disabled");
                    IsTTS_DetailsInIt = true;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                strMsg = "TTS_M6:" + strline + "  " + strMsg;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "custAlertMsg('" + strMsg + "','error');", true);
            }
        }
        private void BindTraineeDepartments(DataTable dtTraineeDept)
        {
            //Trainee department           
            ddlTraineeDepartment.DataSource = dtTraineeDept;
            ddlTraineeDepartment.DataTextField = "DepartmentName";
            ddlTraineeDepartment.DataValueField = "DeptID";
            ddlTraineeDepartment.DataBind();
            ddlTraineeDepartment.Items.Insert(0, new ListItem("-- All Departments --", "0"));
        }

        private void BindSelectAssignedTrainers(DataTable dtAllTrainers, DataTable dtAssignedTrainers)
        {
            lbxTrainersTTS.DataSource = dtAllTrainers;
            lbxTrainersTTS.DataTextField = "EmpName";
            lbxTrainersTTS.DataValueField = "EmpID";
            lbxTrainersTTS.DataBind();
            for (int t = 0; t < lbxTrainersTTS.Items.Count; t++)
            {
                string TrainerID = lbxTrainersTTS.Items[t].Value;
                if (dtAssignedTrainers.Select("EmpID='" + TrainerID + "'").Length > 0)
                {
                    lbxTrainersTTS.Items[t].Selected = true;
                }
            }
        }

        public void BindAssignedTrainers(DataTable dtTrainers)
        {
            try
            {
                lbxTrainersTTS.DataSource = dtTrainers;
                lbxTrainersTTS.DataTextField = "EmpName";
                lbxTrainersTTS.DataValueField = "EmpID";
                lbxTrainersTTS.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M7:" + strline + "  " + strMsg, "error");
            }
        }

        private void ApplyGridStylesToShow4Columns()
        {
            gvTraineeSelector.Columns[0].Visible = true;
            gvTraineeSelected.Columns[0].Visible = true;
            gvTraineeSelector.Columns[0].HeaderStyle.CssClass = "col-3";
            gvTraineeSelector.Columns[0].ItemStyle.CssClass = "col-3";
            gvTraineeSelector.Columns[3].HeaderStyle.CssClass = "col-3";
            gvTraineeSelector.Columns[3].ItemStyle.CssClass = "col-3 textAlignLeft";
            gvTraineeSelector.Columns[4].HeaderStyle.CssClass = "col-3";
            gvTraineeSelector.Columns[4].ItemStyle.CssClass = "col-3";
            gvTraineeSelector.Columns[5].HeaderStyle.CssClass = "col-3";
            gvTraineeSelector.Columns[5].ItemStyle.CssClass = "col-3 textAlignLeft";

            gvTraineeSelected.Columns[0].HeaderStyle.CssClass = "col-3";
            gvTraineeSelected.Columns[0].ItemStyle.CssClass = "col-3";
            gvTraineeSelected.Columns[3].HeaderStyle.CssClass = "col-3";
            gvTraineeSelected.Columns[3].ItemStyle.CssClass = "col-3 textAlignLeft";
            gvTraineeSelected.Columns[4].HeaderStyle.CssClass = "col-3";
            gvTraineeSelected.Columns[4].ItemStyle.CssClass = "col-3";
            gvTraineeSelected.Columns[5].HeaderStyle.CssClass = "col-3";
            gvTraineeSelected.Columns[5].ItemStyle.CssClass = "col-3 textAlignLeft";
        }

        private void ApplyGridStylesToShow3Columns()
        {
            gvTraineeSelector.Columns[3].HeaderStyle.CssClass = "col-4";
            gvTraineeSelector.Columns[3].ItemStyle.CssClass = "col-4 textAlignLeft";
            gvTraineeSelector.Columns[4].HeaderStyle.CssClass = "col-4";
            gvTraineeSelector.Columns[4].ItemStyle.CssClass = "col-4";
            gvTraineeSelector.Columns[5].HeaderStyle.CssClass = "col-4";
            gvTraineeSelector.Columns[5].ItemStyle.CssClass = "col-4 textAlignLeft";
            gvTraineeSelector.Enabled = false;

            gvTraineeSelected.Columns[3].HeaderStyle.CssClass = "col-4";
            gvTraineeSelected.Columns[3].ItemStyle.CssClass = "col-4 textAlignLeft";
            gvTraineeSelected.Columns[4].HeaderStyle.CssClass = "col-4";
            gvTraineeSelected.Columns[4].ItemStyle.CssClass = "col-4";
            gvTraineeSelected.Columns[5].HeaderStyle.CssClass = "col-4";
            gvTraineeSelected.Columns[5].ItemStyle.CssClass = "col-4 textAlignLeft";
            gvTraineeSelected.Enabled = false;
        }

        private void bindTrainees(DataTable dtTrainees, string DeptID)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();

                if (Convert.ToInt32(ddlTargetType.SelectedValue) == 4) //added to load Trainees for General Type
                {
                    if (Convert.ToInt32(hdnTTS_CurrentStatus.Value) == 2)
                    {
                        if (dtTrainees.Rows.Count == 0)
                        {
                            dtTrainees = objTMS_BAL.EditTraineesForGeneralTraining(DeptID, ddlTrainingDocument.SelectedValue, hdnTTS_ID.Value);
                        }
                        else
                        {
                            dtTrainees = objTMS_BAL.EditTraineesForGeneralTraining(DeptID, ddlTrainingDocument.SelectedValue, hdnTTS_ID.Value, dtTrainees);
                        }
                    }
                    else
                    {
                        if (dtTrainees.Rows.Count == 0)
                        {
                            dtTrainees = objTMS_BAL.GetDeptTraineesForGeneral(DeptID, ddlTrainingDocument.SelectedValue);
                        }
                        else
                        {
                            dtTrainees = objTMS_BAL.GetDeptTraineesForGeneral(DeptID, ddlTrainingDocument.SelectedValue, dtTrainees);
                        }
                    }
                }
                else
                {
                    if (dtTrainees.Rows.Count == 0)
                    {
                        dtTrainees = objTMS_BAL.GetDeptTrainees(DeptID);
                    }
                    else
                    {
                        dtTrainees = objTMS_BAL.GetDeptTrainees(DeptID, dtTrainees);
                    }
                }
                ViewState["Trainees"] = null;
                ViewState["Trainees"] = dtTrainees;
                DataView dvTrainees = new DataView(dtTrainees);
                dvTrainees.RowFilter = "Status='Y' and DeptID=" + DeptID;
                gvTraineeSelector.DataSource = dvTrainees.ToTable();
                gvTraineeSelector.DataBind();

                if (ddlTraineeDepartment.SelectedValue == "0")
                {
                    dvTrainees.RowFilter = "Status='N'";
                    gvTraineeSelected.DataSource = dvTrainees.ToTable();
                    gvTraineeSelected.DataBind();
                }
                else
                {
                    dvTrainees.RowFilter = "Status='N' and DeptID=" + DeptID;
                    gvTraineeSelected.DataSource = dvTrainees.ToTable();
                    gvTraineeSelected.DataBind();
                }
                EnableDisableButtons();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M8:" + strline + "  " + strMsg, "error");
            }
        }
        //private void BindDataForTrainingSession()
        //{
        //    try
        //    {
        //        bool ShowNewTrainingBtn = false;
        //        bool ShowReSheduleTrainingBtn = false;
        //        objTMS_BAL = new TMS_BAL();
        //        DataTable dtResult = objTMS_BAL.GetTrainingSessionDetailsBal(hdnTTS_ID.Value, out SessionsCount);
        //        if (dtResult.Rows.Count > 0)
        //        {
        //            hdnRecentSessionStatus.Value = dtResult.Rows[0]["Status"].ToString();
        //            txtReSchedulDate.Value = dtResult.Rows[0]["TrainingDate"].ToString();
        //            txtReScheduleStartTime.Text = dtResult.Rows[0]["FromTime"].ToString();
        //            txtReScheduleEndTime.Text = dtResult.Rows[0]["ToTime"].ToString();
        //            upReSchedule.Update();
        //            int SessionStatus = Convert.ToInt32(dtResult.Rows[0]["Status"]);

        //            // 1-Open, 2-Re-Schedule, 3-Closed, 4-Cancelled, 5-Evaluated
        //            if (SessionsCount == 1 && (new[] { 1, 2, 4 }.Contains(SessionStatus)))
        //            {
        //                btnAttendees.Visible = false;
        //            }
        //            else
        //            {
        //                btnAttendees.Visible = true;
        //            }
        //        }
        //        else
        //        {
        //            btnAttendees.Visible = false; // if there is no rows, then there is no any sessions                    
        //            ShowNewTrainingBtn = true;
        //        }
        //        if (hdnRecentSessionStatus.Value == "1" || hdnRecentSessionStatus.Value == "2")
        //        {
        //            ShowReSheduleTrainingBtn = true;
        //        }
        //        else if (hdnRecentSessionStatus.Value == "3" || hdnRecentSessionStatus.Value == "5")
        //        {
        //            if (hdnTTS_Status.Value != "3")
        //            {
        //                ShowNewTrainingBtn = true;
        //            }
        //        }
        //        // Check Whether the User is Assigned Trainer
        //        if (hdnIsAssignedTrainer.Value == "true")
        //        {
        //            if (ShowNewTrainingBtn)
        //            {
        //                btnNewTrainingSession.Visible = true;
        //            }
        //            else if (ShowReSheduleTrainingBtn)
        //            {
        //                btnReScheduleTrainingSession.Visible = true;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string strline = HelpClass.LineNo(ex);
        //        string strMsg = HelpClass.SQLEscapeString(ex.Message);
        //        HelpClass.custAlertMsg(this, this.GetType(), "TTS_M-3:" + strline + "  " + strMsg, "error");
        //    }
        //}

        private void BindDataForTrainingSession()
        {
            try
            {
                int TraineeNonAttendes = 0;
                bool ShowSheduleTrainingBtn = false;
                bool ShowReSheduleTrainingBtn = false;
                objTMS_BAL = new TMS_BAL();
                DataSet dtResult = objTMS_BAL.GetTrainingSessionDetailsBal(hdnTTS_ID.Value, out SessionsCount);
                if (dtResult.Tables[1].Rows.Count > 0)
                {
                    TraineeNonAttendes = Convert.ToInt32(dtResult.Tables[1].Rows[0]["TraineeNonAttendes"]);
                }
                if (dtResult.Tables[0].Rows.Count > 0)
                {
                    hdnRecentSessionStatus.Value = dtResult.Tables[0].Rows[0]["Status"].ToString();
                    txtReSchedulDate.Value = dtResult.Tables[0].Rows[0]["TrainingDate"].ToString();
                    txtReScheduleStartTime.Text = dtResult.Tables[0].Rows[0]["FromTime"].ToString();
                    txtReScheduleEndTime.Text = dtResult.Tables[0].Rows[0]["ToTime"].ToString();
                    upReSchedule.Update();
                    int SessionStatus = Convert.ToInt32(dtResult.Tables[0].Rows[0]["Status"]);

                    // 1-Open, 2-Re-Schedule, 3-Closed, 4-Cancelled, 5-Evaluated
                    if (SessionsCount == 1 && (new[] { 1, 2, 4 }.Contains(SessionStatus)))
                    {
                        btnAttendees.Visible = false;
                    }
                    else
                    {
                        btnAttendees.Visible = true;
                    }
                }
                else
                {
                    btnAttendees.Visible = false; // if there is no rows, then there is no any sessions
                    
                    if (TraineeNonAttendes != 0)
                    {
                        ShowSheduleTrainingBtn = true;
                    }
                }
                if (hdnRecentSessionStatus.Value == "1" || hdnRecentSessionStatus.Value == "2")
                {
                    ShowReSheduleTrainingBtn = true;
                }
                
                // Check Whether the User is Assigned Trainer
                if (hdnIsAssignedTrainer.Value == "true")
                {
                    if ((ShowSheduleTrainingBtn || TraineeNonAttendes!=0) && !ShowReSheduleTrainingBtn && hdnTTS_Status.Value!="4")
                    {
                        btnNewTrainingSession.Visible = true;
                    }
                    if (ShowReSheduleTrainingBtn)
                    {
                        btnReScheduleTrainingSession.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M9:" + strline + "  " + strMsg, "error");
            }
        }

        private void UpdateTrainingSessionAfterCreate(string SheduledDate, string FromTime, string ToTime)
        {
            btnReScheduleTrainingSession.Visible = true;
            btnNewTrainingSession.Visible = false;
            upClassRoomSessionBtns.Update();
            txtReSchedulDate.Value = SheduledDate;
            txtReScheduleStartTime.Text = FromTime;
            txtReScheduleEndTime.Text = ToTime;
            upReSchedule.Update();
            upTrainingSession.Update();
        }
        private void DisableDropDowns()
        {
            ddlDepartmentTTS.Enabled = false;
            ddlDocumentType.Enabled = false;
            ddlTargetType.Enabled = false;
            ddlRefresherMonth.Enabled = false;
            ddlTrainingDocument.Enabled = false;
            ddlTypeOfTraining.Enabled = false;
            txtTTSDueDate.Attributes.Add("disabled", "disabled");
            gvTraineeSelected.Enabled = false;
            gvTraineeSelector.Enabled = false;
            btnAddTrainees.Enabled = false;
            btnAddTrainees.CssClass = "col-lg-4 float-left offset-lg-4 btn btn-signup_popup btnAddTrainers";
            btnAddTrainees.Attributes.Add("style", "cursor:not-allowed;");
            btnRemoveTrainees.Enabled = false;
            btnRemoveTrainees.CssClass = "col-lg-4 float-left offset-lg-4 btn btn-signup_popup btnAddTrainers";
            btnRemoveTrainees.Attributes.Add("style", "cursor:not-allowed;");
        }
        public void BindDocType()
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                DataTable dt = objTMS_BAL.DocumentType();
                ddlDocumentType.DataSource = dt;
                ddlDocumentType.DataValueField = "DocumentTypeID";
                ddlDocumentType.DataTextField = "DocumentType";
                ddlDocumentType.DataBind();
                upDeptAndDocType.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M10:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindDocType(DataTable dt)
        {
            ddlDocumentType.DataSource = dt;
            ddlDocumentType.DataValueField = "DocumentTypeID";
            ddlDocumentType.DataTextField = "DocumentType";
            ddlDocumentType.DataBind();
        }

        public void BindAuthorAccessiableDept(DataTable dt)
        {
            ddlDepartmentTTS.DataSource = dt;
            ddlDepartmentTTS.DataTextField = "DepartmentName";
            ddlDepartmentTTS.DataValueField = "DeptID";
            ddlDepartmentTTS.DataBind();
            ddlDepartmentTTS.Items.Insert(0, new ListItem("- Select Department -", "0"));
        }
        public void bindDepartments(int EmpID)
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                int[] RoleIDs = new int[] { (int)TMS_UserRole.HOD_TMS };
                DataTable dt = objUMS_BAL.getRoleWiseDepts(EmpID, RoleIDs);
                ddlDepartmentTTS.Items.Clear();
                ddlDepartmentTTS.DataSource = dt;
                ddlDepartmentTTS.DataTextField = "DepartmentName";
                ddlDepartmentTTS.DataValueField = "DeptID";
                ddlDepartmentTTS.DataBind();
                ddlDepartmentTTS.Items.Insert(0, new ListItem("- Select Department -", "0"));

                BindTraineeDepartment(); //Trainee department
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M11:" + strline + "  " + strMsg, "error");
            }
        }

        public void BindTraineeDepartment()
        {
            try
            {
                //Trainee department
                objUMS_BAL = new UMS_BAL();
                DataTable dtTraineeDept = objUMS_BAL.BindDepartmentsBAL();
                ddlTraineeDepartment.DataSource = dtTraineeDept;
                ddlTraineeDepartment.DataTextField = "DepartmentName";
                ddlTraineeDepartment.DataValueField = "DeptID";
                ddlTraineeDepartment.DataBind();
                ddlTraineeDepartment.Items.Insert(0, new ListItem("-- All Departments --", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M12:" + strline + "  " + strMsg, "error");
            }
        }

        public void ClearGV()
        {
            DataTable ds = new DataTable();
            ds = null;
            gvTraineeSelector.DataSource = ds;
            gvTraineeSelector.DataBind();
            gvTraineeSelected.DataSource = ds;
            gvTraineeSelected.DataBind();
        }
        protected void ddlTargetType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(ddlTargetType.SelectedValue) == 0 || Convert.ToInt32(ddlTargetType.SelectedValue) > 0)
                {
                    ddlTraineeDepartment.Enabled = false;
                    ddlTraineeDepartment.SelectedIndex = 0;
                    ClearGV();
                }
                if (ddlTargetType.SelectedValue == "4")
                {
                    getEffectedDocsForGeneral();
                }
                else
                {
                    BindDocAndMonths(ddlTargetType.SelectedValue);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M13:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindDocAndMonths(string TargetType)
        {
            objTMS_BAL = new TMS_BAL();
            ddlTrainingDocument.Items.Clear();
            // New SOP / Revision
            if (ddlTargetType.SelectedValue == "2" || ddlTargetType.SelectedValue == "3")
            {
                if (ddlDepartmentTTS.SelectedValue != "0" && ddlDocumentType.SelectedValue != "")
                {
                    // Get Documents 
                    DataTable dt = bindDocuments(Convert.ToInt32(ddlDepartmentTTS.SelectedValue),
                        Convert.ToInt32(ddlTargetType.SelectedValue),
                        Convert.ToInt32(ddlDocumentType.SelectedValue),Convert.ToInt32(hdnTTS_ID.Value));
                    if (dt.Rows.Count > 0)
                    {
                        BindDocuments(dt);
                    }
                }
                else if (ddlDepartmentTTS.SelectedValue != "0" && ddlDocumentType.SelectedValue == "")
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Document Type Not Exists to Get Documents", "warning");
                }
            }
            else if (ddlTargetType.SelectedValue == "1") //Refresher
            {
                divMonth.Visible = true;
                LoadMonth(false);
            }
            upMonth.Update();
            upTrainingDoc.Update();
            ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "showHideDivsByTargetType('" + ddlTargetType.SelectedValue + "');", true);
        }
        private void BindDocuments(DataTable dt)
        {
            try
            {
                ddlTrainingDocument.DataSource = dt;
                ddlTrainingDocument.DataValueField = "DocumentID";
                ddlTrainingDocument.DataTextField = "DocumentName";
                ddlTrainingDocument.DataBind();
                ddlTrainingDocument.Items.Insert(0, new ListItem("-Select Document-", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M14:" + strline + "  " + strMsg, "error");
            }
        }
        public DataTable bindDocuments(int DeptID, int targetType, int DocumentType,int ttsID)
        {           
            objTMS_BAL = new TMS_BAL();
            DataTable dt = objTMS_BAL.getEffectedDocsByTargetType(DeptID, targetType, DocumentType, ttsID);
            return dt;
        }
        protected void ddlDocumentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlTargetType.SelectedValue = "0";
                ddlTrainingDocument.Items.Clear();
               
                upTargetType.Update();
                upMonth.Update();
                upTrainingDoc.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "showHideDivsByDocType('1','" + ddlTargetType.SelectedValue + "');", true);                
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M15:" + strline + "  " + strMsg, "error");
            }
        }

        private void LoadMonth(bool getAllMonths = false)
        {
            try
            {
                ddlRefresherMonth.Items.Clear();
                ddlRefresherMonth.Items.Add(new ListItem("-Select Month-", "0"));
                var months = Enumerable.Range(1, 12).Select(i => new { MonthNumber = i, MonthName = DateTimeFormatInfo.CurrentInfo.GetMonthName(i) });
                foreach (var item in months)
                {
                    if (getAllMonths)
                    {
                        ddlRefresherMonth.Items.Add(new ListItem(item.MonthName, item.MonthNumber.ToString()));
                    }
                    else
                    {
                        if (DateTime.Now.Month <= item.MonthNumber)
                        {
                            ddlRefresherMonth.Items.Add(new ListItem(item.MonthName, item.MonthNumber.ToString()));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M16:" + strline + "  " + strMsg, "error");
            }
        }
        protected void ddlRefresherMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int RefresherMonth = Convert.ToInt32(ddlRefresherMonth.SelectedValue);
                if (RefresherMonth > 0)
                {
                    getDocsForRefresher(RefresherMonth);
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "showHideDivsByTargetType('" + ddlTargetType.SelectedValue + "');", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M17:" + strline + "  " + strMsg, "error");
            }
        }
        public void getDocsForRefresher(int RefresherMonth)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                DataTable dtRefresherDocsList = new DataTable();
                if (Convert.ToInt32(ddlDepartmentTTS.SelectedValue) > 0 && ddlDocumentType.SelectedValue != "" && RefresherMonth > 0)
                {
                    dtRefresherDocsList = objTMS_BAL.getDocsForRefresher(Convert.ToInt32(ddlDepartmentTTS.SelectedValue), RefresherMonth, DateTime.Now.Year, Convert.ToInt32(ddlDocumentType.SelectedValue));
                }
                if (dtRefresherDocsList.Rows.Count > 0)
                {
                    BindDocuments(dtRefresherDocsList);
                }
                else
                {
                    ddlTrainingDocument.Items.Clear();
                }
                upTrainingDoc.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M18:" + strline + "  " + strMsg, "error");
            }
        }

        public void getEffectedDocsForGeneral()
        {
            try
            {
                objTMS_BAL = new TMS_BAL();

                if (ddlDepartmentTTS.SelectedValue != "0" && ddlDocumentType.SelectedValue != "")
                {
                    DataTable dt = objTMS_BAL.getEffectedDocumentsForGeneral(Convert.ToInt32(ddlDepartmentTTS.SelectedValue),
                        Convert.ToInt32(ddlDocumentType.SelectedValue));
                    if (dt.Rows.Count > 0)
                    {
                        BindDocuments(dt);
                    }
                }
                else if (ddlDepartmentTTS.SelectedValue != "0" && ddlDocumentType.SelectedValue == "")
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Document Type Not Exists to Get Documents", "warning");
                }
                upTrainingDoc.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "showHideDivsByTargetType('" + ddlTargetType.SelectedValue + "');", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M19:" + strline + "  " + strMsg, "error");
            }
        }
        protected void ddlDepartmentTTS_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlTrainingDocument.Items.Clear();
                ddlTraineeDepartment.Enabled = false;
                ddlTraineeDepartment.SelectedIndex = 0;
                ddlTargetType.SelectedValue = "0";
                ClearGV();

                if (ddlDepartmentTTS.SelectedValue != "0")
                {
                    if (ddlDocumentType.Items.Count == 0)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Document Type Not Exists to get details.", "error");
                    }
                    else
                    {
                        BindDropDowns(ddlDepartmentTTS.SelectedValue, hdnEmpID.Value);
                    } 
                }
                else
                {
                    lbxTrainersTTS.Items.Clear();
                    ddlReviewerTTS.Items.Clear();
                    ddlTargetType.Enabled = false;
                    ddlTargetType.CssClass = "selectpicker regulatory_dropdown_style form-control";
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "showHideDivsByTargetType('0');", true);

                upTargetType.Update();
                upActionUsers.Update();
                upTrainingDoc.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M20:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindDropDowns(string Dept_ID, string Author, int IsEmpActive = 0)
        {
            try
            {
                ddlTargetType.Enabled = true;
                ddlTargetType.CssClass = "selectpicker regulatory_dropdown_style form-control";
                objTMS_BAL = new TMS_BAL();
                DataSet ds = objTMS_BAL.GetRoleWiseEmployees(Convert.ToInt32(Dept_ID));

                // Trainer 
                lbxTrainersTTS.DataSource = ds.Tables[0];
                lbxTrainersTTS.DataTextField = "EmpName";
                lbxTrainersTTS.DataValueField = "EmpID";
                lbxTrainersTTS.DataBind();

                // Reviewer(QA)
                BindReviewer(Author, ds.Tables[2]);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M21:" + strline + "  " + strMsg, "error");
            }
        }

        private void BindReviewer(string Author, DataTable dt)
        {
            DataView dvReviewer = new DataView(dt);
            dvReviewer.RowFilter = "EmpID<>" + Author;
            ddlReviewerTTS.DataSource = dvReviewer.ToTable();
            ddlReviewerTTS.DataTextField = "EmpName";
            ddlReviewerTTS.DataValueField = "EmpID";
            ddlReviewerTTS.DataBind();
            ddlReviewerTTS.Items.Remove(hdnEmpID.Value);
            ddlReviewerTTS.Items.Insert(0, new ListItem("- Select Approver -", "0"));
        }

        //All Employee's in View Mode
        public void BindDropDownsAllEmps(string Dept_ID, string Author, int IsEmpActive = 0)
        {
            try
            {
                ddlTargetType.Enabled = true;
                ddlTargetType.CssClass = "selectpicker regulatory_dropdown_style form-control";
                objTMS_BAL = new TMS_BAL();
                DataSet ds = objTMS_BAL.GetRoleWiseEmployees(Convert.ToInt32(Dept_ID), 1);

                // Trainer 
                lbxTrainersTTS.DataSource = ds.Tables[0];
                lbxTrainersTTS.DataTextField = "EmpName";
                lbxTrainersTTS.DataValueField = "EmpID";
                lbxTrainersTTS.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M22:" + strline + "  " + strMsg, "error");
            }
        }

        #endregion

        #region Trainee List
        protected void ddlTraineeDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindTraineesGv(ddlTraineeDepartment.SelectedValue);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M23:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindTraineesGv(string DeptID)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                DataTable dtVSTrainees = ViewState["Trainees"] as DataTable;
                bindTrainees(dtVSTrainees, DeptID);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M24:" + strline + "  " + strMsg, "error");
            }
        }
        public void InitializeDataTables()
        {
            try
            {
                DataTable dtTrainers = new DataTable();
                dtTrainers.Columns.Add(new DataColumn("EmpID"));
                dtTrainers.Columns.Add(new DataColumn("EmpCode"));
                dtTrainers.Columns.Add(new DataColumn("EmpName"));
                dtTrainers.Columns.Add(new DataColumn("DeptID"));
                dtTrainers.Columns.Add(new DataColumn("DeptCode"));
                dtTrainers.Columns.Add(new DataColumn("Status"));
                ViewState["Trainees"] = dtTrainers;
                gvTraineeSelected.DataSource = new List<object>();
                gvTraineeSelected.DataBind();
                gvTraineeSelector.DataSource = new List<object>();
                gvTraineeSelector.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M25:" + strline + "  " + strMsg, "error");
            }
        }
        protected void ddlTrainingDocument_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();


                if (ddlTargetType.SelectedValue == "3")
                {
                    if (ddlTrainingDocument.SelectedValue != "0")
                    {
                        GetPreviousTraineesForRevisionDoc();  //PreviousTraineesForRevisionDoc
                    }
                }
                if(ddlTrainingDocument.SelectedValue != "0")
                {
                    ddlTraineeDepartment.SelectedIndex = 0;
                    ddlTraineeDepartment.Enabled = true;
                }
                else if (ddlTrainingDocument.SelectedIndex == 0)
                {
                    ddlTraineeDepartment.SelectedIndex = 0;
                    ddlTraineeDepartment.Enabled = false;
                    ClearGV();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M26:" + strline + "  " + strMsg, "error");
            }
        }


        public void GetPreviousTraineesForRevisionDoc()
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                DataTable dtTemp = objTMS_BAL.GetPreviousTraineesForRevisionDocs(ddlDepartmentTTS.SelectedValue, ddlTrainingDocument.SelectedValue);
                DataColumn dcStatus = new DataColumn("Status");
                dtTemp.Columns.Add(dcStatus);
                foreach (DataRow dr in dtTemp.Rows)
                {
                    dr["Status"] = "N";
                }
               ViewState["Trainees"] = dtTemp;
                bindTrainees(dtTemp, ddlTraineeDepartment.SelectedValue);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M27:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnAddTrainees_Click(object sender, EventArgs e)
        {
            try
            {
                int TrainerCount = 0;
                foreach (GridViewRow gvrTrainees in gvTraineeSelector.Rows)
                {
                    if (((CheckBox)gvrTrainees.FindControl("cbTraineeGv")).Checked)
                    {
                        TrainerCount++;
                    }
                }
                if (TrainerCount == 0)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "select atleast one Trainee from available Trainees", "error");
                }
                else
                {
                    DataTable dtTrainees = (DataTable)ViewState["Trainees"];
                    foreach (GridViewRow gvrTrainees in gvTraineeSelector.Rows)
                    {
                        if (((CheckBox)gvrTrainees.FindControl("cbTraineeGv")).Checked)
                        {
                            int EmpID = Convert.ToInt32(((Label)gvrTrainees.FindControl("lblEmpIDGv")).Text);
                            int DeptID = Convert.ToInt32(((Label)gvrTrainees.FindControl("lblEmpDeptIDGv")).Text);

                            foreach (DataRow drSelectedTrainees in dtTrainees.Rows)
                            {
                                if (Convert.ToInt32(drSelectedTrainees["EmpID"]) == EmpID && Convert.ToInt32(drSelectedTrainees["DeptID"]) == DeptID)
                                {
                                    drSelectedTrainees["Status"] = "N";
                                }
                            }
                        }
                    }
                    //ViewState["Trainees"] = dtTrainees;
                    bindTrainees(dtTrainees, ddlTraineeDepartment.SelectedValue);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M28:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnRemoveTrainees_Click(object sender, EventArgs e)
        {
            try
            {
                int TrainerCount = 0;
                foreach (GridViewRow gvrTrainees in gvTraineeSelected.Rows)
                {
                    if (((CheckBox)gvrTrainees.FindControl("cbTraineeGv")).Checked)
                    {
                        TrainerCount++;
                    }
                }
                if (TrainerCount == 0)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "select atleast one Trainee from Assigned Trainees", "error");
                }
                else
                {
                    DataTable dtTrainees = (DataTable)ViewState["Trainees"];

                    foreach (GridViewRow gvrAssignedTrainees in gvTraineeSelected.Rows)
                    {
                        if (((CheckBox)gvrAssignedTrainees.FindControl("cbTraineeGv")).Checked)
                        {
                            int EmpID = Convert.ToInt32(((Label)gvrAssignedTrainees.FindControl("lblEmpIDGv")).Text);
                            int DeptID = Convert.ToInt32(((Label)gvrAssignedTrainees.FindControl("lblEmpDeptIDGv")).Text);

                            foreach (DataRow drSelectedTrainees in dtTrainees.Rows)
                            {
                                if (Convert.ToInt32(drSelectedTrainees["EmpID"]) == EmpID && Convert.ToInt32(drSelectedTrainees["DeptID"]) == DeptID)
                                {
                                    drSelectedTrainees["Status"] = "Y";
                                }
                            }
                        }
                    }
                    ViewState["Trainers"] = dtTrainees;
                    bindTrainees(dtTrainees, ddlTraineeDepartment.SelectedValue);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M29:" + strline + "  " + strMsg, "error");
            }
        }
        public void EnableDisableButtons()
        {
            try
            {
                if (hdnTTS_CurrentStatus.Value == "0" || hdnTTS_CurrentStatus.Value == "2") // 0 for creation, 2 reverted tts
                {
                    if (gvTraineeSelected.Rows.Count == 0)
                    {
                        btnRemoveTrainees.Enabled = false;
                        btnRemoveTrainees.CssClass = "col-lg-4 float-left offset-lg-4 btn btn-signup_popup ";
                        btnRemoveTrainees.Attributes.Add("style", "cursor:not-allowed;");
                    }
                    else
                    {
                        btnRemoveTrainees.Enabled = true;
                        btnRemoveTrainees.Style.Remove("cursor");
                    }
                    if (gvTraineeSelector.Rows.Count == 0)
                    {
                        btnAddTrainees.Enabled = false;
                        btnAddTrainees.CssClass = "col-lg-4 float-left offset-lg-4 btn btn-signup_popup btnAddTrainers";
                        btnAddTrainees.Attributes.Add("style", "cursor:not-allowed;");
                    }
                    else
                    {
                        btnAddTrainees.Enabled = true;
                        btnAddTrainees.Style.Remove("cursor");
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M30:" + strline + "  " + strMsg, "error");
            }
        }
        #endregion

        #region Training Sessions

        #region Online Session
        public void CreateOnlineSession(char IsSelfCompleteAllowed)
        {
            try
            {
                DataTable dt = objTMS_BAL.CreateOnlineTraining(Convert.ToInt32(hdnTTS_ID.Value), (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString(), txtOnlineDueDate.Value.Trim(), txtCommentsOnlineTraining.Value.Trim(), IsSelfCompleteAllowed, out int Result);

                if (Result == 1)
                {
                    hdnModeOfTraining.Value = "2";
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Online Training Session Created Successfully.", "success", "closemodalSchedule();");
                    if (dt.Rows.Count > 0)
                    {
                        gvOnlineTraining.DataSource = dt;
                        gvOnlineTraining.DataBind();
                        divOnlineGrid.Visible = true;
                        upTrainingSession.Update();
                        divClassRoomGrid.Visible = divClassroomActionBtns.Visible = false;
                        upClassRoomSessionBtns.Update();
                    }
                    //BindOnlineSessionGrid(hdnTTS_ID.Value);                        
                }
                else if (Result == 2)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Failed to Create Online Session, Session Already Created.", "error");
                }
                else if (Result == 3)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Failed to Create Online Session, Since There is a Classroom Session to this Target Training.", "error");
                }
                else if (Result == 4)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Failed to Create Online Session, Since you are not Authorized to create session.", "error");
                }
                if (Result != 1)
                {
                    hdnModeOfTraining.Value = "1";
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M31:" + strline + "  " + strMsg, "error");
            }
        }
        private void SubmitOnlineSession()
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                if (txtOnlineDueDate.Value.Trim() == ""/*|| txtCommentsOnlineTraining.InnerText.Trim() == ""*/)
                {
                    ArrayList Mandatory = new ArrayList();
                    if (txtOnlineDueDate.Value.Trim() == "")
                    {
                        Mandatory.Add("Enter Due Date");
                    }

                    StringBuilder s = new StringBuilder();
                    foreach (string x in Mandatory)
                    {
                        s.Append(x);
                        s.Append(",");
                    }
                    HelpClass.custAlertMsg(this, this.GetType(), s.ToString(), "error");
                }
                else
                {
                    if (chkbxOnline.Checked == true)
                    {
                        CreateOnlineSession('T');
                    }
                    else
                    {
                        CreateOnlineSession('F');
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M32:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindOnlineSessionGrid(string TTS_ID)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                DataTable dtOnlineSession = objTMS_BAL.getTTS_OnlineSessionDetails(TTS_ID);
                if (dtOnlineSession.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "ShowHideOnlineClassroomGrids('Online');", true);
                    gvOnlineTraining.DataSource = dtOnlineSession;
                    gvOnlineTraining.DataBind();
                }
                else
                {
                    gvOnlineTraining.DataSource = new List<object>();
                    gvOnlineTraining.DataBind();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M33:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindOnlineSessionGrid(DataTable dtOnlineSession)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                if (dtOnlineSession.Rows.Count > 0)
                {
                    gvOnlineTraining.DataSource = dtOnlineSession;
                    gvOnlineTraining.DataBind();
                }
                else
                {
                    gvOnlineTraining.DataSource = new List<object>();
                    gvOnlineTraining.DataBind();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M34:" + strline + "  " + strMsg, "error");
            }
        }
        protected void gvOnlineTraining_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                if (e.CommandName == "ViewOnlineTraining")
                {
                    int rowIndex = Convert.ToInt32(e.CommandArgument);
                    int TTS_ID = Convert.ToInt32(gvOnlineTraining.DataKeys[rowIndex].Values[0]);
                    hdnTTS_OnlineSessionID.Value = gvOnlineTraining.DataKeys[rowIndex].Values[1].ToString();
                    DataTable dt = objTMS_BAL.GetSelfCompleteTTSIDBAL(Convert.ToInt32(hdnTTS_ID.Value));
                    if (dt.Rows.Count > 0)
                    {
                        hdnIs_Selfcomplte.Value = "1";
                    }
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "showOnlineDetails();", true);
                    
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M35:" + strline + "  " + strMsg, "error");
            }
        }
        #endregion

        #region ClassRoom Session                     
        //Create Target Training Sessions Submit Code
        protected void btnSubmitSchedule_Click(object sender, EventArgs e)
        {
            try
            {

                objTMS_BAL = new TMS_BAL();
                objTMS_BAL.ToGetTTS_StatusClose(hdnTTS_ID.Value, out int Result);
                if (Result == 1)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "OpenTTSCloseValidation();", true);
                }
                else
                {
                    if (ddlModeOfTraining.SelectedValue == "1") //Classroom
                    {
                        hdnModeOfTraining.Value = "1";
                        //validation for Date Time of schedule
                        DateTime CurrentDate = DateTime.Now.Date;
                        DateTime GivenDate = Convert.ToDateTime(txtNewScheduleDate.Value).Date;
                        DateTime CurrentTime = DateTime.Now;
                        DateTime GivenStartTime = Convert.ToDateTime(txtScheduleNewStart.Text);
                        DateTime GivenEndTime = Convert.ToDateTime(txtScheduleNewEnd.Text);
                        if (GivenEndTime.TimeOfDay >= GivenStartTime.AddMinutes(30).TimeOfDay)//session should have 30 minutes of time span
                        {
                            if (GivenDate > CurrentDate)
                            {
                                //Success
                                SubmitSession();
                            }
                            else if (GivenDate == CurrentDate)
                            {
                                if (GivenStartTime.TimeOfDay > CurrentTime.AddMinutes(5).TimeOfDay)
                                {
                                    //Success
                                    SubmitSession();
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "custAlertMsg('Session Start Time should be Greater than 5 minutes to present time.','error');", true);
                                }
                            }
                            else
                            {
                                HelpClass.custAlertMsg(this, this.GetType(), "Session Date should be Greater than or equal to present date.", "error");
                            }
                        }
                        else if ((GivenEndTime.TimeOfDay <= GivenStartTime.AddMinutes(30).TimeOfDay) && (GivenStartTime.TimeOfDay < CurrentTime.AddMinutes(5).TimeOfDay))
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Session Start Time should be Greater than 5 minutes to present time. <br/> Session End Time Should be More than 30 minutes of Start time.", "error");
                        }
                        else
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Session End Time Should be More than 30 minutes of Start time.", "error");
                        }
                        //GetTargetTS_Details(hdnTTS_ID.Value);
                    }
                    else // Mode of training is Online
                    {
                        SubmitOnlineSession();
                    }
                    upTrainingSession.Update();
                }
              
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M36:" + strline + "  " + strMsg, "error");
            }
        }
        public void SubmitSession()
        {
            try
            {
                BindDataForTrainingSession();
                if (SessionsCount != 0)
                {
                    if (hdnRecentSessionStatus.Value == "5" || hdnRecentSessionStatus.Value == "3" || hdnRecentSessionStatus.Value=="1")// 0:Non-Approved,3:Close (represents to Create new training Sessions)
                    {
                        CreateNewSession();
                    }
                }
                else
                {
                    CreateNewSession();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M37:" + strline + "  " + strMsg, "error");
            }
        }

        public void CreateNewSession()
        {
            try
            {
                //Create Schedule
                objTMS_BAL.CreateTargetSessions(Convert.ToInt32(hdnTTS_ID.Value), Convert.ToInt32(hdnEmpID.Value),
                    txtNewScheduleDate.Value.Trim(), txtScheduleNewStart.Text.Trim(),
                    txtScheduleNewEnd.Text.Trim(), txtScheduleNewComments.InnerText.Trim(), out int Result);

                if (Result == 1)
                {
                    UpdateTrainingSessionAfterCreate(txtNewScheduleDate.Value.Trim(), txtScheduleNewStart.Text.Trim(),
                   txtScheduleNewEnd.Text.Trim());
                    hdnRecentSessionStatus.Value = "1";
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('Training Session Created Successfully !','success',ReloadSessionDetails());", true);
                }
                else if (Result == 2)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "You Are Not Authorized Trainer to create the Session.", "error");
                }
                else if (Result == 3)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Target Training is not in a Status of Session Creation.", "error");
                }
                else if (Result == 4)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "There is no non Attendees, So you can\\'t create a Session.", "error");
                }
                else if (Result == 5)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "There is already a Session.", "error");
                }
                else if (Result == 6)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "There is already a Online Session Created.", "error");
                }
                else
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Can not create the Session, Contact Admin.", "error");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M38:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnUpdateReSchedule_Click(object sender, EventArgs e)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                objTMS_BAL.ToGetTTS_StatusClose(hdnTTS_ID.Value, out int Result);
                if (Result == 1)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "OpenTTSCloseValidation();", true);
                }
                else
                {
                    //validation for Date Time of schedule
                    DateTime CurrentDate = DateTime.Now.Date;
                    DateTime GivenDate = Convert.ToDateTime(txtReSchedulDate.Value).Date;
                    DateTime CurrentTime = DateTime.Now;
                    DateTime GivenStartTime = Convert.ToDateTime(txtReScheduleStartTime.Text);
                    DateTime GivenEndTime = Convert.ToDateTime(txtReScheduleEndTime.Text);
                    if (GivenEndTime.TimeOfDay >= GivenStartTime.AddMinutes(30).TimeOfDay)//session should have 30 minutes of time span
                    {
                        if (GivenDate > CurrentDate)
                        {
                            if (hdnRecentSessionStatus.Value == "1")// 1 represents open Status
                            {
                                ResecheduleSessionDateTime();
                            }
                        }
                        else if (GivenDate == CurrentDate)
                        {
                            if (GivenStartTime.TimeOfDay > CurrentTime.AddMinutes(5).TimeOfDay)
                            {
                                if (hdnRecentSessionStatus.Value == "1" || hdnRecentSessionStatus.Value == "2")// 1 represents open,represents 2 Rescheduling Status 
                                {
                                    ResecheduleSessionDateTime();
                                }
                                else
                                {
                                    HelpClass.custAlertMsg(this, this.GetType(), "There is no Sessions with open State.", "error");
                                }
                            }
                            else
                            {
                                HelpClass.custAlertMsg(this, this.GetType(), "Session Start Time should be Greater than 5 minutes to present time.", "error");
                            }
                        }
                        else
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Session Date should be Greater than or equal to present date.", "error");
                        }
                    }
                    else
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Session End Time Should be More than 30 minutes of Start time.", "error");
                    }
                    // BindDataForTrainingSession();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M39:" + strline + "  " + strMsg, "error");
            }
        }

        public void ResecheduleSessionDateTime()
        {
            try
            {
                //Re-schedule Update 
                objTMS_BAL.ReScheduleTargetSessions(hdnTTS_ID.Value, txtReSchedulDate.Value.Trim(), txtReScheduleStartTime.Text.Trim(), txtReScheduleEndTime.Text.Trim(), txtCommentsReSchedule.InnerText.Trim(), (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString(), out int Result1);
                if (Result1 == 1)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Training Session ReScheduled Successfully !", "success", "ReloadSessionDetails();");
                }
                else if (Result1 == 2)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "There is no any Scheduled sessions to ReSchedule.", "info");
                }
                else if (Result1 == 3)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Failed to Reschedule, Since session is not in a status of Reschedule.", "info");
                }
                else
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Failed to Reschedule the Training Session, Contact your Admin.", "error");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M40:" + strline + "  " + strMsg, "error");
            }
        }

        // Take Attandence to Trainees in TTS Submit
        protected void btnAttandenceSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                objTMS_BAL.ToGetTTS_StatusClose(hdnTTS_ID.Value, out int OutResult);
                if (OutResult == 1)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "OpenTTSCloseValidation();", true);
                }
                else
                {
                    string GetTraineeIDs = hdnTraineeIds.Value.ToString();
                    string[] TraineeIDs = GetTraineeIDs.Split(',');
                    DataTable dtTrainees = new DataTable();
                    dtTrainees.Columns.Add("TraineeIDs");
                    for (int i = 0; i < TraineeIDs.Length; i++)
                    {
                        DataRow dr = dtTrainees.NewRow();
                        dr["TraineeIDs"] = TraineeIDs[i];
                        dtTrainees.Rows.Add(dr);
                    }
                    objTMS_BAL = new TMS_BAL();
                    objTMS_BAL.InsertTraineesAttandence(Convert.ToInt32(hdnTrainingSessionID.Value),
                        (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString(), dtTrainees,
                        Convert.ToChar(ddlExamType.SelectedValue), out int Result);
                    string timestamp = DateTime.Now.ToString("hh.mm.ss.ffffff",
                                                CultureInfo.InvariantCulture);

                    ScriptManager.RegisterStartupScript(this, GetType(), timestamp, "TraineeAttendanceresult(" + Result + ")", true);
                    BindDataForTrainingSession();
                    upTypeOfTrainingAndStatus.Update();
                    if (Result != 2 || Result != 5)
                    {
                        btnAttendees.Visible = true;
                        btnReScheduleTrainingSession.Visible = false;
                        //if (Result == 4)
                        //{
                        //    btnNewTrainingSession.Visible = false;
                        //}
                        //else
                        //{
                        //    btnNewTrainingSession.Visible = true;
                        //}
                    }
                    if (Result == 5 || Result == 6)
                    {
                        btnAttendees.Visible = true;
                        if (Result == 5)
                        {
                            btnNewTrainingSession.Visible = false;
                        }
                        else
                        {
                            btnNewTrainingSession.Visible = true;
                        }
                    }
                    if (Result == 2)
                    {
                        btnReScheduleTrainingSession.Visible = true;
                    }
                    upClassRoomSessionBtns.Update();
                }
               
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M41:" + strline + "  " + strMsg, "error");
            }
        }
        #endregion

        #endregion

        #region HOD Actions    
        protected void btnHodSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                //For HOD Create or Modify
                if (hdnTTS_CurrentStatus.Value == "0" || hdnTTS_CurrentStatus.Value == "2")
                {
                    SubmitByHOD(hdnTTS_CurrentStatus.Value);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M42:" + strline + "  " + strMsg, "error");
            }
        }

        public void SubmitByHOD(string CurrentStatus)
        {
            try
            {
                DataTable dtViewStateTrainees = (DataTable)ViewState["Trainees"];
                DataTable dtAssignedTrainees = new DataTable();
                dtAssignedTrainees.TableName = "Trainees";
                dtAssignedTrainees.Columns.Add("EmpID");

                //converting view state of trainees to datatable
                foreach (DataRow dr in dtViewStateTrainees.Rows)
                {
                    if (dr["Status"].ToString() == "N")
                    {
                        DataRow drTrainee = dtAssignedTrainees.NewRow();
                        drTrainee["EmpID"] = dr["EmpID"];
                        dtAssignedTrainees.Rows.Add(drTrainee);                      
                    }
                }
               
                //converting listbox selected trainers to datatable
                DataTable dtAssignedTrainers = new DataTable();
                dtAssignedTrainers.TableName = "Trainers";
                dtAssignedTrainers.Columns.Add("EmpID");
                for (int x = 0; x < lbxTrainersTTS.Items.Count; x++)
                {
                    if (lbxTrainersTTS.Items[x].Selected.Equals(true))
                    {
                        DataRow dr = dtAssignedTrainers.NewRow();
                        dr["EmpID"] = lbxTrainersTTS.Items[x].Value;
                        dtAssignedTrainers.Rows.Add(dr);
                    }
                }
                DataSet dsTrainersAndTrainees = new DataSet();
                if (dtAssignedTrainers.Rows.Count == 0 ||dtAssignedTrainees.Rows.Count == 0)
                { 
                    ArrayList Mandatory = new ArrayList(); 
                    if (dtAssignedTrainers.Rows.Count == 0)
                    {
                        Mandatory.Add("Select at least one Trainer");
                    }
                    if (dtAssignedTrainees.Rows.Count == 0)
                    {
                        Mandatory.Add("Select at least one Trainee");
                    }
                    StringBuilder s = new StringBuilder();
                    foreach (string x in Mandatory)
                    {
                        s.Append(x);
                        s.Append(",");
                    } 
                    HelpClass.custAlertMsg(this, this.GetType(), s.ToString(), "error");
                }
                else
                {
                    bool IsTraineeAsTrainerorApprover = false;
                    if (dtAssignedTrainees.Rows.Count > 0  && dtAssignedTrainers.Rows.Count > 0)
                    {
                        if (lbxTrainersTTS.Items.Count > 0)
                        {
                            for (int x = 0; x < lbxTrainersTTS.Items.Count; x++)
                            {
                                if (lbxTrainersTTS.Items[x].Selected.Equals(true))
                                {
                                    foreach (DataRow dr in dtAssignedTrainees.Rows)
                                    {
                                        if (dr["EmpID"].ToString() == lbxTrainersTTS.Items[x].Value || ddlReviewerTTS.SelectedValue == dr["EmpID"].ToString())
                                        {
                                            IsTraineeAsTrainerorApprover = true;
                                        }
                                    }
                                }
                            }
                        }
                        dsTrainersAndTrainees.Tables.Add(dtAssignedTrainers);
                        dsTrainersAndTrainees.Tables.Add(dtAssignedTrainees);
                    }
                    if (!IsTraineeAsTrainerorApprover)
                    {
                        SubmitTS(CurrentStatus, dsTrainersAndTrainees, ddlDepartmentTTS.SelectedValue, ddlDocumentType.SelectedValue
                      , ddlTrainingDocument.SelectedValue, ddlTypeOfTraining.SelectedValue, hdnEmpID.Value, txtTTSDueDate.Value,/* TraineeName, TotalInActiveCount,*/
                      ddlReviewerTTS.SelectedValue, txtAuthorComments.Value, ddlTargetType.SelectedValue, ddlRefresherMonth.SelectedValue );
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "custAlertMsg('Trainee shouldn\\'t be Trainer or Approver','info');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M43:" + strline + "  " + strMsg, "error");
            }
        }

        public void SubmitTS(string CurrentStatus, DataSet dsTrainersAndTrainees, string Dept_ID,
            string Document_Type, string Document_ID, string TypeOFTraining, string AuthorBy, string DueDate,/* string TraineeName,int TotalInActiveCount,*/ string ReviewBy = "0",
            string Remarks = "", string TargetType = "0", string MonthID = "0")
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                TTS_Objects objTTS = new TTS_Objects();
                objTTS.dsTrainersAndTrainees = dsTrainersAndTrainees;
                objTTS.Dept_ID = Convert.ToInt32(ddlDepartmentTTS.SelectedValue);
                objTTS.Document_Type = Convert.ToInt32(Document_Type);
                objTTS.Document_ID = Convert.ToInt32(Document_ID);
                objTTS.TypeOFTraining = Convert.ToChar(TypeOFTraining);
                objTTS.AuthorEmpId = Convert.ToInt32(AuthorBy);
                objTTS.Remarks = Remarks;
                objTTS.DueDate = DueDate;
                objTTS.ReviewEmpId = Convert.ToInt32(ReviewBy);
                if (MonthID == "")
                {
                    objTTS.MonthID = 0;
                }
                else
                {
                    objTTS.MonthID = Convert.ToInt32(MonthID);
                }
                if (TargetType == "")
                {
                    objTTS.TargetType = 0;
                }
                else
                {
                    objTTS.TargetType = Convert.ToInt32(TargetType);
                }
               
                if (CurrentStatus == "0")// HOD Creation
                {

                    objTMS_BAL.CreateTargetTraining(objTTS, out int Result);
                    switch (Result)
                    {
                        case 1:
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Training Schedule Created,Except InActive Trainees ", "success", "TTS_Created_Success()");
                            break;
                        case 4:
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Training Schedule Created.", "success", "TTS_Created_Success()");
                            break;
                        case 5:
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "All the Assigned Trainees are InActive Trainees ", "error", "TTS_Created_Success()");
                            break;

                    }
                    if (Result == 2)
                    {
                        if (TargetType == "1")
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "not allowed to Create Target Training Schedule on this Document.", "warning");
                        }
                        else
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Target Training Schedule already Created.", "info");
                        }
                    }
                    if (Result == 3)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "You are not allowed to create Target Training Schedule on withdrawal Document.", "error");
                    }
                 }
                if (CurrentStatus == "2")// HOD Modification
                {
                    objTTS.TargetTS_ID = Convert.ToInt32(hdnTTS_ID.Value);
                    objTMS_BAL.UpdateTargetTraining(objTTS, out int Result);
                    if (Result == 1)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Modified Training Schedule, Except InActive Trainees.", "success", "gotoPendingList()");
                    }
                    else if (Result == 2)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "You are not a Authorized Person to Modified.", "warning");
                    }
                    else if (Result == 3)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Record Already Modified", "info");
                    }
                    else if (Result == 4)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "You are not allowed to create Target Training Schedule on withdrawal Document.", "error");
                    }
                    else if (Result == 5)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "All the Assigned Trainees are InActive Trainees.", "error");
                    }
                    else if (Result == 6)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Modified Training Schedule.", "success", "gotoPendingList()");
                    }
                    else
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Internal Error Occurred", "error");
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M44:" + strline + "  " + strMsg, "error");
            }
        }
        #endregion

        #region QA Actions
        protected void btnQaApprove_Click(object sender, EventArgs e)
        {
            //1- Created, 3- Modified
            if (hdnTTS_CurrentStatus.Value == "1" || hdnTTS_CurrentStatus.Value == "3")
            {
                ViewState["QAApproveOrRevert"] = "approve";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "openElectronicSignModal();", true);
            }
        }

        protected void btnQaRevert_Click(object sender, EventArgs e)
        {
            //1- Created, 3- Modified
            if (hdnTTS_CurrentStatus.Value == "1" || hdnTTS_CurrentStatus.Value == "3")
                if (String.IsNullOrEmpty(txtReviewerComments.InnerText.Trim()))
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Enter Revert Reason in Reviewer comments", "error", "TTS_QA_RevertReason()");
                    return;
                }
            ViewState["QAApproveOrRevert"] = "revert";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "openElectronicSignModal();", true);
        }

        //QA Action Submits
        private void SubmitByQA(string QA_Action)
        {
            objTMS_BAL = new TMS_BAL();
            objTMS_BAL.SubmitTTS_QA_ActionSubmit(hdnTTS_ID.Value, hdnEmpID.Value, QA_Action, out int ResultStatus, txtReviewerComments.InnerText.Trim());
            
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HideElectronicSignModal();", true);
            string msgStr = QA_Action == "2" ? "Target Training Schedule Reverted Successfully" : "Target Training Schedule Approved Successfully";
            if (ResultStatus == 1)
            {
                HelpClass.custAlertMsgWithFunc(this, this.GetType(), msgStr, "success", "gotoApprovalList()");
            }
            else if (ResultStatus == 2)
            {
                HelpClass.custAlertMsg(this, this.GetType(), "Action Already Done.", "error");
            }
            else if (ResultStatus == 3)
            {
                HelpClass.custAlertMsg(this, this.GetType(), "Not Authorized to do Action.", "error");
            }
        }

        #endregion

        //To Take Electronic Sign
        protected void ElectronicSign_buttonClick(object sender, EventArgs e)
        {
            try
            {
                bool b = ElectronicSign.IsPasswordValid;
                if (b == true)
                {
                    if (ViewState["QAApproveOrRevert"].ToString() != null)
                    {
                        if (ViewState["QAApproveOrRevert"].ToString().ToLower() == "approve")//QA Approve
                        {
                            SubmitByQA("4");
                        }
                    }
                    if (ViewState["QAApproveOrRevert"].ToString() != null)
                    {
                        if (ViewState["QAApproveOrRevert"].ToString().ToLower() == "revert")// QA Revert
                        {
                            SubmitByQA("2");
                        }
                    }
                }
                if (b == false)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Password is Incorrect.", "error", "focusOnElecPwd()");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M45:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnBackToList_Click(object sender, EventArgs e)
        {
            try
            {
                //string RedirectURL = ViewState["PreviousURL"].ToString();
                //string RedirectURL;
                //switch (hdnPreviousPage.Value)
                //{
                //    case "PL": //HOD
                //        RedirectURL = "/TMS/TargetTrainingSchedule/TTS_Pending.aspx?FilterType=2";
                //        break;
                //    case "AL": //QA
                //        RedirectURL = "/TMS/TargetTrainingSchedule/TTS_ApprovalList.aspx?FilterType=2";
                //        break;
                //    case "QA": //Trainer
                //        RedirectURL = "/TMS/TargetTrainingSchedule/TTS_ApprovedList.aspx";
                //        break;
                //    case "ML": //HOD or QA
                //        RedirectURL = "/TMS/Reports/TargetTS_List.aspx";
                //        break;
                //    case "CL": //HOD
                //        RedirectURL = "/TMS/TargetTrainingSchedule/TTS_CreationList.aspx";
                //        break;
                //    default:
                //        RedirectURL = "/TMS/TargetTrainingSchedule/TTS_Master.aspx";
                //        break;
                //}
                Response.Redirect(hdnPreviousPage.Value, false);                
                if (Session["PreviousPage"] != null)
                {
                    Session.Remove("PreviousPage");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M46:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnViewExamAnswerSheet_Click(object sender, EventArgs e)
        {
            try
            {
                bool IsClassroom = true;
                if (hdnModeOfTraining.Value == "2")
                {
                    IsClassroom = false;
                }
                ucExamResultSheet.showExamResult(Convert.ToInt32(hdnTrainingSessionID.Value), Convert.ToInt32(hdnTargetExamID.Value),
                    hdnTrainee.Value, false, 0, IsClassroom);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M47:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnNewTrainingSession_Click(object sender, EventArgs e)
        {
            try
            {
                GetActiveQuestionnnaires();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M48:" + strline + "  " + strMsg, "error");
            }
        }
        public void GetActiveQuestionnnaires()
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                DataTable dt = objTMS_BAL.GetActiveDocsBal(ddlTrainingDocument.SelectedValue, out int Result);
                if (Result == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss"), "lblWarningText();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M49:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnAskFeedBack_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdnEmpFeedbackID.Value != "0")
                {
                    ucFeedback.ViewFeedbackForm(Convert.ToInt32(hdnEmpFeedbackID.Value), (int)TmsBaseType.Target_Training);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_M50:" + strline + "  " + strMsg, "error");
            }
        }
    }
}
