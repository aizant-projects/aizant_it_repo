﻿<%@ Page Title="Target Approved List" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="TTS_ApprovedList.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.TargetTrainingSchedule.TTS_ApprovedList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

       <asp:HiddenField ID="hdnEmpID" runat="server" Value="0"/>
     <asp:UpdatePanel ID="UpHdnFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnTTS_ID" runat="server" />
             <asp:HiddenField runat="server" ID="hdnNotificationID" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
     <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_panel_full padding-none">
       <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="mainContainer" runat="server" visible="true">
            <div class="float-left col-lg-12 padding-none" id="Div3" runat="server">     
               <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 "><asp:Label ID="Label1" runat="server" Font-Bold="true" Text="Approved Target Training Schedule"></asp:Label></div>
                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 bottom padding-none" style="overflow-y: auto; margin-top: 10px">
                    <table id="tblTTS_ApprovalList" class="datatable_cust tblTTS_ApprovalListClass display breakword padding-none" style="width:100%;">
                        <thead>
                            <tr>
                                <th>TTS_ID</th>
                                <th>Document</th>
                                <th>Dept Code</th>
                                <th>Type of Training</th>
                                <th>Author</th>
                                <th>Reviewer</th>
                                <th>Approved Date</th>
                                <th>Status</th>
                                <th>View</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnViewTTS_ApprovalList" OnClick="btnViewTTS_ApprovalList_Click" runat="server" Text="Button" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    
    <script>
        function ViewTTS_ApprovalList(TTS_ID) {
            $("#<%=hdnTTS_ID.ClientID%>").val(TTS_ID);
            $("#<%=btnViewTTS_ApprovalList.ClientID%>").click();
        }
    </script>

    <!--TTS_ApprovalList jQuery DataTable-->
    <script>
        $.ajaxSetup({
            cache: false
        });

        var table = $('#tblTTS_ApprovalList').DataTable({
            columns: [
                { 'data': 'TTS_ID' },
                { 'data': 'DocumentName' },
                { 'data': 'DepartmentName' },
                { 'data': 'TypeOfTraining' },
                { 'data': 'Author' },
                { 'data': 'Reviewer' },
                { 'data': 'ApprovedDate' },
                { 'data': 'StatusName' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a class="view" title="View"  href="#" onclick="ViewTTS_ApprovalList(' + o.TTS_ID + ');"></a>'; }
                }
            ],
            "aoColumnDefs": [{ "targets": [0], "visible": false }, { className: "textAlignLeft", "targets": [1, 3, 4, 5, 7] }],
            "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TTS_ListService.asmx/TTS_QA_ApprovedList")%>",
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "TrainerID", "value": <%=hdnEmpID.Value%> }, { "name": "Notification_ID", "value": <%=hdnNotificationID.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        UserSessionCheck();
                        //$("#tblTTS_ApprovalList").show();
                        //$(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        //$(".tblTTS_ApprovalListClass").css({ "width": "100%" });
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
            
        });

        function showDetails() {
            //so something funky with the data
        }
    </script>
   
</asp:Content>
