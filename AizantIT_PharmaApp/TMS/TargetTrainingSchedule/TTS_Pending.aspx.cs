﻿using AizantIT_PharmaApp.Common;
using System;
using System.Collections;
using System.Data;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.TargetTrainingSchedule
{
    public partial class TTS_Pending : System.Web.UI.Page
    {
        UMS_BAL objUMS_BAL;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = (Session["UserDetails"] as DataSet).Tables[1].Select("ModuleID="+(int)Modules.TMS);
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0)
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                LoadUnAuthorizedMessage();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }                  
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_PL1:" + strline + "  " + strMsg, "error");
            }
        }

        private void LoadUnAuthorizedMessage()
        {
            try
            {
                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                divAutorizedMsg.Visible = true;
                mainContainer.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_PL2:" + strline + "  " + strMsg, "error");
            }
        }

        public void InitializeThePage()
        {
            try
            {
                hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                if (Request.QueryString["Notification_ID"] != null)
                {
                    objUMS_BAL = new UMS_BAL();
                    int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs); //2 represents NotifyEmp had visited the page through notification link.
                    hdnNotificationID.Value = Request.QueryString["Notification_ID"].ToString();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_PL3:" + strline + "  " + strMsg, "error");
            }
        }
        protected void lbTTSList_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["PreviousPage"] != null)
                {
                    Session.Remove("PreviousPage");
                }
                Response.Redirect("~/TMS/TargetTrainingSchedule/TTS_Master.aspx?TTS_Type=Create");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_PL4:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnViewTTS_Pending_Click(object sender, EventArgs e)
        {
            try
            {
                //int TTS_ID = Convert.ToInt32(hdnTTS_Pending.Value);
                //Response.Redirect("~/TMS/TargetTrainingSchedule/TTS_Master.aspx?TTS_ID=" + TTS_ID);

                //Session.Remove("PendingTTS_ID");
                //Session.Remove("ApproveTTS_ID");
                //Session.Remove("QAApproveTTS_ID");
                //Session["PendingTTS_ID"] = hdnTTS_Pending.Value;
                Hashtable ht = new Hashtable();
                ht.Add("TTS_ID", hdnTTS_Pending.Value);
                ht.Add("Previous_PageValue", "PL");
                Session["PreviousPage"] = ht;
                Response.Redirect("~/TMS/TargetTrainingSchedule/TTS_Master.aspx");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_PL5:" + strline + "  " + strMsg, "error");
            }
        }
    }
}