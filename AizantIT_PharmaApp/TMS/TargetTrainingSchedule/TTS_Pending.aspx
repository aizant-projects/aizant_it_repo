﻿<%@ Page Title="target Pending" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="TTS_Pending.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.TargetTrainingSchedule.TTS_Pending" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:LinkButton ID="lbTTSList" class="float-right btn btn-dashboard" OnClick="lbTTSList_Click" runat="server">Create Target Training Schedule</asp:LinkButton>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
      <asp:HiddenField ID="hdnEmpID" runat="server" Value="0"/>
    <asp:UpdatePanel ID="UpHdnFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnTTS_Pending" runat="server" />
            <asp:HiddenField ID="hdnPrint" runat="server" Value="false" />
             <asp:HiddenField runat="server" ID="hdnNotificationID" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="mainContainer" runat="server" visible="true">
            <div class="col-12 padding-none" id="Div3" runat="server">
               <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 "><asp:Label ID="lblTTS_Pending" runat="server" Font-Bold="true" Text="Pending Target Training Schedule"></asp:Label></div>
                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 bottom padding-none" style="margin-top: 10px">
                    <table id="tblTTS_Pending" class=" datatable_cust tblTTS_PendingClass display breakword" style="width: 100%;">
                        <thead>
                            <tr>
                                <th>TTS_ID</th>
                                <th>Document</th>
                                <th>Department</th>
                                <th>Type of Training</th>
                                <th>Reviewer</th>
                                <th>Status</th>
                                <th>StatusID</th>
                                <th>View</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnViewTTS_Pending" runat="server" Text="Button" OnClick="btnViewTTS_Pending_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    
    <script>
        function ViewTTS_Pending(TTS_ID) {
            $("#<%=hdnTTS_Pending.ClientID%>").val(TTS_ID);
            $("#<%=btnViewTTS_Pending.ClientID%>").click();
        }
    </script>

    <!--TTS_Pending jQuery DataTable--> 
    <script>
        function getUrlVars() {
            var vars = [], hash;
            var url = window.location.href;
            var hashes = url.slice(url.indexOf('?') + 1).split('&');
            if (url.indexOf("#") > 0) {
                var hashes = url.slice(url.indexOf('?') + 1, url.indexOf('#')).split('&');
            }

            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }

        var table = $('#tblTTS_Pending').DataTable({

            columns: [
                { 'data': 'TTS_ID' },
                { 'data': 'DocumentName' },
                { 'data': 'DepartmentName' },
                { 'data': 'TypeOfTraining' },
                { 'data': 'Reviewer' },
                { 'data': 'CurrentStatus' },
                { 'data': 'StatusID' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) {
                        if (o.CurrentStatus == "Reverted by Reviewer") {
                            return '<a class="revert_tms" href="#" title="View" onclick="ViewTTS_Pending(' + o.TTS_ID + ');"></a>';
                        }
                        else {
                            return '<a class="view" href="#" title="View" onclick="ViewTTS_Pending(' + o.TTS_ID + ');"></a>';
                        }
                    }
                }
            ],
            "order": [[5, "desc"]],
            "scrollY": "371px",
            "aoColumnDefs": [{ "targets": [0, 6], "visible": false }, { className: "textAlignLeft", "targets": [1, 2, 3, 4, 5] }],
            "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TTS_ListService.asmx/TTS_Pending")%>",
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "AuthorID", "value": <%=hdnEmpID.Value%> }, { "name": "FilterType", "value": getUrlVars()["FilterType"] }, { "name": "Notification_ID", "value": <%=hdnNotificationID.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        UserSessionCheck();
                        $("#tblTTS_Pending").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        $(".tblTTS_PendingClass").css({ "width": "100%" });
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
            //fnDrawCallback: function () {
            //    $('.image-details').bind("click", showDetails);
            //}
        });

        function showDetails() {
            //so something funky with the data
        }
    </script>
</asp:Content>
