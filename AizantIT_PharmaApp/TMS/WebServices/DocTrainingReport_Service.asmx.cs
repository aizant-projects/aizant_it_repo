﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using TMS_BusinessLayer;
using TMS_BusinessObjects;

namespace AizantIT_PharmaApp.TMS.WebServices
{
    /// <summary>
    /// Summary description for DocTrainingReport_Service
    /// </summary>
    [WebService(Namespace = "http://Aizant.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class DocTrainingReport_Service : System.Web.Services.WebService
    {
        TMS_BAL objTMS_Bal;
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetDocReports()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var DocumentID = int.Parse(HttpContext.Current.Request.Params["DocumentID"]);
                var FromDate = HttpContext.Current.Request.Params["FromDate"].ToString();
                var ToDate = HttpContext.Current.Request.Params["ToDate"].ToString();

                var sSearch_Document = HttpContext.Current.Request.Params["sSearch_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch_VersionNumber = HttpContext.Current.Request.Params["sSearch_1"].ToString(CultureInfo.CurrentCulture);
                var sSearch_TraineeEmpCode = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
                var sSearch_TraineeName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
                var sSearch_TrainingDate = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
                var sSearch_TrainerName = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
                var sSearch_Type = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);

                List<DocTrainingReportsObjects> objDocReports = new List<DocTrainingReportsObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetDocWiseTrainingRecords(displayLength, displayStart, sortCol, sSortDir, sSearch, DocumentID, FromDate, ToDate,
                  sSearch_Document, sSearch_VersionNumber, sSearch_TraineeEmpCode, sSearch_TraineeName, sSearch_TrainingDate, sSearch_TrainerName, sSearch_Type);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objDocReports.Add(
                        new DocTrainingReportsObjects(
                        dt.Rows[i]["Document"].ToString(), Convert.ToInt32(dt.Rows[i]["VersionNumber"]), dt.Rows[i]["TraineeEmpCode"].ToString(), dt.Rows[i]["TraineeName"].ToString(),
                       dt.Rows[i]["TrainingDate"].ToString(), dt.Rows[i]["TrainerName"].ToString(), dt.Rows[i]["Type"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objDocReports
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetDocOnGoingTrainingReports()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var DocumentID = int.Parse(HttpContext.Current.Request.Params["DocumentID"]);

                var sSearch_Document = HttpContext.Current.Request.Params["sSearch_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch_VersionNumber = HttpContext.Current.Request.Params["sSearch_1"].ToString(CultureInfo.CurrentCulture);
                var sSearch_TraineeEmpCode = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
                var sSearch_TraineeName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
                var sSearch_TrainingStatus = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
                var sSearch_TrainerName = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
                var sSearch_Type = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);

                List<DocTrainingReportsObjects> objDocReports = new List<DocTrainingReportsObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetDocWiseOnGoingTrainingRecords(displayLength, displayStart, sortCol, sSortDir, sSearch, DocumentID, sSearch_Document,
                  sSearch_VersionNumber, sSearch_TraineeEmpCode, sSearch_TraineeName, sSearch_TrainingStatus, sSearch_TrainerName, sSearch_Type);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objDocReports.Add(
                        new DocTrainingReportsObjects(
                        Convert.ToInt32(dt.Rows[i]["VersionNumber"]), dt.Rows[i]["Document"].ToString(), dt.Rows[i]["TraineeEmpCode"].ToString(), dt.Rows[i]["TraineeName"].ToString(),
                       dt.Rows[i]["TrainingStatus"].ToString(), dt.Rows[i]["TrainerNames"].ToString(), dt.Rows[i]["Type"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objDocReports
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
