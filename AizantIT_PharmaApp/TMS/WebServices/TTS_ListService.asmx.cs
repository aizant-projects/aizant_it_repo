﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using TMS_BusinessLayer;
using TMS_BusinessObjects;

namespace AizantIT_PharmaApp.TMS.WebServices
{
    /// <summary>
    /// Summary description for TTS_ListService
    /// </summary>
    [WebService(Namespace = "http://Aizant.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class TTS_ListService : System.Web.Services.WebService
    {
        TMS_BAL objTMS_Bal;

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string TTS_CreationList()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var EmpID = int.Parse(HttpContext.Current.Request.Params["EmpID"]);
                var FilterType = int.Parse(HttpContext.Current.Request.Params["FilterType"]);
                List<TTS_CreationListObjects> objTraineeList = new List<TTS_CreationListObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.TTS_CreationListBal(displayLength, displayStart, sortCol, sSortDir, sSearch, EmpID, FilterType);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objTraineeList.Add(
                       new TTS_CreationListObjects(Convert.ToInt32(dt.Rows[i]["DeptID"]),
                       dt.Rows[i]["DepartmentName"].ToString(), Convert.ToInt32(dt.Rows[i]["DocumentID"]), dt.Rows[i]["DocumentName"].ToString(),
                      Convert.ToInt32(dt.Rows[i]["DocumentTypeID"]), dt.Rows[i]["DocumentType"].ToString(), dt.Rows[i]["TargetType"].ToString(), Convert.ToInt32(dt.Rows[i]["TargetTypeID"])));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objTraineeList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string TrainingSessionTrainees()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var TTS_TrainingSessionID = int.Parse(HttpContext.Current.Request.Params["TTS_TrainingSessionID"]);
                var EvaluateOn = int.Parse(HttpContext.Current.Request.Params["EvaluateOn"]);
                List<TrainingSessionEvaluation> objTraineeList = new List<TrainingSessionEvaluation>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetTraineesOnEvaluation(displayLength, displayStart, sortCol, sSortDir, sSearch, TTS_TrainingSessionID, EvaluateOn);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objTraineeList.Add(
                       new TrainingSessionEvaluation(Convert.ToInt32(dt.Rows[i]["TTS_TrainingSessionID"]),
                       Convert.ToInt32(dt.Rows[i]["TraineeID"]), Convert.ToInt32(dt.Rows[i]["EvaluationStatus"]),
                       Convert.ToInt32(dt.Rows[i]["TargetExamID"]), dt.Rows[i]["EmpCode"].ToString(),
                       dt.Rows[i]["Trainee"].ToString(), dt.Rows[i]["DepartmentName"].ToString(), Convert.ToInt32(dt.Rows[i]["Attempts"])));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objTraineeList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string ViewTrainingSessionDetails()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var TTS_TrainingSessionID = int.Parse(HttpContext.Current.Request.Params["TrainingSessionID"]);
                var ExamType = HttpContext.Current.Request.Params["ExamType"].ToString();
                var TTS_ID = HttpContext.Current.Request.Params["TTS_ID"].ToString();
                List<GetTrainingSessionDetailsObjects> objTraineeList = new List<GetTrainingSessionDetailsObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetTrainingSessionDetails(displayLength, displayStart, sortCol, sSortDir, sSearch, TTS_TrainingSessionID, ExamType,Convert.ToInt32(TTS_ID));
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objTraineeList.Add(
                       new GetTrainingSessionDetailsObjects(Convert.ToInt32(dt.Rows[i]["TTS_TrainingSessionID"]),
                       Convert.ToInt32(dt.Rows[i]["TraineeID"]), Convert.ToInt32(dt.Rows[i]["EvaluationStatus"]),
                       Convert.ToInt32(dt.Rows[i]["TargetExamID"]), dt.Rows[i]["EmpCode"].ToString(),
                       dt.Rows[i]["Trainee"].ToString(), dt.Rows[i]["DepartmentName"].ToString(), Convert.ToInt32(dt.Rows[i]["Attempts"]), dt.Rows[i]["StatusText"].ToString(), Convert.ToInt32(dt.Rows[i]["EmpFeedbackID"]), Convert.ToInt32(dt.Rows[i]["TrainerSatisfactory"]), dt.Rows[i]["EvaluationRemarks"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objTraineeList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string TTS_Pending()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var AuthorID = int.Parse(HttpContext.Current.Request.Params["AuthorID"]);
                int FilterType;
                var NotificationID = int.Parse(HttpContext.Current.Request.Params["Notification_ID"]);
                if (NotificationID == 0)
                {
                    FilterType = int.Parse(HttpContext.Current.Request.Params["FilterType"]);
                }
                else
                {
                    FilterType = 1;
                }
                List<TTS_PendingObjects> objEmpJrList = new List<TTS_PendingObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetPendingTTS_List(displayLength, displayStart, sortCol, sSortDir, sSearch, AuthorID, FilterType, NotificationID, out totalRecordCount);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objEmpJrList.Add(
                       new TTS_PendingObjects(Convert.ToInt32(dt.Rows[i]["TTS_ID"]),
                       dt.Rows[i]["DocumentName"].ToString(), dt.Rows[i]["DepartmentName"].ToString(), dt.Rows[i]["TypeOfTraining"].ToString(),
                       dt.Rows[i]["Reviewer"].ToString(), dt.Rows[i]["CurrentStatus"].ToString(), (Convert.ToInt32(dt.Rows[i]["StatusID"].ToString()))));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objEmpJrList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string TTS_ApprovalList()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var ReviewerID = int.Parse(HttpContext.Current.Request.Params["ReviewerID"]);
                int FilterType;
                var NotificationID = int.Parse(HttpContext.Current.Request.Params["Notification_ID"]);
                if (NotificationID == 0)
                {
                    FilterType = int.Parse(HttpContext.Current.Request.Params["FilterType"]);
                }
                else
                {
                    FilterType = 1;
                }
                //var NotificationID = int.Parse(HttpContext.Current.Request.Params["Notification_ID"]);
                List<TTS_ApprovalListObjects> objEmpJrList = new List<TTS_ApprovalListObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetApproveTTS_List(displayLength, displayStart, sortCol, sSortDir, sSearch, ReviewerID, FilterType, NotificationID, out totalRecordCount);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objEmpJrList.Add(
                       new TTS_ApprovalListObjects(Convert.ToInt32(dt.Rows[i]["TTS_ID"]),
                       dt.Rows[i]["DocumentName"].ToString(), dt.Rows[i]["DepartmentName"].ToString(), dt.Rows[i]["TypeOfTraining"].ToString(),
                       dt.Rows[i]["Author"].ToString(), dt.Rows[i]["CurrentStatus"].ToString(), (Convert.ToInt32(dt.Rows[i]["StatusID"].ToString()))));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objEmpJrList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string TTS_QA_ApprovedList()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var TrainerID = int.Parse(HttpContext.Current.Request.Params["TrainerID"]);
                var NotificationID = int.Parse(HttpContext.Current.Request.Params["Notification_ID"]);
                List<TTS_QA_ApprovedListObjects> objQA_ApprovedList = new List<TTS_QA_ApprovedListObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetQA_ApprovedTTS_List(displayLength, displayStart, sortCol, sSortDir, sSearch, TrainerID, NotificationID, out totalRecordCount);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objQA_ApprovedList.Add(
                       new TTS_QA_ApprovedListObjects(Convert.ToInt32(dt.Rows[i]["TTS_ID"]),
                       dt.Rows[i]["DocumentName"].ToString(), dt.Rows[i]["DepartmentName"].ToString(), dt.Rows[i]["TypeOfTraining"].ToString(),
                       dt.Rows[i]["Author"].ToString(), dt.Rows[i]["Reviewer"].ToString(), dt.Rows[i]["ApproveDate"].ToString(), dt.Rows[i]["StatusName"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objQA_ApprovedList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string TargetTrainingList()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

                var sSearch_DocumentName = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
                var sSearch_DeptCode = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
                var sSearch_TypeOfTraining = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
                var sSearch_TargetType = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
                var sSearch_Author = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
                var sSearch_Reviewer = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
                var sSearch_ApproveDate = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);
                var sSearch_StatusName = HttpContext.Current.Request.Params["sSearch_9"].ToString(CultureInfo.CurrentCulture);

                var EMP_ID = int.Parse(HttpContext.Current.Request.Params["EMP_ID"]);
                int Year;
                int.TryParse(HttpContext.Current.Request.Params["Year"], out Year);
                int FilterType;
                int.TryParse(HttpContext.Current.Request.Params["FilterType"], out FilterType);
                int DeptID;
                int.TryParse(HttpContext.Current.Request.Params["DeptID"], out DeptID);
                int Month;
                int.TryParse(HttpContext.Current.Request.Params["Month"], out Month);
                var TargetType=(HttpContext.Current.Request.Params["TargetType"].ToString());
                var Status = (HttpContext.Current.Request.Params["Status"].ToString());
                List<TTS_ListObjects> objQA_ApprovedList = new List<TTS_ListObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                int SubRefID = 0;
                DataTable dt = objTMS_Bal.GetQA_ApprovedTTS_ListForHodAndQa(displayLength, displayStart, sortCol, sSortDir, sSearch, sSearch_DocumentName, sSearch_DeptCode, sSearch_TypeOfTraining, sSearch_TargetType, sSearch_Author, sSearch_Reviewer, sSearch_ApproveDate, sSearch_StatusName, EMP_ID, Year,FilterType, DeptID, Month, TargetType, Status,SubRefID , out totalRecordCount);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objQA_ApprovedList.Add(
                       new TTS_ListObjects(Convert.ToInt32(dt.Rows[i]["RowNumber"]),Convert.ToInt32(dt.Rows[i]["TTS_ID"]),
                       dt.Rows[i]["DocumentName"].ToString(), dt.Rows[i]["DeptCode"].ToString(), dt.Rows[i]["TypeOfTraining"].ToString(), dt.Rows[i]["TargetType"].ToString(),
                       dt.Rows[i]["Author"].ToString(), dt.Rows[i]["Reviewer"].ToString(), dt.Rows[i]["ApproveDate"].ToString(), dt.Rows[i]["StatusName"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objQA_ApprovedList
                };
                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        // for getting the list of target Training Assigned Trainees
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string TargetTrainingAssignedTraineeList()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var TTS_ID = int.Parse(HttpContext.Current.Request.Params["TTS_ID"]);
                var TTSSession_ID = int.Parse(HttpContext.Current.Request.Params["TTS_TrainingSessionsID"]);
                List<TTS_TraineeAsignedListObjects> objTTS_TraineeAssignedList = new List<TTS_TraineeAsignedListObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetTTS_TraineeAssignedList(displayLength, displayStart, sortCol, sSortDir, sSearch, TTS_ID, TTSSession_ID, out totalRecordCount);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objTTS_TraineeAssignedList.Add(
                       new TTS_TraineeAsignedListObjects(Convert.ToInt32(dt.Rows[i]["Trainee_ID"]),
                       dt.Rows[i]["Trainee_Code"].ToString(), dt.Rows[i]["Trainee_Name"].ToString(), dt.Rows[i]["Dept_Code"].ToString(), dt.Rows[i]["ExamType"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objTTS_TraineeAssignedList
                };
                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //for getting the data to Trainee Classroom session List
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string TTS_Trainee_SessionList()
        {
            try
            { 
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var TraineeID = int.Parse(HttpContext.Current.Request.Params["TraineeID"]);
                int DeptID;
                int.TryParse(HttpContext.Current.Request.Params["DeptID"], out DeptID);
                int FilterType;
                int.TryParse(HttpContext.Current.Request.Params["FilterType"], out FilterType);
                var TargetType = (HttpContext.Current.Request.Params["TargetType"].ToString());
                int Year;
                int.TryParse(HttpContext.Current.Request.Params["Year"], out Year);
                var Status = (HttpContext.Current.Request.Params["Status"].ToString());
                List<TTS_Trainee_SessionListObjects> objTrainee_SessionList = new List<TTS_Trainee_SessionListObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetTraineeSession_List(displayLength, displayStart, sortCol, sSortDir, sSearch, TraineeID, DeptID, FilterType, TargetType, Year, Status, out totalRecordCount);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }               
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objTrainee_SessionList.Add(
                       new TTS_Trainee_SessionListObjects(Convert.ToInt32(dt.Rows[i]["TTS_ID"]), 
                       Convert.ToInt32(dt.Rows[i]["TTS_ClassRoomTrainingSessionID"]), Convert.ToInt32(dt.Rows[i]["TargetExamID"]),
                       Convert.ToInt32(dt.Rows[i]["DocumentID"]), Convert.ToInt32(dt.Rows[i]["DocumentVersionID"]), dt.Rows[i]["Document"].ToString(), dt.Rows[i]["DeptCode"].ToString(),
                       dt.Rows[i]["TrainingDate"].ToString(), dt.Rows[i]["StartTime"].ToString(), dt.Rows[i]["EndTime"].ToString(), 
                       dt.Rows[i]["Trainer"].ToString(), dt.Rows[i]["ExamType"].ToString(), dt.Rows[i]["Remarks"].ToString(), dt.Rows[i]["TypeOfTraining"].ToString(),  
                       dt.Rows[i]["StatusName"].ToString(),Convert.ToInt32(dt.Rows[i]["EmpFeedbackID"])));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objTrainee_SessionList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
        public void UpdateTraineeDocRead(int TraineeReadingDocID)
        {
            try
            {
                Session["TraineeReadingDocID"] = TraineeReadingDocID;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
        public string CheckTraineeActiveDocRead(int DocID)
        {
            try
            {
                string result = "0";
                if (Session["TraineeReadingDocID"] != null)
                {
                    result = Session["TraineeReadingDocID"].ToString();
                }
                return result;     
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //for getting the data to Trainee Online session List
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string TTS_Trainee_OnlineSessionList()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var TraineeID = int.Parse(HttpContext.Current.Request.Params["TraineeID"]);
                int DeptID;
                int.TryParse(HttpContext.Current.Request.Params["DeptID"], out DeptID);
                int FilterType;
                int.TryParse(HttpContext.Current.Request.Params["FilterType"], out FilterType);
                var TargetType = (HttpContext.Current.Request.Params["TargetType"].ToString());
                int Year;
                int.TryParse(HttpContext.Current.Request.Params["Year"], out Year);
                var Status = (HttpContext.Current.Request.Params["Status"].ToString());
                List<TTS_Trainee_OnlineSessionListObjects> objTrainee_SessionList = new List<TTS_Trainee_OnlineSessionListObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetTraineeOnlineSession_List(displayLength, displayStart, sortCol, sSortDir, sSearch, TraineeID, DeptID, FilterType, TargetType, Year, Status, out totalRecordCount);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objTrainee_SessionList.Add(
                       new TTS_Trainee_OnlineSessionListObjects(Convert.ToInt32(dt.Rows[i]["TTS_ID"]),Convert.ToInt32(dt.Rows[i]["TTS_OnlineTrainingSessionID"]),
                      Convert.ToInt32(dt.Rows[i]["TargetExamID"]), Convert.ToInt32(dt.Rows[i]["DocumentID"]), Convert.ToInt32(dt.Rows[i]["DocumentVersionID"]), dt.Rows[i]["Document"].ToString(), dt.Rows[i]["DeptCode"].ToString(),
                       dt.Rows[i]["DueDate"].ToString(), dt.Rows[i]["Trainer"].ToString(), dt.Rows[i]["Remarks"].ToString(), Convert.ToBoolean(dt.Rows[i]["SelfComplete"]),
                       dt.Rows[i]["StatusName"].ToString(), Convert.ToInt32(dt.Rows[i]["Status"])));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objTrainee_SessionList
                };
                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        //for getting the data to TTS_Trainer evaluation List
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string TTS_Trainer_EvaluationList()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

                //var sSearch_TTS_TrainingSessionID = HttpContext.Current.Request.Params["sSearch_1"].ToString();
                //var sSearch_DocumentID = HttpContext.Current.Request.Params["sSearch_2"].ToString();
                var sSearch_Document = HttpContext.Current.Request.Params["sSearch_3"].ToString();
                var sSearch_DeptCode = HttpContext.Current.Request.Params["sSearch_4"].ToString();
                var sSearch_TrainingDate = HttpContext.Current.Request.Params["sSearch_5"].ToString();
                var sSearch_StartTime = HttpContext.Current.Request.Params["sSearch_6"].ToString();
                var sSearch_EndTime = HttpContext.Current.Request.Params["sSearch_7"].ToString();
                var sSearch_Trainer = HttpContext.Current.Request.Params["sSearch_8"].ToString();
                var sSearch_TypeOfTraining = HttpContext.Current.Request.Params["sSearch_9"].ToString();
                var sSearch_ExamType = HttpContext.Current.Request.Params["sSearch_10"].ToString();

                var TrainerID = int.Parse(HttpContext.Current.Request.Params["TrainerID"]);
                var Evaluated = (HttpContext.Current.Request.Params["OnlyEvalutedRecords"]);
                List<TTS_Trainer_EvaluationListObjects> objTrainer_EvaluationList = new List<TTS_Trainer_EvaluationListObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetTrainerEvaluationTTS_List(displayLength, displayStart, sortCol, sSortDir, sSearch,
                 sSearch_Document, sSearch_DeptCode, sSearch_TrainingDate, sSearch_StartTime,
                 sSearch_EndTime, sSearch_Trainer, sSearch_TypeOfTraining, sSearch_ExamType,TrainerID, Evaluated, out totalRecordCount);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objTrainer_EvaluationList.Add(
                       new TTS_Trainer_EvaluationListObjects(Convert.ToInt32(dt.Rows[i]["TTS_ID"]), Convert.ToInt32(dt.Rows[i]["TTS_TrainingSessionID"]), Convert.ToInt32(dt.Rows[i]["DocumentID"]),
                       dt.Rows[i]["Document"].ToString(), dt.Rows[i]["DeptCode"].ToString(), dt.Rows[i]["TrainingDate"].ToString(),
                       dt.Rows[i]["StartTime"].ToString(), dt.Rows[i]["EndTime"].ToString(),
                       dt.Rows[i]["Trainer"].ToString(), dt.Rows[i]["TypeOfTraining"].ToString(), dt.Rows[i]["ExamType"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objTrainer_EvaluationList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetTTS_TraingSessionList()
        {
            try
            {

                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var TTSID = int.Parse(HttpContext.Current.Request.Params["TTS_ID"].ToString(CultureInfo.CurrentCulture));
                List<TTS_TargetSessionsObjects> objTargetSessionList = new List<TTS_TargetSessionsObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetTTS_TraingSessionList(displayLength, displayStart, sortCol, sSortDir, sSearch, TTSID, out totalRecordCount);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objTargetSessionList.Add(
                       new TTS_TargetSessionsObjects(Convert.ToInt32(dt.Rows[i]["TTS_TrainingSessionsID"]),
                       dt.Rows[i]["TrainingDate"].ToString(), dt.Rows[i]["StartTime"].ToString(), dt.Rows[i]["EndTime"].ToString(), dt.Rows[i]["ExamType"].ToString(),
                       dt.Rows[i]["Trainer"].ToString(), dt.Rows[i]["StatusName"].ToString(), (Convert.ToInt32(dt.Rows[i]["StatusID"].ToString()))));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objTargetSessionList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}

