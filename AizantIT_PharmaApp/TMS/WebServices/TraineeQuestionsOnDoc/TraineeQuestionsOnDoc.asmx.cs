﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using TMS_BusinessLayer;
using TMS_BusinessObjects;
using TMS_BusinessObjects.TQ_OnDocs;

namespace AizantIT_PharmaApp.TMS.WebServices.TraineeQuestionsOnDoc
{
    /// <summary>
    /// Summary description for TraineeQuestionsOnDoc
    /// </summary>
    [WebService(Namespace = "http://Aizant.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class TraineeQuestionsOnDoc : System.Web.Services.WebService
    {
        TMS_BAL objTMS_Bal;
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetDocmentsWithTraineeQuestions()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString();
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString();
                var TrainerEmpID = int.Parse(HttpContext.Current.Request.Params["TrainerEmpID"]);
                List<DocsWithTraineeQuestions> objEmpJrList = new List<DocsWithTraineeQuestions>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetTQ_Docs(displayLength, displayStart, sortCol, sSortDir, sSearch, TrainerEmpID);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objEmpJrList.Add(
                       new DocsWithTraineeQuestions(Convert.ToInt32(dt.Rows[i]["DocID"]), dt.Rows[i]["Document"].ToString(),
                       dt.Rows[i]["Department"].ToString(),dt.Rows[i]["TraineeQuestions"].ToString())
                       );
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objEmpJrList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [WebMethod]
        public string getTraineeRaisedQuestionsonDocumentToTrainee(string BaseType, string BaseID, string DocID, string TraineeID)
        {
            objTMS_Bal = new TMS_BAL();
            DataSet dsTraineeRaisedQuestions = objTMS_Bal.getTraineeRaisedQuestionstoTraineeBal(BaseType, BaseID, DocID, TraineeID);
            List<DocTraineeRaisedQuestions> DocTraineeAnsweredQuestions = new List<DocTraineeRaisedQuestions>();
            foreach (DataRow drresult in dsTraineeRaisedQuestions.Tables[1].Rows)
            {
                DocTraineeAnsweredQuestions.Add(new DocTraineeRaisedQuestions
                {
                    QuestionID = drresult["TraineeQuestionID"].ToString(),
                    QuestionDescription = drresult["Question"].ToString(),
                    QuestionAnsweredBy = drresult["AnsweredBy"].ToString(),
                    QuestionAnsweredDate = drresult["AnsweredDate"].ToString(),
                    IsPreviousQuestionsShow = drresult["IsPreviousQuestionsShow"].ToString(),
                });
            }
            List<DocTraineeRaisedQuestions> DocTraineeUnAnsweredQuestions = new List<DocTraineeRaisedQuestions>();
            foreach (DataRow drresult in dsTraineeRaisedQuestions.Tables[0].Rows)
            {
                DocTraineeUnAnsweredQuestions.Add(new DocTraineeRaisedQuestions
                {
                    QuestionID = drresult["TraineeQuestionID"].ToString(),
                    QuestionDescription = drresult["Question"].ToString(),
                    QuestionRaisedDate = drresult["RaisedDate"].ToString(),
                });
            }
            var result = new
            {
                AnsweredQuestions = DocTraineeAnsweredQuestions,
                UnAnsweredQuestions = DocTraineeUnAnsweredQuestions
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        [WebMethod]
        public string getTraineeRaisedQuestionsonDocumentToTrainer(string TrainerID, string DocID)
        {

            objTMS_Bal = new TMS_BAL();
            DataSet dsTraineeQuestions = objTMS_Bal.getTraineeRaisedQuestionstoTrainerBal(TrainerID, DocID);
            List<DocTraineeRaisedQuestions> DocTraineeAnsweredQuestions = new List<DocTraineeRaisedQuestions>();
            foreach (DataRow drresult in dsTraineeQuestions.Tables[1].Rows) // Answered /Defined Comment as answered
            {
                DocTraineeAnsweredQuestions.Add(new DocTraineeRaisedQuestions
                {
                    QuestionID = drresult["TraineeQuestionID"].ToString(),
                    QuestionDescription = drresult["Question"].ToString(),
                    QuestionRaisedBy = drresult["RaisedBy"].ToString(),
                    QuestionRaisedDate = drresult["RaisedDate"].ToString(),
                    QuestionAnsweredBy = drresult["AnsweredBy"].ToString(),
                    QuestionAnsweredDate = drresult["AnsweredDate"].ToString(),
                    IsQuestionMarkedAsFAQ = drresult["IsMarkedasFAQ"].ToString(),
                    IsPreviousQuestionsShow = drresult["IsPreviousQuestionsShow"].ToString(),
                });
            }
            List<DocTraineeRaisedQuestions> DocTraineeUnAnsweredQuestions = new List<DocTraineeRaisedQuestions>();
            foreach (DataRow drresult in dsTraineeQuestions.Tables[0].Rows)  //Unanswered /Open
            {
                DocTraineeUnAnsweredQuestions.Add(new DocTraineeRaisedQuestions
                {
                    QuestionID = drresult["TraineeQuestionID"].ToString(),
                    QuestionDescription = drresult["Question"].ToString(),
                    QuestionRaisedBy = drresult["RaisedBy"].ToString(),
                    QuestionRaisedDate = drresult["RaisedDate"].ToString(),
                });
            }
            var result = new
            {
                AnsweredQuestions = DocTraineeAnsweredQuestions,
                UnAnsweredQuestions = DocTraineeUnAnsweredQuestions
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
            //var result = new
            //{
            //    AnsweredQuestions = dsTraineeQuestions.Tables[0].Rows,
            //    UnAnsweredQuestions = dsTraineeQuestions.Tables[1].Rows
            //};
            //JavaScriptSerializer js = new JavaScriptSerializer();
            //return js.Serialize(result);

        }
        [WebMethod]
        public string GetLatestAnswertoTQ(string TQ_ID)
        {
            objTMS_Bal = new TMS_BAL();
            DataTable dtDocFAQsAnswer = objTMS_Bal.getLatestAnswertoTQ(TQ_ID);
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(dtDocFAQsAnswer.Rows[0]["Answer"]);
        }

        [WebMethod]
        public string GetTQ_PreviousAnswer(string TQ_ID)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            if (TQ_ID != "")
            {
                objTMS_Bal = new TMS_BAL();
                DataTable dtTQ_PA = objTMS_Bal.GetTQ_PreviousAnswers(Convert.ToInt32(TQ_ID));
                if (dtTQ_PA.Rows.Count > 0)
                {
                    List<TQ_PreviousAnswers> objLstTQ_PA = new List<TQ_PreviousAnswers>();
                    foreach (DataRow drItems in dtTQ_PA.Rows)
                    {
                        objLstTQ_PA.Add(new TQ_PreviousAnswers(
                          drItems["Employee"].ToString(),
                          drItems["MarkedOn"].ToString(),
                          drItems["Answer"].ToString()
                        ));
                    }
                    var result = new
                    {
                        PreviousAnswers = objLstTQ_PA
                    };
                    return js.Serialize(result);
                }
            }
            return "";
        }
        [WebMethod]
        public string GetTQ_Conversation(string TQ_ID)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            objTMS_Bal = new TMS_BAL();
            DataTable dtTQ_PA = objTMS_Bal.GetTQ_Conversation(TQ_ID);
            List<TQ_ConversationListObjects> objLstTQ_Conversation = new List<TQ_ConversationListObjects>();
            foreach (DataRow drItems in dtTQ_PA.Rows)
            {
                objLstTQ_Conversation.Add(new TQ_ConversationListObjects(
                  drItems["TQ_CommentID"].ToString(),
                  drItems["Comment"].ToString(),
                  drItems["Commentor"].ToString(),
                  drItems["CommentorRole"].ToString(),
                  drItems["MarkedAsAnswer"].ToString()
                ));
            }
            var result = new
            {
                TQ_Conversation = objLstTQ_Conversation
            };
            return js.Serialize(result);
        }

        [WebMethod]
        public string PostTQ_ToFAQ(string[] strTQ_IDs, string EmpID)
        {
            DataTable dtTQ_IDs = new DataTable();
            dtTQ_IDs.Columns.Add("TQ_ID");
            if (strTQ_IDs.Length > 0)
            {
                DataRow drTqID;
                foreach (var itemTQ_ID in strTQ_IDs)
                {
                    drTqID = dtTQ_IDs.NewRow();
                    drTqID["TQ_ID"] = itemTQ_ID;
                    dtTQ_IDs.Rows.Add(drTqID);
                }
            }

            int resultStatus = 0; string strMsg = "0";
            if (dtTQ_IDs.Rows.Count > 0)
            {
                objTMS_Bal = new TMS_BAL();
                objTMS_Bal.PostTQ_ToFAQs(dtTQ_IDs, Convert.ToInt32(EmpID), out resultStatus);
            }
            if (resultStatus == 1)
            {
                strMsg = "1"; // Only some of your selected Question(s) are not Added to FAQs
            }
            else if (resultStatus == 2)
            {
                strMsg = "2"; // All the Selected Question(s) are Added to FAQs
            }
            return strMsg;
        }

        [WebMethod]
        public string MarkCommentAsAnswer(string MarkedCommentID, string EmpID, string TQ_ID)
        {
            int resultStatus = 0;
            if (MarkedCommentID != "0" && EmpID != "0" && TQ_ID != "0")
            {
                objTMS_Bal = new TMS_BAL();
                objTMS_Bal.MarkCommentAsAnswer(MarkedCommentID, EmpID, TQ_ID, out resultStatus);
            }
            return resultStatus.ToString();
        }
    }
}
