﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using TMS_BusinessLayer;
using TMS_BusinessObjects;

namespace AizantIT_PharmaApp.TMS.WebServices
{
    /// <summary>
    /// Summary description for AssignedATC_Service
    /// </summary>
    [WebService(Namespace = "http://Aizant.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class AssignedATC_Service : System.Web.Services.WebService
    {
        TMS_BAL objTMS_Bal;        
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string AssignedATC()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var AuthorID = int.Parse(HttpContext.Current.Request.Params["AuthorID"]);
                var NotificationID = int.Parse(HttpContext.Current.Request.Params["Notification_ID"]);
                List<CreateATC_Objects> objEmpJrList = new List<CreateATC_Objects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetAssigned_ATC_List(displayLength, displayStart, sortCol, sSortDir, sSearch, AuthorID, NotificationID, out totalRecordCount);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objEmpJrList.Add(
                       new CreateATC_Objects(Convert.ToInt32(dt.Rows[i]["ATC_ID"]), Convert.ToInt32(dt.Rows[i]["DeptID"]),
                       dt.Rows[i]["DepartmentName"].ToString(), Convert.ToInt32(dt.Rows[i]["CalendarYear"]),
                       dt.Rows[i]["InitiatedBy"].ToString(), dt.Rows[i]["InitiatedDate"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objEmpJrList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }            
        }
    }
}
