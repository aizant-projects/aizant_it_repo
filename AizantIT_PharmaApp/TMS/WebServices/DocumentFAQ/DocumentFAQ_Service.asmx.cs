﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using TMS_BusinessLayer;
using TMS_BusinessObjects;

namespace AizantIT_PharmaApp.TMS.WebServices.Document_FAQ
{
    /// <summary>
    /// Summary description for DocumentFAQ_Service
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class DocumentFAQ_Service : System.Web.Services.WebService
    {
        TMS_BAL objTMS_Bal;

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string DocFAQ_List()
        {
            try
            {

                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var DocID = int.Parse(HttpContext.Current.Request.Params["DocID"]);
                List<DocFAQ_ListObjects> objDocFAQ_List = new List<DocFAQ_ListObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetDocFAQs_List(displayLength, displayStart, sortCol, sSortDir, sSearch, DocID);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objDocFAQ_List.Add(
                       new DocFAQ_ListObjects(Convert.ToInt32(dt.Rows[i]["DocQuestionID"]),
                       dt.Rows[i]["DocQuestion"].ToString(), dt.Rows[i]["CreatedBy"].ToString(),
                       dt.Rows[i]["CreatedDate"].ToString(), dt.Rows[i]["CurrentStatusName"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objDocFAQ_List
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string DocFAQ_History_List()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var DocFaqID = int.Parse(HttpContext.Current.Request.Params["DocFaqID"]);
                List<DocFAQ_History_ListObjects> objDocFAQ_List = new List<DocFAQ_History_ListObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetDocFAQ_History_List(displayLength, displayStart, sortCol, sSortDir, sSearch, DocFaqID
                    //, out totalRecordCount
                    );
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objDocFAQ_List.Add(
                       new DocFAQ_History_ListObjects(Convert.ToInt32(dt.Rows[i]["DocFAQ_ID"]),
                       dt.Rows[i]["ModifiedBy"].ToString(), dt.Rows[i]["ModifiedDate"].ToString(),
                       dt.Rows[i]["ModifiedReason"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objDocFAQ_List
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string DocFAQs(string PageIndex, string PageSize, string DocID, String Search)
        {
            List<DocFAQ_Objects> DocFAQ = new List<DocFAQ_Objects>();
            objTMS_Bal = new TMS_BAL();
            DataTable dtDocFAQs = objTMS_Bal.getDocFAQs(PageIndex,PageSize,DocID,Search,out int RecordCount);
            foreach (DataRow drresult in dtDocFAQs.Rows)
            {
                DocFAQ.Add(new DocFAQ_Objects
                {
                    QuestionID = drresult["DocFaqID"].ToString(),
                    QuestionDescription = drresult["Question"].ToString(),
                });
            }
            var result = new
            {
                recordsCount = RecordCount,
                records = DocFAQ
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }

        [WebMethod]
        public string getDocFAQsAnswer(string QuestionID)
        {
            objTMS_Bal = new TMS_BAL();
            DataTable dtDocFAQsAnswer = objTMS_Bal.getDocFAQsAnswer(QuestionID);
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(dtDocFAQsAnswer.Rows[0]["Answer"]);
        }
    }
}
