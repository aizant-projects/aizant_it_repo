﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using TMS_BusinessLayer;
using TMS_BusinessObjects;

namespace AizantIT_PharmaApp.TMS.WebServices
{
    /// <summary>
    /// Summary description for InitializeATC_ListService
    /// </summary>
    [WebService(Namespace = "http://Aizant.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class InitializeATC_ListService : System.Web.Services.WebService
    {
        TMS_BAL objTMS_Bal;

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string Initialize_ATC()
        {
            try
            {

                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var EmpID = int.Parse(HttpContext.Current.Request.Params["EmpID"]);
                List<InitializeATC_ListObjects> objEmpJrList = new List<InitializeATC_ListObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetInitialize_ATC(displayLength, displayStart, sortCol, sSortDir, sSearch, EmpID);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objEmpJrList.Add(
                       new InitializeATC_ListObjects(Convert.ToInt32(dt.Rows[i]["ATC_ID"]), Convert.ToInt32(dt.Rows[i]["DeptID"]),
                       dt.Rows[i]["DeptCode"].ToString(), dt.Rows[i]["DeptName"].ToString(), Convert.ToInt32(dt.Rows[i]["CalendarYear"]),
                       dt.Rows[i]["InitiatedBy"].ToString(), dt.Rows[i]["InitiatedDate"].ToString(), Convert.ToInt32(dt.Rows[i]["ATC_Author_ID"]),
                       dt.Rows[i]["Author"].ToString(), Convert.ToInt32(dt.Rows[i]["ATC_HOD_Approval_ID"]), dt.Rows[i]["Reviewer"].ToString(),
                       Convert.ToInt32(dt.Rows[i]["ATC_QA_Approval_ID"]), dt.Rows[i]["Approver"].ToString(), Convert.ToInt32(dt.Rows[i]["CurrentStatus"])));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objEmpJrList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string ATC_HistoryList()
        {
            try
            {

                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var ATC_ID = int.Parse(HttpContext.Current.Request.Params["ATC_ID"]);
                List<ATC_HistoryListObjects> objATC_HistoryList = new List<ATC_HistoryListObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.get_ATC_HistoryList(displayLength, displayStart, sortCol, sSortDir, sSearch, ATC_ID);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objATC_HistoryList.Add(
                       new ATC_HistoryListObjects(Convert.ToInt32(dt.Rows[i]["ATC_HistoryID"]),dt.Rows[i]["ActionName"].ToString(), 
                       dt.Rows[i]["ActionByName"].ToString(),dt.Rows[i]["RoleName"].ToString(), dt.Rows[i]["ActionDate"].ToString(),
                       dt.Rows[i]["Remarks"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objATC_HistoryList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
