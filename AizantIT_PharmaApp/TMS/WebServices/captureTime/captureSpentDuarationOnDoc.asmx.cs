﻿using System;
using System.Web.Script.Services;
using System.Web.Services;
using TMS_BusinessLayer;

namespace AizantIT_PharmaApp.TMS.WebServices.readTimer
{
    /// <summary>
    /// Summary description for captureSpentDuarationOnDoc
    /// </summary>
    [WebService(Namespace = "http://Aizant.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class captureSpentDuarationOnDoc : System.Web.Services.WebService
    {
        TMS_BAL objTMS_Bal;
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void RecordSpentDuarationOnDoc(string JR_ID, string docVersionID, string docID, string Hours, string Minutes, string Seconds)
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                objTMS_Bal.UpdateDocDuration(JR_ID, docVersionID, docID, Hours, Minutes, Seconds);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void UpdateTraineeReadTimeOnDoc(string TTS_ID, string TraineeIDs, string Hours, string Minutes, string Seconds)
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                objTMS_Bal.UpdateTraineeDocDuration(TTS_ID, TraineeIDs, Hours, Minutes, Seconds);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
