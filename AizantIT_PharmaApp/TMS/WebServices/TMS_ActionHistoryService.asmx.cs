﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using TMS_BusinessLayer;
using TMS_BusinessObjects;

namespace AizantIT_PharmaApp.TMS.WebServices
{
    /// <summary>
    /// Summary description for TMS_ActionHistoryService
    /// </summary>
    [WebService(Namespace = "http://Aizant.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)] 
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class TMS_ActionHistoryService : System.Web.Services.WebService
    {
        TMS_BAL objTMS_Bal;
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetTMS_ActionHistory()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var BaseID = HttpContext.Current.Request.Params["Base_ID"].ToString(CultureInfo.CurrentCulture);
                var Table_ID = HttpContext.Current.Request.Params["Table_ID"].ToString(CultureInfo.CurrentCulture);
                List<TMS_ActionHistoryObjects> objTMS_ActionHistory = new List<TMS_ActionHistoryObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetTMS_ActionHistory(displayLength, displayStart, sortCol, sSortDir, sSearch, Table_ID, BaseID);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objTMS_ActionHistory.Add(
                        new TMS_ActionHistoryObjects(dt.Rows[i]["ActionHistoryID"].ToString(), dt.Rows[i]["ActionStatus"].ToString(), dt.Rows[i]["ActionBy"].ToString(), dt.Rows[i]["ActionRole"].ToString(), dt.Rows[i]["ActionDate"].ToString(),
                        dt.Rows[i]["Remarks"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objTMS_ActionHistory
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
