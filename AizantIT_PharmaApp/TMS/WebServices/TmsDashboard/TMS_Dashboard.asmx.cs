﻿using AizantIT_PharmaApp.Common;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Services;
using TMS_BusinessLayer;
using TMS_BusinessObjects.TMS_Dashboard;

namespace AizantIT_PharmaApp.TMS.WebServices.TmsDashboard
{
    /// <summary>
    /// Summary description for TMS_Dashboard
    /// </summary>
    [WebService(Namespace = "http://www.aizantitanalytics.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class TMS_Dashboard : System.Web.Services.WebService
    {
        TMS_BAL objTMS_Bal;
        // For getting Documents Count along with Month Based on Dept
        [WebMethod]
        public string GetATC_DocCountforEachMonthByDept(string DeptID)
        {
            if (DeptID != "" || DeptID != "0")
            {
                objTMS_Bal = new TMS_BAL();
                DataTable dtDocCount = objTMS_Bal.GetDocCountandMonth(DeptID);
                if (dtDocCount.Rows.Count > 0)
                {
                    StringBuilder sbJson = new StringBuilder();
                    sbJson.Append("{");
                    for (int i = 1; i <= 12; i++)
                    {
                        DataRow[] dr = dtDocCount.Select("Month=" + i);                     
                        if (dr.Length>0)
                        {                           
                            sbJson.AppendFormat("\"{0}\":\"{1}\"", HelpClass.GetMonthName(i.ToString(),false), dr[0]["DocumentsCount"].ToString());
                        }
                        else
                        {
                            sbJson.AppendFormat("\"{0}\":\"{1}\"", HelpClass.GetMonthName(i.ToString(),false),0);
                        }
                        if (i!=12)
                        {
                            sbJson.Append(",");
                        }
                    }
                    sbJson.Append("}");                    
                    return sbJson.ToString();
                }
            }
            return "";
        }

        // For getting Document Details Based on Dept and month
        [WebMethod]
        public string GetATC_DocDetailsByDeptAndMonth(string DeptID, string MonthNo)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            if (DeptID != "" || DeptID != "0" || MonthNo != "")
            {
                objTMS_Bal = new TMS_BAL();
                DataTable dtDocumentDetails = objTMS_Bal.GetATC_DocumentDetails(DeptID, MonthNo);
                //if (dtDocumentDetails.Rows.Count > 0)
                //{
                    List<ATC_DocumentDeatils> objDocumentDetails = new List<ATC_DocumentDeatils>();
                    foreach (DataRow drItems in dtDocumentDetails.Rows)
                    {
                        objDocumentDetails.Add(new ATC_DocumentDeatils(
                          drItems["Document"].ToString(),
                          drItems["DocumentType"].ToString()
                        ));
                    }
                    var result = new
                    {
                        DocumentDetails = objDocumentDetails
                    };
                    return js.Serialize(result);
                //}
            }
            return "";
        }
    }
}
