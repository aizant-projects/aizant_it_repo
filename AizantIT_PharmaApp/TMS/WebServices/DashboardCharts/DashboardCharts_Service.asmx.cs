﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using TMS_BusinessLayer;
using TMS_BusinessObjects;

namespace AizantIT_PharmaApp.TMS.WebServices.DashboardCharts
{
    /// <summary>
    /// Summary description for DashboardCharts_Service
    /// </summary>
    [WebService(Namespace = "http://Aizant.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class DashboardCharts_Service : System.Web.Services.WebService
    {

        TMS_BAL objTMS_Bal;
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetCompletedandPendingCount()
        {
            try
            {
                var Year =int.Parse(HttpContext.Current.Request.Params["Year"]);
                var EmpID =int.Parse(HttpContext.Current.Request.Params["EmpID"]);
                var DeptID =int.Parse(HttpContext.Current.Request.Params["DeptID"]);
                List<Dashborad_ChartsObjects> objChart = new List<Dashborad_ChartsObjects>();
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetCompletedandPendingCountBal(Year, EmpID, DeptID);
               
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objChart.Add(
                        new Dashborad_ChartsObjects(dt.Rows[i]["EventName"].ToString(), Convert.ToInt32(dt.Rows[i]["Pending"]),
                        Convert.ToInt32(dt.Rows[i]["Completed"])));
                }
                JavaScriptSerializer js = new JavaScriptSerializer();
                string JsonStr= js.Serialize(objChart);
                return js.Serialize(objChart);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //To get JR and Target Over Due Records
         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetOverDueCount()
        {
            try
            {
                var EmpID = int.Parse(HttpContext.Current.Request.Params["EmpID"]);
                var DeptID = int.Parse(HttpContext.Current.Request.Params["DeptID"]);
                List<DashboardOverDueCount_Objects> objChart = new List<DashboardOverDueCount_Objects>();
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetOverDueCountBal(EmpID, DeptID);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objChart.Add(
                        new DashboardOverDueCount_Objects(dt.Rows[i]["EventName"].ToString(), Convert.ToInt32(dt.Rows[i]["OverDue"])));
                }
                JavaScriptSerializer js = new JavaScriptSerializer();
                string JsonStr = js.Serialize(objChart);
                return js.Serialize(objChart);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetCompletedandPendingCountForATC()
        {
            try
            {
                var Year = int.Parse(HttpContext.Current.Request.Params["Year"]);
                var EmpID = int.Parse(HttpContext.Current.Request.Params["EmpID"]);
                var DeptID = int.Parse(HttpContext.Current.Request.Params["DeptID"]);
                List<ATCDashboardChart_Objects> objChart = new List<ATCDashboardChart_Objects>();
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetCompletedandPendingCountForATCBal(Year, EmpID, DeptID);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objChart.Add(
                        new ATCDashboardChart_Objects(dt.Rows[i]["MonthName"].ToString(), Convert.ToInt32(dt.Rows[i]["Pending"]),
                        Convert.ToInt32(dt.Rows[i]["Completed"])));
                }
                JavaScriptSerializer js = new JavaScriptSerializer();
                string JsonStr = js.Serialize(objChart);
                return js.Serialize(objChart);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetTraineeOverDueCount()
        {
            try
            {
                var EmpID = int.Parse(HttpContext.Current.Request.Params["EmpID"]);
                var DeptID = int.Parse(HttpContext.Current.Request.Params["DeptID"]);
                List<TraineeOverDueCount_Objects> objChart = new List<TraineeOverDueCount_Objects>();
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetTraineeOverDueCountBal(EmpID, DeptID);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objChart.Add(
                        new TraineeOverDueCount_Objects(dt.Rows[i]["EventName"].ToString(), Convert.ToInt32(dt.Rows[i]["OverDue"])));
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                string JsonStr = js.Serialize(objChart);
                return js.Serialize(objChart);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetTraineePendingCount()
        {
            try
            {
                var EmpID = int.Parse(HttpContext.Current.Request.Params["EmpID"]);
                var Year = int.Parse(HttpContext.Current.Request.Params["Year"]);
                var DeptID = int.Parse(HttpContext.Current.Request.Params["DeptID"]);
                List<TraineePendingCount_Objects> objChart = new List<TraineePendingCount_Objects>();
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetTraineePendingCountBal(EmpID, Year, DeptID);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objChart.Add(
                        new TraineePendingCount_Objects(dt.Rows[i]["EventName"].ToString(), Convert.ToInt32(dt.Rows[i]["Pending"]),
                        Convert.ToInt32(dt.Rows[i]["Completed"])));
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                string JsonStr = js.Serialize(objChart);
                return js.Serialize(objChart);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetAssignedDepartments(int EmpId)
        {
            objTMS_Bal = new TMS_BAL();
            DataTable dtDeptName = objTMS_Bal.BindAssignedDeptBAL(EmpId);
            List<AssignTraineeDept_Objects> objTraineeDept = new List<AssignTraineeDept_Objects>();
            foreach (DataRow drItems in dtDeptName.Rows)
            {
                objTraineeDept.Add(new AssignTraineeDept_Objects(
                  Convert.ToInt32(drItems["DeptID"]),
                  drItems["DepartmentName"].ToString()
                ));
            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            string JsonStr = js.Serialize(objTraineeDept);
            return js.Serialize(objTraineeDept);
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetOverallDepartments(int EmpId)
        {
            objTMS_Bal = new TMS_BAL();
            DataTable dtDeptName = objTMS_Bal.BindOverallDepartsDeptBAL(EmpId);
            List<AssignTraineeDept_Objects> objTraineeDept = new List<AssignTraineeDept_Objects>();
            foreach (DataRow drItems in dtDeptName.Rows)
            {
                objTraineeDept.Add(new AssignTraineeDept_Objects(
                  Convert.ToInt32(drItems["DeptID"]),
                  drItems["DepartmentName"].ToString()
                ));
            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            string JsonStr = js.Serialize(objTraineeDept);
            return js.Serialize(objTraineeDept);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetClassroomTrainingCount()
        {
            try
            {
                var EmpID = int.Parse(HttpContext.Current.Request.Params["EmpID"]);
                var TargetType = HttpContext.Current.Request.Params["TargetType"];
                objTMS_Bal = new TMS_BAL();
                int TotalCount = 0;
                DataTable dtCount = objTMS_Bal.GetClassroomTrainingCountBal(EmpID, TargetType);
                if (dtCount.Rows.Count > 0)
                {
                    TotalCount = Convert.ToInt32(dtCount.Rows[0]["TotalCount"]);
                }
                var result = new
                {
                    iTotalRecordsCount = TotalCount,
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}

