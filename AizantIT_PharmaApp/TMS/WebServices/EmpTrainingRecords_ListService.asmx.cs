﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using TMS_BusinessLayer;
using TMS_BusinessObjects;

namespace AizantIT_PharmaApp.TMS.WebServices
{
    /// <summary>
    /// Summary description for EmpTrainingRecords_ListService
    /// </summary>
    [WebService(Namespace = "http://Aizant.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class EmpTrainingRecords_ListService : System.Web.Services.WebService
    {
        TMS_BAL objTMS_Bal;

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetEmpTrainingRecordsDataTable()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var EmpID = int.Parse(HttpContext.Current.Request.Params["EmpID"]);

                var sSearch_DocumentName = HttpContext.Current.Request.Params["sSearch_1"].ToString(CultureInfo.CurrentCulture);
                var sSearch_DocRefNo = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
                var sSearch_DeptName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
                var sSearch_ModeOfTraining = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
                var sSearch_Opinion = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
                var sSearch_EvaluatedBy = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
                var sSearch_EvaluatedDate = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
                var sSearch_TrainingType = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);
                var sSearch_ReadDuration = HttpContext.Current.Request.Params["sSearch_9"].ToString(CultureInfo.CurrentCulture);

                List<EmpTrainingRecords_Objects> objEmpTrainingRecords = new List<EmpTrainingRecords_Objects>();
                int TotalRecordCount = 0;
                string TotalDuration = "";
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetEmpTrainingRecords(displayLength, displayStart, sortCol,
                    sSortDir, sSearch, EmpID, sSearch_DocumentName,sSearch_DocRefNo, sSearch_DeptName, sSearch_ModeOfTraining, sSearch_Opinion,
                    sSearch_EvaluatedBy, sSearch_EvaluatedDate, sSearch_TrainingType, sSearch_ReadDuration,out TotalDuration);
                if (dt.Rows.Count > 0)
                {
                    TotalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objEmpTrainingRecords.Add(
                       new EmpTrainingRecords_Objects(Convert.ToInt32(dt.Rows[i]["DocumentID"]), dt.Rows[i]["DocumentName"].ToString(),
                       dt.Rows[i]["DocRefNo"].ToString(), dt.Rows[i]["DeptName"].ToString(),
                       dt.Rows[i]["ModeOfTraining"].ToString(), dt.Rows[i]["Opinion"].ToString(), 
                       dt.Rows[i]["EvaluatedBy"].ToString(), dt.Rows[i]["EvaluatedDate"].ToString(),
                       dt.Rows[i]["TrainingType"].ToString(),Convert.ToInt32(dt.Rows[i]["DeptID"]), dt.Rows[i]["ReadDuration"].ToString(),Convert.ToInt32(dt.Rows[i]["EmpFeedbackID"])));
                }
                var result = new
                {
                    iTotalDuration = TotalDuration,
                    iTotalRecords = TotalRecordCount,
                    iTotalDisplayRecords = TotalRecordCount,
                    aaData = objEmpTrainingRecords
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetMyTrainingRecordsDataTable()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var EmpID = int.Parse(HttpContext.Current.Request.Params["EmpID"]);
                int FilterType;
                int.TryParse(HttpContext.Current.Request.Params["FilterType"], out FilterType);
                int Year;
                int.TryParse(HttpContext.Current.Request.Params["Year"], out Year);
                int DeptID;
                int.TryParse(HttpContext.Current.Request.Params["DeptID"], out DeptID);
                var TargetType = (HttpContext.Current.Request.Params["TargetType"].ToString());
                List<MyTrainingList_Objects> objMyTrainingRecords = new List<MyTrainingList_Objects>();
                int TotalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetMyTrainingRecords(displayLength, displayStart, sortCol,
                    sSortDir, sSearch, EmpID, FilterType, Year, DeptID, TargetType);
                if (dt.Rows.Count > 0)
                {
                    TotalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objMyTrainingRecords.Add(
                       new MyTrainingList_Objects(Convert.ToInt32(dt.Rows[i]["DocumentID"]), dt.Rows[i]["DocumentName"].ToString(),
                       dt.Rows[i]["DocRefNo"].ToString(), dt.Rows[i]["DeptName"].ToString(),
                       dt.Rows[i]["ModeOfTraining"].ToString(), dt.Rows[i]["Opinion"].ToString(),
                       dt.Rows[i]["EvaluatedBy"].ToString(), dt.Rows[i]["EvaluatedDate"].ToString(),
                       dt.Rows[i]["TrainingType"].ToString(), Convert.ToInt32(dt.Rows[i]["DeptID"]), dt.Rows[i]["ReadDuration"].ToString(), Convert.ToInt32(dt.Rows[i]["EmpFeedbackID"]),
                       Convert.ToInt32(dt.Rows[i]["CreatedYear"])));
                }
                var result = new
                {
                    iTotalRecords = TotalRecordCount,
                    iTotalDisplayRecords = TotalRecordCount,
                    aaData = objMyTrainingRecords
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
