﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using TMS_BusinessLayer;
using TMS_BusinessObjects;

namespace AizantIT_PharmaApp.TMS.WebServices
{
    /// <summary>
    /// Summary description for ATC_ListService
    /// </summary>
    [WebService(Namespace = "http://Aizant.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ATC_ListService : System.Web.Services.WebService
    {
        TMS_BAL objTMS_Bal;       
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string ATC_PendingList()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var AuthorID = int.Parse(HttpContext.Current.Request.Params["AuthorID"]);
                int FilterType;
                var NotificationID = int.Parse(HttpContext.Current.Request.Params["Notification_ID"]);
                if (NotificationID == 0)
                {
                    FilterType = int.Parse(HttpContext.Current.Request.Params["FilterType"]);
                } 
                else
                {
                    FilterType = 1;
                }
                List<ATC_PendingListObjects> objEmpJrList = new List<ATC_PendingListObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetATC_PendingList(displayLength, displayStart, sortCol, sSortDir, sSearch, AuthorID, FilterType, NotificationID, out totalRecordCount);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objEmpJrList.Add(
                       new ATC_PendingListObjects(Convert.ToInt32(dt.Rows[i]["ATC_ID"]),
                       dt.Rows[i]["DepartmentName"].ToString(), Convert.ToInt32(dt.Rows[i]["CalendarYear"]),
                       dt.Rows[i]["Reviewer"].ToString(), dt.Rows[i]["Approver"].ToString(), dt.Rows[i]["CurrentStatus"].ToString(), (Convert.ToInt32(dt.Rows[i]["StatusID"].ToString()))));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objEmpJrList
                };
                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string ATC_Hod_Approval()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var ReviewerID = int.Parse(HttpContext.Current.Request.Params["ReviewerID"]);
                int FilterType;
                var NotificationID = int.Parse(HttpContext.Current.Request.Params["Notification_ID"]);
                if (NotificationID == 0)
                {
                    FilterType = int.Parse(HttpContext.Current.Request.Params["FilterType"]);
                }
                else
                {
                    FilterType = 1;
                }
                List<ATC_Hod_ApprovalObjects> objEmpJrList = new List<ATC_Hod_ApprovalObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetATC_Hod_Approval(displayLength, displayStart, sortCol, sSortDir, sSearch, ReviewerID, FilterType, NotificationID, out totalRecordCount);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objEmpJrList.Add(
                       new ATC_Hod_ApprovalObjects(Convert.ToInt32(dt.Rows[i]["ATC_ID"]),
                       dt.Rows[i]["DepartmentName"].ToString(), Convert.ToInt32(dt.Rows[i]["CalendarYear"]),
                       dt.Rows[i]["Author"].ToString(), dt.Rows[i]["Approver"].ToString(), dt.Rows[i]["CurrentStatus"].ToString(), (Convert.ToInt32(dt.Rows[i]["StatusID"].ToString()))));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objEmpJrList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string ATC_QA_Approval()
        {
            try
            {

                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var ApproverID = int.Parse(HttpContext.Current.Request.Params["ApproverID"]);
                int FilterType;
                var NotificationID = int.Parse(HttpContext.Current.Request.Params["Notification_ID"]);
                if (NotificationID == 0)
                {
                    FilterType = int.Parse(HttpContext.Current.Request.Params["FilterType"]);
                }
                else
                {
                    FilterType = 1;
                }
                List<ATC_QA_ApproverObjects> objEmpJrList = new List<ATC_QA_ApproverObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetATC_QA_Approval(displayLength, displayStart, sortCol, sSortDir, sSearch, ApproverID, FilterType, NotificationID, out totalRecordCount);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objEmpJrList.Add(
                       new ATC_QA_ApproverObjects(Convert.ToInt32(dt.Rows[i]["ATC_ID"]),
                       dt.Rows[i]["DepartmentName"].ToString(), Convert.ToInt32(dt.Rows[i]["CalendarYear"]),
                       dt.Rows[i]["Author"].ToString(), dt.Rows[i]["Reviewer"].ToString(), dt.Rows[i]["CurrentStatus"].ToString(), (Convert.ToInt32(dt.Rows[i]["StatusID"].ToString()))));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objEmpJrList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string ATC_List()
        {
            try
            {

                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var EmpID = int.Parse(HttpContext.Current.Request.Params["EmpID"]);
                var NotificationID = int.Parse(HttpContext.Current.Request.Params["Notification_ID"]);
                List<ATC_ListObjects> objEmpJrList = new List<ATC_ListObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetATC_List(displayLength, displayStart, sortCol, sSortDir, sSearch, EmpID, NotificationID, out totalRecordCount);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objEmpJrList.Add(
                       new ATC_ListObjects(Convert.ToInt32(dt.Rows[i]["ATC_ID"]),
                       dt.Rows[i]["DepartmentName"].ToString(), Convert.ToInt32(dt.Rows[i]["CalendarYear"]), dt.Rows[i]["Author"].ToString(),
                       dt.Rows[i]["Reviewer"].ToString(), dt.Rows[i]["Approver"].ToString(), dt.Rows[i]["CurrentStatus"].ToString(), (Convert.ToInt32(dt.Rows[i]["StatusID"].ToString()))));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objEmpJrList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string ATC_MRT_List()
        {
            try
            {

                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var ATC_ID = int.Parse(HttpContext.Current.Request.Params["ATC_ID"]);
                List<ACT_MRTList> objEmpJrList = new List<ACT_MRTList>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetATC_MRTList(displayLength, displayStart, sortCol, sSortDir, sSearch, ATC_ID);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                   // string monthNumber = HelpClass.GetMonthName(dt.Rows[i]["MonthName"].ToString());
                    objEmpJrList.Add(
                       new ACT_MRTList(Convert.ToInt32(dt.Rows[i]["DocumentID"]),
                       dt.Rows[i]["DocumentName"].ToString(), dt.Rows[i]["MonthName"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objEmpJrList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
