﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using TMS_BusinessLayer;
using TMS_BusinessObjects;

namespace AizantIT_PharmaApp.TMS.WebServices.FeedbackMaster
{
    /// <summary>
    /// Summary description for FeedbackMaster
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class FeedbackMaster : System.Web.Services.WebService
    {
        TMS_BAL objTMS_Bal;
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string getFeedbackQuestionList()
        {
            try
            {
                var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString();
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString();
                var FeedbackID = int.Parse(HttpContext.Current.Request.Params["FeedbackID"]);
                List<FeedbackMasterObjects> objFeedbackMaster = new List<FeedbackMasterObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.getFeedbackQuestionListBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, FeedbackID);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objFeedbackMaster.Add(
                       new FeedbackMasterObjects(Convert.ToInt32(dt.Rows[i]["FeedbackQuestionID"]),dt.Rows[i]["QuestionTitle"].ToString(), dt.Rows[i]["Statusname"].ToString(), Convert.ToInt32(dt.Rows[i]["EmpFeedbackonQuestion"])));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objFeedbackMaster
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string getFeedbackHistoryList()
        {
            try
            {
                var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString();
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString();
                var FeedbackID = int.Parse(HttpContext.Current.Request.Params["FeedbackID"]);
                List<FeedbackMasterHistoryObjects> objfeedBackHistoryList = new List<FeedbackMasterHistoryObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.getFeedbackHistoryListBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, FeedbackID);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objfeedBackHistoryList.Add(
                       new FeedbackMasterHistoryObjects(Convert.ToInt32(dt.Rows[i]["ActionID"]),dt.Rows[i]["ActionBy"].ToString(), dt.Rows[i]["ActionDate"].ToString(),
                       dt.Rows[i]["StatusName"].ToString(), dt.Rows[i]["Remarks"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objfeedBackHistoryList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
