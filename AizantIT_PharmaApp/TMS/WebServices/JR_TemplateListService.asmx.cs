﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using TMS_BusinessLayer;
using TMS_BusinessObjects;

namespace AizantIT_PharmaApp.TMS.WebServices
{
    /// <summary>
    /// Summary description for JR_TemplateListService
    /// </summary>
    [WebService(Namespace = "http://Aizant.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class JR_TemplateListService : System.Web.Services.WebService
    {

        TMS_BAL objTMS_Bal;

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetJR_TemplateList()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
                List<JR_TemplateListObjects> objJrList = new List<JR_TemplateListObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetJR_TemplateList(displayLength, displayStart, sortCol, sSortDir, sSearch, EmpID);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objJrList.Add(
                        new JR_TemplateListObjects(Convert.ToInt32(dt.Rows[i]["JR_TemplateID"]), dt.Rows[i]["JR_TemplateName"].ToString(),Convert.ToInt32(dt.Rows[i]["DeptID"]),
                        dt.Rows[i]["DepartmentName"].ToString(), dt.Rows[i]["DeptCode"].ToString(), dt.Rows[i]["CreatedByName"].ToString(), dt.Rows[i]["CreatedDate"].ToString(),
                        dt.Rows[i]["ModifiedByName"].ToString(), dt.Rows[i]["ModifiedDate"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objJrList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }     
    }
}
