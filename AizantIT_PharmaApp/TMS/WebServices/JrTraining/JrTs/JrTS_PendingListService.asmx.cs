﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using TMS_BusinessLayer;
using TMS_BusinessObjects;
namespace AizantIT_PharmaApp.TMS.WebServices
{
    /// <summary>
    /// Summary description for JrTS_PendingListService
    /// </summary>
    [WebService(Namespace = "http://Aizant.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class JrTS_PendingListService : System.Web.Services.WebService
    {

        TMS_BAL objTMS_Bal;

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetJrTS_PendingList()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
                int FilterType= int.Parse(HttpContext.Current.Request.Params["FilterType"]); ;
                var NotificationID = int.Parse(HttpContext.Current.Request.Params["NotificationID"]);
                //if (NotificationID == 0)
                //{
                //    FilterType = int.Parse(HttpContext.Current.Request.Params["FilterType"]);
                //}
                //else
                //{
                //    FilterType = 3;
                //}
                List<JrTS_PendingListObject> objJrList = new List<JrTS_PendingListObject>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetJrTS_PendingList(displayLength, displayStart, sortCol, sSortDir, sSearch, EmpID, FilterType, NotificationID);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objJrList.Add(
                        new JrTS_PendingListObject(Convert.ToInt32(dt.Rows[i]["TS_ID"]), dt.Rows[i]["JR_Name"].ToString(), 
                        dt.Rows[i]["JR_Version"].ToString(),dt.Rows[i]["TraineeName"].ToString(), 
                        dt.Rows[i]["EmpCode"].ToString(), dt.Rows[i]["DeptName"].ToString(), 
                        dt.Rows[i]["DeptCode"].ToString(),dt.Rows[i]["DesignationName"].ToString(), 
                        dt.Rows[i]["TS_Reviewer"].ToString().Trim()==""?"N/A": dt.Rows[i]["TS_Reviewer"].ToString(), 
                        dt.Rows[i]["StatusName"].ToString(), Convert.ToInt32(dt.Rows[i]["StatusID"])));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objJrList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}

