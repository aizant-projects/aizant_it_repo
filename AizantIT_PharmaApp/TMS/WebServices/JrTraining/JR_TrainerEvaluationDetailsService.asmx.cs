﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using TMS_BusinessLayer;
using TMS_BusinessObjects;

namespace AizantIT_PharmaApp.TMS.WebServices.JrTraining
{
    /// <summary>
    /// Summary description for JR_TrainerEvaluationDetailsService
    /// </summary>
    [WebService(Namespace = "http://Aizant.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class JR_TrainerEvaluationDetailsService : System.Web.Services.WebService
    {

        TMS_BAL objTMS_Bal;

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetJR_TrainerEvaluationDetails()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

                var sSearch_JR_Name = HttpContext.Current.Request.Params["sSearch_1"].ToString();
                var sSearch_JR_Version = HttpContext.Current.Request.Params["sSearch_2"].ToString();
                var sSearch_TraineeName = HttpContext.Current.Request.Params["sSearch_3"].ToString();
                var sSearch_TraineeDeptName = HttpContext.Current.Request.Params["sSearch_4"].ToString();
                var sSearch_JR_AssignedBy = HttpContext.Current.Request.Params["sSearch_5"].ToString();
                var sSearch_JR_AssignedDate = HttpContext.Current.Request.Params["sSearch_6"].ToString();

                var TrainerID = HttpContext.Current.Request.Params["TrainerID"].ToString(CultureInfo.CurrentCulture);
                List<JR_TrainerEvaluationDetailsObjects> objJrList = new List<JR_TrainerEvaluationDetailsObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetJR_TrainerEvaluationDetails(displayLength, displayStart, sortCol, sSortDir, sSearch,
                     sSearch_JR_Name, sSearch_JR_Version, sSearch_TraineeName, sSearch_TraineeDeptName, sSearch_JR_AssignedBy, sSearch_JR_AssignedDate, TrainerID);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objJrList.Add(
                        new JR_TrainerEvaluationDetailsObjects(Convert.ToInt32(dt.Rows[i]["JR_ID"]), dt.Rows[i]["JR_Name"].ToString(), dt.Rows[i]["JR_Version"].ToString(),
                        dt.Rows[i]["TraineeName"].ToString(), dt.Rows[i]["TraineeDeptName"].ToString(), dt.Rows[i]["JR_AssignedBy"].ToString(), dt.Rows[i]["JR_AssignedDate"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objJrList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetJR_TrainedDocs()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var JR_ID = HttpContext.Current.Request.Params["JR_ID"].ToString(CultureInfo.CurrentCulture);
                var TrainerID = HttpContext.Current.Request.Params["TrainerID"].ToString(CultureInfo.CurrentCulture);
                List<JR_TrainedDocsObject> objJrTrainedDocsForTrainer = new List<JR_TrainedDocsObject>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetJR_TrainedDocs(displayLength, displayStart, sortCol, sSortDir, sSearch, JR_ID, TrainerID);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objJrTrainedDocsForTrainer.Add(
                        new JR_TrainedDocsObject(Convert.ToInt32(dt.Rows[i]["DocumentID"]),Convert.ToInt32(dt.Rows[i]["EvaluatedStatusID"]),dt.Rows[i]["Document"].ToString(), Convert.ToInt32(dt.Rows[i]["DocVersion"]),dt.Rows[i]["DepartmentName"].ToString(),
                       Convert.ToInt32(dt.Rows[i]["Attempts"]),dt.Rows[i]["QualifiedDate"].ToString(), Convert.ToInt32(dt.Rows[i]["Marks"]), dt.Rows[i]["ReadDuration"].ToString(), dt.Rows[i]["EvaluatedStatus"].ToString()));
                }
               var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objJrTrainedDocsForTrainer
               };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
