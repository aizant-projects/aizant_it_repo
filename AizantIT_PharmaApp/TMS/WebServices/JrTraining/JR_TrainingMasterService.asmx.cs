﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using TMS_BusinessLayer;
using TMS_BusinessObjects;

namespace AizantIT_PharmaApp.TMS.WebServices
{
    /// <summary>
    /// Summary description for JR_TrainingMasterServices
    /// </summary>
    [WebService(Namespace = "http://Aizant.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class JR_TrainingMasterServices : System.Web.Services.WebService
    {
        TMS_BAL objTMS_Bal;
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetJR_TrainingRegular()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var JR_ID = HttpContext.Current.Request.Params["JR_ID"].ToString(CultureInfo.CurrentCulture);

                List<JR_TrainingRegularObjects> objEmpJrList = new List<JR_TrainingRegularObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetJR_TrainingRegular(displayLength, displayStart, sortCol, sSortDir, sSearch, JR_ID);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objEmpJrList.Add(
                        new JR_TrainingRegularObjects(Convert.ToInt32(dt.Rows[i]["Dept_ID"]),
                        dt.Rows[i]["DepartmentName"].ToString(),Convert.ToInt32(dt.Rows[i]["DocumentCount"]), Convert.ToInt32(dt.Rows[i]["TrainerCount"]), Convert.ToInt32(dt.Rows[i]["AssignedDays"]), Convert.ToInt32(dt.Rows[i]["RemainingDays"]),
                        dt.Rows[i]["StatusName"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objEmpJrList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetJR_TrainingRetrospective()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var JR_ID = HttpContext.Current.Request.Params["JR_ID"].ToString(CultureInfo.CurrentCulture);

                List<JR_TrainingRetrospectiveObjects> objEmpJrList = new List<JR_TrainingRetrospectiveObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetJR_TrainingRetrospective(displayLength, displayStart, sortCol, sSortDir, sSearch, JR_ID);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objEmpJrList.Add(
                        new JR_TrainingRetrospectiveObjects(Convert.ToInt32(dt.Rows[i]["Dept_ID"]),
                        dt.Rows[i]["DepartmentName"].ToString(), Convert.ToInt32(dt.Rows[i]["Document"])));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objEmpJrList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetJR_TrainingDocDetails()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString();
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString();
                var JR_ID = HttpContext.Current.Request.Params["JR_ID"].ToString();
                var Dept_ID = HttpContext.Current.Request.Params["Dept_ID"].ToString();
                var TrainingOn = HttpContext.Current.Request.Params["TrainingOn"].ToString();

                List<JR_TrainingDocDetailsObjects> objEmpJrList = new List<JR_TrainingDocDetailsObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetJR_TrainingDocDetails(displayLength, displayStart, sortCol, sSortDir, sSearch, JR_ID, Dept_ID, TrainingOn);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objEmpJrList.Add(
                        new JR_TrainingDocDetailsObjects(Convert.ToInt32(dt.Rows[i]["DocumentID"]), Convert.ToInt32(dt.Rows[i]["DocumentVesrsionID"]),
                        dt.Rows[i]["Document"].ToString(),dt.Rows[i]["DocumentType"].ToString(),dt.Rows[i]["TrainingStatus"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objEmpJrList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetJR_DeptTrainerDetails()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString();
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString();
                var JR_ID = HttpContext.Current.Request.Params["JR_ID"].ToString();
                var DeptID = HttpContext.Current.Request.Params["DeptID"].ToString();

                List<JR_DeptTrainerDetailsObjects> objJR_DeptTrainerDetails = new List<JR_DeptTrainerDetailsObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetJR_DeptTrainerDetails(displayLength, displayStart, sortCol, sSortDir, sSearch, JR_ID, DeptID);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objJR_DeptTrainerDetails.Add(
                        new JR_DeptTrainerDetailsObjects(dt.Rows[i]["EmpCode"].ToString(), dt.Rows[i]["TrainerName"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objJR_DeptTrainerDetails
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public void UpdateJrTrainingStatus(int DocID,int DocVersionID,int JrID)
        {
            objTMS_Bal = new TMS_BAL();
            objTMS_Bal.UpdateTrainingDeptStatus(DocID, DocVersionID,JrID);
        }
    }
}

