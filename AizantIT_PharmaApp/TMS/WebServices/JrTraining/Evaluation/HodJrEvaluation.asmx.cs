﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using TMS_BusinessLayer;
using TMS_BusinessObjects;

namespace AizantIT_PharmaApp.TMS.WebServices.JrTraining.Evaluation
{
    /// <summary>
    /// Summary description for HodJrEvaluation
    /// </summary>
    [WebService(Namespace = "http://Aizant.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class HodJrEvaluation : System.Web.Services.WebService
    {
        TMS_BAL objTMS_Bal;

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string getHodJrEvaluationList()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString();
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString();

                var sSearch_JR_Name = HttpContext.Current.Request.Params["sSearch_1"].ToString();
                var sSearch_JR_Version = HttpContext.Current.Request.Params["sSearch_2"].ToString();
                var sSearch_TraineeName = HttpContext.Current.Request.Params["sSearch_3"].ToString();
                var sSearch_TraineeDeptName = HttpContext.Current.Request.Params["sSearch_4"].ToString();
                var sSearch_JR_AssignedBy = HttpContext.Current.Request.Params["sSearch_5"].ToString();
                var sSearch_JR_AssignedDate = HttpContext.Current.Request.Params["sSearch_6"].ToString();
               // var sSearch_EmpFeedbackID = HttpContext.Current.Request.Params["sSearch_7"].ToString();
    
                var HodEmpID = HttpContext.Current.Request.Params["HodEmpID"].ToString();
                var NotificationID = HttpContext.Current.Request.Params["NotificationID"].ToString(CultureInfo.CurrentCulture);
                List<HodJrEvaluationDetails> objJrEvaluateList = new List<HodJrEvaluationDetails>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetHodJrEvaluationDetails(displayLength, displayStart, sortCol, sSortDir, sSearch,
                    sSearch_JR_Name,sSearch_JR_Version,sSearch_TraineeName,sSearch_TraineeDeptName,sSearch_JR_AssignedBy,sSearch_JR_AssignedDate,
                    HodEmpID, NotificationID);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objJrEvaluateList.Add(
                        new HodJrEvaluationDetails(Convert.ToInt32(dt.Rows[i]["JR_ID"]), Convert.ToInt32(dt.Rows[i]["EmpFeedbackID"]), dt.Rows[i]["JR_Name"].ToString(), dt.Rows[i]["JR_Version"].ToString(),
                        dt.Rows[i]["TraineeName"].ToString(), dt.Rows[i]["TraineeDeptName"].ToString(), dt.Rows[i]["JR_AssignedBy"].ToString(), 
                        dt.Rows[i]["JR_AssignedDate"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objJrEvaluateList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetJR_EmpTrainedDocs()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString();
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString();
                var SJR_ID =  HttpContext.Current.Request.Params["JR_ID"].ToString();
                List<JR_EmpTrainedDocsObjects> objJrTrainedDocs = new List<JR_EmpTrainedDocsObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetJR_EmpTrainedDocs(displayLength, displayStart, sortCol, sSortDir, sSearch, SJR_ID);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objJrTrainedDocs.Add(
                        new JR_EmpTrainedDocsObjects(Convert.ToInt32(dt.Rows[i]["JrID"]), Convert.ToInt32(dt.Rows[i]["DocID"]), dt.Rows[i]["Document"].ToString(), Convert.ToInt32(dt.Rows[i]["DocVersion"]), dt.Rows[i]["Department"].ToString(),
                       dt.Rows[i]["ReadDuration"].ToString(), Convert.ToInt32(dt.Rows[i]["Attempts"]), dt.Rows[i]["QualifiedDate"].ToString(), Convert.ToInt32(dt.Rows[i]["MarksObtained"]), dt.Rows[i]["EvaluatedTrainer"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objJrTrainedDocs
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
