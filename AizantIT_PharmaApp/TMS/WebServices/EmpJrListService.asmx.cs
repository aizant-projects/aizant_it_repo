﻿using AizantIT_PharmaApp.Common;
using AizantIT_PharmaApp.TMS.Reports.TF_Report.Datasource;
using CrystalDecisions.CrystalReports.Engine;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using TMS_BusinessLayer;
using TMS_BusinessObjects;

namespace AizantIT_PharmaApp.TMS.WebServices
{
    /// <summary>
    /// Summary description for EmpJrListService
    /// </summary>
    [WebService(Namespace = "http://Aizant.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class EmpJrListService : System.Web.Services.WebService
    {
        TMS_BAL objTMS_Bal;
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetDatatoEmpJR_Report()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

                var sSearch_TraineeName = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
                var sSearch_JR_Name = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
                var sSearch_JR_Version = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
                var sSearch_HOD_Name = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
                var sSearch_DepartmentName = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
                var sSearch_DesignationName = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
                var sSearch_StatusName = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);

                var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
                int Year;
                int.TryParse(HttpContext.Current.Request.Params["Year"], out Year);
                int DeptID;
                int.TryParse(HttpContext.Current.Request.Params["DeptID"], out DeptID);
                int FilterType;
                int.TryParse(HttpContext.Current.Request.Params["FilterType"], out FilterType);
                var Status = (HttpContext.Current.Request.Params["Status"].ToString());
                List<EmpJrListObjects> objEmpJrList = new List<EmpJrListObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetJR_LIST(displayLength, displayStart, sortCol, sSortDir, sSearch, sSearch_TraineeName,
                    sSearch_JR_Name, sSearch_JR_Version, sSearch_HOD_Name, sSearch_DepartmentName, sSearch_DesignationName, sSearch_StatusName,
                    Year, EmpID, DeptID, FilterType, Status, out totalRecordCount);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objEmpJrList.Add(
                        new EmpJrListObjects(Convert.ToInt32(dt.Rows[i]["JR_ID"]), Convert.ToInt32(dt.Rows[i]["TS_ID"]), dt.Rows[i]["TraineeName"].ToString(),
                        dt.Rows[i]["JR_Name"].ToString(), Convert.ToInt32(dt.Rows[i]["JR_Version"]), dt.Rows[i]["HOD_Name"].ToString(),
                        dt.Rows[i]["StatusName"].ToString(), dt.Rows[i]["DesignationName"].ToString(), dt.Rows[i]["DepartmentName"].ToString(),
                        dt.Rows[i]["CurrentStatus"].ToString(), Convert.ToInt32(dt.Rows[i]["EmpFeedbackID"])));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objEmpJrList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetJrTS_TrainerDetails()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var JR_ID = HttpContext.Current.Request.Params["JR_ID"].ToString(CultureInfo.CurrentCulture);
                List<JrTrainerDetailsObjects> objJrTrainerDetails = new List<JrTrainerDetailsObjects>();
                int totalRecordCount = 0;
                int IsRetrospective = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetJR_TrainerDetails(displayLength, displayStart, sortCol, sSortDir, sSearch, JR_ID, out IsRetrospective);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objJrTrainerDetails.Add(
                        new JrTrainerDetailsObjects(dt.Rows[i]["Department"].ToString(), dt.Rows[i]["Document"].ToString(),
                        dt.Rows[i]["Trainer"].ToString(), Convert.ToInt32(dt.Rows[i]["Days"])));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iIsRetrospective = IsRetrospective,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objJrTrainerDetails
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetJrTS_RetrospectiveDetails()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var JR_ID = HttpContext.Current.Request.Params["JR_ID"].ToString(CultureInfo.CurrentCulture);
                List<JrTsRetropectiveDetailsObject> objJrRetrospectiveDetails = new List<JrTsRetropectiveDetailsObject>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetJR_RetrospectiveDetails(displayLength, displayStart, sortCol, sSortDir, sSearch, JR_ID);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objJrRetrospectiveDetails.Add(
                        new JrTsRetropectiveDetailsObject(dt.Rows[i]["DeptName"].ToString(), dt.Rows[i]["DocumentNumber"].ToString(),
                        dt.Rows[i]["DeptCode"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objJrRetrospectiveDetails
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetIsJrTrainingFileExists(int JRID)
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                DataTable dtTrainingFilePath = objTMS_Bal.GetTrainingFilePath(JRID);
                int totalRecordCount = 1;
                if (dtTrainingFilePath.Rows.Count == 0)
                {
                    totalRecordCount = dtTrainingFilePath.Rows.Count;
                }

                var result = new
                {
                    iTotalRecords = totalRecordCount,
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return JsonConvert.SerializeObject(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetJrTrainingFilePaths(int JRID)
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                DataTable dtTrainingFilePath = objTMS_Bal.GetTrainingFilePath(JRID);
                DataRow drFilePath;
                int totalRecordCount = 1;
                if (dtTrainingFilePath.Rows.Count == 0)
                {
                    totalRecordCount = dtTrainingFilePath.Rows.Count;
                    if (!Directory.Exists(Server.MapPath("~/TMS/TrainingFile")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/TMS/TrainingFile"));
                    }
                    DataSet dsSP_CR_Main = objTMS_Bal.GetDatatoCR(JRID.ToString());
                    DataTable dtJR_Details = new DataTable();
                    DataTable dtEmpDetails = new DataTable();
                    DataTable dtTS_Details = new DataTable();
                    DataTable dtRQ_Details = new DataTable();
                    DataTable dtRQ_Justification = new DataTable();
                    DataTable TS_AuditDetails = new DataTable();
                    DataTable TrainingRecord = new DataTable();
                    DataTable TrainingQuestions = new DataTable();
                    DataTable Questionoptions = new DataTable();
                    DataTable ExamDetails = new DataTable();
                    DataTable TrainingCertificate = new DataTable();

                    dtJR_Details = dsSP_CR_Main.Tables[0];
                    dtEmpDetails = dsSP_CR_Main.Tables[1];
                    dtTS_Details = dsSP_CR_Main.Tables[2];
                    dtRQ_Details = dsSP_CR_Main.Tables[3];
                    dtRQ_Justification = dsSP_CR_Main.Tables[4];
                    TS_AuditDetails = dsSP_CR_Main.Tables[5];
                    TrainingRecord = dsSP_CR_Main.Tables[6];
                    TrainingQuestions = dsSP_CR_Main.Tables[7];
                    Questionoptions = dsSP_CR_Main.Tables[8];
                    ExamDetails = dsSP_CR_Main.Tables[9];
                    TrainingCertificate = dsSP_CR_Main.Tables[10];

                    ReportDocument objMainRpt = new ReportDocument();
                    ReportDocument objJR_Rpt = new ReportDocument();
                    ReportDocument objTS_Rpt = new ReportDocument();
                    ReportDocument objRetTS_Rpt = new ReportDocument();
                    ReportDocument objTR_Rpt = new ReportDocument();
                    ReportDocument objTQue_Rpt = new ReportDocument();
                    ReportDocument objTC_Rpt = new ReportDocument();

                    #region Crystal report

                    // Generate Crystal report based on the category selection.
                    #region Filling Dataset for Main Reports
                    TF_MainDataSet dsTF_Main = new TF_MainDataSet();
                    foreach (DataRow dr in dtJR_Details.Rows)
                    {
                        dsTF_Main.JR_Details.AddJR_DetailsRow(dr["JR_Name"].ToString(), dr["JR_Description"].ToString(), dr["EmpAcceptedDate"].ToString(), dr["ApprovedBy"].ToString(), dr["ApprovedDate"].ToString(), dr["Version"].ToString(), dr["AssignedBy"].ToString(), dr["AssignedDate"].ToString());
                    }
                    foreach (DataRow dr in dtEmpDetails.Rows)
                    {
                        dsTF_Main.EmpDetails.AddEmpDetailsRow(dr["EmpCode"].ToString(), dr["TraineeName"].ToString(), dr["DeptName"].ToString(), dr["Designation"].ToString(), dr["EmpStartDate"].ToString());
                    }
                    foreach (DataRow dr in dtTS_Details.Rows)
                    {
                        dsTF_Main.TS_Details.AddTS_DetailsRow(dr["RowNo"].ToString(), dr["DeptName"].ToString(), dr["DocumentNums"].ToString(), dr["Trainers"].ToString(), dr["Days"].ToString());
                    }
                    foreach (DataRow dr in dtRQ_Details.Rows)
                    {
                        dsTF_Main.Retrospective.AddRetrospectiveRow(dr["DeptName"].ToString(), dr["RQ_Documents"].ToString());
                    }
                    foreach (DataRow dr in dtRQ_Justification.Rows)
                    {
                        dsTF_Main.RQ_Justification.AddRQ_JustificationRow(dr["RQ_JustificationNote"].ToString(), dr["CommentDate"].ToString());
                    }
                    foreach (DataRow dr in TS_AuditDetails.Rows)
                    {
                        dsTF_Main.TS_AuditDetails.AddTS_AuditDetailsRow(dr["TS_CreatedBy"].ToString(), dr["TS_ApprovedBy"].ToString(), dr["ApprovedDate"].ToString(), dr["CreatedDate"].ToString());
                    }
                    foreach (DataRow dr in TrainingRecord.Rows)
                    {
                        dsTF_Main.TrainingRecord.AddTrainingRecordRow(dr["RowNum"].ToString(), dr["DocumentName"].ToString(), dr["Document_RefNo"].ToString(),
                            dr["DeptName"].ToString(), dr["ModeOfTraining"].ToString(), dr["Opinion"].ToString(), dr["EvaluatedBy"].ToString(), dr["EvaluatedDate"].ToString());
                    }
                    foreach (DataRow dr in TrainingQuestions.Rows)
                    {
                        dsTF_Main.TrainingQuestions.AddTrainingQuestionsRow(dr["ExamID"].ToString(), dr["QuestionID"].ToString(), dr["QuestionTitle"].ToString(),
                            dr["EmpAnswer"].ToString(), dr["CorrectAnswer"].ToString());
                    }
                    foreach (DataRow dr in Questionoptions.Rows)
                    {
                        dsTF_Main.Questionoptions.AddQuestionoptionsRow(dr["QuestionID"].ToString(), dr["Option"].ToString(), dr["OptionTitle"].ToString());
                    }
                    foreach (DataRow dr in ExamDetails.Rows)
                    {
                        dsTF_Main.ExamDetails.AddExamDetailsRow(dr["ExamID"].ToString(), dr["Marks"].ToString(), dr["AttendedDate"].ToString(),
                            dr["DocRefNo"].ToString(), dr["DocumentName"].ToString(), dr["ExplainedStatus"].ToString(), dr["TrainerOpinion"].ToString(), dr["EvaluatedDate"].ToString(), dr["EvaluatedBy"].ToString(), dr["DeptName"].ToString());
                    }
                    foreach (DataRow dr in TrainingCertificate.Rows)
                    {
                        dsTF_Main.TrainingCertificate.AddTrainingCertificateRow(dr["EvaluatedHOD"].ToString(), dr["HodEvaluatedDate"].ToString());
                    }
                    string strMainReportPath = Server.MapPath("~/TMS/Reports/TF_Report/CrystalReport/TF_Main_Report.rpt");
                    objMainRpt.Load(strMainReportPath);
                    objMainRpt.Subreports[0].SetDataSource(dsTF_Main); //JD_Report
                    objMainRpt.Subreports[1].SetDataSource(dsTF_Main); //TS_Report
                    objMainRpt.Subreports[2].SetDataSource(dsTF_Main); //Retrospective
                    objMainRpt.Subreports[3].SetDataSource(dsTF_Main); //TrainingRecord_Report
                    objMainRpt.Subreports[4].SetDataSource(dsTF_Main); //Questionnaire_Report
                    objMainRpt.Subreports[5].SetDataSource(dsTF_Main); //TrainingCertificate_Report 
                    objMainRpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Server.MapPath("~/TMS/TrainingFile/" + JRID + "1.pdf"));
                    drFilePath = dtTrainingFilePath.NewRow();
                    drFilePath["Type"] = 1;
                    drFilePath["TrainingFilePath"] = "TMS/TrainingFile/" + JRID + "1.pdf";
                    dtTrainingFilePath.Rows.Add(drFilePath);
                    #endregion

                    #region Filling Dataset for JR Reports
                    string strJR_ReportPath = Server.MapPath("~/TMS/Reports/TF_Report/CrystalReport/TF_JR_Report.rpt");
                    objJR_Rpt.Load(strJR_ReportPath);

                    TF_JR_DetailsDS dsTF_JR = new TF_JR_DetailsDS();
                    foreach (DataRow dr in dtJR_Details.Rows)
                    {
                        dsTF_JR.JR_Details.AddJR_DetailsRow(dr["JR_Name"].ToString(), dr["JR_Description"].ToString(), dr["EmpAcceptedDate"].ToString(), dr["ApprovedBy"].ToString(), dr["ApprovedDate"].ToString(), dr["Version"].ToString(), dr["AssignedBy"].ToString(), dr["AssignedDate"].ToString());
                    }
                    foreach (DataRow dr in dtEmpDetails.Rows)
                    {
                        dsTF_JR.EmpDetails.AddEmpDetailsRow(dr["EmpCode"].ToString(), dr["TraineeName"].ToString(), dr["DeptName"].ToString(), dr["Designation"].ToString(), dr["EmpStartDate"].ToString());
                    }
                    objJR_Rpt.SetDataSource(dsTF_JR); //JD_Report
                    objJR_Rpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Server.MapPath("~/TMS/TrainingFile/" + JRID + "2.pdf"));
                    drFilePath = dtTrainingFilePath.NewRow();
                    drFilePath["Type"] = 2;
                    drFilePath["TrainingFilePath"] = "TMS/TrainingFile/" + JRID + "2.pdf";
                    dtTrainingFilePath.Rows.Add(drFilePath);
                    #endregion

                    #region Filling Dataset for TS Reports
                    string strTS_ReportPath = Server.MapPath("~/TMS/Reports/TF_Report/CrystalReport/TF_TS_Report.rpt");
                    objTS_Rpt.Load(strTS_ReportPath);
                    TF_TSDetailsDS dsTF_TS = new TF_TSDetailsDS();

                    foreach (DataRow dr in dtEmpDetails.Rows)
                    {
                        dsTF_TS.EmpDetails.AddEmpDetailsRow(dr["EmpCode"].ToString(), dr["TraineeName"].ToString(), dr["DeptName"].ToString(), dr["Designation"].ToString(), dr["EmpStartDate"].ToString());
                    }
                    foreach (DataRow dr in dtTS_Details.Rows)
                    {
                        dsTF_TS.TS_Details.AddTS_DetailsRow(dr["RowNo"].ToString(), dr["DeptName"].ToString(), dr["DocumentNums"].ToString(), dr["Trainers"].ToString(), dr["Days"].ToString());
                    }
                    foreach (DataRow dr in TS_AuditDetails.Rows)
                    {
                        dsTF_TS.TS_AuditDetails.AddTS_AuditDetailsRow(dr["TS_CreatedBy"].ToString(), dr["TS_ApprovedBy"].ToString(), dr["ApprovedDate"].ToString(), dr["CreatedDate"].ToString());
                    }
                    objTS_Rpt.SetDataSource(dsTF_TS); //TS_Report
                    objTS_Rpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Server.MapPath("~/TMS/TrainingFile/" + JRID + "3.pdf"));
                    drFilePath = dtTrainingFilePath.NewRow();
                    drFilePath["Type"] = 3;
                    drFilePath["TrainingFilePath"] = "TMS/TrainingFile/" + JRID + "3.pdf";
                    dtTrainingFilePath.Rows.Add(drFilePath);
                    #endregion

                    #region Filling Dataset for TR report
                    string strTR_ReportPath = Server.MapPath("~/TMS/Reports/TF_Report/CrystalReport/TF_TrainingRecordReport.rpt");
                    objTR_Rpt.Load(strTR_ReportPath);
                    TF_TrainingRecordDS dsTF_TR = new TF_TrainingRecordDS();
                    foreach (DataRow dr in dtEmpDetails.Rows)
                    {
                        dsTF_TR.EmpDetails.AddEmpDetailsRow(dr["EmpCode"].ToString(), dr["TraineeName"].ToString(), dr["DeptName"].ToString(), dr["Designation"].ToString(), dr["EmpStartDate"].ToString());
                    }
                    foreach (DataRow dr in TrainingRecord.Rows)
                    {
                        dsTF_TR.TrainingRecord.AddTrainingRecordRow(dr["RowNum"].ToString(), dr["DocumentName"].ToString(), dr["Document_RefNo"].ToString(),
                            dr["DeptName"].ToString(), dr["ModeOfTraining"].ToString(), dr["Opinion"].ToString(), dr["EvaluatedBy"].ToString(), dr["EvaluatedDate"].ToString());
                    }
                    objTR_Rpt.SetDataSource(dsTF_TR);
                    objTR_Rpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Server.MapPath("~/TMS/TrainingFile/" + JRID + "5.pdf"));
                    drFilePath = dtTrainingFilePath.NewRow();
                    drFilePath["Type"] = 5;
                    drFilePath["TrainingFilePath"] = "TMS/TrainingFile/" + JRID + "5.pdf";
                    dtTrainingFilePath.Rows.Add(drFilePath);
                    #endregion

                    #region Filling Dataset for Ret Reports
                    string strRet_ReportPath = Server.MapPath("~/TMS/Reports/TF_Report/CrystalReport/TF_RetReport.rpt");
                    objRetTS_Rpt.Load(strRet_ReportPath);
                    if (dtRQ_Details.Rows.Count != 0)
                    {
                        TF_RetDetailsDS dsTF_RetTS = new TF_RetDetailsDS();

                        foreach (DataRow dr in dtEmpDetails.Rows)
                        {
                            dsTF_RetTS.EmpDetails.AddEmpDetailsRow(dr["EmpCode"].ToString(), dr["TraineeName"].ToString(), dr["DeptName"].ToString(), dr["Designation"].ToString(), dr["EmpStartDate"].ToString());
                        }

                        foreach (DataRow dr in dtRQ_Details.Rows)
                        {
                            dsTF_RetTS.Retrospective.AddRetrospectiveRow(dr["DeptName"].ToString(), dr["RQ_Documents"].ToString());
                        }

                        foreach (DataRow dr in dtRQ_Justification.Rows)
                        {
                            dsTF_RetTS.RQ_Justification.AddRQ_JustificationRow(dr["RQ_JustificationNote"].ToString(), dr["CommentDate"].ToString());
                        }
                        foreach (DataRow dr in TS_AuditDetails.Rows)
                        {
                            dsTF_RetTS.TS_AuditDetails.AddTS_AuditDetailsRow(dr["TS_CreatedBy"].ToString(), dr["TS_ApprovedBy"].ToString(), dr["ApprovedDate"].ToString(), dr["CreatedDate"].ToString());
                        }
                        objRetTS_Rpt.SetDataSource(dsTF_RetTS); //Ret_Report
                        if (dtRQ_Details.Rows.Count != 0)
                        {
                            objRetTS_Rpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Server.MapPath("~/TMS/TrainingFile/" + JRID + "4.pdf"));
                            drFilePath = dtTrainingFilePath.NewRow();
                            drFilePath["Type"] = 4;
                            drFilePath["TrainingFilePath"] = "TMS/TrainingFile/" + JRID + "4.pdf";
                            dtTrainingFilePath.Rows.Add(drFilePath);
                        }
                    }
                    #endregion

                    #region Filling DataSet For Questionnaire Report
                    if (TrainingQuestions.Rows.Count>0)
                    {
                        string strQue_ReportPath = Server.MapPath("~/TMS/Reports/TF_Report/CrystalReport/TF_QuestionnairReport.rpt");
                        objTQue_Rpt.Load(strQue_ReportPath);
                        TF_QuestionnaireDS dsTF_Que = new TF_QuestionnaireDS();
                        foreach (DataRow dr in dtEmpDetails.Rows)
                        {
                            dsTF_Que.EmpDetails.AddEmpDetailsRow(dr["EmpCode"].ToString(), dr["TraineeName"].ToString(), dr["DeptName"].ToString(), dr["Designation"].ToString(), dr["EmpStartDate"].ToString());
                        }
                        foreach (DataRow dr in TrainingQuestions.Rows)
                        {
                            dsTF_Que.TrainingQuestions.AddTrainingQuestionsRow(dr["ExamID"].ToString(), dr["QuestionID"].ToString(), dr["QuestionTitle"].ToString(),
                                dr["EmpAnswer"].ToString(), dr["CorrectAnswer"].ToString());
                        }
                        foreach (DataRow dr in Questionoptions.Rows)
                        {
                            dsTF_Que.Questionoptions.AddQuestionoptionsRow(dr["QuestionID"].ToString(), dr["Option"].ToString(), dr["OptionTitle"].ToString());
                        }
                        foreach (DataRow dr in ExamDetails.Rows)
                        {
                            dsTF_Que.ExamDetails.AddExamDetailsRow(dr["ExamID"].ToString(), dr["Marks"].ToString(), dr["AttendedDate"].ToString(),
                                dr["DocRefNo"].ToString(), dr["DocumentName"].ToString(), dr["ExplainedStatus"].ToString(), dr["TrainerOpinion"].ToString(), dr["EvaluatedDate"].ToString(), dr["EvaluatedBy"].ToString(), dr["DeptName"].ToString());
                        }
                        objTQue_Rpt.SetDataSource(dsTF_Que);
                        objTQue_Rpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Server.MapPath("~/TMS/TrainingFile/" + JRID + "6.pdf"));
                        drFilePath = dtTrainingFilePath.NewRow();
                        drFilePath["Type"] = 6;
                        drFilePath["TrainingFilePath"] = "TMS/TrainingFile/" + JRID + "6.pdf";
                        dtTrainingFilePath.Rows.Add(drFilePath);
                    }                    
                    #endregion

                    #region Filling DataSet for Training certificate Report
                    string strTC_ReportPath = Server.MapPath("~/TMS/Reports/TF_Report/CrystalReport/TF_TrainingCertificateReport.rpt");
                    objTC_Rpt.Load(strTC_ReportPath);
                    TF_TrainingCertificateDS dsTF_TC = new TF_TrainingCertificateDS();
                    foreach (DataRow dr in dtEmpDetails.Rows)
                    {
                        dsTF_TC.EmpDetails.AddEmpDetailsRow(dr["EmpCode"].ToString(), dr["TraineeName"].ToString(), dr["DeptName"].ToString(), dr["Designation"].ToString(), dr["EmpStartDate"].ToString());
                    }
                    foreach (DataRow dr in TrainingCertificate.Rows)
                    {
                        dsTF_TC.TrainingCertificate.AddTrainingCertificateRow(dr["EvaluatedHOD"].ToString(), dr["HodEvaluatedDate"].ToString());
                    }
                    objTC_Rpt.SetDataSource(dsTF_TC);
                    objTC_Rpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Server.MapPath("~/TMS/TrainingFile/" + JRID + "7.pdf"));
                    drFilePath = dtTrainingFilePath.NewRow();
                    drFilePath["Type"] = 7;
                    drFilePath["TrainingFilePath"] = "TMS/TrainingFile/" + JRID + "7.pdf";
                    dtTrainingFilePath.Rows.Add(drFilePath);
                    #endregion

                    objTMS_Bal.InsertJrTrainingFile(JRID, dtTrainingFilePath);
                }

                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    dtFilePathData = dtTrainingFilePath
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return JsonConvert.SerializeObject(result);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string LogTrainingFilePrintBtn(string JRID, string TrainingFileType)
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                int Outresult = objTMS_Bal.InseretJR_PrintLog(JRID, (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString(), TrainingFileType);
                var result = new
                {
                    msg = "Print log Recorderd.",
                    msgType = "success"
                };
                JavaScriptSerializer js = new JavaScriptSerializer();
                return JsonConvert.SerializeObject(result);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    msg = HelpClass.LineNo(ex) + HelpClass.SQLEscapeString(ex.Message),
                    msgType = "error"
                };
                JavaScriptSerializer js = new JavaScriptSerializer();
                return JsonConvert.SerializeObject(result);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string PrintLogTrainingFileList()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var JR_ID = HttpContext.Current.Request.Params["Jr_ID"].ToString(CultureInfo.CurrentCulture);
                List<PrintLogTrainingFile> objPrintLogFile = new List<PrintLogTrainingFile>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetPrintLogTrainingFileDetails(displayLength, displayStart, sortCol, sSortDir, sSearch, JR_ID);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objPrintLogFile.Add(
                        new PrintLogTrainingFile(
                            Convert.ToInt32(dt.Rows[i]["Log_ID"]),
                        Convert.ToString(dt.Rows[i]["PrintBy"]),
                        Convert.ToString(dt.Rows[i]["PrintDate"]),
                        Convert.ToString(dt.Rows[i]["TF_Type"])));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objPrintLogFile
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
