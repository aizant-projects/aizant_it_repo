﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using TMS_BusinessLayer;
using TMS_BusinessObjects;

namespace AizantIT_PharmaApp.TMS.WebServices.JrTrainedRecords
{
    /// <summary>
    /// Summary description for JrTrainedRecordService
    /// </summary>
    [WebService(Namespace = "http://Aizant.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class JrTrainedRecordService : System.Web.Services.WebService
    {

        TMS_BAL objTMS_Bal;

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetJrRegularRecords()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var JR_ID = HttpContext.Current.Request.Params["JR_ID"].ToString(CultureInfo.CurrentCulture);
                var TrainerID = HttpContext.Current.Request.Params["TrainerID"].ToString(CultureInfo.CurrentCulture);
                List<JR_RegularObjects> objJrRegularList = new List<JR_RegularObjects>();
                int totalRecordCount = 0;
                int IsRetrospective = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetJR_RegularRecordsBal(displayLength, displayStart, sortCol, sSortDir, sSearch, JR_ID, TrainerID,out IsRetrospective);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objJrRegularList.Add(
                       new JR_RegularObjects(Convert.ToInt32(dt.Rows[i]["DocumentID"]),dt.Rows[i]["Document"].ToString(), dt.Rows[i]["DocumentType"].ToString(),
                       Convert.ToInt32(dt.Rows[i]["VersionNumber"]), dt.Rows[i]["DepartmentName"].ToString(), dt.Rows[i]["ExamType"].ToString(),
                       Convert.ToInt32(dt.Rows[i]["Attempts"]),dt.Rows[i]["StatusName"].ToString(), dt.Rows[i]["ReadDuration"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iIsRetrospective= IsRetrospective,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objJrRegularList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetJrRetrospectiveRecords()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var JR_ID = HttpContext.Current.Request.Params["JR_ID"].ToString(CultureInfo.CurrentCulture);
                var TrainerID = HttpContext.Current.Request.Params["TrainerID"].ToString(CultureInfo.CurrentCulture);
                List<JR_RetrospectiveObjects> objJrRetrospectiveList = new List<JR_RetrospectiveObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetJR_RetrospectiveRecordsBal(displayLength, displayStart, sortCol, sSortDir, sSearch, JR_ID, TrainerID);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objJrRetrospectiveList.Add(
                       new JR_RetrospectiveObjects(dt.Rows[i]["Document"].ToString(),dt.Rows[i]["DocumentType"].ToString(),
                       Convert.ToInt32(dt.Rows[i]["VersionNumber"]), dt.Rows[i]["DepartmentName"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objJrRetrospectiveList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}

