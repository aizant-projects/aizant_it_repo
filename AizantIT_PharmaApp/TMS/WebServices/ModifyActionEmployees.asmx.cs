﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using TMS_BusinessLayer;
using TMS_BusinessObjects;

namespace AizantIT_PharmaApp.TMS.WebServices
{
    /// <summary>
    /// Summary description for ModifyActionEmployees
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ModifyActionEmployees : System.Web.Services.WebService
    {
        TMS_BAL objTMS_Bal;
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]

        public string JrActionEmps()
        {
            try
            {
                var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString();
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString();
                List<JrActionEmps> objEmpJrList = new List<JrActionEmps>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetUnderQaApproveJRs(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objEmpJrList.Add(
                       new JrActionEmps(Convert.ToInt32(dt.Rows[i]["JR_ID"]), Convert.ToInt32(dt.Rows[i]["DeptID"]), Convert.ToInt32(dt.Rows[i]["StatusID"]),
                       Convert.ToInt32(dt.Rows[i]["TraineeID"]), Convert.ToInt32(dt.Rows[i]["AuthorID"]), Convert.ToInt32(dt.Rows[i]["ReviewerID"]),
                       dt.Rows[i]["JR_Name"].ToString(), dt.Rows[i]["Version"].ToString(), dt.Rows[i]["Department"].ToString(),
                       dt.Rows[i]["Trainee"].ToString(), dt.Rows[i]["Author"].ToString(), dt.Rows[i]["Reviewer"].ToString(), dt.Rows[i]["Status"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objEmpJrList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string JrTsActionEmps()
        {
            try
            {
                var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString();
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString();
                List<JrTS_ActionEmps> objEmpJrTSList = new List<JrTS_ActionEmps>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetUnderQaApproveJrTSList(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objEmpJrTSList.Add(
                       new JrTS_ActionEmps(Convert.ToInt32(dt.Rows[i]["JrTS_ID"]), Convert.ToInt32(dt.Rows[i]["DeptID"]), Convert.ToInt32(dt.Rows[i]["StatusID"]),
                       Convert.ToInt32(dt.Rows[i]["TraineeID"]), Convert.ToInt32(dt.Rows[i]["AuthorID"]), Convert.ToInt32(dt.Rows[i]["ReviewerID"]),
                       dt.Rows[i]["JR_Name"].ToString(), dt.Rows[i]["Version"].ToString(), dt.Rows[i]["Department"].ToString(),
                       dt.Rows[i]["Trainee"].ToString(), dt.Rows[i]["Author"].ToString(), 
                       dt.Rows[i]["Reviewer"].ToString().Trim()==""?"N/A": dt.Rows[i]["Reviewer"].ToString(), dt.Rows[i]["Status"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objEmpJrTSList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string TargetTsActionEmps()
        {
            try
            {
                var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString();
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString();
                List<TargetTS_ActionEmployees> objEmpTarget_TS_List = new List<TargetTS_ActionEmployees>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetUnderQaApproveTargetTS_List(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objEmpTarget_TS_List.Add(
                       new TargetTS_ActionEmployees(Convert.ToInt32(dt.Rows[i]["TTS_ID"]), Convert.ToInt32(dt.Rows[i]["DeptID"]), Convert.ToInt32(dt.Rows[i]["StatusID"]),
                       Convert.ToInt32(dt.Rows[i]["AuthorID"]), Convert.ToInt32(dt.Rows[i]["ReviewerID"]),dt.Rows[i]["Document"].ToString(), dt.Rows[i]["Department"].ToString(),
                       dt.Rows[i]["TargetType"].ToString(), dt.Rows[i]["Author"].ToString(), dt.Rows[i]["Reviewer"].ToString(), dt.Rows[i]["Status"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objEmpTarget_TS_List
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string JrEvaluator()
        {
            try
            {
                var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString();
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString();
                List<JR_EvaluatorObject> objEmpJrList = new List<JR_EvaluatorObject>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetJR_Evaluator(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objEmpJrList.Add(
                       new JR_EvaluatorObject(Convert.ToInt32(dt.Rows[i]["JR_ID"]), Convert.ToInt32(dt.Rows[i]["DeptID"]), Convert.ToInt32(dt.Rows[i]["EvaluatroHodEmpID"]),
                       dt.Rows[i]["JR_Name"].ToString(), dt.Rows[i]["JR_Version"].ToString(), dt.Rows[i]["Department"].ToString(),
                       dt.Rows[i]["TraineeName"].ToString(), dt.Rows[i]["EvaluatorHodName"].ToString()));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objEmpJrList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
