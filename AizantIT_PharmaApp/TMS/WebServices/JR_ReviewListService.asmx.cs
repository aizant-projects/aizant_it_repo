﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using TMS_BusinessLayer;
using TMS_BusinessObjects;


namespace AizantIT_PharmaApp.TMS.WebServices
{
    /// <summary>
    /// Summary description for JR_ReviewListService
    /// </summary>
    [WebService(Namespace = "http://Aizant.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class JR_ReviewListService : System.Web.Services.WebService
    {

        TMS_BAL objTMS_Bal;

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetJR_ReviewList()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
                var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
                int FilterType;
                var NotificationID = int.Parse(HttpContext.Current.Request.Params["NotificationID"]);
                if (NotificationID == 0)
                {
                    FilterType = int.Parse(HttpContext.Current.Request.Params["FilterType"]);
                }
                else
                {
                    FilterType = 1;
                }
                List<JR_ReviewListObjects> objJrList = new List<JR_ReviewListObjects>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetJR_ReviewList(displayLength, displayStart, sortCol, sSortDir, sSearch, EmpID, FilterType, NotificationID);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objJrList.Add(
                        new JR_ReviewListObjects(Convert.ToInt32(dt.Rows[i]["JR_ID"]), dt.Rows[i]["JR_Name"].ToString(), dt.Rows[i]["JR_Version"].ToString(), dt.Rows[i]["EmpCode"].ToString(), dt.Rows[i]["EmpName"].ToString(), dt.Rows[i]["DeptCode"].ToString(), dt.Rows[i]["DesignationName"].ToString(),
                        dt.Rows[i]["HOD_AssignedDate"].ToString(), dt.Rows[i]["TraineeAcceptedDate"].ToString(),
                        dt.Rows[i]["DeclinedBy"].ToString(), dt.Rows[i]["QARevertedDate"].ToString(),
                        dt.Rows[i]["Approver"].ToString(), dt.Rows[i]["StatusName"].ToString(), Convert.ToInt32(dt.Rows[i]["StatusID"])));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objJrList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
