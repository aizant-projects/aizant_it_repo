﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using TMS_BusinessLayer;
using TMS_BusinessObjects;
using TMS_BusinessObjects.TQ_OnDocs;

namespace AizantIT_PharmaApp.TMS.WebServices.TrainerAnswersOnDoc
{
    /// <summary>
    /// Summary description for TrainerAnswersOnDoc
    /// </summary>
    [WebService(Namespace = "http://Aizant.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class TrainerAnswersOnDoc : System.Web.Services.WebService
    {
        TMS_BAL objTMS_Bal;
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetDocmentsWithTrainerAnswers()
        {
            try
            {
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
                var sSortDir = HttpContext.Current.Request.Params["sSortDir_0"].ToString();
                var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString();
                var TraineeEmpID = int.Parse(HttpContext.Current.Request.Params["TraineeEmpID"]);
                List<DocsWithTraineeQuestions> objEmpJrList = new List<DocsWithTraineeQuestions>();
                int totalRecordCount = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetTA_Docs(displayLength, displayStart, sortCol, sSortDir, sSearch, TraineeEmpID);
                if (dt.Rows.Count > 0)
                {
                    totalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objEmpJrList.Add(
                       new DocsWithTraineeQuestions(Convert.ToInt32(dt.Rows[i]["DocID"]), dt.Rows[i]["Document"].ToString(),
                       dt.Rows[i]["Department"].ToString(),dt.Rows[i]["TraineeQuestions"].ToString()
                       ));
                }
                var result = new
                {
                    iTotalRecords = totalRecordCount,
                    iTotalDisplayRecords = totalRecordCount,
                    aaData = objEmpJrList
                };

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [WebMethod]
        public string GetTraineeAskedQuestionsOnDocumentToTrainee(string DocID, string TraineeID)
        {
            objTMS_Bal = new TMS_BAL();
            DataSet dsTraineeAskedQuestions = objTMS_Bal.GetTraineeaskedQuestionstoTraineeBal(DocID, TraineeID);
            List<DocTraineeRaisedQuestions> DocTraineeAnsweredQuestions = new List<DocTraineeRaisedQuestions>();
            foreach (DataRow drresult in dsTraineeAskedQuestions.Tables[1].Rows)
            {
                DocTraineeAnsweredQuestions.Add(new DocTraineeRaisedQuestions
                {
                    QuestionID = drresult["TraineeQuestionID"].ToString(),
                    QuestionDescription = drresult["Question"].ToString(),
                    QuestionAnsweredBy = drresult["AnsweredBy"].ToString(),
                    QuestionAnsweredDate = drresult["AnsweredDate"].ToString(),
                    IsPreviousQuestionsShow = drresult["IsPreviousQuestionsShow"].ToString(),
                });
            }
            List<DocTraineeRaisedQuestions> DocTraineeUnAnsweredQuestions = new List<DocTraineeRaisedQuestions>();
            foreach (DataRow drresult in dsTraineeAskedQuestions.Tables[0].Rows)
            {
                DocTraineeUnAnsweredQuestions.Add(new DocTraineeRaisedQuestions
                {
                    QuestionID = drresult["TraineeQuestionID"].ToString(),
                    QuestionDescription = drresult["Question"].ToString(),
                    QuestionRaisedDate = drresult["RaisedDate"].ToString(),
                });
            }
            var result = new
            {
                AnsweredQuestions = DocTraineeAnsweredQuestions,
                UnAnsweredQuestions = DocTraineeUnAnsweredQuestions
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
    }
}
