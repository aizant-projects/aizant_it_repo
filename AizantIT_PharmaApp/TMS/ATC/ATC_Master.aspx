﻿<%@ Page Title="ATC Master" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="ATC_Master.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.ATC.ATC_Master" %>

<%@ Register Src="~/UserControls/ElectronicSign.ascx" TagPrefix="uc1" TagName="ElectronicSign" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:HiddenField ID="hdnStatus" runat="server" Value="0" />
    <asp:HiddenField ID="hdnATC_ID" runat="server" Value="0" />
   <asp:HiddenField ID="hdnPreviousPage" runat="server" Value="" />
    <div class="form-group float-right col-lg-4 padding-none">
        <asp:Button ID="btnRedirectToList" CssClass="btn btn-dashboard float-right" runat="server" Text="Back To List" OnClick="btnRedirectToAtcList_Click" />
    </div>
    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 bottom padding-none">
            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="div1" runat="server" visible="false">
                <asp:Label ID="Label1" ForeColor="Red" runat="server" Text=""></asp:Label>
            </div>
            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12  padding-none" style="border-bottom: 1px solid #07889a;">
                <div class="grid_header float-left col-lg-12 col-sm-12 col-12 col-md-12">Annual Training Calendar</div>
                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none top bottom" id="divMainContainer" runat="server">
                    <div class="form-group float-left col-lg-4 padding-none bottom">
                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12">
                            <label>Department</label>
                            <asp:DropDownList ID="ddlDepartment" CssClass="selectpicker regulatory_dropdown_style  show-tick form-control" Enabled="false" data-live-search="true" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group float-left col-lg-2 padding-none" runat="server" visible="false" id="divDocType">
                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12">
                            <label>Doc Type</label>
                            <asp:UpdatePanel ID="upddlDocType" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlDocType" CssClass="selectpicker regulatory_dropdown_style  show-tick form-control" data-live-search="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlDocType_SelectedIndexChanged"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    
                        <div class=" col-12  col-sm-5 col-md-5 col-lg-5 float-left bottom padding-none" id="SopDiv" runat="server" visible="false">
                            <label>Select Document(s)<span class="smallred_label">*</span></label>
                            <asp:UpdatePanel ID="uplbSops" runat="server">
                                <ContentTemplate>
                                    <asp:ListBox ID="lbSops" CssClass="float-left col-lg-12 col-sm-12 col-12 col-md-12 regulatory_dropdown_style selectpicker padding-none" data-live-search="true" data-live-search-placeholder="Search" runat="server" data-size="7" SelectionMode="Multiple"></asp:ListBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                   
                    <div class=" float-right col-11  col-sm-1 col-md-1 float-left col-lg-1 top bottom" id="btnApplydiv" Visible="false" runat="server">
                        <label class=" col-12  col-sm-12 col-md-12 float-left col-lg-12"></label>
                        <asp:UpdatePanel ID="upApplyDocs" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnApply"  runat="server" CssClass="btn btn-signup_popup float-right top applyButton" Text="Apply" OnClick="btnApply_Click" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                   
                  <div class="col-12  col-sm-6 col-md-6 float-left col-lg-4" id="divStatus" runat="server" visible="false" >
                        <label class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none control-label custom_label_answer">Current Status</label>
                        <asp:TextBox ID="txtStatus" runat="server" Enabled="false" CssClass="form-control login_input_sign_up float-left col-lg-12 col-sm-12 col-12 col-md-12"></asp:TextBox>
                    </div>
                     
                        <div class=" col-12  col-sm-4 col-md-4 float-left col-lg-4 bottom" id="colsize_year" runat="server">
                            <label class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none control-label custom_label_answer">Year</label>
                            <asp:TextBox ID="txtYear" runat="server" CssClass="form-control login_input_sign_up float-left col-lg-12 col-sm-12 col-12 col-md-12" Enabled="false"></asp:TextBox>
                        </div>
                       
                        <div class="col-12  col-sm-4 col-md-4 float-left col-lg-4 bottom" id="colsize_author" runat="server">
                            <label class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none control-label custom_label_answer">Author</label>
                            <asp:TextBox ID="txtCreatedby" runat="server" CssClass="form-control login_input_sign_up  float-left col-lg-12 col-sm-12 col-12 col-md-12" Enabled="false"></asp:TextBox>
                        </div>
                        
                        <div class="col-12  col-sm-4 col-md-4 float-left col-lg-4 bottom" id="colsize_reviewer" runat="server">
                            <label class="float-left col-lg-12 col-sm-12 col-12 col-md-12  padding-none control-label custom_label_answer">Reviewer</label>
                            <asp:TextBox ID="txtHodApprover" runat="server" CssClass="form-control login_input_sign_up float-left col-lg-12 col-sm-12 col-12 col-md-12" Enabled="false"></asp:TextBox>
                        </div>
                        <div class="col-12  col-sm-4 col-md-4 float-left col-lg-4  " id="colsize_apprvr" runat="server">
                            <label class="float-left col-lg-12 col-sm-12 col-12 col-md-12  padding-none control-label custom_label_answer">Approver</label>
                            <asp:TextBox ID="txtQAApprover" runat="server" CssClass="form-control float-left login_input_sign_up col-lg-12 col-sm-12 col-12 col-md-12" Enabled="false"></asp:TextBox>
                        </div>
                </div>
            </div>
        </div>
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12  padding-none" >
            <div class="float-left col-lg-12 float-left col-lg-12 col-sm-12 col-12 col-md-12 col-12 col-md-12" >
                <asp:UpdatePanel ID="upAtcContent" runat="server">
                    <ContentTemplate>
                        <div id="SubmitDiv" runat="server" visible="false">
                            <div>
                                <asp:GridView ID="gvATC" runat="server" Width="100%" CssClass="table table-fixed fixgvATC AspGrid table-striped" AutoGenerateColumns="false" Enabled="false">
                                    <RowStyle CssClass="col-12 padding-none" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="DocID" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDocID" runat="server" Text='<%# Bind("DocumentID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Document" ItemStyle-CssClass="textAlignLeft" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="28%" ItemStyle-Width="28%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSop" runat="server" Text='<%# Bind("Document") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="JAN" HeaderStyle-Width="6%" ItemStyle-Width="6%">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbJAN" runat="server" Checked='<%# bool.Parse(Eval("JAN").ToString()) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="FEB" HeaderStyle-Width="6%" ItemStyle-Width="6%">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbFEB" runat="server" Checked='<%# bool.Parse(Eval("FEB").ToString()) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="MAR" HeaderStyle-Width="6%" ItemStyle-Width="6%">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbMAR" runat="server" Checked='<%# bool.Parse(Eval("MAR").ToString()) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="APR" HeaderStyle-Width="6%" ItemStyle-Width="6%">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbAPR" runat="server" Checked='<%# bool.Parse(Eval("APR").ToString()) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="MAY" HeaderStyle-Width="6%" ItemStyle-Width="6%">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbMAY" runat="server" Checked='<%# bool.Parse(Eval("MAY").ToString()) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="JUN" HeaderStyle-Width="6%" ItemStyle-Width="6%">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbJUN" runat="server" Checked='<%# bool.Parse(Eval("JUN").ToString()) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="JUL" HeaderStyle-Width="6%" ItemStyle-Width="6%">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbJUL" runat="server" Checked='<%# bool.Parse(Eval("JUL").ToString()) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="AUG" HeaderStyle-Width="6%" ItemStyle-Width="6%">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbAUG" runat="server" Checked='<%# bool.Parse(Eval("AUG").ToString()) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SEP" HeaderStyle-Width="6%" ItemStyle-Width="6%">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbSEP" runat="server" Checked='<%# bool.Parse(Eval("SEP").ToString()) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OCT" HeaderStyle-Width="6%" ItemStyle-Width="6%">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbOCT" runat="server" Checked='<%# bool.Parse(Eval("OCT").ToString()) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="NOV" HeaderStyle-Width="6%" ItemStyle-Width="6%">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbNOV" runat="server" Checked='<%# bool.Parse(Eval("NOV").ToString()) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DEC" HeaderStyle-Width="6%" ItemStyle-Width="6%">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbDEC" runat="server" Checked='<%# bool.Parse(Eval("DEC").ToString()) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="upJQHistoryGrid" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divFooterArea" class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none " style="border-top: 1px solid #07889a;">
                            <div class="float-right top bottom">
                                <asp:Button ID="btnHistory" class=" btn btn-revert_popup historyCalend" OnClick="btnHistory_Click" runat="server" Text="History" />
                                <asp:Button ID="btnComment" class=" btn btn-revert_popup commentsCalend" OnClick="btnComment_Click" runat="server" Text="Comments" />
                            </div>
                             
                            <%-- This Grid is For History--%>
                            <div id="divHistoryGrid" runat="server">
                                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none history_Section top" id="divHistoryTitle" runat="server" visible="false" style="border: 1px solid #c0d4d7;">
                                    <div class="grid_header float-left col-lg-12 col-sm-12 col-12 col-md-12">History</div>
                                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 top bottom padding-none" style="width: 100%; overflow: auto;">
                                        <table id="tblATC_History" class="datatable_cust tblATC_HistoryClass display breakword" style="width: 100%">
                                            <thead>
                                                <tr>
                                                    <th>ATC_HistoryID</th>
                                                    <th>Action</th>
                                                    <th>Action By</th>
                                                    <th>Role</th>
                                                    <th>Action Date</th>
                                                    <th>Remarks</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="upFooter" runat="server">
                    <ContentTemplate>
                        <div id="divRevertComment" runat="server" class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none comments_Section">
                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                <div class=" float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none" id="RevertDiv" runat="server" visible="false">
                                    <div class=" float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                        <label for="inputPassword3" id="lblTitleforRevertBy" runat="server" class=" control-label  custom_label_answer"></label>
                                        <asp:Label ID="lblRevertBy" runat="server" ForeColor="Black"></asp:Label>
                                    </div>
                                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control textarea_height" TextMode="MultiLine" Rows="3" Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div class=" float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none" id="CommentsDiv" runat="server" visible="false">
                                    <label for="inputPassword3" class=" padding-none float-left col-lg-12 col-sm-12 col-12 col-md-12 control-label  custom_label_answer">Comments</label>
                                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                        <asp:TextBox ID="txtcomments" runat="server" CssClass="form-control textarea_height" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Action Buttons-->
                        <div id="ActionButtonsDiv" class=" float-left col-lg-12 col-sm-12 col-12 col-md-12 top bottom padding-none">
                            <div class=" float-right" runat="server" id="divButtons" visible="false">
                                <asp:Button ID="btnSubmit" runat="server" CssClass=" btn-signup_popup" Text="Submit" OnClick="btnSubmit_Click" OnClientClick="return showloadGif();" Visible="false" />
                                <asp:Button ID="btnUpdate" runat="server" CssClass=" btn-signup_popup" Text="Update" OnClick="btnUpdate_Click" Visible="false" OnClientClick="return showloadGif();"/>
                                <asp:Button ID="btnHodapprove" runat="server" CssClass=" btn-signup_popup" Text="Approve" OnClick="btnHodapprove_Click" Visible="false" />
                                <asp:Button ID="btnHodRevert" runat="server" CssClass=" btn-revert_popup" Text="Revert" OnClick="btnHodRevert_Click" Visible="false" />
                                <asp:Button ID="btnQAApprove" runat="server" CssClass=" btn-signup_popup" Text="Approve" OnClick="btnQAApprove_Click" Visible="false" />
                                <asp:Button ID="btnQARevert" runat="server" CssClass=" btn-revert_popup" Text="Revert" OnClick="btnQARevert_Click" Visible="false" />
                                <asp:Button ID="btnCancel" runat="server" value="Cancel" CssClass=" btn-cancel_popup" Text="Cancel" OnClick="btnCancel_Click" Visible="false" />
                            </div>
                        </div>
                        <!--Action Buttons End-->
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <uc1:ElectronicSign runat="server" ID="ElectronicSign" />

    <script>
        function showloadGif() {
            ShowPleaseWait('show');
            return true;
        }
        function ShowPleaseWaitGifDisable() {
             ShowPleaseWait('hide');
        }
        function ViewATC_History() {
            loadATC_HistoryList();
        } 
        function SubmissionMsgFromServer(MsgValue) {
            ShowPleaseWait('hide');
            if (MsgValue == 1) {
                custAlertMsg("Submitted Successfully.", "success", "RedirecttoPendingPage()");
            }
            else if (MsgValue == 2) {
                custAlertMsg("Calendar Already Exists.", "error");
            }
            else if (MsgValue == 3) {
                custAlertMsg("You Are not Authorized.", "error");
            }
        }

        function SubmissionServerValidation(MsgValue) {
            ShowPleaseWait('hide');
            custAlertMsg(MsgValue, "error");
        }
    </script>

    <!--JQ DT For ATC_History -->
    <script>
        var oTblATC_History;
        function loadATC_HistoryList() {
            var ATC_ID = $('#<%=hdnATC_ID.ClientID%>').val();
            if (ATC_ID != "0") {
                if (oTblATC_History != null) {
                    oTblATC_History.destroy();
                }
                oTblATC_History = $('#tblATC_History').DataTable({
                    columns: [
                        { 'data': 'ATC_HistoryID' },
                        { 'data': 'Action' },
                        { 'data': 'Action_By' },
                        { 'data': 'Role' },
                        { 'data': 'ActionDate' },
                        { 'data': 'Remarks' }
                    ],
                    "fixedHeader": true,
                    "lengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
                    "aoColumnDefs": [{ "targets": [0], "visible": false }, { className: "textAlignLeft", "targets": [1, 2, 3, 5] }],
                    "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/InitializeATC_ListService.asmx/ATC_HistoryList")%>",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData.push({ "name": "ATC_ID", "value": ATC_ID });
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success": function (msg) {
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                UserSessionCheck();
                                $("#tblATC_History").show();
                                $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                                $(".tblATC_HistoryClass").css({ "width": "100%" });
                            },
                            error: function (xhr, textStatus, error) {
                                if (typeof console == "object") {
                                    console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                                }
                            }
                        });
                    }

                });
            }
            else {
                oTblATC_History = $('#tblATC_History').DataTable();
            }
        }
    </script>

    <script>
        $(document).ready(function () {
            $('.closebtn').on('click', function () {
                $('#paragraph2').addClass('col-12');
                $('#paragraph2').removeClass('col-10');
                $('#paragraph1').hide();
                $('.closebtn1').show();
                $('.closebtn').hide();

            });
            $('.closebtn1').on('click', function () {

                $('#paragraph2').addClass('col-10');
                $('#paragraph2').removeClass('col-12');
                $('#paragraph1').show();
                $('.closebtn').show();
                $('.closebtn1').hide();
            });

            $('.historyCalend').on('click', function () {
                $('.comments_Section').hide();
                $('.historyCalend').hide();
                $('.commentsCalend').show();
                $('.history_Section').show();

            });


            $('.abc').on('click', function () {
                $('.history_Section').hide();

            });
            $('.commentsCalend').on('click', function () {
                $('.comments_Section').show();
                $('.commentsCalend').hide();
                $('.historyCalend').show();
                $('.history_Section').hide();
            });
            $('.btn_assign').on('click', function () {
                $(input, this).removeAttr('checked');
                $(this).removeClass('active');
            });
            var table = $('#example').DataTable({
                fixedHeader: {
                    header: true
                }
            });
            var selector = '.landing_links ul li a';

            $(selector).on('click', function () {
                $(selector).removeClass('active');
                $(this).addClass('active');
            });
        });
    </script>

    <script type="text/javascript">
        function ApplybtnValidate() {
            var Sops = document.getElementById('<%=lbSops.ClientID%>').value;
            errors = [];
            if (Sops.length <= 0) {
                errors.push("You Should Select Atleast One Document");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            fixAtcHeader();
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fixAtcHeader();
        });

        function fixAtcHeader() {
            var tbl = document.getElementsByClassName("fixgvATC");
            // Fix up GridView to support THEAD tags  
            if ($(".fixgvATC").find("thead").length == 0) {
                $(".fixgvATC tbody").before("<thead><tr></tr></thead>");
                $(".fixgvATC thead tr").append($(".fixgvATC th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixgvATC tbody tr:first").remove();
                }
            }
        }
    </script>

    <script>
        function RedirecttoPendingPage() {
            ShowPleaseWait('show');
            window.open("<%=ResolveUrl("~/TMS/ATC/List/ATC_PendingList.aspx?FilterType=2")%>", "_self");
        }
        function RedirecttoHodPage() {
            ShowPleaseWait('show');
            window.open("<%=ResolveUrl("~/TMS/ATC/List/ATC_Hod_Approval.aspx?FilterType=2")%>", "_self");
         }
         function RedirecttoQAPage() {
             ShowPleaseWait('show');
             window.open("<%=ResolveUrl("~/TMS/ATC/List/ATC_QA_Approve.aspx?FilterType=2")%>", "_self");
        }
        function RedirecttoMainListPage() {
            ShowPleaseWait('show');
            window.open("<%=ResolveUrl("~/TMS/ATC/List/ATC_List.aspx")%>", "_self");
        }
    </script>

    <script>
        // To Get Electronic Sign Model
        function openElectronicSignModal() {
            openUC_ElectronicSign();
        }
    </script>

    <script>
            // To Hide Electronic Sign Before Success/Error Message 
            function HideElectronicSignModal() {
                $('#usES_Modal').modal('hide');
            }
    </script>
    <script>
            $(function () {
                $(".applyButton").on("click", function () {
                    $('.dataTable').DataTable().columns.adjust();
                });
            });
    </script>
</asp:Content>
