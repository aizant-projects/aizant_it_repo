﻿<%@ Page Title="ATC List" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="ATC_List.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.ATC.List.ATC_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
      <asp:HiddenField ID="hdnEmpID" runat="server" Value="0"/>
    <asp:UpdatePanel ID="UpHdnFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnATC_List" runat="server" />
            <asp:HiddenField ID="hdnStatus" runat="server" Value="0" />
            <asp:HiddenField ID="hdnPrint" runat="server" Value="false" />
            <asp:HiddenField runat="server" ID="hdnNotificationID" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div class="col-12 float-left padding-none" id="Div3" runat="server">
                  <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 "><asp:Label ID="lblATC_List" runat="server" Font-Bold="true" Text="Annual Training Calendar List"></asp:Label></div>
                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 bottom padding-none" style="overflow: auto; margin-top: 10px">
                    <table id="tblATC_List" class="datatable_cust tblATC_ListClass display breakword" style="width: 100%">
                        <thead>
                            <tr>
                                <th>ATC_ID</th>
                                <th>Department</th>
                                <th>Calendar Year</th>
                                <th>Author</th>
                                <th>Reviewer</th>
                                <th>Approver</th>
                                <th>Status</th>
                                <th>StatusID</th>
                                <th>Missed Training</th>
                                <th>View</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnViewATC_List" runat="server" OnClick="btnViewATC_List_Click" Text="Button"/>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>


    <!-- MRT  Pop-up -->
<div class="modal fade department" id="modelATC_MRT_List" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content" style="border-radius: 0px">
            <div class="modal-header">
                <h4 class="modal-title Panel_Title">Missed Training</h4>
                <button type="button" class="close" data-target="#modelATC_MRT_List" data-togle="modal" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" style="overflow: visible;">
                <table id="tbl_ATC_MRT_List" class="datatable_cust  display breakword" style="width: 100%">
                    <thead>
                        <tr>
                            <th>DocumentID</th>
                            <th>Document Name</th>
                            <th>Month</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

    <script>
        function ViewATC_List(ATC_ID, StatusID) {
            $("#<%=hdnATC_List.ClientID%>").val(ATC_ID);
            $("#<%=hdnStatus.ClientID%>").val(StatusID);
            $("#<%=btnViewATC_List.ClientID%>").click();
        }
    </script>

    <!--ATC_Pending List jQuery DataTable-->
    <script>
        $.ajaxSetup({
            cache: false
        });

        var oTable = $('#tblATC_List').DataTable({
            columns: [
                { 'data': 'ATC_ID' },
                { 'data': 'DepartmentName' },
                { 'data': 'CalendarYear' },
                { 'data': 'Author' },
                { 'data': 'Reviewer' }, 
                { 'data': 'Approver' },
                { 'data': 'CurrentStatus' },
                { 'data': 'StatusID' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a class="view" title="Missed Training"  href="#" onclick="ViewATC_MRT_List(' + o.ATC_ID + ',' + o.CalendarYear + ');"></a>'; }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a class="view" title="View"  href="#" onclick="ViewATC_List(' + o.ATC_ID + ',' + o.StatusID + ');"></a>'; }
                }
            ],
            "scrollY": "47vh",
            "aoColumnDefs": [{ "targets": [0, 6, 7], "visible": false }, { className: "textAlignLeft", "targets": [1, 3, 4, 5,6] }],
            "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/ATC_ListService.asmx/ATC_List")%>",
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> }, { "name": "Notification_ID", "value": <%=hdnNotificationID.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        UserSessionCheck();
                        $("#tblATC_List").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        $(".tblATC_ListClass").css({ "width": "100%" });
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
            
        });

    </script>

    <script>
        function ViewATC_MRT_List(ATC_ID) {
            $('#modelATC_MRT_List').modal('show');
            LoadATC_MRTList(ATC_ID);
        }
        var otbl_ATC_MRT=null;
        function LoadATC_MRTList(ATC_ID) {
            if (otbl_ATC_MRT != null) {
                otbl_ATC_MRT.destroy();
            }
            otbl_ATC_MRT= $('#tbl_ATC_MRT_List').DataTable({
                columns: [
                    { 'data': 'DocumentID' },
                    { 'data': 'DocumentName' },
                    { 'data': 'MonthName' }
                ],
                "order": [0, "asc"],
                "scrollY": "47vh",
                "aoColumnDefs": [{ "targets": [0], "visible": false }, { className: "textAlignLeft", "targets": [1, 2] }],
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/ATC_ListService.asmx/ATC_MRT_List")%>",
                "fnServerData": function (sSource, aoData, fnCallback)
                {
                    aoData.push({ "name": "ATC_ID", "value": ATC_ID });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                      }
                   });
             }
        });
        }
    </script>
  
</asp:Content>
