﻿using AizantIT_PharmaApp.Common;
using System;
using System.Collections;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BusinessLayer;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.ATC
{
    public partial class ATC_Master : System.Web.UI.Page
    {
        TMS_BAL objTMS_BAL;
        UMS_BAL objUMS_BAL;
        static int InitializeATC_ID = 0;
        static int ATC_ID = 0;
        static int status = 0;
        static int Currentstatus = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ElectronicSign.buttonClick += new EventHandler(ElectronicSign_buttonClick);
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        if (!Request.UrlReferrer.ToString().Contains("ATC_Master.aspx"))
                        {
                            hdnPreviousPage.Value = Request.UrlReferrer.ToString();
                            Session["TMS_BackToList"] = Request.UrlReferrer.ToString();
                        }
                        else
                        {
                            hdnPreviousPage.Value = Session["TMS_BackToList"].ToString();
                        }
                        ViewState["HodApproveOrRevert"] = string.Empty;
                        ViewState["QAApproveOrRevert"] = string.Empty;
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drTmsRoles = dtx.Select("ModuleID="+(int)Modules.TMS);
                        if (drTmsRoles.Length > 0)
                        {
                            dtTemp = drTmsRoles.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0 || dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainer_TMS).Length > 0 || dtTemp.Select("RoleID=" + (int)TMS_UserRole.QA_TMS).Length > 0)
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }                   
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATC_M1:" + strline + "  " + strMsg, "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            try
            {
                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                divAutorizedMsg.Visible = true;
                divMainContainer.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATC_M2:" + strline + "  " + strMsg, "error");
            }
        }
        private void InitializeThePage()
        {
            try
            {
                if (Session["MainListACT_ID"] != null)
                {
                    ATC_ID = Convert.ToInt32(Session["MainListACT_ID"]);
                    hdnATC_ID.Value = ATC_ID.ToString();
                    status = Convert.ToInt32(Session["AtcListStatus"]);
                    GetDataforAnnaualCalendar();
                    btnUpdate.Visible = false;
                    if (status == 7)//QA Approved Status=7
                    {
                        divStatus.Visible = false;
                        btnHodapprove.Visible = false;
                        btnHodRevert.Visible = false;
                        btnQAApprove.Visible = false;
                        btnQARevert.Visible = false;
                        divRevertComment.Visible = false;
                        btnComment.Visible = false;
                        btnHistory.Visible = false;
                        // gvATCHistory.Visible = true;
                        GetHistoryData();
                        divHistoryTitle.Visible = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "showalert", "ViewATC_History();", true);
                    }
                }
                else if (Session["HodACT_ID"] != null)
                {
                    ATC_ID = Convert.ToInt32(Session["HodACT_ID"]);
                    hdnATC_ID.Value = ATC_ID.ToString();
                    GetDataforAnnaualCalendar();
                }
                else if (Session["QAACT_ID"] != null)
                {
                    ATC_ID = Convert.ToInt32(Session["QAACT_ID"]);
                    hdnATC_ID.Value = ATC_ID.ToString();
                    GetDataforAnnaualCalendar();
                }
                else if (Session["PendingACT_ID"] != null)
                {
                    ATC_ID = Convert.ToInt32(Session["PendingACT_ID"]);
                    hdnATC_ID.Value = ATC_ID.ToString();
                    GetDataforAnnaualCalendar();

                }
                else if (Session["InitializeATC_ID"] != null) //From Create ATC
                {
                    InitializeATC_ID = Convert.ToInt32(Session["InitializeATC_ID"]);
                    hdnATC_ID.Value = InitializeATC_ID.ToString();
                    //btnApply.Visible = true;
                    btnApplydiv.Visible = true;
                    divDocType.Visible = SopDiv.Visible = true;
                    gvATC.Enabled = true;
                    CommentsDiv.Visible = true;
                    btnRedirectToList.Visible = false;
                    btnComment.Visible = false;
                    divButtons.Visible = true;
                    btnSubmit.Visible = true;
                    btnCancel.Visible = true;
                    BindDepartments(Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                    objTMS_BAL = new TMS_BAL();
                    DataSet ds = objTMS_BAL.GetInitializeATCDetails(Convert.ToInt32(Session["InitializeATC_ID"]));
                    DataTable dt = ds.Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        txtYear.Text = dt.Rows[0]["CalendarYear"].ToString();
                        ddlDepartment.Items.Insert(0, new ListItem(dt.Rows[0]["DepartmentName"].ToString(), dt.Rows[0]["DeptID"].ToString()));
                        txtCreatedby.Text = dt.Rows[0]["Author"].ToString();
                        txtHodApprover.Text = dt.Rows[0]["Reviewer"].ToString();
                        txtQAApprover.Text = dt.Rows[0]["Approver"].ToString();
                    }
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        BindDocType(ds.Tables[1]);
                        if (ds.Tables[2].Rows.Count > 0)
                        {
                            lbSops.DataSource = ds.Tables[2];
                            lbSops.DataValueField = "DocumentID";
                            lbSops.DataTextField = "DocumentName";
                            lbSops.DataBind();
                        }
                    }
                    else
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Document Type Not Exists to Get Documents", "warning");
                    }
                    //BindDocType();
                    //  BindDocument();
                    //ddlYear.SelectedValue = Session["Year"].ToString();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATC_M3:" + strline + "  " + strMsg, "error");
            }
        }
        
        public void BindDepartments(int EmpID)
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                int[] RoleIDs = new int[] { (int)TMS_UserRole.Trainer_TMS, (int)TMS_UserRole.HOD_TMS, (int)TMS_UserRole.QA_TMS };
                DataTable dt = objUMS_BAL.getRoleWiseDepts(EmpID, RoleIDs);
                ddlDepartment.DataSource = dt;
                ddlDepartment.DataTextField = "DepartmentName";
                ddlDepartment.DataValueField = "DeptID";
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("- Select Department -", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATC_M5:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindDocType(DataTable dtDocType)
        {
            try
            {
                ddlDocType.DataSource = dtDocType;
                ddlDocType.DataValueField = "DocumentTypeID";
                ddlDocType.DataTextField = "DocumentType";
                ddlDocType.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATC_M6:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindDocType()
        {
            try
            {
                TMS_BAL objTMS_BAL = new TMS_BAL();
                DataTable dt = objTMS_BAL.DocumentType();
                ddlDocType.DataSource = dt;
                ddlDocType.DataValueField = "DocumentTypeID";
                ddlDocType.DataTextField = "DocumentType";
                ddlDocType.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATC_M6:" + strline + "  " + strMsg, "error");
            }
        }

        private void BindDocument(DataTable dt)
        {
            try
            {
                lbSops.DataSource = dt;
                lbSops.DataTextField = "DocumentName";
                lbSops.DataValueField = "DocumentID";
                lbSops.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATC_M7:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();

                DataTable dtSelectedDocs = new DataTable();
                if (ddlDocType.Items.Count > 0)
                {
                    CreateCalendarForSelectedDocs(out dtSelectedDocs);
                    if (ViewState["dtSelectedDocs"] != null)
                    {
                        DataTable dtTest = gvATC.DataSource as DataTable;
                        DataTable dtExistingDocs = ViewState["dtSelectedDocs"] as DataTable;
                        DataRow[] drExistingDocsByDocType = dtExistingDocs.Select("DocType=" + ddlDocType.SelectedValue);
                        if (drExistingDocsByDocType.Length > 0)
                        {
                            for (int x = 0; x < lbSops.Items.Count; x++)
                            {
                                if (lbSops.Items[x].Selected.Equals(false))
                                {
                                    string DocID = lbSops.Items[x].Value;
                                    DataRow[] drExists = dtExistingDocs.Select("DocumentID='" + DocID + "'");
                                    if (drExists.Length > 0)
                                    {
                                        dtExistingDocs.Rows.Remove(drExists[0]);
                                        dtExistingDocs.AcceptChanges();
                                    }
                                }
                            }
                            dtExistingDocs.AcceptChanges();

                            drExistingDocsByDocType = dtExistingDocs.Select("DocType=" + ddlDocType.SelectedValue);
                            if (drExistingDocsByDocType.Length > 0)
                            {
                                DataTable dtExistingDocsByDocType = drExistingDocsByDocType.CopyToDataTable();
                                for (int i = 0; i < dtExistingDocsByDocType.Rows.Count; i++)
                                {
                                    string DocID = dtExistingDocsByDocType.Rows[i]["DocumentID"].ToString();
                                    int selectedDocLength = dtSelectedDocs.Select("DocumentID='" + DocID + "'").Length;
                                    if (selectedDocLength != 0)
                                    {
                                        DataRow[] drExists = dtSelectedDocs.Select("DocumentID='" + DocID + "'");
                                        if (drExists.Length > 0)
                                        {
                                            dtSelectedDocs.Rows.Remove(drExists[0]);
                                            dtSelectedDocs.AcceptChanges();
                                        }
                                    }
                                }
                            }
                        }
                        dtSelectedDocs.Merge(dtExistingDocs);
                        //DataView dvDocs = dtSelectedDocs.DefaultView;
                        //dvDocs.Sort = "DocType asc";
                        //dtSelectedDocs = dvDocs.ToTable();
                    }
                    if (gvATC.Rows.Count > 0)
                    {
                        dtSelectedDocs = SelectedMonthsOnDocs(dtSelectedDocs);
                    }
                    if (dtSelectedDocs.Rows.Count > 0)
                    {
                        gvATC.DataSource = dtSelectedDocs;
                        gvATC.DataBind();
                        ViewState["dtSelectedDocs"] = dtSelectedDocs;
                    }
                    else
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "No Documents Selected to Apply", "warning");
                    }
                    SubmitDiv.Visible = true;
                    btnComment.Visible = false;
                    btnHistory.Visible = false;
                }
                else
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "No Documents Selected to Apply", "warning");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATC_M8:" + strline + "  " + strMsg, "error");
            }
        }

        private DataTable SelectedMonthsOnDocs(DataTable dtSelectedDocs)
        {
            foreach (GridViewRow row in gvATC.Rows)
            {
                Label lblDocID = row.FindControl("lblDocID") as Label;
                CheckBox cbJan = (CheckBox)row.FindControl("cbJAN");
                CheckBox cbfeb = (CheckBox)row.FindControl("cbFEB");
                CheckBox cbmar = (CheckBox)row.FindControl("cbMAR");
                CheckBox cbapr = (CheckBox)row.FindControl("cbAPR");
                CheckBox cbmay = (CheckBox)row.FindControl("cbMAY");
                CheckBox cbjun = (CheckBox)row.FindControl("cbJUN");
                CheckBox cbjul = (CheckBox)row.FindControl("cbJUL");
                CheckBox cbaug = (CheckBox)row.FindControl("cbAUG");
                CheckBox cbsep = (CheckBox)row.FindControl("cbSEP");
                CheckBox cboct = (CheckBox)row.FindControl("cbOCT");
                CheckBox cbnov = (CheckBox)row.FindControl("cbNOV");
                CheckBox cbdec = (CheckBox)row.FindControl("cbDEC");
                string strDocID = lblDocID.Text;

                DataRow[] drExists = dtSelectedDocs.Select("DocumentID='" + strDocID + "'");
                if (drExists.Length > 0)
                {
                    drExists[0]["JAN"] = cbJan.Checked;
                    drExists[0]["FEB"] = cbfeb.Checked;
                    drExists[0]["MAR"] = cbmar.Checked;
                    drExists[0]["APR"] = cbapr.Checked;
                    drExists[0]["MAY"] = cbmay.Checked;
                    drExists[0]["JUN"] = cbjun.Checked;
                    drExists[0]["JUL"] = cbjul.Checked;
                    drExists[0]["AUG"] = cbaug.Checked;
                    drExists[0]["SEP"] = cbsep.Checked;
                    drExists[0]["OCT"] = cboct.Checked;
                    drExists[0]["NOV"] = cbnov.Checked;
                    drExists[0]["DEC"] = cbdec.Checked;
                }
            }
            return dtSelectedDocs;
        }

        private void CreateCalendarForSelectedDocs(out DataTable dtResult)
        {
            dtResult = new DataTable();
            dtResult.Columns.Add("Document");
            dtResult.Columns.Add("DocumentID");
            dtResult.Columns.Add("DocType");
            dtResult.Columns.Add("JAN");
            dtResult.Columns.Add("FEB");
            dtResult.Columns.Add("MAR");
            dtResult.Columns.Add("APR");
            dtResult.Columns.Add("MAY");
            dtResult.Columns.Add("JUN");
            dtResult.Columns.Add("JUL");
            dtResult.Columns.Add("AUG");
            dtResult.Columns.Add("SEP");
            dtResult.Columns.Add("OCT");
            dtResult.Columns.Add("NOV");
            dtResult.Columns.Add("DEC");
            for (int x = 0; x < lbSops.Items.Count; x++)
            {
                if (lbSops.Items[x].Selected.Equals(true))
                {
                    DataRow dr = dtResult.NewRow();
                    dr["Document"] = lbSops.Items[x].Text;
                    dr["DocumentID"] = lbSops.Items[x].Value;
                    dr["DocType"] = ddlDocType.SelectedValue;
                    dr["JAN"] = false;
                    dr["FEB"] = false;
                    dr["MAR"] = false;
                    dr["APR"] = false;
                    dr["MAY"] = false;
                    dr["JUN"] = false;
                    dr["JUL"] = false;
                    dr["AUG"] = false;
                    dr["SEP"] = false;
                    dr["OCT"] = false;
                    dr["NOV"] = false;
                    dr["DEC"] = false;

                    dtResult.Rows.Add(dr);
                }
            }
        }

        private DataTable ConvertCalendarToSendDB(GridView gvCalendar)
        {
            DataTable dtResult = new DataTable();
            dtResult.Columns.Add("Month");
            dtResult.Columns.Add("DocID");
            DataRow dr;
            foreach (GridViewRow row in gvCalendar.Rows)
            {
                Label lblDocID = row.FindControl("lblDocID") as Label;
                CheckBox cbJan = (CheckBox)row.FindControl("cbJAN");
                CheckBox cbfeb = (CheckBox)row.FindControl("cbFEB");
                CheckBox cbmar = (CheckBox)row.FindControl("cbMAR");
                CheckBox cbapr = (CheckBox)row.FindControl("cbAPR");
                CheckBox cbmay = (CheckBox)row.FindControl("cbMAY");
                CheckBox cbjun = (CheckBox)row.FindControl("cbJUN");
                CheckBox cbjul = (CheckBox)row.FindControl("cbJUL");
                CheckBox cbaug = (CheckBox)row.FindControl("cbAUG");
                CheckBox cbsep = (CheckBox)row.FindControl("cbSEP");
                CheckBox cboct = (CheckBox)row.FindControl("cbOCT");
                CheckBox cbnov = (CheckBox)row.FindControl("cbNOV");
                CheckBox cbdec = (CheckBox)row.FindControl("cbDEC");

                string DocId = lblDocID.Text;

                if (cbJan.Checked == true)
                {
                    dr = dtResult.NewRow();
                    dr["Month"] = "1";
                    dr["DocID"] = DocId;
                    dtResult.Rows.Add(dr);
                }
                if (cbfeb.Checked == true)
                {
                    dr = dtResult.NewRow();
                    dr["DocID"] = DocId;
                    dr["Month"] = "2";
                    dtResult.Rows.Add(dr);
                }
                if (cbmar.Checked == true)
                {
                    dr = dtResult.NewRow();
                    dr["DocID"] = DocId;
                    dr["Month"] = "3";
                    dtResult.Rows.Add(dr);
                }
                if (cbapr.Checked == true)
                {
                    dr = dtResult.NewRow();
                    dr["DocID"] = DocId;
                    dr["Month"] = "4";
                    dtResult.Rows.Add(dr);
                }
                if (cbmay.Checked == true)
                {
                    dr = dtResult.NewRow();
                    dr["DocID"] = DocId;
                    dr["Month"] = "5";
                    dtResult.Rows.Add(dr);
                }
                if (cbjun.Checked == true)
                {
                    dr = dtResult.NewRow();
                    dr["DocID"] = DocId;
                    dr["Month"] = "6";
                    dtResult.Rows.Add(dr);
                }
                if (cbjul.Checked == true)
                {
                    dr = dtResult.NewRow();
                    dr["DocID"] = DocId;
                    dr["Month"] = "7";
                    dtResult.Rows.Add(dr);
                }
                if (cbaug.Checked == true)
                {
                    dr = dtResult.NewRow();
                    dr["DocID"] = DocId;
                    dr["Month"] = "8";
                    dtResult.Rows.Add(dr);
                }
                if (cbsep.Checked == true)
                {
                    dr = dtResult.NewRow();
                    dr["DocID"] = DocId;
                    dr["Month"] = "9";
                    dtResult.Rows.Add(dr);
                }
                if (cboct.Checked == true)
                {
                    dr = dtResult.NewRow();
                    dr["DocID"] = DocId;
                    dr["Month"] = "10";
                    dtResult.Rows.Add(dr);
                }
                if (cbnov.Checked == true)
                {
                    dr = dtResult.NewRow();
                    dr["DocID"] = DocId;
                    dr["Month"] = "11";
                    dtResult.Rows.Add(dr);
                }
                if (cbdec.Checked == true)
                {
                    dr = dtResult.NewRow();
                    dr["DocID"] = DocId;
                    dr["Month"] = "12";
                    dtResult.Rows.Add(dr);
                }
            }
            return dtResult;
        }

        private void SelectDocsOnLB(DataTable dtDocs)
        {
            foreach (DataRow itemDr in dtDocs.Rows)
            {
                string DocID = itemDr["DocumentID"].ToString();
                for (int x = 0; x < lbSops.Items.Count; x++)
                {
                    if (lbSops.Items[x].Value == DocID)
                    {
                        lbSops.Items[x].Selected = true;
                    }
                }
            }
        }

        private void GetHistoryData()
        {
            objTMS_BAL = new TMS_BAL();
            DataSet ds = objTMS_BAL.GetAnnualTrainingCalenderView(ATC_ID);
            DataTable dt = ds.Tables[0];
            
        }
        private void GetDataforAnnaualCalendar()
        {

            try
            {
                //gvATCHistory.Visible = false;
                btnComment.Visible = false;
                btnHistory.Visible = false;
                divHistoryTitle.Visible = true;
                int LatestStatus = 0;
                objTMS_BAL = new TMS_BAL();
                DataSet ds = objTMS_BAL.GetAnnualTrainingCalenderView(ATC_ID);
                DataTable dt = ds.Tables[0];

                if (ds.Tables[2].Rows.Count > 0)
                {
                    var txtcmts = ds.Tables[2].Rows[0]["Remarks"].ToString();
                    txtRemarks.Text= txtcmts == "" ? "NA": txtcmts;
                    lblRevertBy.Text = ds.Tables[2].Rows[0]["HodAction"].ToString();
                }
                if (ds.Tables[3].Rows.Count > 0)
                {
                    int AtcHod = Convert.ToInt32(ds.Tables[3].Rows[0]["ATC_HOD_Approval"]);
                    int AtcQA = Convert.ToInt32(ds.Tables[3].Rows[0]["ATC_QA_Approval"]);
                    int AtcTrainer = Convert.ToInt32(ds.Tables[3].Rows[0]["ATC_Author"]);
                    LatestStatus = Convert.ToInt32(ds.Tables[0].Rows[0]["ATC_Status"]);
                    if (Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) == AtcHod)
                    {
                        if (LatestStatus == 1 || LatestStatus == 3 || LatestStatus == 6)//Author creation and Modification when hod and qa revert
                        {
                            CommentsDiv.Visible = true;
                            divHistoryTitle.Visible = false;
                            btnHistory.Visible = true;
                            lblTitleforRevertBy.InnerText = "Author Comments :";
                            RevertDiv.Visible = true;
                            divButtons.Visible = true;
                            btnHodapprove.Visible = true;
                            btnHodRevert.Visible = true;
                        }
                    }
                    if (Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) == AtcQA)
                    {

                        if (LatestStatus == 4 || LatestStatus == 7)//hod approve status=4,qa approve status=7
                        {
                            lblTitleforRevertBy.InnerText = "Approved By :";
                            if (LatestStatus == 4)
                            {
                                CommentsDiv.Visible = true;
                                btnHistory.Visible = true;
                                divHistoryTitle.Visible = false;
                                RevertDiv.Visible = true;
                                divButtons.Visible = true;
                                btnQAApprove.Visible = true;
                                btnQARevert.Visible = true;
                            }
                        }
                    }
                    if (Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) == AtcTrainer)
                    {

                        //RevertDiv.Visible = true;
                        CommentsDiv.Visible = false;
                        //btnHistory.Visible = true;
                        divHistoryTitle.Visible = true;
                        lblTitleforRevertBy.InnerText = "Author Comments :";
                        if (LatestStatus == 2 || LatestStatus == 5)//hod revert =2,QA Revert=5
                        {
                            RevertDiv.Visible = true;
                            btnHistory.Visible = true;
                            //btnApply.Visible = true;
                            btnApplydiv.Visible = true;
                            divButtons.Visible = true;
                            divHistoryTitle.Visible = false;
                            btnUpdate.Visible = true;
                            btnCancel.Visible = true;
                            btnSubmit.Visible = false;
                            gvATC.Enabled = true;
                            CommentsDiv.Visible = true;
                            lblTitleforRevertBy.InnerText = "Revert By :";
                            divDocType.Visible = SopDiv.Visible = true;
                            //code for loading and binding the lbsops 
                            DataSet dsDocuments = objTMS_BAL.getATC_DataforModification(ATC_ID);
                            if (dsDocuments.Tables[1].Rows.Count > 0)
                            {
                                BindDocType(dsDocuments.Tables[1]);
                            }
                            if (dsDocuments.Tables[2].Rows.Count > 0)
                            {
                                BindDocument(dsDocuments.Tables[2]);
                            }
                            // in dsDocuments.Tables[3] document assigned details like DocID,Docname,DocType,Month
                        }
                        if (LatestStatus == 7)//QA Approved status=7
                        {
                            divStatus.Visible = false;
                            btnHodapprove.Visible = false;
                            btnHodRevert.Visible = false;
                            btnQAApprove.Visible = false;
                            btnQARevert.Visible = false;
                            divRevertComment.Visible = false;
                            btnComment.Visible = false;
                            CommentsDiv.Visible = false;
                            divHistoryTitle.Visible = true;
                        }
                    }
                }
                if (ds.Tables[0].Rows.Count > 0)
                {
                    BindDepartments(Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                    ddlDepartment.SelectedValue = ds.Tables[0].Rows[0]["DeptID"].ToString();
                    txtYear.Text = ds.Tables[0].Rows[0]["CalendarYear"].ToString();
                    txtCreatedby.Text = ds.Tables[0].Rows[0]["EmpName"].ToString();
                    txtStatus.Text = ds.Tables[2].Rows[0]["CurrentStatus"].ToString();
                    txtHodApprover.Text = ds.Tables[0].Rows[0]["HodName"].ToString();
                    txtQAApprover.Text = ds.Tables[0].Rows[0]["QAName"].ToString();
                    Currentstatus = Convert.ToInt32(ds.Tables[0].Rows[0]["ATC_Status"]);
                    hdnStatus.Value = Currentstatus.ToString();
                    //CreatedbyDiv.Visible = true;
                    colsize_year.Visible = true;
                    colsize_author.Visible = true;
                    colsize_reviewer.Visible = true;
                    colsize_apprvr.Visible = true;
                    ddlDepartment.Enabled = false;
                    txtYear.Enabled = false;
                    // btnSubmit.Visible = false;
                    SubmitDiv.Visible = true;
                    divStatus.Visible = true;

                    DataTable dtDbList = ds.Tables[1];

                    DataTable dtTempCalendar = new DataTable();
                    dtTempCalendar.Columns.Add("DocumentID");
                    dtTempCalendar.Columns.Add("Document");
                    dtTempCalendar.Columns.Add("DocType");
                    dtTempCalendar.Columns.Add("JAN");
                    dtTempCalendar.Columns.Add("FEB");
                    dtTempCalendar.Columns.Add("MAR");
                    dtTempCalendar.Columns.Add("APR");
                    dtTempCalendar.Columns.Add("MAY");
                    dtTempCalendar.Columns.Add("JUN");
                    dtTempCalendar.Columns.Add("JUL");
                    dtTempCalendar.Columns.Add("AUG");
                    dtTempCalendar.Columns.Add("SEP");
                    dtTempCalendar.Columns.Add("OCT");
                    dtTempCalendar.Columns.Add("NOV");
                    dtTempCalendar.Columns.Add("DEC");

                    DataView view = new DataView(dtDbList);
                    DataTable distinctDocuments = view.ToTable(true, "DocumentID", "Document", "DocType");

                    //Generate Calendar from Table
                    foreach (DataRow drDistinctDocID in distinctDocuments.Rows)
                    {
                        DataRow drdtTempCalendar = dtTempCalendar.NewRow();
                        drdtTempCalendar["DocumentID"] = drDistinctDocID["DocumentID"];
                        drdtTempCalendar["Document"] = drDistinctDocID["Document"];
                        drdtTempCalendar["DocType"] = drDistinctDocID["DocType"];
                        drdtTempCalendar["JAN"] = false;
                        drdtTempCalendar["FEB"] = false;
                        drdtTempCalendar["MAR"] = false;
                        drdtTempCalendar["APR"] = false;
                        drdtTempCalendar["MAY"] = false;
                        drdtTempCalendar["JUN"] = false;
                        drdtTempCalendar["JUL"] = false;
                        drdtTempCalendar["AUG"] = false;
                        drdtTempCalendar["SEP"] = false;
                        drdtTempCalendar["OCT"] = false;
                        drdtTempCalendar["NOV"] = false;
                        drdtTempCalendar["DEC"] = false;
                        dtTempCalendar.Rows.Add(drdtTempCalendar);
                    }

                    //Based on DocID Generate Calendar
                    foreach (DataRow drDocID in dtTempCalendar.Rows)
                    {
                        string strDocID = drDocID["DocumentID"].ToString();

                        //Verify DocID on Each Month

                        if (dtDbList.Select(String.Format("DocumentID={0} and Month=1", strDocID)).Length > 0)
                        {
                            drDocID["JAN"] = true;
                        }
                        if (dtDbList.Select(String.Format("DocumentID={0} and Month=2", strDocID)).Length > 0)
                        {
                            drDocID["FEB"] = true;
                        }
                        if (dtDbList.Select(String.Format("DocumentID={0} and Month=3", strDocID)).Length > 0)
                        {
                            drDocID["MAR"] = true;
                        }
                        if (dtDbList.Select(String.Format("DocumentID={0} and Month=4", strDocID)).Length > 0)
                        {
                            drDocID["APR"] = true;
                        }
                        if (dtDbList.Select(String.Format("DocumentID={0} and Month=5", strDocID)).Length > 0)
                        {
                            drDocID["MAY"] = true;
                        }
                        if (dtDbList.Select(String.Format("DocumentID={0} and Month=6", strDocID)).Length > 0)
                        {
                            drDocID["JUN"] = true;
                        }
                        if (dtDbList.Select(String.Format("DocumentID={0} and Month=7", strDocID)).Length > 0)
                        {
                            drDocID["JUL"] = true;
                        }
                        if (dtDbList.Select(String.Format("DocumentID={0} and Month=8", strDocID)).Length > 0)
                        {
                            drDocID["AUG"] = true;
                        }
                        if (dtDbList.Select(String.Format("DocumentID={0} and Month=9", strDocID)).Length > 0)
                        {
                            drDocID["SEP"] = true;
                        }
                        if (dtDbList.Select(String.Format("DocumentID={0} and Month=10", strDocID)).Length > 0)
                        {
                            drDocID["OCT"] = true;
                        }
                        if (dtDbList.Select(String.Format("DocumentID={0} and Month=11", strDocID)).Length > 0)
                        {
                            drDocID["NOV"] = true;
                        }
                        if (dtDbList.Select(String.Format("DocumentID={0} and Month=12", strDocID)).Length > 0)
                        {
                            drDocID["DEC"] = true;
                        }
                    }
                    gvATC.DataSource = dtTempCalendar;
                    gvATC.DataBind();
                    ViewState["dtSelectedDocs"] = dtTempCalendar;
                    if (ddlDocType.Items.Count > 0)
                    {
                        DataRow[] drTemp = dtTempCalendar.Select("DocType=" + ddlDocType.SelectedValue);
                        DataTable dtTemp = new DataTable();
                        if (drTemp.Length > 0)
                        {
                            dtTemp = drTemp.CopyToDataTable();
                        }
                        SelectDocsOnLB(dtTemp);
                    }
                }
                upJQHistoryGrid.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "showalert", "ViewATC_History();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATC_M9:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                bool IsValid = ValidateAtcSubmit();
                if (IsValid)
                {
                    DataTable dtResult = ConvertCalendarToSendDB(gvATC);
                    objTMS_BAL.InsertTrainingCalender(Convert.ToInt32(ddlDepartment.SelectedValue), Convert.ToInt32(txtYear.Text),
                        Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]),
                        txtcomments.Text.Trim(), dtResult, InitializeATC_ID, out int result);                    
                    ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), 
                        "SubmissionMsgFromServer(" + result + ");", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATC_M16:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                bool IsValid = ValidateAtcSubmit();
                if (IsValid)
                {
                    DataTable dtResult = ConvertCalendarToSendDB(gvATC);
                    if (Currentstatus == 2)
                    {
                        hdnStatus.Value = "3";
                    }
                    else if (Currentstatus == 5)
                    {
                        hdnStatus.Value = "6";
                    }
                    objTMS_BAL.UpdateTrainingCalenderTrainer(ATC_ID, 
                        Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]), 
                        txtcomments.Text.Trim(), dtResult, 1, Convert.ToInt32(hdnStatus.Value), out int result);
                    if (result == 1)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Updated Successfully", "success", "RedirecttoPendingPage()");
                    }
                    else if (result == 2)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Calendar Already Exists.", "error");
                    }
                    else if (result == 3)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "You Are not Authorized Take Action.", "error");
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATC_M10:" + strline + "  " + strMsg, "error");
            }
        }

        private bool ValidateAtcSubmit()
        {
            bool IsAtcValid = false;

            int SelectedDocuments = gvATC.Rows.Count;
            int DocsNotSelectedOnAnyMonths = 0;
            if (SelectedDocuments > 0)
            {
                ValidateDocsSelection(gvATC, out DocsNotSelectedOnAnyMonths);
            }
            if (txtYear.Text == "" || SelectedDocuments == 0 || DocsNotSelectedOnAnyMonths > 0)
            {
                ArrayList Mandatory = new ArrayList();
                if (txtYear.Text == "")
                {
                    Mandatory.Add("Select Year");
                }
                if (SelectedDocuments == 0)
                {
                    Mandatory.Add("Select at least one Document");
                }
                else
                {
                    if (DocsNotSelectedOnAnyMonths != 0)
                    {
                        Mandatory.Add("Select at least one Month from each Document");
                    }
                }
                string strMsg = string.Join("<br/> ", Mandatory.ToArray());
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"),
                   "SubmissionServerValidation('" + strMsg + "');", true);
                IsAtcValid = false;
            }
            else
            {
                IsAtcValid = true;
            }
            return IsAtcValid;
        }

        private void ValidateDocsSelection(GridView gvAtcDocs, out int DocsNotSelectedOnAnyMonths)
        {
            DocsNotSelectedOnAnyMonths = 0;
            try
            {
                foreach (GridViewRow row in gvATC.Rows)
                {
                    int count = 0;
                    Label lblDocID = row.FindControl("lblDocID") as Label;
                    CheckBox cbJan = (CheckBox)row.FindControl("cbJAN");
                    CheckBox cbfeb = (CheckBox)row.FindControl("cbFEB");
                    CheckBox cbmar = (CheckBox)row.FindControl("cbMAR");
                    CheckBox cbapr = (CheckBox)row.FindControl("cbAPR");
                    CheckBox cbmay = (CheckBox)row.FindControl("cbMAY");
                    CheckBox cbjun = (CheckBox)row.FindControl("cbJUN");
                    CheckBox cbjul = (CheckBox)row.FindControl("cbJUL");
                    CheckBox cbaug = (CheckBox)row.FindControl("cbAUG");
                    CheckBox cbsep = (CheckBox)row.FindControl("cbSEP");
                    CheckBox cboct = (CheckBox)row.FindControl("cbOCT");
                    CheckBox cbnov = (CheckBox)row.FindControl("cbNOV");
                    CheckBox cbdec = (CheckBox)row.FindControl("cbDEC");

                    string DocId = lblDocID.Text;

                    if (cbJan.Checked == true)
                    {
                        count += 1;
                    }
                    if (cbfeb.Checked == true)
                    {
                        count += 1;
                    }
                    if (cbmar.Checked == true)
                    {
                        count += 1;
                    }
                    if (cbapr.Checked == true)
                    {
                        count += 1;
                    }
                    if (cbmay.Checked == true)
                    {
                        count += 1;
                    }
                    if (cbjun.Checked == true)
                    {
                        count += 1;
                    }
                    if (cbjul.Checked == true)
                    {
                        count += 1;
                    }
                    if (cbaug.Checked == true)
                    {
                        count += 1;
                    }
                    if (cbsep.Checked == true)
                    {
                        count += 1;
                    }
                    if (cboct.Checked == true)
                    {
                        count += 1;
                    }
                    if (cbnov.Checked == true)
                    {
                        count += 1;
                    }
                    if (cbdec.Checked == true)
                    {
                        count += 1;
                    }
                    if (count == 0)
                    {
                        DocsNotSelectedOnAnyMonths += 1;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATC_M16b:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnHodapprove_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["HodApproveOrRevert"] = "approve";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "openElectronicSignModal();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATC_M11:" + strline + "  " + strMsg, "error");
            }
        }

        private void HODApprove()
        {
            objTMS_BAL = new TMS_BAL();
            objTMS_BAL.UpdateTrainingCalender(ATC_ID, 
                Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]), 
                txtcomments.Text.Trim(), 2, 4, out int result);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HideElectronicSignModal();", true);
            if (result == 1)
            {
                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Approved Successfully", "success", "RedirecttoHodPage()");
            }
            else if (result == 2)
            {
                HelpClass.custAlertMsg(this, this.GetType(), "Action Already Done", "error");
            }
            else if (result == 3)
            {
                HelpClass.custAlertMsg(this, this.GetType(), "You are Not Authorized To Do Action.", "error");
            }
        }

        protected void btnHodRevert_Click(object sender, EventArgs e)
        {
            //gvATCHistory.Visible = false;
            divRevertComment.Visible = true;
            try
            {
                if (String.IsNullOrEmpty(txtcomments.Text.Trim()))
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Enter Revert Reason in Comments", "error");
                    return;
                }
                ViewState["HodApproveOrRevert"] = "revert";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "openElectronicSignModal();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATC_M12:" + strline + "  " + strMsg, "error");
            }
        }

        private void HODRevert()
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                objTMS_BAL.UpdateTrainingCalender(ATC_ID,
                    Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]), 
                    txtcomments.Text.Trim(), 2, 2, out int result);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HideElectronicSignModal();", true);
                if (result == 1)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Reverted Successfully", "success", "RedirecttoHodPage()");
                }
                else if (result == 2)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Action Already Done", "error");
                }
                else if (result == 3)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "You are Not Authorized To Do Action.", "error");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATC_M12:" + strline + "  " + strMsg, "error");
            }

        }
        protected void btnQAApprove_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["QAApproveOrRevert"] = "approve";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "openElectronicSignModal();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATC_M13:" + strline + "  " + strMsg, "error");
            }
        }

        private void QAApprove()
        {
            objTMS_BAL = new TMS_BAL();
            objTMS_BAL.UpdateTrainingCalender(ATC_ID, 
                Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]), 
                txtcomments.Text.Trim(), 3, 7, out int result);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HideElectronicSignModal();", true);
            if (result == 1)
            {
                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Approved Successfully", "success", "RedirecttoQAPage()");
            }
            else if (result == 2)
            {
                HelpClass.custAlertMsg(this, this.GetType(), "Action Already Done", "error");
            }
            else if (result == 3)
            {
                HelpClass.custAlertMsg(this, this.GetType(), "You are Not Authorized To Do Action.", "error");
            }
        }

        protected void btnQARevert_Click(object sender, EventArgs e)
        {
            //gvATCHistory.Visible = false;
            divRevertComment.Visible = true;
            try
            {
                if (String.IsNullOrEmpty(txtcomments.Text.Trim()))
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Enter Revert Reason in Comments", "error");
                    return;
                }
                ViewState["QAApproveOrRevert"] = "revert";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "openElectronicSignModal();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATC_M14:" + strline + "  " + strMsg, "error");
            }
        }

        private void QARevert()
        {
            objTMS_BAL = new TMS_BAL();
            objTMS_BAL.UpdateTrainingCalender(ATC_ID, 
                Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]), 
                txtcomments.Text.Trim(), 3, 5, out int result);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HideElectronicSignModal();", true);
            if (result == 1)
            {
                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Reverted Successfully", "success", "RedirecttoQAPage()");
            }
            else if (result == 2)
            {
                HelpClass.custAlertMsg(this, this.GetType(), "Action Already Done", "error");
            }
            else if (result == 3)
            {
                HelpClass.custAlertMsg(this, this.GetType(), "You are Not Authorized To Do Action.", "error");
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["MainListACT_ID"] != null)
                {
                    Response.Redirect("~/TMS/ATC/List/ATC_List.aspx");
                }
                else if (Session["HodACT_ID"] != null)
                {
                    Response.Redirect("~/TMS/ATC/List/ATC_Hod_Approval.aspx?FilterType=2");
                }
                else if (Session["QAACT_ID"] != null)
                {
                    Response.Redirect("~/TMS/ATC/List/ATC_QA_Approve.aspx?FilterType=2");
                }
                else if (Session["PendingACT_ID"] != null)
                {
                    Response.Redirect("~/TMS/ATC/List/ATC_PendingList.aspx?FilterType=2");
                }
                else if (Session["InitializeATC_ID"] != null)
                {
                    Response.Redirect("~/TMS/ATC/List/CreateATC_List.aspx");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATC_M15:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnHistory_Click(object sender, EventArgs e)
        {
            try
            {
                // gvATCHistory.Visible = true;
                divRevertComment.Visible = false;
                btnHistory.Visible = false;
                divHistoryTitle.Visible = true;
                btnComment.Visible = true;
                objTMS_BAL = new TMS_BAL();
                DataSet ds = objTMS_BAL.GetAnnualTrainingCalenderView(ATC_ID);
                if (ds.Tables[3].Rows.Count > 0)
                {
                    int Status1 = Convert.ToInt32(ds.Tables[0].Rows[0]["ATC_Status"]);
                    if (Status1 == 7)
                    {
                        btnComment.Visible = false;
                    }
                }
                divHistoryTitle.Visible = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "showalert", "ViewATC_History();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATC_M16:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnComment_Click(object sender, EventArgs e)
        {
            btnHistory.Visible = true;
            divHistoryTitle.Visible = false;
            divRevertComment.Visible = true;
            //  gvATCHistory.Visible = false;
            btnComment.Visible = false;
        }
        protected void btnRedirectToAtcList_Click(object sender, EventArgs e)
        {
            try
            {
                //if (Session["MainListACT_ID"] != null)
                //{
                //    Response.Redirect("~/TMS/ATC/List/ATC_List.aspx");
                //}
                //else if (Session["HodACT_ID"] != null)
                //{
                //    Response.Redirect("~/TMS/ATC/List/ATC_Hod_Approval.aspx?FilterType=2");
                //}
                //else if (Session["QAACT_ID"] != null)
                //{
                //    Response.Redirect("~/TMS/ATC/List/ATC_QA_Approve.aspx?FilterType=2");
                //}
                //else if (Session["PendingACT_ID"] != null)
                //{
                //    Response.Redirect("~/TMS/ATC/List/ATC_PendingList.aspx?FilterType=2");
                //}
                //else if (Session["InitializeATC_ID"] != null)
                //{
                //    Response.Redirect("~/TMS/ATC/List/CreateATC_List.aspx");
                //}

                Response.Redirect(hdnPreviousPage.Value);

            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATC_M17:" + strline + "  " + strMsg, "error");
            }
        }

        //added for electronic sign
        protected void ElectronicSign_buttonClick(object sender, EventArgs e)
        {
            try
            {
                bool b = ElectronicSign.IsPasswordValid;
                if (b == true)
                {
                    if (ViewState["HodApproveOrRevert"].ToString() != null)
                    {
                        if (ViewState["HodApproveOrRevert"].ToString().ToLower() == "approve")//Hod Approve
                        {
                            HODApprove();
                        }
                    }
                    if (ViewState["HodApproveOrRevert"].ToString() != null)
                    {
                        if (ViewState["HodApproveOrRevert"].ToString().ToLower() == "revert")// Hod Revert
                        {
                            HODRevert();
                        }
                    }
                    if (ViewState["QAApproveOrRevert"].ToString() != null)
                    {
                        if (ViewState["QAApproveOrRevert"].ToString().ToLower() == "approve")//QA Approve
                        {
                            QAApprove();
                        }
                    }
                    if (ViewState["QAApproveOrRevert"].ToString() != null)
                    {
                        if (ViewState["QAApproveOrRevert"].ToString().ToLower() == "revert")// QA Revert
                        {
                            QARevert();
                        }
                    }
                }
                if (b == false)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Password is Incorrect.", "error", "focusOnElecPwd()");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATC_M18:" + strline + "  " + strMsg, "error");
            }
        }

        protected void ddlDocType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(ddlDocType.SelectedValue) > 0)
                {
                    int DocType = Convert.ToInt32(ddlDocType.SelectedValue);
                    objTMS_BAL = new TMS_BAL();
                    DataSet ds = objTMS_BAL.GetEffectedDocsforAtcCreation(DocType, Convert.ToInt32(ddlDepartment.SelectedValue));
                    DataTable dt = ds.Tables[0];
                    BindDocument(dt);

                    if (ViewState["dtSelectedDocs"] != null)
                    {
                        DataTable dtDocs = ViewState["dtSelectedDocs"] as DataTable;
                        DataRow[] drTemp = dtDocs.Select("DocType=" + ddlDocType.SelectedValue);
                        DataTable dtTemp = new DataTable();
                        if (drTemp.Length > 0)
                        {
                            dtTemp = drTemp.CopyToDataTable();
                        }
                        SelectDocsOnLB(dtTemp);
                    }
                }
            }
            catch (Exception ex)
            {

                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATC_M19:" + strline + "  " + strMsg, "error");
            }
        }
    }
}