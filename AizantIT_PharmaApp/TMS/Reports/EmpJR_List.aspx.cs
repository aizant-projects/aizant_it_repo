﻿using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.Web.UI;
using TMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.Reports
{
    public partial class EmpJR_List : System.Web.UI.Page
    {        
        TMS_BAL objTMS_BAL;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {                    
                    if (!IsPostBack)
                    {
                        hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID="+(int)Modules.TMS);
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0 || dtTemp.Select("RoleID=" + (int)TMS_UserRole.QA_TMS).Length > 0 || dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainer_TMS).Length > 0)
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }                   
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "EJRL_1:" + strline + "  " + strMsg, "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            try
            {
                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                divAutorizedMsg.Visible = true;
                divMainContainer.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "EJRL_2:" + strline + "  " + strMsg, "error");
            }
        }
        private void InitializeThePage()
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "EJRL_3:" + strline + "  " + strMsg, "error");
            }
        } 
        
        protected void btnViewJR_Details_Click(object sender, EventArgs e)
        {
            viewJR_Details();
        }       
        private void viewJR_Details()
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                if (hdnJR_ID.Value != "")
                {
                    int JR_ID = Convert.ToInt32(hdnJR_ID.Value);
                    DataTable dt = objTMS_BAL.GetEmployeeJrList_View(JR_ID);
                    if (dt.Rows.Count > 0)
                    {
                        int a = Convert.ToInt32(dt.Rows[0]["JR_ID"].ToString());
                        lblJrName.Text = dt.Rows[0]["JR_Name"].ToString();
                        lblJrVersion.Text = dt.Rows[0]["JR_Version"].ToString();
                        lblTraineeName.Text = dt.Rows[0]["TraineeName"].ToString();
                        lblTraineeCode.Text= dt.Rows[0]["TraineeCode"].ToString();
                        lblTraineeDOJ.Text= dt.Rows[0]["TraineeDOJ"].ToString(); ;
                        Literal1.Text = dt.Rows[0]["JR_Description"].ToString();
                        lblPreparedBy.Text = dt.Rows[0]["PreparedBy_Hod"].ToString();
                        lblPreparedDate.Text = dt.Rows[0]["HOD_AssignedDate"].ToString();
                        lblTS_PreparedBy.Text = dt.Rows[0]["TS_PreparedBy_HOD"].ToString();
                        lblTS_PreparedDate.Text = dt.Rows[0]["TS_CreatedDate"].ToString();
                        lblApprovedBy.Text = dt.Rows[0]["Approvedby_QA"].ToString();
                        lblApprovedDate.Text = dt.Rows[0]["ApprovedDate_QA"].ToString();
                        lblTS_ApprovedBy.Text = dt.Rows[0]["TS_ApprovedBy_QA"].ToString();
                        lblTS_ApprovedDate.Text = dt.Rows[0]["TS_ApprovalDate"].ToString();
                    }

                    upJrDetails.Update();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "OpenModelViewJR();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "EJRL_8:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnAskFeedBack_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdnEmpFeedbackID.Value != "0")
                {
                    ucFeedback.ViewFeedbackForm(Convert.ToInt32(hdnEmpFeedbackID.Value),(int)TmsBaseType.JR_Training);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "EJRL_9:" + strline + "  " + strMsg, "error");
            }
        }
    }
}