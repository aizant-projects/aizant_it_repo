﻿<%@ Page Title="Document Reports" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" AutoEventWireup="true" CodeBehind="DocumentReports.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Reports.DocumentReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:HiddenField ID="hdnMinDate" runat="server" />
    <asp:HiddenField ID="hdnDocID" runat="server" Value="0" />

    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div class="col-12 padding-none grid_panel_full float-left" id="mainContainer" runat="server" style="padding-bottom: 10px">
                <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 ">Document Reports</div>
                <div id="divHearderBody" class="float-left col-lg-12 padding-none">
                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 top bottom">
                        <div class="form-group col-3  float-left padding-none">
                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                <label>Training<span class="mandatoryStar">*</span></label>
                                <select class="col-lg-12 regulatory_dropdown_style form-control selectpicker padding-none" onchange="ddlTrainingOnChange();" id="ddlTraining">
                                    <option value="1">Completed Training</option>
                                    <option value="2">Ongoing Training</option>
                                </select>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="upDeptAndDocType" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <div>
                                    <div class="form-group  float-left col-lg-3 col-12 col-md-3 col-sm-12">
                                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                            <label>Department<span class="mandatoryStar">*</span></label>
                                            <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="regulatory_dropdown_style  selectpicker form-control"
                                                data-live-search="true" data-size="8" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="form-group  float-left col-lg-3 col-12 col-md-3 col-sm-12 ">
                                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                            <label>Document Type<span class="mandatoryStar">*</span></label>
                                            <asp:DropDownList ID="ddlDocType" runat="server" CssClass="regulatory_dropdown_style  selectpicker form-control"
                                                data-live-search="true" data-size="4" OnSelectedIndexChanged="ddlDocType_SelectedIndexChanged"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group float-left col-lg-3 col-12 col-md-3 col-sm-12 padding-none">
                                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                        <label>Document<span class="mandatoryStar">*</span></label>
                                        <asp:DropDownList ID="ddlDocument" runat="server" CssClass="regulatory_dropdown_style  selectpicker form-control"
                                            data-live-search="true" data-size="8"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="form-group col-sm-3 float-left padding-none" id="fromdtdiv">
                            <label for="lblDate" class="padding-none float-left col-lg-12 col-sm-12 col-12 col-md-12 control-label custom_label_answer">From Date</label>
                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                <input type='text' autocomplete="off" runat="server" id="txtFromDate" class="form-control login_input_sign_up" placeholder="DD MMM YYYY" onpaste="return false" />
                            </div>
                        </div>
                        <div class="col-sm-3  float-left form-group bootstrap-timepicker timepicker padding-none" id="todtdiv">
                            <label for="lblToDate" class=" float-left col-lg-12 col-sm-12 col-12 col-md-12">To Date</label>
                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 ">
                                <input type='text' autocomplete="off" runat="server" id="txtToDate" class="form-control login_input_sign_up" placeholder="DD MMM YYYY" onpaste="return false" />
                            </div>
                        </div>
                        <div class=" form-group col-sm-4 padding-none float-right" style="margin-top: 20px;">
                            <input type="button" id="btnGetReports" class="float-right  btn-revert_popup" onclick="DocumentReportValidations();" value="Get Reports" />
                        </div>
                    </div>
                </div>
                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 bottom" id="divDocTrainingReports">
                    <table id="tblDocTrainingReport" class="datatable_cust tblDocTrainingReportClass breakword display" style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Document</th>
                                <th style="width:1%;">Version</th>
                                <th>Emp.Code</th>
                                <th>Trainee</th>
                                <th>Training Date</th>
                                <th>Trainer</th>
                                <th>Type</th>
                            </tr>
                        </thead>
                    </table>
                </div>

                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 bottom padding-none" id="divDocOnGoingTrainingReports">
                    <table id="tblDocOnGoingTrainingReport" class="datatable_cust tblDocOnGoingTrainingReportClass breakword display" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Document</th>
                                <th style="width:1%;">Version</th>
                                <th>Emp.Code</th>
                                <th>Trainee</th>
                                <th>Training Status</th>
                                <th>Trainer</th>
                                <th>Type</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!--jQuery Datatable Script For Doc On Going Training Report-->
    <script>
        $.ajaxSetup({
            cache: false
        }
        );

        var DocOnGoingTrainingReport;
        $(function () {
            loadDocOnGoingTrainingList();
        });

        $('#tblDocOnGoingTrainingReport thead tr').clone(true).appendTo('#tblDocOnGoingTrainingReport thead');
        $('#tblDocOnGoingTrainingReport thead tr:eq(1) th').each(function (i) {
            if (i < 7) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Search " />');

                $('input', this).on('keyup change', function () {
                    if (DocOnGoingTrainingReport.column(i).search() !== this.value) {
                        DocOnGoingTrainingReport
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
                //if (i == 1) {
                //    $(this).html('');
                //}
            }
            else {
                $(this).html('');
            }
        });
        function loadDocOnGoingTrainingList() {
            if (DocOnGoingTrainingReport != null) {
                DocOnGoingTrainingReport.destroy();
            }
            DocOnGoingTrainingReport = $('#tblDocOnGoingTrainingReport').DataTable({
                columns: [
                    { 'data': 'Document' },
                    { 'data': 'VersionNumber' },
                    { 'data': 'TraineeEmpCode' },
                    { 'data': 'TraineeName' },
                    { 'data': 'TrainingStatus' },
                    { 'data': 'TrainerNames' },
                    { 'data': 'Type' }
                ],
                "order": [[4, "desc"]],
                "scrollY": "285px",
                "bServerSide": true,
                "orderCellsTop": true,
                "lengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
                "aoColumnDefs": [{ className: "textAlignLeft", "targets": [0, 2, 3, 5, 6] },
                { "bSortable": false, "aTargets": [0] }],
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/DocTrainingReport_Service.asmx/GetDocOnGoingTrainingReports")%>",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData.push({ "name": "DocumentID", "value": $('#<%=hdnDocID.ClientID%>').val() },
                            { "name": "FromDate", "value": $('#<%=txtFromDate.ClientID%>').val() },
                            { "name": "ToDate", "value": $('#<%=txtToDate.ClientID%>').val() });
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success": function (msg) {
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                UserSessionCheck();
                                $("#tblDocOnGoingTrainingReport").show();
                                $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                                $(".tblDocOnGoingTrainingReportClass").css({ "width": "100%" });
                            },
                            error: function (xhr, textStatus, error) {
                                if (typeof console == "object") {
                                    console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                                }
                            }
                        });
                    }
                });
        }

        function showDetails() {
            //so something funky with the data
        }
    </script>

    <!--jQuery Datatable Script For Doc Training Report-->
    <script>
        $.ajaxSetup({
            cache: false
        });

        var oTableDocTrainingList;
        $(function () {
            loadDocTrainingList();
        });

        $('#tblDocTrainingReport thead tr').clone(true).appendTo('#tblDocTrainingReport thead');
        $('#tblDocTrainingReport thead tr:eq(1) th').each(function (i) {
            if (i < 7) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Search " />');

                $('input', this).on('keyup change', function () {
                    if (oTableDocTrainingList.column(i).search() !== this.value) {
                        oTableDocTrainingList
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
                //if (i == 1) {
                //    $(this).html('');
                //}
            }
            else {
                $(this).html('');
            }
        });

        function loadDocTrainingList() {
            if (oTableDocTrainingList != null) {
                oTableDocTrainingList.destroy();
            }
            oTableDocTrainingList = $('#tblDocTrainingReport').DataTable({
                columns: [
                    { 'data': 'Document' },
                    { 'data': 'VersionNumber' },
                    { 'data': 'TraineeEmpCode' },
                    { 'data': 'TraineeName' },
                    { 'data': 'TrainingDate' },
                    { 'data': 'TrainerName' },
                    { 'data': 'Type' }
                ],
                "order": [[4, "desc"]],
                "scrollY": "285px",
                "bServerSide": true,
                "orderCellsTop": true,
                "lengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
                "aoColumnDefs": [{ className: "textAlignLeft", "targets": [0, 2, 3, 5, 6] }],
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/DocTrainingReport_Service.asmx/GetDocReports")%>",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData.push({ "name": "DocumentID", "value": $('#<%=hdnDocID.ClientID%>').val() },
                            { "name": "FromDate", "value": $('#<%=txtFromDate.ClientID%>').val() },
                            { "name": "ToDate", "value": $('#<%=txtToDate.ClientID%>').val() });
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success": function (msg) {
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                UserSessionCheck();
                                $("#tblDocTrainingReport").show();
                                $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                                $(".tblDocTrainingReportClass").css({ "width": "100%" });
                            },
                            error: function (xhr, textStatus, error) {
                                if (typeof console == "object") {
                                    console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                                }
                            }
                        });
                    }
                });
        }

        function showDetails() {
            //so something funky with the data
        }
    </script>


    <!--Date picker Script-->
    <script>
        $(function () {
            loadDatePickers();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            loadDatePickers();
        });

        function loadDatePickers() {
            $('#<%=txtFromDate.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                //maxDate: new Date('<%=hdnMinDate.Value%>'),
                useCurrent: false
            });
            $('#<%=txtToDate.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                //maxDate: new Date('<%=hdnMinDate.Value%>'),
                useCurrent: false
            });
        }

        $('#<%=txtFromDate.ClientID%>').on("dp.change", function (e) {
            $('#<%=txtToDate.ClientID%>').data("DateTimePicker").minDate(e.date);
            $('#<%=txtToDate.ClientID%>').val("");
        });
    </script>

    <!--Validation on Get Report Button-->
    <script>
        function DocumentReportValidations() {
            var DocumentType = document.getElementById("<%=ddlDocType.ClientID%>");
            var Department = document.getElementById("<%=ddlDepartment.ClientID%>");
            var Document = document.getElementById("<%=ddlDocument.ClientID%>");
            var FromDate = document.getElementById("<%=txtFromDate.ClientID%>");
            var ToDate = document.getElementById("<%=txtToDate.ClientID%>");
            errors = [];
            if (DocumentType.value == 0) {
                errors.push("Select DocumentType");
            }
            if (Department.value == 0) {
                errors.push("Select Department");
            }
            if (Document.value == 0) {
                errors.push("Select Document");
            }
            if (FromDate.value != "" && ToDate.value != "") {
                if ((new Date(FromDate.value)) > (new Date(ToDate.value))) {
                    errors.push("From Date Should be less than To Date");
                }
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                // DocID
                $('#<%=hdnDocID.ClientID%>').val($('#<%=ddlDocument.ClientID%> option:selected').val());
                var ddlValue = $("#ddlTraining").val();
                if (ddlValue == 1) {
                    loadDocTrainingList();
                }
                else {
                    loadDocOnGoingTrainingList();
                }
            }
        }
    </script>

    <!--On Change-->
    <script>
        $(function () {
            $("#divDocOnGoingTrainingReports").hide();
        })
        function ddlTrainingOnChange() {
            $('#<%=hdnDocID.ClientID%>').val("0");
            $('#<%=txtFromDate.ClientID%>').val('');
            $('#<%=txtToDate.ClientID%>').val('');
            $('#<%=ddlDepartment.ClientID%>').val("0");
            $('#<%=ddlDepartment.ClientID%>').selectpicker("refresh");
            $('#<%=ddlDocument.ClientID%>').empty();
            $('#<%=ddlDocument.ClientID%>').selectpicker("refresh");
            $('#<%=ddlDocType.ClientID%>').val("1");
            $('#<%=ddlDocType.ClientID%>').selectpicker("refresh");
            var ddlValue = $("#ddlTraining").val();
            if (ddlValue == 1) {
                $("#fromdtdiv").show();
                $("#todtdiv").show();
                $("#divDocOnGoingTrainingReports").hide();
                $("#divDocTrainingReports").show();
                loadDocTrainingList();
            }
            else {
                $("#fromdtdiv").hide();
                $("#todtdiv").hide();
                $("#divDocTrainingReports").hide();
                $("#divDocOnGoingTrainingReports").show();
                loadDocOnGoingTrainingList();
            }
        }
    </script>

</asp:Content>
