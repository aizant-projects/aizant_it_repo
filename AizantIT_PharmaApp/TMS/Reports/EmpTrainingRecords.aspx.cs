﻿using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.Web.UI.WebControls;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.Reports
{
    public partial class EmpTrainingRecords : System.Web.UI.Page
    {
        UMS_BAL objUMS_BAL;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID="+(int)Modules.TMS);
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0 || dtTemp.Select("RoleID=" + (int)TMS_UserRole.QA_TMS).Length > 0 || dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainer_TMS).Length > 0)
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }                   
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ETR_1:" + strline + "  " + strMsg, "error");
            }
        }

            private void loadAuthorizeErrorMsg()
            {
                try
                {
                    lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                    divAutorizedMsg.Visible = true;
                    divMainContainer.Visible = false;
                }
                catch (Exception ex)
                {
                    string strline = HelpClass.LineNo(ex);
                    string strMsg = HelpClass.SQLEscapeString(ex.Message);
                    HelpClass.custAlertMsg(this, this.GetType(), "ETR_2:" + strline + "  " + strMsg, "error");
                }
            }

        private void InitializeThePage()
        {
            try
            {
                BindDepartments(Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]), Modules.TMS);
                upDropdowns.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ETR_3:" + strline + "  " + strMsg, "error");
            }
        }

        public void bindEmptyDropdowns()
        {
            try
            {
                ddlAccesibleDepartments.Items.Clear();
                ddlAccesibleDepartments.Items.Insert(0, new ListItem("- Select Department -", "0"));
                ddlEmployee.Items.Clear();
                ddlEmployee.Items.Insert(0, new ListItem("- Select Employee -", "0"));
                upDropdowns.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ETR_4:" + strline + "  " + strMsg, "error");
            }
        }

        public void BindDepartments(int EmpID, Modules modules)
        {
            try
            {
                bindEmptyDropdowns();
                objUMS_BAL = new UMS_BAL();
                int[] RoleIDs = new int[] { (int)TMS_UserRole.HOD_TMS, (int)TMS_UserRole.QA_TMS, (int)TMS_UserRole.Trainer_TMS };
                DataTable dt = objUMS_BAL.getRoleWiseDepts(EmpID, RoleIDs);
                if (dt.Rows.Count > 0)
                {
                    ddlAccesibleDepartments.Items.Clear();
                    ddlAccesibleDepartments.DataSource = dt;
                    ddlAccesibleDepartments.DataTextField = "DepartmentName";
                    ddlAccesibleDepartments.DataValueField = "DeptID";
                    ddlAccesibleDepartments.DataBind();
                    ddlAccesibleDepartments.Items.Insert(0, new ListItem("- Select Department -", "0"));
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ETR_5:" + strline + "  " + strMsg, "error");
            }
        }

        protected void ddlAccesibleDepartments_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(ddlAccesibleDepartments.SelectedValue) > 0)
                {
                    bindTrainees(ddlAccesibleDepartments.SelectedValue);                   
                }
                else
                {
                    ddlEmployee.Items.Clear();
                    ddlEmployee.Items.Insert(0, new ListItem("- Select Employee -", "0"));
                }
                upDropdowns.Update();
                
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ETR_6:" + strline + "  " + strMsg, "error");
            }
        }

        private void bindTrainees(string DeptID)
        {
            try
            {
                ddlEmployee.Items.Clear();
                objUMS_BAL = new UMS_BAL();
                DataTable dtemp = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(DeptID), ((int)TMS_UserRole.Trainee_TMS), false,2);
                if (dtemp.Rows.Count > 0)
                {
                    ddlEmployee.DataSource = dtemp;
                    ddlEmployee.DataValueField = "EmpID";
                    ddlEmployee.DataTextField = "EmpName";
                    ddlEmployee.DataBind();
                }
                ddlEmployee.Items.Insert(0, new ListItem("- Select Employee -", "0"));
                upDropdowns.Update();
                //ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "MyFun1", "ReloadTableEmpTrainingRecords();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ETR_7:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnViewFeedBack_Click(object sender, EventArgs e)
        {
            try
            {
                ucFeedback.ViewFeedbackForm(Convert.ToInt32(hdnEmpFeedbackID.Value), 0);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ETR_8:" + strline + "  " + strMsg, "error");
            }
        }
    }
 }
