﻿using AizantIT_PharmaApp.Common;
using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.Reports
{
    public partial class ScheduledJRAndTT_PendingList : System.Web.UI.Page
    {
        UMS_BAL objUMS_BAL;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=" + (int)Modules.TMS);
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.Dept_Head).Length > 0)
                                InitializeThePage();
                            else
                                LoadUnAuthorizedMessage();
                        }
                        else
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                    }
                }
                else
                    Response.Redirect("~/UserLogin.aspx", false);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ETR_1:" + strline + "  " + strMsg, "error");
            }
        }

        private void LoadUnAuthorizedMessage()
        {
            try
            {
                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                divAutorizedMsg.Visible = true;
                mainContainer.Visible = false;
            }
            catch (Exception ex)
            {
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_QAAL2:" + HelpClass.LineNo(ex) + "  " + HelpClass.SQLEscapeString(ex.Message), "error");
            }
        }

        private void InitializeThePage()
        {
            try
            {
                //Code to get the Page load Details.
                //objUMS_BAL = new UMS_BAL();
                //int[] RoleIDs = new int[] { (int)TMS_UserRole.HOD_TMS, (int)TMS_UserRole.QA_TMS, (int)TMS_UserRole.Trainer_TMS };
                //DataTable dt = objUMS_BAL.getRoleWiseDepts(Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]), new int[] { (int)TMS_UserRole.Dept_Head });
                //if (dt.Rows.Count > 0)
                //{
                //    ddlAccesibleDepartments.Items.Clear();
                //    ddlAccesibleDepartments.DataSource = dt;
                //    ddlAccesibleDepartments.DataTextField = "DepartmentName";
                //    ddlAccesibleDepartments.DataValueField = "DeptID";
                //    ddlAccesibleDepartments.DataBind();
                //    ddlAccesibleDepartments.Items.Insert(0, new ListItem("- All Departments -", "0"));
                //}
            }
            catch (Exception ex)
            {
                HelpClass.custAlertMsg(this, this.GetType(), "TTS_QAAL3:" + HelpClass.LineNo(ex) + "  " + HelpClass.SQLEscapeString(ex.Message), "error");
            }
        }

    }   
}