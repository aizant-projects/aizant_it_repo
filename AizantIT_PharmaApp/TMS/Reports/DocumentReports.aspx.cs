﻿using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.Web.UI.WebControls;
using TMS_BusinessLayer;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.Reports
{
    public partial class DocumentReports : System.Web.UI.Page
    {
        TMS_BAL objTMS_BAL;
        UMS_BAL objUMS_BAL;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID="+(int)Modules.TMS);
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0|| dtTemp.Select("RoleID=" + (int)TMS_UserRole.QA_TMS).Length > 0 || dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainer_TMS).Length > 0) 
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }                   
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "DocRep_1:" + strline + "  " + strMsg, "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            try
            {
                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                divAutorizedMsg.Visible = true;
                divMainContainer.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AQ2" + strline + "  " + strMsg, "error");
            }
        }
        public void InitializeThePage()
        {
            try
            {
                hdnMinDate.Value = DateTime.Now.Date.ToString("dd MMM yyyy");
                BindDocType();
                bindAllAccessibleDepartment(Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]), Modules.TMS);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void BindDocType()
        {
            try
            {
                TMS_BAL objTMS_BAL = new TMS_BAL();
                DataTable dt = objTMS_BAL.DocumentType();
                ddlDocType.DataSource = dt;
                ddlDocType.DataValueField = "DocumentTypeID";
                ddlDocType.DataTextField = "DocumentType";
                ddlDocType.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "DocRep_2:" + strline + "  " + strMsg, "error");
            }
        }

        public void bindAllAccessibleDepartment(int EmpID, Modules modules)
        {
            try
            {
                objUMS_BAL = new UMS_BusinessLayer.UMS_BAL();
                int[] RoleIDs = new int[] { (int)TMS_UserRole.HOD_TMS, (int)TMS_UserRole.Trainer_TMS,(int)TMS_UserRole.QA_TMS };
                DataTable dt = objUMS_BAL.getRoleWiseDepts(EmpID, RoleIDs);
                ddlDepartment.DataSource = dt;
                ddlDepartment.DataTextField = "DepartmentName";
                ddlDepartment.DataValueField = "DeptID";
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("-- Select Department --", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "Q5:" + strline + "  " + strMsg, "error");
            }

        }

        protected void ddlDocType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(ddlDocType.SelectedValue) > 0)
                {
                    BindDocuments(ddlDepartment.SelectedValue, ddlDocType.SelectedValue);
                    ddlDocument.Enabled = true;
                }
                else
                {
                    ddlDocument.Enabled = false;
                    ddlDocument.Items.Clear();
                    ddlDocument.Items.Insert(0, new ListItem("--Select Document--", "0"));

                }
            }
            catch (Exception ex)
            {

                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "DocRep_4:" + strline + "  " + strMsg, "error");
            }
        }

        public void BindDocuments( string DeptID,String DocumentType)
        {
            try
            {
                ddlDocument.Enabled = false;
                ddlDocument.Items.Clear();
                TMS_BAL objTMS_BAL = new TMS_BAL();
                DataTable dt = new DataTable();
                dt = objTMS_BAL.GetAllDocuments(DeptID, DocumentType);
                ddlDocument.DataSource = dt;
                ddlDocument.DataTextField = "DocumentName";
                ddlDocument.DataValueField = "DocumentID";
                ddlDocument.DataBind();
                ddlDocument.Items.Insert(0, new ListItem("-- Select Document --", "0"));
                ddlDocument.Enabled = true;
            }
            catch (Exception ex)
            {

                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "DocRep_5:" + strline + "  " + strMsg, "error");
            }
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlDocument.Enabled = false;
                ddlDocument.Items.Clear();
                int SelectedDepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue);
                if (SelectedDepartmentID > 0)
                {
                    if (ddlDocType.Items.Count>0)
                    {
                       BindDocuments(SelectedDepartmentID.ToString(),ddlDocType.SelectedValue);
                    }
                    else
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Document Type Not Exists to Get Documents", "warning");
                    }
                }
                else
                {
                    ddlDocument.Items.Insert(0, new ListItem("-- Select Document --", "0"));
                    ddlDocument.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "DocRep_6:" + strline + "  " + strMsg, "error");
            }
        }
    }
}