﻿using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.Reports
{
    public partial class MyTrainingRecords : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = (Session["UserDetails"] as DataSet).Tables[1].Select("ModuleID="+(int)Modules.TMS);
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID="+(int)TMS_UserRole.Trainee_TMS).Length > 0) // only for Trainee role
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                LoadUnAuthorizedMessage();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }                   
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "MTR1:" + strline + "  " + strMsg, "error");
            }
        }

        private void LoadUnAuthorizedMessage()
        {
            try
            {
                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                divAutorizedMsg.Visible = true;
                mainContainer.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "MTR2:" + strline + "  " + strMsg, "error");
            }
        }

        public void InitializeThePage()
        {
            try
            {
                hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "MTR3:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnViewFeedBack_Click(object sender, EventArgs e)
        {
            try
            {
                ucFeedback.ViewFeedbackForm(Convert.ToInt32(hdnEmpFeedbackID.Value), 0);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "MTR4:" + strline + "  " + strMsg, "error");
            }
        }
    }
}