﻿<%@ Page Title="My Training Records" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" AutoEventWireup="true" CodeBehind="MyTrainingRecords.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Reports.MyTrainingRecords" %>

<%@ Register Src="~/UserControls/TMS_UserControls/ucFeedback.ascx" TagPrefix="uc1" TagName="ucFeedback" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnEmpFeedbackID" runat="server" Value="0" />
    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="mainContainer" runat="server" visible="true">
            <div class="col-12 padding-none" id="Div3" runat="server">
                <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 ">
                    <asp:Label ID="Label1" runat="server" Font-Bold="true" Text="My Training Records"></asp:Label>
                </div>
                <div class="float-left col-lg-12 col-sm-12 top col-12 col-md-12 padding-none" style="overflow-y: auto;">
                    <table id="tblMyTrainingRecords" class="datatable_cust tblMyTrainingRecordsClass breakword display" style="width: 100%">
                        <thead>
                            <tr>
                                <th>DocumentID</th>
                                <th>Document</th>
                                <th>Version</th>
                                <th>Department</th>
                                <th>Mode of Training</th>
                                <th>Opinion</th>
                                <th>Evaluated By</th>
                                <th>Evaluated Date</th>
                                <th>Training Type</th>
                                <th>View FF</th>
                            </tr>
                        </thead>
                    </table>
                    <div class="col-12 grid_legend_tms">'FF' : Feedback Form, 'NS' : Not Satisfactory and 'S' :  Satisfactory </div>
                </div>
            </div>
        </div>
    </div>
    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upServerButtons" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <asp:Button ID="btnViewFeedBack" runat="server" Text="Button" OnClick="btnViewFeedBack_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <uc1:ucFeedback runat="server" ID="ucFeedback" />

    <!--Employee Training Records Datatable-->
    <script>
        function getUrlVars() {
            var vars = [], hash;
            var url = window.location.href;
            var hashes = url.slice(url.indexOf('?') + 1).split('&');
            if (url.indexOf("#") > 0) {
                var hashes = url.slice(url.indexOf('?') + 1, url.indexOf('#')).split('&');
            }

            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }

        var oTableMyTrainingRecords = $('#tblMyTrainingRecords').DataTable({
            columns: [
                { 'data': 'DocumentID' },
                { 'data': 'DocumentName' },
                { 'data': 'DocRefNo' },
                { 'data': 'DeptName' },
                { 'data': 'ModeOfTraining' },
                { 'data': 'Opinion' },
                { 'data': 'EvaluatedBy' },
                { 'data': 'EvaluatedDate' },
                { 'data': 'TrainingType' }, 
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) {
                        if (o.EmpFeedbackID == 0) {
                            return '';
                        }
                        else {
                            return '<a class="feedback_tms" title="View Feedback"  href="#" onclick="ViewEmp_Feedback(' + o.EmpFeedbackID + ');"></a>';
                        }
                    }
                },
                { 'data': 'CreatedYear' }
            ],
            "order": [[7, "desc"]],
            "scrollY": "380px",
            "aoColumnDefs": [{ "targets": [0,10], "visible": false }, { className: "textAlignLeft", "targets": [1, 3, 4, 6, 8] }],
            "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/EmpTrainingRecords_ListService.asmx/GetMyTrainingRecordsDataTable")%>",
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "EmpID", "value": $('#<%=hdnEmpID.ClientID%>').val() }, { "name": "FilterType", "value": getUrlVars()["FilterType"] },
                    { "name": "Year", "value": getUrlVars()["Year"] }, { "name": "DeptID", "value": getUrlVars()["DeptID"] },
                    { "name": "TargetType", "value": getUrlVars()["TargetType"] });//Passing Trainee ID
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        UserSessionCheck();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
        //To Reload Datatable
        function ReloadTableMyTrainingRecords() {
            oTableMyTrainingRecords.ajax.reload();
        }
    </script>
    <script>
        function ViewEmp_Feedback(EmpFeedbackID) {
            $('#<%=hdnEmpFeedbackID.ClientID%>').val(EmpFeedbackID);
            $("#<%=btnViewFeedBack.ClientID%>").click();
        }
    </script>
</asp:Content>
