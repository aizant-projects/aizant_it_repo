﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" AutoEventWireup="true" CodeBehind="TraineeQuestionDocList.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.TraineeQuestionsOnDocs.TraineeQuestionDocList" %>

<%@ Register Src="~/UserControls/TMS_UserControls/ucViewDocFAQs.ascx" TagPrefix="uc1" TagName="ucViewDocFAQs" %>
<%@ Register Src="~/UserControls/TMS_UserControls/ucViewQuestions.ascx" TagPrefix="uc1" TagName="ucViewQuestions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <style>
        #dvViewDociframe iframe{
            height:550px !important;
        }
    </style>

    <asp:HiddenField ID="hdnTrainerEmpID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnDocID" runat="server" Value="0" />

    <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12 grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="mainContainer" runat="server" visible="true">
            <div class="col-xs-12 padding-none">
                <div class="grid_header float-left col-lg-12  col-xs-12 col-md-12 col-sm-12 ">
                    <asp:Label ID="Label2" runat="server" Font-Bold="true" Text="Trainee Question On Document"></asp:Label>
                </div>
                <div class="float-left col-lg-12 col-sm-12 col-xs-12 col-md-12 top bottom padding-none">
                    <table id="tblDocumentWithQuestions" class="datatable_cust display breakword" style="width: 100%">
                        <thead>
                            <tr>
                                <th>DocID</th>
                                <th>Document</th>
                                <th>Department</th>
                                <th>UnAnswered Questions</th>
                                <th>Read</th>
                                <th>FAQs</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <%--modal popup to load Document Content--%>
    <div class="modal fade department" id="ReadDocModal" role="dialog">
        <div class="modal-dialog modal-lg" style="max-width: 90%; height: 600px">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header tmsModalHeader">
                    
                    <h4 class="modal-title Panel_Title" style="color: aliceblue">Document</h4>
                    <button type="button" class="close " data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row tmsModalContent">
                        <div class="float-left col-lg-12 col-md-12 col-xs-12 col-sm-12 padding-none">
                             <div id="dvViewDociframe"  style="width: 100%">
                                     <noscript>
                                                Please Enable Javascript.
                                            </noscript>
                                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--End modal popup to load Document Content--%>

    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <asp:Button ID="btnViewDocument" runat="server" Text="Button" OnClick="btnViewDocument_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <uc1:ucViewDocFAQs runat="server" ID="ucViewDocFAQs" />
    <uc1:ucViewQuestions runat="server" ID="ucViewQuestions" />

    <script>
        function ReadDoc(DocID) {
            $('#<%=hdnDocID.ClientID%>').val(DocID);
            $("#<%=btnViewDocument.ClientID%>").click();
            PdfViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>', 2, $('#<%=hdnDocID.ClientID%>').val(), 0, 0, "#dvViewDociframe",'');
        }

        function ViewDocument() {
            $('#ReadDocModal').modal({ backdrop: 'static', keyboard: false });
        }
    </script>

    <!--Documents which has Trainee Question jQuery DataTable-->
    <script>
        var oDocumentWithQuestionsForTrainer = $('#tblDocumentWithQuestions').DataTable({
            columns: [
                { 'data': 'DocID' },
                { 'data': 'Document' },
                { 'data': 'Department' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a class="grid_button"  data-toggle="modal" data-target="#modalViewQuestions" href="#" title="UnAnswered Questions/Asked Questions" onclick="ViewQuestions(' + o.DocID + ');">' + o.TraineeQuestions + '</a>'; }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a class="read_tms" href="#" onclick="ReadDoc(' + o.DocID + ');"></a>'; }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a class="view" href="#" data-toggle="modal" data-target="#modalDocFAQs"  onclick="ViewFAQs(' + o.DocID + ');"></a>'; }
                }
            ],
            "order": [3, "asc"],
            "scrollY": "360px",
            "aoColumnDefs": [{ "targets": [0], "visible": false }, { className: "textAlignLeft", "targets": [1, 2] }],
            "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TraineeQuestionsOnDoc/TraineeQuestionsOnDoc.asmx/GetDocmentsWithTraineeQuestions")%>",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "TrainerEmpID", "value": $(<%=hdnTrainerEmpID.ClientID%>).val() });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            UserSessionCheck();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
        });
        function ReloadTraineeQuestionForTrainer() {
            oDocumentWithQuestionsForTrainer.ajax.reload();
        }

    </script>

    <!--Document wise Questioned Trainees on JR and Target Training jQuery DataTable-->
    <script>
        function ViewJR_DQ_Trainees(DocID) {
            var oTableDQ_Trainees = $('#tblDQ_Trainees').DataTable({
                columns: [
                    { 'data': 'JrID' },
                    { 'data': 'JrName' },
                    { 'data': 'EmpCode' },
                    { 'data': 'Trainee' },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) { return '<a class="view"  href="#" onclick="ViewQuestions(' + o.JrID + ');"></a>'; }
                    }
                ],
                "order": [3, "asc"],
                "scrollY": "360px",
                "aoColumnDefs": [{ "targets": [0, 1], "visible": false }, { className: "textAlignLeft", "targets": [2, 3] }],
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TraineeQuestionsOnDoc/TraineeQuestionsOnDoc.asmx/GetDocmentsWithTraineeQuestions")%>",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "TrainerEmpID", "value": $(<%=hdnTrainerEmpID.ClientID%>).val() });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            UserSessionCheck();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        }
    </script>
    <script>
        function ViewFAQs(DocID) {
            OpenDocFAQPopup(DocID);
        }
        function ViewQuestions(DocID) {
            var TrainerID = $('#<%=hdnTrainerEmpID.ClientID%>').val();
            ViewQuestionsByTrainer(DocID, TrainerID);
        }
    </script>

    <script src='<%= Page.ResolveUrl("~/AppScripts/PDF_Viewer.min.js")%>'></script>
    
</asp:Content>
