﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.TraineeQuestionsOnDocs
{
    public partial class TraineeQuestionDocList : System.Web.UI.Page
    {

        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocObjects docObjects = new DocObjects();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = (Session["UserDetails"] as DataSet).Tables[1].Select("ModuleID="+(int)Modules.TMS);                        
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainer_TMS).Length > 0)
                            {
                                hdnTrainerEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                            }
                            else
                            {
                                LoadUnAuthorizedMessage();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }                  
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TQ01:" + strline + "  " + strMsg, "error");
            }
        }

        private void LoadUnAuthorizedMessage()
        {
            try
            {
                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                divAutorizedMsg.Visible = true;
                mainContainer.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TQ02:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnViewDocument_Click(object sender, EventArgs e)
        {
            try
            {
                AddDocDetailsToSession(hdnDocID.Value);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ViewDocument();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TQ03:" + strline + "  " + strMsg, "error");
            }
        }

        private void AddDocDetailsToSession(string DocID)
        {
            if (Convert.ToInt32(DocID) > 0)
            {
                Hashtable ht = new Hashtable();
                ht.Add("DocID", DocID.ToString());
                Session["Training"] = ht;
                string Url = ResolveUrl("~/TMS/Training/ReadDocInIFrame.aspx");
            }
        }
    }
}