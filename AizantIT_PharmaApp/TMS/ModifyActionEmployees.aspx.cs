﻿using AizantIT_PharmaApp.Common;
using System;
using System.Collections;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS
{
    public partial class ModifyActionEmployees : System.Web.UI.Page
    {
        TMS_BAL objTMS_BAL;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = (Session["UserDetails"] as DataSet).Tables[1].Select("ModuleID="+(int)Modules.TMS);
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.Admin_TMS).Length == 0) // if no Admin role
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }                   
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "MAE1:" + strline + "  " + strMsg, "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            try
            {
                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                divAutorizedMsg.Visible = true;
                divMainContainer.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "MAE2:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnBindEmployees_Click(object sender, EventArgs e)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                int DeptID = Convert.ToInt32(hdnDeptID.Value);
                int empBindStatus = Convert.ToInt32(hdnEmpBindStatus.Value);
                DataSet dsEmp = objTMS_BAL.getAllEmpbyDeptIdandRoleID(DeptID, empBindStatus);
                if (dsEmp.Tables.Count > 0)
                {
                    BindEmployeesByStatus(dsEmp, empBindStatus);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "MAE3:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindEmployeesByStatus(DataSet ds, int BindStatus)
        {
            try
            {
                
                //lblAuthors.Text = "Author";
                string TraineeEmpID = "0";
                ddlAuthor.Items.Clear();
                ddlReviewer.Items.Clear();
                ddlApprover.Items.Clear();
                divAuthorDropdown.Attributes.CssStyle.Add("display", "none");
                divReviewerDropdown.Attributes.CssStyle.Add("display", "none");
                divApproverDropdown.Attributes.CssStyle.Add("display", "none");
                if (hdnBaseID.Value == "1" || hdnBaseID.Value == "2")
                {
                    TraineeEmpID = hdnTraineeID.Value;
                }
                if (BindStatus == 1 || BindStatus == 2 || BindStatus == 3)
                {
                    divAuthorDropdown.Attributes.CssStyle.Remove("display");
                    DataView dvAuthor = new DataView(ds.Tables[0]);
                    dvAuthor.RowFilter = "EmpID<>" + TraineeEmpID;
                    ddlAuthor.DataSource = dvAuthor.ToTable();
                    //ddlAuthor.DataSource = ds.Tables[0];
                    ddlAuthor.DataTextField = "EmpName";
                    ddlAuthor.DataValueField = "EmpID";
                    ddlAuthor.DataBind();
                    ddlAuthor.Items.Insert(0, new ListItem("-- Select Author --", "0"));
                }
                if (BindStatus == 2 || BindStatus == 3)
                {
                    divReviewerDropdown.Attributes.CssStyle.Remove("display");
                    DataView dvReviewer = new DataView(ds.Tables[1]);
                    dvReviewer.RowFilter = "EmpID<>" + TraineeEmpID;
                    ddlReviewer.DataSource = dvReviewer.ToTable();
                    //ddlReviewer.DataSource = ds.Tables[1];
                    ddlReviewer.DataTextField = "EmpName";
                    ddlReviewer.DataValueField = "EmpID";
                    ddlReviewer.DataBind();
                    ddlReviewer.Items.Insert(0, new ListItem("-- Select Reviewer --", "0"));
                }
                if (BindStatus == 3)
                {
                    divApproverDropdown.Attributes.CssStyle.Remove("display");
                    DataView dvApprover = new DataView(ds.Tables[2]);
                    dvApprover.RowFilter = "EmpID<>" + TraineeEmpID;
                    ddlApprover.DataSource = dvApprover.ToTable();
                    //ddlApprover.DataSource = ds.Tables[2];
                    ddlApprover.DataTextField = "EmpName";
                    ddlApprover.DataValueField = "EmpID";
                    ddlApprover.DataBind();
                    ddlApprover.Items.Insert(0, new ListItem("-- Select Reviewer --", "0"));
                }
                if (ddlType.SelectedValue=="3")
                {
                    lblAuthors.Text = "Evaluator<span class='mandatoryStar'>*</span>";
                }
                else
                {
                    lblAuthors.Text = "Author<span class='mandatoryStar'>*</span>";
                }
                var TTS_CurrentStatusID = Convert.ToInt32(hdnTTS_CurrentStatusID.Value);
                if (TTS_CurrentStatusID > 3)
                {
                    ddlReviewer.Enabled = false;
                }
                else
                {
                    ddlReviewer.Enabled = true;
                }

                upModalActionEmps.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "modal", "openActionEmpPopup();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "MAE4:" + strline + "  " + strMsg, "error");
            }
        }

        #region For Submitting or Updating
        protected void btnEmployeesSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();

                int empID = (Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                if (ddlType.SelectedValue == "1" || ddlType.SelectedValue == "4")// for JR or Target TS
                {
                    ArrayList Mandatory = new ArrayList();
                    if (ddlAuthor.SelectedValue == "0")
                    {
                        Mandatory.Add("select Author");
                    }
                    if (ddlApprover.SelectedValue == "0")
                    {
                        Mandatory.Add("select Approver");
                    }
                    if (ddlAuthor.SelectedValue == ddlApprover.SelectedValue)
                    {
                        Mandatory.Add("Author and Reviewer Should not be same as a user");
                    }
                    if (txtRemarks.Value.Trim() == "")
                    {
                        Mandatory.Add("Enter Purpose of modification in reason field");
                    }
                    string strMsg = string.Join(",",Mandatory.ToArray());
                    if (Mandatory.Count > 0)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), strMsg+".", "error");
                    }
                    else
                    {
                        if (ddlType.SelectedValue == "1")
                        {
                            submitForJR(empID);
                        }
                        else
                        {
                            submitForTargetTs(empID);
                        }
                    }
                }
                else if (ddlType.SelectedValue == "2") // For JR TS.
                {
                    if (hdnEmpBindStatus.Value == "2")
                    {

                        ArrayList Mandatory = new ArrayList();
                        if (ddlAuthor.SelectedValue == "0")
                        {
                            Mandatory.Add("select Author");
                        }
                        if (ddlApprover.SelectedValue == "0")
                        {
                            Mandatory.Add("select Approver");
                        }
                        if (ddlAuthor.SelectedValue == ddlApprover.SelectedValue)
                        {
                            Mandatory.Add("Author and Reviewer Should not be same as a user");
                        }
                        if (txtRemarks.Value.Trim() == "")
                        {
                            Mandatory.Add("Enter Purpose of modification in reason field");
                        }
                        string strMsg = string.Join(",", Mandatory.ToArray());
                        if (Mandatory.Count > 0)
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), strMsg + ".", "error");
                        }
                        else
                        {
                            submitForJR_TS(empID, ddlReviewer.SelectedValue);
                        }
                    }
                    else if (hdnEmpBindStatus.Value == "1")
                    {
                        ArrayList Mandatory = new ArrayList();
                        if (ddlAuthor.SelectedValue == "0")
                        {
                            Mandatory.Add("select Author");
                        }
                        if (txtRemarks.Value.Trim() == "")
                        {
                            Mandatory.Add("Enter Purpose of modification in reason field");
                        }
                        string strMsg = string.Join(",", Mandatory.ToArray());
                        if (Mandatory.Count > 0)
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), strMsg + ".", "error");
                        }
                        else
                        {
                            submitForJR_TS(empID);
                        }
                    }
                }
                else if (ddlType.SelectedValue == "3")
                {
                    ArrayList Mandatory = new ArrayList();
                    if (txtRemarks.Value.Trim() == "")
                    {
                        Mandatory.Add("Enter Purpose of modification in reason field");
                    }
                    string strMsg = string.Join(",", Mandatory.ToArray());
                    if (Mandatory.Count > 0)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), strMsg + ".", "error");
                    }
                    else
                    {
                        submitJrEvaluator(empID);
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "MAE5:" + strline + "  " + strMsg, "error");
            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", "HideImg();", true);
        }
        public void submitForJR(int AdminEmpID)
        {
            try
            {
                int Result = 0;
                int JrID = Convert.ToInt32(hdnBaseID.Value);
                objTMS_BAL.submitModifiedHODorQAforJR(JrID, Convert.ToInt32(ddlAuthor.SelectedValue), Convert.ToInt32(ddlReviewer.SelectedValue),
                  AdminEmpID, txtRemarks.Value.Trim(), out Result);
                string msgType = "", msg = "";
                switch (Result)
                {
                    case 1:
                        msg = "Modified Author and Reviewer Successfully!";
                        msgType = "success";
                        break;
                    case 2:
                        msg = "Modified Author Successfully!";
                        msgType = "success";
                        break;
                    case 3:
                        msg = "Modified Reviewer Successfully!";
                        msgType = "success";
                        break;
                    case 4:
                        msg = "Unable to Modify, Since neither the Author nor Reviewer is changed.";
                        msgType = "info";
                        break;
                    case 5:
                        msg = "Unable to perform the action, Since it is not in a status of Modification.";
                        msgType = "info";
                        break;
                    default:
                        msg = "Unable to perform the action, Contact Admin.";
                        msgType = "info";
                        break;
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "func", "CustomAlertmsginMAE('"+ msg +"','"+msgType+"');", true);

            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "MAE6:" + strline + "  " + strMsg, "error");
            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", "HideImg();", true);
        }
        public void submitForJR_TS(int AdminEmpID, string ReviewerID = "0")
        {
            try
            {
                int Result = 0;
                string TS_ID = hdnBaseID.Value;
                objTMS_BAL.submitModifiedHODorQAforJRTS(TS_ID, ddlAuthor.SelectedValue, ReviewerID, AdminEmpID, txtRemarks.Value.Trim(), out Result);
                string msgType = "", msg = "";
                switch (Result)
                {
                    case 1:
                        msg = "Modified Author Successfully!";
                        msgType = "success";
                        break;
                    case 2:
                        msg = "Unable to Modify, since the Same user a Author.";
                        msgType = "info";
                        break;
                    case 3:
                        msg = "Modified both the Author and Reviewer Successfully!";
                        msgType = "success";
                        break;
                    case 4:
                        msg = "Modified the Author Successfully!";
                        msgType = "success";
                        break;
                    case 5:
                        msg = "Modified the Reviewer Successfully!";
                        msgType = "success";
                        break;
                    case 6:
                        msg = "Unable to Modify, Since neither the Author nor Reviewer is changed.";
                        msgType = "info";
                        break;
                    case 7:
                        msg = "Unable to perform the action, Since it is not in a status of Modification.";
                        msgType = "info";
                        break;
                    default:
                        msg = "Unable to perform the action, Contact Admin.";
                        msgType = "info";
                        break;
                }
                HelpClass.CallJS_Func(this.Page, this.GetType(), "CustomAlertmsginMAE('" + msg + "','" + msgType + "');");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "MAE7:" + strline + "  " + strMsg, "error");
            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", "HideImg();", true);
        }
        public void submitForTargetTs(int AdminEmpID)
        {
            try
            {
                int Result = 0;
                int TargetTS_ID = Convert.ToInt32(hdnBaseID.Value);
                objTMS_BAL.submitModifiedHODorQAforTargetTS(TargetTS_ID, ddlAuthor.SelectedValue, ddlReviewer.SelectedValue,
                  AdminEmpID, txtRemarks.Value.Trim(), out Result);
                string msgType = "", msg = "";
                switch (Result)
                {
                    case 1:
                        msg = "Modified Author and Reviewer Successfully!";
                        msgType = "success";
                        break;
                    case 2:
                        msg = "Modified Author Successfully!";
                        msgType = "success";
                        break;
                    case 3:
                        msg = "Modified Reviewer Successfully!";
                        msgType = "success";
                        break;
                    case 4:
                        msg = "Unable to Modify, Since neither the Author nor Reviewer is changed.";
                        msgType = "info";
                        break;
                    case 5:
                        msg = "Unable to perform the action, Since it is not in a status of Modification.";
                        msgType = "info";
                        break;
                    default:
                        msg = "Unable to perform the action, Contact Admin.";
                        msgType = "info";
                        break;
                }
                HelpClass.CallJS_Func(this.Page, this.GetType(), "CustomAlertmsginMAE('" + msg + "','" + msgType + "');");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "MAE8:" + strline + "  " + strMsg, "error");
            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", "HideImg();", true);
        }
        public void submitJrEvaluator(int AdminEmpID)
        {
            try
            {
                int Result = 0;
                int JrID = Convert.ToInt32(hdnBaseID.Value);
                objTMS_BAL.submitModifyHodEvaluator(JrID, Convert.ToInt32(ddlAuthor.SelectedValue),
                  AdminEmpID, txtRemarks.Value.Trim(), out Result);
                string msgType = "", msg = "";
                switch (Result)
                {
                    case 1:
                        msg = "Admin Modified Training Coordinator Successfully!";
                        msgType = "success";
                        break;
                    case 2:
                        msg = "Unable to modify because Training Coordinator are already updated!";
                        msgType = "info";
                        break;
                    case 3:
                        msg = "Unable to perform the action, Since it is not in a status of Modification.";
                        msgType = "info";
                        break;
                    default:
                        msg = "Unable to perform the action, Contact Admin.";
                        msgType = "info";
                        break;
                }
                HelpClass.CallJS_Func(this.Page, this.GetType(), "CustomAlertmsginMAE('" + msg + "','" + msgType + "');");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "MAE9:" + strline + "  " + strMsg, "error");
            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", "HideImg();", true);
        }
        #endregion
    }
}