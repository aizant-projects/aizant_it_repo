﻿using AizantIT_PharmaApp.Common;
using System;
using System.Collections;
using System.Data;
using System.Text;
using System.Web.UI;
using TMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.Administration.Feedback
{
    public partial class Feedback_Master : System.Web.UI.Page
    {
        TMS_BAL objTMS_BAL;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {                       
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drTmsRoles = dtx.Select("ModuleID=" + (int)Modules.TMS);
                        if (drTmsRoles.Length > 0)
                        {
                            dtTemp = drTmsRoles.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0)
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FM1:" + strline + "  " + strMsg, "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            try
            {
                lblAutorizedMsg.Text = "Your Are Not Authorized To View This Page. Contact Admin.";
                divAutorizedMsg.Visible = true;
                divMainContainer.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FM2:" + strline + "  " + strMsg, "error");
            }
        }
        private void InitializeThePage()
        {
            try
            {
                // ddlStatus.Enabled = false;
                // ddlStatus.CssClass = "show-tick form-control";
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FM3:" + strline + "  " + strMsg, "error");
            }
        }
        public void ToAddStylesForFB_Status()
        {
            try
            {
                lblFbActive.Style.Add("pointer-events", "auto");
                lblFbInActive.Style.Add("pointer-events", "auto");
                divFeedbackrbtns.Style.Add("cursor", "none");
                upbtnSubmitFb.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FM4:" + strline + "  " + strMsg, "error");
            }
        }
        public void ToAddCursorNotAllowedstyles()
        {
            try
            {
                lblFbActive.Style.Add("pointer-events", "none");
                lblFbInActive.Style.Add("pointer-events", "none");
                divFeedbackrbtns.Style.Add("cursor", "not-allowed");
                lblFbActive.Attributes.Add("class", "btn btn-primary_radio radio_check");
                lblFbInActive.Attributes.Add("class", "btn btn-primary_radio active radio_check");
                rbtnFeedbackInActive.Checked = true;
                upbtnSubmitFb.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FM5:" + strline + "  " + strMsg, "error");
            }
        } 
        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(ddlType.SelectedValue) > 0)
                {
                    objTMS_BAL = new TMS_BAL();
                    DataTable dtFeedback = objTMS_BAL.GetFeedbackMasterDetails(ddlType.SelectedValue);
                    if (dtFeedback.Rows.Count > 0)
                    {
                        btnSubmit.Text = "Update";
                        btnSubmit.Attributes.Add("class", "btn-signup_popup");
                        spanFeedbackComments.Visible = true;
                        txtFeedbackTitle.Value = dtFeedback.Rows[0]["Title"].ToString();
                        string FeedbackStatus = dtFeedback.Rows[0]["Status"].ToString();
                        if (FeedbackStatus=="1")
                        {
                            rbtnFeedbackActive.Checked = true;
                            upbtnSubmitFb.Update();
                            lblFbActive.Attributes.Add("class", "btn btn-primary_radio radio_check active");
                            lblFbInActive.Attributes.Add("class", "btn btn-primary_radio radio_check");
                        }
                        else
                        {
                            rbtnFeedbackInActive.Checked = true;
                            upbtnSubmitFb.Update();
                            lblFbActive.Attributes.Add("class", "btn btn-primary_radio radio_check");
                            lblFbInActive.Attributes.Add("class", "btn btn-primary_radio radio_check active");
                        }
                        hdnFeedbackID.Value = dtFeedback.Rows[0]["FeedbackID"].ToString();
                        hdnFeedbackAction.Value = "Update";
                        if (Convert.ToInt32(dtFeedback.Rows[0]["ActiveQuestionsCount"]) > 0)
                        {
                            ToAddStylesForFB_Status();
                        }
                        else
                        {
                            lblFbActive.Style.Add("pointer-events", "none");
                            lblFbInActive.Style.Add("pointer-events", "none");
                            divFeedbackrbtns.Style.Add("cursor", "not-allowed");
                            upbtnSubmitFb.Update();
                        }
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "function", "ShowHideDiv('" + ddlType.SelectedValue + "','Yes');", true);
                    }
                    else
                    {
                        lblFbActive.Attributes.Add("class", "btn btn-primary_radio radio_check");
                        lblFbInActive.Attributes.Add("class", "btn btn-primary_radio radio_check active");
                        divFeedbackrbtns.Style.Add("cursor", "not-allowed");
                        rbtnFeedbackInActive.Checked = true;
                        hdnFeedbackAction.Value = "Update";
                        upbtnSubmitFb.Update();
                        spanFeedbackComments.Visible = false;
                        txtFeedbackTitle.Value = string.Empty;
                        hdnFeedbackID.Value = "0";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "function", "ShowHideDiv('" + ddlType.SelectedValue + "','No');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "function", "ShowHideDiv('" + ddlType.SelectedValue + "','No');", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FM6:" + strline + "  " + strMsg, "error");
            }
        }
        public void CreateNewFeedBackForm()
        {
            try
            {
                int empID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                int FeedbackID = Convert.ToInt32(hdnFeedbackID.Value);
                int Feedbackstatus = 0;
                if (rbtnFeedbackActive.Checked)
                {
                    Feedbackstatus = 1;
                }
                else if (rbtnFeedbackInActive.Checked)
                {
                    Feedbackstatus = 2;
                }
                objTMS_BAL = new TMS_BAL();
                objTMS_BAL.CreateFeedbackForm(txtFeedbackTitle.Value.Trim(), Convert.ToInt32(ddlType.SelectedValue), empID, txtFeedbackComments.Value.Trim(),
                    Feedbackstatus, FeedbackID, out int FeedbackResult);

                ddlType.SelectedValue = "0";
                txtFeedbackComments.Value = "";
                if (FeedbackResult == 1)
                {
                    txtFeedbackComments.Value = "";
                    ddlType.SelectedValue = "0";
                    HelpClass.custAlertMsg(this, this.GetType(), "Feedback Updated Successfully!", "success");
                }
                else if (FeedbackResult == 3)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Can Not Update, Since There Is No At-least One Active Question On This Feedback.", "error", "VisibleFM_Questionsdiv();");
                }
                else
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Unable To Create, Contact Admin.", "error");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FM7:" + strline + "  " + strMsg, "error");
            }
        }
        public void UpdateFeedbackForm()
        {
            try
            {
                int Feedbackstatus = 0;
                if (rbtnFeedbackActive.Checked)
                {
                    Feedbackstatus = 1;
                }
                else if (rbtnFeedbackInActive.Checked)
                {
                    Feedbackstatus = 2;
                }
                objTMS_BAL = new TMS_BAL();
                objTMS_BAL.UpdateFeedbackForm(txtFeedbackTitle.Value.Trim(), Convert.ToInt32(ddlType.SelectedValue), 
                    Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]),
                txtFeedbackComments.Value.Trim(), Feedbackstatus, Convert.ToInt32(hdnFeedbackID.Value), out int FeedbackResult);
                if (FeedbackResult == 1)
                {
                    txtFeedbackComments.Value = "";
                    ddlType.SelectedValue = "0";
                    HelpClass.custAlertMsg(this, this.GetType(), "Feedback Updated Successfully!", "success");
                }
                else if (FeedbackResult == 3)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Can Not Update, Since There Is No Atleast One Active Question On This Feedback.", "error", "VisibleFM_Questionsdiv();");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FM8:" + strline + "  " + strMsg, "error");
            }
        }
        public void CreateNewFeedBackQuestion()
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                int FeedbackID = int.Parse(hdnFeedbackID.Value);
                int Result;
                int FeedbackResultID;
                var QuestionComments = txtFeedback_QuestionComments.Value.Trim();
                int c = objTMS_BAL.CreateFeedbackQuestions(FeedbackID, txtFeedback_Question.Value.Trim(),QuestionComments==""?"N/A": QuestionComments, 
                    Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]), txtFeedbackTitle.Value.Trim(),
                    Convert.ToInt32(ddlType.SelectedValue), out FeedbackResultID, out Result);
                if (FeedbackResultID > 0)
                {
                    hdnFeedbackID.Value = FeedbackResultID.ToString();
                }
                if (Result == 1)
                {
                    lblFbActive.Attributes.Add("class", "btn btn-primary_radio active radio_check");
                    lblFbInActive.Attributes.Add("class", "btn btn-primary_radio radio_check");
                    divFeedbackrbtns.Style.Add("cursor", "none");
                    rbtnFeedbackActive.Checked = true;
                    upbtnSubmitFb.Update();
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Feedback along with Question Created Successfully !", "success", "ClearDatainQuestionsandReloadQuestionstable();");
                }
                else if (Result == 2)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Failed To Create Feedback On Initial Creation.", "info");
                }
                else if (Result == 3)
                {
                    ToAddStylesForFB_Status();
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Question Created Successfully !", "success", "ClearDatainQuestionsandReloadQuestionstable();");
                }
                else if (Result == 4)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Cannot Create Question, Since Question Is Already Exists", "info");
                }
                else
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Cannot Create New Feedback Question, Contact Admin.", "error");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FM9:" + strline + "  " + strMsg, "error");
            }
        }

        public void UpdateFeedBackQuestion()
        {
            try
            {
                int Questionstatus = 0;
                if (rbtnActive.Checked)
                {
                    Questionstatus = 1;
                }
                else if (rbtnInActive.Checked)
                {
                    Questionstatus = 2;
                }
                objTMS_BAL = new TMS_BAL();
                int Result = 0;
                int c = objTMS_BAL.UpdateFeedbackFormQuestions(Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]), 
                    txtFeedback_Question.Value.Trim(), txtFeedback_QuestionComments.Value.Trim(), Questionstatus, hdnFeedbackQuestionID.Value, 
                    Convert.ToInt32(hdnFeedbackID.Value), Convert.ToInt32(ddlType.SelectedValue), out Result);
                if (Result == 1)
                {
                    ToAddStylesForFB_Status();
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Feedback Updated Successfully !", "success", "CloseFMQuestionsModalandloadQuestionsTable()");
                }
                else if (Result == 2)
                {
                    ToAddCursorNotAllowedstyles();
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Feedback Updated!, Since There Is No active questions Feedback is also made Inactive.", "success", "CloseFMQuestionsModalandloadQuestionsTable()");
                }
                else if (Result == 3)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Cannot Create Question, Since Question is already exists", "info", "BtnTextChanges()");
                }
                else if (Result == 4)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Feedback Status Modified, Not Able To Change The Question, Due To Trainee Already Submit The Feedback On This Question.", "success", "CloseFMQuestionsModalandloadQuestionsTable()");
                }
                else if (Result == 5)
                {
                    ToAddCursorNotAllowedstyles();
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Feedback Status Modified, Since there is no active questions Feedback is also made Inactive, Not able to Change the Question, Due to Trainee already Submit the Feedback on this Question", "success", "CloseFMQuestionsModalandloadQuestionsTable()");
                }
                else if (Result == 6)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Trainee Already Submitted Feedback On This Question, So you\\'re Not Allowed To Modify This Question.", "error", "CloseFMQuestionsModalandloadQuestionsTable()");
                }
                else if (Result == 7)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "There Are No Changes To Update.", "warning", "BtnTextChanges()");
                }
                else
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Cannot Update Question,Please contact admin.", "error");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FM10:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                ArrayList Mandatory = new ArrayList();
                if (ddlType.SelectedValue == "0")
                {
                    Mandatory.Add("select Tye");
                }
                //if (ddlStatus.SelectedValue == "0")
                //{
                //    Mandatory.Add("select Status");
                //}
                if (txtFeedbackTitle.Value.Trim() == "")
                {
                    Mandatory.Add("Enter Feedback Title.");
                }
                if (hdnFeedbackAction.Value == "Update")
                {
                    if (txtFeedbackComments.Value.Trim() == "")
                    {
                        Mandatory.Add("Enter Modification reason in Feedback Comments.");
                    }
                }
                StringBuilder s = new StringBuilder();
                foreach (string x in Mandatory)
                {
                    s.Append(x);
                    s.Append(",");
                }
                if (Mandatory.Count > 0)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), s.ToString(), "error");
                }
                else
                {
                    UpdateFeedbackForm();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FM11:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnSubmitFeedbackQuestion_Click(object sender, EventArgs e)
        {
            try
            {
                ArrayList Mandatory = new ArrayList();
                if (txtFeedback_Question.Value.Trim() == "")
                {
                    Mandatory.Add("Enter Feedback Question");
                }
                if (hdnFeedbackQuestionAction.Value == "Update")
                {
                    if (txtFeedback_QuestionComments.Value.Trim() == "")
                    {
                        Mandatory.Add("Enter Modification reason in Feedback Question Comments");
                    }
                }
                string strMsg = string.Join(",<br/>",Mandatory.ToArray());                
                if (Mandatory.Count > 0)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), strMsg+".", "error");
                }
                else
                {
                    if (hdnFeedbackQuestionAction.Value == "Update")
                    {
                        UpdateFeedBackQuestion();
                    }
                    else if (hdnFeedbackQuestionAction.Value == "Create")
                    {
                        CreateNewFeedBackQuestion();
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FM12:" + strline + "  " + strMsg, "error");
            }
        }
    }
}