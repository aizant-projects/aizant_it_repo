﻿using AizantIT_PharmaApp.Common;
using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;
using TMS_BusinessLayer;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.Administration.Feedback
{
    public partial class Feedbackmaster : System.Web.UI.Page
    {
        TMS_BAL objTMS_BAL;
        static int EmpID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drTmsRoles = dtx.Select("ModuleID=" + (int)Modules.TMS);
                        if (drTmsRoles.Length > 0)
                        {
                            dtTemp = drTmsRoles.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0)
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FM1:" + strline + "  " + strMsg, "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            try
            {
                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                divAutorizedMsg.Visible = true;
                divMainContainer.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FM2:" + strline + "  " + strMsg, "error");
            }
        }
        private void InitializeThePage()
        {
            try
            {
                // ddlStatus.Enabled = false;
                //ddlStatus.CssClass = "show-tick form-control";
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FM3:" + strline + "  " + strMsg, "error");
            }
        }
        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(ddlType.SelectedValue) > 0)
                {
                    objTMS_BAL = new TMS_BAL();
                    DataTable dtFeedback = objTMS_BAL.GetFeedbackMasterDetails(ddlType.SelectedValue);
                    if (dtFeedback.Rows.Count > 0)
                    {
                        btnSubmit.Text = "Update";
                        spanFeedbackComments.Visible = true;
                        txtFeedbackTitle.Value = dtFeedback.Rows[0]["Title"].ToString();
                        ddlStatus.SelectedValue = dtFeedback.Rows[0]["Status"].ToString();
                        hdnFeedbackID.Value = dtFeedback.Rows[0]["FeedbackID"].ToString();
                        hdnFeedbackAction.Value = "Update";
                        if (Convert.ToInt32(dtFeedback.Rows[0]["ActiveQuestionsCount"]) > 0)
                        {
                            ddlStatus.Enabled = true;
                        }
                        else
                        {
                            ddlStatus.Enabled = false;
                        }
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "function", "ShowHideDiv('" + ddlType.SelectedValue + "','Yes');", true);
                    }
                    else
                    {
                        ddlStatus.SelectedValue = "2";
                        ddlStatus.Enabled = false;
                        hdnFeedbackAction.Value = "Create";
                        btnSubmit.Text = "Create";
                        spanFeedbackComments.Visible = false;
                        txtFeedbackTitle.Value = string.Empty;
                        hdnFeedbackID.Value = "0";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "function", "ShowHideDiv('" + ddlType.SelectedValue + "','No');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "function", "ShowHideDiv('" + ddlType.SelectedValue + "','No');", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FM4:" + strline + "  " + strMsg, "error");
            }
        }
        public void CreateNewFeedBackForm()
        {
            try
            {
                //objTMS_BAL = new TMS_BAL();
                ////objTMS_BAL.CreateFeedbackForm(txtFeedbackTitle.Value.Trim(), Convert.ToInt32(ddlType.SelectedValue), EmpID,txtFeedbackComments.Value.Trim()
                ////    status, out int FeedbackID,out int FeedbackResult);
                //if (FeedbackID > 0)
                //{
                //    hdnFeedbackID.Value = FeedbackID.ToString();
                //    // btnCreate.Visible = true;
                //    //btnHistory.Visible = true;
                //    btnSubmit.Text = "Update";
                //    spanFeedbackComments.Visible = true;
                //    hdnFeedbackAction.Value = "Update";
                //   // ddlStatus.Enabled = true;


                //}
                //else
                //{
                //   // btnCreate.Visible = false;
                //   // btnHistory.Visible = false;
                //}
                //if (FeedbackResult == 2)
                //{
                //    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Feedback Created Successfully", "success", "VisibleFM_Questionsdiv();");
                //}
                //else if (FeedbackResult == 1)
                //{
                //    HelpClass.custAlertMsg(this, this.GetType(), "There is alredy a Feedback for this Type.", "info");
                //}
                //else
                //{
                //    HelpClass.custAlertMsg(this, this.GetType(), "Unable to Create, contact admin.", "error");
                //}
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FM5:" + strline + "  " + strMsg, "error");
            }
        }
        public void UpdateFeedbackForm()
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                objTMS_BAL.UpdateFeedbackForm(txtFeedbackTitle.Value.Trim(), Convert.ToInt32(ddlType.SelectedValue), EmpID,
                    txtFeedbackComments.Value.Trim(), Convert.ToInt32(ddlStatus.SelectedValue), Convert.ToInt32(hdnFeedbackID.Value), out int FeedbackResult);
                if (FeedbackResult == 1)
                {
                    txtFeedbackComments.Value = "";
                    HelpClass.custAlertMsg(this, this.GetType(), "Feedback Updated Successfully!", "success");
                }
                else if (FeedbackResult == 3)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Can not update, Since there is no atleast one active questions on this feedback. ", "error");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FM6:" + strline + "  " + strMsg, "error");
            }
        }
        public void CreateNewFeedBackQuestion()
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                int FeedID = int.Parse(hdnFeedbackID.Value);
                int Result = 0;
                //int c = objTMS_BAL.CreateFeedbackQuestions(FeedID, txtFeedback_Question.Value.Trim(), txtFeedback_QuestionComments.Value.Trim(), EmpID, out Result);
                if (Result == 1)
                {
                    ddlStatus.Enabled = true;
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Created Successfully !", "success", "ClearDatainQuestionsandReloadQuestionstable()");
                }
                if (Result == 2)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Cannot Create Question, Since Question is already exist.", "info");
                }
                else
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Cannot create new feedback question, contact admin.", "error");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FM7:" + strline + "  " + strMsg, "error");
            }
        }
        public void UpdateFeedBackQuestion()
        {
            try
            {
                int Questionstatus = 0;
                if (rbtnActive.Checked)
                {
                    Questionstatus = 1;
                }
                else if (rbtnInActive.Checked)
                {
                    Questionstatus = 2;
                }
                objTMS_BAL = new TMS_BAL();
                int c = objTMS_BAL.UpdateFeedbackFormQuestions(EmpID, txtFeedback_Question.Value.Trim(), txtFeedback_QuestionComments.Value.Trim(), Questionstatus, hdnFeedbackQuestionID.Value, Convert.ToInt32(hdnFeedbackID.Value),out int Result);
                if (Result == 1)
                {
                    //ddlStatus.Enabled = true;
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Updated Successfully !", "success", "CloseFMQuestionsModalandloadQuestionsTable()");
                }
                if (Result == 2)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Cannot Update Question is already exist.", "info");
                }
                if (Result == 3)
                {
                    ddlStatus.SelectedValue = "2";
                    ddlStatus.Enabled = false;
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Question is updated Successfully!, Since there is no active questions Feedback is also made Inactive.", "success", "CloseFMQuestionsModalandloadQuestionsTable()");
                }
                else
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Cannot Update Question,Please contact admin.", "error");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FM8:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                ArrayList Mandatory = new ArrayList();
                if (ddlType.SelectedValue == "0")
                {
                    Mandatory.Add("select Tye");
                }
                if (ddlStatus.SelectedValue == "0")
                {
                    Mandatory.Add("select Status");
                }
                if (txtFeedbackTitle.Value.Trim() == "")
                {
                    Mandatory.Add("Enter Feedback Title.");
                }
                if (hdnFeedbackAction.Value == "Update")
                {
                    if (txtFeedbackComments.Value.Trim() == "")
                    {
                        Mandatory.Add("Enter Modification reason in Feedback Comments.");
                    }
                }
                StringBuilder s = new StringBuilder();
                foreach (string x in Mandatory)
                {
                    s.Append(x);
                    s.Append(",");
                }
                if (Mandatory.Count > 0)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), s.ToString(), "error");
                }
                else
                {
                    if (hdnFeedbackAction.Value == "Update")
                    {
                        UpdateFeedbackForm();
                    }
                    else if (hdnFeedbackAction.Value == "Create")
                    {
                        CreateNewFeedBackForm();
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FM9:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnSubmitFeedbackQuestion_Click(object sender, EventArgs e)
        {
            try
            {
                ArrayList Mandatory = new ArrayList();

                if (txtFeedback_Question.Value.Trim() == "")
                {
                    Mandatory.Add("Enter Feedback Question.");
                }
                if (hdnFeedbackQuestionAction.Value == "Update")
                {
                    if (txtFeedback_QuestionComments.Value.Trim() == "")
                    {
                        Mandatory.Add("Enter Modification reason in Feedback Question Comments.");
                    }
                }
                StringBuilder s = new StringBuilder();
                foreach (string x in Mandatory)
                {
                    s.Append(x);
                    s.Append(",");
                }
                if (Mandatory.Count > 0)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), s.ToString(), "error");
                }
                else
                {
                    if (hdnFeedbackQuestionAction.Value == "Update")
                    {
                        UpdateFeedBackQuestion();
                    }
                    else if (hdnFeedbackQuestionAction.Value == "Create")
                    {
                        CreateNewFeedBackQuestion();
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FM10:" + strline + "  " + strMsg, "error");
            }
        }
    }
}