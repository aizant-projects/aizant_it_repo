﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" AutoEventWireup="true" CodeBehind="Feedback_Master.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Administration.Feedback.Feedback_Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <asp:UpdatePanel runat="server" ID="upHdnFields" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnFeedbackAction" runat="server" Value="Update" />
            <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnFeedbackID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnFeedbackQuestionID" runat="server" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
     
    <asp:HiddenField ID="hdnFeedbackQuestionAction" runat="server" Value="0" />

    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12  grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" CssClass="capitalize" runat="server" Text="Your Are Not Authorized to view this Page. Contact Admin.">            
            </asp:Label>
        </div>
        <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12">Feedback Master</div>
        <div class="float-left col-lg-6 col-sm-4 col-4 col-md-4 bottom top ">
            <label class="padding-none  control-label custom_label_answer float-left float-left col-lg-3">Feedback Type<span class="mandatoryStar">*</span></label>
            <asp:UpdatePanel runat="server" ID="upHeaderDiv" UpdateMode="Always">
                <ContentTemplate>
                    <asp:DropDownList ID="ddlType" CssClass=" float-left regulatory_dropdown_style col-lg-7 form-control ddlTypeClass selectpicker" OnSelectedIndexChanged="ddlType_SelectedIndexChanged"
                        AutoPostBack="true" data-size="5" runat="server">
                        <asp:ListItem Text="-- Select Feedback Type --" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Job Responsibility (JR)" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Target Training" Value="2"></asp:ListItem>
                    </asp:DropDownList>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div id="divFeedbackMasterHeader" class="float-left col-lg-12">
                <div id="divFeedbackHeader" class="float-left col-lg-12 padding-none grid_panel_full bottom" style="display: none">
                    <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 bottom">
                        Feedback Template
                         <button type="button" class=" btn btn-dashboard float-right" id="btnFeedBackHistory" onclick="openFM_Historypopup();" style="margin-bottom: 0px;">History</button>
                        <button type="button" class=" btn btn-dashboard float-right" runat="server" id="btnCreateNewQuestion" onclick="CreateNewQuestion();" style="margin-bottom: 0px;">Create Question</button>
                    </div>
                    <asp:UpdatePanel runat="server" ID="upbtnSubmitFb" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="float-left col-lg-4 col-sm-12 col-12 col-md-12">
                                <label class="padding-none float-left col-lg-12 col-sm-12 col-12 col-md-12 control-label custom_label_answer">Feedback Title<span class="mandatoryStar">*</span></label>
                                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                    <input type="text" class="form-control login_input_sign_up" runat="server" name="name" id="txtFeedbackTitle" placeholder="Enter Feedback Title" maxlength="50" onkeypress="return ValidateAlpha(event)" onpaste="return false" />
                                </div>
                            </div>
                            <div class="float-left col-lg-4 col-sm-12 col-12 col-md-12">
                                <div class="padding-none col-sm-12 col-12 float-left col-lg-12 col-sm-12 col-12 col-md-12 col-md-12 float-left" runat="server" data-toggle="buttons" id="divFeedbackrbtns">
                                    <label class=" col-sm-12 float-left padding-none control-label">Feedback Status</label>
 
                                    <span class="float-left" id="feedbackstatusspan">
                                    <label class="float-left  btn-primary_radio radio_check active" for="<%=rbtnFeedbackActive.ClientID%>" id="lblFbActive" runat="server" title="Make Active" style="pointer-events: auto;margin:0px;padding:4px 20px !important">
                                        <asp:RadioButton ID="rbtnFeedbackActive" name="rbtnFeedbackActive" runat="server" GroupName="FeedbackStatus" Text="Active" />
                                    </label>
                                    <label class="float-left btn-primary_radio radio_check" for="<%=rbtnFeedbackInActive.ClientID%>" id="lblFbInActive" runat="server" title="Make InActive" style="pointer-events: auto;margin-bottom:0px !important;padding: 4px 12px !important;">
                                        <asp:RadioButton ID="rbtnFeedbackInActive" name="rbtnFeedbackInActive" runat="server" GroupName="FeedbackStatus" Text="In-Active" />
                                    </label>
                                        </span>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div id="divFeedbackQuestion" style="display: none">
                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12  top ">
                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12  grid_panel_full padding-none">
                                <div class="grid_header float-left col-lg-12 col-sm-12 col-12 col-md-12">Feedback Questions List</div>
                                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 top">
                                    <div class="float-right padding-none">
                                    </div>
                                </div>
                                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 top bottom padding-none">
                                    <div>
                                        <table id="tblFeedbackQuestions" class="datatable_cust tblFeedbackQuestionsClass display breakword" style="width: 100%">
                                            <thead>
                                                <tr>
                                                    <th>FeedbackQuestionID</th>
                                                    <th>Question</th>
                                                    <th>Status</th>
                                                    <th>EmpFeedbackId</th>
                                                    <th>Edit</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="Panel_Footer_Message_Label">
                            <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none" id="divFeedbackComments" style="display:none">
                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 ">
                            <label class="padding-none float-left col-lg-12 col-sm-12 col-12 col-md-12 control-label custom_label_answer">Comments<span class="smallred_label" id="spanFeedbackComments" runat="server" visible="false">*</span></label>
                            <textarea class="form-control" textmode="MultiLine" runat="server" id="txtFeedbackComments" placeholder="Enter Feedback Comments" title="Comments" maxlength="250" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 bottom">
                        <div class="float-right top">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="btn-signup_popup" Text="Update" OnClientClick="return FeedbackMasterSubmitValidate();" OnClick="btnSubmit_Click" />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn-cancel_popup" OnClientClick="CloseSelectedType();" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-------- Modal For Create and Edit Feedback-------------->
    <div id="modalFMCreateOrEdit" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                   
                    <h4 class="modal-title">
                        <label id="lblFeedbackTitle" class="modal-title"></label>
                    </h4>
                     <button type="button" class="close" data-dismiss="modal" title="Close">&times;</button>
                </div>
                <div class="modal-body">
                    <div class=" float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                        <%-- <asp:UpdatePanel ID="upFaqModal" runat="server" UpdateMode="Always">
                            <ContentTemplate>--%>
                        <div id="divQuestionStatus" style="display: none">
                            <div class=" padding-none col-sm-12 col-12 float-left col-lg-12 col-sm-12 col-12 col-md-12 col-md-12 float-left" data-toggle="buttons" id="divRadioBtns">
                                <label for="inputPassword3" class=" col-sm-12 padding-none control-label">Question Status</label>
                                <label class=" btn btn-primary_radio col-6 float-left col-lg-6 col-md-6 col-sm-6 active radio_check" id="lblRadioActiveFQ" runat="server" title="Make Active">
                                    <asp:RadioButton ID="rbtnActive" runat="server" GroupName="QuestionStatus" Text="Active" />
                                </label>
                                <label class="btn  btn-primary_radio col-6 float-left col-lg-6 col-md-6 col-sm-6 radio_check" id="lblRadioInActiveFQ" runat="server" title="Make InActive">
                                    <asp:RadioButton ID="rbtnInActive" runat="server" GroupName="QuestionStatus" Text="In-Active" />
                                </label>
                            </div>
                        </div>
                        <div class="form-group float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none" id="divtxtFeedbackQuestions">
                            <label for="inputEmail3" class=" padding-none float-left col-lg-12 col-sm-12 col-12 col-md-12 control-label custom_label_answer">Question<span class="smallred_label">*</span></label>
                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                <textarea class="form-control " runat="server" id="txtFeedback_Question" placeholder="Enter Question" maxlength="250" title="Question"></textarea>
                            </div>
                        </div>
                        <div class="form-group float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none" id="divFaqComments">
                            <label for="inputEmail3" class=" padding-none float-left col-lg-12 col-sm-12 col-12 col-md-12 control-label custom_label_answer">Remarks<span class="smallred_label" id="spanComments" style="display: none">*</span></label>
                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                <textarea class="form-control" runat="server" id="txtFeedback_QuestionComments" maxlength="250" placeholder="Enter Modification Reason" title="Remarks"></textarea>
                            </div>
                        </div>
                        <%--    </ContentTemplate>
                        </asp:UpdatePanel>--%>
                    </div>
                </div>
                <div class="modal-footer" style="margin-right: 6px;">
                    <asp:UpdatePanel runat="server" ID="upbtn" UpdateMode="Always" style="display: inline-block;">
                        <ContentTemplate>
                            <asp:Button ID="btnSubmitFeedbackQuestion" CssClass="btn btn-signup_popup" OnClick="btnSubmitFeedbackQuestion_Click"
                              OnClientClick="return FeedbackMasterQuestionsSubmitValidate();"
                                runat="server" Text="Create" ToolTip="Click to Create" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <button type="button" class="btn btn-cancel_popup" data-dismiss="modal" title="Click to Cancel">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!--End of Create and Edit Feedback model-->

    <!-------- Modal For Viewing the History of Feedback------------->
    <div id="modelFeedback_History" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg" style="min-width:85%;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">                    
                    <h4 class="modal-title">
                        <asp:Label ID="lblmodelFeedback_HistoryTitle" runat="server" Text="Feedback History"></asp:Label>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" title="Close">&times;</button>
                </div>
                <div class="modal-body">
                    <div class=" float-left float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                        <table id="tblFeedback_History" class="datatable_cust tblFeedback_HistoryClass display breakword" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>ActionID</th>
                                    <th>Status</th>
                                    <th>Action By</th>
                                    <th>Action Date</th>
                                    <th>Remarks</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!-------- End Feedback History-------------->

    <script>
        function ShowHideDiv(ddlSelected, ISQuetionsVisible) {
            if (ddlSelected == "1" || ddlSelected == "2") {
                $("#divFeedbackHeader").show();
            }
            else {
                $("#divFeedbackHeader").hide();
                $("#divFeedbackQuestion").hide();
            }
            if (ISQuetionsVisible == "Yes") {
                $("#divFeedbackQuestion").show();
                <%--$("#<%=btnSubmit.ClientID%>").attr("disabled", false);
                $("#<%=btnSubmit.ClientID%>").removeClass("disabled");--%>
                $("#<%=btnSubmit.ClientID%>").show();
                $("#btnFeedBackHistory").show();
                document.getElementById("<%=btnSubmit.ClientID %>").value = "Update";
                loadFeedbackQuestions();
                $("#divFeedbackComments").show();
            }
            else {
                $("#divFeedbackQuestion").hide();
                $("#btnFeedBackHistory").hide();
                $("#divFeedbackComments").hide();
                //document.getElementById("<%=btnSubmit.ClientID %>").value = "Submit";
                <%--$("#<%=btnSubmit.ClientID%>").addClass("disabled");
                $("#<%=btnSubmit.ClientID%>").attr("disabled", true);--%>
                $("#<%=btnSubmit.ClientID%>").hide();
                $("#<%=lblFbActive.ClientID%>").css('pointer-events', 'none');
                $("#<%=lblFbInActive.ClientID%>").css('pointer-events', 'none');
                $("#<%=lblFbActive.ClientID%>").removeClass("active");
                $("#<%=lblFbInActive.ClientID%>").addClass("active");
            }
        }

        function VisibleFM_Questionsdiv() {
            document.getElementById("<%=txtFeedbackComments.ClientID %>").value = "";
            $("#divFeedbackHeader").show();
            $("#divFeedbackQuestion").show();
            loadFeedbackQuestions();
        }
        
        function FeedbackMasterSubmitValidate() {
            var FeedbackType = document.getElementById('<%=ddlType.ClientID%>').value;
            var Title = document.getElementById('<%=txtFeedbackTitle.ClientID%>').value.trim();
            var FeedbackComments = document.getElementById('<%=txtFeedbackComments.ClientID%>').value.trim();
            var FeedbackAction = document.getElementById('<%=hdnFeedbackAction.ClientID%>').value;
            errors = [];
            if (FeedbackType == 0) {
                errors.push("Select Feedback Type");
            }
            if (Title == "") {
                errors.push("Enter Feedback Title");
            }
            if (FeedbackAction == "Update") {
                if (FeedbackComments == "") {
                    errors.push("Enter Modification Reason In Comments.");
                }
            }
            if (errors.length > 0) {
                if (FeedbackAction == "Update") {
                    custAlertMsg(errors.join("<br/>"), "error", "FocusonFBComments()");
                }
                else {
                    custAlertMsg(errors.join("<br/>"), "error");
                }
                return false;
            }
            else {
                ShowPleaseWait('show');
                return true;
            }
        }
        function FocusonFBComments() {
            $("#<%=txtFeedbackComments.ClientID%>").focus();
        }
        function FeedbackMasterQuestionsSubmitValidate() {
            var QuestionTitle = document.getElementById('<%=txtFeedback_Question.ClientID%>').value;
            var QuestionComments = document.getElementById('<%=txtFeedback_QuestionComments.ClientID%>').value.trim();
            var QuestionAction = document.getElementById('<%=hdnFeedbackQuestionAction.ClientID%>').value;
            errors = [];
            if (QuestionTitle == "") {
                errors.push("Enter Feedback Question.");
            }
            if (QuestionAction == "Update") {
                if (QuestionComments == "") {
                    errors.push("Enter Modification Reason In Remarks.");
                }
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                ShowPleaseWait('show');
                return true;
            }
        }
        function CloseSelectedType() {
            document.getElementById('<%=ddlType.ClientID%>').value = 0;
        }
    </script>
    <!--Get Feedback Details by Dropdown-->
    <script>
        function CloseFMQuestionsModalandloadQuestionsTable() {
            loadFeedbackQuestions();
            $('#modalFMCreateOrEdit').modal('hide');
           
        }
        
        function ClearDatainQuestionsandReloadQuestionstable() {
            if ($("#<%=lblFbActive.ClientID%>").hasClass('active')) {
                $("#<%=rbtnFeedbackActive.ClientID%>").prop('checked', true);
            }
            else {
                $("#<%=rbtnFeedbackInActive.ClientID%>").prop('checked', true);
            }
            $("#divFeedbackQuestion").show();
            $("#<%=btnSubmit.ClientID%>").removeClass("disabled");
            $("#<%=btnSubmit.ClientID%>").attr("disabled", false);
            $("#btnFeedBackHistory").show();
            ClearDatainQuestionsFields();
            loadFeedbackQuestions();
            $('#divFeedbackComments').show();
            $("#<%=btnSubmit.ClientID%>").show();
        }
        function ClearDatainQuestionsFields() {
            document.getElementById("<%=txtFeedback_QuestionComments.ClientID %>").value = "";
            document.getElementById("<%=txtFeedback_Question.ClientID %>").value = "";
        }
        function EditFeedbackQuestion(FeedbackQuestionID, QuestionTitle, QuestionStatus, EmpFeedbackonQuestion) {
            ClearDatainQuestionsFields();
            if (EmpFeedbackonQuestion > 0) {
                $("#<%=txtFeedback_Question.ClientID%>").attr("disabled", true);
            }
            else {
                $("#<%=txtFeedback_Question.ClientID%>").attr("disabled", false);
            }
            $("#<%=hdnFeedbackQuestionAction.ClientID%>").val('Update');
            $("#<%=hdnFeedbackQuestionID.ClientID%>").val(FeedbackQuestionID);
            document.getElementById("<%=txtFeedback_Question.ClientID %>").value = QuestionTitle;
            if (QuestionStatus == "Active") {
                $("#<%=lblRadioInActiveFQ.ClientID%>").removeClass("active");
                $("#<%=lblRadioActiveFQ.ClientID%>").addClass("active");
                $("#<%=rbtnActive.ClientID%>").prop('checked', true);
            }
            else {
                $("#<%=lblRadioActiveFQ.ClientID%>").removeClass("active");
                $("#<%=lblRadioInActiveFQ.ClientID%>").addClass("active");
                $("#<%=rbtnInActive.ClientID%>").prop('checked', true);
            }
            $('#lblFeedbackTitle').text("Edit Question");
            $("#spanComments").show();
            document.getElementById("<%=btnSubmitFeedbackQuestion.ClientID %>").value = "Update";
            $("#<%=btnSubmitFeedbackQuestion.ClientID%>").attr('title', 'Click to Update');
            $("#divQuestionStatus").show();
            $('#modalFMCreateOrEdit').modal({ backdrop: 'static', keyboard: false });
            $("#<%=txtFeedback_QuestionComments.ClientID%>").attr('autofocus', 'true');
        }
        function CreateNewQuestion() {
            var Title = document.getElementById('<%=txtFeedbackTitle.ClientID%>').value.trim();
            errors = [];
            if (Title == "") {
                errors.push("Enter Feedback Title Before Going To Create Feedback Question");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            } else {
                $('#modalFMCreateOrEdit').modal({ backdrop: 'static', keyboard: false });
            }
            ClearDatainQuestionsFields();
            $("#<%=hdnFeedbackQuestionAction.ClientID%>").val('Create');
            $('#lblFeedbackTitle').text("Create New Question");
            //$("#divtxtFeedbackQuestions").show();
            $("#<%=txtFeedback_Question.ClientID%>").removeAttr("disabled");
            $("#spanComments").hide();
            document.getElementById("<%=btnSubmitFeedbackQuestion.ClientID %>").value = "Create";
            $("#divQuestionStatus").hide();
            $("#<%=txtFeedback_Question.ClientID%>").attr('autofocus', 'true');
        }
        function BtnTextChanges() {
            document.getElementById("<%=btnSubmitFeedbackQuestion.ClientID %>").value = "Update";
        }

        function openFM_Historypopup() {
            document.getElementById("<%=txtFeedback_QuestionComments.ClientID %>").value = "";
            $('#modelFeedback_History').modal({ backdrop: 'static', keyboard: false });
        }
        $('#modelFeedback_History').on('shown.bs.modal', function (e) {
            ReloadtblFeedback_History();
        });
    </script>

    <!--For Feedback Questions List Regular jQuery Datatable-->
    <script>
        var oTblFeedbackQuestions;
        function loadFeedbackQuestions() {
            if (oTblFeedbackQuestions != null) {
                oTblFeedbackQuestions.destroy();
            }
            oTblFeedbackQuestions = $('#tblFeedbackQuestions').DataTable({
                columns: [
                    { 'data': 'FeedbackQuestionID' },
                    { 'data': 'QuestionTitle' },
                    { 'data': 'StatusName' },
                    { 'data': 'EmpFeedbackonQuestion' },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            return '<a class="Edit" href="#" onclick="EditFeedbackQuestion(\'' + o.FeedbackQuestionID + '\',`' + o.QuestionTitle + '`,\'' + o.StatusName + '\',\'' + o.EmpFeedbackonQuestion + '\');"></a>';
                        }
                    }
                ],
                "aoColumnDefs": [{ "targets": [0,3], "visible": false, "searchable": false }, { className: "textAlignLeft", "targets": [1, 2] }],
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/FeedbackMaster/FeedbackMaster.asmx/getFeedbackQuestionList")%>",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "FeedbackID", "value": $('#<%=hdnFeedbackID.ClientID%>').val() });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            UserSessionCheck();
                            $("#tblFeedbackQuestions").show();
                            $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                            $(".tblFeedbackQuestionsClass").css({ "width": "100%" });
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }

            });
        }
    </script>


    <!--For Feedback History List Regular jQuery Datatable-->
    <script>
        var otblFeedback_History;

        function loadFeedback_History() {
            if ($('#<%=hdnFeedbackID.ClientID%>').val() != "0") {
                if (otblFeedback_History != null) {
                    otblFeedback_History.destroy();
                }
                otblFeedback_History = $('#tblFeedback_History').DataTable({
                    columns: [
                        {'data':'ActionID'},
                        { 'data': 'StatusName' },
                        { 'data': 'ActionBy' },
                        { 'data': 'ActionDate' },
                        { 'data': 'Remarks' }
                    ],
                    "aoColumnDefs": [{ "targets": [0], "visible": false },{ className: "textAlignLeft", "targets": [0, 1, 3] },{"orderable": false, "targets": [0,1,2,3,4]}],
                    "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/FeedbackMaster/Feedbackmaster.asmx/getFeedbackHistoryList")%>",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData.push({ "name": "FeedbackID", "value": $('#<%=hdnFeedbackID.ClientID%>').val() });
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success": function (msg) {
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                UserSessionCheck();
                                $("#tblFeedback_History").show();
                              //  $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                                $(".tblFeedback_HistoryClass").css({ "width": "100%" });
                            },
                            error: function (xhr, textStatus, error) {
                                if (typeof console == "object") {
                                    console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                                }
                            }
                        });
                    }
                });
            }
            else {
                otblFeedback_History = $('#tblFeedback_History').DataTable();
            }
        }

        function ReloadtblFeedback_History() {
            loadFeedback_History();
        }
    </script>

    <script>
          function HideImg() {
              ShowPleaseWait('hide');
          }
    </script>
</asp:Content>
