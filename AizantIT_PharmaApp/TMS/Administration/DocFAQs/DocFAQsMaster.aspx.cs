﻿using AizantIT_PharmaApp.Common;
using System;
using System.Collections;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BusinessLayer;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.Administration.DocFAQs
{
    public partial class DocFAQsMaster : System.Web.UI.Page
    {
        UMS_BAL objUMS_BAL;
        TMS_BAL objTMS_BAL;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = (Session["UserDetails"] as DataSet).Tables[1].Select("ModuleID=" + (int)Modules.TMS);
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainer_TMS).Length > 0)
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                LoadUnAuthorizedMessage();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "DocFAQ_M1:" + strline + "  " + strMsg, "error");
            }
        }

        private void LoadUnAuthorizedMessage()
        {
            try
            {
                lblAutorizedMsg.Text = "Your Are Not Authorized To View This Page. Contact Admin.";
                divAutorizedMsg.Visible = true;
                divMainContainer.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "DocFAQ_M2:" + strline + "  " + strMsg, "error");
            }
        }

        public void InitializeThePage()
        {
            try
            {
                if (Request.QueryString["Notification_ID"] != null)
                {
                    objUMS_BAL = new UMS_BusinessLayer.UMS_BAL();
                    int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs); //2 represents NotifyEmp had visited the page through notification link.
                }
                BindDocType();
                bindAllAccessibleDepartment(Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "DocFAQ_M3:" + strline + "  " + strMsg, "error");
            }
        }
        public void bindAllAccessibleDepartment(int EmpID)
        {
            try
            {
                objUMS_BAL = new UMS_BusinessLayer.UMS_BAL();
                int[] RoleIDs = new int[] { (int)TMS_UserRole.Trainer_TMS };
                DataTable dt = objUMS_BAL.getRoleWiseDepts(EmpID, RoleIDs);
                ddlDepartment.DataSource = dt;
                ddlDepartment.DataTextField = "DepartmentName";
                ddlDepartment.DataValueField = "DeptID";
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("-- Select Department --", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "DocFAQ_M4:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindDocType()
        {
            try
            {
                TMS_BAL objTMS_BAL = new TMS_BAL();
                DataTable dt = objTMS_BAL.DocumentType();
                ddlDocumentType.DataSource = dt;
                ddlDocumentType.DataValueField = "DocumentTypeID";
                ddlDocumentType.DataTextField = "DocumentType";
                ddlDocumentType.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "DocFAQ_M5:" + strline + "  " + strMsg, "error");
            }
        }
        protected void ddlDocumentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int SelectedDocType = 0;
                if (ddlDocumentType.SelectedValue != "")
                {
                    SelectedDocType = Convert.ToInt32(ddlDocumentType.SelectedValue);
                    if (Convert.ToInt32(ddlDocumentType.SelectedValue) > 0 && SelectedDocType > 0)
                    {
                        BindDocuments(ddlDepartment.SelectedValue, ddlDocumentType.SelectedValue);
                        ddlDocument.Enabled = true;
                    }
                    else
                    {
                        ddlDocument.Enabled = false;
                        ddlDocument.Items.Clear();
                        ddlDocument.Items.Insert(0, new ListItem("--Select Document--", "0"));
                    }
                }
                else
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Document Type Not Exists To Get Documents", "warning");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "DocFAQ_M6:" + strline + "  " + strMsg, "error");
            }
        }

        public void BindDocuments(string DeptID, string DocumentType)
        {
            try
            {
                ddlDocument.Enabled = false;
                ddlDocument.Items.Clear();
                TMS_BAL objTMS_BAL = new TMS_BAL();
                DataTable dt = new DataTable();
                dt = objTMS_BAL.GetAllDocuments(DeptID, DocumentType);
                ddlDocument.DataSource = dt;
                ddlDocument.DataTextField = "DocumentName";
                ddlDocument.DataValueField = "DocumentID";
                ddlDocument.DataBind();
                ddlDocument.Items.Insert(0, new ListItem("-- Select Document --", "0"));
                ddlDocument.Enabled = true;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "DocFAQ_M7:" + strline + "  " + strMsg, "error");
            }
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlDocument.Enabled = false;
                ddlDocument.Items.Clear();
                int SelectedDepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue);

                if (SelectedDepartmentID > 0 && ddlDocumentType.Items.Count > 0)
                {
                    BindDocuments(SelectedDepartmentID.ToString(), ddlDocumentType.SelectedValue);
                }
                else
                {
                    ddlDocument.Items.Insert(0, new ListItem("-- Select Document --", "0"));
                    ddlDocument.Enabled = false;
                }
                if (SelectedDepartmentID > 0 && ddlDocumentType.Items.Count == 0)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Document Type Not Exists To Get Documents", "warning");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "DocFAQ_M8::" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnSubmitDocFAQ_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtDocFAQ_Answer.Value.Trim() == "" || txtDocFAQ_Question.Value.Trim() == "" || (hdnNewOrEditFAQ.Value == "EditFAQ" && txtDocFAQ_Comments.Value == ""))
                {
                    ArrayList Mandatory = new ArrayList();
                    if (txtDocFAQ_Question.Value.Trim() == "")
                    {
                        Mandatory.Add("Enter Question");
                    }
                    if (txtDocFAQ_Answer.Value.Trim() == "")
                    {
                        Mandatory.Add("Enter Answer");
                    }
                    if (hdnNewOrEditFAQ.Value == "EditFAQ" && txtDocFAQ_Comments.Value == "")
                    {
                        Mandatory.Add("Enter Modified Reason in Comments");
                    }
                    string strMsg = string.Join(",<br/>", Mandatory.ToArray());
                    HelpClass.custAlertMsg(this, this.GetType(), strMsg + ".", "error");
                }
                else
                {
                    // code for submitting the DOCFAQ
                    SubmitDocFAQ();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "DocFAQ_M9:" + strline + "  " + strMsg, "error");
            }
        }
        public void SubmitDocFAQ()
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                string comments = txtDocFAQ_Comments.Value.Trim();
                int QuestionStatus = 0;
                int Result = 0;
                if (hdnNewOrEditFAQ.Value == "NewFAQ")
                {
                    objTMS_BAL.CreateDocFAQ(hdnDocID.Value, txtDocFAQ_Question.Value.Trim(), txtDocFAQ_Answer.Value.Trim(),
                        (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString(), out Result);
                    if (Result == 1)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Failed To Create ! Question Already Exists", "error","changebtnText()");
                    }
                    if (Result == 2)
                    {
                        txtDocFAQ_Answer.Value = txtDocFAQ_Comments.Value = txtDocFAQ_Question.Value = "";
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Question Created Successfully !", "success", "loadDocFAQ_Master()");
                    }
                    else
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Failed To Create.", "error");
                    }
                }
                if (hdnNewOrEditFAQ.Value == "EditFAQ")
                {
                    if (rbtnActive.Checked)
                    {
                        QuestionStatus = 1;
                    }
                    else if (rbtnInActive.Checked)
                    {
                        QuestionStatus = 2;
                    }
                    string ModifiedReason = txtDocFAQ_Comments.Value.Trim();
                    objTMS_BAL.ModifyDOCFAQ(hdnDocFAQQuestionID.Value, txtDocFAQ_Question.Value.Trim(),
                        txtDocFAQ_Answer.Value.Trim(), (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString(),
                        QuestionStatus.ToString(), ModifiedReason, out Result);
                    if (Result == 1)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Question Updated Successfully !", "success", "CloseFAQModal()");
                    }
                    else if (Result == 2)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Failed To Update, Question Already Exists with same content.", "info", "CheckWethereNewOrEditFAQ()");
                    }
                    else
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Failed To Update.", "error");
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "DocFAQ_M10:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnEditDocFAQ_Click(object sender, EventArgs e)
        {
            try
            {
                txtDocFAQ_Answer.Value = txtDocFAQ_Comments.Value = txtDocFAQ_Question.Value = "";
                objTMS_BAL = new TMS_BAL();
                DataTable dtDocQueDetails = objTMS_BAL.GetDocFaqQuestionDetails(hdnDocFAQQuestionID.Value);
                if (dtDocQueDetails.Rows.Count > 0)
                {
                    DataRow dr = dtDocQueDetails.Rows[0];
                    txtDocFAQ_Answer.Value = dr["Answer"].ToString();
                    txtDocFAQ_Question.Value = dr["Question"].ToString();
                    if (dr["Status"].ToString() == "1")
                    {
                        lblRadioActiveFQ.Attributes.Add("class", "btn btn-primary_radio radio_check col-6 col-lg-6 col-md-6 col-sm-6 float-left active");
                        lblRadioInActiveFQ.Attributes.Add("class", "btn btn-primary_radio radio_check col-6 col-lg-6 col-md-6 col-sm-6 float-left ");
                        rbtnActive.Checked = true;
                        rbtnInActive.Checked = false;
                    }
                    if (dr["Status"].ToString() == "2")
                    {
                        lblRadioActiveFQ.Attributes.Add("class", "btn btn-primary_radio radio_check col-6 col-lg-6 col-md-6 col-sm-6 float-left");
                        lblRadioInActiveFQ.Attributes.Add("class", "btn btn-primary_radio radio_check col-6 col-lg-6 col-md-6 col-sm-6 float-left active");
                        rbtnActive.Checked = false;
                        rbtnInActive.Checked = true;
                    }
                    upFaqModal.Update();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "OpenModelFAQ('EditFAQ')", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "DocFAQ_M11:" + strline + "  " + strMsg, "error");
            }
        }
    }
}