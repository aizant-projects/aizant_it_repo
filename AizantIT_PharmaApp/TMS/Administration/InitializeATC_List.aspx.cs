﻿using Aizant_Enums;
using AizantIT_PharmaApp.Common;
using System;
using System.Collections;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BusinessLayer;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.Administration
{
    public partial class InitializeATC_List : System.Web.UI.Page
    {
        UMS_BusinessLayer.UMS_BAL objUMS_BAL;
        TMS_BAL objTMS_BAL;
        public string DeptID { get; private set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        hdnEmpID.Value = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]).ToString();
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drTmsRoles = dtx.Select("ModuleID="+(int)Modules.TMS);
                        if (drTmsRoles.Length > 0)
                        {
                            dtTemp = drTmsRoles.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0)
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }                    
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "I_ATC1:" + strline + "  " + strMsg, "error");
            }
        }

        private void loadAuthorizeErrorMsg()
        {
            try
            {
                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                divAutorizedMsg.Visible = true;
                divMainContainer.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "I_ATC2:" + strline + "  " + strMsg, "error");
            }
        }
        private void InitializeThePage()
        {
            try
            {
                BindDepartments_Create(Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                BindYears(DateTime.Now.Year); 
                BindApprovalHOD();
                BindApprovalQA();
                BindATC_Author();
                // GetATC_intializeList();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "I_ATC3:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindDepartments_Create(int EmpID)
        {
            try
            {
                objUMS_BAL = new UMS_BusinessLayer.UMS_BAL();
                int[] RoleIDs = new int[] { (int)TMS_UserRole.HOD_TMS };
                DataTable dt = objUMS_BAL.getRoleWiseDepts(EmpID, RoleIDs);
                ddlDepartment.Items.Clear();
                ddlDepartment.DataSource = dt;
                ddlDepartment.DataTextField = "DepartmentName";
                ddlDepartment.DataValueField = "DeptID";
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("- Select Department -", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "I_ATC4:" + strline + "  " + strMsg, "error");
            }
        }
        protected async void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
              
                if (Convert.ToInt32(ddlDepartment.SelectedValue) == 0)
                {
                    ddlATC_Author.Items.Clear();
                    ddlATC_Author.Items.Insert(0, new ListItem("- Select Author -", "0"));
                    ddlApprovalHod.Items.Clear();
                    ddlApprovalHod.Items.Insert(0, new ListItem("- Select Approval -", "0"));
                    ddlApprovalQA.Items.Clear();
                    ddlApprovalQA.Items.Insert(0, new ListItem("- Select Approval QA -", "0"));
                }
                else
                {
                    Task<string> taskAuthor = loadAtcAuthors();
                    Task<string> taskApprovalHod = loadApprovalHods();
                    Task<string> taskApprovalQa = loadApprovalQAs();

                    await taskAuthor;
                    await taskApprovalHod;
                    await taskApprovalQa;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "I_ATC5:" + strline + "  " + strMsg, "error");
            }
        }

        private async Task<string> loadApprovalQAs()
        {
            DataTable dt = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(ddlDepartment.SelectedValue), 5, true,2); //QA=5
            ddlApprovalQA.Items.Clear();
            ddlApprovalQA.DataSource = dt;
            ddlApprovalQA.DataTextField = "EmpName";
            ddlApprovalQA.DataValueField = "EmpID";
            ddlApprovalQA.DataBind();
            ddlApprovalQA.Items.Insert(0, new ListItem("- Select Approval QA -", "0"));
            return "Done";
        }

        private async Task<string> loadApprovalHods()
        {
            DataTable dt = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(ddlDepartment.SelectedValue), 3, true,2); //3=HOD
            ddlApprovalHod.Items.Clear();
            ddlApprovalHod.DataSource = dt;
            ddlApprovalHod.DataTextField = "EmpName";
            ddlApprovalHod.DataValueField = "EmpID";
            ddlApprovalHod.DataBind();
            ddlApprovalHod.Items.Insert(0, new ListItem("- Select Approval -", "0"));
            return "Done";
        }

        private async Task<string> loadAtcAuthors()
        {
            DataTable dtemp = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(ddlDepartment.SelectedValue), 7, true,2); //7=Trainer
            ddlATC_Author.DataSource = dtemp;
            ddlATC_Author.DataTextField = "EmpName";
            ddlATC_Author.DataValueField = "EmpID";
            ddlATC_Author.DataBind();
            ddlATC_Author.Items.Insert(0, new ListItem("- Select Author -", "0"));
            return "Done";
        }

        protected void ddlATC_Author_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindApprovalHOD(ddlDepartment.SelectedValue, ddlApprovalHod.SelectedValue);
                BindApprovalQA(ddlDepartment.SelectedValue, ddlApprovalQA.SelectedValue);
                remove();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "I_ATC6:" + strline + "  " + strMsg, "error");
            }
        }
        protected void ddlApprovalHod_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindATC_Author(ddlDepartment.SelectedValue, ddlATC_Author.SelectedValue);
                BindApprovalQA(ddlDepartment.SelectedValue, ddlApprovalQA.SelectedValue);
                remove();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "I_ATC7:" + strline + "  " + strMsg, "error");
            }
        }

        public void remove()
        {
            try
            {
                if (ddlApprovalHod.SelectedValue != "0")
                {
                    ddlApprovalQA.Items.Remove(ddlApprovalQA.Items.FindByValue(ddlApprovalHod.SelectedValue));
                    ddlATC_Author.Items.Remove(ddlATC_Author.Items.FindByValue(ddlApprovalHod.SelectedValue));
                }
                if (ddlApprovalQA.SelectedValue != "0")
                {
                    ddlATC_Author.Items.Remove(ddlATC_Author.Items.FindByValue(ddlApprovalQA.SelectedValue));
                    ddlApprovalHod.Items.Remove(ddlApprovalHod.Items.FindByValue(ddlApprovalQA.SelectedValue));
                }
                if (ddlATC_Author.SelectedValue != "0")
                {
                    ddlApprovalQA.Items.Remove(ddlApprovalQA.Items.FindByValue(ddlATC_Author.SelectedValue));
                    ddlApprovalHod.Items.Remove(ddlApprovalHod.Items.FindByValue(ddlATC_Author.SelectedValue));
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "I_ATC8:" + strline + "  " + strMsg, "error");
            }
        }

        protected void ddlApprovalQA_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindATC_Author(ddlDepartment.SelectedValue, ddlATC_Author.SelectedValue);
                BindApprovalHOD(ddlDepartment.SelectedValue, ddlApprovalHod.SelectedValue);
                remove();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "I_ATC17:" + strline + "  " + strMsg, "error");
            }
        }
        private void BindATC_Author(string DeptID = "0", string SelectedValue = "0")
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                DataTable dtemp = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(ddlDepartment.SelectedValue), (int)AizantEnums.TMS_UserRole.Trainer_TMS, true,2);
                ddlATC_Author.Items.Clear();
                ddlATC_Author.DataSource = dtemp;
                ddlATC_Author.DataTextField = "EmpName";
                ddlATC_Author.DataValueField = "EmpID";
                ddlATC_Author.DataBind();
                ddlATC_Author.Items.Insert(0, new ListItem("- Select Author -", "0"));
                ddlATC_Author.SelectedValue = "0";
                if(ddlATC_Author.Items.FindByValue(SelectedValue)!=null)
                    ddlATC_Author.SelectedValue = SelectedValue;

                //foreach (DataRow drAuthor in dtemp.Rows)
                //{
                //    if (SelectedValue == drAuthor["EmpID"].ToString())
                //    {
                //        ddlATC_Author.SelectedValue = SelectedValue;
                //    }
                //}
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "I_ATC9:" + strline + "  " + strMsg, "error");
            }
        }
        private void BindApprovalHOD(string DeptID = "0", string selectedValue = "0")
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                DataTable dtemp = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(ddlDepartment.SelectedValue), (int)AizantEnums.TMS_UserRole.HOD_TMS, true,2);
                ddlApprovalHod.Items.Clear();
                ddlApprovalHod.DataSource = dtemp;
                ddlApprovalHod.DataTextField = "EmpName";
                ddlApprovalHod.DataValueField = "EmpID";
                ddlApprovalHod.DataBind();
                ddlApprovalHod.Items.Insert(0, new ListItem("- Select Approval -", "0"));
                ddlApprovalHod.SelectedValue = "0";

                if (ddlApprovalHod.Items.FindByValue(selectedValue) != null)
                    ddlApprovalHod.SelectedValue = selectedValue;

                //foreach (DataRow drApprovalHod in dtemp.Rows)
                //{
                //    if (selectedValue == drApprovalHod["EmpID"].ToString())
                //    {
                //        ddlApprovalHod.SelectedValue = selectedValue;
                //    }
                //}
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "I_ATC10:" + strline + "  " + strMsg, "error");
            }
        }
        private void BindApprovalQA(string DeptID = "0", string SelectedValue = "0")
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                DataTable dtemp = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(ddlDepartment.SelectedValue), (int)AizantEnums.TMS_UserRole.QA_TMS, true,2);
                ddlApprovalQA.Items.Clear();
                ddlApprovalQA.DataSource = dtemp;
                ddlApprovalQA.DataTextField = "EmpName";
                ddlApprovalQA.DataValueField = "EmpID";
                ddlApprovalQA.DataBind();
                ddlApprovalQA.Items.Insert(0, new ListItem("- Select Approval QA -", "0"));
                ddlApprovalQA.SelectedValue = "0";

                if (ddlApprovalQA.Items.FindByValue(SelectedValue) != null)
                    ddlApprovalQA.SelectedValue = SelectedValue;

                //foreach (DataRow drApprovalQA in dtemp.Rows)
                //{
                //    if (SelectedValue == drApprovalQA["EmpID"].ToString())
                //    {
                //        ddlApprovalQA.SelectedValue = SelectedValue;
                //    }
                //}
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "I_ATC11:" + strline + "  " + strMsg, "error");
            }
        }
        private void BindYears(int StartFrom)
        {
            try
            {
                ddlYear.Items.Clear();
                int year;
                for (year = StartFrom; year <= StartFrom + 4; year++)
                {
                    ddlYear.Items.Add(year.ToString());
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "I_ATC12:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnUpdateATC_Click(object sender, EventArgs e)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                if (txtRemarks.Text == "" || ddlYear.SelectedValue == "0" || ddlDepartment.SelectedValue == "0" || ddlApprovalHod.SelectedValue == "0"
                   || ddlATC_Author.SelectedValue == "0" || ddlApprovalQA.SelectedValue == "0")
                {
                    ArrayList Mandatory = new ArrayList();
                    if (ddlYear.SelectedValue == "0")
                    {
                        Mandatory.Add("Select Year");
                    }
                    if (ddlDepartment.SelectedValue == "0")
                    {
                        Mandatory.Add("Select Department");
                    }
                    if (ddlApprovalHod.SelectedValue == "0")
                    {
                        Mandatory.Add("Select Approval");
                    }
                    if (ddlApprovalQA.SelectedValue == "0")
                    {
                        Mandatory.Add("Select Approval QA");
                    }
                    if (ddlATC_Author.SelectedValue == "0")
                    {
                        Mandatory.Add("Select Author");
                    }
                    if (txtRemarks.Text == "")
                    {
                        Mandatory.Add("Specify Modify Reason in Remarks Box.");
                    }
                    StringBuilder s = new StringBuilder();
                    foreach (string x in Mandatory)
                    {
                        s.Append(x);
                        s.Append(",");
                    }
                    HelpClass.custAlertMsg(this, this.GetType(), "" + s.ToString() + "", "error");
                }
                else
                {
                    objTMS_BAL.UpdateInitializedATC(hdnATC_ID.Value, (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString(),
                    txtRemarks.Text, ddlATC_Author.SelectedValue, ddlApprovalHod.SelectedValue, ddlApprovalQA.SelectedValue, out int UpdateStatus);
                    string msgType = "", msg = "";
                    switch (UpdateStatus)
                    {
                        case 1:
                            msg = "There is no ATC to update.";
                            msgType = "error";
                            break;
                        case 2:
                            msg = "ATC is not in a status of update.";
                            msgType = "info";
                            break;
                        case 3:
                            msg = "Modified Author,Approver and Reviewer Successfully!";
                            msgType = "success";
                            ClearText();
                            break;
                        case 4:
                            msg = "Modified Approver and Reviewer Successfully!";
                            msgType = "success";
                            ClearText(); 
                            break;
                        case 5:
                            msg = "Modified Author and Approver Successfully!";
                            msgType = "success";
                            ClearText(); ;
                            break;
                        case 6:
                            msg = "Modified Author and Reviewer Successfully!";
                            msgType = "success";
                            ClearText();
                            break;
                        case 7:
                            msg = "Modified Approver Successfully!";
                            msgType = "success";
                            ClearText();
                            break;
                        case 8:
                            msg = "Modified Reviewer Successfully!";
                            msgType = "success";
                            ClearText();
                            break;
                        case 9:
                            msg = "Modified Author Successfully!";
                            msgType = "success";
                            ClearText();
                            break;
                        case 10:
                            msg = "None of the Author or Approver or Reviewer were Modified.";
                            msgType = "info";
                            break;
                        default:
                            msg = "Unable to perform the action, Contact Admin.";
                            msgType = "info";
                            break;
                    }
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "func", "CustomAlertmsginIATC('" + msg + "','" + msgType + "');", true);
                    //GetATC_intializeList();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "I_ATC13:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnInitialize_Click(object sender, EventArgs e)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                if (ddlYear.SelectedValue == "0" || ddlDepartment.SelectedValue == "0" || ddlApprovalHod.SelectedValue == "0"
                    || ddlATC_Author.SelectedValue == "0" || ddlApprovalQA.SelectedValue == "0")
                {
                    ArrayList Mandatory = new ArrayList();
                    if (ddlYear.SelectedValue == "0")
                    {
                        Mandatory.Add("Select Year");
                    }
                    if (ddlDepartment.SelectedValue == "0")
                    {
                        Mandatory.Add("Select Department");
                    }
                    if (ddlApprovalHod.SelectedValue == "0")
                    {
                        Mandatory.Add("Select Approval");
                    }
                    if (ddlApprovalQA.SelectedValue == "0")
                    {
                        Mandatory.Add("Select Approval QA");
                    }
                    if (ddlATC_Author.SelectedValue == "0")
                    {
                        Mandatory.Add("Select Author");
                    }
                    StringBuilder s = new StringBuilder();
                    foreach (string x in Mandatory)
                    {
                        s.Append(x);
                        s.Append(",");
                    }
                    HelpClass.custAlertMsg(this, this.GetType(), "" + s.ToString() + "", "error");
                }
                else
                {
                    objTMS_BAL.SubmitInitializeATC(ddlDepartment.SelectedValue, ddlYear.SelectedValue,
                (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString(), txtRemarks.Text,
                ddlATC_Author.SelectedValue, ddlApprovalHod.SelectedValue, ddlApprovalQA.SelectedValue, "/TMS/ATC/List/CreateATC_List.aspx", out int CreationStatus);
                    if (CreationStatus == 1)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Annual Training Calendar Initialized Successfully!", "success", "ReloadI_ATC_JQ_Dtable()");
                        ClearText();
                    }
                    else if (CreationStatus == 2)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Annual Training Calendar Already Initialized.", "info");
                    }
                    else
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Failed to Initialize the Annual Training Calendar, Contact Admin.", "error");
                    }
                    //  GetATC_intializeList();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "I_ATC14:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                ClearText();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "I_ATC15:" + strline + "  " + strMsg, "error");
            }
        }
        public void ClearText()
        {
            try
            {
                BindDepartments_Create(Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                BindATC_Author();
                BindApprovalQA();
                BindApprovalHOD();
                BindYears(DateTime.Now.Year);              
                txtRemarks.Text = string.Empty;
                spanRemarks.Visible = false;
                ddlDepartment.Enabled = true;
                ddlYear.Enabled = true;
                btnInitialize.Visible = true;
                btnUpdateATC.Visible = false;
                btnCancel.Text = "Reset";
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "I_ATC16:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnEditInitializeATC_Click(object sender, EventArgs e)
        {
            try
            {
                btnUpdateATC.Visible = true;
                objTMS_BAL = new TMS_BAL();
                int atcID = Convert.ToInt32(hdnATC_ID.Value);
                string Department = Convert.ToString(hdnDeptName.Value);
                string CalendarYear = Convert.ToString(hdnCalendarYear.Value);
                string Author = Convert.ToString(hdnAuthor.Value).Trim();
                string Reviewer = Convert.ToString(hdnReviewer.Value).Trim();
                string Approver = Convert.ToString(hdnApprover.Value).Trim();

                ddlDepartment.SelectedValue = Department;
                ddlDepartment.Enabled = false;
                //ddlDepartment.Attributes["style"] = "cursor:not-allowed;";
                // ddlDepartment.Style.Add("cursor", "not-allowed");
                BindATC_Author(Department, Author);
                BindApprovalHOD(Department, Reviewer);
                BindApprovalQA(Department, Approver);
                //ddlYear.SelectedValue = DateTime.Now.Year.ToString();
                ddlYear.SelectedValue = CalendarYear;
                ddlYear.Enabled = false;
                //ddlYear.Attributes.Add("style", "cursor:not-allowed;");
                remove();
                btnUpdateATC.Visible = true;
                btnInitialize.Visible = false;
                spanRemarks.Visible = true;
                btnCancel.Text = "Cancel";
                // collapseFG.Attributes.Add("class", "panel-collapse collapse in");
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ExpandDiv();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "I_ATC17:" + strline + "  " + strMsg, "error");
            }
        }
    }
}