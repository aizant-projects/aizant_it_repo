﻿using Aizant_Enums;
using AizantIT_PharmaApp.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BusinessLayer;
using TMS_BusinessObjects;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.Administration.Questionnaire
{
    public partial class Questionnaire : System.Web.UI.Page
    {
        UMS_BusinessLayer.UMS_BAL objUMS_BAL;
        TMS_BAL objTMS_BAL;
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            ConfirmCustomAlert.buttonClick += new EventHandler(ConfirmCustomAlert_buttonClick);
            if (HelpClass.IsUserAuthenticated())
            {
                if (!IsPostBack)
                {
                    DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                    DataTable dtTemp = new DataTable();
                    DataRow[] drUMS = dtx.Select("ModuleID="+(int)Modules.TMS);
                    if (drUMS.Length > 0)
                    {
                        dtTemp = drUMS.CopyToDataTable();
                        if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainer_TMS).Length > 0)
                        {
                            InitializeThePage();
                        }
                        else
                        {
                            loadAuthorizeErrorMsg();
                        }
                    }
                    else
                    {
                        Response.Redirect("~/Dashboards/Home.aspx", false);
                    }
                }            
            }
            else
            {
                Response.Redirect("~/UserLogin.aspx", false);
            }
        }

        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized To View This Page. Contact Admin.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        public void InitializeThePage()
        {
            bindDropDown();
            if (btnCreate.Text == "Update")
            {
                btnCreate.Text = "Create";
            }
            if (btnClear.Text == "Cancel")
            {
                btnClear.Text = "Clear";
            }
            gvQuestionnaire.DataSource = new List<object>();
            gvQuestionnaire.DataBind();
            gvRevertedQuestionnaire.DataSource = new List<object>();
            gvRevertedQuestionnaire.DataBind();
            gvApprovedQuestionnaire.DataSource = new List<object>();
            gvApprovedQuestionnaire.DataBind();
            lblApprovedCount.Text = lblRevertedCount.Text = lblApprovalPendingCount.Text = "0";
            upTabs.Update();
            //ddlAnswer.Items[3].Attributes["disabled"] = "disabled";
            ddlAnswer.Items.FindByValue("C").Enabled = false;
            ddlAnswer.Items.FindByValue("D").Enabled = false;
            ddlAnswer.Items.FindByValue("E").Enabled = false;
            //ddlAnswer.Items[4].Attributes["disabled"] = "disabled";
            //ddlAnswer.Items[5].Attributes["disabled"] = "disabled";
        }
        protected void ConfirmCustomAlert_buttonClick(object sender, EventArgs e)
        {
            try
            {
                if (hdnDeleteInactive.Value == "Delete")
                {
                    objTMS_BAL = new TMS_BAL();
                    int QueID = Convert.ToInt32(hdnDeleteQueID.Value);
                    int DelStatus;
                    objTMS_BAL.DeleteQuestionnaire(QueID, out DelStatus);
                    if (DelStatus == 1)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Questionnaire Is Deleted !", "success", "CloseModal()");
                    }
                    else
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Questionnaire Is Failed To Delete, It's Not In State Of Delete !", "error");
                    }
                }
                else if (hdnDeleteInactive.Value == "InActive")
                {
                    objTMS_BAL = new TMS_BAL();
                    int Result = 0;
                    int QueID = Convert.ToInt32(hdnDeleteQueID.Value);
                    int s = objTMS_BAL.InActiveQuestionnaire(ddlDocuments.SelectedValue, QueID, ((int)AizantEnums.Questionnaire_Status.Questionnaire_InActive), Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString()), out Result);
                    if (Result == 1)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Questionnaire Is In-Activated !", "success");
                        BindGV(ddlDocuments.SelectedItem.Value);
                    }
                    if (Result == 2)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "When Trainings Are Undergoing On This Document You\\'re Not Able To In-Active The Questionnaire.", "error");
                    }
                }
                LoadDefault();
                BindGV(ddlDocuments.SelectedItem.Value);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "Q1" + strline + "  " + strMsg, "error");
            }
        }

        #region DropDownsCode
        public void bindDropDown()
        {
            try
            {
                if (Request.QueryString["Reverted"] != null)
                {
                    bindRevertedDepartmentAndDocTypeForModify((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                }
                else
                {
                    bindAllDepartmentAndDocTypeForCreate(Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString())); //To bind trainer Accessible dept and Doc Type
                }
                ddlDocuments.Enabled = false;
                ddlDocuments.Items.Clear();
                ddlDocuments.Items.Insert(0, new ListItem("-- Select Document --", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "Q2:" + strline + "  " + strMsg, "error");
            }
        }
        public void bindAllDepartmentAndDocTypeForCreate(int EmpID)
        {
            try
            {
                TMS_BAL objTMS_BAL = new TMS_BAL();
                DataSet ds = objTMS_BAL.getModuleWiseDeptsAndDocTypes(EmpID);
                BindDepts(ds.Tables[0]);
                BindDocType(ds.Tables[1]);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "Q3:" + strline + "  " + strMsg, "error");
            }

        }

        private void BindDepts(DataTable dtDept)
        {
            try
            {
                ddlDepartment.DataSource = dtDept;
                ddlDepartment.DataTextField = "DepartmentName";
                ddlDepartment.DataValueField = "DeptID";
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("-- Select Department --", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "Q4:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindDocType(DataTable dtDoctype)
        {
            try
            {
                ddlDocumentType.DataSource = dtDoctype;
                ddlDocumentType.DataValueField = "DocumentTypeID";
                ddlDocumentType.DataTextField = "DocumentType";
                ddlDocumentType.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "Q5:" + strline + "  " + strMsg, "error");
            }
        }
        public void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlDocuments.Enabled = false;
                ddlDocuments.Items.Clear();
                int SelectedDepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue);
                int SelectedDocType = 0;
                if (ddlDocumentType.Items.Count > 0)
                {
                    SelectedDocType = Convert.ToInt32(ddlDocumentType.SelectedValue);
                }
                //else
                //{
                if (SelectedDepartmentID > 0 && SelectedDocType > 0)
                {
                    bindDocuments(SelectedDepartmentID.ToString(), ddlDocumentType.SelectedValue);
                }
                else
                {
                    ddlDocuments.Items.Insert(0, new ListItem("-- Select Document --", "0"));
                    ddlDocuments.Enabled = false;
                }
                LoadDefault();
                BindGV(ddlDocuments.SelectedItem.Value);
                // }
                if (ddlDocumentType.Items.Count == 0 && SelectedDepartmentID > 0)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Document Type Not Exists To Get Documents", "warning");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "Q6:" + strline + "  " + strMsg, "error");
            }
        }
        private void bindRevertedDepartmentAndDocTypeForModify(string EmpID)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                DataSet dsRevertedData = objTMS_BAL.getAccessibleAndToDoActionDepts(EmpID, "RevertedQuestionnaire");
                BindDepts(dsRevertedData.Tables[0]);
                BindDocType(dsRevertedData.Tables[1]);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "Q7:" + strline + "  " + strMsg, "error");
            }
        }
        public void bindDocuments(string DeptID, string DocType)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                DataTable dt = new DataTable();
                if (Request.QueryString["Reverted"] != null)
                {
                    dt = objTMS_BAL.GetDeptWiseDocsForQuestionnaireApprovalOrRevert(DeptID, (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString(), "RevertedQuestionnaire", DocType);
                }
                else
                {
                    dt = objTMS_BAL.GetActiveDocs(DeptID, DocType);
                }
                ddlDocuments.DataSource = dt;
                ddlDocuments.DataTextField = "DocumentName";
                ddlDocuments.DataValueField = "DocumentID";
                ddlDocuments.DataBind();
                ddlDocuments.Items.Insert(0, new ListItem("-- Select Documents --", "0"));
                ddlDocuments.Enabled = true;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "Q8:" + strline + "  " + strMsg, "error");
            }
        }
        protected void ddlDocumentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(ddlDocumentType.SelectedValue) > 0 && Convert.ToInt32(ddlDepartment.SelectedValue) > 0)
                {
                    bindDocuments(ddlDepartment.SelectedValue, ddlDocumentType.SelectedValue);
                }
                else
                {
                    ddlDocuments.Enabled = false;
                    ddlDocuments.Items.Clear();
                    ddlDocuments.Items.Insert(0, new ListItem("--Select Document--", "0"));
                }
                BindGV(ddlDocuments.SelectedValue);
            }
            catch (Exception ex)
            {

                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "Q9:" + strline + "  " + strMsg, "error");
            }
        }

        protected void ddlNoofOptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlAnswer.SelectedValue = "0";
                if (ddlNoofOptions.SelectedValue == "2")
                {
                    divOptionC.Visible = false;
                    divOptionD.Visible = false;
                    divOptionE.Visible = false;
                    //ddlAnswer.Items[3].Attributes["disabled"] = "disabled";
                    //ddlAnswer.Items[4].Attributes["disabled"] = "disabled";
                    //ddlAnswer.Items[5].Attributes["disabled"] = "disabled";
                    ddlAnswer.Items.FindByValue("C").Enabled = false;
                    ddlAnswer.Items.FindByValue("D").Enabled = false;
                    ddlAnswer.Items.FindByValue("E").Enabled = false;
                }
                else if (ddlNoofOptions.SelectedValue == "3")
                {
                    divOptionC.Visible = true;
                    divOptionD.Visible = false;
                    divOptionE.Visible = false;
                    //ddlAnswer.Items[4].Attributes["disabled"] = "disabled";
                    //ddlAnswer.Items[5].Attributes["disabled"] = "disabled";
                    ddlAnswer.Items.FindByValue("D").Enabled = false;
                    ddlAnswer.Items.FindByValue("E").Enabled = false;
                    ddlAnswer.Items.FindByValue("C").Enabled = true;
                }
                else if (ddlNoofOptions.SelectedValue == "4")
                {
                    divOptionC.Visible = true;
                    divOptionD.Visible = true;
                    divOptionE.Visible = false;
                    //ddlAnswer.Items[5].Attributes["disabled"] = "disabled";
                    ddlAnswer.Items.FindByValue("E").Enabled = false;
                    ddlAnswer.Items.FindByValue("C").Enabled = true;
                    ddlAnswer.Items.FindByValue("D").Enabled = true;
                }
                else if (ddlNoofOptions.SelectedValue == "5")
                {
                    divOptionC.Visible = true;
                    divOptionD.Visible = true;
                    divOptionE.Visible = true;
                    ddlAnswer.Items.FindByValue("C").Enabled = true;
                    ddlAnswer.Items.FindByValue("D").Enabled = true;
                    ddlAnswer.Items.FindByValue("E").Enabled = true;
                }
                else
                {
                    divOptionC.Visible = false;
                    divOptionD.Visible = false;
                    divOptionE.Visible = false;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "Q10:" + strline + "  " + strMsg, "error");
            }
        }

        #endregion

        #region GridViews Code
        public void BindGV(string DocID)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                int EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());

                lblApprovalPendingCount.Text = lblRevertedCount.Text = lblApprovedCount.Text = "0";
                if (DocID != "0")
                {
                    DataSet dsQuestonnaire = objTMS_BAL.gvDocQuestionnaire(DocID, EmpID, out int OnTraining);
                    if (OnTraining == 0)
                    {
                        if (dsQuestonnaire.Tables[0].Rows.Count >= 0)
                        {
                            gvQuestionnaire.DataSource = dsQuestonnaire.Tables[0];
                            gvQuestionnaire.DataBind();
                            lblApprovalPendingCount.Text = dsQuestonnaire.Tables[0].Rows.Count.ToString();
                        }
                        else
                        {
                            EmptyPendingGv();
                        }
                        if (dsQuestonnaire.Tables[1].Rows.Count >= 0)
                        {
                            gvRevertedQuestionnaire.DataSource = dsQuestonnaire.Tables[1];
                            gvRevertedQuestionnaire.DataBind();
                            lblRevertedCount.Text = dsQuestonnaire.Tables[1].Rows.Count.ToString();
                        }
                        else
                        {
                            EmptyRevertedGv();
                        }
                        if (dsQuestonnaire.Tables[2].Rows.Count >= 0)
                        {
                            gvApprovedQuestionnaire.DataSource = dsQuestonnaire.Tables[2];
                            gvApprovedQuestionnaire.DataBind();
                            lblApprovedCount.Text = dsQuestonnaire.Tables[2].Rows.Count.ToString();
                        }
                        else
                        {
                            EmptyApprovedGv();
                        }
                    }
                    else
                    {
                        EmptyPendingGv();
                        EmptyRevertedGv();
                        EmptyApprovedGv();
                        btnCreate.Enabled = false;
                        HelpClass.custAlertMsg(this, this.GetType(), "Training Is going On This Document, So you\\'re Not Able To Access Questionnaire On This Document.", "info");
                    }
                }
                else
                {
                    EmptyPendingGv();
                    EmptyRevertedGv();
                    EmptyApprovedGv();
                }
                upTabs.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "Q11:" + strline + "  " + strMsg, "error");
            }
        }

        private void EmptyApprovedGv()
        {
            gvApprovedQuestionnaire.DataSource = new List<object>();
            gvApprovedQuestionnaire.DataBind();
        }

        private void EmptyRevertedGv()
        {
            gvRevertedQuestionnaire.DataSource = new List<object>();
            gvRevertedQuestionnaire.DataBind();
        }

        private void EmptyPendingGv()
        {
            gvQuestionnaire.DataSource = new List<object>();
            gvQuestionnaire.DataBind();
        }

        protected void gvQuestionnaire_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                int QueID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName == "EditButton")
                {
                    hdnEditQueID.Value = QueID.ToString();
                    //LoadDefault();
                    btnCreate.Text = "Update";
                    btnClear.Text = "Cancel";
                    btnCreate.Attributes.Remove("title");
                    btnClear.Attributes.Remove("title");
                    btnCreate.Attributes.Add("title", "Click to Update");
                    btnClear.Attributes.Add("title","Click to Cancel");
                    DataSet ds = objTMS_BAL.GetQuestionnairetoEdit(QueID);
                    DataTable dtQuestonnaire = ds.Tables[0];
                    if (dtQuestonnaire.Rows.Count > 0)
                    {
                        txtQuestion.Value = ds.Tables[0].Rows[0]["QuestionTitle"].ToString();
                        string Answer = ds.Tables[0].Rows[0]["Answer"].ToString();
                        if (Answer == "A")
                        {
                            ddlAnswer.SelectedValue = "A";
                        }
                        else if (Answer == "B")
                        {
                            ddlAnswer.SelectedValue = "B";
                        }
                        else if (Answer == "C")
                        {
                            ddlAnswer.SelectedValue = "C";
                        }
                        else if (Answer == "D")
                        {
                            ddlAnswer.SelectedValue = "D";
                        }
                        else if (Answer == "E")
                        {
                            ddlAnswer.SelectedValue = "E";
                        }
                    }
                    DataTable dtOptions = ds.Tables[1];
                    if (dtOptions.Rows.Count > 0)
                    {
                        if (dtOptions.Rows.Count == 5)
                        {
                            txtOptionA.Value = ds.Tables[1].Rows[0]["OptionTitle"].ToString();
                            txtOptionB.Value = ds.Tables[1].Rows[1]["OptionTitle"].ToString();
                            txtOptionC.Value = ds.Tables[1].Rows[2]["OptionTitle"].ToString();
                            txtOptionD.Value = ds.Tables[1].Rows[3]["OptionTitle"].ToString();
                            txtOptionE.Value = ds.Tables[1].Rows[4]["OptionTitle"].ToString();
                            divOptionC.Visible = true;
                            divOptionD.Visible = true;
                            divOptionE.Visible = true;
                            ddlNoofOptions.SelectedValue = "5";
                            // ddlAnswer.Attributes.Remove("disabled");
                            ddlAnswer.Items.FindByValue("C").Enabled = true;
                            ddlAnswer.Items.FindByValue("D").Enabled = true;
                            ddlAnswer.Items.FindByValue("E").Enabled = true;
                        }
                        else if (dtOptions.Rows.Count == 4)
                        {
                            txtOptionA.Value = ds.Tables[1].Rows[0]["OptionTitle"].ToString();
                            txtOptionB.Value = ds.Tables[1].Rows[1]["OptionTitle"].ToString();
                            txtOptionC.Value = ds.Tables[1].Rows[2]["OptionTitle"].ToString();
                            txtOptionD.Value = ds.Tables[1].Rows[3]["OptionTitle"].ToString();
                            txtOptionE.Value = string.Empty;
                            divOptionC.Visible = true;
                            divOptionD.Visible = true;
                            divOptionE.Visible = false;
                            ddlNoofOptions.SelectedValue = "4";
                            //ddlAnswer.Items[5].Attributes["disabled"] = "disabled";
                            ddlAnswer.Items.FindByValue("C").Enabled = true;
                            ddlAnswer.Items.FindByValue("D").Enabled = true;
                            ddlAnswer.Items.FindByValue("E").Enabled = false;
                        }
                        else if (dtOptions.Rows.Count == 3)
                        {
                            txtOptionA.Value = ds.Tables[1].Rows[0]["OptionTitle"].ToString();
                            txtOptionB.Value = ds.Tables[1].Rows[1]["OptionTitle"].ToString();
                            txtOptionC.Value = ds.Tables[1].Rows[2]["OptionTitle"].ToString();
                            txtOptionD.Value = txtOptionE.Value = string.Empty;
                            divOptionC.Visible = true;
                            divOptionD.Visible = false;
                            divOptionE.Visible = false;
                            ddlNoofOptions.SelectedValue = "3";
                            //ddlAnswer.Items[1].Attributes["enabled"] = "enabled";
                            //ddlAnswer.Items[2].Attributes["enabled"] = "enabled";
                            //ddlAnswer.Items[3].Attributes["enabled"] = "enabled";
                            //ddlAnswer.Items[4].Attributes["disabled"] = "disabled";
                            //ddlAnswer.Items[5].Attributes["disabled"] = "disabled";
                            ddlAnswer.Items.FindByValue("C").Enabled = true;
                            ddlAnswer.Items.FindByValue("D").Enabled = false;
                            ddlAnswer.Items.FindByValue("E").Enabled = false;
                        }
                        else
                        {
                            txtOptionA.Value = ds.Tables[1].Rows[0]["OptionTitle"].ToString();
                            txtOptionB.Value = ds.Tables[1].Rows[1]["OptionTitle"].ToString();
                            txtOptionC.Value = txtOptionD.Value = txtOptionE.Value = string.Empty;
                            divOptionC.Visible = false;
                            divOptionD.Visible = false;
                            divOptionE.Visible = false;
                            ddlNoofOptions.SelectedValue = "2";
                            //ddlAnswer.Items[3].Attributes["disabled"] = "disabled";
                            //ddlAnswer.Items[4].Attributes["disabled"] = "disabled";
                            //ddlAnswer.Items[5].Attributes["disabled"] = "disabled";
                            ddlAnswer.Items.FindByValue("C").Enabled = false;
                            ddlAnswer.Items.FindByValue("D").Enabled = false;
                            ddlAnswer.Items.FindByValue("E").Enabled = false;
                        }

                    }
                }
                if (e.CommandName == "DeleteButton")
                {
                    hdnDeleteInactive.Value = "Delete";
                    hdnDeleteQueID.Value = QueID.ToString();

                    // code for loading the confirm msg
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "OnConfirmDelete();", true);

                    BindGV(ddlDocuments.SelectedItem.Value);

                    if (btnCreate.Text == "Update")
                    {
                        btnCreate.Text = "Create";
                    }
                    if (btnClear.Text == "Cancel")
                    {
                        btnClear.Text = "Clear";
                    }
                    LoadDefault();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "Q12:" + strline + "  " + strMsg, "error");
            }
        }
        protected void gvQuestionnaire_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    objTMS_BAL = new TMS_BAL();
                    string QuestionID = gvQuestionnaire.DataKeys[e.Row.RowIndex].Value.ToString();
                    GridView gvOptions = e.Row.FindControl("gvOptions") as GridView;
                    DataSet ds = objTMS_BAL.GetQuestionnairetoEdit(Convert.ToInt32(QuestionID));
                    DataTable dtOptions = ds.Tables[1];
                    gvOptions.DataSource = dtOptions;
                    gvOptions.DataBind();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "Q13:" + strline + "  " + strMsg, "error");
            }
        }
        protected void gvApprovedQuestionnaire_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                if (e.CommandName == "InactiveButton")
                {
                    hdnDeleteInactive.Value = "InActive";
                    int QueID = Convert.ToInt32(e.CommandArgument);
                    hdnDeleteQueID.Value = QueID.ToString();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "OnConfirmInActive();", true);

                    if (btnCreate.Text == "Update")
                    {
                        btnCreate.Text = "Create";
                    }
                    if (btnClear.Text == "Cancel")
                    {
                        btnClear.Text = "Clear";
                    }
                    //hdnDeleteQuestion.Value = "";
                    LoadDefault();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "Q14:" + strline + "  " + strMsg, "error");
            }
        }
        protected void gvApprovedQuestionnaire_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    objTMS_BAL = new TMS_BAL();
                    string QuestionID = gvApprovedQuestionnaire.DataKeys[e.Row.RowIndex].Value.ToString();
                    GridView gvOptions = e.Row.FindControl("gvOptions") as GridView;
                    DataSet ds = objTMS_BAL.GetQuestionnairetoEdit(Convert.ToInt32(QuestionID));
                    DataTable dtOptions = ds.Tables[1];
                    gvOptions.DataSource = dtOptions;
                    gvOptions.DataBind();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "Q15:" + strline + "  " + strMsg, "error");
            }
        }
        protected void gvRevertedQuestionnaire_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    objTMS_BAL = new TMS_BAL();
                    string QuestionID = gvRevertedQuestionnaire.DataKeys[e.Row.RowIndex].Value.ToString();
                    GridView gvOptions = e.Row.FindControl("gvOptions") as GridView;
                    DataSet ds = objTMS_BAL.GetQuestionnairetoEdit(Convert.ToInt32(QuestionID));
                    DataTable dtOptions = ds.Tables[1];
                    gvOptions.DataSource = dtOptions;
                    gvOptions.DataBind();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "Q16:" + strline + "  " + strMsg, "error");
            }
        }
        protected void gvRevertedQuestionnaire_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                int QueID = Convert.ToInt32(e.CommandArgument);
                hdnEditQueID.Value = QueID.ToString();
                if (e.CommandName == "EditButton")
                {
                    //LoadDefault();
                    btnCreate.Text = "Update";
                    btnClear.Text = "Cancel";
                    DataSet ds = objTMS_BAL.GetQuestionnairetoEdit(QueID);
                    DataTable dtQuestonnaire = ds.Tables[0];
                    if (dtQuestonnaire.Rows.Count > 0)
                    {
                        txtQuestion.Value = ds.Tables[0].Rows[0]["QuestionTitle"].ToString();
                        txtDeclinedReason.Value = ds.Tables[0].Rows[0]["RevertReason"].ToString();
                        divremarks.Visible = true;
                        string Answer = ds.Tables[0].Rows[0]["Answer"].ToString();
                        if (Answer == "A")
                        {
                            ddlAnswer.SelectedValue = "A";
                        }
                        else if (Answer == "B")
                        {
                            ddlAnswer.SelectedValue = "B";
                        }
                        else if (Answer == "C")
                        {
                            ddlAnswer.SelectedValue = "C";
                        }
                        else if (Answer == "D")
                        {
                            ddlAnswer.SelectedValue = "D";
                        }
                        else if (Answer == "E")
                        {
                            ddlAnswer.SelectedValue = "E";
                        }
                    }
                    DataTable dtOptions = ds.Tables[1];
                    if (dtOptions.Rows.Count > 0)
                    {
                        if (dtOptions.Rows.Count == 5)
                        {
                            txtOptionA.Value = ds.Tables[1].Rows[0]["OptionTitle"].ToString();
                            txtOptionB.Value = ds.Tables[1].Rows[1]["OptionTitle"].ToString();
                            txtOptionC.Value = ds.Tables[1].Rows[2]["OptionTitle"].ToString();
                            txtOptionD.Value = ds.Tables[1].Rows[3]["OptionTitle"].ToString();
                            txtOptionE.Value = ds.Tables[1].Rows[4]["OptionTitle"].ToString();
                            divOptionC.Visible = true;
                            divOptionD.Visible = true;
                            divOptionE.Visible = true;
                            ddlNoofOptions.SelectedValue = "5";
                            // ddlAnswer.Attributes.Remove("disabled");
                            ddlAnswer.Items.FindByValue("C").Enabled = true;
                            ddlAnswer.Items.FindByValue("D").Enabled = true;
                            ddlAnswer.Items.FindByValue("E").Enabled = true;
                        }
                        else if (dtOptions.Rows.Count == 4)
                        {
                            txtOptionA.Value = ds.Tables[1].Rows[0]["OptionTitle"].ToString();
                            txtOptionB.Value = ds.Tables[1].Rows[1]["OptionTitle"].ToString();
                            txtOptionC.Value = ds.Tables[1].Rows[2]["OptionTitle"].ToString();
                            txtOptionD.Value = ds.Tables[1].Rows[3]["OptionTitle"].ToString();
                            txtOptionE.Value = string.Empty;
                            divOptionC.Visible = true;
                            divOptionD.Visible = true;
                            divOptionE.Visible = false;
                            ddlNoofOptions.SelectedValue = "4";
                            //ddlAnswer.Items[5].Attributes["disabled"] = "disabled";
                            ddlAnswer.Items.FindByValue("C").Enabled = true;
                            ddlAnswer.Items.FindByValue("D").Enabled = true;
                            ddlAnswer.Items.FindByValue("E").Enabled = false;
                        }
                        else if (dtOptions.Rows.Count == 3)
                        {
                            txtOptionA.Value = ds.Tables[1].Rows[0]["OptionTitle"].ToString();
                            txtOptionB.Value = ds.Tables[1].Rows[1]["OptionTitle"].ToString();
                            txtOptionC.Value = ds.Tables[1].Rows[2]["OptionTitle"].ToString();
                            txtOptionD.Value = txtOptionE.Value = string.Empty;
                            divOptionC.Visible = true;
                            divOptionD.Visible = false;
                            divOptionE.Visible = false;
                            ddlNoofOptions.SelectedValue = "3";
                            //ddlAnswer.Items[1].Attributes["enabled"] = "enabled";
                            //ddlAnswer.Items[2].Attributes["enabled"] = "enabled";
                            //ddlAnswer.Items[3].Attributes["enabled"] = "enabled";
                            //ddlAnswer.Items[4].Attributes["disabled"] = "disabled";
                            //ddlAnswer.Items[5].Attributes["disabled"] = "disabled";
                            ddlAnswer.Items.FindByValue("C").Enabled = true;
                            ddlAnswer.Items.FindByValue("D").Enabled = false;
                            ddlAnswer.Items.FindByValue("E").Enabled = false;

                        }
                        else
                        {
                            txtOptionA.Value = ds.Tables[1].Rows[0]["OptionTitle"].ToString();
                            txtOptionB.Value = ds.Tables[1].Rows[1]["OptionTitle"].ToString();
                            txtOptionC.Value = txtOptionD.Value = txtOptionE.Value = string.Empty;
                            divOptionC.Visible = false;
                            divOptionD.Visible = false;
                            divOptionE.Visible = false;
                            ddlNoofOptions.SelectedValue = "2";
                            //ddlAnswer.Items[3].Attributes["disabled"] = "disabled";
                            //ddlAnswer.Items[4].Attributes["disabled"] = "disabled";
                            //ddlAnswer.Items[5].Attributes["disabled"] = "disabled";
                            ddlAnswer.Items.FindByValue("C").Enabled = false;
                            ddlAnswer.Items.FindByValue("D").Enabled = false;
                            ddlAnswer.Items.FindByValue("E").Enabled = false;
                        }

                    }
                }
                if (e.CommandName == "DeleteButton")
                {
                    hdnDeleteInactive.Value = "Delete";
                    hdnDeleteQueID.Value = QueID.ToString();

                    // code for loading the confirm msg
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "OnConfirmDelete();", true);

                    BindGV(ddlDocuments.SelectedItem.Value);

                    if (btnCreate.Text == "Update")
                    {
                        btnCreate.Text = "Create";
                    }
                    if (btnClear.Text == "Cancel")
                    {
                        btnClear.Text = "Clear";
                    }
                    LoadDefault();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                // HelpClass.showMsg(this, this.GetType(), "Q_gvRQ_RC:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Q17:" + strline + "  " + strMsg, "error");
            }
        }
        #endregion

        private DataTable getOptionsAssigned()
        {

            DataTable dtOptions = new DataTable();
            dtOptions.Columns.Add("Option");
            dtOptions.Columns.Add("OptionTitle");
            DataRow drOptions;
            if (ddlNoofOptions.SelectedValue == "5")
            {
                drOptions = dtOptions.NewRow();
                drOptions["Option"] = "A";
                drOptions["OptionTitle"] = txtOptionA.Value.Trim();
                dtOptions.Rows.Add(drOptions);
                drOptions = dtOptions.NewRow();
                drOptions["Option"] = "B";
                drOptions["OptionTitle"] = txtOptionB.Value.Trim();
                dtOptions.Rows.Add(drOptions);
                drOptions = dtOptions.NewRow();
                drOptions["Option"] = "C";
                drOptions["OptionTitle"] = txtOptionC.Value.Trim();
                dtOptions.Rows.Add(drOptions);
                drOptions = dtOptions.NewRow();
                drOptions["Option"] = "D";
                drOptions["OptionTitle"] = txtOptionD.Value.Trim();
                dtOptions.Rows.Add(drOptions);
                drOptions = dtOptions.NewRow();
                drOptions["Option"] = "E";
                drOptions["OptionTitle"] = txtOptionE.Value.Trim();
                dtOptions.Rows.Add(drOptions);
            }
            else if (ddlNoofOptions.SelectedValue == "4")
            {
                drOptions = dtOptions.NewRow();
                drOptions["Option"] = "A";
                drOptions["OptionTitle"] = txtOptionA.Value.Trim();
                dtOptions.Rows.Add(drOptions);
                drOptions = dtOptions.NewRow();
                drOptions["Option"] = "B";
                drOptions["OptionTitle"] = txtOptionB.Value.Trim();
                dtOptions.Rows.Add(drOptions);
                drOptions = dtOptions.NewRow();
                drOptions["Option"] = "C";
                drOptions["OptionTitle"] = txtOptionC.Value.Trim();
                dtOptions.Rows.Add(drOptions);
                drOptions = dtOptions.NewRow();
                drOptions["Option"] = "D";
                drOptions["OptionTitle"] = txtOptionD.Value.Trim();
                dtOptions.Rows.Add(drOptions);
            }
            else if (ddlNoofOptions.SelectedValue == "3")
            {
                drOptions = dtOptions.NewRow();
                drOptions["Option"] = "A";
                drOptions["OptionTitle"] = txtOptionA.Value.Trim();
                dtOptions.Rows.Add(drOptions);
                drOptions = dtOptions.NewRow();
                drOptions["Option"] = "B";
                drOptions["OptionTitle"] = txtOptionB.Value.Trim();
                dtOptions.Rows.Add(drOptions);
                drOptions = dtOptions.NewRow();
                drOptions["Option"] = "C";
                drOptions["OptionTitle"] = txtOptionC.Value.Trim();
                dtOptions.Rows.Add(drOptions);
            }
            else
            {
                drOptions = dtOptions.NewRow();
                drOptions["Option"] = "A";
                drOptions["OptionTitle"] = txtOptionA.Value.Trim();
                dtOptions.Rows.Add(drOptions);
                drOptions = dtOptions.NewRow();
                drOptions["Option"] = "B";
                drOptions["OptionTitle"] = txtOptionB.Value.Trim();
                dtOptions.Rows.Add(drOptions);
            }
            return dtOptions;
        }
        protected void btnCreate_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtOptions = getOptionsAssigned();
                if (ddlDepartment.SelectedValue == "0" || ddlDocuments.SelectedValue == "0" || txtQuestion.Value.Trim() == "" || ddlAnswer.SelectedValue == "0"
                    || (ddlNoofOptions.SelectedValue == "2" && (txtOptionA.Value.Trim() == "" || txtOptionB.Value.Trim() == ""))
                    || (ddlNoofOptions.SelectedValue == "3" && (txtOptionA.Value.Trim() == "" || txtOptionB.Value.Trim() == "" || txtOptionC.Value.Trim() == ""))
                    || (ddlNoofOptions.SelectedValue == "4" && (txtOptionA.Value.Trim() == "" || txtOptionB.Value.Trim() == "" || txtOptionC.Value.Trim() == "" || txtOptionD.Value.Trim() == ""))
                    || (ddlNoofOptions.SelectedValue == "5" && (txtOptionA.Value.Trim() == "" || txtOptionB.Value.Trim() == "" || txtOptionC.Value.Trim() == "" || txtOptionD.Value.Trim() == "" || txtOptionE.Value.Trim() == ""))
                    )
                {
                    ArrayList Mandatory = new ArrayList();
                    if (ddlDocuments.SelectedValue == "0")
                    {
                        Mandatory.Add("Select Documents");
                    }
                    if (ddlDepartment.SelectedValue == "0")
                    {
                        Mandatory.Add("Select Department");
                    }
                    if (txtQuestion.Value.Trim() == "")
                    {
                        Mandatory.Add("Enter Question");
                    }
                    if (ddlAnswer.SelectedValue == "0")
                    {
                        Mandatory.Add("Select Answer");
                    }

                    if (ddlNoofOptions.SelectedValue == "3")
                    {
                        if (txtOptionA.Value.Trim() == "")
                        {
                            Mandatory.Add("Enter Option A");
                        }
                        if (txtOptionB.Value.Trim() == "")
                        {
                            Mandatory.Add("Enter Option B");
                        }
                        if (txtOptionC.Value.Trim() == "")
                        {
                            Mandatory.Add("Enter Option C");
                        }
                    }
                    else if (ddlNoofOptions.SelectedValue == "4")
                    {
                        if (txtOptionA.Value.Trim() == "")
                        {
                            Mandatory.Add("Enter Option A");
                        }
                        if (txtOptionB.Value.Trim() == "")
                        {
                            Mandatory.Add("Enter Option B");
                        }
                        if (txtOptionC.Value.Trim() == "")
                        {
                            Mandatory.Add("Enter Option C");
                        }
                        if (txtOptionD.Value.Trim() == "")
                        {
                            Mandatory.Add("Enter Option D");
                        }
                    }
                    else if (ddlNoofOptions.SelectedValue == "5")
                    {
                        if (txtOptionA.Value.Trim() == "")
                        {
                            Mandatory.Add("Enter Option A");
                        }
                        if (txtOptionB.Value.Trim() == "")
                        {
                            Mandatory.Add("Enter Option B");
                        }
                        if (txtOptionC.Value.Trim() == "")
                        {
                            Mandatory.Add("Enter Option C");
                        }
                        if (txtOptionD.Value.Trim() == "")
                        {
                            Mandatory.Add("Enter Option D");
                        }
                        if (txtOptionE.Value.Trim() == "")
                        {
                            Mandatory.Add("Enter Option E");
                        }
                    }
                    else
                    {
                        if (txtOptionA.Value.Trim() == "")
                        {
                            Mandatory.Add("Enter Option A");
                        }
                        if (txtOptionB.Value.Trim() == "")
                        {
                            Mandatory.Add("Enter Option B");
                        }
                    }

                    StringBuilder s = new StringBuilder();
                    foreach (string x in Mandatory)
                    {
                        s.Append(x);
                        s.Append(",");
                    }
                    lblMsg.Text = s.ToString();
                }
                else
                {
                    SubmitQuestionnaire(dtOptions);
                    BindGV(ddlDocuments.SelectedItem.Value);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "Q18:" + strline + "  " + strMsg, "error");
            }
        }
        public void SubmitQuestionnaire(DataTable dtOptions)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                //TMS_BusinessObjects.QuestionnaireObjects Qobj = new TMS_BusinessObjects.
                QuestionnaireObjects Questionobj = new QuestionnaireObjects();
                Questionobj.QueID = 0;
                if (btnCreate.Text == "Create")
                {
                    Questionobj.Operation = "Insert";
                }
                if (btnCreate.Text == "Update")
                {
                    Questionobj.Operation = "Update";
                    Questionobj.QueID = Convert.ToInt32(hdnEditQueID.Value);
                }
                Questionobj.DocID = Convert.ToInt32(ddlDocuments.SelectedValue);
                Questionobj.Question_Descriptipon = txtQuestion.Value.Trim();
                Questionobj.Question_Answer = ddlAnswer.SelectedItem.Text;
                Questionobj.createdOrModifiedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                Questionobj.CurrentStatus = ((int)AizantEnums.Questionnaire_Status.Questionnaire_Created).ToString();
                objTMS_BAL.Questionnaire(Questionobj, dtOptions, out int Result);
                //if (Result > 0)
                //{
                //    if (btnCreate.Text == "Create")
                //    {
                //        // ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Question Is Added ! ');", true);
                //        HelpClass.custAlertMsg(this, this.GetType(), "Questionnaire Added.", "success");
                //    }
                //    if (btnCreate.Text == "Update")
                //    {
                //        // ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Question Is Updated ! ');", true);
                //        HelpClass.custAlertMsg(this, this.GetType(), "Questionnaire Update.", "success");
                //    }
                //    LoadDefault();
                //}
                string msgType = "", msg = "";
                switch (Result)
                {
                    case 1:
                        msg = "Questionnaire Created Successfully!";
                        msgType = "success";
                        LoadDefault();
                        break;
                    case 2:
                        msg = "Failed To Create, Since There Is Already a Question Exist.";
                        msgType = "error";
                        break;
                    case 3:
                        msg = "Failed To Update, Since There Is Already a Question Exist.";
                        msgType = "error";
                        break;
                    case 4:
                        msg = "Failed To Update, Since Questionnaire Is Not In a Status Of Update.";
                        msgType = "info";
                        break;
                    case 5:
                        msg = "Questionnaire Updated Successfully!";
                        msgType = "success";
                        LoadDefault();
                        break;
                    case 6:
                        msg = "Failed to perform the Action, Since you are not a valid user.";
                        msgType = "warning";
                        break;
                    default:
                        msg = "Unable To Perform The Action, Contact Admin.";
                        msgType = "warning";
                        break;
                }
                HelpClass.custAlertMsg(this, this.GetType(), msg, msgType);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                //  HelpClass.showMsg(this, this.GetType(), "submit_Q:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Q19:" + strline + "  " + strMsg, "error");
            }
        }
        public void LoadDefault()
        {
            try
            {
                txtQuestion.Value = txtOptionA.Value = txtOptionB.Value = txtOptionC.Value = txtOptionD.Value = txtOptionE.Value = string.Empty;
                ddlNoofOptions.SelectedValue = "2";
                ddlAnswer.SelectedValue = "0";
                divremarks.Visible = false;
                divOptionC.Visible = false;
                divOptionD.Visible = false;
                divOptionE.Visible = false;
                //ddlAnswer.Items[3].Attributes["disabled"] = "disabled";
                //ddlAnswer.Items[4].Attributes["disabled"] = "disabled";
                //ddlAnswer.Items[5].Attributes["disabled"] = "disabled";
                ddlAnswer.Items.FindByValue("C").Enabled = false;
                ddlAnswer.Items.FindByValue("D").Enabled = false;
                ddlAnswer.Items.FindByValue("E").Enabled = false;
                if (btnCreate.Text == "Update")
                {
                    btnCreate.Text = "Create";
                }
                if (btnClear.Text == "Cancel")
                {
                    btnClear.Text = "Clear";
                }
                //hdnDeleteQuestion.Value = "";
                //hdnEditQueID.Value = "";
                lblMsg.Text = "";
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                //HelpClass.showMsg(this, this.GetType(), "AQ->LoadDefault:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Q20:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                LoadDefault();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                //HelpClass.showMsg(this, this.GetType(), "Q->btnClear_Click:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Q21:" + strline + "  " + strMsg, "error");
            }
        }

        protected void ddlDocuments_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int SelectedDocID = Convert.ToInt32(ddlDocuments.SelectedItem.Value);
                BindGV(SelectedDocID.ToString());
                LoadDefault();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "Q22:" + strline + "  " + strMsg, "error");
            }
        }
    }
}