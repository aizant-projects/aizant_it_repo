﻿using Aizant_Enums;
using AizantIT_PharmaApp.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.Administration.Questionnaire
{
    public partial class ApproveQuestionnaire : System.Web.UI.Page
    {
        UMS_BusinessLayer.UMS_BAL objUMS_BAL;
        TMS_BAL objTMS_BAL;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblMsg.Text = "";
                ConfirmCustomAlert.buttonClick += new EventHandler(ConfirmCustomAlert_buttonClick);
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID="+(int)Modules.TMS);
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0)
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }                   
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AQ1" + strline + "  " + strMsg, "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            try
            {
                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                divAutorizedMsg.Visible = true;
                divMainContainer.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AQ2" + strline + "  " + strMsg, "error");
            }
        }
        public void InitializeThePage()
        {
            try
            {
                bindDropDown();
                gvApproveQuestionnaire.DataSource = new List<object>();
                gvApproveQuestionnaire.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AQ3" + strline + "  " + strMsg, "error");
            }
        }
        protected void ConfirmCustomAlert_buttonClick(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "openModal();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AQ4" + strline + "  " + strMsg, "error");
            }
        }

        #region DropDownCode
        public void bindDropDown()
        {
            try
            {
                //if (Request.QueryString["Approval"] != null)
                //{
                bindApprovalQuestionnaireDepts((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                //}
                //else
                //{
                //}
                ddlDocuments.Enabled = false;
                ddlDocuments.Items.Clear();
                ddlDocuments.Items.Insert(0, new ListItem("-- Select Document --", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AQ5:" + strline + "  " + strMsg, "error");
            }
        }
        private void bindApprovalQuestionnaireDepts(string EmpID)
        {
            try
            {
                ddlDepartment.Items.Clear();
                objTMS_BAL = new TMS_BAL();
                DataSet ds = objTMS_BAL.getAccessibleAndToDoActionDepts(EmpID, "ApprovalQuestionnaire");
                DataTable dt = ds.Tables[0];
                ddlDepartment.DataSource = dt;
                ddlDepartment.DataTextField = "DepartmentName";
                ddlDepartment.DataValueField = "DeptID";
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("-- Select Department --", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AQ6:" + strline + "  " + strMsg, "error");
            }
        }
        public void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlDocuments.Enabled = false;
                ddlDocuments.Items.Clear();
                int SelectedDepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue);
                if (SelectedDepartmentID > 0)
                {
                    bindDocuments(SelectedDepartmentID.ToString());
                }
                else
                {
                    ddlDocuments.Items.Insert(0, new ListItem("-- Select Document --", "0"));
                    ddlDocuments.Enabled = false;
                }
                BindGV("0");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AQ6:" + strline + "  " + strMsg, "error");
            }
        }

        public void bindDocuments(string DeptID)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                // DataTable dt = objTMS_BAL.GetDeptWiseSOPs(DeptID);
                //DataTable dt = objTMS_BAL.GetActiveSops(DeptID);
                DataTable dt = objTMS_BAL.GetDeptWiseDocsForQuestionnaireApprovalOrRevert(DeptID, (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString(), "ApprovalQuestionnaire", "0");
                ddlDocuments.Items.Clear();
                if (dt.Rows.Count > 0)
                {
                    ddlDocuments.DataSource = dt;
                    ddlDocuments.DataTextField = "DocumentName";
                    ddlDocuments.DataValueField = "DocumentID";
                    ddlDocuments.DataBind();
                }
                else
                {
                    bindApprovalQuestionnaireDepts((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                }
                ddlDocuments.Items.Insert(0, new ListItem("-- Select Document --", "0"));
                ddlDocuments.Enabled = true;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AQ7:" + strline + "  " + strMsg, "error");
            }
        }       
        #endregion

        #region GridViewCode
        public void BindGV(string DocID)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                DataSet dsQuestonnaireDetails = objTMS_BAL.gvDocQuestionnaireApproval(DocID, ((int)AizantEnums.Questionnaire_Status.Questionnaire_Created).ToString(), Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                DataTable dtDocumentType = dsQuestonnaireDetails.Tables[0];
                DataTable dtQuestonnaire = dsQuestonnaireDetails.Tables[1];
                if (dtDocumentType.Rows.Count > 0)
                {
                    // DataRow drDocumentType = dtDocumentType.Rows[0];
                    txtDocumentType.Value = dtDocumentType.Rows[0]["DocumentType"].ToString();
                }
                else
                {
                    txtDocumentType.Value = string.Empty;
                }
                if (dtQuestonnaire.Rows.Count >= 0)
                {
                    gvApproveQuestionnaire.DataSource = dtQuestonnaire;
                    gvApproveQuestionnaire.DataBind();
                }
                if (dtQuestonnaire.Rows.Count > 0)
                {
                    divBtnSubmit.Visible = true;
                    upBtnSubmit.Update();
                }
                else
                {
                    bindDocuments(ddlDepartment.SelectedValue);
                    divBtnSubmit.Visible = false;
                    upBtnSubmit.Update();
                    txtDocumentType.Value = string.Empty;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AQ8:" + strline + "  " + strMsg, "error");
            }
        }
        protected void gvApproveQuestionnaire_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                if (e.CommandName == "RevertButton")
                {
                    hdnRevertQueID.Value = e.CommandArgument.ToString();
                    txtRevertReason.Value = lblmodalMsg.Text = string.Empty;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "OnConfirmRevert();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AQ10:" + strline + "  " + strMsg, "error");
            }
        }
        protected void gvApproveQuestionnaire_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    objTMS_BAL = new TMS_BAL();
                    string QuestionID = gvApproveQuestionnaire.DataKeys[e.Row.RowIndex].Value.ToString();
                    GridView gvOptions = e.Row.FindControl("gvOptions") as GridView;
                    //gvOptions.DataSource = GetData(string.Format("select top 3 * from Orders where CustomerId='{0}'", QuestionID));
                    DataSet ds = objTMS_BAL.GetQuestionnairetoEdit(Convert.ToInt32(QuestionID));
                    DataTable dtOptions = ds.Tables[1];
                    gvOptions.DataSource = dtOptions;
                    gvOptions.DataBind();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AQ11:" + strline + "  " + strMsg, "error");
            }
        }
        #endregion
        
        #region ButtonsCode
        protected void btnRevert_Click(object sender, EventArgs e)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                if (txtRevertReason.Value.Trim() == "")
                {
                    lblmodalMsg.Text = "Enter Revert Reason";
                }
                else
                {
                    int QueID = Convert.ToInt32(hdnRevertQueID.Value);
                    int s = objTMS_BAL.RevertQuestionnaire(QueID, txtRevertReason.Value.Trim(), Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]), ((int)AizantEnums.Questionnaire_Status.Questionnaire_Reverted), out int Result);
                    if (Result == 0)
                    {
                        // ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Failed to Revert ! Action already performed by another user.');", true);
                        HelpClass.custAlertMsg(this, this.GetType(), "Failed to Revert ! Action already performed by another user.", "info");
                    }
                    else
                    {
                        if (s > 0)
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Question Is Reverted ! ", "success");
                            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "$('#RevertRemarks').modal('hide');", true);
                        }
                        else
                        {
                            // ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Failed to Revert this Question ! ');", true);
                            HelpClass.custAlertMsg(this, this.GetType(), "Failed to Revert this Question ! ", "error");
                        }
                    }
                    BindGV(ddlDocuments.SelectedItem.Value);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AQ12:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                int Empid = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);

                DataTable dtQuestionIDs = new DataTable();
                dtQuestionIDs.Columns.Add("QuestionsIDs");
                for (int j = 0; j < gvApproveQuestionnaire.Rows.Count; j++)
                {
                    DataRow dr = dtQuestionIDs.NewRow();
                    dr["QuestionsIDs"] = gvApproveQuestionnaire.DataKeys[j].Value.ToString();
                    dtQuestionIDs.Rows.Add(dr);
                }
                int s = objTMS_BAL.AcceptQuestionnaire(ddlDocuments.SelectedValue, Empid, ((int)AizantEnums.Questionnaire_Status.Questionnaire_Approved).ToString(), dtQuestionIDs);
                if (s > 0)
                {
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Questionnaires Accepted ! ');", true);
                    HelpClass.custAlertMsg(this, this.GetType(), "Questionnaire Approved ! ", "success");
                }
                else
                {
                    // ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Failed to Accept Questionnaires ! Action is Already Performed. ');", true);
                    HelpClass.custAlertMsg(this, this.GetType(), "Failed to Accept Questionnaire ! Action is Already Performed. ", "info");
                }
                BindGV(ddlDocuments.SelectedItem.Value);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AQ13:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                txtRevertReason.Value = lblmodalMsg.Text = string.Empty;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "$('#RevertRemarks').modal('hide');", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AQ14:" + strline + "  " + strMsg, "error");
            }
        }
        #endregion
        //Not in Use
        public void bindAllDepartment(int EmpID)
        {
            try
            {
                objUMS_BAL = new UMS_BusinessLayer.UMS_BAL();
                int[] RoleIDs = new int[] { (int)TMS_UserRole.HOD_TMS };
                DataTable dt = objUMS_BAL.getRoleWiseDepts(EmpID, RoleIDs);
                ddlDepartment.DataSource = dt;
                ddlDepartment.DataTextField = "DepartmentName";
                ddlDepartment.DataValueField = "DeptID";
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("-- Select Department --", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AQ6:" + strline + "  " + strMsg, "error");
            }
        }

        protected void ddlDocuments_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int SelectedDocID = Convert.ToInt32(ddlDocuments.SelectedItem.Value);
                BindGV(SelectedDocID.ToString());
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "AQ9:" + strline + "  " + strMsg, "error");
            }
        }
    }
}