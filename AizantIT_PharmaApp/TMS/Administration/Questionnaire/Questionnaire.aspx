﻿<%@ Page Title="Questionnaire" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="Questionnaire.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Administration.Questionnaire.Questionnaire" %>

<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <style>
        a.tabstyle:hover {
            color: #160e1e !important;
        }

        .label_Questionnaire {
            padding-right: 0px;
            width: 21%;
        }

        .Questionnaire_fieldSet {
            border-color: #1c2d5b;
            border-width: 1px;
            border-style: solid;
            padding: 10px;
        }

        .Questionnaire_legend {
            margin-bottom: 2px;
            color: #757171;
            font-weight: bold;
            border-bottom: 1px solid #fffbfb;
        }

        .gvOptions {
            margin-bottom: 0px;
        }
    </style>

    <asp:UpdatePanel ID="upHiddenFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnEditQueID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnDeleteQueID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnDeleteInactive" runat="server" Value="0" />
            <asp:HiddenField ID="hdnMaintainTab" runat="server" Value="ApprovalpendingTab" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="col-lg-12 col-sm-12 float-left col-12 col-md-12 grid_panel_full padding-none">
        <div class="col-lg-12 col-sm-12 float-left col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div>
                <div class="panel panel-default" id="mainContainer" runat="server" style="padding-bottom: 10px">
                    <div class="grid_header col-lg-12 col-12 col-md-12 col-sm-12 float-left">Questionnaire Master</div>
                    <div>
                        <div class=" col-lg-4 col-12 col-md-4 col-sm-12 float-left padding-none">
                            <div class="form-horizontal  padding-none col-lg-12 col-sm-12 float-left col-12 col-md-12 bottom">
                                <asp:UpdatePanel ID="upDropdowns" runat="server" UpdateMode="Always">
                                    <ContentTemplate>
                                        <div class="padding-none col-lg-7  col-12 col-md-12 col-sm-12 float-left ">
                                            <label class="control-label col-12">Department<span class="mandatoryStar">*</span></label>
                                            <div class="col-lg-12  col-12 col-md-12 col-sm-12 float-left">
                                                <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="selectpicker regulatory_dropdown_style form-control"
                                                    data-live-search="true" data-size="7" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="padding-none col-lg-5  col-12 col-md-12 col-sm-12 float-left ">
                                            <label class="control-label col-12">Document Type</label>
                                            <div class="col-lg-12  col-12 col-md-12 col-sm-12 float-left">
                                                <asp:DropDownList ID="ddlDocumentType" runat="server" CssClass="selectpicker regulatory_dropdown_style form-control"
                                                    data-live-search="true" data-size="7" OnSelectedIndexChanged="ddlDocumentType_SelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-12  col-12 col-md-12 col-sm-12 float-left ">
                                            <label class="control-label col-12 col-md-12 col-sm-12 float-left padding-none">Document<span class="mandatoryStar">*</span></label>
                                            <div class="col-lg-12  col-12 col-md-12 col-sm-12 float-left padding-none">
                                                <asp:DropDownList ID="ddlDocuments" runat="server" CssClass="selectpicker regulatory_dropdown_style form-control"
                                                    data-live-search="true" data-size="7" OnSelectedIndexChanged="ddlDocuments_SelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="col-lg-12 col-sm-12 float-left col-12 col-md-12 float-left padding-left_div ">
                                <div class=" col-lg-12 col-sm-12 float-left col-12 col-md-12  grid_panel_full padding-none bottom">
                                    <div class="grid_header col-lg-12  col-12 col-md-12 col-sm-12 float-left ">Questionnaire</div>
                                    <asp:UpdatePanel ID="upQuestionnaire" runat="server" UpdateMode="Always">
                                        <ContentTemplate>
                                            <div class="col-lg-12 col-sm-12 float-left col-12 col-md-12 padding-none">
                                                <div class="form-horizontal">
                                                    <div class="form-group padding-none">
                                                        <label for="Question" class="col-sm-12 float-left control-label label_Questionnaire">Question<span class="mandatoryStar">*</span></label>
                                                        <div class="col-sm-12 float-left">
                                                            <textarea class="form-control " id="txtQuestion" rows="10" runat="server" placeholder="Enter Question" aria-multiline="true" title="Question"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group padding-none">

                                                        <div class="col-sm-12 float-left padding-none ">
                                                            <label for="name" class="col-sm-12 float-left control-label">No. Of Options</label>
                                                        </div>
                                                        <div class="col-sm-12 float-left">
                                                            <asp:DropDownList ID="ddlNoofOptions" CssClass="form-control selectpicker regulatory_dropdown_style" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlNoofOptions_SelectedIndexChanged" ToolTip="-- Select No Of Options--">
                                                                <asp:ListItem Text="B" Value="2">2</asp:ListItem>
                                                                <asp:ListItem Text="c" Value="3">3</asp:ListItem>
                                                                <asp:ListItem Text="D" Value="4">4</asp:ListItem>
                                                                <asp:ListItem Text="E" Value="5">5</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="form-group padding-none">
                                                        <label for="Options" class="col-sm-3 control-label label_Questionnaire">Options<span class="mandatoryStar">*</span></label>
                                                        <div class="col-sm-12 float-left" id="divoptions">
                                                            <div class="col-lg-12 col-sm-12 float-left col-12 col-md-12 grid_panel_full">
                                                                <div class="form-group padding-none col-lg-12 col-sm-12 float-left col-12 col-md-12">
                                                                    <label class="col-sm-1 float-left control-label Options_label">A<span class="mandatoryStar">*</span></label>
                                                                    <div class="col-sm-11 float-left Options_Textbox">
                                                                        <input type="text" class="form-control login_input_sign_up option" maxlength="200" runat="server" name="name" id="txtOptionA" placeholder="Enter First Option" title="Option A" />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group padding-none col-lg-12 col-sm-12 float-left col-12 col-md-12">
                                                                    <label class="col-sm-1 float-left control-label Options_label">B<span class="mandatoryStar">*</span></label>
                                                                    <div class="col-sm-11 float-left Options_Textbox">
                                                                        <input type="text" class="form-control login_input_sign_up option" maxlength="200" runat="server" name="name" id="txtOptionB" placeholder="Enter Second Option" title="Option B" />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group padding-none col-lg-12 col-sm-12 float-left col-12 col-md-12" id="divOptionC" runat="server" visible="false">
                                                                    <label class="col-sm-1 float-left control-label Options_label">C<span class="mandatoryStar">*</span></label>
                                                                    <div class="col-sm-11 float-left Options_Textbox">
                                                                        <input type="text" class="form-control login_input_sign_up option" maxlength="200" runat="server" name="name" id="txtOptionC" placeholder="Enter Third Option" title="Option C" />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group padding-none col-lg-12 col-sm-12 float-left col-12 col-md-12" id="divOptionD" runat="server" visible="false">
                                                                    <label class="col-sm-1 float-left control-label Options_label">D<span class="mandatoryStar">*</span></label>
                                                                    <div class="col-sm-11 float-left Options_Textbox">
                                                                        <input type="text" class="form-control login_input_sign_up option" maxlength="200" runat="server" name="name" id="txtOptionD" placeholder="Enter Fourth Option" title="Option D" />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group padding-none col-lg-12 col-sm-12 float-left col-12 col-md-12" id="divOptionE" runat="server" visible="false">
                                                                    <label class="col-sm-1 float-left control-label Options_label">E<span class="mandatoryStar">*</span></label>
                                                                    <div class="col-sm-11 float-left Options_Textbox">
                                                                        <input type="text" class="form-control login_input_sign_up option" maxlength="200" runat="server" name="name" id="txtOptionE" placeholder="Enter Fifth Option" title="Option E" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group padding-none col-lg-12 col-sm-12 float-left col-12 col-md-12">
                                                        <label for="name" class="col-12 control-label label_Questionnaire">Answer<span class="mandatoryStar">*</span></label>
                                                        <div class="col-lg-12 col-sm-12 float-left col-12 col-md-12">
                                                            <asp:DropDownList ID="ddlAnswer" CssClass="form-control selectpicker regulatory_dropdown_style" runat="server" ToolTip="-- Select Answer --">
                                                                <asp:ListItem Text="o" Value="0">-- Select Answer --</asp:ListItem>
                                                                <asp:ListItem Text="A" Value="A">A</asp:ListItem>
                                                                <asp:ListItem Text="B" Value="B">B</asp:ListItem>
                                                                <asp:ListItem Text="c" Value="C">C</asp:ListItem>
                                                                <asp:ListItem Text="D" Value="D">D</asp:ListItem>
                                                                <asp:ListItem Text="E" Value="E">E</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-horizontal col-lg-12 col-sm-12 float-left col-12 col-md-12">
                                                <div class="form-group padding-none" runat="server" id="divremarks" visible="false">
                                                    <label for="Remarks" class="col-sm-3 padding-none control-label label_Questionnaire">Remarks</label>
                                                    <div class="col-sm-12 padding-none">
                                                        <textarea class="form-control" id="txtDeclinedReason" style="height: 100px" cols="12" runat="server" placeholder="Enter Declined Reason" maxlength="250" aria-multiline="true" title="Reverted Reason" disabled="disabled"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 float-right text-right bottom">
                                                <asp:Button ID="btnCreate" CssClass=" btn-signup_popup" runat="server" Text="Create" ToolTip="Click to Create" OnClick="btnCreate_Click" OnClientClick="javascript:return QuestionnaireRequired();" />
                                                <asp:Button ID="btnClear" CssClass=" btn-revert_popup" runat="server" ToolTip="Click to Clear" Text="Clear" OnClick="btnClear_Click" />
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-8 col-12 col-md-8 col-sm-12 float-left top  padding-none">
                            <div class="col-lg-12 col-sm-12 float-left col-12 col-md-12 bottom padding-none">
                                <div id="Question_List" runat="server" class="col-lg-12 col-sm-12 float-left col-12 col-md-12">
                                    <div class="grid_header col-lg-12  col-12 col-md-12 col-sm-12 float-left ">Questionnaire List</div>
                                    <div class="col-lg-12 col-sm-12 float-left col-12 col-md-12  grid_panel_full">
                                        <asp:UpdatePanel ID="upTabs" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div id="Tabs" role="tabpanel">
                                                    <ul class="nav nav-tabs top tab_grid" id="ultabActive">
                                                        <li runat="server" id="liGvApprovalPending" class="ApprovalpendingTab nav-item  "><a data-toggle="tab" href="#tabgvApprovalPending" class="tabstyle nav-link active" title="Approval Pending Questionnaires">Approval Pending
                                                            <sup>
                                                                <asp:Label ID="lblApprovalPendingCount" runat="server"></asp:Label></sup>
                                                        </a></li>
                                                        <li runat="server" id="liGvReverted" class="nav-item  RevertedTab"><a data-toggle="tab" href="#tabgvReverted" class="tabstyle nav-link " title="Reverted Questionnaires">Reverted
                                                            <sup>
                                                                <asp:Label ID="lblRevertedCount" runat="server"></asp:Label></sup>
                                                        </a></li>
                                                        <li runat="server" id="liGvApproved" class="nav-item  ApprovedTab"><a data-toggle="tab" href="#tabgvApproved" class="tabstyle nav-link " title="Approved Questionnaires">Approved
                                                            <sup>
                                                                <asp:Label ID="lblApprovedCount" runat="server"></asp:Label></sup>
                                                        </a></li>
                                                    </ul>

                                                    <div class="tab-content">
                                                        <div id="tabgvApprovalPending" class="ApprovalPendingTabBody tab-pane fade show active">
                                                            <div class=" col-lg-12 col-sm-12 float-left col-12 col-md-12 float-left padding-none" style="max-height: 406px; overflow-y: auto;">
                                                                <div class=" col-lg-12 col-sm-12 float-left col-12 col-md-12 divGV_Styles top">
                                                                    <asp:UpdatePanel ID="upgvQuestionnaire" runat="server" UpdateMode="Always">
                                                                        <ContentTemplate>
                                                                            <asp:GridView ID="gvQuestionnaire" OnRowCommand="gvQuestionnaire_RowCommand" OnRowDataBound="gvQuestionnaire_RowDataBound" DataKeyNames="QuestionID"
                                                                                runat="server" AutoGenerateColumns="False" CssClass="table question_list table-striped table-bordered AspGrid"
                                                                                EmptyDataText="No Questionnaires" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle question_none">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Question ID" SortExpression="QueID" Visible="false">
                                                                                        <EditItemTemplate>
                                                                                            <asp:TextBox ID="txtGVQueID" runat="server" Text='<%# Bind("QuestionID") %>'></asp:TextBox>
                                                                                        </EditItemTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblGVQueID" runat="server" Text='<%# Bind("QuestionID") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Questionnaire" SortExpression="Question">
                                                                                        <ItemTemplate>
                                                                                            <table class="table table-bordered">
                                                                                                <tr>
                                                                                                    <td style="color: #2e2e2e; background: #fff; font-size: 12pt; font-weight: bold;">
                                                                                                        <asp:Label ID="lblGVQuestion" runat="server" Text='<%# Bind("QuestionTitle") %>' Style="text-decoration-style: solid;"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="padding: 0px;">
                                                                                                        <asp:GridView ID="gvOptions" runat="server" AutoGenerateColumns="False" CssClass="table  question_list table-striped table-bordered gvOptions" ShowHeader="false">
                                                                                                            <Columns>
                                                                                                                <asp:BoundField DataField="Option" HeaderText="Option" ItemStyle-BackColor="#07889a" ItemStyle-ForeColor="#ffffff" ItemStyle-Width="1px" />
                                                                                                                <asp:BoundField DataField="OptionTitle" HeaderText="Value" />
                                                                                                            </Columns>
                                                                                                        </asp:GridView>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblGVAnswer" runat="server" Text='<%#"Answer : " + Eval("Answer")%>'></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <%-- <th>
                                                         Edit  :  <asp:LinkButton ID="lnkEdit" CssClass="glyphicon glyphicon-edit GV_Edit_Icon" runat="server" CommandName="EditButton" CommandArgument='<%# Eval("QuestionID") %>'  ToolTip="Click To Edit" Style="top:5px;"></asp:LinkButton>
                                                         Delete  :  <asp:LinkButton ID="lnkDelete"  class="glyphicon glyphicon-trash GV_Edit_Icon" runat="server" CommandName="DeleteButton" CommandArgument='<%# Eval("QuestionID") %>' OnClientClick="javascript:return OnConfirmDelete();"  ToolTip="Click To Delete" Style="top:5px;"></asp:LinkButton>
                                                     </th>--%>
                                                                                                    <th style="background: transparent !important">
                                                                                                        <asp:LinkButton ID="lnkEdit" CssClass="float-left" runat="server" CommandName="EditButton" CommandArgument='<%# Eval("QuestionID") %>' ToolTip="Click To Edit">
                                                                                                <table>
                                                                                                    <tr>
                                                                                                        <td><span class=" btn-signup_popup">Edit</span> </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                                        </asp:LinkButton>
                                                                                                        <asp:LinkButton ID="lnkDelete" CssClass="float-left" runat="server" CommandName="DeleteButton" CommandArgument='<%# Eval("QuestionID") %>' ToolTip="Click To Delete">
                                                                                                <table>
                                                                                                    <tr>
                                                                                                        <td><span class=" btn-cancel_popup">Delete</span> </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                                        </asp:LinkButton>
                                                                                                    </th>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="tabgvReverted" class="RevertedTabBody tab-pane fade">
                                                            <div>
                                                                <div class=" col-lg-12 col-sm-12 float-left col-12 col-md-12 divGV_Styles top" style="max-height: 406px; overflow-y: auto;">
                                                                    <asp:UpdatePanel ID="upgvRevertQuestionnaire" runat="server" UpdateMode="Always">
                                                                        <ContentTemplate>
                                                                            <asp:GridView ID="gvRevertedQuestionnaire" OnRowCommand="gvRevertedQuestionnaire_RowCommand" OnRowDataBound="gvRevertedQuestionnaire_RowDataBound" DataKeyNames="QuestionID"
                                                                                runat="server" AutoGenerateColumns="False" CssClass="table question_list table-striped table-bordered AspGrid"
                                                                                EmptyDataText="No Questionnaires" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle question_none">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Question ID" SortExpression="QueID" Visible="false">
                                                                                        <EditItemTemplate>
                                                                                            <asp:TextBox ID="txtGVQueID" runat="server" Text='<%# Bind("QuestionID") %>'></asp:TextBox>
                                                                                        </EditItemTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblGVQueID" runat="server" Text='<%# Bind("QuestionID") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Questionnaire" SortExpression="Question">
                                                                                        <ItemTemplate>
                                                                                            <table class="table table-bordered">
                                                                                                <tr>
                                                                                                    <td style="color: #2e2e2e; background: #c0d4d7; font-size: 12pt; font-weight: bold;">
                                                                                                        <asp:Label ID="lblGVQuestion" runat="server" Text='<%# Bind("QuestionTitle") %>'></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="padding: 0px;">
                                                                                                        <asp:GridView ID="gvOptions" runat="server" AutoGenerateColumns="False" CssClass="table question_list table-striped table-bordered gvOptions" ShowHeader="false">
                                                                                                            <Columns>
                                                                                                                <asp:BoundField DataField="Option" HeaderText="Option" ItemStyle-BackColor="#07889a" ItemStyle-ForeColor="#ffffff" ItemStyle-Width="1px" />
                                                                                                                <asp:BoundField DataField="OptionTitle" HeaderText="Value" />
                                                                                                            </Columns>
                                                                                                        </asp:GridView>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblGVAnswer" runat="server" Text='<%#"Answer : " + Eval("Answer")%>'></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="" SortExpression="Description" ItemStyle-Width="40%">
                                                                                        <ItemTemplate>
                                                                                            <table class="table table-bordered">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblGVRevertedBy" runat="server" Text='<%#"Reverted By : " + Eval("ReverterName")%>'></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblGVRevertedDate" runat="server" Text='<%#"Reverted Date : " + Eval("RevertedDate")%>'></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblGVRevertedReason" runat="server" Text='<%#"Reverted Reason : " + Eval("RevertReason")%>'></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <%-- <th>
                                                         Edit  :  <asp:LinkButton ID="lnkEdit" CssClass="glyphicon glyphicon-edit GV_Edit_Icon" runat="server" CommandName="EditButton" CommandArgument='<%# Eval("QuestionID") %>'  ToolTip="Click To Edit" Style="top:5px;"></asp:LinkButton>
                                                         Delete  :  <asp:LinkButton ID="lnkDelete"  class="glyphicon glyphicon-trash GV_Edit_Icon" runat="server" CommandName="DeleteButton" CommandArgument='<%# Eval("QuestionID") %>' OnClientClick="javascript:return OnConfirmDelete();"  ToolTip="Click To Delete" Style="top:5px;"></asp:LinkButton>
                                                     </th>--%>
                                                                                                    <th style="background: transparent !important">
                                                                                                        <asp:LinkButton ID="lnkEdit" CssClass="float-left" Text="Edit" runat="server" CommandName="EditButton" CommandArgument='<%# Eval("QuestionID") %>' ToolTip="Click To Edit">
                                                                                                 <table>
                                                                                                    <tr>
                                                                                                        <td><span class="btn-signup_popup">Edit</span> </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                                        </asp:LinkButton>
                                                                                                        <asp:LinkButton ID="lnkDelete" CssClass="float-left" Text="Delete" runat="server" CommandName="DeleteButton" CommandArgument='<%# Eval("QuestionID") %>' ToolTip="Click To Delete">
                                                                                                 <table>
                                                                                                    <tr>
                                                                                                        <td><span class="btn-cancel_popup">Delete</span> </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                                        </asp:LinkButton>
                                                                                                    </th>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="tabgvApproved" class="ApprovedTabBody tab-pane fade">
                                                            <div>
                                                                <div class=" col-lg-12 col-sm-12 float-left col-12 col-md-12 divGV_Styles top" style="max-height: 406px; overflow-y: auto;">
                                                                    <asp:UpdatePanel ID="upgvApprovedQuestionnaire" runat="server" UpdateMode="Always">
                                                                        <ContentTemplate>
                                                                            <asp:GridView ID="gvApprovedQuestionnaire" OnRowCommand="gvApprovedQuestionnaire_RowCommand" OnRowDataBound="gvApprovedQuestionnaire_RowDataBound"
                                                                                DataKeyNames="QuestionID" runat="server" AutoGenerateColumns="False" CssClass="table question_list table-striped table-bordered AspGrid"
                                                                                EmptyDataText="No Questionnaires" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle question_none">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Question ID" SortExpression="QueID" Visible="false">
                                                                                        <EditItemTemplate>
                                                                                            <asp:TextBox ID="txtGVQueID" runat="server" Text='<%# Bind("QuestionID") %>'></asp:TextBox>
                                                                                        </EditItemTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblGVQueID" runat="server" Text='<%# Bind("QuestionID") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Questionnaire" SortExpression="Question">
                                                                                        <ItemTemplate>
                                                                                            <table class="table table-bordered">
                                                                                                <tr>
                                                                                                    <td style="color: #2e2e2e; background: #c0d4d7; font-size: 12pt; font-weight: bold;">
                                                                                                        <asp:Label ID="lblGVQuestion" runat="server" Text='<%# Bind("QuestionTitle") %>'></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="padding: 0px;">
                                                                                                        <asp:GridView ID="gvOptions" runat="server" AutoGenerateColumns="False" CssClass="table question_list table-striped table-bordered gvOptions" ShowHeader="false">
                                                                                                            <Columns>
                                                                                                                <asp:BoundField DataField="Option" HeaderText="Option" ItemStyle-BackColor="#07889a" ItemStyle-ForeColor="#ffffff" ItemStyle-Width="1px" />
                                                                                                                <asp:BoundField DataField="OptionTitle" HeaderText="Value" />
                                                                                                            </Columns>
                                                                                                        </asp:GridView>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblGVAnswer" runat="server" Text='<%#"Answer : " + Eval("Answer")%>'></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="" SortExpression="Description" ItemStyle-Width="40%">
                                                                                        <ItemTemplate>
                                                                                            <table class="table table-bordered">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="Label1" runat="server" Text='<%#"Created By : " + Eval("CreatedBy")%>'></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="Label2" runat="server" Text='<%#"Created Date : " + Eval("CreatedDate")%>'></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblGVRevertedBy" runat="server" Text='<%#"Approved By : " + Eval("ApprovalName")%>'></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblGVRevertedDate" runat="server" Text='<%#"Approved Date : " + Eval("ApprovalDate")%>'></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th style="background: transparent !important">
                                                                                                        <%--<asp:LinkButton ID="lnkInActive" CssClass=" btn btn-canceldms_popup" Text="In Active" runat="server" CommandName="InactiveButton" CommandArgument='<%# Eval("QuestionID") %>' OnClientClick="javascript:return OnConfirmInActive();" ToolTip="Click To In-Active">--%>
                                                                                                        <asp:LinkButton ID="lnkInActive" CssClass=" btn btn-cancel_popup" Text="In Active" runat="server" CommandName="InactiveButton" CommandArgument='<%# Eval("QuestionID") %>' ToolTip="Click To In-Active">
                                                                                                        </asp:LinkButton>
                                                                                                    </th>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-sm-12 float-left col-12 col-md-12">
                            <asp:UpdatePanel ID="uplblMsg" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <div class="Panel_Footer_Message_Label" style="padding-left: 2%; padding-right: 2%;">
                                        <asp:Label ID="lblMsg" runat="server" Text="" ForeColor="Red"></asp:Label>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />
    <script>
        function OnConfirmDelete() {
            custAlertMsg('Do You Want To Delete This Question ?', 'confirm', true);
        }
        function OnConfirmInActive() {
            custAlertMsg('Do You Want To In-Active This Question ?', 'confirm', true);
        }
        function CloseModal() {
            $('#ModalConfirm').modal('hide');
        }
    </script>
    <script>
        $(document).ready(function () {
            MainTabState();
            TabClick();
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            MainTabState();
            TabClick();
        });

        function MainTabState() {
            var _TabState = $('#<%=hdnMaintainTab.ClientID%>').val();
            var _ActiveTab = "", _ActiveTabBody = "";           
            switch (_TabState) {
                case "ApprovalpendingTab":
                    _ActiveTab = ".ApprovalpendingTab a";
                    _ActiveTabBody = ".ApprovalPendingTabBody";
                    break;
                case "RevertedTab":
                    _ActiveTab = ".RevertedTab a";
                    _ActiveTabBody = ".RevertedTabBody";
                    break;
                default:
                    _ActiveTab = ".ApprovedTab a";
                    _ActiveTabBody = ".ApprovedTabBody";
            }

            $('.ApprovedTabBody,.RevertedTabBody,.ApprovalPendingTabBody').removeClass('show active');
            $('.RevertedTab a,.ApprovedTab a,.ApprovalpendingTab a').removeClass('active show');
            $(_ActiveTab).addClass('active show');
            $(_ActiveTabBody).addClass('show active');
        }
    </script>
    <script>
        function TabClick() {
            $(".ApprovedTab").click(function () {
                $('#<%=hdnMaintainTab.ClientID%>').val('ApprovedTab');
            });
            $(".RevertedTab").click(function () {
                $('#<%=hdnMaintainTab.ClientID%>').val('RevertedTab');
            });
            $(".ApprovalpendingTab").click(function () {
                $('#<%=hdnMaintainTab.ClientID%>').val('ApprovalpendingTab');
            });
        }
    </script>
    <script>
        //for mandatory fields 
        function QuestionnaireRequired() {
            errors = [];
            if ($('#<%=ddlDepartment.ClientID%>').val() == 0) {
                errors.push("Select Department");
            }
            if ($('#<%=ddlDocuments.ClientID%>').val() == 0) {
                errors.push("Select Document");
            }
            if ($("#<%=txtQuestion.ClientID%>").val().trim() == "") {
                errors.push("Enter Question");
            }
            var OptionValue;
            var AryOptons = ["A", "B", "C", "D","E"];
            var GivenOption=[];
            for (var i = 0; i < $('#<%=ddlNoofOptions.ClientID%>').val(); i++) {
                var _OptionElement = $('input.option[type=text]').get(i);
                if ($(_OptionElement).val().trim()=="")
                {
                    errors.push("Enter Option " + AryOptons[i]);
                }
                else {
                    GivenOption.push($(_OptionElement).val().trim());
                }
            }
            if (GivenOption.length>=2) {
                var uniqueAry = GetUniqueArray(GivenOption);
                if (uniqueAry.length != GivenOption.length) {
                    errors.push("Options should be Unique");
                }
            }

            if ($('#<%=ddlAnswer.ClientID%>').val() == 0) {
                errors.push("Select Answer");
            }

            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                ShowPleaseWait('show');
                return true;
            }
        }

    </script>
</asp:Content>
