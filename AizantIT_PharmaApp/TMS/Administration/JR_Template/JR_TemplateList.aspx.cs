﻿using AizantIT_PharmaApp.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BusinessLayer;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;
using static UMS_BusinessLayer.UMS_BAL;

namespace AizantIT_PharmaApp.TMS.Administration.JR_Template
{
    public partial class JR_TemplateList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {                    
                    if (!IsPostBack)
                    {
                        hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID="+(int)Modules.TMS);
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0)
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }                   
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }

            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_TL-1:" + strline + "  " + strMsg, "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            try
            {
                lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                divAutorizedMsg.Visible = true;
                divMainContainer.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_TL-2:" + strline + "  " + strMsg, "error");
            }

        }
        private void InitializeThePage()
        {
            try
            {

            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_TL-3:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnEditJrTemplateList_Click(object sender, EventArgs e)
        {
            try
            {
                int JRTemplateID = Convert.ToInt32(hdnJRTemplateID.Value);
                Response.Redirect("~/TMS/Administration/JR_Template/JR_Template.aspx?TempLateID=" + JRTemplateID);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_TL-4:" + strline + "  " + strMsg, "error");
            }
        }
    }
}