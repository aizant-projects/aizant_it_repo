﻿using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.Administration.JR_Template
{
    public partial class JR_Template : System.Web.UI.Page
    {
        UMS_BusinessLayer.UMS_BAL objUMS_BAL;
        TMS_BAL objTMS_BAL;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HelpClass.IsUserAuthenticated())
            {
                if (!IsPostBack)
                {
                    DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                    DataTable dtTemp = new DataTable();
                    DataRow[] drTMS = dt.Select("ModuleID=" + (int)Modules.TMS);
                    if (drTMS.Length > 0)
                    {
                        dtTemp = drTMS.CopyToDataTable();
                        if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0)
                        {
                            InitializeThePage();
                        }
                        else
                        {
                            lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
                            divAutorizedMsg.Visible = true;
                            divMainContainer.Visible = false;
                        }
                    }
                    else
                    {
                        Response.Redirect("~/Dashboards/Home.aspx", false);
                    }
                }
            }
            else
            {
                Response.Redirect("~/UserLogin.aspx", false);
            }
            lblMsg.Text = "";
        }

        private void InitializeThePage()
        {

            BindDepartments_Create(Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));

            if (Request.QueryString["TempLateID"] != null)
            {
                lblTitle.Text = "Modify JR Template";
                btnCancel.Text = "Cancel";
                btnCancel.CssClass = "btn-cancel_popup";
                btnSubmit.Text = "Update";
                GetTemplateDetails(Request.QueryString["TempLateID"].ToString());
                ddlDepartments.Enabled = false;
            }
            else
            {

                lblTitle.Text = "Create JR Template";
                divJR_txtField.Visible = true;
            }
            upDepartment.Update();
        }
        public void GetTemplateDetails(string TempID)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                DataTable dt = objTMS_BAL.getTemplateDetails(TempID);
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    txtJR_TemplateName.Value = dr["JR_TemplateName"].ToString();
                    ddlDepartments.Items.FindByValue(dr["DeptID"].ToString()).Selected = true;
                    //txtJR.InnerText = dr["JR_Description"].ToString();
                    JRDescription.Html = dr["JR_Description"].ToString();
                }

            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_Temp->1:" + strline + "  " + strMsg, "error");

            }
        }

        //AccessibleDepartments of an Login Hod
        public void BindDepartments_Create(int EmpID)
        {
            try
            {
                objUMS_BAL = new UMS_BusinessLayer.UMS_BAL();
                int[] RoleIDs = new int[] { (int)TMS_UserRole.HOD_TMS };
                DataTable dt = objUMS_BAL.getRoleWiseDepts(EmpID, RoleIDs);
                ddlDepartments.DataSource = dt;
                ddlDepartments.DataTextField = "DepartmentName";
                ddlDepartments.DataValueField = "DeptID";
                ddlDepartments.DataBind();
                ddlDepartments.Items.Insert(0, new ListItem("- Select Department -", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_T2:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {      
            try
            {
                objTMS_BAL = new TMS_BAL();
                if (Request.QueryString["TempLateID"] != null)
                {
                    if (txtJR_TemplateName.Value.Trim() != "" && JRDescription.Html.Trim().ToString() != "" && ddlDepartments.SelectedValue != "0")
                    {
                        int Result = -1;

                        objTMS_BAL.JR_TemplateOperations(txtJR_TemplateName.Value.Trim(), JRDescription.Html, ddlDepartments.SelectedValue,
                        Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]).ToString(), "Update", out Result, Request.QueryString["TempLateID"].ToString());
                        if (Result == 1)
                        {
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "JR Template modified successfully.", "success", "BackToList()");
                        }
                        else if (Result == 2)
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "JR Template Already Exists with this Name.", "info");
                        }
                    }
                    else
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "All fields represented with * are mandatory", "error");
                    }
                }
                else
                {
                    if (txtJR_TemplateName.Value.Trim() != "" && JRDescription.Html.ToString().Trim() != "" && ddlDepartments.SelectedValue != "0")
                    {
                        int Result = -1;
                        objTMS_BAL.JR_TemplateOperations(txtJR_TemplateName.Value.Trim(), JRDescription.Html, ddlDepartments.SelectedValue,
                       Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]).ToString(), "Insert", out Result);
                        if (Result == 1)
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "JR Template Created.", "success");
                            ClearFields();
                        }
                        else if (Result == 2)
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "JR Template Already Exists with this Name.", "info");
                        }
                    }
                    else
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "All fields represented with * are mandatory", "error");
                    }
                }
                //upJR_Description.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_T3:" + strline + "  " + strMsg, "error");
            }
        }
        public void ClearFields()
        {
            try
            {
                JRDescription.Html = "";
                txtJR_TemplateName.Value = "";
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_T3:" + strline + "  " + strMsg, "error");
            }
        }

        private void MsgSucess(string msg)
        {
            HelpClass.custAlertMsg(this, this.GetType(), "" + msg + "", "success");
        }
        private void MsgWarning(string msg)
        {
            HelpClass.custAlertMsg(this, this.GetType(), "" + msg + "", "warning");
        }
        private void JsMethod(string msg)
        {
            msg = msg.Replace("'", "");
            HelpClass.custAlertMsg(this, this.GetType(), "" + msg + "", "info");
        }
        private void JsMethodForSucessAndReload(string msg)
        {
            msg = msg.Replace("'", "");
            if (Request.QueryString["TempLateID"] != null)
            {
                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "" + msg + "", "success", "ReloadCurrentPageUpdate()");
            }
            else
            {
                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "" + msg + "", "success", "ReloadCurrentPageCreate()");
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["TempLateID"] != null)
                {
                    Response.Redirect("~/TMS/Administration/JR_Template/JR_TemplateList.aspx");
                }
                else
                {
                    Response.Redirect("~/TMS/Administration/JR_Template/JR_Template.aspx");
                }
                upJR_Description.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_T5:" + strline + "  " + strMsg, "error");
            }
        }
    }
}