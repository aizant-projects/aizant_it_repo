﻿ <%@ Page Title="JR Template" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" AutoEventWireup="true" ValidateRequest="false" EnableEventValidation="false" CodeBehind="JR_Template.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.Administration.JR_Template.JR_Template" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <style>
        .fa-video-camera {
            visibility: hidden;
        }

        .richText .richText-help {
            display: none !important;
        }

        .dxpcLite .dxpc-mainDiv, .dxpcLite.dxpc-mainDiv, .dxdpLite .dxpc-mainDiv, .dxdpLite.dxpc-mainDiv {
            top: 0;
            left: 0;
            background-color: white;
            border: 1px solid #8B8B8B;
            height: 500px;
            overflow-y: scroll;
        }
    </style>

    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none" id="divMainContainer" runat="server">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_header">
                        <asp:Label ID="lblTitle" runat="server"></asp:Label>
                    </div>
                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                        <div class="">
                            <div class="form-group float-left col-lg-6 col-sm-6 col-12 col-md-6 padding-none top">
                                <label class="float-left col-lg-12 col-sm-12 col-12 col-md-12 control-label">Department<span class="mandatoryStar">*</span></label>
                                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12">
                                    <asp:UpdatePanel ID="upDepartment" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlDepartments" runat="server" CssClass="regulatory_dropdown_style  selectpicker form-control"
                                                data-live-search="true" data-size="7"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlDepartments" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                            <div class="form-group float-left col-lg-6 col-sm-6 col-12 col-md-6 padding-none top" id="divJR_txtField" runat="server" visible="true">
                                <label class="float-left col-lg-12 col-sm-12 col-12 col-md-12 control-label">JR Template Name<span class="mandatoryStar">*</span></label>
                                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12">
                                    <asp:UpdatePanel ID="uptxtJrTemplateName" runat="server" UpdateMode="Always">
                                        <ContentTemplate>
                                            <input type="text" class="login_input_sign_up form-control" runat="server" name="name" id="txtJR_TemplateName" placeholder="Enter Template Name" maxlength="100" onkeypress="return ValidateAlphaNumericSpace(event)" onpaste="return false" tabindex="2" autocomplete="off" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                        <div class="form-horizontal float-left col-12 padding-none">
                            <div class="form-group float-left col-12 top padding-none">
                                <label class="float-left col-lg-12 col-sm-12 col-12 col-md-12 control-label">JR Description<span class="mandatoryStar">*</span></label>
                                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12">
                                    <asp:UpdatePanel ID="upJR_Description" runat="server" UpdateMode="Always">
                                        <ContentTemplate>
                                            <dx:ASPxHtmlEditor ID="JRDescription" runat="server" Theme="Default" Width="100%" EnableTheming="True" ClientInstanceName="txtJrDescription" Height="496px">
                                                <Toolbars>
                                                    <dx:HtmlEditorToolbar Name="StandardToolbar1">
                                                         
                                                        <Items>
                                                            <dx:ToolbarParagraphFormattingEdit AdaptivePriority="2" Width="120px">
                                                                <Items>
                                                                    <dx:ToolbarListEditItem Text="Normal" Value="p" />
                                                                    <dx:ToolbarListEditItem Text="Heading  1" Value="h1" />
                                                                    <dx:ToolbarListEditItem Text="Heading  2" Value="h2" />
                                                                    <dx:ToolbarListEditItem Text="Heading  3" Value="h3" />
                                                                    <dx:ToolbarListEditItem Text="Heading  4" Value="h4" />
                                                                    <dx:ToolbarListEditItem Text="Heading  5" Value="h5" />
                                                                    <dx:ToolbarListEditItem Text="Heading  6" Value="h6" />
                                                                    <dx:ToolbarListEditItem Text="Address" Value="address" />
                                                                    <dx:ToolbarListEditItem Text="Normal (DIV)" Value="div" />
                                                                </Items>
                                                              
                                                            </dx:ToolbarParagraphFormattingEdit>
                                                            <dx:ToolbarFontNameEdit AdaptivePriority="2">
                                                                <Items>
                                                                    <dx:ToolbarListEditItem Text="Times New Roman" Value="Times New Roman" />
                                                                    <dx:ToolbarListEditItem Text="Tahoma" Value="Tahoma" />
                                                                    <dx:ToolbarListEditItem Text="Verdana" Value="Verdana" />
                                                                    <dx:ToolbarListEditItem Text="Arial" Value="Arial" />
                                                                    <dx:ToolbarListEditItem Text="MS Sans Serif" Value="MS Sans Serif" />
                                                                    <dx:ToolbarListEditItem Text="Courier" Value="Courier" />
                                                                    <dx:ToolbarListEditItem Text="Segoe UI" Value="Segoe UI" />
                                                                </Items>
                                                            </dx:ToolbarFontNameEdit>
                                                            <dx:ToolbarFontSizeEdit AdaptivePriority="2">
                                                                <Items>
                                                                    <dx:ToolbarListEditItem Text="1 (8pt)" Value="1" />
                                                                    <dx:ToolbarListEditItem Text="2 (10pt)" Value="2" />
                                                                    <dx:ToolbarListEditItem Text="3 (12pt)" Value="3" />
                                                                    <dx:ToolbarListEditItem Text="4 (14pt)" Value="4" />
                                                                    <dx:ToolbarListEditItem Text="5 (18pt)" Value="5" />
                                                                    <dx:ToolbarListEditItem Text="6 (24pt)" Value="6" />
                                                                </Items>
                                                            </dx:ToolbarFontSizeEdit>
                                                            <dx:ToolbarBoldButton AdaptivePriority="1" BeginGroup="True">
                                                            </dx:ToolbarBoldButton>
                                                            <dx:ToolbarItalicButton AdaptivePriority="1">
                                                            </dx:ToolbarItalicButton>
                                                            <dx:ToolbarUnderlineButton AdaptivePriority="1">
                                                            </dx:ToolbarUnderlineButton>
                                                            <dx:ToolbarStrikethroughButton AdaptivePriority="1">
                                                            </dx:ToolbarStrikethroughButton>
                                                            <dx:ToolbarJustifyLeftButton AdaptivePriority="1" BeginGroup="True">
                                                            </dx:ToolbarJustifyLeftButton>
                                                            <dx:ToolbarJustifyCenterButton AdaptivePriority="1">
                                                            </dx:ToolbarJustifyCenterButton>
                                                            <dx:ToolbarJustifyRightButton AdaptivePriority="1">
                                                            </dx:ToolbarJustifyRightButton>
<%--                                                            <dx:ToolbarBackColorButton AdaptivePriority="1" BeginGroup="True">
                                                            </dx:ToolbarBackColorButton>--%>
                                                            <dx:ToolbarFontColorButton AdaptivePriority="1">
                                                            </dx:ToolbarFontColorButton>
                                                            <dx:ToolbarCutButton AdaptivePriority="2">
                                                            </dx:ToolbarCutButton>
                                                            <dx:ToolbarCopyButton AdaptivePriority="2">
                                                            </dx:ToolbarCopyButton>
                                                            <dx:ToolbarPasteButton AdaptivePriority="2">
                                                            </dx:ToolbarPasteButton>
                                                            <dx:ToolbarPasteFromWordButton AdaptivePriority="2">
                                                            </dx:ToolbarPasteFromWordButton>
                                                            <dx:ToolbarUndoButton AdaptivePriority="1" BeginGroup="True">
                                                            </dx:ToolbarUndoButton>
                                                            <dx:ToolbarRedoButton AdaptivePriority="1">
                                                            </dx:ToolbarRedoButton>
                                                            <dx:ToolbarRemoveFormatButton AdaptivePriority="2" BeginGroup="True">
                                                            </dx:ToolbarRemoveFormatButton>
                                                            <dx:ToolbarSuperscriptButton AdaptivePriority="1" BeginGroup="True" Visible="False">
                                                            </dx:ToolbarSuperscriptButton>
                                                            <dx:ToolbarSubscriptButton AdaptivePriority="1" Visible="False">
                                                            </dx:ToolbarSubscriptButton>
                                                            <dx:ToolbarInsertOrderedListButton AdaptivePriority="1" BeginGroup="True">
                                                            </dx:ToolbarInsertOrderedListButton>
                                                            <dx:ToolbarInsertUnorderedListButton AdaptivePriority="1">
                                                            </dx:ToolbarInsertUnorderedListButton>
                                                            <dx:ToolbarIndentButton AdaptivePriority="2" BeginGroup="True">
                                                            </dx:ToolbarIndentButton>
                                                            <dx:ToolbarOutdentButton AdaptivePriority="2">
                                                            </dx:ToolbarOutdentButton>
                                                            <dx:ToolbarInsertLinkDialogButton AdaptivePriority="1" BeginGroup="True" Visible="False">
                                                            </dx:ToolbarInsertLinkDialogButton>
                                                            <dx:ToolbarUnlinkButton AdaptivePriority="1" Visible="False">
                                                            </dx:ToolbarUnlinkButton>
                                                            <dx:ToolbarInsertImageDialogButton AdaptivePriority="1" Visible="False">
                                                            </dx:ToolbarInsertImageDialogButton>
                                                            <dx:ToolbarTableOperationsDropDownButton AdaptivePriority="2" BeginGroup="True">
                                                                <Items>
                                                                    <dx:ToolbarInsertTableDialogButton BeginGroup="True" Text="Insert Table..." ToolTip="Insert Table...">
                                                                    </dx:ToolbarInsertTableDialogButton>
                                                                    <dx:ToolbarTablePropertiesDialogButton BeginGroup="True">
                                                                    </dx:ToolbarTablePropertiesDialogButton>
                                                                    <dx:ToolbarTableRowPropertiesDialogButton>
                                                                    </dx:ToolbarTableRowPropertiesDialogButton>
                                                                    <dx:ToolbarTableColumnPropertiesDialogButton>
                                                                    </dx:ToolbarTableColumnPropertiesDialogButton>
                                                                    <dx:ToolbarTableCellPropertiesDialogButton>
                                                                    </dx:ToolbarTableCellPropertiesDialogButton>
                                                                    <dx:ToolbarInsertTableRowAboveButton BeginGroup="True">
                                                                    </dx:ToolbarInsertTableRowAboveButton>
                                                                    <dx:ToolbarInsertTableRowBelowButton>
                                                                    </dx:ToolbarInsertTableRowBelowButton>
                                                                    <dx:ToolbarInsertTableColumnToLeftButton>
                                                                    </dx:ToolbarInsertTableColumnToLeftButton>
                                                                    <dx:ToolbarInsertTableColumnToRightButton>
                                                                    </dx:ToolbarInsertTableColumnToRightButton>
                                                                    <dx:ToolbarSplitTableCellHorizontallyButton BeginGroup="True">
                                                                    </dx:ToolbarSplitTableCellHorizontallyButton>
                                                                    <dx:ToolbarSplitTableCellVerticallyButton>
                                                                    </dx:ToolbarSplitTableCellVerticallyButton>
                                                                    <dx:ToolbarMergeTableCellRightButton>
                                                                    </dx:ToolbarMergeTableCellRightButton>
                                                                    <dx:ToolbarMergeTableCellDownButton>
                                                                    </dx:ToolbarMergeTableCellDownButton>
                                                                    <dx:ToolbarDeleteTableButton BeginGroup="True">
                                                                    </dx:ToolbarDeleteTableButton>
                                                                    <dx:ToolbarDeleteTableRowButton>
                                                                    </dx:ToolbarDeleteTableRowButton>
                                                                    <dx:ToolbarDeleteTableColumnButton>
                                                                    </dx:ToolbarDeleteTableColumnButton>
                                                                </Items>
                                                            </dx:ToolbarTableOperationsDropDownButton>
                                                            <dx:ToolbarFindAndReplaceDialogButton AdaptivePriority="2" BeginGroup="True">
                                                            </dx:ToolbarFindAndReplaceDialogButton>
                                                            <dx:ToolbarFullscreenButton AdaptivePriority="1" BeginGroup="True">
                                                            </dx:ToolbarFullscreenButton>
                                                        </Items>
                                                    </dx:HtmlEditorToolbar>
                                                </Toolbars>
                                                <Settings AllowHtmlView="False">
                                                </Settings>
                                                <SettingsSpellChecker Culture="(Default)">
                                                </SettingsSpellChecker>
                                            </dx:ASPxHtmlEditor>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 top bottom">
                                <div class="float-right">
                                    <asp:UpdatePanel ID="upMainFormButtons" runat="server" style="display: inline-block">
                                        <ContentTemplate>
                                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClientClick="javascript:return MandatoryFields();" CssClass="btn-signup_popup" OnClick="btnSubmit_Click" TabIndex="4" />
                                            <asp:Button ID="btnCancel" runat="server" Text="Reset" CssClass=" btn-revert_popup" OnClick="btnCancel_Click" TabIndex="5" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12">
                    <asp:UpdatePanel ID="upLblMsg" UpdateMode="Always" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <div id="jrDescriptionBody" style="display: none"></div>
    <script>
        function MandatoryFields() {
            $('#jrDescriptionBody').html(txtJrDescription.GetHtml());
            var TemplateName = document.getElementById("<%=txtJR_TemplateName.ClientID%>").value.trim();
            // var Description = txtJrDescription.GetHtml().replace(/&nbsp;/gi, "");
            var Description = $('#jrDescriptionBody').text();
            errors = [];
            if (document.getElementById('<%=ddlDepartments.ClientID%>').value == 0) {
                errors.push("Select Department");
            }
            if (TemplateName == '') {
                errors.push("Enter JR Template Name");
            }
            if (Description.trim() == "") {
                errors.push("Enter JR Description");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                ShowPleaseWait('show');
                return true;
            }
        }
    </script>
    <script>
        function ReloadCurrentPageCreate() {
            window.open("<%=ResolveUrl("~/TMS/Administration/JR_Template/JR_Template.aspx")%>", "_self");
        }
        function ReloadCurrentPageUpdate() {
            window.open("<%=ResolveUrl("~/TMS/Administration/JR_Template/JR_Template.aspx?Operation=M")%>", "_self");
        }
    </script>

    <script>
        function BackToList() {
            window.open("<%=ResolveUrl("~/TMS/Administration/JR_Template/JR_TemplateList.aspx")%>", "_self");
        }
    </script>

</asp:Content>
