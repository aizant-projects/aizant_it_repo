﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" AutoEventWireup="true" CodeBehind="TrainerAnswerDocList.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.TrainerAnswersOnDocs.TrainerAnswerDocList" %>

<%@ Register Src="~/UserControls/TMS_UserControls/ucViewQuestions.ascx" TagPrefix="uc1" TagName="ucViewQuestions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <asp:HiddenField ID="hdnTraineeEmpID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnDocID" runat="server" Value="0" />

    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="mainContainer" runat="server" visible="true">
            <div class="col-12 padding-none">
                <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 ">
                    <asp:Label ID="Label2" runat="server" Font-Bold="true" Text="Trainer Answer On Document"></asp:Label>
                </div>
                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 top bottom padding-none">
                    <table id="tblDocumentWithQuestions" class="datatable_cust display breakword" style="width: 100%">
                        <thead>
                            <tr>                              
                                <th>DocID</th>
                                <th>Document</th>
                                <th>Department</th>                             
                                <th>Trainee Questions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
   
    <uc1:ucViewQuestions runat="server" ID="ucViewQuestions" />

     <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <%--<asp:Button ID="btnViewDocument" runat="server" Text="Button" OnClick="btnViewDocument_Click" />--%>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>


    <script>
        function ReadDoc(DocID) {
            $('#<%=hdnDocID.ClientID%>').val(DocID);
        }

        function ViewDocument() {
            $('#ReadDocModal').modal({ backdrop: 'static', keyboard: false });
        }
    </script>

    <!--Documents which has Trainee Question jQuery DataTable-->
    <script>
        var oDocumentWithQuestions = $('#tblDocumentWithQuestions').DataTable({
            columns: [
                { 'data': 'DocID' },
                { 'data': 'Document' },
                { 'data': 'Department' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a class="grid_button" data-toggle="modal" data-target="#modalViewQuestions" title="Answered Questions/Asked Questions" href="#" onclick="ViewQuestions(' + o.DocID + ');">' + o.TraineeQuestions+'</a>'; }
                }                                          
            ],
            "order": [3, "asc"],
            "scrollY": "360px",
            "aoColumnDefs": [{ "targets": [0], "visible": false }, { className: "textAlignLeft", "targets": [1, 2] }],
            "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TrainerAnswersOnDoc/TrainerAnswersOnDoc.asmx/GetDocmentsWithTrainerAnswers")%>",
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "TraineeEmpID", "value": $(<%=hdnTraineeEmpID.ClientID%>).val() });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        UserSessionCheck();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
    </script>

    <!--Document wise Questioned Trainees on JR Training jQuery DataTable-->
    <script>
        function ViewJR_DQ_Trainees(DocID) {
            var oTableDQ_Trainees = $('#tblDQ_Trainees').DataTable({
                columns: [
                    { 'data': 'JrID' },
                    { 'data': 'JrName' },
                    { 'data': 'EmpCode' },
                    { 'data': 'Trainee' },                    
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) { return '<a class="view"  href="#" onclick="ViewQuestions(' + o.JrID + ');"></a>'; }
                    }
                ],
                "order": [3, "asc"],
                "scrollY": "360px",
                "aoColumnDefs": [{ "targets": [0, 1], "visible": false }, { className: "textAlignLeft", "targets": [2, 3] }],
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/TraineeQuestionsOnDoc/TraineeQuestionsOnDoc.asmx/GetDocmentsWithTraineeQuestions")%>",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "TraineeEmpID", "value": $(<%=hdnTraineeEmpID.ClientID%>).val() });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            UserSessionCheck();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        }
    </script>

    <script>
        function ViewQuestions(DocID) {
            var TraineeID = $('#<%=hdnTraineeEmpID.ClientID%>').val();
            ViewAskedQuestionsByTrainee(DocID, TraineeID);          
        }
    </script>

</asp:Content>
