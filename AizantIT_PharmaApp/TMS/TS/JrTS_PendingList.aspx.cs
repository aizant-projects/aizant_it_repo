﻿using AizantIT_PharmaApp.Common;
using System;
using System.Collections;
using System.Data;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.TS
{
    public partial class JrTS_PendingList : System.Web.UI.Page
    {
        UMS_BAL objUMS_BAL = new UMS_BAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = (Session["UserDetails"] as DataSet).Tables[1].Select("ModuleID="+(int)Modules.TMS);
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0) //HOD
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                lblAutorizedMsg.Text = "Your Are Not Authorized to This Page. Contact Admin.";
                                divAutorizedMsg.Visible = true;
                                divMainContainer.Visible = false;
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }                   
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "QA_RTS-1:" + strline + "  " + strMsg, "error");
            }
        }
        private void InitializeThePage()
        {
            try
            {
                if (Request.QueryString["Notification_ID"] != null)
                {
                    objUMS_BAL = new UMS_BAL();
                    int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs); //2 represents NotifyEmp had visited the page through notification link.
                    hdnNotificationID.Value = Request.QueryString["Notification_ID"].ToString();
                }
                if (Session["JR_TS_Details"] != null)
                {
                    Session["JR_TS_Details"] = null;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TS_A-4:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnViewJrTS_Click(object sender, EventArgs e)
        {
            try
            {
                //Session["QA_Reverted_JR_ID"] = Convert.ToInt32((sender as LinkButton).CommandArgument);
                if (Session["JR_TS_Details"] != null)
                {
                    Session["JR_TS_Details"] = null;
                }
                Hashtable ht = new Hashtable();
                ht.Add("TS_ID",hdnTS_ID.Value);
                ht.Add("TS_Status", hdnTS_Status.Value);
                ht.Add("Previous_PageValue", "PL");
                Session["JR_TS_Details"] = ht;
                Response.Redirect("~/TMS/TS/AssignTS.aspx", false);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "QA_RTS-3:" + strline + "  " + strMsg, "error");
            }
        }
    }
}