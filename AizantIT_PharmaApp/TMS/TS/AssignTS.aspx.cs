﻿using Aizant_Enums;
using AizantIT_PharmaApp.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BusinessLayer;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.TS
{
    public partial class AssignTS1 : System.Web.UI.Page
    {
        TMS_BAL objTMS_BAL;
        UMS_BAL objUMS_BAL = new UMS_BAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ElectronicSign.buttonClick += new EventHandler(ElectronicSign_buttonClick);
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        if (!Request.UrlReferrer.ToString().Contains("AssignTS.aspx"))
                        {
                            hdnPrevipusPageURL.Value = Request.UrlReferrer.ToString();
                            Session["TMS_BackToList"] = Request.UrlReferrer.ToString();
                        }
                        else
                        {
                            hdnPrevipusPageURL.Value = Session["TMS_BackToList"].ToString();
                        }
                        hdnEmpID.Value = ((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString()).ToString();
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=" + (int)Modules.TMS);
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0 || dtTemp.Select("RoleID=" + (int)TMS_UserRole.QA_TMS).Length > 0) //only for HOD and QA
                            {
                                if (Session["JR_TS_Details"] != null)
                                {
                                    InitializeThePage();
                                }
                                else
                                {
                                    loadAuthorizeErrorMsg();
                                }
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATS-1:" + strline + "  " + strMsg, "error");
            }
        }

        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to view this Page. Contact Admin.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }

        public void InitializeThePage()
        {
            try
            {
                if (Request.QueryString["Notification_ID"] != null)
                {
                    objUMS_BAL = new UMS_BAL();
                    int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs); //2 represents NotifyEmp had visited the page through notification link.
                }
                if (Session["JR_TS_Details"] != null)
                {
                    Hashtable ht = Session["JR_TS_Details"] as Hashtable;
                    hdnTS_ID.Value = ht["TS_ID"].ToString();

                    if (ht["Previous_PageValue"].ToString() == "PL") //from JrTS_PendingList.aspx page for HOD
                    {
                        //hdnPrevipusPageURL.Value = "~/TMS/TS/JrTS_PendingList.aspx?FilterType=2";
                        if (ht["TS_Status"].ToString() == "3") // reverted by qa and to modify by HOD
                        {
                            divRevertReason.Visible = true;
                            lblTitle.Text = "Modify JR Training Schedule";
                            btnSubmitTS.Text = "Update";
                            hdnActionPurpose.Value = "Update";
                            BindTSDetails(Convert.ToInt32(ht["TS_ID"]), "Modify");
                            divHOD_Remarks.Visible = true;
                            txtHOD_Remarks.Text = string.Empty;
                        }
                        else if (ht["TS_Status"].ToString() == "2" || ht["TS_Status"].ToString() == "1") // modified by hod and created by hod
                        {
                            // Code for HOD view
                            divButtons.Visible = false;
                            lblTitle.Text = "View JR Training Schedule ";
                            BindTSDetails(Convert.ToInt32(ht["TS_ID"]), "View");
                            hdnActionPurpose.Value = "HodView";
                            txtHOD_Remarks.Enabled = false;
                            DisableCode();
                            ApplyGridStylesToHideSelectionColumns();
                        }
                        else
                        {
                            // Code for HOD create
                            lblTitle.Text = "Create JR Training Schedule";
                            BindTSDetails(Convert.ToInt32(ht["TS_ID"]), "Create");
                            hdnActionPurpose.Value = "Create";
                            txtHOD_Remarks.Text = string.Empty;
                            //divHOD_Remarks.Attributes.Remove("form-horizontal col-sm-6 padding-none float-left");
                            divHOD_Remarks.Attributes.Add("class", "form-horizontal col-sm-12 padding-none float-left");
                            divHOD_Remarks.Visible = true;
                        }
                    }
                    if (ht["Previous_PageValue"].ToString() == "RL")// from JrTS_ReviewList.aspx page for QA
                    {
                        //hdnPrevipusPageURL.Value = "~/TMS/TS/JrTS_ReviewList.aspx?FilterType=2";
                        if (ht["TS_Status"].ToString() == "1" || ht["TS_Status"].ToString() == "2") // created or modified by hod
                        {
                            // Created by Hod or modified by HOD, QA should review it
                            lblTitle.Text = "Approval Training Schedule ";
                            BindTSDetails(Convert.ToInt32(ht["TS_ID"]), "View");
                            btnSubmitTS.Text = "Approve";
                            btnCancelTS.Text = "Revert";
                            hdnActionPurpose.Value = "Approval";
                            divRevertReason.Visible = true;
                            divHOD_Remarks.Visible = true;
                            txtHOD_Remarks.Enabled = false;
                            txtRevertReason.Enabled = true;
                            txtRevertReason.Text = string.Empty;
                            ApplyGridStylesToHideSelectionColumns();
                        }
                        else if (ht["TS_Status"].ToString() == "3") // reverted by QA
                        {
                            // reverted by qa, QA to view
                            divButtons.Visible = false;
                            lblTitle.Text = "View Training Schedule ";
                            BindTSDetails(Convert.ToInt32(ht["TS_ID"]), "View");
                            hdnActionPurpose.Value = "QaView";
                            txtHOD_Remarks.Attributes.Add("style", "cursor:not-allowed;");
                            ApplyGridStylesToHideSelectionColumns();
                        }
                        DisableCode();
                        divAuthor.Visible = true;
                        divReviewer.Visible = false;
                    }
                    hlBackToList.NavigateUrl = hdnPrevipusPageURL.Value;
                    hlBackToList.Visible = true;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATS-3:" + strline + "  " + strMsg, "error");
            }
        }
        public void DisableCode()
        {
            try
            {
                string leftButtonStyle = "btn-signup_popup leftArrowButton";
                string rightButtonStyle = "btn-signup_popup rightArrowButton";
                string CursorNotAllow = "cursor:not-allowed;";

                txtDuration.Attributes.Add("disabled", "disabled");
                lnkBtnAdd.Enabled = false;
                lnkBtnAdd.CssClass = rightButtonStyle;
                lnkBtnAdd.Attributes.Add("style", CursorNotAllow);
                lnkBtnRemove.Enabled = false;
                lnkBtnRemove.CssClass = leftButtonStyle;
                lnkBtnRemove.Attributes.Add("style", CursorNotAllow);
                lnkRetBtnAdd.Enabled = false;
                lnkRetBtnAdd.CssClass = rightButtonStyle;
                lnkRetBtnAdd.Attributes.Add("style", CursorNotAllow);
                lnkRetBtnRemove.Enabled = false;
                lnkRetBtnRemove.CssClass = leftButtonStyle;
                lnkRetBtnRemove.Attributes.Add("style", CursorNotAllow);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void BindTSDetails(int TS_ID, string Operation)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                DataSet ds = objTMS_BAL.getTsForModification(TS_ID);
                if (Operation == "Modify" || Operation == "Create")
                {
                    BindDepartment(ds.Tables[0]);
                }
                else
                {
                    BindDepartment(ds.Tables[1]);
                }
                hdnTraineeID.Value = ds.Tables[2].Rows[0]["EmpID"].ToString();
                txtTraineeName.Text = ds.Tables[2].Rows[0]["TraineeName"].ToString();
                ddlEmpDepartments.Items.Insert(0, new ListItem(ds.Tables[2].Rows[0]["DeptName"].ToString(), ds.Tables[2].Rows[0]["DeptID"].ToString()));
                txtDesignation.Text = ds.Tables[2].Rows[0]["DesignationName"].ToString();
                txtJrName.Text = ds.Tables[2].Rows[0]["JR_Name"].ToString();
                if (Operation == "Create")
                {
                    bindQaEmployees(Convert.ToInt32(ddlEmpDepartments.SelectedValue));
                }
                else
                {
                    ddlReviewer.Items.Insert(0, new ListItem(ds.Tables[2].Rows[0]["ReviewerName"].ToString(), ds.Tables[2].Rows[0]["ReviewerEmpID"].ToString()));
                    ddlReviewer.Enabled = false;
                }

                txtAuthor.Text = ds.Tables[2].Rows[0]["AuthorName"].ToString();
                ViewState["Trainers"] = ds.Tables[3];
                ViewState["Docs"] = ds.Tables[4];
                DataView dvAssignedDocuments = new DataView(ds.Tables[4]);
                dvAssignedDocuments.RowFilter = "Status<>'RN'";
                hdnDocumentsCountForValidation.Value = dvAssignedDocuments.ToTable().Rows.Count.ToString();

                ViewState["DeptDuration"] = ds.Tables[5];
                txtRevertReason.Visible = true;
                if (ds.Tables[6].Rows.Count > 0)
                {
                    if (ds.Tables[6].Rows[0]["ReviewerLastCommments"].ToString() != "")
                    {
                        txtRevertReason.Text = ds.Tables[6].Rows[0]["ReviewerLastCommments"].ToString();
                    }
                    if (ds.Tables[6].Rows[0]["AuthorLastComments"].ToString() != "")
                    {
                        txtHOD_Remarks.Text = ds.Tables[6].Rows[0]["AuthorLastComments"].ToString();
                    }
                }
                hdnJR_ID.Value = ds.Tables[2].Rows[0]["JR_ID"].ToString();
                bindEmptyGrids();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATS-3:" + strline + "  " + strMsg, "error");
            }
        }
        //added for electronic sign for HOD Assign and Update TS
        protected void ElectronicSign_buttonClick(object sender, EventArgs e)
        {
            try
            {
                bool b = ElectronicSign.IsPasswordValid;
                if (b == true)
                {
                    DataTable dtAssignedTrainers = ViewState["Trainers"] as DataTable;
                    DataTable dtAssignedDocs = ViewState["Docs"] as DataTable;
                    DataTable dtDeptDuration = ViewState["DeptDuration"] as DataTable;
                    if (hdnAction.Value == "Submit")
                    {
                        SubmitTS(dtAssignedTrainers, dtAssignedDocs, dtDeptDuration, hdnJR_ID.Value,
                        Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                    }
                    else if (hdnAction.Value == "Update")
                    {
                        SubmitTS(dtAssignedTrainers, dtAssignedDocs, dtDeptDuration, hdnJR_ID.Value,
                       Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]), "Update");
                    }
                    else if (hdnAction.Value == "Revert")
                    {
                        SubmitApproval(((int)AizantEnums.JR_Status.Training_Schedule_Declined_By_QA).ToString(), txtRevertReason.Text.Trim());
                        txtRevertReason.Text = string.Empty;
                    }
                    else if (hdnAction.Value == "Approve")
                    {
                        SubmitApproval(((int)AizantEnums.JR_Status.Training_Schedule_Approved_By_QA).ToString(), txtRevertReason.Text.Trim());
                    }

                }
                if (b == false)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Password is Incorrect.", "error", "focusOnElecPwd();");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATS-4:" + strline + "  " + strMsg, "error");
            }
        }

        public void BindDepartment(DataTable dtDept)
        {
            try
            {
                ddlTrainingDept.DataSource = dtDept;
                ddlTrainingDept.DataTextField = "DepartmentName";
                ddlTrainingDept.DataValueField = "DeptID";
                ddlTrainingDept.DataBind();
                ddlTrainingDept.Items.Insert(0, new ListItem("-- Select Department --", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATS-6:" + strline + "  " + strMsg, "error");
            }
        }
        public void bindQaEmployees(int DeptID)
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                int RoleID = (int)TMS_UserRole.QA_TMS;
                DataTable dtApprovers = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(DeptID), RoleID, true);
                //Bind QA's
                DataView dvQA = new DataView(dtApprovers);
                dvQA.RowFilter = "EmpID not in (" + hdnTraineeID.Value + "," + hdnEmpID.Value + ")";
                ddlReviewer.DataSource = dvQA.ToTable();
                ddlReviewer.DataValueField = "EmpID";
                ddlReviewer.DataTextField = "EmpName";
                ddlReviewer.DataBind();
                ddlReviewer.Items.Insert(0, new ListItem("-- Select Reviewer --", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATS-7:" + strline + "  " + strMsg, "error");
            }
        }

        public void bindEmptyGrids()
        {
            gvAvailableTrainers.DataSource = new List<object>();
            gvAvailableTrainers.DataBind();

            gvAssignedTrainers.DataSource = new List<object>();
            gvAssignedTrainers.DataBind();

            gvAvailableDocs.DataSource = new List<object>();
            gvAvailableDocs.DataBind();

            gvAssignedDocs.DataSource = new List<object>();
            gvAssignedDocs.DataBind();

            gvRetAvailableDocs.DataSource = new List<object>();
            gvRetAvailableDocs.DataBind();

            gvRetAssignedDocs.DataSource = new List<object>();
            gvRetAssignedDocs.DataBind();

            txtDuration.Text = "";
            upTxtDuration.Update();

        }
        private void ApplyGridStylesToHideSelectionColumns()
        {
            string ColSize9 = "col-9", ColSize7 = "col-7";
            string leftAlign = " textAlignLeft";

            gvAvailableTrainers.Columns[0].Visible = false;
            gvAvailableTrainers.Columns[3].HeaderStyle.CssClass = ColSize9;
            gvAvailableTrainers.Columns[3].ItemStyle.CssClass = ColSize9 + leftAlign;
            gvAssignedTrainers.Columns[0].Visible = false;
            gvAssignedTrainers.Columns[3].HeaderStyle.CssClass = ColSize9;
            gvAssignedTrainers.Columns[3].ItemStyle.CssClass = ColSize9 + leftAlign;
            gvAvailableDocs.Columns[0].Visible = false;
            gvAvailableDocs.Columns[3].HeaderStyle.CssClass = ColSize7;
            gvAvailableDocs.Columns[3].ItemStyle.CssClass = ColSize7 + leftAlign;
            gvAssignedDocs.Columns[0].Visible = false;
            gvAssignedDocs.Columns[3].HeaderStyle.CssClass = ColSize7;
            gvAssignedDocs.Columns[3].ItemStyle.CssClass = ColSize7 + leftAlign;
            gvRetAvailableDocs.Columns[0].Visible = false;
            gvRetAvailableDocs.Columns[3].HeaderStyle.CssClass = ColSize9;
            gvRetAvailableDocs.Columns[3].ItemStyle.CssClass = ColSize9 + leftAlign;
            gvRetAssignedDocs.Columns[0].Visible = false;
            gvRetAssignedDocs.Columns[3].HeaderStyle.CssClass = ColSize9;
            gvRetAssignedDocs.Columns[3].ItemStyle.CssClass = ColSize9 + leftAlign;
        }

        protected void ddlTrainingDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["PreviousValue"] != null)
                {
                    if (ViewState["PreviousValue"].ToString() != "0" && gvAssignedDocs.Rows.Count > 0 && gvAssignedTrainers.Rows.Count > 0)
                    {
                        if (gvAssignedTrainers.Rows.Count > 0 && gvAssignedTrainers.Rows.Count > 0 && txtDuration.Text != "")
                        {
                            AddDuration(ViewState["PreviousValue"].ToString());
                        }
                    }
                }

                ViewState["PreviousValue"] = ddlTrainingDept.SelectedValue;
                txtDuration.Text = string.Empty;
                btnSubmitTS.Visible = true;
                btnCancelTS.Visible = true;
                objTMS_BAL = new TMS_BAL();
                if (ddlTrainingDept.SelectedValue == "0")
                {
                    bindEmptyGrids();
                }
                else
                {
                    string DeptID = ddlTrainingDept.SelectedValue;
                    DataTable dtTrainers = ViewState["Trainers"] as DataTable;
                    bindTrainers(dtTrainers, DeptID);

                    DataTable dtDocs = ViewState["Docs"] as DataTable;
                    bindDocs(dtDocs, DeptID);

                    bindDuration(DeptID);

                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "onDepartmentChange();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATS-10:" + strline + "  " + strMsg, "error");
            }
        }

        private void bindDuration(string DeptID)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                DataTable dtDuration = ViewState["DeptDuration"] as DataTable;
                ViewState["DeptDuration"] = dtDuration;

                DataView dvTrainers = new DataView(dtDuration);
                dvTrainers.RowFilter = "DeptID=" + DeptID;
                if (dvTrainers.ToTable().Rows.Count > 0)
                {
                    txtDuration.Text = dvTrainers.ToTable().Rows[0]["Duration"].ToString();
                }
                else
                {
                    txtDuration.Text = "";
                }

                upTxtDuration.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATS-11:" + strline + "  " + strMsg, "error");
            }
        }

        private void bindTrainers(DataTable dtTrainers, string DeptID)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                if (dtTrainers.Rows.Count == 0)
                {
                    dtTrainers = objTMS_BAL.GetDeptTrainers(DeptID);
                }
                else
                {
                    dtTrainers = objTMS_BAL.GetDeptTrainers(DeptID, dtTrainers);
                }
                ViewState["Trainers"] = dtTrainers;

                DataView dvTrainers = new DataView(dtTrainers);
                dvTrainers.RowFilter = "Status='Y' and DeptID=" + DeptID + " and EmpID<>" + hdnTraineeID.Value;
                gvAvailableTrainers.DataSource = dvTrainers.ToTable();
                gvAvailableTrainers.DataBind();

                dvTrainers.RowFilter = "Status='N' and DeptID=" + DeptID;
                gvAssignedTrainers.DataSource = dvTrainers.ToTable();
                gvAssignedTrainers.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATS-12:" + strline + "  " + strMsg, "error");
            }
        }

        private void bindDocs(DataTable dtDocs, string DeptID)
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                if (dtDocs.Rows.Count == 0)
                {
                    dtDocs = objTMS_BAL.GetDeptWiseSOPs(DeptID);
                }
                else
                {
                    dtDocs = objTMS_BAL.GetDeptWiseSOPs(DeptID, dtDocs);
                }
                ViewState["Docs"] = dtDocs;
                DataView dvDocs = new DataView(dtDocs);
                dvDocs.RowFilter = "Status='Y' and DeptID=" + DeptID;
                gvAvailableDocs.DataSource = dvDocs.ToTable();
                gvAvailableDocs.DataBind();
                gvRetAvailableDocs.DataSource = dvDocs.ToTable();
                gvRetAvailableDocs.DataBind();

                dvDocs.RowFilter = "Status='N' and DeptID=" + DeptID;
                gvAssignedDocs.DataSource = dvDocs.ToTable();
                gvAssignedDocs.DataBind();
                dvDocs.RowFilter = "Status='RN' and DeptID=" + DeptID;
                gvRetAssignedDocs.DataSource = dvDocs.ToTable();
                gvRetAssignedDocs.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATS-13:" + strline + "  " + strMsg, "error");
            }
        }

        protected void lnkBtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                AddData(gvAvailableTrainers, gvAvailableDocs, "N");
                AddDuration(ViewState["PreviousValue"].ToString());
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATS-14:" + strline + "  " + strMsg, "error");
            }
        }

        private void AddDuration(string DeptDuration)
        {
            try
            {
                DataTable dtDuration = ViewState["DeptDuration"] as DataTable;
                if (dtDuration.Rows.Count == 0)
                {
                    dtDuration = AddDurationForSOPs(dtDuration);
                }
                else
                {
                    bool DeptExists = false;
                    foreach (DataRow dr in dtDuration.Rows)
                    {
                        if (dr["DeptID"].ToString() == DeptDuration)
                        {
                            dr["Duration"] = txtDuration.Text;
                            DeptExists = true;
                        }
                    }
                    if (!DeptExists)
                    {
                        dtDuration = AddDurationForSOPs(dtDuration);
                    }
                }
                ViewState["DeptDuration"] = dtDuration;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATS-15:" + strline + "  " + strMsg, "error");
            }
        }
        private void RemoveDuration(string DeptID)
        {
            try
            {
                DataTable dtDuration = ViewState["DeptDuration"] as DataTable;
                foreach (DataRow row in dtDuration.Rows)
                {
                    if (row["DeptID"].ToString() == DeptID)
                    {
                        DataView dvDuration = new DataView(dtDuration);
                        dvDuration.RowFilter = "DeptID<>" + DeptID;
                        ViewState["DeptDuration"] = dvDuration.ToTable();
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATS-16:" + strline + "  " + strMsg, "error");
            }
        }

        private DataTable AddDurationForSOPs(DataTable dtDuration)
        {
            DataRow dr = dtDuration.NewRow();
            dr["DeptID"] = ddlTrainingDept.SelectedValue;
            dr["Duration"] = txtDuration.Text;
            dtDuration.Rows.Add(dr);
            return dtDuration;
        }

        protected void lnkBtnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                RemoveData(gvAssignedTrainers, gvAssignedDocs, "Y");
                if (gvAssignedTrainers.Rows.Count == 0 && gvAssignedDocs.Rows.Count == 0)
                {
                    RemoveDuration(ViewState["PreviousValue"].ToString());
                    txtDuration.Text = "";
                    upTxtDuration.Update();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATS-18:" + strline + "  " + strMsg, "error");
            }
        }

        #region Assign SOPs and Trainers
        private void AddData(GridView gvTrainers, GridView gvDocs, string Status)
        {
            int TrainerCount = 0, SopCount = 0;
            foreach (GridViewRow gvrTrainers in gvTrainers.Rows)
            {
                if (((CheckBox)gvrTrainers.FindControl("chkRow")).Checked)
                {
                    TrainerCount++;
                }
            }

            foreach (GridViewRow oItemLeft in gvDocs.Rows)
            {
                if (((CheckBox)oItemLeft.FindControl("chkRow")).Checked)
                {
                    SopCount++;
                }
            }

            if (TrainerCount == 0 && SopCount == 0)
            {
                StringBuilder sbMsg = new StringBuilder();
                if (TrainerCount == 0)
                {
                    sbMsg.Append("Trainer,");
                }
                if (SopCount == 0)
                {
                    sbMsg.Append("Document");
                }
                string strMsg = "Select Atleast One " + sbMsg.ToString().TrimEnd(',') + ".";
                HelpClass.custAlertMsg(this, this.GetType(), "" + strMsg + "", "error");
            }
            else
            {
                if ((TrainerCount != 0 && SopCount == 0))
                {
                    if (gvAssignedDocs.Rows.Count > 0)
                    {
                        hdnDocumentsCountForValidation.Value = (Convert.ToInt32(hdnDocumentsCountForValidation.Value) + SopCount).ToString();
                        AddSelection(gvTrainers, gvDocs, Status);
                    }
                    else
                    {
                        string strMsg = "Not able to Assign Trainers when there is no Assigned Documents. Select atleast one Document.";
                        HelpClass.custAlertMsg(this, this.GetType(), "" + strMsg + "", "error");
                    }
                }
                else if ((TrainerCount == 0 && SopCount != 0))
                {
                    if (gvAssignedTrainers.Rows.Count > 0)
                    {
                        hdnDocumentsCountForValidation.Value = (Convert.ToInt32(hdnDocumentsCountForValidation.Value) + SopCount).ToString();
                        AddSelection(gvTrainers, gvDocs, Status);
                    }
                    else
                    {
                        string strMsg = "Not able to Assign Documents when there is no Assigned Trainers. Select Trainers";
                        HelpClass.custAlertMsg(this, this.GetType(), "" + strMsg + "", "error");
                    }
                }
                else
                {
                    hdnDocumentsCountForValidation.Value = (Convert.ToInt32(hdnDocumentsCountForValidation.Value) + SopCount).ToString();
                    AddSelection(gvTrainers, gvDocs, Status);
                }
            }
        }

        private void AddSelection(GridView gvTrainers, GridView gvDocs, string Status)
        {
            DataTable dtTrainers = (DataTable)ViewState["Trainers"];
            foreach (GridViewRow gvrTrainers in gvTrainers.Rows)
            {
                if (((CheckBox)gvrTrainers.FindControl("chkRow")).Checked)
                {
                    int TrainerID = Convert.ToInt32(((Label)gvrTrainers.FindControl("lblEmpID")).Text);
                    int DeptID = Convert.ToInt32(((Label)gvrTrainers.FindControl("lblDeptID")).Text);

                    foreach (DataRow dr in dtTrainers.Rows)
                    {
                        if (Convert.ToInt32(dr["EmpID"]) == TrainerID && Convert.ToInt32(dr["DeptID"]) == DeptID)
                        {
                            dr["Status"] = Status;
                        }
                    }
                }
            }
            ViewState["Trainers"] = dtTrainers;
            bindTrainers(dtTrainers, ddlTrainingDept.SelectedValue);

            DataTable dtDocs = (DataTable)ViewState["Docs"];
            foreach (GridViewRow gvrDocs in gvDocs.Rows)
            {
                if (((CheckBox)gvrDocs.FindControl("chkRow")).Checked)
                {
                    int DocID = Convert.ToInt32(((Label)gvrDocs.FindControl("lblSopID")).Text);
                    int DeptID = Convert.ToInt32(((Label)gvrDocs.FindControl("lblDeptID")).Text);

                    foreach (DataRow dr in dtDocs.Rows)
                    {
                        if (Convert.ToInt32(dr["DocumentID"]) == DocID && Convert.ToInt32(dr["DeptID"]) == DeptID)
                        {
                            dr["Status"] = Status;
                        }
                    }
                }
            }
            ViewState["Docs"] = dtDocs;
            bindDocs(dtDocs, ddlTrainingDept.SelectedValue);
        }
        #endregion

        #region Remove Trainers or SOPs
        private void RemoveData(GridView gvTrainers, GridView gvDocs, string Status)
        {
            int TrainerCount = 0, SopCount = 0;
            foreach (GridViewRow gvrTrainers in gvTrainers.Rows)
            {
                if (((CheckBox)gvrTrainers.FindControl("chkRow")).Checked)
                {
                    TrainerCount++;
                }
            }

            foreach (GridViewRow oItemLeft in gvDocs.Rows)
            {
                if (((CheckBox)oItemLeft.FindControl("chkRow")).Checked)
                {
                    SopCount++;
                }
            }

            if (TrainerCount == 0 && SopCount == 0)
            {
                string strMsg = "Select Atleast One Trainer or Document.";
                // HelpClass.showMsg(this, this.GetType(), strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "" + strMsg + "", "error");
            }
            else
            {
                if (SopCount == gvDocs.Rows.Count || TrainerCount == gvTrainers.Rows.Count)
                {
                    if (SopCount == gvDocs.Rows.Count && TrainerCount == gvTrainers.Rows.Count)
                    {
                        hdnDocumentsCountForValidation.Value = (Convert.ToInt32(hdnDocumentsCountForValidation.Value) - SopCount).ToString();
                        RemoveSelection(gvTrainers, gvDocs, Status);
                    }
                    else
                    {
                        if (SopCount == gvDocs.Rows.Count)
                        {
                            string strMsg = "You are not allow to remove all Documents when Trainers remains.";
                            //  HelpClass.showMsg(this, this.GetType(), strMsg);
                            HelpClass.custAlertMsg(this, this.GetType(), "" + strMsg + "", "error");
                        }
                        else
                        {
                            string strMsg = "You are not allow to remove all Trainers when Documents remains.";
                            //HelpClass.showMsg(this, this.GetType(), strMsg);
                            HelpClass.custAlertMsg(this, this.GetType(), "" + strMsg + "", "error");
                        }
                    }
                }
                else
                {
                    hdnDocumentsCountForValidation.Value = (Convert.ToInt32(hdnDocumentsCountForValidation.Value) - SopCount).ToString();
                    RemoveSelection(gvTrainers, gvDocs, Status);
                }
            }
        }

        private void RemoveSelection(GridView gvTrainers, GridView gvDocs, string Status)
        {
            DataTable dtTrainers = (DataTable)ViewState["Trainers"];
            foreach (GridViewRow gvrTrainers in gvTrainers.Rows)
            {
                if (((CheckBox)gvrTrainers.FindControl("chkRow")).Checked)
                {
                    int TrainerID = Convert.ToInt32(((Label)gvrTrainers.FindControl("lblEmpID")).Text);
                    int DeptID = Convert.ToInt32(((Label)gvrTrainers.FindControl("lblDeptID")).Text);

                    foreach (DataRow dr in dtTrainers.Rows)
                    {
                        if (Convert.ToInt32(dr["EmpID"]) == TrainerID && Convert.ToInt32(dr["DeptID"]) == DeptID)
                        {
                            dr["Status"] = Status;
                        }
                    }
                }
            }
            ViewState["Trainers"] = dtTrainers;
            bindTrainers(dtTrainers, ddlTrainingDept.SelectedValue);

            DataTable dtDocs = (DataTable)ViewState["Docs"];
            foreach (GridViewRow gvrDocs in gvDocs.Rows)
            {
                if (((CheckBox)gvrDocs.FindControl("chkRow")).Checked)
                {
                    int DocID = Convert.ToInt32(((Label)gvrDocs.FindControl("lblSopID")).Text);
                    int DeptID = Convert.ToInt32(((Label)gvrDocs.FindControl("lblDeptID")).Text);

                    foreach (DataRow dr in dtDocs.Rows)
                    {
                        if (Convert.ToInt32(dr["DocumentID"]) == DocID && Convert.ToInt32(dr["DeptID"]) == DeptID)
                        {
                            dr["Status"] = Status;
                        }
                    }
                }
            }
            ViewState["Docs"] = dtDocs;
            bindDocs(dtDocs, ddlTrainingDept.SelectedValue);
        }
        #endregion

        protected void btnSubmitTS_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnSubmitTS.Text == "Submit")
                {
                    if (Convert.ToInt32(ddlReviewer.SelectedValue) == 0 && Convert.ToInt32(hdnDocumentsCountForValidation.Value) == 0)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Select Reviewer. <br/> Select at-least one Document and Trainer in Regular.", "error", "onDepartmentChange()");
                    }
                    else if (Convert.ToInt32(ddlReviewer.SelectedValue) == 0)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Select Reviewer.", "error", "onDepartmentChange()");
                    }
                    else if (Convert.ToInt32(hdnDocumentsCountForValidation.Value) > 0)
                    {
                        hdnAction.Value = "Submit";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "openElectronicSignModal();", true);
                    }
                    else
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Select At-least one Document and Trainer in Regular.", "error", "onDepartmentChange()");
                    }
                }
                else if (btnSubmitTS.Text == "Update")
                {
                    if (Convert.ToInt32(hdnDocumentsCountForValidation.Value) > 0)
                    {
                        hdnAction.Value = "Update";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "openElectronicSignModal();", true);
                    }
                    else
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Select At-least one Document and Trainer in Regular.", "error", "onDepartmentChange()");
                    }
                }
                else if (btnSubmitTS.Text == "Approve")
                {
                    hdnAction.Value = "Approve";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "openElectronicSignModal();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATS-23:" + strline + "  " + strMsg, "error");
            }
        }

        private void SubmitTS(DataTable dtAssignedTrainers, DataTable dtAssignedDocs, DataTable dtDeptDuration,
            string JR_ID, int EmpID, string Operation = null)
        {
            try
            {
                if (ViewState["PreviousValue"] != null)
                {
                    if (ViewState["PreviousValue"].ToString() != "0" && gvAssignedDocs.Rows.Count > 0 && gvAssignedTrainers.Rows.Count > 0)
                    {
                        if (gvAssignedTrainers.Rows.Count > 0 && gvAssignedTrainers.Rows.Count > 0 && txtDuration.Text != "")
                        {
                            AddDuration(ViewState["PreviousValue"].ToString());
                        }
                    }
                }
                objTMS_BAL = new TMS_BAL();
                int Result = 0;
                DataTable dtDocs = new DataTable();
                dtDocs.TableName = "Sops";
                dtDocs.Columns.Add("SopID");
                dtDocs.Columns.Add("SopVersionID");
                dtDocs.Columns.Add("Status");
                foreach (DataRow dr in dtAssignedDocs.Rows)
                {
                    if (dr["Status"].ToString() == "N" || dr["Status"].ToString() == "RN")
                    {
                        DataRow drSop = dtDocs.NewRow();
                        drSop["SopID"] = dr["DocumentID"];
                        drSop["SopVersionID"] = dr["DocVersionID"];
                        drSop["Status"] = dr["Status"];
                        dtDocs.Rows.Add(drSop);
                    }
                }
                DataTable dtTrainers = new DataTable();
                dtTrainers.TableName = "Trainers";
                dtTrainers.Columns.Add("EmpID");
                dtTrainers.Columns.Add("DeptID");
                foreach (DataRow dr in dtAssignedTrainers.Rows)
                {
                    if (dr["Status"].ToString() == "N")
                    {
                        DataRow drTrainer = dtTrainers.NewRow();
                        drTrainer["EmpID"] = dr["EmpID"];
                        drTrainer["DeptID"] = dr["DeptID"];
                        dtTrainers.Rows.Add(drTrainer);
                    }
                }
                DataTable dtDuration = new DataTable();
                dtDuration.TableName = "Durations";
                if (Operation != null)
                {
                    dtDuration = dtDeptDuration.Clone();
                    dtDuration = dtDeptDuration;
                }
                else
                {
                    dtDuration.Columns.Add("DeptID", typeof(int));
                    dtDuration.Columns.Add("Duration", typeof(int));
                    dtDuration.Merge(dtDeptDuration);
                }

                if (dtDocs.Rows.Count > 0 && dtTrainers.Rows.Count > 0)
                {
                    DataSet ds = new DataSet();
                    ds.Tables.Add(dtTrainers);
                    ds.Tables.Add(dtDocs);
                    ds.Tables.Add(dtDuration);
                    if (Operation != null)
                    {
                        objTMS_BAL.ModifyTS(ds, Convert.ToInt32(JR_ID), EmpID, txtHOD_Remarks.Text, out Result);
                        if (Result == 1)
                        {
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Training Schedule Modified Successfully ! ", "success", "JrTS_PendingList()");
                        }
                        else if (Result == 2)
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Training Schedule is not in a Status of Modification.", "info");
                        }
                        else if (Result == 3)
                        {
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "You are not Authorized User to perform action.", "info", "JrTS_PendingList()");
                        }
                        else if (Result == 4)
                        {
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), @"Not allowed to Perform any action on this JR, Since Employee <span style=\'background-color:yellow;\'>"+txtTraineeName.Text+"</span> Status is InActive. ", "error", "JrTS_PendingList()");
                        }
                    }
                    else
                    {
                        objTMS_BAL.CreateTS(ds, Convert.ToInt32(JR_ID), EmpID, ddlReviewer.SelectedValue, txtHOD_Remarks.Text, out Result);
                        if (Result == 1 || Result == 6)
                        {
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(),
                                Result == 1 ? "Training Schedule Created" : "Training Schedule is Created only on Effective Documents",
                                "success", "JrTS_PendingList()");
                        }
                        else if (Result == 2)
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Training Schedule already created for this JR.", "info");
                        }
                        else if (Result == 3)
                        {
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "You are not Authorized User to perform action.", "info", "JrTS_PendingList()");
                        }
                        else if (Result == 4)
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "There is no Effective Document/s on Selected List to Create Training Schedule.", "error");
                        }
                        else if (Result == 5)
                        {
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(),@"Not allowed to Perform any action on this JR, Since Employee <span style =\'background-color:yellow;\'>"+txtTraineeName.Text+"</span> Status is InActive.", "error", "JrTS_PendingList();");
                        }
                    }
                }
                else
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Assign Trainers and Documents to Submit the Training Schedule.", "warning");
                }
                objTMS_BAL = new TMS_BAL();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATS-24:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnCancelTS_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnCancelTS.Text == "Revert")
                {
                    if (txtRevertReason.Text.Trim() == "")
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Enter revert reason in QA remarks", "error", "focusOnRevertreason()");
                    }
                    else
                    {
                        hdnAction.Value = "Revert";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), DateTime.Now.ToString("hh.mm.ss"), "openElectronicSignModal();", true);
                    }
                }
                else
                {
                    Response.Redirect(hdnPrevipusPageURL.Value, false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATS-25:" + strline + "  " + strMsg, "error");
            }
        }
        private void SubmitApproval(string Status, string Comments = "")
        {
            try
            {
                objTMS_BAL = new TMS_BAL();
                int EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                int JR_ID = Convert.ToInt32(hdnJR_ID.Value);
                objTMS_BAL.SubmitTS_Approval(EmpID, JR_ID, Status, Comments, out int Result);
                if (Result == 6)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "You are not Authorized User to perform this action.", "info", "JrTS_ReviewList()");
                }
                else if (Result == 5)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Training Schedule is not in a status of Approval.", "info");
                }
                else if (Result == 4)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Training Schedule is already Approved.", "info", "JrTS_ReviewList()");
                }
                else if (Result == 3)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), " Training Schedule Approved Successfully ! ", "success", "JrTS_ReviewList()");
                }
                else if (Result == 2)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Training Schedule is already Reverted.", "info", "JrTS_ReviewList()");
                }
                else if (Result == 1)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), " Training Schedule Reverted Successfully ! ", "success", "JrTS_ReviewList()");
                }
                else if (Result == 7)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), @"Not allowed to Perform any action on this JR, Since Employee <span style=\'background-color:yellow;\'>" + txtTraineeName.Text + "</span> Status is InActive. ", "error", "JrTS_ReviewList()");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TS_A-8:" + strline + "  " + strMsg, "error");
            }
        }

        //Added for Retrospective code
        protected void lnkRetBtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                GridView gvDocs = gvRetAvailableDocs;
                AddRetSopsData(gvDocs, "RN");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATS-26:" + strline + "  " + strMsg, "error");
            }
        }
        private void AddRetSopsData(GridView gvDocs, string Status)
        {
            int SopCount = 0;
            foreach (GridViewRow oItemLeft in gvDocs.Rows)
            {
                if (((CheckBox)oItemLeft.FindControl("chkRow")).Checked)
                {
                    SopCount++;
                }
            }

            if (SopCount == 0)
            {
                string strMsg = "Select At-least One Document from Available Documents";
                HelpClass.custAlertMsg(this, this.GetType(), "" + strMsg + "", "error");
            }
            else
            {
                DataTable dtDocs = (DataTable)ViewState["Docs"];
                foreach (GridViewRow gvrDocs in gvDocs.Rows)
                {
                    if (((CheckBox)gvrDocs.FindControl("chkRow")).Checked)
                    {
                        int DocID = Convert.ToInt32(((Label)gvrDocs.FindControl("lblSopID")).Text);
                        int DeptID = Convert.ToInt32(((Label)gvrDocs.FindControl("lblDeptID")).Text);

                        foreach (DataRow dr in dtDocs.Rows)
                        {
                            if (Convert.ToInt32(dr["DocumentID"]) == DocID && Convert.ToInt32(dr["DeptID"]) == DeptID)
                            {
                                dr["Status"] = Status;
                            }
                        }
                    }
                }
                ViewState["Docs"] = dtDocs;
                bindDocs(dtDocs, ddlTrainingDept.SelectedValue);
            }
        }
        protected void lnkRetBtnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                RemoveRetSopsData(gvRetAssignedDocs, "Y");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ATS-28:" + strline + "  " + strMsg, "error");
            }
        }
        private void RemoveRetSopsData(GridView gvRetAssignedDocs, string Status)
        {
            int SopCount = 0;
            foreach (GridViewRow oItemLeft in gvRetAssignedDocs.Rows)
            {
                if (((CheckBox)oItemLeft.FindControl("chkRow")).Checked)
                {
                    SopCount++;
                }
            }

            if (SopCount == 0)
            {
                string strMsg = "Select At-least One Document.";
                HelpClass.custAlertMsg(this, this.GetType(), "" + strMsg + "", "error");
            }
            else
            {
                DataTable dtDocs = (DataTable)ViewState["Docs"];
                foreach (GridViewRow gvrDocs in gvRetAssignedDocs.Rows)
                {
                    if (((CheckBox)gvrDocs.FindControl("chkRow")).Checked)
                    {
                        int DocID = Convert.ToInt32(((Label)gvrDocs.FindControl("lblSopID")).Text);
                        int DeptID = Convert.ToInt32(((Label)gvrDocs.FindControl("lblDeptID")).Text);

                        foreach (DataRow dr in dtDocs.Rows)
                        {
                            if (Convert.ToInt32(dr["DocumentID"]) == DocID && Convert.ToInt32(dr["DeptID"]) == DeptID)
                            {
                                dr["Status"] = Status;
                            }
                        }
                    }
                }
                ViewState["Docs"] = dtDocs;
                bindDocs(dtDocs, ddlTrainingDept.SelectedValue);
            }
        }
    }
}