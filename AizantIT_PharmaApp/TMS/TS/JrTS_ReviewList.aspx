﻿<%@ Page Title="JR TS Review" Language="C#" MasterPageFile="~/MasterPages/TMS_Master/TMS_NestedMaster.master" AutoEventWireup="true" CodeBehind="JrTS_ReviewList.aspx.cs" Inherits="AizantIT_PharmaApp.TMS.TS.JrTS_ReviewList" %>

<%@ Register Src="~/UserControls/ElectronicSign.ascx" TagPrefix="uc1" TagName="ElectronicSign" %>
<%--<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpHdnFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField runat="server" ID="hdnEmpID" Value="0" />
            <asp:HiddenField runat="server" ID="hdnTS_IDorJR_ID" Value="0" />
            <asp:HiddenField runat="server" ID="hdnViewAt" Value="0" />
            <asp:HiddenField runat="server" ID="hdnTS_Status" Value="0" />
            <asp:HiddenField ID="hdnAction" runat="server" Value="0" />
             <asp:HiddenField runat="server" ID="hdnTraineeName" Value="" />
             <asp:HiddenField runat="server" ID="hdnNotificationID" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12  grid_panel_full padding-none">
        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div class="col-12 pull-left padding-none" id="Div3" runat="server">
                <div class="grid_header float-left col-lg-12  col-12 col-md-12 col-sm-12 ">JR Training Schedule Review List</div>
                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 top bottom padding-none">
                    <table id="tblJrTS_ReviewList" class="datatable_cust tblJrTS_ReviewListClass display breakword" style="width: 100%">
                        <thead>
                            <tr>
                                <th>TS_ID</th>
                                <th>JR_ID</th>
                                <th>JR Name</th>
                                <th>Version</th>
                                <th>Trainee</th>
                                <th>Emp.Code</th>
                                <th>Department</th>
                                <th>Dept.Code</th>
                                <th>Designation</th>
                                <th>Author</th>
                                <th>Status</th>
                                <th>StatusID</th>
                                <th>Quick View</th>
                                <th>Detail View</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modelTS_View" role="dialog">
        <div class="modal-dialog department modal-lg" style="width: 90%">
            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 0px">
                <div class="modal-header tmsModalHeader">
                   
                    <h6 class="modal-title">Training Schedule Details</h6>
                     <button type="button" id="btnTS_Close" class="close TabFocus" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body tmsModalBody">
                    <div class="tmsModalContent">
                        <asp:UpdatePanel ID="upGvModalGrids" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <ul class="nav nav-tabs tab_grid col-12 float-left">
                                    <li class="nav-item"><a data-toggle="tab" class="nav-link active" href="#Regular_TS">Regular</a></li> <%--<sup><asp:Label ID="lblRegularRecCount" runat="server"></asp:Label></sup>--%>
                                    <li class="nav-item"><a data-toggle="tab" class="nav-link" href="#Retrospective_TS">Retrospective</a></li> <%--<sup><asp:Label ID="lblRetRecCount" runat="server"></asp:Label></sup>--%>
                                </ul>
                                <div class="tab-content float-left col-12 padding-none" style="padding-top: 5px">
                                    <div id="Regular_TS" class="tab-pane fade show active">
                                        <div id="row" style="margin: 0px; padding: 3px 10px; overflow: auto; max-height: 400px;">
                                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                                <%-- <asp:UpdatePanel ID="upGvViewTS" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>--%>
                                                <asp:GridView ID="gvViewTS" runat="server" AutoGenerateColumns="False"
                                                    CssClass="table table-fixed fixview AspGrid col-12 float-left"
                                                    EmptyDataText="No Training Schedule for Approval" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                                    <AlternatingRowStyle BackColor="White" />
                                                    <RowStyle CssClass="col-12 padding-none float-left" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="S.No." HeaderStyle-CssClass="col-1 float-left" ItemStyle-CssClass="col-1 float-left">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditRowNo" runat="server" Text='<%# Bind("RowNo") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRowNo" runat="server" Text='<%# Bind("RowNo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Department Name" HeaderStyle-CssClass="col-3 float-left" ItemStyle-CssClass="col-3 float-left textAlignLeft">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditDeptName" runat="server" Text='<%# Bind("DeptName") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDeptName" runat="server" Text='<%# Bind("DeptName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Doc. Numbers" HeaderStyle-CssClass="col-3 float-left" ItemStyle-CssClass="col-3 float-left textAlignLeft">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditSopNums" runat="server" Text='<%# Bind("DocumentNums") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSopNums" runat="server" Text='<%# Bind("DocumentNums") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Trainers" HeaderStyle-CssClass="col-3 float-left" ItemStyle-CssClass="col-3 float-left textAlignLeft">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditTrainers" runat="server" Text='<%# Bind("Trainers") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTrainers" runat="server" Text='<%# Bind("Trainers") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="No.of Days" HeaderStyle-CssClass="col-2 float-left" ItemStyle-CssClass="col-2 float-left">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditDays" runat="server" Text='<%# Bind("Days") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDays" runat="server" Text='<%# Bind("Days") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                <%-- </ContentTemplate>
                                            </asp:UpdatePanel>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="Retrospective_TS" class="tab-pane fade">
                                        <div  style="margin: 0px; padding: 3px 10px; overflow: auto; max-height: 400px;">
                                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                                <%-- <asp:UpdatePanel ID="upGvViewTS" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>--%>
                                                <asp:GridView ID="gvRet_TS" runat="server" AutoGenerateColumns="False"
                                                    CssClass="table table-fixed fixRetrospective AspGrid col-12 float-left"
                                                    EmptyDataText="No Training Schedule for Approval" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle question_none">
                                                    <AlternatingRowStyle BackColor="White" />
                                                    <RowStyle CssClass="col-12 padding-none float-left" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="S.No." HeaderStyle-CssClass="col-1 float-left" ItemStyle-CssClass="col-1 float-left">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditRowNo" runat="server" Text='<%# Bind("RowNo") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRowNo" runat="server" Text='<%# Bind("RowNo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Dept Name" HeaderStyle-CssClass="col-3 float-left" ItemStyle-CssClass="col-3 float-left textAlignLeft">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditDeptName" runat="server" Text='<%# Bind("DeptName") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDeptName" runat="server" Text='<%# Bind("DeptName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Doc. Numbers" HeaderStyle-CssClass="col-8 float-left" ItemStyle-CssClass="col-8 float-left textAlignLeft">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditRQ_Sops" runat="server" Text='<%# Bind("RQ_Documents") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRQ_Sops" runat="server" Text='<%# Bind("RQ_Documents") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                <%-- </ContentTemplate>
                                            </asp:UpdatePanel>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 top" style="border-top: 1px solid #d4d4d4;">
                            <div id="divtxtComments" class="top" style="display: none">
                                <asp:UpdatePanel ID="upmodaltxtComments" runat="server" UpdateMode="Always">
                                    <ContentTemplate>

                                        <div class="form-horizontal col-sm-6 float-left padding-none" id="divHodRemarks">
                                            <div class="form-group">
                                                <label class="control-label col-sm-12">Training Coordinator Remarks</label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtHOD_Remarks" runat="server" Visible="true" Enabled="false" CssClass="form-control" MaxLength="250" placeholder="Training Coordinator Remarks" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-horizontal col-sm-6 float-left padding-none">
                                            <div class="form-group">
                                                <label class="control-label col-sm-12">QA Remarks</label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtRevertReason" runat="server" Visible="true" CssClass="form-control" MaxLength="250" placeholder="Enter QA Remarks" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer tmsModalContent">
                    <div class="top col-12 float-left" style="text-align: right">
                        <div id="divPopupbtns" style="display: none">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:LinkButton ID="lnkCloseModel" CssClass="float-right" href="#"  data-dismiss="modal" runat="server"> 
                                    <table>
                                        <tr>
                                            <td><span class="btn-cancel_popup">Cancel</span> </td>
                                        </tr>
                                    </table>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkRevertTS"  CssClass="float-right" runat="server" OnClick="lnkRevertTS_Click" OnClientClick="return RevertRequired();"> 
                                    <table>
                                        <tr><td><span class="btn-revert_popup" style="margin-right:4px">Revert</span> </td>
                                        </tr>
                                    </table>
                                    </asp:LinkButton>
                                    
                                    <asp:LinkButton ID="lnkAcceptTS" runat="server" OnClick="lnkAcceptTS_Click" CssClass="float-right">
                                     <table>
                                        <tr><td><span class="btn-signup_popup" style="margin-right:4px">Approve</span> </td>
                                        </tr>
                                     </table> 
                                    </asp:LinkButton>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnViewJrTS" runat="server" Text="ViewJR" OnClick="btnViewJrTS_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--<uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />--%>
    <uc1:ElectronicSign runat="server" ID="ElectronicSign" />
    
    <script type="text/javascript">
        $(document).ready(function () {
            // Fix up GridView to support THEAD tags            
            fixGvHeader();
            addThead();
            addTheader();
            addTheadHistory();
            // $("#gvFeedbackHistory").tablesorter({ sortList: [[1, 0]] });
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fixGvHeader();
            addThead();
            addTheader();
            addTheadHistory();
        });

        function fixGvHeader() {
            var tbl = document.getElementsByClassName("fixHead");
            if ($(".fixHead").find("thead").length == 0) {
                $(".fixHead tbody").before("<thead><tr></tr></thead>");
                $(".fixHead thead tr").append($(".fixHead th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixHead tbody tr:first").remove();
                }
            }

        }
        // Fix up GridView to support THEAD tags 
        function addThead() {
            var tbl = document.getElementsByClassName("fixview");
            if ($(".fixview").find("thead").length == 0) {
                $(".fixview tbody").before("<thead><tr></tr></thead>");
                $(".fixview thead tr").append($(".fixview th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixview tbody tr:first").remove();
                }
            }
        }
        // Fix up GridView to support THEAD tags  
        function addTheader() {
            var tbl = document.getElementsByClassName("fixRetrospective");
            if ($(".fixRetrospective").find("thead").length == 0) {
                $(".fixRetrospective tbody").before("<thead><tr></tr></thead>");
                $(".fixRetrospective thead tr").append($(".fixRetrospective th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixRetrospective tbody tr:first").remove();
                }
            }
        }
        // Fix up GridView to support THEAD tags
        function addTheadHistory() {
            var tbl = document.getElementsByClassName("fixHistory");
            if ($(".fixHistory").find("thead").length == 0) {
                $(".fixHistory tbody").before("<thead><tr></tr></thead>");
                $(".fixHistory thead tr").append($(".fixHistory th"));
                var rows = $('tr.GV_EmptyDataStyle:first', tbl);
                if (rows.length == 0) {
                    $(".fixHistory tbody tr:first").remove();
                }
            }
        }
    </script>

    <script>
        function CustomAlertmsginJrTS_RL(msg, msgType) {
            if (msgType == "success") {
                CloseModelViewTS();
                LoadJtTS_RLJQDatatable();
            }
            CloseEsModalinJrTS_RL();
            custAlertMsg(msg, msgType);
        }
        function openElectronicSignModalinJrTS_RL() {
            $('#btnTS_Close').removeClass('TabFocus');
            openUC_ElectronicSign();
        }
        function CloseEsModalinJrTS_RL() {
            $('#usES_Modal').modal('hide');
        }
        function OpenModelViewTS() {
            $('#modelTS_View').modal({ backdrop: 'static', keyboard: false });
        }
        function CloseModelViewTS() {
            $('#modelTS_View').modal('hide');
        }
        function RevertRequired() {
            if (document.getElementById("<%=txtRevertReason.ClientID%>").value.trim() == "") {
                custAlertMsg("Enter Revert Reason in QA Remarks.", "error", "CursorInRevert();");
                return false;
            }
        }
        function CursorInRevert() {
            document.getElementById("<%=txtRevertReason.ClientID%>").focus();
        }
        
    </script>

    <script>
        function ViewJrTS_ReviewList(TS_ID, TS_Status, TypeofView) {
            if (TS_Status == "1" || TS_Status == "2") {
                $("#divPopupbtns").show();
                $("#divtxtComments").show();
            }
            $("#<%=hdnViewAt.ClientID%>").val(TypeofView);
            $("#<%=hdnTS_IDorJR_ID.ClientID%>").val(TS_ID);
            $("#<%=hdnTS_Status.ClientID%>").val(TS_Status);
            $("#<%=btnViewJrTS.ClientID%>").click();
        }
    </script>

    <!--JrTS ReviewList jQuery DataTable-->
    <script>
        function getUrlVars() {
            var vars = [], hash;
            var url = window.location.href;
            var hashes = url.slice(url.indexOf('?') + 1).split('&');
            if (url.indexOf("#") > 0) {
                var hashes = url.slice(url.indexOf('?') + 1, url.indexOf('#')).split('&');
            }

            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }

        $(function () {
            LoadJtTS_RLJQDatatable();
        });

        var oTableJrTS_ReviewList;
        function LoadJtTS_RLJQDatatable() {
            if (oTableJrTS_ReviewList != null) {
                oTableJrTS_ReviewList.destroy();
            }
            oTableJrTS_ReviewList = $('#tblJrTS_ReviewList').DataTable({
                columns: [
                    { 'data': 'TS_ID' },
                    { 'data': 'JR_ID' },
                    { 'data': 'JR_Name' },
                    { 'data': 'JR_Version' },
                    { 'data': 'TraineeName' },
                    { 'data': 'EmpCode' },
                    { 'data': 'DeptName' },
                    { 'data': 'DeptCode' },
                    { 'data': 'DesignationName' },
                    { 'data': 'TS_Author' },
                    { 'data': 'StatusName' },
                    { 'data': 'StatusID' },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            return '<a class="view"  href="#" onclick="ViewJrTS_ReviewList(' + o.JR_ID + ',' + o.StatusID + ',\'' + 'ViewAtHere' + '\');"></a>';
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) { return '<a class="view"  href="#" onclick="ViewJrTS_ReviewList(' + o.TS_ID + ',' + o.StatusID + ',\'' + 'Navigate' + '\');"></a>'; }
                    }
                ],
                "order": [[1, "desc"]],
                "scrollY": "47vh",
                "aoColumnDefs": [{ "targets": [0, 1, 7, 11], "visible": false }, { className: "textAlignLeft", "targets": [2, 4, 5, 6, 8, 9, 10, 11] }],
                "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/JrTraining/JrTs/JrTS_ReviewListService.asmx/GetJrTS_ReviewList")%>",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> }, { "name": "FilterType", "value": getUrlVars()["FilterType"] }, { "name": "NotificationID", "value": <%=hdnNotificationID.Value%> });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            UserSessionCheck();
                            $("#tblJrTS_ReviewList").show();
                            $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                            $(".tblJrTS_ReviewListClass").css({ "width": "100%" });
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
                
            });
        }
        
    </script>


</asp:Content>
