﻿using Aizant_Enums;
using AizantIT_PharmaApp.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using TMS_BusinessLayer;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.TMS.TS
{
    public partial class JrTS_ReviewList : System.Web.UI.Page
    {
        TMS_BAL objBAL_TMS;
        UMS_BAL objUMS_BAL = new UMS_BAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    ElectronicSign.buttonClick += new EventHandler(ElectronicSign_buttonClick);
                    if (!IsPostBack)
                    {
                        hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID="+(int)Modules.TMS);
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.QA_TMS).Length > 0)
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                lblAutorizedMsg.Text = "Your Are Not Authorized to This Page. Contact Admin.";
                                divAutorizedMsg.Visible = true;
                                divMainContainer.Visible = false;
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Dashboards/Home.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TS_A-1:" + strline + "  " + strMsg, "error");
            }
        }

        private void InitializeThePage()
        {
            try
            {
                if (Request.QueryString["Notification_ID"] != null)
                {
                    objUMS_BAL = new UMS_BAL();
                    int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs); //2 represents NotifyEmp had visited the page through notification link.
                    hdnNotificationID.Value = Request.QueryString["Notification_ID"].ToString();
                }
                if (Session["JR_TS_Details"] != null)
                {
                    Session["JR_TS_Details"] = null;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TS_A-4:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnViewJrTS_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdnViewAt.Value == "ViewAtHere")
                {
                    int JR_ID = Convert.ToInt32(hdnTS_IDorJR_ID.Value);

                    // code to show the JR ts in this page itself.
                    objBAL_TMS = new TMS_BAL();
                    DataSet ds = objBAL_TMS.getViewTS(JR_ID);
                    hdnTraineeName.Value = ds.Tables[3].Rows[0]["TraineeName"].ToString();
                    DataTable dtTs = ds.Tables[0];
                    DataTable dtRet_TS = ds.Tables[1];
                    if (dtTs.Rows.Count > 0)
                    {
                        gvViewTS.DataSource = dtTs;
                        gvViewTS.DataBind();
                    }
                    else
                    {
                        gvViewTS.DataSource = new List<object>();
                        gvViewTS.DataBind();
                    }
                    if (dtRet_TS.Rows.Count > 0)
                    {
                        gvRet_TS.DataSource = dtRet_TS;
                        gvRet_TS.DataBind();
                    }
                    else
                    {
                        gvRet_TS.DataSource = new List<object>();
                        gvRet_TS.DataBind();
                    }
                    txtHOD_Remarks.Text = txtRevertReason.Text = string.Empty;
                    txtHOD_Remarks.Text = ds.Tables[2].Rows[0]["AuthorLastComments"].ToString();
                    upGvModalGrids.Update();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "OpenModelViewTS();", true);
            }
                else
                {
                    if (Session["JR_TS_Details"] != null)
                    {
                        Session["JR_TS_Details"] = null;
                    }
                    Hashtable ht = new Hashtable();
                    ht.Add("TS_ID", hdnTS_IDorJR_ID.Value);
                    ht.Add("TS_Status", hdnTS_Status.Value);
                    ht.Add("Previous_PageValue", "RL");
                    Session["JR_TS_Details"] = ht;
                    Response.Redirect("~/TMS/TS/AssignTS.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TS_A-4:" + strline + "  " + strMsg, "error");
            }
        }
        protected void lnkAcceptTS_Click(object sender, EventArgs e)
        {
            try
            {
                hdnAction.Value = "Approve";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "openElectronicSignModalinJrTS_RL();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TS_A-7:" + strline + "  " + strMsg, "error");
            }
        }

        private void SubmitApproval(string Status, string Comments = "")
        {
            try
            {
                objBAL_TMS = new TMS_BAL();
                int Empid = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                int JR_ID = Convert.ToInt32(hdnTS_IDorJR_ID.Value);
                objBAL_TMS.SubmitTS_Approval(Empid, JR_ID, Status, Comments, out int Result);
                string msgType = "", msg = "";
                switch (Result)
                {
                    case 1:
                        msg = "Training Schedule Reverted Successfully!";
                        msgType = "success";
                        break;
                    case 2:
                        msg = "Training Schedule is already Reverted.";
                        msgType = "info";
                        break;
                    case 3:
                        msg = "Training Schedule Approved Successfully!";
                        msgType = "success";
                        break;
                    case 4:
                        msg = "Training Schedule is already Approved.";
                        msgType = "info";
                        break;
                    case 5:
                        msg = "Unable to perform the action, Since it is not in a status of Modification.";
                        msgType = "info";
                        break;
                    case 6:
                        msg = "You are not Authorised User to perform action.";
                        msgType = "info";
                        break;
                    case 7:
                        msg = @"Not allowed to Perform any action on this JR, Since Employee <span style=\'background-color:yellow;\'>" +hdnTraineeName.Value + "</span> Status is InActive.";
                        msgType = "error";
                        break;
                    default:
                        msg = "Unable to perform the action, Contact Admin.";
                        msgType = "info";
                        break;
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "func", "CustomAlertmsginJrTS_RL('" + msg + "','" + msgType + "');", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TS_A-8:" + strline + "  " + strMsg, "error");
            }
        }

        protected void lnkRevertTS_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtRevertReason.Text.Trim() == "")
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Enter Revert Reason", "error");
                    txtRevertReason.Focus();
                }
                else
                {
                    hdnAction.Value = "Revert";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "openElectronicSignModalinJrTS_RL();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TS_A-9:" + strline + "  " + strMsg, "error");
            }
        }
        //added for electronic sign for QA Approve and Revert TS.
        protected void ElectronicSign_buttonClick(object sender, EventArgs e)
        {
            try
            {
                bool b = ElectronicSign.IsPasswordValid;
                if (b == true)
                {
                    DataTable dtAssignedTrainers = ViewState["Trainers"] as DataTable;
                    DataTable dtAssignedSOPs = ViewState["SOPs"] as DataTable;
                    DataTable dtDeptDuration = ViewState["DeptDuration"] as DataTable;
                    if (hdnAction.Value == "Approve")
                    {
                        SubmitApproval(((int)AizantEnums.JR_Status.Training_Schedule_Approved_By_QA).ToString(), txtRevertReason.Text.Trim());
                    }
                    else if (hdnAction.Value == "Revert")
                    {
                        SubmitApproval(((int)AizantEnums.JR_Status.Training_Schedule_Declined_By_QA).ToString(), txtRevertReason.Text.Trim());
                        txtRevertReason.Text = string.Empty;
                    }

                }
                if (b == false)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Password is Incorrect!", "error");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "TS_A-3:" + strline + "  " + strMsg, "error");
            }
        }
        
    }
}