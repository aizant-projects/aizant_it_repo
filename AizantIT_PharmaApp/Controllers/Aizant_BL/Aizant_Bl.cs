﻿using Aizant_API_Entities;
using AizantIT_PharmaApp.UserManagement.UMSModel;
using QMS_BO.QMSComonBO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UMS_BusinessLayer;

namespace AizantIT_PharmaApp.Controllers.Aizant_BL
{
    public class Aizant_Bl
    {
     public   AizantIT_DevEntities dbAizantIT_Dev = new AizantIT_DevEntities();
        //role Based Employees
        public List<SelectListItem> GetListofEmpByRole(int[] ExceptEmpID = null, int RoleID = 0, string EmpStatus = "", string DefaultSelectionEmpID = "", string NonSelectionText = "")
        {
            try
            {
                if(EmpStatus=="" || EmpStatus==null)
                {
                    EmpStatus = "A";
                }
                List<SelectListItem> EmployeeList = new List<SelectListItem>();
                var EmployeesItemsList = dbAizantIT_Dev.AizantIT_SP_GetEmployeesByRoleIDAndStatus(RoleID, EmpStatus);
                if (NonSelectionText != "")
                {
                    EmployeeList.Add(new SelectListItem
                    {
                        Text = NonSelectionText,
                        Value = "0",
                    });
                }
                foreach (var EmployeeItem in EmployeesItemsList)
                {                   
                        EmployeeList.Add(new SelectListItem
                        {
                            Text = EmployeeItem.EmpName,
                            Value = EmployeeItem.EmpID.ToString(),
                            Selected = EmployeeItem.EmpID.ToString() == DefaultSelectionEmpID ? true : false
                        });                   
                }
                if (ExceptEmpID != null)
                {
                    foreach (var item in ExceptEmpID)
                    {
                        var itemToRemove = EmployeeList.SingleOrDefault(r => r.Value == item.ToString());
                        if (itemToRemove != null)
                        {
                            EmployeeList.Remove(itemToRemove);
                        }
                    }
                }
                return EmployeeList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Dept Based Employees
        public List<SelectListItem> GetListofEmpByDeptIDRoleIDAndStatus(int[] ExceptEmpID=null, int DeptID = 0, int RoleID = 0, string EmpStatus = "", string DefaultSelectionEmpID = "",string NonSelectionText="")
        {
            try
            {
                List<SelectListItem> EmployeeList = new List<SelectListItem>();
                bool IsOnlyActiveEmployees = false;
                if (EmpStatus == "A")
                {
                    IsOnlyActiveEmployees = true;
                }
                if (NonSelectionText != "")
                {
                    EmployeeList.Add(new SelectListItem
                    {
                        Text = NonSelectionText,
                        Value = "0",
                    });
                }
                UMS_BAL objUMS_Bal = new UMS_BAL();
                DataTable EmployeesItemsList = objUMS_Bal.GetEmployeesByDeptIDandRoleID_BAL(DeptID, RoleID, IsOnlyActiveEmployees);
                foreach (DataRow drEmpItem in EmployeesItemsList.Rows)
                {
                    EmployeeList.Add(new SelectListItem
                    {
                        Text = drEmpItem["EmpName"].ToString(),
                        Value = drEmpItem["EmpID"].ToString(),
                        Selected = drEmpItem["EmpID"].ToString() == DefaultSelectionEmpID ? true : false
                    });
                }
                if (ExceptEmpID!=null)
                {
                    foreach (var item in ExceptEmpID)
                    {
                        var itemToRemove = EmployeeList.SingleOrDefault(r => r.Value == item.ToString());
                        if (itemToRemove != null)
                        {
                            EmployeeList.Remove(itemToRemove);
                        }
                    }
                }              
                return EmployeeList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Departmets List
        public List<SelectListItem> GetDepartmentsList()
        {
            List<SelectListItem> DepartmentLists = new List<SelectListItem>();

            UMS_BAL objUMS_BAL = new UMS_BAL();
            DataTable dt = objUMS_BAL.BindDepartmentsBAL();
            foreach (DataRow drEmpItem in dt.Rows)
            {
                DepartmentLists.Add(new SelectListItem
                {
                    Text = drEmpItem["DepartmentName"].ToString(),
                    Value = drEmpItem["DeptID"].ToString(),
                   
                });
            }
            return DepartmentLists;

        }

        public List<SelectListItem> BindCountries(string DefaultSelection)
        {
            List<SelectListItem> CountriesList = new List<SelectListItem>();
            try
            {
                var CountriesItemsList = dbAizantIT_Dev.AizantIT_SP_GetCountries();
                CountriesList.Add(new SelectListItem
                {
                    Text = "--Select Country--",
                    Value = "0",
                });

                foreach (var Item in CountriesItemsList)
                {
                    CountriesList.Add(new SelectListItem
                    {
                        Text = Item.CountryName,
                        Value = Item.CountryID.ToString(),
                        Selected = Item.CountryID.ToString() == DefaultSelection ? true : false
                    });

                }
                return CountriesList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
      
}
        public List<SelectListItem> BindCountryWiseStatesList(string DefaultSelection, string CountryID)
        {
            List<SelectListItem> StatesList = new List<SelectListItem>();
            try
            {
                var StatesItemsList = dbAizantIT_Dev.AizantIT_SP_GetStates(Convert.ToInt32(CountryID));
                StatesList.Add(new SelectListItem
                {
                    Text = "--Select State--",
                    Value = "0",
                });

                foreach (var Item in StatesItemsList)
                {
                    StatesList.Add(new SelectListItem
                    {
                        Text = Item.StateName,
                        Value = Item.StateID.ToString(),
                        Selected = Item.StateID.ToString() == DefaultSelection ? true : false
                    });

                }
                return StatesList;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        public List<SelectListItem> BindStateWiseCityList(string DefaultSelection, string StateID)
        {
            List<SelectListItem> CitysList = new List<SelectListItem>();
            try
            {

                var StatesItemsList = dbAizantIT_Dev.AizantIT_SP_GetCitys(Convert.ToInt32(StateID));
                CitysList.Add(new SelectListItem
                {
                    Text = "--Select City--",
                    Value = "0",
                });

                foreach (var Item in StatesItemsList)
                {
                    CitysList.Add(new SelectListItem
                    {
                        Text = Item.CityName,
                        Value = Item.CityID.ToString(),
                        Selected = Item.CityID.ToString() == DefaultSelection ? true : false
                    });

                }
                //var StateId = Convert.ToInt32(StateID);
                //var cities = dbAizantIT_Dev.AizantIT_Cities.Where(c => c.StateID == StateId).ToList();
                //CitysList.Add(new SelectListItem { Text = "--Select City--", Value = "0" });
                //foreach (var city in cities)
                //{
                //    CitysList.Add(new SelectListItem
                //    {
                //        Text = city.CityName,
                //        Value = city.CityID.ToString(),
                //        Selected = city.CityID == Convert.ToInt32(DefaultSelectionEmpID) ? true : false
                //    });
                //}
                return CitysList;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

    }
}