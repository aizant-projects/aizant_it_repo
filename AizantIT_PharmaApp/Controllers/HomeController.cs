﻿//using AizantIT_PharmaApp.Areas.PRODUCT.Models.ViewModels;
using AizantIT_PharmaApp.Controllers.Aizant_BL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.Controllers
{
    public class HomeController : Controller
    {
        API_BussinessObjects.LandingPageBO landingPageBO = new API_BussinessObjects.LandingPageBO();
        API_BusinessLayer.LandingPageBAL landingPageBAL = new API_BusinessLayer.LandingPageBAL();
        // GET: Home
        public ActionResult Index()
        {
            //return View();
            return Redirect("~/Dashboards/Home.aspx");
        }
        //Department Binding
        public ActionResult GetDepartmentsList()
        {
         Aizant_Bl aizant_Bl = new Aizant_Bl();
         var DeptList=aizant_Bl.GetDepartmentsList();
        return Json(DeptList, JsonRequestBehavior.AllowGet);
        }

        //Country Binding
        public ActionResult GetCountrysList()
        {
            Aizant_Bl aizant_Bl = new Aizant_Bl();
            var CountrysList = aizant_Bl.BindCountries(null);
            return Json(CountrysList, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult GetStatesList(string CountryID)
        {
            Aizant_Bl aizant_Bl = new Aizant_Bl();
            var StatestList = aizant_Bl.BindCountryWiseStatesList(null, CountryID);
            return Json(StatestList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCitysList(string StateID)
        {
            Aizant_Bl aizant_Bl = new Aizant_Bl();
            var CitysList = aizant_Bl.BindStateWiseCityList(null, StateID);
            return Json(CitysList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Home()
        {
            //Maintain Module Roles
            //TempData.Keep("EmpModuleRoles");
            try
            {
                DataSet UserDetails = Session["UserDetails"] as DataSet;
                string ShowPwdExpireMsg = Session["ShowPwdExpireMsg"].ToString();               

                for (int i = 0; i < 2; i++)
                {
                    landingPageBO = landingPageBAL.HomePage(UserDetails, ShowPwdExpireMsg,i);
                }

                //if (ds.Tables[0].Rows.Count > 0)
                //{
                //    if (Session["ShowPwdExpireMsg"] != null)
                //    {
                //        int EmpID = Convert.ToInt32(ds.Tables[0].Rows[0]["EmpID"]);
                //        DataSet ds1 = UMBAL.ShowExpireDate(EmpID, out int RemainingCount, out int Status);
                //        string expireMsg = "";
                //        if (Status == 1)
                //        {
                //            expireMsg = "Your password will expire in " + (RemainingCount == 1 ? RemainingCount + " day" : RemainingCount + " days") + ", Please reset your password.";
                //        }

                //        landingPage.ExpireMessage = expireMsg;
                //    }
                //}
                //if(ds.Tables[1].Rows.Count > 0)
                //{
                //    int ModuleRoleCount = 0;
                //    DataTable dtUserModuleRoles = ds.Tables[1];
                //    DataRow[] drUMS = dtUserModuleRoles.Select("ModuleID=1");
                //    if (drUMS.Length > 0)
                //    {
                //        landingPage.ShowUMSModule = true;
                //        ModuleRoleCount += 1;
                //        DataTable dtx = ds.Tables[1];
                //        DataTable dtTemp = new DataTable();
                //        DataRow[] drUMS_Roles = dtx.Select("ModuleID=" + (int)Modules.AdminSetup);
                //        dtTemp = drUMS_Roles.CopyToDataTable();
                //        if ((drUMS_Roles.Length == 2) && (((dtTemp.Select("RoleID=" + (int)AdminSetup_UserRole.GlobalAdmin_UMS).Length > 0)) && ((dtTemp.Select("RoleID=" + (int)AdminSetup_UserRole.ProductAdmin).Length > 0))))
                //        {
                //            landingPage.UMSNavigateUrl = "~/AdminSetup/BusinessDivisionAndSite/BusinessDivision";
                //        }
                //        else if ((drUMS_Roles.Length == 1) && ((dtTemp.Select("RoleID=" + (int)AdminSetup_UserRole.GlobalAdmin_UMS).Length > 0)))// Has only Global Admin Role
                //        {
                //            landingPage.UMSNavigateUrl = "~/AdminSetup/BusinessDivisionAndSite/BusinessDivision";
                //        }
                //        else if ((drUMS_Roles.Length == 1) && ((dtTemp.Select("RoleID=" + (int)AdminSetup_UserRole.ProductAdmin).Length > 0)))
                //        {
                //            landingPage.UMSNavigateUrl = "~/AdminSetup/Product/Product";
                //        }
                //    }
                //    DataRow[] drDMS = dtUserModuleRoles.Select("ModuleID=2");
                //    if (drDMS.Length > 0)
                //    {
                //        landingPage.ShowDMSModule= true;
                //        ModuleRoleCount += 1;
                //    }
                //    DataRow[] drTMS = dtUserModuleRoles.Select("ModuleID=3");
                //    if (drTMS.Length > 0)
                //    {
                //        landingPage.ShowTMSModule = true;
                //        ModuleRoleCount += 1;
                //    }
                //    DataRow[] drQMS = dtUserModuleRoles.Select("ModuleID=4");
                //    if (drQMS.Length > 0)
                //    {
                //        landingPage.ShowQMSModule = true;

                //        //Checking weather User had only Audit Viewer and Admin role in QMS Module if has redirecting to main list
                //        DataTable dtx = ds.Tables[1];
                //        DataTable dtTemp = new DataTable();
                //        DataRow[] drQMSRoles = dtx.Select("ModuleID=" + (int)Modules.QMS);
                //        dtTemp = drQMSRoles.CopyToDataTable();
                //        if (((drQMSRoles.Length == 2) && (dtTemp.Select("RoleID=" + (int)QMS_Audit_Roles.AuditViewer).Length > 0) && (dtTemp.Select("RoleID=" + (int)QMS_Audit_Roles.AuditAdmin).Length > 0))
                //            ||
                //            ((drQMSRoles.Length == 1) && ((dtTemp.Select("RoleID=" + (int)QMS_Audit_Roles.AuditAdmin).Length > 0)))
                //            )
                //        {
                //            landingPage.QMSNavigateUrl = "~/QMS/AuditMaster/AuditMasterList";
                //        }
                //        else if (((drQMSRoles.Length == 1) && ((dtTemp.Select("RoleID=" + (int)QMS_Audit_Roles.AuditViewer).Length > 0))))// Has only Viewer Role
                //        {
                //            landingPage.QMSNavigateUrl = "~/QMS/AuditMain/InitAuditReportList?listType=AuditReportMain";
                //        }
                //        ModuleRoleCount += 1;
                //    }
                //    DataRow[] drPMS = dtUserModuleRoles.Select("ModuleID=5");
                //    if (drPMS.Length > 0)
                //    {
                //        landingPage.ShowPMSModule = true;
                //        ModuleRoleCount += 1;
                //    }
                //    DataRow[] drVMS = dtUserModuleRoles.Select("ModuleID=6");
                //    if (drVMS.Length > 0)
                //    {
                //        landingPage.ShowVMSModule = true;
                //        ModuleRoleCount += 1;
                //    }                    
                //    DataRow[] drRA = dtUserModuleRoles.Select("ModuleID=7");
                //    if (drRA.Length > 0)
                //    {
                //        landingPage.ShowRMSModule = true;
                //        DataTable dtx = ds.Tables[1];
                //        DataTable dtTemp = new DataTable();
                //        DataRow[] drRARoles = dtx.Select("ModuleID=" + (int)Modules.RA);
                //        dtTemp = drRARoles.CopyToDataTable();
                //        if (((drRARoles.Length == 1) && ((dtTemp.Select("RoleID=" + (int)RA_UserRole.RA_Admin).Length > 0))))// Has only Admin Role
                //        {
                //            landingPage.RMSNavigateUrl = "~/Regulatory/RegulatoryMaster/RegulatoryMasterList";
                //        }
                //        else
                //        {
                //            landingPage.RMSNavigateUrl = "~/Regulatory/RegulatoryMain/RegulatoryDashBoard";
                //        }

                //        ModuleRoleCount += 1;
                //    }
                //    landingPage.ModuleCount = ModuleRoleCount;
                //}
                   
                    return View(landingPageBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}