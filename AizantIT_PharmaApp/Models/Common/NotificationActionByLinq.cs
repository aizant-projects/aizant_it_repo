﻿using Aizant_API_Entities;
//using AizantIT_PharmaApp.Models.UMS;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.Models.Common
{
    public class NotificationActionByLinq
    {
        public int InsertNotifications(NotificationBO objNotifications)
        {
            try
            {
                using (AizantIT_DevEntities dbNotification = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbNotification.Database.BeginTransaction())
                    {
                        try
                        {
                            AizantIT_NotificationMaster objNotificationMasterBO = new AizantIT_NotificationMaster();
                            objNotificationMasterBO.Description = objNotifications.Description;
                            objNotificationMasterBO.Module = objNotifications.ModuleID;
                            objNotificationMasterBO.GeneratedDate = DateTime.Now;
                            objNotificationMasterBO.NotificationLink = objNotifications.NotificationLink;
                            dbNotification.AizantIT_NotificationMaster.Add(objNotificationMasterBO);
                            dbNotification.SaveChanges();
                            objNotifications.NotificationID = objNotificationMasterBO.NotificationID;

                            AizantIT_NotifyTo objNotifyToBO = new AizantIT_NotifyTo();
                            objNotifyToBO.NotificationID = objNotificationMasterBO.NotificationID;
                            objNotifyToBO.NotifyTo = objNotifications.NotifyToStatus; //Represents : 1-> All the Employees, 2->Particular Employees In the particular role,3-> Particular Employees.
                            dbNotification.AizantIT_NotifyTo.Add(objNotifyToBO);

                            if (objNotifications.NotifyToStatus == 2 && objNotifications.NotificationRoleIDSandDeptIDs.Count != 0)
                            {
                                List<AizantIT_RoleNotifications> objRoleNotificationBO = new List<AizantIT_RoleNotifications>();
                                foreach (var NotificationRoleandDeptID in objNotifications.NotificationRoleIDSandDeptIDs)
                                {
                                    if (NotificationRoleandDeptID.RoleID != 0)
                                    {
                                        AizantIT_Roles objRoles = dbNotification.AizantIT_Roles.SingleOrDefault(p => p.RoleID == NotificationRoleandDeptID.RoleID);
                                        AizantIT_RoleNotifications objRoleNotification = new AizantIT_RoleNotifications();
                                        objRoleNotification.NotificationID = objNotifications.NotificationID;
                                        objRoleNotification.RoleID = NotificationRoleandDeptID.RoleID;
                                        if (NotificationRoleandDeptID.DeptID != 0 && objRoles.IsAccessibleDepartments)
                                        {
                                            objRoleNotification.DeptID = NotificationRoleandDeptID.DeptID;
                                        }
                                        else
                                        {
                                            objRoleNotification.DeptID = null;
                                        }
                                        objRoleNotificationBO.Add(objRoleNotification);
                                    }
                                }
                                dbNotification.AizantIT_RoleNotifications.AddRange(objRoleNotificationBO);
                            }

                            List<AizantIT_UserNotifications> objUserNotificationBO = new List<AizantIT_UserNotifications>();
                            foreach (var NotificationEmpID in objNotifications.NotificationEmpIDS)
                            {
                                if (NotificationEmpID != 0)
                                {
                                    AizantIT_UserNotifications objUserNoti = new AizantIT_UserNotifications();
                                    objUserNoti.NotificationID = objNotifications.NotificationID;
                                    objUserNoti.EmpID = NotificationEmpID;
                                    objUserNoti.Status = objNotifications.NotificationStatus;
                                    objUserNotificationBO.Add(objUserNoti);
                                }
                            }
                            dbNotification.AizantIT_UserNotifications.AddRange(objUserNotificationBO);
                            dbNotification.SaveChanges();
                            transaction.Commit();
                            return objNotifications.NotificationID;
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void NotificationToNextEmp(NotificationBO objNotifications)
        {
            try
            {
                using (AizantIT_DevEntities dbNotification = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbNotification.Database.BeginTransaction())
                    {
                        try
                        {
                            AizantIT_NotificationMaster objNotif_MasterBO = dbNotification.AizantIT_NotificationMaster.SingleOrDefault(p => p.NotificationID == objNotifications.NotificationID);
                            objNotif_MasterBO.Description = objNotifications.Description;
                            objNotif_MasterBO.NotificationLink = objNotifications.NotificationLink;
                            objNotif_MasterBO.Module = objNotifications.ModuleID;
                            objNotif_MasterBO.GeneratedDate = DateTime.Now;
                            //dbNotification.SaveChanges();

                            AizantIT_NotifyTo objNotifyToBO = dbNotification.AizantIT_NotifyTo.SingleOrDefault(p => p.NotificationID == objNotifications.NotificationID);
                            objNotifyToBO.NotifyTo = objNotifications.NotifyToStatus;
                            //To remove the previous records for this NotificationID from RoleNotifications
                            dbNotification.AizantIT_RoleNotifications.RemoveRange(dbNotification.AizantIT_RoleNotifications
                                .Where(p => p.NotificationID == objNotifications.NotificationID));
                            if (objNotifications.NotifyToStatus == 2 && objNotifications.NotificationRoleIDSandDeptIDs.Count != 0)
                            {
                                List<AizantIT_RoleNotifications> objRoleNotificationBO = new List<AizantIT_RoleNotifications>();
                                foreach (var NotificationRoleandDeptID in objNotifications.NotificationRoleIDSandDeptIDs)
                                {
                                    if (NotificationRoleandDeptID.RoleID != 0)
                                    {
                                        AizantIT_Roles objRoles = dbNotification.AizantIT_Roles.SingleOrDefault(p => p.RoleID == NotificationRoleandDeptID.RoleID);
                                        AizantIT_RoleNotifications objRoleNotification = new AizantIT_RoleNotifications();
                                        objRoleNotification.NotificationID = objNotifications.NotificationID;
                                        objRoleNotification.RoleID = NotificationRoleandDeptID.RoleID;
                                        if (NotificationRoleandDeptID.DeptID != 0 && objRoles.IsAccessibleDepartments)
                                        {
                                            objRoleNotification.DeptID = NotificationRoleandDeptID.DeptID;
                                        }
                                        else
                                        {
                                            objRoleNotification.DeptID = null;
                                        }
                                        objRoleNotificationBO.Add(objRoleNotification);
                                    }
                                }
                                dbNotification.AizantIT_RoleNotifications.AddRange(objRoleNotificationBO);
                            }

                            //To remove the previous records for this NotificationID
                            dbNotification.AizantIT_UserNotifications.RemoveRange(dbNotification.AizantIT_UserNotifications
                                .Where(p => p.NotificationID == objNotifications.NotificationID));
                            List<AizantIT_UserNotifications> objUserNotif_BO = new List<AizantIT_UserNotifications>();
                            foreach (var NotificationEmpID in objNotifications.NotificationEmpIDS)
                            {
                                if (NotificationEmpID != 0)
                                {
                                    AizantIT_UserNotifications objUserNoti = new AizantIT_UserNotifications();
                                    objUserNoti.NotificationID = objNotifications.NotificationID;
                                    objUserNoti.EmpID = NotificationEmpID;
                                    objUserNoti.Status = objNotifications.NotificationStatus;
                                    objUserNotif_BO.Add(objUserNoti);
                                }
                            }
                            dbNotification.AizantIT_UserNotifications.AddRange(objUserNotif_BO);
                            dbNotification.SaveChanges();
                            transaction.Commit();
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void DeleteNotification(int[] aryNotificationID)
        {
            try
            {
                using (AizantIT_DevEntities dbNotification = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbNotification.Database.BeginTransaction())
                    {
                        try
                        {
                            dbNotification.AizantIT_UserNotifications.RemoveRange(
                            dbNotification.AizantIT_UserNotifications.Where(p => aryNotificationID.Contains(p.NotificationID)));
                            dbNotification.AizantIT_RoleNotifications.RemoveRange(dbNotification.AizantIT_RoleNotifications.Where(p => aryNotificationID.Contains(p.NotificationID)));
                            dbNotification.AizantIT_NotifyTo.RemoveRange(
                                dbNotification.AizantIT_NotifyTo.Where(p => aryNotificationID.Contains(p.NotificationID)));
                            dbNotification.AizantIT_NotificationMaster.RemoveRange(
                                dbNotification.AizantIT_NotificationMaster.Where(p => aryNotificationID.Contains(p.NotificationID)));
                            dbNotification.SaveChanges();
                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void UpdateNotificationStatus(int NotificationID, int[] aryEmpID, int Status)// Status : 1) Newly created or not visited,2) Visited the Notification,3) Closed the Notification
        {
            try
            {
                using (AizantIT_DevEntities dbNotification = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbNotification.Database.BeginTransaction())
                    {
                        try
                        {
                            dbNotification.AizantIT_UserNotifications
                            .Where(p => p.NotificationID == NotificationID && aryEmpID.Contains(p.EmpID)).ToList().ForEach(o =>
                            {
                                o.Status = Status;
                            });
                            dbNotification.SaveChanges();
                            transaction.Commit();
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        //For Updating the Notifications Status irrespective of Employees (System Closed the Notification)
        public void UpdateNotificationStatus(int NotificationID, int Status)
        {
            try
            {
                using (AizantIT_DevEntities dbNotification = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbNotification.Database.BeginTransaction())
                    {
                        try
                        {
                            dbNotification.AizantIT_UserNotifications
                            .Where(p => p.NotificationID == NotificationID).ToList().ForEach(o =>
                            {
                                o.Status = Status;
                            });
                            dbNotification.SaveChanges();
                            transaction.Commit();
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        //For Updating the NotificationEmpIDs
        public void UpdateNotificationEmps(int NotificationID, int[] NewEmpIDs, int Status)
        {
            try
            {
                using (AizantIT_DevEntities dbNotification = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbNotification.Database.BeginTransaction())
                    {
                        try
                        {
                            dbNotification.AizantIT_UserNotifications.RemoveRange(
                            dbNotification.AizantIT_UserNotifications.Where(p => !NewEmpIDs.Contains(p.EmpID) && p.NotificationID== NotificationID));

                            List<AizantIT_UserNotifications> objRoleNotificationBO = new List<AizantIT_UserNotifications>();
                            foreach (var item in NewEmpIDs)
                            {
                                if (!(from itemAuditCategory in dbNotification.AizantIT_UserNotifications
                                     where itemAuditCategory.NotificationID == NotificationID
                                      && itemAuditCategory.EmpID == item
                                     select new { itemAuditCategory.EmpID }).Any())
                                {
                                    AizantIT_UserNotifications objRoleNotification = new AizantIT_UserNotifications();
                                    objRoleNotification.NotificationID = NotificationID;
                                    objRoleNotification.EmpID = item;
                                    objRoleNotification.Status =1;
                                    objRoleNotificationBO.Add(objRoleNotification);
                                }
                            }
                            dbNotification.AizantIT_UserNotifications.AddRange(objRoleNotificationBO);
                           
                            dbNotification.SaveChanges();
                            transaction.Commit();
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        //For Updating the Multiple  NotificationEmpIDs
        public void UpdateNotificationEmps(List<AizantIT_UserNotifications> objNotifications)
        {
            try
            {
                using (AizantIT_DevEntities dbNotification = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbNotification.Database.BeginTransaction())
                    {
                        int[] NotificationIDs = objNotifications.Select(a => a.NotificationID).ToArray();
                        try
                        {
                            dbNotification.AizantIT_UserNotifications.RemoveRange(
                            dbNotification.AizantIT_UserNotifications.Where(p => NotificationIDs.Contains(p.NotificationID)));
                            dbNotification.AizantIT_UserNotifications.AddRange(objNotifications);
                            dbNotification.SaveChanges();
                            transaction.Commit();
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}