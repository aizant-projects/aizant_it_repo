﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.Models.Common
{
    public class NotificationBO
    {
        public int NotificationID { get; set; }
        public string Description { get; set; }
        public string NotificationLink { get; set; }
        public int ModuleID { get; set; }
        public int NotificationStatus { get; set; } = 1; // 1 for new notification, 2 for Visited, 3 for closed.
        public int NotifyToStatus { get; set; } = 2;// 1 for all employees, 2 for particular employees in particular role, 3 ony to particular emloyees but not role.
        public List<int> NotificationEmpIDS { get; set; }
        public List<NotificationRoleIDandDeptID> NotificationRoleIDSandDeptIDs { get; set; }
    }
    public class NotificationRoleIDandDeptID
    {
        public int RoleID { get; set; }
        public int DeptID { get; set; }
    }
}