﻿using Aizant_API_Entities;
using AizantIT_PharmaApp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AizantIT_PharmaApp.Areas.PRODUCT.Models.ModelProductList;
using System.IO;

namespace AizantIT_PharmaApp.ProductViewers
{
    public partial class AttachmentViewer : System.Web.UI.Page
    {
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //private AizantIT_QMS_EF db = new AizantIT_QMS_EF();
        private AizantIT_DevEntities db = new AizantIT_DevEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        if (Request.QueryString.Count > 0)
                        {
                            string EventID = Request.QueryString[0];
                            string FID = Request.QueryString[1];
                            if (EventID == "TRPT")
                            {
                                ViewTempReport(FID);
                            }
                            else
                            {
                                ToViewReferralDocument(FID, EventID);
                            }
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Uclose", "CloseBrowser();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("QMSIF_M1:" + strline + "  " + strMsg);
                string strError = "QMSIF_M1:" + strline + "  " + "Page loading failed";
                literalSop.Visible = true;
                Spreadsheet.Visible = false;
                RichEdit.Visible = false;
                binImage.Visible = false;
                literalSop.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }

        public void ToViewReferralDocument(string FID, string EventID)
        {
            ViewDocumnetModel obj = new ViewDocumnetModel();
            int ID = Convert.ToInt32(FID);
            string fname = string.Empty, FileExtn = string.Empty; byte[] filecontent = null;
            if (EventID == "1")//Incident
            {
                var DocFiles = (from a in db.AizantIT_ProdAttachments
                                where a.PKID == ID
                                select new { a.ProductID, a.FileType, a.FileName, a.AttachamentBody }).ToList();

                foreach (var item in DocFiles)
                {

                    filecontent = item.AttachamentBody;
                    FileExtn = item.FileType;
                    fname = item.FileName;

                }
            }
            if (filecontent == null || filecontent.Length == 0)
            {
                literalSop.Visible = true;
                Spreadsheet.Visible = false;
                RichEdit.Visible = false;
                binImage.Visible = false;
                literalSop.Text = "<p style='color: red; '><b>File is not available</b></p>";
                return;
            }
            switch (FileExtn)
            {
                case ".jpeg":
                    binImage.Visible = true;
                    Spreadsheet.Visible = false;
                    RichEdit.Visible = false;
                    literalSop.Visible = false;
                    binImage.ContentBytes = filecontent;
                    break;
                case ".jpg":
                    binImage.Visible = true;
                    Spreadsheet.Visible = false;
                    RichEdit.Visible = false;
                    literalSop.Visible = false;
                    binImage.ContentBytes = filecontent;
                    break;
                case ".png":
                    binImage.Visible = true;
                    Spreadsheet.Visible = false;
                    RichEdit.Visible = false;
                    literalSop.Visible = false;
                    binImage.ContentBytes = filecontent;
                    break;
                case ".gif":
                    binImage.Visible = true;
                    Spreadsheet.Visible = false;
                    RichEdit.Visible = false;
                    literalSop.Visible = false;
                    binImage.ContentBytes = filecontent;
                    break;
                case ".bmp":
                    binImage.Visible = true;
                    Spreadsheet.Visible = false;
                    RichEdit.Visible = false;
                    literalSop.Visible = false;
                    binImage.ContentBytes = filecontent;
                    break;
                case ".pdf":
                    literalSop.Visible = true;
                    Spreadsheet.Visible = false;
                    RichEdit.Visible = false;
                    binImage.Visible = false;
                    filecontent = null;
                    literalSop.Text = " <embed src=\"" + ResolveUrl("~/ASPXHandlers/QMSPDFViewHandler.ashx") + "?EventID=" + EventID + "&DOCID=" + ID + " #toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" height=\"661px\" />";
                    break;
                case ".xls":
                    Spreadsheet.Visible = true;
                    RichEdit.Visible = false;
                    binImage.Visible = false;
                    literalSop.Visible = false;
                    Spreadsheet.Open(
                       Guid.NewGuid().ToString(),
                       DevExpress.Spreadsheet.DocumentFormat.Xls,
                       () =>
                       {
                           byte[] docBytes = filecontent;
                           return new MemoryStream(docBytes);
                       }
                       );
                    break;
                case ".xlsx":
                    Spreadsheet.Visible = true;
                    RichEdit.Visible = false;
                    binImage.Visible = false;
                    literalSop.Visible = false;
                    Spreadsheet.Open(
                        Guid.NewGuid().ToString(),
                        DevExpress.Spreadsheet.DocumentFormat.Xlsx,
                        () =>
                        {
                            byte[] docBytes = filecontent;
                            return new MemoryStream(docBytes);
                        }
                        );
                    break;
                case ".rtf":
                    RichEdit.Visible = true;
                    Spreadsheet.Visible = false;
                    binImage.Visible = false;
                    literalSop.Visible = false;
                    RichEdit.Open(
                       Guid.NewGuid().ToString(),
                       DevExpress.XtraRichEdit.DocumentFormat.Rtf,
                       () =>
                       {
                           byte[] docBytes = filecontent;
                           return new MemoryStream(docBytes);
                       }
                       );
                    break;
                case ".doc":
                    RichEdit.Visible = true;
                    Spreadsheet.Visible = false;
                    binImage.Visible = false;
                    literalSop.Visible = false;
                    RichEdit.Open(
                      Guid.NewGuid().ToString(),
                      DevExpress.XtraRichEdit.DocumentFormat.Doc,
                      () =>
                      {
                          byte[] docBytes = filecontent;
                          return new MemoryStream(docBytes);
                      }
                      );
                    break;
                case ".docx":
                    RichEdit.Visible = true;
                    Spreadsheet.Visible = false;
                    binImage.Visible = false;
                    literalSop.Visible = false;
                    RichEdit.Open(
                      Guid.NewGuid().ToString(),
                      DevExpress.XtraRichEdit.DocumentFormat.OpenXml,
                      () =>
                      {
                          byte[] docBytes = filecontent;
                          return new MemoryStream(docBytes);
                      }
                      );
                    break;
                case ".txt":
                    RichEdit.Visible = true;
                    Spreadsheet.Visible = false;
                    binImage.Visible = false;
                    literalSop.Visible = false;
                    RichEdit.Open(
                      Guid.NewGuid().ToString(),
                      DevExpress.XtraRichEdit.DocumentFormat.PlainText,
                      () =>
                      {
                          byte[] docBytes = filecontent;
                          return new MemoryStream(docBytes);
                      }
                      );
                    break;
            }
        }

        public void ViewTempReport(string fName)
        {
            literalSop.Visible = true;
            Spreadsheet.Visible = false;
            RichEdit.Visible = false;
            binImage.Visible = false;

            literalSop.Text = " <embed src=\"" + ResolveUrl("~/ASPXHandlers/QMSPDFViewHandler.ashx") + "?EventID=TRPT&DOCID=" + fName + "  #toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" height=\"661px\" />";
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}