﻿using System;
using System.Data;
using System.Data.SqlClient;

using System.Web.UI;
using VMS_BAL;
using VMS_BO;
using AizantIT_PharmaApp.Common;
using DevExpress.XtraReports.UI;
using System.IO;

using AizantIT_PharmaApp.VMS.Reports;
using DevExpress.XtraPrinting;

namespace AizantIT_PharmaApp.VMS.DevReports
{
    public partial class frmDocumentViewer : System.Web.UI.Page
    {
        VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString.Count == 1)
                    PrintVisitorPass(Convert.ToInt32(Request.QueryString[0]));
                if (Request.QueryString.Count == 2)
                    PrintOutwardMaterialReport(Convert.ToInt32(Request.QueryString[1]));
            }
        }
        public void PrintVisitorPass(int Vid)
        {
            try
            {
                VisitorRegisterBO objvisitorRegisterBO = new VisitorRegisterBO();
                objvisitorRegisterBO.DeptName = string.Empty;
                objvisitorRegisterBO.Purpose_of_VisitID = 0;
                objvisitorRegisterBO.FirstName = string.Empty;
                objvisitorRegisterBO.VisitorID = string.Empty;
                objvisitorRegisterBO.MobileNo = string.Empty;
                objvisitorRegisterBO.Organization = string.Empty;
                objvisitorRegisterBO.IDCardNo = string.Empty;
                objvisitorRegisterBO.InDateTime = string.Empty;
                objvisitorRegisterBO.OutDateTime = string.Empty;
                DataTable dt = new DataTable();
                objvisitorRegisterBO.SNo = Vid;
                dt = objVisitorRegisterBAL.SearchFilterVisitor(objvisitorRegisterBO);
                if (dt.Rows.Count > 0)
                {
                    VisitorPassReport1 visitorRpt = new VisitorPassReport1();

                    ((XRTableCell)(visitorRpt.FindControl("tblDate", false))).Text = DateTime.Now.ToString("dd MMM yyyy hh:mm:ss");
                    ((XRTableCell)(visitorRpt.FindControl("tblName", false))).Text = dt.Rows[0]["VisitorName"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblAddress", false))).Text = dt.Rows[0]["Address"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblMobile", false))).Text = dt.Rows[0]["MobileNo"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblPurpose", false))).Text = dt.Rows[0]["PurposeofVisit"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblNoofVisitors", false))).Text = dt.Rows[0]["NoofVisitors"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblVisitorID", false))).Text = dt.Rows[0]["VisitorID"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblWhomtoMeet", false))).Text = dt.Rows[0]["WhomToVisit"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblVehicleNo", false))).Text = dt.Rows[0]["VechileNo"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblBelongings", false))).Text = dt.Rows[0]["Belongings"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblIntime", false))).Text = dt.Rows[0]["InDateTime"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("xrcellRemarks", false))).Text = dt.Rows[0]["Remarks"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblSecurityName", false))).Text = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblVisitorCard", false))).Text = dt.Rows[0]["VisitorIDCardNo"].ToString();

                    if (!string.IsNullOrEmpty(dt.Rows[0]["Photo"].ToString()))
                    {
                        ((XRPictureBox)(visitorRpt.FindControl("xrVisitorPic", false))).Image = BindApplicantImage((byte[])dt.Rows[0]["PhotoFileData"]);
                    }
                    else
                    {
                        // conver  file image to bytearray.
                        byte[]defaultimg = File.ReadAllBytes(Server.MapPath(@"~/Images/UserLogin/DefaultImage.jpg"));
                        ((XRPictureBox)(visitorRpt.FindControl("xrVisitorPic", false))).Image = BindApplicantImage(defaultimg);
                    }
                    
                    visitorRpt.CreateDocument();

                    // Create the 2nd report and generate its document. 
                    PassConditionsReport visitorpassConditionRpt = new PassConditionsReport();
                    visitorpassConditionRpt.CreateDocument();

                    // Add all pages of the 2nd report to the end of the 1st report. 
                    visitorRpt.Pages.AddRange(visitorpassConditionRpt.Pages);


                    if (System.Configuration.ConfigurationManager.AppSettings["SecurityPrinter"].ToString().Length > 12)
                    {
                        System.Drawing.Printing.PrinterSettings instance = new System.Drawing.Printing.PrinterSettings();
                        string DefaultPrinter = instance.PrinterName = System.Configuration.ConfigurationManager.AppSettings["SecurityPrinter"].ToString();
                        if (instance.CanDuplex)
                            instance.Duplex = System.Drawing.Printing.Duplex.Default;
                        PrintToolBase tool = new PrintToolBase(visitorRpt.PrintingSystem);

                        if (DefaultPrinter == "Microsoft Print to PDF" || DefaultPrinter == "Microsoft XPS Document Writer" || DefaultPrinter.Contains("Send To OneNote"))
                        {
                            // HelpClass.custAlertMsg(this, this.GetType(), "Sorry ! There is no Printer configured to print.", "error");
                        }
                        else
                        {
                            //try
                            //{
                                tool.Print(DefaultPrinter);
                            //}
                            //catch { }
                        }
                    }

                    ASPxWebDocumentViewer1.OpenReport(visitorRpt);
                    ASPxWebDocumentViewer1.DataBind();
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSREP1:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSREP1:" + strline + " " + strMsg);
            }
        }
        public void PrintOutwardMaterialReport(int OutMaterialID)
        {
            try
            {
                JQDatatableBO objJQDataTableBO = new JQDatatableBO();
                objJQDataTableBO.iMode = 0;
                objJQDataTableBO.pkid = Convert.ToInt32(OutMaterialID);
                objJQDataTableBO.iDisplayLength = 0;
                objJQDataTableBO.iDisplayStart = 0;
                objJQDataTableBO.iSortCol = 0;
                objJQDataTableBO.sSortDir = "";
                objJQDataTableBO.sSearch = "";
                objJQDataTableBO.EmpID = 0;
                objJQDataTableBO.Status = 0;
                OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
                DataTable dt = objOutwardMaterialBAL.GetOutwardMaterialList(objJQDataTableBO);
                if (dt.Rows.Count > 0)
                {
                    OutwardReport outwardRpt = new OutwardReport();
                    //((XRTableCell)(outwardRpt.FindControl("tblDate", false))).Text = DateTime.Now.ToString("dd MMM yyyy hh:mm:ss");
                    ((XRTableCell)(outwardRpt.FindControl("tblMaterialType", false))).Text = dt.Rows[0]["OutwardMaterialType"].ToString();
                    ((XRTableCell)(outwardRpt.FindControl("tblMaterialName", false))).Text = dt.Rows[0]["MaterialName"].ToString();
                    ((XRTableCell)(outwardRpt.FindControl("tblQuantity", false))).Text = dt.Rows[0]["Quantityu"].ToString();
                    ((XRTableCell)(outwardRpt.FindControl("tblGPDCNO", false))).Text = dt.Rows[0]["GPorDCPNo"].ToString();
                    ((XRTableCell)(outwardRpt.FindControl("tblVehicleNo", false))).Text = dt.Rows[0]["VechileNo"].ToString();
                    ((XRTableCell)(outwardRpt.FindControl("tblDepartment", false))).Text = dt.Rows[0]["Department"].ToString();
                    ((XRTableCell)(outwardRpt.FindControl("tblGivenBy", false))).Text = dt.Rows[0]["GivenBy"].ToString();
                    ((XRTableCell)(outwardRpt.FindControl("tblNameOfParty", false))).Text = dt.Rows[0]["NameOfParty"].ToString();
                    ((XRTableCell)(outwardRpt.FindControl("tblMaterialOutDateTime", false))).Text = dt.Rows[0]["OutDateTime"].ToString();
                    ((XRTableCell)(outwardRpt.FindControl("tblDOM", false))).Text = dt.Rows[0]["DescriptionOfMaterial"].ToString();
                    ((XRTableCell)(outwardRpt.FindControl("tblRemarks", false))).Text = dt.Rows[0]["Remarks"].ToString();
                    ((XRTableCell)(outwardRpt.FindControl("tblPrintedby", false))).Text = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString();
                    outwardRpt.CreateDocument();

                    // Create the 2nd report and generate its document. 
                    PassConditionsReport MaterialConditionRpt = new PassConditionsReport();
                    MaterialConditionRpt.CreateDocument();

                    // Add all pages of the 2nd report to the end of the 1st report. 
                    outwardRpt.Pages.AddRange(MaterialConditionRpt.Pages);

                    ASPxWebDocumentViewer1.OpenReport(outwardRpt);
                    ASPxWebDocumentViewer1.DataBind();
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSREP1:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSREP1:" + strline + " " + strMsg);
            }
        }
        private System.Drawing.Image BindApplicantImage(byte[] applicantImage)
        {
            //string base64ImageString = ConvertBytesToBase64(applicantImage);
            //MyImage.ImageUrl = "data:image/jpg;base64," + base64ImageString;
            using (var ms = new MemoryStream(applicantImage))
            {
                return System.Drawing.Image.FromStream(ms);
            }
        }
        public string ConvertBytesToBase64(byte[] imageBytes)
        {
            return Convert.ToBase64String(imageBytes);
        }
        #region documentviewer
        protected void ASPxDocumentViewer1_CacheReportDocument(object sender, DevExpress.XtraReports.Web.CacheReportDocumentEventArgs e)
        {
            e.Key = Guid.NewGuid().ToString();
            Page.Session[e.Key] = e.SaveDocumentToMemoryStream();
        }
        protected void ASPxDocumentViewer1_RestoreReportDocumentFromCache(object sender, DevExpress.XtraReports.Web.RestoreReportDocumentFromCacheEventArgs e)
        {
            Stream stream = Page.Session[e.Key] as Stream;
            if (stream != null)
                e.RestoreDocumentFromStream(stream);
        }
        #endregion documentviewer
    }
}