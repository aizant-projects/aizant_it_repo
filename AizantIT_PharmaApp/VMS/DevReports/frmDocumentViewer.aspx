﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmDocumentViewer.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.DevReports.frmDocumentViewer" %>

<%@ Register Assembly="DevExpress.XtraReports.v18.1.Web.WebForms, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>                             
    <form id="form1" runat="server">
        <div>            
            <dx:ASPxWebDocumentViewer ID="ASPxWebDocumentViewer1" runat="server" ClientInstanceName="DocumentViewer" OnCacheReportDocument="ASPxDocumentViewer1_CacheReportDocument" OnRestoreReportDocumentFromCache="ASPxDocumentViewer1_RestoreReportDocumentFromCache">
                <ClientSideEvents Init="function(s, e) {
                                s.previewModel.reportPreview.zoom(1);
                            }" />
            </dx:ASPxWebDocumentViewer>
        </div>
    </form>
</body>
</html>
