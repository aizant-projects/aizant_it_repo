﻿using System;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VMS_BAL;
using VMS_BO;
using System.Collections;
using AizantIT_PharmaApp.Common;
using System.Text;
using DevExpress.XtraCharts;
using System.Data;
using System.Drawing;

namespace AizantIT_PharmaApp.VMS
{
    public partial class VMS_HomePage : System.Web.UI.Page
    {
        VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
        VMSRolePrivilegesBAL objVMSRolePrivilegesBAL = new VMSRolePrivilegesBAL();

        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    fillRole();
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "hide", "$('#filter_dropdown4').hide();", true);
                    if (HelpClass.IsUserAuthenticated())
                    {
                       
                        ViewState["EmpID"] = "0";                        
                            ViewState["EmpID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);                       
                        
                        ArrayList lstRoleids = new ArrayList() { 22,8,28,18,17};
                        graphic.Visible = false;
                        divNoTask.Visible = false;
                        if (Session["sesGMSRoleID"].ToString()=="22" || Session["sesGMSRoleID"].ToString() == "8")
                        {
                            divVehicleChart.Visible = true;
                        }
                        else
                        {
                            divVehicleChart.Visible = false;
                        }
                        //Security=22 ,admin=8,Materail Store=19,Materail hod=20,Materail user=21
                        
                            if (lstRoleids.Contains(Convert.ToInt32(Session["sesGMSRoleID"].ToString())))
                            {
                                graphic.Visible = true;
                            }
                            GetCardCount(Convert.ToInt32(Session["sesGMSRoleID"].ToString()));
                                               
                                                
                            InitializeThePage();
                            if (divReverted.Visible == false && divApprover.Visible == false)
                            {
                                divcsscard.Visible = false;
                            }
                            else
                            {
                                divcsscard.Visible = true;
                            }
                        
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
           
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSCh1:" + strline + " " + strMsg);
            }
        }
        private void InitializeThePage()
        {
            try
            {
                ChartFill(0);
                ChartFill3(0);
                ChartFill2(0);
                LoadAllMonths();
                ShowHideReportTypeSelection();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", " getDate();", true);
            }
            
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSCh2:" + strline + " " + strMsg);
            }
        }
        private void GetCardCount(int RoleId)
        {
            try
            {
                if (RoleId == 17)
                {
                    //hr Count
                    MaterialBO objMaterialBO = new MaterialBO();
                    OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
                    objMaterialBO.EmpID = 0;
                    objMaterialBO.DeptID = 0;
                    objMaterialBO.TransStatus = 1;
                    objMaterialBO.RoleType = 1;
                    DataTable dtCardCount = objOutwardMaterialBAL.GetCardCount(objMaterialBO);
                    StringBuilder sbCard = new StringBuilder();
                    //approve
                    if (Convert.ToInt32(dtCardCount.Rows[0][0].ToString()) > 0)
                    {
                        divApprover.Visible = true;
                        string qs0 = HttpUtility.UrlEncode(HelpClass.Encrypt("hr"));
                        sbCard.Append("<a class='col-12 float-left' href ='VisitorFacilities/VisitorFacilityList.aspx?c=" + qs0 + "'>");
                        sbCard.Append("<div class='float-left padding-none col-lg-11'>Facilities Pending for Approval</div><div class='float-right padding-none col-lg-1'>" + dtCardCount.Rows[0][0].ToString() + "</div></a> ");
                        liApprove.InnerHtml = sbCard.ToString();
                       
                        lblApproverQA.Text = dtCardCount.Rows[0][0].ToString();
                    }
                }
                else if (RoleId == 28 || RoleId == 18)//BD-roleid 28
                {
                    //helpdesk count
                    MaterialBO objMaterialBO = new MaterialBO();
                    OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
                    objMaterialBO.EmpID = Convert.ToInt32(ViewState["EmpID"].ToString());
                    objMaterialBO.DeptID = 0;
                    objMaterialBO.TransStatus = 2;
                    objMaterialBO.RoleType = 2;
                    DataTable dtCardCount1 = objOutwardMaterialBAL.GetCardCount(objMaterialBO);
                    StringBuilder sbCard = new StringBuilder();
                    //Revert
                    if (Convert.ToInt32(dtCardCount1.Rows[0][0].ToString()) > 0)
                    {
                        divReverted.Visible = true;
                        string qs1 = HttpUtility.UrlEncode(HelpClass.Encrypt("bd"));
                        sbCard.Append("<a class='col-12 float-left' href = 'VisitorFacilities/VisitorFacilityList.aspx?c=" + qs1 + "'>");
                        sbCard.Append("<div class='float-left padding-none col-lg-11'>Facilities Reverted back</div><div class='float-right padding-none col-lg-1'>" + dtCardCount1.Rows[0][0].ToString() + "</div></a> ");
                        liReverted.InnerHtml = sbCard.ToString();
                        lblRevertedinitiation.Text = dtCardCount1.Rows[0][0].ToString();
                    }
                    else
                    {
                        divNoTask.Visible = true;
                        graphic.Visible = false;
                    }
                }
                else if (RoleId == 19)
                {
                    //materialStore Count
                    MaterialBO objMaterialBO = new MaterialBO();
                    OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
                    objMaterialBO.EmpID = 0;
                    objMaterialBO.DeptID = 0;
                    objMaterialBO.TransStatus = 2;
                    objMaterialBO.RoleType = 4;
                    DataTable dtCardCount2 = objOutwardMaterialBAL.GetCardCount(objMaterialBO);
                    StringBuilder sbCard = new StringBuilder();
                    //approve
                    if (Convert.ToInt32(dtCardCount2.Rows[0][0].ToString()) > 0)
                    {
                        divApprover.Visible = true;
                        string qs2 = HttpUtility.UrlEncode(HelpClass.Encrypt("mstore"));
                        sbCard.Append("<a class='col-12 float-left' href = 'MaterialRegister/MaterialRegister.aspx?c=" + qs2 + "' class='float-left col-lg-12' name='navApprove' >");
                        sbCard.Append("<div class='float-left padding-none col-lg-11'>Materials Pending for Approval</div><div class='float-right padding-none col-lg-1'>" + dtCardCount2.Rows[0][0].ToString() + "</div></ a > ");
                        liApprove.InnerHtml = sbCard.ToString();
                        lblApproverQA.Text = dtCardCount2.Rows[0][0].ToString();
                    }
                    else
                    {                      
                        divNoTask.Visible = true;
                    }
                }
                else if (RoleId == 20)
                {
                    //materialHod Count
                    MaterialBO objMaterialBO = new MaterialBO();
                    OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
                    objMaterialBO.EmpID = Convert.ToInt32(ViewState["EmpID"].ToString());
                    objMaterialBO.DeptID = 0;
                    objMaterialBO.TransStatus = 1;
                    objMaterialBO.RoleType = 3;
                    DataTable dtCardCount3 = objOutwardMaterialBAL.GetCardCount(objMaterialBO);
                    StringBuilder sbCard = new StringBuilder();
                    //approve
                    if (Convert.ToInt32(dtCardCount3.Rows[0][0].ToString()) > 0)
                    {
                        divApprover.Visible = true;
                        string qs3 = HttpUtility.UrlEncode(HelpClass.Encrypt("mhod"));
                        sbCard.Append("<a class='col-12 float-left' href = 'MaterialRegister/MaterialRegister.aspx?c=" + qs3 + "' class='float-left col-lg-12' name='navApprove' >");
                        sbCard.Append("<div class='float-left padding-none col-lg-11'>Materials Pending for Approval</div><div class='float-right padding-none col-lg-1'>" + dtCardCount3.Rows[0][0].ToString() + "</div></ a > ");
                        liApprove.InnerHtml = sbCard.ToString();
                        lblApproverQA.Text = dtCardCount3.Rows[0][0].ToString();
                    }
                    else
                    {
                        divNoTask.Visible = true;
                    }
                }
                else if (RoleId == 21)
                {
                    //materialUser Count
                    MaterialBO objMaterialBO = new MaterialBO();
                    OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
                    objMaterialBO.EmpID = Convert.ToInt32(ViewState["EmpID"].ToString());
                    objMaterialBO.DeptID = 0;
                    objMaterialBO.TransStatus = 2;
                    objMaterialBO.RoleType = 5;
                    DataTable dtCardCount4 = objOutwardMaterialBAL.GetCardCount(objMaterialBO);
                    StringBuilder sbCard = new StringBuilder();
                    //Revert
                    if (Convert.ToInt32(dtCardCount4.Rows[0][0].ToString()) > 0)
                    {
                        divReverted.Visible = true;
                        string qs4 = HttpUtility.UrlEncode(HelpClass.Encrypt("muser"));
                        sbCard.Append("<a class='col-12 float-left' href = 'MaterialRegister/MaterialRegister.aspx?c=" + qs4 + "' class='float-left col-lg-12' name='navReverted' >");
                        sbCard.Append("<div class='float-left padding-none col-lg-11'>Materials Reverted back</div><div class='float-right padding-none col-lg-1'>" + dtCardCount4.Rows[0][0].ToString() + "</div></ a > ");
                        liReverted.InnerHtml = sbCard.ToString();
                        lblRevertedinitiation.Text = dtCardCount4.Rows[0][0].ToString();
                    }
                    else
                    {
                        divNoTask.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSCh3:" + strline + " " + strMsg);
            }
        }
        private void LoadAllMonths()
        {
            txtMonthYear.Text = "2018";
            ddlfromMonths.Items.Insert(0, new ListItem("--Select--", "0"));
            ddlToMonth.Items.Insert(0, new ListItem("--Select--", "0"));
            var months = CultureInfo.CurrentCulture.DateTimeFormat.MonthGenitiveNames;
            for (int i = 0; i < months.Length - 1; i++)
            {
                ddlfromMonths.Items.Add(new ListItem(months[i], (i + 1).ToString()));
            }
        }

        public void ChartFill(int Mode = 0)
        {
            try
            {
                VisitorRegisterBO objVisitorRegisterBO = new VisitorRegisterBO();
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                DataTable dtChartData = new DataTable();
                if (ddlReportType.SelectedValue == "1")//weekly
                {
                    objVisitorRegisterBO.InDateTime = txtSearchVisitorsFromDate.Text.Trim();
                    objVisitorRegisterBO.OutDateTime = txtSearchVisitorsToDate.Text.Trim();
                    objVisitorRegisterBO.FromMonth = 0;
                    objVisitorRegisterBO.ToMonth = 0;
                    objVisitorRegisterBO.FromYear = 0;
                    objVisitorRegisterBO.ToYear = 0;
                    objVisitorRegisterBO.Mode = Mode;
                    dtChartData = objVisitorRegisterBAL.LoadChartNoofVisitors(objVisitorRegisterBO);
                    this.WebChartControl3.DataSource = dtChartData;
                    this.WebChartControl3.Series[0].ArgumentDataMember = "InDateTime";
                    XYDiagram diagram = (XYDiagram)WebChartControl3.Diagram;
                    diagram.AxisX.Title.Visible = true;
                    diagram.AxisX.Title.Alignment = StringAlignment.Center;
                    diagram.AxisX.Title.Text = "Visited Date";
                    diagram.AxisX.Title.TextColor = Color.Black;
                    diagram.AxisX.Title.Antialiasing = true;
                    diagram.AxisX.Title.Font = new Font("Tahoma", 14, FontStyle.Bold);
                    diagram.AxisY.Title.Visible = true;
                    diagram.AxisY.Title.Alignment = StringAlignment.Center;
                    diagram.AxisY.Title.Text = "No of Visitors";
                    diagram.AxisY.Title.TextColor = Color.Black;
                    diagram.AxisY.Title.Antialiasing = true;
                    diagram.AxisY.Title.Font = new Font("Tahoma", 14, FontStyle.Bold);
                    this.WebChartControl3.Series[0].ValueDataMembers.AddRange(new string[] { "NoofVisitors" });
                    this.WebChartControl3.Series[0].ChangeView(ViewType.Bar);
                    this.WebChartControl3.DataBind();
                }
                else if (ddlReportType.SelectedValue == "2")//monthly
                {
                    objVisitorRegisterBO.FromMonth = Convert.ToInt32(ddlfromMonths.SelectedValue);
                    objVisitorRegisterBO.ToMonth = Convert.ToInt32(ddlToMonth.SelectedValue);
                    objVisitorRegisterBO.InDateTime = "";
                    objVisitorRegisterBO.OutDateTime = "";
                    objVisitorRegisterBO.FromYear = Convert.ToInt32(txtMonthYear.Text.Trim());
                    objVisitorRegisterBO.ToYear = 0;
                    objVisitorRegisterBO.Mode = 2;
                    dtChartData = objVisitorRegisterBAL.LoadChartNoofVisitors(objVisitorRegisterBO);
                    this.WebChartControl3.DataSource = dtChartData;
                    this.WebChartControl3.Series[0].ArgumentDataMember = "ShortMonthName";
                    XYDiagram diagram = (XYDiagram)WebChartControl3.Diagram;
                    diagram.AxisX.Title.Visible = true;
                    diagram.AxisX.Title.Alignment = StringAlignment.Center;
                    diagram.AxisX.Title.Text = "Month";
                    diagram.AxisX.Title.TextColor = Color.Black;
                    diagram.AxisX.Title.Antialiasing = true;
                    diagram.AxisX.Title.Font = new Font("Tahoma", 14, FontStyle.Bold);
                    diagram.AxisY.Title.Visible = true;
                    diagram.AxisY.Title.Alignment = StringAlignment.Center;
                    diagram.AxisY.Title.Text = "No of Visitors";
                    diagram.AxisY.Title.TextColor = Color.Black;
                    diagram.AxisY.Title.Antialiasing = true;
                    diagram.AxisY.Title.Font = new Font("Tahoma", 14, FontStyle.Bold);
                    this.WebChartControl3.Series[0].ValueDataMembers.AddRange(new string[] { "NoofVisitors" });
                    this.WebChartControl3.Series[0].ChangeView(ViewType.Bar);
                    this.WebChartControl3.DataBind();
                }
                else if (ddlReportType.SelectedValue == "3")//yearly
                {
                    if (!string.IsNullOrEmpty(txtFromYear.Text.Trim()))
                    {
                        objVisitorRegisterBO.FromYear = Convert.ToInt32(txtFromYear.Text.Trim());
                    }
                    else
                    {
                        objVisitorRegisterBO.FromYear = 0;
                    }
                    if (!string.IsNullOrEmpty(txtToYear.Text.Trim()))
                    {
                        objVisitorRegisterBO.ToYear = Convert.ToInt32(txtToYear.Text.Trim());
                    }
                    else
                    {
                        objVisitorRegisterBO.ToYear = 0;
                    }
                    objVisitorRegisterBO.FromMonth = 0;
                    objVisitorRegisterBO.ToMonth = 0;
                    objVisitorRegisterBO.InDateTime = "";
                    objVisitorRegisterBO.OutDateTime = "";
                    objVisitorRegisterBO.Mode = 3;
                    dtChartData = objVisitorRegisterBAL.LoadChartNoofVisitors(objVisitorRegisterBO);
                    this.WebChartControl3.DataSource = dtChartData;
                    this.WebChartControl3.Series[0].ArgumentDataMember = "Year";
                    XYDiagram diagram = (XYDiagram)WebChartControl3.Diagram;
                    diagram.AxisX.Title.Visible = true;
                    diagram.AxisX.Title.Alignment = StringAlignment.Center;
                    diagram.AxisX.Title.Text = "Year";
                    diagram.AxisX.Title.TextColor = Color.Black;
                    diagram.AxisX.Title.Antialiasing = true;
                    diagram.AxisX.Title.Font = new Font("Tahoma", 14, FontStyle.Bold);

                    diagram.AxisY.Title.Visible = true;
                    diagram.AxisY.Title.Alignment = StringAlignment.Center;
                    diagram.AxisY.Title.Text = "No of Visitors";
                    diagram.AxisY.Title.TextColor = Color.Black;
                    diagram.AxisY.Title.Antialiasing = true;
                    diagram.AxisY.Title.Font = new Font("Tahoma", 14, FontStyle.Bold);
                    this.WebChartControl3.Series[0].ValueDataMembers.AddRange(new string[] { "NoofVisitors" });
                    this.WebChartControl3.Series[0].ChangeView(ViewType.Bar);
                    this.WebChartControl3.DataBind();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSCh3:" + strline + " " + strMsg, "error");
            }
        }

        public void ChartFill2(int Mode1 = 0)
        {
            try
            {
                VisitorRegisterBO objVisitorRegisterBO = new VisitorRegisterBO();
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                DataTable dtChartData2 = new DataTable();
                if (ddlReportType.SelectedValue == "1")//weekly
                {
                    objVisitorRegisterBO.InDateTime = txtSearchVisitorsFromDate.Text.Trim();
                    objVisitorRegisterBO.OutDateTime = txtSearchVisitorsToDate.Text.Trim();
                    objVisitorRegisterBO.FromMonth = 0;
                    objVisitorRegisterBO.ToMonth = 0;
                    objVisitorRegisterBO.FromYear = 0;
                    objVisitorRegisterBO.ToYear = 0;
                    objVisitorRegisterBO.Mode = Mode1;
                    dtChartData2 = objVisitorRegisterBAL.LoadChartPurposeofVisit(objVisitorRegisterBO);
                    this.WebChartControl1.DataSource = dtChartData2;
                    this.WebChartControl1.Series[0].ArgumentDataMember = "Purpose_of_Visit";
                    this.WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "NoofVisitors" });
                    this.WebChartControl1.Series[0].ChangeView(ViewType.StackedBar);
                    this.WebChartControl1.Series[0].LegendTextPattern = "{A}";
                    this.WebChartControl1.DataBind();
                }
                if (ddlReportType.SelectedValue == "2")//weekly
                {
                    objVisitorRegisterBO.FromMonth = Convert.ToInt32(ddlfromMonths.SelectedValue);
                    objVisitorRegisterBO.ToMonth = Convert.ToInt32(ddlToMonth.SelectedValue);
                    objVisitorRegisterBO.InDateTime = "";
                    objVisitorRegisterBO.OutDateTime = "";
                    objVisitorRegisterBO.FromYear = Convert.ToInt32(txtMonthYear.Text.Trim());
                    objVisitorRegisterBO.ToYear = 0;
                    objVisitorRegisterBO.Mode = 2;
                    dtChartData2 = objVisitorRegisterBAL.LoadChartPurposeofVisit(objVisitorRegisterBO);
                    this.WebChartControl1.DataSource = dtChartData2;
                    this.WebChartControl1.Series[0].ArgumentDataMember = "Purpose_of_Visit";
                    this.WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "NoofVisitors" });
                    this.WebChartControl1.Series[0].ChangeView(ViewType.StackedBar);
                    this.WebChartControl1.Series[0].LegendTextPattern = "{A}";
                    this.WebChartControl1.DataBind();
                }
                else if (ddlReportType.SelectedValue == "3")//yearly
                {
                    if (!string.IsNullOrEmpty(txtFromYear.Text.Trim()))
                    {
                        objVisitorRegisterBO.FromYear = Convert.ToInt32(txtFromYear.Text.Trim());
                    }
                    else
                    {
                        objVisitorRegisterBO.FromYear = 0;
                    }
                    if (!string.IsNullOrEmpty(txtToYear.Text.Trim()))
                    {
                        objVisitorRegisterBO.ToYear = Convert.ToInt32(txtToYear.Text.Trim());
                    }
                    else
                    {
                        objVisitorRegisterBO.ToYear = 0;
                    }
                    objVisitorRegisterBO.FromMonth = 0;
                    objVisitorRegisterBO.ToMonth = 0;
                    objVisitorRegisterBO.InDateTime = "";
                    objVisitorRegisterBO.OutDateTime = "";
                    objVisitorRegisterBO.Mode = 3;
                    dtChartData2 = objVisitorRegisterBAL.LoadChartPurposeofVisit(objVisitorRegisterBO);
                    this.WebChartControl1.DataSource = dtChartData2;
                    this.WebChartControl1.Series[0].ArgumentDataMember = "Purpose_of_Visit";
                    this.WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "NoofVisitors" });
                    this.WebChartControl1.Series[0].ChangeView(ViewType.StackedBar);
                    this.WebChartControl1.Series[0].LegendTextPattern = "{A}";
                    this.WebChartControl1.DataBind();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSCh4:" + strline + " " + strMsg);
            }
        }
        public void ChartFill3(int Mode1 = 0)
        {
            try
            {
                VisitorRegisterBO objVisitorRegisterBO = new VisitorRegisterBO();
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                DataTable dtChartData2 = new DataTable();
                if (ddlReportType.SelectedValue == "1")//weekly
                {
                    objVisitorRegisterBO.InDateTime = txtSearchVisitorsFromDate.Text.Trim();
                    objVisitorRegisterBO.OutDateTime = txtSearchVisitorsToDate.Text.Trim();
                    objVisitorRegisterBO.FromMonth = 0;
                    objVisitorRegisterBO.ToMonth = 0;
                    objVisitorRegisterBO.FromYear = 0;
                    objVisitorRegisterBO.ToYear = 0;
                    objVisitorRegisterBO.Mode = Mode1;
                    dtChartData2 = objVisitorRegisterBAL.LoadChartVehicle(objVisitorRegisterBO);
                    this.WebChartControl2.DataSource = dtChartData2;
                    this.WebChartControl2.Series[0].ArgumentDataMember = "VechileNo";
                    this.WebChartControl2.Series[0].ValueDataMembers.AddRange(new string[] { "KMS" });
                    this.WebChartControl2.Series[0].ChangeView(ViewType.StackedBar);
                    this.WebChartControl2.Series[0].LegendTextPattern = "{A}";
                    this.WebChartControl2.DataBind();
                }
                if (ddlReportType.SelectedValue == "2")//weekly
                {
                    objVisitorRegisterBO.FromMonth = Convert.ToInt32(ddlfromMonths.SelectedValue);
                    objVisitorRegisterBO.ToMonth = Convert.ToInt32(ddlToMonth.SelectedValue);
                    objVisitorRegisterBO.InDateTime = "";
                    objVisitorRegisterBO.OutDateTime = "";
                    objVisitorRegisterBO.FromYear = Convert.ToInt32(txtMonthYear.Text.Trim());
                    objVisitorRegisterBO.ToYear = 0;
                    objVisitorRegisterBO.Mode = 2;
                    dtChartData2 = objVisitorRegisterBAL.LoadChartVehicle(objVisitorRegisterBO);
                    this.WebChartControl2.DataSource = dtChartData2;
                    this.WebChartControl2.Series[0].ArgumentDataMember = "VechileNo";
                    this.WebChartControl2.Series[0].ValueDataMembers.AddRange(new string[] { "KMS" });
                    this.WebChartControl2.Series[0].ChangeView(ViewType.StackedBar);
                    this.WebChartControl2.Series[0].LegendTextPattern = "{A}";
                    this.WebChartControl2.DataBind();
                }
                else if (ddlReportType.SelectedValue == "3")//yearly
                {
                    if (!string.IsNullOrEmpty(txtFromYear.Text.Trim()))
                    {
                        objVisitorRegisterBO.FromYear = Convert.ToInt32(txtFromYear.Text.Trim());
                    }
                    else
                    {
                        objVisitorRegisterBO.FromYear = 0;
                    }
                    if (!string.IsNullOrEmpty(txtToYear.Text.Trim()))
                    {
                        objVisitorRegisterBO.ToYear = Convert.ToInt32(txtToYear.Text.Trim());
                    }
                    else
                    {
                        objVisitorRegisterBO.ToYear = 0;
                    }
                    objVisitorRegisterBO.FromMonth = 0;
                    objVisitorRegisterBO.ToMonth = 0;
                    objVisitorRegisterBO.InDateTime = "";
                    objVisitorRegisterBO.OutDateTime = "";
                    objVisitorRegisterBO.Mode = 3;
                    dtChartData2 = objVisitorRegisterBAL.LoadChartVehicle(objVisitorRegisterBO);
                    this.WebChartControl2.DataSource = dtChartData2;
                    this.WebChartControl2.Series[0].ArgumentDataMember = "VechileNo";
                    this.WebChartControl2.Series[0].ValueDataMembers.AddRange(new string[] { "KMS" });
                    this.WebChartControl2.Series[0].ChangeView(ViewType.StackedBar);
                    this.WebChartControl2.Series[0].LegendTextPattern = "{A}";
                    this.WebChartControl2.DataBind();
                }
            }
           
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSCh4:" + strline + " " + strMsg);
            }
        }
        protected void ddlReportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowHideReportTypeSelection();
        }

        private void ShowHideReportTypeSelection()
        {
            if (ddlReportType.SelectedValue == "1")
            {
                div_FromDate.Visible = true;
                div_ToDate.Visible = true;
                div_FromMonths.Visible = false;
                div_ToMonth.Visible = false;
                div_FromYear.Visible = false;
                div_ToYear.Visible = false;
                div_MonthYear.Visible = false;
            }
            else if (ddlReportType.SelectedValue == "2")
            {
                div_FromMonths.Visible = true;
                div_ToMonth.Visible = true;
                div_FromDate.Visible = false;
                div_ToDate.Visible = false;
                div_FromYear.Visible = false;
                div_ToYear.Visible = false;
                div_MonthYear.Visible = true;
            }
            else if (ddlReportType.SelectedValue == "3")
            {
                div_FromYear.Visible = true;
                div_ToYear.Visible = true;
                div_FromMonths.Visible = false;
                div_ToMonth.Visible = false;
                div_FromDate.Visible = false;
                div_ToDate.Visible = false;
                div_MonthYear.Visible = false;
            }
        }
        protected void ddlfromMonths_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlToMonth.Items.Clear();
            ddlToMonth.Items.Insert(0, new ListItem("--Select--", "0"));
            if (ddlfromMonths.SelectedIndex > 0)
            {
                var months = CultureInfo.CurrentCulture.DateTimeFormat.MonthGenitiveNames;
                for (int i = 1; i <= months.Length - 1; i++)
                {
                    if (i >= Convert.ToInt32(ddlfromMonths.SelectedValue))
                    {
                        ddlToMonth.Items.Add(new ListItem(months[i - 1], (i).ToString()));
                    }
                }
            }
        }

        protected void btn_chartSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                ArrayList Mandatory = new ArrayList();
                if (ddlReportType.SelectedValue == "1")//weekly
                {
                    if (string.IsNullOrEmpty(txtSearchVisitorsFromDate.Text.Trim()))
                    {
                        Mandatory.Add("Please select from date");
                    }
                    if (string.IsNullOrEmpty(txtSearchVisitorsToDate.Text.Trim()))
                    {
                        Mandatory.Add("Please select to date");
                    }
                    StringBuilder s = new StringBuilder();
                    foreach (string x in Mandatory)
                    {
                        s.Append(x);
                        s.Append("<br/>");
                    }
                    if (Mandatory.Count > 0)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "" + s.ToString() + "", "error");
                        return;
                    }
                }
                if (ddlReportType.SelectedValue == "2")//monthly
                {
                    if (string.IsNullOrEmpty(txtMonthYear.Text.Trim()))
                    {
                        Mandatory.Add("Please enter year");
                    }
                    if (ddlfromMonths.SelectedIndex==0)
                    {
                        Mandatory.Add("Please select from month");
                    }
                    if (ddlToMonth.SelectedIndex == 0)
                    {
                        Mandatory.Add("Please select to month");
                    }
                    StringBuilder s = new StringBuilder();
                    foreach (string x in Mandatory)
                    {
                        s.Append(x);
                        s.Append("<br/>");
                    }
                    if (Mandatory.Count > 0)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "" + s.ToString() + "", "error");
                        return;
                    }
                }
                if (ddlReportType.SelectedValue == "3")//yearly
                {
                    if (string.IsNullOrEmpty(txtFromYear.Text.Trim()))
                    {
                        Mandatory.Add("Please enter from year");
                    }
                    if (string.IsNullOrEmpty(txtToYear.Text.Trim()))
                    {
                        Mandatory.Add("Please enter to year");
                    }
                    StringBuilder s = new StringBuilder();
                    foreach (string x in Mandatory)
                    {
                        s.Append(x);
                        s.Append("<br/>");
                    }
                    if (Mandatory.Count > 0)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "" + s.ToString() + "", "error");
                        return;
                    }
                    if (Convert.ToInt32(txtFromYear.Text.Trim()) > Convert.ToInt32(txtToYear.Text.Trim()))
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "From Year should be less than To Year.", "error");
                        return;
                    }
                }
                ChartFill(Convert.ToInt32(ddlReportType.SelectedValue));
                ChartFill3(Convert.ToInt32(ddlReportType.SelectedValue));
                ChartFill2(Convert.ToInt32(ddlReportType.SelectedValue));
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "hide", "$('#filter_dropdown4').hide();", true);
                upVisitorChart.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSCh41:" + strline + " " + strMsg);
            }
        }
        

        private void HidevehicleChart()
        {
            if (true)
            {

            }
        }

        public void fillRole()
        {
            RolePrivilegesBO objRolePrivilegesBO = new RolePrivilegesBO();
            objRolePrivilegesBO.RoleID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            DataTable dtRole = objVMSRolePrivilegesBAL.GetEmpRoleID(objRolePrivilegesBO);
            if (dtRole.Rows.Count > 1)
            {
                ddlVMSAccessableRoles.DataTextField = dtRole.Columns["RoleName"].ToString();
                ddlVMSAccessableRoles.DataValueField = dtRole.Columns["RoleID"].ToString();
                ddlVMSAccessableRoles.DataSource = dtRole;
                ddlVMSAccessableRoles.DataBind();
                if (Session["sesGMSRoleID"].ToString() == "0")
                {
                    Session["sesGMSRoleID"] = dtRole.Rows[0]["RoleID"].ToString();
                }
                ddlVMSAccessableRoles.SelectedValue = Session["sesGMSRoleID"].ToString();
            }
            else
            {
                if (Session["sesGMSRoleID"].ToString() == "0")
                {
                    Session["sesGMSRoleID"] = dtRole.Rows[0]["RoleID"].ToString();
                }
                // For VMS  
                dvMultiRoles.Visible = false;
                if (dtRole.Rows[0]["RoleID"].ToString() == "22")//Security RoleID
                {
                    Response.Redirect("~/VMS/Dashboard/SecurityLandingPage.aspx", false);
                }
                
            }
        }

        protected void ddlVMSAccessableRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["sesGMSRoleID"] = ddlVMSAccessableRoles.SelectedValue.ToString();
            Response.Redirect("~/VMS/VMS_HomePage.aspx");
        }
    }
}