﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using VMS_BAL;
using VMS_BO;


namespace AizantIT_PharmaApp.VMS.WebService
{
    /// <summary>
    /// Summary description for VMS_Service
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class VMS_Service : System.Web.Services.WebService
    {
        VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();

        #region Visitor Datatable
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetVisitorList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
            JQDatatableBO objJQDataTableBO = new JQDatatableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            List<VisitorRegisterBO> listDocuments = new List<VisitorRegisterBO>();
            int filteredCount = 0;
            DataTable dt = objVisitorRegisterBAL.GetVisitorDetails_List(objJQDataTableBO);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    VisitorRegisterBO objvisitorRegisterBO = new VisitorRegisterBO();
                    objvisitorRegisterBO.SNo = Convert.ToInt32(rdr["SNo"]);
                    objvisitorRegisterBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    objvisitorRegisterBO.VisitorID = (rdr["VisitorID"].ToString());
                    objvisitorRegisterBO.VisitorName = (rdr["VisitorName"].ToString());
                    objvisitorRegisterBO.MobileNo = (rdr["MobileNo"].ToString());
                    objvisitorRegisterBO.DeptName = (rdr["DeptName"].ToString());
                    objvisitorRegisterBO.EmpName = (rdr["EmpName"].ToString());
                    objvisitorRegisterBO.PurposeofVisitTxt = (rdr["PurposeofVisit"].ToString());
                    objvisitorRegisterBO.Organization = (rdr["Organization"].ToString());
                    objvisitorRegisterBO.InDateTime = (rdr["InDateTime"].ToString());
                    objvisitorRegisterBO.OutDateTime = (rdr["OutDateTime"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listDocuments.Add(objvisitorRegisterBO);
                }
            }
            JQDatatableBO _objJQDataTableBO = new JQDatatableBO();
            _objJQDataTableBO.iMode = 2;
            _objJQDataTableBO.pkid = 0;
            _objJQDataTableBO.iDisplayLength = iDisplayLength;
            _objJQDataTableBO.iDisplayStart = iDisplayStart;
            _objJQDataTableBO.iSortCol = iSortCol_0;
            _objJQDataTableBO.sSortDir = sSortDir_0;
            _objJQDataTableBO.sSearch = sSearch;
            DataTable dtcount = objVisitorRegisterBAL.GetVisitorDetails_List(_objJQDataTableBO);

            var result = new
            {
                iTotalRecords = dtcount.Rows.Count > 0 ? Convert.ToInt32(dtcount.Rows[0][0]) : 0,
                iTotalDisplayRecords = filteredCount,
                aaData = listDocuments
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);

        }
        #endregion Visitor Datatable

        #region Vechicle Datatable
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetVechile_DetailsList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
            JQDatatableBO objJQDataTableBO = new JQDatatableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            List<ServiceAndVechileBO> listDocuments = new List<ServiceAndVechileBO>();
            int filteredCount = 0;
            VechileRegisterBAL objVechileRegisterBAL = new VechileRegisterBAL();
            DataTable dt = objVechileRegisterBAL.GetVechile_Details(objJQDataTableBO);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    //VisitorRegisterBO objvisitorRegisterBO = new VisitorRegisterBO();
                    ServiceAndVechileBO objvechicleDetailsBO = new ServiceAndVechileBO();
                    objvechicleDetailsBO.SNo = Convert.ToInt32(rdr["SNo"].ToString());
                    objvechicleDetailsBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    objvechicleDetailsBO.VechileNo = (rdr["VechileNo"].ToString());
                    objvechicleDetailsBO.DriverName = (rdr["Name"].ToString());
                    objvechicleDetailsBO.MobileNo = (rdr["MobileNo"].ToString());
                    objvechicleDetailsBO.PlaceVisited = (rdr["PlaceVisited"].ToString());
                    objvechicleDetailsBO.OutDateTime = (rdr["OutDateTime"].ToString());

                    if (rdr["OutReading"].ToString() != "")
                    {
                        objvechicleDetailsBO.OutReading = long.Parse(rdr["OutReading"].ToString());
                    }

                    if (rdr["InReading"].ToString() != "")
                    {
                        objvechicleDetailsBO.InReading = long.Parse(rdr["InReading"].ToString());
                    }
                    if (rdr["DistanceKMS"].ToString() != "")
                    {
                        objvechicleDetailsBO.DistanceKMS = long.Parse(rdr["DistanceKMS"].ToString());
                    }
                    objvechicleDetailsBO.InDateTime = (rdr["InDateTime"].ToString());

                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listDocuments.Add(objvechicleDetailsBO);
                }
            }
            JQDatatableBO _objJQDataTableBO = new JQDatatableBO();
            _objJQDataTableBO.iMode = 2;
            _objJQDataTableBO.pkid = 0;
            _objJQDataTableBO.iDisplayLength = iDisplayLength;
            _objJQDataTableBO.iDisplayStart = iDisplayStart;
            _objJQDataTableBO.iSortCol = iSortCol_0;
            _objJQDataTableBO.sSortDir = sSortDir_0;
            _objJQDataTableBO.sSearch = sSearch;
            DataTable dtcount = objVisitorRegisterBAL.GetVisitorDetails_List(_objJQDataTableBO);

            var result = new
            {
                iTotalRecords = dtcount.Rows.Count > 0 ? Convert.ToInt32(dtcount.Rows[0][0]) : 0,
                iTotalDisplayRecords = filteredCount,
                aaData = listDocuments
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);

        }
        #endregion Visitor Datatable

        #region Visitor Appointment Datatable
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetAppointmentList()
        {
            var iDisplayLength1 = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart1 = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_01 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_01 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
            JQDatatableBO objJQDataTableBO1 = new JQDatatableBO();
            objJQDataTableBO1.iMode = 1;
            objJQDataTableBO1.pkid = 0;
            objJQDataTableBO1.iDisplayLength = iDisplayLength1;
            objJQDataTableBO1.iDisplayStart = iDisplayStart1;
            objJQDataTableBO1.iSortCol = iSortCol_01;
            objJQDataTableBO1.sSortDir = sSortDir_01;
            objJQDataTableBO1.sSearch = sSearch;
            List<VisitorAppointmentBookingsBO> listDocuments1 = new List<VisitorAppointmentBookingsBO>();
            int filteredCount = 0;
            DataTable dt = objVisitorRegisterBAL.GetVisitorAppointmentDetails_List(objJQDataTableBO1);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    VisitorAppointmentBookingsBO objVisitorAppointmentBookingsBO = new VisitorAppointmentBookingsBO();
                    objVisitorAppointmentBookingsBO.Appointment_SNO = Convert.ToInt32(rdr["Appointment_SNO"]);
                    objVisitorAppointmentBookingsBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    objVisitorAppointmentBookingsBO.Appointment_ID = (rdr["Appointment_ID"].ToString());
                    objVisitorAppointmentBookingsBO.Appointment_NAME = (rdr["FirstName"].ToString());//here Firstname is Visitor Full name
                    objVisitorAppointmentBookingsBO.Appointment_MOBILE_NO = (rdr["Appointment_MOBILE_NO"].ToString());
                    objVisitorAppointmentBookingsBO.PurposeofVisitTxt = (rdr["PurposeofVisit"].ToString());
                    objVisitorAppointmentBookingsBO.Appointment_ORGANISATION = (rdr["Organization"].ToString());
                    objVisitorAppointmentBookingsBO.DeptName = (rdr["Department"].ToString());
                    objVisitorAppointmentBookingsBO.EmpName = (rdr["EmpName"].ToString());
                    objVisitorAppointmentBookingsBO.Appointment_DATE = (rdr["Appointment_DATE"].ToString());
                    objVisitorAppointmentBookingsBO.AppointmentStatusName = (rdr["AppointmentStatusName"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listDocuments1.Add(objVisitorAppointmentBookingsBO);
                }
            }
            JQDatatableBO _objJQDataTableBO = new JQDatatableBO();
            _objJQDataTableBO.iMode = 2;
            _objJQDataTableBO.pkid = 0;
            _objJQDataTableBO.iDisplayLength = iDisplayLength1;
            _objJQDataTableBO.iDisplayStart = iDisplayStart1;
            _objJQDataTableBO.iSortCol = iSortCol_01;
            _objJQDataTableBO.sSortDir = sSortDir_01;
            _objJQDataTableBO.sSearch = sSearch;
            DataTable dtcount = objVisitorRegisterBAL.GetVisitorAppointmentDetails_List(_objJQDataTableBO);

            var result = new
            {
                iTotalRecords = dtcount.Rows.Count > 0 ? Convert.ToInt32(dtcount.Rows[0][0]) : 0,
                iTotalDisplayRecords = filteredCount,
                aaData = listDocuments1
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);

        }
        #endregion Visitor Appointment Datatable

        #region Visitor Facilities Request Datatable 
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetVisitorFacilitiesRequestList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
            JQDatatableBO objJQDataTableBO = new JQDatatableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            if (Session["sesGMSRoleID"].ToString() == "8" || Session["sesGMSRoleID"].ToString() == "17")//8-admin,17-hr
            {
                objJQDataTableBO.EmpID = 0;
            }
            else
            {
                objJQDataTableBO.EmpID = Convert.ToInt32((HttpContext.Current.Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
            }
            objJQDataTableBO.Status = Convert.ToInt32(Session["VFRApproveStatus"].ToString());
            List<VisitorFacilitiesBO> listDocuments = new List<VisitorFacilitiesBO>();
            int filteredCount = 0;
            VisitorFacilitiesBAL objVisitorFacilitiesBAL = new VisitorFacilitiesBAL();
            DataTable dt = objVisitorFacilitiesBAL.GetVisitorFacilitiesDetails_List(objJQDataTableBO);
            // DataTable dtTemp=new DataTable();
            if (dt.Rows.Count > 0)
            {
                //if (Convert.ToInt32(Session["VFRApproveStatus"].ToString()) > 0)
                //{
                //    DataRow[] drSearch = dt.Select("ApproveStatus="+ Session["VFRApproveStatus"].ToString());
                //    if (drSearch.Length > 0)
                //    {
                //        dtTemp = drSearch.CopyToDataTable();
                //    }
                //}
                //else
                //{
                //    dtTemp = dt;
                //}
                foreach (DataRow rdr in dt.Rows)
                {
                    VisitorFacilitiesBO objVisitorFacilitiesBO = new VisitorFacilitiesBO();
                    objVisitorFacilitiesBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    objVisitorFacilitiesBO.FacilityRequestID = Convert.ToInt32(rdr["FacilityRequestID"]);
                    objVisitorFacilitiesBO.ClientName = (rdr["ClientName"].ToString());
                    objVisitorFacilitiesBO.NoofVisitors = Convert.ToInt32(rdr["NoofVisitors"].ToString());
                    objVisitorFacilitiesBO.DateofVisit = (rdr["DateofVisit"].ToString());
                    objVisitorFacilitiesBO.Initiatedby = (rdr["EmpName"].ToString());
                    objVisitorFacilitiesBO.DeptName = (rdr["DepartmentName"].ToString());
                    objVisitorFacilitiesBO.TotalEstimatedCost = Convert.ToDecimal(rdr["EstimatedTotalCostFoodandTravel"].ToString());
                    objVisitorFacilitiesBO.TotalActualCost = Convert.ToDecimal(rdr["ActualTotalCostFoodandTravel"].ToString());
                    objVisitorFacilitiesBO.ApproveStatus = (rdr["ApproveStatus1"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listDocuments.Add(objVisitorFacilitiesBO);
                }
            }
            JQDatatableBO _objJQDataTableBO = new JQDatatableBO();
            _objJQDataTableBO.iMode = 2;
            _objJQDataTableBO.pkid = 0;
            _objJQDataTableBO.iDisplayLength = iDisplayLength;
            _objJQDataTableBO.iDisplayStart = iDisplayStart;
            _objJQDataTableBO.iSortCol = iSortCol_0;
            _objJQDataTableBO.sSortDir = sSortDir_0;
            _objJQDataTableBO.sSearch = sSearch;
            if (Session["sesGMSRoleID"].ToString() == "8" && Session["sesGMSRoleID"].ToString() == "17")//8-admin,17-hr
            {
                _objJQDataTableBO.EmpID = 0;
            }
            else
            {
                _objJQDataTableBO.EmpID = Convert.ToInt32((HttpContext.Current.Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
            }
            _objJQDataTableBO.Status = Convert.ToInt32(Session["VFRApproveStatus"].ToString());
            DataTable dtcount = objVisitorFacilitiesBAL.GetVisitorFacilitiesDetails_List(_objJQDataTableBO);

            var result = new
            {
                iTotalRecords = dtcount.Rows.Count > 0 ? Convert.ToInt32(dtcount.Rows[0][0]) : 0,
                iTotalDisplayRecords = filteredCount,
                aaData = listDocuments
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);

        }
        #endregion Visitor Facilities Request Datatable

        #region OutwardMaterial Datatable
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetOutwardList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
            JQDatatableBO objJQDataTableBO = new JQDatatableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.Status = Convert.ToInt32(Session["MStatus"].ToString());
            List<MaterialBO> listMaterial = new List<MaterialBO>();
            int filteredCount = 0;
            OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
            DataTable dtoutward = new DataTable();

            if (Session["sesGMSRoleID"].ToString() == "19" || Session["sesGMSRoleID"].ToString() == "21" || Session["sesGMSRoleID"].ToString() == "8")//8-admin,19-MStore,21-MUser
            {
                if (Session["sesGMSRoleID"].ToString() == "21")
                {
                    objJQDataTableBO.EmpID = Convert.ToInt32((HttpContext.Current.Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                    if (objJQDataTableBO.Status == 8)
                        dtoutward = objOutwardMaterialBAL.GetRevertedOutwardMaterialListforUser(objJQDataTableBO);
                    else
                        dtoutward = objOutwardMaterialBAL.GetOutwardMaterialList(objJQDataTableBO);
                }
                else
                {
                    objJQDataTableBO.EmpID = 0;
                    dtoutward = objOutwardMaterialBAL.GetOutwardMaterialList(objJQDataTableBO);
                }
            }
            if (Session["sesGMSRoleID"].ToString() == "20")//20-mhod Material Manager
            {
                objJQDataTableBO.EmpID = Convert.ToInt32((HttpContext.Current.Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                dtoutward = objOutwardMaterialBAL.GetManagerOutwardMaterialList(objJQDataTableBO);
            }
            if (dtoutward.Rows.Count > 0)
            {
                foreach (DataRow rdr in dtoutward.Rows)
                {
                    MaterialBO objMaterialBO = new MaterialBO();
                    objMaterialBO.OutMaterialID = Convert.ToInt32(rdr["OutMaterialID"]);
                    objMaterialBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    objMaterialBO.MaterialType = (rdr["OutwardMaterialType"].ToString());
                    objMaterialBO.MaterialName = (rdr["MaterialName"].ToString());
                    objMaterialBO.MQuantity = (rdr["Quantityu"].ToString());
                    objMaterialBO.DeptName = (rdr["Department"].ToString());
                    objMaterialBO.EmpName = (rdr["GivenBy"].ToString());
                    objMaterialBO.VechileNo = (rdr["VechileNo"].ToString());
                    objMaterialBO.GPorDCPNo = (rdr["GPorDCPNo"].ToString());
                    objMaterialBO.NameOfParty = (rdr["NameOfParty"].ToString());
                    objMaterialBO.OutDateTime = (rdr["OutDateTime"].ToString());
                    objMaterialBO.StatusName = (rdr["StatusName"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listMaterial.Add(objMaterialBO);
                }
            }
            JQDatatableBO _objJQDataTableBO = new JQDatatableBO();
            _objJQDataTableBO.iMode = 2;
            _objJQDataTableBO.pkid = 0;
            _objJQDataTableBO.iDisplayLength = iDisplayLength;
            _objJQDataTableBO.iDisplayStart = iDisplayStart;
            _objJQDataTableBO.iSortCol = iSortCol_0;
            _objJQDataTableBO.sSortDir = sSortDir_0;
            _objJQDataTableBO.sSearch = sSearch;
            DataTable dtcount = new DataTable();
            if (Session["sesGMSRoleID"].ToString() == "19" || Session["sesGMSRoleID"].ToString() == "21" || Session["sesGMSRoleID"].ToString() == "8")//8-admin,19-MStore,21-MUser
            {
                dtcount = objVisitorRegisterBAL.GetVisitorDetails_List(_objJQDataTableBO);
            }
            if (Session["sesGMSRoleID"].ToString() == "20")//20-mhod Material Manager
            {
                dtcount = objOutwardMaterialBAL.GetManagerOutwardMaterialList(_objJQDataTableBO);
            }


            var result = new
            {
                iTotalRecords = dtcount.Rows.Count > 0 ? Convert.ToInt32(dtcount.Rows[0][0]) : 0,
                iTotalDisplayRecords = filteredCount,
                aaData = listMaterial

            };



            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);

        }
        #endregion OutwardMaterial Datatable

        #region InwardMaterial Datatable
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetInwardList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
            JQDatatableBO objJQDataTableBO = new JQDatatableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            List<MaterialBO> listMaterial = new List<MaterialBO>();
            int filteredCount = 0;

            InwardMaterialBAL objInwardMaterialBAL = new InwardMaterialBAL();
            DataTable dtinward = objInwardMaterialBAL.GetInMaterialList(objJQDataTableBO);
            if (dtinward.Rows.Count > 0)
            {
                foreach (DataRow rdr in dtinward.Rows)
                {
                    MaterialBO objMaterialBO = new MaterialBO();
                    objMaterialBO.Sno = Convert.ToInt32(rdr["Sno"]);
                    objMaterialBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    objMaterialBO.MaterialType = (rdr["InwardMaterialType"].ToString());
                    objMaterialBO.MaterialName = (rdr["MaterialName"].ToString());
                    objMaterialBO.MQuantity = (rdr["Quantity1"].ToString());
                    objMaterialBO.DeptName = (rdr["Department"].ToString());
                    objMaterialBO.EmpName = (rdr["ReceivedBy"].ToString());
                    objMaterialBO.VechileNo = (rdr["VechileNo"].ToString());
                    objMaterialBO.DCP_InvoiceNo = (rdr["DCP_InvoiceNo"].ToString());
                    objMaterialBO.SupplierName = (rdr["SupplierName"].ToString());
                    objMaterialBO.InDateTime = (rdr["InDateTime"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listMaterial.Add(objMaterialBO);
                }
            }
            JQDatatableBO _objJQDataTableBO = new JQDatatableBO();
            _objJQDataTableBO.iMode = 2;
            _objJQDataTableBO.pkid = 0;
            _objJQDataTableBO.iDisplayLength = iDisplayLength;
            _objJQDataTableBO.iDisplayStart = iDisplayStart;
            _objJQDataTableBO.iSortCol = iSortCol_0;
            _objJQDataTableBO.sSortDir = sSortDir_0;
            _objJQDataTableBO.sSearch = sSearch;
            DataTable dtcount = objVisitorRegisterBAL.GetVisitorDetails_List(_objJQDataTableBO);
            var result = new
            {
                iTotalRecords = dtcount.Rows.Count > 0 ? Convert.ToInt32(dtcount.Rows[0][0]) : 0,
                iTotalDisplayRecords = filteredCount,
                aaData = listMaterial
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);

        }
        #endregion InwardMaterial Datatable

        #region DateWiseVisit Chart
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetDateWiseVisitChart()
        {
            var FromDate = HttpContext.Current.Request.Params["InDateTime"];
            var ToDate = HttpContext.Current.Request.Params["OutDateTime"];
            var FromMonth = int.Parse(HttpContext.Current.Request.Params["FromMonth"]);
            var ToMonth = int.Parse(HttpContext.Current.Request.Params["ToMonth"]);
            var FromYear = int.Parse(HttpContext.Current.Request.Params["FromYear"]);
            var ToYear = int.Parse(HttpContext.Current.Request.Params["ToYear"]);
            var Mode= int.Parse(HttpContext.Current.Request.Params["Mode"]);
            VisitorRegisterBO objVisitorRegisterBO = new VisitorRegisterBO();
            objVisitorRegisterBO.InDateTime = FromDate;
            objVisitorRegisterBO.OutDateTime = ToDate;
            objVisitorRegisterBO.FromMonth = FromMonth;
            objVisitorRegisterBO.ToMonth = ToMonth;
            objVisitorRegisterBO.FromYear = FromYear;
            objVisitorRegisterBO.ToYear = ToYear;
            objVisitorRegisterBO.Mode = Mode;
            List<ChartResult> lstChartResult = new List<ChartResult>();

            DataTable dtChartData = objVisitorRegisterBAL.LoadChartNoofVisitors(objVisitorRegisterBO);

            if (dtChartData.Rows.Count > 0)
            {
                for (int i = 0; i < dtChartData.Rows.Count; i++)
                {
                    if(Mode == 0 || Mode == 1)
                    {
                        lstChartResult.Add(
                        new ChartResult(Convert.ToInt32(dtChartData.Rows[i]["NoofVisitors"]), dtChartData.Rows[i]["InDateTime"].ToString()));
                    }
                    else if(Mode==2)
                    {
                        lstChartResult.Add(
                        new ChartResult(Convert.ToInt32(dtChartData.Rows[i]["NoofVisitors"]), dtChartData.Rows[i]["ShortMonthName"].ToString()));
                    }
                    else if (Mode == 3)
                    {
                        lstChartResult.Add(
                        new ChartResult(Convert.ToInt32(dtChartData.Rows[i]["NoofVisitors"]), dtChartData.Rows[i]["Year"].ToString()));
                    }
                }
            }

            JavaScriptSerializer js = new JavaScriptSerializer();
            //string JsonStr = js.Serialize(lstChartResult);
            return js.Serialize(lstChartResult);
        }
        #endregion DateWiseVisit Chart

        #region Purpose_Of_Visit Chart
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetPurpose_Of_VisitChart()
        {
            var FromDate = HttpContext.Current.Request.Params["InDateTime"];
            var ToDate = HttpContext.Current.Request.Params["OutDateTime"];
            var FromMonth = int.Parse(HttpContext.Current.Request.Params["FromMonth"]);
            var ToMonth = int.Parse(HttpContext.Current.Request.Params["ToMonth"]);
            var FromYear = int.Parse(HttpContext.Current.Request.Params["FromYear"]);
            var ToYear = int.Parse(HttpContext.Current.Request.Params["ToYear"]);
            var Mode = int.Parse(HttpContext.Current.Request.Params["Mode"]);
            VisitorRegisterBO objVisitorRegisterBO = new VisitorRegisterBO();
            objVisitorRegisterBO.InDateTime = FromDate.ToString();
            objVisitorRegisterBO.OutDateTime = ToDate.ToString();
            objVisitorRegisterBO.FromMonth = FromMonth;
            objVisitorRegisterBO.ToMonth = ToMonth;
            objVisitorRegisterBO.FromYear = FromYear;
            objVisitorRegisterBO.ToYear = ToYear;
            objVisitorRegisterBO.Mode = Mode;
            List<ChartResult> lstChartResult = new List<ChartResult>();

            DataTable dtChartData = objVisitorRegisterBAL.LoadChartPurposeofVisit(objVisitorRegisterBO);

            if (dtChartData.Rows.Count > 0)
            {
                for (int i = 0; i < dtChartData.Rows.Count; i++)
                {
                    lstChartResult.Add(
                        new ChartResult(Convert.ToInt32(dtChartData.Rows[i]["NoofVisitors"]), dtChartData.Rows[i]["Purpose_of_Visit"].ToString()));
                }
            }

            JavaScriptSerializer js = new JavaScriptSerializer();
            //string JsonStr = js.Serialize(lstChartResult);
            return js.Serialize(lstChartResult);
        }
        #endregion Purpose_Of_Visit Chart

        #region Vehicle Chart
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetVehicleChart()
        {
            var FromDate = HttpContext.Current.Request.Params["InDateTime"];
            var ToDate = HttpContext.Current.Request.Params["OutDateTime"];
            var FromMonth = int.Parse(HttpContext.Current.Request.Params["FromMonth"]);
            var ToMonth = int.Parse(HttpContext.Current.Request.Params["ToMonth"]);
            var FromYear = int.Parse(HttpContext.Current.Request.Params["FromYear"]);
            var ToYear = int.Parse(HttpContext.Current.Request.Params["ToYear"]);
            var Mode = int.Parse(HttpContext.Current.Request.Params["Mode"]);
            VisitorRegisterBO objVisitorRegisterBO = new VisitorRegisterBO();
            objVisitorRegisterBO.InDateTime = FromDate.ToString();
            objVisitorRegisterBO.OutDateTime = ToDate.ToString();
            objVisitorRegisterBO.FromMonth = FromMonth;
            objVisitorRegisterBO.ToMonth = ToMonth;
            objVisitorRegisterBO.FromYear = FromYear;
            objVisitorRegisterBO.ToYear = ToYear;
            objVisitorRegisterBO.Mode = Mode;
            List<ChartResult> lstChartResult = new List<ChartResult>();

            DataTable dtChartData = objVisitorRegisterBAL.LoadChartVehicle(objVisitorRegisterBO);

            if (dtChartData.Rows.Count > 0)
            {
                for (int i = 0; i < dtChartData.Rows.Count; i++)
                {
                    lstChartResult.Add(
                        new ChartResult(Convert.ToInt32(dtChartData.Rows[i]["KMS"]), dtChartData.Rows[i]["VechileNo"].ToString()));
                }
            }

            JavaScriptSerializer js = new JavaScriptSerializer();
            //string JsonStr = js.Serialize(lstChartResult);
            return js.Serialize(lstChartResult);
        }
        #endregion Vehicle Chart

        #region Visitor Chart List
        public string GetVisitorChartList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
            var FromDate = (HttpContext.Current.Request.Params["FromDate"].ToString());
            var ToDate = (HttpContext.Current.Request.Params["ToDate"].ToString());
            int Mode;
            int.TryParse(HttpContext.Current.Request.Params["Mode"], out Mode);
            int FromYear;
            int.TryParse(HttpContext.Current.Request.Params["FromYear"], out FromYear);
            int ToYear;
            int.TryParse(HttpContext.Current.Request.Params["ToYear"], out ToYear);
            int FromMonth;
            int.TryParse(HttpContext.Current.Request.Params["FromMonth"], out FromMonth);
            int ToMonth;
            int.TryParse(HttpContext.Current.Request.Params["ToMonth"], out ToMonth);
            var purposeofvisit = (HttpContext.Current.Request.Params["purposeofvisit"].ToString());
            var TimePeriod = (HttpContext.Current.Request.Params["TimePeriod"].ToString());
            JQDatatableBO objJQDataTableBO = new JQDatatableBO();
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            List<VisitorRegisterBO> listDocuments = new List<VisitorRegisterBO>();
            int filteredCount = 0;
            DataTable dt = objVisitorRegisterBAL.GetVisitorChartDetails_ListDb(objJQDataTableBO, FromDate,ToDate,Mode,FromYear,ToYear,FromMonth,ToMonth, purposeofvisit, TimePeriod);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    VisitorRegisterBO objvisitorRegisterBO = new VisitorRegisterBO();
                    objvisitorRegisterBO.SNo = Convert.ToInt32(rdr["SNo"]);
                    objvisitorRegisterBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    objvisitorRegisterBO.VisitorID = (rdr["VisitorID"].ToString());
                    objvisitorRegisterBO.VisitorName = (rdr["VisitorName"].ToString());
                    objvisitorRegisterBO.MobileNo = (rdr["MobileNo"].ToString());
                    objvisitorRegisterBO.DeptName = (rdr["DeptName"].ToString());
                    objvisitorRegisterBO.EmpName = (rdr["EmpName"].ToString());
                    objvisitorRegisterBO.PurposeofVisitTxt = (rdr["PurposeofVisit"].ToString());
                    objvisitorRegisterBO.Organization = (rdr["Organization"].ToString());
                    objvisitorRegisterBO.InDateTime = (rdr["InDateTime"].ToString());
                    objvisitorRegisterBO.OutDateTime = (rdr["OutDateTime"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listDocuments.Add(objvisitorRegisterBO);
                }
            }
            JQDatatableBO _objJQDataTableBO = new JQDatatableBO();
            _objJQDataTableBO.iMode = 2;
            _objJQDataTableBO.pkid = 0;
            _objJQDataTableBO.iDisplayLength = iDisplayLength;
            _objJQDataTableBO.iDisplayStart = iDisplayStart;
            _objJQDataTableBO.iSortCol = iSortCol_0;
            _objJQDataTableBO.sSortDir = sSortDir_0;
            _objJQDataTableBO.sSearch = sSearch;
            DataTable dtcount = objVisitorRegisterBAL.GetVisitorDetails_List(_objJQDataTableBO);

            var result = new
            {
                iTotalRecords = dtcount.Rows.Count > 0 ? Convert.ToInt32(dtcount.Rows[0][0]) : 0,
                iTotalDisplayRecords = filteredCount,
                aaData = listDocuments
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);

        }

        #endregion Visitor Chart List
    }
    public class ChartResult{
        private int _ValueCount;

        public int ValueCount
        {
            get { return _ValueCount; }
            set { _ValueCount = value; }
        }

        private string _ArgumentText;
        public string ArgumentText
        {
            get { return _ArgumentText; }
            set { _ArgumentText = value; }
        }

        public ChartResult(int iValueCount,string iArgumentText)
        {
            ValueCount = iValueCount;
            ArgumentText = iArgumentText;
        }
    }
}
