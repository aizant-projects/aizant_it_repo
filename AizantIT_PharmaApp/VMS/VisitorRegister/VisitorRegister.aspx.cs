﻿using VMS_BAL;
using VMS_BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using AizantIT_PharmaApp.Common;
using System.Text;
using System.Collections;
using AizantIT_PharmaApp.VMS.DevReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;

namespace AizantIT_PharmaApp.VMS.VisitorRegister
{
    public partial class VisitorRegister : System.Web.UI.Page
    {
        VisitorRegisterBAL objVisitorRegisterBAL;
        DataTable dt;
        VisitorRegisterBO objvisitorRegisterBO;
        VisitorAppointmentBookingsBO objVisitorAppointmentBookingBO;
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ConfirmCustomAlert.buttonClick += new EventHandler(ConfirmCustomAlert_buttonClick);

                if (!IsPostBack)
                {
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                        hdnMinDate.Value = DateTime.Now.Date.ToString("dd MMM yyyy");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            ViewState["EmpID"] = "0";
                           
                            ViewState["EmpID"] = Convert.ToInt32(((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                            ViewState["RoleID"] = Session["sesGMSRoleID"].ToString();
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSV1:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSV1:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSV1:" + strline + " " + strMsg, "error");
            }
        }
        protected void ConfirmCustomAlert_buttonClick(object sender, EventArgs e)
        {
            try
            {
                if (hdnCustom.Value == "Submit")
                {
                    string PurposeOfVisit;
                    if (txt_purposeofvisit.Visible == true)
                    {
                        objvisitorRegisterBO = new VisitorRegisterBO();
                        objvisitorRegisterBO.PurposeofVisitTxt = txt_purposeofvisit.Text.Trim();
                        objVisitorRegisterBAL = new VisitorRegisterBAL();
                        PurposeOfVisit = objVisitorRegisterBAL.PurposeOfVisitUpdate(objvisitorRegisterBO).ToString();
                        if (PurposeOfVisit == "0")
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Purpose of visit already exists.", "error");

                            return;
                        }
                    }
                    else
                    {
                        PurposeOfVisit = ddlVisitorType.SelectedValue;
                    }
                    objVisitorAppointmentBookingBO = new VisitorAppointmentBookingsBO();
                    objVisitorAppointmentBookingBO.CreatedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                    objVisitorAppointmentBookingBO.CreatedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                    objVisitorAppointmentBookingBO.Appointment_ID = txtAppointmentId.Text.Trim();
                    objVisitorAppointmentBookingBO.Appointment_NAME = txt_VisitorName.Text.Trim();
                    objVisitorAppointmentBookingBO.Appointment_LastName = txtLastName.Text.Trim();
                    objVisitorAppointmentBookingBO.Appointment_MOBILE_NO = txt_MobNo.Text.Trim();
                    objVisitorAppointmentBookingBO.Appointment_EMAILID = txt_EmailID.Text.Trim();
                    objVisitorAppointmentBookingBO.Appointment_ORGANISATION = txtOrganisationName.Text.Trim();
                    objVisitorAppointmentBookingBO.Appointment_DATE = Convert.ToDateTime(txtAppointmentDate.Text).ToString("yyyyMMdd");
                    objVisitorAppointmentBookingBO.Appointment_TIME = txtAppointmentTime.Text;
                    objVisitorAppointmentBookingBO.Appointment_PURPOSEOF_VISIT = Convert.ToInt32(PurposeOfVisit);
                    objVisitorAppointmentBookingBO.Appointment_REMARKS = txt_Remarks.Text.Trim();
                    if (ddlDeptToVisit.SelectedIndex > 0)
                        objVisitorAppointmentBookingBO.Appointment_DEPARTMENT_TOVISIT = Convert.ToInt32(ddlDeptToVisit.SelectedValue.Trim());
                    else
                        objVisitorAppointmentBookingBO.Appointment_DEPARTMENT_TOVISIT = 0;
                    if (ddlWhomToVisit.SelectedIndex > 0)
                        objVisitorAppointmentBookingBO.Appointment_WHOMTO_VISIT = Convert.ToInt32(ddlWhomToVisit.SelectedValue.Trim());
                    else
                        objVisitorAppointmentBookingBO.Appointment_WHOMTO_VISIT = 0;
                    objVisitorRegisterBAL = new VisitorRegisterBAL();
                    int c = objVisitorRegisterBAL.BookAppointment(objVisitorAppointmentBookingBO);
                    if (c > 0)
                    {
                        VisitorAutoGenerateID();
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Visitor appointment details submitted successfully.", "success", "Reloadtable();");
                    }
                    else
                    {
                        HelpClass.custAlertMsg(btnCreate, btnCreate.GetType(), "Appointment submission failed.", "error");
                    }
                }
                else
                {
                    if (hdnCustom.Value == "Update")
                    {
                        objVisitorAppointmentBookingBO = new VisitorAppointmentBookingsBO();
                        objVisitorAppointmentBookingBO.Appointment_SNO = Convert.ToInt32(hdnPkIDAppointment.Value);
                        objVisitorAppointmentBookingBO.Appointment_ID = txtAppointmentId.Text.Trim();
                        objVisitorAppointmentBookingBO.Appointment_NAME = txt_VisitorName.Text.Trim();
                        objVisitorAppointmentBookingBO.Appointment_LastName = txtLastName.Text.Trim();
                        objVisitorAppointmentBookingBO.Appointment_MOBILE_NO = txt_MobNo.Text.Trim();
                        objVisitorAppointmentBookingBO.Appointment_EMAILID = txt_EmailID.Text.Trim();
                        objVisitorAppointmentBookingBO.Appointment_ORGANISATION = txtOrganisationName.Text.Trim();
                        btnCreate.Visible = false;
                        btnCancel.Visible = false;

                        if (ddlVisitorType.SelectedIndex > 0)
                        {
                            objVisitorAppointmentBookingBO.Appointment_PURPOSEOF_VISIT = Convert.ToInt32(ddlVisitorType.SelectedValue);
                        }
                        else
                        {
                            ddlVisitorType.SelectedValue = "0";
                        }
                        objVisitorAppointmentBookingBO.Appointment_REMARKS = txt_Remarks.Text.Trim();
                        if (ddlDeptToVisit.SelectedIndex > 0)
                        {
                            objVisitorAppointmentBookingBO.Appointment_DEPARTMENT_TOVISIT = Convert.ToInt32(ddlDeptToVisit.SelectedValue);
                        }
                        else
                        {
                            ddlDeptToVisit.SelectedValue = "0";
                        }
                        if (ddlWhomToVisit.SelectedIndex > 0)
                        {
                            objVisitorAppointmentBookingBO.Appointment_WHOMTO_VISIT = Convert.ToInt32(ddlWhomToVisit.SelectedValue);
                        }
                        objVisitorAppointmentBookingBO.Appointment_DATE = txtAppointmentDate.Text.Trim();
                        objVisitorAppointmentBookingBO.Appointment_TIME = txtAppointmentTime.Text.Trim();
                        objVisitorAppointmentBookingBO.LastChangedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                        objVisitorAppointmentBookingBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                        objVisitorAppointmentBookingBO.LastChangedComments = txtVEditComments.Text;
                        objVisitorRegisterBAL = new VisitorRegisterBAL();
                        int c = objVisitorRegisterBAL.Update_Appointment(objVisitorAppointmentBookingBO);
                        if (c > 0)
                        {
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Visitor appointment details updated successfully.", "success", "Reloadtable();");
                        }
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSV2:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSV2:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSV2:" + strline + " " + strMsg, "error");
            }
        }

        private void InitializeThePage()
        {
            try
            {
                screenAuthentication();
                LoadDepartment();
                LoadPurposeofVisit();
                VisitorAutoGenerateID();

            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSV3:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSV3:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSV3:" + strline + " " + strMsg, "error");
            }
        }

        public void VisitorAutoGenerateID()
        {
            try
            {
                objVisitorRegisterBAL = new VisitorRegisterBAL();
                string strAVID = objVisitorRegisterBAL.AppointmentVisitorAutoGenerateID();
                txtAppointmentId.Text = strAVID;
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSV4:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSV4:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSV4:" + strline + " " + strMsg, "error");
            }
        }

        private void LoadDepartment()
        {
            try
            {
                dt = new DataTable();
                objVisitorRegisterBAL = new VisitorRegisterBAL();
                dt = objVisitorRegisterBAL.LoadDepartment();
                ddlDeptToVisit.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlDeptToVisit.DataValueField = dt.Columns["DeptID"].ToString();
                ddlDeptToVisit.DataSource = dt;
                ddlDeptToVisit.DataBind();
                ddlDeptToVisit.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSV5:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSV5:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSV5:" + strline + " " + strMsg, "error");
            }
        }

        private void LoadPurposeofVisit()
        {
            try
            {
                dt = new DataTable();
                objVisitorRegisterBAL = new VisitorRegisterBAL();
                dt = objVisitorRegisterBAL.LoadPurposeofVisit();
                ddlVisitorType.DataTextField = dt.Columns["Purpose_of_Visit"].ToString();
                ddlVisitorType.DataValueField = dt.Columns["Purpose_of_VisitID"].ToString();
                ddlVisitorType.DataSource = dt;
                ddlVisitorType.DataBind();
                ddlVisitorType.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSV6:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSV6:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSV6:" + strline + " " + strMsg, "error");
            }
        }

        public void WhomToVisit()
        {
            try
            {
                int DeptID = Convert.ToInt32(ddlDeptToVisit.SelectedValue);
                objVisitorRegisterBAL = new VisitorRegisterBAL();
                DataTable dt = objVisitorRegisterBAL.GetEmpData(DeptID);
                ddlWhomToVisit.DataSource = dt;
                ddlWhomToVisit.DataTextField = "Name";
                ddlWhomToVisit.DataValueField = "EmpID";
                ddlWhomToVisit.DataBind();
                ddlWhomToVisit.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSV7:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSV7:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSV7:" + strline + " " + strMsg, "error");
            }
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            if (txt_VisitorName.Text.Trim() == "" || txtLastName.Text.Trim() == "" || txt_MobNo.Text.Trim() == "" || txt_MobNo.Text.Length != 10 || txtAppointmentDate.Text.Trim() == "")
            {
                ArrayList Mandatory = new ArrayList();

                if (string.IsNullOrEmpty(txt_VisitorName.Text.Trim()))
                {
                    Mandatory.Add("Please Enter First Name.");
                }
                if (String.IsNullOrEmpty(txtLastName.Text.Trim()))
                {
                    Mandatory.Add("Please Enter Last Name.");
                }
                if (string.IsNullOrEmpty(txt_MobNo.Text.Trim()))
                {
                    Mandatory.Add("Please Enter Mobile No.");
                }
                else if (txt_MobNo.Text.Length != 10)
                {
                    Mandatory.Add("Mobile No Should be 10 Digits.");
                }
                if (String.IsNullOrEmpty(txtAppointmentDate.Text.Trim()))
                {
                    Mandatory.Add("Please Select Appointment Date.");
                }
                if (txt_purposeofvisit.Visible == true)
                {
                    if (string.IsNullOrEmpty(txt_purposeofvisit.Text.Trim()))
                    {
                        Mandatory.Add("Please Enter Purpose of Visit.");
                    }
                }
                if (ddlVisitorType.Visible == true)
                {
                    if (ddlVisitorType.SelectedIndex == 0)
                    {
                        Mandatory.Add("Please Select Purpose of Visit.");
                    }
                }
                StringBuilder s = new StringBuilder();
                foreach (string x in Mandatory)
                {
                    s.Append(x);
                    s.Append("<br/>");
                }
                HelpClass.custAlertMsg(this, this.GetType(), "" + s.ToString() + "", "error");
                return;
            }
            hdnCustom.Value = "Submit";
            HelpClass.custAlertMsg(this, this.GetType(), "Do you want to submit.", "confirm");
        }

        private void VisitorAppoinment_Reset()
        {
            ddlVisitorType.SelectedIndex = 0;
            ddlDeptToVisit.SelectedIndex = 0;
            txtAppointmentTime.Text = "";
            txtAppointmentDate.Text = "";
            txtOrganisationName.Text = "";
            txt_EmailID.Text = "";
            txt_MobNo.Text = "";
            txt_Remarks.Text = "";
            txt_VisitorName.Text = "";
            txtLastName.Text = "";
            ddlWhomToVisit.Items.Clear();
            txt_purposeofvisit.Text = "";
            if (btn_purposeofvisit1.Text == "<<")
            {
                btn_purposeofvisit1.Text = "+";
                ddlVisitorType.Visible = true;
                txt_purposeofvisit.Visible = false;
                upPurposeofVisit.Update();
            }
            upAppointment.Update();
        }

        protected void addpurposeofvisit1(object sender, EventArgs e)
        {
            if (btn_purposeofvisit1.Text == "+")
            {
                //VisitorAppoinment_Reset();
                btn_purposeofvisit1.Text = "<<";
                txt_purposeofvisit.Visible = true;
                ddlVisitorType.Visible = false;

            }
            else if (btn_purposeofvisit1.Text == "<<")
            {
                //VisitorAppoinment_Reset();
                btn_purposeofvisit1.Text = "+";
                txt_purposeofvisit.Visible = false;
                ddlVisitorType.Visible = true;
            }
        }

        protected void ddlDeptToVisit_SelectedIndexChanged(object sender, EventArgs e)
        {
            WhomToVisit();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            VisitorAppoinment_Reset();
        }

        protected void txt_purposeofvisit_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txt_purposeofvisit.Visible == true)
                {
                    if (string.IsNullOrEmpty(txt_purposeofvisit.Text.Trim()))
                    {
                        HelpClass.custAlertMsg(btnCreate, btnCreate.GetType(), "Please enter purpose of visit.", "error");
                        return;
                    }
                }
                objVisitorRegisterBAL = new VisitorRegisterBAL();
                int Count = objVisitorRegisterBAL.PurposeofVisitifExistsorNot(txt_purposeofvisit.Text.Trim());
                if (Count > 0)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Purpose of visit already exists.", "error");
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSV8:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSV8:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSV8:" + strline + " " + strMsg, "error");
            }
        }

        public void GetAppointmentDetails()
        {
            try
            {
                VisitorAppointmentBookingsBO objVisitorAppointmentBookingBO = new VisitorAppointmentBookingsBO();
                if (ddlDeptToVisit.SelectedValue != "")
                {
                    objVisitorAppointmentBookingBO.Appointment_DEPARTMENT_TOVISIT = Convert.ToInt32(ddlDeptToVisit.SelectedValue.Trim());
                }
                else
                {
                    objVisitorAppointmentBookingBO.Appointment_DEPARTMENT_TOVISIT = 0;
                }
                objVisitorAppointmentBookingBO.Appointment_NAME = txt_VisitorName.Text;
                objVisitorAppointmentBookingBO.Appointment_ID = string.Empty;
                objVisitorAppointmentBookingBO.Appointment_MOBILE_NO = txt_MobNo.Text;
                objVisitorAppointmentBookingBO.Appointment_ORGANISATION = txtOrganisationName.Text;
                objVisitorAppointmentBookingBO.Appointment_PURPOSEOF_VISIT = Convert.ToInt32(ddlVisitorType.SelectedValue);
                objVisitorAppointmentBookingBO.Appointment_DATE = string.Empty;
                objVisitorAppointmentBookingBO.Appointment_DATE = string.Empty;
                dt = new DataTable();
                objVisitorRegisterBAL = new VisitorRegisterBAL();
                dt = objVisitorRegisterBAL.SearchFilterAppointment(objVisitorAppointmentBookingBO);
                if (dt.Rows.Count > 0)
                {
                    //txtAppointmentId.Text = dt.Rows[0]["Appointment_ID"].ToString();
                    txt_VisitorName.Text = dt.Rows[0]["Appointment_NAME"].ToString();
                    txtLastName.Text = dt.Rows[0]["Appointment_LastName"].ToString();
                    txt_MobNo.Text = dt.Rows[0]["Appointment_MOBILE_NO"].ToString();
                    txt_EmailID.Text = dt.Rows[0]["Appointment_EMAILID"].ToString();
                    //ddlVisitorType.SelectedValue = dt.Rows[0]["Purpose_of_VisitID"].ToString();
                    //txtAppointmentDate.Text = dt.Rows[0]["Appointment_DATE"].ToString();
                    //txtAppointmentTime.Text = dt.Rows[0]["Appointment_TIME"].ToString();
                    txtOrganisationName.Text = dt.Rows[0]["Organization"].ToString();
                    //txt_Remarks.Text = dt.Rows[0]["Appointment_REMARKS"].ToString();
                    //ddlDeptToVisit.SelectedValue = dt.Rows[0]["DeptID"].ToString();
                    //WhomToVisit();
                    //ddlWhomToVisit.SelectedValue = dt.Rows[0]["Appointment_WHOMTO_VISIT"].ToString();
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSV9:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSV9:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSV9:" + strline + " " + strMsg, "error");
            }
        }

        protected void txt_VisitorName_TextChanged(object sender, EventArgs e)
        {
            //GetAppointmentDetails();
        }

        protected void txt_MobNo_TextChanged(object sender, EventArgs e)
        {
            // GetAppointmentDetails();
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetVisitorAppintmentNameList(string prefixText)
        {
            VisitorAppointmentBookingsBO objVisitorAppointmentBookingsBO = new VisitorAppointmentBookingsBO();
            VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
            objVisitorAppointmentBookingsBO.Appointment_DEPARTMENT_TOVISIT = 0;
            objVisitorAppointmentBookingsBO.Appointment_PURPOSEOF_VISIT = 0;
            objVisitorAppointmentBookingsBO.Appointment_NAME = prefixText;
            objVisitorAppointmentBookingsBO.Appointment_ID = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_MOBILE_NO = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_ORGANISATION = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_DATE = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_DATE = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_SNO = 0;
            DataTable dt = new DataTable();
            dt = objVisitorRegisterBAL.SearchFilterAppointment(objVisitorAppointmentBookingsBO);
            var distinctNames = (from row in dt.AsEnumerable()
                                 select row.Field<string>("FirstName")).Distinct();
            List<string> VisitorNames = distinctNames.ToList();
            return VisitorNames;
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetVisitorAppintmentListByMobile(string prefixText)
        {
            VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
            VisitorAppointmentBookingsBO objVisitorAppointmentBookingsBO = new VisitorAppointmentBookingsBO();
            objVisitorAppointmentBookingsBO.Appointment_DEPARTMENT_TOVISIT = 0;
            objVisitorAppointmentBookingsBO.Appointment_PURPOSEOF_VISIT = 0;
            objVisitorAppointmentBookingsBO.Appointment_NAME = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_ID = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_MOBILE_NO = prefixText;
            objVisitorAppointmentBookingsBO.Appointment_ORGANISATION = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_DATE = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_DATE = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_SNO = 0;
            DataTable dt = new DataTable();
            dt = objVisitorRegisterBAL.SearchFilterAppointment(objVisitorAppointmentBookingsBO);
            var distinctMobileNo = (from row in dt.AsEnumerable()
                                    select row.Field<string>("Appointment_MOBILE_NO")).Distinct();
            List<string> VisitordistinctMobileNo = distinctMobileNo.ToList();
            return VisitordistinctMobileNo;
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetVisitorListByOrganization(string prefixText)
        {
            VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
            VisitorAppointmentBookingsBO objVisitorAppointmentBookingsBO = new VisitorAppointmentBookingsBO();
            objVisitorAppointmentBookingsBO.Appointment_DEPARTMENT_TOVISIT = 0;
            objVisitorAppointmentBookingsBO.Appointment_PURPOSEOF_VISIT = 0;
            objVisitorAppointmentBookingsBO.Appointment_NAME = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_ID = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_MOBILE_NO = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_ORGANISATION = prefixText;
            objVisitorAppointmentBookingsBO.Appointment_DATE = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_DATE = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_SNO = 0;
            DataTable dt = new DataTable();
            dt = objVisitorRegisterBAL.SearchFilterAppointment(objVisitorAppointmentBookingsBO);
            var distinctOrganization = (from row in dt.AsEnumerable()
                                        select row.Field<string>("Organization")).Distinct();
            List<string> VisitordistinctOrganization = distinctOrganization.ToList();
            return VisitordistinctOrganization;
        }


        protected void btnEdit_List_Click(object sender, EventArgs e)
        {
            string ID = hdnPkID.Value.ToString();
            Response.Redirect("~/VMS/VisitorRegister/VisitorRegistration.aspx?VID=" + ID);
        }

        protected void btnAppointmentEdit_List_Click(object sender, EventArgs e)
        {

            JQDatatableBO objJQDataTableBO1 = new JQDatatableBO();
            objJQDataTableBO1.iMode = 0;
            objJQDataTableBO1.pkid = Convert.ToInt32(hdnPkIDAppointment.Value);
            objJQDataTableBO1.iDisplayLength = 0;
            objJQDataTableBO1.iDisplayStart = 0;
            objJQDataTableBO1.iSortCol = 0;
            objJQDataTableBO1.sSortDir = "";
            objJQDataTableBO1.sSearch = "";
            objVisitorRegisterBAL = new VisitorRegisterBAL();
            DataTable dt = objVisitorRegisterBAL.GetVisitorAppointmentDetails_List(objJQDataTableBO1);
            if (dt.Rows.Count > 0)
            {
                // txtEditSNo.Text = dt.Rows[0]["Appointment_SNO"].ToString();
                txtAppointmentId.Text = dt.Rows[0]["Appointment_ID"].ToString();
                txt_VisitorName.Text = dt.Rows[0]["Appointment_NAME"].ToString();
                txtLastName.Text = dt.Rows[0]["Appointment_LastName"].ToString();
                txt_MobNo.Text = dt.Rows[0]["Appointment_MOBILE_NO"].ToString();
                txt_EmailID.Text = dt.Rows[0]["Appointment_EMAILID"].ToString();
                LoadPurposeofVisit();
                ddlVisitorType.SelectedValue = dt.Rows[0]["Purpose_of_VisitID"].ToString();
                txtAppointmentDate.Text = dt.Rows[0]["Appointment_DATE"].ToString();
                //hdnMinDate.Value = dt.Rows[0]["Appointment_DATE"].ToString();
                //txtAppointmentDate.Text = hdnMinDate.Value;
                txtAppointmentTime.Text = dt.Rows[0]["Appointment_TIME"].ToString();
                txtOrganisationName.Text = dt.Rows[0]["Organization"].ToString();
                txt_Remarks.Text = dt.Rows[0]["Appointment_REMARKS"].ToString();
                LoadDepartment();
                ddlDeptToVisit.SelectedValue = dt.Rows[0]["DeptID"].ToString();
                WhomToVisit();
                ddlWhomToVisit.SelectedValue = dt.Rows[0]["Appointment_WHOMTO_VISIT"].ToString();
                btnCancel.Visible = false;
                btnCreate.Visible = false;
                if (ViewState["vwAppointmentListModify"].ToString() != "0")
                {
                    btnAppointmentUpdate.Visible = true;
                }
                divcomments.Visible = true;
                btn_purposeofvisit1.Visible = false;
                //if (ViewState["vwAppointmentView"].ToString() != "0")
                //{
                //    btnAppointmentUpdate.Visible = false;
                //}
                upAppointment.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "showalert", " OpenModelVisitorEdit();", true);
            }
        }

        protected void btnAppointmentUpdate_Click(object sender, EventArgs e)
        {

        }


        protected void btnNavCreateVisitor_Click(object sender, EventArgs e)
        {
            Session.Remove("pkid");
            Response.Redirect("~/VMS/VisitorRegister/VisitorRegistration.aspx");
        }

        protected void btnCheckOut_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", "getVisitorCheckoutDate();", true);
            try
            {
                if (ViewState["vwCheckoutcount"].ToString() != "0")//to Security
                {
                    VisitorRegisterBO objvisitorRegisterBO = new VisitorRegisterBO();
                    objvisitorRegisterBO.SNo = Convert.ToInt32(hdnPkID.Value);
                    objvisitorRegisterBO.OutDateTime = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");//hdnCheckout.Value;
                    JQDatatableBO objJQDataTableBO = new JQDatatableBO();
                    objJQDataTableBO.iMode = 0;
                    objJQDataTableBO.pkid = Convert.ToInt32(hdnPkID.Value);
                    objJQDataTableBO.iDisplayLength = 0;
                    objJQDataTableBO.iDisplayStart = 0;
                    objJQDataTableBO.iSortCol = 0;
                    objJQDataTableBO.sSortDir = "";
                    objJQDataTableBO.sSearch = "";
                    VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                    DataTable dt = objVisitorRegisterBAL.GetVisitorDetails_List(objJQDataTableBO);
                    if (dt.Rows.Count > 0)
                    {
                        hdnVisitorName.Value = dt.Rows[0]["VisitorName"].ToString();
                    }
                    int c = objVisitorRegisterBAL.CheckoutVisitor(objvisitorRegisterBO);
                    if (c > 0)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "" + hdnVisitorName.Value + " " + "Checked out successfully !", "success", "ReloadVisitortable();");
                    }
                }
                else
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Access is denied ", "error");
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSV10:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSV10:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSV10:" + strline + " " + strMsg, "error");
            }
        }

        protected void btnNavCreateAppointment_Click(object sender, EventArgs e)
        {
            txtAppointmentId.Text = string.Empty;
            VisitorAutoGenerateID();
            ddlDeptToVisit.SelectedIndex = 0;
            ddlWhomToVisit.Items.Clear();
            btn_purposeofvisit1.Visible = true;
            divcomments.Visible = false;
            btnAppointmentUpdate.Visible = false;
            btnCreate.Visible = true;
            btnCancel.Visible = true;
            hdnMinDate.Value = DateTime.Now.Date.ToString("dd MMM yyyy");
            if (ddlVisitorType.Visible == true)
            {
                ddlVisitorType.SelectedIndex = 0;
            }
            upAppointment.Update();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "showalert", "OpenModelViewVisitor();", true);
        }

        private void VisitorAppointmentDisableControls()
        {
            txtAppointmentId.Enabled = false;
            txt_VisitorName.Enabled = false;
            txtLastName.Enabled = false;
            txt_MobNo.Enabled = false;
            txt_EmailID.Enabled = false;
            txtAppointmentDate.Enabled = false;
            txtAppointmentTime.Enabled = false;
            txtOrganisationName.Enabled = false;
            txt_purposeofvisit.Enabled = false;
            ddlVisitorType.Enabled = false;
            ddlDeptToVisit.Enabled = false;
            ddlWhomToVisit.Enabled = false;
            txt_Remarks.Enabled = false;
            txtVEditComments.Enabled = false;
        }

        protected void btnPrintPass_Click(object sender, EventArgs e)
        {
            string filename2 = string.Empty;
            PrintVisitorPass(Convert.ToInt32(hdnPkID.Value), ref filename2);
            Literal1.Text = "<embed src=\"" + ResolveUrl("~/VMS/PdfFolder/" + filename2) + "#toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" height=\"620px\" />";
            UpViewDL.Update();
            UpHeading.Update();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_lock').modal({ show: 'true', backdrop: 'static', keyboard: false });", true);

        }
        public void PrintVisitorPass(int Vid, ref string filename2)
        {
            try
            {
                VisitorRegisterBO objvisitorRegisterBO = new VisitorRegisterBO();
                objvisitorRegisterBO.DeptName = string.Empty;
                objvisitorRegisterBO.Purpose_of_VisitID = 0;
                objvisitorRegisterBO.FirstName = string.Empty;
                objvisitorRegisterBO.VisitorID = string.Empty;
                objvisitorRegisterBO.MobileNo = string.Empty;
                objvisitorRegisterBO.Organization = string.Empty;
                objvisitorRegisterBO.IDCardNo = string.Empty;
                objvisitorRegisterBO.InDateTime = string.Empty;
                objvisitorRegisterBO.OutDateTime = string.Empty;
                DataTable dt = new DataTable();
                objvisitorRegisterBO.SNo = Vid;
                objVisitorRegisterBAL = new VisitorRegisterBAL();
                dt = objVisitorRegisterBAL.SearchFilterVisitor(objvisitorRegisterBO);
                if (dt.Rows.Count > 0)
                {
                    VisitorPassReport1 visitorRpt = new VisitorPassReport1();

                    ((XRTableCell)(visitorRpt.FindControl("tblDate", false))).Text = DateTime.Now.ToString("dd MMM yyyy hh:mm:ss");
                    ((XRTableCell)(visitorRpt.FindControl("tblName", false))).Text = dt.Rows[0]["VisitorName"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblAddress", false))).Text = dt.Rows[0]["Address"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblMobile", false))).Text = dt.Rows[0]["MobileNo"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblPurpose", false))).Text = dt.Rows[0]["PurposeofVisit"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblNoofVisitors", false))).Text = dt.Rows[0]["NoofVisitors"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblVisitorID", false))).Text = dt.Rows[0]["VisitorID"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblWhomtoMeet", false))).Text = dt.Rows[0]["WhomToVisit"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblVehicleNo", false))).Text = dt.Rows[0]["VechileNo"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblBelongings", false))).Text = dt.Rows[0]["Belongings"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblIntime", false))).Text = dt.Rows[0]["InDateTime"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("xrcellRemarks", false))).Text = dt.Rows[0]["Remarks"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblSecurityName", false))).Text = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblVisitorCard", false))).Text = dt.Rows[0]["VisitorIDCardNo"].ToString();

                    if (!string.IsNullOrEmpty(dt.Rows[0]["Photo"].ToString()))
                    {
                        ((XRPictureBox)(visitorRpt.FindControl("xrVisitorPic", false))).Image = BindApplicantImage((byte[])dt.Rows[0]["PhotoFileData"]);
                    }
                    else
                    {
                        // conver  file image to bytearray.
                        byte[] defaultimg = File.ReadAllBytes(Server.MapPath(@"~/Images/UserLogin/DefaultImage.jpg"));
                        ((XRPictureBox)(visitorRpt.FindControl("xrVisitorPic", false))).Image = BindApplicantImage(defaultimg);
                    }

                    visitorRpt.CreateDocument();

                    // Create the 2nd report and generate its document. 
                    PassConditionsReport visitorpassConditionRpt = new PassConditionsReport();
                    visitorpassConditionRpt.CreateDocument();

                    // Add all pages of the 2nd report to the end of the 1st report. 
                    visitorRpt.Pages.AddRange(visitorpassConditionRpt.Pages);

                    string filename1 = Server.MapPath(@"~/Vms/PdfFolder/");
                    filename2 = DateTime.Now.ToString("ddMMyyyyhhmmss") + ".pdf";
                    visitorRpt.ExportToPdf(filename1 + filename2);
                    if (ConfigurationSettings.AppSettings["SecurityPrinter"].ToString().Length > 12)
                    {
                        System.Drawing.Printing.PrinterSettings instance = new System.Drawing.Printing.PrinterSettings();
                        string DefaultPrinter = instance.PrinterName = ConfigurationSettings.AppSettings["SecurityPrinter"].ToString();
                        if (instance.CanDuplex)
                            instance.Duplex = System.Drawing.Printing.Duplex.Default;
                        PrintToolBase tool = new PrintToolBase(visitorRpt.PrintingSystem);

                        if (DefaultPrinter == "Microsoft Print to PDF" || DefaultPrinter == "Microsoft XPS Document Writer" || DefaultPrinter.Contains("Send To OneNote"))
                        {
                            // HelpClass.custAlertMsg(this, this.GetType(), "Sorry ! There is no Printer configured to print.", "error");
                        }
                        else
                        {
                            try
                            {
                                tool.Print(DefaultPrinter);
                            }
                            catch { }
                        }
                    }


                    //ASPxWebDocumentViewer1.OpenReport(visitorRpt);
                    //ASPxWebDocumentViewer1.DataBind();
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSVREP1:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVREP1:" + strline + " " + strMsg);
                HelpClass.showMsg(this, this.GetType(), "VMSVREP1:" + strline + " " + strMsg);
            }
        }
        private System.Drawing.Image BindApplicantImage(byte[] applicantImage)
        {
            //string base64ImageString = ConvertBytesToBase64(applicantImage);
            //MyImage.ImageUrl = "data:image/jpg;base64," + base64ImageString;
            using (var ms = new MemoryStream(applicantImage))
            {
                return System.Drawing.Image.FromStream(ms);
            }
        }
        public string ConvertBytesToBase64(byte[] imageBytes)
        {
            return Convert.ToBase64String(imageBytes);
        }
        void screenAuthentication()
        {
            int visitorvisible = 0;
            int appointmentvisible = 0;
            DataTable dtAthuntication = GmsHelpClass.GetRolePrivileges();
            if (dtAthuntication.Rows.Count > 0)
            {
                //modid=1,PrivilegeID=1,2,3,4,5,6
                int VisitorCreate = dtAthuntication.AsEnumerable()
           .Count(row => row.Field<int>("PrivilegeID") == 1);
                int VisitorPrintcount = dtAthuntication.AsEnumerable()
         .Count(row => row.Field<int>("PrivilegeID") == 2);

                int VisitorCheckoutcount = dtAthuntication.AsEnumerable()
         .Count(row => row.Field<int>("PrivilegeID") == 3);
                ViewState["vwCheckoutcount"] = VisitorCheckoutcount.ToString();

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", "HidePrintbtn('" + VisitorPrintcount + "','" + VisitorCheckoutcount + "');", true);

                int VisitorModifycount = dtAthuntication.AsEnumerable()
         .Count(row => row.Field<int>("PrivilegeID") == 4);
                int AppointmentCreate = dtAthuntication.AsEnumerable()
           .Count(row => row.Field<int>("PrivilegeID") == 5);
                int AppointmentListModify = dtAthuntication.AsEnumerable()
           .Count(row => row.Field<int>("PrivilegeID") == 6);
                ViewState["vwAppointmentListModify"] = AppointmentListModify.ToString();
                int VisitorView = dtAthuntication.AsEnumerable()
          .Count(row => row.Field<int>("PrivilegeID") == 23);
                int AppointmentView = dtAthuntication.AsEnumerable()
           .Count(row => row.Field<int>("PrivilegeID") == 24);
                ViewState["vwAppointmentView"] = AppointmentView.ToString();
                bool isVisitor = false, isAppointment = false;
                btnNavCreateVisitor.Visible = false;
                btnNavCreateAppointment.Visible = false;
                btnAppointmentUpdate.Visible = false;
                if (VisitorCreate == 0 && VisitorCheckoutcount == 0 && VisitorModifycount == 0 && VisitorPrintcount == 0 && VisitorView == 0)
                {
                    Visitortab1.Visible = false;
                }
                else
                {
                    isVisitor = true;
                    Visitortab1.Visible = true;
                    visitorvisible = 1;
                }
                if (AppointmentCreate == 0 && AppointmentListModify == 0 && AppointmentView == 0)
                {
                    Appointmenttab2.Visible = false;
                }
                else
                {
                    isAppointment = true;
                    Appointmenttab2.Visible = true;
                    appointmentvisible = 2;
                }
                if (isVisitor == false && isAppointment == false)
                {
                    Response.Redirect("~/UserLogin.aspx");
                }
                if (VisitorCreate > 0)
                {
                    btnNavCreateVisitor.Visible = true;
                }

                if (AppointmentCreate > 0)
                {
                    btnNavCreateAppointment.Visible = true;
                }
                if (AppointmentListModify > 0)
                {
                    btnAppointmentUpdate.Visible = true;
                }
                if (visitorvisible == 1)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "visitorFun1", "myfunctionVisitortab1();", true);
                }
                if (appointmentvisible == 2)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "visitorFun1", "myfunctionAppointmenttab2();", true);
                }
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}