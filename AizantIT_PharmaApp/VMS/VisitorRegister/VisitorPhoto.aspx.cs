﻿using System;
using System.Data.SqlClient;
using VMS_BAL;
using System.Configuration;
using AizantIT_PharmaApp.Common;

namespace AizantIT_PharmaApp.VMS
{
    public partial class VisitorPhoto : System.Web.UI.Page
    {
        VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["VisitorPhoto"] != null)
                {
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ConnectionString);
                    conn.Open();
                    SqlCommand com = new SqlCommand("select PhotoFileData,Photo from VMS.AizantIT_VisitorRegister where SNo='" + Request.QueryString["VisitorPhoto"].ToString() + "'", conn);
                    com.Parameters.AddWithValue("@Photo", Request.QueryString["VisitorPhoto"].ToString());
                    SqlDataReader dr = com.ExecuteReader();
                    if (dr.Read())
                    {
                        Response.Clear();
                        Response.Buffer = true;
                        Response.ContentType = "image/png";
                        // Response.AddHeader("content-length", "inline; filename = ayz.pdf");
                        Response.AddHeader("content-length", "inline; filename = " + dr["Photo"].ToString());
                        // Response.BinaryWrite(bytes.ToArray());
                        Response.BinaryWrite((byte[])dr["PhotoFileData"]);
                        //   Response.Flush();
                        // Response.End();
                    }
                    else
                    {
                        
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSPhoto:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSPhoto:" + strline + " " + strMsg);
            }
            

            //3 tier changes to do.
            //if (Request.QueryString["VisitorPhoto"] != null)
            //{

            //    SqlDataReader dr = objVisitorRegisterBAL.GetVisitor(Request.QueryString["VisitorPhoto"].ToString());
            //    if (dr.Read())
            //    {
            //        Response.Clear();
            //        Response.Buffer = true;
            //        Response.ContentType = "image/png";
            //        // Response.AddHeader("content-length", "inline; filename = ayz.pdf");
            //        Response.AddHeader("content-length", "inline; filename = " + dr["Photo"].ToString());
            //        // Response.BinaryWrite(bytes.ToArray());
            //        Response.BinaryWrite((byte[])dr["PhotoFileData"]);
            //        //   Response.Flush();
            //        // Response.End();
            //    }
            //}

        }
    }
}