﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/NestedVMS_Master.master" AutoEventWireup="true" CodeBehind="VisitorRegistration.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.VisitorRegister.VisitorRegistration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>

<asp:Content ID="Content2" ContentPlaceHolderID="VMSBody" runat="server">

    <input type="hidden" id="hdnVID" runat="server" value="" />
    <asp:HiddenField ID="hdnVisitorRegisterCustom" runat="server" Value="" />
    <div class="float-right">
        <asp:Button ID="btnBackToList" Text="Back" CssClass=" btn-revert_popup" runat="server" PostBackUrl="~/VMS/VisitorRegister/VisitorRegister.aspx" />
    </div>
    <div class="col-md-12 col-lg-12 col-sm-12 col-12 float-left padding-none ">
        <div class="col-lg-12 grid_panel_full padding-none float-left">
            <div class=" grid_header col-md-12 col-lg-12 col-sm-12 col-12 float-left ">Visitor Registration</div>
            <div class="col-lg-10 float-left">
                <div class="col-sm-12 padding-none grid_border bottom top float-left">
                    <div class="grid_header_vms col-md-12 col-lg-12 col-sm-12 col-12 float-left  bottom">Personal Info</div>
                    <div class="form-group col-sm-4  padding-none float-left" runat="server" id="div5">
                        <label class="control-label col-sm-12" id="lblVisitorID">Visitor ID</label>
                        <div class="col-sm-12">
                            <asp:TextBox ID="txtVisitorID" runat="server" BackColor="White" CssClass="form-control login_input_sign_up" Enabled="false" TabIndex="1" MaxLength="20"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-sm-4  padding-none float-left" runat="server" id="div1">
                        <label class="control-label col-sm-12" id="lblFirstName">First Name<span class="mandatoryStar">*</span></label>
                        <div class="col-sm-12">

                            <asp:TextBox ID="txtVisitorName" runat="server" CssClass="form-control login_input_sign_up ValidateAlpha" placeholder="Enter First Name" onkeypress="return ValidateAlpha(event)" TabIndex="2" MaxLength="50" AutoPostBack="true" OnTextChanged="txtVisitorName_TextChanged"></asp:TextBox>
                            <asp:AutoCompleteExtender ServiceMethod="GetVisitorNameList" MinimumPrefixLength="1" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1"
                                TargetControlID="txtVisitorName" ID="AutoCompleteExtender1" UseContextKey="true" runat="server" FirstRowSelected="false">
                            </asp:AutoCompleteExtender>
                        </div>
                    </div>
                    <div class="form-group col-sm-4  padding-none float-left" runat="server" id="div2">
                        <label class="control-label col-sm-12" id="lblLastName">Last Name<span class="mandatoryStar">*</span></label>
                        <div class="col-sm-12">
                            <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control login_input_sign_up ValidateAlpha" placeholder="Enter Last Name" onkeypress="return ValidateAlpha(event)" MaxLength="50" TabIndex="3"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group col-sm-4  padding-none float-left" runat="server" id="div3">
                        <label class="control-label col-sm-12" id="lblMobileNo">Mobile<span class="mandatoryStar">*</span></label>
                        <div class="col-sm-12">
                            <asp:TextBox ID="txtMobNo" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Mobile" MaxLength="10" onkeypress="return ValidatePhoneNo(event);" OnTextChanged="txtMobNo_TextChanged" AutoPostBack="true" TabIndex="4" onpaste="return false;"></asp:TextBox>
                            <asp:AutoCompleteExtender ServiceMethod="GetVisitorListByMobile" MinimumPrefixLength="1" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1"
                                TargetControlID="txtMobNo" ID="AutoCompleteExtender2" runat="server" FirstRowSelected="false">
                            </asp:AutoCompleteExtender>
                        </div>
                    </div>

                    <div class="form-group col-sm-4  padding-none float-left" runat="server" id="div4">
                        <label class="control-label col-sm-12" id="lblEmail">Email ID</label>
                        <div class="col-sm-12">
                            <asp:TextBox ID="txtVisitEmail" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Email" TabIndex="5" onblur="_IsValidEmail(this.value);" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group col-sm-4  padding-none float-left" runat="server" id="div13">
                        <label class="control-label col-sm-12" id="lblAddress">Address<span class="mandatoryStar">*</span></label>
                        <div class="col-sm-12">
                            <asp:TextBox ID="txtAddress" row="1" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Address" TabIndex="6" MaxLength="200"></asp:TextBox>
                        </div>
                    </div>
                    <asp:UpdatePanel runat="server" ID="upCountry" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="form-group col-sm-4  padding-none float-left" runat="server" id="div14">
                                <label class="control-label col-sm-12" id="lblCountry">Country</label>
                                <div class="col-sm-12">
                                    <asp:DropDownList ID="ddlCountry" runat="server" TabIndex="7" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlCountry" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <div>
                        <asp:UpdatePanel runat="server" ID="upStates" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-group col-sm-4  padding-none float-left" runat="server" id="div15">
                                    <label class="control-label col-sm-12" id="lblState">State</label>
                                    <div class="col-sm-12">
                                        <asp:DropDownList ID="ddlState" runat="server" TabIndex="8" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlState" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel runat="server" ID="upCities" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-group col-sm-4  padding-none float-left" runat="server" id="div16">
                                    <label class="control-label col-sm-12" id="lblCity">City</label>
                                    <div class="col-sm-12">
                                        <asp:DropDownList ID="ddlCities" runat="server" TabIndex="9" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlCities" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel runat="server" ID="upIDProofType" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-group col-sm-4  padding-none float-left" runat="server" id="div8">
                                    <label class="control-label col-sm-10" id="lblIDProofType">ID Proof Type</label><asp:Button ID="btnIDProofType" runat="server" CssClass="btnnVisit float-right" Text="+" Font-Bold="true" OnClick="btnIDProofType_Click" />
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtIDProofType" runat="server" CssClass="form-control login_input_sign_up" TabIndex="10" placeholder="Enter ID Proof Type" Visible="false" AutoPostBack="true" onkeypress="return NoSpace(event)" OnTextChanged="txtIDProofType_TextChanged" MaxLength="20"></asp:TextBox>
                                        <asp:DropDownList ID="ddlIDProofType" runat="server" TabIndex="10" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" placeholder="Enter ID Proof Type"></asp:DropDownList>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="form-group col-sm-4  padding-none float-left" runat="server" id="div9">
                            <label class="control-label col-sm-12" id="lblIDProofNo">ID Proof No</label>
                            <div class="col-sm-12">
                                <asp:TextBox ID="txtIdProofNo" runat="server" CssClass="form-control login_input_sign_up" TabIndex="11" placeholder="Enter ID Proof No" OnTextChanged="txtIdProofNo_TextChanged" AutoPostBack="true" onkeyup="SetContextKey()" MaxLength="50"></asp:TextBox>
                                <asp:AutoCompleteExtender ServiceMethod="GetVisitorListByIDProofNo" MinimumPrefixLength="1" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1"
                                    TargetControlID="txtIdProofNo" ID="AutoCompleteExtender3" UseContextKey="true" runat="server" FirstRowSelected="false">
                                </asp:AutoCompleteExtender>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 padding-none grid_border bottom  float-left">
                    <div class=" grid_header_vms col-md-12 col-lg-12 col-sm-12 col-12 float-left bottom">Company Info</div>
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                        <ContentTemplate>

                            <div class="form-group col-sm-4  padding-none float-left" runat="server" id="div10">
                                <label class="control-label col-sm-10" id="lblPurpose">Purpose of Visit<span class="mandatoryStar">*</span></label><asp:Button ID="btn_purposeofvisit" runat="server" CssClass="btnnVisit float-right" Text="+" Font-Bold="true" OnClick="btn_purposeofvisit_Click" />
                                <div class="col-sm-12">
                                    <asp:TextBox ID="txt_purposeother" runat="server" TabIndex="12" ClientIDMode="Static" CssClass="form-control login_input_sign_up" placeholder="Enter Purpose Of Visit" Visible="false" onkeypress="return NoSpace(event)" AutoPostBack="true" OnTextChanged="txt_purposeother_TextChanged" MaxLength="50"></asp:TextBox>
                                    <asp:DropDownList ID="ddlPurposeofVisit" runat="server" TabIndex="12" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" placeholder="Enter Purpose Of Visit"></asp:DropDownList>
                                    <asp:HiddenField ID="HDFPurposeofvisit" runat="server" />
                                </div>
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                        <ContentTemplate>

                            <div class="form-group col-sm-4  padding-none float-left" runat="server" id="div28">
                                <label class="control-label col-sm-12" id="lblDepartment">Department<span class="mandatoryStar">*</span></label>
                                <div class="col-sm-12">
                                    <asp:DropDownList ID="ddlDepartment" TabIndex="13" runat="server" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" Visible="false"></asp:DropDownList>
                                    <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control login_input_sign_up ValidateAlpha" TabIndex="13" placeholder="Enter Department" MaxLength="50" onkeypress="return ValidateAlpha(event)"></asp:TextBox>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlDepartment" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                        <ContentTemplate>

                            <div class="form-group col-sm-4  padding-none float-left" runat="server" id="div12">
                                <label class="control-label col-sm-12" id="lblWhomToVisit">Whom To Visit<span class="mandatoryStar">*</span></label>
                                <div class="col-sm-12">
                                    <asp:DropDownList ID="ddlWhomToVisit" runat="server" TabIndex="14" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" AutoPostBack="true" Visible="false"></asp:DropDownList>
                                    <asp:TextBox ID="txtWhomToVisit" runat="server" CssClass="form-control login_input_sign_up ValidateAlpha" TabIndex="14" placeholder="Enter WhomToVisit" onkeypress="return ValidateAlpha(event)" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlDepartment" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="ddlWhomToVisit" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <div class="col-12 float-left padding-none">
                        <div class="form-group col-sm-4  padding-none float-left" runat="server" id="div24">
                            <label class="control-label col-sm-12" id="lblAccompanyPerson">Accompany Person</label>
                            <div class="col-sm-12">
                                <asp:TextBox ID="txtAccompanyPerson" runat="server" CssClass="form-control login_input_sign_up ValidateAlphawithDot" TabIndex="15" placeholder="Enter Accompany Person" MaxLength="100" onkeypress="return ValidateAlphawithDot(event)"></asp:TextBox>
                            </div>
                        </div>


                        <div class="form-group col-sm-4  padding-none float-left" runat="server" id="div6">
                            <label class="control-label col-sm-12" id="lblOrganization">Organization</label>
                            <div class="col-sm-12">
                                <asp:TextBox ID="txtOrganizeName" runat="server" CssClass="form-control login_input_sign_up ValidateAlpha" TabIndex="16" placeholder="Enter Organization" MaxLength="200" onkeypress="return ValidateAlpha(event)"></asp:TextBox>
                                <asp:AutoCompleteExtender ServiceMethod="GetVisitorListByOrganization" MinimumPrefixLength="1" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1"
                                    TargetControlID="txtOrganizeName" ID="AutoCompleteExtender4" UseContextKey="true" runat="server" FirstRowSelected="false">
                                </asp:AutoCompleteExtender>
                            </div>
                        </div>
                        <div class="form-group col-sm-4  padding-none float-left" runat="server" id="div23">
                            <label class="control-label col-sm-12" id="lblAccessLocation">Access Location</label>
                            <div class="col-sm-12">
                                <asp:TextBox ID="txtAccessLocation" runat="server" TabIndex="17" CssClass="form-control login_input_sign_up" placeholder="Enter Access Location" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-sm-4  padding-none float-left" runat="server" id="div25">
                        <label class="control-label col-sm-12" id="lblRoomNumber">Room Number</label>
                        <div class="col-sm-12">
                            <asp:TextBox ID="txtRoomNo" runat="server" TabIndex="18" CssClass="form-control login_input_sign_up" placeholder="Enter Room Number" MaxLength="20"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-sm-4  padding-none float-left" runat="server" id="div11">
                        <label class="control-label col-sm-12" id="lblNoofVisitors" style="text-align: left; font-weight: lighter">No of Visitors<span class="mandatoryStar">*</span></label>
                        <div class="col-sm-12">
                            <asp:TextBox ID="txtNoofVisitors" runat="server" TabIndex="19" CssClass="form-control login_input_sign_up" placeholder="Enter No of Visitors" onkeypress="if(event.keyCode<48 || event.keyCode>57)event.returnValue=false;" MaxLength="3"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-sm-4  padding-none float-left" runat="server" id="div17">
                        <label class="control-label col-sm-12" id="lblVehicleNo">Vehicle No</label>
                        <div class="col-sm-12">
                            <asp:TextBox ID="txtVechileNo" runat="server" TabIndex="20" CssClass="form-control login_input_sign_up" placeholder="Enter Vehicle No" MaxLength="20"></asp:TextBox>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="upInDateTime" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="form-group col-sm-4  padding-none float-left" runat="server" id="div7">
                                <label class="control-label col-sm-12" id="lblInDateTime">Check In Time</label>
                                <div class="col-sm-12">
                                    <asp:TextBox ID="txtIntime" runat="server" TabIndex="21" CssClass="form-control login_input_sign_up" Enabled="false" BackColor="White"></asp:TextBox>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="form-group col-sm-4 padding-none float-left" runat="server" id="div19">
                        <label class="control-label col-sm-12" id="lblPlannedOutTime">Planned OutTime</label>
                        <div class="col-sm-12">
                            <asp:TextBox ID="txtxPlanTimeOut" runat="server" TabIndex="22" CssClass="form-control login_input_sign_up" placeholder="Enter Planned Out Time" TextMode="Time"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-sm-4 padding-none float-left" runat="server" id="div18">
                        <label class="control-label col-sm-12" id="lblBelongings" style="text-align: left; font-weight: lighter">Belongings</label>
                        <div class="col-sm-12">
                            <asp:TextBox ID="txtBelongings" runat="server" TabIndex="23" CssClass="form-control login_input_sign_up" placeholder="Enter Belongings" MaxLength="200"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 padding-right_div top float-left">
                <div class=" grid_header_vms  col-md-12 col-lg-12 col-sm-12 col-12 float-left ">Visitor Photo</div>
                <div class="col-md-12 col-lg-12 col-sm-12 col-12 bottom grid_border padding-none float-left">


                    <div class="form-group" runat="server" id="div20">
                        <div class="col-sm-12 float-left">
                            <div id="divcam" runat="server">
                                <div id="webcam">
                                </div>
                            </div>
                            <div class="col-lg-12 grid_border top" style="height: 190px" runat="server" id="divimage">
                                <asp:Image ID="imgCapture" runat="server" CssClass="image_profile" />

                            </div>
                            <div class="col-lg-12 top bottom " style="text-align: center;">
                                <asp:Button ID="btnCapture" Text="Capture" CssClass=" btn-signup_popup" runat="server" OnClick="btnCapture_Click1" />
                                <asp:Button ID="btnRetake" Text="Retake" CssClass="btnn" runat="server" Visible="false" />
                                <span id="camStatus"></span>
                            </div>
                        </div>
                    </div>



                    <div class="form-group col-sm-12 padding-none float-left" runat="server" id="div22">
                        <label class="control-label col-sm-12" id="lblIDCardNumber" style="text-align: left; font-weight: lighter">ID Card Number</label>
                        <div class="col-sm-12">
                            <asp:TextBox ID="txtIdCardNo" runat="server" TabIndex="24" CssClass="form-control login_input_sign_up" placeholder="Enter ID Card Number" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group col-sm-12 padding-none float-left" runat="server" id="div21">
                        <label class="control-label col-sm-12" id="lblRemarks">Remarks</label>
                        <div class="col-sm-12">
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TabIndex="25" placeholder="Enter Remarks" TextMode="MultiLine" Rows="7" MaxLength="200"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-sm-12 padding-none float-left" runat="server" id="divcomments" visible="false">
                        <label class="control-label col-sm-12" id="">Comments<span class="mandatoryStar">*</span></label>
                        <div class="col-sm-12">
                            <asp:TextBox ID="txtComments" runat="server" CssClass="form-control" TabIndex="26" placeholder="Enter Comments" TextMode="MultiLine" Rows="3" MaxLength="200"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="upVisitorbuttons" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <div class="col-lg-12 padding-none float-left">
                <div class="float-right top bottom padding-none">
                    <asp:Button ID="btnInsert" Text="Submit" CssClass=" btn-signup_popup" runat="server" OnClick="btnInsert_Click" TabIndex="26" ValidationGroup="Visitor" />
                    <asp:Button ID="btnUpdate" Text="Update" CssClass=" btn-signup_popup" Style="margin-right: 5px !important" runat="server" OnClientClick="javascript:return Updatevalidate();" OnClick="btnUpdate_Click" ValidationGroup="Visitor" Visible="false" />
                    <asp:Button ID="btnReset" Text="Reset" CssClass=" btn-revert_popup" runat="server" TabIndex="27" OnClick="btnReset_Click" CausesValidation="false" />
                    <asp:Button ID="btnCancel" Text="Cancel" CssClass=" btn-cancel_popup" runat="server" PostBackUrl="~/VMS/VisitorRegister/VisitorRegister.aspx" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:Button ID="btnCamClose" runat="server" OnClick="btnCamClose_Click" Text="close" Style="display: none" />
    <uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />

    <!-------- File VIEW Print in Model-------------->
    <div id="myModal_lock" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 86%; height: 500px">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">

                    <asp:UpdatePanel ID="UpHeading" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <span id="span_VisitorTitle" runat="server"></span>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpViewDL" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div>
                                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <!-------- End File VIEW Print in Model-------------->
    <!-------- File VIEW Cam in Model-------------->
    <div id="myModal_lockCam" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 86%; height: 500px">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">

                    <asp:UpdatePanel ID="upCamHeader" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <span id="span1" runat="server"></span>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <button type="button" class="close" onclick="return bindImage();">&times;</button>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="upCamBody" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="dvCam" runat="server">
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <!-------- End File VIEW Print in Model-------------->
    <!--------Visitor Webcam Script-------------->

    <!--------End Visitor Webcam Script-------------->


    <!--------Visitor Submit Validation-------------->
    <script type="text/javascript">
        function bindImage() {

            var btnclose = document.getElementById("<%=btnCamClose.ClientID%>");
            btnclose.click();

        }
        function Submitvaidate() {
            $('#<%=divcomments.ClientID %>').hide();
            var VFirstName = document.getElementById("<%=txtVisitorName.ClientID%>").value;
            var VLastName = document.getElementById("<%=txtLastName.ClientID%>").value;
            var VMobile = document.getElementById("<%=txtMobNo.ClientID%>").value;
            var VAddress = document.getElementById("<%=txtAddress.ClientID%>").value;
            var VNoofVisit = document.getElementById("<%=txtNoofVisitors.ClientID%>").value;
            errors = [];
            if (VFirstName.trim() == "") {
                errors.push("Please enter first name.");
            }
            if (VLastName.trim() == "") {
                errors.push("Please enter last name.");
            }
            if (VMobile == "") {
                errors.push("Please enter your mobile number.");
            }
            else {
                if (VMobile.length != 10) {
                    errors.push("Mobile no should be 10 digits.");
                }
            }
            if (VAddress.trim() == "") {
                errors.push("Please enter address.");
            }
            var ddlPurposeofVisit = $('#<%=ddlPurposeofVisit.ClientID%>');
            var isVisible5 = ddlPurposeofVisit.is(':visible');
            if (isVisible5 == true) {
                var VPurpose = document.getElementById("<%=ddlPurposeofVisit.ClientID%>").value;
                if (VPurpose == 0) {
                    errors.push("Please select purpose of visit.");
                }
            }
            var txtPurposeofVisit = $('#<%=txt_purposeother.ClientID%>');
            var isVisible6 = txtPurposeofVisit.is(':visible');
            if (isVisible6 == true) {
                var VtxtPurpose = document.getElementById("<%=txt_purposeother.ClientID%>").value;
                if (VtxtPurpose.trim() == "") {
                    errors.push("Please enter purpose of visit.");
                }
            }
            var query = $('#<%=ddlDepartment.ClientID%>');
            var isVisible = query.is(':visible');
            if (isVisible == true) {
                var VDept = document.getElementById("<%=ddlDepartment.ClientID%>").value;
                if (VDept == 0) {
                    errors.push("Please select department.");
                }
            }
            var query1 = $('#<%=ddlWhomToVisit.ClientID%>');
            var isVisible1 = query1.is(':visible');
            if (isVisible1 == true) {
                var VWhomtoVisit = document.getElementById("<%=ddlWhomToVisit.ClientID%>").value;
                if (VWhomtoVisit == 0) {
                    errors.push("Please select whom to visit.");
                }
            }
            var query2 = $('#<%=txtDepartment.ClientID%>');
            var isVisible2 = query2.is(':visible');
            if (isVisible2 == true) {
                var VtxtDepartment = document.getElementById("<%=txtDepartment.ClientID%>").value;
                if (VtxtDepartment.trim() == "") {
                    errors.push("Please enter department.");
                }
            }
            var query3 = $('#<%=txtWhomToVisit.ClientID%>');
            var isVisible3 = query3.is(':visible');
            if (isVisible3 == true) {
                var VtxtWhomtovisit = document.getElementById("<%=txtWhomToVisit.ClientID%>").value;
                if (VtxtWhomtovisit.trim() == "") {
                    errors.push("Please enter whom to visit.");
                }
            }
            if (VNoofVisit.trim() == "" || VNoofVisit == 0) {
                errors.push("Please enter no of visitors.");
            }
            var email = document.getElementById("<%=txtVisitEmail.ClientID%>").value;
            if (email.length > 0) {
                if (!IsValidEmail(email)) {
                    errors.push("Invalid email address.");
                }
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }

    </script>
    <!--------End Visitor Submit Validation-------------->

    <!--------Visitor Update Validation-------------->
    <script type="text/javascript">
        function Updatevalidate() {
            var VFirstName = document.getElementById("<%=txtVisitorName.ClientID%>").value;
            var VLastName = document.getElementById("<%=txtLastName.ClientID%>").value;
            var VMobile = document.getElementById("<%=txtMobNo.ClientID%>").value;
            var VAddress = document.getElementById("<%=txtAddress.ClientID%>").value;
            var VNoofVisit = document.getElementById("<%=txtNoofVisitors.ClientID%>").value;
            errors = [];
            if (VFirstName.trim() == "") {
                errors.push("Please enter first name.");
            }
            if (VLastName.trim() == "") {
                errors.push("Please enter last name.");
            }
            if (VMobile == "") {
                errors.push("Please enter your mobile number.");
            }
            else {
                if (VMobile.length != 10) {
                    errors.push("Mobile no should be 10 digits.");
                }
            }
            if (VAddress.trim() == "") {
                errors.push("Please enter address.");
            }
            var ddlPurposeofVisit = $('#<%=ddlPurposeofVisit.ClientID%>');
            var isVisible5 = ddlPurposeofVisit.is(':visible');
            if (isVisible5 == true) {
                var VPurpose = document.getElementById("<%=ddlPurposeofVisit.ClientID%>").value;
                if (VPurpose == 0) {
                    errors.push("Please select purpose of visit.");
                }
            }
            var txtPurposeofVisit = $('#<%=txt_purposeother.ClientID%>');
            var isVisible6 = txtPurposeofVisit.is(':visible');
            if (isVisible6 == true) {
                var VtxtPurpose = document.getElementById("<%=txt_purposeother.ClientID%>").value;
                if (VtxtPurpose.trim() == "") {
                    errors.push("Please enter purpose of visit.");
                }
            }
            var query = $('#<%=ddlDepartment.ClientID%>');
            var isVisible = query.is(':visible');
            if (isVisible == true) {
                var VDept = document.getElementById("<%=ddlDepartment.ClientID%>").value;
                if (VDept == 0) {
                    errors.push("Please select department.");
                }
            }
            var query1 = $('#<%=ddlWhomToVisit.ClientID%>');
            var isVisible1 = query1.is(':visible');
            if (isVisible1 == true) {
                var VWhomtoVisit = document.getElementById("<%=ddlWhomToVisit.ClientID%>").value;
                if (VWhomtoVisit == 0) {
                    errors.push("Please select whom to visit.");
                }
            }
            var query2 = $('#<%=txtDepartment.ClientID%>');
            var isVisible2 = query2.is(':visible');
            if (isVisible2 == true) {
                var VtxtDepartment = document.getElementById("<%=txtDepartment.ClientID%>").value;
                if (VtxtDepartment.trim() == "") {
                    errors.push("Please enter department.");
                }
            }
            var query3 = $('#<%=txtWhomToVisit.ClientID%>');
            var isVisible3 = query3.is(':visible');
            if (isVisible3 == true) {
                var VtxtWhomtovisit = document.getElementById("<%=txtWhomToVisit.ClientID%>").value;
                if (VtxtWhomtovisit.trim() == "") {
                    errors.push("Please enter whom to visit.");
                }
            }
            if (VNoofVisit.trim() == "" || VNoofVisit == 0) {
                errors.push("Please enter no of visitors.");
            }
            if (VComments.trim() == "") {
                errors.push("Please enter modification reason in comments.");
            }
            var email = document.getElementById("<%=txtVisitEmail.ClientID%>").value;
            if (email.length > 0) {
                if (!IsValidEmail(email)) {
                    errors.push("Invalid email address.");
                }
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }
    </script>
    <!--------End Visitor Update Validation-------------->

    <script>
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
    </script>




    <!--------Visitor Get Datetimenow from Client Side-------------->
    <script>
        function getVisitorDate() {
            var dtTO = new Date();
            var Todate = document.getElementById("<%=txtIntime.ClientID%>");
            Todate.value = formatDate(dtTO);
        }
        function formatDate(dateObj) {
            var m_names = new Array("Jan", "Feb", "Mar",
                "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                "Oct", "Nov", "Dec");
            var curr_date = dateObj.getDate();
            var curr_month = dateObj.getMonth();
            var curr_year = dateObj.getFullYear();
            var curr_min = dateObj.getMinutes();
            var curr_hr = dateObj.getHours();
            var curr_sc = dateObj.getSeconds();
            return curr_date + " " + m_names[curr_month] + " " + curr_year + " " + curr_hr + ":" + curr_min + ":" + curr_sc;
        }
    </script>
    <!--------End Visitor Get Datetimenow from Client Side-------------->

    <!--------Visitor Page Redirection-------------->
    <script>
        function RedirecttoVisitorListPage() {
            window.open("<%=ResolveUrl("~/VMS/VisitorRegister/VisitorRegister.aspx")%>", "_self");
        }
        function _IsValidEmail(email) {
            if (email != '') {
                var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if (expr.test(email) == false) {
                    document.getElementById("<%=txtVisitEmail.ClientID%>").value = "";
                    custAlertMsg("Invalid email address", "error");

                    return false;
                }
                else { return true; }
            }
        };
  

        
$(document).keydown(function(objEvent) {
    if (objEvent.keyCode == 9) {  //tab pressed
        objEvent.preventDefault(); // stops its action
    }
})

</script>
    <!--------End Visitor Page Redirection-------------->
</asp:Content>
