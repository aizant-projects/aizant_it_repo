﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Webcam1.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.VisitorRegister.Webcam1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="<%=ResolveUrl("~/Content/bootstrap.css")%>" rel="stylesheet" />
    <script src="../../Scripts_WebCam/jquery-3.3.1.min.js"></script>
    <script src="../../Scripts_WebCam/bootstrap.min.js"></script>
    <script src="../../Scripts_WebCam/webcam/jquery.webcam.js"></script>
    <style>
        .div{
    display:inline !important
    }
    </style>
 
</head>
<body>
    

    
    <script type="text/javascript">
        var pageUrl = '<%=ResolveUrl("~/VMS/VisitorRegister/Webcam1.aspx") %>';
        $(function () {
            $("[id*=btnRetake]").hide();
            jQuery("#webcam").webcam({
                width: 400,
                height: 350,
                mode: "save",
                swffile: '<%=ResolveUrl("~/jquery_webcam/webcam/jscam.swf") %>',
                onSave: function (data) {
                    $.ajax({
                        type: "POST",
                        url: pageUrl + "/GetCapturedImage",
                        data: '',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (r) {
                            $("[id*=imgCapture]").css("display", "block");
                            $("[id*=imgCapture]").attr("src", r.d);
                           
                        },
                        failure: function (response) {
                            alert(response.d);
                        }
                    });
                },
                onCapture: function () {
                    webcam.save(pageUrl);
                }
            });
        });
        function Capture() {
            webcam.capture();
            return false;
        }
    </script>
    <form id="form1" runat="server">
        <div>

           
                <div class="col-sm-12">
                    <div class="col-sm-6 col-lg-4">
                    <div class="col-sm-12 float-left" style="margin-top:10px;" id="webcam">
                        
                    </div>
                    <div class="col-sm-12 float-left" style="text-align: left;">
                        <asp:Button ID="btnCapture" Text="Capture" CssClass=" col-sm-10 col-lg-11 btn btn-primary" runat="server" OnClientClick="return Capture();" />
                        <asp:Button ID="btnRetake" Text="Retake" CssClass="btnn" runat="server" Visible="false" />
                        <span id="camStatus"></span>
                    </div>
                   </div>
                    <div class="col-sm-6 col-lg-4 float-left">
                        <asp:Image ID="imgCapture" runat="server" Style="width: 400px; height:350px;border: 4px solid #d4d4d4; border-radius:4px; margin-top:10px;" />
                       </div>                   
                </div>
            </div>
        
    </form>
</body>
</html>
