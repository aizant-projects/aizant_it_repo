﻿using VMS_BAL;
using VMS_BO;
using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

using AizantIT_PharmaApp.Common;


namespace AizantIT_PharmaApp.VMS.VisitorRegister
{
    public partial class VisitorRegistrationList : System.Web.UI.Page
    {
        VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
        VisitorRegisterBO objvisitorRegisterBO;
        DataTable dt;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                       
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            ViewState["EmpID"] = "0";
                            if (HelpClass.IsUserAuthenticated())
                            {
                                ViewState["EmpID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            }
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL14:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL14:" + strline + " " + strMsg, "error");
            }
        }

        private void InitializeThePage()
        {
            try
            {
                screenAuthentication();
                GetDataFromVisitorsLatest();
                LoadDepartment();
                LoadPurposeofVisit();
                BindCountries();
                LoadIDProofType();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL15:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL15:" + strline + " " + strMsg, "error");
            }
        }
        private void LoadDepartment()
        {
            try
            {
                dt = new DataTable();
                dt = objVisitorRegisterBAL.LoadDepartment();
                ddlDepatmentSearchVisitors.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlDepatmentSearchVisitors.DataValueField = dt.Columns["DeptID"].ToString();
                ddlDepatmentSearchVisitors.DataSource = dt;
                ddlDepatmentSearchVisitors.DataBind();
                ddlDepatmentSearchVisitors.Items.Insert(0, new ListItem("All", "0"));

                ddlVEditDept.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlVEditDept.DataValueField = dt.Columns["DeptID"].ToString();
                ddlVEditDept.DataSource = dt;
                ddlVEditDept.DataBind();
                ddlVEditDept.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL1:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL1:" + strline + " " + strMsg, "error");
            }
        }

        private void LoadIDProofType()
        {
            try
            {
                dt = new DataTable();
                dt = objVisitorRegisterBAL.LoadddlIDProofType();
                ddlIDProofType.DataTextField = dt.Columns["IDCardTypeName"].ToString();
                ddlIDProofType.DataValueField = dt.Columns["IDCardTypeID"].ToString();
                ddlIDProofType.DataSource = dt;
                ddlIDProofType.DataBind();
                ddlIDProofType.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL2:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL2:" + strline + " " + strMsg, "error");
            }
        }

        private void LoadPurposeofVisit()
        {
            try
            {
                dt = new DataTable();
                dt = objVisitorRegisterBAL.LoadPurposeofVisit();
                ddlSearchVisitorsPurposeofVisit.DataTextField = dt.Columns["Purpose_of_Visit"].ToString();
                ddlSearchVisitorsPurposeofVisit.DataValueField = dt.Columns["Purpose_of_VisitID"].ToString();
                ddlSearchVisitorsPurposeofVisit.DataSource = dt;
                ddlSearchVisitorsPurposeofVisit.DataBind();
                ddlSearchVisitorsPurposeofVisit.Items.Insert(0, new ListItem("--Select--", "0"));

                ddlPurposeofVisit.DataTextField = dt.Columns["Purpose_of_Visit"].ToString();
                ddlPurposeofVisit.DataValueField = dt.Columns["Purpose_of_VisitID"].ToString();
                ddlPurposeofVisit.DataSource = dt;
                ddlPurposeofVisit.DataBind();
                ddlPurposeofVisit.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL3:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL3:" + strline + " " + strMsg, "error");
            }
        }

        //Manage Visitors Reset Code
        protected void btnSearchVisitorsReset_Click(object sender, EventArgs e)
        {
            ddlSearchVisitorsPurposeofVisit.SelectedIndex = 0;
            txtSearchVisitorsName.Text = txtSearchVisitorsID.Text = txtSearchVisitorsMobileNo.Text = txtSearchVisitorsOrganizationName.Text = txtSearchVisitorsFromDate.Text = 
            txtSearchVisitorsToDate.Text=txtSearchDepartment.Text = string.Empty;
            VisitorGridView.Visible = false;
            GetDataFromVisitorsLatest();
        }


        //Manage Visitors Find Code
        protected void btnSearchVisitorsFind_Click(object sender, EventArgs e)
        {
            try
            {
                objvisitorRegisterBO = new VisitorRegisterBO();
               
                objvisitorRegisterBO.Purpose_of_VisitID = Convert.ToInt32(ddlSearchVisitorsPurposeofVisit.SelectedValue);
                objvisitorRegisterBO.FirstName = txtSearchVisitorsName.Text.Trim();
                objvisitorRegisterBO.VisitorID = txtSearchVisitorsID.Text.Trim();
                objvisitorRegisterBO.MobileNo = txtSearchVisitorsMobileNo.Text.Trim();
                objvisitorRegisterBO.Organization = txtSearchVisitorsOrganizationName.Text.Trim();
                objvisitorRegisterBO.IDCardNo = string.Empty;
                objvisitorRegisterBO.SNo = 0;
                objvisitorRegisterBO.DeptName = txtSearchDepartment.Text.Trim();
                if (!string.IsNullOrEmpty(txtSearchVisitorsFromDate.Text.Trim()))
                {
                    objvisitorRegisterBO.InDateTime = Convert.ToDateTime(txtSearchVisitorsFromDate.Text.Trim()).ToString("yyyyMMdd");
                }
                else
                    objvisitorRegisterBO.InDateTime = txtSearchVisitorsFromDate.Text.Trim();
                if (!string.IsNullOrEmpty(txtSearchVisitorsToDate.Text.Trim()))
                {
                    objvisitorRegisterBO.OutDateTime = Convert.ToDateTime(txtSearchVisitorsToDate.Text.Trim()).ToString("yyyyMMdd");
                }
                else
                    objvisitorRegisterBO.OutDateTime = txtSearchVisitorsToDate.Text.Trim();

           
                dt = new DataTable();
                dt = objVisitorRegisterBAL.SearchFilterVisitor(objvisitorRegisterBO);
                VisitorGridView.DataSource = dt;
                VisitorGridView.DataBind();
                ViewState["dtVisitor"] = dt;
                VisitorGridView.Visible = true;
                ViewState["isDefaultData"] = false;
                
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL4:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL4:" + strline + " " + strMsg, "error");
            } 
        }

        private void GetDataFromVisitorsLatest()
        {
            objvisitorRegisterBO = new VisitorRegisterBO();
            objvisitorRegisterBO.Purpose_of_VisitID = 0;
            objvisitorRegisterBO.FirstName = string.Empty;
            objvisitorRegisterBO.VisitorID = string.Empty;
            objvisitorRegisterBO.MobileNo = string.Empty;
            objvisitorRegisterBO.Organization = string.Empty;
            objvisitorRegisterBO.IDCardNo = string.Empty;
            objvisitorRegisterBO.SNo = 0;
            objvisitorRegisterBO.DeptName = string.Empty;
            DateTime date = DateTime.Now.AddDays(-7);
            objvisitorRegisterBO.InDateTime = date.ToString("yyyyMMdd");
            objvisitorRegisterBO.OutDateTime = DateTime.Now.ToString("yyyyMMdd");
            dt = new DataTable();
            dt = objVisitorRegisterBAL.SearchFilterVisitor(objvisitorRegisterBO);
            VisitorGridView.DataSource = dt;
            VisitorGridView.DataBind();
            ViewState["dtVisitor"] = dt;
            VisitorGridView.Visible = true;
            ViewState["isDefaultData"] = true;
        }

        protected void VisitorGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            VisitorGridView.PageIndex = e.NewPageIndex;
            if (ViewState["dtVisitor"] != null)
            {
                VisitorGridView.DataSource = (DataTable)ViewState["dtVisitor"];
                VisitorGridView.DataBind();
            }
        }


        //Visitor Link Edit Button Code
        protected void LnkEditVisitor_Click(object sender, EventArgs e)
        {
            try
            {
                objvisitorRegisterBO = new VisitorRegisterBO();
                objvisitorRegisterBO.DeptName = string.Empty;
                objvisitorRegisterBO.Purpose_of_VisitID = 0;
                objvisitorRegisterBO.FirstName = string.Empty;
                objvisitorRegisterBO.VisitorID = string.Empty;
                objvisitorRegisterBO.MobileNo = string.Empty;
                objvisitorRegisterBO.Organization = string.Empty;
                objvisitorRegisterBO.IDCardNo = string.Empty;
                objvisitorRegisterBO.InDateTime = string.Empty;
                objvisitorRegisterBO.OutDateTime = string.Empty;
                LinkButton lnk = sender as LinkButton;
                GridViewRow gr = (GridViewRow)lnk.NamingContainer;
                string tempID = VisitorGridView.DataKeys[gr.RowIndex].Value.ToString();
                objvisitorRegisterBO.SNo = Convert.ToInt32(tempID);
                ViewState["tempId1"] = tempID;
                dt = new DataTable();
                dt = objVisitorRegisterBAL.SearchFilterVisitor(objvisitorRegisterBO);
                if (dt.Rows.Count > 0)
                {
                    txtVEditSNo.Text = dt.Rows[0]["SNo"].ToString();
                    txtVisitorID.Text= dt.Rows[0]["VisitorID"].ToString();
                    txtVisitorName.Text = dt.Rows[0]["FirstName"].ToString();
                    txtLastName.Text = dt.Rows[0]["LastName"].ToString();
                    txtMobNo.Text = dt.Rows[0]["MobileNo"].ToString();
                    txtVisitEmail.Text = dt.Rows[0]["Email"].ToString();
                    ddlPurposeofVisit.SelectedValue = dt.Rows[0]["Purpose_of_VisitID"].ToString();
                    txtVechileNo.Text = dt.Rows[0]["VechileNo"].ToString();
                    txtNoofVisitors.Text = dt.Rows[0]["NoofVisitors"].ToString();
                    txtOrganizeName.Text= dt.Rows[0]["Organization"].ToString();
                    txtPlanTimeOut.Text = dt.Rows[0]["PlannedOutTime"].ToString();
                    txtAccessLocation.Text = dt.Rows[0]["AccessLocation"].ToString();
                    txtAccompanyPerson.Text = dt.Rows[0]["AccompanyPerson"].ToString();
                    txtRoomNo.Text = dt.Rows[0]["RoomNo"].ToString();
                    txtRemarks.Text = dt.Rows[0]["Remarks"].ToString();
                    txtVEditComments.Text = dt.Rows[0]["LastChangedByComments"].ToString();
                    txtIntime.Text= dt.Rows[0]["InDateTime"].ToString();
                    txtBelongings.Text= dt.Rows[0]["Belongings"].ToString(); 
                    txtIdCardNo.Text= dt.Rows[0]["VisitorIDCardNo"].ToString();
                    txtIdProofNo.Text= dt.Rows[0]["IDCardNo"].ToString();
                    ddlIDProofType.SelectedValue= dt.Rows[0]["IDCardTypeID"].ToString();
                    txtAddress.Text= dt.Rows[0]["Address"].ToString();
                    ddlCountry.SelectedValue= dt.Rows[0]["CountryID"].ToString();
                    BindStates();
                    ddlState.SelectedValue= dt.Rows[0]["StateID"].ToString();
                    CitiesBind();
                    ddlCities.SelectedValue= dt.Rows[0]["CityID"].ToString();
                   
                    txtDepartment.Text = dt.Rows[0]["DeptName"].ToString();
                    txtWhomToVisit.Text = dt.Rows[0]["EmpName"].ToString();
                    if (!string.IsNullOrEmpty(dt.Rows[0]["Photo"].ToString()))
                    {
                        BindApplicantImage((byte[])dt.Rows[0]["PhotoFileData"]);
                    }
                    else
                    {
                        MyImage.ImageUrl = "~/Images/DefaultImage.jpg";
                    }
                    ModalPopupVisitor.Show();
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL5:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL5:" + strline + " " + strMsg, "error");
            }
        }

        private void BindApplicantImage(byte[] applicantImage)
        {
            string base64ImageString = ConvertBytesToBase64(applicantImage);
            MyImage.ImageUrl = "data:image/jpg;base64," + base64ImageString;
        }
        public string ConvertBytesToBase64(byte[] imageBytes)
        {
            return Convert.ToBase64String(imageBytes);
        }


        //Visitor Register BindData
        private void VisitorRegisterBind_Data()
        {
            try
            {
                if (Convert.ToBoolean(ViewState["isDefaultData"]) == false)
                {
                    btnSearchVisitorsFind_Click(null, null);
                }
                else
                {
                    GetDataFromVisitorsLatest();
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL6:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL6:" + strline + " " + strMsg, "error");
            }
        }

        protected void btnIDProofType_Click(object sender, EventArgs e)
        {
            if (btnIDProofType.Text == "+")
            {
                btnIDProofType.Text = "<<";
                txtIDProofType.Visible = true;
                ddlIDProofType.Visible = false;
            }
            else if (btnIDProofType.Text == "<<")
            {
                btnIDProofType.Text = "+";
                txtIDProofType.Visible = false;
                ddlIDProofType.Visible = true;
            }
        }


        //Visitor Register Update Code
        protected void btnVisitorUpdate_Click1(object sender, EventArgs e)
        {
            int abc;

            if (txtNoofVisitors.Text == "")
            { abc = 0; }

            else { abc = Convert.ToInt32(txtNoofVisitors.Text); }


            
            try
            {
                objvisitorRegisterBO = new VisitorRegisterBO();
                objvisitorRegisterBO.SNo = Convert.ToInt32(txtVEditSNo.Text);
                objvisitorRegisterBO.VisitorID = txtVisitorID.Text.Trim();
                objvisitorRegisterBO.FirstName = txtVisitorName.Text.Trim();
                objvisitorRegisterBO.LastName = txtLastName.Text.Trim();
               
                if (ddlIDProofType.SelectedIndex > 0)
                {
                    objvisitorRegisterBO.IDCardType = Convert.ToInt32(ddlIDProofType.SelectedValue);
                }
                ddlIDProofType.SelectedValue = "0";
                objvisitorRegisterBO.IDCardNo = txtIdProofNo.Text.Trim();
                objvisitorRegisterBO.MobileNo = txtMobNo.Text.Trim();
                objvisitorRegisterBO.Address = txtAddress.Text.Trim();
                objvisitorRegisterBO.CountryID = Convert.ToInt32(ddlCountry.SelectedValue.Trim());
                if (ddlState.SelectedIndex > 0)
                {
                    objvisitorRegisterBO.StateID = Convert.ToInt32(ddlState.SelectedValue.Trim());
                }
                else
                {
                    ddlState.SelectedValue = "0";
                }
                if (ddlCities.SelectedIndex > 0)
                {
                    objvisitorRegisterBO.CityID = Convert.ToInt32(ddlCities.SelectedValue.Trim());
                }
                ddlCities.SelectedValue = "0";
                objvisitorRegisterBO.Email = txtVisitEmail.Text.Trim();
                objvisitorRegisterBO.Organization = txtOrganizeName.Text.Trim();
                
                if (ddlPurposeofVisit.SelectedIndex > 0)
                {
                    objvisitorRegisterBO.Purpose_of_VisitID = Convert.ToInt32(ddlPurposeofVisit.SelectedValue);
                }
                else
                {
                    ddlPurposeofVisit.SelectedValue = "0";
                }
                objvisitorRegisterBO.Remarks = txtRemarks.Text.Trim();
                if (ddlVEditDept.SelectedIndex > 0)
                {
                    objvisitorRegisterBO.Department = Convert.ToInt32(ddlVEditDept.SelectedValue);
                }
                else
                {
                    ddlVEditDept.SelectedValue = "0";
                }
                if (ddlWhomtoVisit.SelectedIndex > 0)
                {
                    objvisitorRegisterBO.Whom_To_VisitID = Convert.ToInt32(ddlWhomtoVisit.SelectedValue);
                }
                else
                {
                    ddlWhomtoVisit.SelectedValue = "0";
                }
                
                objvisitorRegisterBO.DeptName = txtDepartment.Text.Trim();
                objvisitorRegisterBO.EmpName = txtWhomToVisit.Text.Trim();
                objvisitorRegisterBO.VisitorIDCardNo = txtIdCardNo.Text.Trim();
                objvisitorRegisterBO.AccessLocation = txtAccessLocation.Text.Trim();
                objvisitorRegisterBO.AccompanyPerson = txtAccompanyPerson.Text.Trim();
                objvisitorRegisterBO.RoomNo = txtRoomNo.Text.Trim();
                objvisitorRegisterBO.NoofVisitors = Convert.ToInt32(abc.ToString());
                objvisitorRegisterBO.VechileNo = txtVechileNo.Text.Trim();
                objvisitorRegisterBO.Belongings = txtBelongings.Text.Trim();
                objvisitorRegisterBO.PlannedOutTime = txtPlanTimeOut.Text.Trim();
                objvisitorRegisterBO.LastChangedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                objvisitorRegisterBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                objvisitorRegisterBO.LastChangedComments = txtVEditComments.Text;
                int c = objVisitorRegisterBAL.Update_Visitor(objvisitorRegisterBO);
                if (c > 0)
                {
                    HelpClass.custAlertMsg(btnVisitorUpdate, btnVisitorUpdate.GetType(), "Visitor Details Updated Successfully.", "success");
                   
                    VisitorRegisterBind_Data();
                }
                else
                {
                    HelpClass.custAlertMsg(btnVisitorUpdate, btnVisitorUpdate.GetType(), "Visitor Details Updation Failed.", "error");
                    
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL7:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL7:" + strline + " " + strMsg, "error");
            }  
        }

        protected void VisitorGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblOuttime = (Label)(e.Row.FindControl("lblGVVisitorOutDateTime"));
                LinkButton lbtnCheckin = (LinkButton)(e.Row.FindControl("lnkCheckOutVisitor"));
                
                if (lblOuttime != null)
                {
                    if (lblOuttime.Text.Length > 6)
                        lblOuttime.Visible = true;
                    else
                        lbtnCheckin.Visible = true;
                }
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblOuttime = (Label)(e.Row.FindControl("lblGVVisitorOutDateTime"));
                LinkButton lnkbtnEdit = (LinkButton)(e.Row.FindControl("LnkEditVisitor"));
                if (lblOuttime != null)
                {
                    if (lblOuttime.Text.Length > 5)
                    {
                        lnkbtnEdit.Visible = true;
                    }
                    else
                    {
                        lnkbtnEdit.Visible = true;
                    }
                }
            }
        }

        protected void lnkCheckOutVisitor_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", " getDate();", true);

            try
            {
                if (ViewState["vwCheckoutcount"].ToString() != "0")
                {
                    LinkButton bt = (LinkButton)sender;
                    GridViewRow gr = (GridViewRow)bt.NamingContainer;
                    Label lbl_id = (Label)gr.FindControl("lblGSno");
                    LinkButton lnk = (LinkButton)gr.FindControl("lnkCheckOutVisitor");
                    Label lbl_Name = (Label)gr.FindControl("lblGVisitorName");
                    objvisitorRegisterBO = new VisitorRegisterBO();
                    objvisitorRegisterBO.SNo = Convert.ToInt32(lbl_id.Text);
                   
                    objvisitorRegisterBO.OutDateTime = hdnCheckout.Value;
                    objvisitorRegisterBO.FirstName = lbl_Name.Text;
                    int c = objVisitorRegisterBAL.CheckoutVisitor(objvisitorRegisterBO);
                    if (c > 0)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), " " + lbl_Name.Text + "  Checked Out Successfully.", "error");
                        
                        VisitorRegisterBind_Data();
                    }
                }
                else
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Access is denied ", "error");
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL8:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL8:" + strline + " " + strMsg, "error");
            }
        }

        protected void addpurposeofvisit(object sender, EventArgs e)
        {
            if (btn_purposeofvisit.Text == "+")
            {
                btn_purposeofvisit.Text = "<<";
                txt_purposeother.Visible = true;
                ddlPurposeofVisit.Visible = false;
            }
            else if (btn_purposeofvisit.Text == "<<")
            {
                btn_purposeofvisit.Text = "+";
                txt_purposeother.Visible = false;
                ddlPurposeofVisit.Visible = true;
            }
        }
       
       protected void ddlVEditDept_SelectedIndexChanged(object sender, EventArgs e)
        {
                WhomToVisit(); 
        }

        public void WhomToVisit()
        {
            try
            {
                int DeptID = Convert.ToInt32(ddlVEditDept.SelectedValue);
                DataTable dtEmp = objVisitorRegisterBAL.GetEmpData(DeptID);
                ddlWhomtoVisit.DataSource = dtEmp;
                ddlWhomtoVisit.DataTextField = "Name";
                ddlWhomtoVisit.DataValueField = "EmpID";
                ddlWhomtoVisit.DataBind();
                ddlWhomtoVisit.Items.Insert(0, new ListItem("-- Select --", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL9:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL9:" + strline + " " + strMsg, "error");
            }
        }

        private void BindStates()
        {
            try
            {
                ddlState.Enabled = false;
                ddlState.Items.Clear();
                ddlState.Items.Insert(0, new ListItem("-- Select State --", "0"));
                int CountryID = Convert.ToInt32(ddlCountry.SelectedValue);
                if (CountryID > 0)
                {
                    DataTable dtStates = new DataTable();
                    dtStates = objVisitorRegisterBAL.BindStatesBAL(CountryID);
                    ddlState.DataSource = dtStates;
                    ddlState.DataTextField = "StateName";
                    ddlState.DataValueField = "StateID";
                    ddlState.DataBind();
                    ddlState.Items.Insert(0, new ListItem("-- Select State --", "0"));
                    ddlState.Enabled = true;
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL10:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL10:" + strline + " " + strMsg, "error");
            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCities.Items.Clear();
            ddlState.Items.Clear();
            BindStates();
        }

        private void CitiesBind()
        {
            try
            {
                ddlCities.Enabled = false;
                ddlCities.Items.Clear();
                ddlCities.Items.Insert(0, new ListItem("-- Select City --", "0"));
                int StateID = Convert.ToInt32(ddlState.SelectedValue);
                if (StateID > 0)
                {
                    DataTable dtCities = new DataTable();
                    dtCities = objVisitorRegisterBAL.BindCities(StateID);
                    ddlCities.DataSource = dtCities;
                    ddlCities.DataTextField = "CityName";
                    ddlCities.DataValueField = "CityID";
                    ddlCities.DataBind();
                    ddlCities.Items.Insert(0, new ListItem("-- Select City --", "0"));
                    ddlCities.Enabled = true;
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL11:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL11:" + strline + " " + strMsg, "error");
            } 
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            CitiesBind();
        }

        public void BindCountries()
        {
            try
            {
                ddlCountry.Items.Insert(0, new ListItem("-- Select Country --", "0"));
                DataTable dt = objVisitorRegisterBAL.BindCountriesBAL();
                ddlCountry.DataSource = dt;
                ddlCountry.DataTextField = "CountryName";
                ddlCountry.DataValueField = "CountryID";
                ddlCountry.DataBind();
                ddlCountry.Items.Insert(0, new ListItem("-- Select Country --", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL12:" + strmsgF,"error");
               
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL12:" + strline + "  " + strMsg, "error");
            }
        }

        public void BindStates(int SelectedCountryID)
        {
            try
            {
                DataTable dt = objVisitorRegisterBAL.BindStatesBAL(SelectedCountryID);
                ddlState.DataSource = dt;
                ddlState.DataTextField = "StateName";
                ddlState.DataValueField = "StateID";
                ddlState.DataBind();
                ddlState.Items.Insert(0, new ListItem("-- Select State --", "0"));
                ddlState.Enabled = true;
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL13:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRL13:" + strline + " " + strMsg, "error");
            }
        }
        void screenAuthentication()
        {
            DataTable dtAthuntication = GmsHelpClass.GetRolePrivileges();
            if (dtAthuntication.Rows.Count > 0)
            {
                //mod=1,PrivilegeID=1,2,3,4

                int Checkincount = dtAthuntication.AsEnumerable()
           .Count(row => row.Field<int>("PrivilegeID") == 1);
                int Printcount = dtAthuntication.AsEnumerable()
         .Count(row => row.Field<int>("PrivilegeID") == 2);
                int Checkoutcount = dtAthuntication.AsEnumerable()
         .Count(row => row.Field<int>("PrivilegeID") == 3);
                ViewState["vwCheckoutcount"] = Checkoutcount.ToString();
                int Modifycount = dtAthuntication.AsEnumerable()
         .Count(row => row.Field<int>("PrivilegeID") == 4);
                if(Checkincount==0 && Checkoutcount==0 && Modifycount==0)
                    Response.Redirect("~/UserLogin.aspx");
                
                if (Printcount == 0)
                    VisitorGridView.Columns[11].Visible = false;
                if (Modifycount == 0)
                {
                    VisitorGridView.Columns[12].HeaderText = "VIEW";
                    btnVisitorUpdate.Visible = false;
                    pttlModifyVisitor.InnerText = "View Visitor Details";
                }
                  
                if (Modifycount > 0)
                {
                    VisitorGridView.Columns[12].HeaderText = "EDIT";
                    btnVisitorUpdate.Visible = true;
                    pttlModifyVisitor.InnerText = "Modify Visitor Details";
                }

            }
        }

        
    }
}