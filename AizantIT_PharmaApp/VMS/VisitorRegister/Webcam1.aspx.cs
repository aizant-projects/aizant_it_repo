﻿using AizantIT_PharmaApp.Common;
using System;
using System.IO;
using System.Web;
using System.Web.Services;


namespace AizantIT_PharmaApp.VMS.VisitorRegister
{
    public partial class Webcam1 : System.Web.UI.Page
    {
        string imageName, imagePath;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                CaptureMethod();
        }
        private void CaptureMethod()
        {
            if (Request.InputStream.Length > 0)
            {
                using (StreamReader reader = new StreamReader(Request.InputStream))
                {
                    string hexString = Server.UrlEncode(reader.ReadToEnd());
                    imageName = DateTime.Now.ToString("ddMMyyyyhhmmss");
                    Session["Imagename"] = imageName;
                    imagePath = string.Format("~/Captures/{0}.png", imageName);
                    File.WriteAllBytes(Server.MapPath(imagePath), FileHelper.ConvertHexToBytes(hexString));
                    Session["CapturedImage"] = ResolveUrl(imagePath);
                }
            }
        }

        [WebMethod(EnableSession = true)]
        public static string GetCapturedImage()
        {
            string url = HttpContext.Current.Session["CapturedImage"].ToString();
           // HttpContext.Current.Session["CapturedImage"] = null;
            return url;
        }
    }
}