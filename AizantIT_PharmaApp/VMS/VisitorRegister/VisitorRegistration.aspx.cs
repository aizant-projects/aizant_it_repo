﻿using VMS_BAL;
using VMS_BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Web.Services;
using AizantIT_PharmaApp.Common;
using System.Text;
using AizantIT_PharmaApp.VMS.DevReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;
using System.Collections;

namespace AizantIT_PharmaApp.VMS.VisitorRegister
{
    public partial class VisitorRegistration : System.Web.UI.Page
    {
        VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
        VisitorRegisterBO objvisitorRegisterBO;
        string imageName, imagePath;
        DataTable dt;
        static int PK_IDVisitor = 0;
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ConfirmCustomAlert.buttonClick += new EventHandler(ConfirmCustomAlert_buttonClick);
                if (!IsPostBack)
                {
                    txtIntime.Attributes.Add("readonly", "readonly");
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            ViewState["EmpID"] = "0";
                            ViewState["EmpID"] = Convert.ToInt32(((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                            ViewState["RoleID"] = Session["sesGMSRoleID"].ToString();
                            InitializeThePage();
                            try
                            {
                                Array.ForEach(Directory.GetFiles(Server.MapPath(@"~/Captures/")), delegate (string path) { File.Delete(path); });
                            }
                            catch
                            {

                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR1:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVR1:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR1:" + strline + " " + strMsg, "error");
            }
        }

        private void InitializeThePage()
        {
            try
            {
                screenAuthentication();
                
                HideEmpandDeptDropdownforSecurity();
                LoadIDProofType();
                LoadDepartment();
                LoadPurposeofVisit();
               
                TimeDate();
                VisitorAutoGenerateID();
                BindCountries();

                txtVisitorName.Focus();
                
                if (Request.QueryString.Count > 0)
                {
                    PK_IDVisitor = Convert.ToInt32(Request.QueryString["VID"]);
                    hdnVisitorRegisterCustom.Value = "Update";
                    GetVisitorData(PK_IDVisitor);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", " getVisitorDate();", true);
                    btnIDProofType.Visible = true;
                    btn_purposeofvisit.Visible = true;
                    btnUpdate.Visible = false;
                    
                    hdnVisitorRegisterCustom.Value = "Submit";
                    VisitorRegisterEnableControls();
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR2:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVR2:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR2:" + strline + " " + strMsg, "error");
            }
        }

        private void HideEmpandDeptDropdownforSecurity()
        {
            if (ViewState["RoleID"].ToString() == "22")
            {
                ddlDepartment.Visible = false;
                ddlWhomToVisit.Visible = false;
                txtDepartment.Visible = true;
                txtWhomToVisit.Visible = true;
            }
            else
            {
                ddlDepartment.Visible = true;
                ddlWhomToVisit.Visible = true;
                txtDepartment.Visible = false;
                txtWhomToVisit.Visible = false;
            }
        }
        private void TimeDate()
        {
           
            txtNoofVisitors.Text = "1";
        }

        public void VisitorAutoGenerateID()
        {
            try
            {
                string strVID = objVisitorRegisterBAL.VisitorAutoGenerateID();
                txtVisitorID.Text = strVID;
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR3:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVR3:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR3:" + strline + " " + strMsg, "error");
            }
        }
        private void LoadIDProofType()
        {
            try
            {
                dt = new DataTable();
                dt = objVisitorRegisterBAL.LoadddlIDProofType();
                ddlIDProofType.DataTextField = dt.Columns["IDCardTypeName"].ToString();
                ddlIDProofType.DataValueField = dt.Columns["IDCardTypeID"].ToString();
                ddlIDProofType.DataSource = dt;
                ddlIDProofType.DataBind();
                ddlIDProofType.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR4:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVR4:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR4:" + strline + " " + strMsg, "error");
            }
        }
        private void LoadDepartment()
        {
            try
            {
                dt = new DataTable();
                dt = objVisitorRegisterBAL.LoadDepartment();
                ddlDepartment.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlDepartment.DataValueField = dt.Columns["DeptID"].ToString();
                ddlDepartment.DataSource = dt;
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR5:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVR5:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR5:" + strline + " " + strMsg, "error");
            }
        }

        private void LoadPurposeofVisit()
        {
            try
            {
                dt = new DataTable();
                dt = objVisitorRegisterBAL.LoadPurposeofVisit();
                ddlPurposeofVisit.DataTextField = dt.Columns["Purpose_of_Visit"].ToString();
                ddlPurposeofVisit.DataValueField = dt.Columns["Purpose_of_VisitID"].ToString();
                ddlPurposeofVisit.DataSource = dt;
                ddlPurposeofVisit.DataBind();
                ddlPurposeofVisit.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR6:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVR6:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR6:" + strline + " " + strMsg, "error");
            }
        }

    

    

        private void CaptureMethod()
        {
            if (Request.InputStream.Length > 0)
            {
                using (StreamReader reader = new StreamReader(Request.InputStream))
                {
                    string hexString = Server.UrlEncode(reader.ReadToEnd());
                    imageName = DateTime.Now.ToString("ddMMyyyyhhmmss");
                    Session["Imagename"] = imageName;
                    imagePath = string.Format("~/Captures/{0}.png", imageName);
                    File.WriteAllBytes(Server.MapPath(imagePath), FileHelper.ConvertHexToBytes(hexString));
                    Session["CapturedImage"] = ResolveUrl(imagePath);
                }
            }
        }

        [WebMethod(EnableSession = true)]
        public static string GetCapturedImage()
        {
            string url = HttpContext.Current.Session["CapturedImage"].ToString();
            //HttpContext.Current.Session["CapturedImage"] = null;
            return url;
        }

        //Visitor Insert Code
        protected void btnInsert_Click(object sender, EventArgs e)
        {
            if (ViewState["RoleID"].ToString() == "22")//here for security visitor validation(Dept,whomtovisit are entery)
            {
                if (txtVisitorName.Text.Trim() == "" || txtLastName.Text.Trim() == "" || txtMobNo.Text.Trim() == "" || txtMobNo.Text.Length != 10 || txtNoofVisitors.Text.Trim() == "" || txtAddress.Text.Trim() == "" || txtDepartment.Text.Trim() == "" || txtWhomToVisit.Text.Trim() == "")
                {
                    ArrayList Mandatory = new ArrayList();

                    if (string.IsNullOrEmpty(txtVisitorName.Text.Trim()))
                    {
                        Mandatory.Add("Please enter first name.");
                    }
                    if (String.IsNullOrEmpty(txtLastName.Text.Trim()))
                    {
                        Mandatory.Add("Please enter last name.");
                    }
                    if (string.IsNullOrEmpty(txtMobNo.Text.Trim()))
                    {
                        Mandatory.Add("Please enter mobile no.");
                    }
                    else if (txtMobNo.Text.Length != 10)
                    {
                        Mandatory.Add("Mobile no should be 10 digits.");
                    }
                    if (String.IsNullOrEmpty(txtAddress.Text.Trim()))
                    {
                        Mandatory.Add("Please enter address.");
                    }
                    if (txt_purposeother.Visible == true)
                    {
                        if (string.IsNullOrEmpty(txt_purposeother.Text.Trim()))
                        {
                            Mandatory.Add("Please enter purpose of visit.");
                        }
                    }
                    if (ddlPurposeofVisit.Visible == true)
                    {
                        if (ddlPurposeofVisit.SelectedIndex == 0)
                        {
                            Mandatory.Add("Please select purpose of visit.");
                        }
                    }
                    if (txtDepartment.Visible == true)
                    {
                        if (String.IsNullOrEmpty(txtDepartment.Text.Trim()))
                        {
                            Mandatory.Add("Please enter department.");
                        }
                    }
                    if (txtWhomToVisit.Visible == true)
                    {
                        if (String.IsNullOrEmpty(txtWhomToVisit.Text.Trim()))
                        {
                            Mandatory.Add("Please enter whom to visit.");
                        }
                    }
                    if (ddlDepartment.Visible == true)
                    {
                        if (ddlDepartment.SelectedIndex == 0)
                        {
                            Mandatory.Add("Please select department.");
                        }
                    }
                    if (ddlWhomToVisit.Visible == true)
                    {
                        if (ddlWhomToVisit.SelectedIndex == 0 || ddlWhomToVisit.SelectedValue == "")
                        {
                            Mandatory.Add("Please select whom to visit.");
                        }
                    }
                    if (string.IsNullOrEmpty(txtNoofVisitors.Text.Trim()))
                    {
                        Mandatory.Add("Please enter no of visitors.");
                    }
                    StringBuilder s = new StringBuilder();
                    foreach (string x in Mandatory)
                    {
                        s.Append(x);
                        s.Append("<br/>");
                    }
                    HelpClass.custAlertMsg(this, this.GetType(), "" + s.ToString() + "", "error");
                    return;
                }
            }
            else
            {
                if (txtVisitorName.Text.Trim() == "" || txtLastName.Text.Trim() == "" || txtMobNo.Text.Trim() == "" || txtMobNo.Text.Length != 10 || txtNoofVisitors.Text.Trim() == "" || txtAddress.Text.Trim() == "" || ddlDepartment.SelectedValue == "0" || ddlWhomToVisit.SelectedValue == "0")
                {
                    ArrayList Mandatory = new ArrayList();

                    if (string.IsNullOrEmpty(txtVisitorName.Text.Trim()))
                    {
                        Mandatory.Add("Please enter first name.");
                    }
                    if (String.IsNullOrEmpty(txtLastName.Text.Trim()))
                    {
                        Mandatory.Add("Please enter last name.");
                    }
                    if (string.IsNullOrEmpty(txtMobNo.Text.Trim()))
                    {
                        Mandatory.Add("Please enter mobile no.");
                    }
                    else if (txtMobNo.Text.Length != 10)
                    {
                        Mandatory.Add("Mobile no should be 10 digits.");
                    }
                    if (String.IsNullOrEmpty(txtAddress.Text.Trim()))
                    {
                        Mandatory.Add("Please enter address.");
                    }
                    if (txt_purposeother.Visible == true)
                    {
                        if (string.IsNullOrEmpty(txt_purposeother.Text.Trim()))
                        {
                            Mandatory.Add("Please enter purpose of visit.");
                        }
                    }
                    if (ddlPurposeofVisit.Visible == true)
                    {
                        if (ddlPurposeofVisit.SelectedIndex == 0)
                        {
                            Mandatory.Add("Please select purpose of visit.");
                        }
                    }
                    if (txtDepartment.Visible == true)
                    {
                        if (String.IsNullOrEmpty(txtDepartment.Text.Trim()))
                        {
                            Mandatory.Add("Please enter department.");
                        }
                    }
                    if (txtWhomToVisit.Visible == true)
                    {
                        if (String.IsNullOrEmpty(txtWhomToVisit.Text.Trim()))
                        {
                            Mandatory.Add("Please enter whom to visit.");
                        }
                    }
                    if (ddlDepartment.Visible == true)
                    {
                        if (ddlDepartment.SelectedIndex == 0)
                        {
                            Mandatory.Add("Please select department.");
                        }
                    }
                    if (ddlWhomToVisit.Visible == true)
                    {
                        if (ddlWhomToVisit.SelectedIndex == 0 || ddlWhomToVisit.SelectedValue == "")
                        {
                            Mandatory.Add("Please select whom to visit.");
                        }
                    }
                    if (string.IsNullOrEmpty(txtNoofVisitors.Text.Trim()))
                    {
                        Mandatory.Add("Please enter no of visitors.");
                    }
                    StringBuilder s = new StringBuilder();
                    foreach (string x in Mandatory)
                    {
                        s.Append(x);
                        s.Append("<br/>");
                    }
                    HelpClass.custAlertMsg(this, this.GetType(), "" + s.ToString() + "", "error");
                    return;
                }
            }
            hdnVisitorRegisterCustom.Value = "Submit";
            HelpClass.custAlertMsg(this, this.GetType(), "Do you want to submit ?", "confirm");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", " getVisitorDate();", true);
        }

        protected void ConfirmCustomAlert_buttonClick(object sender, EventArgs e)
        {
            if (hdnVisitorRegisterCustom.Value == "Submit")
            {
                try
                {
                    btnInsert.Enabled = false;
                    byte[] Abcd = null;
                    string str1;
                    string ImgName;
                    if (Session["CapturedImage"] != null)
                    {
                        str1 = Session["CapturedImage"].ToString();
                        ImgName = Session["Imagename"].ToString();
                        str1 = Server.MapPath(str1);
                        Abcd = FileHelper.byteImg(str1);
                    }
                    else
                    {
                        str1 = "";
                        ImgName = "";
                    }
                    
                    int abc;

                    if (txtNoofVisitors.Text == "")
                    { abc = 0; }

                    else { abc = Convert.ToInt32(txtNoofVisitors.Text); }

                    string PurposeOfVisit;
                    if (txt_purposeother.Visible == true)
                    {
                        objvisitorRegisterBO = new VisitorRegisterBO();
                        objvisitorRegisterBO.PurposeofVisitTxt = txt_purposeother.Text.Trim();
                        PurposeOfVisit = objVisitorRegisterBAL.PurposeOfVisitUpdate(objvisitorRegisterBO).ToString();
                        if (PurposeOfVisit == "0")
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Purpose of visit already exists.", "error");
                            return;
                        }
                    }
                    else
                    {
                        PurposeOfVisit = ddlPurposeofVisit.SelectedValue;
                    }

                    string IDProoftype;
                    if (txtIDProofType.Visible == true)
                    {
                        objvisitorRegisterBO = new VisitorRegisterBO();
                        objvisitorRegisterBO.IDProofTxt = txtIDProofType.Text.Trim();
                        IDProoftype = objVisitorRegisterBAL.IDProofTypeUpdate(objvisitorRegisterBO).ToString();
                        if (IDProoftype == "0")
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "IDProof type already exists.", "error");
                            return;
                        }
                    }
                    else
                    {
                        IDProoftype = ddlIDProofType.SelectedValue;
                    }
                    objvisitorRegisterBO = new VisitorRegisterBO();
                    objvisitorRegisterBO.VisitorID = txtVisitorID.Text.Trim();
                    objvisitorRegisterBO.FirstName = txtVisitorName.Text.Trim();
                    objvisitorRegisterBO.LastName = txtLastName.Text.Trim();
                    objvisitorRegisterBO.IDCardType = Convert.ToInt32(IDProoftype);
                    objvisitorRegisterBO.IDCardNo = txtIdProofNo.Text.Trim();
                    objvisitorRegisterBO.MobileNo = txtMobNo.Text.Trim();
                    objvisitorRegisterBO.Address = txtAddress.Text.Trim();
                    if (ddlCountry.SelectedIndex > 0)
                    {
                        objvisitorRegisterBO.CountryID = Convert.ToInt32(ddlCountry.SelectedValue.Trim());
                    }
                    else
                    {
                        ddlCountry.SelectedValue = "0";
                    }
                    if (ddlState.SelectedIndex > 0)
                    {
                        objvisitorRegisterBO.StateID = Convert.ToInt32(ddlState.SelectedValue.Trim());
                    }
                    else
                    {
                        ddlState.SelectedValue = "0";
                    }
                    if (ddlCities.SelectedIndex > 0)
                    {
                        objvisitorRegisterBO.CityID = Convert.ToInt32(ddlCities.SelectedValue.Trim());
                    }
                    else
                    {
                        ddlCities.SelectedValue = "0";
                    }
                    objvisitorRegisterBO.Email = txtVisitEmail.Text.Trim();
                    objvisitorRegisterBO.Organization = txtOrganizeName.Text.Trim();
                    objvisitorRegisterBO.Purpose_of_VisitID = Convert.ToInt32(PurposeOfVisit);
                    objvisitorRegisterBO.Remarks = txtRemarks.Text.Trim();
                    if (ddlWhomToVisit.SelectedIndex > 0)
                    {
                        objvisitorRegisterBO.Whom_To_VisitID = Convert.ToInt32(ddlWhomToVisit.SelectedValue);
                    }
                    else
                    {
                        ddlWhomToVisit.SelectedValue = "0";
                    }
                    objvisitorRegisterBO.DeptName = txtDepartment.Text.Trim();
                    objvisitorRegisterBO.EmpName = txtWhomToVisit.Text.Trim();
                   
                    objvisitorRegisterBO.InDateTime = DateTime.Now.ToString("yyyyMMdd HH:mm:ss"); // txtIntime.Text.Trim();
                    objvisitorRegisterBO.Photo = ImgName;
                    objvisitorRegisterBO.PhotoFileData = Abcd;
                    objvisitorRegisterBO.VisitorIDCardNo = txtIdCardNo.Text.Trim();
                    objvisitorRegisterBO.AccessLocation = txtAccessLocation.Text.Trim();
                    objvisitorRegisterBO.AccompanyPerson = txtAccompanyPerson.Text.Trim();
                    objvisitorRegisterBO.RoomNo = txtRoomNo.Text.Trim();
                    objvisitorRegisterBO.NoofVisitors = Convert.ToInt32(abc.ToString());
                    objvisitorRegisterBO.VechileNo = txtVechileNo.Text.Trim();
                    objvisitorRegisterBO.Belongings = txtBelongings.Text.Trim();
                    objvisitorRegisterBO.PlannedOutTime = txtxPlanTimeOut.Text.Trim();
                    objvisitorRegisterBO.CreatedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                    objvisitorRegisterBO.CreatedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                    

                    int VSID = objVisitorRegisterBAL.Visitor_Insert(objvisitorRegisterBO);
                    divcomments.Visible = false;
                   
                    if (VSID > 0)
                    {
                        Session["ID"] = txtVisitorID.Text;
                        VisitorAutoGenerateID();
                        LoadIDProofType();
                        LoadPurposeofVisit();
                        Visitor_Reset();
                        txtIntime.Text = DateTime.Now.ToString();
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Visitor details submitted successfully.", "success", "RedirecttoVisitorListPage()");
                        
                        if (ViewState["vwPrintcount"].ToString() != "0")
                        {
                           

                            string filename2 = string.Empty;
                            PrintVisitorPass(VSID, ref filename2);
                            Literal1.Text = "<embed src=\"" + ResolveUrl("~/VMS/PdfFolder/" + filename2) + "#toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" height=\"620px\" />";
                            UpViewDL.Update();
                            UpHeading.Update();
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_lock').modal({ show: 'true', backdrop: 'static', keyboard: false });", true);
                        }
                    }
                    else
                    {
                        HelpClass.custAlertMsg(btnInsert, btnInsert.GetType(), "Visitor details submission failed.", "error");
                    }

                }
                catch (SqlException sqe)
                {
                    string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                    HelpClass.custAlertMsg(this, this.GetType(), "VMSVR9:" + strmsgF, "error");

                }
                catch (Exception ex)
                {
                    string strline = HelpClass.LineNo(ex);
                    string strMsg = HelpClass.SQLEscapeString(ex.Message);
                    Aizant_log.Error("VMSVR9:" + strline + " " + strMsg);
                    HelpClass.custAlertMsg(this, this.GetType(), "VMSVR9:" + strline + " " + strMsg, "error");

                }
                finally
                {
                    btnInsert.Enabled = true;
                }
            }
            else if (hdnVisitorRegisterCustom.Value == "Update")
            {
                try
                {
                    objvisitorRegisterBO = new VisitorRegisterBO();
                    objvisitorRegisterBO.SNo = Convert.ToInt32(PK_IDVisitor);
                    objvisitorRegisterBO.VisitorID = txtVisitorID.Text.Trim();
                    objvisitorRegisterBO.FirstName = txtVisitorName.Text.Trim();
                    objvisitorRegisterBO.LastName = txtLastName.Text.Trim();
                    if (ddlIDProofType.SelectedIndex > 0)
                    {
                        objvisitorRegisterBO.IDCardType = Convert.ToInt32(ddlIDProofType.SelectedValue);
                    }
                    ddlIDProofType.SelectedValue = "0";
                    objvisitorRegisterBO.IDCardNo = txtIdProofNo.Text.Trim();
                    objvisitorRegisterBO.MobileNo = txtMobNo.Text.Trim();
                    objvisitorRegisterBO.Address = txtAddress.Text.Trim();
                    objvisitorRegisterBO.CountryID = Convert.ToInt32(ddlCountry.SelectedValue.Trim());
                    if (ddlState.SelectedIndex > 0)
                    {
                        objvisitorRegisterBO.StateID = Convert.ToInt32(ddlState.SelectedValue.Trim());
                    }
                    else
                    {
                        ddlState.SelectedValue = "0";
                    }
                    if (ddlCities.SelectedIndex > 0)
                    {
                        objvisitorRegisterBO.CityID = Convert.ToInt32(ddlCities.SelectedValue.Trim());
                    }
                    ddlCities.SelectedValue = "0";
                    objvisitorRegisterBO.Email = txtVisitEmail.Text.Trim();
                    objvisitorRegisterBO.Organization = txtOrganizeName.Text.Trim();
                    if (ddlPurposeofVisit.SelectedIndex > 0)
                    {
                        objvisitorRegisterBO.Purpose_of_VisitID = Convert.ToInt32(ddlPurposeofVisit.SelectedValue);
                    }
                    else
                    {
                        ddlPurposeofVisit.SelectedValue = "0";
                    }
                    objvisitorRegisterBO.Remarks = txtRemarks.Text.Trim();
                    if (ddlDepartment.SelectedIndex > 0)
                    {
                        objvisitorRegisterBO.Department = Convert.ToInt32(ddlDepartment.SelectedValue);
                    }
                    else
                    {
                        ddlDepartment.SelectedValue = "0";
                    }
                    if (ddlWhomToVisit.SelectedIndex > 0)
                    {
                        objvisitorRegisterBO.Whom_To_VisitID = Convert.ToInt32(ddlWhomToVisit.SelectedValue);
                    }
                    else
                    {
                        ddlWhomToVisit.SelectedValue = "0";
                    }
                   
                    objvisitorRegisterBO.DeptName = txtDepartment.Text.Trim();
                    objvisitorRegisterBO.EmpName = txtWhomToVisit.Text.Trim();
                    objvisitorRegisterBO.VisitorIDCardNo = txtIdCardNo.Text.Trim();
                    objvisitorRegisterBO.AccessLocation = txtAccessLocation.Text.Trim();
                    objvisitorRegisterBO.AccompanyPerson = txtAccompanyPerson.Text.Trim();
                    objvisitorRegisterBO.RoomNo = txtRoomNo.Text.Trim();
                    objvisitorRegisterBO.NoofVisitors = Convert.ToInt32(txtNoofVisitors.Text.Trim());
                    objvisitorRegisterBO.VechileNo = txtVechileNo.Text.Trim();
                    objvisitorRegisterBO.Belongings = txtBelongings.Text.Trim();
                    objvisitorRegisterBO.PlannedOutTime = txtxPlanTimeOut.Text.Trim();
                    objvisitorRegisterBO.LastChangedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                    objvisitorRegisterBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                    objvisitorRegisterBO.LastChangedComments = txtComments.Text;
                    int c = objVisitorRegisterBAL.Update_Visitor(objvisitorRegisterBO);
                    if (c > 0)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Visitor details updated successfully.", "success", "RedirecttoVisitorListPage()");
                    }
                    else
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Visitor details updation failed.", "error");
                    }
                }
                catch (SqlException sqe)
                {
                    string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                    HelpClass.custAlertMsg(this, this.GetType(), "VMSVR10:" + strmsgF, "error");
                }
                catch (Exception ex)
                {
                    string strline = HelpClass.LineNo(ex);
                    string strMsg = HelpClass.SQLEscapeString(ex.Message);
                    Aizant_log.Error("VMSVR10:" + strline + " " + strMsg);
                    HelpClass.custAlertMsg(this, this.GetType(), "VMSVR10:" + strline + " " + strMsg, "error");
                }
            }
            
        }
        public void PrintVisitorPass(int Vid, ref string filename2)
        {
            try
            {
                VisitorRegisterBO objvisitorRegisterBO = new VisitorRegisterBO();
                objvisitorRegisterBO.DeptName = string.Empty;
                objvisitorRegisterBO.Purpose_of_VisitID = 0;
                objvisitorRegisterBO.FirstName = string.Empty;
                objvisitorRegisterBO.VisitorID = string.Empty;
                objvisitorRegisterBO.MobileNo = string.Empty;
                objvisitorRegisterBO.Organization = string.Empty;
                objvisitorRegisterBO.IDCardNo = string.Empty;
                objvisitorRegisterBO.InDateTime = string.Empty;
                objvisitorRegisterBO.OutDateTime = string.Empty;
                DataTable dt = new DataTable();
                objvisitorRegisterBO.SNo = Vid;
                objVisitorRegisterBAL = new VisitorRegisterBAL();
                dt = objVisitorRegisterBAL.SearchFilterVisitor(objvisitorRegisterBO);
                if (dt.Rows.Count > 0)
                {
                    VisitorPassReport1 visitorRpt = new VisitorPassReport1();

                    ((XRTableCell)(visitorRpt.FindControl("tblDate", false))).Text = DateTime.Now.ToString("dd MMM yyyy hh:mm:ss");
                    ((XRTableCell)(visitorRpt.FindControl("tblName", false))).Text = dt.Rows[0]["VisitorName"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblAddress", false))).Text = dt.Rows[0]["Address"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblMobile", false))).Text = dt.Rows[0]["MobileNo"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblPurpose", false))).Text = dt.Rows[0]["PurposeofVisit"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblNoofVisitors", false))).Text = dt.Rows[0]["NoofVisitors"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblVisitorID", false))).Text = dt.Rows[0]["VisitorID"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblWhomtoMeet", false))).Text = dt.Rows[0]["WhomToVisit"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblVehicleNo", false))).Text = dt.Rows[0]["VechileNo"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblBelongings", false))).Text = dt.Rows[0]["Belongings"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblIntime", false))).Text = dt.Rows[0]["InDateTime"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("xrcellRemarks", false))).Text = dt.Rows[0]["Remarks"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblSecurityName", false))).Text = ""; (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblVisitorCard", false))).Text = dt.Rows[0]["VisitorIDCardNo"].ToString();

                    if (!string.IsNullOrEmpty(dt.Rows[0]["Photo"].ToString()))
                    {
                        ((XRPictureBox)(visitorRpt.FindControl("xrVisitorPic", false))).Image = BindApplicantImage((byte[])dt.Rows[0]["PhotoFileData"]);
                    }
                    else
                    {
                        // conver  file image to bytearray.
                        byte[] defaultimg = File.ReadAllBytes(Server.MapPath(@"~/Images/UserLogin/DefaultImage.jpg"));
                        ((XRPictureBox)(visitorRpt.FindControl("xrVisitorPic", false))).Image = BindApplicantImage(defaultimg);
                    }

                    visitorRpt.CreateDocument();

                    // Create the 2nd report and generate its document. 
                    PassConditionsReport visitorpassConditionRpt = new PassConditionsReport();
                    visitorpassConditionRpt.CreateDocument();

                    // Add all pages of the 2nd report to the end of the 1st report. 
                    visitorRpt.Pages.AddRange(visitorpassConditionRpt.Pages);

                    string filename1 = Server.MapPath(@"~/Vms/PdfFolder/");
                    filename2 = DateTime.Now.ToString("ddMMyyyyhhmmss") + ".pdf";
                    visitorRpt.ExportToPdf(filename1 + filename2);
                    if (ConfigurationSettings.AppSettings["SecurityPrinter"].ToString().Length > 12)
                    {
                        System.Drawing.Printing.PrinterSettings instance = new System.Drawing.Printing.PrinterSettings();
                        string DefaultPrinter = instance.PrinterName = ConfigurationSettings.AppSettings["SecurityPrinter"].ToString();
                        if (instance.CanDuplex)
                            instance.Duplex = System.Drawing.Printing.Duplex.Default;
                        PrintToolBase tool = new PrintToolBase(visitorRpt.PrintingSystem);

                        if (DefaultPrinter == "Microsoft Print to PDF" || DefaultPrinter == "Microsoft XPS Document Writer" || DefaultPrinter.Contains("Send To OneNote"))
                        {
                            // HelpClass.custAlertMsg(this, this.GetType(), "Sorry ! There is no Printer configured to print.", "error");
                        }
                        else
                        {
                            try
                            {
                                tool.Print(DefaultPrinter);
                            }
                            catch { }
                        }
                    }


                   
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSREP1:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSREP1:" + strline + " " + strMsg);
                HelpClass.showMsg(this, this.GetType(), "VMSREP1:" + strline + " " + strMsg);
            }
        }
        private System.Drawing.Image BindApplicantImage(byte[] applicantImage)
        {
            
            using (var ms = new MemoryStream(applicantImage))
            {
                return System.Drawing.Image.FromStream(ms);
            }
        }
        public string ConvertBytesToBase64(byte[] imageBytes)
        {
            return Convert.ToBase64String(imageBytes);
        }
        protected void asd(object sender, EventArgs e)
        {
            Response.Redirect("~/Visitor_Report.aspx");
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/VMS/VisitorRegister/VisitorRegistration.aspx");
            
        }

        private void Visitor_Reset()
        {
           
            txtVisitorName.Text = "";
            txtVisitEmail.Text = "";
            txtLastName.Text = "";
            txtRemarks.Text = "";
            txtIdProofNo.Text = "";
            txtMobNo.Text = "";
            txtOrganizeName.Text = "";
            txtAddress.Text = "";
            txtxPlanTimeOut.Text = "";
            txtNoofVisitors.Text = "";
            txtBelongings.Text = "";
            txtVechileNo.Text = "";
            txtIdCardNo.Text = txtRoomNo.Text = txtAccompanyPerson.Text = txtAccessLocation.Text = "";
            ddlIDProofType.SelectedIndex = 0;
            ddlPurposeofVisit.SelectedIndex = 0;
            ddlDepartment.SelectedIndex = 0;
            txtIDProofType.Text = "";
            txt_purposeother.Text = "";
            ddlCountry.SelectedIndex = 0;
            ddlState.Items.Clear();
            ddlCities.Items.Clear();
            ddlWhomToVisit.Items.Clear();
            if (btnIDProofType.Text == "<<")
            {
                btnIDProofType.Text = "+";
                ddlIDProofType.Visible = true;
                txtIDProofType.Visible = false;
            }
            if (btn_purposeofvisit.Text == "<<")
            {
                btn_purposeofvisit.Text = "+";
                ddlPurposeofVisit.Visible = true;
                txt_purposeother.Visible = false;
            }
            TimeDate();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", " getVisitorDate();", true);
            txtDepartment.Text = txtWhomToVisit.Text = string.Empty;
        }

        protected void addpurposeofvisit(object sender, EventArgs e)
        {

        }

        protected void btnCapture_Click(object sender, EventArgs e)
        {

        }

        protected void btnIDProofType_Click(object sender, EventArgs e)
        {
            if (btnIDProofType.Text == "+")
            {
                btnIDProofType.Text = "<<";
                txtIDProofType.Visible = true;
                ddlIDProofType.Visible = false;
                ddlIDProofType.SelectedIndex = 0;

            }
            else if (btnIDProofType.Text == "<<")
            {
                btnIDProofType.Text = "+";
                txtIDProofType.Visible = false;
                ddlIDProofType.Visible = true;
                txtIDProofType.Text = "";
            }
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            WhomToVisit();
        }

        public void BindCountries()
        {
            try
            {
                ddlCities.Items.Clear();
                ddlState.Items.Clear();
                ddlCountry.Items.Insert(0, new ListItem("-- Select Country --", "0"));
                DataTable dtCountry = objVisitorRegisterBAL.BindCountriesBAL();
                ddlCountry.DataSource = dtCountry;
                ddlCountry.DataTextField = "CountryName";
                ddlCountry.DataValueField = "CountryID";
                ddlCountry.DataBind();
                ddlCountry.Items.Insert(0, new ListItem("-- Select Country --", "0"));
                ddlCountry.SelectedValue = "101";
                ddlCountry_SelectedIndexChanged(null, null);
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR11:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVR11:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR11:" + strline + " " + strMsg, "error");
            }
        }

        public void WhomToVisit()
        {
            try
            {
                int DeptID = Convert.ToInt32(ddlDepartment.SelectedValue);
                DataTable dt = objVisitorRegisterBAL.GetEmpData(DeptID);
                ddlWhomToVisit.DataSource = dt;
                ddlWhomToVisit.DataTextField = "Name";
                ddlWhomToVisit.DataValueField = "EmpID";
                ddlWhomToVisit.DataBind();
                ddlWhomToVisit.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR12:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVR12:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR12:" + strline + " " + strMsg, "error");
            }
        }

        public void BindStates(int SelectedCountryID)
        {
            try
            {
                DataTable dtStates = objVisitorRegisterBAL.BindStatesBAL(SelectedCountryID);
                ddlState.DataSource = dtStates;
                ddlState.DataTextField = "StateName";
                ddlState.DataValueField = "StateID";
                ddlState.DataBind();
                ddlState.Items.Insert(0, new ListItem("-- Select State --", "0"));
                ddlState.Enabled = true;
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR13:" + strmsgF, "error");

            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVR13:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR13:" + strline + " " + strMsg, "error");
            }
        }

        private void StatesBind()
        {
            try
            {
                ddlState.Enabled = false;
                ddlState.Items.Clear();
                ddlState.Items.Insert(0, new ListItem("-- Select State --", "0"));
                int CountryID = Convert.ToInt32(ddlCountry.SelectedValue);
                if (CountryID > 0)
                {
                    dt = objVisitorRegisterBAL.BindStatesBAL(CountryID);
                    ddlState.DataSource = dt;
                    ddlState.DataTextField = "StateName";
                    ddlState.DataValueField = "StateID";
                    ddlState.DataBind();
                    ddlState.Items.Insert(0, new ListItem("-- Select State --", "0"));
                    ddlState.Enabled = true;
                    upStates.Update();
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR14:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVR14:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR14:" + strline + " " + strMsg, "error");
            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCities.Items.Clear();
            ddlState.Items.Clear();
            StatesBind();
            upStates.Update();
            upCities.Update();
        }

        private void CitiesBind(int SelectedStateID)
        {
            try
            {
                ddlCities.Enabled = false;
                ddlCities.Items.Clear();
                ddlCities.Items.Insert(0, new ListItem("-- Select City --", "0"));
                
                DataTable dtCities = objVisitorRegisterBAL.BindCities(SelectedStateID);
                ddlCities.DataSource = dtCities;
                ddlCities.DataTextField = "CityName";
                ddlCities.DataValueField = "CityID";
                ddlCities.DataBind();
                ddlCities.Items.Insert(0, new ListItem("-- Select City --", "0"));
                ddlCities.Enabled = true;
                upCities.Update();
               
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR15:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVR15:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR15:" + strline + " " + strMsg, "error");
            }
        }

        protected void txtIDProofType_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", " getVisitorDate();", true);
                if (txtIDProofType.Visible == true)
                {
                    if (string.IsNullOrEmpty(txtIDProofType.Text.Trim()))
                    {
                        HelpClass.custAlertMsg(btnInsert, btnInsert.GetType(), "Please enter ID proof type.", "error");
                        return;
                    }
                    int Count = objVisitorRegisterBAL.IDCardTypeifExistsorNot(txtIDProofType.Text.Trim());
                    if (Count > 0)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "IDProof type already exists.", "error");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR16:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVR16:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR16:" + strline + " " + strMsg, "error");
            }
        }

        protected void txt_purposeother_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", " getVisitorDate();", true);
                if (txt_purposeother.Visible == true)
                {
                    if (string.IsNullOrEmpty(txt_purposeother.Text.Trim()))
                    {
                        HelpClass.custAlertMsg(btnInsert, btnInsert.GetType(), "Please enter purpose of visit.", "error");
                        return;
                    }
                }
                int Count = objVisitorRegisterBAL.PurposeofVisitifExistsorNot(txt_purposeother.Text);
                if (Count > 0)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Purpose of visit already exists.", "error");

                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR17:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVR17:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR17:" + strline + " " + strMsg, "error");
            }
        }

        protected void btn_purposeofvisit_Click(object sender, EventArgs e)
        {
            if (btn_purposeofvisit.Text == "+")
            {
                btn_purposeofvisit.Text = "<<";
                txt_purposeother.Visible = true;
                ddlPurposeofVisit.Visible = false;
                ddlPurposeofVisit.SelectedIndex = 0;
            }
            else if (btn_purposeofvisit.Text == "<<")
            {
                btn_purposeofvisit.Text = "+";
                txt_purposeother.Visible = false;
                ddlPurposeofVisit.Visible = true;
                txt_purposeother.Text = "";
            }
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            CitiesBind(Convert.ToInt32(ddlState.SelectedValue));
            upCities.Update();
        }

        private void GetVisitorDetails()
        {
            try
            {
                objvisitorRegisterBO = new VisitorRegisterBO();
                
                //Modified by pradeep
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", " getVisitorDate();", true);
                objvisitorRegisterBO.DeptName = txtDepartment.Text.Trim();
                objvisitorRegisterBO.Purpose_of_VisitID = Convert.ToInt32(ddlPurposeofVisit.SelectedValue);
                objvisitorRegisterBO.FirstName = txtVisitorName.Text.Trim();
                objvisitorRegisterBO.VisitorID = string.Empty;
                objvisitorRegisterBO.MobileNo = txtMobNo.Text.Trim();
                objvisitorRegisterBO.Organization = txtOrganizeName.Text.Trim();
                objvisitorRegisterBO.InDateTime = string.Empty;
                objvisitorRegisterBO.OutDateTime = string.Empty;
                objvisitorRegisterBO.IDCardNo = txtIdProofNo.Text.Trim();
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                DataTable dtVisitorReg = objVisitorRegisterBAL.SearchFilterVisitor(objvisitorRegisterBO);
                if (dtVisitorReg.Rows.Count > 0)
                {
                    txtVisitorName.Text = dtVisitorReg.Rows[0]["FirstName"].ToString();
                    txtLastName.Text = dtVisitorReg.Rows[0]["LastName"].ToString();
                    txtMobNo.Text = dtVisitorReg.Rows[0]["MobileNo"].ToString();
                    txtVisitEmail.Text = dtVisitorReg.Rows[0]["Email"].ToString();
                    txtOrganizeName.Text = dtVisitorReg.Rows[0]["Organization"].ToString();
                    txtIdProofNo.Text = dtVisitorReg.Rows[0]["IDCardNo"].ToString();
                    ddlIDProofType.SelectedValue = dtVisitorReg.Rows[0]["IDCardTypeID"].ToString();
                    txtAddress.Text = dtVisitorReg.Rows[0]["Address"].ToString();
                    ddlCountry.SelectedValue = dtVisitorReg.Rows[0]["CountryID"].ToString();
                    BindStates(Convert.ToInt32(ddlCountry.SelectedValue));
                    ddlState.SelectedValue = dtVisitorReg.Rows[0]["StateID"].ToString();
                    CitiesBind(Convert.ToInt32(ddlState.SelectedValue));
                    ddlCities.SelectedValue = dtVisitorReg.Rows[0]["CityID"].ToString();
                    
                }
                else
                {
                    
                    //Modified by pradeep
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", " getVisitorDate();", true);
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR18:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVR18:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR18:" + strline + " " + strMsg, "error");
            }
        }


        protected void txtVisitorName_TextChanged(object sender, EventArgs e)
        {
            GetVisitorDetails();
        }

        protected void txtMobNo_TextChanged(object sender, EventArgs e)
        {
          
            GetVisitorDetails();
        }

        protected void txtIdProofNo_TextChanged(object sender, EventArgs e)
        {
            GetVisitorDetails();
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetVisitorNameList(string prefixText)
        {
            VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
            VisitorRegisterBO objvisitorRegisterBO = new VisitorRegisterBO();
            objvisitorRegisterBO.DeptName = string.Empty;
            objvisitorRegisterBO.Purpose_of_VisitID = 0;
            objvisitorRegisterBO.FirstName = prefixText;
            objvisitorRegisterBO.VisitorID = string.Empty;
            objvisitorRegisterBO.MobileNo = string.Empty;
            objvisitorRegisterBO.Organization = string.Empty;
            objvisitorRegisterBO.InDateTime = string.Empty;
            objvisitorRegisterBO.OutDateTime = string.Empty;
            objvisitorRegisterBO.IDCardNo = string.Empty;
            DataTable dtVName = new DataTable();
            dtVName = objVisitorRegisterBAL.SearchFilterVisitor(objvisitorRegisterBO);
            var distinctNames = (from row in dtVName.AsEnumerable()
                                 select row.Field<string>("VisitorName")).Distinct();
            List<string> VisitorNames = distinctNames.ToList();
            return VisitorNames;
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetVisitorListByMobile(string prefixText)
        {
            VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
            VisitorRegisterBO objvisitorRegisterBO = new VisitorRegisterBO();
            objvisitorRegisterBO.DeptName = string.Empty;
            objvisitorRegisterBO.Purpose_of_VisitID = 0;
            objvisitorRegisterBO.FirstName = string.Empty;
            objvisitorRegisterBO.VisitorID = string.Empty;
            objvisitorRegisterBO.MobileNo = prefixText;
            objvisitorRegisterBO.Organization = string.Empty;
            objvisitorRegisterBO.InDateTime = string.Empty;
            objvisitorRegisterBO.OutDateTime = string.Empty;
            objvisitorRegisterBO.IDCardNo = string.Empty;
            DataTable dtMobile = new DataTable();
            dtMobile = objVisitorRegisterBAL.SearchFilterVisitor(objvisitorRegisterBO);
            var distinctMobileNo = (from row in dtMobile.AsEnumerable()
                                    select row.Field<string>("MobileNo")).Distinct();
            List<string> VisitordistinctMobileNo = distinctMobileNo.ToList();
            return VisitordistinctMobileNo;
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetVisitorListByIDProofNo(string prefixText)
        {
            VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
            VisitorRegisterBO objvisitorRegisterBO = new VisitorRegisterBO();
            objvisitorRegisterBO.DeptName = string.Empty;
            objvisitorRegisterBO.Purpose_of_VisitID = 0;
            objvisitorRegisterBO.FirstName = string.Empty;
            objvisitorRegisterBO.VisitorID = string.Empty;
            objvisitorRegisterBO.MobileNo = string.Empty;
            objvisitorRegisterBO.Organization = string.Empty;
            objvisitorRegisterBO.InDateTime = string.Empty;
            objvisitorRegisterBO.OutDateTime = string.Empty;
            objvisitorRegisterBO.IDCardNo = prefixText;
            DataTable dtIDProof = new DataTable();
            dtIDProof = objVisitorRegisterBAL.SearchFilterVisitor(objvisitorRegisterBO);
            var distinctIDCardNo = (from row in dtIDProof.AsEnumerable()
                                    select row.Field<string>("IDCardNo")).Distinct();
            List<string> VisitordistinctIDCardNo = distinctIDCardNo.ToList();
            return VisitordistinctIDCardNo;
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetVisitorListByOrganization(string prefixText)
        {
            VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
            VisitorRegisterBO objvisitorRegisterBO = new VisitorRegisterBO();
            objvisitorRegisterBO.DeptName = string.Empty;
            objvisitorRegisterBO.Purpose_of_VisitID = 0;
            objvisitorRegisterBO.FirstName = string.Empty;
            objvisitorRegisterBO.VisitorID = string.Empty;
            objvisitorRegisterBO.MobileNo = string.Empty;
            objvisitorRegisterBO.Organization = prefixText;
            objvisitorRegisterBO.InDateTime = string.Empty;
            objvisitorRegisterBO.OutDateTime = string.Empty;
            objvisitorRegisterBO.IDCardNo = string.Empty;
            DataTable dtOrgan = new DataTable();
            dtOrgan = objVisitorRegisterBAL.SearchFilterVisitor(objvisitorRegisterBO);
            var distinctOrganization = (from row in dtOrgan.AsEnumerable()
                                        select row.Field<string>("Organization")).Distinct();
            List<string> VisitordistinctOrganization = distinctOrganization.ToList();
            return VisitordistinctOrganization;
        }

        public void GetVisitorData(int Pkid)
        {
            try
            {
                JQDatatableBO objJQDataTableBO = new JQDatatableBO();
                objJQDataTableBO.iMode = 0;
                objJQDataTableBO.pkid = Pkid;
                objJQDataTableBO.iDisplayLength = 0;
                objJQDataTableBO.iDisplayStart = 0;
                objJQDataTableBO.iSortCol = 0;
                objJQDataTableBO.sSortDir = "";
                objJQDataTableBO.sSearch = "";
                objVisitorRegisterBAL = new VisitorRegisterBAL();
                DataTable dt = objVisitorRegisterBAL.GetVisitorDetails_List(objJQDataTableBO);
                if (dt.Rows.Count > 0)
                {
                   
                    txtVisitorID.Text = dt.Rows[0]["VisitorID"].ToString();
                    txtVisitorName.Text = dt.Rows[0]["FirstName"].ToString();
                    txtLastName.Text = dt.Rows[0]["LastName"].ToString();
                    txtMobNo.Text = dt.Rows[0]["MobileNo"].ToString();
                    txtVisitEmail.Text = dt.Rows[0]["Email"].ToString();
                    ddlPurposeofVisit.SelectedValue = dt.Rows[0]["Purpose_of_VisitID"].ToString();
                    txtVechileNo.Text = dt.Rows[0]["VechileNo"].ToString();
                    txtNoofVisitors.Text = dt.Rows[0]["NoofVisitors"].ToString();
                    txtOrganizeName.Text = dt.Rows[0]["Organization"].ToString();
                    txtxPlanTimeOut.Text = dt.Rows[0]["PlannedOutTime"].ToString();
                    txtAccessLocation.Text = dt.Rows[0]["AccessLocation"].ToString();
                    txtAccompanyPerson.Text = dt.Rows[0]["AccompanyPerson"].ToString();
                    txtRoomNo.Text = dt.Rows[0]["RoomNo"].ToString();
                    txtRemarks.Text = dt.Rows[0]["Remarks"].ToString();
                    
                    txtIntime.Text = dt.Rows[0]["InDateTime"].ToString();
                    txtBelongings.Text = dt.Rows[0]["Belongings"].ToString();
                    txtIdCardNo.Text = dt.Rows[0]["VisitorIDCardNo"].ToString();
                    txtIdProofNo.Text = dt.Rows[0]["IDCardNo"].ToString();
                    ddlIDProofType.SelectedValue = dt.Rows[0]["IDCardTypeID"].ToString();
                    txtAddress.Text = dt.Rows[0]["Address"].ToString();
                    ddlCountry.SelectedValue = dt.Rows[0]["CountryID"].ToString();
                    BindStates(Convert.ToInt32(ddlCountry.SelectedValue));
                    ddlState.SelectedValue = dt.Rows[0]["StateID"].ToString();
                    CitiesBind(Convert.ToInt32(ddlState.SelectedValue));
                    ddlCities.SelectedValue = dt.Rows[0]["CityID"].ToString();
                    ddlDepartment.SelectedValue = dt.Rows[0]["DeptID"].ToString();
                    WhomToVisit();
                    ddlWhomToVisit.SelectedValue = dt.Rows[0]["EmpID"].ToString();
                    txtDepartment.Text = dt.Rows[0]["DeptName"].ToString();
                    txtWhomToVisit.Text = dt.Rows[0]["EmpName"].ToString();
                    if (!string.IsNullOrEmpty(dt.Rows[0]["Photo"].ToString()))
                    {
                        BindApplicantImage((byte[])dt.Rows[0]["PhotoFileData"]);
                    }
                    else
                    {
                        imgCapture.ImageUrl = "~/Images/UserLogin/DefaultImage.jpg";
                    }
                    btnInsert.Visible = false;
                    btnReset.Visible = false;
                   
                    btnCancel.Visible = true;
                    divcomments.Visible = true;
                    btnCapture.Visible = false;
                    btnIDProofType.Visible = false;
                    btn_purposeofvisit.Visible = false;
                    divimage.Visible = true;

                    if (ViewState["vwVisitorModify"].ToString() == "0")
                    {
                        VisitorRegisterDisableControls();
                        btnUpdate.Visible = false;
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR19:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVR19:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVR19:" + strline + " " + strMsg, "error");
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (ViewState["RoleID"].ToString() == "22")
            {
                if (txtVisitorName.Text.Trim() == "" || txtLastName.Text.Trim() == "" || txtMobNo.Text.Trim() == "" || txtMobNo.Text.Length != 10 || txtNoofVisitors.Text.Trim() == "" || txtAddress.Text.Trim() == "" || txtDepartment.Text.Trim() == "" || txtWhomToVisit.Text.Trim() == "")
                {
                    ArrayList Mandatory = new ArrayList();

                    if (string.IsNullOrEmpty(txtVisitorName.Text.Trim()))
                    {
                        Mandatory.Add("Please enter first name.");
                    }
                    if (String.IsNullOrEmpty(txtLastName.Text.Trim()))
                    {
                        Mandatory.Add("Please enter last name.");
                    }
                    if (string.IsNullOrEmpty(txtMobNo.Text.Trim()))
                    {
                        Mandatory.Add("Please enter mobile no.");
                    }
                    else if (txtMobNo.Text.Length != 10)
                    {
                        Mandatory.Add("Mobile no should be 10 digits.");
                    }
                    if (String.IsNullOrEmpty(txtAddress.Text.Trim()))
                    {
                        Mandatory.Add("Please enter address.");
                    }
                    if (txt_purposeother.Visible == true)
                    {
                        if (string.IsNullOrEmpty(txt_purposeother.Text.Trim()))
                        {
                            Mandatory.Add("Please enter purpose of visit.");
                        }
                    }
                    if (ddlPurposeofVisit.Visible == true)
                    {
                        if (ddlPurposeofVisit.SelectedIndex == 0)
                        {
                            Mandatory.Add("Please select purpose of visit.");
                        }
                    }
                    if (txtDepartment.Visible == true)
                    {
                        if (String.IsNullOrEmpty(txtDepartment.Text.Trim()))
                        {
                            Mandatory.Add("Please enter department.");
                        }
                    }
                    if (txtWhomToVisit.Visible == true)
                    {
                        if (String.IsNullOrEmpty(txtWhomToVisit.Text.Trim()))
                        {
                            Mandatory.Add("Please enter whom to visit.");
                        }
                    }
                    if (ddlDepartment.Visible == true)
                    {
                        if (ddlDepartment.SelectedIndex == 0)
                        {
                            Mandatory.Add("Please select department.");
                        }
                    }
                    if (ddlWhomToVisit.Visible == true)
                    {
                        if (ddlWhomToVisit.SelectedIndex == 0 || ddlWhomToVisit.SelectedValue == "")
                        {
                            Mandatory.Add("Please select whom to visit");
                        }
                    }
                    if (string.IsNullOrEmpty(txtNoofVisitors.Text.Trim()))
                    {
                        Mandatory.Add("Please enter no of visitors.");
                    }
                    if (string.IsNullOrEmpty(txtComments.Text.Trim()))
                    {
                        Mandatory.Add("Please enter modification reason in comments.");
                    }
                    StringBuilder s = new StringBuilder();
                    foreach (string x in Mandatory)
                    {
                        s.Append(x);
                        s.Append("<br/>");
                    }
                    HelpClass.custAlertMsg(this, this.GetType(), "" + s.ToString() + "", "error");
                    return;
                }
            }
            else
            {
                if (txtVisitorName.Text.Trim() == "" || txtLastName.Text.Trim() == "" || txtMobNo.Text.Trim() == "" || txtMobNo.Text.Length != 10 || txtNoofVisitors.Text.Trim() == "" || txtAddress.Text.Trim() == "" || ddlDepartment.SelectedValue == "0" || ddlWhomToVisit.SelectedValue == "0")
                {
                    ArrayList Mandatory = new ArrayList();

                    if (string.IsNullOrEmpty(txtVisitorName.Text.Trim()))
                    {
                        Mandatory.Add("Please enter first name.");
                    }
                    if (String.IsNullOrEmpty(txtLastName.Text.Trim()))
                    {
                        Mandatory.Add("Please enter last name.");
                    }
                    if (string.IsNullOrEmpty(txtMobNo.Text.Trim()))
                    {
                        Mandatory.Add("Please enter mobile no.");
                    }
                    else if (txtMobNo.Text.Length != 10)
                    {
                        Mandatory.Add("Mobile no should be 10 digits.");
                    }
                    if (String.IsNullOrEmpty(txtAddress.Text.Trim()))
                    {
                        Mandatory.Add("Please enter address.");
                    }
                    if (txt_purposeother.Visible == true)
                    {
                        if (string.IsNullOrEmpty(txt_purposeother.Text.Trim()))
                        {
                            Mandatory.Add("Please enter purpose of visit.");
                        }
                    }
                    if (ddlPurposeofVisit.Visible == true)
                    {
                        if (ddlPurposeofVisit.SelectedIndex == 0)
                        {
                            Mandatory.Add("Please select purpose of visit");
                        }
                    }
                    if (txtDepartment.Visible == true)
                    {
                        if (String.IsNullOrEmpty(txtDepartment.Text.Trim()))
                        {
                            Mandatory.Add("Please enter department.");
                        }
                    }
                    if (txtWhomToVisit.Visible == true)
                    {
                        if (String.IsNullOrEmpty(txtWhomToVisit.Text.Trim()))
                        {
                            Mandatory.Add("Please enter whom to visit.");
                        }
                    }
                    if (ddlDepartment.Visible == true)
                    {
                        if (ddlDepartment.SelectedIndex == 0)
                        {
                            Mandatory.Add("Please select department.");
                        }
                    }
                    if (ddlWhomToVisit.Visible == true)
                    {
                        if (ddlWhomToVisit.SelectedIndex == 0 || ddlWhomToVisit.SelectedValue == "")
                        {
                            Mandatory.Add("Please select whom to visit.");
                        }
                    }
                    if (string.IsNullOrEmpty(txtNoofVisitors.Text.Trim()))
                    {
                        Mandatory.Add("Please enter no of visitors.");
                    }
                    StringBuilder s = new StringBuilder();
                    foreach (string x in Mandatory)
                    {
                        s.Append(x);
                        s.Append("<br/>");
                    }
                    HelpClass.custAlertMsg(this, this.GetType(), "" + s.ToString() + "", "error");
                    return;
                }
            }
            hdnVisitorRegisterCustom.Value = "Update";
            HelpClass.custAlertMsg(this, this.GetType(), "Do you want to update.", "confirm");
        }




        public void PrintVisitorPass(int Vid)
        {
            try
            {
                VisitorRegisterBO objvisitorRegisterBO = new VisitorRegisterBO();
                objvisitorRegisterBO.DeptName = string.Empty;
                objvisitorRegisterBO.Purpose_of_VisitID = 0;
                objvisitorRegisterBO.FirstName = string.Empty;
                objvisitorRegisterBO.VisitorID = string.Empty;
                objvisitorRegisterBO.MobileNo = string.Empty;
                objvisitorRegisterBO.Organization = string.Empty;
                objvisitorRegisterBO.IDCardNo = string.Empty;
                objvisitorRegisterBO.InDateTime = string.Empty;
                objvisitorRegisterBO.OutDateTime = string.Empty;
                DataTable dt = new DataTable();
                objvisitorRegisterBO.SNo = Vid;
                dt = objVisitorRegisterBAL.SearchFilterVisitor(objvisitorRegisterBO);
                if (dt.Rows.Count > 0)
                {
                    VisitorPassReport visitorRpt = new VisitorPassReport();

                    ((XRTableCell)(visitorRpt.FindControl("tblDate", false))).Text = DateTime.Now.ToString("dd MMM yyyy hh:mm:ss");
                    ((XRTableCell)(visitorRpt.FindControl("tblName", false))).Text = dt.Rows[0]["VisitorName"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblAddress", false))).Text = dt.Rows[0]["Address"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblMobile", false))).Text = dt.Rows[0]["MobileNo"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblPurpose", false))).Text = dt.Rows[0]["PurposeofVisit"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblNoofVisitors", false))).Text = dt.Rows[0]["NoofVisitors"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblVisitorCard", false))).Text = dt.Rows[0]["VisitorID"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblWhomtoMeet", false))).Text = dt.Rows[0]["EmpName"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblVehicleNo", false))).Text = dt.Rows[0]["VechileNo"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblBelongings", false))).Text = dt.Rows[0]["Belongings"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblIntime", false))).Text = dt.Rows[0]["InDateTime"].ToString();
                    ((XRTableCell)(visitorRpt.FindControl("tblOuttime", false))).Text = dt.Rows[0]["OutDateTime"].ToString();
                    

                    visitorRpt.CreateDocument();

                    // Create the 2nd report and generate its document. 
                    PassConditionsReport visitorpassConditionRpt = new PassConditionsReport();
                    visitorpassConditionRpt.CreateDocument();

                    // Add all pages of the 2nd report to the end of the 1st report. 
                    visitorRpt.Pages.AddRange(visitorpassConditionRpt.Pages);
                    using (MemoryStream ms = new MemoryStream())

                    {

                        PdfExportOptions opts = new PdfExportOptions();

                        opts.ShowPrintDialogOnOpen = true;

                        visitorRpt.ExportToPdf(ms, opts);

                        ms.Seek(0, SeekOrigin.Begin);

                        byte[] report = ms.ToArray();

                        Page.Response.ContentType = "application/pdf";

                        Page.Response.Clear();

                        Page.Response.OutputStream.Write(report, 0, report.Length);

                        Page.Response.End();

                    }

                   


                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSVR20:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVR20:" + strline + " " + strMsg);
                HelpClass.showMsg(this, this.GetType(), "VMSVR20:" + strline + " " + strMsg);
            }
        }

        private void VisitorRegisterDisableControls()
        {
            txtVisitorID.Enabled = false;
            txtVisitorName.Enabled = false;
            txtLastName.Enabled = false;
            txtMobNo.Enabled = false;
            txtVisitEmail.Enabled = false;
            txtAddress.Enabled = false;
            ddlCountry.Enabled = false;
            ddlState.Enabled = false;
            ddlCities.Enabled = false;
            txtIDProofType.Enabled = false;
            ddlIDProofType.Enabled = false;
            txtIdProofNo.Enabled = false;
            txt_purposeother.Enabled = false;
            ddlPurposeofVisit.Enabled = false;
            ddlDepartment.Enabled = false;
            txtDepartment.Enabled = false;
            ddlWhomToVisit.Enabled = false;
            txtWhomToVisit.Enabled = false;
            txtAccompanyPerson.Enabled = false;
            txtOrganizeName.Enabled = false;
            txtAccessLocation.Enabled = false;
            txtRoomNo.Enabled = false;
            txtNoofVisitors.Enabled = false;
            txtVechileNo.Enabled = false;
            txtIntime.Enabled = false;
            txtxPlanTimeOut.Enabled = false;
            txtBelongings.Enabled = false;
            txtIdCardNo.Enabled = false;
            txtRemarks.Enabled = false;
            txtComments.Enabled = false;
        }

        private void VisitorRegisterEnableControls()
        {
            txtVisitorName.Enabled = true;
            txtLastName.Enabled = true;
            txtMobNo.Enabled = true;
            txtVisitEmail.Enabled = true;
            txtAddress.Enabled = true;
            ddlCountry.Enabled = true;
            ddlState.Enabled = true;
            ddlCities.Enabled = true;
            txtIDProofType.Enabled = true;
            ddlIDProofType.Enabled = true;
            txtIdProofNo.Enabled = true;
            txt_purposeother.Enabled = true;
            ddlPurposeofVisit.Enabled = true;
            ddlDepartment.Enabled = true;
            txtDepartment.Enabled = true;
            ddlWhomToVisit.Enabled = true;
            txtWhomToVisit.Enabled = true;
            txtAccompanyPerson.Enabled = true;
            txtOrganizeName.Enabled = true;
            txtAccessLocation.Enabled = true;
            txtRoomNo.Enabled = true;
            txtNoofVisitors.Enabled = true;
            txtVechileNo.Enabled = true;
            txtxPlanTimeOut.Enabled = true;
            txtBelongings.Enabled = true;
            txtIdCardNo.Enabled = true;
            txtRemarks.Enabled = true;
            txtComments.Enabled = true;
        }

        protected void btnCapture_Click1(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_lockCam').modal({ show: 'true', backdrop: 'static', keyboard: false });", true);
            dvCam.InnerHtml = "&nbsp";
            dvCam.InnerHtml = "<iframe  src=\"" + ResolveUrl("~/VMS/visitorregister/Webcam1.aspx") + "#toolbar=0&navpanes=0&scrollbar=0\" width=\"80%\" height=\"450px\"></iframe>";
            upCamHeader.Update();
            upCamBody.Update();
        }

        protected void btnCamClose_Click(object sender, EventArgs e)
        {
            if (Session["CapturedImage"] != null)
                imgCapture.ImageUrl = Session["CapturedImage"].ToString();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_lockCam').model('hide');", true);

        }

        void screenAuthentication()
        {
            DataTable dtAthuntication = GmsHelpClass.GetRolePrivileges();
            if (dtAthuntication.Rows.Count > 0)
            {
                int VisitorCheckIN = dtAthuntication.AsEnumerable()
               .Count(row => row.Field<int>("PrivilegeID") == 1);
                int VisitorModify = dtAthuntication.AsEnumerable()
                .Count(row => row.Field<int>("PrivilegeID") == 4);
                ViewState["vwVisitorModify"] = VisitorModify.ToString();
                int Printcount = dtAthuntication.AsEnumerable()
               .Count(row => row.Field<int>("PrivilegeID") == 2);
                ViewState["vwPrintcount"] = Printcount.ToString();
                int VisitorView = dtAthuntication.AsEnumerable()
               .Count(row => row.Field<int>("PrivilegeID") == 23);
                ViewState["vwVisitorView"] = VisitorView.ToString();
                if (VisitorCheckIN == 0 && VisitorModify == 0 && VisitorView == 0)
                {
                    Response.Redirect("~/UserLogin.aspx");
                }

                if (VisitorView > 0)
                {
                    btnUpdate.Visible = false;
                }
                if (VisitorModify > 0)
                {
                    btnUpdate.Visible = true;
                }
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}


