﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using AizantIT_PharmaApp.Common;
using System.Data;
using System.Data.SqlClient;
using VMS_BAL;
using VMS_BO;

namespace AizantIT_PharmaApp.VMS.VisitorRegister
{
    public partial class AppointmentList : System.Web.UI.Page
    {
        VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
        DataTable dt;
        VisitorAppointmentBookingsBO objVisitorAppointmentBookingBO;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            ViewState["EmpID"] = "0";
                            if (HelpClass.IsUserAuthenticated())
                            {
                                ViewState["EmpID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            }
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSVAL1:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSVAL1:" + strline + " " + strMsg);
            }
        }

        private void InitializeThePage()
        {
            try
            {
                screenAuthentication();
                LoadDepartment();
                LoadPurposeofVisit();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSVAL2:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSVAL2:" + strline + " " + strMsg);
            }
        }

        private void LoadDepartment()
        {
            try
            {
                dt = new DataTable();
                dt = objVisitorRegisterBAL.LoadDepartment();
                ddlDeptToVisit.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlDeptToVisit.DataValueField = dt.Columns["DeptID"].ToString();
                ddlDeptToVisit.DataSource = dt;
                ddlDeptToVisit.DataBind();
                ddlDeptToVisit.Items.Insert(0, new ListItem("--Select--", "0"));

                ddlDepatmentSearchVisitors.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlDepatmentSearchVisitors.DataValueField = dt.Columns["DeptID"].ToString();
                ddlDepatmentSearchVisitors.DataSource = dt;
                ddlDepatmentSearchVisitors.DataBind();
                ddlDepatmentSearchVisitors.Items.Insert(0, new ListItem("All", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSVAL3:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSVAL3:" + strline + " " + strMsg);
            }
        }

        private void LoadPurposeofVisit()
        {
            try
            {
                dt = new DataTable();
                dt = objVisitorRegisterBAL.LoadPurposeofVisit();
                ddlVisitorType.DataTextField = dt.Columns["Purpose_of_Visit"].ToString();
                ddlVisitorType.DataValueField = dt.Columns["Purpose_of_VisitID"].ToString();
                ddlVisitorType.DataSource = dt;
                ddlVisitorType.DataBind();
                ddlVisitorType.Items.Insert(0, new ListItem("--Select--", "0"));

                ddlSearchVisitorsPurposeofVisit.DataTextField = dt.Columns["Purpose_of_Visit"].ToString();
                ddlSearchVisitorsPurposeofVisit.DataValueField = dt.Columns["Purpose_of_VisitID"].ToString();
                ddlSearchVisitorsPurposeofVisit.DataSource = dt;
                ddlSearchVisitorsPurposeofVisit.DataBind();
                ddlSearchVisitorsPurposeofVisit.Items.Insert(0, new ListItem("All", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSVAL4:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSVAL4:" + strline + " " + strMsg);
            }
        }

        public void WhomToVisit()
        {
            try
            {
                int DeptID = Convert.ToInt32(ddlDeptToVisit.SelectedValue);
                DataTable dt = objVisitorRegisterBAL.GetEmpData(DeptID);
                ddlWhomToVisit.DataSource = dt;
                ddlWhomToVisit.DataTextField = "Name";
                ddlWhomToVisit.DataValueField = "EmpID";
                ddlWhomToVisit.DataBind();
                ddlWhomToVisit.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSVAL5:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSVAL5:" + strline + " " + strMsg);
            }
        }


        protected void btnFind_Click(object sender, EventArgs e)
        {
            try
            {
                VisitorAppointmentBookingsBO objVisitorAppointmentBookingBO = new VisitorAppointmentBookingsBO();
                if (ddlDepatmentSearchVisitors.SelectedValue != "")
                {
                    objVisitorAppointmentBookingBO.Appointment_DEPARTMENT_TOVISIT = Convert.ToInt32(ddlDepatmentSearchVisitors.SelectedValue.Trim());
                }
                else
                {
                    objVisitorAppointmentBookingBO.Appointment_DEPARTMENT_TOVISIT = 0;
                }
                objVisitorAppointmentBookingBO.Appointment_NAME = txtSearchVisitorsName.Text;
                objVisitorAppointmentBookingBO.Appointment_ID = txtSearchVisitorsID.Text;
                objVisitorAppointmentBookingBO.Appointment_MOBILE_NO = txtSearchVisitorsMobileNo.Text;
                objVisitorAppointmentBookingBO.Appointment_ORGANISATION = txtSearchVisitorsOrganizationName.Text;
                objVisitorAppointmentBookingBO.Appointment_PURPOSEOF_VISIT = Convert.ToInt32(ddlSearchVisitorsPurposeofVisit.SelectedValue);
                objVisitorAppointmentBookingBO.Appointment_SNO = 0;
                if (!string.IsNullOrEmpty(txtSearchVisitorsFromDate.Text.Trim()))
                {
                    objVisitorAppointmentBookingBO.Appointment_DATE = Convert.ToDateTime(txtSearchVisitorsFromDate.Text.Trim()).ToString("yyyyMMdd");
                }
                else
                    objVisitorAppointmentBookingBO.Appointment_DATE = txtSearchVisitorsFromDate.Text.Trim();
                if (!string.IsNullOrEmpty(txtSearchVisitorsToDate.Text.Trim()))
                {
                    objVisitorAppointmentBookingBO.Appointment_DATE = Convert.ToDateTime(txtSearchVisitorsToDate.Text.Trim()).ToString("yyyyMMdd");//Ask sir
                }
                else
                    objVisitorAppointmentBookingBO.Appointment_DATE = txtSearchVisitorsToDate.Text.Trim();
                dt = new DataTable();
                dt = objVisitorRegisterBAL.SearchFilterAppointment(objVisitorAppointmentBookingBO);
                AppointmentGridView.DataSource = dt;
                AppointmentGridView.DataBind();
                ViewState["dtAppointment"] = dt;
                AppointmentGridView.Visible = true;
               
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSVAL6:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSVAL6:" + strline + " " + strMsg);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            ddlDepatmentSearchVisitors.SelectedIndex = ddlSearchVisitorsPurposeofVisit.SelectedIndex = 0;
            txtSearchVisitorsName.Text = txtSearchVisitorsID.Text = txtSearchVisitorsMobileNo.Text = txtSearchVisitorsOrganizationName.Text = txtSearchVisitorsFromDate.Text = txtSearchVisitorsToDate.Text = string.Empty;
            AppointmentGridView.Visible = false;
        }

        protected void AppointmentGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            AppointmentGridView.PageIndex = e.NewPageIndex;
            if (ViewState["dtAppointment"] != null)
            {
                AppointmentGridView.DataSource = (DataTable)ViewState["dtAppointment"];
                AppointmentGridView.DataBind();
            }
        }

        protected void AppointmentGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblOuttime = (Label)(e.Row.FindControl("lblGVVisitorOutDateTime"));
                LinkButton lbtnCheckin = (LinkButton)(e.Row.FindControl("lnkCheckOutVisitor"));

                if (lblOuttime != null)
                {
                    if (lblOuttime.Text.Length > 6)
                        lblOuttime.Visible = true;
                    else
                        lbtnCheckin.Visible = true;
                }
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblOuttime = (Label)(e.Row.FindControl("lblGVVisitorOutDateTime"));
                LinkButton lnkbtnEdit = (LinkButton)(e.Row.FindControl("LnkEditVisitor"));
                if (lblOuttime != null)
                {
                    if (lblOuttime.Text.Length > 5)
                    {
                        lnkbtnEdit.Visible = false;
                    }
                    else
                    {
                        lnkbtnEdit.Visible = true;
                    }
                }
            }
        }

        protected void LnkEditVisitor_Click(object sender, EventArgs e)
        {
            try
            {
                objVisitorAppointmentBookingBO = new VisitorAppointmentBookingsBO();
                objVisitorAppointmentBookingBO.Appointment_DEPARTMENT_TOVISIT = 0;
                objVisitorAppointmentBookingBO.Appointment_NAME = string.Empty;
                objVisitorAppointmentBookingBO.Appointment_ID = string.Empty;
                objVisitorAppointmentBookingBO.Appointment_MOBILE_NO = string.Empty;
                objVisitorAppointmentBookingBO.Appointment_ORGANISATION = string.Empty;
                objVisitorAppointmentBookingBO.Appointment_PURPOSEOF_VISIT = 0;
                objVisitorAppointmentBookingBO.Appointment_DATE = string.Empty;
                LinkButton lnk = sender as LinkButton;
                GridViewRow gr = (GridViewRow)lnk.NamingContainer;
                string tempID = AppointmentGridView.DataKeys[gr.RowIndex].Value.ToString();
                objVisitorAppointmentBookingBO.Appointment_SNO = Convert.ToInt32(tempID);
                ViewState["tempId1"] = tempID;
                dt = new DataTable();
                dt = objVisitorRegisterBAL.SearchFilterAppointment(objVisitorAppointmentBookingBO);
                if (dt.Rows.Count > 0)
                {
                    txtEditSNo.Text = dt.Rows[0]["Appointment_SNO"].ToString();
                    txtAppointmentId.Text = dt.Rows[0]["Appointment_ID"].ToString();
                    txt_VisitorName.Text = dt.Rows[0]["Appointment_NAME"].ToString();
                    txtLastName.Text = dt.Rows[0]["Appointment_LastName"].ToString();
                    txt_MobNo.Text = dt.Rows[0]["Appointment_MOBILE_NO"].ToString();
                    txt_EmailID.Text = dt.Rows[0]["Appointment_EMAILID"].ToString();
                    ddlVisitorType.SelectedValue = dt.Rows[0]["Purpose_of_VisitID"].ToString();
                    txtdate.Value = dt.Rows[0]["Appointment_DATE"].ToString();
                    txtAppointmentTime.Text = dt.Rows[0]["Appointment_TIME"].ToString();
                    txtOrganisationName.Text = dt.Rows[0]["Organization"].ToString();
                    txt_Remarks.Text = dt.Rows[0]["Appointment_REMARKS"].ToString();
                    txtVEditComments.Text = dt.Rows[0]["LastChangedByComments"].ToString();
                    ddlDeptToVisit.SelectedValue = dt.Rows[0]["DeptID"].ToString();
                    WhomToVisit();
                    ddlWhomToVisit.SelectedValue = dt.Rows[0]["Appointment_WHOMTO_VISIT"].ToString();
                    int AppointmentStatus = Convert.ToInt32(dt.Rows[0]["Appointment_Status"].ToString());
                    if (AppointmentStatus == 1)
                    {
                        btnCancel.Visible = true;
                    }
                    else
                    {
                        btnCancel.Visible = false;
                    }
                    ModalPopupAppointment.Show();

                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSVAL7:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSVAL7:" + strline + " " + strMsg);
            }
        }

        protected void ddlDeptToVisit_SelectedIndexChanged(object sender, EventArgs e)
        {
            WhomToVisit();
        }

        protected void btnVisitorUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                objVisitorAppointmentBookingBO = new VisitorAppointmentBookingsBO();
                objVisitorAppointmentBookingBO.Appointment_SNO = Convert.ToInt32(txtEditSNo.Text);
                objVisitorAppointmentBookingBO.Appointment_ID = txtAppointmentId.Text.Trim();
                objVisitorAppointmentBookingBO.Appointment_NAME = txt_VisitorName.Text.Trim();
                objVisitorAppointmentBookingBO.Appointment_LastName = txtLastName.Text.Trim();
                objVisitorAppointmentBookingBO.Appointment_MOBILE_NO = txt_MobNo.Text.Trim();
                objVisitorAppointmentBookingBO.Appointment_EMAILID = txt_EmailID.Text.Trim();
                objVisitorAppointmentBookingBO.Appointment_ORGANISATION = txtOrganisationName.Text.Trim();
                if (ddlVisitorType.SelectedIndex > 0)
                {
                    objVisitorAppointmentBookingBO.Appointment_PURPOSEOF_VISIT = Convert.ToInt32(ddlVisitorType.SelectedValue);
                }
                else
                {
                    ddlVisitorType.SelectedValue = "0";
                }
                objVisitorAppointmentBookingBO.Appointment_REMARKS = txt_Remarks.Text.Trim();
                if (ddlDeptToVisit.SelectedIndex > 0)
                {
                    objVisitorAppointmentBookingBO.Appointment_DEPARTMENT_TOVISIT = Convert.ToInt32(ddlDeptToVisit.SelectedValue);
                }
                else
                {
                    ddlDeptToVisit.SelectedValue = "0";
                }
                if (ddlWhomToVisit.SelectedIndex > 0)
                {
                    objVisitorAppointmentBookingBO.Appointment_WHOMTO_VISIT = Convert.ToInt32(ddlWhomToVisit.SelectedValue);
                }
                objVisitorAppointmentBookingBO.Appointment_DATE = txtdate.Value.Trim();
                objVisitorAppointmentBookingBO.Appointment_TIME = txtAppointmentTime.Text.Trim();
                objVisitorAppointmentBookingBO.LastChangedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                objVisitorAppointmentBookingBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                objVisitorAppointmentBookingBO.LastChangedComments = txtVEditComments.Text;
                int c = objVisitorRegisterBAL.Update_Appointment(objVisitorAppointmentBookingBO);
                if (c > 0)
                {
                    HelpClass.showMsg(btnVisitorUpdate, btnVisitorUpdate.GetType(), "Visitor Appointment Details Updated Successfully.");
                    btnFind_Click(null, null);
                }
                else
                {
                    HelpClass.showMsg(btnVisitorUpdate, btnVisitorUpdate.GetType(), "Visitor Appointment Details Updation Failed.");
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSVAL8:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSVAL8:" + strline + " " + strMsg);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                objVisitorAppointmentBookingBO = new VisitorAppointmentBookingsBO();
                objVisitorAppointmentBookingBO.Appointment_SNO = Convert.ToInt32(txtEditSNo.Text);
                objVisitorAppointmentBookingBO.Appointment_ID = txtAppointmentId.Text.Trim();
                int c = objVisitorRegisterBAL.AppointmentCancel(objVisitorAppointmentBookingBO);
                if (c > 0)
                {
                    HelpClass.showMsg(btnCancel, btnCancel.GetType(), "Visitor Appointment Details Canceled Successfully.");
                    btnFind_Click(null, null);
                }
                else
                {
                    HelpClass.showMsg(btnCancel, btnCancel.GetType(), "Visitor Appointment Details Cancelation Failed.");
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSVAL9:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSVAL9:" + strline + " " + strMsg);
            }
        }

        void screenAuthentication()
        {
            DataTable dtAthuntication = GmsHelpClass.GetRolePrivileges();
            if (dtAthuntication.Rows.Count > 0)
            {
                //modid=1,PrivilegeID=5,6
                int AppointmentCreate = dtAthuntication.AsEnumerable()
        .Count(row => row.Field<int>("PrivilegeID") == 5);
                int AppointmentListModify = dtAthuntication.AsEnumerable()
           .Count(row => row.Field<int>("PrivilegeID") == 6);
                if ( AppointmentListModify == 0 && AppointmentCreate == 0)
                    Response.Redirect("~/UserLogin.aspx");
                if (AppointmentListModify == 0)
                    btnVisitorUpdate.Visible = false;
            }
        }
    }
}