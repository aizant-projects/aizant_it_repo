﻿using VMS_BAL;
using VMS_BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using AizantIT_PharmaApp.Common;

namespace AizantIT_PharmaApp.VMS.VisitorRegister
{
    public partial class VisitorAppointmentBooking : System.Web.UI.Page
    {
        VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
        DataTable dt;
        VisitorRegisterBO objvisitorRegisterBO;
        VisitorAppointmentBookingsBO objVisitorAppointmentBookingBO;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            ViewState["EmpID"] = "0";
                            if (HelpClass.IsUserAuthenticated())
                            {
                                ViewState["EmpID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            }
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVAB9:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVAB9:" + strline + " " + strMsg,"error");
            }
        }

        private void InitializeThePage()
        {
            try
            {
                screenAuthentication();
                LoadDepartment();
                LoadPurposeofVisit();
                VisitorAutoGenerateID();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVAB8:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVAB8:" + strline + " " + strMsg,"error");
            }
        }
        public void VisitorAutoGenerateID()
        {
            try
            {
                string strAVID = objVisitorRegisterBAL.AppointmentVisitorAutoGenerateID();
                txtAppointmentId.Text = strAVID;
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVAB1:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVAB1:" + strline + " " + strMsg,"error");
            }
        }

        private void LoadDepartment()
        {
            try
            {
                dt = new DataTable();
                dt = objVisitorRegisterBAL.LoadDepartment();
                ddlDeptToVisit.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlDeptToVisit.DataValueField = dt.Columns["DeptID"].ToString();
                ddlDeptToVisit.DataSource = dt;
                ddlDeptToVisit.DataBind();
                ddlDeptToVisit.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVAB2:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVAB2:" + strline + " " + strMsg,"error");
            }
        }

        private void LoadPurposeofVisit()
        {
            try
            {
                dt = new DataTable();
                dt = objVisitorRegisterBAL.LoadPurposeofVisit();
                ddlVisitorType.DataTextField = dt.Columns["Purpose_of_Visit"].ToString();
                ddlVisitorType.DataValueField = dt.Columns["Purpose_of_VisitID"].ToString();
                ddlVisitorType.DataSource = dt;
                ddlVisitorType.DataBind();
                ddlVisitorType.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVAB3:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVAB3:" + strline + " " + strMsg,"error");
            }
        }


        public void WhomToVisit()
        {
            try
            {
                int DeptID = Convert.ToInt32(ddlDeptToVisit.SelectedValue);
                DataTable dt = objVisitorRegisterBAL.GetEmpData(DeptID);
                ddlWhomToVisit.DataSource = dt;
                ddlWhomToVisit.DataTextField = "Name";
                ddlWhomToVisit.DataValueField = "EmpID";
                ddlWhomToVisit.DataBind();
                ddlWhomToVisit.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVAB4:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVAB4:" + strline + " " + strMsg,"error");
            }
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_VisitorName.Text.Trim() == "" || txt_MobNo.Text.Trim() == "" || txtAppointmentDate.Text.Trim() == "")
                {
                    HelpClass.custAlertMsg(btnCreate, btnCreate.GetType(), "All fields represented with * are mandatory.","error");
                    return;
                }
                if (txt_purposeofvisit.Visible == true)
                {
                    if (string.IsNullOrEmpty(txt_purposeofvisit.Text.Trim()))
                    {
                        HelpClass.custAlertMsg(btnCreate, btnCreate.GetType(), "Please Enter Purpose of Visit.","error");
                        return;
                    }
                }
                if (ddlVisitorType.Visible == true)
                {
                    if (ddlVisitorType.SelectedIndex == 0)
                    {
                        HelpClass.custAlertMsg(btnCreate, btnCreate.GetType(), "Please Select Purpose of Visit.","error");
                        return;
                    }
                }
                string PurposeOfVisit;
                if (txt_purposeofvisit.Visible == true)
                {
                    objvisitorRegisterBO = new VisitorRegisterBO();
                    objvisitorRegisterBO.PurposeofVisitTxt = txt_purposeofvisit.Text.Trim();
                    PurposeOfVisit = objVisitorRegisterBAL.PurposeOfVisitUpdate(objvisitorRegisterBO).ToString();
                    if (PurposeOfVisit == "0")
                    {
                        lblError.Text = "Purpose of Visit Already Exists.";
                        return;
                    }
                    else
                    {
                        lblError.Text = "";
                    }
                }
                else
                {
                    PurposeOfVisit = ddlVisitorType.SelectedValue;
                }
                objVisitorAppointmentBookingBO = new VisitorAppointmentBookingsBO();
                objVisitorAppointmentBookingBO.CreatedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                objVisitorAppointmentBookingBO.CreatedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                objVisitorAppointmentBookingBO.Appointment_ID = txtAppointmentId.Text.Trim();
                objVisitorAppointmentBookingBO.Appointment_NAME = txt_VisitorName.Text.Trim();
                objVisitorAppointmentBookingBO.Appointment_LastName = txtLastName.Text.Trim();
                objVisitorAppointmentBookingBO.Appointment_MOBILE_NO = txt_MobNo.Text.Trim();
                objVisitorAppointmentBookingBO.Appointment_EMAILID = txt_EmailID.Text.Trim();
                objVisitorAppointmentBookingBO.Appointment_ORGANISATION = txtOrganisationName.Text.Trim();
                objVisitorAppointmentBookingBO.Appointment_DATE = Convert.ToDateTime(txtAppointmentDate.Text).ToString("yyyyMMdd");
                objVisitorAppointmentBookingBO.Appointment_TIME = txtAppointmentTime.Text;
                objVisitorAppointmentBookingBO.Appointment_PURPOSEOF_VISIT = Convert.ToInt32(PurposeOfVisit);
                objVisitorAppointmentBookingBO.Appointment_REMARKS = txt_Remarks.Text.Trim();
                if (ddlDeptToVisit.SelectedIndex > 0)
                    objVisitorAppointmentBookingBO.Appointment_DEPARTMENT_TOVISIT = Convert.ToInt32(ddlDeptToVisit.SelectedValue.Trim());
                else
                    objVisitorAppointmentBookingBO.Appointment_DEPARTMENT_TOVISIT = 0;
                if (ddlWhomToVisit.SelectedIndex > 0)
                    objVisitorAppointmentBookingBO.Appointment_WHOMTO_VISIT = Convert.ToInt32(ddlWhomToVisit.SelectedValue.Trim());
                else
                    objVisitorAppointmentBookingBO.Appointment_WHOMTO_VISIT = 0;
                int c = objVisitorRegisterBAL.BookAppointment(objVisitorAppointmentBookingBO);
                if (c > 0)
                {
                    
                    VisitorAutoGenerateID();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", "savealert();", true);
                    
                }
                else
                {
                    HelpClass.custAlertMsg(btnCreate, btnCreate.GetType(), "Appointment Submission Failed.","error");
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVAB5:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVAB5:" + strline + " " + strMsg,"error");
            }
        }

        private void VisitorAppoinment_Reset()
        {
            ddlVisitorType.SelectedIndex = 0;
            ddlDeptToVisit.SelectedIndex = 0;
            txtAppointmentTime.Text = "";
            txtAppointmentDate.Text = "";
            txtOrganisationName.Text = "";
            txt_EmailID.Text = "";
            txt_MobNo.Text = "";
            txt_Remarks.Text = "";
            txt_VisitorName.Text = "";
            txtLastName.Text = "";
            ddlWhomToVisit.Items.Clear();
            txt_purposeofvisit.Text = "";
            lblError.Text = "";
            if (btn_purposeofvisit1.Text == "<<")
            {
                btn_purposeofvisit1.Text = "+";
                ddlVisitorType.Visible = true;
                txt_purposeofvisit.Visible = false;
            }
        }

        protected void addpurposeofvisit1(object sender, EventArgs e)
        {
            if (btn_purposeofvisit1.Text == "+")
            {
                VisitorAppoinment_Reset();
                btn_purposeofvisit1.Text = "<<";
                txt_purposeofvisit.Visible = true;
                ddlVisitorType.Visible = false;

            }
            else if (btn_purposeofvisit1.Text == "<<")
            {
                VisitorAppoinment_Reset();
                btn_purposeofvisit1.Text = "+";
                txt_purposeofvisit.Visible = false;
                ddlVisitorType.Visible = true;
            }
        }
        protected void ddlDeptToVisit_SelectedIndexChanged(object sender, EventArgs e)
        {
            WhomToVisit();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/VMS/VisitorRegister/VisitorAppointmentBooking.aspx");
            VisitorAppoinment_Reset();
        }

        protected void txt_purposeofvisit_TextChanged(object sender, EventArgs e)
        {
            try
            { 
            if (txt_purposeofvisit.Visible == true)
            {
                if (string.IsNullOrEmpty(txt_purposeofvisit.Text.Trim()))
                {
                    HelpClass.custAlertMsg(btnCreate, btnCreate.GetType(), "Please Enter Purpose of Visit.","error");
                    return;
                }
            }
            int Count = objVisitorRegisterBAL.PurposeofVisitifExistsorNot(txt_purposeofvisit.Text.Trim());
            if (Count > 0)
            {
                lblError.Text = "Purpose of Visit Already Exists.";
            }
            else
            {
                lblError.Text = "";
            }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVAB6:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVAB6:" + strline + " " + strMsg,"error");
            }
        }

        public void GetAppointmentDetails()
        {
            try
            {
            VisitorAppointmentBookingsBO objVisitorAppointmentBookingBO = new VisitorAppointmentBookingsBO();
            if (ddlDeptToVisit.SelectedValue != "")
            {
                objVisitorAppointmentBookingBO.Appointment_DEPARTMENT_TOVISIT = Convert.ToInt32(ddlDeptToVisit.SelectedValue.Trim());
            }
            else
            {
                objVisitorAppointmentBookingBO.Appointment_DEPARTMENT_TOVISIT = 0;
            }
            objVisitorAppointmentBookingBO.Appointment_NAME = txt_VisitorName.Text;
            objVisitorAppointmentBookingBO.Appointment_ID = string.Empty;
            objVisitorAppointmentBookingBO.Appointment_MOBILE_NO = txt_MobNo.Text;
            objVisitorAppointmentBookingBO.Appointment_ORGANISATION = txtOrganisationName.Text;
            objVisitorAppointmentBookingBO.Appointment_PURPOSEOF_VISIT = Convert.ToInt32(ddlVisitorType.SelectedValue);
            objVisitorAppointmentBookingBO.Appointment_DATE = string.Empty;
            objVisitorAppointmentBookingBO.Appointment_DATE = string.Empty;
            dt = new DataTable();
            dt = objVisitorRegisterBAL.SearchFilterAppointment(objVisitorAppointmentBookingBO);
            if (dt.Rows.Count > 0)
            {
                
                txt_VisitorName.Text = dt.Rows[0]["Appointment_NAME"].ToString();
                txtLastName.Text = dt.Rows[0]["Appointment_LastName"].ToString();
                txt_MobNo.Text = dt.Rows[0]["Appointment_MOBILE_NO"].ToString();
                txt_EmailID.Text = dt.Rows[0]["Appointment_EMAILID"].ToString();
                
                txtOrganisationName.Text = dt.Rows[0]["Organization"].ToString();
                
            }
            else
            {
              
            }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVAB7:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVAB7:" + strline + " " + strMsg,"error");
            }
        }

        protected void txt_VisitorName_TextChanged(object sender, EventArgs e)
        {
            GetAppointmentDetails();
        }

        protected void txt_MobNo_TextChanged(object sender, EventArgs e)
        {
            GetAppointmentDetails();
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetVisitorAppintmentNameList(string prefixText)
        {
            VisitorAppointmentBookingsBO objVisitorAppointmentBookingsBO = new VisitorAppointmentBookingsBO();
            VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
            objVisitorAppointmentBookingsBO.Appointment_DEPARTMENT_TOVISIT = 0;
            objVisitorAppointmentBookingsBO.Appointment_PURPOSEOF_VISIT = 0;
            objVisitorAppointmentBookingsBO.Appointment_NAME = prefixText;
            objVisitorAppointmentBookingsBO.Appointment_ID = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_MOBILE_NO = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_ORGANISATION = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_DATE     = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_DATE = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_SNO = 0;
            DataTable dt = new DataTable();
            dt = objVisitorRegisterBAL.SearchFilterAppointment(objVisitorAppointmentBookingsBO);
            var distinctNames = (from row in dt.AsEnumerable()
                                 select row.Field<string>("FirstName")).Distinct();
            List<string> VisitorNames = distinctNames.ToList();
            return VisitorNames;
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetVisitorAppintmentListByMobile(string prefixText)
        {
            VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
            VisitorAppointmentBookingsBO objVisitorAppointmentBookingsBO = new VisitorAppointmentBookingsBO();
            objVisitorAppointmentBookingsBO.Appointment_DEPARTMENT_TOVISIT = 0;
            objVisitorAppointmentBookingsBO.Appointment_PURPOSEOF_VISIT = 0;
            objVisitorAppointmentBookingsBO.Appointment_NAME = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_ID = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_MOBILE_NO = prefixText;
            objVisitorAppointmentBookingsBO.Appointment_ORGANISATION = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_DATE = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_DATE = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_SNO = 0;
            DataTable dt = new DataTable();
            dt = objVisitorRegisterBAL.SearchFilterAppointment(objVisitorAppointmentBookingsBO);
            var distinctMobileNo = (from row in dt.AsEnumerable()
                                    select row.Field<string>("Appointment_MOBILE_NO")).Distinct();
            List<string> VisitordistinctMobileNo = distinctMobileNo.ToList();
            return VisitordistinctMobileNo;
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetVisitorListByOrganization(string prefixText)
        {
            VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
            VisitorAppointmentBookingsBO objVisitorAppointmentBookingsBO = new VisitorAppointmentBookingsBO();
            objVisitorAppointmentBookingsBO.Appointment_DEPARTMENT_TOVISIT = 0;
            objVisitorAppointmentBookingsBO.Appointment_PURPOSEOF_VISIT = 0;
            objVisitorAppointmentBookingsBO.Appointment_NAME = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_ID = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_MOBILE_NO = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_ORGANISATION = prefixText;
            objVisitorAppointmentBookingsBO.Appointment_DATE = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_DATE = string.Empty;
            objVisitorAppointmentBookingsBO.Appointment_SNO = 0;
            DataTable dt = new DataTable();
            dt = objVisitorRegisterBAL.SearchFilterAppointment(objVisitorAppointmentBookingsBO);
            var distinctOrganization = (from row in dt.AsEnumerable()
                                        select row.Field<string>("Organization")).Distinct();
            List<string> VisitordistinctOrganization = distinctOrganization.ToList();
            return VisitordistinctOrganization;
        }
        void screenAuthentication()
        {
            DataTable dtAthuntication = GmsHelpClass.GetRolePrivileges();
            if (dtAthuntication.Rows.Count > 0)
            {
                //modid=1,PrivilegeID=5

                int AppointmentCreate = dtAthuntication.AsEnumerable()
           .Count(row => row.Field<int>("PrivilegeID") == 5);
                if (AppointmentCreate == 0)
                    Response.Redirect("~/UserLogin.aspx");
            }
        }
    }
}