﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VMS_BAL;
using VMS_BO;
using AizantIT_PharmaApp.Common;
using System.Data;
using System.Data.SqlClient;
using UMS_BusinessLayer;
using System.Collections;
using System.Text;
using AizantIT_PharmaApp.VMS.Reports;
using DevExpress.XtraReports.UI;


namespace AizantIT_PharmaApp.VMS.MaterialRegister
{
    public partial class MaterialRegister : System.Web.UI.Page
    {
        OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
        MaterialBO objMaterialBO;
        UMS_BAL objUMS_BAL;
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ConfirmCustomAlert.buttonClick += new EventHandler(ConfirmCustomAlert_buttonClick);
                ElectronicSign.buttonClick += new EventHandler(ElectronicSign_buttonClick);
                if (!IsPostBack)
                {
                    ViewState["HODStatusApproveorReject"] = string.Empty;
                    ViewState["StoreStatusApproveorReject"] = string.Empty;
                    ViewState["vwOutwardUpdateReturnable"] = string.Empty;
                    ViewState["vwOutwardView"] = string.Empty;
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                           
                            if (HelpClass.IsUserAuthenticated())
                            {

                                ViewState["MRoleID"] = Session["sesGMSRoleID"];//
                               
                            }
                            ViewState["roleuser"] = string.Empty;
                            ViewState["isEditMode"] = 0;
                            Session["MStatus"] = 0;
                            InitializeThePage();
                            if (Request.QueryString.Count > 0)
                            {
                                if (Request.QueryString["c"] != null)
                                {
                                    ViewState["roleuser"] = HelpClass.Decrypt(HttpUtility.UrlDecode(Request.QueryString["c"]));
                                    hdfUserRole.Value = Request.QueryString["c"];
                                    if (ViewState["roleuser"].ToString() == "muser")
                                    {
                                        Session["MStatus"] = 8; //Store User Revert

                                    }
                                    else if (ViewState["roleuser"].ToString() == "mstore")
                                    {
                                        Session["MStatus"] = 6; //Hod Approved
                                    }
                                    else if (ViewState["roleuser"].ToString() == "mhod")
                                    {
                                        Session["MStatus"] = 4;  //In hod Review
                                    }
                                    ViewState["isEditMode"] = 1;
                                    btnNavCreateOutward.Visible = false;
                                    upNavCreateOutward.Update();
                                }

                                if (Request.QueryString["Notification_ID"] != null)
                                {
                                    if (ViewState["MRoleID"].ToString() == "21")
                                    {
                                        Session["MStatus"] = 8; //Store User Revert
                                        hdfUserRole.Value = HttpUtility.UrlEncode(HelpClass.Encrypt("muser"));
                                    }
                                    else if (ViewState["MRoleID"].ToString() == "19")
                                    {
                                        Session["MStatus"] = 6; //Hod Approved
                                        hdfUserRole.Value = HttpUtility.UrlEncode(HelpClass.Encrypt("mstore"));
                                    }
                                    else if (ViewState["MRoleID"].ToString() == "20")
                                    {
                                        Session["MStatus"] = 4;  //In hod Review
                                        hdfUserRole.Value = HttpUtility.UrlEncode(HelpClass.Encrypt("mhod"));
                                    }
                                    ViewState["isEditMode"] = 1;
                                    btnNavCreateOutward.Visible = false;
                                    upNavCreateOutward.Update();
                                    // update notification status
                                    objUMS_BAL = new UMS_BAL();
                                    int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs);
                                    // Get OutMaterialId
                                    MaterialBO objMaterialBO = new MaterialBO();
                                    objMaterialBO.NotificationID = Convert.ToInt32(Request.QueryString["Notification_ID"]);
                                    DataTable dtOM = objOutwardMaterialBAL.GetOutMaterialIDFromNotification(objMaterialBO);
                                    hdnOutwardPkID.Value = dtOM.Rows[0]["OutMaterialID"].ToString();
                                    Session["sesGMSRoleID"]= dtOM.Rows[0]["NRoleID"].ToString();
                                    //Open materialDetails
                                    GetMaterialDetialsList(Convert.ToInt32(dtOM.Rows[0]["OutMaterialID"].ToString()));
                                }
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                    btnApprove.Visible = false;
                    btnReject.Visible = false;
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSM1:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSM1:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSM1:" + strline + " " + strMsg, "error");
            }
        }

        #region OutwardMaterial
        private void InitializeThePage()
        {
            try
            {
                screenAuthentication();
                LoadDepartment();               
                LoadUnits();
                BindMaterialType();
                InwardLoadUnits();
                InwardLoadDepartment();               
                InwardEmployee();
                BindInwardMaterialType();
                LoadOutwardMaterialNameMaster();
                LoadInwardMaterialNameMaster();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOM1:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSOM1:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOM1:" + strline + " " + strMsg, "error");
            }
        }

       

        private void LoadOutwardMaterialNameMaster()
        {
            try
            {
                objOutwardMaterialBAL = new OutwardMaterialBAL();
                DataTable dtMaterialNames = objOutwardMaterialBAL.LoadOutwardMaterialNameMaster();
                ddlMaterialName.DataTextField = "MaterialName";
                ddlMaterialName.DataValueField = "MaterialMasterID";
                ddlMaterialName.DataSource = dtMaterialNames;
                ddlMaterialName.DataBind();
                ddlMaterialName.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOM2:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSOM2:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOM2:" + strline + " " + strMsg, "error");
            }
        }

        private void LoadDepartment()
        {
            try
            {
                
                OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
               
                int RoleIDs=Convert.ToInt32(Session["sesGMSRoleID"]);
                DataTable dt = objOutwardMaterialBAL.getRoleWiseDepartments(Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString()), RoleIDs);
                ddlOutwardDept.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlOutwardDept.DataValueField = dt.Columns["DeptID"].ToString();
                ddlOutwardDept.DataSource = dt;
                ddlOutwardDept.DataBind();
                ddlOutwardDept.Items.Insert(0, new ListItem("--Select--", "0"));

                ddlReceivedByDept.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlReceivedByDept.DataValueField = dt.Columns["DeptID"].ToString();
                ddlReceivedByDept.DataSource = dt;
                ddlReceivedByDept.DataBind();
                ddlReceivedByDept.Items.Insert(0, new ListItem("-- Select --", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOM3:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSOM3:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOM3:" + strline + " " + strMsg, "error");
            }
        }

        private void LoadUnits()
        {
            try
            {
                DataTable dtUnits = new DataTable();
                dtUnits = objOutwardMaterialBAL.BindUnits();
                ddlUnits.DataTextField = "UnitName";
                ddlUnits.DataValueField = "UnitID";
                ddlUnits.DataSource = dtUnits;
                ddlUnits.DataBind();
                ddlUnits.Items.Insert(0, new ListItem("", "0"));

                ddlUnitsReturn.DataTextField = dtUnits.Columns["UnitName"].ToString();
                ddlUnitsReturn.DataValueField = dtUnits.Columns["UnitID"].ToString();
                ddlUnitsReturn.DataSource = dtUnits;
                ddlUnitsReturn.DataBind();
                ddlUnitsReturn.Items.Insert(0, new ListItem("", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOM4:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSOM4:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOM4:" + strline + " " + strMsg, "error");
            }
        }

        private void BindMaterialType()
        {
            try
            {
                DataTable dtMaterial = new DataTable();
                dtMaterial = objOutwardMaterialBAL.BindMaterialType();
                ddlMaterialType.DataTextField = "OutwardMaterialType";
                ddlMaterialType.DataValueField = "OutwardMaterialTypeID";
                ddlMaterialType.DataSource = dtMaterial;
                ddlMaterialType.DataBind();
                ddlMaterialType.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOM5:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSOM5:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOM5:" + strline + " " + strMsg, "error");
            }
        }

        //MaterialOutWard Submit Code
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if ( txtQuantity.Text.Trim() == "" || txtNameoftheParty.Text.Trim() == "" || ddlUnits.SelectedValue == "0" || ddlOutwardDept.SelectedValue == "0" || ddlGivenBy.SelectedValue == "0" || ddlMaterialType.SelectedValue == "0")
            {
                ArrayList Mandatory = new ArrayList();

                if (ddlMaterialName.SelectedIndex == 0)
                {
                    Mandatory.Add("Please select materail type.");
                }
                if (txtMaterialName.Visible == true)
                {
                    if (string.IsNullOrEmpty(txtMaterialName.Text.Trim()))
                    {
                        Mandatory.Add("Please enter materail name.");
                    }
                }
                if (ddlMaterialName.Visible == true)
                {
                    if (ddlMaterialName.SelectedIndex == 0)
                    {
                        Mandatory.Add("Please select materail name.");
                    }
                }
                if (String.IsNullOrEmpty(txtNameoftheParty.Text.Trim()))
                {
                    Mandatory.Add("Please enter name of the party.");
                }
                if (string.IsNullOrEmpty(txtQuantity.Text.Trim()))
                {
                    Mandatory.Add("Please enter material quantity.");
                }
                if (ddlUnits.SelectedIndex == 0)
                {
                    Mandatory.Add("Please select units of material.");
                }
                if (ddlOutwardDept.SelectedIndex == 0)
                {
                    Mandatory.Add("Please select department.");
                }
                if (ddlGivenBy.SelectedIndex == 0 || ddlGivenBy.SelectedValue == "")
                {
                    Mandatory.Add("Please select GivenBy.");
                }

                StringBuilder s = new StringBuilder();
                foreach (string x in Mandatory)
                {
                    s.Append(x);
                    s.Append("<br/>");
                }
                HelpClass.custAlertMsg(this, this.GetType(), "" + s.ToString() + "", "error");
                return;
            }
            hdnMaterialCustom.Value = "OutwardSubmit";
            HelpClass.custAlertMsg(this, this.GetType(), "Do you want to submit ?", "confirm");
        }

        protected void btnMOutWardReset_Click(object sender, EventArgs e)
        {
            ddlMaterialType.SelectedIndex = 0;
            OutwardMaterial_Reset();
            upOutwardModel.Update();
        }

        //MaterialOutWard Reset Code
        private void OutwardMaterial_Reset()
        {
            txtDcNo.Text = txtDescriptionofMat.Text = txtMaterialName.Text = txtNameoftheParty.Text = txtExpectedDate.Text = txtOutDate.Text = txtQuantity.Text = txtRemarks_MO.Text = txtVechileNo_MO.Text = string.Empty;
            ddlOutwardDept.SelectedIndex = 0;
            ddlGivenBy.Items.Clear();
            ddlUnits.SelectedIndex = 0;
           
            if (btnMaterialName.Text == "<<")
            {
                btnMaterialName.Text = "+";
                txtMaterialName.Visible = false;
                ddlMaterialName.Visible = true;
            }
            ddlMaterialName.SelectedIndex = 0;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", " getOutwardDate();", true);
        }

        private void GivenBy()
        {
            try
            {
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                int DeptID = Convert.ToInt32(ddlOutwardDept.SelectedValue);
                DataTable dtEmp = objVisitorRegisterBAL.GetEmpData(DeptID);
                ddlGivenBy.DataSource = dtEmp;
                ddlGivenBy.DataTextField = "Name";
                ddlGivenBy.DataValueField = "EmpID";
                ddlGivenBy.DataBind();
                ddlGivenBy.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOM6:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSOM6:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOM6:" + strline + " " + strMsg, "error");
            }
        }

        protected void ddlOutwardDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlOutwardDept.SelectedIndex > 0)
            {
                GivenBy();
                AutoGenerateGP_DC_NO();
                upGivenBy.Update();
            }
        }

        protected void ddlMaterialType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowHideMaterialType();
            OutwardMaterial_Reset();
        }

        public void ShowHideMaterialType()
        {
            if (ddlMaterialType.SelectedIndex == 1 || ddlMaterialType.SelectedIndex == 0)
            {
                divExpectedDate.Visible = true;
            }
            else if (ddlMaterialType.SelectedIndex == 2)
            {
                divExpectedDate.Visible = false;
            }
            upExpectedDate.Update();
        }

        private void AutoGenerateGP_DC_NO()
        {
            try
            {
                MaterialBO objMaterialBO = new MaterialBO();
                objMaterialBO.DeptID = Convert.ToInt32(ddlOutwardDept.SelectedValue);
                txtDcNo.Text = objOutwardMaterialBAL.AutoGenerateGP_DC_NO(objMaterialBO).ToString();
                upOutwardModel.Update();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOM7:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSOM7:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOM7:" + strline + " " + strMsg, "error");
            }
        }

        private void ReceivedBy()
        {
            try
            {
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                int DeptID = Convert.ToInt32(ddlReceivedByDept.SelectedValue);
                DataTable dtReceivedBy = objVisitorRegisterBAL.GetEmpData(DeptID);
                ddlReceivedBy.DataSource = dtReceivedBy;
                ddlReceivedBy.DataTextField = "Name";
                ddlReceivedBy.DataValueField = "EmpID";
                ddlReceivedBy.DataBind();
                ddlReceivedBy.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOM8:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSOM8:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOM8:" + strline + " " + strMsg, "error");
            }
        }

        protected void btnOutwardEdit_List_Click(object sender, EventArgs e)
        {
            GetMaterialDetialsList(Convert.ToInt32(hdnOutwardPkID.Value));
        }

        private void GetMaterialDetialsList(int MaterailID)
        {
            try
            {
                ddlReceivedByDept.SelectedIndex = 0;
                ddlReceivedBy.Items.Clear();
                txtActualReturnDate.Text = "";
                txtOutwardEditReturnQuantity.Text = "";
                txtOutwardComments.Text = "";
                ddlUnitsReturn.SelectedIndex = 0;
                JQDatatableBO objJQDataTableBO = new JQDatatableBO();
                objJQDataTableBO.iMode = 0;
                objJQDataTableBO.pkid = MaterailID;
                objJQDataTableBO.iDisplayLength = 0;
                objJQDataTableBO.iDisplayStart = 0;
                objJQDataTableBO.iSortCol = 0;
                objJQDataTableBO.sSortDir = "";
                objJQDataTableBO.sSearch = "";
                objJQDataTableBO.EmpID = 0;
                objJQDataTableBO.Status = 0;
                OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
                DataTable dt = objOutwardMaterialBAL.GetOutwardMaterialList(objJQDataTableBO);
                if (dt.Rows.Count > 0)
                {
                    LoadOutwardMaterialNameMaster();
                    ddlMaterialType.SelectedValue = dt.Rows[0]["MaterialType"].ToString();
                    ddlMaterialName.SelectedValue = dt.Rows[0]["MaterialMasterID"].ToString();
                    txtDescriptionofMat.Text = dt.Rows[0]["DescriptionOfMaterial"].ToString();
                    txtQuantity.Text = dt.Rows[0]["Quantity"].ToString();
                    ddlUnitsReturn.SelectedValue = ddlUnits.SelectedValue = dt.Rows[0]["UnitID"].ToString();
                    txtNameoftheParty.Text = dt.Rows[0]["NameOfParty"].ToString();
                    txtVechileNo_MO.Text = dt.Rows[0]["VechileNo"].ToString();
                    txtDcNo.Text = dt.Rows[0]["GPorDCPNo"].ToString();
                    txtOutDate.Text = dt.Rows[0]["OutDateTime"].ToString();
                    txtSumReturnedQuantity.Text = dt.Rows[0]["returnedQty"].ToString();
                    //Modified by pradeep
                    if (txtSumReturnedQuantity.Text == "0.00")
                    {
                        hideQuantityWhenZero.Visible = false;
                    }
                    else
                    {
                        hideQuantityWhenZero.Visible = true;
                    }
                    txtExpectedDate.Text = dt.Rows[0]["ExpectedReturnDate1"].ToString();
                    txtRemarks_MO.Text = dt.Rows[0]["Remarks"].ToString();
                    ddlOutwardDept.SelectedValue = dt.Rows[0]["DeptID"].ToString();
                    GivenBy();
                    ddlGivenBy.SelectedValue = dt.Rows[0]["EmpID"].ToString();
                    ViewState["MStatus"] = Convert.ToInt32(dt.Rows[0]["Status"].ToString());
                    btnMOutWardReset.Visible = false;
                    btnSubmit.Visible = false;
                    btnMaterialName.Visible = false;
                    if (ViewState["isEditMode"].ToString() == "1")
                    {
                        if (ViewState["MStatus"].ToString() == "6")// Hod Approve Status=6.
                        {
                            if (ViewState["roleuser"].ToString() == "mstore")
                            {
                                btnStoreApprove.Visible = true;
                                btnStoreReject.Visible = true;
                            }
                        }
                        
                        ddlMaterialType.Enabled = false;
                        divOutwardComments.Visible = true;
                        if (ViewState["MStatus"].ToString() == "5" || ViewState["MStatus"].ToString() == "8")//5-Hod Revert,8-Store Revert
                        {
                            btnOutwardUpdate.Visible = true;
                        }
                        else
                        {
                            OutwardMaterialDisableControls();
                            btnOutwardUpdate.Visible = false;
                        }

                        if (ViewState["vwOutwardUpdateReturnable"].ToString() != "0")
                        {
                            if (ViewState["MStatus"].ToString() == "9")// To Give Access to Update Recevied by,dept etc,when HOD,Store Approves
                            {
                                if (ddlMaterialType.SelectedValue == "1")
                                {
                                    DivForEdit.Visible = true;
                                    btnOutwardUpdate.Visible = true;
                                    btnStoreApprove.Visible = false;
                                    btnStoreReject.Visible = false;
                                }
                                else
                                {
                                    DivForEdit.Visible = false;
                                    OutwardMaterialDisableControls();
                                    btnOutwardUpdate.Visible = false;
                                }
                            }
                        }
                        if (ViewState["vwOutwardView"].ToString() != "0")
                        {
                            OutwardMaterialDisableControls();
                            btnOutwardUpdate.Visible = false;
                        }
                    }
                    else if (ViewState["isEditMode"].ToString() == "0")
                    {
                        btnApprove.Visible = false;
                        btnReject.Visible = false;
                        btnStoreApprove.Visible = false;
                        btnStoreReject.Visible = false;
                        OutwardMaterialDisableControls();
                        btnOutwardUpdate.Visible = false;
                        if (ViewState["vwOutwardUpdateReturnable"].ToString() != "0")
                        {
                            if (ViewState["MStatus"].ToString() == "9")// To Give Access to Update Recevied by,dept etc,when HOD,Store Approves
                            {
                                if (ddlMaterialType.SelectedValue == "1")
                                {
                                    DivForEdit.Visible = true;
                                    divOutwardComments.Visible = true;
                                    txtOutwardComments.Enabled = true;
                                    btnOutwardUpdate.Visible = true;
                                }
                                else
                                {
                                    DivForEdit.Visible = false;
                                    divOutwardComments.Visible = false;
                                    OutwardMaterialDisableControls();
                                    btnOutwardUpdate.Visible = false;
                                }
                            }
                            else
                            {
                                DivForEdit.Visible = false;
                                OutwardMaterialDisableControls();
                                btnOutwardUpdate.Visible = false;
                            }
                        }
                        btnNavCreateOutward.Visible = true;
                    }
                    if (DivForEdit.Visible == true)
                    {
                        if (Convert.ToDecimal(txtSumReturnedQuantity.Text.Trim()) == Convert.ToDecimal(txtQuantity.Text.Trim()))
                        {
                            OutwardMaterialDisableControls();
                            txtOutwardComments.Enabled = false;
                            btnOutwardUpdate.Visible = false;
                        }
                    }
                    ShowHideMaterialType();
                    upOutwardActionbuttons.Update();
                    upOutwardModel.Update();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "showalert", " OpenModelOutwardMaterialEdit();", true);
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOM9:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSOM9:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOM9:" + strline + " " + strMsg, "error");
            }
        }

        protected void btnReturnDetails_Click(object sender, EventArgs e)
        {
            try
            {
                MaterialBO objMaterialBO = new MaterialBO();
                objMaterialBO.OutMaterialID = Convert.ToInt32(hdnOutwardPkID.Value);
                OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
                DataTable dt = objOutwardMaterialBAL.GetOutwardMaterialReturnQuantityHistory(objMaterialBO);
                gv_ReturnedQuantityDetails.DataSource = dt;
                gv_ReturnedQuantityDetails.DataBind();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#QuantityHistory').modal({ show: true, backdrop: 'static', keyboard: false });", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSOM10:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOM10:" + strline + " " + strMsg, "error");
            }
        }

        protected void btnOutwardHistory_Click(object sender, EventArgs e)
        {
            try
            {
                MaterialBO objMaterialBO = new MaterialBO();
                objMaterialBO.OutMaterialID = Convert.ToInt32(hdnOutwardPkID.Value);
                OutwardMaterialBAL objoutwardMaterialBAL = new OutwardMaterialBAL();
                DataTable dtHistory = objOutwardMaterialBAL.GetOutwardMaterialGridHistory(objMaterialBO);

                gv_CommentHistory.DataSource = dtHistory;
                gv_CommentHistory.DataBind();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myCommentHistory').modal({ show: true, backdrop: 'static', keyboard: false });", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSOM11:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOM11:" + strline + " " + strMsg, "error");
            }
        }

        protected void ddlReceivedByDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReceivedBy();
            upReceivedBy.Update();
        }

        protected void ElectronicSign_buttonClick(object sender, EventArgs e)
        {
            bool b = ElectronicSign.IsPasswordValid;
            if (b == true)
            {
                if (ViewState["HODStatusApproveorReject"].ToString() != null)
                {
                    if (ViewState["HODStatusApproveorReject"].ToString().ToLower() == "approve")
                    {
                        ApproveMethod();
                    }
                }
                if (ViewState["HODStatusApproveorReject"].ToString() != null)
                {
                    if (ViewState["HODStatusApproveorReject"].ToString().ToLower() == "revert")
                    {
                        RevertMethod();
                    }
                }
                if (ViewState["StoreStatusApproveorReject"].ToString() != null)
                {
                    if (ViewState["StoreStatusApproveorReject"].ToString().ToLower() == "approve")
                    {
                        StoreApproveMethod();
                    }
                }
                if (ViewState["StoreStatusApproveorReject"].ToString() != null)
                {
                    if (ViewState["StoreStatusApproveorReject"].ToString().ToLower() == "revert")
                    {
                        StoreRevertMethod();
                    }
                }
            }
            if (b == false)
            {
                HelpClass.custAlertMsg(this, this.GetType(), "Invalid Password", "error");
            }
        }

        private void ApproveMethod()
        {
            objMaterialBO = new MaterialBO();
            objMaterialBO.OutMaterialID = Convert.ToInt32(hdnOutwardPkID.Value);
            objMaterialBO.LastChangedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
            objMaterialBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            objMaterialBO.LastChangedComments = txtOutwardComments.Text.Trim();
            objOutwardMaterialBAL = new OutwardMaterialBAL();
            int c = objOutwardMaterialBAL.MaterialApproval(objMaterialBO);
            if (c > 0)
            {
                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Approved successfully", "success", "ReloadOutwardMaterialtable(1);");
            }
            else
            {
                HelpClass.custAlertMsg(this, this.GetType(), "Approved failed", "error");
            }
        }

        private void RevertMethod()
        {
            objMaterialBO = new MaterialBO();
            objMaterialBO.OutMaterialID = Convert.ToInt32(hdnOutwardPkID.Value);
            objMaterialBO.LastChangedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
            objMaterialBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            objMaterialBO.LastChangedComments = txtOutwardComments.Text.Trim();
            objOutwardMaterialBAL = new OutwardMaterialBAL();
            int c = objOutwardMaterialBAL.MaterialRejectOrCancel(objMaterialBO);
            if (c > 0)
            {
                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Reverted successfully", "success", "ReloadOutwardMaterialtable(1);");
            }
            else
            {
                HelpClass.custAlertMsg(this, this.GetType(), "Reverted failed", "error");
            }
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            ViewState["HODStatusApproveorReject"] = "Approve";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "openElectronicSignModal();", true);
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtOutwardComments.Text.Trim()))
            {
                HelpClass.custAlertMsg(this, this.GetType(), "Please enter revert reason in comments.", "error");
                return;
            }
            ViewState["HODStatusApproveorReject"] = "Revert";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "openElectronicSignModal();", true);
        }

        protected void btnStoreApprove_Click(object sender, EventArgs e)
        {
            ViewState["StoreStatusApproveorReject"] = "Approve";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "openElectronicSignModal();", true);
        }

        protected void btnStoreReject_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtOutwardComments.Text.Trim()))
            {
                HelpClass.custAlertMsg(this, this.GetType(), "Please enter revert reason in comments.", "error");
                return;
            }
            ViewState["StoreStatusApproveorReject"] = "Revert";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "openElectronicSignModal();", true);
        }

        private void StoreApproveMethod()
        {
            objMaterialBO = new MaterialBO();
            objMaterialBO.OutMaterialID = Convert.ToInt32(hdnOutwardPkID.Value);
            objMaterialBO.LastChangedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
            objMaterialBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            objMaterialBO.LastChangedComments = txtOutwardComments.Text.Trim();
            objOutwardMaterialBAL = new OutwardMaterialBAL();
            int c = objOutwardMaterialBAL.MaterialStoreApproval(objMaterialBO);
            if (c > 0)
            {
                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Approved successfully.", "success", "ReloadOutwardMaterialtable(1);");
            }
            else
            {
                HelpClass.custAlertMsg(this, this.GetType(), "Approved failed.", "error");
            }
        }

        private void StoreRevertMethod()
        {
            objMaterialBO = new MaterialBO();
            objMaterialBO.OutMaterialID = Convert.ToInt32(hdnOutwardPkID.Value);
            objMaterialBO.LastChangedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
            objMaterialBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            objMaterialBO.LastChangedComments = txtOutwardComments.Text.Trim();
            objOutwardMaterialBAL = new OutwardMaterialBAL();
            int c = objOutwardMaterialBAL.MaterialStoreRejectOrCancel(objMaterialBO);
            if (c > 0)
            {
                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Reverted successfully.", "success", "ReloadOutwardMaterialtable(1);");
            }
            else
            {
                HelpClass.custAlertMsg(this, this.GetType(), "Reverted failed.", "error");
            }
        }

        protected void btnOutwardMaterialCheckOut_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", " getCheckoutDate();", true);
            try
            {
                if (ViewState["vwCheckIncount"].ToString() != "0")
                {
                    MaterialBO objMaterialBO = new MaterialBO();
                    objMaterialBO.OutMaterialID = Convert.ToInt32(hdnOutwardPkID.Value);
                    objMaterialBO.OutDateTime = hdnOutwardCheckout.Value;
                    JQDatatableBO objJQDataTableBO = new JQDatatableBO();
                    objJQDataTableBO.iMode = 0;
                    objJQDataTableBO.pkid = Convert.ToInt32(hdnOutwardPkID.Value);
                    objJQDataTableBO.iDisplayLength = 0;
                    objJQDataTableBO.iDisplayStart = 0;
                    objJQDataTableBO.iSortCol = 0;
                    objJQDataTableBO.sSortDir = "";
                    objJQDataTableBO.sSearch = "";
                    objJQDataTableBO.EmpID = 0;
                    objJQDataTableBO.Status = 0;
                    objOutwardMaterialBAL = new OutwardMaterialBAL();
                    DataTable dt = objOutwardMaterialBAL.GetOutwardMaterialList(objJQDataTableBO);
                    if (dt.Rows.Count > 0)
                    {
                        hdnMaterialName.Value = dt.Rows[0]["MaterialName"].ToString();
                    }
                    int c = objOutwardMaterialBAL.CheckoutOutwardMaterial(objMaterialBO);
                    if (c > 0)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "" + hdnMaterialName.Value + " " + "Checked in successfully.", "success", "ReloadOutwardMaterialtable(0);");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOM12:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSOM12:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOM12:" + strline + " " + strMsg, "error");
            }
        }

        protected void btnNavCreateOutward_Click(object sender, EventArgs e)
        {
            if (ddlMaterialType.SelectedIndex == 1 || ddlMaterialType.SelectedIndex == 2 || ddlMaterialType.SelectedIndex == 0)
            {
                DivForEdit.Visible = false;
            }
            LoadOutwardMaterialNameMaster();
            txtExpectedDate.Text = "";
            btnOutwardUpdate.Visible = false;
            ddlMaterialType.Enabled = true;
            btnSubmit.Visible = true;
            btnMOutWardReset.Visible = true;
            divOutwardComments.Visible = false;
            btnMaterialName.Visible = true;
            btnApprove.Visible = false;
            btnReject.Visible = false;
            btnStoreApprove.Visible = false;
            btnStoreReject.Visible = false;
            OutwardMaterialEnableControls();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", " getOutwardDate();", true);
            upOutwardModel.Update();
            upOutwardMaterialType.Update();
            upOutwardActionbuttons.Update();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "showalert", " OpenModelOutwardMaterial();", true);
        }

        //Outward Material Update Code
        protected void btnOutwardUpdate_Click(object sender, EventArgs e)
        {
            if (DivForEdit.Visible == true)
            {
                if (ddlReceivedByDept.SelectedIndex == 0 || ddlReceivedBy.SelectedValue == "0" || txtOutwardEditReturnQuantity.Text.Trim() == "" || ddlUnitsReturn.SelectedIndex == 0)
                {
                    ArrayList Mandatory = new ArrayList();
                    if (ddlReceivedByDept.SelectedIndex == 0)
                    {
                        Mandatory.Add("Select received by department.");
                    }
                    if (ddlReceivedBy.SelectedValue == "" || ddlReceivedBy.SelectedValue == "0")
                    {
                        Mandatory.Add("Select received by.");
                    }
                    if (string.IsNullOrEmpty(txtOutwardEditReturnQuantity.Text.Trim()))
                    {
                        Mandatory.Add("Enter return quantity.");
                    }
                    

                    if (ddlUnitsReturn.SelectedIndex == 0)
                    {
                        Mandatory.Add("Select return units.");
                    }
                    if (String.IsNullOrEmpty(txtOutwardComments.Text.Trim()))
                    {
                        Mandatory.Add("Please enter modification reason in comments.");
                    }
                    StringBuilder s = new StringBuilder();
                    foreach (string x in Mandatory)
                    {
                        s.Append(x);
                        s.Append("<br/>");
                    }
                    HelpClass.custAlertMsg(this, this.GetType(), "" + s.ToString() + "", "error");
                    return;
                }
            }

            if (String.IsNullOrEmpty(txtOutwardComments.Text.Trim()))
            {
                HelpClass.custAlertMsg(this, this.GetType(), "Please enter modification reason in comments.", "error");
                return;
            }
            hdnMaterialCustom.Value = "OutwardUpdate";
            HelpClass.custAlertMsg(this, this.GetType(), "Do you want to update ?", "confirm");
        }

        protected void txtMaterialName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", " getOutwardDate();", true);
                if (txtMaterialName.Visible == true)
                {
                    if (string.IsNullOrEmpty(txtMaterialName.Text.Trim()))
                    {
                        HelpClass.custAlertMsg(btnMaterialName, btnMaterialName.GetType(), "Please enter material name.", "error");
                        return;
                    }
                }
                int Count = objOutwardMaterialBAL.MaterialNameifExistsorNot(txtMaterialName.Text.Trim());
                if (Count > 0)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Material name already exists.", "error");
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOM13:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSOM13:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOM13:" + strline + " " + strMsg, "error");
            }
        }

        private void OutwardMaterialEnableControls()
        {
            ddlMaterialType.Enabled = true;
            txtMaterialName.Enabled = true;
            ddlMaterialName.Enabled = true;
            txtNameoftheParty.Enabled = true;
            txtQuantity.Enabled = true;
            ddlUnits.Enabled = true;
            ddlOutwardDept.Enabled = true;
            ddlGivenBy.Enabled = true;
           // txtDcNo.Enabled = true;
            txtVechileNo_MO.Enabled = true;
            //txtOutDate.Enabled = true;
            txtExpectedDate.Enabled = true;
            ddlReceivedByDept.Enabled = true;
            ddlReceivedBy.Enabled = true;
            txtSumReturnedQuantity.Enabled = true;
            btnReturnDetails.Enabled = true;
            txtOutwardEditReturnQuantity.Enabled = true;
            ddlUnitsReturn.Enabled = true;
            txtActualReturnDate.Enabled = true;
            txtDescriptionofMat.Enabled = true;
            txtRemarks_MO.Enabled = true;
            
        }

        private void OutwardMaterialDisableControls()
        {
            ddlMaterialType.Enabled = false;
            txtMaterialName.Enabled = false;
            ddlMaterialName.Enabled = false;
            txtNameoftheParty.Enabled = false;
            txtQuantity.Enabled = false;
            ddlUnits.Enabled = false;
            ddlOutwardDept.Enabled = false;
            ddlGivenBy.Enabled = false;
            txtDcNo.Enabled = false;
            txtVechileNo_MO.Enabled = false;
            txtOutDate.Enabled = false;
            txtExpectedDate.Enabled = false;
            
            txtSumReturnedQuantity.Enabled = false;
           
            txtDescriptionofMat.Enabled = false;
            txtRemarks_MO.Enabled = false;
           
        }

        #endregion OutwardMaterial

        #region Inward Material
        


        private void LoadInwardMaterialNameMaster()
        {
            try
            {
                objOutwardMaterialBAL = new OutwardMaterialBAL();
                DataTable dtMaterialNames = objOutwardMaterialBAL.LoadOutwardMaterialNameMaster();
                ddlInMaterialName.DataTextField = "MaterialName";
                ddlInMaterialName.DataValueField = "MaterialMasterID";
                ddlInMaterialName.DataSource = dtMaterialNames;
                ddlInMaterialName.DataBind();
                ddlInMaterialName.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVER1:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVER1:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVER1:" + strline + " " + strMsg, "error");
            }
        }

        private void InwardLoadDepartment()
        {
            try
            {
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                DataTable dtdept = objVisitorRegisterBAL.LoadDepartment();
                ddlInwardDepartment.DataTextField = dtdept.Columns["DepartmentName"].ToString();
                ddlInwardDepartment.DataValueField = dtdept.Columns["DeptID"].ToString();
                ddlInwardDepartment.DataSource = dtdept;
                ddlInwardDepartment.DataBind();
                ddlInwardDepartment.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIM1:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSIM1:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIM1:" + strline + " " + strMsg, "error");
            }
        }

        private void InwardLoadUnits()
        {
            try
            {
                OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
                DataTable dtUnits = objOutwardMaterialBAL.BindUnits();
                ddlInwardUnits.DataTextField = dtUnits.Columns["UnitName"].ToString();
                ddlInwardUnits.DataValueField = dtUnits.Columns["UnitID"].ToString();
                ddlInwardUnits.DataSource = dtUnits;
                ddlInwardUnits.DataBind();
                ddlInwardUnits.Items.Insert(0, new ListItem("", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIM2:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSIM2:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIM2:" + strline + " " + strMsg, "error");
            }
        }

        protected void ddlInwardDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            InwardEmployee();
            // UpdatePanel13.Update();
        }

        //Inward Materials Submit Code
        protected void btnInWardMateralSubmit_Click(object sender, EventArgs e)
        {
            if ( txtINWDQuantity.Text.Trim() == "" || txtINWDNameoftheSupplier.Text.Trim() == "" || ddlInwardUnits.SelectedValue == "0" || ddlInwardDepartment.SelectedValue == "0" || ddlINRecievedBy.SelectedValue == "0" || ddlInWardTypePO.SelectedValue == "0")
            {
                ArrayList Mandatory = new ArrayList();

                if (ddlInWardTypePO.SelectedIndex == 0)
                {
                    Mandatory.Add("Please select materail type.");
                }
                if (txtINWDMaterialName.Visible == true)
                {
                    if (string.IsNullOrEmpty(txtINWDMaterialName.Text.Trim()))
                    {
                        Mandatory.Add("Please enter materail name.");
                    }
                }
                if (ddlInMaterialName.Visible == true)
                {
                    if (ddlInMaterialName.SelectedIndex == 0)
                    {
                        Mandatory.Add("Please select materail name.");
                    }
                }
                if (String.IsNullOrEmpty(txtINWDNameoftheSupplier.Text.Trim()))
                {
                    Mandatory.Add("Please enter name of the supplier.");
                }
                if (string.IsNullOrEmpty(txtINWDQuantity.Text.Trim()))
                {
                    Mandatory.Add("Please enter material quantity.");
                }
                if (ddlInwardUnits.SelectedIndex == 0)
                {
                    Mandatory.Add("Please select units of material.");
                }
                if (ddlInwardDepartment.SelectedIndex == 0)
                {
                    Mandatory.Add("Please select department.");
                }
                if (ddlINRecievedBy.SelectedIndex == 0 || ddlINRecievedBy.SelectedValue == "")
                {
                    Mandatory.Add("Please select recieved by.");
                }
                StringBuilder s = new StringBuilder();
                foreach (string x in Mandatory)
                {
                    s.Append(x);
                    s.Append("<br/>");
                }
                HelpClass.custAlertMsg(this, this.GetType(), "" + s.ToString() + "", "error");
                return;
            }
            hdnMaterialCustom.Value = "InwardSubmit";
            HelpClass.custAlertMsg(this, this.GetType(), "Do you want to submit ?", "confirm");
        }

        protected void btnInWardMateralReset_Click(object sender, EventArgs e)
        {
            INWardMaterial_Reset();
            ddlInWardTypePO.SelectedIndex = 0;
            
        }

        //Inward Materials Reset Code
        private void INWardMaterial_Reset()
        {
            txtINWDDcNo.Text = txtINWDDescriptionofMat.Text = txtINWDMaterialName.Text = txtINWDInDate.Text = txtINWDQuantity.Text = txtINWDRemarks.Text = txtINWDVechileNo.Text = txtINWDAssessable.Text = txtINWDNameoftheSupplier.Text = string.Empty;
            ddlInwardDepartment.SelectedIndex = 0;
            ddlINRecievedBy.Items.Clear();
            ddlInwardUnits.SelectedIndex = 0;            
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", "getInwardDate();", true);
           
            if (btnInMaterialName.Text == "<<")
            {
                btnInMaterialName.Text = "+";
                txtINWDMaterialName.Visible = false;
                ddlInMaterialName.Visible = true;
            }
            ddlInMaterialName.SelectedIndex = 0;
            txtINWDMaterialName.Text = string.Empty;
            upInwardModal.Update(); upInMaterialName.Update();
        }

        private void InwardEmployee()
        {
            try
            {
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                int DeptID = Convert.ToInt32(ddlInwardDepartment.SelectedValue);
                DataTable dtEmp = objVisitorRegisterBAL.GetEmpData(DeptID);
                ddlINRecievedBy.DataSource = dtEmp;
                ddlINRecievedBy.DataTextField = "Name";
                ddlINRecievedBy.DataValueField = "EmpID";
                ddlINRecievedBy.DataBind();
                ddlINRecievedBy.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIM4:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSIM4:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIM4:" + strline + " " + strMsg, "error");
            }
        }

        protected void txtINWDMaterialName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", "getInwardDate();", true);
                if (txtINWDMaterialName.Visible == true)
                {
                    if (string.IsNullOrEmpty(txtINWDMaterialName.Text.Trim()))
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Please enter material name.", "error");
                        return;
                    }
                }
                int Count = objOutwardMaterialBAL.MaterialNameifExistsorNot(txtINWDMaterialName.Text.Trim());
                if (Count > 0)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Material name already exists.", "error");
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVER4:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVER4:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVER4:" + strline + " " + strMsg, "error");
            }
        }

        protected void btnInMaterialName_Click(object sender, EventArgs e)
        {
            if (btnInMaterialName.Text == "+")
            {
                btnInMaterialName.Text = "<<";
                txtINWDMaterialName.Visible = true;
                ddlInMaterialName.Visible = false;
            }
            else if (btnInMaterialName.Text == "<<")
            {
                btnInMaterialName.Text = "+";
                txtINWDMaterialName.Visible = false;
                ddlInMaterialName.Visible = true;
            }
        }

        private void BindInwardMaterialType()
        {
            try
            {
                InwardMaterialBAL objInwardMaterialBAL = new InwardMaterialBAL();
                DataTable dtMaterial = new DataTable();
                dtMaterial = objInwardMaterialBAL.BindMaterialType();
                ddlInWardTypePO.DataTextField = "InwardMaterialType";
                ddlInWardTypePO.DataValueField = "InwardMaterialTypeID";
                ddlInWardTypePO.DataSource = dtMaterial;
                ddlInWardTypePO.DataBind();
                ddlInWardTypePO.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIM5:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSIM5:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIM5:" + strline + " " + strMsg, "error");
            }
        }

        protected void ddlInWardTypePO_SelectedIndexChanged1(object sender, EventArgs e)
        {
            //INWardMaterial_Reset();
            //upInwardModal.Update();
        }
        protected void btnInwardEdit_List_Click(object sender, EventArgs e)
        {
            try
            {
                JQDatatableBO objJQDataTableBO = new JQDatatableBO();
                objJQDataTableBO.iMode = 0;
                objJQDataTableBO.pkid = Convert.ToInt32(hdnInwardPkID.Value);
                objJQDataTableBO.iDisplayLength = 0;
                objJQDataTableBO.iDisplayStart = 0;
                objJQDataTableBO.iSortCol = 0;
                objJQDataTableBO.sSortDir = "";
                objJQDataTableBO.sSearch = "";
                InwardMaterialBAL objInwardMaterialBAL = new InwardMaterialBAL();
                DataTable dt = objInwardMaterialBAL.GetInMaterialList(objJQDataTableBO);
                ddlInWardTypePO.SelectedValue = dt.Rows[0]["MaterialType"].ToString();
                ddlInMaterialName.SelectedValue = dt.Rows[0]["MaterialMasterID"].ToString();
                txtINWDDescriptionofMat.Text = dt.Rows[0]["DescriptionOfMaterial"].ToString();
                txtINWDQuantity.Text = dt.Rows[0]["Quantity"].ToString();
                ddlInwardUnits.SelectedValue = dt.Rows[0]["UnitID"].ToString();
                txtINWDNameoftheSupplier.Text = dt.Rows[0]["SupplierName"].ToString();
                txtINWDVechileNo.Text = dt.Rows[0]["VechileNo"].ToString();
                txtINWDDcNo.Text = dt.Rows[0]["DCP_InvoiceNo"].ToString();
                txtINWDRemarks.Text = dt.Rows[0]["Remarks"].ToString();
                txtINWDInDate.Text = dt.Rows[0]["InDateTime"].ToString();
                txtINWDAssessable.Text = dt.Rows[0]["AssessableValue"].ToString();
                ddlInwardDepartment.SelectedValue = dt.Rows[0]["DeptID"].ToString();
                InwardEmployee();
                ddlINRecievedBy.SelectedValue = dt.Rows[0]["ReceivedByID"].ToString();
                div_Invechile_comments.Visible = true;
                btnInWardMaterialUpdate.Visible = true;
                btnInWardMateralSubmit.Visible = false;
                btnInWardMateralReset.Visible = false;
                btnInMaterialName.Visible = false;
                if (ViewState["vwInwardView"].ToString() != "0")
                {
                    //Modified by pradeep
                    InwardMaterialDisableControls();
                    btnInWardMaterialUpdate.Visible = false;

                }
                upInwardModal.Update();
                upInwardActionbtns.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "showalert", " OpenModelInwardMaterialEdit();", true);
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIML4:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSIML4:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIML4:" + strline + " " + strMsg, "error");
            }
        }

        protected void btnInWardMaterialUpdate_Click(object sender, EventArgs e)
        {

        }

        protected void btnNavInwardMaterial_Click(object sender, EventArgs e)
        {
            ddlInWardTypePO.SelectedIndex = 0;
            ddlInwardUnits.SelectedIndex = 0;
            ddlInwardDepartment.SelectedIndex = 0;
            ddlInMaterialName.SelectedIndex = 0;
            div_Invechile_comments.Visible = false;
            btnInWardMaterialUpdate.Visible = false;
            btnInWardMateralSubmit.Visible = true;
            btnInWardMateralReset.Visible = true;
            btnInMaterialName.Visible = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", "getInwardDate();", true);
            upInwardModal.Update();
            upInwardActionbtns.Update();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "showalert", "OpenModelInwardMaterial();", true);
        }

        protected void btnMaterialName_Click(object sender, EventArgs e)
        {
            if (btnMaterialName.Text == "+")
            {
                btnMaterialName.Text = "<<";
                txtMaterialName.Visible = true;
                ddlMaterialName.Visible = false;
            }
            else if (btnMaterialName.Text == "<<")
            {
                btnMaterialName.Text = "+";
                txtMaterialName.Visible = false;
                ddlMaterialName.Visible = true;

            }
        }

        private void InwardMaterialDisableControls()
        {
            ddlInWardTypePO.Enabled = false;
            txtINWDMaterialName.Enabled = false;
            ddlInMaterialName.Enabled = false;
            txtINWDNameoftheSupplier.Enabled = false;
            txtINWDQuantity.Enabled = false;
            ddlInwardUnits.Enabled = false;
            txtINWDDcNo.Enabled = false;
            txtINWDVechileNo.Enabled = false;
            txtINWDInDate.Enabled = false;
            ddlInwardDepartment.Enabled = false;
            txtDepartment.Enabled = false;
            ddlINRecievedBy.Enabled = false;
            txtINReceivedBy.Enabled = false;
            txtINWDAssessable.Enabled = false;
            txtINWDDescriptionofMat.Enabled = false;
            txtINWDRemarks.Enabled = false;
            txtINWDComments.Enabled = false;
        }

        #endregion Inward Materail

        protected void ConfirmCustomAlert_buttonClick(object sender, EventArgs e)
        {
            try
            {
                if (hdnMaterialCustom.Value == "InwardSubmit")
                {
                    objMaterialBO = new MaterialBO();
                    objMaterialBO.MaterialType = ddlInWardTypePO.SelectedValue.Trim();

                    string AddInMaterialName;
                    if (txtINWDMaterialName.Visible == true)
                    {
                        objMaterialBO.MaterialName = txtINWDMaterialName.Text.Trim();
                        objMaterialBO.CreatedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                        objMaterialBO.CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                        objOutwardMaterialBAL = new OutwardMaterialBAL();
                        AddInMaterialName = objOutwardMaterialBAL.MaterialClientNameInsert(objMaterialBO).ToString();
                        if (AddInMaterialName == "0")
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Material name already exists.", "error");
                            return;
                        }
                    }
                    else
                    {
                        AddInMaterialName = ddlInMaterialName.SelectedValue;
                    }
                    objMaterialBO.MaterialMasterID = Convert.ToInt32(AddInMaterialName);
                    objMaterialBO.DescriptionOfMaterial = txtINWDDescriptionofMat.Text.Trim();
                    objMaterialBO.Quantity = Convert.ToDecimal(txtINWDQuantity.Text.Trim());
                    objMaterialBO.UnitsID = Convert.ToInt32(ddlInwardUnits.SelectedValue.Trim());
                    if (ddlInwardDepartment.SelectedIndex > 0)
                        objMaterialBO.Department = Convert.ToInt32(ddlInwardDepartment.SelectedValue.Trim());
                    else
                        ddlInwardDepartment.SelectedValue = "0";
                    if (ddlINRecievedBy.SelectedIndex > 0)
                        objMaterialBO.ReceivedBy = Convert.ToInt32(ddlINRecievedBy.SelectedValue.Trim());
                    else
                        ddlINRecievedBy.SelectedValue = "0";
                    objMaterialBO.VechileNo = txtINWDVechileNo.Text.Trim();
                    objMaterialBO.DCP_InvoiceNo = txtINWDDcNo.Text.Trim();
                    objMaterialBO.AssessableValue = txtINWDAssessable.Text.Trim();                    
                    objMaterialBO.InDateTime = txtINWDInDate.Text.Trim();
                    objMaterialBO.Remarks = txtINWDRemarks.Text.Trim();
                    objMaterialBO.SupplierName = txtINWDNameoftheSupplier.Text.Trim();
                    objMaterialBO.CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                    objMaterialBO.CreatedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                    objMaterialBO.DeptName = string.Empty;
                    objMaterialBO.EmpName = string.Empty;
                    InwardMaterialBAL objInwardMaterialBAL = new InwardMaterialBAL();
                    int c = objInwardMaterialBAL.InsertInwardMaterial(objMaterialBO);
                    if (c > 0)
                    {
                        LoadInwardMaterialNameMaster();
                        INWardMaterial_Reset();
                        ddlInWardTypePO.SelectedIndex = 0;
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Material details submitted successfully.", "success", "ReloadInwardtable()");
                    }
                    else
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Material details submission failed.", "error");
                    }
                }
                else if (hdnMaterialCustom.Value == "Update")
                {
                    MaterialBO objMaterialBO = new MaterialBO();
                    objMaterialBO.Sno = Convert.ToInt32(hdnInwardPkID.Value);
                    objMaterialBO.MaterialType = ddlInWardTypePO.SelectedValue.Trim();
                    objMaterialBO.MaterialMasterID = Convert.ToInt32(ddlInMaterialName.SelectedValue);
                    objMaterialBO.Quantity = Convert.ToDecimal(HelpClass.ToDecimal(txtINWDQuantity.Text.Trim()));
                    objMaterialBO.SupplierName = txtINWDNameoftheSupplier.Text.Trim();
                    objMaterialBO.VechileNo = txtINWDVechileNo.Text.Trim();
                    objMaterialBO.DCP_InvoiceNo = txtINWDDcNo.Text.Trim();
                    objMaterialBO.LastChangedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                    objMaterialBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                    objMaterialBO.LastChangedComments = txtINWDComments.Text.Trim();
                    objMaterialBO.AssessableValue = txtINWDAssessable.Text.Trim();                    
                    objMaterialBO.Remarks = txtINWDRemarks.Text.Trim();
                    objMaterialBO.DescriptionOfMaterial = txtINWDDescriptionofMat.Text.Trim();
                    objMaterialBO.UnitsID = Convert.ToInt32(ddlInwardUnits.SelectedValue.Trim());
                    if (ddlInwardDepartment.SelectedIndex > 0)
                        objMaterialBO.Department = Convert.ToInt32(ddlInwardDepartment.SelectedValue.Trim());
                    else
                        ddlInwardDepartment.SelectedValue = "0";
                    if (ddlINRecievedBy.SelectedIndex > 0)
                        objMaterialBO.ReceivedBy = Convert.ToInt32(ddlINRecievedBy.SelectedValue.Trim());
                    else
                        ddlINRecievedBy.SelectedValue = "0";
                    objMaterialBO.DeptName = string.Empty;
                    objMaterialBO.EmpName = string.Empty;
                    InwardMaterialBAL objInwardMaterialBAL = new InwardMaterialBAL();
                    int c = objInwardMaterialBAL.Update_InwardMaterialEdit(objMaterialBO);
                    if (c > 0)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Materials details updated successfully.", "success", "ReloadInwardtable();");
                    }
                    else
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Materials details updation failed.", "error");
                    }
                }
                else if (hdnMaterialCustom.Value == "OutwardSubmit")
                {
                    try
                    {
                        objMaterialBO = new MaterialBO();
                        objMaterialBO.MaterialType = ddlMaterialType.SelectedValue.Trim();

                        string AddMaterialName;
                        if (txtMaterialName.Visible == true)
                        {
                            objMaterialBO.MaterialName = txtMaterialName.Text.Trim();
                            objMaterialBO.CreatedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                            objMaterialBO.CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                            AddMaterialName = objOutwardMaterialBAL.MaterialClientNameInsert(objMaterialBO).ToString();
                            if (AddMaterialName == "0")
                            {
                                HelpClass.custAlertMsg(this, this.GetType(), "Material name already exists.", "error");
                                return;
                            }
                        }
                        else
                        {
                            AddMaterialName = ddlMaterialName.SelectedValue;
                        }
                        objMaterialBO.MaterialMasterID = Convert.ToInt32(AddMaterialName);
                        objMaterialBO.DescriptionOfMaterial = txtDescriptionofMat.Text.Trim();
                        objMaterialBO.Quantity = Convert.ToDecimal(txtQuantity.Text.Trim());
                        objMaterialBO.UnitsID = Convert.ToInt32(ddlUnits.SelectedValue.Trim());
                        objMaterialBO.Department = Convert.ToInt32(ddlOutwardDept.SelectedValue.Trim());
                        objMaterialBO.GivenBy = Convert.ToInt32(ddlGivenBy.SelectedValue);
                        objMaterialBO.VechileNo = txtVechileNo_MO.Text.Trim();
                        objMaterialBO.GPorDCPNo = txtDcNo.Text.Trim();
                        objMaterialBO.NameOfParty = txtNameoftheParty.Text.Trim();                        
                        objMaterialBO.OutDateTime = txtOutDate.Text.Trim();
                        objMaterialBO.Remarks = txtRemarks_MO.Text.Trim();
                        objMaterialBO.CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                        objMaterialBO.CreatedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                        objMaterialBO.ExpectedReturnDate = txtExpectedDate.Text.Trim();
                        int OutwardID = objOutwardMaterialBAL.InsertMaterial(objMaterialBO);
                        if (OutwardID > 0)
                        {
                            OutwardMaterial_Reset();
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Material Details Submitted Successfully.", "success", "ReloadOutwardMaterialtable(0);");
                        }
                        else
                        {
                            HelpClass.custAlertMsg(btnSubmit, btnSubmit.GetType(), "Material details submission failed.");
                        }
                    }
                    catch (SqlException sqe)
                    {
                        string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                        HelpClass.custAlertMsg(this, this.GetType(), "VMSIM1:" + strmsgF, "error");
                    }
                    catch (Exception ex)
                    {
                        string strline = HelpClass.LineNo(ex);
                        string strMsg = HelpClass.SQLEscapeString(ex.Message);
                        Aizant_log.Error("VMSIM1:" + strline + " " + strMsg);
                        HelpClass.custAlertMsg(this, this.GetType(), "VMSIM1:" + strline + " " + strMsg, "error");
                    }
                }
                else if (hdnMaterialCustom.Value == "OutwardUpdate")
                {
                    try
                    {
                        objMaterialBO = new MaterialBO();
                        objMaterialBO.OutMaterialID = Convert.ToInt32(hdnOutwardPkID.Value);
                        objMaterialBO.MaterialType = ddlMaterialType.SelectedValue.Trim();
                        objMaterialBO.DescriptionOfMaterial = txtDescriptionofMat.Text.Trim();
                        objMaterialBO.MaterialMasterID = Convert.ToInt32(ddlMaterialName.SelectedValue);
                        objMaterialBO.Quantity = Convert.ToDecimal(txtQuantity.Text.Trim());
                        objMaterialBO.UnitsID = Convert.ToInt32(ddlUnits.SelectedValue.Trim());
                        objMaterialBO.NameOfParty = txtNameoftheParty.Text.Trim();
                        objMaterialBO.Department = Convert.ToInt32(ddlOutwardDept.SelectedValue.Trim());
                        objMaterialBO.VechileNo = txtVechileNo_MO.Text.Trim();
                        objMaterialBO.GPorDCPNo = txtDcNo.Text.Trim();
                        objMaterialBO.GivenBy = Convert.ToInt32(ddlGivenBy.SelectedValue.Trim());
                        objMaterialBO.LastChangedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                        objMaterialBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                        objMaterialBO.LastChangedComments = txtOutwardComments.Text.Trim();
                        objMaterialBO.Remarks = txtRemarks_MO.Text.Trim();
                        objMaterialBO.ExpectedReturnDate = txtExpectedDate.Text.Trim();
                        objMaterialBO.ActualReturnDate = txtActualReturnDate.Text.Trim();
                        objMaterialBO.UpdateMode = 1;
                        if (ViewState["vwOutwardUpdateReturnable"].ToString() != "0")
                        {
                            objMaterialBO.ReceivedDepartment = Convert.ToInt32(ddlReceivedByDept.SelectedValue.Trim());
                            objMaterialBO.ReceivedBy = Convert.ToInt32(ddlReceivedBy.SelectedValue.Trim());
                            objMaterialBO.Return_Quantity = Convert.ToDecimal(HelpClass.ToDecimal(txtOutwardEditReturnQuantity.Text.Trim()));
                            objMaterialBO.UnitsID = Convert.ToInt32(ddlUnitsReturn.SelectedValue.Trim());
                            objMaterialBO.UpdateMode = 2;
                        }
                        OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
                        int c = objOutwardMaterialBAL.Update_OutwardMaterialEdit(objMaterialBO);
                        if (c > 0)
                        {
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Outward materials details updated successfully.", "success", "ReloadOutwardMaterialtable(1);");
                        }
                        else
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Outward materials details updation failed.", "error");
                        }
                    }
                    catch (SqlException sqe)
                    {
                        string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                        HelpClass.custAlertMsg(this, this.GetType(), "VMSIM2:" + strmsgF, "error");
                    }
                    catch (Exception ex)
                    {
                        string strline = HelpClass.LineNo(ex);
                        string strMsg = HelpClass.SQLEscapeString(ex.Message);
                        Aizant_log.Error("VMSIM2:" + strline + " " + strMsg);
                        HelpClass.custAlertMsg(this, this.GetType(), "VMSIM2:" + strline + " " + strMsg, "error");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIM3:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSIM3:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIM3:" + strline + " " + strMsg, "error");
            }
        }

        void screenAuthentication()
        {
            int outwardvisible = 0;
            int inwardvisible = 0;
            DataTable dtAthuntication = GmsHelpClass.GetRolePrivileges();
            btnNavCreateOutward.Visible = false;
            if (dtAthuntication.Rows.Count > 0)
            {
                //mod=3,PrivilegeID=10,11,12,13,14,15,19
                int OutCreateCount = dtAthuntication.AsEnumerable().Count(row => row.Field<int>("PrivilegeID") == 10);//Outward Create
                ViewState["vwCreateCount"] = OutCreateCount.ToString();

                if (OutCreateCount > 0)
                {
                    btnNavCreateOutward.Visible = true;
                }
                int OutApprovalcount = dtAthuntication.AsEnumerable().Count(row => row.Field<int>("PrivilegeID") == 11);//Outward Hod Approval
                if (OutApprovalcount > 0)
                {
                    OutwardMaterialDisableControls();
                    btnApprove.Visible = true;
                    btnReject.Visible = true;
                }
                else
                {
                    btnApprove.Visible = false;
                    btnReject.Visible = false;
                }
                int OutModifycount = dtAthuntication.AsEnumerable().Count(row => row.Field<int>("PrivilegeID") == 12);//Outward Modify
                //ViewState["vwModifycount"] = OutModifycount.ToString();

                int OutAcknowlegecount = dtAthuntication.AsEnumerable().Count(row => row.Field<int>("PrivilegeID") == 13);//Outward Store Approval
                ViewState["vwAcknowlegecount"] = OutAcknowlegecount.ToString();
                if (OutAcknowlegecount > 0)
                {
                    OutwardMaterialDisableControls();
                    btnStoreApprove.Visible = true;
                    btnStoreReject.Visible = true;
                }
                else
                {
                    btnStoreApprove.Visible = false;
                    btnStoreReject.Visible = false;
                }

                int OutCheckOutcount = dtAthuntication.AsEnumerable().Count(row => row.Field<int>("PrivilegeID") == 14);//Outward CheckOut create
                int OutCheckIncount = dtAthuntication.AsEnumerable().Count(row => row.Field<int>("PrivilegeID") == 15);//Outward CheckIn
                int OutPrintcount = dtAthuntication.AsEnumerable().Count(row => row.Field<int>("PrivilegeID") == 19);//Outward Print
                ViewState["vwOutPrintcount"] = OutPrintcount.ToString();
                if (OutPrintcount==0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", "HidePrintbtn();", true);
                }
                int OutwardView = dtAthuntication.AsEnumerable().Count(row => row.Field<int>("PrivilegeID") == 26);//Outward View
                ViewState["vwOutwardView"] = OutwardView.ToString();
                int OutwardUpdateReturnable = dtAthuntication.AsEnumerable().Count(row => row.Field<int>("PrivilegeID") == 29);//to update only Retunable Details
                ViewState["vwOutwardUpdateReturnable"] = OutwardUpdateReturnable.ToString();
                int InwardCreate = dtAthuntication.AsEnumerable().Count(row => row.Field<int>("PrivilegeID") == 16);//Inward Material Create
                int InwardModify = dtAthuntication.AsEnumerable().Count(row => row.Field<int>("PrivilegeID") == 17);//Inward Material Modify
                int InwardView = dtAthuntication.AsEnumerable().Count(row => row.Field<int>("PrivilegeID") == 27);//Inward Material View
                ViewState["vwInwardView"] = InwardView.ToString();
                bool isOutward = false, isInward = false;
                btnNavInwardMaterial.Visible = false;
                btnInWardMaterialUpdate.Visible = false;
                if (OutCreateCount == 0 && OutApprovalcount == 0 && OutModifycount == 0 && OutCheckOutcount == 0 && OutCheckIncount == 0 && OutAcknowlegecount == 0 && OutwardView == 0 && OutwardUpdateReturnable==0)
                {
                    Outwardtab1.Visible = false;
                }
                else
                {
                    isOutward = true;
                    Outwardtab1.Visible = true;
                    outwardvisible = 1;
                }
                if (InwardCreate == 0 && InwardModify == 0 && InwardView == 0)
                {
                    Inwardtab2.Visible = false;
                }
                else
                {
                    isInward = true;
                    Inwardtab2.Visible = true;
                    inwardvisible = 2;
                }
                if (InwardCreate > 0)
                {
                    btnNavInwardMaterial.Visible = true;
                }
                if (InwardModify > 0)
                {
                    btnInWardMaterialUpdate.Visible = true;
                }
                if (isOutward == false && isInward == false)
                {
                    Response.Redirect("~/UserLogin.aspx");
                }
                if (outwardvisible == 1)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "outwardFun1", "myfunctionOutwardtab1();", true);
                }
                if (inwardvisible == 2)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "outwardFun1", "myfunctionInwardtab2();", true);
                }
            }
        }

        protected void btnPrintPassOutward_Click(object sender, EventArgs e)
        {
            if (ViewState["vwOutPrintcount"].ToString() != "0")
            {
                
                string filename2 = string.Empty;
                PrintOutwardMaterialReport(Convert.ToInt32(hdnOutwardPkID.Value), ref filename2);
                Literal1.Text = "<embed src=\"" + ResolveUrl("~/VMS/PdfFolder/" + filename2) + "#toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" height=\"620px\" />";
                upPrintHeader.Update();
                upPrintBody.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_Print').modal({ show: true, backdrop: 'static', keyboard: false });", true);
            }
        }
        public void PrintOutwardMaterialReport(int OutMaterialID, ref string filename2)
        {
            try
            {
                JQDatatableBO objJQDataTableBO = new JQDatatableBO();
                objJQDataTableBO.iMode = 0;
                objJQDataTableBO.pkid = Convert.ToInt32(OutMaterialID);
                objJQDataTableBO.iDisplayLength = 0;
                objJQDataTableBO.iDisplayStart = 0;
                objJQDataTableBO.iSortCol = 0;
                objJQDataTableBO.sSortDir = "";
                objJQDataTableBO.sSearch = "";
                objJQDataTableBO.EmpID = 0;
                objJQDataTableBO.Status = 0;
                OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
                DataTable dt = objOutwardMaterialBAL.GetOutwardMaterialList(objJQDataTableBO);
                if (dt.Rows.Count > 0)
                {
                    OutwardReport outwardRpt = new OutwardReport();
                    
                    ((XRTableCell)(outwardRpt.FindControl("tblMaterialType", false))).Text = dt.Rows[0]["OutwardMaterialType"].ToString();
                    ((XRTableCell)(outwardRpt.FindControl("tblMaterialName", false))).Text = dt.Rows[0]["MaterialName"].ToString();
                    ((XRTableCell)(outwardRpt.FindControl("tblQuantity", false))).Text = dt.Rows[0]["Quantityu"].ToString();
                    ((XRTableCell)(outwardRpt.FindControl("tblGPDCNO", false))).Text = dt.Rows[0]["GPorDCPNo"].ToString();
                    ((XRTableCell)(outwardRpt.FindControl("tblVehicleNo", false))).Text = dt.Rows[0]["VechileNo"].ToString();
                    ((XRTableCell)(outwardRpt.FindControl("tblDepartment", false))).Text = dt.Rows[0]["Department"].ToString();
                    ((XRTableCell)(outwardRpt.FindControl("tblGivenBy", false))).Text = dt.Rows[0]["GivenBy"].ToString();
                    ((XRTableCell)(outwardRpt.FindControl("tblNameOfParty", false))).Text = dt.Rows[0]["NameOfParty"].ToString();
                    ((XRTableCell)(outwardRpt.FindControl("tblMaterialOutDateTime", false))).Text = dt.Rows[0]["OutDateTime"].ToString();
                    ((XRTableCell)(outwardRpt.FindControl("tblDOM", false))).Text = dt.Rows[0]["DescriptionOfMaterial"].ToString();
                    ((XRTableCell)(outwardRpt.FindControl("tblRemarks", false))).Text = dt.Rows[0]["Remarks"].ToString();
                    ((XRTableCell)(outwardRpt.FindControl("tblPrintedby", false))).Text = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString();
                    outwardRpt.CreateDocument();

                    
                    string filename1 = Server.MapPath(@"~/Vms/PdfFolder/");
                    filename2 = DateTime.Now.ToString("ddMMyyyyhhmmss") + ".pdf";
                    outwardRpt.ExportToPdf(filename1 + filename2);
                   
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSREP1:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSREP1:" + strline + " " + strMsg);
            }
        }

        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}