﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/NestedVMS_Master.master" AutoEventWireup="true" CodeBehind="InwardMaterialCreation.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.MaterialRegister.InwardMaterialCreation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content2" ContentPlaceHolderID="VMSBody" runat="server">
     <script>
         window.onload = function () {
             getDate();
         };
         function getDate() {
             var dtTO = new Date();
             var Todate = document.getElementById("<%=txtINWDInDate.ClientID%>");
             Todate.value = formatDate(dtTO);
         }
         function formatDate(dateObj) {
             var m_names = new Array("Jan", "Feb", "Mar",
                 "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                 "Oct", "Nov", "Dec");
             var curr_date = dateObj.getDate();
             var curr_month = dateObj.getMonth();
             var curr_year = dateObj.getFullYear();
             var curr_min = dateObj.getMinutes();
             var curr_hr = dateObj.getHours();
             var curr_sc = dateObj.getSeconds();
             return curr_date + " " + m_names[curr_month] + " " + curr_year + " " + curr_hr + ":" + curr_min + ":" + curr_sc;
         }
    </script>
    <div style="margin-top: 10px;">
        <div class="row Panel_Frame" style="width: 900px; margin-left: 17%">
            <div id="header">
                <div class="col-sm-12 Panel_Header">
                    <div class="col-sm-6" style="padding-left: 5px">
                        <span class="Panel_Title">Inward Material Check In</span>
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div>
            <div id="body">
             
                <asp:UpdatePanel ID="UpdatePanel13" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="Row">
                            <div class="col-sm-6">
                                <asp:UpdatePanel ID="UpdatePanel28" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="form-horizontal" style="margin: 5px;">
                                            <div class="form-group" runat="server" id="div5">
                                                <label class="control-label col-sm-4" id="lblMaterialType" style="text-align: left; font-weight: lighter">Material Type<span class="mandatoryStar">*</span></label>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="ddlInWardTypePO" CssClass="form-control SearchDropDown" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlInWardTypePO_SelectedIndexChanged1">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div1">
                                        <label class="control-label col-sm-4" id="lblMaterialName" style="text-align: left; font-weight: lighter">Material Name<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtINWDMaterialName" runat="server" CssClass="form-control" placeholder="Enter Material Name"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div2">
                                        <label class="control-label col-sm-4" id="lblDes" style="text-align: left; font-weight: lighter">Description of the Material</label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtINWDDescriptionofMat" runat="server" CssClass="form-control" placeholder="Enter Material Descrption" TextMode="MultiLine" Rows="4" Style="resize: none"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div8">
                                        <label class="control-label col-sm-4" id="lblQuantity" style="text-align: left; font-weight: lighter">Quantity<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtINWDQuantity" runat="server" CssClass="form-control" placeholder="Quantity" onkeypress="return onlyDotsAndNumbers(this,event);"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-1" style="padding-left: 0px; padding-top: 5px">
                                            <label id="lblUnits" style="text-align: left; font-weight: lighter">Units<span class="mandatoryStar">*</span></label>
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlUnits" runat="server" CssClass="form-control SearchDropDown" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div6">
                                        <label class="control-label col-sm-4" id="lblGP" style="text-align: left; font-weight: lighter">DC/Invoice No</label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtINWDDcNo" runat="server" CssClass="form-control" placeholder="Enter DC/Invoice No"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div7">
                                        <label class="control-label col-sm-4" id="lblVechileNo" style="text-align: left; font-weight: lighter">Vechile No</label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtINWDVechileNo" runat="server" CssClass="form-control" placeholder="Enter Vechile No"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-6">

                                <asp:UpdatePanel ID="UpdatePanel29" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="form-horizontal" style="margin: 5px;">
                                            <div class="form-group" runat="server" id="div3">
                                                <label class="control-label col-sm-4" id="lblDepartment" style="text-align: left; font-weight: lighter">Department<span class="mandatoryStar">*</span></label>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="ddlDepartment3" runat="server" CssClass="form-control SearchDropDown" AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment3_SelectedIndexChanged" ></asp:DropDownList>
                                                     <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control" placeholder="Enter Department" maxlength="50" Visible="false"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlDepartment3" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="ddlReceivedBy" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>

                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div4">
                                        <label class="control-label col-sm-4" id="lblReceivedby" style="text-align: left; font-weight: lighter">Received By<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlReceivedBy" runat="server" CssClass="form-control SearchDropDown" AutoPostBack="true"></asp:DropDownList>
                                            <asp:TextBox ID="txtReceivedBy" runat="server" CssClass="form-control" placeholder="Enter ReceivedBy" onkeypress="return ValidateAlpha(event)" Visible="false"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div9">
                                        <label class="control-label col-sm-4" id="lblPartyName" style="text-align: left; font-weight: lighter">Name of the Supplier<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtINWDNameoftheSupplier" runat="server" CssClass="form-control" placeholder="Enter Supplier Name" TextMode="MultiLine" Rows="2" Style="resize: none"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="DIVINWDINDate">
                                        <label class="control-label col-sm-4" id="lblInDateTime" style="text-align: left; font-weight: lighter">InDateTime</label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtINWDInDate" runat="server" BackColor="White" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div10">
                                        <label class="control-label col-sm-4" id="lblAssessableValue" style="text-align: left; font-weight: lighter">Assessable Value</label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtINWDAssessable" runat="server" CssClass="form-control" placeholder="Enter Assessable value"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div11">
                                        <label class="control-label col-sm-4" id="lblRemarks" style="text-align: left; font-weight: lighter">Remarks</label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtINWDRemarks" runat="server" CssClass="form-control" placeholder="Enter Remarks" TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
            <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                <asp:Button ID="btnInWardMateralSubmit" Text="Submit" CssClass="button" runat="server" OnClick="btnInWardMateralSubmit_Click" OnClientClick="javascript:return Submitvaidate();" />
                <asp:Button ID="btnInWardMateralReset" Text="Reset" CssClass="button" runat="server" OnClick="btnInWardMateralReset_Click" CausesValidation="false" />
            </div>
                      

                        <script type="text/javascript">
                            function Submitvaidate() {
                            var MType = document.getElementById("<%=ddlInWardTypePO.ClientID%>").value;
                            var MName = document.getElementById("<%=txtINWDMaterialName.ClientID%>").value;
                            var MDept = document.getElementById("<%=ddlDepartment3.ClientID%>").value;
                            var ReceivedBy = document.getElementById("<%=ddlReceivedBy.ClientID%>").value;
                          
                            var Quantity = document.getElementById("<%=txtINWDQuantity.ClientID%>").value;
                            var SuppilerName = document.getElementById("<%=txtINWDNameoftheSupplier.ClientID%>").value;
                            var MUnits = document.getElementById("<%=ddlUnits.ClientID%>").value;
                            errors = [];
                            if (MType == 0) {
                                errors.push("Please Select Material Type.");
                            }
                            if (MName.trim() == "") {
                                errors.push("Please Enter Material Name.");
                            }
                            if (Quantity.trim() == "") {
                                errors.push("Please Enter Material Quantity.");
                            }
                            if (MUnits == 0) {
                                errors.push("Please Select Units of Material.");
                            }
                            
                            if (MDept == 0) {
                                errors.push("Please Select Department.");
                            }
                            if (ReceivedBy == 0) {
                                errors.push("Please Select Received By.");
                            }
                            if (SuppilerName.trim() == "") {
                                errors.push("Please Enter Name of the Supplier.");
                            }
                            if (errors.length > 0) {
                                custAlertMsg(errors.join("<br/>"), "error");
                                return false;
                            }
                            }
                        </script>

                        <script>
                            $(document).ready(function () {
                                $("#ddlDepartment3").select2();
                                $("#ddlReceivedBy").select2();

                            });
                            var prm = Sys.WebForms.PageRequestManager.getInstance();
                            prm.add_endRequest(function () {
                                $(document).ready(function () {
                                    $("#ddlDepartment3").select2();
                                    $("#ddlReceivedBy").select2();
                                });
                            });
                        </script>

                        <script>
                            $("#NavLnkMaterial").attr("class", "active");
                            $("#InwardMaterial").attr("class", "active");
                        </script>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
</div>
</asp:Content>
