﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/NestedVMS_Master.master" AutoEventWireup="true" CodeBehind="OutwardMaterialCreation.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.MaterialRegister.OutwardMaterialCreation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content2" ContentPlaceHolderID="VMSBody" runat="server">
     <script>
         window.onload = function () {
             getDate();
         };
         function getDate() {
             var dtTO = new Date();
             var Todate = document.getElementById("<%=txtOutDate.ClientID%>");
             Todate.value = formatDate(dtTO);
         }
         function formatDate(dateObj) {
             var m_names = new Array("Jan", "Feb", "Mar",
                 "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                 "Oct", "Nov", "Dec");
             var curr_date = dateObj.getDate();
             var curr_month = dateObj.getMonth();
             var curr_year = dateObj.getFullYear();
             var curr_min = dateObj.getMinutes();
             var curr_hr = dateObj.getHours();
             var curr_sc = dateObj.getSeconds();
             return curr_date + " " + m_names[curr_month] + " " + curr_year + " " + curr_hr + ":" + curr_min + ":" + curr_sc;
         }
    </script>

    <div style="margin-top: 10px;">
        <div class="row Panel_Frame" style="width: 900px; margin-left: 17%">
            <div id="header">
                <div class="col-sm-12 Panel_Header">
                    <div class="col-sm-6" style="padding-left: 5px">
                        <span class="Panel_Title">Outward Material Check Out</span>
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div>           

            <div class="Row">
                <div class="col-sm-6">
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div5">
                                    <label class="control-label col-sm-4" id="lblMaterialType" style="text-align: left; font-weight: lighter">Material Type<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:DropDownList ID="ddlMaterialType" CssClass="form-control SearchDropDown" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlMaterialType_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div1">
                            <label class="control-label col-sm-4" id="lblMaterialName" style="text-align: left; font-weight: lighter">Material Name<span class="mandatoryStar">*</span></label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtMaterialName" runat="server" CssClass="form-control" placeholder="Enter Material Name"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div2">
                            <label class="control-label col-sm-4" id="lblDes" style="text-align: left; font-weight: lighter">Description of the Material</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtDescriptionofMat" runat="server" CssClass="form-control" placeholder="Enter Material Descrption" TextMode="MultiLine" Rows="4"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div8">
                            <label class="control-label col-sm-4" id="lblQuantity" style="text-align: left; font-weight: lighter">Quantity<span class="mandatoryStar">*</span></label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control" placeholder="Quantity" onkeypress="return onlyDotsAndNumbers(this,event);"></asp:TextBox>
                            </div>
                            <div class="col-sm-1" style="padding-left: 0px; padding-top: 5px">
                                <label id="lblUnits" style="text-align: left; font-weight: lighter">Units<span class="mandatoryStar">*</span></label>
                            </div>
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlUnits" runat="server" CssClass="form-control SearchDropDown" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div6">
                            <label class="control-label col-sm-4" id="lblGP" style="text-align: left; font-weight: lighter">GP/DC No</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtDcNo" runat="server" CssClass="form-control" placeholder="Enter GP/DC No"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div7">
                            <label class="control-label col-sm-4" id="lblVechileNo" style="text-align: left; font-weight: lighter">Vechile No</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtVechileNo_MO" runat="server" CssClass="form-control" placeholder="Enter Vechile No"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <asp:UpdatePanel ID="upOutwardDept" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div3">
                                    <label class="control-label col-sm-4" id="lblDepartment" style="text-align: left; font-weight: lighter">Department<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:DropDownList ID="ddlOutwardDept" runat="server" CssClass="form-control SearchDropDown" AutoPostBack="true" OnSelectedIndexChanged="ddlOutwardDept_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="ddlOutwardDept"  />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div4">
                                    <label class="control-label col-sm-4" id="lblGivenby" style="text-align: left; font-weight: lighter">GivenBy<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:DropDownList ID="ddlGivenBy" runat="server" CssClass="form-control SearchDropDown" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlGivenBy" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div9">
                            <label class="control-label col-sm-4" id="lblPartyName" style="text-align: left; font-weight: lighter">Name of the Party<span class="mandatoryStar">*</span></label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtNameoftheParty" runat="server" CssClass="form-control" placeholder="Enter Name of the Party" TextMode="MultiLine" Rows="2"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div10">
                            <label class="control-label col-sm-4" id="lblOutDateTime" style="text-align: left; font-weight: lighter">Created On</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtOutDate" runat="server" CssClass="form-control" BackColor="White"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div12">
                            <label class="control-label col-sm-4" id="lblExDateTime" style="text-align: left; font-weight: lighter">Expected ReturnDate</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtExpectedDate" runat="server" CssClass="form-control" Style="margin-top: 2%" placeholder="Select ExpectedDate"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div11">
                            <label class="control-label col-sm-4" id="lblRemarks" style="text-align: left; font-weight: lighter">Remarks</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtRemarks_MO" runat="server" CssClass="form-control" placeholder="Enter Remarks" TextMode="MultiLine" Rows="3"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                <asp:Button ID="btnSubmit" Text="Submit" CssClass="button" runat="server" OnClick="btnSubmit_Click" OnClientClick="javascript:return Submitvaidate();" />
                <asp:Button ID="btnMOutWardReset" Text="Reset" CssClass="button" runat="server" OnClick="btnReset_Click1" CausesValidation="false" />
            </div>
           

            <script type="text/javascript">
                function Submitvaidate() {
                    var MType = document.getElementById("<%=ddlMaterialType.ClientID%>").value;
                    var MName = document.getElementById("<%=txtMaterialName.ClientID%>").value;
                    var MDept = document.getElementById("<%=ddlOutwardDept.ClientID%>").value;
                            var GivenBy = document.getElementById("<%=ddlGivenBy.ClientID%>").value;
                            var Quantity = document.getElementById("<%=txtQuantity.ClientID%>").value;
                var PartyName = document.getElementById("<%=txtNameoftheParty.ClientID%>").value;
                        var MUnits = document.getElementById("<%=ddlUnits.ClientID%>").value;
                        errors = [];
                        if (MType == 0) {
                            errors.push("Please Select Material Type.");
                        }
                        if (MName.trim() == "") {
                            errors.push("Please Enter Material Name.");
                        }
                        if (Quantity.trim() == "") {
                            errors.push("Please Enter Material Quantity.");
                        }
                        if (MUnits == 0) {
                            errors.push("Please Select Units of Material.");
                        }
                        if (MDept == 0) {
                            errors.push("Please Select Department.");
                        }
                        if (GivenBy == 0) {
                            errors.push("Please Select Given By.");
                        }
                        if (PartyName.trim() == "") {
                            errors.push("Please Enter Name of the Party.");
                        }
                        if (errors.length > 0) {
                            custAlertMsg(errors.join("<br/>"), "error");
                            return false;
                        }
                }
            </script>

            <script>
                $("#NavLnkMaterial").attr("class", "active");
                $("#OutwardMaterial").attr("class", "active");
    </script>
            <script>
                $(document).ready(function () {
                    $("#ddlOutwardDept").select2();
                    $("#ddlGivenBy").select2();

                });
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_endRequest(function () {
                    $(document).ready(function () {
                        $("#ddlOutwardDept").select2();
                        $("#ddlGivenBy").select2();
                    });
                });
            </script>            
            <script>
                        var currDate = new Date();
                        $(function () {
                            $('#<%=txtExpectedDate.ClientID%>').datetimepicker({
                                format: 'DD MMM YYYY',
                                minDate: new Date(),
                            });
                        });
            </script>
        </div>
    </div>
</asp:Content>
