﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/NestedVMS_Master.master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="MaterialRegister.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.MaterialRegister.MaterialRegister" %>

<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>
<%@ Register Src="~/UserControls/ElectronicSign.ascx" TagPrefix="uc1" TagName="ElectronicSign" %>

<asp:Content ID="Content1" ContentPlaceHolderID="VMSBody" runat="server">

    <asp:UpdatePanel ID="UpHdnFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnOutwardPkID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnInwardPkID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnMaterialCustom" runat="server" Value="" />
            <asp:HiddenField ID="hdnMaterialName" runat="server" Value="" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="hdnOutwardCheckout" runat="server" />
    <asp:HiddenField ID="hdfUserRole" runat="server" />
    <style>
        table tbody tr th {
            background-color: #07889a;
            color: #fff;
            text-align: center;
            padding: 4px;
            border-right: 1px solid #fff;
        }

        .dxreControl .dxreView {
            background-color: #E0E0E0;
            height: 392px !important;
        }

        .dxreView {
            position: relative;
            overflow: auto;
            padding: 0 10px;
            -webkit-overflow-scrolling: touch;
            -webkit-appearance: none;
            -ms-touch-action: pan-y pinch-zoom;
            touch-action: pan-y pinch-zoom;
            height: 455px !important;
        }

        .CreatorGrid thead tr th {
            background-color: #07889a;
            color: #fff;
            text-align: center;
            padding: 4px;
            border-right: 1px solid #fff;
        }

        table tbody tr td {
            text-align: center;
            height: 30px;
        }

        .CreatorGrid tbody tr td {
            text-align: center;
            height: 30px;
            width: 20px;
        }

        .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
            background-color: #07889a !important;
        }

        .datepicker {
            z-index: 3000 !important;
        }
    </style>

    <asp:Button ID="btnAdd" runat="server" class="float-right btn btn-signup_popup" PostBackUrl="~/VMS/VMS_HomePage.aspx" Text="Dashboard" />


    <div class="col-md-12 col-lg-12 col-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server">
        <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
    </div>
    <div class="col-md-12 col-lg-12 col-12 padding-none float-left" id="divMainContainer" runat="server" visible="true">
        <ul class="nav nav-tabs tab_grid">
            <li id="Outwardtab1" class="  nav-item" runat="server"><a class="Outward nav-link active" data-toggle="tab" href="#OutwardTabPanel">Outward Material</a></li>
            <li id="Inwardtab2" runat="server" class=" nav-item"><a class="InwardTab nav-link " data-toggle="tab" href="#InwardTabPanel">Inward Material</a></li>
        </ul>
        
        <div class="tab-content">
            <div id="OutwardTabPanel" class="NewDocumentTabBody tab-pane fade in active show">
                
                    <div class=" col-md-12 col-lg-12 col-sm-12 col-12 grid_panel_full float-left padding-none top">
                        <div class=" col-md-12 col-lg-12 col-sm-12 col-12 padding-none float-right ">
                            <div class="col-md-12 col-lg-12 col-sm-12 col-12  padding-none float-left">
                                <div class="  col-md-12 col-lg-12 col-sm-12 col-12 float-left grid_header ">
                                    <div class="col-lg-6 visitor_lists float-left padding-none">Outward Material List</div>
                                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="upNavCreateOutward">
                                        <ContentTemplate>
                                            <div class=" float-right">
                                                <asp:Button ID="btnNavCreateOutward" runat="server" CssClass="float-right  btn-signup_popup" Text="Create Outward Material" OnClick="btnNavCreateOutward_Click" />
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12 col-sm-12 col-12 grid_container top float-left  padding-none">

                            <table id="dtOutwardList" class="display dtOutwardListClass datatable_cust" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>S.No</th>
                                        <th>Material Type</th>
                                        <th>Material Name</th>
                                        <th>Quantity</th>
                                        <th>Department</th>
                                        <th>Given By</th>
                                        <th>Vechile No</th>
                                        <th>GP/DC NO</th>
                                        <th>Name of the Party</th>
                                        <th>Created On</th>
                                        <th>Status</th>
                                        <th>Edit</th>
                                        <th>History</th>
                                        <th>Print</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
         

            <div id="InwardTabPanel" class="NewDocumentTabBody1 tab-pane fade ">
                <div class=" col-md-12 col-lg-12 col-sm-12 col-12 padding-none float-left top">
                    <div class=" col-md-12 col-lg-12 col-sm-12 col-12 grid_panel_full float-left padding-none">
                        <div class=" col-md-12 col-lg-12 col-sm-12 col-12 padding-none float-right ">
                            <div class="col-md-12 col-lg-12 col-sm-12 col-12  padding-none">
                                <div class=" grid_header col-md-12 col-lg-12 col-sm-12 col-12 float-left ">
                                    Inward Material List
                                    <asp:UpdatePanel ID="upCreateInwardMaterial" runat="server" UpdateMode="Conditional" class="float-right">
                                        <ContentTemplate>
                                            <asp:Button type="button" class=" btn-signup_popup" ID="btnNavInwardMaterial" runat="server" OnClick="btnNavInwardMaterial_Click" Text="Create Inward Material" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                             
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12 col-sm-12 col-12 grid_container top float-left">
                            <table id="dtInwardList" class="dtInwardListClass display datatable_cust" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>S.No</th>
                                        <th>Category</th>
                                        <th>Material Name</th>
                                        <th>Quantity</th>
                                        <th>Department</th>
                                        <th>Received By</th>
                                        <th>Vechile No</th>
                                        <th>DCP Invoice No</th>
                                        <th>Supplier Name</th>
                                        <th>Check In Time</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnOutwardEdit_List" runat="server" OnClick="btnOutwardEdit_List_Click" Text="Button" />
                <asp:Button ID="btnInwardEdit_List" runat="server" OnClick="btnInwardEdit_List_Click" Text="Button" />
                <asp:Button ID="btnOutwardMaterialCheckOut" runat="server" Text="Button" OnClick="btnOutwardMaterialCheckOut_Click" />
                <asp:Button ID="btnOutwardHistory" runat="server" Text="Button" OnClick="btnOutwardHistory_Click" />
                <asp:Button ID="btnPrintPassOutward" runat="server" OnClick="btnPrintPassOutward_Click" Text="Button" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <!--------Comment History View-------------->
    <div id="myCommentHistory" class="modal department fade" role="dialog">
        <div class="modal-dialog" style="min-width:44%;">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                           
                            <span id="span1" runat="server">Comment History</span>
                             <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body" style="max-height: 600px; overflow-y: auto">
                            <div class=" history_grid" style="overflow-y: auto;">
                                <asp:GridView ID="gv_CommentHistory" runat="server" CssClass="table table-hover table-responsive" ClientIDMode="Static" EmptyDataText="No Records Found" AutoGenerateColumns="False" PagerSettings-Mode="NumericFirstLast" BackColor="White" BorderColor="#d2d2d2" BorderStyle="None" BorderWidth="1px">
                                    <Columns>
                                        <asp:TemplateField HeaderText="User" ItemStyle-CssClass="gridtextalign" ItemStyle-Width="118px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_EmpName" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role" ItemStyle-CssClass="gridtextalign" ItemStyle-Width="88px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_RoleName" runat="server" Text='<%# Eval("RoleName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action Date" ItemStyle-Width="158px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ActionDate" runat="server" Text='<%# Eval("ActionDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" ItemStyle-CssClass="gridtextalign" ItemStyle-Width="218px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Actions" runat="server" Text='<%# Eval("Actions") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments" ItemStyle-CssClass="gridtextalign">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Comments" runat="server" Text='<%# Eval("Comments") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                       

</div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!--------Comment History View End-------------->
     <!--Modal PopUp for Inward Material Register-->
    <div id="ModalInwardMaterial" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg" style="min-width: 80%">
            <!-- Modal content-->
            <div class="modal-content ">
                <div class="modal-header">
                    <h4 class="modal-title col-sm-8">Inward Material</h4>
                    <button type="button" class="close col-sm-4" style="text-align: right;" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel runat="server" ID="upInwardModal" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="col-sm-12 padding-none float-left">
                                <div class="col-sm-12 padding-none float-left">
                                    <asp:UpdatePanel ID="upMaterialType" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div12">
                                                <label class="control-label col-sm-12" id="lblInMaterialType">Category<span class="mandatoryStar">*</span></label>
                                                <div class="col-sm-12">
                                                    <asp:DropDownList ID="ddlInWardTypePO" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlInWardTypePO_SelectedIndexChanged1">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div13">
                                        <asp:UpdatePanel ID="upInMaterialName" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <label class="control-label col-sm-10" id="lblInMaterialName">Material Name<span class="mandatoryStar">*</span></label>
                                                <asp:Button ID="btnInMaterialName" runat="server" CssClass="btnnVisit float-right" Text="+" Font-Bold="true" OnClick="btnInMaterialName_Click" />
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtINWDMaterialName" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Material Name" Visible="false" AutoPostBack="true" OnTextChanged="txtINWDMaterialName_TextChanged"></asp:TextBox>
                                                    <asp:DropDownList ID="ddlInMaterialName" runat="server" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" TabIndex="1" AutoPostBack="true"></asp:DropDownList>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div20">
                                        <label class="control-label col-sm-12" id="lblInPartyName">Name of the Supplier<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtINWDNameoftheSupplier" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Supplier Name" MaxLength="100"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div15">
                                        <div class="col-sm-6 padding-none float-left">
                                            <label class="control-label col-sm-12" id="lblInQuantity">Quantity<span class="mandatoryStar">*</span></label>
                                            <div class="col-sm-12">
                                                <asp:TextBox ID="txtINWDQuantity" runat="server" CssClass="form-control login_input_sign_up" placeholder="Quantity" onkeypress="return onlyDotsAndNumbers(this,event);" MaxLength="18"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 padding-none float-left">
                                            <div class="col-sm-12">
                                                <label id="lblInUnits">Units<span class="mandatoryStar">*</span></label>
                                            </div>
                                            <div class="col-sm-12">
                                                <asp:DropDownList ID="ddlInwardUnits" runat="server" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" AutoPostBack="true"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div16">
                                        <label class="control-label col-sm-12" id="lblInGP">DC/Invoice No</label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtINWDDcNo" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter DC/Invoice No" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div17">
                                        <label class="control-label col-sm-12" id="lblInVechileNo">Vechile No</label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtINWDVechileNo" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Vechile No" MaxLength="20"></asp:TextBox>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group col-lg-4 padding-none float-left" runat="server" id="DIVINWDINDate">
                                        <label class="control-label col-sm-12" id="lblInDateTime">Check In Time</label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtINWDInDate" runat="server" CssClass="form-control login_input_sign_up"></asp:TextBox>
                                        </div>
                                    </div>
                                    <asp:UpdatePanel ID="UpdatePanel29" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>

                                            <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div18">
                                                <label class="control-label col-sm-12" id="lblInDepartment">Department<span class="mandatoryStar">*</span></label>
                                                <div class="col-sm-12">
                                                    <asp:DropDownList ID="ddlInwardDepartment" runat="server" CssClass="regulatory_dropdown_style col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none" data-size="5" data-live-search="true" AutoPostBack="true" OnSelectedIndexChanged="ddlInwardDepartment_SelectedIndexChanged"></asp:DropDownList>
                                                    <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Department" MaxLength="50" Visible="false"></asp:TextBox>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlInwardDepartment" EventName="SelectedIndexChanged" />
                                                                                   </Triggers>
                                    </asp:UpdatePanel>
                                 
                                    <asp:UpdatePanel ID="upddlRecievedBy" runat="server" UpdateMode="Always">
                                        <ContentTemplate>
                                            <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div19">
                                                <label class="control-label col-sm-12" id="lblInReceivedby">Received by<span class="mandatoryStar">*</span></label>
                                                <div class="col-sm-12">
                                                    <asp:DropDownList ID="ddlINRecievedBy" runat="server" CssClass="regulatory_dropdown_style col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none" data-size="5" data-live-search="true" AutoPostBack="true"></asp:DropDownList>
                                                    <asp:TextBox ID="txtINReceivedBy" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter ReceivedBy" onkeypress="return ValidateAlpha(event)" Visible="false"></asp:TextBox>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                       <div class="col-12 float-left padding-none">
                                    <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div22">
                                        <label class="control-label col-sm-12" id="lblInAssessableValue">Assessable Value</label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtINWDAssessable" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Assessable value"></asp:TextBox>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-sm-12 padding-none float-left">
                                        <div class="form-group col-lg-6 padding-none float-left" runat="server" id="div14">
                                            <label class="control-label col-sm-12" id="lblInDes">Material Description</label>
                                            <div class="col-sm-12">
                                                <asp:TextBox ID="txtINWDDescriptionofMat" runat="server" CssClass="form-control " placeholder="Enter Material Descrption" TextMode="MultiLine" Rows="2" Style="resize: none" MaxLength="300"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-6 padding-none float-left" runat="server" id="div25">
                                            <label class="control-label col-sm-12" id="lblInRemarks">Remarks</label>
                                            <div class="col-sm-12">
                                                <asp:TextBox ID="txtINWDRemarks" runat="server" CssClass="form-control" placeholder="Enter Remarks" TextMode="MultiLine" MaxLength="200"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12 padding-none float-left" runat="server" id="div_Invechile_comments">
                                        <label class="control-label col-sm-12" id="labelComments">Comments<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtINWDComments" runat="server" CssClass="form-control" placeholder="Enter Comments" TextMode="MultiLine" Rows="3" MaxLength="200"></asp:TextBox>
                                                                                   </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer top" style="border: none;">
                    <div class="col-sm-12 text-right">
                        <asp:UpdatePanel runat="server" ID="upInwardActionbtns" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Button ID="btnInWardMateralSubmit" Text="Submit" CssClass="btn btn-signup_popup" runat="server" OnClick="btnInWardMateralSubmit_Click" OnClientClick="javascript:return SubmitInwardvalidate();" />
                                <asp:Button ID="btnInWardMateralReset" Text="Reset" CssClass="btn btn-revert_popup" runat="server" OnClick="btnInWardMateralReset_Click" CausesValidation="false" />
                                <asp:Button ID="btnInWardMaterialUpdate" Text="Update" CssClass="btn btn-signup_popup" Style="margin-right: 5px !important" runat="server" OnClientClick="javascript:return UpdateInwardvalidate();" OnClick="btnInWardMaterialUpdate_Click" />
                                <input type="button" value="Cancel" class="btn btn-cancel_popup" data-dismiss="modal" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End ModalPopUp for Inward Material Register-->
    <!--Modal PopUp for Outward Material Register-->
    <div id="ModalOutwardMaterial" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg" style="min-width: 80%">
            <!-- Modal content-->
            <div class="modal-content ">
                <div class="modal-header">
                    <h4 class="modal-title col-sm-8">Outward Material</h4>
                    <button type="button" class="close col-sm-4" style="text-align: right;" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel runat="server" ID="upOutwardModel" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="col-sm-12 padding-none float-left">
                                <div class="col-sm-12 padding-none float-left">
                                    <asp:UpdatePanel runat="server" ID="upOutwardMaterialType" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div5">
                                                <label class="control-label col-sm-12" id="lblMaterialType">Material Type<span class="mandatoryStar">*</span></label>
                                                <div class="col-sm-12">
                                                    <asp:DropDownList ID="ddlMaterialType" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlMaterialType_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div class="form-group col-lg-4 padding-none float-left"  runat="server" id="div1">
                                        <asp:UpdatePanel ID="upMaterialName" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <label class="control-label col-sm-10" id="lblMaterialName">Material Name<span class="mandatoryStar">*</span></label>
                                                <asp:Button ID="btnMaterialName" runat="server" CssClass="btnnVisit float-right" Text="+" Font-Bold="true" OnClick="btnMaterialName_Click" />
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtMaterialName" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Material Name" Visible="false" AutoPostBack="true" OnTextChanged="txtMaterialName_TextChanged"></asp:TextBox>
                                                    <asp:DropDownList ID="ddlMaterialName" runat="server" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" TabIndex="1" AutoPostBack="true"></asp:DropDownList>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div9">
                                        <label class="control-label col-sm-12" id="lblPartyName">Name of the Party<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtNameoftheParty" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Name of the Party" MaxLength="100"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div8">
                                        <div class="col-sm-6 padding-none float-left">
                                            <label class="control-label col-sm-12" id="lblQuantity">Quantity<span class="mandatoryStar">*</span></label>
                                            <div class="col-sm-12">
                                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control login_input_sign_up" placeholder="Quantity" onkeypress="return onlyDotsAndNumbers(this,event);" MaxLength="18"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 padding-none float-left">
                                            <div class="col-sm-12">
                                                <label id="lblUnits">Units<span class="mandatoryStar">*</span></label>
                                            </div>
                                            <asp:UpdatePanel ID="upddlUnits" runat="server" UpdateMode="Always">
                                                <ContentTemplate>
                                                    <div class="col-sm-12">
                                                        <asp:DropDownList ID="ddlUnits" runat="server" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" AutoPostBack="true"></asp:DropDownList>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                    <asp:UpdatePanel ID="upOutwardDept" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div3">
                                                <label class="control-label col-sm-12" id="lblDepartment">Department<span class="mandatoryStar">*</span></label>
                                                <div class="col-sm-12">
                                                    <asp:DropDownList ID="ddlOutwardDept" runat="server" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" AutoPostBack="true" OnSelectedIndexChanged="ddlOutwardDept_SelectedIndexChanged"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                       
                                    </asp:UpdatePanel>
                                    <asp:UpdatePanel ID="upGivenBy" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div4">
                                                <label class="control-label col-sm-12" id="lblGivenby">Given By<span class="mandatoryStar">*</span></label>
                                                <div class="col-sm-12">
                                                    <asp:DropDownList ID="ddlGivenBy" runat="server" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" AutoPostBack="true"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlGivenBy" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <div class="col-12 float-left paddding-none">
                                    <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div6">
                                        <label class="control-label col-sm-12 padding-none" id="lblGP">GP/DC No</label>
                                        <div class="col-sm-12 padding-none">
                                            <asp:TextBox ID="txtDcNo" runat="server" CssClass="form-control login_input_sign_up" Enabled="false" placeholder="Enter GP/DC No" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div7">
                                        <label class="control-label col-sm-12" id="lblVechileNo">Vechile No</label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtVechileNo_MO" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Vechile No" MaxLength="20"></asp:TextBox>
                                        </div>
                                    </div>
                                    
                                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="upCreated_on">
                                        <ContentTemplate>
                                            <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div10">
                                                <label class="control-label col-sm-12 padding-none" id="lblOutDateTime">Created On</label>
                                                <div class="col-sm-12 padding-none">
                                                    <asp:TextBox ID="txtOutDate" runat="server" CssClass="form-control login_input_sign_up" Enabled="false"></asp:TextBox>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                        </div>
                                    <asp:UpdatePanel runat="server" ID="upExpectedDate" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group col-lg-4 padding-none float-left" runat="server" id="divExpectedDate">
                                                <label class="control-label col-sm-12" id="lblExDateTime">Expected Return Date</label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtExpectedDate" runat="server" CssClass="form-control login_input_sign_up" placeholder="Select ExpectedDate"></asp:TextBox>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div runat="server" id="DivForEdit" visible="false">
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div23">
                                                    <label class="control-label col-sm-12" id="lblRecDepartment">Received by Department<span class="mandatoryStar">*</span></label>
                                                    <div class="col-sm-12">
                                                        <asp:DropDownList ID="ddlReceivedByDept" runat="server" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" AutoPostBack="true" OnSelectedIndexChanged="ddlReceivedByDept_SelectedIndexChanged"></asp:DropDownList>
                                                        
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddlReceivedByDept" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <asp:UpdatePanel ID="upReceivedBy" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div24">
                                                    <label class="control-label col-sm-12" id="lblReceivedBy">Received by<span class="mandatoryStar">*</span></label>
                                                    <div class="col-sm-12">
                                                        <asp:DropDownList ID="ddlReceivedBy" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" runat="server" AutoPostBack="true"></asp:DropDownList>
                                                        
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddlReceivedBy" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                         <div class="col-12 float-left padding-none">
                                        <asp:UpdatePanel runat="server" ID="upReturnQuantity" UpdateMode="Conditional">
                                            <ContentTemplate>

                                                <div id="hideQuantityWhenZero" runat="server">
                                                    <div class="col-sm-2 padding-none float-left">
                                                        <label class="control-label col-sm-12" id="lblReQuantity">Returned Quantity</label>
                                                        <div class="col-sm-12">
                                                            <asp:TextBox ID="txtSumReturnedQuantity" runat="server" CssClass="form-control login_input_sign_up" onkeypress="return onlyDotsAndNumbers(this,event);" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2 padding-none float-left">
                                                        <label class="control-label col-sm-12" id="lblDetails">Details</label>
                                                        <div class="col-sm-12">
                                                            <asp:Button ID="btnReturnDetails" runat="server" Text="Return Details History" CssClass=" btn-revert_popup" OnClick="btnReturnDetails_Click" />
                                                        </div>
                                                    </div>
                                                </div>
                                               
                                                <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div21">
                                                    <div class="col-sm-6 padding-none float-left">
                                                        <label class="control-label col-sm-12" id="lblReturnQuantity">Returnable Quantity<span class="mandatoryStar">*</span></label>
                                                        <div class="col-sm-12">
                                                            <asp:TextBox ID="txtOutwardEditReturnQuantity" runat="server" CssClass="form-control login_input_sign_up" placeholder="Quantity" onkeypress="return onlyDotsAndNumbers(this,event);"></asp:TextBox>
                                                           
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 padding-none float-left">
                                                        <div class="col-sm-12">
                                                            <label id="lblReturnUnits">Units<span class="mandatoryStar">*</span></label>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <asp:DropDownList ID="ddlUnitsReturn" runat="server" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" AutoPostBack="true"></asp:DropDownList>
                                                           
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="form-group col-lg-4 padding-none float-left" runat="server" id="div26">
                                                    <label class="control-label col-sm-12" id="lblActualReturnDate">Actual Return Date</label>
                                                    <div class="col-sm-12">
                                                        <asp:TextBox ID="txtActualReturnDate" runat="server" CssClass="form-control login_input_sign_up" placeholder="Select Actual Return Date"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                             </div>
                                    </div>

                                    <div class="col-sm-12 padding-none float-left">
                                        <div class="form-group col-lg-6 padding-none float-left" runat="server" id="div2">
                                            <label class="control-label col-sm-12" id="lblDes">Material Description</label>
                                            <div class="col-sm-12">
                                                <asp:TextBox ID="txtDescriptionofMat" runat="server" CssClass="form-control" placeholder="Enter Material Descrption" TextMode="MultiLine" Rows="2" MaxLength="300"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-6 padding-none float-left" runat="server" id="div11">
                                            <label class="control-label col-sm-12" id="lblRemarks">Remarks</label>
                                            <div class="col-sm-12">
                                                <asp:TextBox ID="txtRemarks_MO" runat="server" CssClass="form-control" placeholder="Enter Remarks" TextMode="MultiLine" Rows="2" MaxLength="200"></asp:TextBox>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="form-group col-lg-12 padding-none float-left" runat="server" id="divOutwardComments" visible="false">
                                        <label class="control-label col-sm-12" id="lblComments">Comments<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtOutwardComments" runat="server" CssClass="form-control" placeholder="Enter Comments" TextMode="MultiLine" Rows="3" MaxLength="200"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
                <div class="modal-footer top" style="border: none;">
                    <div class="col-sm-12 top text-right">
                        <asp:UpdatePanel runat="server" ID="upOutwardActionbuttons" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Button ID="btnSubmit" Text="Submit" CssClass=" btn-signup_popup" runat="server" OnClick="btnSubmit_Click" OnClientClick="javascript:return Submitvaidate();" />
                                <asp:Button ID="btnMOutWardReset" Text="Reset" CssClass=" btn-revert_popup" runat="server" OnClick="btnMOutWardReset_Click" />
                                <asp:Button ID="btnOutwardUpdate" Text="Update"  CssClass=" btn-signup_popup" runat="server" OnClientClick="javascript:return OutwardUpdatevalidate();" OnClick="btnOutwardUpdate_Click" />
                                <asp:Button ID="btnApprove" Text="Approve" CssClass=" btn-signup_popup"  runat="server" OnClick="btnApprove_Click" Visible="false"/>
                                <asp:Button ID="btnReject" Text="Revert" CssClass=" btn-revert_popup" runat="server" OnClick="btnReject_Click" Visible="false" />
                                <asp:Button ID="btnStoreApprove" Text="Approve" CssClass=" btn-signup_popup" runat="server" OnClick="btnStoreApprove_Click" Visible="false" />
                                <asp:Button ID="btnStoreReject" Text="Revert" CssClass=" btn-revert_popup" runat="server" OnClick="btnStoreReject_Click" Visible="false" />
                                <input type="button" value="Cancel" class=" btn-cancel_popup" data-dismiss="modal" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End ModalPopUp for Outward Material Register-->

   

    

    <!--------Material Returned Quantity Details View-------------->
    <div id="QuantityHistory" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                           
                            <span id="span2" runat="server">Returned Material Quantity History</span>
                             <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body" style="max-height: 600px; overflow-y: auto">
                            <div class=" history_grid" style="overflow-y: auto;">
                                <asp:GridView ID="gv_ReturnedQuantityDetails" runat="server" CssClass="table table-hover table-responsive" ClientIDMode="Static" EmptyDataText="No Records Found" AutoGenerateColumns="False" PagerSettings-Mode="NumericFirstLast" BackColor="White" BorderColor="#d2d2d2" BorderStyle="None" BorderWidth="1px">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Received Department" ItemStyle-CssClass="gridtextalign" ItemStyle-Width="218px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_DeptName" runat="server" Text='<%# Eval("DepartmentName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Received By" ItemStyle-CssClass="gridtextalign" ItemStyle-Width="218px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ReceivedBy" runat="server" Text='<%# Eval("ReceivedBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Returned Quantity" ItemStyle-Width="168px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ReturnQuantity" runat="server" Text='<%# Eval("ReturnQuantity") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Units" ItemStyle-CssClass="gridtextalign" ItemStyle-Width="48px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ReturnUnits" runat="server" Text='<%# Eval("UnitName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Actual Return Date" ItemStyle-Width="158px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ActualReturnDate" runat="server" Text='<%# Eval("ActualReturnDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!--------Material Returned Quantity Details View End-------------->


    <uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />
    <uc1:ElectronicSign runat="server" ID="ElectronicSign" />



    <!-------- File VIEW-------------->
    <div id="myModal_lock" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 86%; height: 500px">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <asp:UpdatePanel ID="UpHeading" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <span id="span_VisitorTitle" runat="server"></span>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpViewDL" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="dvDocViewer" runat="server">
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <!-------- End File VIEW-------------->

    <script>
        function ConfirmAlertInwardUpdate() {
            custAlertMsg('Do you want to update', 'confirm', true);
            document.getElementById("<%=hdnMaterialCustom.ClientID%>").value = "Update";
        }
        function ConfirmAlertOutwardUpdate() {
            custAlertMsg('Do you want to update', 'confirm', true);
            document.getElementById("<%=hdnMaterialCustom.ClientID%>").value = "OutwardUpdate";
        }
    </script>

    <!--Submit Validation Visitor Inward Material Register-->
    <script type="text/javascript">
        function SubmitInwardvalidate() {
            var MType = document.getElementById("<%=ddlInWardTypePO.ClientID%>").value;
            var MDept = document.getElementById("<%=ddlInwardDepartment.ClientID%>").value;
            var ReceivedBy = document.getElementById("<%=ddlINRecievedBy.ClientID%>").value;
            var Quantity = document.getElementById("<%=txtINWDQuantity.ClientID%>").value;
            var NameOfSupplier = document.getElementById("<%=txtINWDNameoftheSupplier.ClientID%>").value;
            var MInUnits = document.getElementById("<%=ddlInwardUnits.ClientID%>").value;
            errors = [];
            if (MType == 0) {
                errors.push("Please select material type.");
            }
            var ddlMaterialName = $('#<%=ddlInMaterialName.ClientID%>');
            var isVisible5 = ddlMaterialName.is(':visible');
            if (isVisible5 == true) {
                var InMaterialName = document.getElementById("<%=ddlInMaterialName.ClientID%>").value;
                if (InMaterialName == 0) {
                    errors.push("Please select material name.");
                }
            }
            var txtMaterialName = $('#<%=txtINWDMaterialName.ClientID%>');
            var isVisible6 = txtMaterialName.is(':visible');
            if (isVisible6 == true) {
                var MaterialName2 = document.getElementById("<%=txtINWDMaterialName.ClientID%>").value;
                if (MaterialName2.trim() == "") {
                    errors.push("Please enter material name.");
                }
            }
            if (NameOfSupplier.trim() == "") {
                errors.push("Please enter name of the supplier.");
            }
            if (Quantity.trim() == "") {
                errors.push("Please enter material quantity.");
            }
            if (MInUnits == 0) {
                errors.push("Please select units of material.");
            }
            if (MDept == 0) {
                errors.push("Please select department.");
            }
            if (ReceivedBy == 0) {
                errors.push("Please select recieved by.");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
        }
    </script>
    <!--End Submit Validation Visitor Inward Material Register-->

    <!--Update Button Validation Inward Material-->
    <script type="text/javascript">
        function UpdateInwardvalidate() {
            var MType = document.getElementById("<%=ddlInWardTypePO.ClientID%>").value;
            var MName = document.getElementById("<%=ddlInMaterialName.ClientID%>").value;
            var MDept = document.getElementById("<%=ddlInwardDepartment.ClientID%>").value;
            var ReceivedBy = document.getElementById("<%=ddlINRecievedBy.ClientID%>").value;
            var Quantity = document.getElementById("<%=txtINWDQuantity.ClientID%>").value;
            var NameOfSupplier = document.getElementById("<%=txtINWDNameoftheSupplier.ClientID%>").value;
            var MInUnits = document.getElementById("<%=ddlInwardUnits.ClientID%>").value;
              var Comments = document.getElementById("<%=txtINWDComments.ClientID%>").value;
              errors = [];
              if (MType == 0) {
                  errors.push("Please select material type.");
              }
              if (MName == 0) {
                  errors.push("Please select material name.");
              }
              if (NameOfSupplier.trim() == "") {
                  errors.push("Please enter name of the supplier.");
              }
              if (Quantity.trim() == "") {
                  errors.push("Please enter material quantity.");
              }
              if (MInUnits == 0) {
                  errors.push("Please select units of material.");
              }
              if (MDept == 0) {
                  errors.push("Please select department.");
              }
              if (ReceivedBy == 0) {
                  errors.push("Please select recieved by.");
              }

              if (Comments.trim() == "") {
                  errors.push("Please enter modification reason in comments.");
              }
              if (errors.length > 0) {
                  custAlertMsg(errors.join("<br/>"), "error");
                  return false;
              }
              ConfirmAlertInwardUpdate();
        }
    </script>
    <!--Update Button Validation Inward Material-->

    <script>
        $("#NavLnkMaterial").attr("class", "active");
        $("#OutwardMaterial").attr("class", "active");
    </script>

    


    <!--Validation Visitor Outward Material Register-->
    <script type="text/javascript">
        function Submitvaidate() {
            var MType = document.getElementById("<%=ddlMaterialType.ClientID%>").value;
              var PartyName = document.getElementById("<%=txtNameoftheParty.ClientID%>").value;
              var Quantity = document.getElementById("<%=txtQuantity.ClientID%>").value;
              var MUnits = document.getElementById("<%=ddlUnits.ClientID%>").value;
              var MDept = document.getElementById("<%=ddlOutwardDept.ClientID%>").value;
              var GivenBy = document.getElementById("<%=ddlGivenBy.ClientID%>").value;
              errors = [];
              if (MType == 0) {
                  errors.push("Please select material type.");
              }
              var ddl_OutMaterialName = $('#<%=ddlMaterialName.ClientID%>');
              var isVisible = ddl_OutMaterialName.is(':visible');
              if (isVisible == true) {
                  var OutMaterialName = document.getElementById("<%=ddlMaterialName.ClientID%>").value;
                  if (OutMaterialName == 0) {
                      errors.push("Please select material name.");
                  }
              }
              var txt_OutMaterialName = $('#<%=txtMaterialName.ClientID%>');
              var isVisible1 = txt_OutMaterialName.is(':visible');
              if (isVisible1 == true) {
                  var OutMaterialName2 = document.getElementById("<%=txtMaterialName.ClientID%>").value;
                if (OutMaterialName2.trim() == "") {
                    errors.push("Please enter material name.");
                }
            }
            if (PartyName.trim() == "") {
                errors.push("Please enter name of the party.");
            }
            if (Quantity.trim() == "") {
                errors.push("Please enter material quantity.");
            }
            if (MUnits == 0) {
                errors.push("Please select units of material.");
            }
            if (MDept == 0) {
                errors.push("Please select department.");
            }
            if (GivenBy == 0) {
                errors.push("Please select given by.");
            }

            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            ConfirmAlertOutwardSubmit();
        }
    </script>
    <!--End Submit Button Validation Outward Material-->

    <!--Update Button Validation Outward Material-->
    <script type="text/javascript">
          function OutwardUpdatevalidate() {
              var MType = document.getElementById("<%=ddlMaterialType.ClientID%>").value;
              var MName = document.getElementById("<%=ddlMaterialName.ClientID%>").value;
              var MDept = document.getElementById("<%=ddlOutwardDept.ClientID%>").value;
             var GivenBy = document.getElementById("<%=ddlGivenBy.ClientID%>").value;
             var Quantity = document.getElementById("<%=txtQuantity.ClientID%>").value;
            var PartyName = document.getElementById("<%=txtNameoftheParty.ClientID%>").value;
            var MUnits = document.getElementById("<%=ddlUnits.ClientID%>").value;
            errors = [];
            if (MType == 0) {
                errors.push("Please select material type.");
            }
            if (MName == 0) {
                errors.push("Please select material name.");
            }
            if (Quantity.trim() == "") {
                errors.push("Please enter material qantity.");
            }
            if (MUnits == 0) {
                errors.push("Please select units of material.");
            }
            if (MDept == 0) {
                errors.push("Please select department.");
            }
            if (GivenBy == 0) {
                errors.push("Please select given by.");
            }
            if (PartyName.trim() == "") {
                errors.push("Please enter name of the party.");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
              
          }
    </script>
    <!--End Update Button Validation Outward Material-->

    <script>
          $("#NavLnkMaterial").attr("class", "active");
          $("#OutwardMaterial").attr("class", "active");
    </script>

    <!--Outward Material Register Datepicker ExpectedReturn Calendar-->
    <script>
         function myfunction() {
             $(function () {
                 $('#<%=txtExpectedDate.ClientID%>').datetimepicker({
                     format: 'DD MMM YYYY',
                     useCurrent: true,
                     minDate: new Date()
                });
            });

            $(function () {
                $('#<%=txtActualReturnDate.ClientID%>').datetimepicker({
                    format: 'DD MMM YYYY',
                    useCurrent: true,
                });
            });
         }

    </script>
    <!--End Outward Material Register Datepicker-->

    <!--Javascript Getdatetime now for CreateOn Outward Material Register-->
    <script>
        window.onload = function () {
            getOutwardDate();
        };
        function getOutwardDate() {
            var dtTO = new Date();
            var Todate = document.getElementById("<%=txtOutDate.ClientID%>");
            Todate.value = formatDate(dtTO);
        }
        function formatDate(dateObj) {
            var m_names = new Array("Jan", "Feb", "Mar",
                "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                "Oct", "Nov", "Dec");
            var curr_date = dateObj.getDate();
            var curr_month = dateObj.getMonth();
            var curr_year = dateObj.getFullYear();
            var curr_min = dateObj.getMinutes();
            var curr_hr = dateObj.getHours();
            var curr_sc = dateObj.getSeconds();
            return curr_date + " " + m_names[curr_month] + " " + curr_year + " " + curr_hr + ":" + curr_min + ":" + curr_sc;
        }
    </script>
    <!--End Javascript Getdatetime now for CreateOn Outward Material Register-->



    <script>
        function ViewOutwardMaterial_List(OutMaterialID) {
            $("#<%=hdnOutwardPkID.ClientID%>").val(OutMaterialID);
            $("#<%=btnOutwardEdit_List.ClientID%>").click();


        }
        function CheckOutMaterialOutward(OutMaterialID) {
            $("#<%=hdnOutwardCheckout.ClientID%>").val(OutMaterialID);
            $("#<%=btnOutwardMaterialCheckOut.ClientID%>").click();
        }
        function GetOutwardMaterial_History(OutMaterialID) {
        
            $("#<%=hdnOutwardPkID.ClientID%>").val(OutMaterialID);
            $("#<%=btnOutwardHistory.ClientID%>").click();
        }
        function ViewInwardMaterial_List(Sno) {
            $("#<%=hdnInwardPkID.ClientID%>").val(Sno);
            $("#<%=btnInwardEdit_List.ClientID%>").click();
        }
        function OutwardPassPrint(OutMaterialID) {
            $("#<%=hdnOutwardPkID.ClientID%>").val(OutMaterialID);
            $("#<%=btnPrintPassOutward.ClientID%>").click();
        }


    </script>
       <!-------- File VIEW Print in Model-------------->
        <div id="myModal_Print" class="modal department fade" role="dialog">
            <div class="modal-dialog modal-lg" style="min-width: 86%; height: 500px">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        
                      
                        <asp:UpdatePanel ID="upPrintHeader" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <span id="span3" runat="server">Outward Material Report</span>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <asp:UpdatePanel ID="upPrintBody" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                               <div >
                                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                            </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    <!--Outward Material Jquery Datatable-->
    <script>
        $('#dtOutwardList').wrap('<div class="dataTables_scroll" />');
        var otable = $('#dtOutwardList').DataTable({
            columns: [
                { 'data': 'OutMaterialID' },
                { 'data': 'RowNumber' },
                { 'data': 'MaterialType' },
                { 'data': 'MaterialName' },
                { 'data': 'MQuantity' },
                { 'data': 'DeptName' },
                { 'data': 'EmpName' },
                { 'data': 'VechileNo' },
                { 'data': 'GPorDCPNo' },
                { 'data': 'NameOfParty' },
                { 'data': 'OutDateTime' },
                { 'data': 'StatusName' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) {
                        var RoleID = '<%= Session["sesGMSRoleID"] %>';
                        if (RoleID == 19 && o.StatusName == "Approved by Store" && o.MaterialType == "Returnable") {
                            return '<a  class="edit_Training" title="Edit" href="#" onclick="ViewOutwardMaterial_List(' + o.OutMaterialID + ');">' + '' + '</a>';
                        }
                        else {
                            return '<a  class="Edit" title="Edit" href="#" onclick="ViewOutwardMaterial_List(' + o.OutMaterialID + ');">' + '' + '</a>';
                        }
                    }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="summary_latest" title="History"  onclick="GetOutwardMaterial_History(' + o.OutMaterialID + ');">' + '' + '</a>'; }
                },
            ],
           
            "lengthMenu": [[10, 15, 25, 100], [10, 15, 25, 100]],
            "aoColumnDefs": [{ "targets": [0,1, 7, 9], "visible": false, "searchable": false },
            {
                targets: [14], render: function (a, b, data, d) {
                    if (data.StatusName == 'Approved by Store') {
                        return '<a class="print" href="#" title="Print" onclick=OutwardPassPrint(' + data.OutMaterialID + ')></a>';
                    } else {
                        return '<a href="#" class="print" title="Print" onclick=OutwardPassPrint(' + data.OutMaterialID + ')></a>';
                    }
                    return '';
                }
            },],
            "order": [[1, "desc"]],
           
            'bAutoWidth': true,
            
            sServerMethod: 'Post',
            "sAjaxSource": '<%= ResolveUrl("/VMS/WebService/VMS_Service.asmx/GetOutwardList" )%>',
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push();
                $.ajax({
                    "dataType": 'json',    
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#dtOutwardList").show();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });

        function ReloadOutwardMaterialtable(isupdate) {
            debugger;
            $('#ModalOutwardMaterial').modal('hide');
            $('#usES_Modal').modal('hide');
            var role = document.getElementById("<%=hdfUserRole.ClientID%>").value;
            if (isupdate == 0)
                window.open("<%=ResolveUrl("~/VMS/MaterialRegister/MaterialRegister.aspx")%>", "_self");
            if (isupdate == 1)
                window.open("<%=ResolveUrl("~/VMS/MaterialRegister/MaterialRegister.aspx?c=")%>" + role, "_self");
        }
    </script>
    <!--End Outward Material Jquery Datatable-->

    <!--Inward Material Jquery Datatable-->
    <script>
        $('#dtInwardList').wrap('<div class="dataTables_scroll" />');

        var oAtable = $('#dtInwardList').DataTable({
            columns: [
                { 'data': 'Sno' },
                { 'data': 'RowNumber' },
                { 'data': 'MaterialType' },
                { 'data': 'MaterialName' },
                { 'data': 'MQuantity' },
                { 'data': 'DeptName' },
                { 'data': 'EmpName' },
                { 'data': 'VechileNo' },
                { 'data': 'DCP_InvoiceNo' },
                { 'data': 'SupplierName' },
                { 'data': 'InDateTime' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="Edit" title="Edit" data-target="#ModalInwardMaterial" data-toggle="modal" onclick="ViewInwardMaterial_List(' + o.Sno + ');">' + '' + '</a>'; }
                }
            ],
           
            "lengthMenu": [[10, 15, 25, 100], [10, 15, 25, 100]],
            "aoColumnDefs": [{ "targets": [0,1], "visible": false, "searchable": false }, { "className": "dt-body-left", "targets": [2,3,5,9] },
            { "className": "dt-body-right", "targets": [4, 5, 7, 8] },
            {
            },],
            "order": [[1, "desc"]],
           
           
            'bAutoWidth': true,
            
            "sServerMethod": "Post",
            "sAjaxSource": '<%= ResolveUrl("/VMS/WebService/VMS_Service.asmx/GetInwardList" )%>',
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push();
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#dtInwardList").show();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
        function ReloadInwardtable() {
           
            oAtable.ajax.reload();
            $('#ModalInwardMaterial').modal('hide');
        }
    </script>
    <script>
        $(document).ready(function () {
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var target = $(e.target).attr("href"); // activated tab
                $($.fn.dataTable.tables(true)).css('width', '100%');
                $($.fn.dataTable.tables(true)).DataTable().columns.adjust().draw();
            });
        });
          </script>
    <!--End Inward Material Jquery Datatable-->

    <!--Inward Material popup function start-->
    <script>
        function OpenModelInwardMaterial() {
            $('#<%=btnInWardMateralSubmit.ClientID %>').show();
            $('#<%=btnInWardMateralReset.ClientID%>').show();
            $('#<%=btnInWardMaterialUpdate.ClientID %>').hide();
            $('#<%=div_Invechile_comments.ClientID %>').hide();
            document.getElementById("<%=ddlInWardTypePO.ClientID%>").value = '';

            document.getElementById("<%=txtINWDDescriptionofMat.ClientID%>").value = '';
            document.getElementById("<%=txtINWDQuantity.ClientID%>").value = '';
            document.getElementById("<%=ddlInwardUnits.ClientID%>").value = '';
            document.getElementById("<%=txtINWDDcNo.ClientID%>").value = '';
                    document.getElementById("<%=txtINWDVechileNo.ClientID%>").value = '';
               document.getElementById("<%=ddlInwardDepartment.ClientID%>").value = '';
               document.getElementById("<%=ddlINRecievedBy.ClientID%>").value = '';
               document.getElementById("<%=txtINWDNameoftheSupplier.ClientID%>").value = '';
               document.getElementById("<%=txtINWDAssessable.ClientID%>").value = '';
               document.getElementById("<%=txtINWDRemarks.ClientID%>").value = '';
               $('#ModalInwardMaterial').modal('show');
        }
    </script>

    <script>
        function OpenModelInwardMaterialEdit() {
            $('#<%=btnInWardMaterialUpdate.ClientID %>').show();
            $('#ModalInwardMaterial').modal('show');
            $('#<%=div_Invechile_comments.ClientID %>').show();
            $('#<%=btnInWardMateralSubmit.ClientID %>').hide();
            $('#<%=btnInWardMateralReset.ClientID %>').hide();

        }
    </script>
    <!-- Inward Material popup function end-->

    <script>
        function OpenModelOutwardMaterial() {
            $('#<%=btnSubmit.ClientID %>').show();
            $('#<%=btnMOutWardReset.ClientID%>').show();
            $('#<%=btnOutwardUpdate.ClientID %>').hide();
            $('#<%=DivForEdit.ClientID%>').hide();
            document.getElementById("<%=ddlMaterialType.ClientID%>").value = '';
            document.getElementById("<%=txtDescriptionofMat.ClientID%>").value = '';
            document.getElementById("<%=txtQuantity.ClientID%>").value = '';
            document.getElementById("<%=ddlUnits.ClientID%>").value = '';
            document.getElementById("<%=txtDcNo.ClientID%>").value = '';
            document.getElementById("<%=txtVechileNo_MO.ClientID%>").value = '';
            document.getElementById("<%=ddlOutwardDept.ClientID%>").value = '';
            document.getElementById("<%=ddlGivenBy.ClientID%>").value = '';
            document.getElementById("<%=txtNameoftheParty.ClientID%>").value = '';
           
            document.getElementById("<%=txtRemarks_MO.ClientID%>").value = '';
            $('#<%=divOutwardComments.ClientID%>').hide();
               $('#ModalOutwardMaterial').modal('show');
        }
    </script>

    <script>
        function OpenModelOutwardMaterialEdit() {
            $('#<%=btnOutwardUpdate.ClientID %>').show();
            $('#ModalOutwardMaterial').modal('show');
            $('#<%=btnSubmit.ClientID %>').hide();
                    $('#<%=btnMOutWardReset.ClientID %>').hide();
        }
    </script>

    <%--Javascript Getdatetime now for Checkout Outward Material Register--%>
    <script>
        window.onload = function () {
            getCheckoutDate();
        };
        function getCheckoutDate() {
            var dtTO = new Date();
            var Todate = document.getElementById("<%=hdnOutwardCheckout.ClientID%>");
                    Todate.value = formatDate(dtTO);
                }
                function formatDate(dateObj) {
                    var m_names = new Array("Jan", "Feb", "Mar",
                        "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                        "Oct", "Nov", "Dec");
                    var curr_date = dateObj.getDate();
                    var curr_month = dateObj.getMonth();
                    var curr_year = dateObj.getFullYear();
                    var curr_min = dateObj.getMinutes();
                    var curr_hr = dateObj.getHours();
                    var curr_sc = dateObj.getSeconds();
                    return curr_date + " " + m_names[curr_month] + " " + curr_year + " " + curr_hr + ":" + curr_min + ":" + curr_sc;
                }
    </script>
    <!--End Javascript Getdatetime now for Checkout Outward Material Register-->

    <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
           if (prm != null) {
               prm.add_endRequest(function (sender, e) {
                   if (sender._postBackSettings.panelsToUpdate != null) {
                       $(".datepicker-orient-bottom").hide();
                   }
                   $('#<%=txtOutwardEditReturnQuantity.ClientID%>').blur(function () {
                       var NewRet_qty = $(this).val();
                       var total_qty = document.getElementById("<%=txtQuantity.ClientID%>").value;
                       if (parseFloat(NewRet_qty) > parseFloat(total_qty)) {
                           custAlertMsg("Return quantity should be less than or equal to " + total_qty, "error");
                           $(this).val("");
                           return false;
                       }
                       var SumReturnedQuantity = $('#<%=txtSumReturnedQuantity.ClientID%>');
                       var isVisible = SumReturnedQuantity.is(':visible');
                       if (isVisible == true) {
                           var totRet_qty = document.getElementById("<%=txtSumReturnedQuantity.ClientID%>").value;
                       }
                       var ActualQty = parseFloat(total_qty) - parseFloat(totRet_qty);
                       if (parseFloat(NewRet_qty) > parseFloat(ActualQty)) {
                           custAlertMsg("Return quantity should be less than or equal to " + ActualQty, "error");
                           $(this).val("");
                           return false;
                       } else {
                           // do something
                       }
                   });
               });
           };
    </script>
    <script>
           $(document).ready(function () {
               //Binding Code
               myfunction();
               Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
               function EndRequestHandler(sender, args) {
                   //Binding Code Again
                   myfunction();
               }
           });
    </script>

    <!--Javascript Getdatetime now for IndateTime Inward Material Register-->
    <script>
       
        function getInwardDate() {
            var dtTO1 = new Date();
            var Todate1 = document.getElementById("<%=txtINWDInDate.ClientID%>");
             Todate1.value = formatDate(dtTO1);
        }
        function formatDate(dateObj) {
            var m_names1 = new Array("Jan", "Feb", "Mar",
                "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                "Oct", "Nov", "Dec");
            var curr_date1 = dateObj.getDate();
            var curr_month1 = dateObj.getMonth();
            var curr_year1 = dateObj.getFullYear();
            var curr_min1 = dateObj.getMinutes();
            var curr_hr1 = dateObj.getHours();
            var curr_sc1 = dateObj.getSeconds();
            return curr_date1 + " " + m_names1[curr_month1] + " " + curr_year1 + " " + curr_hr1 + ":" + curr_min1 + ":" + curr_sc1;
        }
    </script>
    <!--End Javascript Getdatetime now for IndateTime Inward Material Register-->

    <script>
         function openElectronicSignModal() {
             $('#usES_Modal').modal({ backdrop: 'static', keyboard: false });
         }
    </script>
    <script>
                function HidePrintbtn() {
                    var table = $('#dtOutwardList').DataTable();
                    table.column(14).visible(false);
                }
    </script>
    <script>
                function myfunctionOutwardtab1() {
                    
                    $('.Outwardtab1').addClass('active');
                    $('.Inwardtab2').removeClass('active');
                    $('.NewDocumentTabBody').addClass('active');
                    $('.NewDocumentTabBody').addClass('show');
                    $('.NewDocumentTabBody1').removeClass('active');
                    $('.NewDocumentTabBody1').removeClass('show');
                   
                }
                function myfunctionInwardtab2() {
                    
                    $('.Outwardtab1').removeClass('active');
                    $('.Inwardtab2').addClass('active');
                    $('.NewDocumentTabBody').removeClass('active');
                    $('.NewDocumentTabBody').removeClass('show');
                    $('.NewDocumentTabBody1').addClass('active');
                    $('.NewDocumentTabBody1').addClass('show');
                    
                }
   </script>
</asp:Content>
