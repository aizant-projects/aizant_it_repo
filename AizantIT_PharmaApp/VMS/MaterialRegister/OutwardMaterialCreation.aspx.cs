﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using VMS_BAL;
using VMS_BO;
using AizantIT_PharmaApp.Common;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.VMS.MaterialRegister
{
    public partial class OutwardMaterialCreation : System.Web.UI.Page
    {
        OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
        MaterialBO objMaterialBO;
        DataTable dt;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    txtOutDate.Attributes.Add("readonly", "readonly");
                   txtDcNo.Attributes.Add("readonly", "readonly");
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            ViewState["EmpID"] = "0";
                            if (HelpClass.IsUserAuthenticated())
                            {
                                ViewState["EmpID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            }
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
               HelpClass.custAlertMsg(this, this.GetType(), "VMSOM6:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
               HelpClass.custAlertMsg(this, this.GetType(), "VMSOM6:" + strline + " " + strMsg, "error");
            }
        }

        private void InitializeThePage()
        {
            try
            {
                LoadDepartment();
                TimeDate();
                LoadUnits();
                BindMaterialType();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
               HelpClass.custAlertMsg(this, this.GetType(), "VMSOM7:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
               HelpClass.custAlertMsg(this, this.GetType(), "VMSOM7:" + strline + " " + strMsg, "error");
            }
        }

        private void TimeDate()
        {
            txtOutDate.Text = DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
        }

        private void LoadDepartment()
        {
            try
            {
                UMS_BAL obj = new UMS_BAL();
                DataTable dt = obj.getModuleWiseDepts(Convert.ToInt32(ViewState["EmpID"].ToString()), (int)Modules.VMS);
                ddlOutwardDept.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlOutwardDept.DataValueField = dt.Columns["DeptID"].ToString();
                ddlOutwardDept.DataSource = dt;
                ddlOutwardDept.DataBind();
                ddlOutwardDept.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
               HelpClass.custAlertMsg(this, this.GetType(), "VMSOM1:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
               HelpClass.custAlertMsg(this, this.GetType(), "VMSOM1:" + strline + " " + strMsg, "error");
            }
        }

        private void LoadUnits()
        {
            try
            {
                DataTable dtUnits = new DataTable();
                dtUnits = objOutwardMaterialBAL.BindUnits();
                ddlUnits.DataTextField = "UnitName";
                ddlUnits.DataValueField = "UnitID";
                ddlUnits.DataSource = dtUnits;
                ddlUnits.DataBind();
                ddlUnits.Items.Insert(0, new ListItem("", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
               HelpClass.custAlertMsg(this, this.GetType(), "VMSOM2:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
               HelpClass.custAlertMsg(this, this.GetType(), "VMSOM2:" + strline + " " + strMsg, "error");
            }
        }

        private void BindMaterialType()
        {
            try
            {
                DataTable dtMaterial = new DataTable();
                dtMaterial = objOutwardMaterialBAL.BindMaterialType();
                ddlMaterialType.DataTextField = "OutwardMaterialType";
                ddlMaterialType.DataValueField = "OutwardMaterialTypeID";
                ddlMaterialType.DataSource = dtMaterial;
                ddlMaterialType.DataBind();
                ddlMaterialType.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
               HelpClass.custAlertMsg(this, this.GetType(), "VMSOM3:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
               HelpClass.custAlertMsg(this, this.GetType(), "VMSOM3:" + strline + " " + strMsg,"error");
            }
        }

        //MaterialOutWard Submit Code
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtMaterialName.Text.Trim() == "" || txtQuantity.Text.Trim() == "" || txtNameoftheParty.Text.Trim() == "" || ddlUnits.SelectedValue == "0" || ddlOutwardDept.SelectedValue == "0" || ddlGivenBy.SelectedValue == "0" || ddlMaterialType.SelectedValue == "0")
                {
                   HelpClass.custAlertMsg(btnSubmit, btnSubmit.GetType(), "All fields represented with * are mandatory.","error");
                    return;
                }
                objMaterialBO = new MaterialBO();
                objMaterialBO.MaterialType = ddlMaterialType.SelectedValue.Trim();
                objMaterialBO.MaterialName = txtMaterialName.Text.Trim();
                objMaterialBO.DescriptionOfMaterial = txtDescriptionofMat.Text.Trim();
                objMaterialBO.Quantity = Convert.ToDecimal(txtQuantity.Text.Trim());
                objMaterialBO.UnitsID = Convert.ToInt32(ddlUnits.SelectedValue.Trim());
                objMaterialBO.Department = Convert.ToInt32(ddlOutwardDept.SelectedValue.Trim());
                objMaterialBO.GivenBy = Convert.ToInt32(ddlGivenBy.SelectedValue);
                objMaterialBO.VechileNo = txtVechileNo_MO.Text.Trim();
                objMaterialBO.GPorDCPNo = txtDcNo.Text.Trim();
                objMaterialBO.NameOfParty = txtNameoftheParty.Text.Trim();
                objMaterialBO.OutDateTime = txtOutDate.Text.Trim();
                objMaterialBO.Remarks = txtRemarks_MO.Text.Trim();
                objMaterialBO.CreatedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                objMaterialBO.CreatedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                objMaterialBO.ExpectedReturnDate = txtExpectedDate.Text.Trim();
                int OutwardID = objOutwardMaterialBAL.InsertMaterial(objMaterialBO);
                if (OutwardID > 0)
                {
                    OutwardMaterial_Reset();
                   HelpClass.custAlertMsg(btnSubmit, btnSubmit.GetType(), "Material Details Submitted Successfully.","success");
                    if (ViewState["vwCreateCount"].ToString() != "0")
                    {
                        string strURL = "http://" + Convert.ToString(ConfigurationManager.AppSettings["hostip"]) + "/VMS/Reports/outward.aspx?";
                        strURL += "OutwardID=";
                        strURL += Convert.ToString(OutwardID);
                        ScriptManager.RegisterStartupScript(btnSubmit, btnSubmit.GetType(), "openwindow", "window.open('" + strURL + "' ,'','height=600,width=1000,status=yes,location=no,toolbar=no,menubar=no,scrollbars=yes,target=_parent');", true);
                    }
                }
                else
                {
                   HelpClass.custAlertMsg(btnSubmit, btnSubmit.GetType(), "Material Details Submission Failed.");
                }

            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
               HelpClass.custAlertMsg(this, this.GetType(), "VMSOM4:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
               HelpClass.custAlertMsg(this, this.GetType(), "VMSOM4:" + strline + " " + strMsg,"error");
            }

        }

        protected void btnReset_Click1(object sender, EventArgs e)
        {
            ddlMaterialType.SelectedIndex = 0;
            OutwardMaterial_Reset();
        }

        //MaterialOutWard Reset Code
        private void OutwardMaterial_Reset()
        {
            txtDcNo.Text = txtDescriptionofMat.Text = txtMaterialName.Text = txtNameoftheParty.Text = txtOutDate.Text = txtQuantity.Text = txtRemarks_MO.Text =txtExpectedDate.Text= txtVechileNo_MO.Text = string.Empty;
            ddlOutwardDept.SelectedIndex = 0;
            ddlGivenBy.Items.Clear();
            ddlUnits.SelectedIndex = 0;
            TimeDate();
        }

        private void GivenBy()
        {
            try
            {
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                int DeptID = Convert.ToInt32(ddlOutwardDept.SelectedValue);
                DataTable dtEmp = objVisitorRegisterBAL.GetEmpData(DeptID);
                ddlGivenBy.DataSource = dtEmp;
                ddlGivenBy.DataTextField = "Name";
                ddlGivenBy.DataValueField = "EmpID";
                ddlGivenBy.DataBind();
                ddlGivenBy.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
               HelpClass.custAlertMsg(this, this.GetType(), "VMSOM5:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
               HelpClass.custAlertMsg(this, this.GetType(), "VMSOM5:" + strline + " " + strMsg, "error");
            }
        }

        protected void ddlOutwardDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlOutwardDept.SelectedIndex > 0)
            {
                GivenBy();
                AutoGenerateGP_DC_NO();
            }
        }

        protected void ddlMaterialType_SelectedIndexChanged(object sender, EventArgs e)
        {
            OutwardMaterial_Reset();
        }

        private void AutoGenerateGP_DC_NO()
        {
            try
            {
                MaterialBO objMaterialBO = new MaterialBO();
                objMaterialBO.DeptID = Convert.ToInt32(ddlOutwardDept.SelectedValue);
                txtDcNo.Text = objOutwardMaterialBAL.AutoGenerateGP_DC_NO(objMaterialBO).ToString();
                 
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
               HelpClass.custAlertMsg(this, this.GetType(), "VMSOM8:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
               HelpClass.custAlertMsg(this, this.GetType(), "VMSOM8:" + strline + " " + strMsg,"error");
            }
        }

        void screenAuthentication()
        {
            DataTable dtAthuntication = GmsHelpClass.GetRolePrivileges();
            if (dtAthuntication.Rows.Count > 0)
            {
                //modid=3,PrivilegeID=10

                int CreateCount = dtAthuntication.AsEnumerable()
           .Count(row => row.Field<int>("PrivilegeID") == 10);
                int Printcount = dtAthuntication.AsEnumerable()
   .Count(row => row.Field<int>("PrivilegeID") == 19);
                if (CreateCount == 0)
                    Response.Redirect("~/UserLogin.aspx");
                ViewState["vwCreateCount"] = Printcount.ToString();

            }
        }
    }
}

















