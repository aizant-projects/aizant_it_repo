﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using VMS_BAL;
using VMS_BO;
using AizantIT_PharmaApp.Common;
using System.Data.SqlClient;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.VMS.MaterialRegister
{
    public partial class OutwardMaterialList : System.Web.UI.Page
    {
        OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
        MaterialBO objMaterialBO;
        DataTable dt;

        protected void Page_Load(object sender, EventArgs e)
        {
            ElectronicSign.buttonClick += new EventHandler(ElectronicSign_buttonClick);
            try
            {
                if (!IsPostBack)
                {
                    ViewState["HODStatusApproveorReject"] = string.Empty;
                    ViewState["StoreStatusApproveorReject"] = string.Empty;
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            ViewState["EmpID"] = "0";
                            if (HelpClass.IsUserAuthenticated())
                            {
                                ViewState["EmpID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                ViewState["RoleID"] = Session["sesGMSRoleID"].ToString();
                            }
                            ViewState["roleuser"] = string.Empty;
                            
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOML10:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOML10:" + strline + " " + strMsg, "error");
            }
        }

        private void InitializeThePage()
        {
            try
            {
               
                LoadDepartment();
                LoadUnits();
                BindMaterialType();               
                if (Request.QueryString.Count > 0)
                {
                    ViewState["roleuser"] = HelpClass.Decrypt(HttpUtility.UrlDecode(Request.QueryString["c"]));
                    // div visible false true
                    GetPendingOutwardMaterial();
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOML11:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOML11:" + strline + " " + strMsg, "error");
            }
        }


        private void LoadDepartment()
        {
            try
            {
                UMS_BAL obj = new UMS_BAL();
                DataTable dt = obj.getModuleWiseDepts(Convert.ToInt32(ViewState["EmpID"].ToString()),(int)Modules.VMS);
                ddlDepartment4.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlDepartment4.DataValueField = dt.Columns["DeptID"].ToString();
                ddlDepartment4.DataSource = dt;
                ddlDepartment4.DataBind();
                ddlDepartment4.Items.Insert(0, new ListItem("All", "0"));

                ddlOutwardEditDepartment.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlOutwardEditDepartment.DataValueField = dt.Columns["DeptID"].ToString();
                ddlOutwardEditDepartment.DataSource = dt;
                ddlOutwardEditDepartment.DataBind();
                ddlOutwardEditDepartment.Items.Insert(0, new ListItem("-- Select --", "0"));

                ddlReceivedByDept.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlReceivedByDept.DataValueField = dt.Columns["DeptID"].ToString();
                ddlReceivedByDept.DataSource = dt;
                ddlReceivedByDept.DataBind();
                ddlReceivedByDept.Items.Insert(0, new ListItem("-- Select --", "0"));


            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOML1:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOML1:" + strline + " " + strMsg, "error");
            }
        }

        private void LoadUnits()
        {
            try
            {
                dt = new DataTable();
                dt = objOutwardMaterialBAL.BindUnits();
                ddlUnits.DataTextField = dt.Columns["UnitName"].ToString();
                ddlUnits.DataValueField = dt.Columns["UnitID"].ToString();
                ddlUnits.DataSource = dt;
                ddlUnits.DataBind();
                ddlUnits.Items.Insert(0, new ListItem("", "0"));

                ddlUnitsReturn.DataTextField = dt.Columns["UnitName"].ToString();
                ddlUnitsReturn.DataValueField = dt.Columns["UnitID"].ToString();
                ddlUnitsReturn.DataSource = dt;
                ddlUnitsReturn.DataBind();
                ddlUnitsReturn.Items.Insert(0, new ListItem("", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOML2:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOML2:" + strline + " " + strMsg, "error");
            }
        }

        private void BindMaterialType()
        {
            try
            {
                DataTable dtMaterial = new DataTable();
                dtMaterial = objOutwardMaterialBAL.BindMaterialType();
                ddlSearchMaterialType.DataTextField = "OutwardMaterialType";
                ddlSearchMaterialType.DataValueField = "OutwardMaterialTypeID";
                ddlSearchMaterialType.DataSource = dtMaterial;
                ddlSearchMaterialType.DataBind();
                ddlSearchMaterialType.Items.Insert(0, new ListItem("All", "0"));

                ddlOutwardEditMaterialType.DataTextField = "OutwardMaterialType";
                ddlOutwardEditMaterialType.DataValueField = "OutwardMaterialTypeID";
                ddlOutwardEditMaterialType.DataSource = dtMaterial;
                ddlOutwardEditMaterialType.DataBind();
                ddlOutwardEditMaterialType.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOML3:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOML3:" + strline + " " + strMsg, "error");
            }
        }

        protected void btnMaterialSearchFind_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(ViewState["roleuser"].ToString()))
                {
                    ViewState["HODStatusApproveorReject"] = string.Empty;
                    ViewState["StoreStatusApproveorReject"] = string.Empty;
                    DataTable dtDeptIds = new DataTable();
                    dtDeptIds.Columns.Add("DeptID", typeof(Int32));
                    objMaterialBO = new MaterialBO();
                    objMaterialBO.Sno = 0;
                    objMaterialBO.MaterialType = ddlSearchMaterialType.SelectedValue.Trim();
                    objMaterialBO.MaterialName = txtSearchMaterialName.Text.Trim();
                    if (ddlDepartment4.SelectedValue != "")
                    {
                        objMaterialBO.Department = Convert.ToInt32(ddlDepartment4.SelectedValue.Trim());
                    }
                    else
                    {
                        objMaterialBO.Department = 0;
                    }

                    objMaterialBO.GPorDCPNo = txtSearchMaterialGPDCInvoiceNO.Text.Trim();
                    objMaterialBO.NameOfParty = txtSearchMaterialNameoftheParty.Text.Trim();

                    if (!string.IsNullOrEmpty(txtSearchMaterialDateFrom.Text.Trim()))
                    {
                        objMaterialBO.OutDateTime = Convert.ToDateTime(txtSearchMaterialDateFrom.Text.Trim()).ToString("yyyyMMdd");
                    }
                    else
                        objMaterialBO.OutDateTime = "";
                    if (!string.IsNullOrEmpty(txtSearchMaterialDateTo.Text.Trim()))
                    {
                        objMaterialBO.InDateTime = Convert.ToDateTime(txtSearchMaterialDateTo.Text.Trim()).ToString("yyyyMMdd");
                    }
                    else
                        objMaterialBO.InDateTime = "";
                    if (ViewState["RoleID"].ToString() == "20")//Hod User
                    {
                        objMaterialBO.CreatedBy = 0;
                        foreach (ListItem liDept in ddlDepartment4.Items)
                        {
                            if (liDept.Value != "0")
                                dtDeptIds.Rows.Add(Convert.ToInt32(liDept.Value));
                        }
                        objMaterialBO.MultiDepartment = dtDeptIds;
                        GridViewOutwardMaterial.Columns[15].HeaderText = "VIEW";
                        ttlpopup.InnerHtml = "<b>View Outward Material</b>";
                    }
                    else if (ViewState["RoleID"].ToString() == "19")//Store User
                    {
                        objMaterialBO.CreatedBy = 0;
                        foreach (ListItem liDept in ddlDepartment4.Items)
                        {
                            if (liDept.Value != "0")
                                dtDeptIds.Rows.Add(Convert.ToInt32(liDept.Value));
                        }
                        objMaterialBO.MultiDepartment = dtDeptIds;
                        GridViewOutwardMaterial.Columns[15].HeaderText = "VIEW";
                        ttlpopup.InnerHtml = "<b>View Outward Material</b>";
                    }
                    else
                    {
                        foreach (ListItem liDept in ddlDepartment4.Items)
                        {
                            if (liDept.Value != "0")
                                dtDeptIds.Rows.Add(Convert.ToInt32(liDept.Value));
                        }
                        objMaterialBO.CreatedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                        objMaterialBO.MultiDepartment = dtDeptIds;
                       
                    }
                    objMaterialBO.TransStatus = 0;
                    dt = new DataTable();
                    dt = objOutwardMaterialBAL.SearchFilter(objMaterialBO);
                    GridViewOutwardMaterial.DataSource = dt;
                    GridViewOutwardMaterial.DataBind();
                    ViewState["dtOutward"] = dt;
                    GridViewOutwardMaterial.Visible = true;
                    ViewState["isDefaultData"] = true;
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOML4:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOML4:" + strline + " " + strMsg, "error");
            }
        }

        //Get latest Records on Page Load
        private void GetDataLatestWeek()
        {
            DataTable dtDeptIds = new DataTable();
            dtDeptIds.Columns.Add("DeptID", typeof(Int32));
            objMaterialBO = new MaterialBO();
            objMaterialBO.Sno = 0;
            objMaterialBO.MaterialType = string.Empty;
            objMaterialBO.MaterialName = string.Empty;
            objMaterialBO.Department = 0;
            objMaterialBO.GPorDCPNo = string.Empty;
            objMaterialBO.NameOfParty = string.Empty;
            DateTime date = DateTime.Now.AddDays(-7);
            objMaterialBO.OutDateTime = date.ToString("yyyyMMdd");
            objMaterialBO.InDateTime = DateTime.Now.ToString("yyyyMMdd");
            if (ViewState["RoleID"].ToString() == "20")//Hod User
            {
                objMaterialBO.CreatedBy = 0;
                foreach (ListItem liDept in ddlDepartment4.Items)
                {
                    if (liDept.Value != "0")
                        dtDeptIds.Rows.Add(Convert.ToInt32(liDept.Value));
                }
                objMaterialBO.MultiDepartment = dtDeptIds;
            }
            else if (ViewState["RoleID"].ToString() == "19")//Store User
            {
                objMaterialBO.CreatedBy = 0;
                foreach (ListItem liDept in ddlDepartment4.Items)
                {
                    if (liDept.Value != "0")
                        dtDeptIds.Rows.Add(Convert.ToInt32(liDept.Value));
                }
                objMaterialBO.MultiDepartment = dtDeptIds;
            }
            else
            {
                foreach (ListItem liDept in ddlDepartment4.Items)
                {
                    if (liDept.Value != "0")
                        dtDeptIds.Rows.Add(Convert.ToInt32(liDept.Value));
                }
                objMaterialBO.CreatedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                objMaterialBO.MultiDepartment = dtDeptIds;
            }
            dt = new DataTable();
            dt = objOutwardMaterialBAL.SearchFilter(objMaterialBO);
            GridViewOutwardMaterial.DataSource = dt;
            GridViewOutwardMaterial.DataBind();
            ViewState["dtOutward"] = dt;
            GridViewOutwardMaterial.Visible = true;
        }

        private void GetPendingOutwardMaterial()
        {
            if (!string.IsNullOrEmpty(ViewState["roleuser"].ToString()))
            {
                if (ViewState["roleuser"].ToString() == "mhod")
                {
                    DataTable dtDeptIds = new DataTable();
                    dtDeptIds.Columns.Add("DeptID", typeof(Int32));
                    objMaterialBO = new MaterialBO();
                    objMaterialBO.Sno = 0;
                    objMaterialBO.MaterialType = "";
                    objMaterialBO.MaterialName = "";                   
                    objMaterialBO.Department = 0;                  
                    objMaterialBO.GPorDCPNo = "";
                    objMaterialBO.NameOfParty = "";
                    objMaterialBO.OutDateTime = "";
                    objMaterialBO.InDateTime = "";
                    if (ViewState["RoleID"].ToString() == "20")//Hod User
                    {
                        objMaterialBO.CreatedBy = 0;
                        foreach (ListItem liDept in ddlDepartment4.Items)
                        {
                            if (liDept.Value != "0")
                                dtDeptIds.Rows.Add(Convert.ToInt32(liDept.Value));
                        }
                        objMaterialBO.MultiDepartment = dtDeptIds;
                        ttloutwardCheckin.InnerHtml = "<b>Pending Approval Outward Material List</b>";
                    }
                    else if (ViewState["RoleID"].ToString() == "19")//Store User
                    {
                        objMaterialBO.CreatedBy = 0;
                        foreach (ListItem liDept in ddlDepartment4.Items)
                        {
                            if (liDept.Value != "0")
                                dtDeptIds.Rows.Add(Convert.ToInt32(liDept.Value));
                        }
                        objMaterialBO.MultiDepartment = dtDeptIds;
                    }
                    else
                    {
                        foreach (ListItem liDept in ddlDepartment4.Items)
                        {
                            if (liDept.Value != "0")
                                dtDeptIds.Rows.Add(Convert.ToInt32(liDept.Value));
                        }
                        objMaterialBO.CreatedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                        objMaterialBO.MultiDepartment = dtDeptIds;
                    }
                    objMaterialBO.TransStatus = 1;
                    dt = new DataTable();
                    dt = objOutwardMaterialBAL.SearchFilter(objMaterialBO);
                    GridViewOutwardMaterial.DataSource = dt;
                    GridViewOutwardMaterial.DataBind();
                    divSearch.Visible = false;
                    divGridViewOM.Visible = true;
                    GridViewOutwardMaterial.Visible = true;
                }
                if (ViewState["roleuser"].ToString() == "muser")
                {
                    ttloutwardCheckin.InnerHtml = "<b>Outward Material List</b>";
                    DataTable dtDeptIds = new DataTable();
                    dtDeptIds.Columns.Add("DeptID", typeof(Int32));
                    objMaterialBO = new MaterialBO();
                    objMaterialBO.Sno = 0;
                    objMaterialBO.MaterialType = "";
                    objMaterialBO.MaterialName = "";
                    objMaterialBO.Department = 0;
                    objMaterialBO.GPorDCPNo = "";
                    objMaterialBO.NameOfParty = "";
                    objMaterialBO.OutDateTime = "";
                    objMaterialBO.InDateTime = "";
                    if (ViewState["RoleID"].ToString() == "20")//Hod User
                    {
                        objMaterialBO.CreatedBy = 0;
                        foreach (ListItem liDept in ddlDepartment4.Items)
                        {
                            if (liDept.Value != "0")
                                dtDeptIds.Rows.Add(Convert.ToInt32(liDept.Value));
                        }
                        objMaterialBO.MultiDepartment = dtDeptIds;
                    }
                    else if (ViewState["RoleID"].ToString() == "19")//Store User
                    {
                        objMaterialBO.CreatedBy = 0;
                        foreach (ListItem liDept in ddlDepartment4.Items)
                        {
                            if (liDept.Value != "0")
                                dtDeptIds.Rows.Add(Convert.ToInt32(liDept.Value));
                        }
                        objMaterialBO.MultiDepartment = dtDeptIds;
                    }
                    else
                    {
                        foreach (ListItem liDept in ddlDepartment4.Items)
                        {
                            if (liDept.Value != "0")
                                dtDeptIds.Rows.Add(Convert.ToInt32(liDept.Value));
                        }
                        objMaterialBO.CreatedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                        objMaterialBO.MultiDepartment = dtDeptIds;
                    }
                    objMaterialBO.TransStatus = 2;
                    dt = new DataTable();
                    dt = objOutwardMaterialBAL.SearchFilter(objMaterialBO);
                    GridViewOutwardMaterial.DataSource = dt;
                    GridViewOutwardMaterial.DataBind();
                    divSearch.Visible = false;
                    divGridViewOM.Visible = true;
                    GridViewOutwardMaterial.Visible = true;
                    ttloutwardCheckin.InnerHtml = "<b>Pending Reverted Outward Material List</b>";
                }
                if (ViewState["roleuser"].ToString() == "mstore")
                {
                    ttloutwardCheckin.InnerHtml = "<b>Pending Approval Outward Material List</b>";
                    DataTable dtDeptIds = new DataTable();
                    dtDeptIds.Columns.Add("DeptID", typeof(Int32));
                    objMaterialBO = new MaterialBO();
                    objMaterialBO.Sno = 0;
                    objMaterialBO.MaterialType = "";
                    objMaterialBO.MaterialName = "";
                    objMaterialBO.Department = 0;
                    objMaterialBO.GPorDCPNo = "";
                    objMaterialBO.NameOfParty = "";
                    objMaterialBO.OutDateTime = "";
                    objMaterialBO.InDateTime = "";
                    if (ViewState["RoleID"].ToString() == "20")//Hod User
                    {
                        objMaterialBO.CreatedBy = 0;
                        foreach (ListItem liDept in ddlDepartment4.Items)
                        {
                            if (liDept.Value != "0")
                                dtDeptIds.Rows.Add(Convert.ToInt32(liDept.Value));
                        }
                        objMaterialBO.MultiDepartment = dtDeptIds;
                    }
                    else if (ViewState["RoleID"].ToString() == "19")//Store User
                    {
                        objMaterialBO.CreatedBy = 0;
                        foreach (ListItem liDept in ddlDepartment4.Items)
                        {
                            if (liDept.Value != "0")
                                dtDeptIds.Rows.Add(Convert.ToInt32(liDept.Value));
                        }
                        objMaterialBO.MultiDepartment = dtDeptIds;
                    }
                    else
                    {
                        foreach (ListItem liDept in ddlDepartment4.Items)
                        {
                            if (liDept.Value != "0")
                                dtDeptIds.Rows.Add(Convert.ToInt32(liDept.Value));
                        }
                        objMaterialBO.CreatedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                        objMaterialBO.MultiDepartment = dtDeptIds;
                    }
                    objMaterialBO.TransStatus = 3;
                    dt = new DataTable();
                    dt = objOutwardMaterialBAL.SearchFilter(objMaterialBO);
                    GridViewOutwardMaterial.DataSource = dt;
                    GridViewOutwardMaterial.DataBind();
                    divSearch.Visible = false;
                    divGridViewOM.Visible = true;
                    GridViewOutwardMaterial.Visible = true;
                }
            }
        }

        protected void btnMaterialSearchReset_Click(object sender, EventArgs e)
        {
            SearchOutwardMaterial_Reset();
        }

        private void SearchOutwardMaterial_Reset()
        {
            txtSearchMaterialName.Text = txtSearchMaterialGPDCInvoiceNO.Text = txtSearchMaterialNameoftheParty.Text = txtSearchMaterialDateFrom.Text = txtSearchMaterialDateTo.Text = string.Empty;
            ddlDepartment4.SelectedIndex = 0;
            ddlSearchMaterialType.SelectedIndex = 0;
            GridViewOutwardMaterial.Visible = false;
        }

        //Outward Link Button Edit Code
        protected void lnkOutWard_Click(object sender, EventArgs e)
        {
            try
            {
                objMaterialBO = new MaterialBO();
                objMaterialBO.MaterialType = string.Empty;
                objMaterialBO.MaterialName = string.Empty;
                objMaterialBO.Department = 0;
                objMaterialBO.MultiDepartment = null;
                objMaterialBO.GPorDCPNo = string.Empty;
                objMaterialBO.NameOfParty = string.Empty;
                objMaterialBO.OutDateTime = string.Empty;
                objMaterialBO.InDateTime = string.Empty;
                objMaterialBO.CreatedBy = 0;
                LinkButton lnk = sender as LinkButton;
                GridViewRow gr = (GridViewRow)lnk.NamingContainer;
                string tempID = GridViewOutwardMaterial.DataKeys[gr.RowIndex].Value.ToString();
                objMaterialBO.Sno = Convert.ToInt32(tempID);
                ViewState["tempId1"] = tempID;
                DataTable dt = new DataTable();
                dt = objOutwardMaterialBAL.SearchFilter(objMaterialBO);
                if (dt.Rows.Count > 0)
                {
                    txtOutwardEditSno.Text = dt.Rows[0]["Sno"].ToString();
                    ddlOutwardEditMaterialType.SelectedValue = dt.Rows[0]["MaterialType"].ToString();
                    txtOutwardEditMaterialName.Text = dt.Rows[0]["MaterialName"].ToString();
                    txtDescriptionofMat.Text = dt.Rows[0]["DescriptionOfMaterial"].ToString();
                    txtOutwardEditQuantity.Text = dt.Rows[0]["Quantity"].ToString();
                    ddlUnits.SelectedValue = dt.Rows[0]["UnitID"].ToString();
                    txtOutwardEditNameoftheParty.Text = dt.Rows[0]["NameOfParty"].ToString();
                    txtOutwardEditVechileNo.Text = dt.Rows[0]["VechileNo"].ToString();
                    txtOutwardEditGPDCPNo.Text = dt.Rows[0]["GPorDCPNo"].ToString();
                    txtOutDate.Text = dt.Rows[0]["OutDateTime"].ToString();
                    txtOutwardEditedComments.Text = dt.Rows[0]["LastChangedByComments"].ToString();
                    txtExpReturnDate.Text = dt.Rows[0]["ExpectedReturnDate1"].ToString();
                    if (txtOutwardEditReturnQuantity == null)
                    {
                        dt.Rows[0]["Return_Quantity"] = 0;
                    }
                    else
                    {
                        txtOutwardEditReturnQuantity.Text = dt.Rows[0]["Return_Quantity"].ToString();
                    }
                    ddlUnitsReturn.SelectedValue = dt.Rows[0]["UnitID"].ToString();
                    txtRemarks_MO.Text = dt.Rows[0]["Remarks"].ToString();
                    ddlOutwardEditDepartment.SelectedValue = dt.Rows[0]["DeptID"].ToString();
                    GivenBy();
                    ddlGivenBy.SelectedValue = dt.Rows[0]["EmpID"].ToString();
                    ddlReceivedByDept.SelectedValue = dt.Rows[0]["ReceivedbyDeptID"].ToString();
                    ReceivedBy();
                    ddlReceivedBy.SelectedValue = dt.Rows[0]["ReceivedByID"].ToString();
                    ViewState["vwHodStatus"] = Convert.ToInt32(dt.Rows[0]["Status"].ToString());
                    ViewState["vwStoreStatus"] = Convert.ToInt32(dt.Rows[0]["StoreStatus"].ToString());
                    if(ViewState["roleuser"].ToString() != "mstore" && ViewState["roleuser"].ToString() != "mhod")
                    {
                        btnStoreApprove.Visible = false;
                        btnStoreReject.Visible = false;
                        btnApprove.Visible = false;
                        btnReject.Visible = false;
                        GridViewOutwardMaterial.Columns[15].HeaderText = "VIEW";
                        ttloutwardCheckin.InnerHtml = "<b>View Outward Material List</b>";
                    }
                    else
                    {
                        ttloutwardCheckin.InnerHtml = "<b>Pending Approval Outward Material List</b>";
                    }
                    if (Convert.ToInt32(ViewState["vwModifycount"]) > 0)
                    {
                        GridViewOutwardMaterial.Columns[15].HeaderText = "EDIT";
                        ttloutwardCheckin.InnerHtml = "<b>Outward Material List</b>";
                    }
                    if (ViewState["vwHodStatus"].ToString() == "3")// To Show Store Approval and Revert Button when Hod Status=3.
                    {
                        if(ViewState["roleuser"].ToString() == "mstore")
                        {
                            btnStoreApprove.Visible = true;
                            btnStoreReject.Visible = true;
                        }
                    }
                    ModalPopupOutward.Show();
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOML5:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOML5:" + strline + " " + strMsg, "error");
            }
        }

        //Outward Material BindData
        private void OutwardBind_Data()
        {
            try
            {
                if (Convert.ToBoolean(ViewState["isDefaultData"]) == false)
                {
                    GetPendingOutwardMaterial();
                }
                else if(Convert.ToBoolean(ViewState["isDefaultData"]) == true)
                {
                    GetDataLatestWeek();
                }
                else
                {
                    btnMaterialSearchFind_Click(null, null);
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOML6:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOML6:" + strline + " " + strMsg, "error");
            }
        }

        //Outward Material Update Code
        protected void btnOutwardUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                objMaterialBO = new MaterialBO();
                objMaterialBO.Sno = Convert.ToInt32(txtOutwardEditSno.Text.Trim());
                objMaterialBO.MaterialType = ddlOutwardEditMaterialType.SelectedValue.Trim();
                objMaterialBO.DescriptionOfMaterial = txtDescriptionofMat.Text.Trim();
                objMaterialBO.MaterialName = txtOutwardEditMaterialName.Text.Trim();
                objMaterialBO.Quantity = Convert.ToDecimal(txtOutwardEditQuantity.Text.Trim());
                objMaterialBO.UnitsID = Convert.ToInt32(ddlUnits.SelectedValue.Trim());
                objMaterialBO.NameOfParty = txtOutwardEditNameoftheParty.Text.Trim();
                objMaterialBO.Department = Convert.ToInt32(ddlOutwardEditDepartment.SelectedValue.Trim());
                objMaterialBO.VechileNo = txtOutwardEditVechileNo.Text.Trim();
                objMaterialBO.GPorDCPNo = txtOutwardEditGPDCPNo.Text.Trim();
                objMaterialBO.GivenBy = Convert.ToInt32(ddlGivenBy.SelectedValue.Trim());
                objMaterialBO.LastChangedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                objMaterialBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                                objMaterialBO.LastChangedComments = txtOutwardEditedComments.Text.Trim();
                objMaterialBO.Return_Quantity = Convert.ToDecimal(HelpClass.ToDecimal(txtOutwardEditReturnQuantity.Text.Trim()));
                objMaterialBO.UnitsID = Convert.ToInt32(ddlUnitsReturn.SelectedValue.Trim());
                objMaterialBO.Remarks = txtRemarks_MO.Text.Trim();
                objMaterialBO.ExpectedReturnDate = txtExpReturnDate.Text.Trim();
                objMaterialBO.ReceivedBy = Convert.ToInt32(ddlReceivedBy.SelectedValue.Trim());
                objMaterialBO.ReceivedDepartment = Convert.ToInt32(ddlReceivedByDept.SelectedValue.Trim());
                objMaterialBO.UpdateMode = 0;
                if (!string.IsNullOrEmpty(ViewState["roleuser"].ToString()))
                {
                    if (ViewState["roleuser"].ToString() == "mhod")
                    {
                        objMaterialBO.UpdateMode = 2;
                    }
                    else if(ViewState["roleuser"].ToString() == "mstore")
                    {
                        objMaterialBO.UpdateMode = 2;
                    }
                    else if(ViewState["roleuser"].ToString() == "muser")
                    {
                        objMaterialBO.UpdateMode = 2;
                    }
                }
                int c = objOutwardMaterialBAL.Update_OutwardMaterialEdit(objMaterialBO);
                if (c > 0)
                {
                    HelpClass.custAlertMsg(btnOutwardUpdate, btnOutwardUpdate.GetType(), "Outward Materials Details Updated Successfully.");
                }
                else
                {
                    HelpClass.custAlertMsg(btnOutwardUpdate, btnOutwardUpdate.GetType(), "Outward Materials Details Updation Failed.");
                }
                OutwardBind_Data();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOML7:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOML7:" + strline + " " + strMsg, "error");
            }
        }

        protected void GridViewOutwardMaterial_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewOutwardMaterial.PageIndex = e.NewPageIndex;
            if (ViewState["dtOutward"] != null)
            {
                GridViewOutwardMaterial.DataSource = (DataTable)ViewState["dtOutward"];
                GridViewOutwardMaterial.DataBind();
            }
        }

        protected void lnkCheckOut_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["vwCheckIncount"].ToString() != "0")
                {
                    LinkButton bt = (LinkButton)sender;
                    GridViewRow gr = (GridViewRow)bt.NamingContainer;
                    Label lbl_id = (Label)gr.FindControl("lblGOMSno");
                    LinkButton lnk = (LinkButton)gr.FindControl("lnkCheckOut");
                    Label lbl_MaterialName = (Label)gr.FindControl("lblGMaterialName");
                    objMaterialBO = new MaterialBO();
                    objMaterialBO.Sno = Convert.ToInt32(lbl_id.Text);
                    objMaterialBO.Material_ReturnDateAndTime = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                    objMaterialBO.MaterialName = lbl_MaterialName.Text;
                    int c = objOutwardMaterialBAL.CheckoutOutwardMaterial(objMaterialBO);
                    if (c > 0)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), " " + lbl_MaterialName.Text + "  Checked In Successfully.","success");
                        OutwardBind_Data();
                    }
                }
                else
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Access is denied","error");
                }
            }

            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOML8:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOML8:" + strline + " " + strMsg, "error");
            }
        }

        private void GivenBy()
        {
            try
            {
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                int DeptID = Convert.ToInt32(ddlOutwardEditDepartment.SelectedValue);
                DataTable dtGivenBy = objVisitorRegisterBAL.GetEmpData(DeptID);
                ddlGivenBy.DataSource = dtGivenBy;
                ddlGivenBy.DataTextField = "Name";
                ddlGivenBy.DataValueField = "EmpID";
                ddlGivenBy.DataBind();
                ddlGivenBy.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOML9:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOML9:" + strline + " " + strMsg, "error");
            }
        }

        private void ReceivedBy()
        {
            try
            {
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                int DeptID = Convert.ToInt32(ddlReceivedByDept.SelectedValue);
                DataTable dtReceivedBy = objVisitorRegisterBAL.GetEmpData(DeptID);
                ddlReceivedBy.DataSource = dtReceivedBy;
                ddlReceivedBy.DataTextField = "Name";
                ddlReceivedBy.DataValueField = "EmpID";
                ddlReceivedBy.DataBind();
                ddlReceivedBy.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOML9:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSOML9:" + strline + " " + strMsg, "error");
            }
        }

        protected void GridViewOutwardMaterial_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblIntime = (Label)(e.Row.FindControl("lblGVReturnDateTime"));
                LinkButton lbtnCheckin = (LinkButton)(e.Row.FindControl("lnkCheckOut"));
                if (lblIntime != null)
                {
                    if (lblIntime.Text.Length > 6)
                        lblIntime.Visible = true;
                    else
                        lbtnCheckin.Visible = true;
                }
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblOuttime = (Label)(e.Row.FindControl("lblGVReturnDateTime"));
                LinkButton lnkbtnEdit = (LinkButton)(e.Row.FindControl("lnkOutWard"));
                if (lblOuttime != null)
                {
                    if (lblOuttime.Text.Length > 5)
                    {
                        lnkbtnEdit.Visible = false;
                    }
                    else
                    {
                        lnkbtnEdit.Visible = true;
                    }
                }
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblmaterial = (Label)(e.Row.FindControl("lblGMaterialType"));
                LinkButton lbtnCheckin = (LinkButton)(e.Row.FindControl("lnkCheckOut"));
                if (lblmaterial != null)
                {
                    if (lblmaterial.Text.ToLower().Trim() == "non returnable")
                    {
                        lbtnCheckin.Visible = false;
                    }
                }
            }
        }

        protected void ddlOutwardEditDepartment_SelectedIndexChanged1(object sender, EventArgs e)
        {
            ModalPopupOutward.Show();
            GivenBy();
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            ViewState["HODStatusApproveorReject"] = "Approve";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "openElectronicSignModal();", true);
        }
       
        protected void ElectronicSign_buttonClick(object sender, EventArgs e)
        {
            bool b = ElectronicSign.IsPasswordValid;
            if (b == true)
            {
                if (ViewState["HODStatusApproveorReject"].ToString() != null)
                {
                    if (ViewState["HODStatusApproveorReject"].ToString().ToLower() == "approve")
                    {
                        ApproveMethod();
                    }
                }
                if (ViewState["HODStatusApproveorReject"].ToString() != null)
                {
                    if (ViewState["HODStatusApproveorReject"].ToString().ToLower() == "revert")
                    {
                        RevertMethod();
                    }
                }
                if (ViewState["StoreStatusApproveorReject"].ToString() != null)
                {
                    if (ViewState["StoreStatusApproveorReject"].ToString().ToLower() == "approve")
                    {
                        StoreApproveMethod();
                    }
                }
                if (ViewState["StoreStatusApproveorReject"].ToString() != null)
                {
                    if (ViewState["StoreStatusApproveorReject"].ToString().ToLower() == "revert")
                    {
                        StoreRevertMethod();
                    }
                }
            }
            else
            {
                JsMethod("Invalid Password");
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "openElectronicSignModal();", true);
                ModalPopupOutward.Show();
            }
        }

        private void ApproveMethod()
        {
            objMaterialBO = new MaterialBO();
            objMaterialBO.Sno = Convert.ToInt32(txtOutwardEditSno.Text.Trim());
            objMaterialBO.LastChangedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
            objMaterialBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            int c = objOutwardMaterialBAL.MaterialApproval(objMaterialBO);
            if (c > 0)
            {
                GetPendingOutwardMaterial();
                JsMethod("Approved");
            }
            else
            {
                JsMethod("Approved Failed");
            }
        }

        private void RevertMethod()
        {
            objMaterialBO = new MaterialBO();
            objMaterialBO.Sno = Convert.ToInt32(txtOutwardEditSno.Text.Trim());
            objMaterialBO.LastChangedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
            objMaterialBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            int c = objOutwardMaterialBAL.MaterialRejectOrCancel(objMaterialBO);
            if (c > 0)
            {
                GetPendingOutwardMaterial();
                JsMethod("Reverted");
            }
            else
            {
                JsMethod("Reverted Failed");
            }
        }
        private void JsMethod(string msg)
        {
            msg = msg.Replace("'", "");
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "alert", "alert('" + msg + "');", true);
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {          
            ViewState["HODStatusApproveorReject"] = "Revert";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "openElectronicSignModal();", true);
        }

        protected void btnStoreApprove_Click(object sender, EventArgs e)
        {
            ViewState["StoreStatusApproveorReject"] = "Approve";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "openElectronicSignModal();", true);
        }

        protected void btnStoreReject_Click(object sender, EventArgs e)
        {
           
            ViewState["StoreStatusApproveorReject"] = "Revert";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "openElectronicSignModal();", true);
        }

        private void StoreApproveMethod()
        {
            objMaterialBO = new MaterialBO();
            objMaterialBO.Sno = Convert.ToInt32(txtOutwardEditSno.Text.Trim());
            objMaterialBO.LastChangedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
            objMaterialBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            int c = objOutwardMaterialBAL.MaterialStoreApproval(objMaterialBO);
            if (c > 0)
            {
                GetPendingOutwardMaterial();
                JsMethod("Approved");
            }
            else
            {
                JsMethod("Approved Failed");
            }
        }

        private void StoreRevertMethod()
        {
            objMaterialBO = new MaterialBO();
            objMaterialBO.Sno = Convert.ToInt32(txtOutwardEditSno.Text.Trim());
            objMaterialBO.LastChangedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
            objMaterialBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            int c = objOutwardMaterialBAL.MaterialStoreRejectOrCancel(objMaterialBO);
            if (c > 0)
            {
                GetPendingOutwardMaterial();
                JsMethod("Reverted");
            }
            else
            {
                JsMethod("Reverted Failed");
            }
        }

        protected void ddlReceivedByDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReceivedBy();
        }

        void screenAuthentication()
        {
            DataTable dtAthuntication = GmsHelpClass.GetRolePrivileges();
            if (dtAthuntication.Rows.Count > 0)
            {
                //mod=3,PrivilegeID=10,11,12,13,14,15,19

                int CreateCount = dtAthuntication.AsEnumerable()
           .Count(row => row.Field<int>("PrivilegeID") == 10);
                ViewState["vwCreateCount"] = CreateCount.ToString();
                int Approvalcount = dtAthuntication.AsEnumerable()
         .Count(row => row.Field<int>("PrivilegeID") == 11);
                int Modifycount = dtAthuntication.AsEnumerable()
         .Count(row => row.Field<int>("PrivilegeID") == 12);
                ViewState["vwModifycount"] = Modifycount.ToString();
                int Acknowlegecount = dtAthuntication.AsEnumerable()
        .Count(row => row.Field<int>("PrivilegeID") == 13);
                ViewState["vwAcknowlegecount"] = Acknowlegecount.ToString();
                int CheckOutcount = dtAthuntication.AsEnumerable()
        .Count(row => row.Field<int>("PrivilegeID") == 14);
                int CheckIncount = dtAthuntication.AsEnumerable()
      .Count(row => row.Field<int>("PrivilegeID") == 15);
                int Printcount = dtAthuntication.AsEnumerable()
    .Count(row => row.Field<int>("PrivilegeID") == 19);
                if (CreateCount == 0 && Approvalcount == 0 && Modifycount == 0 && CheckOutcount == 0 && CheckIncount == 0 && Acknowlegecount == 0)
                    Response.Redirect("~/UserLogin.aspx");
              
                btnApprove.Visible = false;
                btnReject.Visible = false;
                btnStoreApprove.Visible = false;
                btnStoreReject.Visible = false;
                if (Approvalcount > 0)
                {
                    btnOutwardUpdate.Visible = true;
                    GridViewOutwardMaterial.Columns[15].HeaderText = "APPROVE";
                    btnApprove.Visible = true;
                    btnReject.Visible = true;
                    btnOutwardUpdate.Visible = false;
                    ttlpopup.InnerHtml = "<b>Approve Outward Material</b>";
                    
                }
                if (Acknowlegecount > 0)
                {
                    btnOutwardUpdate.Visible = false;
                    btnApprove.Visible = false;
                    btnReject.Visible = false;
                    GridViewOutwardMaterial.Columns[15].HeaderText = "APPROVE";
                    ttlpopup.InnerHtml = "<b> Approve Outward Material</b>";
                                   }
                if (Printcount == 0)
                    GridViewOutwardMaterial.Columns[13].Visible = false;
                ViewState["vwCheckIncount"] = CheckIncount.ToString();
                if (string.IsNullOrEmpty(ViewState["roleuser"].ToString()))
                {
                    if (ViewState["roleuser"].ToString() != "mstore" || ViewState["roleuser"].ToString() != "mhod")
                    {
                        ttloutwardCheckin.InnerHtml = "<b>View Outward Material List</b>";
                    }
                }
                if (Modifycount > 0)
                {
                    GridViewOutwardMaterial.Columns[15].HeaderText = "EDIT";
                    ttloutwardCheckin.InnerHtml = "<b>Outward Material List</b>";
                }
            }
        }
    }
}