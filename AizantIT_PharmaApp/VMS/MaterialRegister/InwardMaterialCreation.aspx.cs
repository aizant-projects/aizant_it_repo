﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using VMS_BAL;
using VMS_BO;
using AizantIT_PharmaApp.Common;
using System.Data;
using System.Data.SqlClient;

namespace AizantIT_PharmaApp.VMS.MaterialRegister
{
    public partial class InwardMaterialCreation : System.Web.UI.Page
    {
        InwardMaterialBAL objInwardMaterialBAL = new InwardMaterialBAL();
        MaterialBO objMaterialBO;
        DataTable dt;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                txtINWDInDate.Attributes.Add("readonly", "readonly");
                if (!IsPostBack)
                {
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            ViewState["EmpID"] = "0";
                            if (HelpClass.IsUserAuthenticated())
                            {
                                ViewState["EmpID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            }
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIM6:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIM6:" + strline + " " + strMsg,"error");
            }
        }

        private void InitializeThePage()
        {
            try
            {
                
                LoadDepartment();
                TimeDate();
                LoadUnits();
                BindMaterialType();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIM7:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIM7:" + strline + " " + strMsg,"error");
            }
        }
        private void TimeDate()
        {
            txtINWDInDate.Text = DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
        }

        private void LoadDepartment()
        {
            try
            {
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                dt = new DataTable();
                dt = objVisitorRegisterBAL.LoadDepartment();
                ddlDepartment3.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlDepartment3.DataValueField = dt.Columns["DeptID"].ToString();
                ddlDepartment3.DataSource = dt;
                ddlDepartment3.DataBind();
                ddlDepartment3.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIM1:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIM1:" + strline + " " + strMsg,"error");
            }
        }

        private void LoadUnits()
        {
            try
            {
                OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
                dt = new DataTable();
                dt = objOutwardMaterialBAL.BindUnits();
                ddlUnits.DataTextField = dt.Columns["UnitName"].ToString();
                ddlUnits.DataValueField = dt.Columns["UnitID"].ToString();
                ddlUnits.DataSource = dt;
                ddlUnits.DataBind();
                ddlUnits.Items.Insert(0, new ListItem("", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIM2:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIM2:" + strline + " " + strMsg,"error");
            }
        }

        //Inward Materials Submit Code
        protected void btnInWardMateralSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtINWDMaterialName.Text.Trim() == "" || txtINWDQuantity.Text.Trim() == "" || txtINWDNameoftheSupplier.Text.Trim() == "" || ddlUnits.SelectedValue == "0" || ddlDepartment3.SelectedValue == "0" || ddlReceivedBy.SelectedValue == "0" || ddlInWardTypePO.SelectedValue == "0")//  txtDepartment.Text.Trim() =="" || txtReceivedBy.Text.Trim() == ""
                {
                    HelpClass.custAlertMsg(btnInWardMateralSubmit, btnInWardMateralSubmit.GetType(), "All fields represented with * are mandatory","error");
                    return;
                }
                objMaterialBO = new MaterialBO();
                objMaterialBO.MaterialType = ddlInWardTypePO.SelectedValue.Trim();
                objMaterialBO.MaterialName = txtINWDMaterialName.Text.Trim();
                objMaterialBO.DescriptionOfMaterial = txtINWDDescriptionofMat.Text.Trim();
                objMaterialBO.Quantity = Convert.ToDecimal(txtINWDQuantity.Text.Trim());
                objMaterialBO.UnitsID = Convert.ToInt32(ddlUnits.SelectedValue.Trim());
                if (ddlDepartment3.SelectedIndex > 0)
                    objMaterialBO.Department = Convert.ToInt32(ddlDepartment3.SelectedValue.Trim());
                else
                    ddlDepartment3.SelectedValue = "0";
                if (ddlReceivedBy.SelectedIndex > 0)
                    objMaterialBO.ReceivedBy = Convert.ToInt32(ddlReceivedBy.SelectedValue.Trim());
                else
                    ddlReceivedBy.SelectedValue = "0";
                objMaterialBO.VechileNo = txtINWDVechileNo.Text.Trim();
                objMaterialBO.DCP_InvoiceNo = txtINWDDcNo.Text.Trim();
                objMaterialBO.AssessableValue = txtINWDAssessable.Text.Trim();
                
                objMaterialBO.InDateTime = txtINWDInDate.Text.Trim();
                objMaterialBO.Remarks = txtINWDRemarks.Text.Trim();
                objMaterialBO.SupplierName = txtINWDNameoftheSupplier.Text.Trim();
                objMaterialBO.CreatedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                objMaterialBO.CreatedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                objMaterialBO.DeptName = string.Empty;
                objMaterialBO.EmpName = string.Empty;
                int c = objInwardMaterialBAL.InsertInwardMaterial(objMaterialBO);
                if (c > 0)
                {
                    INWardMaterial_Reset();
                    ddlInWardTypePO.SelectedIndex = 0;
                    HelpClass.custAlertMsg(btnInWardMateralSubmit, btnInWardMateralSubmit.GetType(), "Material Details Submitted Successfully.","success");
                }
                else
                {
                    HelpClass.custAlertMsg(btnInWardMateralSubmit, btnInWardMateralSubmit.GetType(), "Material Details Submission Failed.","error");
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIM3:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIM3:" + strline + " " + strMsg,"error");
            }
        }

        protected void btnInWardMateralReset_Click(object sender, EventArgs e)
        {
            INWardMaterial_Reset();
            ddlInWardTypePO.SelectedIndex = 0;
        }

        //Inward Materials Reset Code
        private void INWardMaterial_Reset()
        {
            txtINWDDcNo.Text = txtINWDDescriptionofMat.Text = txtINWDMaterialName.Text = txtINWDQuantity.Text = txtINWDRemarks.Text = txtINWDVechileNo.Text = txtINWDAssessable.Text = txtINWDNameoftheSupplier.Text = string.Empty;
            ddlDepartment3.SelectedIndex = 0;
            ddlReceivedBy.Items.Clear();
            ddlUnits.SelectedIndex = 0;
            TimeDate();
            
        }

        private void Employee()
        {
            try
            {
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                int DeptID = Convert.ToInt32(ddlDepartment3.SelectedValue);
                DataTable dtEmp = objVisitorRegisterBAL.GetEmpData(DeptID);
                ddlReceivedBy.DataSource = dtEmp;
                ddlReceivedBy.DataTextField = "Name";
                ddlReceivedBy.DataValueField = "EmpID";
                ddlReceivedBy.DataBind();
                ddlReceivedBy.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIM4:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIM4:" + strline + " " + strMsg,"error");
            }
        }

        protected void ddlDepartment3_SelectedIndexChanged(object sender, EventArgs e)
        {
            Employee();
            UpdatePanel13.Update();
        }

        private void BindMaterialType()
        {
            try
            {
                DataTable dtMaterial = new DataTable();
                dtMaterial = objInwardMaterialBAL.BindMaterialType();
                ddlInWardTypePO.DataTextField = "InwardMaterialType";
                ddlInWardTypePO.DataValueField = "InwardMaterialTypeID";
                ddlInWardTypePO.DataSource = dtMaterial;
                ddlInWardTypePO.DataBind();
                ddlInWardTypePO.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIM5:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIM5:" + strline + " " + strMsg,"error");
            }
        }

        protected void ddlInWardTypePO_SelectedIndexChanged1(object sender, EventArgs e)
        {
            INWardMaterial_Reset();
            UpdatePanel13.Update();
        }

        void screenAuthentication()
        {
            DataTable dtAthuntication = GmsHelpClass.GetRolePrivileges();
            if (dtAthuntication.Rows.Count > 0)
            {
                
                int CreateCount = dtAthuntication.AsEnumerable()
           .Count(row => row.Field<int>("PrivilegeID") == 16);
                if (CreateCount == 0)
                    Response.Redirect("~/UserLogin.aspx");
            }
        }
    }
}