﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Data;
using VMS_BAL;
using VMS_BO;
using AizantIT_PharmaApp.Common;
using System.Data.SqlClient;

namespace AizantIT_PharmaApp.VMS.MaterialRegister
{
    public partial class InwardMaterialList : System.Web.UI.Page
    {
        InwardMaterialBAL objInwardMaterialBAL = new InwardMaterialBAL();
        MaterialBO objMaterialBO;
        DataTable dt;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            ViewState["EmpID"] = "0";
                            if (HelpClass.IsUserAuthenticated())
                            {
                                ViewState["EmpID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            }
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIML9:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIML9:" + strline + " " + strMsg,"error");
            }
        }

        private void InitializeThePage()
        {
            try
            {
                //screenAuthentication();
                LoadDepartment();
                LoadUnits();
                BindMaterialType();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIML10:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIML10:" + strline + " " + strMsg,"error");
            }
        }
        private void LoadDepartment()
        {
            try
            {
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                dt = new DataTable();
                dt = objVisitorRegisterBAL.LoadDepartment();
                ddlInWardDepartmentSearch.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlInWardDepartmentSearch.DataValueField = dt.Columns["DeptID"].ToString();
                ddlInWardDepartmentSearch.DataSource = dt;
                ddlInWardDepartmentSearch.DataBind();
                ddlInWardDepartmentSearch.Items.Insert(0, new ListItem("All", "0"));

                ddlDepartment3.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlDepartment3.DataValueField = dt.Columns["DeptID"].ToString();
                ddlDepartment3.DataSource = dt;
                ddlDepartment3.DataBind();
                ddlDepartment3.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIML1:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIML1:" + strline + " " + strMsg,"error");
            }
        }

        private void LoadUnits()
        {
            try
            {
                OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
                dt = new DataTable();
                dt = objOutwardMaterialBAL.BindUnits();
                ddlUnits.DataTextField = dt.Columns["UnitName"].ToString();
                ddlUnits.DataValueField = dt.Columns["UnitID"].ToString();
                ddlUnits.DataSource = dt;
                ddlUnits.DataBind();
                ddlUnits.Items.Insert(0, new ListItem("", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIML2:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIML2:" + strline + " " + strMsg,"error");
            }
        }

        //Inward Materials Search Find Code
        protected void btnInWardSearchFind_Click(object sender, EventArgs e)
        {
            try
            {
                objMaterialBO = new MaterialBO();
                objMaterialBO.MaterialType = ddlInwardMaterialType.SelectedValue.Trim();
                objMaterialBO.MaterialName = txtInWardSearchMaterialName.Text.Trim();
                if (ddlInWardDepartmentSearch.SelectedValue != "")
                {
                    objMaterialBO.Department = Convert.ToInt32(ddlInWardDepartmentSearch.SelectedValue.Trim());
                }
                else
                {
                    objMaterialBO.Department = 0;
                }
                               objMaterialBO.DCP_InvoiceNo = txtInWardGPDCSearch.Text.Trim();
                objMaterialBO.SupplierName = txtInWardSupplierSearch.Text.Trim();
                if (!string.IsNullOrEmpty(txtInWardDateFromSearch.Text.Trim()))
                { 
                objMaterialBO.InDateTime =Convert.ToDateTime(txtInWardDateFromSearch.Text.Trim()).ToString("yyyyMMdd");
                }
                else
                    objMaterialBO.InDateTime = txtInWardDateFromSearch.Text.Trim();
                if (!string.IsNullOrEmpty(txtInWardDateToSearch.Text.Trim()))
                {
                    objMaterialBO.OutDateTime = Convert.ToDateTime(txtInWardDateToSearch.Text.Trim()).ToString("yyyyMMdd");
                }
                else
                objMaterialBO.OutDateTime = txtInWardDateToSearch.Text.Trim();
                objMaterialBO.Sno = 0;
                dt = new DataTable();
                dt = objInwardMaterialBAL.SearchFilterInwardMaterial(objMaterialBO);
                GridViewSearchInWard.DataSource = dt;
                GridViewSearchInWard.DataBind();
                ViewState["dtInward"] = dt;
                GridViewSearchInWard.Visible = true;
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIML3:" + strmsgF,"errro");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIML3:" + strline + " " + strMsg,"error");
            }
        }

        protected void btnInWardSearchReset_Click(object sender, EventArgs e)
        {
            InWardMaterialsSearch_Reset();
            GridViewSearchInWard.Visible = false;
        }

        //Inward Materials Search Reset Code
        private void InWardMaterialsSearch_Reset()
        {
            txtInWardSearchMaterialName.Text = txtInWardGPDCSearch.Text = txtInWardSupplierSearch.Text = txtInWardDateFromSearch.Text = txtInWardDateToSearch.Text = string.Empty;
            ddlInWardDepartmentSearch.SelectedIndex = 0;
            ddlInwardMaterialType.SelectedIndex = 0;
            txtSearchDepartment.Text = "";           
        }

        //Inward Link Button Edit Code
        protected void LnkEdit_Click(object sender, EventArgs e)
        {
            try
            {
                objMaterialBO = new MaterialBO();
                objMaterialBO.MaterialType = "0";
                objMaterialBO.MaterialName = string.Empty;
                objMaterialBO.DeptName = string.Empty;
                objMaterialBO.DCP_InvoiceNo = string.Empty;
                objMaterialBO.SupplierName = string.Empty;
                objMaterialBO.InDateTime = string.Empty;
                objMaterialBO.OutDateTime = string.Empty;
                LinkButton lnk = sender as LinkButton;
                GridViewRow gr = (GridViewRow)lnk.NamingContainer;
                string tempID = GridViewSearchInWard.DataKeys[gr.RowIndex].Value.ToString();
                ViewState["tempId"] = tempID;
                dt = new DataTable();
                objMaterialBO.Sno = Convert.ToInt32(tempID);
                dt = objInwardMaterialBAL.SearchFilterInwardMaterial(objMaterialBO);
                txtEditSno.Text = dt.Rows[0]["Sno"].ToString();
                ddlInWardTypePO.SelectedValue = dt.Rows[0]["MaterialType"].ToString();
                txtINWDMaterialName.Text = dt.Rows[0]["MaterialName"].ToString();
                txtINWDDescriptionofMat.Text= dt.Rows[0]["DescriptionOfMaterial"].ToString();
                txtINWDQuantity.Text = dt.Rows[0]["Quantity"].ToString();
                ddlUnits.SelectedValue = dt.Rows[0]["UnitID"].ToString();
                txtINWDNameoftheSupplier.Text = dt.Rows[0]["SupplierName"].ToString();
                txtINWDVechileNo.Text = dt.Rows[0]["VechileNo"].ToString();
                txtINWDDcNo.Text = dt.Rows[0]["DCP_InvoiceNo"].ToString();
                txtINWDRemarks.Text = dt.Rows[0]["Remarks"].ToString();
                txtINWDInDate.Text = dt.Rows[0]["InDateTime"].ToString();
                txtINWDAssessable.Text= dt.Rows[0]["AssessableValue"].ToString();
                txtComments.Text = dt.Rows[0]["LastChangedByComments"].ToString();
                ddlDepartment3.SelectedValue = dt.Rows[0]["DeptID"].ToString();
                RecivedBy();
                ddlReceivedBy.SelectedValue = dt.Rows[0]["ReceivedByID"].ToString();
                modelpopup.Show();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIML4:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIML4:" + strline + " " + strMsg,"error");
            } 
        }

        private void InwardBind_Data()
        {
            try
            {
                btnInWardSearchFind_Click(null,null);
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIML5:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIML5:" + strline + " " + strMsg,"error");
            }
        }

        //Inward Material Update Code
        protected void btnInwardUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                objMaterialBO = new MaterialBO();
                objMaterialBO.Sno = Convert.ToInt32(txtEditSno.Text.Trim());
                objMaterialBO.MaterialType = ddlInWardTypePO.SelectedValue.Trim();
                objMaterialBO.MaterialName = txtINWDMaterialName.Text.Trim();
                objMaterialBO.Quantity = Convert.ToDecimal(HelpClass.ToDecimal(txtINWDQuantity.Text.Trim()));
                objMaterialBO.SupplierName = txtINWDNameoftheSupplier.Text.Trim();
                objMaterialBO.VechileNo = txtINWDVechileNo.Text.Trim();
                objMaterialBO.DCP_InvoiceNo = txtINWDDcNo.Text.Trim();
                objMaterialBO.LastChangedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                objMaterialBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                objMaterialBO.LastChangedComments = txtComments.Text.Trim();
                objMaterialBO.AssessableValue = txtINWDAssessable.Text.Trim();
                objMaterialBO.Remarks = txtINWDRemarks.Text.Trim();
                objMaterialBO.DescriptionOfMaterial = txtINWDDescriptionofMat.Text.Trim();
                objMaterialBO.UnitsID = Convert.ToInt32(ddlUnits.SelectedValue.Trim());
                if (ddlDepartment3.SelectedIndex > 0)
                    objMaterialBO.Department = Convert.ToInt32(ddlDepartment3.SelectedValue.Trim());
                else
                    ddlDepartment3.SelectedValue = "0";
                if (ddlReceivedBy.SelectedIndex > 0)
                    objMaterialBO.ReceivedBy = Convert.ToInt32(ddlReceivedBy.SelectedValue.Trim());
                else
                    ddlReceivedBy.SelectedValue = "0";
                objMaterialBO.DeptName = string.Empty;
                objMaterialBO.EmpName = string.Empty;
                int c = objInwardMaterialBAL.Update_InwardMaterialEdit(objMaterialBO);
                if (c > 0)
                {
                    HelpClass.custAlertMsg(btnInwardUpdate, btnInwardUpdate.GetType(), "Materials Details Updated Successfully.","success");
                }
                else
                {
                    HelpClass.custAlertMsg(btnInwardUpdate, btnInwardUpdate.GetType(), "Materials Details Updation Failed.","error");
                }
                InwardBind_Data();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIML6:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIML6:" + strline + " " + strMsg,"error");
            }
        }

        protected void GridViewSearchInWard_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewSearchInWard.PageIndex = e.NewPageIndex;
            if (ViewState["dtInward"]!= null)
            {
                GridViewSearchInWard.DataSource= (DataTable)ViewState["dtInward"];
                GridViewSearchInWard.DataBind();
            }
        }

        protected void ddlDepartment3_SelectedIndexChanged(object sender, EventArgs e)
        {
            RecivedBy();
        }

        
       
        private void RecivedBy()
        {
            try
            {
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                int DeptID = Convert.ToInt32(ddlDepartment3.SelectedValue);
                DataTable dt = objVisitorRegisterBAL.GetEmpData(DeptID);
                ddlReceivedBy.DataSource = dt;
                ddlReceivedBy.DataTextField = "Name";
                ddlReceivedBy.DataValueField = "EmpID";
                ddlReceivedBy.DataBind();
                ddlReceivedBy.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIML7:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIML7:" + strline + " " + strMsg,"error");
            }
        }

       

        private void BindMaterialType()
        {
            try
            {
                DataTable dtMaterial = new DataTable();
                dtMaterial = objInwardMaterialBAL.BindMaterialType();
                ddlInWardTypePO.DataTextField = "InwardMaterialType";
                ddlInWardTypePO.DataValueField = "InwardMaterialTypeID";
                ddlInWardTypePO.DataSource = dtMaterial;
                ddlInWardTypePO.DataBind();
                ddlInWardTypePO.Items.Insert(0, new ListItem("--Select--", "0"));

                ddlInwardMaterialType.DataTextField = "InwardMaterialType";
                ddlInwardMaterialType.DataValueField = "InwardMaterialTypeID";
                ddlInwardMaterialType.DataSource = dtMaterial;
                ddlInwardMaterialType.DataBind();
                ddlInwardMaterialType.Items.Insert(0, new ListItem("All", "0"));

            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIML8:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSIML8:" + strline + " " + strMsg,"error");
            }
        }

        void screenAuthentication()
        {
            DataTable dtAthuntication = GmsHelpClass.GetRolePrivileges();
            if (dtAthuntication.Rows.Count > 0)
            {
                //modid=3,PrivilegeID=16,17
                int Createcount = dtAthuntication.AsEnumerable()
        .Count(row => row.Field<int>("PrivilegeID") == 16);
                int Modifycount = dtAthuntication.AsEnumerable()
           .Count(row => row.Field<int>("PrivilegeID") == 17);
                if (Modifycount == 0 && Createcount == 0)
                    Response.Redirect("~/UserLogin.aspx");
                if (Modifycount == 0)
                    btnInwardUpdate.Visible = false;
            }
        }
    }
}