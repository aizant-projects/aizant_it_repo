﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/VMS_Master.Master" AutoEventWireup="true" CodeBehind="SnacksSupplyList.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.SupplyRegister.SnacksSupplyList" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        th {
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="margin-top: 10px;">
        <div class="row Panel_Frame" style="width: 1100px; margin-left: 10%">
            <div id="header">
                <div class="col-sm-12 Panel_Header">
                    <div class="col-sm-6" style="padding-left: 5px">
                        <span class="Panel_Title">Supply Register Check Out</span>
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div>
            <div id="body">
              
                <div class="Row">
                    <div class="col-sm-6">
                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div5">
                                        <label class="control-label col-sm-4" id="lblSupplyType" style="text-align: left; font-weight: lighter">Supply Type<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlSupplyType" runat="server" CssClass="form-control SearchDropDown" AutoPostBack="true" OnSelectedIndexChanged="ddlSupplyType_SelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                             <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlSupplyType" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>

                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div1">
                                        <label class="control-label col-sm-4" id="labelDept" style="text-align: left; font-weight: lighter">Department</label>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="form-control SearchDropDown" AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlDepartment" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>

                        <asp:UpdatePanel runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div2">
                                        <label class="control-label col-sm-4" id="labelEmployeeName" style="text-align: left; font-weight: lighter">Employee Name</label>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlEmp" runat="server" AutoPostBack="true" CssClass="form-control SearchDropDown"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlEmp" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div4">
                                <label class="control-label col-sm-4" id="labelFromDate" style="text-align: left; font-weight: lighter">From Date</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtDateFrom" runat="server" CssClass="form-control" placeholder="Select From Date"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div6">
                                <label class="control-label col-sm-4" id="label3" style="text-align: left; font-weight: lighter">To Date</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtDateTo" runat="server" CssClass="form-control" placeholder="Select To Date"></asp:TextBox><br />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                    <asp:Button ID="btnSupplyFind" Text="Find" CssClass="button" runat="server" OnClick="btnSupplyFind_Click" OnClientClick="javascript:return Submitvaidate();" ValidationGroup="ab" />
                    <asp:Button ID="btnSupplyReset" Text="Reset" CssClass="button" runat="server" OnClick="btnSupplyReset_Click" CausesValidation="false" />
                </div>
                <div class="clearfix">
                </div>

                <asp:GridView ID="SnacksGridView" runat="server" DataKeyNames="SNo" PagerStyle-CssClass="GridPager" Style="text-align: center" CssClass="table table-bordered table-inverse" Font-Size="12px" HeaderStyle-Wrap="false" AutoGenerateColumns="False" HeaderStyle-Width="5%" EmptyDataText="Currently No Records Found" CellPadding="3" HeaderStyle-HorizontalAlign="Center" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" AllowPaging="True" ForeColor="#66CCFF" OnPageIndexChanging="SnacksGridView_PageIndexChanging">
                    <EmptyDataRowStyle ForeColor="#FF3300" HorizontalAlign="Center" />
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <HeaderStyle BackColor="#748CB2" Font-Bold="True" ForeColor="White" Height="25px" />
                    <PagerStyle ForeColor="White" HorizontalAlign="Center" BackColor="White" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#0c99f0" />
                    <SortedAscendingHeaderStyle BackColor="#0c99f0" />
                    <SortedDescendingCellStyle BackColor="#0c99f0" />
                    <SortedDescendingHeaderStyle BackColor="#0c99f0" />
                    <Columns>
                        <asp:TemplateField HeaderText="SNO" SortExpression="SNo">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtGVSno" runat="server" Text='<%# Bind("SNo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGVSno" runat="server" Text='<%# Bind("SNo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                        <asp:TemplateField HeaderText="EMPLOYEE NAME" SortExpression="EmpName">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtGVEmployeeName" runat="server" Text='<%# Bind("EmpName") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGVEmployeeName" runat="server" Text='<%# Bind("EmpName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DEPARTMENT" SortExpression="Department">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtGVSDepartment" runat="server" Text='<%# Bind("Department") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGVDepartment" runat="server" Text='<%# Bind("Department") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ISSUED ITEMS" SortExpression="IssuedItems">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtGVSIssuedItems" runat="server" Text='<%# Bind("IssuedItems") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGVIssuedItems" runat="server" Text='<%# Bind("IssuedItems") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ISSUED DATE TIME" SortExpression="InDateTime">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtGVInDateTime" runat="server" Text='<%# Bind("IssuedOn") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGVInDateTime" runat="server" Text='<%# Bind("IssuedOn") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EDIT">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkSnacksIssue" runat="server" CssClass="glyphicon glyphicon-edit GV_Edit_Icon" OnClick="LinkSnacksIssue_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

                <!--Panel to add new record-->
                <asp:ModalPopupExtender ID="ModalPopupSnacksRegister" runat="server" TargetControlID="btnSnacksUpdate" PopupControlID="panelSnacks" RepositionMode="RepositionOnWindowResizeAndScroll" DropShadow="true" PopupDragHandleControlID="panelAddNewTitle" BackgroundCssClass="modalBackground"></asp:ModalPopupExtender>
                <asp:Panel ID="panelSnacks" runat="server" Style="display: none; background-color: white;" ForeColor="Black" Width="900px" Height="325px">
                    <asp:Panel ID="panel3" runat="server" Style="cursor: move; font-family: Tahoma; padding: 8px;" HorizontalAlign="Center" BackColor="#748CB2" Font-Bold="true" ForeColor="White" Height="35px">
                        <b>Modify Snacks Supply Register Check In</b>
                    </asp:Panel>
                    <div class="Row">
                        <div class="col-sm-6">

                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div3">
                                    <label class="control-label col-sm-4" id="lblSNo" style="text-align: left; font-weight: lighter">SNo</label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtSnacksEditSno" runat="server" CssClass="form-control" BackColor="White" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="form-horizontal" style="margin: 5px;">
                                        <div class="form-group" runat="server" id="div28">
                                            <label class="control-label col-sm-4" id="lblDepartment" style="text-align: left; font-weight: lighter">Department<span class="mandatoryStar">*</span></label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddlSnacksDepartment" ClientIDMode="Static" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSnacksDepartment_SelectedIndexChanged"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Select Department" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlSnacksDepartment" InitialValue="0" ValidationGroup="Snacks"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlSnacksDepartment" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>

                            <asp:UpdatePanel runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <div class="form-horizontal" style="margin: 5px;">
                                        <div class="form-group" runat="server" id="div7">
                                            <label class="control-label col-sm-4" id="lblSnacksEmployeeName" style="text-align: left; font-weight: lighter">Employee Name<span class="mandatoryStar">*</span></label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddlSnacksEmp" runat="server" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Select Employee" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlSnacksEmp" InitialValue="" ValidationGroup="Snacks"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlSnacksEmp" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>

                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="DIVIssuedItems">
                                    <label class="control-label col-sm-4" id="lblIssuedItems" style="text-align: left; font-weight: lighter">Issued Items<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtSnacksIssuedItems" runat="server" CssClass="form-control" placeholder="Enter Issued Items"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Enter Issued Items" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtSnacksIssuedItems" ValidationGroup="Snacks"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="DIV8">
                                    <label class="control-label col-sm-4" id="lblItemsCost" style="text-align: left; font-weight: lighter">Item Cost</label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtItemCost" runat="server" CssClass="form-control" placeholder="Enter Items Cost" onkeypress="return onlyDotsAndNumbers(this,event);"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">

                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server">
                                    <label class="control-label col-sm-4" id="lblIssuedDateTime" style="text-align: left; font-weight: lighter">Issued DateTime</label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtSnacksInDateTime" runat="server" BackColor="White" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div9">
                                    <label class="control-label col-sm-4" id="lblRemarks" style="text-align: left; font-weight: lighter">Remarks</label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtSnacksRemarks" runat="server" CssClass="form-control" placeholder="Enter Remarks" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                    </div>
                                </div>
                            </div>


                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div10">
                                    <label class="control-label col-sm-4" id="lblComments" style="text-align: left; font-weight: lighter">Comments<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtSnacksEditedComments" runat="server" CssClass="form-control" placeholder="Enter Comments" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Enter Comments" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtSnacksEditedComments" ValidationGroup="Snacks"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                        <asp:Button ID="btnSnacksUpdate" runat="server" Text="Update" CssClass="button" OnClick="btnSnacksUpdate_Click" UseSubmitBehavior="false" ValidationGroup="Snacks" />
                        <asp:Button ID="btnSnacksCancel" runat="server" Text="Cancel" CssClass="button" CausesValidation="false" OnClick="btnSnacksCancel_Click"  />
                    </div>
                </asp:Panel>
                <!--Panel to Edit record-->
                <div class="clearfix">
                </div>
                <div>
                    <asp:GridView ID="keyIssueGridView" runat="server" HorizontalAlign="Center" PagerStyle-CssClass="GridPager" CssClass="table table-bordered table-inverse" Style="text-align: center" OnRowDataBound="keyIssueGridView_RowDataBound" DataKeyNames="SNo" Font-Size="12px" HeaderStyle-Wrap="false" AutoGenerateColumns="False" HeaderStyle-Width="5%" EmptyDataText="Currently No Records Found" CellPadding="3" HeaderStyle-HorizontalAlign="Center" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" AllowPaging="True" ForeColor="#66CCFF" OnPageIndexChanging="keyIssueGridView_PageIndexChanging">
                        <EmptyDataRowStyle ForeColor="#FF3300" HorizontalAlign="Center" />
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <HeaderStyle BackColor="#748CB2" Font-Bold="True" ForeColor="White" Height="30px" />
                        <PagerStyle ForeColor="White" HorizontalAlign="Center" BackColor="White" />
                        <RowStyle ForeColor="#000066" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#0c99f0" />
                        <SortedAscendingHeaderStyle BackColor="#0c99f0" />
                        <SortedDescendingCellStyle BackColor="#0c99f0" />
                        <SortedDescendingHeaderStyle BackColor="#0c99f0" />
                        <Columns>
                            <asp:TemplateField HeaderText="SNO" SortExpression="SNo">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtGVSno" runat="server" Text='<%# Bind("SNo") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblGVKeySno" runat="server" Text='<%# Bind("SNo") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="EMP NAME" SortExpression="EmpName">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtGVEmployeeName" runat="server" Text='<%# Bind("EmpName") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblGVKeyEmployeeName" runat="server" Text='<%# Bind("EmpName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DEPARTMENT" SortExpression="Department">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtGVSDepartment" runat="server" Text='<%# Bind("Department") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblGVDepartment" runat="server" Text='<%# Bind("Department") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DEPARTMENT AREA" SortExpression="KeyDeptArea">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtGVSDepartmentArea" runat="server" Text='<%# Bind("KeyDeptArea") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblGVDepartmentArea" runat="server" Text='<%# Bind("KeyDeptArea") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="NO OF KEYS ISSUED" SortExpression="NoofKeys">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtGVSNoofKeys" runat="server" Text='<%# Bind("NoofKeys") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblGVNoofKeys" runat="server" Text='<%# Bind("NoofKeys") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ISSUED ON" SortExpression="IssuedOn">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtGVIssuedDateTime" runat="server" Text='<%# Bind("IssuedOn") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblGVIssuedDateTime" runat="server" Text='<%# Bind("IssuedOn") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="RETURNED ON" SortExpression="ReturnDateTime">
                                <ItemTemplate>
                                    <asp:Label ID="lblGVKeyReturnDateTime" runat="server" Text='<%# Bind("ReturnOn") %>'></asp:Label>
                                    <asp:LinkButton ID="lnkCheckOutKey" runat="server" ForeColor="#ff0000" OnClick="lnkCheckOutKey_Click" Visible="false">CheckIn</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="EDIT">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkKey" runat="server" CssClass="glyphicon glyphicon-edit GV_Edit_Icon" OnClick="LinkKey_Click"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <!--Panel to add new record-->
                <asp:ModalPopupExtender ID="ModalPopupKeySupply" runat="server" TargetControlID="btnKeyUpdate" PopupControlID="panel1" RepositionMode="RepositionOnWindowResizeAndScroll" DropShadow="true" PopupDragHandleControlID="panelAddNewTitle" BackgroundCssClass="modalBackground"></asp:ModalPopupExtender>
                <asp:Panel ID="panel1" runat="server" Style="display: none; background-color: white;" ForeColor="Black" Width="900px" Height="345px">
                    <asp:Panel ID="panel2" runat="server" Style="cursor: move; font-family: Tahoma; padding: 8px;" HorizontalAlign="Center" BackColor="#748CB2" Font-Bold="true" ForeColor="White" Height="35px">
                        <b>Modify Key Supply Check In</b>
                    </asp:Panel>
                    <div class="Row">
                        <div class="col-sm-6">

                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div11">
                                    <label class="control-label col-sm-4" id="lblKeySNo" style="text-align: left; font-weight: lighter">SNo</label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtKeySNo" runat="server" CssClass="form-control" BackColor="White" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="form-horizontal" style="margin: 5px;">
                                        <div class="form-group" runat="server" id="div12">
                                            <label class="control-label col-sm-4" id="lblKeyDepartment" style="text-align: left; font-weight: lighter">Department<span class="mandatoryStar">*</span></label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddlKeyDepartment" ClientIDMode="Static" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlKeyDepartment_SelectedIndexChanged"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Select Department" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlKeyDepartment" InitialValue="0" ValidationGroup="Key"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlKeyDepartment" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>

                            <asp:UpdatePanel runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <div class="form-horizontal" style="margin: 5px;">
                                        <div class="form-group" runat="server" id="div13">
                                            <label class="control-label col-sm-4" id="lblKeyEmployeeName" style="text-align: left; font-weight: lighter">Employee Name<span class="mandatoryStar">*</span></label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddlKeyEmp" runat="server" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Select Employee" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlKeyEmp" InitialValue="0" ValidationGroup="Key"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlKeyEmp" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>

                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server">
                                    <label class="control-label col-sm-4" id="lblDepartmentArea" style="text-align: left; font-weight: lighter">Department Area<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtKeyDepartmentArea" runat="server" CssClass="form-control" placeholder="Enter DepartmentArea"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Enter DepartmentArea" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtKeyDepartmentArea" ValidationGroup="Key"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>

                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server">
                                    <label class="control-label col-sm-4" id="lblNoofKeys" style="text-align: left; font-weight: lighter">No of Keys<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtKeyNoofkeys" runat="server" CssClass="form-control" placeholder="Enter No of Keys" onkeypress="if(event.keyCode<48 || event.keyCode>57)event.returnValue=false;"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Enter No of Keys" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtKeyNoofkeys" ValidationGroup="Key"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Enter Value Greater Than 0" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtKeyNoofkeys" ValidationGroup="Visitor" ValidationExpression="^0*[1-9]\d*$" ></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server">
                                    <label class="control-label col-sm-4" id="lblKeyIssuedDateTime" style="text-align: left; font-weight: lighter">Issued DateTime</label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtKeyIssueDateTime" runat="server" BackColor="White" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div16">
                                    <label class="control-label col-sm-4" id="lblKeyRemarks" style="text-align: left; font-weight: lighter">Remarks</label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtKeyRemarks" runat="server" CssClass="form-control" placeholder="Enter Remarks" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div17">
                                    <label class="control-label col-sm-4" id="lblKeyComments" style="text-align: left; font-weight: lighter">Comments<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtKeyComments" runat="server" CssClass="form-control" placeholder="Enter Comments" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Enter Comments" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtKeyComments" ValidationGroup="Key"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                        <asp:Button ID="btnKeyUpdate" runat="server" Text="Update" CssClass="button" OnClick="btnKeyUpdate_Click" UseSubmitBehavior="false" ValidationGroup="Key"/>
                        <asp:Button ID="btnKeyCancel" runat="server" Text="Cancel" CssClass="button" CausesValidation="false" OnClick="btnKeyCancel_Click" />
                    </div>
                </asp:Panel>
                <!--Panel to Edit record-->
                <div class="clearfix">
                </div>

                <asp:GridView ID="AccessGridView" runat="server" HorizontalAlign="Center" DataKeyNames="SNo" PagerStyle-CssClass="GridPager" CssClass="table table-bordered table-inverse" Style="text-align: center" OnRowDataBound="AccessGridView_RowDataBound" Font-Size="12px" HeaderStyle-Wrap="false" AutoGenerateColumns="False" HeaderStyle-Width="5%" EmptyDataText="Currently No Records Found" CellPadding="3" HeaderStyle-HorizontalAlign="Center" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" AllowPaging="True" ForeColor="#66CCFF" OnPageIndexChanging="AccessGridView_PageIndexChanging">
                    <EmptyDataRowStyle ForeColor="#FF3300" HorizontalAlign="Center" />
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <HeaderStyle BackColor="#748CB2" Font-Bold="True" ForeColor="White" Height="30px" />
                    <PagerStyle ForeColor="White" HorizontalAlign="Center" BackColor="White" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#0c99f0" />
                    <SortedAscendingHeaderStyle BackColor="#0c99f0" />
                    <SortedDescendingCellStyle BackColor="#0c99f0" />
                    <SortedDescendingHeaderStyle BackColor="#0c99f0" />
                    <Columns>
                        <asp:TemplateField HeaderText="SNO" SortExpression="SNo">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtGVSno" runat="server" Text='<%# Bind("SNo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGVAccessSno" runat="server" Text='<%# Bind("SNo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                        <asp:TemplateField HeaderText="EMPLOYEE NAME" SortExpression="EmpName">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtGVEmployeeName" runat="server" Text='<%# Bind("EmpName") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGVAccessCardEmployeeName" runat="server" Text='<%# Bind("EmpName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DEPARTMENT" SortExpression="Department">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtGVSDepartment" runat="server" Text='<%# Bind("Department") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGVDepartment" runat="server" Text='<%# Bind("Department") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ACCESS CARD NO" SortExpression="AccessCardNo">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtGVSAccessCardNo" runat="server" Text='<%# Bind("AccessCardNo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGVAccessCardNo" runat="server" Text='<%# Bind("AccessCardNo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="MOBILE NO" SortExpression="MobileNo">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtGVMobileNo" runat="server" Text='<%# Bind("MobileNo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGVAccessCardMobileNo" runat="server" Text='<%# Bind("MobileNo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ISSUED DATETIME" SortExpression="IssuedOn">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtGVIssuedDateTime" runat="server" Text='<%# Bind("IssuedOn") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGVIssuedDateTime" runat="server" Text='<%# Bind("IssuedOn") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="RETURNED DATETIME" SortExpression="ReturnOn">
                            <ItemTemplate>
                                <asp:Label ID="lblGVAccessReturnDateTime" runat="server" Text='<%# Bind("ReturnOn") %>'></asp:Label>
                                <asp:LinkButton ID="lnkCheckOutAccess" runat="server" ForeColor="#ff0000" OnClick="lnkCheckOutAccess_Click" Visible="false">CheckIn</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EDIT">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkAccessCard" runat="server" CssClass="glyphicon glyphicon-edit GV_Edit_Icon" OnClick="LinkAccessCard_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <!--Panel to add new record-->
            <asp:ModalPopupExtender ID="ModalPopupAccessCard" runat="server" TargetControlID="btnAccessUpdate" PopupControlID="panel4" RepositionMode="RepositionOnWindowResizeAndScroll" DropShadow="true" PopupDragHandleControlID="panelAddNewTitle" BackgroundCssClass="modalBackground"></asp:ModalPopupExtender>
            <asp:Panel ID="panel4" runat="server" Style="display: none; background-color: white;" ForeColor="Black" Width="900px" Height="325px">
                <asp:Panel ID="panel5" runat="server" Style="cursor: move; font-family: Tahoma; padding: 8px;" HorizontalAlign="Center" BackColor="#748CB2" Font-Bold="true" ForeColor="White" Height="35px">
                    <b>Modify AccessCard Supply Check In</b>
                </asp:Panel>
                <div class="Row">
                    <div class="col-sm-6">
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div14">
                                <label class="control-label col-sm-4" id="lblAccessSNo" style="text-align: left; font-weight: lighter">SNo</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtAccessSNo" runat="server" CssClass="form-control" BackColor="White" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div15">
                                        <label class="control-label col-sm-4" id="lblAccessDepartment" style="text-align: left; font-weight: lighter">Department<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlAccessDepartment" ClientIDMode="Static" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlAccessDepartment_SelectedIndexChanged"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Select Department" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlAccessDepartment" InitialValue="0" ValidationGroup="Access"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlAccessDepartment" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div18">
                                        <label class="control-label col-sm-4" id="lblAccessEmployeeName" style="text-align: left; font-weight: lighter">Employee Name<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlAccessEmp" runat="server" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Select Employee" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlAccessEmp" InitialValue="0" ValidationGroup="Access"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlAccessEmp" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="DIVAccessCardNo">
                                <label class="control-label col-sm-4" id="lbl" style="text-align: left; font-weight: lighter">AccessCardNo<span class="mandatoryStar">*</span></label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtAccessCardNo" runat="server" CssClass="form-control" placeholder="Enter Access Card No"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Enter Access Card No" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtAccessCardNo" ValidationGroup="Access"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="DIVMobile">
                                <label class="control-label col-sm-4" id="lblMobileNo" style="text-align: left; font-weight: lighter">Mobile No</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtAccessMobileNo" runat="server" CssClass="form-control" placeholder="Enter Mobile No" MaxLength="10" onkeypress="return ValidatePhoneNo(event);"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorMobile" runat="server" ErrorMessage="Mobile No Should be 10 Digits" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtAccessMobileNo" ValidationGroup="Access" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server">
                                <label class="control-label col-sm-4" id="lblAccessIssuedDateTime" style="text-align: left; font-weight: lighter">Issued DateTime</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtAccessIndateTime" runat="server" BackColor="White" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div19">
                                <label class="control-label col-sm-4" id="lblAccessRemarks" style="text-align: left; font-weight: lighter">Remarks</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtAccessRemarks" runat="server" CssClass="form-control" placeholder="Enter Remarks" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div20">
                                <label class="control-label col-sm-4" id="lblAccessComments" style="text-align: left; font-weight: lighter">Comments<span class="mandatoryStar">*</span></label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtAccessComments" runat="server" CssClass="form-control" placeholder="Enter Comments" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Enter Comments" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtAccessComments" ValidationGroup="Access"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                    <asp:Button ID="btnAccessUpdate" runat="server" Text="Update" CssClass="button" OnClick="btnAccessUpdate_Click" UseSubmitBehavior="false" ValidationGroup="Access"/>
                    <asp:Button ID="btnAccessCancel" runat="server" Text="Cancel" CssClass="button" CausesValidation="false" OnClick="btnAccessCancel_Click"/>
                </div>
            </asp:Panel>
            <!--Panel to Edit record-->
        </div>
    </div>

    <%--</ContentTemplate>
</asp:UpdatePanel> --%>

 
    <script>
        $("#NavLnkSupply").attr("class", "active");
        $("#SupplyRegisterList").attr("class", "active");
    </script>

    <script type="text/javascript">
         function Submitvaidate() {
             var SupplyType = document.getElementById("<%=ddlSupplyType.ClientID%>");

             errors = [];
             if (SupplyType.value == 0) {
                 errors.push("Please Select Supply Type.");
             }

             if (errors.length > 0) {
                 alert(errors.join("\n"));
                 return false;
             }
         }
    </script>
    

       <script>
           //for start date and end date validations startdate should be < enddate
           $(function () {
               $('#<%=txtDateFrom.ClientID%>').datetimepicker({
                  format: 'DD MMM YYYY',
                  useCurrent: false,
              });
              var startdate = document.getElementById('<%=txtDateFrom.ClientID%>').value
              $('#<%=txtDateTo.ClientID%>').datetimepicker({
                         format: 'DD MMM YYYY',
                         minDate: startdate,
                         useCurrent: false,
                     });
                     $('#<%=txtDateFrom.ClientID%>').on("dp.change", function (e) {
                         $('#<%=txtDateTo.ClientID%>').data("DateTimePicker").minDate(e.date);
              
                          $('#<%=txtDateTo.ClientID%>').val("");
                     });
                     $('#<%=txtDateTo.ClientID%>').on("dp.change", function (e) {
                      });
           });
    </script>

      <script type="text/javascript">
          //for datepickers
          var startdate2 = document.getElementById('<%=txtDateFrom.ClientID%>').value
          if (startdate2 == "") {
              $('#<%=txtDateFrom.ClientID%>').datetimepicker({
                         format: 'DD MMM YYYY',
                         useCurrent: false,
                     });
                     $('#<%=txtDateTo.ClientID%>').datetimepicker({
                         format: 'DD MMM YYYY',
                         useCurrent: false,
                     });
                 }
    </script>
  
</asp:Content>
