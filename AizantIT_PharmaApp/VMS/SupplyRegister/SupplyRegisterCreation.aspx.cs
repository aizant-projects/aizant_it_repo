﻿using System;

using System.Web.UI.WebControls;
using VMS_BAL;
using VMS_BO;
using AizantIT_PharmaApp.Common;
using System.Data;
using System.Data.SqlClient;

namespace AizantIT_PharmaApp.VMS.SupplyRegister
{
    public partial class SupplyRegisterCreation : System.Web.UI.Page
    {
        SupplyRegisterBAL objSupplyRegisterBAL = new SupplyRegisterBAL();
        SupplyRegisterBO objSupplyRegisterBO;
        DataTable dt;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            ViewState["EmpID"] = "0";
                            if (HelpClass.IsUserAuthenticated())
                            {
                                ViewState["EmpID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            }
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaff7:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaff7:" + strline + " " + strMsg);
            }
        }

        private void InitializeThePage()
        {
            try
            {
                LoadDepartment();
                TimeDate();
                BindSupplyType();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSINWLPL:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSINWLPL:" + strline + " " + strMsg);
            }
        }
        private void TimeDate()
        {
            txtSnacksInDateTime.Text = DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");            
        }

        private void LoadDepartment()
        {
            try
            {
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                dt = new DataTable();
                dt = objVisitorRegisterBAL.LoadDepartment();
                ddlDepartment.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlDepartment.DataValueField = dt.Columns["DeptID"].ToString();
                ddlDepartment.DataSource = dt;
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSR1:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSR1:" + strline + " " + strMsg);
            }
        }

        protected void ddlSupplyType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSupplyType.SelectedIndex == 0)
            {
                DIVIssuedItems.Visible = true;
                DIVItemCost.Visible = true;
                DIVNoofkeys.Visible = false;
                DIVAreaoftheDept.Visible = false;
                DIVAccessCardNo.Visible = false;
                DIVMobile.Visible = false;
                SupplyRegister_Reset();
            }
            else if (ddlSupplyType.SelectedIndex == 1)
            {
                DIVIssuedItems.Visible = false;
                DIVItemCost.Visible = false;
                DIVNoofkeys.Visible = true;
                DIVAreaoftheDept.Visible = true;
                DIVAccessCardNo.Visible = false;
                DIVMobile.Visible = false;
                SupplyRegister_Reset();
            }
            else if (ddlSupplyType.SelectedIndex == 2)
            {
                DIVIssuedItems.Visible = false;
                DIVItemCost.Visible = false;
                DIVNoofkeys.Visible = false;
                DIVAreaoftheDept.Visible = false;
                DIVAccessCardNo.Visible = true;
                DIVMobile.Visible = true;
                SupplyRegister_Reset();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            objSupplyRegisterBO = new SupplyRegisterBO();
            if (ddlSupplyType.SelectedIndex == 0)
            {
                try
                {
                    if (txtSnacksIssuedItems.Text.Trim() == "" || ddlDepartment.SelectedValue == "0" || ddlEmp.SelectedValue == "0")
                    {
                        HelpClass.showMsg(btnSubmit, btnSubmit.GetType(), "All fields represented with * are mandatory");
                        return;
                    }
                    objSupplyRegisterBO.EmployeeID = ddlEmp.SelectedValue.Trim();
                    objSupplyRegisterBO.IssuedItems = txtSnacksIssuedItems.Text.Trim();
                    objSupplyRegisterBO.InDateTime = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                    objSupplyRegisterBO.Remarks = txtSnacksRemarks.Text.Trim();
                    if (!string.IsNullOrEmpty(txtItemCost.Text.Trim()))
                        objSupplyRegisterBO.ItemCost = Convert.ToDecimal(txtItemCost.Text.Trim());
                    else
                        objSupplyRegisterBO.ItemCost = 0;
                    objSupplyRegisterBO.CreatedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                    objSupplyRegisterBO.CreatedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                    int c = objSupplyRegisterBAL.Snacks_Insert(objSupplyRegisterBO);
                    if (c > 0)
                    {
                        SupplyRegister_Reset();
                        HelpClass.showMsg(btnSubmit, btnSubmit.GetType(), "Snacks Supply Details Submitted Successfully");
                    }
                    else
                    {
                        HelpClass.showMsg(btnSubmit, btnSubmit.GetType(), "Snacks Supply Details Submission Failed");
                    }
                }
                catch (SqlException sqe)
                {
                    string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                    HelpClass.showMsg(this, this.GetType(), "VMSSSR2:" + strmsgF);
                }
                catch (Exception ex)
                {
                    string strline = HelpClass.LineNo(ex);
                    string strMsg = HelpClass.SQLEscapeString(ex.Message);
                    HelpClass.showMsg(this, this.GetType(), "VMSSSR2:" + strline + " " + strMsg);
                }
            }

            else if (ddlSupplyType.SelectedIndex == 1)
            {
                try
                {
                    if (txtKeyIssueDepartmentArea.Text.Trim() == "" || txtKeyIssueNoofKeys.Text.Trim() == "" || ddlDepartment.SelectedValue == "0" || ddlEmp.SelectedValue == "0")
                    {
                        HelpClass.showMsg(btnSubmit, btnSubmit.GetType(), "All fields represented with * are mandatory");
                        return;
                    }
                    objSupplyRegisterBO.EmployeeID = ddlEmp.SelectedValue.Trim();
                    objSupplyRegisterBO.Department = ddlDepartment.SelectedValue.Trim();
                    objSupplyRegisterBO.InDateTime = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                    objSupplyRegisterBO.Remarks = txtSnacksRemarks.Text.Trim();
                    if (!string.IsNullOrEmpty(txtKeyIssueNoofKeys.Text.Trim()))
                        objSupplyRegisterBO.NoofKeys = Convert.ToInt32(txtKeyIssueNoofKeys.Text.Trim());
                    else
                        objSupplyRegisterBO.NoofKeys = 0;
                    objSupplyRegisterBO.DepartmentArea = txtKeyIssueDepartmentArea.Text.Trim();
                    objSupplyRegisterBO.CreatedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                    objSupplyRegisterBO.CreatedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                    int c = objSupplyRegisterBAL.Key_Insert(objSupplyRegisterBO);
                    if (c > 0)
                    {
                        SupplyRegister_Reset();
                        HelpClass.showMsg(btnSubmit, btnSubmit.GetType(), "Key Supply Details Submitted Successfully");
                    }
                    else
                    {
                        HelpClass.showMsg(btnSubmit, btnSubmit.GetType(), "Key Supply Details Submission Failed");
                    }
                }
                catch (SqlException sqe)
                {
                    string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                    HelpClass.showMsg(this, this.GetType(), "VMSKSR3:" + strmsgF);
                }
                catch (Exception ex)
                {
                    string strline = HelpClass.LineNo(ex);
                    string strMsg = HelpClass.SQLEscapeString(ex.Message);
                    HelpClass.showMsg(this, this.GetType(), "VMSKSR3:" + strline + " " + strMsg);
                }
            }

            else if (ddlSupplyType.SelectedIndex == 2)
            {
                try
                {
                    if (txtAccessCardNo.Text.Trim() == "" || ddlDepartment.SelectedValue == "0" || ddlEmp.SelectedValue == "0")
                    {
                        HelpClass.showMsg(btnSubmit, btnSubmit.GetType(), "All fields represented with * are mandatory");
                        return;
                    }
                    objSupplyRegisterBO.EmployeeID = ddlEmp.SelectedValue.Trim();
                    objSupplyRegisterBO.Department = ddlDepartment.SelectedValue.Trim();
                    objSupplyRegisterBO.InDateTime = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                    objSupplyRegisterBO.Remarks = txtSnacksRemarks.Text.Trim();
                    objSupplyRegisterBO.MobileNo = txtMobileNo.Text.Trim();
                    objSupplyRegisterBO.AccessCardNo = txtAccessCardNo.Text.Trim();
                    objSupplyRegisterBO.CreatedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                    objSupplyRegisterBO.CreatedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                    int c = objSupplyRegisterBAL.Access_Insert(objSupplyRegisterBO);
                    if (c > 0)
                    {
                        SupplyRegister_Reset();
                        HelpClass.showMsg(btnSubmit, btnSubmit.GetType(), "AccesssCard Supply Details Submitted Successfully");
                    }
                    else
                    {
                        HelpClass.showMsg(btnSubmit, btnSubmit.GetType(), "AccesssCard Supply Details Submission Failed");
                    }
                }
                catch (SqlException sqe)
                {
                    string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                    HelpClass.showMsg(this, this.GetType(), "VMSASR4:" + strmsgF);
                }
                catch (Exception ex)
                {
                    string strline = HelpClass.LineNo(ex);
                    string strMsg = HelpClass.SQLEscapeString(ex.Message);
                    HelpClass.showMsg(this, this.GetType(), "VMSASR4:" + strline + " " + strMsg);
                }
            }
        }

        private void SupplyRegister_Reset()
        {
            txtAccessCardNo.Text = txtKeyIssueDepartmentArea.Text = txtItemCost.Text = txtKeyIssueNoofKeys.Text = txtMobileNo.Text = txtSnacksIssuedItems.Text = txtSnacksRemarks.Text = string.Empty;
            ddlDepartment.SelectedIndex = 0;
            ddlEmp.Items.Clear();
            TimeDate();
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            SupplyRegister_Reset();
        }

        private void Employee()
        {
            try
            {
                
                ddlEmp.Items.Clear();
                ddlEmp.Items.Insert(0, new ListItem("-- Select --", "0"));
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                int DeptID = Convert.ToInt32(ddlDepartment.SelectedValue);
                if (DeptID > 0)
                {
                    DataTable dt = objVisitorRegisterBAL.GetEmpData(DeptID);
                    ddlEmp.DataSource = dt;
                    ddlEmp.DataTextField = "Name";
                    ddlEmp.DataValueField = "EmpID";
                    ddlEmp.DataBind();
                    ddlEmp.Items.Insert(0, new ListItem("--Select--", "0"));
                   
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSASR5:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSASR5:" + strline + " " + strMsg);
            }
        }
            protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
            {
                Employee();
            }

        private void BindSupplyType()
        {
            try
            {
                DataTable dtSupply = new DataTable();
                dtSupply = objSupplyRegisterBAL.BindSupplyType();
                ddlSupplyType.DataTextField = "SupplyTypeName";
                ddlSupplyType.DataValueField = "SupplyTypeID";
                ddlSupplyType.DataSource = dtSupply;
                ddlSupplyType.DataBind();
               
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSASR6:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSASR6:" + strline + " " + strMsg);
            }
        }
    }
    }