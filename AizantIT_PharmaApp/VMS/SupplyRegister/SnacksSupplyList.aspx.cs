﻿using System;

using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

using AizantIT_PharmaApp.Common;
using VMS_BAL;
using VMS_BO;

namespace AizantIT_PharmaApp.VMS.SupplyRegister
{
    public partial class SnacksSupplyList : System.Web.UI.Page
    {
        DataTable dt;
        SupplyRegisterBO objSupplyRegisterBO;
        SupplyRegisterBAL objSupplyRegisterBAL = new SupplyRegisterBAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            ViewState["EmpID"] = "0";
                            if (HelpClass.IsUserAuthenticated())
                            {
                                ViewState["EmpID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            }
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaff7:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaff7:" + strline + " " + strMsg);
            }
        }

        private void InitializeThePage()
        {
            try
            {
                LoadDepartment();
                BindSupplyType();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSINWLPL:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSINWLPL:" + strline + " " + strMsg);
            }
        }

        private void LoadDepartment()
        {
            try
            {
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                dt = new DataTable();
                dt = objVisitorRegisterBAL.LoadDepartment();
                ddlDepartment.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlDepartment.DataValueField = dt.Columns["DeptID"].ToString();
                ddlDepartment.DataSource = dt;
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("All", "0"));

                ddlSnacksDepartment.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlSnacksDepartment.DataValueField = dt.Columns["DeptID"].ToString();
                ddlSnacksDepartment.DataSource = dt;
                ddlSnacksDepartment.DataBind();
                ddlSnacksDepartment.Items.Insert(0, new ListItem("--Select--", "0"));

                ddlKeyDepartment.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlKeyDepartment.DataValueField = dt.Columns["DeptID"].ToString();
                ddlKeyDepartment.DataSource = dt;
                ddlKeyDepartment.DataBind();
                ddlKeyDepartment.Items.Insert(0, new ListItem("--Select--", "0"));

                ddlAccessDepartment.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlAccessDepartment.DataValueField = dt.Columns["DeptID"].ToString();
                ddlAccessDepartment.DataSource = dt;
                ddlAccessDepartment.DataBind();
                ddlAccessDepartment.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList1:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList1:" + strline + " " + strMsg);
            }
        }

        private void SnacksBind_Data()
        {
            try
            {
                dt = new DataTable();
                dt = objSupplyRegisterBAL.BindDataSnacksSupplyRegister();
                SnacksGridView.DataSource = dt;
                SnacksGridView.DataBind();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList2:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList2:" + strline + " " + strMsg);
            }
        }

        private void KeyBind_Data()
        {
            try
            {
                dt = new DataTable();
                dt = objSupplyRegisterBAL.BindDataKeySupplyRegister();
                keyIssueGridView.DataSource = dt;
                keyIssueGridView.DataBind();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList3:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList3:" + strline + " " + strMsg);
            }
        }

        private void AccessCardBind_Data()
        {
            try
            {
                dt = new DataTable();
                dt = objSupplyRegisterBAL.BindDataAccessCardSupplyRegister();
                AccessGridView.DataSource = dt;
                AccessGridView.DataBind();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList4:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList4:" + strline + " " + strMsg);
            }
        }

        protected void btnSnacksUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                objSupplyRegisterBO = new SupplyRegisterBO();
                objSupplyRegisterBO.SNo = Convert.ToInt32(txtSnacksEditSno.Text.Trim());
                objSupplyRegisterBO.EmployeeID = ddlSnacksEmp.SelectedValue.Trim();                
                objSupplyRegisterBO.IssuedItems = txtSnacksIssuedItems.Text.Trim();
                if (!string.IsNullOrEmpty(txtItemCost.Text.Trim()))
                    objSupplyRegisterBO.ItemCost = Convert.ToDecimal(txtItemCost.Text.Trim());
                else
                    objSupplyRegisterBO.ItemCost = 0;
                objSupplyRegisterBO.Remarks = txtSnacksRemarks.Text.Trim();
                objSupplyRegisterBO.LastChangedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                objSupplyRegisterBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                objSupplyRegisterBO.LastChangedComments = txtSnacksEditedComments.Text.Trim();
                int c = objSupplyRegisterBAL.Update_SnacksSupplyRegisterEdit(objSupplyRegisterBO);
                if (c > 0)
                {
                    HelpClass.showMsg(btnSnacksUpdate, btnSnacksUpdate.GetType(), "Snacks Supply Details Updated Successfully");
                }
                else
                {
                    HelpClass.showMsg(btnSnacksUpdate, btnSnacksUpdate.GetType(), "Snacks Supply Details Updation Failed");
                }
                SnacksBind_Data();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList5:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList5:" + strline + " " + strMsg);
            }
        }

        protected void btnSnacksCancel_Click(object sender, EventArgs e)
        {
            ModalPopupSnacksRegister.Hide();
        }

        protected void btnKeyUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                objSupplyRegisterBO = new SupplyRegisterBO();
                objSupplyRegisterBO.SNo = Convert.ToInt32(txtKeySNo.Text.Trim());
                objSupplyRegisterBO.EmployeeID = ddlKeyEmp.SelectedValue.Trim();
                objSupplyRegisterBO.Department = ddlKeyDepartment.SelectedValue.Trim();
                if (!string.IsNullOrEmpty(txtKeyNoofkeys.Text.Trim()))
                    objSupplyRegisterBO.NoofKeys = Convert.ToInt32(txtKeyNoofkeys.Text.Trim());
                else
                    objSupplyRegisterBO.NoofKeys = 0;
                objSupplyRegisterBO.DepartmentArea = txtKeyDepartmentArea.Text.Trim();                
                objSupplyRegisterBO.Remarks = txtKeyRemarks.Text.Trim();
                objSupplyRegisterBO.LastChangedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                objSupplyRegisterBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                objSupplyRegisterBO.LastChangedComments = txtKeyComments.Text.Trim();
                int c = objSupplyRegisterBAL.Update_KeySupplyRegisterEdit(objSupplyRegisterBO);
                if (c > 0)
                {
                    HelpClass.showMsg(btnKeyUpdate, btnKeyUpdate.GetType(), "Key Supply Details Updated Successfully");
                }
                else
                {
                    HelpClass.showMsg(btnKeyUpdate, btnKeyUpdate.GetType(), "Key Supply Details Updation Failed");
                }
                KeyBind_Data();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList6:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList6:" + strline + " " + strMsg);
            }
           
        }

        protected void btnKeyCancel_Click(object sender, EventArgs e)
        {
            ModalPopupKeySupply.Hide();
        }

        protected void btnAccessUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                objSupplyRegisterBO = new SupplyRegisterBO();
                objSupplyRegisterBO.SNo = Convert.ToInt32(txtAccessSNo.Text.Trim());
                objSupplyRegisterBO.EmployeeID =ddlAccessEmp.SelectedValue.Trim();
                objSupplyRegisterBO.Department = ddlAccessDepartment.SelectedValue.Trim();
                objSupplyRegisterBO.MobileNo = txtAccessMobileNo.Text.Trim();
                objSupplyRegisterBO.AccessCardNo = txtAccessCardNo.Text.Trim();               
                objSupplyRegisterBO.Remarks = txtAccessRemarks.Text.Trim();
                objSupplyRegisterBO.LastChangedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                objSupplyRegisterBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                objSupplyRegisterBO.LastChangedComments = txtAccessComments.Text.Trim();
                int c = objSupplyRegisterBAL.Update_AccessCardSupplyRegisterEdit(objSupplyRegisterBO);
                if (c > 0)
                {
                    HelpClass.showMsg(btnAccessUpdate, btnAccessUpdate.GetType(), "AccessCard Supply Details Updated Successfully");
                }
                else
                {
                    HelpClass.showMsg(btnAccessUpdate, btnAccessUpdate.GetType(), "AccessCard Supply Details Updation Failed");
                }
                AccessCardBind_Data();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList7:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList7:" + strline + " " + strMsg);
            }
        }

        protected void btnAccessCancel_Click(object sender, EventArgs e)
        {
            ModalPopupAccessCard.Hide();
        }

        protected void LinkSnacksIssue_Click(object sender, EventArgs e)
        {
            try
            {
                objSupplyRegisterBO = new SupplyRegisterBO();
                LinkButton lnk = sender as LinkButton;
                GridViewRow gr = (GridViewRow)lnk.NamingContainer;
                string tempID = SnacksGridView.DataKeys[gr.RowIndex].Value.ToString();
                objSupplyRegisterBO.SNo = Convert.ToInt32(tempID);
                ViewState["tempId1"] = tempID;
                dt = new DataTable();
                dt = objSupplyRegisterBAL.SnacksSupplyRegisterlinkbuttonFilldata(objSupplyRegisterBO);
                txtSnacksEditSno.Text = dt.Rows[0]["SNo"].ToString();
                txtSnacksIssuedItems.Text = dt.Rows[0]["IssuedItems"].ToString();
                txtSnacksInDateTime.Text = dt.Rows[0]["IssuedOn"].ToString();
                txtSnacksRemarks.Text= dt.Rows[0]["Remarks"].ToString();
                txtItemCost.Text= dt.Rows[0]["ItemCost"].ToString();
                txtSnacksEditedComments.Text = dt.Rows[0]["LastChangedByComments"].ToString();
                ddlSnacksDepartment.SelectedValue = dt.Rows[0]["DeptID"].ToString();
                SnacksEmployee();
                ddlSnacksEmp.SelectedValue = dt.Rows[0]["EmpID"].ToString();
                ModalPopupSnacksRegister.Show();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList8:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList8:" + strline + " " + strMsg);
            } 
        }

        protected void LinkKey_Click(object sender, EventArgs e)
        {
            try
            {
                objSupplyRegisterBO = new SupplyRegisterBO();
                LinkButton lnk = sender as LinkButton;
                GridViewRow gr = (GridViewRow)lnk.NamingContainer;
                string tempID = keyIssueGridView.DataKeys[gr.RowIndex].Value.ToString();
                objSupplyRegisterBO.SNo = Convert.ToInt32(tempID);
                ViewState["tempId1"] = tempID;
                dt = new DataTable();
                dt = objSupplyRegisterBAL.KeySupplyRegisterlinkbuttonFilldata(objSupplyRegisterBO);
                txtKeySNo.Text = dt.Rows[0]["SNo"].ToString();
                txtKeyDepartmentArea.Text = dt.Rows[0]["KeyDeptArea"].ToString();
                txtKeyNoofkeys.Text = dt.Rows[0]["NoofKeys"].ToString();
                txtKeyIssueDateTime.Text= dt.Rows[0]["IssuedOn"].ToString();
                txtKeyRemarks.Text= dt.Rows[0]["Remarks"].ToString();
                txtKeyComments.Text = dt.Rows[0]["LastChangedByComments"].ToString();
                ddlKeyDepartment.SelectedValue = dt.Rows[0]["KeyDeptID"].ToString();
                KeySupplyEmployee();
                ddlKeyEmp.SelectedValue = dt.Rows[0]["EmpID"].ToString();
                ModalPopupKeySupply.Show();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList9:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList9:" + strline + " " + strMsg);
            }
        }

        protected void LinkAccessCard_Click(object sender, EventArgs e)
        {
            try
            {
                objSupplyRegisterBO = new SupplyRegisterBO();
                LinkButton lnk = sender as LinkButton;
                GridViewRow gr = (GridViewRow)lnk.NamingContainer;
                string tempID = AccessGridView.DataKeys[gr.RowIndex].Value.ToString();
                objSupplyRegisterBO.SNo = Convert.ToInt32(tempID);
                ViewState["tempId1"] = tempID;
                dt = new DataTable();
                dt = objSupplyRegisterBAL.AccessCardSupplyRegisterlinkbuttonFilldata(objSupplyRegisterBO);
                txtAccessSNo.Text = dt.Rows[0]["SNo"].ToString();
                txtAccessRemarks.Text= dt.Rows[0]["Remarks"].ToString();
                txtAccessIndateTime.Text= dt.Rows[0]["IssuedOn"].ToString();
                txtAccessMobileNo.Text = dt.Rows[0]["MobileNo"].ToString();
                txtAccessCardNo.Text = dt.Rows[0]["AccessCardNo"].ToString();
                txtAccessComments.Text = dt.Rows[0]["LastChangedByComments"].ToString();
                ddlAccessDepartment.SelectedValue = dt.Rows[0]["DeptID"].ToString();
                AccessSupplyEmployee();
                ddlAccessEmp.SelectedValue = dt.Rows[0]["EmpID"].ToString();
                ModalPopupAccessCard.Show();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList10:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList10:" + strline + " " + strMsg);
            }
        }

        protected void btnSupplyFind_Click(object sender, EventArgs e)
        {
            objSupplyRegisterBO = new SupplyRegisterBO();

            if (ddlSupplyType.SelectedIndex == 1)
            {
                try
                {
                    objSupplyRegisterBO.Department = ddlDepartment.SelectedValue.Trim();
                    objSupplyRegisterBO.EmployeeID = ddlEmp.SelectedValue.Trim();
                    if (!string.IsNullOrEmpty(txtDateFrom.Text.Trim()))
                    {
                        objSupplyRegisterBO.InDateTime = Convert.ToDateTime(txtDateFrom.Text.Trim()).ToString("yyyyMMdd");
                    }
                    else
                    {
                        objSupplyRegisterBO.InDateTime = txtDateFrom.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(txtDateTo.Text.Trim()))
                    {
                        objSupplyRegisterBO.ReturnDateTime = Convert.ToDateTime(txtDateTo.Text.Trim()).ToString("yyyyMMdd");
                    }
                    else
                    {
                        objSupplyRegisterBO.ReturnDateTime = txtDateTo.Text.Trim();
                    }
                    dt = new DataTable();
                    dt = objSupplyRegisterBAL.SearchFilterSnacksSupply(objSupplyRegisterBO);
                    SnacksGridView.DataSource = dt;
                    SnacksGridView.DataBind();
                    ViewState["dtSnacks"] = dt;
                    SnacksGridView.Visible = true;
                    keyIssueGridView.Visible = false;
                    AccessGridView.Visible = false;

                }
                catch (SqlException sqe)
                {
                    string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                    HelpClass.showMsg(this, this.GetType(), "VMSSupplyList11:" + strmsgF);
                }
                catch (Exception ex)
                {
                    string strline = HelpClass.LineNo(ex);
                    string strMsg = HelpClass.SQLEscapeString(ex.Message);
                    HelpClass.showMsg(this, this.GetType(), "VMSSupplyList11:" + strline + " " + strMsg);
                }
            }
            else if (ddlSupplyType.SelectedIndex == 2)
            {
                try
                {
                    objSupplyRegisterBO.Department = ddlDepartment.SelectedValue.Trim();
                    objSupplyRegisterBO.EmployeeID = ddlEmp.SelectedValue.Trim();
                    if (!string.IsNullOrEmpty(txtDateFrom.Text.Trim()))
                    {
                        objSupplyRegisterBO.InDateTime = Convert.ToDateTime(txtDateFrom.Text.Trim()).ToString("yyyyMMdd");
                    }
                    else
                    {
                        objSupplyRegisterBO.InDateTime = txtDateFrom.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(txtDateTo.Text.Trim()))
                    {
                        objSupplyRegisterBO.ReturnDateTime = Convert.ToDateTime(txtDateTo.Text.Trim()).ToString("yyyyMMdd");
                    }
                    else
                    {
                        objSupplyRegisterBO.ReturnDateTime = txtDateTo.Text.Trim();
                    }
                    dt = new DataTable();
                    dt = objSupplyRegisterBAL.SearchFilterKeySupply(objSupplyRegisterBO);
                    keyIssueGridView.DataSource = dt;
                    keyIssueGridView.DataBind();
                    ViewState["dtKey"] = dt;
                    keyIssueGridView.Visible = true;
                    AccessGridView.Visible = false;
                    SnacksGridView.Visible = false;
                }
                catch (SqlException sqe)
                {
                    string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                    HelpClass.showMsg(this, this.GetType(), "VMSSupplyList12:" + strmsgF);
                }
                catch (Exception ex)
                {
                    string strline = HelpClass.LineNo(ex);
                    string strMsg = HelpClass.SQLEscapeString(ex.Message);
                    HelpClass.showMsg(this, this.GetType(), "VMSSupplyList12:" + strline + " " + strMsg);
                }

            }
            else if (ddlSupplyType.SelectedIndex == 3)
            {
                try
                {
                    objSupplyRegisterBO.Department = ddlDepartment.SelectedValue.Trim();
                    objSupplyRegisterBO.EmployeeID = ddlEmp.SelectedValue.Trim();
                    if (!string.IsNullOrEmpty(txtDateFrom.Text.Trim()))
                    {
                        objSupplyRegisterBO.InDateTime = Convert.ToDateTime(txtDateFrom.Text.Trim()).ToString("yyyyMMdd");
                    }
                    else
                    {
                        objSupplyRegisterBO.InDateTime = txtDateFrom.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(txtDateTo.Text.Trim()))
                    {
                        objSupplyRegisterBO.ReturnDateTime = Convert.ToDateTime(txtDateTo.Text.Trim()).ToString("yyyyMMdd");
                    }
                    else
                    {
                        objSupplyRegisterBO.ReturnDateTime = txtDateTo.Text.Trim();
                    }
                    dt = new DataTable();
                    dt = objSupplyRegisterBAL.SearchFilterAccessCardSupply(objSupplyRegisterBO);
                    AccessGridView.DataSource = dt;
                    AccessGridView.DataBind();
                    ViewState["dtAccess"] = dt;
                    AccessGridView.Visible = true;
                    SnacksGridView.Visible = false;
                    keyIssueGridView.Visible = false;
                }
                catch (SqlException sqe)
                {
                    string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                    HelpClass.showMsg(this, this.GetType(), "VMSSupplyList13:" + strmsgF);
                }
                catch (Exception ex)
                {
                    string strline = HelpClass.LineNo(ex);
                    string strMsg = HelpClass.SQLEscapeString(ex.Message);
                    HelpClass.showMsg(this, this.GetType(), "VMSSupplyList13:" + strline + " " + strMsg);
                }
            }
        }

        protected void btnSupplyReset_Click(object sender, EventArgs e)
        {
            ddlDepartment.SelectedIndex = 0;
            ddlEmp.Items.Clear();
            txtDateFrom.Text = txtDateTo.Text = string.Empty;
            SnacksGridView.Visible = false;
            keyIssueGridView.Visible = false;
            AccessGridView.Visible = false;
        }

        protected void lnkCheckOutKey_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton bt = (LinkButton)sender;
                GridViewRow gr = (GridViewRow)bt.NamingContainer;
                Label lbl_id = (Label)gr.FindControl("lblGVKeySno");
                Label lbl_Name = (Label)gr.FindControl("lblGVKeyEmployeeName");
                LinkButton lnk = (LinkButton)gr.FindControl("lnkCheckOutKey");
                objSupplyRegisterBO = new SupplyRegisterBO();
                objSupplyRegisterBO.SNo = Convert.ToInt32(lbl_id.Text);
                objSupplyRegisterBO.ReturnDateTime = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                objSupplyRegisterBO.EmployeeName = lbl_Name.Text;
                int c = objSupplyRegisterBAL.CheckoutOutwardKey(objSupplyRegisterBO);
                if (c > 0)
                {
                    HelpClass.showMsg(this, this.GetType(), " " + lbl_Name.Text + "  Ckecked Out Successfully");
                    KeyBind_Data();
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList14:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList14:" + strline + " " + strMsg);
            }
        }

        protected void keyIssueGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIntime = (Label)(e.Row.FindControl("lblGVKeyReturnDateTime"));
                    LinkButton lbtnCheckin = (LinkButton)(e.Row.FindControl("lnkCheckOutKey"));
                    if (lblIntime != null)
                    {
                        if (lblIntime.Text.Length > 6)
                            lblIntime.Visible = true;
                        else
                            lbtnCheckin.Visible = true;
                    }
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        Label lblOuttime = (Label)(e.Row.FindControl("lblGVKeyReturnDateTime"));
                        LinkButton lnkbtnEdit = (LinkButton)(e.Row.FindControl("LinkKey"));
                        if (lblOuttime != null)
                        {
                            if (lblOuttime.Text.Length > 5)
                            {
                                lnkbtnEdit.Visible = false;
                            }
                            else
                            {
                                lnkbtnEdit.Visible = true;
                            }
                        }
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList21:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList21:" + strline + " " + strMsg);
            }
        }

        protected void lnkCheckOutAccess_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton bt = (LinkButton)sender;
                GridViewRow gr = (GridViewRow)bt.NamingContainer;
                Label lbl_id = (Label)gr.FindControl("lblGVAccessSno");
                Label lbl_Name = (Label)gr.FindControl("lblGVAccessCardEmployeeName");
                LinkButton lnk = (LinkButton)gr.FindControl("lnkCheckOutAccess");
                objSupplyRegisterBO = new SupplyRegisterBO();
                objSupplyRegisterBO.SNo = Convert.ToInt32(lbl_id.Text);
                objSupplyRegisterBO.ReturnDateTime = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                objSupplyRegisterBO.EmployeeName = lbl_Name.Text;
                int c = objSupplyRegisterBAL.CheckoutOutwardAccess(objSupplyRegisterBO);
                if (c > 0)
                {
                    HelpClass.showMsg(this, this.GetType(), " " + lbl_Name.Text + "   Checked Out Successfully");
                    AccessCardBind_Data();
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList15:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList15:" + strline + " " + strMsg);
            }
        }

        protected void AccessGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIntime = (Label)(e.Row.FindControl("lblGVAccessReturnDateTime"));
                    LinkButton lbtnCheckin = (LinkButton)(e.Row.FindControl("lnkCheckOutAccess"));
                    if (lblIntime != null)
                    {
                        if (lblIntime.Text.Length > 6)
                            lblIntime.Visible = true;
                        else
                            lbtnCheckin.Visible = true;
                    }
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblOuttime = (Label)(e.Row.FindControl("lblGVAccessReturnDateTime"));
                    LinkButton lnkbtnEdit = (LinkButton)(e.Row.FindControl("LinkAccessCard"));
                    if (lblOuttime != null)
                    {
                        if (lblOuttime.Text.Length > 5)
                        {
                            lnkbtnEdit.Visible = false;
                        }
                        else
                        {
                            lnkbtnEdit.Visible = true;
                        }
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList16:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList16:" + strline + " " + strMsg);
            }
        }

        protected void ddlSupplyType_SelectedIndexChanged(object sender, EventArgs e)
        {
            
                btnSupplyReset_Click(null, null);           
        }

        private void Employee()
        {
            try
            {
                if (ddlDepartment.SelectedIndex == 0)
                {
                    ddlEmp.Items.Clear();
                }
                else
                {
                    ddlEmp.Enabled = false;
                    ddlEmp.Items.Clear();
                    ddlEmp.Items.Insert(0, new ListItem("All", "0"));
                    VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                    int DeptID = Convert.ToInt32(ddlDepartment.SelectedValue);
                    if (DeptID >= 0)
                    {
                        dt = objVisitorRegisterBAL.GetEmpData(DeptID);
                        ddlEmp.DataSource = dt;
                        ddlEmp.DataTextField = "Name";
                        ddlEmp.DataValueField = "EmpID";
                        ddlEmp.DataBind();
                        ddlEmp.Items.Insert(0, new ListItem("All", "0"));
                        ddlEmp.Enabled = true;
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList17:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList17:" + strline + " " + strMsg);
            }
        }

        private void SnacksEmployee()
        {
            try
            {
                if (ddlSnacksDepartment.SelectedIndex == 0)
                {
                    ddlSnacksEmp.Items.Clear();
                }
                else
                {
                   
                    VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                    int DeptID1 = Convert.ToInt32(ddlSnacksDepartment.SelectedValue);
                    if (DeptID1 >= 0)
                    {
                        DataTable dtSnacks = objVisitorRegisterBAL.GetEmpData(DeptID1);
                        ddlSnacksEmp.DataSource = dtSnacks;
                        ddlSnacksEmp.DataTextField = "Name";
                        ddlSnacksEmp.DataValueField = "EmpID";
                        ddlSnacksEmp.DataBind();
                        ddlSnacksEmp.Items.Insert(0, new ListItem("--Select--", "0"));
                       
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList18:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList18:" + strline + " " + strMsg);
            }
        }

        private void KeySupplyEmployee()
        {
            try
            {
                if (ddlKeyDepartment.SelectedIndex == 0)
                {
                    ddlKeyEmp.Items.Clear();
                }
                else
                {
                   
                    VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                    int DeptID2 = Convert.ToInt32(ddlKeyDepartment.SelectedValue);
                    if (DeptID2 >= 0)
                    {
                        DataTable dtKey = objVisitorRegisterBAL.GetEmpData(DeptID2);
                        ddlKeyEmp.DataSource = dtKey;
                        ddlKeyEmp.DataTextField = "Name";
                        ddlKeyEmp.DataValueField = "EmpID";
                        ddlKeyEmp.DataBind();
                        ddlKeyEmp.Items.Insert(0, new ListItem("--Select--", "0"));
                       
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList19:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList19:" + strline + " " + strMsg);
            }
        }

        private void AccessSupplyEmployee()
        {
            try
            {
                if (ddlAccessDepartment.SelectedIndex == 0)
                {
                    ddlAccessEmp.Items.Clear();
                }
                else
                {
                   
                    VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                    int DeptID3 = Convert.ToInt32(ddlAccessDepartment.SelectedValue);
                    if (DeptID3 >= 0)
                    {
                        DataTable dtAccess = objVisitorRegisterBAL.GetEmpData(DeptID3);
                        ddlAccessEmp.DataSource = dtAccess;
                        ddlAccessEmp.DataTextField = "Name";
                        ddlAccessEmp.DataValueField = "EmpID";
                        ddlAccessEmp.DataBind();
                        ddlAccessEmp.Items.Insert(0, new ListItem("--Select--", "0"));
                        
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList20:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList20:" + strline + " " + strMsg);
            }
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDepartment.SelectedIndex == 0)
            {
                ddlEmp.Items.Clear();
            }
            Employee();
        }

        protected void ddlSnacksDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            SnacksEmployee();
        }

        protected void ddlKeyDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            KeySupplyEmployee();
        }

        protected void ddlAccessDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            AccessSupplyEmployee();
        }

        protected void SnacksGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            SnacksGridView.PageIndex = e.NewPageIndex;
            if (ViewState["dtSnacks"] != null)
            {
                SnacksGridView.DataSource = (DataTable)ViewState["dtSnacks"];
                SnacksGridView.DataBind();
            }
        }

        protected void keyIssueGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            keyIssueGridView.PageIndex = e.NewPageIndex;
            if (ViewState["dtKey"] != null)
            {
                keyIssueGridView.DataSource = (DataTable)ViewState["dtKey"];
                keyIssueGridView.DataBind();
            }
        }

        protected void AccessGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            AccessGridView.PageIndex = e.NewPageIndex;
            if (ViewState["dtAccess"] != null)
            {
                AccessGridView.DataSource = (DataTable)ViewState["dtAccess"];
                AccessGridView.DataBind();
            }
        }

        private void BindSupplyType()
        {
            try
            {
                DataTable dtMaterial = new DataTable();
                dtMaterial = objSupplyRegisterBAL.BindSupplyType();
                ddlSupplyType.DataTextField = "SupplyTypeName";
                ddlSupplyType.DataValueField = "SupplyTypeID";
                ddlSupplyType.DataSource = dtMaterial;
                ddlSupplyType.DataBind();
                ddlSupplyType.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList22:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSupplyList22:" + strline + " " + strMsg);
            }
        }
    }
}