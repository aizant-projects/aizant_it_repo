﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/NestedVMS_Master.master" AutoEventWireup="true" CodeBehind="VmsHomePage.aspx.cs" Inherits="AizantIT_PharmaApp.VmsHomePage" %>

<%@ Register Assembly="DevExpress.XtraCharts.v18.1.Web, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="dx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="VMSBody" runat="server">
     <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../AppCSS/dashboardstyle.css" rel="stylesheet" />
    <asp:HiddenField runat="server" ID="hdFromDate" Value="= '<%=this.Fromdate %>'"></asp:HiddenField>

    <div class=" col-lg-12 col-sm-12 col-xs-12 col-md-12 dms_outer_border">

        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 grid_header">Dashboard</div>

        <%--Cards--%>
        <div class="col-lg-3 col-sm-6 col-xs-12 col-md-4 top fade-inTms one" id="divCardshtml" runat="server">
        </div>
        <%-- Start Hr Div--%>

        <%-- End Hr Div--%>
        <%-- Visitor Charts--%>
        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 dashboard_msgVMS" id="divNoTask" runat="server" visible="false">"Currently there are no tasks to show on the dashboard."</div>
        <div class="" runat="server" id="divVisitorCharts" visible="false">
            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12" style="border-bottom: 1px solid #07889a">
                    <div class="col-sm-2 padding-none ">
                        <div class="form-group" runat="server" id="div2">
                            <label class="control-label col-sm-12 lbl" id="lblChartType">Chart Type</label>
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
                               
                                <asp:DropDownList ID="ddlChartType" CssClass="form-control" runat="server">
                                    <asp:ListItem Value="1">Visitor List</asp:ListItem>
                                    <asp:ListItem Value="2">Purpose of Visit</asp:ListItem>
                                    <asp:ListItem Value="3">Vehicle</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <asp:UpdatePanel runat="server" ID="upReportType" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="col-sm-2 padding-none ">
                                <div class="form-group" runat="server" id="div1">
                                    <label class="control-label col-sm-12 lbl" id="lblReportType">Report Type</label>
                                    <div class="col-sm-12">
                                        <asp:DropDownList ID="ddlReportType" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlReportType_SelectedIndexChanged">
                                            <asp:ListItem Value="1">Week</asp:ListItem>
                                            <asp:ListItem Value="2">Month</asp:ListItem>
                                            <asp:ListItem Value="3">Year</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel runat="server" ID="upcontrolsVisible" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="col-sm-2 padding-none" id="div_MonthYear" runat="server" visible="false">
                                <div class="form-group" runat="server">
                                    <label class="control-label col-sm-12 lbl" id="lblyear">Enter Year</label>
                                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
                                        <asp:TextBox ID="txtMonthYear" CssClass="form-control" runat="server" placeholder="Enter Year"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2 padding-none ">
                                <div class="form-group" runat="server" id="div_FromDate" visible="false">
                                    <label class="control-label col-sm-12 lbl" id="lblFromDate">From Date</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtSearchVisitorsFromDate"  CssClass="form-control" runat="server" placeholder="Select From Date"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group" runat="server" id="div_FromMonths" visible="false">
                                    <label class="control-label col-sm-12 lbl" id="lblFromMonths">From Months</label>
                                    <div class="col-sm-12">
                                        <asp:DropDownList ID="ddlfromMonths" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlfromMonths_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group" runat="server" id="div_FromYear" visible="false">
                                    <label class="control-label col-sm-12 lbl" id="lblFromYear">From Year</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtFromYear" CssClass="form-control" runat="server" placeholder="Enter Year" MaxLength="4" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2 bottom">
                                <div class="form-group" runat="server" id="div_ToDate" visible="false">
                                    <label class="control-label col-sm-12  lbl" id="lblToDate">To Date</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtSearchVisitorsToDate" runat="server" CssClass="form-control" placeholder="Select To Date"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group" runat="server" id="div_ToMonth" visible="false">
                                    <label class="control-label col-sm-12  lbl" id="lblToMonths">To Month</label>
                                    <div class="col-sm-12">
                                        <asp:DropDownList ID="ddlToMonth" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group" runat="server" id="div_ToYear" visible="false">
                                    <label class="control-label col-sm-12  lbl" id="lblToYears">To year</label>
                                    <div class="col-sm-12">
                                         <asp:TextBox ID="txtToYear" CssClass="form-control" runat="server" placeholder="Enter Year" MaxLength="4" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="col-lg-2 float-right top" style="margin-top: 20px;">
                        <asp:UpdatePanel ID="asd" runat="server">
                            <ContentTemplate>
                                
                                <asp:Button ID="btnReset" class=" btn-revert_popup float-right " Style="margin-right: 4px;" OnClientClick="javascript:return Clearfromtodate();" Text="Reset" runat="server" OnClick="btnReset_Click" />
                                <asp:LinkButton ID="btnFind1" runat="server" CssClass="float-right  btn-signup_popup  " OnClick="btnFind1_Click">Submit</asp:LinkButton>
                                  <asp:LinkButton ID="lbtnExport" runat="server" CssClass="float-right  btn-signup_popup  " OnClick="lbtnExport_Click" Visible="false">Export</asp:LinkButton>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnFind1" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="upChart1">
                        <ContentTemplate>
                             <div class="col-md-12  grid_panel_full bottom float-left">
                            <dx:WebChartControl ID="WebChartControl1" runat="server"  CrosshairEnabled="False"  Width="1200" Height="500"
                                PaletteName="Flow"  EnableCallbackAnimation="True" BackColor="#FFFFFF" ToolTipEnabled="True">
                                <EmptyChartText Text="No Records Found" TextColor="Red" Font="Tahoma, 15.75pt" Tag="No Records Found" />
                                <DiagramSerializable>
                                    <dx:XYDiagram>
                                        <AxisX VisibleInPanesSerializable="-1">
                                            <VisualRange Auto="False" AutoSideMargins="False" MaxValueSerializable="0" MinValueSerializable="0" SideMarginsValue="1" />
                                            <WholeRange AutoSideMargins="False" MaxValueSerializable="0" SideMarginsValue="1" />
                                            <NumericScaleOptions AutoGrid="False" />
                                        </AxisX>
                                        <AxisY VisibleInPanesSerializable="-1">
                                            <NumericScaleOptions AutoGrid="False" />
                                        </AxisY>
                                    </dx:XYDiagram>
                                </DiagramSerializable>
                                <Legend Name="Default Legend" Visibility="False"><Title Text="{A}{V}"></Title></Legend>
                                <Legends>
                                    <dx:Legend Name="Legend1">
                                    </dx:Legend>
                                    <dx:Legend Name="Legend2">
                                    </dx:Legend>
                                </Legends>
                                <SeriesSerializable>
                                    <dx:Series Name="Series 2" LabelsVisibility="True" ToolTipEnabled="True" ToolTipPointPattern="{A}:{V}">
                                        <LabelSerializable>
                                            <dx:SideBySideBarSeriesLabel Position="Top" ShowForZeroValues="True" TextPattern="{V}">
                                            </dx:SideBySideBarSeriesLabel>
                                        </LabelSerializable>
                                    </dx:Series>
                                </SeriesSerializable>
                                <SeriesTemplate ToolTipEnabled="True" ToolTipPointPattern="{A}:{V}">
                                </SeriesTemplate>
                                <Titles>
                                    <dx:ChartTitle Font="Tahoma, 18pt, style=Underline" Text="Visitors List" />
                                </Titles>
                                <ClientSideEvents ObjectHotTracked="function(s, e) {
     var hitInCategory = e.hitInfo.inSeries &amp;&amp; e.chart.series[0].visible;
     var hitInBackTitle = e.hitInfo.inChartTitle &amp;&amp; !e.chart.series[0].visible;
     s.SetCursor((hitInCategory || hitInBackTitle) ? 'pointer' : 'default');    
 }"
                                    ObjectSelected="function(s, e) {
     var hitInCategory = e.hitInfo.inSeries &amp;&amp; e.chart.series[0].visible;
     var hitInBackTitle = e.hitInfo.inChartTitle &amp;&amp; !e.chart.series[0].visible;
     window.location = 'VMS/VisitorRegister/VisitorRegister.aspx'

//e.processOnServer = hitInCategory || hitInBackTitle;
 }
 " />
                            </dx:WebChartControl>
                                 </div>
                              <div class="col-md-12  grid_panel_full bottom float-left">
                            <dx:WebChartControl ID="WebChartControl2" runat="server"  CrosshairEnabled="True"  RightToLeft="True" SelectionMode="Multiple"  PaletteName="Apex"  EnableCallbackAnimation="True" ToolTipEnabled="True" Width="1200" Height="500">
                                <EmptyChartText Text="No Records Found" TextColor="Red" Font="Tahoma, 15.75pt" />
                                <Legend Name="Default Legend"></Legend>
                                <Legends>
                                    <dx:Legend Name="Legend1" Visibility="True">
                                    </dx:Legend>
                                </Legends>
                                <SeriesSerializable>
                                    <dx:Series Name="Series 2">
                                        <ViewSerializable>
                                            <dx:PieSeriesView>
                                            </dx:PieSeriesView>
                                        </ViewSerializable>
                                    </dx:Series>
                                </SeriesSerializable>
                                <Titles>
                                    <dx:ChartTitle Font="Tahoma, 18pt, style=Underline" Text="Purpose of Visit" TextColor="Black" Visibility="True" />
                                </Titles>
                                <ClientSideEvents ObjectHotTracked="function(s, e) {
     var hitInCategory = e.hitInfo.inSeries &amp;&amp; e.chart.series[0].visible;
     var hitInBackTitle = e.hitInfo.inChartTitle &amp;&amp; !e.chart.series[0].visible;
     s.SetCursor((hitInCategory || hitInBackTitle) ? 'pointer' : 'default');    
 }"
                                    ObjectSelected="function(s, e) {
     var hitInCategory = e.hitInfo.inSeries &amp;&amp; e.chart.series[0].visible;
     var hitInBackTitle = e.hitInfo.inChartTitle &amp;&amp; !e.chart.series[0].visible;
     window.location = 'VMS/VisitorRegister/VisitorRegister.aspx'

//e.processOnServer = hitInCategory || hitInBackTitle;
 }
 " />
                            </dx:WebChartControl>
                                  </div>
                             <div class="col-md-12  grid_panel_full bottom float-left">
                            <dx:WebChartControl ID="WebChartControl3" runat="server" CrosshairEnabled="True"  RightToLeft="True" SelectionMode="Multiple"  PaletteName="Apex"  EnableCallbackAnimation="True" ToolTipEnabled="True" Width="1200" Height="500">
                                <EmptyChartText Text="No Records Found" TextColor="Red" Font="Tahoma, 15.75pt" />
                                <Legend Name="Default Legend"></Legend>
                                <Legends>
                                    <dx:Legend Name="Legend1" Visibility="True">
                                    </dx:Legend>
                                </Legends>
                                <SeriesSerializable>
                                    <dx:Series Name="Series 2">
                                        <ViewSerializable>
                                            <dx:PieSeriesView>
                                            </dx:PieSeriesView>
                                        </ViewSerializable>
                                    </dx:Series>
                                </SeriesSerializable>
                                <Titles>
                                    <dx:ChartTitle Font="Tahoma, 18pt, style=Underline" Text="Vehicle" TextColor="Black" Visibility="True" />
                                </Titles>
                                <ClientSideEvents ObjectHotTracked="function(s, e) {
     var hitInCategory = e.hitInfo.inSeries &amp;&amp; e.chart.series[0].visible;
     var hitInBackTitle = e.hitInfo.inChartTitle &amp;&amp; !e.chart.series[0].visible;
     s.SetCursor((hitInCategory || hitInBackTitle) ? 'pointer' : 'default');    
 }"
                                    ObjectSelected="function(s, e) {
     var hitInCategory = e.hitInfo.inSeries &amp;&amp; e.chart.series[0].visible;
     var hitInBackTitle = e.hitInfo.inChartTitle &amp;&amp; !e.chart.series[0].visible;
     window.location = 'VMS/VehicleRegister/VechileRegister.aspx'

//e.processOnServer = hitInCategory || hitInBackTitle;
 }
 " />
                            </dx:WebChartControl>
                                  </div>
                            
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnFind1" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>

        <%--End Visitor Charts--%>
    </div>
     <!----To Load Calendar ---->
    <script>
        $(document).ready(function () {
            //Binding Code
            myfunction();
              
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                //Binding Code Again
                myfunction();
            }
        });
    </script>
    <!----End To Load Calendar for Appointment Date---->
  <script>
      function myfunction() {
          //for start date and end date validations startdate should be < enddate
          $(function () {
              $('#<%=txtSearchVisitorsFromDate.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        useCurrent: false,
                    });

                    $('#<%=txtSearchVisitorsToDate.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        useCurrent: false,
                    });
                    $('#<%=txtSearchVisitorsFromDate.ClientID%>').on("dp.change", function (e) {
                        $('#<%=txtSearchVisitorsToDate.ClientID%>').data("DateTimePicker").minDate(e.date);
                        var _mindate = new Date(e.date);
                        _mindate.setDate(_mindate.getDate() + 6)
                        $('#<%=txtSearchVisitorsToDate.ClientID%>').val(_mindate.format('dd MMM yyyy'));
                        $('#<%=txtSearchVisitorsToDate.ClientID%>').data("DateTimePicker").maxDate(moment().add(7, 'days'));
                    });
          });
      }
    </script>

    <script type="text/javascript">
        //for datepickers
        var startdate2 = document.getElementById('<%=txtSearchVisitorsFromDate.ClientID%>').value
        if (startdate2 == "") {
            $('#<%=txtSearchVisitorsFromDate.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                useCurrent: false,
            });
            $('#<%=txtSearchVisitorsToDate.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                useCurrent: false,
            });
        }
    </script>
    
    <script>
        function Clearfromtodate() {
            document.getElementById('<%=txtSearchVisitorsFromDate.ClientID%>').value = '';
            document.getElementById('<%=txtSearchVisitorsToDate.ClientID%>').value = '';
        }
    </script>

    <script type="text/javascript">

        window.onload = function () {
            getDate();
        };

        function getDate() {
            var m_names = new Array("Jan", "Feb", "Mar",
                "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                "Oct", "Nov", "Dec");
            var dtTO = new Date();
            var curr_date = dtTO.getDate();
            var curr_month = dtTO.getMonth();
            var curr_year = dtTO.getFullYear();
            var dtFrom = new Date(dtTO.getTime() - (6 * 24 * 60 * 60 * 1000));
            var Todate = document.getElementById("<%=txtSearchVisitorsToDate.ClientID%>");
            Todate.value = curr_date + " " + m_names[curr_month] + " " + curr_year;

            var Fromdate = document.getElementById("<%=txtSearchVisitorsFromDate.ClientID%>");
            var curr_date1 = dtFrom.getDate();
            var curr_month1 = dtFrom.getMonth();
            var curr_year1 = dtFrom.getFullYear();
            Fromdate.value = curr_date1 + " " + m_names[curr_month1] + " " + curr_year1;
        }
    </script>

</asp:Content>
