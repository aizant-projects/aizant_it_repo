﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using AizantIT_PharmaApp.Common;


namespace AizantIT_PharmaApp.VMS.HomePage
{
    public partial class VMS_HomePage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                       
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            ViewState["EmpID"] = "0";
                            if (HelpClass.IsUserAuthenticated())
                            {
                                ViewState["EmpID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            }
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSVRL14:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSVRL14:" + strline + " " + strMsg);
            }
        }

        private void InitializeThePage()
        {
            try
            {
                ChartFill();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSVR17:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSVR17:" + strline + " " + strMsg);
            }
        }

      
        public void ChartFill()
        {
            
            string str = ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ConnectionString;
            SqlConnection conn = new SqlConnection(str);
            SqlDataAdapter da = new SqlDataAdapter("select sum(NoofVisitors) as 'NoofVisitors',CONVERT(VARCHAR(11), InDateTime, 106) as 'InDateTime' from VMS.AizantIT_VisitorRegister group by CONVERT(VARCHAR(11), InDateTime, 106)", conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            DataTable ChartData = ds.Tables[0];
            //storing total rows count to loop on each Record  
            string[] XPointMember = new string[ChartData.Rows.Count];
            int[] YPointMember = new int[ChartData.Rows.Count];
            for (int count = 0; count < ChartData.Rows.Count; count++)
            {
                //storing Values for X axis  
                XPointMember[count] = ChartData.Rows[count]["InDateTime"].ToString();
                //storing values for Y Axis  
                YPointMember[count] = Convert.ToInt32(ChartData.Rows[count]["NoofVisitors"]);
            }
            //binding chart control  
            Chart1.Series[0].Points.DataBindXY(XPointMember, YPointMember);
            //Setting width of line  
            Chart1.Series[0].BorderWidth = 10;
            //setting Chart type   
            //Chart1.Series[0].ChartType = SeriesChartType.Column;
            //Chart1.Series[0].ChartType = SeriesChartType.StackedColumn;  

            //Hide or show chart back GridLines  
            Chart1.ChartAreas["ChartArea1"].AxisX.MajorGrid.Enabled = false;
            Chart1.ChartAreas["ChartArea1"].AxisY.MajorGrid.Enabled = false;

            //Enabled 3D  
            //Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;  
            conn.Close();
        }
    }
}

