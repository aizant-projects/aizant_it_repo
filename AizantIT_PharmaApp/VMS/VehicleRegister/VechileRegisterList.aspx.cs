﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using VMS_BAL;
using VMS_BO;
using AizantIT_PharmaApp.Common;
using System.Data.SqlClient;


namespace AizantIT_PharmaApp.VMS.VechileRegister
{
    public partial class VechileRegisterList : System.Web.UI.Page
    {

        VechileRegisterBAL objVechileRegisterBAL = new VechileRegisterBAL();
        ServiceAndVechileBO objServiceAndVechileBO;
        DataTable dt;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            ViewState["EmpID"] = "0";
                            if (HelpClass.IsUserAuthenticated())
                            {
                                ViewState["EmpID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            }
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVERL10:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVERL10:" + strline + " " + strMsg,"error");
            }
        }

        private void InitializeThePage()
        {
            try
            {
                screenAuthentication();
                GetDataVechileLatest();
                LoadVechileNo();
                LoadDriverNames();
                txtIndateTime.Text = DateTime.Now.ToString("dd MM yyyy HH:mm:ss");
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVERL11:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVERL11:" + strline + " " + strMsg,"error");
            }
        }

        private void LoadVechileNo()
        {
            try
            {
                dt = new DataTable();
                dt = objVechileRegisterBAL.LoadVechileNo();
                ddlVechileNoSearch.DataTextField = dt.Columns["VechileNo"].ToString();
                ddlVechileNoSearch.DataValueField = dt.Columns["VechileID"].ToString();
                ddlVechileNoSearch.DataSource = dt;
                ddlVechileNoSearch.DataBind();
                ddlVechileNoSearch.Items.Insert(0, new ListItem("--Select--", "0"));

                ddlVRVechileNo.DataTextField = dt.Columns["VechileNo"].ToString();
                ddlVRVechileNo.DataValueField = dt.Columns["VechileID"].ToString();
                ddlVRVechileNo.DataSource = dt;
                ddlVRVechileNo.DataBind();
                ddlVRVechileNo.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVERL1:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVERL1:" + strline + " " + strMsg,"error");
            }
        }

        private void LoadDriverNames()
        {
            try
            {
                dt = new DataTable();
                dt = objVechileRegisterBAL.GetDriverNames(objServiceAndVechileBO);
                ddlDriverName.DataTextField = dt.Columns["Name"].ToString();
                ddlDriverName.DataValueField = dt.Columns["EmpID"].ToString();
                ddlDriverName.DataSource = dt;
                ddlDriverName.DataBind();
                ddlDriverName.Items.Insert(0, new ListItem("--Select--", "0"));

                ddlEditDriverName.DataTextField = dt.Columns["Name"].ToString();
                ddlEditDriverName.DataValueField = dt.Columns["EmpID"].ToString();
                ddlEditDriverName.DataSource = dt;
                ddlEditDriverName.DataBind();
                ddlEditDriverName.Items.Insert(0, new ListItem("--Select--", "0"));

            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVER2:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVER2:" + strline + " " + strMsg,"error");
            }
        }

        //Vechile Readings Find Code
        protected void btnVechileNoSearchFind_Click(object sender, EventArgs e)
        {
            try
            {
                objServiceAndVechileBO = new ServiceAndVechileBO();
                objServiceAndVechileBO.VechileID = Convert.ToInt32(ddlVechileNoSearch.SelectedValue.Trim());
                objServiceAndVechileBO.FirstName = txtSearchFirstName.Text.Trim();
                objServiceAndVechileBO.PlaceVisited = txtVRSearchPlaceVisited.Text.Trim();

                if (!string.IsNullOrEmpty(txtVRDateFrom.Text.Trim()))
                {
                    objServiceAndVechileBO.InDateTime = Convert.ToDateTime(txtVRDateFrom.Text.Trim()).ToString("yyyyMMdd");
                }
                else
                    objServiceAndVechileBO.InDateTime = txtVRDateFrom.Text.Trim();
                if (!string.IsNullOrEmpty(txtVRDateTo.Text.Trim()))
                {
                    objServiceAndVechileBO.OutDateTime = Convert.ToDateTime(txtVRDateTo.Text.Trim()).ToString("yyyyMMdd");
                }
                else
                    objServiceAndVechileBO.OutDateTime = txtVRDateTo.Text.Trim();
                objServiceAndVechileBO.SNo = 0;
                dt = new DataTable();
                dt = objVechileRegisterBAL.SearchFilterVechileReading(objServiceAndVechileBO);
                GridViewVechileReadings.DataSource = dt;
                GridViewVechileReadings.DataBind();
                ViewState["dtvechile"] = dt;
                GridViewVechileReadings.Visible = true;
                ViewState["isVehicleDefaultData"] = false;
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVERL3:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVERL3:" + strline + " " + strMsg,"error");
            }
        }

        private void GetDataVechileLatest()
        {
            objServiceAndVechileBO = new ServiceAndVechileBO();
            objServiceAndVechileBO.VechileID = 0;
            objServiceAndVechileBO.FirstName = string.Empty;
            objServiceAndVechileBO.PlaceVisited = string.Empty;
            DateTime date = DateTime.Now.AddDays(-7);
            objServiceAndVechileBO.InDateTime = date.ToString("yyyyMMdd");
            ViewState["vwIndatetime"] = objServiceAndVechileBO.InDateTime;
            objServiceAndVechileBO.OutDateTime = DateTime.Now.ToString("yyyyMMdd");
            ViewState["vwOutdatetime"] = objServiceAndVechileBO.OutDateTime;
            objServiceAndVechileBO.SNo = 0;
            dt = new DataTable();
            dt = objVechileRegisterBAL.SearchFilterVechileReading(objServiceAndVechileBO);
            GridViewVechileReadings.DataSource = dt;
            GridViewVechileReadings.DataBind();
            ViewState["dtvechile"] = dt;
            GridViewVechileReadings.Visible = true;
            ViewState["isVehicleDefaultData"] = true;
        }

        protected void btnVechileNoSearchReset_Click(object sender, EventArgs e)
        {
            ddlVechileNoSearch.SelectedIndex = 0;
            txtVRSearchPlaceVisited.Text = txtVRDateFrom.Text = txtVRDateTo.Text = txtSearchFirstName.Text = string.Empty;
            ddlDriverName.SelectedIndex = 0;
            GridViewVechileReadings.Visible = false;
            GetDataVechileLatest();
        }

        private void VechileReadingsReset()
        {
            txtVRDateFrom.Text = txtVRDateTo.Text = txtVRDistanceKms.Text = txtVREditComments.Text = txtVRInReading.Text = txtVRMobileNo.Text = txtVROutReading.Text = txtVRPlaceVisited.Text = txtVRSearchPlaceVisited.Text = string.Empty;
            ddlVechileNoSearch.SelectedIndex = 0;
            ddlDriverName.SelectedIndex = 0;
            txtFirstName.Text = txtLastName.Text = string.Empty;
        }

        protected void btnVechileCancel_Click(object sender, EventArgs e)
        {
            VechileReadingsReset();
            this.ModalPopupVechile.Hide();
        }

        //Vechile Register BindData
        private void VechileRegisterBind_Data()
        {
            try
            {
               
                if (Convert.ToBoolean(ViewState["isVehicleDefaultData"]) == false)
                {
                    btnVechileNoSearchFind_Click(null, null);
                }
                else
                {
                    GetDataVechileLatest();
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVERL4:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVERL4:" + strline + " " + strMsg,"error");
            }
        }

        //Vechile Register Link Button Code
        protected void lnkVechileEdit_Click1(object sender, EventArgs e)
        {
            try
            {
                objServiceAndVechileBO = new ServiceAndVechileBO();
                objServiceAndVechileBO.VechileID = 0;
                objServiceAndVechileBO.FirstName = string.Empty;
                objServiceAndVechileBO.PlaceVisited = string.Empty;
                objServiceAndVechileBO.InDateTime = string.Empty;
                objServiceAndVechileBO.OutDateTime = string.Empty;
                LinkButton lnk = sender as LinkButton;
                GridViewRow gr = (GridViewRow)lnk.NamingContainer;
                string tempID = GridViewVechileReadings.DataKeys[gr.RowIndex].Value.ToString();
                objServiceAndVechileBO.SNo = Convert.ToInt32(tempID);
                ViewState["tempId1"] = tempID;
                dt = new DataTable();
                dt = objVechileRegisterBAL.SearchFilterVechileReading(objServiceAndVechileBO);
                txtVRSno.Text = dt.Rows[0]["SNo"].ToString();
                ddlVRVechileNo.SelectedValue = dt.Rows[0]["VechileID"].ToString();
                txtVRMobileNo.Text = dt.Rows[0]["MobileNo"].ToString();
                txtVRPlaceVisited.Text = dt.Rows[0]["PlaceVisited"].ToString();
                txtVROutReading.Text = dt.Rows[0]["OutReading"].ToString();
                txtVRInReading.Text = dt.Rows[0]["InReading"].ToString();
                txtVRTotalKms.Text = dt.Rows[0]["TotalKiloMeters"].ToString();
                txtVRDistanceKms.Text = dt.Rows[0]["DistanceKMS"].ToString();
                txtVREditComments.Text = dt.Rows[0]["LastChangedByComments"].ToString();
                txtVROutDateTime.Text = dt.Rows[0]["OutDateTime"].ToString();
                txtVRRemarks.Text = dt.Rows[0]["Remarks"].ToString();
                txtFirstName.Text = dt.Rows[0]["FirstName"].ToString();
                txtLastName.Text = dt.Rows[0]["LastName"].ToString();
                                this.ModalPopupVechile.Show();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVERL5:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVERL5:" + strline + " " + strMsg,"error");
            }
        }

        protected void txtVRInReading_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtVRInReading.Text.Trim()) && !string.IsNullOrEmpty(txtVROutReading.Text.Trim()))
                {
                    int a = 0, b = 0, d = 0;
                    a = Convert.ToInt32(txtVROutReading.Text.Trim());
                    b = Convert.ToInt32(txtVRInReading.Text.Trim());
                    d = b - a;
                    txtVRDistanceKms.Text = d.ToString();
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVERL6:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVERL6:" + strline + " " + strMsg,"error");
            }
        }

        protected void GridViewVechileReadings_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewVechileReadings.PageIndex = e.NewPageIndex;
            if (ViewState["dtvechile"] != null)
            {
                GridViewVechileReadings.DataSource = (DataTable)ViewState["dtvechile"];
                GridViewVechileReadings.DataBind();
            }
        }

        protected void btnVRUpdate_Click1(object sender, EventArgs e)
        {
            try
            {
                objServiceAndVechileBO = new ServiceAndVechileBO();
                objServiceAndVechileBO.SNo = Convert.ToInt32(txtVRSno.Text.Trim());
                objServiceAndVechileBO.VechileID = Convert.ToInt32(ddlVRVechileNo.SelectedValue.Trim());
                if (!string.IsNullOrEmpty(txtVRInReading.Text.Trim()))
                {
                    objServiceAndVechileBO.InReading = Convert.ToInt64(txtVRInReading.Text.Trim());
                }
                else
                {
                    objServiceAndVechileBO.InReading = 0;
                }
                objServiceAndVechileBO.DistanceKMS = Convert.ToInt64(txtVRDistanceKms.Text.Trim());
                                objServiceAndVechileBO.FirstName = txtFirstName.Text.Trim();
                objServiceAndVechileBO.LastName = txtLastName.Text.Trim();
                objServiceAndVechileBO.MobileNo = txtVRMobileNo.Text.Trim();
                objServiceAndVechileBO.PlaceVisited = txtVRPlaceVisited.Text.Trim();
                if (!string.IsNullOrEmpty(txtVRInReading.Text.Trim()))
                {
                    objServiceAndVechileBO.OutReading = Convert.ToInt64(txtVROutReading.Text.Trim());
                }
                else
                {
                    objServiceAndVechileBO.OutReading = 0;
                }
                
                objServiceAndVechileBO.LastChangedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                objServiceAndVechileBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                objServiceAndVechileBO.InDateTime = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                objServiceAndVechileBO.LastChangedComments = txtVREditComments.Text.Trim();
                
                objServiceAndVechileBO.Remarks = txtVRRemarks.Text.Trim();
                int c = objVechileRegisterBAL.Update_VechileRegister(objServiceAndVechileBO);
                if (c > 0)
                {
                    HelpClass.custAlertMsg(btnVRUpdate, btnVRUpdate.GetType(), "Vehicle Register Details Updated Successfully.","success");
                }
                else
                {
                    HelpClass.custAlertMsg(btnVRUpdate, btnVRUpdate.GetType(), "Vehicle Register Details Updation Failed.","error");
                }
                VechileRegisterBind_Data();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVERL7:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVERL7:" + strline + " " + strMsg,"error");
            }
        }

        protected void GridViewVechileReadings_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIntime = (Label)(e.Row.FindControl("lblGVVechileInDateTime"));
                    LinkButton lbtnCheckin = (LinkButton)(e.Row.FindControl("lnkCheckOutVechile"));
                    if (lblIntime != null)
                    {
                        if (lblIntime.Text.Length > 6)
                            lblIntime.Visible = true;
                        else
                            lbtnCheckin.Visible = true;
                    }
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblOuttime = (Label)(e.Row.FindControl("lblGVVechileInDateTime"));
                    LinkButton lnkbtnEdit = (LinkButton)(e.Row.FindControl("lnkVechileEdit"));
                    if (lblOuttime != null)
                    {
                        if (lblOuttime.Text.Length > 5)
                        {
                            lnkbtnEdit.Visible = true;
                        }
                        else
                        {
                            lnkbtnEdit.Visible = true;
                        }
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVERL8:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVERL8:" + strline + " " + strMsg,"error");
            }

        }

        protected void lnkCheckOutVechile_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton bt = (LinkButton)sender;
                GridViewRow gr = (GridViewRow)bt.NamingContainer;
                Label lbl_id = (Label)gr.FindControl("lblGVVechileSno");
                LinkButton lnk = (LinkButton)gr.FindControl("lnkCheckOutVechile");
                Label lbl_VechileNo = (Label)gr.FindControl("lblGVVechileNo");
                objServiceAndVechileBO = new ServiceAndVechileBO();
                objServiceAndVechileBO.SNo = Convert.ToInt32(lbl_id.Text);
                objServiceAndVechileBO.InDateTime = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                objServiceAndVechileBO.VechileNo = lbl_VechileNo.Text;
                int c = objVechileRegisterBAL.CheckoutVechile(objServiceAndVechileBO);
                if (c > 0)
                {
                    HelpClass.showMsg(this, this.GetType(), "Vehicle No " + lbl_VechileNo.Text + " Checked In Successfully.");
                    VechileRegisterBind_Data();
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVERL9:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVERL9:" + strline + " " + strMsg,"error");
            }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetPlaceToVisitList(string prefixText)
        {
            ServiceAndVechileBO objServiceAndVechileBO = new ServiceAndVechileBO();
            VechileRegisterBAL objVechileRegisterBAL = new VechileRegisterBAL();
            objServiceAndVechileBO.VechileID = 0;
            objServiceAndVechileBO.FirstName = string.Empty;
            objServiceAndVechileBO.PlaceVisited = prefixText;
            objServiceAndVechileBO.InDateTime = string.Empty;
            objServiceAndVechileBO.OutDateTime = string.Empty;
            objServiceAndVechileBO.SNo = 0;
            DataTable dt = new DataTable();
            dt = objVechileRegisterBAL.SearchFilterVechileReading(objServiceAndVechileBO);
            var distinctPlaceToVisit = (from row in dt.AsEnumerable()
                                        select row.Field<string>("PlaceVisited")).Distinct();
            List<string> VechiledistinctPlaceToVisit = distinctPlaceToVisit.ToList();
            return VechiledistinctPlaceToVisit;
        }

        void screenAuthentication()
        {
            DataTable dtAthuntication = GmsHelpClass.GetRolePrivileges();
            if (dtAthuntication.Rows.Count > 0)
            {
                //modid=2,PrivilegeID=
                int Checkoutcount = dtAthuntication.AsEnumerable()
        .Count(row => row.Field<int>("PrivilegeID") == 7);
                int Checkincount = dtAthuntication.AsEnumerable()
           .Count(row => row.Field<int>("PrivilegeID") == 8);
                int Modifycount = dtAthuntication.AsEnumerable()
         .Count(row => row.Field<int>("PrivilegeID") == 9);
                 if (Checkincount == 0 && Checkoutcount == 0 && Modifycount == 0)
                 {
                    Response.Redirect("~/UserLogin.aspx");
                 }
                GridViewVechileReadings.Columns[10].HeaderText = "VIEW";
                if (Modifycount == 0)
                 {
                    btnVRUpdate.Visible = false;
                    ddlVRVechileNo.Enabled = false;
                    txtFirstName.Enabled = false;
                    txtLastName.Enabled = false;
                    txtVRMobileNo.Enabled = false;
                    txtVRPlaceVisited.Enabled = false;
                    txtVROutReading.Enabled = false; 
                 }

                if (Modifycount > 0)
                {
                    GridViewVechileReadings.Columns[10].HeaderText = "EDIT";
                    btnVRUpdate.Visible = true;
                                    }
                if (Checkincount > 0)
                {
                    GridViewVechileReadings.Columns[10].HeaderText = "EDIT";//CHECKIN
                    btnVRUpdate.Visible = true;
                    ddlVRVechileNo.Enabled = false;
                    txtFirstName.Enabled = false;
                    txtLastName.Enabled = false;
                    txtVRMobileNo.Enabled = false;
                    txtVRPlaceVisited.Enabled = false;
                    txtVROutReading.Enabled = false;
                }
            }
        }
    }
}