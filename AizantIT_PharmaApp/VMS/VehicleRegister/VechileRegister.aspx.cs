﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using VMS_BAL;
using VMS_BO;
using AizantIT_PharmaApp.Common;
using System.Data.SqlClient;

namespace AizantIT_PharmaApp.VMS.VechileRegister
{
    public partial class VechileRegister : System.Web.UI.Page
    {
        VechileRegisterBAL objVechileRegisterBAL = new VechileRegisterBAL();
        DataTable dt;
        ServiceAndVechileBO objServiceAndVechileBO;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    txtVROutDateTime.Attributes.Add("readonly","readonly");
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            ViewState["EmpID"] = "0";
                            if (HelpClass.IsUserAuthenticated())
                            {
                                ViewState["EmpID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            }
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVER6:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVER6:" + strline + " " + strMsg,"error");
            }
        }

        private void InitializeThePage()
        {
            try
            {
                screenAuthentication();               
                LoadVechileNo();
                LoadDriverNames();
                ddlVRVechileNo.Focus();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVER5:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVER5:" + strline + " " + strMsg,"error");
            }
        }
       

        private void LoadVechileNo()
        {
            try
            {
                dt = new DataTable();
                dt = objVechileRegisterBAL.LoadVechileNo();
                ddlVRVechileNo.DataTextField = dt.Columns["VechileNo"].ToString();
                ddlVRVechileNo.DataValueField = dt.Columns["VechileID"].ToString();
                ddlVRVechileNo.DataSource = dt;
                ddlVRVechileNo.DataBind();
                ddlVRVechileNo.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVER1:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVER1:" + strline + " " + strMsg,"error");
            }
        }

        private void LoadDriverNames()
        {
            try
            {
                dt = new DataTable();
                dt = objVechileRegisterBAL.GetDriverNames(objServiceAndVechileBO);
                ddlDriverName.DataTextField = dt.Columns["Name"].ToString();
                ddlDriverName.DataValueField = dt.Columns["EmpID"].ToString();
                ddlDriverName.DataSource = dt;
                ddlDriverName.DataBind();
                ddlDriverName.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVER2:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVER2:" + strline + " " + strMsg,"error");
            }
        }

        protected void btnVechileSubmit_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtVRMobileNo.Text.Trim() == "" || txtVROutReading.Text.Trim() == "" || txtVRPlaceVisited.Text.Trim() == "" || txtFirstName.Text.Trim()=="" || txtLastName.Text.Trim()=="" /*ddlDriverName.SelectedValue == "0"*/)
                {
                    HelpClass.custAlertMsg(btnVechileSubmit, btnVechileSubmit.GetType(), "All fields represented with * are mandatory.","error");
                    return;
                }
                if (txtVechileNo.Visible == true)
                {
                    if (string.IsNullOrEmpty(txtVechileNo.Text.Trim()))
                    {
                        HelpClass.custAlertMsg(btnVechileSubmit, btnVechileSubmit.GetType(), "Please Enter Vehicle No.","error");
                        return;
                    }
                }
                if (ddlVRVechileNo.Visible == true)
                {
                    if (ddlVRVechileNo.SelectedIndex == 0)
                    {
                        HelpClass.custAlertMsg(btnVechileSubmit, btnVechileSubmit.GetType(), "Please Select Vehicle No","error");
                        return;
                    }
                }
                string AddVechileNo;
                if (txtVechileNo.Visible == true)
                {
                    objServiceAndVechileBO = new ServiceAndVechileBO();
                    objServiceAndVechileBO.VechileNo = txtVechileNo.Text.Trim();
                    AddVechileNo = objVechileRegisterBAL.AddVechlieNoMaster(objServiceAndVechileBO).ToString();
                    if (AddVechileNo == "0")
                    {
                        lblError.Text = "Vehicle No Already Exists.";
                        return;
                    }
                    else
                    {
                        lblError.Text = "";
                    }
                }
                else
                {
                    AddVechileNo = ddlVRVechileNo.SelectedValue;
                }
                objServiceAndVechileBO = new ServiceAndVechileBO();
                objServiceAndVechileBO.VechileID = Convert.ToInt32(AddVechileNo);
                objServiceAndVechileBO.FirstName = txtFirstName.Text.Trim();
                objServiceAndVechileBO.LastName = txtLastName.Text.Trim();
                objServiceAndVechileBO.MobileNo = txtVRMobileNo.Text.Trim();
                objServiceAndVechileBO.PlaceVisited = txtVRPlaceVisited.Text.Trim();
                
                objServiceAndVechileBO.OutDateTime = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");// txtVROutDateTime.Text.Trim();
                objServiceAndVechileBO.Remarks = txtVRRemarks.Text.Trim();
                if (!string.IsNullOrEmpty(txtVROutReading.Text.Trim()))
                {
                    objServiceAndVechileBO.OutReading = Convert.ToInt64(txtVROutReading.Text.Trim());
                }
                else
                {
                    objServiceAndVechileBO.OutReading = 0;
                }
               
                objServiceAndVechileBO.CreatedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                objServiceAndVechileBO.CreatedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                int c = objVechileRegisterBAL.VechileReading_Insert(objServiceAndVechileBO);
                if (c > 0)
                {
                    LoadVechileNo();
                    ddlVRVechileNo.SelectedIndex = 0;
                    VechileReadingsReset();
                    HelpClass.custAlertMsg(btnVechileSubmit, btnVechileSubmit.GetType(), "Vehicle Register Details Submitted Successfully.","success");
                }
                else
                {
                    HelpClass.custAlertMsg(btnVechileSubmit, btnVechileSubmit.GetType(), "Vehicle Register Details Submission Failed.","error");
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVER3:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVER3:" + strline + " " + strMsg,"error");
            }  
        }

        protected void btnVechileReset_Click(object sender, EventArgs e)
        {
            ddlVRVechileNo.SelectedIndex = 0;
            VechileReadingsReset();
        }

        private void ResetddlServicetype()
        {
            txtVROutDateTime.Text = txtVRMobileNo.Text = txtVRPlaceVisited.Text = txtVROutReading.Text = txtVRRemarks.Text = string.Empty;
            ddlDriverName.SelectedIndex = 0;
            lblError.Text = "";
            txtVechileNo.Text = "";
            txtFirstName.Text = txtLastName.Text = string.Empty;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", " getDate();", true);
        }
        private void VechileReadingsReset()
        {
            ResetddlServicetype();
            if (btnVechileNo.Text == "<<")
            {
                btnVechileNo.Text = "+";
                ddlVRVechileNo.Visible = true;
                txtVechileNo.Visible = false;
            }
        }

        protected void btnVechileNo_Click(object sender, EventArgs e)
        {
            if (btnVechileNo.Text == "+")
            {
                btnVechileNo.Text = "<<";
                txtVechileNo.Visible = true;
                ddlVRVechileNo.Visible = false;
                ResetddlServicetype();
            }
            else if (btnVechileNo.Text == "<<")
            {
                btnVechileNo.Text = "+";
                txtVechileNo.Visible = false;
                ddlVRVechileNo.Visible = true;
                ResetddlServicetype();
                ddlVRVechileNo.SelectedIndex = 0;
            }
        }

        protected void txtVechileNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", " getDate();", true);
            if (txtVechileNo.Visible == true)
            {
                if (string.IsNullOrEmpty(txtVechileNo.Text.Trim()))
                {
                    HelpClass.custAlertMsg(btnVechileSubmit, btnVechileSubmit.GetType(), "Please Enter Vehicle No.","error");
                    return;
                }
            }
            int Count = objVechileRegisterBAL.VechileNoifExistsorNot(txtVechileNo.Text.Trim());
            if (Count > 0)
            {
                lblError.Text = "Vehicle No Already Exists";
            }
            else
            {
                lblError.Text = "";
            }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVER4:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVER4:" + strline + " " + strMsg,"error");
            }
        }

        protected void ddlVRVechileNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            VechileReadingsReset();
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetPlaceToVisitList(string prefixText)
        {
            ServiceAndVechileBO objServiceAndVechileBO = new ServiceAndVechileBO();
            VechileRegisterBAL objVechileRegisterBAL = new VechileRegisterBAL();
            objServiceAndVechileBO.VechileID = 0;
            objServiceAndVechileBO.FirstName = string.Empty;
            objServiceAndVechileBO.PlaceVisited = prefixText;
            objServiceAndVechileBO.InDateTime = string.Empty;
            objServiceAndVechileBO.OutDateTime = string.Empty;
            objServiceAndVechileBO.SNo = 0;
            DataTable dt = new DataTable();
            dt = objVechileRegisterBAL.SearchFilterVechileReading(objServiceAndVechileBO);
            var distinctPlaceToVisit = (from row in dt.AsEnumerable()
                                    select row.Field<string>("PlaceVisited")).Distinct();
            List<string> VechiledistinctPlaceToVisit = distinctPlaceToVisit.ToList();
            return VechiledistinctPlaceToVisit;
        }


        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetDriverNamesList(string prefixText)
        {
            ServiceAndVechileBO objServiceAndVechileBO = new ServiceAndVechileBO();
            VechileRegisterBAL objVechileRegisterBAL = new VechileRegisterBAL();
            objServiceAndVechileBO.VechileID = 0;
            objServiceAndVechileBO.FirstName = prefixText;
            objServiceAndVechileBO.PlaceVisited = string.Empty; 
            objServiceAndVechileBO.InDateTime = string.Empty;
            objServiceAndVechileBO.OutDateTime = string.Empty;
            objServiceAndVechileBO.SNo = 0;
            DataTable dt = new DataTable();
            dt = objVechileRegisterBAL.SearchFilterVechileReading(objServiceAndVechileBO);
            var distinctDriverNames = (from row in dt.AsEnumerable()
                                        select row.Field<string>("Name")).Distinct();
            List<string> VechiledistinctDriverNames = distinctDriverNames.ToList();
            return VechiledistinctDriverNames;
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetDriverList(string prefixText)
        {
            ServiceAndVechileBO objServiceAndVechileBO = new ServiceAndVechileBO();
            VechileRegisterBAL objVechileRegisterBAL = new VechileRegisterBAL();
            objServiceAndVechileBO.VechileID = 0;
            objServiceAndVechileBO.FirstName = prefixText;
            objServiceAndVechileBO.PlaceVisited = string.Empty;
            objServiceAndVechileBO.InDateTime = string.Empty;
            objServiceAndVechileBO.OutDateTime = string.Empty;
            objServiceAndVechileBO.SNo = 0;
            DataTable dt = new DataTable();
            dt = objVechileRegisterBAL.SearchFilterVechileReading(objServiceAndVechileBO);
            var distinctDriverNames = (from row in dt.AsEnumerable()
                                       select row.Field<string>("FirstName")).Distinct();
            List<string> VechiledistinctDriverNames = distinctDriverNames.ToList();
            return VechiledistinctDriverNames;
        }

        void screenAuthentication()
        {
            DataTable dtAthuntication = GmsHelpClass.GetRolePrivileges();
            if (dtAthuntication.Rows.Count > 0)
            {
                //modid=2,PrivilegeID=7

                int CheckOutcount = dtAthuntication.AsEnumerable()
           .Count(row => row.Field<int>("PrivilegeID") == 7);
                if (CheckOutcount == 0)
                    Response.Redirect("~/UserLogin.aspx");
            }
        }
    }
}