﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/NestedVMS_Master.master" AutoEventWireup="true" CodeBehind="VechileRegister.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.VechileRegister.VechileRegister" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content2" ContentPlaceHolderID="VMSBody" runat="server">
    <div style="margin-top: 10px;">
        <div class="row Panel_Frame" style="width: 900px; margin-left: 17%">
            <div id="header">
                <div class="col-sm-12 Panel_Header">
                    <div class="col-sm-6" style="padding-left: 5px">
                        <span class="Panel_Title">Vehicle Check Out</span>
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div>
                     <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                <ContentTemplate>
                    <div class="Row">
                        <div class="col-sm-6">

                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <div class="form-horizontal" style="margin: 5px;">
                                        <div class="form-group" runat="server" id="div8">
                                            <label class="control-label col-sm-4" id="lblVechileNo" style="text-align: left; font-weight: lighter">Vehicle No<span class="mandatoryStar">*</span></label><asp:Button ID="btnVechileNo" runat="server" CssClass="btnnVisit" Text="+" Font-Bold="true" OnClick="btnVechileNo_Click" />
                                            <div class="col-sm-7">
                                                <asp:TextBox ID="txtVechileNo" runat="server" CssClass="form-control" placeholder="Enter Vehicle No" Visible="false" AutoPostBack="true" OnTextChanged="txtVechileNo_TextChanged"></asp:TextBox>
                                                <asp:DropDownList ID="ddlVRVechileNo" runat="server" CssClass="form-control SearchDropDown" TabIndex="1" AutoPostBack="true" OnSelectedIndexChanged="ddlVRVechileNo_SelectedIndexChanged"></asp:DropDownList>
                                                <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Size="Smaller"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlVRVechileNo" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>

                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <div class="form-horizontal" style="margin: 5px;">
                                        <div class="form-group" runat="server" id="div5">
                                            <label class="control-label col-sm-4" id="lblDriverName" style="text-align: left; font-weight: lighter">Driver Name<span class="mandatoryStar">*</span></label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddlDriverName" runat="server" CssClass="form-control SearchDropDown" AutoPostBack="true" Visible="false"></asp:DropDownList>
                                                <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" placeholder="Enter First Name" TabIndex="2" onkeypress="return ValidateAlpha(event)" onkeyup="SetContextKey()" onpaste="return false" maxlength="50"></asp:TextBox>
                                                <asp:AutoCompleteExtender MinimumPrefixLength="1" ServiceMethod="GetDriverList" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1"
                                                 TargetControlID="txtFirstName" ID="AutoCompleteExtender1" UseContextKey="true" runat="server" FirstRowSelected="false"></asp:AutoCompleteExtender>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlDriverName" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div7">
                                    <label class="control-label col-sm-4" id="lblLastName" style="text-align: left; font-weight: lighter">Last Name<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" placeholder="Enter Last Name" onkeypress="return ValidateAlpha(event)" TabIndex="3" onpaste="return false" maxlength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div1">
                                    <label class="control-label col-sm-4" id="lblMobileNo" style="text-align: left; font-weight: lighter">Mobile No<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtVRMobileNo" runat="server" CssClass="form-control" placeholder="Enter Mobile No" MaxLength="10" TabIndex="4" onkeypress="return ValidatePhoneNo(event);"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                             <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div2">
                                    <label class="control-label col-sm-4" id="lblPlaceToVisit" style="text-align: left; font-weight: lighter">Place To Visit<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtVRPlaceVisited" runat="server" CssClass="form-control" placeholder="Place To Visit" TabIndex="5" onkeypress="return event.keyCode != 13;"></asp:TextBox>
                                        <asp:AutoCompleteExtender ServiceMethod="GetPlaceToVisitList" MinimumPrefixLength="1" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1"
                                         TargetControlID="txtVRPlaceVisited" ID="AutoCompleteExtender2" UseContextKey="true" runat="server" FirstRowSelected="false"></asp:AutoCompleteExtender>
                                    </div>
                                </div>
                            </div>

                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div3">
                                    <label class="control-label col-sm-4" id="lblVechileOutReading" style="text-align: left; font-weight: lighter">Out Reading<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtVROutReading" runat="server" CssClass="form-control" placeholder="Enter Out Reading" TabIndex="6" onkeypress="if(event.keyCode<48 || event.keyCode>57)event.returnValue=false;"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div4">
                                    <label class="control-label col-sm-4" id="lblVechileOutDateTime" style="text-align: left; font-weight: lighter">OutDateTime</label>
                                    <div class="col-sm-7"> 
                                        <asp:TextBox ID="txtVROutDateTime" runat="server" BackColor="White" CssClass="form-control"  TabIndex="7" onkeypress="return event.keyCode != 13;"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div6">
                                    <label class="control-label col-sm-4" id="lblRemarks" style="text-align: left; font-weight: lighter">Remarks</label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtVRRemarks" runat="server" CssClass="form-control" placeholder="Enter Remarks" TabIndex="8" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                        <asp:Button ID="btnVechileSubmit" Text="Submit" CssClass="button" runat="server" OnClick="btnVechileSubmit_Click" TabIndex="9" OnClientClick="javascript:return Submitvaidate();" />
                        <asp:Button ID="btnVechileReset" Text="Reset" CssClass="button" runat="server" OnClick="btnVechileReset_Click" TabIndex="10" CausesValidation="false" />
                    </div>
                    </div>
        </div>
    
                    <script>
                        window.onload = function () {
                            getDate();
                        };
                        function getDate() {
                            var dtTO = new Date();
                            var Todate = document.getElementById("<%=txtVROutDateTime.ClientID%>");
                        Todate.value = formatDate(dtTO);
                        }
                        function formatDate(dateObj) {
                            var m_names = new Array("Jan", "Feb", "Mar",
                                "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                                "Oct", "Nov", "Dec");
                            var curr_date = dateObj.getDate();
                            var curr_month = dateObj.getMonth();
                            var curr_year = dateObj.getFullYear();
                            var curr_min = dateObj.getMinutes();
                            var curr_hr = dateObj.getHours();
                            var curr_sc = dateObj.getSeconds();
                            return curr_date + " " + m_names[curr_month] + " " + curr_year + " " + curr_hr + ":" + curr_min + ":" + curr_sc;
                        }
    </script>

                    <script type="text/javascript">
                        function Submitvaidate() {
                        var Vechile = document.getElementById("<%=ddlVRVechileNo.ClientID%>");
                        var FirstName = document.getElementById("<%=txtFirstName.ClientID%>").value;
                        var LastName = document.getElementById("<%=txtLastName.ClientID%>").value;
                        var Mobile = document.getElementById("<%=txtVRMobileNo.ClientID%>").value;
                        var PlacetoVisit = document.getElementById("<%=txtVRPlaceVisited.ClientID%>").value;
                        var Outreading = document.getElementById("<%=txtVROutReading.ClientID%>").value;

                        errors = [];
                        if (Vechile.value == 0) {
                            errors.push("Please Select Vehicle Number.");
                        }
                        
                        if (FirstName.trim() == "") {
                            errors.push("Please Enter First Name.");
                        }
                        if (LastName.trim() == "") {
                            errors.push("Please Enter Last Name.");
                        }
                        if (Mobile.trim() == "") {
                            errors.push("Please Enter Mobile No.");
                        }
                        else
                        {
                            if (Mobile.length != 10)
                            {
                                errors.push("Mobile No Should be 10 Digits.");
                            }
                        }
                        if (PlacetoVisit.trim() == "") {
                            errors.push("Please Enter Place to Visit.");
                        }
                        if (Outreading.trim() == "") {
                            errors.push("Please Enter Vechile OutReading.");
                        }

                        if (errors.length > 0) {
                            custAlertMsg(errors.join("<br/>"), "error");
                            return false;
                        }
                        }
                    </script>
                    <script>
                        $("#NavLnkVechile").attr("class", "active");
                        $("#VechileRegister").attr("class", "active");
    </script>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
