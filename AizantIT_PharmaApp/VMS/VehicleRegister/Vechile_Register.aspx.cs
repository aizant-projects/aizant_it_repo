﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using VMS_BAL;
using VMS_BO;
using AizantIT_PharmaApp.Common;
using System.Data.SqlClient;
using System.Collections;
using System.Text;

namespace AizantIT_PharmaApp.VMS.VehicleRegister
{
    public partial class Vechile_Register : System.Web.UI.Page
    {
        VechileRegisterBAL objVechileRegisterBAL = new VechileRegisterBAL();
        DataTable dt;
        ServiceAndVechileBO objServiceAndVechileBO;
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ConfirmCustomAlert.buttonClick += new EventHandler(ConfirmCustomAlert_buttonClick);
                if (!IsPostBack)
                {
                    txtVROutDateTime.Attributes.Add("readonly", "readonly");
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            ViewState["EmpID"] = "0";
                            if (HelpClass.IsUserAuthenticated())
                            {
                                ViewState["EmpID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                ViewState["vwRoleID"] = Session["sesGMSRoleID"].ToString();
                            }
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVEH1:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVEH1:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVEH1:" + strline + " " + strMsg, "error");
            }
        }

        private void InitializeThePage()
        {
            try
            {
                screenAuthentication();               
                LoadVechileNo();
                LoadDriverNames();
                ddlVRVechileNo.Focus();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVEH2:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVEH2:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVEH2:" + strline + " " + strMsg, "error");
            }
        }
        

        private void LoadVechileNo()
        {
            try
            {
                DataTable dtVehicleNo = objVechileRegisterBAL.LoadVechileNo();
                ddlVRVechileNo.DataTextField = dtVehicleNo.Columns["VechileNo"].ToString();
                ddlVRVechileNo.DataValueField = dtVehicleNo.Columns["VechileID"].ToString();
                ddlVRVechileNo.DataSource = dtVehicleNo;
                ddlVRVechileNo.DataBind();
                ddlVRVechileNo.Items.Insert(0, new ListItem("--Select--", "0"));

            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVEH3:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVEH3:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVEH3:" + strline + " " + strMsg, "error");
            }
        }

        private void LoadDriverNames()
        {
            try
            {
                dt = new DataTable();
                dt = objVechileRegisterBAL.GetDriverNames(objServiceAndVechileBO);
                ddlDriverName.DataTextField = dt.Columns["Name"].ToString();
                ddlDriverName.DataValueField = dt.Columns["EmpID"].ToString();
                ddlDriverName.DataSource = dt;
                ddlDriverName.DataBind();
                ddlDriverName.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVEH4:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVEH4:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVEH4:" + strline + " " + strMsg, "error");
            }
        }

        protected void btnVechileSubmit_Click(object sender, EventArgs e)
        {
            if (txtVRMobileNo.Text.Trim() == "" || txtVRMobileNo.Text.Length != 10 || txtVROutReading.Text.Trim() == "" || txtVRPlaceVisited.Text.Trim() == "" || txtFirstName.Text.Trim() == "" || txtLastName.Text.Trim() == "" /*ddlDriverName.SelectedValue == "0"*/)
            {
                ArrayList Mandatory = new ArrayList();
                if (txtVechileNo.Visible == true)
                {
                    if (string.IsNullOrEmpty(txtVechileNo.Text.Trim()))
                    {
                        Mandatory.Add("Please enter vehicle no.");
                    }
                }
                if (ddlVRVechileNo.Visible == true)
                {
                    if (ddlVRVechileNo.SelectedIndex == 0)
                    {
                        Mandatory.Add("Please select vehicle no.");
                    }
                }
                if (string.IsNullOrEmpty(txtFirstName.Text.Trim()))
                {
                    Mandatory.Add("Please enter driver first name.");
                }
                if (String.IsNullOrEmpty(txtLastName.Text.Trim()))
                {
                    Mandatory.Add("Please enter driver last name.");
                }
                if (string.IsNullOrEmpty(txtVRMobileNo.Text.Trim()))
                {
                    Mandatory.Add("Please enter mobile no.");
                }
                else if (txtVRMobileNo.Text.Length != 10)
                {
                    Mandatory.Add("Mobile no should be 10 digits.");
                }
                if (string.IsNullOrEmpty(txtVRPlaceVisited.Text.Trim()))
                {
                    Mandatory.Add("Please enter place to visit.");
                }
                if (String.IsNullOrEmpty(txtVROutReading.Text.Trim()))
                {
                    Mandatory.Add("Please enter vehicle out reading.");
                }
                StringBuilder s = new StringBuilder();
                foreach (string x in Mandatory)
                {
                    s.Append(x);
                    s.Append("<br/>");
                }
                HelpClass.custAlertMsg(this, this.GetType(), "" + s.ToString() + "", "error");
                return;
            }
            hdnVehicleCustom.Value = "Submit";
            HelpClass.custAlertMsg(this, this.GetType(), "Do you want to submit ?", "confirm");
        }

        protected void btnVechileReset_Click(object sender, EventArgs e)
        {
            // btnVechileUpdate.Visible = false;
            ddlVRVechileNo.SelectedIndex = 0;
            VechileReadingsReset();
            upVehicleModal.Update();
        }
        
        private void ResetddlServicetype()
        {
            txtVROutDateTime.Text = txtVRMobileNo.Text = txtVRPlaceVisited.Text = txtVROutReading.Text = txtVRRemarks.Text = string.Empty;
           
            txtVechileNo.Text = "";
            txtFirstName.Text = txtLastName.Text = string.Empty;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", " getDate();", true);
        }
        private void VechileReadingsReset()
        {
            ResetddlServicetype();
            if (btnVechileNo.Text == "<<")
            {
                btnVechileNo.Text = "+";
                ddlVRVechileNo.Visible = true;
                txtVechileNo.Visible = false;
                               upVehicleModal.Update();
            }
        }

        protected void btnVechileNo_Click(object sender, EventArgs e)
        {
            if (btnVechileNo.Text == "+")
            {
                btnVechileNo.Text = "<<";
                txtVechileNo.Visible = true;
                ddlVRVechileNo.Visible = false;
                ResetddlServicetype();
                            }
            else if (btnVechileNo.Text == "<<")
            {
                btnVechileNo.Text = "+";
                txtVechileNo.Visible = false;
                ddlVRVechileNo.Visible = true;
                ResetddlServicetype();
                ddlVRVechileNo.Items.Clear();
               
            }
            LoadVechileNo();
        }

        protected void txtVechileNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", " getDate();", true);
                if (txtVechileNo.Visible == true)
                {
                    if (string.IsNullOrEmpty(txtVechileNo.Text.Trim()))
                    {
                        HelpClass.custAlertMsg(btnVechileSubmit, btnVechileSubmit.GetType(), "Please enter vehicle no.", "error");
                        return;
                    }
                }
                int Count = objVechileRegisterBAL.VechileNoifExistsorNot(txtVechileNo.Text.Trim());
                if (Count > 0)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Vehicle no already exists.", "error");
                }
               
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVEH5:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVEH5:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVEH5:" + strline + " " + strMsg, "error");
            }
        }

        protected void ddlVRVechileNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            VechileReadingsReset();
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetPlaceToVisitList(string prefixText)
        {
            ServiceAndVechileBO objServiceAndVechileBO = new ServiceAndVechileBO();
            VechileRegisterBAL objVechileRegisterBAL = new VechileRegisterBAL();
            objServiceAndVechileBO.VechileID = 0;
            objServiceAndVechileBO.FirstName = string.Empty;
            objServiceAndVechileBO.PlaceVisited = prefixText;
            objServiceAndVechileBO.InDateTime = string.Empty;
            objServiceAndVechileBO.OutDateTime = string.Empty;
            objServiceAndVechileBO.SNo = 0;
            DataTable dt = new DataTable();
            dt = objVechileRegisterBAL.SearchFilterVechileReading(objServiceAndVechileBO);
            var distinctPlaceToVisit = (from row in dt.AsEnumerable()
                                        select row.Field<string>("PlaceVisited")).Distinct();
            List<string> VechiledistinctPlaceToVisit = distinctPlaceToVisit.ToList();
            return VechiledistinctPlaceToVisit;
        }


        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetDriverNamesList(string prefixText)
        {
            ServiceAndVechileBO objServiceAndVechileBO = new ServiceAndVechileBO();
            VechileRegisterBAL objVechileRegisterBAL = new VechileRegisterBAL();
            objServiceAndVechileBO.VechileID = 0;
            objServiceAndVechileBO.FirstName = prefixText;
            objServiceAndVechileBO.PlaceVisited = string.Empty;
            objServiceAndVechileBO.InDateTime = string.Empty;
            objServiceAndVechileBO.OutDateTime = string.Empty;
            objServiceAndVechileBO.SNo = 0;
            DataTable dt = new DataTable();
            dt = objVechileRegisterBAL.SearchFilterVechileReading(objServiceAndVechileBO);
          
            var distinctDriverNames = (from row in dt.AsEnumerable()
                                       select row.Field<string>("Name")).Distinct();
            List<string> VechiledistinctDriverNames = distinctDriverNames.ToList();
            return VechiledistinctDriverNames;
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetDriverList(string prefixText)
        {
            ServiceAndVechileBO objServiceAndVechileBO = new ServiceAndVechileBO();
            VechileRegisterBAL objVechileRegisterBAL = new VechileRegisterBAL();
            objServiceAndVechileBO.VechileID = 0;
            objServiceAndVechileBO.FirstName = prefixText;
            objServiceAndVechileBO.PlaceVisited = string.Empty;
            objServiceAndVechileBO.InDateTime = string.Empty;
            objServiceAndVechileBO.OutDateTime = string.Empty;
            objServiceAndVechileBO.SNo = 0;
            DataTable dt = new DataTable();
            dt = objVechileRegisterBAL.SearchFilterVechileReading(objServiceAndVechileBO);
            var distinctDriverNames = (from row in dt.AsEnumerable()
                                       select row.Field<string>("Name")).Distinct();
            List<string> VechiledistinctDriverNames = distinctDriverNames.ToList();
            return VechiledistinctDriverNames;
        }



        //Vehicle Register Edit Button
        protected void btnEditVechicle_Click(object sender, EventArgs e)
        {
            if (ViewState["vwVehicleRegisterView"].ToString() == "0"&& ViewState["vwVehicleInReading"].ToString() == "0"&& ViewState["vwVehicleModifycount"].ToString() == "0")
            {
                return;
            }
                JQDatatableBO objJQDataTableBO = new JQDatatableBO();
            objJQDataTableBO.iMode = 0;
            objJQDataTableBO.pkid = Convert.ToInt32(hdnPkVechileEditID.Value);
            objJQDataTableBO.iDisplayLength = 0;
            objJQDataTableBO.iDisplayStart = 0;
            objJQDataTableBO.iSortCol = 0;
            objJQDataTableBO.sSortDir = "";
            objJQDataTableBO.sSearch = "";
            objVechileRegisterBAL = new VechileRegisterBAL();
            DataTable dt = objVechileRegisterBAL.GetVechile_Details(objJQDataTableBO);
            if (dt.Rows.Count > 0)
            {
                
                LoadVechileNo();
                ddlVRVechileNo.SelectedValue = dt.Rows[0]["VechileID"].ToString();
                txtVRMobileNo.Text = dt.Rows[0]["MobileNo"].ToString();
                txtVRPlaceVisited.Text = dt.Rows[0]["PlaceVisited"].ToString();
                txtVROutReading.Text = dt.Rows[0]["OutReading"].ToString();
                txtVRInReading.Text = dt.Rows[0]["InReading"].ToString();
                txtVRDistanceKms.Text = dt.Rows[0]["DistanceKMS"].ToString();
                
                txtVROutDateTime.Text = dt.Rows[0]["OutDateTime"].ToString();
                txtVRRemarks.Text = dt.Rows[0]["Remarks"].ToString();
                txtFirstName.Text = dt.Rows[0]["FirstName"].ToString();
                txtLastName.Text = dt.Rows[0]["LastName"].ToString();
                
                btnVechileSubmit.Visible = false;
                btnVechileReset.Visible = false;
                div_vechile_comments.Visible = true;
                div_InReading.Visible = true;
                btnVechileUpdate.Visible = true;
                btnVechileNo.Visible = false;
                if (ViewState["vwVehicleInReading"].ToString() != "0")
                {
                    Vehicle_RegisterDisableControls();
                    if (!string.IsNullOrEmpty(dt.Rows[0]["InReading"].ToString()))
                    {
                        if (Convert.ToInt64(dt.Rows[0]["InReading"].ToString()) > 0)
                        {
                            txtVRInReading.Enabled = false;
                            btnVechileUpdate.Visible = false;
                            upVehicleButons.Update();
                        }
                    }
                    else
                    {
                        txtVRInReading.Enabled = true;
                        txtVREditComments.Enabled = true;
                    }
                }
                if (ViewState["vwVehicleRegisterView"].ToString() != "0")
                {
                    Vehicle_RegisterDisableControls();
                    btnVechileUpdate.Visible = false;
                }
                if (ViewState["vwVehicleInReading"].ToString() != "0")
                {
                    ddlVRVechileNo.Enabled = false;
                    txtFirstName.Enabled = false;
                    txtLastName.Enabled = false;
                    txtVRMobileNo.Enabled = false;
                    txtVRPlaceVisited.Enabled = false;
                    txtVROutReading.Enabled = false;
                }

                if (ViewState["vwVehicleModifycount"].ToString() != "0")
                {
                    btnVechileUpdate.Visible = true;
                    ddlVRVechileNo.Enabled = true;
                    txtFirstName.Enabled = true;
                    txtLastName.Enabled = true;
                    txtVRMobileNo.Enabled = true;
                    txtVRPlaceVisited.Enabled = true;
                    txtVROutReading.Enabled = true;
                    txtVRInReading.Enabled = true;
                    txtVREditComments.Enabled = true;
                    txtVRRemarks.Enabled = true;
                }
                upVehicleModal.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "showalert", "OpenModeVehicle();", true);
            }
        }

        protected void btnVechileUpdate_Click(object sender, EventArgs e)
        {
            if (txtVRMobileNo.Text.Trim() == "" || txtVROutReading.Text.Trim() == "" || txtVRPlaceVisited.Text.Trim() == "" || txtFirstName.Text.Trim() == "" || txtLastName.Text.Trim() == "" /*ddlDriverName.SelectedValue == "0"*/)
            {
                ArrayList Mandatory = new ArrayList();
                if (txtVechileNo.Visible == true)
                {
                    if (string.IsNullOrEmpty(txtVechileNo.Text.Trim()))
                    {
                        Mandatory.Add("Please enter vehicle no.");
                    }
                }
                if (ddlVRVechileNo.Visible == true)
                {
                    if (ddlVRVechileNo.SelectedIndex == 0)
                    {
                        Mandatory.Add("Please select vehicle no.");
                    }
                }
                if (string.IsNullOrEmpty(txtFirstName.Text.Trim()))
                {
                    Mandatory.Add("Please enter first name.");
                }
                if (String.IsNullOrEmpty(txtLastName.Text.Trim()))
                {
                    Mandatory.Add("Please enter last name.");
                }
                if (string.IsNullOrEmpty(txtVRMobileNo.Text.Trim()))
                {
                    Mandatory.Add("Please enter mobile no.");
                }
                else if (txtVRMobileNo.Text.Length != 10)
                {
                    Mandatory.Add("Mobile no should be 10 digits.");
                }
                if (string.IsNullOrEmpty(txtVRPlaceVisited.Text.Trim()))
                {
                    Mandatory.Add("Please enter place to visit.");
                }
                if (String.IsNullOrEmpty(txtVROutReading.Text.Trim()))
                {
                    Mandatory.Add("Please enter vechile Outreading.");
                }
                if (String.IsNullOrEmpty(txtVRInReading.Text.Trim()))
                {
                    Mandatory.Add("Please enter vechile in reading.");
                }
                StringBuilder s = new StringBuilder();
                foreach (string x in Mandatory)
                {
                    s.Append(x);
                    s.Append("<br/>");
                }
                HelpClass.custAlertMsg(this, this.GetType(), "" + s.ToString() + "", "error");
                return;
            }
        }

        protected void btnNavVehicleCreate_Click(object sender, EventArgs e)
        {
            LoadVechileNo();
            btnVechileNo.Visible = true;
            btnVechileUpdate.Visible = false;
            div_vechile_comments.Visible = false;
            div_InReading.Visible = false;
            btnVechileSubmit.Visible = true;
            btnVechileReset.Visible = true;

            Vehicle_RegistersEnableControls();
            upVehicleModal.Update();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "showalert", "OpenModelViewVehicle();", true);
        }

        protected void txtVRInReading_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtVRInReading.Text.Trim()) && !string.IsNullOrEmpty(txtVROutReading.Text.Trim()))
                {
                    int a = 0, b = 0, d = 0;
                    a = Convert.ToInt32(txtVROutReading.Text.Trim());
                    b = Convert.ToInt32(txtVRInReading.Text.Trim());
                    d = b - a;
                    txtVRDistanceKms.Text = d.ToString();
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVEH6:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVEH6:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVEH6:" + strline + " " + strMsg, "error");
            }
        }

        protected void txtFirstName_TextChanged(object sender, EventArgs e)
        {
            GetVehicleDetails();
        }

        private void GetVehicleDetails()
        {
            ServiceAndVechileBO objServiceAndVechileBO = new ServiceAndVechileBO();
            VechileRegisterBAL objVechileRegisterBAL = new VechileRegisterBAL();
            objServiceAndVechileBO.VechileID = 0;
            objServiceAndVechileBO.FirstName = txtFirstName.Text;
            objServiceAndVechileBO.PlaceVisited = string.Empty;
            objServiceAndVechileBO.InDateTime = string.Empty;
            objServiceAndVechileBO.OutDateTime = string.Empty;
            objServiceAndVechileBO.SNo = 0;
            DataTable dt = new DataTable();
            dt = objVechileRegisterBAL.SearchFilterVechileReading(objServiceAndVechileBO);
            if (dt.Rows.Count > 0)
            {
                txtFirstName.Text= dt.Rows[0]["FirstName"].ToString();
                txtLastName.Text = dt.Rows[0]["LastName"].ToString();
                txtVRMobileNo.Text= dt.Rows[0]["MobileNo"].ToString();
            }
        }

        protected void ConfirmCustomAlert_buttonClick(object sender, EventArgs e)
        {
            try
            {
                if (hdnVehicleCustom.Value == "Submit")
                {
                    string AddVechileNo;
                    if (txtVechileNo.Visible == true)
                    {
                        objServiceAndVechileBO = new ServiceAndVechileBO();
                        objServiceAndVechileBO.VechileNo = txtVechileNo.Text.Trim();
                        AddVechileNo = objVechileRegisterBAL.AddVechlieNoMaster(objServiceAndVechileBO).ToString();
                        if (AddVechileNo == "0")
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Vehicle no already exists.", "error");
                            return;
                        }
                    }
                    else
                    {
                        AddVechileNo = ddlVRVechileNo.SelectedValue;
                    }
                    objServiceAndVechileBO = new ServiceAndVechileBO();
                    objServiceAndVechileBO.VechileID = Convert.ToInt32(AddVechileNo);
                    objServiceAndVechileBO.FirstName = txtFirstName.Text.Trim();
                    objServiceAndVechileBO.LastName = txtLastName.Text.Trim();
                    objServiceAndVechileBO.MobileNo = txtVRMobileNo.Text.Trim();
                    objServiceAndVechileBO.PlaceVisited = txtVRPlaceVisited.Text.Trim();
                   
                    objServiceAndVechileBO.OutDateTime = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");// txtVROutDateTime.Text.Trim();
                    objServiceAndVechileBO.Remarks = txtVRRemarks.Text.Trim();
                    if (!string.IsNullOrEmpty(txtVROutReading.Text.Trim()))
                    {
                        objServiceAndVechileBO.OutReading = Convert.ToInt64(txtVROutReading.Text.Trim());
                    }
                    else
                    {
                        objServiceAndVechileBO.OutReading = 0;
                    }
                    objServiceAndVechileBO.CreatedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                    objServiceAndVechileBO.CreatedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                    int c = objVechileRegisterBAL.VechileReading_Insert(objServiceAndVechileBO);
                    if (c > 0)
                    {
                        LoadVechileNo();
                        ddlVRVechileNo.SelectedIndex = 0;
                        VechileReadingsReset();
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Vehicle register details submitted successfully.", "success", "ReloadVehicletable();");
                    }
                    else
                    {
                        HelpClass.custAlertMsg(btnVechileSubmit, btnVechileSubmit.GetType(), "Vehicle register details submission failed.", "error");
                    }
                }
                else
                {
                    if (hdnVehicleCustom.Value == "Update")
                    {
                        if (String.IsNullOrEmpty(txtVRInReading.Text.Trim()))
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Please enter vehicle Inreading.", "error");
                            return;
                        }
                        ServiceAndVechileBO objServiceAndVechileBO = new ServiceAndVechileBO();
                        objServiceAndVechileBO.SNo = Convert.ToInt32(hdnPkVechileEditID.Value);
                        objServiceAndVechileBO.VechileID = Convert.ToInt32(ddlVRVechileNo.SelectedValue.Trim());
                        objServiceAndVechileBO.OutReading = Convert.ToInt64(txtVROutReading.Text.Trim());
                        objServiceAndVechileBO.InReading = Convert.ToInt64(txtVRInReading.Text.Trim());
                        objServiceAndVechileBO.FirstName = txtFirstName.Text.Trim();
                        objServiceAndVechileBO.LastName = txtLastName.Text.Trim();
                        objServiceAndVechileBO.MobileNo = txtVRMobileNo.Text.Trim();
                        objServiceAndVechileBO.PlaceVisited = txtVRPlaceVisited.Text.Trim();
                        objServiceAndVechileBO.LastChangedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                        objServiceAndVechileBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                        objServiceAndVechileBO.InDateTime = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                        objServiceAndVechileBO.LastChangedComments = txtVREditComments.Text.Trim();                       
                        objServiceAndVechileBO.Remarks = txtVRRemarks.Text.Trim();
                        objServiceAndVechileBO.DistanceKMS = Convert.ToInt64(txtVRDistanceKms.Text.Trim());
                        int c = objVechileRegisterBAL.Update_VechileRegister(objServiceAndVechileBO);
                        if (c > 0)
                        {
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Vehicle register details updated successfully.", "success", "ReloadVehicletable();");
                        }
                        else
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Vehicle register details updation failed.", "error");
                        }
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVEH7:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVEH7:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVEH7:" + strline + " " + strMsg, "error");
            }
        }

       

        private void Vehicle_RegisterDisableControls()
        {
            txtVechileNo.Enabled = false;
            ddlVRVechileNo.Enabled = false;
            ddlDriverName.Enabled = false;
            txtFirstName.Enabled = false;
            txtLastName.Enabled = false;
            txtVRMobileNo.Enabled = false;
            txtVRPlaceVisited.Enabled = false;
            txtVROutReading.Enabled = false;
            txtVRInReading.Enabled = false;
            txtVRRemarks.Enabled = false;
            txtVRDistanceKms.Enabled = false;
            txtVREditComments.Enabled = false;
        }
        private void Vehicle_RegistersEnableControls()
        {
            txtVechileNo.Enabled = true;
            ddlVRVechileNo.Enabled = true;
            ddlDriverName.Enabled = true;
            txtFirstName.Enabled = true;
            txtLastName.Enabled = true;
            txtVRMobileNo.Enabled = true;
            txtVRPlaceVisited.Enabled = true;
            txtVROutReading.Enabled = true;
            txtVRInReading.Enabled = true;
            txtVRRemarks.Enabled = true;
            txtVRDistanceKms.Enabled = true;
        }

        void screenAuthentication()
        {
            DataTable dtAthuntication = GmsHelpClass.GetRolePrivileges();
            if (dtAthuntication.Rows.Count > 0)
            {
                //modid=2,PrivilegeID=7,8,9,25
                int VehicleCreate = dtAthuntication.AsEnumerable()
            .Count(row => row.Field<int>("PrivilegeID") == 7);//
                int VehicleCheckIn_InReading = dtAthuntication.AsEnumerable()
            .Count(row => row.Field<int>("PrivilegeID") == 8);//VehicleInReading - Checkin
                ViewState["vwVehicleInReading"] = VehicleCheckIn_InReading.ToString();
                int VehicleModifycount = dtAthuntication.AsEnumerable()
            .Count(row => row.Field<int>("PrivilegeID") == 9);
                ViewState["vwVehicleModifycount"] = VehicleModifycount.ToString();
                int VehicleRegisterView = dtAthuntication.AsEnumerable()
            .Count(row => row.Field<int>("PrivilegeID") == 25);
                ViewState["vwVehicleRegisterView"] = VehicleRegisterView.ToString();
               
                if (VehicleCheckIn_InReading == 0 && VehicleCreate == 0 && VehicleModifycount == 0 && VehicleRegisterView == 0)
                {
                    Response.Redirect("~/UserLogin.aspx");
                }
                btnVechileUpdate.Visible = false;
                btnNavVehicleCreate.Visible = false;
                if (VehicleCreate > 0)
                {
                    ddlVRVechileNo.Enabled = true;
                    txtFirstName.Enabled = true;
                    txtLastName.Enabled = true;
                    txtVRMobileNo.Enabled = true;
                    txtVRPlaceVisited.Enabled = true;
                    txtVROutReading.Enabled = true;
                    btnNavVehicleCreate.Visible = true;
                }
                
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }

    }
}