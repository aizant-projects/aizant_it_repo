﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/NestedVMS_Master.master" AutoEventWireup="true" CodeBehind="VechileRegisterList.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.VechileRegister.VechileRegisterList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content2" ContentPlaceHolderID="VMSBody" runat="server">
    <div style="margin-top: 10px;">
        <div class="row Panel_Frame" style="width: 1100px; margin-left: 10%">
            <div id="header">
                <div class="col-sm-12 Panel_Header">
                    <div class="col-sm-6" style="padding-left: 5px">
                        <span class="Panel_Title">Vehicle Check In</span>
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div>
            <div id="body">
               
                <div class="Row">
                    <div class="col-sm-6">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div8">
                                        <label class="control-label col-sm-4" id="lblVechileNo" style="text-align: left; font-weight: lighter">Vehicle No</label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtVechileNo" runat="server" CssClass="form-control" placeholder="Enter Vechile No" Visible="false"></asp:TextBox>
                                            <asp:DropDownList ID="ddlVechileNoSearch" runat="server" CssClass="form-control SearchDropDown" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlVechileNoSearch" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div5">
                                        <label class="control-label col-sm-4" id="lblDriverName" style="text-align: left; font-weight: lighter">Driver Name</label>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlDriverName" runat="server" CssClass="form-control SearchDropDown" AutoPostBack="true" Visible="false"></asp:DropDownList>
                                            <asp:TextBox ID="txtSearchFirstName" runat="server" CssClass="form-control" placeholder="Enter Driver Name" onkeypress="return ValidateAlpha(event)" onkeyup="SetContextKey()" onpaste="return false" MaxLength="50" AutoPostBack="true"></asp:TextBox>
                                            <asp:AutoCompleteExtender MinimumPrefixLength="1" ServiceMethod="GetDriverNamesList" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1"
                                                TargetControlID="txtSearchFirstName" ServicePath="~/VMS/VechileRegister/VechileRegister.aspx" ID="AutoCompleteExtender3" UseContextKey="true" runat="server" FirstRowSelected="false">
                                            </asp:AutoCompleteExtender>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlDriverName" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>

                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div2">
                                <label class="control-label col-sm-4" id="lblPlaceToVisit" style="text-align: left; font-weight: lighter">Place To Visit</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtVRSearchPlaceVisited" runat="server" CssClass="form-control" placeholder="Place To Visit"></asp:TextBox>
                                    <asp:AutoCompleteExtender ServiceMethod="GetPlaceToVisitList" MinimumPrefixLength="1" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1"
                                        TargetControlID="txtVRSearchPlaceVisited" ID="AutoCompleteExtender2" UseContextKey="true" runat="server" FirstRowSelected="false">
                                    </asp:AutoCompleteExtender>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div4">
                                <label class="control-label col-sm-4" id="labelFromDate" style="text-align: left; font-weight: lighter">From Date</label>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txtVRDateFrom" runat="server" CssClass="form-control" placeholder="Select From Date"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div6">
                                <label class="control-label col-sm-4" id="label3" style="text-align: left; font-weight: lighter">To Date</label>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txtVRDateTo" runat="server" CssClass="form-control" placeholder="Select To Date"></asp:TextBox><br />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                    <asp:Button ID="btnVechileNoSearchFind" Text="Find" CssClass="button" runat="server" OnClick="btnVechileNoSearchFind_Click" ValidationGroup="ab" OnClientClick="return Submitvalidate()" />
                    <asp:Button ID="btnVechileNoSearchReset" Text="Reset" CssClass="button" runat="server" OnClick="btnVechileNoSearchReset_Click" CausesValidation="false" />
                </div>
                <div class="clearfix">
                </div>
                <asp:GridView ID="GridViewVechileReadings" runat="server" HorizontalAlign="Center" PagerStyle-CssClass="GridPager" CssClass="table table-bordered table-inverse" Style="text-align: center" OnRowDataBound="GridViewVechileReadings_RowDataBound" DataKeyNames="SNo" Font-Size="11px" HeaderStyle-Wrap="false" AutoGenerateColumns="False" HeaderStyle-Width="5%" EmptyDataText="Currently No Records Found" CellPadding="3" HeaderStyle-HorizontalAlign="Center" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" AllowPaging="True" ForeColor="#66CCFF" OnPageIndexChanging="GridViewVechileReadings_PageIndexChanging">
                    <EmptyDataRowStyle ForeColor="#FF3300" HorizontalAlign="Center" />
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <HeaderStyle BackColor="#748CB2" Font-Bold="True" ForeColor="White" Height="30px" />
                    <PagerStyle ForeColor="White" HorizontalAlign="Center" BackColor="White" />
                    <PagerSettings Mode="NextPreviousFirstLast" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#0c99f0" />
                    <SortedAscendingHeaderStyle BackColor="#0c99f0" />
                    <SortedDescendingCellStyle BackColor="#0c99f0" />
                    <SortedDescendingHeaderStyle BackColor="#0c99f0" />
                    <Columns>
                        <asp:TemplateField HeaderText="SNO" SortExpression="SNo">
                            <ItemTemplate>
                                <asp:Label ID="lblGVVechileSno" runat="server" Text='<%# Bind("SNo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="VEHICLE NO" SortExpression="VechileNo">
                            <ItemTemplate>
                                <asp:Label ID="lblGVVechileNo" runat="server" Text='<%# Bind("VechileNo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Name" HeaderText="DRIVER NAME" SortExpression="Name" />
                        <asp:BoundField DataField="MobileNo" HeaderText="MOBILE NO" SortExpression="MobileNo" />
                        <asp:BoundField DataField="PlaceVisited" HeaderText="PLACE VISITED" SortExpression="PlaceVisited" />
                        <asp:BoundField DataField="OutReading" HeaderText="OUT READING" SortExpression="OutReading" />
                        <asp:BoundField DataField="OutDateTime" HeaderText="OUT DATE TIME" SortExpression="OutDateTime" />
                        <asp:BoundField DataField="InReading" HeaderText="IN READING" SortExpression="InReading" />
                        <asp:TemplateField HeaderText="IN DATE TIME" SortExpression="InDateTime">
                            <ItemTemplate>
                                <asp:Label ID="lblGVVechileInDateTime" runat="server" Text='<%# Bind("InDateTime") %>'></asp:Label>
                                <asp:LinkButton ID="lnkCheckOutVechile" runat="server" ForeColor="#ff0000" Visible="false">CheckIn</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DistanceKMS" HeaderText="DISTANCE KMS" SortExpression="DistanceKMS" />
                        <asp:TemplateField HeaderText="EDIT">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkVechileEdit" runat="server" CssClass="glyphicon glyphicon-edit GV_Edit_Icon" OnClick="lnkVechileEdit_Click1"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>

            <!--Panel to add new record-->

            <asp:ModalPopupExtender ID="ModalPopupVechile" runat="server" TargetControlID="lbtPop" PopupControlID="panelVechile" RepositionMode="RepositionOnWindowResizeAndScroll" DropShadow="true" PopupDragHandleControlID="panelAddNewTitle" BackgroundCssClass="modalBackground"></asp:ModalPopupExtender>
            <asp:Panel ID="panelVechile" runat="server" Style="display: none; background-color: white;" ForeColor="Black" Width="800px" Height="410px">
                <asp:Panel ID="panel3" runat="server" Style="cursor: move; font-family: Tahoma; padding: 7px;" HorizontalAlign="Center" BackColor="#748CB2" Font-Bold="true" ForeColor="White" Height="35px">
                    <b>Modify Vehicle Register</b>
                </asp:Panel>
                <asp:LinkButton ID="lbtPop" runat="server"></asp:LinkButton>
                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="Row">
                            <div class="col-sm-6">
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div13">
                                        <label class="control-label col-sm-4" id="labelSNo" style="text-align: left; font-weight: lighter">SNo</label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtVRSno" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>
                                        <div class="form-horizontal" style="margin: 5px;">
                                            <div class="form-group" runat="server" id="div1">
                                                <label class="control-label col-sm-4" id="lblVRLVechileNo" style="text-align: left; font-weight: lighter">Vehicle No<span class="mandatoryStar">*</span></label>
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" placeholder="Enter Vehicle No" Visible="false"></asp:TextBox>
                                                    <asp:DropDownList ID="ddlVRVechileNo" runat="server" ClientIDMode="Static" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Select Vehicle No" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlVRVechileNo" InitialValue="0" ValidationGroup="Vechile"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlVRVechileNo" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
                                        <div class="form-horizontal" style="margin: 5px;">
                                            <div class="form-group" runat="server" id="div3">
                                                <label class="control-label col-sm-4" id="lblEditDriverName" style="text-align: left; font-weight: lighter">Driver Name<span class="mandatoryStar">*</span></label>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="ddlEditDriverName" runat="server" ClientIDMode="Static" CssClass="form-control" AutoPostBack="true" Visible="false"></asp:DropDownList>
                                                    <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" placeholder="Enter First Name" onkeypress="return ValidateAlpha(event)" onkeyup="SetContextKey()" onpaste="return false" MaxLength="50"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter Driver Name" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtFirstName" ValidationGroup="Vechile"></asp:RequiredFieldValidator>
                                                    <asp:AutoCompleteExtender MinimumPrefixLength="1" ServiceMethod="GetDriverList" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1"
                                                        TargetControlID="txtFirstName" OnClientShown="PopupShown" ServicePath="~/VMS/VechileRegister/VechileRegister.aspx" ID="AutoCompleteExtender4" UseContextKey="true" runat="server" FirstRowSelected="false">
                                                    </asp:AutoCompleteExtender>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlEditDriverName" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div17">
                                        <label class="control-label col-sm-4" id="lblLastName" style="text-align: left; font-weight: lighter">Last Name<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" placeholder="Enter Last Name" onkeypress="return ValidateAlpha(event)" onpaste="return false" MaxLength="50"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Enter Last Name" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtLastName" ValidationGroup="Vechile"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div7">
                                        <label class="control-label col-sm-4" id="lblMobileNo" style="text-align: left; font-weight: lighter">Mobile No<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtVRMobileNo" runat="server" CssClass="form-control" placeholder="Enter Mobile No" MaxLength="10" onkeypress="return ValidatePhoneNo(event);"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Enter Mobile No" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtVRMobileNo" ValidationGroup="Vechile"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorMobile" runat="server" ErrorMessage="Mobile No Should be 10 Digits" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtVRMobileNo" ValidationGroup="Vechile" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                </div>
                                <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <div class="form-horizontal" style="margin: 5px;">
                                            <div class="form-group" runat="server" id="div9">
                                                <label class="control-label col-sm-4" id="lblEditPlaceToVisit" style="text-align: left; font-weight: lighter">Place To Visit<span class="mandatoryStar">*</span></label>
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtVRPlaceVisited" runat="server" CssClass="form-control" placeholder="Place To Visit"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter Place To Visit" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtVRPlaceVisited" ValidationGroup="Vechile"></asp:RequiredFieldValidator>
                                                    <asp:AutoCompleteExtender ServiceMethod="GetPlaceToVisitList" MinimumPrefixLength="1" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1"
                                                        TargetControlID="txtVRPlaceVisited" OnClientShown="PopupShown" ID="AutoCompleteExtender1" UseContextKey="true" runat="server" FirstRowSelected="false">
                                                    </asp:AutoCompleteExtender>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtVRPlaceVisited" EventName="TextChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div10">
                                        <label class="control-label col-sm-4" id="lblVechileOutReading" style="text-align: left; font-weight: lighter">Out Reading<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtVROutReading" runat="server" CssClass="form-control" placeholder="Enter Out Reading" onkeypress="if(event.keyCode<48 || event.keyCode>57)event.returnValue=false;"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Enter Out Reading" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtVROutReading" ValidationGroup="Vechile"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div15">
                                        <label class="control-label col-sm-4" id="lblVechileInReading" style="text-align: left; font-weight: lighter">In Reading<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtVRInReading" runat="server" CssClass="form-control" placeholder="Enter In Reading" OnTextChanged="txtVRInReading_TextChanged" AutoPostBack="true" onkeypress="if(event.keyCode<48 || event.keyCode>57)event.returnValue=false;"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Enter In Reading" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtVRInReading" ValidationGroup="Vechile"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="CVInReading" runat="server" ErrorMessage="Should Be Greater Than OutReading" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtVRInReading" ControlToCompare="txtVROutReading" ValidationGroup="Vechile" Operator="GreaterThan" Type="Integer"></asp:CompareValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div11">
                                        <label class="control-label col-sm-4" id="lblVechileOutDateTime" style="text-align: left; font-weight: lighter">OutDateTime</label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtVROutDateTime" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div16">
                                        <label class="control-label col-sm-4" id="lblVechileInDateTime" style="text-align: left; font-weight: lighter">In Date Time</label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtIndateTime" runat="server" CssClass="form-control" placeholder="Enter In Reading" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div12">
                                        <label class="control-label col-sm-4" id="lblRemarks" style="text-align: left; font-weight: lighter">Remarks</label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtVRRemarks" runat="server" CssClass="form-control" placeholder="Enter Remarks" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div14">
                                        <label class="control-label col-sm-4" id="labelComments" style="text-align: left; font-weight: lighter">Comments<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtVREditComments" runat="server" CssClass="form-control" placeholder="Enter Comments" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Enter Comments" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtVREditComments" ValidationGroup="Vechile"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="DivVRDistance" visible="false">
                                        <label class="control-label col-sm-4" id="lblDistance" style="text-align: left; font-weight: lighter">Distance Travelled KMS</label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtVRDistanceKms" runat="server" CssClass="form-control" placeholder="Enter Remarks" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="DivVRTotalKms" visible="false">
                                        <label class="control-label col-sm-4" id="labelTotalNoofKMS" style="text-align: left; font-weight: lighter">Total No of KMS</label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtVRTotalKms" runat="server" CssClass="form-control" placeholder="Enter Comments" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                    <asp:Button ID="btnVRUpdate" Text="Update" CssClass="button" runat="server" OnClick="btnVRUpdate_Click1" UseSubmitBehavior="false" ValidationGroup="Vechile" />
                    <asp:Button ID="btnVechileCancel" Text="Cancel" CssClass="button" runat="server" CausesValidation="false" OnClick="btnVechileCancel_Click" />
                </div>
            </asp:Panel>

        </div>
    </div>
  

    <script>
        $("#NavLnkVechile").attr("class", "active");
        $("#VechileRegisterList").attr("class", "active");
    </script>
    <script>
        //for start date and end date validations startdate should be < enddate
        $(function () {
            $('#<%=txtVRDateFrom.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                useCurrent: false,
            });
            var startdate = document.getElementById('<%=txtVRDateFrom.ClientID%>').value
            $('#<%=txtVRDateTo.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                minDate: startdate,
                useCurrent: false,
            });
            $('#<%=txtVRDateFrom.ClientID%>').on("dp.change", function (e) {
                $('#<%=txtVRDateTo.ClientID%>').data("DateTimePicker").minDate(e.date);
              
                  $('#<%=txtVRDateTo.ClientID%>').val("");
            });
            $('#<%=txtVRDateTo.ClientID%>').on("dp.change", function (e) {
              });
        });
    </script>

    <script type="text/javascript">
        //for datepickers
        var startdate2 = document.getElementById('<%=txtVRDateFrom.ClientID%>').value
        if (startdate2 == "") {
            $('#<%=txtVRDateFrom.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                useCurrent: false,
            });
            $('#<%=txtVRDateTo.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                useCurrent: false,
            });
        }
    </script>
    <script type="text/javascript">
        function Submitvalidate() {
            var VechileNo = document.getElementById("<%=ddlVechileNoSearch.ClientID%>").value;
             var DriverName = document.getElementById("<%=txtSearchFirstName.ClientID%>").value;
                    var PlaceVisit = document.getElementById("<%=txtVRSearchPlaceVisited.ClientID%>").value;
                    var FromDate = document.getElementById("<%=txtVRDateFrom.ClientID%>").value;
                    var ToDate = document.getElementById("<%=txtVRDateTo.ClientID%>").value;
                    if (VechileNo == 0 && DriverName == "" && PlaceVisit == "" && FromDate == "" && ToDate == "") {
                       
                        custAlertMsg('Please Select or Enter atleast one Field to Find Data', "error");
                        return false;
                    }
        }

    </script>
    <script>
         //For autocomplete in Model Popup textbox
         function PopupShown(sender, args) {
             sender._popupBehavior._element.style.zIndex = 99999999;
         }
    </script>
</asp:Content>
