﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/NestedVMS_Master.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="Vechile_Register.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.VehicleRegister.Vechile_Register" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>

<asp:Content ID="Content1" ContentPlaceHolderID="VMSBody" runat="server">
    <asp:UpdatePanel ID="UpHdnFields" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnPkVechileEditID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnVehicleCustom" runat="server" Value="" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upbtnNavVehicleCreate" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class=" float-right ">
                <asp:Button ID="btnNavVehicleCreate" CssClass="float-right  btn-signup_popup" runat="server" Text="Create" OnClick="btnNavVehicleCreate_Click" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="col-lg-12 col-sm-12 col-12 col-md-12 dms_outer_border float-left padding-none">
        <div class="col-lg-12 col-sm-12 col-12 col-md-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div class=" col-md-12 col-lg-12 col-sm-12 col-12 padding-none float-left">
            <div class=" col-md-12 col-lg-12 col-sm-12 col-12 padding-none grid_panel_full float-left">
                <div class=" col-md-12 col-lg-12 col-sm-12 col-12 padding-none float-right ">

                        <div class=" grid_header col-md-12 col-lg-12 col-sm-12 col-12 float-left ">
                            Vehicle List
                        </div>
            
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12 col-12 grid_container top float-left padding-none">
                    <table id="tblVechicleList" class="tblATC_ListClass display datatable_cust" style="width: 100%">
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>S.No</th>
                                <th>Vehicle No.</th>
                                <th>Driver Name</th>
                                <th>Mobile No.</th>
                                <th>Place of Visit</th>
                                <th>Out Reading</th>
                                <th>Check Out Time</th>
                                <th>In Reading</th>
                                <th>Check In Time</th>
                                <th>Distance (Kms)</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <%--</div>
    </div>--%>

    <div id="divServerBtn" style="display: none">
        <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnEditVechicle" runat="server" OnClick="btnEditVechicle_Click" Text="Button" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <!--Modal Popup for Vehicle Register-->
    <div id="ModalVehicle" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content ">
                <div class="modal-header">
                    <h4 class="modal-title col-sm-8">Vehicle Register</h4>
                    <button type="button" class="close col-sm-4" style="text-align: right;" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="upVehicleModal" runat="server" UpdateMode="Conditional" style="display: inline-block;">
                        <ContentTemplate>
                            <div class="col-sm-12 padding-none float-left">
                                <div class="col-sm-12 padding-none float-left">
                                    <asp:UpdatePanel ID="upAddVehicleNo" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group col-lg-6 padding-none float-left" runat="server" id="div8">
                                                <label class="control-label col-sm-10" id="lblVechileNo">Vehicle No.<span class="mandatoryStar">*</span></label>
                                                <asp:Button ID="btnVechileNo" runat="server" CssClass="btnnVisit float-right" Text="+" Font-Bold="true" OnClick="btnVechileNo_Click" />
                                                <div class="col-sm-12 float-right">
                                                    <asp:TextBox ID="txtVechileNo" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Vehicle No" Visible="false" AutoPostBack="true" OnTextChanged="txtVechileNo_TextChanged" MaxLength="20"></asp:TextBox>
                                                    <asp:DropDownList ID="ddlVRVechileNo" runat="server" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" TabIndex="1" AutoPostBack="true" OnSelectedIndexChanged="ddlVRVechileNo_SelectedIndexChanged"></asp:DropDownList>
                                                </div>
                                            </div>

                                        </ContentTemplate>
                                       
                                    </asp:UpdatePanel>

                                    <div class="form-group col-lg-6 padding-none float-left" runat="server" id="div6">
                                        <label class="control-label col-sm-12" id="lblVechileOutDateTime">Check Out Time</label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtVROutDateTime" runat="server" Enabled="false" CssClass="form-control login_input_sign_up" TabIndex="7" onkeypress="return event.keyCode != 13;"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-12 float-left padding-none">
                                    <div class="form-group col-lg-6 padding-none float-left" runat="server" id="div5">
                                        <label class="control-label col-sm-12" id="lblDriverName">Driver First Name<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-12">
                                            <asp:DropDownList ID="ddlDriverName" runat="server" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" AutoPostBack="true" Visible="false"></asp:DropDownList>
                                            <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter First Name" TabIndex="2" onkeypress="return ValidateAlpha(event)" onkeyup="SetContextKey()" onpaste="return false" MaxLength="50" AutoPostBack="true" OnTextChanged="txtFirstName_TextChanged"></asp:TextBox>
                                            <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" MinimumPrefixLength="1" ServiceMethod="GetDriverList" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1"
                                                TargetControlID="txtFirstName" OnClientShown="PopupShown" UseContextKey="true" FirstRowSelected="false">
                                            </ajaxToolkit:AutoCompleteExtender>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-6 padding-none float-left" runat="server" id="div7">
                                        <label class="control-label col-sm-12" id="lblLastName">Driver Last Name<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Last Name" onkeypress="return ValidateAlpha(event)" TabIndex="3" onpaste="return false" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                        </div>
                                    <div class="form-group col-lg-6 padding-none float-left" runat="server" id="div1">
                                        <label class="control-label col-sm-12" id="lblMobileNo">Mobile No.<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtVRMobileNo" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Mobile No" MaxLength="10" TabIndex="4" onkeypress="return ValidatePhoneNo(event);" onpaste="return false;"></asp:TextBox>
                                        </div>
                                    </div>
                                 
                                    <div class="form-group col-lg-6 padding-none float-left" runat="server" id="div2">
                                        <label class="control-label col-sm-12" id="lblPlaceToVisit">Place of Visit<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtVRPlaceVisited" runat="server" CssClass="form-control login_input_sign_up" placeholder="Place To Visit" TabIndex="5" onkeypress="return event.keyCode != 13;" MaxLength="50"></asp:TextBox>
                                            <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" ServiceMethod="GetPlaceToVisitList" MinimumPrefixLength="1" CompletionInterval="10" EnableCaching="false" CompletionSetCount="1"
                                                TargetControlID="txtVRPlaceVisited" OnClientShown="PopupShown" UseContextKey="true" FirstRowSelected="false">
                                            </ajaxToolkit:AutoCompleteExtender>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-6 padding-none float-left" runat="server" id="div4">
                                        <label class="control-label col-sm-12" id="lblVechileOutReading">Out Reading(kms)<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtVROutReading" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Out Reading" TabIndex="6" onkeypress="if(event.keyCode<48 || event.keyCode>57)event.returnValue=false;" MaxLength="10" onpaste="return false;"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-6 padding-none float-left" runat="server" id="div_InReading">
                                        <label class="control-label col-sm-12" id="lblVechileInReading">In Reading(kms)<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtVRInReading" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter In Reading" TabIndex="6" onkeypress="if(event.keyCode<48 || event.keyCode>57)event.returnValue=false;" OnTextChanged="txtVRInReading_TextChanged" MaxLength="10" onpaste="return false;"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-12 padding-none float-left" runat="server" id="div9">
                                        <label class="control-label col-sm-12" id="lblRemarks">Remarks</label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtVRRemarks" runat="server" CssClass="form-control" placeholder="Enter Remarks" TabIndex="8" TextMode="MultiLine" Rows="2"  MaxLength="200"></asp:TextBox>
                                            <asp:TextBox ID="txtVRDistanceKms" runat="server" Visible="false" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <%--visible="false"--%>
                                    <div class="form-group col-lg-12 padding-none float-left" runat="server" id="div_vechile_comments">
                                        <label class="control-label col-sm-12" id="labelComments">Comments<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtVREditComments" runat="server" CssClass="form-control" placeholder="Enter Comments" TextMode="MultiLine" Rows="2" MaxLength="200"></asp:TextBox>
                                                                                   </div>
                                    </div>
                                </div>

                                <div class="col-sm-12 top float-left" style="text-align: right;">
                                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="upVehicleButons">
                                        <ContentTemplate>
                                            <asp:Button ID="btnVechileSubmit" Text="Submit" CssClass=" btn-signup_popup" runat="server" OnClick="btnVechileSubmit_Click" TabIndex="9" OnClientClick="javascript:return Submitvaidate();" />
                                            <asp:Button ID="btnVechileReset" Text="Reset" CssClass=" btn-revert_popup" runat="server" OnClick="btnVechileReset_Click" TabIndex="10" CausesValidation="false" />
                                            <asp:Button ID="btnVechileUpdate" Text="Update" CssClass=" btn-signup_popup" runat="server" OnClick="btnVechileUpdate_Click" OnClientClick="javascript:return Updatevalidate();" />
                                            <button type="button" class=" btn-cancel_popup"  data-dismiss="modal">Cancel</button>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <!--End Modal Popup for Vehicle Register-->

    <uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />

    <!--Vehicle Register To Get Confirm Box Alerts-->
    <script>
        function ConfirmAlertVehicleSubmit() {
            custAlertMsg('Do you want to submit', 'confirm', true);
            document.getElementById("<%=hdnVehicleCustom.ClientID%>").value = "Submit";
        }
        function ConfirmAlertVehicleUpdate() {
            custAlertMsg('Do you want to update', 'confirm', true);
            document.getElementById("<%=hdnVehicleCustom.ClientID%>").value = "Update";
            
        }
    </script>
    <!--End Vehicle Register To Get Confirm Box Alerts-->

    <!--------Vehicle Get Datetimenow from Client Side for OutDatetime-------------->
    <script>
        window.onload = function () {
            getDate();
        };
        function getDate() {
            var dtTO = new Date();
            var Todate = document.getElementById("<%=txtVROutDateTime.ClientID%>");
            Todate.value = formatDate(dtTO);
        }
        function formatDate(dateObj) {
            var m_names = new Array("Jan", "Feb", "Mar",
                "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                "Oct", "Nov", "Dec");
            var curr_date = dateObj.getDate();
            var curr_month = dateObj.getMonth();
            var curr_year = dateObj.getFullYear();
            var curr_min = dateObj.getMinutes();
            var curr_hr = dateObj.getHours();
            var curr_sc = dateObj.getSeconds();
            return curr_date + " " + m_names[curr_month] + " " + curr_year + " " + curr_hr + ":" + curr_min + ":" + curr_sc;
        }
    </script>
    <!--------End Vehicle Get Datetimenow from Client Side for OutDatetime-------------->

    <!--------Vehicle Submit Validation-------------->
    <script type="text/javascript">
        function Submitvaidate() {
            var Vechile = document.getElementById("<%=ddlVRVechileNo.ClientID%>").value;
            var FirstName = document.getElementById("<%=txtFirstName.ClientID%>").value;
             var LastName = document.getElementById("<%=txtLastName.ClientID%>").value;
            var Mobile = document.getElementById("<%=txtVRMobileNo.ClientID%>").value;
            var PlacetoVisit = document.getElementById("<%=txtVRPlaceVisited.ClientID%>").value;
            var Outreading = document.getElementById("<%=txtVROutReading.ClientID%>").value;
            errors = [];
            var ddlVechile = $('#<%=ddlVRVechileNo.ClientID%>');
            var isVisible5 = ddlVechile.is(':visible');
            if (isVisible5 == true) {
                var Vechile = document.getElementById("<%=ddlVRVechileNo.ClientID%>").value;
                if (Vechile == 0) {
                    errors.push("Please select vehicle number.");
                }
            }
            var txtVechile = $('#<%=txtVechileNo.ClientID%>');
            var isVisible6 = txtVechile.is(':visible');
            if (isVisible6 == true) {
                var VechileNo = document.getElementById("<%=txtVechileNo.ClientID%>").value;
                if (VechileNo.trim() == "") {
                    errors.push("Please enter vehicle number.");
                }
            }
            
            if (FirstName.trim() == "") {
                errors.push("Please enter driver first name.");
            }
            if (LastName.trim() == "") {
                errors.push("Please enter driver last name.");
            }
            if (Mobile.trim() == "") {
                errors.push("Please enter mobile no.");
            }
            else {
                if (Mobile.length != 10) {
                    errors.push("Mobile no should be 10 digits.");
                }
            }
            if (PlacetoVisit.trim() == "") {
                errors.push("Please enter place to visit.");
            }
            if (Outreading.trim() == "") {
                errors.push("Please enter vechile out reading.");
            }

            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            ConfirmAlertVehicleSubmit();
        }
    </script>
    <!--------End Vehicle Submit Validation-------------->

    <!--------Vehicle Update Validation-------------->
    <script type="text/javascript">
        function Updatevalidate() {
            var Vechile = document.getElementById("<%=ddlVRVechileNo.ClientID%>").value;
            var FirstName = document.getElementById("<%=txtFirstName.ClientID%>").value;
            var LastName = document.getElementById("<%=txtLastName.ClientID%>").value;
            var Mobile = document.getElementById("<%=txtVRMobileNo.ClientID%>").value;
            var PlacetoVisit = document.getElementById("<%=txtVRPlaceVisited.ClientID%>").value;
            var Outreading = document.getElementById("<%=txtVROutReading.ClientID%>").value;
            var Inreading = document.getElementById("<%=txtVRInReading.ClientID%>").value;
            var Comments = document.getElementById("<%=txtVREditComments.ClientID%>").value;
            errors = [];
            if (Vechile == 0) {
                errors.push("Please select vehicle number.");
            }
            if (FirstName.trim() == "") {
                errors.push("Please enter first name.");
            }
            if (LastName.trim() == "") {
                errors.push("Please enter last name.");
            }
            if (Mobile.trim() == "") {
                errors.push("Please enter mobile no.");
            }
            else {
                if (Mobile.length != 10) {
                    errors.push("Mobile no should be 10 digits.");
                }
            }
            if (PlacetoVisit.trim() == "") {
                errors.push("Please enter place to visit.");
            }
            if (Outreading.trim() == "") {
                errors.push("Please enter vechile Outreading.");
            }
            if (Inreading.trim() == "") {
                errors.push("Please enter vechile Inreading.");
            }
            if (Comments.trim() == "") {
                errors.push("Please enter modification reason in comments.");
            }

            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            ConfirmAlertVehicleUpdate();
        }
    </script>
    <!--------End Vehicle Update Validation-------------->

    <!--Vechicle Register function Get Deatils when edit click-->
    <script>
        function ViewVechile_List(SNo) {
            $("#<%=hdnPkVechileEditID.ClientID%>").val(SNo);
            $("#<%=btnEditVechicle.ClientID%>").click();
        }
    </script>
    <!--End Vechicle Register function Get Deatils when edit click-->

    <!--VehicleRegister List jQuery DataTable-->
    <script>
        $.ajaxSetup({
            cache: false
        });

        var oTable = $('#tblVechicleList').DataTable({
            columns: [
                { 'data': 'SNo' },
                { 'data': 'RowNumber' },
                { 'data': 'VechileNo' },
                { 'data': 'DriverName' },
                { 'data': 'MobileNo' },
                { 'data': 'PlaceVisited' },
                { 'data': 'OutReading' },
                { 'data': 'OutDateTime' },
                { 'data': 'InReading' },
                { 'data': 'InDateTime' },
                { 'data': 'DistanceKMS' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a class="Edit" title="Edit"  href="#" onclick="ViewVechile_List(' + o.SNo + ');"></a>'; }
                }
            ],
            "pagingType": "simple_numbers",
            "language": {
                "infoFiltered": ""
            },

            "processing": true,
            "orderClasses": false,

            "order": [[1, "desc"]],
            "info": true,
           
            "aoColumnDefs": [{ "targets": [0,1], "visible": false }, { "className": "dt-body-left", "targets": [2, 3, 4, 5] },
            { "className": "dt-body-right", "targets": [6, 8, 10] }],
           
           

            "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
            "sAjaxSource": "<%=ResolveUrl("~/VMS/WebService/VMS_Service.asmx/GetVechile_DetailsList")%>",
            "fnServerData": function (sSource, aoData, fnCallback) {
              
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#tblATC_List").show();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }

        });

        function ReloadVehicletable() {
            oTable.ajax.reload();
            $('#ModalVehicle').modal('hide');
        }
    </script>
    <!--End VehicleRegister List jQuery DataTable-->

    <!--Vehicle Register Open Model Popup for Create-->
    <script>
        function OpenModelViewVehicle() {
            $('#<%=btnVechileSubmit.ClientID %>').show();
            $('#<%=btnVechileReset.ClientID %>').show();
                document.getElementById("<%=txtFirstName.ClientID%>").value = '';
                document.getElementById("<%=txtLastName.ClientID%>").value = '';
                document.getElementById("<%=txtVROutDateTime.ClientID%>").value = '';
                getDate();
                document.getElementById("<%=txtVROutReading.ClientID%>").value = '';
                document.getElementById("<%=txtVRMobileNo.ClientID%>").value = '';
                document.getElementById("<%=txtVRPlaceVisited.ClientID%>").value = '';
                document.getElementById("<%=txtVRRemarks.ClientID%>").value = '';
                $('#<%=btnVechileUpdate.ClientID%>').hide();
                $('#ModalVehicle').modal('show');
        }
    </script>
    <!--End Vehicle Register Open Model Popup for Create-->

    <!--Vehicle Register Open Model Popup for Edit-->
    <script>
        function OpenModeVehicle() {
            $('#ModalVehicle').modal('show');
            $('#<%=div_InReading.ClientID%>').show();
            $('#<%=div_vechile_comments.ClientID%>').show();
            $('#<%=btnVechileUpdate.ClientID %>').show();
            $('#<%=btnVechileSubmit.ClientID %>').hide();
            $('#<%=btnVechileReset.ClientID %>').hide();
        }
    </script>
    <!--End Vehicle Register Open Model Popup for Edit-->

    <!--Vehicle Register Validation for In-Reading onblur-->
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $(".datepicker-orient-bottom").hide();
                }
                $('#<%=txtVRInReading.ClientID%>').blur(function () {
                        var NewInreading = $(this).val();
                        var OutReading = document.getElementById("<%=txtVROutReading.ClientID%>").value;
                            if (parseFloat(OutReading) > parseFloat(NewInreading)) {
                                custAlertMsg("Inreading should be greater than Outreading " + OutReading, "error");
                                $(this).val("");
                                return false;
                            } else {
                                // do something
                            }
                    });
                });
            };
    </script>
    <!--End Vehicle Register Validation for In-Reading onblur-->


    <script>
                //For autocomplete in Model Popup textbox
                function PopupShown(sender, args) {
                    sender._popupBehavior._element.style.zIndex = 99999999;
                }
    </script>
    <!--Validation for Remarks mas length-->
    <script>
                function CheckLength() {
                    var textbox = document.getElementById("<%=txtVRRemarks.ClientID%>").value;
                    if (textbox.trim().length >= 200) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }
    </script>
</asp:Content>
