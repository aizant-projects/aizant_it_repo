﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VisitorReport.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.Reports.VisitorReport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 65%;
        }
        .auto-style2 {
            text-align: left;
            font-size: large;
        }
        .auto-style3 {
            text-decoration: underline;
            text-align: center;
            font-size: xx-large;
        }
        .auto-style6 {
            height: 23px;
            text-align: center;
            font-size: x-large;
        }
        .auto-style7 {
            font-size: x-large;
        }
        .auto-style8 {
            width: 479px;
            text-align: right;
            font-size: large;
        }
        .auto-style12 {
            color: #000000;
        }
        .auto-style14 {
            width: 479px;
            text-align: left;
            font-size: large;
        }
        .auto-style11 {
            text-decoration: underline;
            color: #000000;
        }
        .auto-style17 {
            width: 479px;
            text-align: left;
        }
        .auto-style18 {
            width: 232px;
            text-align: right;
            font-size: large;
        }
        .auto-style21 {
            width: 232px;
        }
        .auto-style23 {
            font-size: medium;
        }
        .auto-style24 {
            width: 258px;
            text-align: left;
            vertical-align: text-top;
            font-size: large;
        }
        .auto-style25 {
            margin-left: 100px;
        }
        .auto-style26 {
            width: 232px;
            text-align: left;
        }
        .auto-style28 {
            height: 90px;
            width: 249px;
            font-size: large;
        }
        textarea {
    resize: none;
}
        .auto-style29 {
            font-size: large;
        }
        </style>
</head>
<body style="width: 1529px; height: 458px">
     <form id="form1" runat="server">
          <div>
              <div class="col-lg-2 ">
                  <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Logo.png" Style="position:relative; left:400px; padding: 0px;" class="navbar-brand" Height="45px" Width="177px" />
              </div>
              <table>
                  <tr>
                      <td style="padding-left:100px">
                               <table class="auto-style1" align="left">
                  <tr>
                      <td class="auto-style3" colspan="3"><span class="auto-style7">Aizant Drug Research Solutions Pvt.Ltd</span><br />
                          <span class="auto-style23">Private Limited Sy No. 172 &amp; 173, Apparel Park Road, Dulapally Village, Quthbullapur Mandal, Hyderabad - 500014</span></td>
                  </tr>
                  <tr>
                      <td class="auto-style6" colspan="3"><span class="auto-style11">Visitor Pass</span><span class="auto-style12">
                          </span></td>
                  </tr>
                  </table>
                      </td>
                  </tr>
                   <tr>
                      <td>
                            <table style="padding-left:150px" align="left" class="auto-style25">
                  <tr>
                      <td class="auto-style24">&nbsp;&nbsp;</td>
                      <td class="auto-style26">
                          <span class="auto-style29">&nbsp;
                          </span>
                          <asp:Label ID="lblSno" runat="server" Visible="false" CssClass="auto-style29"></asp:Label>
                      </td>
                  </tr>
                  <tr>
                      <td class="auto-style24">Visitor ID :</td>
                      <td class="auto-style21">
                          <asp:Label ID="lblID" runat="server" CssClass="auto-style29"></asp:Label>
                      </td>
                      <td class="auto-style17" rowspan="8">
                          <asp:Image ID="MyImage" runat="server" Height="180px" Width="224px" CssClass="auto-style29" />
                      </td>
                  </tr>
                  <tr>
                      <td class="auto-style24">Visitor Name :</td>
                      <td class="auto-style21">
                          <asp:Label ID="lblName" runat="server" CssClass="auto-style29"></asp:Label>
                      </td>
                  </tr>
                  <tr>
                      <td class="auto-style24">Mobile No :</td>
                      <td class="auto-style21">
                          <asp:Label ID="lblMobile" runat="server" CssClass="auto-style29"></asp:Label>
                      </td>
                  </tr>
                  <tr>
                      <td class="auto-style24">Organization :</td>
                      <td class="auto-style21">
                          <asp:Label ID="lblOrganization" runat="server" CssClass="auto-style29"></asp:Label>
                      </td>
                  </tr>
                  <tr>
                      <td class="auto-style24">Vechile No :</td>
                      <td class="auto-style21">
                          <asp:Label ID="lblVechileNo" runat="server" CssClass="auto-style29"></asp:Label>
                      </td>
                  </tr>
                  <tr>
                      <td class="auto-style24">Purpose of Visit :</td>
                      <td class="auto-style21">
                          <asp:Label ID="lblPurpose" runat="server" CssClass="auto-style29"></asp:Label>
                      </td>
                  </tr>
                  <tr>
                      <td class="auto-style24">Department :</td>
                      <td class="auto-style21">
                          <asp:Label ID="lblDepartment" runat="server" CssClass="auto-style29"></asp:Label>
                      </td>
                  </tr>
                  <tr>
                      <td class="auto-style24">Whom To Visit :</td>
                      <td class="auto-style21">
                          <asp:Label ID="lblWhomtovisit" runat="server" CssClass="auto-style29"></asp:Label>
                      </td>
                  </tr>
                  <tr>
                      <td class="auto-style24">No of Visitors :</td>
                      <td class="auto-style21">
                          <asp:Label ID="lblNoofvisitors" runat="server" CssClass="auto-style29"></asp:Label>
                      </td>
                      <td class="auto-style14">
                          &nbsp;</td>
                  </tr>
                  <tr>
                      <td class="auto-style24">Belongings :</td>
                      <td class="auto-style21">
                          <asp:Label ID="lblBelongings" runat="server" CssClass="auto-style29"></asp:Label>
                      </td>
                      <td class="auto-style14">
                          &nbsp;</td>
                  </tr>
                  <tr>
                      <td class="auto-style24">Visitor InDateTime :</td>
                      <td class="auto-style21">
                          <asp:Label ID="lblInDateTime" runat="server" CssClass="auto-style29"></asp:Label>
                      </td>
                      <td class="auto-style14">
                          &nbsp;</td>
                  </tr>
                  <tr>
                      <td class="auto-style24">Visitor OutDateTime :</td>
                      <td class="auto-style21">
                          <asp:Label ID="lblOutDateTime" runat="server" CssClass="auto-style29"></asp:Label>
                      </td>
                      <td class="auto-style14">
                          &nbsp;</td>
                  </tr>
                  <tr>
                      <td class="auto-style24">Address :</td>
                      <td class="auto-style21">
                          <textarea runat="server" id="txtAddress" readonly="readonly" style="background-color: white; padding-left: 5px;overflow:hidden; padding-right: 5px;border:none" class="auto-style28" cols="20" name="S1" rows="1"></textarea></td>
                      <td class="auto-style14">
                          &nbsp;</td>
                  </tr>
                  <tr>
                      <td class="auto-style24">&nbsp;</td>
                      <td class="auto-style21"></td>
                      <td class="auto-style14">
                          &nbsp;</td>
                  </tr>
                  <tr>
                      <td class="auto-style24">&nbsp;</td>
                      <td class="auto-style18">
                          &nbsp;</td>
                  </tr>
                  <tr>
                      <td class="auto-style24">&nbsp;</td>
                      <td class="auto-style18">
                          &nbsp;</td>
                      <td class="auto-style8">&nbsp;</td>
                  </tr>
                  <tr>
                      <td class="auto-style2" colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;Signature of Visitor</td>
                      <td class="auto-style14">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Security Signature</td>
                  </tr>
                  </table>
                      </td>
                  </tr>
              </table>
         
            
          </div>
    </form>
</body>
</html>
