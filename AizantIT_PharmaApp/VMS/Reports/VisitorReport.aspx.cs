﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using VMS_BAL;
using VMS_BO;
using AizantIT_PharmaApp.Common;


namespace AizantIT_PharmaApp.VMS.Reports
{
    public partial class VisitorReport : System.Web.UI.Page
    {
        VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            PrintVisitorPass(Convert.ToInt32(Request.QueryString[0]));
                    }

        public void PrintVisitorPass(int Vid)
        {
            try
            {
            VisitorRegisterBO objvisitorRegisterBO = new VisitorRegisterBO();
            objvisitorRegisterBO.DeptName = string.Empty;
            objvisitorRegisterBO.Purpose_of_VisitID = 0;
            objvisitorRegisterBO.FirstName = string.Empty;
            objvisitorRegisterBO.VisitorID = string.Empty;
            objvisitorRegisterBO.MobileNo = string.Empty;
            objvisitorRegisterBO.Organization = string.Empty;
            objvisitorRegisterBO.IDCardNo = string.Empty;
            objvisitorRegisterBO.InDateTime = string.Empty;
            objvisitorRegisterBO.OutDateTime = string.Empty;
            DataTable dt = new DataTable();
            objvisitorRegisterBO.SNo = Vid;
            dt = objVisitorRegisterBAL.SearchFilterVisitor(objvisitorRegisterBO);
            if (dt.Rows.Count > 0)
            {
                lblSno.Text= Vid.ToString();
                lblID.Text= dt.Rows[0]["VisitorID"].ToString();
                lblName.Text = dt.Rows[0]["VisitorName"].ToString();
                txtAddress.Value= dt.Rows[0]["Address"].ToString();
                lblMobile.Text = dt.Rows[0]["MobileNo"].ToString();
                lblPurpose.Text = dt.Rows[0]["PurposeofVisit"].ToString();
                lblNoofvisitors.Text = dt.Rows[0]["NoofVisitors"].ToString();
                lblDepartment.Text = dt.Rows[0]["DeptName"].ToString(); 
                lblWhomtovisit.Text = dt.Rows[0]["EmpName"].ToString();
                lblInDateTime.Text = dt.Rows[0]["InDateTime"].ToString();                
                lblBelongings.Text = dt.Rows[0]["Belongings"].ToString();
                lblOrganization.Text = dt.Rows[0]["Organization"].ToString();
                lblVechileNo.Text= dt.Rows[0]["VechileNo"].ToString();
                lblOutDateTime.Text = dt.Rows[0]["OutDateTime"].ToString();
                if (!string.IsNullOrEmpty(dt.Rows[0]["Photo"].ToString()))
                {
                    BindApplicantImage((byte[])dt.Rows[0]["PhotoFileData"]);
                }
                else
                {
                    MyImage.ImageUrl = "~/Images/DefaultImage.jpg";
                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), "openwindow", "javascript:window.print();", true);
            }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSREP1:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSREP1:" + strline + " " + strMsg,"error");
            }
        }

        private void BindApplicantImage(byte[] applicantImage)
        {
            string base64ImageString = ConvertBytesToBase64(applicantImage);
            MyImage.ImageUrl = "data:image/jpg;base64," + base64ImageString;
        }
        public string ConvertBytesToBase64(byte[] imageBytes)
        {
            return Convert.ToBase64String(imageBytes);
        }
    }
}