﻿using System;
using System.Web.UI;
using VMS_BAL;
using VMS_BO;
using System.Data;
using AizantIT_PharmaApp.Common;
using System.Data.SqlClient;

namespace AizantIT_PharmaApp.VMS.Reports
{
    public partial class Outward : System.Web.UI.Page
    {
        OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PrintOutwardMaterialReport(Convert.ToInt32(Request.QueryString[0]));
            }
        }

        public void PrintOutwardMaterialReport(int Vid)
        {
            try
            {
                MaterialBO objMaterialBO = new MaterialBO();
                DataTable dt = new DataTable();
                objMaterialBO.Sno = Vid;
                dt = objOutwardMaterialBAL.OutwardMateriallinkbuttonFilldata(objMaterialBO);
                if (dt.Rows.Count > 0)
                {
                    lblSno.Text = Vid.ToString();
                    lblMType.Text = dt.Rows[0]["OutwardMaterialType"].ToString();
                    lblMName.Text = dt.Rows[0]["MaterialName"].ToString();
                    txtDes.Value = dt.Rows[0]["DescriptionOfMaterial"].ToString();
                    lblQuantity.Text = dt.Rows[0]["Quantityu"].ToString();
                    lblDCNo.Text = dt.Rows[0]["GPorDCPNo"].ToString();
                    lblVechileNo.Text = dt.Rows[0]["VechileNo"].ToString();
                    lblDept.Text = dt.Rows[0]["Department"].ToString();
                    lblGivenBy.Text = dt.Rows[0]["GivenBy"].ToString();
                    lblNameoftheParty.Text = dt.Rows[0]["NameOfParty"].ToString();
                                      txtRemarks.Value = dt.Rows[0]["Remarks"].ToString();
                    lblPrintedON.Text = DateTime.Now.ToString("dd MMM YYYY hh:mm:ss");
                    lblPrintedBY.Text = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "openwindow", "javascript:window.print();", true);
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSREP2:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSREP2:" + strline + " " + strMsg,"error");
            }
        }
    }
}