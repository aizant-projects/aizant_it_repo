﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using VMS_BAL;
using VMS_BO;
using System.Collections;
using AizantIT_PharmaApp.Common;
using System.Text;
using System.Globalization;
using DevExpress.XtraReports.UI;
using UMS_BO;
using UMS_BusinessLayer;

namespace AizantIT_PharmaApp.VMS.Reports
{
    public partial class VehicleReportPage : System.Web.UI.Page
    {
       
            VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
            VMSRolePrivilegesBAL objVMSRolePrivilegesBAL = new VMSRolePrivilegesBAL();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Page_Load(object sender, EventArgs e)
            {
                try
                {
                    if (!IsPostBack)
                    {

                        if (HelpClass.IsUserAuthenticated())
                        {
                            DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                            DataTable dtTemp = new DataTable();
                            DataRow[] drUMS = dt.Select("ModuleID=6");
                            RolePrivilegesBO objRolePrivilegesBO = new RolePrivilegesBO();
                            objRolePrivilegesBO.RoleID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            DataTable dtRole = objVMSRolePrivilegesBAL.GetEmpRoleID(objRolePrivilegesBO);
                            objRolePrivilegesBO.RoleID = Convert.ToInt32(dtRole.Rows[0]["RoleID"].ToString()); //8 admin

                            if (drUMS.Length > 0)
                            {
                                dtTemp = drUMS.CopyToDataTable();
                                ViewState["EmpID"] = "0";
                                if (HelpClass.IsUserAuthenticated())
                                {
                                    ViewState["EmpID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                }
                                InitializeThePage();
                            }
                            else
                            {
                                Response.Redirect("~/UserLogin.aspx");
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                }
                catch (SqlException sqe)
                {
                    string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                    HelpClass.showMsg(this, this.GetType(), "VMSCh1:" + strmsgF);
                }
                catch (Exception ex)
                {
                    string strline = HelpClass.LineNo(ex);
                    string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSCh1:" + strline + " " + strMsg);
                HelpClass.showMsg(this, this.GetType(), "VMSCh1:" + strline + " " + strMsg);
                }
            }

            private void InitializeThePage()
            {
                try
                {
                    LoadAllMonths();
                   
                    ShowHideReportTypeSelection();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", " getDate();", true);
                }

                catch (Exception ex)
                {
                    string strline = HelpClass.LineNo(ex);
                    string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVRP1:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVRP1:" + strline + " " + strMsg, "error");
                }
            }

            private void LoadAllMonths()
            {
                txtMonthYear.Text = "2018";
                ddlfromMonths.Items.Insert(0, new ListItem("--Select--", "0"));
                ddlToMonth.Items.Insert(0, new ListItem("--Select--", "0"));
                var months = CultureInfo.CurrentCulture.DateTimeFormat.MonthGenitiveNames;
                for (int i = 0; i < months.Length - 1; i++)
                {
                    ddlfromMonths.Items.Add(new ListItem(months[i], (i + 1).ToString()));

                }
            }
       

            protected void btnReset_Click(object sender, EventArgs e)
            {

                Response.Redirect("~/VMS/Reports/VehicleReportPage.aspx");

            }


            protected void btnFind1_Click(object sender, EventArgs e)
            {
            ArrayList Mandatory = new ArrayList();
            if (ddlReportType.SelectedValue == "1")//weekly
            {
                if (string.IsNullOrEmpty(txtSearchVisitorsFromDate.Text.Trim()))
                {
                    Mandatory.Add("Please select from date");
                }
                if (string.IsNullOrEmpty(txtSearchVisitorsToDate.Text.Trim()))
                {
                    Mandatory.Add("Please select to date");
                }
                StringBuilder s = new StringBuilder();
                foreach (string x in Mandatory)
                {
                    s.Append(x);
                    s.Append("<br/>");
                }
                if (Mandatory.Count > 0)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "" + s.ToString() + "", "error");

                    return;
                }
            }
            if (ddlReportType.SelectedValue == "2")//monthly
            {
                if (string.IsNullOrEmpty(txtMonthYear.Text.Trim()))
                {
                    Mandatory.Add("Please enter year");
                }
                if (ddlfromMonths.SelectedIndex == 0)
                {
                    Mandatory.Add("Please select to from month");
                }
                if (ddlToMonth.SelectedIndex == 0)
                {
                    Mandatory.Add("Please select to to month");
                }
                StringBuilder s = new StringBuilder();
                foreach (string x in Mandatory)
                {
                    s.Append(x);
                    s.Append("<br/>");
                }
                if (Mandatory.Count > 0)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "" + s.ToString() + "", "error");

                    return;
                }

            }
            if (ddlReportType.SelectedValue == "3")//yearly
            {
                if (string.IsNullOrEmpty(txtFromYear.Text.Trim()))
                {
                    Mandatory.Add("Please enter from year");
                }
                if (string.IsNullOrEmpty(txtToYear.Text.Trim()))
                {
                    Mandatory.Add("Please enter to year");
                }
                StringBuilder s = new StringBuilder();
                foreach (string x in Mandatory)
                {
                    s.Append(x);
                    s.Append("<br/>");
                }
                if (Mandatory.Count > 0)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "" + s.ToString() + "", "error");

                    return;
                }
                if (Convert.ToInt32(txtFromYear.Text.Trim()) > Convert.ToInt32(txtToYear.Text.Trim()))
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "From Year should be less than To Year.", "error");
                    return;
                }
            }
            DataTable dt = new DataTable();

            dt = GetExportData(1, 2);
            if (dt.Rows.Count > 0)
            {
                VMS.DevReports.VehicleReport VehicleRpt = new VMS.DevReports.VehicleReport();
                if (ddlReportType.SelectedValue == "1")//weekly
                {
                    ((XRLabel)(VehicleRpt.FindControl("xrLblFrom", false))).Text = "From Date :" + DateTime.Parse(txtSearchVisitorsFromDate.Text.Trim()).ToString("dd MMM yyyy");
                    ((XRLabel)(VehicleRpt.FindControl("xrLblTo", false))).Text = "To Date :" + DateTime.Parse(txtSearchVisitorsToDate.Text.Trim()).ToString("dd MMM yyyy");
                }
                if (ddlReportType.SelectedValue == "2")//Monthly
                {
                    ((XRLabel)(VehicleRpt.FindControl("xrLblFrom", false))).Text = "From Month :" + ddlfromMonths.SelectedItem.Text.ToString() + "-" + txtMonthYear.Text.Trim();
                    ((XRLabel)(VehicleRpt.FindControl("xrLblTo", false))).Text = "To Month :" + ddlToMonth.SelectedItem.Text.ToString() + "-" + txtMonthYear.Text.Trim();
                }
                if (ddlReportType.SelectedValue == "3")//yearly
                {
                    ((XRLabel)(VehicleRpt.FindControl("xrLblFrom", false))).Text = "From Year :" + txtFromYear.Text.Trim();
                    ((XRLabel)(VehicleRpt.FindControl("xrLblTo", false))).Text = "To Year :" + txtToYear.Text.Trim();
                }

                ((XRLabel)(VehicleRpt.FindControl("lblPrintedBy", false))).Text = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString();
                DataTable _dtCompany = fillCompany();
                ((XRLabel)(VehicleRpt.FindControl("lblCompanyNamne", false))).Text = _dtCompany.Rows[0]["CompanyName"].ToString();
                if (!string.IsNullOrEmpty(_dtCompany.Rows[0]["CompanyLogo"].ToString()))
                {
                    ((XRPictureBox)(VehicleRpt.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                }
                VehicleRpt.DataSource = dt;
                VehicleRpt.DataMember = dt.TableName;
                VehicleRpt.CreateDocument();
                ASPxWebDocumentViewer1.OpenReport(VehicleRpt);
                ASPxWebDocumentViewer1.DataBind();
            }
            else
                    HelpClass.custAlertMsg(this, this.GetType(), "Vehicle data not available ", "info");



            }

        private System.Drawing.Image BindApplicantImage(byte[] applicantImage)
        {
            
            using (var ms = new MemoryStream(applicantImage))
            {
                return System.Drawing.Image.FromStream(ms);
            }
        }
        public string ConvertBytesToBase64(byte[] imageBytes)
        {
            return Convert.ToBase64String(imageBytes);
        }

        protected void ddlReportType_SelectedIndexChanged(object sender, EventArgs e)
            {
                ShowHideReportTypeSelection();
            }

            private void ShowHideReportTypeSelection()
            {
                if (ddlReportType.SelectedValue == "1")
                {
                    div_FromDate.Visible = true;
                    div_ToDate.Visible = true;
                    div_FromMonths.Visible = false;
                    div_ToMonth.Visible = false;
                    div_FromYear.Visible = false;
                    div_ToYear.Visible = false;
                    div_MonthYear.Visible = false;
                }
                else if (ddlReportType.SelectedValue == "2")
                {
                    div_FromMonths.Visible = true;
                    div_ToMonth.Visible = true;
                    div_FromDate.Visible = false;
                    div_ToDate.Visible = false;
                    div_FromYear.Visible = false;
                    div_ToYear.Visible = false;
                    div_MonthYear.Visible = true;
                }
                else if (ddlReportType.SelectedValue == "3")
                {
                    div_FromYear.Visible = true;
                    div_ToYear.Visible = true;
                    div_FromMonths.Visible = false;
                    div_ToMonth.Visible = false;
                    div_FromDate.Visible = false;
                    div_ToDate.Visible = false;
                    div_MonthYear.Visible = false;
                }
                
            }

            protected void ddlfromMonths_SelectedIndexChanged(object sender, EventArgs e)
            {
                ddlToMonth.Items.Clear();
                ddlToMonth.Items.Insert(0, new ListItem("--Select--", "0"));
                if (ddlfromMonths.SelectedIndex > 0)
                {
                    var months = CultureInfo.CurrentCulture.DateTimeFormat.MonthGenitiveNames;
                    for (int i = 1; i <= months.Length - 1; i++)
                    {
                        if (i >= Convert.ToInt32(ddlfromMonths.SelectedValue))
                        {
                            ddlToMonth.Items.Add(new ListItem(months[i - 1], (i).ToString()));
                        }
                    }
                }
            }


            DataTable GetExportData(int Mode = 0, int chartno = 1)
            {
                DataTable dtChartData = new DataTable();
                try
                {
                    VisitorRegisterBO objVisitorRegisterBO = new VisitorRegisterBO();
                    VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();

                    if (ddlReportType.SelectedValue == "1")//weekly
                    {
                        objVisitorRegisterBO.SNo = chartno;
                        objVisitorRegisterBO.InDateTime = txtSearchVisitorsFromDate.Text.Trim();
                        objVisitorRegisterBO.OutDateTime = txtSearchVisitorsToDate.Text.Trim();
                        objVisitorRegisterBO.FromMonth = 0;
                        objVisitorRegisterBO.ToMonth = 0;
                        objVisitorRegisterBO.FromYear = 0;
                        objVisitorRegisterBO.ToYear = 0;
                        objVisitorRegisterBO.Mode = Mode;
                    objVisitorRegisterBO.DeptName=txtVehicleNo.Text.Trim();
                    dtChartData = objVisitorRegisterBAL.GetExportData(objVisitorRegisterBO);

                    }
                    else if (ddlReportType.SelectedValue == "2")//monthly
                    {
                        objVisitorRegisterBO.SNo = chartno;
                        objVisitorRegisterBO.FromMonth = Convert.ToInt32(ddlfromMonths.SelectedValue);
                        objVisitorRegisterBO.ToMonth = Convert.ToInt32(ddlToMonth.SelectedValue);
                        objVisitorRegisterBO.InDateTime = "";
                        objVisitorRegisterBO.OutDateTime = "";
                        objVisitorRegisterBO.FromYear = Convert.ToInt32(txtMonthYear.Text.Trim());
                        objVisitorRegisterBO.ToYear = 0;
                        objVisitorRegisterBO.Mode = 2;
                    objVisitorRegisterBO.DeptName = txtVehicleNo.Text.Trim();
                    dtChartData = objVisitorRegisterBAL.GetExportData(objVisitorRegisterBO);

                    }
                    else if (ddlReportType.SelectedValue == "3")//yearly
                    {
                        if (!string.IsNullOrEmpty(txtFromYear.Text.Trim()))
                        {
                            objVisitorRegisterBO.FromYear = Convert.ToInt32(txtFromYear.Text.Trim());
                        }
                        else
                        {
                            objVisitorRegisterBO.FromYear = 0;
                        }
                        if (!string.IsNullOrEmpty(txtToYear.Text.Trim()))
                        {
                            objVisitorRegisterBO.ToYear = Convert.ToInt32(txtToYear.Text.Trim());
                        }
                        else
                        {
                            objVisitorRegisterBO.ToYear = 0;
                        }
                        objVisitorRegisterBO.SNo = chartno;
                        objVisitorRegisterBO.FromMonth = 0;
                        objVisitorRegisterBO.ToMonth = 0;
                        objVisitorRegisterBO.InDateTime = "";
                        objVisitorRegisterBO.OutDateTime = "";
                        objVisitorRegisterBO.Mode = 3;
                    objVisitorRegisterBO.DeptName = txtVehicleNo.Text.Trim();
                    dtChartData = objVisitorRegisterBAL.GetExportData(objVisitorRegisterBO);
                    }
                }
                catch (SqlException sqe)
                {
                    string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                    HelpClass.custAlertMsg(this, this.GetType(), "VMSCh31:" + strmsgF, "error");
                }
                catch (Exception ex)
                {
                    string strline = HelpClass.LineNo(ex);
                    string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSCh31:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSCh31:" + strline + " " + strMsg, "error");
                }
                return dtChartData;
            }
        DataTable fillCompany()
        {
            CompanyBO objCompanyBO;
            UMS_BAL objUMS_BAL;
            DataTable dtcompany = new DataTable();
            objCompanyBO = new CompanyBO();
            objCompanyBO.Mode = 2;
            objCompanyBO.CompanyID = 0;
            objCompanyBO.CompanyCode = "";
            objCompanyBO.CompanyName = "";
            objCompanyBO.CompanyDescription = "";
            objCompanyBO.CompanyPhoneNo1 = "";
            objCompanyBO.CompanyPhoneNo2 = "";
            objCompanyBO.CompanyFaxNo1 = "";
            objCompanyBO.CompanyFaxNo2 = ""; //txtFaxNo2.Value;
            objCompanyBO.CompanyEmailID = "";
            objCompanyBO.CompanyWebUrl = "";
            objCompanyBO.CompanyAddress = "";
            objCompanyBO.CompanyLocation = "";// txtLocat.Value;
            objCompanyBO.CompanyCity = "";
            objCompanyBO.CompanyState = "";
            objCompanyBO.CompanyCountry = "";
            objCompanyBO.CompanyPinCode = "";
            objCompanyBO.CompanyStartDate = "";
            objCompanyBO.CompanyLogo = new byte[0];

            objUMS_BAL = new UMS_BAL();
            dtcompany = objUMS_BAL.CompanyQuery(objCompanyBO);
            return dtcompany;
        }
        #region document viewer
        protected void ASPxDocumentViewer1_CacheReportDocument(object sender, DevExpress.XtraReports.Web.CacheReportDocumentEventArgs e)
            {
                e.Key = Guid.NewGuid().ToString();
                Page.Session[e.Key] = e.SaveDocumentToMemoryStream();
            }

            protected void ASPxDocumentViewer1_RestoreReportDocumentFromCache(object sender, DevExpress.XtraReports.Web.RestoreReportDocumentFromCacheEventArgs e)
            {
                Stream stream = Page.Session[e.Key] as Stream;
                if (stream != null)
                    e.RestoreDocumentFromStream(stream);
            }
        #endregion document viewer
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
    }