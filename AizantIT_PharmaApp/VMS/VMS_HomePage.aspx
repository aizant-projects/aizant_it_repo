﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/NestedVMS_Master.master" AutoEventWireup="true" CodeBehind="VMS_HomePage.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.VMS_HomePage" %>

<%@ Register Assembly="DevExpress.XtraCharts.v18.1.Web, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="VMSBody" runat="server">
   <%-- <link href="../AppCSS/CardsFlip.css" rel="stylesheet" />
    <link href="../AppCSS/dashboardstyle.css" rel="stylesheet" />--%>
    <div>
          
                    <div class="form-group col-sm-4  padding-none float-left" runat="server" id="dvMultiRoles" >
                        <label class="control-label float-left top"  id="lblAccessableRoles">Access Role :</label>
                        <div class="col-sm-7   float-left" style="margin-top:4px;">
                            <asp:DropDownList ID="ddlVMSAccessableRoles" runat="server" AutoPostBack="true" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker regulatory_dropdown_style drop_down padding-none" OnSelectedIndexChanged="ddlVMSAccessableRoles_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        
                    </div>
           
        </div>
    <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none  float-left">
        <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none panel_list_bg  float-left" id="divcsscard" runat="server" style="position: relative; z-index: 9;">
            <div class="col-lg-3 col-3 col-sm-6 col-md-4  float-left padding-right_div f1_container" id="divReverted" runat="server" visible="false">
                <div class=" f1_card col-lg-12  float-left">
                    <div class="front face outer_dashboard1  float-left">
                        <div style="position: absolute; right: 0; top: 20;">
                            <img src="../Images/common_dashboard/reverted.png" class="" alt="Approve" />
                        </div>
                        <div class="col-lg-12 col-12 col-sm-12 col-md-12 header_text_dshboard">Reverted Requests</div>
                        <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none  float-left">
                            <div class="col-lg-6 col-6 col-sm-6 col-md-6 padding-none float-left ">
                                <img src="../Images/common_dashboard/revetred_small.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-6 col-sm-6 col-md-6  float-left">
                                <span class="link_dashboard">
                                    <asp:Label ID="lblRevertedinitiation" CssClass="link_dashboard" runat="server" Text="0"></asp:Label></span>
                            </div>
                        </div>
                    </div>
                    <div class="back face center outer_dashboard1  float-left">
                        <div class="col-lg-12 col-12 col-sm-12 col-md-12 float-left  padding-none">

                            <div class="col-lg-12 padding-none  float-left">

                                <div class="col-lg-12 padding-none  float-left">
                                    <div class=" col-lg-12 padding-none  float-right ">
                                        <ul class="col-lg-12 col-12 col-sm-12 col-md-12 flip_list style-2 ">
                                           
                                            <li id="liReverted" runat="server"></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-3 col-sm-6 col-md-4  float-left padding-right_div f1_container " id="divApprover" runat="server" visible="false">
                <div class=" f1_card col-lg-12  float-left">
                    <div class="front face outer_dashboard">
                        <div style="position: absolute; right: 0; opacity: 0.2; top: 20;">
                            <img src="../Images/common_dashboard/approve_big.png" class="" alt="Approve" />
                        </div>
                        <div class="col-lg-12 col-12 col-sm-12 col-md-12 header_text_dshboard">Pending Approval</div>
                        <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none  float-left">
                            <div class="col-lg-6 col-6 col-sm-6 col-md-6 padding-none  float-left">
                                <img src="../Images/common_dashboard/approve_small.png" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6 col-6 col-sm-6 col-md-6  float-left">
                                <span class="link_dashboard">
                                    <asp:Label ID="lblApproverQA" CssClass="link_dashboard" runat="server" Text="0"></asp:Label></span>
                            </div>
                        </div>
                    </div>
                  
                    <div class="back face center outer_dashboard  float-left" style="height: 120px !important;">
                        <div class="col-lg-12 col-12 col-sm-12 col-md-12 float-left padding-none" id="divReviewViewDetails">

                            <div class="col-lg-12 padding-none  float-left">

                                <div class="col-lg-12 padding-none  float-left">
                                    <div class=" col-lg-12 padding-none  float-right ">
                                        <ul class="col-lg-12 col-12 col-sm-12 col-md-12 flip_list style-2 ">
                                           
                                            <li id="liApprove" runat="server">
                                               
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="graphic" runat="server" class="col-md-12 padding-none bottom float-left">
        <asp:UpdatePanel ID="upVisitorChart" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class=" bottom">
                    <div class="col-md-12 padding-none  float-left grid_panel_full" id="DocumentStatusChart" runat="server">
                        <div class="grid_header  col-lg-12 col-12 col-sm-12 col-md-12  float-left">
                            <div class="col-lg-6 col-6 col-sm-6 col-md-6 padding-none float-left">Security Dashboard</div>
                            <div class="col-lg-6 col-6 col-sm-6 col-md-6 padding-none  float-left">
                                <div class="panel_list float-right">
                                    <ul>
                                        <li id="lirefresh">
                                            <a href="VMS_HomePage.aspx" class="refresh" name="navrefresh"></a>
                                        </li>
                                        <li>
                                            <a class="filter_icon filter_four"></a>
                                        </li>
                                        <div class="filter_dropdown" id="filter_dropdown4">
                                            <ul class="top dropdown-menu_vms">
                                                <li>
                                                    <div class="form-group  col-lg-12 col-12 col-sm-12 col-md-12 padding-none">

                                                        <asp:DropDownList ID="ddlReportType" CssClass="col-lg-12 col-12 col-sm-12 col-md-12 padding-none selectpicker drop_down  regulatory_dropdown_style" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlReportType_SelectedIndexChanged">
                                                            <asp:ListItem Value="1">Week</asp:ListItem>
                                                            <asp:ListItem Value="2">Month</asp:ListItem>
                                                            <asp:ListItem Value="3">Year</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </li>
                                                <li id="div_FromYear" runat="server">
                                                    <div class="form-group  col-lg-12 col-12 col-sm-12 col-md-12 padding-none">
                                                        <asp:TextBox ID="txtFromYear" CssClass="form-control login_input_sign_up" runat="server" placeholder="Enter From Year" MaxLength="4" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"></asp:TextBox>
                                                    </div>
                                                </li>
                                                <li id="div_ToYear" runat="server">
                                                    <div class="form-group  col-lg-12 col-12 col-sm-12 col-md-12 padding-none">
                                                        <asp:TextBox ID="txtToYear" CssClass="form-control login_input_sign_up" runat="server" placeholder="Enter To Year" MaxLength="4" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"></asp:TextBox>
                                                    </div>
                                                </li>
                                                <li id="div_MonthYear" runat="server">
                                                    <div class="form-group  col-lg-12 col-12 col-sm-12 col-md-12 padding-none">
                                                        <asp:TextBox ID="txtMonthYear" CssClass="form-control login_input_sign_up" MaxLength="4" runat="server" placeholder="Enter Year" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"></asp:TextBox>
                                                    </div>
                                                </li>
                                                <li id="div_FromMonths" runat="server">
                                                    <div class="form-group  col-lg-12 col-12 col-sm-12 col-md-12 padding-none">
                                                        <asp:DropDownList ID="ddlfromMonths" CssClass="col-lg-12 col-12 col-sm-12 col-md-12 padding-none selectpicker drop_down regulatory_dropdown_style" data-size="7" data-live-search="true" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlfromMonths_SelectedIndexChanged"></asp:DropDownList>
                                                    </div>
                                                </li>
                                                <li id="div_ToMonth" runat="server">
                                                    <div class="form-group  col-lg-12 col-12 col-sm-12 col-md-12 padding-none">
                                                        <asp:DropDownList ID="ddlToMonth" runat="server" CssClass="col-lg-12 col-12 col-sm-12 col-md-12 padding-none selectpicker drop_down regulatory_dropdown_style" data-size="7" data-live-search="true"></asp:DropDownList>
                                                    </div>
                                                </li>
                                                <li id="div_FromDate" runat="server">
                                                    <div class="form-group  col-lg-12 col-12 col-sm-12 col-md-12 padding-none">
                                                        <asp:TextBox ID="txtSearchVisitorsFromDate" CssClass="form-control login_input_sign_up" runat="server" placeholder="Select From Date" autocomplete="off"></asp:TextBox>
                                                    </div>
                                                </li>
                                                <li id="div_ToDate" runat="server">
                                                    <div class="form-group  col-lg-12 col-12 col-sm-12 col-md-12 padding-none">
                                                        <asp:TextBox ID="txtSearchVisitorsToDate" runat="server" CssClass="form-control login_input_sign_up" placeholder="Select To Date" autocomplete="off"></asp:TextBox>
                                                    </div>
                                                </li>
                                                <li class="float-right">
                                                    <div href="#" style="background: none; padding: 0px;">
                                                       
                                                        <asp:Button ID="btn_chartSubmit" class=" btn-signup_popup" runat="server" Text="Submit" OnClick="btn_chartSubmit_Click" />
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12  grid_panel_full_vms bottom float-left">
                            <div class="col-md-4  top bottom float-left">
                                <dx:WebChartControl ID="WebChartControl1" CssClass="float-left" runat="server" Width="300px" Height="300px" BackColor="Transparent" CrosshairEnabled="True" ToolTipEnabled="True" PaletteName="Apex">
                                    <EmptyChartText Text="No visitors for current Dates" TextColor="#07889a" />
                                    <Legend Name="Default Legend"></Legend>
                                    <SeriesSerializable>
                                        <dx:Series Name="Series1" LegendTextPattern="{A}:{V}" ValueDataMembersSerializable="NoofVisitors" ArgumentDataMember="Purpose_of_Visit" LegendName="Default Legend">
                                            <ViewSerializable>
                                                <dx:PieSeriesView>
                                                </dx:PieSeriesView>
                                            </ViewSerializable>
                                            <LabelSerializable>
                                                <dx:PieSeriesLabel TextPattern="{V}" BackColor="0, 0, 0" Position="Outside" TextAlignment="Near" TextColor="Azure">
                                                </dx:PieSeriesLabel>
                                            </LabelSerializable>
                                        </dx:Series>
                                    </SeriesSerializable>
                                    <Titles>
                                        <dx:ChartTitle Text="Purpose of Visit" />
                                    </Titles>
                                    <ClientSideEvents ObjectHotTracked="function(s, e) {
     var hitInCategory = e.hitInfo.inSeries &amp;&amp; e.chart.series[0].visible;
     var hitInBackTitle = e.hitInfo.inChartTitle &amp;&amp; !e.chart.series[0].visible;
     s.SetCursor((hitInCategory || hitInBackTitle) ? 'pointer' : 'default');    
 }"
                                        ObjectSelected="function(s, e) {
     var hitInCategory = e.hitInfo.inSeries &amp;&amp; e.chart.series[0].visible;
     var hitInBackTitle = e.hitInfo.inChartTitle &amp;&amp; !e.chart.series[0].visible;
     window.location = 'VisitorRegister/VisitorRegister.aspx'

//e.processOnServer = hitInCategory || hitInBackTitle;
 }
 " />
                                </dx:WebChartControl>
                            </div>
                            <div class="col-md-4  top bottom float-left" id="divVehicleChart" runat="server">
                                <dx:WebChartControl ID="WebChartControl2" CssClass="float-left" Width="300px" Height="300px" runat="server" BackColor="Transparent" CrosshairEnabled="True" ToolTipEnabled="True" PaletteName="Apex">
                                    <EmptyChartText Text="No vehicles for current Dates" TextColor="#07889a" />

                                    <Legend Name="Default Legend"></Legend>
                                    <SeriesSerializable>
                                        <dx:Series Name="Series1" LegendTextPattern="{A}:{V}" ValueDataMembersSerializable="KMS" ArgumentDataMember="VechileNo" LegendName="Default Legend">
                                            <ViewSerializable>
                                                <dx:PieSeriesView>
                                                </dx:PieSeriesView>
                                            </ViewSerializable>
                                            <LabelSerializable>
                                                <dx:PieSeriesLabel TextPattern="{V}" BackColor="0, 0, 0" Position="Outside" TextAlignment="Near" TextColor="Azure">
                                                </dx:PieSeriesLabel>
                                            </LabelSerializable>
                                        </dx:Series>
                                    </SeriesSerializable>
                                    <Titles>
                                        <dx:ChartTitle Text="Vehicle" />
                                    </Titles>
                                    <ClientSideEvents ObjectHotTracked="function(s, e) {
     var hitInCategory = e.hitInfo.inSeries &amp;&amp; e.chart.series[0].visible;
     var hitInBackTitle = e.hitInfo.inChartTitle &amp;&amp; !e.chart.series[0].visible;
     s.SetCursor((hitInCategory || hitInBackTitle) ? 'pointer' : 'default');    
 }"
                                        ObjectSelected="function(s, e) {
     var hitInCategory = e.hitInfo.inSeries &amp;&amp; e.chart.series[0].visible;
     var hitInBackTitle = e.hitInfo.inChartTitle &amp;&amp; !e.chart.series[0].visible;
     window.location = 'VehicleRegister/Vechile_Register.aspx'

//e.processOnServer = hitInCategory || hitInBackTitle;
 }
 " />
                                </dx:WebChartControl>
                            </div>
                            <div class="col-4 top bottom float-left">
                                <dx:WebChartControl ID="WebChartControl3" runat="server" Width="300px" Height="300px" CrosshairEnabled="False"
                                    PaletteName="Flow" EnableCallbackAnimation="True" BackColor="#FFFFFF" ToolTipEnabled="True">
                                    <EmptyChartText Text="No Records Found" TextColor="Red" Font="Tahoma, 15.75pt" Tag="No Records Found" />
                                    <DiagramSerializable>
                                        <dx:XYDiagram>
                                            <AxisX VisibleInPanesSerializable="-1">
                                                <VisualRange Auto="False" AutoSideMargins="False" MaxValueSerializable="0" MinValueSerializable="0" SideMarginsValue="1" />
                                                <WholeRange AutoSideMargins="False" MaxValueSerializable="0" SideMarginsValue="1" />
                                                <NumericScaleOptions AutoGrid="False" />
                                            </AxisX>
                                            <AxisY VisibleInPanesSerializable="-1">
                                                <NumericScaleOptions AutoGrid="False" />
                                            </AxisY>
                                        </dx:XYDiagram>
                                    </DiagramSerializable>
                                    <Legend Name="Default Legend" Visibility="False"><Title Text="{A}{V}"></Title></Legend>
                                    <Legends>
                                        <dx:Legend Name="Legend1">
                                        </dx:Legend>
                                        <dx:Legend Name="Legend2">
                                        </dx:Legend>
                                    </Legends>
                                    <SeriesSerializable>
                                        <dx:Series Name="Series 2" LabelsVisibility="True" ToolTipEnabled="True" ToolTipPointPattern="{A}:{V}">
                                            <LabelSerializable>
                                                <dx:SideBySideBarSeriesLabel Position="Top" ShowForZeroValues="True" TextPattern="{V}">
                                                </dx:SideBySideBarSeriesLabel>
                                            </LabelSerializable>
                                        </dx:Series>
                                    </SeriesSerializable>
                                    <SeriesTemplate ToolTipEnabled="True" ToolTipPointPattern="{A}:{V}">
                                    </SeriesTemplate>
                                    <Titles>
                                        <dx:ChartTitle Font="Tahoma, 18pt" Text="Visitors" />
                                    </Titles>
                                    <ClientSideEvents ObjectHotTracked="function(s, e) {
     var hitInCategory = e.hitInfo.inSeries &amp;&amp; e.chart.series[0].visible;
     var hitInBackTitle = e.hitInfo.inChartTitle &amp;&amp; !e.chart.series[0].visible;
     s.SetCursor((hitInCategory || hitInBackTitle) ? 'pointer' : 'default');    
 }"
                                        ObjectSelected="function(s, e) {
     var hitInCategory = e.hitInfo.inSeries &amp;&amp; e.chart.series[0].visible;
     var hitInBackTitle = e.hitInfo.inChartTitle &amp;&amp; !e.chart.series[0].visible;
     window.location = 'VisitorRegister/VisitorRegister.aspx'

//e.processOnServer = hitInCategory || hitInBackTitle;
 }
 " />
                                </dx:WebChartControl>
                            </div>

                        </div>
                    </div>
                   

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>


    <div class="col-lg-12 col-12 col-sm-12 col-md-12 col-sm-12 col-12 col-md-12 dashboard_msgVMS float-left" id="divNoTask" runat="server" visible="false">"Tasks are not available ...!"</div>



   
    <script>
        function dashb() {
            $(".filter_four").on("click", function () {
                $("#filter_dropdown4").toggle();
            });
        }

    </script>
    <!----To Load Calendar ---->
    <script>
        $(document).ready(function () {
            //Binding Code
            myfunction();
            dashb();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                //Binding Code Again
                myfunction();
                dashb();
            }
        });

    </script>
    <!----End To Load Calendar for Appointment Date---->
    <script>
            function myfunction() {
                //for start date and end date validations startdate should be < enddate
                $(function () {
                    $('#<%=txtSearchVisitorsFromDate.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        useCurrent: false,
                    });

                    $('#<%=txtSearchVisitorsToDate.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        useCurrent: false,
                    });
                    $('#<%=txtSearchVisitorsFromDate.ClientID%>').on("dp.change", function (e) {
                    $('#<%=txtSearchVisitorsToDate.ClientID%>').data("DateTimePicker").minDate(e.date);
                    var _mindate = new Date(e.date);
                    _mindate.setDate(_mindate.getDate() + 6)
                    $('#<%=txtSearchVisitorsToDate.ClientID%>').val(_mindate.format('dd MMM yyyy'));
                    $('#<%=txtSearchVisitorsToDate.ClientID%>').data("DateTimePicker").maxDate(moment().add(7, 'days'));
                });
                });
            }
    </script>



    <script>
            function Clearfromtodate() {
                document.getElementById('<%=txtSearchVisitorsFromDate.ClientID%>').value = '';
                document.getElementById('<%=txtSearchVisitorsToDate.ClientID%>').value = '';
            }
    </script>

    <script type="text/javascript">

            window.onload = function () {
                getDate();
            };

            function getDate() {
                try {
                    var m_names = new Array("Jan", "Feb", "Mar",
                        "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                        "Oct", "Nov", "Dec");
                    var dtTO = new Date();
                    var curr_date = dtTO.getDate();
                    var curr_month = dtTO.getMonth();
                    var curr_year = dtTO.getFullYear();
                    var dtFrom = new Date(dtTO.getTime() - (6 * 24 * 60 * 60 * 1000));
                    var Todate = document.getElementById("<%=txtSearchVisitorsToDate.ClientID%>");
                    Todate.value = curr_date + " " + m_names[curr_month] + " " + curr_year;

                    var Fromdate = document.getElementById("<%=txtSearchVisitorsFromDate.ClientID%>");
                var curr_date1 = dtFrom.getDate();
                var curr_month1 = dtFrom.getMonth();
                var curr_year1 = dtFrom.getFullYear();
                Fromdate.value = curr_date1 + " " + m_names[curr_month1] + " " + curr_year1;
                }
                catch{ }
            }
    </script>
</asp:Content>
