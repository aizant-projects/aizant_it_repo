﻿using System;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VMS_BAL;
using VMS_BO;
using System.Collections;
using AizantIT_PharmaApp.Common;
using System.Text;
using DevExpress.XtraCharts;
using System.Data;
using System.Drawing;

namespace AizantIT_PharmaApp.VMS
{
    public partial class VMSDashboard : System.Web.UI.Page
    {
        VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
        VMSRolePrivilegesBAL objVMSRolePrivilegesBAL = new VMSRolePrivilegesBAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    fillRole();
                    if (HelpClass.IsUserAuthenticated())
                    {

                        ViewState["EmpID"] = "0";
                        ViewState["EmpID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);

                        ArrayList lstRoleids = new ArrayList() { 22, 8, 28, 18, 17 };
                        graphic.Visible = false;
                        divNoTask.Visible = false;
                        //if (Session["sesGMSRoleID"].ToString() == "22" || Session["sesGMSRoleID"].ToString() == "8")
                        //{
                        //    divVehicleChart.Visible = true;
                        //}
                        //else
                        //{
                        //    divVehicleChart.Visible = false;
                        //}
                        //Security=22 ,admin=8,Materail Store=19,Materail hod=20,Materail user=21

                        if (lstRoleids.Contains(Convert.ToInt32(Session["sesGMSRoleID"].ToString())))
                        {
                            graphic.Visible = true;
                        }
                        GetCardCount(Convert.ToInt32(Session["sesGMSRoleID"].ToString()));


                        //InitializeThePage();
                        if (divReverted.Visible == false && divApprover.Visible == false)
                        {
                            divcsscard.Visible = false;
                        }
                        else
                        {
                            divcsscard.Visible = true;
                        }

                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSCh1:" + strline + " " + strMsg);
            }
        }
        private void GetCardCount(int RoleId)
        {
            try
            {
                if (RoleId == 17)
                {
                    //hr Count
                    MaterialBO objMaterialBO = new MaterialBO();
                    OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
                    objMaterialBO.EmpID = 0;
                    objMaterialBO.DeptID = 0;
                    objMaterialBO.TransStatus = 1;
                    objMaterialBO.RoleType = 1;
                    DataTable dtCardCount = objOutwardMaterialBAL.GetCardCount(objMaterialBO);
                    StringBuilder sbCard = new StringBuilder();
                    //approve
                    if (Convert.ToInt32(dtCardCount.Rows[0][0].ToString()) > 0)
                    {
                        divApprover.Visible = true;
                        string qs0 = HttpUtility.UrlEncode(HelpClass.Encrypt("hr"));
                        sbCard.Append("<a class='col-12 float-left' href ='VisitorFacilities/VisitorFacilityList.aspx?c=" + qs0 + "'>");
                        sbCard.Append("<div class='float-left padding-none col-lg-11'>Facilities Pending for Approval</div><div class='float-right padding-none col-lg-1'>" + dtCardCount.Rows[0][0].ToString() + "</div></a> ");
                        liApprove.InnerHtml = sbCard.ToString();

                        lblApproverQA.Text = dtCardCount.Rows[0][0].ToString();
                    }
                }
                else if (RoleId == 28 || RoleId == 18)//BD-roleid 28
                {
                    //helpdesk count
                    MaterialBO objMaterialBO = new MaterialBO();
                    OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
                    objMaterialBO.EmpID = Convert.ToInt32(ViewState["EmpID"].ToString());
                    objMaterialBO.DeptID = 0;
                    objMaterialBO.TransStatus = 2;
                    objMaterialBO.RoleType = 2;
                    DataTable dtCardCount1 = objOutwardMaterialBAL.GetCardCount(objMaterialBO);
                    StringBuilder sbCard = new StringBuilder();
                    //Revert
                    if (Convert.ToInt32(dtCardCount1.Rows[0][0].ToString()) > 0)
                    {
                        divReverted.Visible = true;
                        string qs1 = HttpUtility.UrlEncode(HelpClass.Encrypt("bd"));
                        sbCard.Append("<a href = 'VisitorFacilities/VisitorFacilityList.aspx?c=" + qs1 + "'>");
                        sbCard.Append("<div>Facilities Reverted back</div><div>" + dtCardCount1.Rows[0][0].ToString() + "</div></a> ");
                        liReverted.InnerHtml = sbCard.ToString();
                        lblRevertedinitiation.Text = dtCardCount1.Rows[0][0].ToString();
                    }
                    else
                    {
                        divNoTask.Visible = true;
                        graphic.Visible = false;
                    }
                }
                else if (RoleId == 19)
                {
                    //materialStore Count
                    MaterialBO objMaterialBO = new MaterialBO();
                    OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
                    objMaterialBO.EmpID = 0;
                    objMaterialBO.DeptID = 0;
                    objMaterialBO.TransStatus = 2;
                    objMaterialBO.RoleType = 4;
                    DataTable dtCardCount2 = objOutwardMaterialBAL.GetCardCount(objMaterialBO);
                    StringBuilder sbCard = new StringBuilder();
                    //approve
                    if (Convert.ToInt32(dtCardCount2.Rows[0][0].ToString()) > 0)
                    {
                        divApprover.Visible = true;
                        string qs2 = HttpUtility.UrlEncode(HelpClass.Encrypt("mstore"));
                        sbCard.Append("<a href = 'MaterialRegister/MaterialRegister.aspx?c=" + qs2 + "' class='float-left col-lg-12' name='navApprove' >");
                        sbCard.Append("<div class='float-left padding-none col-lg-11'>Materials Pending for Approval</div><div class='float-right padding-none col-lg-1'>" + dtCardCount2.Rows[0][0].ToString() + "</div></ a > ");
                        liApprove.InnerHtml = sbCard.ToString();
                        lblApproverQA.Text = dtCardCount2.Rows[0][0].ToString();
                    }
                    else
                    {
                        divNoTask.Visible = true;
                    }
                }
                else if (RoleId == 20)
                {
                    //materialHod Count
                    MaterialBO objMaterialBO = new MaterialBO();
                    OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
                    objMaterialBO.EmpID = Convert.ToInt32(ViewState["EmpID"].ToString());
                    objMaterialBO.DeptID = 0;
                    objMaterialBO.TransStatus = 1;
                    objMaterialBO.RoleType = 3;
                    DataTable dtCardCount3 = objOutwardMaterialBAL.GetCardCount(objMaterialBO);
                    StringBuilder sbCard = new StringBuilder();
                    //approve
                    if (Convert.ToInt32(dtCardCount3.Rows[0][0].ToString()) > 0)
                    {
                        divApprover.Visible = true;
                        string qs3 = HttpUtility.UrlEncode(HelpClass.Encrypt("mhod"));
                        sbCard.Append("<a href = 'MaterialRegister/MaterialRegister.aspx?c=" + qs3 + "' class='float-left col-lg-12' name='navApprove' >");
                        sbCard.Append("<div class='float-left padding-none col-lg-11'>Materials Pending for Approval</div><div class='float-right padding-none col-lg-1'>" + dtCardCount3.Rows[0][0].ToString() + "</div></ a > ");
                        liApprove.InnerHtml = sbCard.ToString();
                        lblApproverQA.Text = dtCardCount3.Rows[0][0].ToString();
                    }
                    else
                    {
                        divNoTask.Visible = true;
                    }
                }
                else if (RoleId == 21)
                {
                    //materialUser Count
                    MaterialBO objMaterialBO = new MaterialBO();
                    OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
                    objMaterialBO.EmpID = Convert.ToInt32(ViewState["EmpID"].ToString());
                    objMaterialBO.DeptID = 0;
                    objMaterialBO.TransStatus = 2;
                    objMaterialBO.RoleType = 5;
                    DataTable dtCardCount4 = objOutwardMaterialBAL.GetCardCount(objMaterialBO);
                    StringBuilder sbCard = new StringBuilder();
                    //Revert
                    if (Convert.ToInt32(dtCardCount4.Rows[0][0].ToString()) > 0)
                    {
                        divReverted.Visible = true;
                        string qs4 = HttpUtility.UrlEncode(HelpClass.Encrypt("muser"));
                        sbCard.Append("<a href = 'MaterialRegister/MaterialRegister.aspx?c=" + qs4 + "' class='float-left col-lg-12' name='navReverted' >");
                        sbCard.Append("<div class='float-left padding-none col-lg-11'>Materials Reverted back</div><div class='float-right padding-none col-lg-1'>" + dtCardCount4.Rows[0][0].ToString() + "</div></ a > ");
                        liReverted.InnerHtml = sbCard.ToString();
                        lblRevertedinitiation.Text = dtCardCount4.Rows[0][0].ToString();
                    }
                    else
                    {
                        divNoTask.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSCh3:" + strline + " " + strMsg);
            }
        }
        public void fillRole()
        {
            RolePrivilegesBO objRolePrivilegesBO = new RolePrivilegesBO();
            objRolePrivilegesBO.RoleID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            DataTable dtRole = objVMSRolePrivilegesBAL.GetEmpRoleID(objRolePrivilegesBO);
            if (dtRole.Rows.Count > 1)
            {
                ddlVMSAccessableRoles.DataTextField = dtRole.Columns["RoleName"].ToString();
                ddlVMSAccessableRoles.DataValueField = dtRole.Columns["RoleID"].ToString();
                ddlVMSAccessableRoles.DataSource = dtRole;
                ddlVMSAccessableRoles.DataBind();
                if (Session["sesGMSRoleID"].ToString() == "0")
                {
                    Session["sesGMSRoleID"] = dtRole.Rows[0]["RoleID"].ToString();
                }
                ddlVMSAccessableRoles.SelectedValue = Session["sesGMSRoleID"].ToString();
            }
            else
            {
                if (Session["sesGMSRoleID"].ToString() == "0")
                {
                    Session["sesGMSRoleID"] = dtRole.Rows[0]["RoleID"].ToString();
                }
                // For VMS  
                dvMultiRoles.Visible = false;
                if (dtRole.Rows[0]["RoleID"].ToString() == "22")//Security RoleID
                {
                    Response.Redirect("~/VMS/Dashboard/SecurityLandingPage.aspx", false);
                }

            }
        }
        protected void ddlVMSAccessableRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["sesGMSRoleID"] = ddlVMSAccessableRoles.SelectedValue.ToString();
            Response.Redirect("~/VMS/VMSDashboard.aspx");
        }
    }
}