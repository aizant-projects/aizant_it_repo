﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using VMS_BAL;
using VMS_BO;
using AizantIT_PharmaApp.Common;
using System.Text;
using DevExpress.XtraCharts;
using System.Globalization;

namespace AizantIT_PharmaApp
{
    public partial class VmsHomePage : System.Web.UI.Page
    {
        VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
        VMSRolePrivilegesBAL objVMSRolePrivilegesBAL = new VMSRolePrivilegesBAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                        RolePrivilegesBO objRolePrivilegesBO = new RolePrivilegesBO();
                        objRolePrivilegesBO.RoleID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        DataTable dtRole = objVMSRolePrivilegesBAL.GetEmpRoleID(objRolePrivilegesBO);
                        objRolePrivilegesBO.RoleID = Convert.ToInt32(dtRole.Rows[0]["RoleID"].ToString());
                        if (Convert.ToInt32(dtRole.Rows[0]["RoleID"].ToString()) == 22 || Convert.ToInt32(dtRole.Rows[0]["RoleID"].ToString()) == 8)//Security=22 //admin=8
                        {
                            divVisitorCharts.Visible = true;
                            divNoTask.Visible = false;
                        }
                        else if (Convert.ToInt32(dtRole.Rows[0]["RoleID"].ToString()) == 19 ||//Materail Store 
                                 Convert.ToInt32(dtRole.Rows[0]["RoleID"].ToString()) == 20  || //Materail hod
                                 Convert.ToInt32(dtRole.Rows[0]["RoleID"].ToString()) == 21  || //Materail user
                                 Convert.ToInt32(dtRole.Rows[0]["RoleID"].ToString()) == 8)     //admin
                        {
                            divCardshtml.Visible = true;
                        }
                        else if (Convert.ToInt32(dtRole.Rows[0]["RoleID"].ToString()) == 28 || Convert.ToInt32(dtRole.Rows[0]["RoleID"].ToString()) == 18)//helpdesk=18,BD=28   
                        {
                            //divVisitorCharts.Visible = true;
                            divCardshtml.Visible = true;
                        }
                        else if (Convert.ToInt32(dtRole.Rows[0]["RoleID"].ToString()) == 17)//hr=17
                        {
                            divVisitorCharts.Visible = true;
                            divNoTask.Visible = false;
                        }
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            ViewState["EmpID"] = "0";
                            if (HelpClass.IsUserAuthenticated())
                            {
                                ViewState["EmpID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            }
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSCh1:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSCh1:" + strline + " " + strMsg);
            }
        }

        private void InitializeThePage()
        {
            try
            {
                WebChartControl2.Visible = false;
                WebChartControl3.Visible = false;
                ChartFill(0);               
                LoadAllMonths();
                ShowHideReportTypeSelection();
              
                GetCardCount();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", " getDate();", true);
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSCh2:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSCh2:" + strline + " " + strMsg);
            }
        }

        private void LoadAllMonths()
        {
            txtMonthYear.Text = "2018";
            ddlfromMonths.Items.Insert(0, new ListItem("--Select--", "0"));
            ddlToMonth.Items.Insert(0, new ListItem("--Select--", "0"));
            var months = CultureInfo.CurrentCulture.DateTimeFormat.MonthGenitiveNames;
            for (int i = 0; i < months.Length - 1; i++)
            {
                ddlfromMonths.Items.Add(new ListItem(months[i], (i+1).ToString()));
               
            }
        }
      
        public void ChartFill(int Mode = 0)
        {
            try
            {
                VisitorRegisterBO objVisitorRegisterBO = new VisitorRegisterBO();
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                DataTable dtChartData = new DataTable();
                if (ddlReportType.SelectedValue == "1")//weekly
                {
                    objVisitorRegisterBO.InDateTime = txtSearchVisitorsFromDate.Text.Trim();
                    objVisitorRegisterBO.OutDateTime = txtSearchVisitorsToDate.Text.Trim();
                    objVisitorRegisterBO.FromMonth =0;
                    objVisitorRegisterBO.ToMonth = 0;
                    objVisitorRegisterBO.FromYear =0;
                    objVisitorRegisterBO.ToYear = 0;
                    objVisitorRegisterBO.Mode = Mode;
                    dtChartData = objVisitorRegisterBAL.LoadChartNoofVisitors(objVisitorRegisterBO);
                    this.WebChartControl1.DataSource = dtChartData;
                    this.WebChartControl1.Series[0].ArgumentDataMember = "InDateTime";
                    XYDiagram diagram = (XYDiagram)WebChartControl1.Diagram;
                    diagram.AxisX.Title.Visible = true;
                    diagram.AxisX.Title.Alignment = StringAlignment.Center;
                    diagram.AxisX.Title.Text = "Visited Date";
                    diagram.AxisX.Title.TextColor = Color.Black;
                    diagram.AxisX.Title.Antialiasing = true;
                    diagram.AxisX.Title.Font = new Font("Tahoma", 14, FontStyle.Bold);

                    diagram.AxisY.Title.Visible = true;
                    diagram.AxisY.Title.Alignment = StringAlignment.Center;
                    diagram.AxisY.Title.Text = "No of Visitors";
                    diagram.AxisY.Title.TextColor = Color.Black;
                    diagram.AxisY.Title.Antialiasing = true;
                    diagram.AxisY.Title.Font = new Font("Tahoma", 14, FontStyle.Bold);
                    
                    this.WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "NoofVisitors" });
                    this.WebChartControl1.Series[0].ChangeView(ViewType.Bar);
                    this.WebChartControl1.DataBind();
                }
                else if (ddlReportType.SelectedValue == "2")//monthly
                {
                    objVisitorRegisterBO.FromMonth = Convert.ToInt32(ddlfromMonths.SelectedValue);
                    objVisitorRegisterBO.ToMonth= Convert.ToInt32(ddlToMonth.SelectedValue);
                    objVisitorRegisterBO.InDateTime = "";
                    objVisitorRegisterBO.OutDateTime = "";
                    objVisitorRegisterBO.FromYear = Convert.ToInt32(txtMonthYear.Text.Trim());
                    objVisitorRegisterBO.ToYear = 0;
                    objVisitorRegisterBO.Mode = 2;
                    dtChartData = objVisitorRegisterBAL.LoadChartNoofVisitors(objVisitorRegisterBO);
                    this.WebChartControl1.DataSource = dtChartData;
                    this.WebChartControl1.Series[0].ArgumentDataMember = "ShortMonthName";
                    XYDiagram diagram = (XYDiagram)WebChartControl1.Diagram;
                    diagram.AxisX.Title.Visible = true;
                    diagram.AxisX.Title.Alignment = StringAlignment.Center;
                    diagram.AxisX.Title.Text = "Month";
                    diagram.AxisX.Title.TextColor = Color.Black;
                    diagram.AxisX.Title.Antialiasing = true;
                    diagram.AxisX.Title.Font = new Font("Tahoma", 14, FontStyle.Bold);

                    diagram.AxisY.Title.Visible = true;
                    diagram.AxisY.Title.Alignment = StringAlignment.Center;
                    diagram.AxisY.Title.Text = "No of Visitors";
                    diagram.AxisY.Title.TextColor = Color.Black;
                    diagram.AxisY.Title.Antialiasing = true;
                    diagram.AxisY.Title.Font = new Font("Tahoma", 14, FontStyle.Bold);
                    this.WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "NoofVisitors" });
                    this.WebChartControl1.Series[0].ChangeView(ViewType.Bar);
                    this.WebChartControl1.DataBind();
                }
                else if (ddlReportType.SelectedValue == "3")//yearly
                {
                    if (!string.IsNullOrEmpty(txtFromYear.Text.Trim()))
                    {
                        objVisitorRegisterBO.FromYear = Convert.ToInt32(txtFromYear.Text.Trim());
                    }
                    else
                    {
                        objVisitorRegisterBO.FromYear = 0;
                    }
                    if (!string.IsNullOrEmpty(txtToYear.Text.Trim()))
                    {
                        objVisitorRegisterBO.ToYear = Convert.ToInt32(txtToYear.Text.Trim());
                    }
                    else
                    {
                        objVisitorRegisterBO.ToYear = 0;
                    }
                    objVisitorRegisterBO.FromMonth = 0;
                    objVisitorRegisterBO.ToMonth = 0;
                    objVisitorRegisterBO.InDateTime = "";
                    objVisitorRegisterBO.OutDateTime = "";
                    objVisitorRegisterBO.Mode = 3;
                    dtChartData = objVisitorRegisterBAL.LoadChartNoofVisitors(objVisitorRegisterBO);
                    this.WebChartControl1.DataSource = dtChartData;
                    this.WebChartControl1.Series[0].ArgumentDataMember = "Year";
                    XYDiagram diagram = (XYDiagram)WebChartControl1.Diagram;
                    diagram.AxisX.Title.Visible = true;
                    diagram.AxisX.Title.Alignment = StringAlignment.Center;
                    diagram.AxisX.Title.Text = "Year";
                    diagram.AxisX.Title.TextColor = Color.Black;
                    diagram.AxisX.Title.Antialiasing = true;
                    diagram.AxisX.Title.Font = new Font("Tahoma", 14, FontStyle.Bold);

                    diagram.AxisY.Title.Visible = true;
                    diagram.AxisY.Title.Alignment = StringAlignment.Center;
                    diagram.AxisY.Title.Text = "No of Visitors";
                    diagram.AxisY.Title.TextColor = Color.Black;
                    diagram.AxisY.Title.Antialiasing = true;
                    diagram.AxisY.Title.Font = new Font("Tahoma", 14, FontStyle.Bold);
                    this.WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "NoofVisitors" });
                    this.WebChartControl1.Series[0].ChangeView(ViewType.Bar);
                    this.WebChartControl1.DataBind();
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSCh3:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSCh3:" + strline + " " + strMsg, "error");
            }
        }

        public void ChartFill2(int Mode1 = 0)
        {
            try
            {
                VisitorRegisterBO objVisitorRegisterBO = new VisitorRegisterBO();
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                DataTable dtChartData2 = new DataTable();
                if (ddlReportType.SelectedValue == "1")//weekly
                {
                    objVisitorRegisterBO.InDateTime = txtSearchVisitorsFromDate.Text.Trim();
                    objVisitorRegisterBO.OutDateTime = txtSearchVisitorsToDate.Text.Trim();
                    objVisitorRegisterBO.FromMonth = 0;
                    objVisitorRegisterBO.ToMonth = 0;
                    objVisitorRegisterBO.FromYear = 0;
                    objVisitorRegisterBO.ToYear = 0;
                    objVisitorRegisterBO.Mode = Mode1;
                    dtChartData2 = objVisitorRegisterBAL.LoadChartPurposeofVisit(objVisitorRegisterBO);
                    this.WebChartControl2.DataSource = dtChartData2;
                    this.WebChartControl2.Series[0].ArgumentDataMember = "Purpose_of_Visit";
                    this.WebChartControl2.Series[0].ValueDataMembers.AddRange(new string[] { "NoofVisitors" });
                    this.WebChartControl2.Series[0].ChangeView(ViewType.Pie);
                    this.WebChartControl2.Series[0].LegendTextPattern = "{A}";
                    this.WebChartControl2.DataBind();
                }
                if (ddlReportType.SelectedValue == "2")//weekly
                {
                    objVisitorRegisterBO.FromMonth = Convert.ToInt32(ddlfromMonths.SelectedValue);
                    objVisitorRegisterBO.ToMonth = Convert.ToInt32(ddlToMonth.SelectedValue);
                    objVisitorRegisterBO.InDateTime = "";
                    objVisitorRegisterBO.OutDateTime = "";
                    objVisitorRegisterBO.FromYear = Convert.ToInt32(txtMonthYear.Text.Trim());
                    objVisitorRegisterBO.ToYear = 0;
                    objVisitorRegisterBO.Mode = 2;
                    dtChartData2 = objVisitorRegisterBAL.LoadChartPurposeofVisit(objVisitorRegisterBO);
                    this.WebChartControl2.DataSource = dtChartData2;
                    this.WebChartControl2.Series[0].ArgumentDataMember = "Purpose_of_Visit";
                    this.WebChartControl2.Series[0].ValueDataMembers.AddRange(new string[] { "NoofVisitors" });
                    this.WebChartControl2.Series[0].ChangeView(ViewType.Pie);
                    this.WebChartControl2.Series[0].LegendTextPattern = "{A}";
                    this.WebChartControl2.DataBind();
                }
                else if (ddlReportType.SelectedValue == "3")//yearly
                {
                    if (!string.IsNullOrEmpty(txtFromYear.Text.Trim()))
                    {
                        objVisitorRegisterBO.FromYear = Convert.ToInt32(txtFromYear.Text.Trim());
                    }
                    else
                    {
                        objVisitorRegisterBO.FromYear = 0;
                    }
                    if (!string.IsNullOrEmpty(txtToYear.Text.Trim()))
                    {
                        objVisitorRegisterBO.ToYear = Convert.ToInt32(txtToYear.Text.Trim());
                    }
                    else
                    {
                        objVisitorRegisterBO.ToYear = 0;
                    }
                    objVisitorRegisterBO.FromMonth = 0;
                    objVisitorRegisterBO.ToMonth = 0;
                    objVisitorRegisterBO.InDateTime = "";
                    objVisitorRegisterBO.OutDateTime = "";
                    objVisitorRegisterBO.Mode = 3;
                    dtChartData2 = objVisitorRegisterBAL.LoadChartPurposeofVisit(objVisitorRegisterBO);
                    this.WebChartControl2.DataSource = dtChartData2;
                    this.WebChartControl2.Series[0].ArgumentDataMember = "Purpose_of_Visit";
                    this.WebChartControl2.Series[0].ValueDataMembers.AddRange(new string[] { "NoofVisitors" });
                    this.WebChartControl2.Series[0].ChangeView(ViewType.Pie);
                    this.WebChartControl2.Series[0].LegendTextPattern = "{A}";
                    this.WebChartControl2.DataBind();
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSCh4:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSCh4:" + strline + " " + strMsg);
            }
        }
        public void ChartFill3(int Mode1 = 0)
        {
            try
            {
                VisitorRegisterBO objVisitorRegisterBO = new VisitorRegisterBO();
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                DataTable dtChartData2 = new DataTable();
                if (ddlReportType.SelectedValue == "1")//weekly
                {
                    objVisitorRegisterBO.InDateTime = txtSearchVisitorsFromDate.Text.Trim();
                    objVisitorRegisterBO.OutDateTime = txtSearchVisitorsToDate.Text.Trim();
                    objVisitorRegisterBO.FromMonth = 0;
                    objVisitorRegisterBO.ToMonth = 0;
                    objVisitorRegisterBO.FromYear = 0;
                    objVisitorRegisterBO.ToYear = 0;
                    objVisitorRegisterBO.Mode = Mode1;
                    dtChartData2 = objVisitorRegisterBAL.LoadChartVehicle(objVisitorRegisterBO);
                    this.WebChartControl3.DataSource = dtChartData2;
                    this.WebChartControl3.Series[0].ArgumentDataMember = "VechileNo";
                    this.WebChartControl3.Series[0].ValueDataMembers.AddRange(new string[] { "KMS" });
                    this.WebChartControl3.Series[0].ChangeView(ViewType.Pie);
                    this.WebChartControl3.Series[0].LegendTextPattern = "{A}";
                    this.WebChartControl3.DataBind();
                }
                if (ddlReportType.SelectedValue == "2")//weekly
                {
                    objVisitorRegisterBO.FromMonth = Convert.ToInt32(ddlfromMonths.SelectedValue);
                    objVisitorRegisterBO.ToMonth = Convert.ToInt32(ddlToMonth.SelectedValue);
                    objVisitorRegisterBO.InDateTime = "";
                    objVisitorRegisterBO.OutDateTime = "";
                    objVisitorRegisterBO.FromYear = Convert.ToInt32(txtMonthYear.Text.Trim());
                    objVisitorRegisterBO.ToYear = 0;
                    objVisitorRegisterBO.Mode = 2;
                    dtChartData2 = objVisitorRegisterBAL.LoadChartVehicle(objVisitorRegisterBO);
                    this.WebChartControl3.DataSource = dtChartData2;
                    this.WebChartControl3.Series[0].ArgumentDataMember = "VechileNo";
                    this.WebChartControl3.Series[0].ValueDataMembers.AddRange(new string[] { "KMS" });
                    this.WebChartControl3.Series[0].ChangeView(ViewType.Pie);
                    this.WebChartControl3.Series[0].LegendTextPattern = "{A}";
                    this.WebChartControl3.DataBind();
                }
                else if (ddlReportType.SelectedValue == "3")//yearly
                {
                    if (!string.IsNullOrEmpty(txtFromYear.Text.Trim()))
                    {
                        objVisitorRegisterBO.FromYear = Convert.ToInt32(txtFromYear.Text.Trim());
                    }
                    else
                    {
                        objVisitorRegisterBO.FromYear = 0;
                    }
                    if (!string.IsNullOrEmpty(txtToYear.Text.Trim()))
                    {
                        objVisitorRegisterBO.ToYear = Convert.ToInt32(txtToYear.Text.Trim());
                    }
                    else
                    {
                        objVisitorRegisterBO.ToYear = 0;
                    }
                    objVisitorRegisterBO.FromMonth = 0;
                    objVisitorRegisterBO.ToMonth = 0;
                    objVisitorRegisterBO.InDateTime = "";
                    objVisitorRegisterBO.OutDateTime = "";
                    objVisitorRegisterBO.Mode = 3;
                    dtChartData2 = objVisitorRegisterBAL.LoadChartVehicle(objVisitorRegisterBO);
                    this.WebChartControl3.DataSource = dtChartData2;
                    this.WebChartControl3.Series[0].ArgumentDataMember = "VechileNo";
                    this.WebChartControl3.Series[0].ValueDataMembers.AddRange(new string[] { "KMS" });
                    this.WebChartControl3.Series[0].ChangeView(ViewType.Pie);
                    this.WebChartControl3.Series[0].LegendTextPattern = "{A}";
                    this.WebChartControl3.DataBind();
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSCh4:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSCh4:" + strline + " " + strMsg);
            }
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
           
            Response.Redirect("~/VmsHomePage.aspx");

        }
        protected void lbtnPurposeSearch_Click(object sender, EventArgs e)
        {
            ChartFill2(1);
        }

        protected void lbtnReset_Click(object sender, EventArgs e)
        {
            ChartFill2(0);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", " getDate();", true);
        }

        protected void btnFind1_Click(object sender, EventArgs e)
        {
            if (ddlChartType.SelectedValue == "1")
            {
                ChartFill(1);
                WebChartControl1.Visible = true;
                WebChartControl2.Visible = false;
                WebChartControl3.Visible = false;
            }
            else if (ddlChartType.SelectedValue == "2")
            {
                ChartFill2(1);
                WebChartControl1.Visible = false;
                WebChartControl2.Visible = true;
                WebChartControl3.Visible = false;
            }
            else if (ddlChartType.SelectedValue == "3")
            {
                ChartFill3(1);
                WebChartControl1.Visible = false;
                WebChartControl2.Visible = false;
                WebChartControl3.Visible = true;
            }
            
           
        }

        private void GetCardCount()
        {
            try
            {
                RolePrivilegesBO objRolePrivilegesBO = new RolePrivilegesBO();
                objRolePrivilegesBO.RoleID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                DataTable dtRole = objVMSRolePrivilegesBAL.GetEmpRoleID(objRolePrivilegesBO);
                objRolePrivilegesBO.RoleID = Convert.ToInt32(dtRole.Rows[0]["RoleID"].ToString());
                if (Convert.ToInt32(dtRole.Rows[0]["RoleID"].ToString()) == 17)
                {
                    //hr Count
                    MaterialBO objMaterialBO = new MaterialBO();
                    OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
                    objMaterialBO.EmpID = 0;
                    objMaterialBO.DeptID = 0;
                    objMaterialBO.TransStatus = 1;
                    objMaterialBO.RoleType = 1;
                    DataTable dtCardCount = objOutwardMaterialBAL.GetCardCount(objMaterialBO);
                    StringBuilder sbCard = new StringBuilder();
                    if (Convert.ToInt32(dtCardCount.Rows[0][0].ToString()) > 0)
                    {
                    string qs0 = HttpUtility.UrlEncode(HelpClass.Encrypt("hr"));
                    sbCard.Append("<a href ='VMS/VisitorFacilities/VisitorFacilityList.aspx?c=" + qs0 +"' >");
                    sbCard.Append("  <div class='col-lg-12 col-sm-12 col-xs-12 col-md-12 outer_dashboard float-left'><div class='col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard'>Facilities Pending for Approval</div><div class='col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none'><div class='col-lg-6 col-sm-6 col-xs-6 col-md-6 padding-none'><img src = '../Images/TmsDashboard/pending.png' class='dashboard-image' alt='Approve'/></div><div class='col-lg-6 col-sm-6 col-xs-6 col-md-6'><div Class='link_dashboard'>" + dtCardCount.Rows[0][0].ToString() + "</div></div></div></div></a>");
                    divCardshtml.InnerHtml = sbCard.ToString();
                    }
                    else
                    {
                        divCardshtml.Visible = false;
                    }
                }
                else if (Convert.ToInt32(dtRole.Rows[0]["RoleID"].ToString()) == 28 || Convert.ToInt32(dtRole.Rows[0]["RoleID"].ToString()) == 18)//BD-roleid 28
                {
                    //helpdesk count
                    MaterialBO objMaterialBO = new MaterialBO();
                    OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
                    objMaterialBO.EmpID = Convert.ToInt32(ViewState["EmpID"].ToString());
                    objMaterialBO.DeptID = 0;
                    objMaterialBO.TransStatus = 2;
                    objMaterialBO.RoleType = 2;
                    DataTable dtCardCount1 = objOutwardMaterialBAL.GetCardCount(objMaterialBO);
                    StringBuilder sbCard = new StringBuilder();
                    if (Convert.ToInt32(dtCardCount1.Rows[0][0].ToString()) > 0)
                    {
                    string qs1 = HttpUtility.UrlEncode(HelpClass.Encrypt("bd"));
                    sbCard.Append("<a href = 'VMS/VisitorFacilities/VisitorFacilityList.aspx?c=" + qs1 + "' >");
                    sbCard.Append("  <div class='col-lg-12 col-sm-12 col-xs-12 col-md-12 outer_dashboard float-left'><div class='col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard'>Facilities Reverted back</div><div class='col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none'><div class='col-lg-6 col-sm-6 col-xs-6 col-md-6 padding-none'><img src = '../Images/TmsDashboard/reverted_questions.png' class='dashboard-image' alt='Approve'/></div><div class='col-lg-6 col-sm-6 col-xs-6 col-md-6'><div Class='link_dashboard'>" + dtCardCount1.Rows[0][0].ToString() + "</div></div></div></div></a>");
                    divCardshtml.InnerHtml = sbCard.ToString();
                    }
                    else
                    {
                        divCardshtml.Visible = false;
                        divNoTask.Visible = true;
                    }
                }
                else if (Convert.ToInt32(dtRole.Rows[0]["RoleID"].ToString()) == 19)
                {
                    //materialStore Count
                    MaterialBO objMaterialBO = new MaterialBO();
                    OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
                    objMaterialBO.EmpID = 0;
                    objMaterialBO.DeptID = 0;
                    objMaterialBO.TransStatus = 2;
                    objMaterialBO.RoleType = 4;
                    DataTable dtCardCount2 = objOutwardMaterialBAL.GetCardCount(objMaterialBO);
                    StringBuilder sbCard = new StringBuilder();
                    if (Convert.ToInt32(dtCardCount2.Rows[0][0].ToString()) > 0)
                    {
                    string qs2 = HttpUtility.UrlEncode(HelpClass.Encrypt("mstore"));
                    sbCard.Append("<a href = 'VMS/MaterialRegister/MaterialRegister.aspx?c=" + qs2 + "' >");
                    sbCard.Append("  <div class='col-lg-12 col-sm-12 col-xs-12 col-md-12 outer_dashboard float-left'><div class='col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard'>Materials Pending for Approval</div><div class='col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none'><div class='col-lg-6 col-sm-6 col-xs-6 col-md-6 padding-none'><img src = '../Images/TmsDashboard/pending.png' class='dashboard-image' alt='Approve'/></div><div class='col-lg-6 col-sm-6 col-xs-6 col-md-6'><div Class='link_dashboard'>" + dtCardCount2.Rows[0][0].ToString() + "</div></div></div></div></a>");
                    divCardshtml.InnerHtml = sbCard.ToString();
                    }
                    else
                    {
                        divCardshtml.Visible = false;
                        divNoTask.Visible = true;
                    }
                }
                else if (Convert.ToInt32(dtRole.Rows[0]["RoleID"].ToString()) == 20)
                {
                    //materialHod Count
                    MaterialBO objMaterialBO = new MaterialBO();
                    OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
                    objMaterialBO.EmpID = Convert.ToInt32(ViewState["EmpID"].ToString());
                    objMaterialBO.DeptID = 0;
                    objMaterialBO.TransStatus = 1;
                    objMaterialBO.RoleType = 3;
                    DataTable dtCardCount3 = objOutwardMaterialBAL.GetCardCount(objMaterialBO);
                    StringBuilder sbCard = new StringBuilder();
                    if (Convert.ToInt32(dtCardCount3.Rows[0][0].ToString()) > 0)
                    {
                    string qs3 = HttpUtility.UrlEncode(HelpClass.Encrypt("mhod"));
                    sbCard.Append("<a href = 'VMS/MaterialRegister/MaterialRegister.aspx?c=" + qs3 + "' >");
                    sbCard.Append("  <div class='col-lg-12 col-sm-12 col-xs-12 col-md-12 outer_dashboard float-left'><div class='col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard'>Materials Pending for Approval</div><div class='col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none'><div class='col-lg-6 col-sm-6 col-xs-6 col-md-6 padding-none'><img src = '../Images/TmsDashboard/pending.png' class='dashboard-image' alt='Approve'/></div><div class='col-lg-6 col-sm-6 col-xs-6 col-md-6'><div Class='link_dashboard'>" + dtCardCount3.Rows[0][0].ToString() + "</div></div></div></div></a>");
                    divCardshtml.InnerHtml = sbCard.ToString();
                    }
                    else
                    {
                        divCardshtml.Visible = false;
                        divNoTask.Visible = true;
                    }
                }
                else if (Convert.ToInt32(dtRole.Rows[0]["RoleID"].ToString()) == 21)
                {
                    //materialUser Count
                    MaterialBO objMaterialBO = new MaterialBO();
                    OutwardMaterialBAL objOutwardMaterialBAL = new OutwardMaterialBAL();
                    objMaterialBO.EmpID = Convert.ToInt32(ViewState["EmpID"].ToString());
                    objMaterialBO.DeptID = 0;
                    objMaterialBO.TransStatus = 2;
                    objMaterialBO.RoleType = 5;
                    DataTable dtCardCount4 = objOutwardMaterialBAL.GetCardCount(objMaterialBO);
                    StringBuilder sbCard = new StringBuilder();
                    if (Convert.ToInt32(dtCardCount4.Rows[0][0].ToString()) > 0)
                    {
                        string qs4 = HttpUtility.UrlEncode(HelpClass.Encrypt("muser"));
                        sbCard.Append("<a href = 'VMS/MaterialRegister/MaterialRegister.aspx?c=" + qs4 + "' >");
                        sbCard.Append(" <div class='col-lg-12 col-sm-12 col-xs-12 col-md-12 outer_dashboard float-left'><div class='col-lg-12 col-sm-12 col-xs-12 col-md-12 header_text_dshboard'>Materials Reverted back</div><div class='col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none'><div class='col-lg-6 col-sm-6 col-xs-6 col-md-6 padding-none'><img src = '../Images/TmsDashboard/reverted_questions.png' class='dashboard-image' alt='Approve'/></div><div class='col-lg-6 col-sm-6 col-xs-6 col-md-6'><div Class='link_dashboard'>" + dtCardCount4.Rows[0][0].ToString() + "</div></div></div></div></a>");
                        divCardshtml.InnerHtml = sbCard.ToString();
                    }
                    else
                    {
                        divCardshtml.Visible = false;
                        divNoTask.Visible = true;
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSCh3:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSCh3:" + strline + " " + strMsg);
            }
        }

        protected void ddlReportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowHideReportTypeSelection();
        }

        private void ShowHideReportTypeSelection()
        {
            if (ddlReportType.SelectedValue== "1")
            {
                div_FromDate.Visible = true;
                div_ToDate.Visible = true;
                div_FromMonths.Visible = false;
                div_ToMonth.Visible = false;
                div_FromYear.Visible = false;
                div_ToYear.Visible = false;
                div_MonthYear.Visible = false;
            }
            else if (ddlReportType.SelectedValue == "2")
            {
                div_FromMonths.Visible = true;
                div_ToMonth.Visible = true;
                div_FromDate.Visible = false;
                div_ToDate.Visible = false;
                div_FromYear.Visible = false;
                div_ToYear.Visible = false;
                div_MonthYear.Visible = true;
            }
            else if (ddlReportType.SelectedValue == "3")
            {
                div_FromYear.Visible = true;
                div_ToYear.Visible = true;
                div_FromMonths.Visible = false;
                div_ToMonth.Visible = false;
                div_FromDate.Visible = false;
                div_ToDate.Visible = false;
                div_MonthYear.Visible = false;
            }
            upReportType.Update(); upcontrolsVisible.Update();
        }

        protected void ddlfromMonths_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlToMonth.Items.Clear();
            ddlToMonth.Items.Insert(0, new ListItem("--Select--", "0"));
            if (ddlfromMonths.SelectedIndex>0)
            {
                var months = CultureInfo.CurrentCulture.DateTimeFormat.MonthGenitiveNames;
                for (int i = 1; i <= months.Length-1; i++)
                {
                    if (i>=Convert.ToInt32(ddlfromMonths.SelectedValue))
                    {
                        ddlToMonth.Items.Add(new ListItem(months[i-1], (i).ToString()));
                    }
                }
            }
        }

       

        protected void lbtnExport_Click(object sender, EventArgs e)
        {
            ExportToExcel objExport = new ExportToExcel();
            DataTable dtExport = new DataTable();
            string strMessage = string.Empty;
            if (ddlChartType.SelectedValue == "1")
            {
                dtExport= Chart1Export(1);
                strMessage = objExport.ExportExcel(dtExport,"No. of Visitors Report",null,null,0,0);
                HelpClass.custAlertMsg(this, this.GetType(), strMessage, "success");
            }
            else if (ddlChartType.SelectedValue == "2")
            {
                ChartFill2(1);

            }
            else if (ddlChartType.SelectedValue == "3")
            {
                ChartFill3(1);
            }
               
        }
        DataTable Chart1Export(int Mode=0)
        {
            DataTable dtChartData = new DataTable();
            try
            {
                VisitorRegisterBO objVisitorRegisterBO = new VisitorRegisterBO();
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
               
                if (ddlReportType.SelectedValue == "1")//weekly
                {
                    objVisitorRegisterBO.SNo = 1;
                   objVisitorRegisterBO.InDateTime = txtSearchVisitorsFromDate.Text.Trim();
                    objVisitorRegisterBO.OutDateTime = txtSearchVisitorsToDate.Text.Trim();
                    objVisitorRegisterBO.FromMonth = 0;
                    objVisitorRegisterBO.ToMonth = 0;
                    objVisitorRegisterBO.FromYear = 0;
                    objVisitorRegisterBO.ToYear = 0;
                    objVisitorRegisterBO.Mode = Mode;
                    dtChartData = objVisitorRegisterBAL.GetExportData(objVisitorRegisterBO);
                   
                }
                else if (ddlReportType.SelectedValue == "2")//monthly
                {
                    objVisitorRegisterBO.SNo = 1;
                    objVisitorRegisterBO.FromMonth = Convert.ToInt32(ddlfromMonths.SelectedValue);
                    objVisitorRegisterBO.ToMonth = Convert.ToInt32(ddlToMonth.SelectedValue);
                    objVisitorRegisterBO.InDateTime = "";
                    objVisitorRegisterBO.OutDateTime = "";
                    objVisitorRegisterBO.FromYear = Convert.ToInt32(txtMonthYear.Text.Trim());
                    objVisitorRegisterBO.ToYear = 0;
                    objVisitorRegisterBO.Mode = 2;
                    dtChartData = objVisitorRegisterBAL.GetExportData(objVisitorRegisterBO);
                   
                }
                else if (ddlReportType.SelectedValue == "3")//yearly
                {
                    if (!string.IsNullOrEmpty(txtFromYear.Text.Trim()))
                    {
                        objVisitorRegisterBO.FromYear = Convert.ToInt32(txtFromYear.Text.Trim());
                    }
                    else
                    {
                        objVisitorRegisterBO.FromYear = 0;
                    }
                    if (!string.IsNullOrEmpty(txtToYear.Text.Trim()))
                    {
                        objVisitorRegisterBO.ToYear = Convert.ToInt32(txtToYear.Text.Trim());
                    }
                    else
                    {
                        objVisitorRegisterBO.ToYear = 0;
                    }
                    objVisitorRegisterBO.SNo = 1;
                    objVisitorRegisterBO.FromMonth = 0;
                    objVisitorRegisterBO.ToMonth = 0;
                    objVisitorRegisterBO.InDateTime = "";
                    objVisitorRegisterBO.OutDateTime = "";
                    objVisitorRegisterBO.Mode = 3;
                    dtChartData = objVisitorRegisterBAL.GetExportData(objVisitorRegisterBO);                   
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSCh31:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSCh31:" + strline + " " + strMsg, "error");
            }
            return dtChartData;
        }
    }
}