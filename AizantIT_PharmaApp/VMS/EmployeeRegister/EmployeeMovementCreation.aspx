﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/VMS_Master.Master" AutoEventWireup="true" CodeBehind="EmployeeMovementCreation.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.EmployeeRegister.EmployeeMovementCreation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="margin-top: 10px;">
        <div class="row Panel_Frame" style="width: 900px; margin-left: 17%">
            <div id="header">
                <div class="col-sm-12 Panel_Header" >
                    <div class="col-sm-6" style="padding-left: 5px">
                        <span class="Panel_Title">Employee Movement</span>
                    </div>
                  
                </div>
            </div>
            
           
                <asp:UpdatePanel ID="UpdatePanel13" runat="server">
               <ContentTemplate>

                   <div class="Row">
   <div class="col-sm-6">

<div class="form-horizontal" style="margin:5px;">
                            <div class="form-group" runat="server" id="div5">
                                <label class="control-label col-sm-4" id="labelEmployeeMovementType" style="text-align: left; font-weight: lighter">Movement Type<span class="mandatoryStar">*</span></label>
                              <div class="col-sm-7">
                               <asp:Dropdownlist ID="ddlEmployeeMovementType" runat="server" CssClass="form-control SearchDropDown" AutoPostBack="true" OnSelectedIndexChanged="ddlEmployeeMovementType_SelectedIndexChanged">
                                
                            </asp:Dropdownlist> 
                            </div>
                            </div>
                                  </div>
       <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
<div class="form-horizontal" style="margin:5px;">
                            <div class="form-group" runat="server" id="div1">
                                <label class="control-label col-sm-4" id="labelDept" style="text-align: left; font-weight: lighter">Department<span class="mandatoryStar">*</span></label>
                              <div class="col-sm-7">
                               <asp:Dropdownlist ID="ddlDepartment" runat="server" CssClass="form-control SearchDropDown" AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"></asp:Dropdownlist> 
                            </div>
                            </div>
                             </div>
                                   </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlDepartment" EventName="SelectedIndexChanged"/>
                            </Triggers>
                                   </asp:UpdatePanel>

       <asp:UpdatePanel runat="server" UpdateMode="Always">
                            <ContentTemplate>
<div class="form-horizontal" style="margin:5px;">
                            <div class="form-group" runat="server" id="div2">
                                <label class="control-label col-sm-4" id="labelEmployeeName" style="text-align: left; font-weight: lighter">Employee Name<span class="mandatoryStar">*</span></label>
                              <div class="col-sm-7">
                                <asp:Dropdownlist ID="ddlEmpID" runat="server" AutoPostBack="true" CssClass="form-control SearchDropDown"></asp:Dropdownlist>
                            </div>
                            </div>
                                  </div>
                                  </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlEmpID" EventName="SelectedIndexChanged"/>
                            </Triggers>
                                   </asp:UpdatePanel>
    </div>
<div class="col-sm-6">

<div class="form-horizontal" style="margin:5px;">
                            <div class="form-group" runat="server" id="div3">
                                <label class="control-label col-sm-4" id="labelPurpose" style="text-align: left; font-weight: lighter">Purpose<span class="mandatoryStar">*</span></label>
                              <div class="col-sm-7">
                                <asp:TextBox ID="txtPurpose" runat="server" CssClass="form-control" placeholder="Enter Purpose" onkeypress="return ValidateAlpha(event);"></asp:TextBox>
                            </div>
                            </div>
                                  </div>

                       <div class="form-horizontal" style="margin:5px;">
                            <div class="form-group" runat="server" id="DIVOutDateTime">
                                <label class="control-label col-sm-4" id="labelOutDateTime" style="text-align: left; font-weight: lighter">OutDateTime</label>
                              <div class="col-sm-7">
                                <asp:TextBox ID="txtOutDateTime" runat="server" BackColor="White" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                            </div>
                            </div>
                                  </div>

                       <div class="form-horizontal" style="margin:5px;">
                            <div class="form-group" runat="server" id="DIVInDateTime" visible="false">
                                <label class="control-label col-sm-4" id="labelInDateTime" style="text-align: left; font-weight: lighter">InDateTime</label>
                              <div class="col-sm-7">
                                <asp:TextBox ID="txtInDateTime" runat="server" CssClass="form-control" BackColor="White" ReadOnly="true"></asp:TextBox>
                            </div>
                            </div>
                                  </div>

<div class="form-horizontal" style="margin:5px;">
                            <div class="form-group" runat="server" id="div6">
                                <label class="control-label col-sm-4" id="labelRemarks" style="text-align: left; font-weight: lighter">Remarks</label>
                              <div class="col-sm-7">
                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" placeholder="Enter Remarks" TextMode="MultiLine" Rows="2"></asp:TextBox>
                            </div>
                            </div>
                                  </div>
</div>
</div>                        
   

                          <div class="col-sm-12 modal-footer" style="text-align: center;padding-bottom:2px;padding-top:2px;margin-top:10px">
                            <asp:Button ID="btnSubmit" Text="Submit" CssClass="button" runat="server" ValidationGroup="ab" OnClick="btnSubmit_Click" OnClientClick="javascript:return Submitvaidate();" />
                            <asp:Button ID="btnReset" Text="Reset" CssClass="button" runat="server" CausesValidation="false" OnClick="btnReset_Click"/>
                         </div>
          

     <script type="text/javascript">
         function Submitvaidate() {
             var EType = document.getElementById("<%=ddlEmployeeMovementType.ClientID%>").value;
             var EDept = document.getElementById("<%=ddlDepartment.ClientID%>").value;
             var Purpose = document.getElementById("<%=txtPurpose.ClientID%>").value;
             var EmpName = document.getElementById("<%=ddlEmpID.ClientID%>").value;

             errors = [];
             if (EType == 0) {
                 errors.push("Please Select Movement Type.");
             }
             if (EDept == 0) {
                 errors.push("Please Select Department.");
             }
             if (EmpName == 0) {
                 errors.push("Please Select Employee.");
             }
             if (Purpose.trim() == "") {
                 errors.push("Please Enter Purpose.");
             }
             if (errors.length > 0) {
                 alert(errors.join("\n"));
                 return false;
             }
        }
        </script>
     <script>
        $("#NavLnkEmployee").attr("class", "active");
        $("#EmployeeRegister").attr("class", "active");
    </script>
     </ContentTemplate>
</asp:UpdatePanel>
                </div>
            </div>
</asp:Content>
