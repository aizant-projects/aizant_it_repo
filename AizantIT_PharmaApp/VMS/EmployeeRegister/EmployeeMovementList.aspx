﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/VMS_Master.Master" AutoEventWireup="true" CodeBehind="EmployeeMovementList.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.EmployeeRegister.EmployeeMovementList" EnableEventValidation="false"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        th {
            text-align: center
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="margin-top: 10px;">
        <div class="row Panel_Frame" style="width: 1100px; margin-left: 10%">
            <div id="header">
                <div class="col-sm-12 Panel_Header">
                    <div class="col-sm-6" style="padding-left: 5px">
                        <span class="Panel_Title">Employee Movement List</span>
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div>
            <div id="body">
              

                <div class="Row">
                    <div class="col-sm-6">
                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div2">
                                        <label class="control-label col-sm-4" id="labelEmpMovement" style="text-align: left; font-weight: lighter">Employee Movement<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlMovementType" runat="server" CssClass="form-control SearchDropDown" AutoPostBack="true" OnSelectedIndexChanged="ddlMovementType_SelectedIndexChanged">
                                              
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div1">
                                        <label class="control-label col-sm-4" id="labelDepartment" style="text-align: left; font-weight: lighter">Department</label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlDepartment" runat="server" AutoPostBack="true" CssClass="form-control SearchDropDown" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlDepartment" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>

                        <asp:UpdatePanel runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div5">
                                        <label class="control-label col-sm-4" id="labelName" style="text-align: left; font-weight: lighter">Employee Name</label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlEmpID" runat="server" CssClass="form-control SearchDropDown"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlEmpID" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div4">
                                <label class="control-label col-sm-4" id="labelFromDate" style="text-align: left; font-weight: lighter">From Date</label>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txtDateFrom" runat="server" CssClass="form-control" placeholder="Select From Date"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div6">
                                <label class="control-label col-sm-4" id="label3" style="text-align: left; font-weight: lighter">To Date</label>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txtDateTo" runat="server" CssClass="form-control" placeholder="Select To Date"></asp:TextBox><br />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                    <asp:Button ID="btnStaffFind" Text="Find" CssClass="button" runat="server" OnClick="btnStaffFind_Click" OnClientClick="javascript:return Submitvaidate();" ValidationGroup="ab" />
                    <asp:Button ID="btnstaffReset" Text="Reset" CssClass="button" runat="server" OnClick="btnstaffReset_Click" CausesValidation="false" />
                </div>
                <div class="clearfix">
                </div>
                <div>
                    <asp:GridView ID="StaffMovementGridView" runat="server" DataKeyNames="SNo" PagerStyle-CssClass="GridPager" CssClass="table table-bordered table-inverse" Style="text-align: center" Font-Size="12px" Width="100%" HeaderStyle-Wrap="false" AutoGenerateColumns="False" OnRowDataBound="StaffMovementGridView_RowDataBound" HeaderStyle-Width="5%" EmptyDataText="Currently No Records Found" CellPadding="3" HeaderStyle-HorizontalAlign="Center" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" AllowPaging="True" ForeColor="#66CCFF" OnPageIndexChanging="StaffMovementGridView_PageIndexChanging">
                        <EmptyDataRowStyle ForeColor="#FF3300" HorizontalAlign="Center" />
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <HeaderStyle BackColor="#748CB2" Font-Bold="True" HorizontalAlign="Center" ForeColor="White" Height="30px" />
                        <PagerStyle ForeColor="White" HorizontalAlign="Center" BackColor="White" />
                        <RowStyle ForeColor="#000066" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#0c99f0" />
                        <SortedAscendingHeaderStyle BackColor="#0c99f0" />
                        <SortedDescendingCellStyle BackColor="#0c99f0" />
                        <SortedDescendingHeaderStyle BackColor="#0c99f0" />
                        <Columns>
                            <asp:TemplateField HeaderText="SNO" SortExpression="SNo">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtGVSno" runat="server" Text='<%# Bind("SNo") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblGVStaffSno" runat="server" Text='<%# Bind("SNo") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                          
                            <asp:TemplateField HeaderText="EMPLOYEE NAME" SortExpression="EmpName">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtGVEmployeeName" runat="server" Text='<%# Bind("EmpName") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblGVStaffEmployeeName" runat="server" Text='<%# Bind("EmpName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DEPARTMENT" SortExpression="Department">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtGVDepartment" runat="server" Text='<%# Bind("Department") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblGVDepartment" runat="server" Text='<%# Bind("Department") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PURPOSE" SortExpression="Purpose">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtGVPurpose" runat="server" Text='<%# Bind("Purpose") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblGVPurpose" runat="server" Text='<%# Bind("Purpose") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="OUT DATE TIME" SortExpression="OutDateTime">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtGVOutDateTime" runat="server" Text='<%# Bind("OutDateTime") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblGVStaffOutDateTime" runat="server" Text='<%# Bind("OutDateTime") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="IN DATE TIME" SortExpression="InDateTime">

                                <ItemTemplate>
                                    <asp:Label ID="lblGVInDateTime" runat="server" Text='<%# Bind("InDateTime") %>'></asp:Label>
                                    <asp:LinkButton ID="lnkCheckOutStaff" runat="server" OnClick="lnkCheckOutStaff_Click" ForeColor="#ff0000" Visible="false">CheckIn</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="EDIT">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkStaffMovment" runat="server" CssClass="glyphicon glyphicon-edit GV_Edit_Icon" OnClick="LinkStaffMovment_Click"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                         
                        </Columns>
                    </asp:GridView>
                </div>
                <asp:ModalPopupExtender ID="ModalPopupStaff" runat="server" TargetControlID="btnStaffUpdate" PopupControlID="panelStaff" RepositionMode="RepositionOnWindowResizeAndScroll" DropShadow="true" PopupDragHandleControlID="panelAddNewTitle" BackgroundCssClass="modalBackground"></asp:ModalPopupExtender>
                <asp:Panel ID="panelStaff" runat="server" Style="display: none; background-color: white;" ForeColor="Black" Width="900px" Height="330px">
                    <asp:Panel ID="panel3" runat="server" Style="cursor: move; font-family: Tahoma; padding: 7px;" HorizontalAlign="Center" BackColor="#748CB2" Font-Bold="true" ForeColor="White" Height="35px">
                        <b>Modify Staff Movement Check Out</b>
                    </asp:Panel>
                    <div class="Row">
                        <div class="col-sm-6">

                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div3">
                                    <label class="control-label col-sm-4" id="labelSNo" style="text-align: left; font-weight: lighter">SNo</label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtStaffSno" runat="server" CssClass="form-control" BackColor="White" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="form-horizontal" style="margin: 5px;">
                                        <div class="form-group" runat="server" id="div7">
                                            <label class="control-label col-sm-4" id="labelDept" style="text-align: left; font-weight: lighter">Department<span class="mandatoryStar">*</span></label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddlStaffDepartment" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlStaffDepartment_SelectedIndexChanged"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Select Department" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlStaffDepartment" InitialValue="0" ValidationGroup="Staff" ></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlStaffDepartment" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <div class="form-horizontal" style="margin: 5px;">
                                        <div class="form-group" runat="server" id="div8">
                                            <label class="control-label col-sm-4" id="labelStaffEmployeeName" style="text-align: left; font-weight: lighter">Employee Name<span class="mandatoryStar">*</span></label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddlStaffEmployee" runat="server" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Select Employee" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlStaffEmployee" InitialValue="0" ValidationGroup="Staff"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlEmpID" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div13">
                                    <label class="control-label col-sm-4" id="labelPurpose" style="text-align: left; font-weight: lighter">Purpose<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtStaffPurpose" runat="server" CssClass="form-control" placeholder="Enter Purpose" onkeypress="return ValidateAlpha(event);"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter Purpose" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtStaffPurpose" ValidationGroup="Staff"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-6">

                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="DIVOutDateTime">
                                    <label class="control-label col-sm-4" id="labelOutDateTime" style="text-align: left; font-weight: lighter">OutDateTime</label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtOutDateTime" runat="server" BackColor="White" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>


                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div14">
                                    <label class="control-label col-sm-4" id="labelRemarks" style="text-align: left; font-weight: lighter">Remarks</label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" placeholder="Enter Remarks" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div12">
                                    <label class="control-label col-sm-4" id="labelComments" style="text-align: left; font-weight: lighter">Comments<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtStaffComments" runat="server" CssClass="form-control" placeholder="Enter Comments" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Enter Comments" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtStaffComments" ValidationGroup="Staff"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                        <asp:Button ID="btnStaffUpdate" runat="server" Text="Update" CssClass="button" OnClick="btnStaffUpdate_Click" UseSubmitBehavior="false" ValidationGroup="Staff"/>
                        <asp:Button ID="btnStaffCancel" runat="server" Text="Cancel" CssClass="button" CausesValidation="false" OnClick="btnStaffCancel_Click" />
                    </div>
                </asp:Panel>
                <!--Panel to Edit record-->
                <div class="clearfix">
                </div>
                <div>
                    <asp:GridView ID="HolidayRegisterGridView" runat="server" DataKeyNames="SNo" PagerStyle-CssClass="GridPager" CssClass="table table-bordered table-inverse" Style="text-align: center" Font-Size="12px" OnRowDataBound="HolidayRegisterGridView_RowDataBound" HeaderStyle-Wrap="false" AutoGenerateColumns="False" Width="100%" HeaderStyle-Width="5%" EmptyDataText="Currently No Records Found" CellPadding="3" HeaderStyle-HorizontalAlign="Center" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" AllowPaging="True" ForeColor="#66CCFF" OnPageIndexChanging="HolidayRegisterGridView_PageIndexChanging">
                        <EmptyDataRowStyle ForeColor="#FF3300" HorizontalAlign="Center" />
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <HeaderStyle BackColor="#748CB2" Font-Bold="True" ForeColor="White" Height="30px" />
                        <PagerStyle ForeColor="#000066" HorizontalAlign="Center" BackColor="White" />
                        <RowStyle ForeColor="#000066" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#0c99f0" />
                        <SortedAscendingHeaderStyle BackColor="#0c99f0" />
                        <SortedDescendingCellStyle BackColor="#0c99f0" />
                        <SortedDescendingHeaderStyle BackColor="#0c99f0" />
                        <Columns>
                            <asp:TemplateField HeaderText="SNO" SortExpression="SNo">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtGVSno" runat="server" Text='<%# Bind("SNo") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblGVHolidaySno" runat="server" Text='<%# Bind("SNo") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                           
                            <asp:TemplateField HeaderText="EMPLOYEE NAME" SortExpression="EmpName">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtGVEmployeeName" runat="server" Text='<%# Bind("EmpName") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblGVHolidayEmployeeName" runat="server" Text='<%# Bind("EmpName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DEPARTMENT" SortExpression="Department">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtGVDepartment" runat="server" Text='<%# Bind("Department") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblGVDepartment" runat="server" Text='<%# Bind("Department") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PURPOSE" SortExpression="Purpose">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtGVPurpose" runat="server" Text='<%# Bind("Purpose") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblGVPurpose" runat="server" Text='<%# Bind("Purpose") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="IN DATE TIME" SortExpression="InDateTime">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtGVInDateTime" runat="server" Text='<%# Bind("InDateTime") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblGVInDateTime" runat="server" Text='<%# Bind("InDateTime") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="OUT DATE TIME" SortExpression="OutDateTime">
                                <ItemTemplate>
                                    <asp:Label ID="lblGVReturnDateTime" runat="server" Text='<%# Bind("OutDateTime") %>'></asp:Label>
                                    <asp:LinkButton ID="lnkCheckOutHoliday" runat="server" OnClick="lnkCheckOutHoliday_Click" ForeColor="#ff0000" Visible="false">CheckOut</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="EDIT">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkHolidayRegister" runat="server" CssClass="glyphicon glyphicon-edit GV_Edit_Icon" OnClick="LinkHolidayRegister_Click"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <asp:ModalPopupExtender ID="ModalPopupHoliday" runat="server" TargetControlID="btnHolidayUpdate" PopupControlID="panelHoliday" RepositionMode="RepositionOnWindowResizeAndScroll" DropShadow="true" PopupDragHandleControlID="panelAddNewTitle" BackgroundCssClass="modalBackground"></asp:ModalPopupExtender>
                <asp:Panel ID="panelHoliday" runat="server" Style="display: none; background-color: white;" ForeColor="Black" Width="900px" Height="300px">
                    <asp:Panel ID="panel2" runat="server" Style="cursor: move; font-family: Tahoma; padding: 7px;" HorizontalAlign="Center" BackColor="#748CB2" Font-Bold="true" ForeColor="White" Height="35px">
                        <b>Modify Holiday Movement Check In</b>
                    </asp:Panel>
                    <div class="Row">
                        <div class="col-sm-6">
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div9">
                                    <label class="control-label col-sm-4" id="lblHSNo" style="text-align: left; font-weight: lighter">SNo</label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtHSNo" runat="server" CssClass="form-control" BackColor="White" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="form-horizontal" style="margin: 5px;">
                                        <div class="form-group" runat="server" id="div10">
                                            <label class="control-label col-sm-4" id="lblHDept" style="text-align: left; font-weight: lighter">Department<span class="mandatoryStar">*</span></label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddlHDepartment" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlHDepartment_SelectedIndexChanged"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Select Department" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlHDepartment" InitialValue="0" ValidationGroup="Holiday"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlHDepartment" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <div class="form-horizontal" style="margin: 5px;">
                                        <div class="form-group" runat="server" id="div11">
                                            <label class="control-label col-sm-4" id="lblHEmployeeName" style="text-align: left; font-weight: lighter">Employee Name<span class="mandatoryStar">*</span></label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddlHEmp" runat="server" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Select Employee" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlHEmp" InitialValue="0" ValidationGroup="Holiday"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlHEmp" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div15">
                                    <label class="control-label col-sm-4" id="lblHPurpose" style="text-align: left; font-weight: lighter">Purpose<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtHPurpose" runat="server" CssClass="form-control" placeholder="Enter Purpose" onkeypress="return ValidateAlpha(event);"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Enter Purpose" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtHPurpose" ValidationGroup="Holiday"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="DIV16">
                                    <label class="control-label col-sm-4" id="lblHDateTime" style="text-align: left; font-weight: lighter">InDateTime</label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtHIndatetime" runat="server" BackColor="White" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div17">
                                    <label class="control-label col-sm-4" id="lblHRemarks" style="text-align: left; font-weight: lighter">Remarks</label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtHRemarks" runat="server" CssClass="form-control" placeholder="Enter Remarks" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div18">
                                    <label class="control-label col-sm-4" id="lblHComments" style="text-align: left; font-weight: lighter">Comments<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtHComments" runat="server" CssClass="form-control" placeholder="Enter Comments" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Enter Comments" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtHComments" ValidationGroup="Holiday"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                        <asp:Button ID="btnHolidayUpdate" runat="server" Text="Update" CssClass="button" OnClick="btnHolidayUpdate_Click" UseSubmitBehavior="false" ValidationGroup="Holiday"/>
                        <asp:Button ID="btnHolidayCancel" runat="server" Text="Cancel" CssClass="button" CausesValidation="false" OnClick="btnHolidayCancel_Click" />
                    </div>
                </asp:Panel>
                <!--Panel to Edit record-->
            </div>
        </div>
    </div>
    <script>
        $("#NavLnkEmployee").attr("class", "active");
        $("#EmployeeRegisterList").attr("class", "active");
    </script>


   
      <script>
          //for start date and end date validations startdate should be < enddate
          $(function () {
              $('#<%=txtDateFrom.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        useCurrent: false,
                    });
                    var startdate = document.getElementById('<%=txtDateFrom.ClientID%>').value
                    $('#<%=txtDateTo.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                minDate: startdate,
                useCurrent: false,
            });
            $('#<%=txtDateFrom.ClientID%>').on("dp.change", function (e) {
                    $('#<%=txtDateTo.ClientID%>').data("DateTimePicker").minDate(e.date);
               <%--  $('#<%=txtEndDate.ClientID%>').value = $('#<%=txtStartDate.ClientID%>').value; --%>
                    $('#<%=txtDateTo.ClientID%>').val("");
                });
                $('#<%=txtDateTo.ClientID%>').on("dp.change", function (e) {
                });
          });
    </script>

      <script type="text/javascript">
                //for datepickers
                var startdate2 = document.getElementById('<%=txtDateFrom.ClientID%>').value
                if (startdate2 == "") {
                    $('#<%=txtDateFrom.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        useCurrent: false,
                    });
                    $('#<%=txtDateTo.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        useCurrent: false,
                    });
                }
    </script>
    <script type="text/javascript">
         function Submitvaidate() {
             var EmployeeType = document.getElementById("<%=ddlMovementType.ClientID%>");

             errors = [];
             if (EmployeeType.value == 0) {
                 errors.push("Please Select Employee Movement Type.");
             }

             if (errors.length > 0) {
                 alert(errors.join("\n"));
                 return false;
             }
         }
    </script>
</asp:Content>
