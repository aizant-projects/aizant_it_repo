﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VMS_BAL;
using VMS_BO;
using AizantIT_PharmaApp.Common;
using System.Data;
using System.Data.SqlClient;


namespace AizantIT_PharmaApp.VMS.EmployeeRegister
{
    public partial class EmployeeMovementCreation : System.Web.UI.Page
    {
        DataTable dt;
        SupplyRegisterBO objSupplyRegisterBO;
        EmployeeMovementBAL objEmployeeMovementBAL = new EmployeeMovementBAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            ViewState["EmpID"] = "0";
                            if (HelpClass.IsUserAuthenticated())
                            {
                                ViewState["EmpID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            }
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaff7:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaff7:" + strline + " " + strMsg);
            }
           
        }
        private void InitializeThePage()
        {
            try
            {
                LoadDepartment();
                TimeDate();
                BindSupplyType();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaff6:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaff6:" + strline + " " + strMsg);
            }
        }

        private void TimeDate()
        {
            txtOutDateTime.Text = DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
            txtInDateTime.Text = DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
        }

        private void LoadDepartment()
        {
            try
            {
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                dt = new DataTable();
                dt = objVisitorRegisterBAL.LoadDepartment();
                ddlDepartment.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlDepartment.DataValueField = dt.Columns["DeptID"].ToString();
                ddlDepartment.DataSource = dt;
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("--Select--", "0"));   
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaffR1:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaffR1:" + strline + " " + strMsg);
            }
        }


        protected void ddlEmployeeMovementType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlEmployeeMovementType.SelectedIndex == 1)
            {
                DIVOutDateTime.Visible = true;
                DIVInDateTime.Visible = false;
                EmployeeMovement_Reset();
            }
            else if(ddlEmployeeMovementType.SelectedIndex==2)
            {
                DIVOutDateTime.Visible = false;
                DIVInDateTime.Visible = true;
                EmployeeMovement_Reset();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (txtPurpose.Text.Trim() == "" || ddlDepartment.SelectedValue == "0" || ddlEmpID.SelectedValue == "0" || ddlEmployeeMovementType.SelectedValue == "0")
            {
                HelpClass.showMsg(btnSubmit, btnSubmit.GetType(), "All fields represented with * are mandatory");
                return;
            }
            objSupplyRegisterBO = new SupplyRegisterBO();
            if (ddlEmployeeMovementType.SelectedIndex == 1)
            {
                try
                {
                    objSupplyRegisterBO.EmployeeID = ddlEmpID.Text.Trim();
                    objSupplyRegisterBO.Purpose = txtPurpose.Text.Trim();
                    objSupplyRegisterBO.ReturnDateTime = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                    objSupplyRegisterBO.Remarks = txtRemarks.Text.Trim();
                    objSupplyRegisterBO.CreatedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                    objSupplyRegisterBO.CreatedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss"); 
                    int c = objEmployeeMovementBAL.StaffMovement_Insert(objSupplyRegisterBO);
                    if (c > 0)
                    {
                        ddlEmployeeMovementType.SelectedIndex = 0;
                        EmployeeMovement_Reset();
                        HelpClass.showMsg(btnSubmit, btnSubmit.GetType(), "StaffMovement Details Submitted Successfully");
                    }
                    else
                    {
                        HelpClass.showMsg(btnSubmit, btnSubmit.GetType(), "StaffMovement Details Submission Failed");
                    }
                }
                catch (SqlException sqe)
                {
                    string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                    HelpClass.showMsg(this, this.GetType(), "VMSStaffR2:" + strmsgF);
                }
                catch (Exception ex)
                {
                    string strline = HelpClass.LineNo(ex);
                    string strMsg = HelpClass.SQLEscapeString(ex.Message);
                    HelpClass.showMsg(this, this.GetType(), "VMSStaffR2:" + strline + " " + strMsg);
                }
            }
            else if (ddlEmployeeMovementType.SelectedIndex == 2)
            {
                try
                {
                    objSupplyRegisterBO.EmployeeID = ddlEmpID.Text.Trim();
                    objSupplyRegisterBO.Purpose = txtPurpose.Text.Trim();
                    objSupplyRegisterBO.InDateTime = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                    objSupplyRegisterBO.Remarks = txtRemarks.Text.Trim();
                    objSupplyRegisterBO.CreatedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                    objSupplyRegisterBO.CreatedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                    objSupplyRegisterBO.ApprovedBy = 0;
                    objSupplyRegisterBO.ApprovedOn= DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                    int c = objEmployeeMovementBAL.HolidayRegister_Insert(objSupplyRegisterBO);
                    if (c > 0)
                    {
                        ddlEmployeeMovementType.SelectedIndex = 0;
                        EmployeeMovement_Reset();
                        HelpClass.showMsg(btnSubmit, btnSubmit.GetType(), "HolidayRegister Details Submitted Successfully");
                    }
                    else
                    {
                        HelpClass.showMsg(btnSubmit, btnSubmit.GetType(), "HolidayRegister Details Submission Failed");
                    }
                }
                catch (SqlException sqe)
                {
                    string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                    HelpClass.showMsg(this, this.GetType(), "VMSStaffR3:" + strmsgF);
                }
                catch (Exception ex)
                {
                    string strline = HelpClass.LineNo(ex);
                    string strMsg = HelpClass.SQLEscapeString(ex.Message);
                    HelpClass.showMsg(this, this.GetType(), "VMSStaffR3:" + strline + " " + strMsg);
                }
            }
        }

        private void EmployeeMovement_Reset()
        {
            txtPurpose.Text = txtRemarks.Text = string.Empty;
            ddlDepartment.SelectedIndex = 0;
            ddlEmpID.Items.Clear();
            TimeDate();
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            ddlEmployeeMovementType.SelectedIndex = 0;
            EmployeeMovement_Reset();
        }

        private void Employee()
        {
            try
            {
                if (ddlDepartment.SelectedIndex == 0)
                {
                    ddlEmpID.Items.Clear();
                }
                else
                {
                    ddlEmpID.Enabled = false;
                    ddlEmpID.Items.Clear();
                    ddlEmpID.Items.Insert(0, new ListItem("-- Select --", "0"));
                    VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                    int DeptID = Convert.ToInt32(ddlDepartment.SelectedValue);
                    if (DeptID >= 0)
                    {
                        dt = objVisitorRegisterBAL.GetEmpData(DeptID);
                        ddlEmpID.DataSource = dt;
                        ddlEmpID.DataTextField = "Name";
                        ddlEmpID.DataValueField = "EmpID";
                        ddlEmpID.DataBind();
                        ddlEmpID.Items.Insert(0, new ListItem("--Select--", "0"));
                        ddlEmpID.Enabled = true;
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaffR4:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaffR4:" + strline + " " + strMsg);
            }
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            Employee();
        }

        private void BindSupplyType()
        {
            try
            {
                DataTable dtMaterial = new DataTable();
                dtMaterial = objEmployeeMovementBAL.BindEmpMovementType();
                ddlEmployeeMovementType.DataTextField = "EmpMovementType";
                ddlEmployeeMovementType.DataValueField = "EmpMovementID";
                ddlEmployeeMovementType.DataSource = dtMaterial;
                ddlEmployeeMovementType.DataBind();
                ddlEmployeeMovementType.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaffR5:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaffR5:" + strline + " " + strMsg);
            }
        }
    }
}