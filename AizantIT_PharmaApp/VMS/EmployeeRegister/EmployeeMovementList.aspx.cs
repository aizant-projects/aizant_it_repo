﻿using System;

using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

using AizantIT_PharmaApp.Common;
using VMS_BAL;
using VMS_BO;


namespace AizantIT_PharmaApp.VMS.EmployeeRegister
{
    public partial class EmployeeMovementList : System.Web.UI.Page
    {
        DataTable dt;
        EmployeeMovementBAL objEmployeeMovementBAL = new EmployeeMovementBAL();
        SupplyRegisterBO objSupplyRegisterBO;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            ViewState["EmpID"] = "0";
                            if (HelpClass.IsUserAuthenticated())
                            {
                                ViewState["EmpID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            }
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaff7:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaff7:" + strline + " " + strMsg);
            }
        }

        private void InitializeThePage()
        {
            try
            {
                LoadDepartment();
                BindSupplyType();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSEMPL:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSEMPL:" + strline + " " + strMsg);
            }
        }

        private void LoadDepartment()
        {
            try
            {
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                dt = new DataTable();
                dt = objVisitorRegisterBAL.LoadDepartment();
                ddlDepartment.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlDepartment.DataValueField = dt.Columns["DeptID"].ToString();
                ddlDepartment.DataSource = dt;
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("All", "0"));

                ddlStaffDepartment.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlStaffDepartment.DataValueField = dt.Columns["DeptID"].ToString();
                ddlStaffDepartment.DataSource = dt;
                ddlStaffDepartment.DataBind();
                ddlStaffDepartment.Items.Insert(0, new ListItem("--Select--", "0"));

                ddlHDepartment.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlHDepartment.DataValueField = dt.Columns["DeptID"].ToString();
                ddlHDepartment.DataSource = dt;
                ddlHDepartment.DataBind();
                ddlHDepartment.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaffList1:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaffList1:" + strline + " " + strMsg);
            }
        }


        protected void btnStaffFind_Click(object sender, EventArgs e)
        {
            objSupplyRegisterBO = new SupplyRegisterBO();
            if (ddlMovementType.SelectedIndex == 1)
            {
                try
                {
                    objSupplyRegisterBO.Department = ddlDepartment.SelectedValue.Trim();
                    objSupplyRegisterBO.EmployeeID = ddlEmpID.SelectedValue.Trim();

                    if (!string.IsNullOrEmpty(txtDateFrom.Text.Trim()))
                    {
                        objSupplyRegisterBO.ReturnDateTime = Convert.ToDateTime(txtDateFrom.Text.Trim()).ToString("yyyyMMdd");
                    }
                    else
                    {
                        objSupplyRegisterBO.ReturnDateTime = txtDateFrom.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(txtDateTo.Text.Trim()))
                    {
                        objSupplyRegisterBO.InDateTime = Convert.ToDateTime(txtDateTo.Text.Trim()).ToString("yyyyMMdd");
                    }
                    else
                    {
                        objSupplyRegisterBO.InDateTime = txtDateTo.Text.Trim();
                    }
                    dt = new DataTable();
                    dt = objEmployeeMovementBAL.SearchFilterStaff(objSupplyRegisterBO);
                    StaffMovementGridView.DataSource = dt;
                    StaffMovementGridView.DataBind();
                    ViewState["dtSearch"] = dt;
                    StaffMovementGridView.Visible = true;
                    HolidayRegisterGridView.Visible = false;
                }
                catch (SqlException sqe)
                {
                    string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                    HelpClass.showMsg(this, this.GetType(), "VMSStaffList2:" + strmsgF);
                }
                catch (Exception ex)
                {
                    string strline = HelpClass.LineNo(ex);
                    string strMsg = HelpClass.SQLEscapeString(ex.Message);
                    HelpClass.showMsg(this, this.GetType(), "VMSStaffList2:" + strline + " " + strMsg);
                }
            }
            else if (ddlMovementType.SelectedIndex == 2)
            {
                try
                {
                    objSupplyRegisterBO.Department = ddlDepartment.SelectedValue.Trim();
                    objSupplyRegisterBO.EmployeeID = ddlEmpID.SelectedValue.Trim();

                    if (!string.IsNullOrEmpty(txtDateFrom.Text.Trim()))
                    {
                        objSupplyRegisterBO.InDateTime = Convert.ToDateTime(txtDateFrom.Text.Trim()).ToString("yyyyMMdd");
                    }
                    else
                    {
                        objSupplyRegisterBO.InDateTime = txtDateFrom.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(txtDateTo.Text.Trim()))
                    {
                        objSupplyRegisterBO.ReturnDateTime = Convert.ToDateTime(txtDateTo.Text.Trim()).ToString("yyyyMMdd");
                    }
                    else
                    {
                        objSupplyRegisterBO.ReturnDateTime = txtDateTo.Text.Trim();
                    }
                    dt = new DataTable();
                    dt = objEmployeeMovementBAL.SearchFilterHoliday(objSupplyRegisterBO);
                    HolidayRegisterGridView.DataSource = dt;
                    HolidayRegisterGridView.DataBind();
                    ViewState["dtHoliday"] = dt;
                    HolidayRegisterGridView.Visible = true;
                    StaffMovementGridView.Visible = false;
                }
                catch (SqlException sqe)
                {
                    string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                    HelpClass.showMsg(this, this.GetType(), "VMSHolidayL1:" + strmsgF);
                }
                catch (Exception ex)
                {
                    string strline = HelpClass.LineNo(ex);
                    string strMsg = HelpClass.SQLEscapeString(ex.Message);
                    HelpClass.showMsg(this, this.GetType(), "VMSHolidayL1:" + strline + " " + strMsg);
                }
            }
        }

        protected void btnstaffReset_Click(object sender, EventArgs e)
        {
            ddlDepartment.SelectedIndex = 0;
            ddlMovementType.SelectedIndex = 0;
            txtDateFrom.Text = txtDateTo.Text = string.Empty;
            ddlEmpID.Items.Clear();
            StaffMovementGridView.Visible = false;
            HolidayRegisterGridView.Visible = false;
        }

        private void StaffBindData()
        {
            try
            {
                dt = new DataTable();
                dt = objEmployeeMovementBAL.BindDataStaffMovementRegister();
                StaffMovementGridView.DataSource = dt;
                StaffMovementGridView.DataBind();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaffList4:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaffList4:" + strline + " " + strMsg);
            }
        }

        private void HolidayBindData()
        {
            try
            {
                dt = new DataTable();
                dt = objEmployeeMovementBAL.BindDataHolidayRegister();
                HolidayRegisterGridView.DataSource = dt;
                HolidayRegisterGridView.DataBind();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSHolidayL2:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSHolidayL2:" + strline + " " + strMsg);
            }
        }

        protected void btnStaffUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                objSupplyRegisterBO = new SupplyRegisterBO();
                objSupplyRegisterBO.SNo = Convert.ToInt32(txtStaffSno.Text.Trim());
                objSupplyRegisterBO.EmployeeID = ddlStaffEmployee.SelectedValue.Trim();
                objSupplyRegisterBO.Purpose = txtStaffPurpose.Text.Trim();               
                objSupplyRegisterBO.Remarks = txtRemarks.Text.Trim();
                objSupplyRegisterBO.LastChangedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                objSupplyRegisterBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss"); 
                objSupplyRegisterBO.LastChangedComments = txtStaffComments.Text.Trim();
                int c = objEmployeeMovementBAL.Update_StaffRegisterEdit(objSupplyRegisterBO);
                if (c > 0)
                {
                    HelpClass.showMsg(btnStaffUpdate, btnStaffUpdate.GetType(), "StaffMovement Details Updated Successfully");
                }
                else
                {
                    HelpClass.showMsg(btnStaffUpdate, btnStaffUpdate.GetType(), "StaffMovement Supply Details Updation Failed");
                }
                StaffBindData();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaffList5:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaffList5:" + strline + " " + strMsg);
            }
        }

        protected void btnStaffCancel_Click(object sender, EventArgs e)
        {
            ModalPopupStaff.Hide();
        }

        protected void LinkStaffMovment_Click(object sender, EventArgs e)
        {
            try
            {
                objSupplyRegisterBO = new SupplyRegisterBO();
                LinkButton lnk = sender as LinkButton;
                GridViewRow gr = (GridViewRow)lnk.NamingContainer;
                string tempID = StaffMovementGridView.DataKeys[gr.RowIndex].Value.ToString();
                objSupplyRegisterBO.SNo = Convert.ToInt32(tempID);
                ViewState["tempId1"] = tempID;
                dt = new DataTable();
                dt = objEmployeeMovementBAL.StaffRegisterlinkbuttonFilldata(objSupplyRegisterBO);
                txtStaffSno.Text = dt.Rows[0]["SNo"].ToString();
                txtStaffPurpose.Text = dt.Rows[0]["Purpose"].ToString();
                txtOutDateTime.Text= dt.Rows[0]["OutDateTime"].ToString();
                txtRemarks.Text= dt.Rows[0]["Remarks"].ToString();
                txtStaffComments.Text = dt.Rows[0]["LastChangedByComments"].ToString();
                ddlStaffDepartment.SelectedValue = dt.Rows[0]["DeptID"].ToString();
                StaffEmployee();
                ddlStaffEmployee.SelectedValue = dt.Rows[0]["EmpID"].ToString();
                ModalPopupStaff.Show();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaffList6:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaffList6:" + strline + " " + strMsg);
            }
        }

        protected void LinkHolidayRegister_Click(object sender, EventArgs e)
        {
            try
            {
                objSupplyRegisterBO = new SupplyRegisterBO();
                LinkButton lnk = sender as LinkButton;
                GridViewRow gr = (GridViewRow)lnk.NamingContainer;
                string tempID = HolidayRegisterGridView.DataKeys[gr.RowIndex].Value.ToString();
                objSupplyRegisterBO.SNo = Convert.ToInt32(tempID);
                ViewState["tempId1"] = tempID;
                dt = new DataTable();
                dt = objEmployeeMovementBAL.HolidayRegisterlinkbuttonFilldata(objSupplyRegisterBO);
                txtHSNo.Text = dt.Rows[0]["SNo"].ToString();
                txtHRemarks.Text = dt.Rows[0]["Remarks"].ToString();
                txtHIndatetime.Text= dt.Rows[0]["InDateTime"].ToString();
                txtHPurpose.Text = dt.Rows[0]["Purpose"].ToString();
                txtHComments.Text = dt.Rows[0]["LastChangedByComments"].ToString();
                ddlHDepartment.SelectedValue = dt.Rows[0]["DeptID"].ToString();
                HolidayEmployee();
                ddlHEmp.SelectedValue = dt.Rows[0]["EmpID"].ToString();
                ModalPopupHoliday.Show();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSHolidayL3:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSHolidayL3:" + strline + " " + strMsg);
            }
        }

        protected void btnHolidayUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                objSupplyRegisterBO = new SupplyRegisterBO();
                objSupplyRegisterBO.SNo = Convert.ToInt32(txtHSNo.Text.Trim());
                objSupplyRegisterBO.EmployeeID = ddlHEmp.SelectedValue.Trim();
                objSupplyRegisterBO.Remarks = txtHRemarks.Text.Trim();                
                objSupplyRegisterBO.Purpose = txtHPurpose.Text.Trim();
                objSupplyRegisterBO.LastChangedBy = Convert.ToInt32(ViewState["EmpID"].ToString()); 
                objSupplyRegisterBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                objSupplyRegisterBO.ApprovedBy = 0;  //Under Review
                objSupplyRegisterBO.ApprovedOn= DateTime.Now.ToString("yyyyMMdd HH:mm:ss");  //Under Review
                objSupplyRegisterBO.LastChangedComments = txtHComments.Text.Trim();
                int c = objEmployeeMovementBAL.Update_HolidayRegisterEdit(objSupplyRegisterBO);
                if (c > 0)
                {
                    HelpClass.showMsg(btnHolidayUpdate, btnHolidayUpdate.GetType(), "Holiday Register Details Updated Successfully");
                }
                else
                {
                    HelpClass.showMsg(btnHolidayUpdate, btnHolidayUpdate.GetType(), "Holiday Register Supply Details Updation Failed");
                }
                HolidayBindData();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSHolidayL4:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSHolidayL4:" + strline + " " + strMsg);
            }
        }

        protected void btnHolidayCancel_Click(object sender, EventArgs e)
        {
            ModalPopupHoliday.Hide();
        }

        protected void lnkCheckOutStaff_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton bt = (LinkButton)sender;
                GridViewRow gr = (GridViewRow)bt.NamingContainer;
                Label lbl_id = (Label)gr.FindControl("lblGVStaffSno");
                LinkButton lnk = (LinkButton)gr.FindControl("lnkCheckOutStaff");
                Label lbl_Name = (Label)gr.FindControl("lblGVStaffEmployeeName");
                objSupplyRegisterBO = new SupplyRegisterBO();
                objSupplyRegisterBO.SNo = Convert.ToInt32(lbl_id.Text);
                objSupplyRegisterBO.InDateTime = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                int c = objEmployeeMovementBAL.Checkout(objSupplyRegisterBO);
                if (c > 0)
                {
                    HelpClass.showMsg(this, this.GetType(), "  " + lbl_Name.Text + "   Checked In Successfully");
                    StaffBindData();
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaffList7:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaffList7:" + strline + " " + strMsg);
            }
        }

        protected void StaffMovementGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIntime = (Label)(e.Row.FindControl("lblGVInDateTime"));
                    LinkButton lbtnCheckin = (LinkButton)(e.Row.FindControl("lnkCheckOutStaff"));
                    if (lblIntime != null)
                    {
                        if (lblIntime.Text.Length > 6)
                            lblIntime.Visible = true;
                        else
                            lbtnCheckin.Visible = true;
                    }
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblOuttime = (Label)(e.Row.FindControl("lblGVInDateTime"));
                    LinkButton lnkbtnEdit = (LinkButton)(e.Row.FindControl("LinkStaffMovment"));
                    if (lblOuttime != null)
                    {
                        if (lblOuttime.Text.Length > 5)
                        {
                            lnkbtnEdit.Visible = false;
                        }
                        else
                        {
                            lnkbtnEdit.Visible = true;
                        }
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaffList8:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaffList8:" + strline + " " + strMsg);
            }
           
        }

        protected void StaffMovementGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            StaffMovementGridView.PageIndex = e.NewPageIndex;
            if (ViewState["dtSearch"]!=null)
            {
                StaffMovementGridView.DataSource =(DataTable)ViewState["dtSearch"];
                StaffMovementGridView.DataBind(); 
            }
        }

        protected void lnkCheckOutHoliday_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton bt = (LinkButton)sender;
                GridViewRow gr = (GridViewRow)bt.NamingContainer;
                Label lbl_id = (Label)gr.FindControl("lblGVHolidaySno");
                Label lbl_Name = (Label)gr.FindControl("lblGVHolidayEmployeeName");
                LinkButton lnk = (LinkButton)gr.FindControl("lnkCheckOutHoliday");
                objSupplyRegisterBO = new SupplyRegisterBO();
                objSupplyRegisterBO.SNo = Convert.ToInt32(lbl_id.Text);
                objSupplyRegisterBO.ReturnDateTime = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                objSupplyRegisterBO.EmployeeName = lbl_Name.Text;
                int c = objEmployeeMovementBAL.CheckoutHoliday(objSupplyRegisterBO);
                if (c > 0)
                {
                    HelpClass.showMsg(this, this.GetType(), " " + lbl_Name.Text + "   Checked Out Successfully");
                    HolidayBindData();
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSHolidayL5:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSHolidayL5:" + strline + " " + strMsg);
            }
        }

        protected void HolidayRegisterGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIntime = (Label)(e.Row.FindControl("lblGVReturnDateTime"));
                    LinkButton lbtnCheckin = (LinkButton)(e.Row.FindControl("lnkCheckOutHoliday"));
                    if (lblIntime != null)
                    {
                        if (lblIntime.Text.Length > 6)
                            lblIntime.Visible = true;
                        else
                            lbtnCheckin.Visible = true;
                    }
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblOuttime = (Label)(e.Row.FindControl("lblGVReturnDateTime"));
                    LinkButton lnkbtnEdit = (LinkButton)(e.Row.FindControl("LinkHolidayRegister"));
                    if (lblOuttime != null)
                    {
                        if (lblOuttime.Text.Length > 5)
                        {
                            lnkbtnEdit.Visible = false;
                        }
                        else
                        {
                            lnkbtnEdit.Visible = true;
                        }
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSHolidayL6:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSHolidayL6:" + strline + " " + strMsg);
            }
        }

        protected void ddlMovementType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlMovementType.SelectedIndex == 1)
            {
                HolidayRegisterGridView.Visible = false;
            }
            else
            {
                StaffMovementGridView.Visible = false;
            }
        }

        protected void HolidayRegisterGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            HolidayRegisterGridView.PageIndex = e.NewPageIndex;
            if (ViewState["dtHoliday"] != null)
            {
                HolidayRegisterGridView.DataSource = (DataTable)ViewState["dtHoliday"];
                HolidayRegisterGridView.DataBind();
            }
        }

        private void Employee()
        {
            try
            {
                if (ddlDepartment.SelectedIndex == 0)
                {
                    ddlEmpID.Items.Clear();
                }
                else
                {
                    ddlEmpID.Enabled = false;
                    ddlEmpID.Items.Clear();
                    ddlEmpID.Items.Insert(0, new ListItem("All", "0"));
                    VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                    int DeptID = Convert.ToInt32(ddlDepartment.SelectedValue);
                    if (DeptID >= 0)
                    {
                        DataTable dtEmp = objVisitorRegisterBAL.GetEmpData(DeptID);
                        ddlEmpID.DataSource = dtEmp;
                        ddlEmpID.DataTextField = "Name";
                        ddlEmpID.DataValueField = "EmpID";
                        ddlEmpID.DataBind();
                        ddlEmpID.Items.Insert(0, new ListItem("All", "0"));
                        ddlEmpID.Enabled = true;
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSEM1:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSEM1:" + strline + " " + strMsg);
            }
        }
        private void StaffEmployee()
        {
            try
            {
                if (ddlStaffDepartment.SelectedIndex == 0)
                {
                    ddlStaffEmployee.Items.Clear();
                }
                else
                {
                    ddlStaffEmployee.Enabled = false;
                    ddlStaffEmployee.Items.Clear();
                    ddlStaffEmployee.Items.Insert(0, new ListItem("-- Select --", "0"));
                    VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                    int DeptID = Convert.ToInt32(ddlStaffDepartment.SelectedValue);
                    if (DeptID >= 0)
                    {
                        DataTable dtStaff = objVisitorRegisterBAL.GetEmpData(DeptID);
                        ddlStaffEmployee.DataSource = dtStaff;
                        ddlStaffEmployee.DataTextField = "Name";
                        ddlStaffEmployee.DataValueField = "EmpID";
                        ddlStaffEmployee.DataBind();
                        ddlStaffEmployee.Items.Insert(0, new ListItem("--Select--", "0"));
                        ddlStaffEmployee.Enabled = true;
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaffList9:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaffList9:" + strline + " " + strMsg);
            }
        }

        private void HolidayEmployee()
        {
            try
            {
                if (ddlHDepartment.SelectedIndex == 0)
                {
                    ddlHEmp.Items.Clear();
                }
                else
                {
                    ddlHEmp.Enabled = false;
                    ddlHEmp.Items.Clear();
                    ddlHEmp.Items.Insert(0, new ListItem("-- Select --", "0"));
                    VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                    int DeptID = Convert.ToInt32(ddlHDepartment.SelectedValue);
                    if (DeptID >= 0)
                    {
                        DataTable dtHoliday = objVisitorRegisterBAL.GetEmpData(DeptID);
                        ddlHEmp.DataSource = dtHoliday;
                        ddlHEmp.DataTextField = "Name";
                        ddlHEmp.DataValueField = "EmpID";
                        ddlHEmp.DataBind();
                        ddlHEmp.Items.Insert(0, new ListItem("--Select--", "0"));
                        ddlHEmp.Enabled = true;
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSHolidayL7:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSHolidayL7:" + strline + " " + strMsg);
            }
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            Employee();
        }

        protected void ddlStaffDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            StaffEmployee();
        }

        protected void ddlHDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            HolidayEmployee();
        }

        private void BindSupplyType()
        {
            try
            {
                DataTable dtMaterial = new DataTable();
                dtMaterial = objEmployeeMovementBAL.BindEmpMovementType();
                ddlMovementType.DataTextField = "EmpMovementType";
                ddlMovementType.DataValueField = "EmpMovementID";
                ddlMovementType.DataSource = dtMaterial;
                ddlMovementType.DataBind();
                ddlMovementType.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaffR5:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaffR5:" + strline + " " + strMsg);
            }
        }
    }
}