﻿using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;
using VMS_BAL;
using VMS_BO;

namespace AizantIT_PharmaApp.VMS.Admin
{
    public partial class SelectVMSRole : System.Web.UI.Page
    {
        VMSRolePrivilegesBAL objVMSRolePrivilegesBAL = new VMSRolePrivilegesBAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                RolePrivilegesBO objRolePrivilegesBO = new RolePrivilegesBO();
                objRolePrivilegesBO.RoleID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                DataTable dtRole = objVMSRolePrivilegesBAL.GetEmpRoleID(objRolePrivilegesBO);
                if (dtRole.Rows.Count > 1)
                {
                    ddlVMSAccessableRoles.DataTextField = dtRole.Columns["RoleName"].ToString();
                    ddlVMSAccessableRoles.DataValueField = dtRole.Columns["RoleID"].ToString();
                    ddlVMSAccessableRoles.DataSource = dtRole;
                    ddlVMSAccessableRoles.DataBind();
                    ddlVMSAccessableRoles.Items.Insert(0, new ListItem("--Select Role--", "0"));
                }
                else
                {
                    Session["sesGMSRoleID"] = dtRole.Rows[0]["RoleID"].ToString();
                    // For VMS                    
                    if (dtRole.Rows[0]["RoleID"].ToString() == "22")//Security RoleID
                    {
                        Response.Redirect("~/VMS/Dashboard/SecurityLandingPage.aspx", false);
                    }
                    else
                    {
                        Response.Redirect("~/VMS/VMS_HomePage.aspx");
                    }
                }

            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            StringBuilder s = new StringBuilder();
            if (ddlVMSAccessableRoles.SelectedIndex == 0)
            {
                s.Append("Please Select Role");
                HelpClass.custAlertMsg(this, this.GetType(), "" + s.ToString() + "", "error");
                return;
            }
           else        
            {
                Session["sesGMSRoleID"] = ddlVMSAccessableRoles.SelectedValue;
                Response.Redirect("~/VMS/VMS_HomePage.aspx");
            }
        }
    }
}