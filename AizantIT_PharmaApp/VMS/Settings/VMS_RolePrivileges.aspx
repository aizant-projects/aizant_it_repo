﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/NestedVMS_Master.master" AutoEventWireup="true" CodeBehind="VMS_RolePrivileges.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.Settings.VMS_RolePrivileges" %>

<%@ Register Assembly="DevExpress.Web.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:Content ID="Content2" ContentPlaceHolderID="VMSBody" runat="server">
   
    <script>
    
        function FacilitySelectedIndexChanged(s, e) {
            if (e.index == 0 || e.index == 1 || e.index == 2)
                s.UnselectIndices([3]);
            if (e.index == 3)
                s.UnselectIndices([0, 1, 2]);
        }
        function VehicleSelectedIndexChanged(s, e) {
            if (e.index == 0 || e.index == 1 || e.index == 2)
                s.UnselectIndices([3]);
            if (e.index == 3)
                s.UnselectIndices([0, 1, 2]);
        }
        function VisitorSelectedIndexChanged(s, e) {
            if (e.index == 0 || e.index == 2 || e.index == 3)
                s.UnselectIndices([6]);
            if (e.index == 6)
                s.UnselectIndices([0,2,3]);
            if (e.index == 4 || e.index == 5)
                s.UnselectIndices([7]);
            if (e.index == 7)
                s.UnselectIndices([4, 5]);
        }
        function MaterialSelectedIndexChanged(s, e) {
            if (e.index == 0 || e.index == 1 || e.index == 2 || e.index == 3 || e.index == 4 || e.index == 5 || e.index == 11)
                s.UnselectIndices([9]);
            if (e.index == 9)
                s.UnselectIndices([0, 1, 2, 3,4,5,11]);
            if (e.index == 6 || e.index == 7)
                s.UnselectIndices([10]);
            if (e.index == 10)
                s.UnselectIndices([6, 7]);
        }
        </script>
    <div class=" col-lg-12 col-md-12 col-sm-12 col-12 top padding-none grid_panel_full float-left">
        <div class="col-lg-12 col-md-12 col-sm-12 col-12  grid_header float-left">
         <div class="col-lg-6 padding-none">Privileges </div>
                    </div>
                    <div class=" col-lg-12 col-md-12 col-sm-12 col-12 padding-none top float-left">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-12 float-left">
                                    <asp:UpdatePanel ID="upBtnPrivilege" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-12 padding-none float-left">
                                                <label class="float-left">Role</label>
                                                <div class="col-lg-4 col-md-5 col-sm-5 col-6 float-left">
                                                    <asp:DropDownList ID="ddlVmsRoles" runat="server" AutoPostBack="true" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker drop_down padding-none regulatory_dropdown_style" data-size="5" data-live-search="true" OnSelectedIndexChanged="ddlVmsRoles_SelectedIndexChanged"></asp:DropDownList>
                                                </div>
                                                <div class=" col-lg-4 col-md-5 col-sm-5 col-6 float-left">
                                                    <asp:Button ID="Button1" Text="Submit" class=" btn-signup_popup" runat="server" OnClick="btnSubmit_Click" />
                                                    <asp:Button ID="btnReset" Text="Reset" class=" btn-cancel_popup" runat="server" CausesValidation="false" OnClick="btnReset_Click" />
                                                    <asp:Button ID="btnViewDetails" Text="View Details" class=" btn-signup_popup" runat="server" OnClick="btnViewDetails_Click" Visible="false" />
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-12 float-left" style="height: 515px; overflow: auto;">
                                <div class="col-12 col-lg-12 col-md-12 col-sm-12 grid_assgin_nav padding-none bottom grid_assgin_nav float-left">
                                    <div class="col-12 col-lg-12 col-md-12 col-sm-12 grid_header bottom">Visitor </div>
                                    <div data-toggle="buttons" class="privilages_panel">
                                        <dx:ASPxCheckBoxList ID="cblVisitorPrivileges1" runat="server" RepeatColumns="8" CssClass="previleges_roleslist"
                                            RepeatDirection="Vertical" RepeatLayout="Table" CheckedImage-Url="/Images/GridActionImages/checked.png"  CheckedImage-Height="18px" CheckedImage-Width="18px" UncheckedImage-Height="18px" UncheckedImage-Width="18px" UncheckedImage-Url="/Images/GridActionImages/uncheked.png" >
                                         <ClientSideEvents SelectedIndexChanged="VisitorSelectedIndexChanged" />
                                            </dx:ASPxCheckBoxList>
                                        <asp:CheckBoxList ID="cblVisitorPrivileges"  Visible="false" runat="server" AutoPostBack="true" Width="100%" RepeatDirection="Horizontal"></asp:CheckBoxList>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-12 col-md-12 col-sm-12 grid_assgin_nav padding-none bottom float-left">
                                    <div class="col-12 col-lg-12 col-md-12 col-sm-12 grid_header bottom">Vehicle </div>
                                    <div class="privilages_panel col-lg-12 col-md-12 col-sm-12 col-12 float-left padding-none" data-toggle="buttons">
                                        <dx:ASPxCheckBoxList ID="clbVechilePrivileges1" runat="server" RepeatColumns="8" CssClass="previleges_roleslist"
                                            RepeatDirection="Vertical" RepeatLayout="Table" CheckedImage-Url="/Images/GridActionImages/checked.png" CheckedImage-Height="18px" CheckedImage-Width="18px" UncheckedImage-Height="18px" UncheckedImage-Width="18px" UncheckedImage-Url="/Images/GridActionImages/uncheked.png">
                                        <ClientSideEvents SelectedIndexChanged="VehicleSelectedIndexChanged" />
                                            </dx:ASPxCheckBoxList>
                                        <asp:CheckBoxList ID="clbVechilePrivileges" Visible="false" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"></asp:CheckBoxList>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-12 col-md-12 col-sm-12 grid_assgin_nav padding-none bottom float-left">
                                    <div class="col-12 col-lg-12 col-md-12 col-sm-12 grid_header  bottom">Visitor Facilities </div>
                                    <div class="privilages_panel" data-toggle="buttons">
                                        <dx:ASPxCheckBoxList ID="clbVisitorFacilities1" runat="server" RepeatColumns="8" CssClass="previleges_roleslist"
                                            RepeatDirection="Vertical" RepeatLayout="Table" CheckedImage-Url="/Images/GridActionImages/checked.png" CheckedImage-Height="18px" CheckedImage-Width="18px" UncheckedImage-Height="18px" UncheckedImage-Width="18px" UncheckedImage-Url="/Images/GridActionImages/uncheked.png">
                                      <ClientSideEvents SelectedIndexChanged="FacilitySelectedIndexChanged" />  
                                            </dx:ASPxCheckBoxList>
                                        <asp:CheckBoxList ID="clbVisitorFacilities" Visible="false" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"></asp:CheckBoxList>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-12 col-md-12 col-sm-12 grid_assgin_nav padding-none bottom float-left">
                                    <div class="col-12 col-lg-12 col-md-12 col-sm-12 grid_header  bottom">Material  </div>
                                    <div class="privilages_panel" data-toggle="buttons">
                                        <asp:CheckBoxList ID="clbMAterialPrivileges" Visible="false" runat="server" AutoPostBack="true" Width="100%" RepeatDirection="Horizontal"></asp:CheckBoxList>
                                        <dx:ASPxCheckBoxList ID="clbMAterialPrivileges1" runat="server" RepeatColumns="8" CssClass="previleges_roleslist"
                                            RepeatDirection="Vertical" RepeatLayout="Table" CheckedImage-Url="/Images/GridActionImages/checked.png" CheckedImage-Height="18px" CheckedImage-Width="18px" UncheckedImage-Height="18px" UncheckedImage-Width="18px" UncheckedImage-Url="/Images/GridActionImages/uncheked.png">
                                        <ClientSideEvents SelectedIndexChanged="MaterialSelectedIndexChanged" /> 
                                            </dx:ASPxCheckBoxList>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-12 col-md-12 col-sm-12 grid_assgin_nav padding-none bottom float-left">
                                    <div class="col-12 col-lg-12 col-md-12 col-sm-12 grid_header bottom">Setting </div>
                                    <div class="privilages_panel" data-toggle="buttons">
                                        <asp:CheckBoxList ID="cblSettingsModule" Visible="false" runat="server" AutoPostBack="true"></asp:CheckBoxList>
                                        <dx:ASPxCheckBoxList ID="cblSettingsModule1" runat="server" RepeatColumns="8" CssClass="previleges_roleslist"
                                            RepeatDirection="Vertical" RepeatLayout="Table" CheckedImage-Url="/Images/GridActionImages/checked.png" CheckedImage-Height="18px" CheckedImage-Width="18px" UncheckedImage-Height="18px" UncheckedImage-Width="18px" UncheckedImage-Url="/Images/GridActionImages/uncheked.png">
                                        </dx:ASPxCheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="col-sm-12" style="text-align: end; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                    </div>

           
         
        </div>
   
</asp:Content>
