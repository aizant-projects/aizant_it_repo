﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Web.UI.WebControls;
using System.Data;
using VMS_BAL;
using VMS_BO;
using AizantIT_PharmaApp.Common;
using System.Data.SqlClient;
using DevExpress.Web;

namespace AizantIT_PharmaApp.VMS.Settings
{
    public partial class VMS_RolePrivileges : System.Web.UI.Page
    {
        DataTable dt;
        VMSRolePrivilegesBAL objVMSRolePrivilegesBAL = new VMSRolePrivilegesBAL();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            ViewState["EmpID"] = "0";
                            if (HelpClass.IsUserAuthenticated())
                            {
                                ViewState["EmpID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            }
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSRP1:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSRP1:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSRP1:" + strline + " " + strMsg,"error");
            }
        }

        private void InitializeThePage()
        {
            try
            {
                screenAuthentication();
                LoadRoles();
                LoadVisitorPrivileges();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSRP2:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSRP2:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSRP2:" + strline + " " + strMsg,"error");
            }
        }

        private void LoadRoles()
        {
            try
            {
                dt = new DataTable();
                dt = objVMSRolePrivilegesBAL.GetRoles();
                ddlVmsRoles.DataTextField = "RoleName";
                ddlVmsRoles.DataValueField = "RoleID";
                ddlVmsRoles.DataSource = dt;
                ddlVmsRoles.DataBind();
                ddlVmsRoles.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSRP3:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSRP3:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSRP3:" + strline + " " + strMsg,"error");
            }
        }

        private void LoadVisitorPrivileges()
        {
            try
            {
                dt = new DataTable();
                dt = objVMSRolePrivilegesBAL.GetPrivileIDAndNames();
                DataRow[] resultVisitor = dt.Select("modiD = 1");
                DataRow[] resultVechile = dt.Select("modiD = 2");
                DataRow[] resultMaterial = dt.Select("modiD = 3");
                DataRow[] resultSettings = dt.Select("modiD = 4");
                DataRow[] resultFacilities = dt.Select("modiD = 5");
                DataTable dtVisitor = resultVisitor.CopyToDataTable();
                DataTable dtVechile = resultVechile.CopyToDataTable();
                DataTable dtMaterial = resultMaterial.CopyToDataTable();
                DataTable dtsettings = resultSettings.CopyToDataTable();
                DataTable dtFacilites = resultFacilities.CopyToDataTable();

                cblVisitorPrivileges1.DataSource = dtVisitor;
                cblVisitorPrivileges1.TextField = "PrivilegeName";
                cblVisitorPrivileges1.ValueField = "PrivilegeID";
                cblVisitorPrivileges1.DataBind();

                clbVechilePrivileges1.DataSource = dtVechile;
                clbVechilePrivileges1.TextField = "PrivilegeName";
                clbVechilePrivileges1.ValueField = "PrivilegeID";
                clbVechilePrivileges1.DataBind();

                clbMAterialPrivileges1.DataSource = dtMaterial;
                clbMAterialPrivileges1.TextField = "PrivilegeName";
                clbMAterialPrivileges1.ValueField = "PrivilegeID";
                clbMAterialPrivileges1.DataBind();

                cblSettingsModule1.DataSource = dtsettings;
                cblSettingsModule1.TextField = "PrivilegeName";
                cblSettingsModule1.ValueField = "PrivilegeID";
                cblSettingsModule1.DataBind();

                clbVisitorFacilities1.DataSource = dtFacilites;
                clbVisitorFacilities1.TextField = "PrivilegeName";
                clbVisitorFacilities1.ValueField = "PrivilegeID";
                clbVisitorFacilities1.DataBind();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSRP4:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSRP4:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSRP4:" + strline + " " + strMsg,"error");
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlVmsRoles.SelectedIndex == 0)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Please select role","error");
                    return;
                }
                int RowCountZero = 0;
                List<RolePrivilegesBO> lstRolePrivilegesBO = new List<RolePrivilegesBO>();
                {
                    int count = 0;
                    foreach (ListEditItem li in cblVisitorPrivileges1.Items)
                    {
                        RolePrivilegesBO objRolePrivilegesBO = new RolePrivilegesBO();
                            if (li.Selected)
                            {
                                objRolePrivilegesBO.RoleID = Convert.ToInt32(ddlVmsRoles.SelectedValue);
                                objRolePrivilegesBO.PrivilegeID = Convert.ToInt32(li.Value);
                                objRolePrivilegesBO.PrivilegeName = li.Text;
                                lstRolePrivilegesBO.Add(objRolePrivilegesBO);
                                count += 1;
                            }
                    }
                    foreach (ListEditItem li1 in clbVechilePrivileges1.Items)
                    {
                        RolePrivilegesBO objRolePrivilegesBO = new RolePrivilegesBO();
                        if (li1.Selected)
                        {
                            objRolePrivilegesBO.RoleID = Convert.ToInt32(ddlVmsRoles.SelectedValue);
                            objRolePrivilegesBO.PrivilegeID = Convert.ToInt32(li1.Value);
                            objRolePrivilegesBO.PrivilegeName = li1.Text;
                            lstRolePrivilegesBO.Add(objRolePrivilegesBO);
                            count += 1;
                        }
                    }
                    foreach (ListEditItem li2 in clbMAterialPrivileges1.Items)
                    {
                        RolePrivilegesBO objRolePrivilegesBO = new RolePrivilegesBO();
                        if (li2.Selected)
                        {
                            objRolePrivilegesBO.RoleID = Convert.ToInt32(ddlVmsRoles.SelectedValue);
                            objRolePrivilegesBO.PrivilegeID = Convert.ToInt32(li2.Value);
                            objRolePrivilegesBO.PrivilegeName = li2.Text;
                            lstRolePrivilegesBO.Add(objRolePrivilegesBO);
                            count += 1;
                        }
                    }
                    foreach (ListEditItem li3 in cblSettingsModule1.Items)
                    {
                        RolePrivilegesBO objRolePrivilegesBO = new RolePrivilegesBO();
                        if (li3.Selected)
                        {
                            objRolePrivilegesBO.RoleID = Convert.ToInt32(ddlVmsRoles.SelectedValue);
                            objRolePrivilegesBO.PrivilegeID = Convert.ToInt32(li3.Value);
                            objRolePrivilegesBO.PrivilegeName = li3.Text;
                            lstRolePrivilegesBO.Add(objRolePrivilegesBO);
                            count += 1;
                        }
                    }
                    foreach (ListEditItem li4 in clbVisitorFacilities1.Items)
                    {
                        RolePrivilegesBO objRolePrivilegesBO = new RolePrivilegesBO();
                        if (li4.Selected)
                        {
                            objRolePrivilegesBO.RoleID = Convert.ToInt32(ddlVmsRoles.SelectedValue);
                            objRolePrivilegesBO.PrivilegeID = Convert.ToInt32(li4.Value);
                            objRolePrivilegesBO.PrivilegeName = li4.Text;
                            lstRolePrivilegesBO.Add(objRolePrivilegesBO);
                            count += 1;
                        }
                    }
                    if (count == 0)
                    {
                        RowCountZero += 1;
                    }
                    if (RowCountZero != 0)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Select at least one module", "error");
                    }
                    else
                    {
                        int c = objVMSRolePrivilegesBAL.InsertRolePrivileges(lstRolePrivilegesBO);
                        if (c > 0)
                        {
                            ResetRolesPrivileges();
                            HelpClass.custAlertMsg(this, this.GetType(), "Submited successfully", "success");
                        }
                        else
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Submission failed", "error");
                        }
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSRP5:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSRP5:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSRP5:" + strline + " " + strMsg, "error");
            }
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            ResetRolesPrivileges();
        }

        private void ResetRolesPrivileges()
        {
            ddlVmsRoles.SelectedIndex = 0;

            for (int i = 0; i < cblVisitorPrivileges1.Items.Count; i++)
            {
                cblVisitorPrivileges1.Items[i].Selected = false;
            }
            for (int i = 0; i < clbVechilePrivileges1.Items.Count; i++)
            {
                clbVechilePrivileges1.Items[i].Selected = false;
            }
            for (int i = 0; i < clbMAterialPrivileges1.Items.Count; i++)
            {
                clbMAterialPrivileges1.Items[i].Selected = false;
            }
            for (int i = 0; i < cblSettingsModule1.Items.Count; i++)
            {
                cblSettingsModule1.Items[i].Selected = false;
            }
            for (int i = 0; i < clbVisitorFacilities1.Items.Count; i++)
            {
                clbVisitorFacilities1.Items[i].Selected = false;
            }
            UpdatePanel1.Update();
            UpdatePanel2.Update();
        }

        protected void btnViewDetails_Click(object sender, EventArgs e)
        {
            try
            {
                RolePrivilegesBO objRolePrivilegesBO = new RolePrivilegesBO();
                objRolePrivilegesBO.RoleID = Convert.ToInt32(ddlVmsRoles.SelectedValue);
                dt = new DataTable();
                dt = objVMSRolePrivilegesBAL.GetRolePrivileges(objRolePrivilegesBO);
                if (dt.Rows.Count > 0)
                {
                    DataRow[] resultVisitor = dt.Select("modiD = 1");
                    DataRow[] resultVechile = dt.Select("modiD = 2");
                    DataRow[] resultMaterial = dt.Select("modiD = 3");
                    DataRow[] resultSettings = dt.Select("modiD = 4");
                    DataRow[] resultFacilites = dt.Select("modiD = 5");
                    if (resultVisitor.Length > 0)
                    {
                        DataTable dtVisitor = resultVisitor.CopyToDataTable();
                        foreach (ListEditItem li in cblVisitorPrivileges1.Items)
                        {
                            foreach (DataRow dr in dtVisitor.Rows)
                            {
                                if (dr["PrivilegeID"].ToString() == li.Value.ToString())
                                {
                                    li.Selected = true;
                                }
                            }
                        }
                    }
                    if (resultVechile.Length > 0)
                    {
                        DataTable dtVechile = resultVechile.CopyToDataTable();
                        foreach (ListEditItem li1 in clbVechilePrivileges1.Items)
                        {
                            foreach (DataRow dr in dtVechile.Rows)
                            {
                                if (dr["PrivilegeID"].ToString() == li1.Value.ToString())
                                {
                                    li1.Selected = true;
                                }
                            }
                        }
                    }
                    if (resultMaterial.Length > 0)
                    {
                        DataTable dtMaterial = resultMaterial.CopyToDataTable();
                        foreach (ListEditItem li2 in clbMAterialPrivileges1.Items)
                        {
                            foreach (DataRow dr in dtMaterial.Rows)
                            {
                                if (dr["PrivilegeID"].ToString() == li2.Value.ToString())
                                {
                                    li2.Selected = true;
                                }
                            }
                        }
                    }
                    if (resultSettings.Length > 0)
                    {
                        DataTable dtsettings = resultSettings.CopyToDataTable();
                        foreach (ListEditItem li3 in cblSettingsModule1.Items)
                        {
                            foreach (DataRow dr in dtsettings.Rows)
                            {
                                if (dr["PrivilegeID"].ToString() == li3.Value.ToString())
                                {
                                    li3.Selected = true;
                                }
                            }
                        }
                    }
                    if (resultFacilites.Length > 0)
                    {
                        DataTable dtFacilites = resultFacilites.CopyToDataTable();
                        foreach (ListEditItem li4 in clbVisitorFacilities1.Items)
                        {
                            foreach (DataRow dr in dtFacilites.Rows)
                            {
                                if (dr["PrivilegeID"].ToString() == li4.Value.ToString())
                                {
                                    li4.Selected = true;
                                }
                            }
                        }
                    }
                }
                UpdatePanel1.Update();
                UpdatePanel2.Update();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSRP6:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSRP6:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSRP6:" + strline + " " + strMsg,"error");
            }
        }

        protected void ddlVmsRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < cblVisitorPrivileges1.Items.Count; i++)
                {
                    cblVisitorPrivileges1.Items[i].Selected = false;
                }
                for (int i = 0; i < clbVechilePrivileges1.Items.Count; i++)
                {
                    clbVechilePrivileges1.Items[i].Selected = false;
                }
                for (int i = 0; i < clbMAterialPrivileges1.Items.Count; i++)
                {
                    clbMAterialPrivileges1.Items[i].Selected = false;
                }
                for (int i = 0; i < cblSettingsModule1.Items.Count; i++)
                {
                    cblSettingsModule1.Items[i].Selected = false;
                }
                for (int i = 0; i < clbVisitorFacilities1.Items.Count; i++)
                {
                    clbVisitorFacilities1.Items[i].Selected = false;
                }
                if (ddlVmsRoles.SelectedIndex > 0)
                {
                    btnViewDetails_Click(null, null);
                }
                UpdatePanel1.Update();
                UpdatePanel2.Update();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSRP7:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSRP7:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSRP7:" + strline + " " + strMsg,"error");
            }
        }

        void screenAuthentication()
        {
            DataTable dtAthuntication = GmsHelpClass.GetRolePrivileges();
            if (dtAthuntication.Rows.Count > 0)
            {
                //modid=3,PrivilegeID=18
                int settingsCount = dtAthuntication.AsEnumerable()
           .Count(row => row.Field<int>("PrivilegeID") == 18);
                if (settingsCount == 0)
                    Response.Redirect("~/UserLogin.aspx");
            }
        }

        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}