﻿using DevExpress.Web;
using System;


namespace AizantIT_PharmaApp.VMS.Settings
{
    public partial class testcheckbox : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void ASPxCheckBoxList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // indexM is getting the first item index no the current item selected
            int indexM =Convert.ToInt32(ASPxCheckBoxList1.SelectedItem.Value);//  want to get current item selected.
            foreach (ListEditItem li in ASPxCheckBoxList1.SelectedItems)
            {
                if (li.Value.ToString() == "0")
                {
                    ASPxCheckBoxList1.Items[2].Selected = false;
                    break;
                }
                if (li.Value.ToString() == "2")
                {
                    ASPxCheckBoxList1.Items[0].Selected = false;
                    ASPxCheckBoxList1.Items[1].Selected = false;
                    break;
                }
            }
        }
    }
}