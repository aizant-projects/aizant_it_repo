﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/NestedVMS_Master.master" AutoEventWireup="true" CodeBehind="VisitorFacilityRequestList.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.VisitorFacilities.VisitorFacilityRequestList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>


<asp:Content ID="Content2" ContentPlaceHolderID="VMSBody" runat="server">
     <style>
        .lbl {
            text-align: left;
            font-weight: lighter;
        }
    </style>
    <div style="margin-top: 10px;">
        <div class="row Panel_Frame" style="width: 1200px; margin-left: 10%">
            <div id="header">
                <div class="col-sm-12 Panel_Header">
                    <div class="col-sm-6" style="padding-left: 5px">
                        <span class="Panel_Title" id="span_ListTitle" runat="server">Visitor Facilities Request List</span>
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div>
            <div id="divSearch" runat="server"><%--Search Textboxes Div--%>
                <div class="Row">
                    <div class="col-sm-6">
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div1">
                                <label class="control-label col-sm-4 lbl" id="lblFirstName">Name of the Client/Visitor<span class="mandatoryStar">*</span></label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtSearchVisitorName" runat="server" CssClass="form-control" placeholder="Enter Visitor/Client Name" TextMode="MultiLine" Rows="3" onkeypress="return ValidateAlpha(event)" onpaste="return false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div4">
                                <label class="control-label col-sm-4 lbl" id="labelFromDate">From Date</label>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txtDateFrom" runat="server" CssClass="form-control" placeholder="Select From Date"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="form-horizontal" style="margin: 5px;">
                            <div class="form-group" runat="server" id="div6">
                                <label class="control-label col-sm-4 lbl" id="label3">To Date</label>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txtDateTo" runat="server" CssClass="form-control" placeholder="Select To Date"></asp:TextBox><br />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                    <asp:Button ID="btnFind" Text="Find" CssClass="button" runat="server" OnClick="btnFind_Click" />
                    <asp:Button ID="btnReset" Text="Reset" CssClass="button" runat="server" CausesValidation="false" OnClick="btnReset_Click" />
                </div>
            </div>
            <div class="clearfix">
            </div>
            <div runat="server" id="divgridviewVFR"><%--GridView Div--%>
                <asp:GridView ID="VisitorFacilityGridView" runat="server" DataKeyNames="FacilityRequestID" PagerStyle-CssClass="GridPager" CssClass="table table-bordered table-inverse"
                    Style="text-align: center" Font-Size="11px" Width="100%" HeaderStyle-Wrap="false" AutoGenerateColumns="False" HeaderStyle-Width="5%" EmptyDataText="Currently No Records Found"
                    CellPadding="3" HeaderStyle-HorizontalAlign="Center" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" AllowPaging="True" ForeColor="#66CCFF"
                    OnPageIndexChanging="VisitorFacilityGridView_PageIndexChanging" OnRowDataBound="VisitorFacilityGridView_RowDataBound">
                    <EmptyDataRowStyle ForeColor="#FF3300" HorizontalAlign="Center" />
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <HeaderStyle BackColor="#748CB2" Font-Bold="True" HorizontalAlign="Center" ForeColor="White" Height="30px" />
                    <PagerStyle ForeColor="White" HorizontalAlign="Center" BackColor="White" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#0c99f0" />
                    <SortedAscendingHeaderStyle BackColor="#0c99f0" />
                    <SortedDescendingCellStyle BackColor="#0c99f0" />
                    <SortedDescendingHeaderStyle BackColor="#0c99f0" />
                    <Columns>
                        <asp:TemplateField HeaderText="SNO" SortExpression="FacilityRequestID">
                            <ItemTemplate>
                                <asp:Label ID="lblGFacilityRequestID" runat="server" Text='<%# Bind("FacilityRequestID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ClientName" HeaderText="CLIENT / VISITOR NAME" SortExpression="ClientName" />
                        <asp:BoundField DataField="NoofVisitors" HeaderText="NO OF VISITORS" ItemStyle-Width="10%" SortExpression="NoofVisitors" />
                        <asp:BoundField DataField="DateofVisit" HeaderText="DATE OF VISIT" SortExpression="DateofVisit" />
                       
                        <asp:BoundField DataField="EmpName" HeaderText="INITIATED BY   " SortExpression="EmpName" />
                        <asp:BoundField DataField="DepartmentName" HeaderText="DEPARTMENT" SortExpression="DepartmentName" />
                        <asp:BoundField DataField="EstimatedTotalCostFoodandTravel" HeaderText="ESTIMATED COST" SortExpression="EstimatedTotalCostFoodandTravel" />
                        <asp:BoundField DataField="ActualTotalCostFoodandTravel" HeaderText="ACTUAL COST" SortExpression="ActualTotalCostFoodandTravel" />
                        <asp:BoundField DataField="StatusName" HeaderText="STATUS" SortExpression="StatusName" />
                        <asp:TemplateField HeaderText="EDIT">
                            <ItemTemplate>
                                <asp:LinkButton ID="LnkEditVisitorFacility" runat="server" CssClass="glyphicon glyphicon-edit GV_Edit_Icon" OnClick="LnkEditVisitorFacility_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupFacility" runat="server" TargetControlID="lbtPop" PopupControlID="panelVisitor" RepositionMode="RepositionOnWindowResizeAndScroll"
                    DropShadow="true" PopupDragHandleControlID="panelAddNewTitle" BackgroundCssClass="modalBackground">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="panelVisitor" runat="server" Style="display: none; background-color: ghostwhite;" ForeColor="Black" Width="1400" Height="700">
                    <asp:Panel ID="panelv" runat="server" Style="cursor: move; font-family: Tahoma; padding: 8px;" HorizontalAlign="Center" BorderColor="#000000" Font-Bold="true" BackColor="#748CB2" ForeColor="White" Height="35">
                        <b id="pttlModifyVisitor" runat="server">Visitor Facilities Request</b>
                    </asp:Panel>
                    <asp:LinkButton ID="lbtPop" runat="server"></asp:LinkButton>
                    <div class="Row">
                        <div class="col-sm-6">
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div5">
                                    <label class="control-label col-sm-4 lbl" id="lblEmpName12">Initiated By</label>
                                    <label class="control-label col-sm-4 lbl" id="lblEmpName" runat="server"></label>
                                </div>
                            </div>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div12">
                                    <label class="control-label col-sm-4 lbl" id="lblDepartment12">Department</label>
                                    <label class="control-label col-sm-4 lbl" id="lblDepartment" runat="server"></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div2">
                                    <label class="control-label col-sm-4 lbl" id="lblEmpID12">Employee ID</label>
                                    <label class="control-label col-sm-4 lbl" id="lblEmpID" runat="server"></label>
                                </div>
                            </div>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div3">
                                    <label class="control-label col-sm-4 lbl" id="lblDate12">Date of Request</label>
                                    <label class="control-label col-sm-4 lbl" id="lblDateofRequest" runat="server"></label>
                                </div>
                            </div>
                        </div>
                        <%-- Start Hr Div--%>
                        <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px"></div>
                        <%-- End Hr Div--%>
                        <div class="col-sm-4">
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div24">
                                    <label class="control-label col-sm-4 lbl" id="lblID">FacilityID</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtFRID" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div7">
                                    <label class="control-label col-sm-4 lbl" id="lblClientName">Name of the Visitor/Client<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtVisitorName" runat="server" CssClass="form-control" placeholder="Enter Visitor/Client Name" TextMode="MultiLine" Rows="3" onkeypress="return ValidateAlpha(event)" onpaste="return false"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div11">
                                    <label class="control-label col-sm-4 lbl" id="lblNoofVisitors">No of Visitors<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtNoofVisitors" runat="server" CssClass="form-control" placeholder="Enter No of Visitors" TabIndex="11" onkeypress="if(event.keyCode<48 || event.keyCode>57)event.returnValue=false;"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div14">
                                    <label class="control-label col-sm-4 lbl" id="lblDateofVisit">Date of Visit<span class="mandatoryStar">*</span></label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtDateofVisit" runat="server" CssClass="form-control" placeholder="Select Date"></asp:TextBox>

                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div8">
                                    <label class="control-label col-sm-4 lbl" id="lblTravel">Travel Arrangements Details If Any</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtTravelArrangements" runat="server" CssClass="form-control" placeholder="Enter Travel Arrangements" TextMode="MultiLine" Rows="6"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div19">
                                    <label class="control-label col-sm-5 lbl" id="lblTrvlECost">Estimated Cost (Rs.)</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtTrvlECost" runat="server" CssClass="form-control" Style="margin-left: 56%; margin-top: -8%;" onkeypress="return onlyDotsAndNumbers(this,event);"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div20">
                                    <label class="control-label col-sm-4 lbl" id="lblTrvlACost">Actual Cost (Rs.)</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtTrvlACost" runat="server" CssClass="form-control" onkeypress="return onlyDotsAndNumbers(this,event);"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div9">
                                    <label class="control-label col-sm-4 lbl" id="lblRemarks">Remarks</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" placeholder="Enter Remarks" TextMode="MultiLine" Rows="6"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div10">
                                    <label class="control-label col-sm-4 lbl" id="lblMorningRD">Morning Refreshment Details</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtMorningRef" runat="server" CssClass="form-control" Width="340px" placeholder="Enter Morning Refreshment Details" TextMode="MultiLine" Rows="8"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div13">
                                    <label class="control-label col-sm-4 lbl" id="lblAfterNoon">AfterNoon Lunch Details</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtAfterLD" runat="server" CssClass="form-control" Width="340px" placeholder="Enter AfterNoon Lunch Details" TextMode="MultiLine" Rows="8"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal" style="margin: 5px;">
                                <div class="form-group" runat="server" id="div15">
                                    <label class="control-label col-sm-4 lbl" id="lblEvening">Evening Refreshment Details</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtEveningRef" runat="server" CssClass="form-control" Width="340px" placeholder="Enter Evening Refreshment Details" TextMode="MultiLine" Rows="8"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-horizontal" style="margin: 70px;">
                                <div class="form-group" runat="server" id="div16">
                                    <label class="control-label col-sm-6 lbl" id="lblMorningECost">Estimated Cost (Rs.)</label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtMorningECost" runat="server" CssClass="form-control" onkeypress="return onlyDotsAndNumbers(this,event);"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group" runat="server" id="div17">
                                    <label class="control-label col-sm-6 lbl" id="lblMorningACost">Actual Cost (Rs.)</label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtMorningACost" runat="server" CssClass="form-control" onkeypress="return onlyDotsAndNumbers(this,event);"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-horizontal" style="margin: 70px; margin-top: 5%">
                                <div class="form-group" runat="server" id="div18">
                                    <label class="control-label col-sm-6 lbl" id="lblAfterECost">Estimated Cost (Rs.)</label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtAfterECost" runat="server" CssClass="form-control" onkeypress="return onlyDotsAndNumbers(this,event);"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group" runat="server" id="div21">
                                    <label class="control-label col-sm-6 lbl" id="lblAfterACost">Actual Cost (Rs.)</label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtAfterACost" runat="server" CssClass="form-control" onkeypress="return onlyDotsAndNumbers(this,event);"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-horizontal" style="margin: 70px; margin-top: 5%">
                                <div class="form-group" runat="server" id="div22">
                                    <label class="control-label col-sm-6 lbl" id="lblEveningECost">Estimated Cost (Rs.)</label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEveningECost" runat="server" CssClass="form-control" onkeypress="return onlyDotsAndNumbers(this,event);"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group" runat="server" id="div23">
                                    <label class="control-label col-sm-6 lbl" id="lblEveningACost">Actual Cost (Rs.)</label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEveningACost" runat="server" CssClass="form-control" onkeypress="return onlyDotsAndNumbers(this,event);"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                            <asp:Button ID="btnUpdate" Text="Update" CssClass="button" runat="server" OnClick="btnUpdate_Click" />
                            <asp:Button ID="btnApprove" Text="Approve" CssClass="button" runat="server" OnClick="btnApprove_Click" Visible="false"/>
                            <asp:Button ID="btnReject" Text="Revert" CssClass="button" runat="server" OnClick="btnReject_Click" Visible="false"/>
                            <asp:Button ID="btnCancel" Text="Cancel" CssClass="button" runat="server" CausesValidation="false" OnClick="btnCancel_Click" />
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
    <script>
        $("#NavLnkVFacilitiesList").attr("class", "active");
        $("#VisitorFacilitiesList").attr("class", "active");
           </script>
    <script>
          $(function () {
              $('#<%=txtDateofVisit.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        minDate: new Date('<%=txtDateofVisit.Text%>'),
                        useCurrent: false,
              });
          });
    </script>

    
    <script>
                //for start date and end date validations startdate should be < enddate
                $(function () {
                    $('#<%=txtDateFrom.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                useCurrent: false,
            });
            var startdate = document.getElementById('<%=txtDateFrom.ClientID%>').value
            $('#<%=txtDateTo.ClientID%>').datetimepicker({
                  format: 'DD MMM YYYY',
                  minDate: startdate,
                  useCurrent: false,
              });
              $('#<%=txtDateFrom.ClientID%>').on("dp.change", function (e) {
                  $('#<%=txtDateTo.ClientID%>').data("DateTimePicker").minDate(e.date);
               <%--  $('#<%=txtEndDate.ClientID%>').value = $('#<%=txtStartDate.ClientID%>').value; --%>
                        $('#<%=txtDateTo.ClientID%>').val("");
              });
              $('#<%=txtDateTo.ClientID%>').on("dp.change", function (e) {
                    });
                });
    </script>

    <script type="text/javascript">
        //for datepickers
        var startdate2 = document.getElementById('<%=txtDateFrom.ClientID%>').value
        if (startdate2 == "") {
            $('#<%=txtDateFrom.ClientID%>').datetimepicker({
                  format: 'DD MMM YYYY',
                  useCurrent: false,
              });
              $('#<%=txtDateTo.ClientID%>').datetimepicker({
                  format: 'DD MMM YYYY',
                  useCurrent: false,
              });
          }
    </script>
</asp:Content>
