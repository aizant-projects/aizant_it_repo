﻿using VMS_BAL;
using VMS_BO;
using System;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

using AizantIT_PharmaApp.Common;

namespace AizantIT_PharmaApp.VMS.VisitorFacilities
{
    public partial class VisitorFacilityRequestList : System.Web.UI.Page
    {
        VisitorFacilitiesBAL objVisitorFacilitiesBAL = new VisitorFacilitiesBAL();
        VisitorFacilitiesBO objVisitorFacilitiesBO;
        DataTable dt;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            ViewState["EmpID"] = 0;
                            ViewState["Status"] = 0;
                            ViewState["RoleID"] = 0;
                            if (HelpClass.IsUserAuthenticated())
                            {
                                ViewState["EmpID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                ViewState["RoleID"] = Session["sesGMSRoleID"].ToString();
                            }
                            ViewState["roleuser"] = string.Empty;
                            if (Request.QueryString.Count > 0)
                            {
                                ViewState["roleuser"] = HelpClass.Decrypt(HttpUtility.UrlDecode(Request.QueryString["c"]));
                                // div visible false true
                            }
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVF1:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVF1:" + strline + " " + strMsg, "error");
            }
        }

        private void InitializeThePage()
        {
            try
            {
                span_ListTitle.InnerHtml = "Visitor Facilities Request List";
                pttlModifyVisitor.InnerHtml = "Facility Request";
                screenAuthentication();
                GetPendingFacilities();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVF2:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVF2:" + strline + " " + strMsg, "error");
            }
        }

        protected void btnFind_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ViewState["roleuser"].ToString()))
            {
                // all records
                objVisitorFacilitiesBO = new VisitorFacilitiesBO();
                objVisitorFacilitiesBO.FacilityRequestID = 0;
                objVisitorFacilitiesBO.ClientName = txtSearchVisitorName.Text.Trim();
                objVisitorFacilitiesBO.DateofRequest = txtDateFrom.Text.Trim();
                objVisitorFacilitiesBO.DateofVisit = txtDateTo.Text.Trim();
                if (ViewState["RoleID"].ToString() == "17") // hr
                {
                    objVisitorFacilitiesBO.EmpID = 0;
                }
                else
                {
                    objVisitorFacilitiesBO.EmpID = Convert.ToInt32(ViewState["EmpID"].ToString());//helpdesk user
                }
                objVisitorFacilitiesBO.StatusID = 0;
                DataTable dt = objVisitorFacilitiesBAL.SearchFilterVisitorFacilities(objVisitorFacilitiesBO);
                VisitorFacilityGridView.DataSource = dt;
                VisitorFacilityGridView.DataBind();
                ViewState["dtVisitorFacility"] = dt;
                VisitorFacilityGridView.Visible = true;
                VisitorFacilityGridView.Columns[9].HeaderText = "View";
            }
            else
            {
                // pending grid
            }
        }

        private void GetPendingFacilities()
        {
            if (!string.IsNullOrEmpty(ViewState["roleuser"].ToString()))
            {


                if (ViewState["roleuser"].ToString() == "hr")
                {
                    objVisitorFacilitiesBO = new VisitorFacilitiesBO();
                    objVisitorFacilitiesBO.FacilityRequestID = 0;
                    objVisitorFacilitiesBO.ClientName = string.Empty;
                    objVisitorFacilitiesBO.DateofRequest = string.Empty;
                    objVisitorFacilitiesBO.DateofVisit = string.Empty;
                    objVisitorFacilitiesBO.EmpID = 0;
                    objVisitorFacilitiesBO.StatusID = 1;
                    DataTable dt = objVisitorFacilitiesBAL.SearchFilterVisitorFacilities(objVisitorFacilitiesBO);
                    VisitorFacilityGridView.DataSource = dt;
                    VisitorFacilityGridView.DataBind();
                    divSearch.Visible = false;
                    divgridviewVFR.Visible = true;
                    VisitorFacilityGridView.Visible = true;
                    span_ListTitle.InnerHtml = "Pending Approval Request List";
                    VisitorFacilityGridView.Columns[9].HeaderText = "APPROVE";
                    pttlModifyVisitor.InnerHtml = "Approve Facility";
                }
                else if (ViewState["roleuser"].ToString() == "hd")
                {
                    objVisitorFacilitiesBO = new VisitorFacilitiesBO();
                    objVisitorFacilitiesBO.FacilityRequestID = 0;
                    objVisitorFacilitiesBO.ClientName = string.Empty;
                    objVisitorFacilitiesBO.DateofRequest = string.Empty;
                    objVisitorFacilitiesBO.DateofVisit = string.Empty;
                    objVisitorFacilitiesBO.EmpID = Convert.ToInt32(ViewState["EmpID"].ToString());
                    objVisitorFacilitiesBO.StatusID = 2;
                    DataTable dt = objVisitorFacilitiesBAL.SearchFilterVisitorFacilities(objVisitorFacilitiesBO);
                    VisitorFacilityGridView.DataSource = dt;
                    VisitorFacilityGridView.DataBind();
                    divSearch.Visible = false;
                    divgridviewVFR.Visible = true;
                    VisitorFacilityGridView.Visible = true;
                    span_ListTitle.InnerHtml = "Pending Reverted Request List";
                    VisitorFacilityGridView.Columns[9].HeaderText = "Edit";
                    pttlModifyVisitor.InnerHtml = "Modify Facility";
                }
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            txtDateFrom.Text = txtDateTo.Text = txtSearchVisitorName.Text = string.Empty;
            VisitorFacilityGridView.Visible = false;
        }

        protected void VisitorFacilityGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            VisitorFacilityGridView.PageIndex = e.NewPageIndex;
            if (ViewState["dtVisitorFacility"] != null)
            {
                VisitorFacilityGridView.DataSource = (DataTable)ViewState["dtVisitorFacility"];
                VisitorFacilityGridView.DataBind();
            }
        }

        protected void VisitorFacilityGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
        protected void LnkEditVisitorFacility_Click(object sender, EventArgs e)
        {
            objVisitorFacilitiesBO = new VisitorFacilitiesBO();
            objVisitorFacilitiesBO.FacilityRequestID = 0;
            objVisitorFacilitiesBO.ClientName = string.Empty;
            objVisitorFacilitiesBO.DateofRequest = string.Empty;
            objVisitorFacilitiesBO.DateofVisit = string.Empty;
            LinkButton lnk = sender as LinkButton;
            GridViewRow gr = (GridViewRow)lnk.NamingContainer;
            string tempID = VisitorFacilityGridView.DataKeys[gr.RowIndex].Value.ToString();
            objVisitorFacilitiesBO.FacilityRequestID = Convert.ToInt32(tempID);
            ViewState["tempId"] = tempID;
            dt = new DataTable();
            dt = objVisitorFacilitiesBAL.SearchFilterVisitorFacilities(objVisitorFacilitiesBO);
            if (dt.Rows.Count > 0)
            {
                txtFRID.Text = dt.Rows[0]["FacilityRequestID"].ToString();
                txtVisitorName.Text = dt.Rows[0]["ClientName"].ToString();
                txtNoofVisitors.Text = dt.Rows[0]["NoofVisitors"].ToString();
                txtDateofVisit.Text = dt.Rows[0]["DateofVisit"].ToString();
                txtTravelArrangements.Text = dt.Rows[0]["TravelArrangement"].ToString();
                txtTrvlECost.Text = dt.Rows[0]["T_ECost"].ToString();
                txtTrvlACost.Text = dt.Rows[0]["T_ACost"].ToString();
                txtMorningRef.Text = dt.Rows[0]["MSnacks"].ToString();
                txtMorningECost.Text = dt.Rows[0]["MS_ECost"].ToString();
                txtMorningACost.Text = dt.Rows[0]["MS_ACost"].ToString();
                txtAfterLD.Text = dt.Rows[0]["AFSnacks"].ToString();
                txtAfterECost.Text = dt.Rows[0]["AF_ECost"].ToString();
                txtAfterACost.Text = dt.Rows[0]["AF_ACost"].ToString();
                txtEveningRef.Text = dt.Rows[0]["ESnacks"].ToString();
                txtEveningECost.Text = dt.Rows[0]["ES_ECost"].ToString();
                txtEveningACost.Text = dt.Rows[0]["ES_ACost"].ToString();
                txtRemarks.Text = dt.Rows[0]["Remarks"].ToString();
                lblEmpName.InnerText = dt.Rows[0]["EmpName"].ToString();
                lblDepartment.InnerText = dt.Rows[0]["DepartmentName"].ToString();
                lblDateofRequest.InnerText = dt.Rows[0]["DateofRequest"].ToString();
                lblEmpID.InnerText = dt.Rows[0]["EmpID"].ToString();
                ViewState["Status"] = Convert.ToInt32(dt.Rows[0]["ApproveStatus"].ToString());
                decimal T_ACost = Convert.ToDecimal(dt.Rows[0]["T_ACost"].ToString());
                decimal MS_ACost = Convert.ToDecimal(dt.Rows[0]["MS_ACost"].ToString());
                decimal AF_ACost = Convert.ToDecimal(dt.Rows[0]["AF_ACost"].ToString());
                decimal ES_ACost = Convert.ToDecimal(dt.Rows[0]["ES_ACost"].ToString());
                if (ViewState["Status"].ToString() == "3")
                {
                    btnApprove.Visible = false;
                    btnReject.Visible = false;
                    txtTrvlECost.Enabled = false;
                    txtMorningECost.Enabled = false;
                    txtAfterECost.Enabled = false;
                    txtEveningECost.Enabled = false;

                    if (T_ACost > 0 || MS_ACost > 0 || AF_ACost > 0 || ES_ACost > 0)
                    {
                        if (ViewState["RoleID"].ToString() == "18")//helpdesk=18 roleid
                        {
                            btnUpdate.Visible = false;
                            txtTrvlACost.Enabled = false;
                            txtMorningACost.Enabled = false;
                            txtAfterACost.Enabled = false;
                            txtEveningACost.Enabled = false;
                        }
                    }
                    else if (ViewState["RoleID"].ToString() == "18")
                    {
                        btnUpdate.Visible = true;
                        txtTrvlACost.Enabled = true;
                        txtMorningACost.Enabled = true;
                        txtAfterACost.Enabled = true;
                        txtEveningACost.Enabled = true;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ViewState["roleuser"].ToString()))
                    {
                        if (ViewState["roleuser"].ToString() == "hr")
                        {
                            btnApprove.Visible = true;
                            btnReject.Visible = true;
                        }
                    }
                    else if (ViewState["RoleID"].ToString() == "18")
                    {
                        txtTrvlECost.Enabled = true;
                        txtMorningECost.Enabled = true;
                        txtAfterECost.Enabled = true;
                        txtEveningECost.Enabled = true;
                        txtTrvlACost.Enabled = true;
                        txtMorningACost.Enabled = true;
                        txtAfterACost.Enabled = true;
                        txtEveningACost.Enabled = true;
                        btnUpdate.Visible = true;
                    }
                }
                if (Convert.ToInt32(ViewState["vwFacilitiesListModify"]) > 0)
                {
                    btnApprove.Visible = false;
                    btnReject.Visible = false;
                }
                ModalPopupFacility.Show();
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            objVisitorFacilitiesBO = new VisitorFacilitiesBO();
            objVisitorFacilitiesBO.FacilityRequestID = Convert.ToInt32(txtFRID.Text.Trim());
            objVisitorFacilitiesBO.ClientName = txtVisitorName.Text.Trim();
            objVisitorFacilitiesBO.NoofVisitors = Convert.ToInt32(txtNoofVisitors.Text.Trim());
            objVisitorFacilitiesBO.DateofVisit = Convert.ToDateTime(txtDateofVisit.Text).ToString("yyyyMMdd");
            objVisitorFacilitiesBO.TravelArrangement = txtTravelArrangements.Text.Trim();
            objVisitorFacilitiesBO.Remarks = txtRemarks.Text.Trim();            
            objVisitorFacilitiesBO.MSnacks = txtMorningRef.Text.Trim();
            objVisitorFacilitiesBO.AFSnacks = txtAfterLD.Text.Trim();
            objVisitorFacilitiesBO.ESnacks = txtEveningRef.Text.Trim();
            objVisitorFacilitiesBO.LastChangedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
            objVisitorFacilitiesBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            if (!string.IsNullOrEmpty(txtTrvlECost.Text.Trim()))
                objVisitorFacilitiesBO.T_ECost = Convert.ToDecimal(txtTrvlECost.Text.Trim());
            else
                objVisitorFacilitiesBO.T_ECost = 0;
            if (!string.IsNullOrEmpty(txtTrvlACost.Text.Trim()))
                objVisitorFacilitiesBO.T_ACost = Convert.ToDecimal(txtTrvlACost.Text.Trim());
            else
                objVisitorFacilitiesBO.T_ACost = 0;
            if (!string.IsNullOrEmpty(txtMorningECost.Text.Trim()))
                objVisitorFacilitiesBO.MS_ECost = Convert.ToDecimal(txtMorningECost.Text.Trim());
            else
                objVisitorFacilitiesBO.MS_ECost = 0;
            if (!string.IsNullOrEmpty(txtMorningACost.Text.Trim()))
                objVisitorFacilitiesBO.MS_ACost = Convert.ToDecimal(txtMorningACost.Text.Trim());
            else
                objVisitorFacilitiesBO.MS_ACost = 0;
            if (!string.IsNullOrEmpty(txtAfterECost.Text.Trim()))
                objVisitorFacilitiesBO.AF_ECost = Convert.ToDecimal(txtAfterECost.Text.Trim());
            else
                objVisitorFacilitiesBO.AF_ECost = 0;
            if (!string.IsNullOrEmpty(txtAfterACost.Text.Trim()))
                objVisitorFacilitiesBO.AF_ACost = Convert.ToDecimal(txtAfterACost.Text.Trim());
            else
                objVisitorFacilitiesBO.AF_ACost = 0;
            if (!string.IsNullOrEmpty(txtEveningECost.Text.Trim()))
                objVisitorFacilitiesBO.ES_ECost = Convert.ToDecimal(txtEveningECost.Text.Trim());
            else
                objVisitorFacilitiesBO.ES_ECost = 0;
            if (!string.IsNullOrEmpty(txtEveningACost.Text.Trim()))
                objVisitorFacilitiesBO.ES_ACost = Convert.ToDecimal(txtEveningACost.Text.Trim());
            else
                objVisitorFacilitiesBO.ES_ACost = 0;
            int c = objVisitorFacilitiesBAL.Update_VisitorFacilites(objVisitorFacilitiesBO);
            if (c > 0)
            {
                HelpClass.showMsg(btnUpdate, btnUpdate.GetType(), "Visitor Facilities Details Updated Successfully.");
                GetPendingFacilities();
               
            }
            else
            {
                HelpClass.showMsg(btnUpdate, btnUpdate.GetType(), "Visitor Facilities Details Updation Failed.");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            objVisitorFacilitiesBO = new VisitorFacilitiesBO();
            objVisitorFacilitiesBO.FacilityRequestID = Convert.ToInt32(txtFRID.Text.Trim());
            objVisitorFacilitiesBO.LastChangedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
            objVisitorFacilitiesBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            int c = objVisitorFacilitiesBAL.VisitorFacilitiesApproval(objVisitorFacilitiesBO);
            if (c > 0)
            {
                HelpClass.custAlertMsg(btnApprove, btnApprove.GetType(), "Facility with ID txtFRID.Text.Trim() has been Approved sucsessfully.", "success");
                GetPendingFacilities();
                
            }
            else
            {
                HelpClass.custAlertMsg(btnApprove, btnApprove.GetType(), "Visitor Facilities Details Approval Failed.", "error");
            }
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            objVisitorFacilitiesBO = new VisitorFacilitiesBO();
            objVisitorFacilitiesBO.FacilityRequestID = Convert.ToInt32(txtFRID.Text.Trim());
            objVisitorFacilitiesBO.LastChangedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
            objVisitorFacilitiesBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            int c = objVisitorFacilitiesBAL.VisitorFacilitiesRevert(objVisitorFacilitiesBO);
            if (c > 0)
            {
                HelpClass.custAlertMsg(btnReject, btnReject.GetType(), "Facility with ID txtFRID.Text.Trim() has been Reverted sucsessfully. ", "success");
                GetPendingFacilities();
                
            }
            else
            {
                HelpClass.custAlertMsg(btnReject, btnReject.GetType(), "Visitor Facilities Details Cancelation Failed.", "error");
            }
        }

        void screenAuthentication()
        {
            DataTable dtAthuntication = GmsHelpClass.GetRolePrivileges();
            if (dtAthuntication.Rows.Count > 0)
            {
                //modid=5,PrivilegeID=20,22,21,23
                int FacilitiesCreate = dtAthuntication.AsEnumerable()
        .Count(row => row.Field<int>("PrivilegeID") == 20);
                int FacilitiesListModify = dtAthuntication.AsEnumerable()
           .Count(row => row.Field<int>("PrivilegeID") == 22);
                ViewState["vwFacilitiesListModify"] = FacilitiesListModify.ToString();
                int FacilitiesApprove = dtAthuntication.AsEnumerable()
       .Count(row => row.Field<int>("PrivilegeID") == 21);
                int FacilitiesPrint = dtAthuntication.AsEnumerable()
           .Count(row => row.Field<int>("PrivilegeID") == 23);
                if (FacilitiesListModify == 0 && FacilitiesCreate == 0 && FacilitiesApprove == 0)
                    Response.Redirect("~/UserLogin.aspx");
                if (FacilitiesListModify > 0)
                {
                    btnApprove.Visible = false;
                    btnReject.Visible = false;
                }
                if (FacilitiesApprove > 0)
                {

                    btnUpdate.Visible = false;
                }
            }
        }
    }
}