﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/NestedVMS_Master.master" AutoEventWireup="true" CodeBehind="VisitorFacilityRequest.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.VisitorFacilities.VisitorFacilityRequest" %>


<asp:Content ID="Content2" ContentPlaceHolderID="VMSBody" runat="server">
     <style>
        .lbl {
            text-align: left;
            font-weight: lighter;
        }
    </style>
    <asp:HiddenField ID="HFDeptID" runat="server" />
    <div style="margin-top: 10px;">
        <div class="row Panel_Frame" style="width: 1380px; margin-left: 7%">
            <div id="header">
                <div class="col-sm-12 Panel_Header">
                    <div class="col-sm-6" style="padding-left: 5px">
                        <span class="Panel_Title">Visitor Facilities Request</span>
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div>
            
            <div class="Row">
                <div class="col-sm-6">
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div5">
                            <label class="control-label col-sm-4 lbl" id="lblEmpName12">Initiated By</label>
                            <label class="control-label col-sm-4 lbl" id="lblEmpName" runat="server"></label>
                        </div>
                    </div>
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div12">
                            <label class="control-label col-sm-4 lbl" id="lblDepartment12">Department</label>
                            <label class="control-label col-sm-4 lbl" id="lblDepartment" runat="server"></label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div1">
                            <label class="control-label col-sm-4 lbl" id="lblEmpID12">Employee ID</label>
                            <label class="control-label col-sm-4 lbl" id="lblEmpID" runat="server"></label>
                        </div>
                    </div>
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div2">
                            <label class="control-label col-sm-4 lbl" id="lblDate12">Date of Request</label>
                            <label class="control-label col-sm-4 lbl" id="lblDateofRequest" runat="server"></label>
                        </div>
                    </div>
                </div>
                <%-- Start Hr Div--%>
                <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px"></div>
                <%-- End Hr Div--%>
                <div class="col-sm-4">
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div3">
                            <label class="control-label col-sm-4 lbl" id="lblFirstName">Name of the Visitor/Client<span class="mandatoryStar">*</span></label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtVisitorName" runat="server" CssClass="form-control" placeholder="Enter Visitor/Client Name" TextMode="MultiLine" Rows="3" onkeypress="return ValidateAlpha(event)" onpaste="return false"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div11">
                            <label class="control-label col-sm-4 lbl" id="lblNoofVisitors">No of Visitors<span class="mandatoryStar">*</span></label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtNoofVisitors" runat="server" CssClass="form-control" placeholder="Enter No of Visitors" TabIndex="11" onkeypress="if(event.keyCode<48 || event.keyCode>57)event.returnValue=false;"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div14">
                            <label class="control-label col-sm-4 lbl" id="lblDateofVisit">Date of Visit<span class="mandatoryStar">*</span></label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtDateofVisit" runat="server" CssClass="form-control" placeholder="Select Date"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div6">
                            <label class="control-label col-sm-4 lbl" id="lblTravel">Travel Arrangements Details If Any</label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtTravelArrangements" runat="server" CssClass="form-control" placeholder="Enter Travel Arrangements" TextMode="MultiLine" Rows="6"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div19">
                            <label class="control-label col-sm-5 lbl" id="lblTrvlECost">Estimated Cost (Rs.)</label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtTrvlECost" runat="server" CssClass="form-control" style="margin-left:56%;margin-top:-8%;" onkeypress="return onlyDotsAndNumbers(this,event);"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div20">
                            <label class="control-label col-sm-4 lbl" id="lblTrvlACost">Actual Cost (Rs.)</label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtTrvlACost" runat="server" CssClass="form-control" onkeypress="return onlyDotsAndNumbers(this,event);"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div4">
                            <label class="control-label col-sm-4 lbl" id="lblRemarks">Remarks</label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" placeholder="Enter Remarks" TextMode="MultiLine" Rows="6"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div7">
                            <label class="control-label col-sm-4 lbl" id="lblMorningRD">Morning Refreshment Details</label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtMorningRef" runat="server" CssClass="form-control" Width="340px" placeholder="Enter Morning Refreshment Details" TextMode="MultiLine" Rows="8"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div8">
                            <label class="control-label col-sm-4 lbl" id="lblAfterNoon">AfterNoon Lunch Details</label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtAfterLD" runat="server" CssClass="form-control" Width="340px" placeholder="Enter AfterNoon Lunch Details" TextMode="MultiLine" Rows="8"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div9">
                            <label class="control-label col-sm-4 lbl" id="lblEvening">Evening Refreshment Details</label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtEveningRef" runat="server" CssClass="form-control" Width="340px" placeholder="Enter Evening Refreshment Details" TextMode="MultiLine" Rows="8"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-horizontal" style="margin: 70px;">
                        <div class="form-group" runat="server" id="div10">
                            <label class="control-label col-sm-6 lbl" id="lblMorningECost">Estimated Cost (Rs.)</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtMorningECost" runat="server" CssClass="form-control" onkeypress="return onlyDotsAndNumbers(this,event);"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group" runat="server" id="div13">
                            <label class="control-label col-sm-6 lbl" id="lblMorningACost">Actual Cost (Rs.)</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtMorningACost" runat="server" CssClass="form-control" onkeypress="return onlyDotsAndNumbers(this,event);"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-horizontal" style="margin: 70px; margin-top: 5%">
                        <div class="form-group" runat="server" id="div15">
                            <label class="control-label col-sm-6 lbl" id="lblAfterECost">Estimated Cost (Rs.)</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtAfterECost" runat="server" CssClass="form-control" onkeypress="return onlyDotsAndNumbers(this,event);"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group" runat="server" id="div16">
                            <label class="control-label col-sm-6 lbl" id="lblAfterACost">Actual Cost (Rs.)</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtAfterACost" runat="server" CssClass="form-control" onkeypress="return onlyDotsAndNumbers(this,event);"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-horizontal" style="margin: 70px; margin-top: 5%">
                        <div class="form-group" runat="server" id="div17">
                            <label class="control-label col-sm-6 lbl" id="lblEveningECost">Estimated Cost (Rs.)</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtEveningECost" runat="server" CssClass="form-control" onkeypress="return onlyDotsAndNumbers(this,event);"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group" runat="server" id="div18">
                            <label class="control-label col-sm-6 lbl" id="lblEveningACost">Actual Cost (Rs.)</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtEveningACost" runat="server" CssClass="form-control" onkeypress="return onlyDotsAndNumbers(this,event);"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                    <asp:Button ID="btnSubmit" Text="Submit" CssClass="button" runat="server" OnClientClick="javascript:return Submitvalidate();" OnClick="btnSubmit_Click" />
                    <asp:Button ID="btnReset" Text="Reset" CssClass="button" runat="server" CausesValidation="false" OnClick="btnReset_Click" />
                </div>
            </div>


            <script type="text/javascript">
                function Submitvalidate() {
                        var VName = document.getElementById("<%=txtVisitorName.ClientID%>").value;
                        var NoofVisitors = document.getElementById("<%=txtNoofVisitors.ClientID%>").value;
                        var DateofVisit = document.getElementById("<%=txtDateofVisit.ClientID%>").value;
                        errors = [];
                        if (VName.trim() == "") {
                            errors.push("Please Enter Visitor/Client Name.");
                        }
                        if (NoofVisitors.trim() == "" || NoofVisitors.trim()==0) {
                            errors.push("Please Enter No of Visitors.");
                        }
                        if (DateofVisit.trim() == "") {
                            errors.push("Please Select Date of Visit.");
                        }
                        if (errors.length > 0) {
                            custAlertMsg(errors.join("<br/>"), "error");
                            return false;
                        }
                }
            </script>

            <script>
                $("#NavLnkVFacilities").attr("class", "active");
                $("#VisitorFacilities").attr("class", "active");
           </script>
            <script>
                $(function () {
                    $('#<%=txtDateofVisit.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        minDate: new Date(),
                    });
                });
            </script>
        </div>
    </div>
</asp:Content>
