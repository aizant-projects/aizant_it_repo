﻿using VMS_BAL;
using VMS_BO;
using System;
using System.Linq;

using System.Data;
using System.Data.SqlClient;

using AizantIT_PharmaApp.Common;

namespace AizantIT_PharmaApp.VMS.VisitorFacilities
{
    public partial class VisitorFacilityRequest : System.Web.UI.Page
    {
        VisitorFacilitiesBAL objVisitorFacilitiesBAL = new VisitorFacilitiesBAL();
        VisitorFacilitiesBO objVisitorFacilitiesBO;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            ViewState["EmpID"] = "0";
                            if (HelpClass.IsUserAuthenticated())
                            {
                                ViewState["EmpID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            }
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVF1:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVF1:" + strline + " " + strMsg,"error");
            }
        }

        private void InitializeThePage()
        {
            try
            {
                screenAuthentication();
                DateofRequest();
                GetEmpDataToVisitorFacilities();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVF2:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVF2:" + strline + " " + strMsg,"error");
            }
        }

        private void DateofRequest()
        {
            lblDateofRequest.InnerText = DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
        }

        private void GetEmpDataToVisitorFacilities()
        {
            objVisitorFacilitiesBAL = new VisitorFacilitiesBAL();
            objVisitorFacilitiesBO = new VisitorFacilitiesBO();
            objVisitorFacilitiesBO.EmpID = Convert.ToInt32(ViewState["EmpID"].ToString());
            DataSet ds = objVisitorFacilitiesBAL.GetEmpDataToVisitorFacilities(objVisitorFacilitiesBO);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblEmpName.InnerText = ds.Tables[0].Rows[0]["EmpName"].ToString();
                lblDepartment.InnerText = ds.Tables[0].Rows[0]["DepartmentName"].ToString();
                lblEmpID.InnerText = ds.Tables[0].Rows[0]["EmpID"].ToString();
                HFDeptID.Value = ds.Tables[0].Rows[0]["DeptID"].ToString();
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                objVisitorFacilitiesBO = new VisitorFacilitiesBO();
                objVisitorFacilitiesBO.EmpID = Convert.ToInt32(ViewState["EmpID"].ToString());
                objVisitorFacilitiesBO.DeptID = Convert.ToInt32(HFDeptID.Value);
                objVisitorFacilitiesBO.DateofRequest = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                objVisitorFacilitiesBO.ClientName = txtVisitorName.Text.Trim();
                objVisitorFacilitiesBO.NoofVisitors = Convert.ToInt32(txtNoofVisitors.Text.Trim());
                objVisitorFacilitiesBO.DateofVisit = Convert.ToDateTime(txtDateofVisit.Text).ToString("yyyyMMdd");
                objVisitorFacilitiesBO.TravelArrangement = txtTravelArrangements.Text.Trim();
                if (!string.IsNullOrEmpty(txtTrvlECost.Text.Trim()))
                    objVisitorFacilitiesBO.T_ECost = Convert.ToDecimal(txtTrvlECost.Text.Trim());
                else
                    objVisitorFacilitiesBO.T_ECost = 0;
                if (!string.IsNullOrEmpty(txtTrvlACost.Text.Trim()))
                    objVisitorFacilitiesBO.T_ACost = Convert.ToDecimal(txtTrvlACost.Text.Trim());
                else
                    objVisitorFacilitiesBO.T_ACost = 0;
                objVisitorFacilitiesBO.MSnacks = txtMorningRef.Text.Trim();
                if (!string.IsNullOrEmpty(txtMorningECost.Text.Trim()))
                    objVisitorFacilitiesBO.MS_ECost = Convert.ToDecimal(txtMorningECost.Text.Trim());
                else
                    objVisitorFacilitiesBO.MS_ECost = 0;
                if (!string.IsNullOrEmpty(txtMorningACost.Text.Trim()))
                    objVisitorFacilitiesBO.MS_ACost = Convert.ToDecimal(txtMorningACost.Text.Trim());
                else
                    objVisitorFacilitiesBO.MS_ACost = 0;
                objVisitorFacilitiesBO.AFSnacks = txtAfterLD.Text.Trim();
                if (!string.IsNullOrEmpty(txtAfterECost.Text.Trim()))
                    objVisitorFacilitiesBO.AF_ECost = Convert.ToDecimal(txtAfterECost.Text.Trim());
                else
                    objVisitorFacilitiesBO.AF_ECost = 0;
                if (!string.IsNullOrEmpty(txtAfterACost.Text.Trim()))
                    objVisitorFacilitiesBO.AF_ACost = Convert.ToDecimal(txtAfterACost.Text.Trim());
                else
                    objVisitorFacilitiesBO.AF_ACost = 0;
                objVisitorFacilitiesBO.ESnacks = txtEveningRef.Text.Trim();
                if (!string.IsNullOrEmpty(txtEveningECost.Text.Trim()))
                    objVisitorFacilitiesBO.ES_ECost = Convert.ToDecimal(txtEveningECost.Text.Trim());
                else
                    objVisitorFacilitiesBO.ES_ECost = 0;
                if (!string.IsNullOrEmpty(txtEveningACost.Text.Trim()))
                    objVisitorFacilitiesBO.ES_ACost = Convert.ToDecimal(txtEveningACost.Text.Trim());
                else
                    objVisitorFacilitiesBO.ES_ACost = 0;
                objVisitorFacilitiesBO.Remarks = txtRemarks.Text.Trim();
                objVisitorFacilitiesBO.CreatedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");

                int c = objVisitorFacilitiesBAL.VisitorFacilities_Insert(objVisitorFacilitiesBO);
                if (c > 0)
                {
                    HelpClass.showMsg(btnSubmit, btnSubmit.GetType(), "Visitor Facilities Details Submitted Successfully.");
                }
                else
                {
                    HelpClass.showMsg(btnSubmit, btnSubmit.GetType(), "Visitor Facilities Details Submission Failed.");
                }
                VisitorFacilities_Reset();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVF3:" + strmsgF,"error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVF3:" + strline + " " + strMsg,"error");
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            VisitorFacilities_Reset();
        }

        private void VisitorFacilities_Reset()
        {
            txtVisitorName.Text = txtNoofVisitors.Text = txtAfterACost.Text = txtAfterECost.Text = txtAfterLD.Text = txtDateofVisit.Text = txtEveningACost.Text = txtEveningECost.Text =
            txtEveningRef.Text = txtMorningACost.Text = txtMorningECost.Text = txtMorningRef.Text = txtRemarks.Text = txtTravelArrangements.Text = txtTrvlACost.Text = txtTrvlECost.Text = string.Empty;
            DateofRequest();
        }

        void screenAuthentication()
        {
            DataTable dtAthuntication = GmsHelpClass.GetRolePrivileges();
            if (dtAthuntication.Rows.Count > 0)
            {
                //modid=5,PrivilegeID=20,22
                int FacilitiesCreate = dtAthuntication.AsEnumerable()
        .Count(row => row.Field<int>("PrivilegeID") == 20);
                int FacilitiesListModify = dtAthuntication.AsEnumerable()
           .Count(row => row.Field<int>("PrivilegeID") == 22);
                if (FacilitiesListModify == 0 && FacilitiesCreate == 0)
                    Response.Redirect("~/UserLogin.aspx");
            }
        }
    }
}