﻿using VMS_BAL;
using VMS_BO;
using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

using AizantIT_PharmaApp.Common;
using System.Text;
using System.Collections;
using UMS_BusinessLayer;

namespace AizantIT_PharmaApp.VMS.VisitorFacilities
{
    public partial class VisitorFacilityList : System.Web.UI.Page
    {
        VisitorFacilitiesBAL objVisitorFacilitiesBAL = new VisitorFacilitiesBAL();
        VisitorFacilitiesBO objVisitorFacilitiesBO;
        UMS_BAL objUMS_BAL;
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ConfirmCustomAlert.buttonClick += new EventHandler(ConfirmCustomAlert_buttonClick);
                if (!IsPostBack)
                {
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                           
                            if (HelpClass.IsUserAuthenticated())
                            {
                               
                               
                            }
                            ViewState["roleuser"] = string.Empty;
                            ViewState["isEditMode"] = 0;
                            Session["VFRApproveStatus"] = 0;
                            InitializeThePage();
                            if (Request.QueryString.Count > 0)
                            {
                                if (Request.QueryString["c"] != null)
                                {
                                    ViewState["roleuser"] = HelpClass.Decrypt(HttpUtility.UrlDecode(Request.QueryString["c"]));
                                    hdfUserRole.Value = Request.QueryString["c"];
                                    if (ViewState["roleuser"].ToString() == "bd")
                                    {
                                        Session["VFRApproveStatus"] = 2;//Revert
                                    }
                                    else
                                    {
                                        Session["VFRApproveStatus"] = 1;//InReview
                                    }
                                    ViewState["isEditMode"] = 1;
                                    btnNavFacilityCreate.Visible = false;
                                    upbtnNavFacilityCreate.Update();
                                }

                                if (Request.QueryString["Notification_ID"] != null)
                                {
                                    if (Session["sesGMSRoleID"].ToString() == "28")
                                    {
                                        Session["VFRApproveStatus"] = 2;//Revert
                                        hdfUserRole.Value = HttpUtility.UrlEncode(HelpClass.Encrypt("bd"));
                                    }
                                    else
                                    {
                                        Session["VFRApproveStatus"] = 1;//InReview
                                        hdfUserRole.Value = HttpUtility.UrlEncode(HelpClass.Encrypt("hr"));
                                    }
                                    ViewState["isEditMode"] = 1;
                                    btnNavFacilityCreate.Visible = false;
                                    upbtnNavFacilityCreate.Update();
                                    // update notification status
                                    objUMS_BAL = new UMS_BAL();
                                    int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs);
                                    VisitorFacilitiesBO objVisitorFacilitiesBO = new VisitorFacilitiesBO();
                                    objVisitorFacilitiesBO.NotificationID = Convert.ToInt32(Request.QueryString["Notification_ID"]);
                                    DataTable dtResult = objVisitorFacilitiesBAL.GetFacilityIDFromNotification(objVisitorFacilitiesBO);
                                    hdnPkVisitorFacilityEditID.Value = dtResult.Rows[0]["FacilityID"].ToString();
                                    Session["sesGMSRoleID"] = dtResult.Rows[0]["NRoleID"].ToString();
                                    GetVisitorfacilitiesData(Convert.ToInt32(dtResult.Rows[0]["FacilityID"].ToString()));
                                }
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }

            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVF1:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVF1:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVF1:" + strline + " " + strMsg, "error");
            }
        }

        protected void ConfirmCustomAlert_buttonClick(object sender, EventArgs e)
        {
            try
            {
                objVisitorFacilitiesBO = new VisitorFacilitiesBO();
                objVisitorFacilitiesBAL = new VisitorFacilitiesBAL();
                if (hdnVisitorFacilityCustom.Value == "Submit")
                {
                    try
                    {
                        objVisitorFacilitiesBO.CreatedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                        objVisitorFacilitiesBO.CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                        string AddClientName;
                        if (txtVisitorName.Visible == true)
                        {
                            objVisitorFacilitiesBO.ClientName = txtVisitorName.Text.Trim();
                            AddClientName = objVisitorFacilitiesBAL.MasterClientNameInsert(objVisitorFacilitiesBO).ToString();
                            if (AddClientName == "0")
                            {
                                HelpClass.custAlertMsg(btnAddVisitorClientName, btnAddVisitorClientName.GetType(), "Please enter visitor/client name.", "error");
                                return;
                            }
                        }
                        else
                        {
                            AddClientName = ddlClientName.SelectedValue;
                        }
                        objVisitorFacilitiesBO.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                        objVisitorFacilitiesBO.DeptID = Convert.ToInt32(HFDeptID.Value);
                        objVisitorFacilitiesBO.DateofRequest = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                        objVisitorFacilitiesBO.ClientID = Convert.ToInt32(AddClientName);
                        objVisitorFacilitiesBO.NoofVisitors = Convert.ToInt32(txtNoofVisitors.Text.Trim());
                        objVisitorFacilitiesBO.DateofVisit = (txtDateofVisit.Text).ToString();
                        objVisitorFacilitiesBO.TravelArrangement = txtTravelArrangements.Text.Trim();
                        if (!string.IsNullOrEmpty(txtTrvlECost.Text.Trim()))
                            objVisitorFacilitiesBO.T_ECost = Convert.ToDecimal(txtTrvlECost.Text.Trim());
                        else
                            objVisitorFacilitiesBO.T_ECost = 0;
                        if (!string.IsNullOrEmpty(txtTrvlACost.Text.Trim()))
                            objVisitorFacilitiesBO.T_ACost = Convert.ToDecimal(txtTrvlACost.Text.Trim());
                        else
                            objVisitorFacilitiesBO.T_ACost = 0;
                        objVisitorFacilitiesBO.MSnacks = txtMorningRef.Text.Trim();
                        if (!string.IsNullOrEmpty(txtMorningECost.Text.Trim()))
                            objVisitorFacilitiesBO.MS_ECost = Convert.ToDecimal(txtMorningECost.Text.Trim());
                        else
                            objVisitorFacilitiesBO.MS_ECost = 0;
                        if (!string.IsNullOrEmpty(txtMorningACost.Text.Trim()))
                            objVisitorFacilitiesBO.MS_ACost = Convert.ToDecimal(txtMorningACost.Text.Trim());
                        else
                            objVisitorFacilitiesBO.MS_ACost = 0;
                        objVisitorFacilitiesBO.AFSnacks = txtAfterLD.Text.Trim();
                        if (!string.IsNullOrEmpty(txtAfterECost.Text.Trim()))
                            objVisitorFacilitiesBO.AF_ECost = Convert.ToDecimal(txtAfterECost.Text.Trim());
                        else
                            objVisitorFacilitiesBO.AF_ECost = 0;
                        if (!string.IsNullOrEmpty(txtAfterACost.Text.Trim()))
                            objVisitorFacilitiesBO.AF_ACost = Convert.ToDecimal(txtAfterACost.Text.Trim());
                        else
                            objVisitorFacilitiesBO.AF_ACost = 0;
                        objVisitorFacilitiesBO.ESnacks = txtEveningRef.Text.Trim();
                        if (!string.IsNullOrEmpty(txtEveningECost.Text.Trim()))
                            objVisitorFacilitiesBO.ES_ECost = Convert.ToDecimal(txtEveningECost.Text.Trim());
                        else
                            objVisitorFacilitiesBO.ES_ECost = 0;
                        if (!string.IsNullOrEmpty(txtEveningACost.Text.Trim()))
                            objVisitorFacilitiesBO.ES_ACost = Convert.ToDecimal(txtEveningACost.Text.Trim());
                        else
                            objVisitorFacilitiesBO.ES_ACost = 0;
                        objVisitorFacilitiesBO.Remarks = txtRemarks.Text.Trim();


                        int c = objVisitorFacilitiesBAL.VisitorFacilities_Insert(objVisitorFacilitiesBO);
                        if (c > 0)
                        {
                            LoadClientNameMaster();
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Visitor facilities details submitted successfully.", "success", "Reloadtable(0);");
                        }
                        else
                        {
                            HelpClass.showMsg(btnSubmit, btnSubmit.GetType(), "Visitor facilities details submission failed.");
                        }
                        VisitorFacilities_Reset();
                    }
                    catch (SqlException sqe)
                    {
                        string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                        HelpClass.custAlertMsg(this, this.GetType(), "VMSVF2:" + strmsgF, "error");
                    }
                    catch (Exception ex)
                    {
                        string strline = HelpClass.LineNo(ex);
                        string strMsg = HelpClass.SQLEscapeString(ex.Message);
                        Aizant_log.Error("VMSVF2:" + strline + " " + strMsg);
                        HelpClass.custAlertMsg(this, this.GetType(), "VMSVF2:" + strline + " " + strMsg, "error");
                    }
                }
                else if (hdnVisitorFacilityCustom.Value == "Update")
                {
                    string UpdateMode = "1";
                    objVisitorFacilitiesBO.FacilityRequestID = Convert.ToInt32(hdnPkVisitorFacilityEditID.Value);
                    objVisitorFacilitiesBO.ClientID = Convert.ToInt32(ddlClientName.SelectedValue);
                    objVisitorFacilitiesBO.NoofVisitors = Convert.ToInt32(txtNoofVisitors.Text.Trim());
                    objVisitorFacilitiesBO.DateofVisit = Convert.ToDateTime(txtDateofVisit.Text).ToString("yyyyMMdd");
                    objVisitorFacilitiesBO.TravelArrangement = txtTravelArrangements.Text.Trim();
                    objVisitorFacilitiesBO.Remarks = txtRemarks.Text.Trim();
                    objVisitorFacilitiesBO.MSnacks = txtMorningRef.Text.Trim();
                    objVisitorFacilitiesBO.AFSnacks = txtAfterLD.Text.Trim();
                    objVisitorFacilitiesBO.ESnacks = txtEveningRef.Text.Trim();
                    objVisitorFacilitiesBO.LastChangedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                    objVisitorFacilitiesBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                    objVisitorFacilitiesBO.Comments = txtFacilitycomments.Text.Trim();
                    if (!string.IsNullOrEmpty(txtTrvlECost.Text.Trim()))
                        objVisitorFacilitiesBO.T_ECost = Convert.ToDecimal(txtTrvlECost.Text.Trim());
                    else
                        objVisitorFacilitiesBO.T_ECost = 0;
                    if (!string.IsNullOrEmpty(txtTrvlACost.Text.Trim()))
                        objVisitorFacilitiesBO.T_ACost = Convert.ToDecimal(txtTrvlACost.Text.Trim());
                    else
                        objVisitorFacilitiesBO.T_ACost = 0;
                    if (!string.IsNullOrEmpty(txtMorningECost.Text.Trim()))
                        objVisitorFacilitiesBO.MS_ECost = Convert.ToDecimal(txtMorningECost.Text.Trim());
                    else
                        objVisitorFacilitiesBO.MS_ECost = 0;
                    if (!string.IsNullOrEmpty(txtMorningACost.Text.Trim()))
                        objVisitorFacilitiesBO.MS_ACost = Convert.ToDecimal(txtMorningACost.Text.Trim());
                    else
                        objVisitorFacilitiesBO.MS_ACost = 0;
                    if (!string.IsNullOrEmpty(txtAfterECost.Text.Trim()))
                        objVisitorFacilitiesBO.AF_ECost = Convert.ToDecimal(txtAfterECost.Text.Trim());
                    else
                        objVisitorFacilitiesBO.AF_ECost = 0;
                    if (!string.IsNullOrEmpty(txtAfterACost.Text.Trim()))
                        objVisitorFacilitiesBO.AF_ACost = Convert.ToDecimal(txtAfterACost.Text.Trim());
                    else
                        objVisitorFacilitiesBO.AF_ACost = 0;
                    if (!string.IsNullOrEmpty(txtEveningECost.Text.Trim()))
                        objVisitorFacilitiesBO.ES_ECost = Convert.ToDecimal(txtEveningECost.Text.Trim());
                    else
                        objVisitorFacilitiesBO.ES_ECost = 0;
                    if (!string.IsNullOrEmpty(txtEveningACost.Text.Trim()))
                        objVisitorFacilitiesBO.ES_ACost = Convert.ToDecimal(txtEveningACost.Text.Trim());
                    else
                        objVisitorFacilitiesBO.ES_ACost = 0;

                    if (ViewState["Status"].ToString() == "3")
                    {
                        if (!string.IsNullOrEmpty(txtTrvlACost.Text.Trim()))
                            objVisitorFacilitiesBO.T_ACost = Convert.ToDecimal(txtTrvlACost.Text.Trim());
                        else
                            objVisitorFacilitiesBO.T_ACost = 0;
                        if (!string.IsNullOrEmpty(txtMorningACost.Text.Trim()))
                            objVisitorFacilitiesBO.MS_ACost = Convert.ToDecimal(txtMorningACost.Text.Trim());
                        else
                            objVisitorFacilitiesBO.MS_ACost = 0;
                        if (!string.IsNullOrEmpty(txtAfterACost.Text.Trim()))
                            objVisitorFacilitiesBO.AF_ACost = Convert.ToDecimal(txtAfterACost.Text.Trim());
                        else
                            objVisitorFacilitiesBO.AF_ACost = 0;
                        if (!string.IsNullOrEmpty(txtEveningACost.Text.Trim()))
                            objVisitorFacilitiesBO.ES_ACost = Convert.ToDecimal(txtEveningACost.Text.Trim());
                        else
                            objVisitorFacilitiesBO.ES_ACost = 0;
                        objVisitorFacilitiesBO.Mode = 1;
                        UpdateMode = "0";
                    }
                    int c = objVisitorFacilitiesBAL.Update_VisitorFacilites(objVisitorFacilitiesBO);
                    if (c > 0)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Visitor facilities details updated successfully.", "success", "Reloadtable(" + UpdateMode + ");");
                    }
                    else
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Visitor facilities details updation failed.", "error");
                    }
                }
                else if (hdnVisitorFacilityCustom.Value == "Approve")
                {
                    objVisitorFacilitiesBO = new VisitorFacilitiesBO();
                    objVisitorFacilitiesBO.FacilityRequestID = Convert.ToInt32(hdnPkVisitorFacilityEditID.Value);
                    objVisitorFacilitiesBO.LastChangedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                    objVisitorFacilitiesBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                    objVisitorFacilitiesBO.Comments = txtFacilitycomments.Text.Trim();
                    int c = objVisitorFacilitiesBAL.VisitorFacilitiesApproval(objVisitorFacilitiesBO);
                    if (c > 0)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Facility with ID " + hdnPkVisitorFacilityEditID.Value + " has been approved successfully.", "success", "Reloadtable(1);");
                    }
                    else
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Visitor facilities details approval failed.", "error");
                    }
                }
                else if (hdnVisitorFacilityCustom.Value == "Revert")
                {
                    objVisitorFacilitiesBO = new VisitorFacilitiesBO();
                    objVisitorFacilitiesBO.FacilityRequestID = Convert.ToInt32(hdnPkVisitorFacilityEditID.Value);
                    objVisitorFacilitiesBO.LastChangedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                    objVisitorFacilitiesBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                    objVisitorFacilitiesBO.Comments = txtFacilitycomments.Text.Trim();
                    int c = objVisitorFacilitiesBAL.VisitorFacilitiesRevert(objVisitorFacilitiesBO);
                    if (c > 0)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Facility with ID " + hdnPkVisitorFacilityEditID.Value + " has been reverted successfully.", "success", "Reloadtable(1);");
                    }
                    else
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Visitor facilities details cancelation failed.", "error");
                    }
                }
                else if (hdnVisitorFacilityCustom.Value == "Reject")
                {
                    objVisitorFacilitiesBO = new VisitorFacilitiesBO();
                    objVisitorFacilitiesBO.FacilityRequestID = Convert.ToInt32(hdnPkVisitorFacilityEditID.Value);
                    objVisitorFacilitiesBO.LastChangedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                    objVisitorFacilitiesBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                    objVisitorFacilitiesBO.Comments = txtFacilitycomments.Text.Trim();
                    int c = objVisitorFacilitiesBAL.VisitorFacilitiesRejectOrCancel(objVisitorFacilitiesBO);
                    if (c > 0)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Facility with ID " + hdnPkVisitorFacilityEditID.Value + " has been rejected successfully.", "success", "Reloadtable();");
                    }
                    else
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Visitor facilities details cancelation failed.", "error");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVF3:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVF3:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVF3:" + strline + " " + strMsg, "error");
            }
        }

        private void InitializeThePage()
        {
            try
            {
                screenAuthentication();
                DateofRequest();
                GetEmpDataToVisitorFacilities();
                LoadClientNameMaster();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVF4:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVF4:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVF4:" + strline + " " + strMsg, "error");
            }
        }

        private void DateofRequest()
        {
            lblDateofRequest.InnerText = DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
        }

        private void GetEmpDataToVisitorFacilities()
        {
            objVisitorFacilitiesBAL = new VisitorFacilitiesBAL();
            objVisitorFacilitiesBO = new VisitorFacilitiesBO();
            objVisitorFacilitiesBO.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
            DataSet ds = objVisitorFacilitiesBAL.GetEmpDataToVisitorFacilities(objVisitorFacilitiesBO);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblEmpName.InnerText = ds.Tables[0].Rows[0]["EmpName"].ToString();
                lblDepartment.InnerText = ds.Tables[0].Rows[0]["DepartmentName"].ToString();
                lblEmpID.InnerText = ds.Tables[0].Rows[0]["EmpID"].ToString();
                HFDeptID.Value = ds.Tables[0].Rows[0]["DeptID"].ToString();
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            VisitorFacilities_Reset();
        }

        private void VisitorFacilities_Reset()
        {
            txtVisitorName.Text = txtNoofVisitors.Text = txtAfterACost.Text = txtAfterECost.Text = txtAfterLD.Text = txtDateofVisit.Text = txtEveningACost.Text = txtEveningECost.Text =
            txtEveningRef.Text = txtMorningACost.Text = txtMorningECost.Text = txtMorningRef.Text = txtRemarks.Text = txtTravelArrangements.Text = txtTrvlACost.Text = txtTrvlECost.Text = string.Empty;
            DateofRequest();
            if (btnAddVisitorClientName.Text == "<<")
            {
                btnAddVisitorClientName.Text = "+";
                txtVisitorName.Visible = false;
                ddlClientName.Visible = true;
            }
            ddlClientName.SelectedIndex = 0;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (txtNoofVisitors.Text.Trim() == "" || txtDateofVisit.Text.Trim() == "")
            {
                ArrayList Mandatory = new ArrayList();
                if (txtVisitorName.Visible == true)
                {
                    if (string.IsNullOrEmpty(txtVisitorName.Text.Trim()))
                    {
                        Mandatory.Add("Please enter client name.");
                    }
                }
                if (ddlClientName.Visible == true)
                {
                    if (ddlClientName.SelectedIndex == 0)
                    {
                        Mandatory.Add("Please select client name.");
                    }
                }
                if (string.IsNullOrEmpty(txtNoofVisitors.Text.Trim()))
                {
                    Mandatory.Add("Please enter no of visitors.");
                }
                if (String.IsNullOrEmpty(txtDateofVisit.Text.Trim()))
                {
                    Mandatory.Add("Please select date of visit.");
                }
                StringBuilder s = new StringBuilder();
                foreach (string x in Mandatory)
                {
                    s.Append(x);
                    s.Append("<br/>");
                }
                HelpClass.custAlertMsg(this, this.GetType(), "" + s.ToString() + "", "error");
                return;
            }
            hdnVisitorFacilityCustom.Value = "Submit";
            HelpClass.custAlertMsg(this, this.GetType(), "Do you want to submit ?", "confirm");
        }

        //Visitor Facility Edit Button
        protected void btnEditVisitorFacility_Click(object sender, EventArgs e)
        {
            GetVisitorfacilitiesData(Convert.ToInt32(hdnPkVisitorFacilityEditID.Value));
        }

        private void VisitorFacilitiesDisableControls(int type)
        {// 0-all,1-actual,2-comments
            txtVisitorName.Enabled = false;
            ddlClientName.Enabled = false;
            txtNoofVisitors.Enabled = false;
            txtDateofVisit.Enabled = false;
            txtTravelArrangements.Enabled = false;
            txtTrvlECost.Enabled = false;
            txtTrvlACost.Enabled = false;
            txtMorningRef.Enabled = false;
            txtMorningECost.Enabled = false;
            txtMorningACost.Enabled = false;
            txtAfterLD.Enabled = false;
            txtAfterECost.Enabled = false;
            txtAfterACost.Enabled = false;
            txtEveningRef.Enabled = false;
            txtEveningECost.Enabled = false;
            txtEveningACost.Enabled = false;
            txtRemarks.Enabled = false;
            txtFacilitycomments.Enabled = false;
            if (type == 1)
            {
                txtTrvlACost.Enabled = true;
                txtMorningACost.Enabled = true;
                txtAfterACost.Enabled = true;
                txtEveningACost.Enabled = true;
                txtFacilitycomments.Enabled = true;
            }
            else if (type == 2)
            {
                txtFacilitycomments.Enabled = true;
            }
        }
        private void VisitorFacilities_EnableControls()
        {
            txtVisitorName.Enabled = true;
            ddlClientName.Enabled = true;
            txtNoofVisitors.Enabled = true;
            txtDateofVisit.Enabled = true;
            txtTravelArrangements.Enabled = true;
            txtTrvlECost.Enabled = true;
            txtTrvlACost.Enabled = true;
            txtMorningRef.Enabled = true;
            txtMorningECost.Enabled = true;
            txtMorningACost.Enabled = true;
            txtAfterLD.Enabled = true;
            txtAfterECost.Enabled = true;
            txtAfterACost.Enabled = true;
            txtEveningRef.Enabled = true;
            txtEveningECost.Enabled = true;
            txtEveningACost.Enabled = true;
            txtRemarks.Enabled = true;
        }

        public void GetVisitorfacilitiesData(int FacilityID)
        {
            JQDatatableBO objJQDataTableBO = new JQDatatableBO();
            objJQDataTableBO.iMode = 0;
            objJQDataTableBO.pkid = FacilityID;
            objJQDataTableBO.iDisplayLength = 0;
            objJQDataTableBO.iDisplayStart = 0;
            objJQDataTableBO.iSortCol = 0;
            objJQDataTableBO.sSortDir = "";
            objJQDataTableBO.sSearch = "";
            objJQDataTableBO.Status = 0;
            objJQDataTableBO.EmpID = 0;
            objVisitorFacilitiesBAL = new VisitorFacilitiesBAL();
            DataTable dt = new DataTable();
            dt = objVisitorFacilitiesBAL.GetVisitorFacilitiesDetails_List(objJQDataTableBO);
            if (dt.Rows.Count > 0)
            {
                LoadClientNameMaster();
                ddlClientName.SelectedValue = dt.Rows[0]["ClientID"].ToString();
                txtNoofVisitors.Text = dt.Rows[0]["NoofVisitors"].ToString();
                txtDateofVisit.Text = dt.Rows[0]["DateofVisit"].ToString();
                txtTravelArrangements.Text = dt.Rows[0]["TravelArrangement"].ToString();
                txtTrvlECost.Text = dt.Rows[0]["T_ECost"].ToString();
                txtTrvlACost.Text = dt.Rows[0]["T_ACost"].ToString();
                txtMorningRef.Text = dt.Rows[0]["MSnacks"].ToString();
                txtMorningECost.Text = dt.Rows[0]["MS_ECost"].ToString();
                txtMorningACost.Text = dt.Rows[0]["MS_ACost"].ToString();
                txtAfterLD.Text = dt.Rows[0]["AFSnacks"].ToString();
                txtAfterECost.Text = dt.Rows[0]["AF_ECost"].ToString();
                txtAfterACost.Text = dt.Rows[0]["AF_ACost"].ToString();
                txtEveningRef.Text = dt.Rows[0]["ESnacks"].ToString();
                txtEveningECost.Text = dt.Rows[0]["ES_ECost"].ToString();
                txtEveningACost.Text = dt.Rows[0]["ES_ACost"].ToString();
                txtRemarks.Text = dt.Rows[0]["Remarks"].ToString();
                lblEmpName.InnerText = dt.Rows[0]["EmpName"].ToString();
                lblDepartment.InnerText = dt.Rows[0]["DepartmentName"].ToString();
                lblDateofRequest.InnerText = dt.Rows[0]["DateofRequest"].ToString();
                lblEmpID.InnerText = dt.Rows[0]["EmpID"].ToString();
                ViewState["Status"] = Convert.ToInt32(dt.Rows[0]["ApproveStatus"].ToString());
                decimal T_ACost = Convert.ToDecimal(dt.Rows[0]["T_ACost"].ToString());
                decimal MS_ACost = Convert.ToDecimal(dt.Rows[0]["MS_ACost"].ToString());
                decimal AF_ACost = Convert.ToDecimal(dt.Rows[0]["AF_ACost"].ToString());
                decimal ES_ACost = Convert.ToDecimal(dt.Rows[0]["ES_ACost"].ToString());
                btnSubmit.Visible = false;
                btnReset.Visible = false;
                btnAddVisitorClientName.Visible = false;
                div_FacilitiesComments.Visible = true;
                if (ViewState["isEditMode"].ToString() == "1")
                {
                    if (ViewState["Status"].ToString() == "3")
                    {
                        btnApprove.Visible = false;
                        btnRevert.Visible = false;
                        btnReject.Visible = false;
                    }
                    if (ViewState["vwFacilitiesApprove"].ToString() != "0")
                    {
                        if (ViewState["Status"].ToString() == "1")
                        {
                            btnApprove.Visible = true;
                            btnRevert.Visible = true;
                            btnReject.Visible = true;
                            VisitorFacilitiesDisableControls(2);
                        }
                        btnUpdate.Visible = false;
                        div_FacilitiesComments.Visible = true;
                    }
                    if (ViewState["vwFacilitiesListModify"].ToString() != "0")
                    {
                        if (ViewState["Status"].ToString() == "2")
                        {
                            btnUpdate.Visible = true;
                        }
                        else
                        {
                            btnUpdate.Visible = false;
                            div_FacilitiesComments.Visible = false;
                        }
                    }

                    if (ViewState["vwFacilitiesView"].ToString() != "0")
                    {
                        btnUpdate.Visible = false;
                    }
                    UpdatePanel1.Update();
                }
                else if (ViewState["isEditMode"].ToString() == "0")
                {
                    VisitorFacilitiesDisableControls(0);
                    btnApprove.Visible = false;
                    btnRevert.Visible = false;
                    btnReject.Visible = false;
                    btnUpdate.Visible = false;
                    btnSubmit.Visible = false;
                    btnReset.Visible = false;
                    btnNavFacilityCreate.Visible = true;
                    if (ViewState["vwFacilitiesListModify"].ToString() != "0")
                    {
                        if (ViewState["Status"].ToString() == "3")
                        {
                            if (T_ACost > 0 && MS_ACost > 0 && AF_ACost > 0 && ES_ACost > 0)
                            {
                                btnUpdate.Visible = false;
                                VisitorFacilitiesDisableControls(0);
                            }
                            else
                            {
                                VisitorFacilitiesDisableControls(1);
                                btnUpdate.Visible = true;
                            }
                        }
                    }
                    UpdatePanel1.Update();
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "showalert", "OpenModalVisitorFacilityEdit();", true);
            }
        }

        protected void btnNavFacilityCreate_Click(object sender, EventArgs e)
        {
            LoadClientNameMaster();
            btnSubmit.Visible = true;
            btnReset.Visible = true;
            btnApprove.Visible = false;
            btnRevert.Visible = false;
            btnReject.Visible = false;
            btnUpdate.Visible = false;
            VisitorFacilities_EnableControls();
            div_FacilitiesComments.Visible = false;
            btnAddVisitorClientName.Visible = true;
            GetEmpDataToVisitorFacilities();
            DateofRequest();
            UpdatePanel1.Update();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "showalert", " OpenModelVisitorFacility();", true);
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            hdnVisitorFacilityCustom.Value = "Approve";
            HelpClass.custAlertMsg(this, this.GetType(), "Do you want to approve", "confirm");
        }

        protected void btnRevert_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtFacilitycomments.Text.Trim()))
            {
                HelpClass.custAlertMsg(this, this.GetType(), "Please enter revert reason in comments.", "error");
                return;
            }
            hdnVisitorFacilityCustom.Value = "Revert";
            HelpClass.custAlertMsg(this, this.GetType(), "Do you want to revert.", "confirm");
        }
        protected void btnReject_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtFacilitycomments.Text.Trim()))
            {
                HelpClass.custAlertMsg(this, this.GetType(), "Please enter reject reason in comments.", "error");
                return;
            }
            hdnVisitorFacilityCustom.Value = "Reject";
            HelpClass.custAlertMsg(this, this.GetType(), "Do you want to reject.", "confirm");
        }

        private void LoadClientNameMaster()
        {
            try
            {
                objVisitorFacilitiesBAL = new VisitorFacilitiesBAL();
                DataTable dtClientNames = objVisitorFacilitiesBAL.LoadVisitorClientNameMaster();
                ddlClientName.DataTextField = "ClientName";
                ddlClientName.DataValueField = "ClientID";
                ddlClientName.DataSource = dtClientNames;
                ddlClientName.DataBind();
                ddlClientName.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVF5:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVF5:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVF5:" + strline + " " + strMsg, "error");
            }
        }

        protected void btnAddVisitorClientName_Click1(object sender, EventArgs e)
        {
            if (btnAddVisitorClientName.Text == "+")
            {
                btnAddVisitorClientName.Text = "<<";
                txtVisitorName.Visible = true;
                ddlClientName.Visible = false;
            }
            else if (btnAddVisitorClientName.Text == "<<")
            {
                btnAddVisitorClientName.Text = "+";
                txtVisitorName.Visible = false;
                ddlClientName.Visible = true;
            }
        }

        protected void btnVisitorFacilityHistory_Click(object sender, EventArgs e)
        {
            try
            {
                VisitorFacilitiesBO objVisitorFacilitiesBO = new VisitorFacilitiesBO();
                objVisitorFacilitiesBO.FacilityRequestID = Convert.ToInt32(hdnPkVisitorFacilityEditID.Value);                
                VisitorFacilitiesBAL objVisitorFacilitiesBAL = new VisitorFacilitiesBAL();
                DataTable dtHistory = objVisitorFacilitiesBAL.GetVisitorFacilitiesGridHistory(objVisitorFacilitiesBO);

                gv_CommentHistory.DataSource = dtHistory;
                gv_CommentHistory.DataBind();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myCommentHistory').modal({ show: 'true', backdrop: 'static', keyboard: false });", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVF6:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVF6:" + strline + " " + strMsg, "error");
            }
        }

        void screenAuthentication()
        {
            DataTable dtAthuntication = GmsHelpClass.GetRolePrivileges();
            if (dtAthuntication.Rows.Count > 0)
            {
                //modid=5,PrivilegeID=20,22,21
                int FacilitiesCreate = dtAthuntication.AsEnumerable()
            .Count(row => row.Field<int>("PrivilegeID") == 20);
                int FacilitiesListModify = dtAthuntication.AsEnumerable()
            .Count(row => row.Field<int>("PrivilegeID") == 22);
                ViewState["vwFacilitiesListModify"] = FacilitiesListModify.ToString();
                int FacilitiesApprove = dtAthuntication.AsEnumerable()
            .Count(row => row.Field<int>("PrivilegeID") == 21);
                ViewState["vwFacilitiesApprove"] = FacilitiesApprove.ToString();
                int FacilitiesView = dtAthuntication.AsEnumerable()
            .Count(row => row.Field<int>("PrivilegeID") == 28);
                ViewState["vwFacilitiesView"] = FacilitiesView.ToString();
                if (FacilitiesCreate == 0 && FacilitiesListModify == 0 && FacilitiesApprove == 0 && FacilitiesView == 0)
                    Response.Redirect("~/UserLogin.aspx");
                if (FacilitiesCreate > 0)
                {
                    btnNavFacilityCreate.Visible = true;
                }
                else
                {
                    btnNavFacilityCreate.Visible = false;
                }
                if (FacilitiesApprove > 0)
                {
                    btnApprove.Visible = true;
                    btnRevert.Visible = true;
                    btnReject.Visible = true;
                }
                else
                {
                    btnApprove.Visible = false;
                    btnRevert.Visible = false;
                    btnReject.Visible = false;
                }
                if (FacilitiesListModify > 0)
                {
                    btnUpdate.Visible = true;
                }
                else
                {
                    btnUpdate.Visible = false;
                }
            }
        }

        protected void txtVisitorName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", " getDate();", true);
                if (txtVisitorName.Visible == true)
                {
                    if (string.IsNullOrEmpty(txtVisitorName.Text.Trim()))
                    {
                        HelpClass.custAlertMsg(btnAddVisitorClientName, btnAddVisitorClientName.GetType(), "Please enter visitor/client name.", "error");
                        return;
                    }
                }
                
                int Count = objVisitorFacilitiesBAL.VisitorClientNameifExistsorNot(txtVisitorName.Text.Trim());
                if (Count > 0)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Visitor/Client name already exists.", "error");
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVF7:" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("VMSVF7:" + strline + " " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "VMSVF7:" + strline + " " + strMsg, "error");
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }


    }
}