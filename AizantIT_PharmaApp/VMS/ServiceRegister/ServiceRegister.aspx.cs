﻿using System;
using System.Web.UI.WebControls;
using System.Data;
using VMS_BAL;
using VMS_BO;
using AizantIT_PharmaApp.Common;
using System.Data.SqlClient;

namespace AizantIT_PharmaApp.VMS.ServiceRegister
{
    public partial class ServiceRegister : System.Web.UI.Page
    {
        DataTable dt;
        ServiceRegisterBAL objServiceRegisterBAL = new ServiceRegisterBAL();
        ServiceAndVechileBO objServiceAndVechileBO;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            ViewState["EmpID"] = "0";
                            if (HelpClass.IsUserAuthenticated())
                            {
                                ViewState["EmpID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            }
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaff7:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaff7:" + strline + " " + strMsg);
            }
        }

        private void InitializeThePage()
        {
            try
            {
                LoadServiceType();
                TimeDate();
                LoadDepartment();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSINWLPL:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSINWLPL:" + strline + " " + strMsg);
            }
        }

        private void TimeDate()
        {
            txtServiceInDateTime.Text = DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
        }

        private void LoadServiceType()
        {
            try
            {
                dt = new DataTable();
                dt = objServiceRegisterBAL.BindDataServiceType();
                ddlServiceType.DataTextField = dt.Columns["ServiceType"].ToString();
                ddlServiceType.DataValueField = dt.Columns["ServiceTypeID"].ToString();
                ddlServiceType.DataSource = dt;
                ddlServiceType.DataBind();
                ddlServiceType.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSR1:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSR1:" + strline + " " + strMsg);
            }
        }

            //Service Register Insertion Code
            protected void btnServiceSubmit_Click(object sender, EventArgs e)
            {
            try
            {
                if (txtServicePersonname.Text.Trim() == "" || txtServiceLastName.Text.Trim() == "" || txtServiceMobileNo.Text.Trim() == "" || txtServicePurpose.Text.Trim() == "" || ddlDept.SelectedValue == "0" || ddlWhomtoVisit.SelectedValue == "0")
                {
                    HelpClass.showMsg(btnServiceSubmit, btnServiceSubmit.GetType(), "All fields represented with * are mandatory");
                    return;
                }

                if (txtServiceType.Visible == true)
                {
                    if (string.IsNullOrEmpty(txtServiceType.Text.Trim()))
                    {
                        HelpClass.showMsg(btnServiceSubmit, btnServiceSubmit.GetType(), "Please Enter Service Type");
                        return;
                    }
                }
                if (ddlServiceType.Visible == true)
                {
                    if (ddlServiceType.SelectedIndex == 0)
                    {
                        HelpClass.showMsg(btnServiceSubmit, btnServiceSubmit.GetType(), "Please Select Service Type");
                        return;
                    }
                }
                if (txtServiceVendorName.Visible == true)
                {
                    if (string.IsNullOrEmpty(txtServiceVendorName.Text.Trim()))
                    {
                        HelpClass.showMsg(btnServiceSubmit, btnServiceSubmit.GetType(), "Please Enter Vendor Name");
                        return;
                    }
                }

                string ServiceType;
                if (txtServiceType.Visible == true)
                {
                    objServiceAndVechileBO = new ServiceAndVechileBO();
                    objServiceAndVechileBO.ServiceType = txtServiceType.Text.Trim();
                    ServiceType = objServiceRegisterBAL.ServiceTypeMasterInsert(objServiceAndVechileBO).ToString();
                    if (ServiceType == "0")
                    {
                        lblMsg.Text = "Service Type Already Exists";
                        return;
                    }
                    else
                    {
                        lblMsg.Text = "";
                    }
                }
                else
                {
                    ServiceType = ddlServiceType.SelectedValue;
                }
                objServiceAndVechileBO = new ServiceAndVechileBO();
                objServiceAndVechileBO.ServiceType = ServiceType;
                objServiceAndVechileBO.FirstName = txtServicePersonname.Text.Trim();
                objServiceAndVechileBO.LastName = txtServiceLastName.Text.Trim();
                objServiceAndVechileBO.MobileNo = txtServiceMobileNo.Text.Trim();
                objServiceAndVechileBO.CommingFrom = txtServiceVendorName.Text.Trim()==""?"NA": txtServiceVendorName.Text.Trim();
                objServiceAndVechileBO.VechileNo = txtServiceVechileNo.Text.Trim();
                objServiceAndVechileBO.Purpose_of_Visit = txtServicePurpose.Text.Trim();
                objServiceAndVechileBO.Whom_To_Visit =Convert.ToInt32(ddlWhomtoVisit.SelectedValue.Trim());
                objServiceAndVechileBO.Remarks = txtServiceRemarks.Text.Trim();
                objServiceAndVechileBO.InDateTime = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                objServiceAndVechileBO.CreatedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                objServiceAndVechileBO.CreatedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                int c = objServiceRegisterBAL.ServiceRegister_Insert(objServiceAndVechileBO);
                if (c > 0)
                {
                    ServiceRegister_Reset();
                    ddlServiceType.SelectedIndex = 0;
                    HelpClass.showMsg(btnServiceSubmit, btnServiceSubmit.GetType(), "Service Register Details Submitted Successfully");
                }
                else
                {
                    HelpClass.showMsg(btnServiceSubmit, btnServiceSubmit.GetType(), "Service Register Details Submission Failed");
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSR2:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSR2:" + strline + " " + strMsg);
            } 
        }

        protected void ddlServiceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ServiceRegister_Reset();
            if (ddlServiceType.SelectedIndex == 1)
            {
                DivVendorName.Visible = true;
            }
            else
            {
                DivVendorName.Visible = false;
            }
        }

        protected void btnServiceReset_Click(object sender, EventArgs e)
        {
            ddlServiceType.SelectedIndex = 0;
            ServiceRegister_Reset();
        }

        //Service Register Reset Code
        private void ServiceRegister_Reset()
        {
            txtServicePersonname.Text =txtServiceLastName.Text= txtServiceType.Text= txtServiceMobileNo.Text = txtServiceVendorName.Text = txtServiceVechileNo.Text = txtServicePurpose.Text = txtServiceRemarks.Text = string.Empty;
            //ddlServiceType.SelectedIndex = 0;
            ddlDept.SelectedIndex = 0;
            ddlWhomtoVisit.Items.Clear();
            lblMsg.Text = "";
            if (btnServiceType.Text == "<<")
            {
                btnServiceType.Text = "+";
                ddlServiceType.Visible = true;
                txtServiceType.Visible = false;
            }
            TimeDate();
             DivVendorName.Visible = false;
        }

        protected void btnServiceType_Click(object sender, EventArgs e)
        {
            if (btnServiceType.Text == "+")
            {
                ServiceRegister_Reset();
                btnServiceType.Text = "<<";
                txtServiceType.Visible = true;
                ddlServiceType.Visible = false;
                DivVendorName.Visible = false;
                ddlServiceType.SelectedIndex = 0;
            }
            else if (btnServiceType.Text == "<<")
            {
                ServiceRegister_Reset();
                btnServiceType.Text = "+";
                txtServiceType.Visible = false;
                ddlServiceType.Visible = true;
                if (ddlServiceType.SelectedIndex == 1)
                {
                    DivVendorName.Visible = true;
                }
            }
        }

        protected void ddlDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            Employee();
        }

        private void LoadDepartment()
        {
            try
            {
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                dt = new DataTable();
                dt = objVisitorRegisterBAL.LoadDepartment();
                ddlDept.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlDept.DataValueField = dt.Columns["DeptID"].ToString();
                ddlDept.DataSource = dt;
                ddlDept.DataBind();
                ddlDept.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSR3:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSR3:" + strline + " " + strMsg);
            }
        }

        private void Employee()
        {
            try
            {
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                int DeptID = Convert.ToInt32(ddlDept.SelectedValue);
                DataTable dtEmp = objVisitorRegisterBAL.GetEmpData(DeptID);
                ddlWhomtoVisit.DataSource = dtEmp;
                ddlWhomtoVisit.DataTextField = "Name";
                ddlWhomtoVisit.DataValueField = "EmpID";
                ddlWhomtoVisit.DataBind();
                ddlWhomtoVisit.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSR4:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSR4:" + strline + " " + strMsg);
            }
        }

        protected void txtServiceType_TextChanged(object sender, EventArgs e)
        {
            if (txtServiceType.Visible == true)
            {
                if (string.IsNullOrEmpty(txtServiceType.Text.Trim()))
                {
                    HelpClass.showMsg(btnServiceSubmit, btnServiceSubmit.GetType(), "Please Enter Service Type");
                    return;
                }
            }
            int Count= objServiceRegisterBAL.ServiceTypeifExistsorNot(txtServiceType.Text.Trim());
            if(Count > 0)
            {
                lblMsg.Text = "Service Type Already Exists";
            }
            else
            {
                lblMsg.Text = "";
            }
        }
    }
}