﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/VMS_Master.Master" AutoEventWireup="true" CodeBehind="ServiceRegister.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.ServiceRegister.ServiceRegister" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="margin-top: 10px;">
        <div class="row Panel_Frame" style="width: 900px; margin-left: 17%">
            <div id="header">
                <div class="col-sm-12 Panel_Header">
                    <div class="col-sm-6" style="padding-left: 5px">
                        <span class="Panel_Title">Service Register Creation</span>
                    </div>
                </div>
            </div>
            <div id="body">
              <%--  <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
                <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                    <ContentTemplate>
                        <div class="Row">
                            <div class="col-sm-6">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="form-horizontal" style="margin: 5px;">
                                            <div class="form-group" runat="server" id="div5">
                                                <label class="control-label col-sm-4" id="lblService" style="text-align: left; font-weight: lighter">Service Type<span class="mandatoryStar">*</span></label><asp:Button ID="btnServiceType" runat="server" CssClass="btnnVisit" Text="+" Font-Bold="true" OnClick="btnServiceType_Click" />
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtServiceType" runat="server" CssClass="form-control" placeholder="Enter Service Type" Visible="false" AutoPostBack="true" OnTextChanged="txtServiceType_TextChanged"></asp:TextBox>
                                                    <asp:DropDownList ID="ddlServiceType" runat="server" CssClass="form-control SearchDropDown" AutoPostBack="true" OnSelectedIndexChanged="ddlServiceType_SelectedIndexChanged"></asp:DropDownList>
                                                    <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Font-Size="Smaller"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlServiceType" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>

                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div1">
                                        <label class="control-label col-sm-4" id="lblFirstName" style="text-align: left; font-weight: lighter">First Name<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtServicePersonname" runat="server" CssClass="form-control" placeholder="Enter First Name" onkeypress="return ValidateAlpha(event)" onpaste="return false" maxlength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div2">
                                        <label class="control-label col-sm-4" id="lblLastName" style="text-align: left; font-weight: lighter">Last Name<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtServiceLastName" runat="server" CssClass="form-control" placeholder="Enter Last Name" onkeypress="return ValidateAlpha(event)" onpaste="return false" maxlength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div3">
                                        <label class="control-label col-sm-4" id="lblMobileNo" style="text-align: left; font-weight: lighter">Mobile No<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtServiceMobileNo" runat="server" CssClass="form-control" placeholder="Enter Mobile No" MaxLength="10" onkeypress="return ValidatePhoneNo(event);"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div4">
                                        <label class="control-label col-sm-4" id="lbl" style="text-align: left; font-weight: lighter">Purpose of Visit<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtServicePurpose" runat="server" CssClass="form-control" placeholder="Enter Purpose of Visit" onkeypress="return event.keyCode != 13;"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">

                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="DivVendorName" visible="false">
                                        <label class="control-label col-sm-4" id="lblVendor" style="text-align: left; font-weight: lighter">Vendor Name<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtServiceVendorName" runat="server" CssClass="form-control" placeholder="Enter Vendor Name"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="form-horizontal" style="margin: 5px;">
                                            <div class="form-group" runat="server" id="div28">
                                                <label class="control-label col-sm-4" id="lblDepartment" style="text-align: left; font-weight: lighter">Department<span class="mandatoryStar">*</span></label>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="ddlDept" runat="server" CssClass="form-control SearchDropDown" AutoPostBack="true" OnSelectedIndexChanged="ddlDept_SelectedIndexChanged"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlDept" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>

                                <asp:UpdatePanel runat="server" UpdateMode="Always">
                                    <ContentTemplate>
                                        <div class="form-horizontal" style="margin: 5px;">
                                            <div class="form-group" runat="server" id="div12">
                                                <label class="control-label col-sm-4" id="lblWhomToVisit" style="text-align: left; font-weight: lighter">Whom To Visit<span class="mandatoryStar">*</span></label>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="ddlWhomtoVisit" runat="server" CssClass="form-control SearchDropDown" AutoPostBack="true"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlWhomtoVisit" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>

                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div8">
                                        <label class="control-label col-sm-4" id="lblVechileNo" style="text-align: left; font-weight: lighter">Vechile No</label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtServiceVechileNo" runat="server" CssClass="form-control" placeholder="Enter Vechile No" onkeypress="return event.keyCode != 13;"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div9">
                                        <label class="control-label col-sm-4" id="lblInDateTime" style="text-align: left; font-weight: lighter">InDateTime</label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtServiceInDateTime" runat="server" BackColor="White" CssClass="form-control" ReadOnly="true" onkeypress="return event.keyCode != 13;"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div10">
                                        <label class="control-label col-sm-4" id="lblRemarks" style="text-align: left; font-weight: lighter">Remarks</label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtServiceRemarks" runat="server" CssClass="form-control" placeholder="Enter Remarks" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                            <asp:Button ID="btnServiceSubmit" Text="Submit" CssClass="button" runat="server" OnClick="btnServiceSubmit_Click" OnClientClick="javascript:return Submitvaidate();" />
                            <asp:Button ID="btnServiceReset" Text="Reset" CssClass="button" runat="server" OnClick="btnServiceReset_Click" CausesValidation="false" />
                        </div>
                        
                        <script type="text/javascript">
                            function Submitvaidate() {
                                var SType = document.getElementById("<%=ddlServiceType.ClientID%>").value;
                                var FName = document.getElementById("<%=txtServicePersonname.ClientID%>").value;
                                var SPurpose = document.getElementById("<%=txtServicePurpose.ClientID%>").value;
                                var LName = document.getElementById("<%=txtServiceLastName.ClientID%>").value;
                                var WhomToVisit = document.getElementById("<%=ddlWhomtoVisit.ClientID%>").value;
                                var Dept = document.getElementById("<%=ddlDept.ClientID%>").value;
                                var Mobile = document.getElementById("<%=txtServiceMobileNo.ClientID%>").value;
                               
                                errors = [];
                                
                                    if (SType == 0) {
                                        errors.push("Please Select Service Type.");
                                    }
                                    if (SType == 1) {
                                        var VendorName = document.getElementById("<%=txtServiceVendorName.ClientID%>").value;
                                    if (VendorName == "") {
                                        errors.push("Please Enter Vendor Name.");
                                    }
                                    }
                                    if (FName.trim() == "") {
                                        errors.push("Please Enter FirstName.");
                                    }
                                    if (LName.trim() == "") {
                                        errors.push("Please Enter LastName.");
                                    }
                                    if (Mobile == "") {
                                        errors.push("Please Enter Mobile No.");
                                    }
                                    else
                                    {
                                        if (Mobile.length != 10)
                                        {
                                            errors.push("Mobile No Should be 10 Digits");
                                        }
                                    }
                                    
                                    if (SPurpose.trim() == "") {
                                        errors.push("Please Enter Purpose of Visit.");
                                    }
                                    if (Dept == 0) {
                                        errors.push("Please Select Department.");
                                    }
                                    if (WhomToVisit == 0) {
                                        errors.push("Please Select Whom to Visit.");
                                    }
                                    //if (VendorName == 0) {
                                    //    errors.push("Please Enter Vendor Name.");
                                    //}

                                    if (errors.length > 0) {
                                        alert(errors.join("\n"));
                                        return false;
                                    }
                                }
                            
                        </script>
                        <script>
                            $(document).ready(function () {
                                $("#ddlDept").select2();
                                $("#ddlWhomtoVisit").select2();
                                $("#ddlServiceType").select2();

                            });
                            var prm = Sys.WebForms.PageRequestManager.getInstance();
                            prm.add_endRequest(function () {
                                $(document).ready(function () {
                                    $("#ddlDept").select2();
                                    $("#ddlWhomtoVisit").select2();
                                    $("#ddlServiceType").select2();
                                });
                            });
                        </script>

                        <script>
                            $("#NavLnkService").attr("class", "active");
                            $("#ServiceRegister").attr("class", "active");
    </script>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
