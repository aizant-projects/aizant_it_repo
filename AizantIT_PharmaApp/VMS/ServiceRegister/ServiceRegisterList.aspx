﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/VMS_Master/VMS_Master.Master" AutoEventWireup="true" CodeBehind="ServiceRegisterList.aspx.cs" Inherits="AizantIT_PharmaApp.VMS.ServiceRegister.ServiceRegisterList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="margin-top: 10px;">
        <div class="row Panel_Frame" style="width: 1100px; margin-left: 10%">
            <div id="header">
                <div class="col-sm-12 Panel_Header">
                    <div class="col-sm-6" style="padding-left: 5px">
                        <span class="Panel_Title">Service Register List</span>
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div>
           <%-- <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
            <%--<asp:UpdatePanel ID="UpdatePanel9" runat="server" UpdateMode="Conditional">
                 <ContentTemplate>--%>

            <div class="Row">
                <div class="col-sm-6">

                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div5">
                            <label class="control-label col-sm-4" id="lblServiceType" style="text-align: left; font-weight: lighter">Service Type</label>
                            <div class="col-sm-6">
                                <asp:DropDownList ID="ddlServiceSearch" runat="server" CssClass="form-control SearchDropDown"></asp:DropDownList>
                            </div>
                        </div>
                    </div>

                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div1">
                            <label class="control-label col-sm-4" id="lblFromDate" style="text-align: left; font-weight: lighter">From Date</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtServiceDateFrom" runat="server" CssClass="form-control" placeholder="Select From Date"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-sm-6">

                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div2">
                            <label class="control-label col-sm-4" id="lblNameofthePerson" style="text-align: left; font-weight: lighter">Name of the Person</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtServiceSearchName" runat="server" CssClass="form-control" placeholder="Enter Name"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="form-horizontal" style="margin: 5px;">
                        <div class="form-group" runat="server" id="div3">
                            <label class="control-label col-sm-4" id="lblToDate" style="text-align: left; font-weight: lighter">To Date</label>
                            <div class="col-sm-6">
                                <asp:TextBox ID="txtServiceDateTo" runat="server" CssClass="form-control" placeholder="Select To Date"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                <asp:Button ID="btnServiceFind" Text="Find" CssClass="button" runat="server" OnClick="btnServiceFind_Click" ValidationGroup="ab" />
                <asp:Button ID="btnServiceSearchReset" Text="Reset" CssClass="button" runat="server" OnClick="btnServiceSearchReset_Click" CausesValidation="false" />
            </div>

            <div class="clearfix">
            </div>

            <asp:GridView ID="GridViewServiceRegister" runat="server" DataKeyNames="SNo" PagerStyle-CssClass="GridPager" OnRowDataBound="GridViewServiceRegister_RowDataBound" CssClass="table table-bordered table-inverse" Style="text-align: center" HorizontalAlign="Center" Font-Size="12px" HeaderStyle-Wrap="false" AutoGenerateColumns="False" HeaderStyle-Width="5%" EmptyDataText="Currently No Records Found" CellPadding="3" HeaderStyle-HorizontalAlign="Center" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" AllowPaging="True" ForeColor="#66CCFF" OnPageIndexChanging="GridViewServiceRegister_PageIndexChanging">
                <EmptyDataRowStyle ForeColor="#FF3300" HorizontalAlign="Center" />
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <HeaderStyle BackColor="#748CB2" Font-Bold="True" ForeColor="White" Height="30px" />
                <PagerStyle ForeColor="White" HorizontalAlign="Center" BackColor="White" />
                <RowStyle ForeColor="#000066" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#0c99f0" />
                <SortedAscendingHeaderStyle BackColor="#0c99f0" />
                <SortedDescendingCellStyle BackColor="#0c99f0" />
                <SortedDescendingHeaderStyle BackColor="#0c99f0" />
                <Columns>
                    <asp:TemplateField HeaderText="SNO" SortExpression="SNo">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVSno" runat="server" Text='<%# Bind("SNo") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGVServiceSno" runat="server" Text='<%# Bind("SNo") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="SERVICE TYPE " SortExpression="ServiceType">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVServiceType" runat="server" Text='<%# Bind("ServiceType") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGVServiceType" runat="server" Text='<%# Bind("ServiceType") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="PERSON NAME" SortExpression="PersonName">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVPersonName" runat="server" Text='<%# Bind("PersonName") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGVPersonName" runat="server" Text='<%# Bind("PersonName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="MOBILE NO" SortExpression="MobileNO">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVMobileNO" runat="server" Text='<%# Bind("MobileNo") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGVMobileNO" runat="server" Text='<%# Bind("MobileNo") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="VENDOR NAME" SortExpression="CommingFrom">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVCommingFrom" runat="server" Text='<%# Bind("CommingFrom") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGVCommingFrom" runat="server" Text='<%# Bind("CommingFrom") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="VECHILE NO" SortExpression="VechileNo">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVVechileNo" runat="server" Text='<%# Bind("VechileNo") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGVechileNo" runat="server" Text='<%# Bind("VechileNo") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="PURPOSE OF VISIT" SortExpression="PurposeofVisit">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVPurposeofVisit" runat="server" Text='<%# Bind("PurposeofVisit") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGVPurposeofVisit" runat="server" Text='<%# Bind("PurposeofVisit") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="WHOM OF VISIT" SortExpression="WhomToVisit">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVWhomToVisit" runat="server" Text='<%# Bind("WhomToVisit") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGVWhomToVisit" runat="server" Text='<%# Bind("WhomToVisit") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="IN DATE TIME" SortExpression="InDateTime">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVInDateTime" runat="server" Text='<%# Bind("InDateTime") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGVInDateTime" runat="server" Text='<%# Bind("InDateTime") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="OUT DATE TIME" SortExpression="OutDateTime">
                        <ItemTemplate>
                            <asp:Label ID="lblGVOutDateTime" runat="server" Text='<%# Bind("OutDateTime") %>'></asp:Label>
                            <asp:LinkButton ID="lnkCheckOut" runat="server" OnClick="lnkCheckOut_Click" ForeColor="#ff0000" Visible="false">CheckOut</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="EDIT">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkServiceEdit" runat="server" CssClass="glyphicon glyphicon-edit GV_Edit_Icon" OnClick="lnkServiceEdit_Click"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

            <asp:ModalPopupExtender ID="ModalPopupServiceRegister" runat="server" TargetControlID="btnServiceUpdate" PopupControlID="panelService" RepositionMode="RepositionOnWindowResizeAndScroll" DropShadow="true" PopupDragHandleControlID="panelAddNewTitle" BackgroundCssClass="modalBackground"></asp:ModalPopupExtender>
            <asp:Panel ID="panelService" runat="server" Style="display: none; background-color: white;" ForeColor="Black" Width="900px" Height="440px">
                <asp:Panel ID="panel3" runat="server" Style="cursor: move; font-family: Tahoma; padding: 8px;" HorizontalAlign="Center" BackColor="#748CB2" Font-Bold="true" ForeColor="White" Height="35px">
                    <b>Modify Service CheckIn</b>
                </asp:Panel>

                <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                    <ContentTemplate>
                        <div class="Row">
                            <div class="col-sm-6">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>

                                        <div class="form-horizontal" style="margin: 5px;">
                                            <div class="form-group" runat="server" id="div20">
                                                <label class="control-label col-sm-4" id="lblSno" style="text-align: left; font-weight: lighter">SNo</label>
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtServiceEditSno" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-horizontal" style="margin: 5px;">
                                            <div class="form-group" runat="server" id="div4">
                                                <label class="control-label col-sm-4" id="lblService" style="text-align: left; font-weight: lighter">Service Type<span class="mandatoryStar">*</span></label><asp:Button ID="btnServiceType" runat="server" CssClass="btnnVisit" Text="+" Font-Bold="true" OnClick="btnServiceType_Click" Visible="false" />
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtServiceType" runat="server" CssClass="form-control" placeholder="Enter Service Type" Visible="false"></asp:TextBox>
                                                    <asp:DropDownList ID="ddlServiceEditServiceType" runat="server" ClientIDMode="Static" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlServiceEditServiceType_SelectedIndexChanged1"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Select Service Type" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlServiceEditServiceType" InitialValue="0" ValidationGroup="Service"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlServiceEditServiceType" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>

                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div6">
                                        <label class="control-label col-sm-4" id="lblFirstName" style="text-align: left; font-weight: lighter">First Name<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtServiceEditPersonName" runat="server" CssClass="form-control" placeholder="Enter First Name" onkeypress="return ValidateAlpha(event)" onpaste="return false" maxlength="50"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Enter First Name" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtServiceEditPersonName" ValidationGroup="Service"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div7">
                                        <label class="control-label col-sm-4" id="lblLastName" style="text-align: left; font-weight: lighter">Last Name<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtServiceLastName" runat="server" CssClass="form-control" placeholder="Enter Last Name" onkeypress="return ValidateAlpha(event)" onpaste="return false" maxlength="50"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Enter Last Name" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtServiceLastName" ValidationGroup="Service"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div8">
                                        <label class="control-label col-sm-4" id="lblMobileNo" style="text-align: left; font-weight: lighter">Mobile No<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtServiceMobileNo" runat="server" CssClass="form-control" placeholder="Enter Mobile No" MaxLength="10" onkeypress="return ValidatePhoneNo(event);"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Enter Mobile No" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtServiceMobileNo" ValidationGroup="Service"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorMobile" runat="server" ErrorMessage="Mobile No Should be 10 Digits" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtServiceMobileNo" ValidationGroup="Service" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div9">
                                        <label class="control-label col-sm-4" id="lblServicePurpose" style="text-align: left; font-weight: lighter">Purpose of Visit<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtServicePurpose" runat="server" CssClass="form-control" placeholder="Enter Purpose of Visit"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Enter Purpose of Visit" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtServicePurpose" ValidationGroup="Service"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="DivVendorName" visible="false">
                                        <label class="control-label col-sm-4" id="lblVendor" style="text-align: left; font-weight: lighter">Vendor Name<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtServiceVendorName" runat="server" CssClass="form-control" placeholder="Enter Vendor Name"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Enter Vendor Name" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtServiceVendorName" ValidationGroup="Service"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>

                                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="form-horizontal" style="margin: 5px;">
                                            <div class="form-group" runat="server" id="div28">
                                                <label class="control-label col-sm-4" id="lblDepartment" style="text-align: left; font-weight: lighter">Department<span class="mandatoryStar">*</span></label>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="ddlDept" ClientIDMode="Static" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlDept_SelectedIndexChanged"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Select Department" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlDept" InitialValue="0" ValidationGroup="Service"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlDept" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>

                                <asp:UpdatePanel runat="server" UpdateMode="Always">
                                    <ContentTemplate>
                                        <div class="form-horizontal" style="margin: 5px;">
                                            <div class="form-group" runat="server" id="div12">
                                                <label class="control-label col-sm-4" id="lblWhomToVisit" style="text-align: left; font-weight: lighter">Whom To Visit<span class="mandatoryStar">*</span></label>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="ddlWhomtoVisit" runat="server" ClientIDMode="Static" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Select Whom to Visit" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="ddlWhomtoVisit" InitialValue="0" ValidationGroup="Service"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlWhomtoVisit" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>

                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div11">
                                        <label class="control-label col-sm-4" id="lblVechileNo" style="text-align: left; font-weight: lighter">Vechile No</label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtServiceVechileNo" runat="server" CssClass="form-control" placeholder="Enter Vechile No"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div13">
                                        <label class="control-label col-sm-4" id="lblInDateTime" style="text-align: left; font-weight: lighter">InDateTime</label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtServiceInDateTime" runat="server" BackColor="White" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div14">
                                        <label class="control-label col-sm-4" id="lblRemarks" style="text-align: left; font-weight: lighter">Remarks</label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtServiceRemarks" runat="server" CssClass="form-control" placeholder="Enter Remarks" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-horizontal" style="margin: 5px;">
                                    <div class="form-group" runat="server" id="div21">
                                        <label class="control-label col-sm-4" id="lblComments" style="text-align: left; font-weight: lighter">Comments<span class="mandatoryStar">*</span></label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtServiceEditedComments" runat="server" CssClass="form-control" placeholder="Enter Comments" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Enter Comments" Display="Dynamic" Font-Size="Smaller" SetFocusOnError="true" ForeColor="Red" ControlToValidate="txtServiceEditedComments" ValidationGroup="Service"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <div class="col-sm-12 modal-footer" style="text-align: center; padding-bottom: 2px; padding-top: 2px; margin-top: 10px">
                    <asp:Button ID="btnServiceUpdate" runat="server" Text="Update" CssClass="button" OnClick="btnServiceUpdate_Click" UseSubmitBehavior="false" ValidationGroup="Service"/>
                    <asp:Button ID="btnServiceCancel" runat="server" Text="Cancel" CssClass="button" CausesValidation="false"/>
                </div>
            </asp:Panel>
            <%--</ContentTemplate>
</asp:UpdatePanel>--%>
        </div>
    </div>
    
    <script>
        $("#NavLnkService").attr("class", "active");
        $("#ServiceRegisterList").attr("class", "active");
    </script>
   <%-- <script>
        $(function () {
            $('#<%=txtServiceDateFrom.ClientID%>').datetimepicker({
                  format: 'DD MMM YYYY',
              });
              $('#<%=txtServiceDateTo.ClientID%>').datetimepicker({
                  format: 'DD MMM YYYY',
              });
        });
    </script>--%>

      <script>
          //for start date and end date validations startdate should be < enddate
          $(function () {
              $('#<%=txtServiceDateFrom.ClientID%>').datetimepicker({
                         format: 'DD MMM YYYY',
                         useCurrent: false,
                     });
                     var startdate = document.getElementById('<%=txtServiceDateFrom.ClientID%>').value
                     $('#<%=txtServiceDateTo.ClientID%>').datetimepicker({
                          format: 'DD MMM YYYY',
                          minDate: startdate,
                          useCurrent: false,
                      });
                      $('#<%=txtServiceDateFrom.ClientID%>').on("dp.change", function (e) {
                          $('#<%=txtServiceDateTo.ClientID%>').data("DateTimePicker").minDate(e.date);
               <%--  $('#<%=txtEndDate.ClientID%>').value = $('#<%=txtStartDate.ClientID%>').value; --%>
                      $('#<%=txtServiceDateTo.ClientID%>').val("");
                      });
                      $('#<%=txtServiceDateTo.ClientID%>').on("dp.change", function (e) {
                 });
          });
    </script>

      <script type="text/javascript">
                 //for datepickers
                 var startdate2 = document.getElementById('<%=txtServiceDateFrom.ClientID%>').value
                 if (startdate2 == "") {
                     $('#<%=txtServiceDateFrom.ClientID%>').datetimepicker({
                          format: 'DD MMM YYYY',
                          useCurrent: false,
                      });
                      $('#<%=txtServiceDateTo.ClientID%>').datetimepicker({
                          format: 'DD MMM YYYY',
                          useCurrent: false,
                      });
                  }
    </script>
</asp:Content>
