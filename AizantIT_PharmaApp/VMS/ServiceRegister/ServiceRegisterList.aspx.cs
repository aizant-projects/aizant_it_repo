﻿using System;
using System.Web.UI.WebControls;
using System.Data;
using VMS_BAL;
using VMS_BO;
using AizantIT_PharmaApp.Common;
using System.Data.SqlClient;

namespace AizantIT_PharmaApp.VMS.ServiceRegister
{
    public partial class ServiceRegisterList : System.Web.UI.Page
    {
        ServiceAndVechileBO objServiceAndVechileBO;
        ServiceRegisterBAL objServiceRegisterBAL = new ServiceRegisterBAL();
        DataTable dt;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (HelpClass.IsUserAuthenticated())
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=6");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            ViewState["EmpID"] = "0";
                            if (HelpClass.IsUserAuthenticated())
                            {
                                ViewState["EmpID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            }
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaff7:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSStaff7:" + strline + " " + strMsg);
            }
        }

        private void InitializeThePage()
        {
            try
            {
                LoadServiceType();
                LoadDepartment();
                Employee();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSINWLPL:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSINWLPL:" + strline + " " + strMsg);
            }
        }
        private void LoadServiceType()
        {
            try
            {
                dt = new DataTable();
                dt = objServiceRegisterBAL.BindDataServiceType();
                ddlServiceEditServiceType.DataTextField = dt.Columns["ServiceType"].ToString();
                ddlServiceEditServiceType.DataValueField = dt.Columns["ServiceTypeID"].ToString();
                ddlServiceEditServiceType.DataSource = dt;
                ddlServiceEditServiceType.DataBind();
                ddlServiceEditServiceType.Items.Insert(0, new ListItem("--Select--", "0"));

                ddlServiceSearch.DataTextField = dt.Columns["ServiceType"].ToString();
                ddlServiceSearch.DataValueField = dt.Columns["ServiceTypeID"].ToString();
                ddlServiceSearch.DataSource = dt;
                ddlServiceSearch.DataBind();
                ddlServiceSearch.Items.Insert(0, new ListItem("All", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSRL1:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSRL1:" + strline + " " + strMsg);
            }
        }


        //Service Register Search/Find Code
        protected void btnServiceFind_Click(object sender, EventArgs e)
        {
            try
            {
                objServiceAndVechileBO = new ServiceAndVechileBO();
                objServiceAndVechileBO.ServiceType =ddlServiceSearch.SelectedValue.Trim();
                objServiceAndVechileBO.FirstName = txtServiceSearchName.Text.Trim();
                if (!string.IsNullOrEmpty(txtServiceDateFrom.Text.Trim()))
                {
                    objServiceAndVechileBO.InDateTime = Convert.ToDateTime(txtServiceDateFrom.Text.Trim()).ToString("yyyyMMdd");
                }
                else
                {
                    objServiceAndVechileBO.InDateTime = txtServiceDateFrom.Text.Trim();
                }
                if (!string.IsNullOrEmpty(txtServiceDateTo.Text.Trim()))
                {
                    objServiceAndVechileBO.OutDateTime = Convert.ToDateTime(txtServiceDateTo.Text.Trim()).ToString("yyyyMMdd");
                }
                else
                {
                    objServiceAndVechileBO.OutDateTime = txtServiceDateTo.Text.Trim();
                }
                dt = new DataTable();
                dt = objServiceRegisterBAL.SearchFilterService(objServiceAndVechileBO);
                GridViewServiceRegister.DataSource = dt;
                GridViewServiceRegister.DataBind();
                ViewState["dtservice"] = dt;
                GridViewServiceRegister.Visible = true;
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSRL2:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSRL2:" + strline + " " + strMsg);
            }
        }

        protected void btnServiceSearchReset_Click(object sender, EventArgs e)
        {
            txtServiceSearchName.Text = txtServiceDateFrom.Text = txtServiceDateTo.Text = string.Empty;
            ddlServiceSearch.SelectedIndex = 0;
            GridViewServiceRegister.Visible = false;
        }

        //Service Register Link Button Edit Code
        protected void lnkServiceEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Employee();
                objServiceAndVechileBO = new ServiceAndVechileBO();
                LinkButton lnk = sender as LinkButton;
                GridViewRow gr = (GridViewRow)lnk.NamingContainer;
                string tempID = GridViewServiceRegister.DataKeys[gr.RowIndex].Value.ToString();
                objServiceAndVechileBO.SNo = Convert.ToInt32(tempID);
                ViewState["tempId1"] = tempID;
                dt = new DataTable();
                dt = objServiceRegisterBAL.ServiceRegisterlinkbuttonFilldata(objServiceAndVechileBO);
                txtServiceEditSno.Text = dt.Rows[0]["SNo"].ToString();
                ddlServiceEditServiceType.SelectedValue = dt.Rows[0]["ServiceTypeID"].ToString();
                txtServiceEditPersonName.Text = dt.Rows[0]["FirstName"].ToString();
                txtServiceLastName.Text = dt.Rows[0]["LastName"].ToString();
                txtServiceMobileNo.Text = dt.Rows[0]["MobileNo"].ToString();
                DisplayVendor();
                txtServiceVendorName.Text = dt.Rows[0]["CommingFrom"].ToString();
                txtServiceVechileNo.Text = dt.Rows[0]["VechileNo"].ToString();
                txtServicePurpose.Text = dt.Rows[0]["PurposeofVisit"].ToString();
                txtServiceRemarks.Text = dt.Rows[0]["Remarks"].ToString();
                txtServiceInDateTime.Text = dt.Rows[0]["InDateTime"].ToString();
                txtServiceEditedComments.Text = dt.Rows[0]["LastChangedByComments"].ToString();
                ddlDept.SelectedValue = dt.Rows[0]["DeptID"].ToString();
                Employee();
                ddlWhomtoVisit.SelectedValue = dt.Rows[0]["EmpID"].ToString();
                ModalPopupServiceRegister.Show();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSRL3:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSRL3:" + strline + " " + strMsg);
            } 
        }

        //Service Register BindData Code
        private void ServiceRegisterBind_Data()
        {
            try
            {
                dt = new DataTable();
                dt = objServiceRegisterBAL.BindDataServiceRegister();
                GridViewServiceRegister.DataSource = dt;
                GridViewServiceRegister.DataBind();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSRL4:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSRL4:" + strline + " " + strMsg);
            }
        }

        //Service Register Update Code
        protected void btnServiceUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                objServiceAndVechileBO = new ServiceAndVechileBO();
                objServiceAndVechileBO.SNo = Convert.ToInt32(txtServiceEditSno.Text.Trim());
                objServiceAndVechileBO.ServiceType = ddlServiceEditServiceType.SelectedValue.Trim();
                objServiceAndVechileBO.FirstName =txtServiceEditPersonName.Text.Trim();
                objServiceAndVechileBO.LastName = txtServiceLastName.Text.Trim();
                objServiceAndVechileBO.MobileNo = txtServiceMobileNo.Text.Trim();
                objServiceAndVechileBO.CommingFrom = txtServiceVendorName.Text.Trim();//Vendor Name 
                objServiceAndVechileBO.VechileNo = txtServiceVechileNo.Text.Trim();
                objServiceAndVechileBO.Purpose_of_Visit = txtServicePurpose.Text.Trim();
                objServiceAndVechileBO.Whom_To_Visit = Convert.ToInt32(ddlWhomtoVisit.SelectedValue.Trim());
                objServiceAndVechileBO.LastChangedBy = Convert.ToInt32(ViewState["EmpID"].ToString());
                objServiceAndVechileBO.LastChangedDate = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                objServiceAndVechileBO.LastChangedComments = txtServiceEditedComments.Text.Trim();
                objServiceAndVechileBO.Remarks = txtServiceRemarks.Text.Trim();
                //objServiceAndVechileBO.InDateTime = txtServiceInDateTime.Text.Trim();
                int c = objServiceRegisterBAL.Update_ServiceRegisterEdit(objServiceAndVechileBO);
                if (c > 0)
                {
                    HelpClass.showMsg(btnServiceUpdate, btnServiceUpdate.GetType(), "Service Register Details Updated Successfully");
                }
                else
                {
                    HelpClass.showMsg(btnServiceUpdate, btnServiceUpdate.GetType(), "Service Register Details Updation Failed");
                }
                ServiceRegisterBind_Data();
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSRL5:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSRL5:" + strline + " " + strMsg);
            } 
        }

        protected void btnServiceType_Click(object sender, EventArgs e)
        {
            if (btnServiceType.Text == "+")
            {
                btnServiceType.Text = "<<";
                txtServiceType.Visible = true;
                ddlServiceEditServiceType.Visible = false;
            }
            else if (btnServiceType.Text == "<<")
            {
                btnServiceType.Text = "+";
                txtServiceType.Visible = false;
                ddlServiceEditServiceType.Visible = true;
            }
        }


        protected void lnkCheckOut_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton bt = (LinkButton)sender;
                GridViewRow gr = (GridViewRow)bt.NamingContainer;
                Label lbl_id = (Label)gr.FindControl("lblGVServiceSno");
                LinkButton lnk = (LinkButton)gr.FindControl("lnkCheckOut");
                Label lbl_Name = (Label)gr.FindControl("lblGVPersonName");
                objServiceAndVechileBO = new ServiceAndVechileBO();
                objServiceAndVechileBO.SNo = Convert.ToInt32(lbl_id.Text);
                objServiceAndVechileBO.FirstName = lbl_Name.Text;
                objServiceAndVechileBO.OutDateTime = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                int c = objServiceRegisterBAL.CheckoutService(objServiceAndVechileBO);
                if (c > 0)
                {
                    HelpClass.showMsg(this, this.GetType(), "  " + lbl_Name.Text + "   Checked Out Successfully");
                    ServiceRegisterBind_Data();
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSRL6:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSRL6:" + strline + " " + strMsg);
            }
        }

        protected void GridViewServiceRegister_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIntime = (Label)(e.Row.FindControl("lblGVOutDateTime"));
                    LinkButton lbtnCheckin = (LinkButton)(e.Row.FindControl("lnkCheckOut"));
                    if (lblIntime != null)
                    {
                        if (lblIntime.Text.Length > 6)
                            lblIntime.Visible = true;
                        else
                            lbtnCheckin.Visible = true;
                    }
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblOuttime = (Label)(e.Row.FindControl("lblGVOutDateTime"));
                    LinkButton lnkbtnEdit = (LinkButton)(e.Row.FindControl("lnkServiceEdit"));
                    if (lblOuttime != null)
                    {
                        if (lblOuttime.Text.Length > 5)
                        {
                            lnkbtnEdit.Visible = false;
                        }
                        else
                        {
                            lnkbtnEdit.Visible = true;
                        }
                    }
                }
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSRL9:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSRL9:" + strline + " " + strMsg);
            }
        }

        private void Employee()
        {
            try
            {
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                int DeptID = Convert.ToInt32(ddlDept.SelectedValue);
                DataTable dtEmp = objVisitorRegisterBAL.GetEmpData(DeptID);
                ddlWhomtoVisit.DataSource = dtEmp;
                ddlWhomtoVisit.DataTextField = "Name";
                ddlWhomtoVisit.DataValueField = "EmpID";
                ddlWhomtoVisit.DataBind();
                ddlWhomtoVisit.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSRL7:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSRL7:" + strline + " " + strMsg);
            }
        }

        protected void ddlDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            Employee();
        }

        private void LoadDepartment()
        {
            try
            {
                VisitorRegisterBAL objVisitorRegisterBAL = new VisitorRegisterBAL();
                dt = new DataTable();
                dt = objVisitorRegisterBAL.LoadDepartment();
                ddlDept.DataTextField = dt.Columns["DepartmentName"].ToString();
                ddlDept.DataValueField = dt.Columns["DeptID"].ToString();
                ddlDept.DataSource = dt;
                ddlDept.DataBind();
                ddlDept.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException sqe)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSRL8:" + strmsgF);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "VMSSRL8:" + strline + " " + strMsg);
            }
        }

        protected void ddlServiceEditServiceType_SelectedIndexChanged1(object sender, EventArgs e)
        {
            DisplayVendor();
        }

        private void DisplayVendor()
        {
            if (ddlServiceEditServiceType.SelectedIndex == 1)
            {
                DivVendorName.Visible = true;
            }
            else
            {
                DivVendorName.Visible = false;
            }
        }

        protected void GridViewServiceRegister_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewServiceRegister.PageIndex = e.NewPageIndex;
            if (ViewState["dtservice"]!= null)
            {
                GridViewServiceRegister.DataSource = (DataTable)ViewState["dtservice"];
                GridViewServiceRegister.DataBind();
            }
        }
    }
}