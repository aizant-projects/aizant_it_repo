﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace AizantIT_PharmaApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.ashx/{*pathInfo}");

            //routes.MapRoute(
            //     "QmsDefault", // Route name
            //     "QMS/{controller}/{action}/{id}", // URL with parameters
            //     new { area = "QMS", controller = "Home", action = "Index", id = UrlParameter.Optional }, // Parameter defaults
            //     null,
            //     new[] { "AizantIT_PharmaApp.Areas.QMS.Controllers" }
            // ).DataTokens.Add("area", "QMS");

            routes.MapRoute(
              name: "Default",
              url: "{controller}/{action}/{id}",
              defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
              namespaces: new[] { "AizantIT_PharmaApp.Controllers" }
            );            
        }
    }
}
