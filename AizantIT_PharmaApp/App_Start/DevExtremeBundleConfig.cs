﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
namespace AizantIT_PharmaApp
{
    public class DevExtremeBundleConfig
    {
        // -- Added by prabhakar 01-Nov-2018 -- 
        public static void RegisterBundles(BundleCollection bundles)
        {
            var scriptBundle_0 = new ScriptBundle("~/Scripts/DevExtremeBundleScripts_0");
            var scriptBundle_1 = new ScriptBundle("~/Scripts/DevExtremeBundleScripts_1");
            var scriptBundle_2 = new ScriptBundle("~/Scripts/DevExtremeBundleScripts_2");
            var styleBundle = new StyleBundle("~/Content/DevExtremeBundleStyles");
            // CLDR scripts
            scriptBundle_0
                .Include("~/DevExtremeScripts/cldr.js")
                .Include("~/DevExtremeScripts/cldr/event.js")
                .Include("~/DevExtremeScripts/cldr/supplemental.js")
                .Include("~/DevExtremeScripts/cldr/unresolved.js");
            // Globalize 1.x
            scriptBundle_1
                .Include("~/DevExtremeScripts/globalize.js")
                .Include("~/DevExtremeScripts/globalize/message.js")
                .Include("~/DevExtremeScripts/globalize/number.js")
                .Include("~/DevExtremeScripts/globalize/currency.js")
                .Include("~/DevExtremeScripts/globalize/date.js");
            // NOTE: jQuery may already be included in the default script bundle. Check the BundleConfig.cs file​​​
            // scriptBundle
            //    .Include("~/Scripts/jquery-1.10.2.js");
            // JSZip for client-side exporting
            // scriptBundle
            //    .Include("~/Scripts/jszip.js");
            // DevExtreme + extensions
            scriptBundle_2
                .Include("~/DevExtremeScripts/dx.all.js")
                .Include("~/DevExtremeScripts/aspnet/dx.aspnet.data.js")
                .Include("~/DevExtremeScripts/aspnet/dx.aspnet.mvc.js");
            // VectorMap data
            // scriptBundle
            //    .Include("~/Scripts/vectormap-data/africa.js")
            //    .Include("~/Scripts/vectormap-data/canada.js")
            //    .Include("~/Scripts/vectormap-data/eurasia.js")
            //    .Include("~/Scripts/vectormap-data/europe.js")
            //    .Include("~/Scripts/vectormap-data/usa.js")
            //    .Include("~/Scripts/vectormap-data/world.js");
            // DevExtreme themes              
            styleBundle
                .Include("~/DevExtremeContent/dx.common.css")
                .Include("~/DevExtremeContent/dx.light.css");
            bundles.Add(styleBundle);
            bundles.Add(scriptBundle_0);
            bundles.Add(scriptBundle_1);
            bundles.Add(scriptBundle_2);
          
#if !DEBUG
            BundleTable.EnableOptimizations = true;
#endif
        }
    }
}