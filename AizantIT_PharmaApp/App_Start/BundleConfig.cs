﻿using System.Web;
using System.Web.Optimization;

namespace AizantIT_PharmaApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Clear();
            //bundles.ResetAll();
            BundleTable.EnableOptimizations = true;

            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.validate*"));

            //// Use the development version of Modernizr to develop with and learn from. Then, when you're
            //// ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js",
            //          "~/Scripts/respond.js"));

            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //          "~/Content/bootstrap.css",
            //          "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Bundles/PredefinedStyles").Include(
                "~/AppCSS/Bootstrap/css/bootstrap4.css",
                "~/Content/bootstrap-datetimepicker.min.css",
                "~/Content/font-awesome.min.css",
                "~/Content/DataTables/css/dataTables.bootstrap4.min.css",          
                "~/Content/bootstrap-select.min.css"));
            //User Defined Main Styles
            bundles.Add(new StyleBundle("~/Bundles/UserdefinedStyles").Include(
                "~/AppCSS/QMS/css/QMS_Style.css",
                "~/AppCSS/CustAlerts/SingleCustAlert.css",
                "~/AppCSS/Common/Progress.css",
                "~/AppCSS/Common/Global.css"));

            bundles.Add(new ScriptBundle("~/Bundles/PredefinedScripts").Include(
                "~/Scripts/popper.js",
                "~/Scripts/jquery-3.2.1.min.js",
                "~/AppCSS/Bootstrap/scripts/bootstrap4.min.js",
                "~/Scripts/moment.min.js",
                "~/Scripts/bootstrap-datetimepicker.js",
                "~/Scripts/bootstrap-select.min.js",
                "~/Scripts/datatables.js",
                "~/Scripts/DataTables/dataTables.bootstrap4.min.js"                   
             ));
            bundles.Add(new ScriptBundle("~/Bundles/UserdefinedScripts").Include(
                "~/AppScripts/GlobalScripts.js",
                "~/AppScripts/CookieFunc.min.js",
                "~/AppScripts/LogOutFunc.js",
                "~/AppScripts/GetNotification.min.js"
                ));

            // Angular bundles

            bundles.Add(new Bundle("~/Bundles/Angular")
              .Include(
                //"~/Scripts/qms-dashboard-angular-build/inline.*",
                "~/Scripts/qms-dashboard-angular/runtime.*",
                "~/Scripts/qms-dashboard-angular/polyfills.*",
                "~/Scripts/qms-dashboard-angular/styles.*",
                "~/Scripts/qms-dashboard-angular/vendor.*",
                "~/Scripts/qms-dashboard-angular/main.*",
                "~/Scripts/qms-dashboard-angular/scripts.*"
                ));

            //"~/AppScripts/GetNotification.min.js"
            /*"~/AppScripts/Notification.min.js"*/
            //"~/AppScripts/GetNotification.js"
            //bundles.Add(new StyleBundle("~/Bundles/PMSStyles").Include(            
            //    // "~/AppCSS/PMS/datatables.css",
            //    // "~/AppCSS/PMS/productstylesheet.css",    
            //    //   "~/AppCSS/PMS/theme.css",
            //    //    "~/AppCSS/PMS/fileinput.css", 
            //   //     "~/AppCSS/PMS/font-awesome.min.css"

            //   ));

            //bundles.Add(new ScriptBundle("~/Bundles/PMSScripts").Include(
            //   //  "~/AppScripts/GlobalScripts.js",
            //  //  "~/AppScripts/PMS/jquery.min.js",
            //  //  "~/AppScripts/PMS/popper.min.js",    
            //  //  "~/AppScripts/PMS/fileinput.js",
            // //   "~/AppScripts/PMS/theme.js"      

            //    ));
        }
    }
}
