﻿using AizantIT_PharmaApp.Common;
using AizantIT_PharmaApp.UserControls.Exam;
using System;
using System.Data;
using System.Web.UI;
using TMS_BusinessLayer;

namespace AizantIT_PharmaApp.UserControls.TMS_UserControls
{
    public partial class ucJrTrainedRecords : System.Web.UI.UserControl
    {
        TMS_BAL objTMS_Bal;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnViewAnswerSheet_Click1(object sender, EventArgs e)
        {
            try
            {
                int TrainerID = Convert.ToInt32(hdnTrainerID.Value);
                if (TrainerID != 0)
                {
                    bool IsEvaluation = Convert.ToBoolean(hdnIsEvaluation.Value);
                    ucExamResultSheet.showExamResult(Convert.ToInt32(hdnJR_ID.Value), 1, Convert.ToInt32(hdnDocID.Value), IsEvaluation,
                        TrainerID);
                }
                else
                {
                    ucExamResultSheet.showExamResult(Convert.ToInt32(hdnJR_ID.Value), 2, Convert.ToInt32(hdnDocID.Value), true, 0);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ucTR-1:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnEvaluateOralPractical_Click(object sender, EventArgs e)
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                string Opinion;
                string Remarks = "";
                int EvaluateResult = 0;
                Opinion = rbtnSatisfactory.Checked == true ? "S": "NS";
                 Remarks = txtEvaluationComments.Text.Trim()==""?"N/A": txtEvaluationComments.Text.Trim();

                objTMS_Bal.EvaluateJrTrainee(Convert.ToInt32(hdnDocID.Value),Convert.ToInt32(hdnJR_ID.Value), Convert.ToInt32(hdnTrainerID.Value),out EvaluateResult,
                                                Opinion, Remarks);
                if (EvaluateResult == 1)
                {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "showalert", "custAlertMsg('Evaluated Trainee Record','success',ReloadTrainedRecords());", true);
                }
                else if (EvaluateResult == 2)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", "custAlertMsg('Already Evaluated','info');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", "custAlertMsg('System Error Occured','error');", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ucTR-2:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnGetTrainerOpinion_Click(object sender, EventArgs e)
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                DataTable dt = objTMS_Bal.GetTrainerOpinionBal(hdnDocID.Value,hdnJR_ID.Value);
                if (dt.Rows.Count>0)
                {
                    if (dt.Rows[0]["Opinion"].ToString() == "S")
                    {
                        lblRadioSatisfactory.Attributes.Add("class", "float-left btn btn-primary_radio col-6 col-lg-6 col-md-6 col-sm-6 active radio_check");
                        lblRadioNotSatisfactory.Attributes.Add("class", "float-left btn btn-primary_radio col-6 col-lg-6 col-md-6 col-sm-6 radio_check");
                    }
                    else
                    {
                        lblRadioSatisfactory.Attributes.Add("class", "float-left btn btn-primary_radio col-6 col-lg-6 col-md-6 col-sm-6 radio_check");
                        lblRadioNotSatisfactory.Attributes.Add("class", "float-left btn btn-primary_radio col-6 col-lg-6 col-md-6 col-sm-6 active radio_check");
                    }
                    txtViewEvaluationComments.Text = dt.Rows[0]["Remarks"].ToString();
                    upEvaluationOpinion.Update();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ucTR-3:" + strline + "  " + strMsg, "error");
            }
        }
    }
}