﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JR_TrainingFile.ascx.cs" Inherits="AizantIT_PharmaApp.UserControls.TMS_UserControls.JR_TrainingFile" %>
<style>
    .embed-responsive-16by9::before {
        padding-top: 34.25% !important;
    }

    .embed-responsive {
        min-height: 688px !important;
        max-height: 688px !important;
    }
</style>
<input type="hidden" value="0" id="hdnJr_ID" />
<div class="modal fade department" data-backdrop="static" id="modelTrainingFile" role="dialog">
    <div class="modal-dialog modal-lg" style="min-width: 98%;">
        <!-- Modal content-->
        <div class="modal-content" style="border-radius: 0px">
            <div class="modal-header">
                <h4 class="modal-title Panel_Title">Training File</h4>
                <button type="button" class="close " data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body tmsModalBody" style="overflow: visible;">
                <div class="col-12 float-left tmsModalContent">
                    <div class="col-sm-12 float-left">
                        <div class="form-horizontal col-sm-12 float-left padding-none">
                            <div class="form-group col-sm-12 float-left padding-none">
                                <label class="control-label float-left padding-none ">Select Training File :</label>
                                <div class="col-sm-3 float-left ">
                                    <select class="regulatory_dropdown_style  col-12 selectpicker drop_down padding-none" data-live-search="true" data-size="7" id="ddlTypeOfTF" onchange="LoadTrainingFile(this)">
                                        <option value="1">Complete Training File</option>
                                        <option value="2">Job Description</option>
                                        <option value="3">Training Schedule</option>
                                        <option value="4">Retrospective Qualification Form</option>
                                        <option value="5">Training Record</option>
                                        <option value="6">Training Questionnaire</option>
                                        <option value="7">Training Certificate</option>
                                    </select>
                                </div>
                                <div id="divPrint" runat="server" visible="false">
                                    <button type="button" id="btnPrintTF" class="btn-signup_popup" title="Print" onclick="PrintTrainingFile();">Print</button>
                                    <button type="button" id="btnPrintTFLog" class="btn-revert_popup" title="click here to view print log" data-target="#modelTrainingLogFile" data-toggle="modal">Log</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="embed-responsive embed-responsive-16by9" id="dvAttachmentFrame">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Training File Log Pop-up -->
<div class="modal fade department" id="modelTrainingLogFile" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content" style="border-radius: 0px">
            <div class="modal-header">
                <h4 class="modal-title Panel_Title">Print Log File</h4>
                <button type="button" class="close" data-target="#modelTrainingLogFile" data-togle="modal" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" style="overflow: visible;">
                <table id="tblJR_TrainingLogFile" class="datatable_cust  display breakword" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Log_ID</th>
                            <th>Print By</th>
                            <th>Print Date</th>
                            <th>Training File Type</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>


<script>
    var tblJR_TrainingLogFile;
    function TrainingLogFile() {
        if (tblJR_TrainingLogFile != null) {
            tblJR_TrainingLogFile.destroy();
        }
        tblJR_TrainingLogFile = $('#tblJR_TrainingLogFile').DataTable({
            columns: [
                { 'data': 'Log_ID' },
                { 'data': 'PrintBy' },
                { 'data': 'PrintDate' },
                { 'data': 'TF_Type' },
            ],
            "order": [[0, "desc"]],
            "aoColumnDefs": [{ "targets": [0], "visible": false }],
            "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/EmpJrListService.asmx/PrintLogTrainingFileList")%>",
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "Jr_ID", "value": $('#hdnJr_ID').val() })
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
    }
    $('#modelTrainingLogFile').on('shown.bs.modal', function () {
        TrainingLogFile();
    });
</script>

<script>
    var JR_ID = "0";
    var IsFile_Exists = 0;
    function GetIsTF_Exists(JRID) {
        $('#hdnJr_ID').val(JRID);
        JR_ID = JRID;
        UserSessionCheck();
        var objStr = { "JRID": JRID };
        var jsonData = JSON.stringify(objStr);
        $.ajax({
            type: "POST",
            url: '<%= Page.ResolveUrl("~/TMS/WebServices/EmpJrListService.asmx/GetIsJrTrainingFileExists")%>',
            data: jsonData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var json = jQuery.parseJSON(response.d);
                IsFile_Exists = json["iTotalRecords"];
                GetTFPaths();
            },
            failure: function (response) {
                // custAlertMsg('Internal Error Occured on Adding Questions to FAQ', 'error');
            },
            error: function (response) {
                custAlertMsg('Internal Error Occured ', 'error');
            }
        });
    }
</script>


<script>
    var JR_ID = "0";
    function viewEmpTF_Details(JRID) {
        $('#hdnJr_ID').val(JRID);
        JR_ID = JRID;
        GetIsTF_Exists(JRID);
    }
    function GetTFPaths() {
        if (IsFile_Exists == 1) {
            ShowPleaseWait('show');
        } else {
            ShowPleaseWaitImg('show');
        }
        UserSessionCheck();
        var objStr = { "JRID": $('#hdnJr_ID').val() };
        var jsonData = JSON.stringify(objStr);
        $.ajax({
            type: "POST",
            url: '<%= Page.ResolveUrl("~/TMS/WebServices/EmpJrListService.asmx/GetJrTrainingFilePaths")%>',
            data: jsonData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var json = jQuery.parseJSON(response.d);
                var IsFile_Exists = json["iTotalRecords"];
                if (IsFile_Exists == 1) {
                    ShowPleaseWait('hide');
                } else {
                    ShowPleaseWaitImg('hide');
                }
                OpenModelTrainingFile(response.d);
            },
            failure: function (response) {
                // custAlertMsg('Internal Error Occured on Adding Questions to FAQ', 'error');
            },
            error: function (response) {
                custAlertMsg('Internal Error Occured ', 'error');
            }
        });
    }
</script>

<script>  
    var TrainingFilePDF_URL = "";
    var URLCompleteTFPath1 = "";
    var URLJobDescription2 = "";
    var URLTrainingSchedule3 = "";
    var URLRetrospectiveQualificationForm4 = "";
    var URLTrainingRecord5 = "";
    var URLTrainingQuestionnaire6 = "";
    var URLTrainingCertificate7 = "";

    function OpenModelTrainingFile(ResponseData) {
        URLCompleteTFPath1 = "";
        URLJobDescription2 = "";
        URLTrainingSchedule3 = "";
        URLRetrospectiveQualificationForm4 = "";
        URLTrainingRecord5 = "";
        URLTrainingQuestionnaire6 = "";
        URLTrainingCertificate7 = "";
        $('#modelTrainingFile').modal({ show: true, backdrop: 'static', keyboard: false });
        var DataFilePath = JSON.parse(ResponseData);
        $(DataFilePath.dtFilePathData).each(function () {
            if (this.Type == "1") {
                URLCompleteTFPath1 = this.TrainingFilePath;
            }
            else if (this.Type == "2") {
                URLJobDescription2 = this.TrainingFilePath;
            }
            else if (this.Type == "3") {
                URLTrainingSchedule3 = this.TrainingFilePath;
            }
            else if (this.Type == "4") {
                URLRetrospectiveQualificationForm4 = this.TrainingFilePath;
            }
            else if (this.Type == "5") {
                URLTrainingRecord5 = this.TrainingFilePath;
            }
            else if (this.Type == "6") {
                URLTrainingQuestionnaire6 = this.TrainingFilePath;
            }
            else if (this.Type == "7") {
                URLTrainingCertificate7 = this.TrainingFilePath;
            }
        });
        $("#ddlTypeOfTF").val('1');
        $("#ddlTypeOfTF").selectpicker("refresh");
        TrainingFilePDF_URL = URLCompleteTFPath1;
        ViewAttachmentsPhysicalPath(URLCompleteTFPath1);
    }

    function ViewAttachmentsPhysicalPath(FilePathandName) {
        //var dvShow = document.getElementById("dvAttachmentFrame");
        PdfViewerContentLoading(FilePathandName, "#dvAttachmentFrame");
    }

    function LoadTrainingFile(ctrl) {
        var DropdownValue = $(ctrl).val();
        if (DropdownValue == 1) {
            TrainingFilePDF_URL = URLCompleteTFPath1;
        }
        else if (DropdownValue == 2) {
            TrainingFilePDF_URL = URLJobDescription2;
        }
        else if (DropdownValue == 3) {
            TrainingFilePDF_URL = URLTrainingSchedule3;
        } else if (DropdownValue == 4) {
            TrainingFilePDF_URL = URLRetrospectiveQualificationForm4;
        } else if (DropdownValue == 5) {
            TrainingFilePDF_URL = URLTrainingRecord5;
        } else if (DropdownValue == 6) {
            TrainingFilePDF_URL = URLTrainingQuestionnaire6;
        } else if (DropdownValue == 7) {
            TrainingFilePDF_URL = URLTrainingCertificate7;
        }
        if (TrainingFilePDF_URL != "") {
            ViewAttachmentsPhysicalPath(TrainingFilePDF_URL);
        }
        else {
            custAlertMsg($(ctrl).find('option:selected').text() + " doesn't exist", "warning");
            $("#dvAttachmentFrame").empty();
            $("#dvAttachmentFrame").html($(ctrl).find('option:selected').text() + " doesn't exist.");
        }
    }
</script>

<!--To Print TF-->
<script>
    function PrintTrainingFile() {
        if (TrainingFilePDF_URL != "") {
            var objFra = document.createElement('iframe');   //Creating IFrame.
            objFra.style.visibility = "hidden";
            objFra.src = window.location.origin + "/" + TrainingFilePDF_URL;  // source.
            document.body.appendChild(objFra);  // Adding the frame to the web page.
            objFra.contentWindow.focus();       // Set focus.
            objFra.contentWindow.print();
            logPrintClick();
        }
        else {
            custAlertMsg($("#ddlTypeOfTF").find('option:selected').text() + " File doesn't exist to print.", "warning");
        }
    }

    function logPrintClick() {
        var objStr = { "JRID": JR_ID, "TrainingFileType": $('#ddlTypeOfTF').val() };
        var jsonData = JSON.stringify(objStr);
        $.ajax({
            type: "POST",
            url: '<%= Page.ResolveUrl("~/TMS/WebServices/EmpJrListService.asmx/LogTrainingFilePrintBtn")%>',
            data: jsonData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var ResponseResult = JSON.parse(response.d);
                if (ResponseResult.msgType == "success") {
                    //custAlertMsg(ResponseResult.msg, "success");
                }
                else if (ResponseResult.msgType == "error") {
                    custAlertMsg(ResponseResult.msg, "error");
                }
                else if (ResponseResult.msgType == "warning") {
                    custAlertMsg(ResponseResult.msg, "warning");
                }
            },
            failure: function (response) {
                custAlertMsg('Failured for log print', 'error');
            },
            error: function (response) {
                custAlertMsg('Internal Error Occured for log print', 'error');
            }
        });
    }
</script>

<script src="<%=ResolveUrl("~/AppScripts/CommonPDF_Viewer.js")%>"></script>

