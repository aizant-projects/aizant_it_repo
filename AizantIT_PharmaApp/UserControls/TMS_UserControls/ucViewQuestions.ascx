﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucViewQuestions.ascx.cs" Inherits="AizantIT_PharmaApp.UserControls.TMS_UserControls.ucViewQuestions" %>
<asp:HiddenField ID="hdnQuestionID" runat="server" Value="0" />
<asp:HiddenField ID="hdnDocID" runat="server" Value="0" />
<asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
<asp:HiddenField ID="hdnViewerRole" runat="server" Value="0" />
<asp:HiddenField ID="hdnCommentType" runat="server" Value="0" />

<!-------- Create new question popup-------------->
<div id="modalViewQuestions" class="modal department fade" role="dialog">
    <div class="modal-dialog" style="min-width: 98%;">
        <!-- Modal content-->
        <div class="modal-content float-left col-lg-12" style="padding: 0px;">
            <div class="modal-header">
                <h4 class="modal-title">View Question</h4>
                <button type="button" class="close" onclick="ReloadDocumentQuestions();" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body float-left col-lg-12">
                <div class="float-left col-lg-12 padding-none">
                    <ul class="nav nav-tabs tab_grid">
                        <li class=" nav-item"><a data-toggle="tab" class="nav-link active" href="#Answered">Answered</a></li>
                        <li class=" nav-item"><a data-toggle="tab" class="nav-link " href="#UnAnswered">Unanswered</a></li>
                    </ul>
                </div>

                <div class="tab-content">
                    <div id="Answered" class="tab-pane fade show active">
                        <div class="float-left col-lg-12 padding-none">
                            <div class="form-group float-left padding-none" id="divAnswered" style="max-height: 600px; overflow-y: auto;">
                                <div class="panel-group faq_acc" id="accordionTQ_Answered">
                                    <div id="divAnsweredQuestions" class="accordion">
                                        Loading Answered Questions...                                      
                                    </div>
                                </div>
                            </div>
                            <!------conversation popup-->
                            <div class="float-left col-lg-4 padding-none" id="divAnswerdQueConversation" style="display: none;">
                                <div class="coversation_setting float-left col-lg-12 padding-none">
                                    <div class="grid_header header_text_label float-left col-lg-12">Conversation<span class="float-right" onclick="ATQ_CloseConversation();">X</span></div>
                                    <div class="question_header_conversation">Loading Question...?</div>
                                    <div id="divAQ_Conversation">
                                        Loading Conversation...                                
                                    </div>
                                </div>
                                <div class=" padding-none float-left col-lg-12 float-left top">
                                    <textarea class="form-control bottom capitalize" rows="5" id="txtATQ_Comment" runat="server" placeholder="Enter Comment"></textarea>
                                    <asp:UpdatePanel ID="upATQSubmitComment" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Button ID="btnAQ_SubmitComment" CssClass="btn btn-send_popup float-right" runat="server" Text="Submit" OnClientClick="return ValidateSubmitComment('AQ');" OnClick="btnAQ_SubmitComment_Click" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div class="modal-footer float-left col-lg-12" id="divbtnTQ_ToFAQ">
                                <button type="button" class="btn-signup_popup" id="btnSubmitTQ_ToFAQ">Submit</button>
                                <button type="button" class="btn-cancel_popup" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>

                    <div id="UnAnswered" class="tab-pane fade">
                        <div class="float-left col-lg-12 padding-none" style="max-height: 500px; overflow: auto;" id="divUnAnswered">
                            <div id="divUnAnsweredQuestions">
                                Loading UnAnswered Questions...                             
                            </div>
                        </div>
                        <div>
                            <!------conversation popup-->
                            <div class="float-left col-lg-4 padding-none" id="divUnAnswerdQueConversation" style="display: none;">
                                <div class="coversation_setting float-left col-lg-12 padding-none">
                                    <div class="grid_header header_text_label_tms float-left col-lg-12">Conversation<span class="float-right" onclick="UAT_CloseAnswerOrConversationPopup('UAT_Conversation');">X</span></div>
                                    <div class="question_header_conversation">Loading Question...?</div>
                                    <div id="divUAQ_Conversation">
                                        Loading Conversation...
                                    </div>
                                </div>
                                <div class=" padding-none float-left col-lg-12 float-left top">
                                    <textarea class="form-control bottom capitalize" rows="5" id="txtUATQ_Comment" runat="server" placeholder="Enter your Comment"></textarea>
                                    <asp:UpdatePanel ID="upSubmitUAQComment" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Button ID="btnUAQ_CommentSubmit" CssClass="btn btn-send_popup float-right" runat="server" Text="Submit" OnClientClick="return ValidateSubmitComment('UAQ');" OnClick="btnSubmitComment_ServerClick" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <!------answer_popup-->
                            <div class="float-left col-lg-4 padding-none" id="divAnswerToTraineeQuestion" style="display: none;">
                                <div class="coversation_setting float-left col-lg-12 padding-none">
                                    <div class="grid_header header_text_label float-left col-lg-12">Answer<span id="spanAnswerClose" onclick="UAT_CloseAnswerOrConversationPopup('UAT_Answer');" class="float-right">X</span></div>
                                    <div class="question_header_conversation">Loading Question...?</div>
                                </div>
                                <div class=" padding-none float-left col-lg-12 float-left top">
                                    <textarea class="form-control bottom capitalize" rows="5" id="txtTrainerAnswer" runat="server" placeholder="Enter Answer"></textarea>
                                    <button type="button" class=" btn-cancel_popup  float-right" style="margin-left: 5px;" onclick="UAT_CloseAnswerOrConversationPopup('UAT_Answer');">Cancel</button>
                                    <asp:UpdatePanel ID="upsubmitAnswerButton" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Button ID="btnUAQ_AswerSubmit" CssClass=" btn-send_popup float-right" runat="server" Text="Submit" OnClientClick="return ValidateSubmitAnswer();" OnClick="btnSubmitAnswerbyTrainee_Click" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-------- End new question-------------->

<!-------- Previous Answers On TQ PopUp-------------->
<div id="modalTQ_PreviousAnswers" class="modal department fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="titleTQ_PA"></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body" id="divPreviousAnswerMain">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancel_popup" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-------- End pervious question-------------->

<%--<script>
    $(document).ready(function () {
        $(document).on('show.bs.modal', '.modal', function (event) {
            var zIndex = 1040 + (10 * $('.modal:visible').length);
            $(this).css('z-index', zIndex);
            setTimeout(function () {
                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            }, 0);
        });

    });
</script>--%>

<!--Show View Questions PopUp-->
<script>
    function ViewQuestionsByTrainer(DocID, TrainerID) {
        $("#<%=hdnViewerRole.ClientID%>").val('7');
        $("#<%=hdnDocID.ClientID%>").val(DocID);
        $("#<%=hdnEmpID.ClientID%>").val(TrainerID);
        GetTraineeQuestionsForTrainerByAjaxCall();
        openQuestionsPopup();
        SetViewQuestionsDefaultState();
    }

    function ViewAskedQuestionsByTrainee(DocID, TrainerID) {
        $("#divbtnTQ_ToFAQ").hide();
        $("#<%=hdnViewerRole.ClientID%>").val('6');
        $("#<%=hdnDocID.ClientID%>").val(DocID);
        $("#<%=hdnEmpID.ClientID%>").val(TrainerID);
        GetTraineeAskedQuestionsForTraineeByAjaxCall();
        openQuestionsPopup();
        SetViewQuestionsDefaultState();
    }

    function ViewQuestionsByTrainee(BaseType, BaseID, DocID, TraineeID) {
        $("#divbtnTQ_ToFAQ").hide();
        $("#<%=hdnViewerRole.ClientID%>").val('6');
        GetTraineeQuestionsForTraineeByAjaxCall(BaseType, BaseID, DocID, TraineeID);
        openQuestionsPopup();
        SetViewQuestionsDefaultState();
    }
    function openQuestionsPopup() {
        $('#modalViewQuestions').modal({ show: true, backdrop: 'static', keyboard: false });
    }
    function openPreviousAns() {
        $('#modalPreviousAns').modal({ show: true, backdrop: 'static', keyboard: false });
    }
</script>

<!--View Questions For Trainee-->
<script>
    function GetTraineeQuestionsForTraineeByAjaxCall(BaseType, BaseID, DocID, TraineeID) {
        UserSessionCheck();
        var objStr = { "BaseType": BaseType, "BaseID": BaseID, "DocID": DocID, "TraineeID": TraineeID };
        var jsonData = JSON.stringify(objStr);
        $.ajax({
            type: "POST",
            url: '<%= Page.ResolveUrl("~/TMS/WebServices/TraineeQuestionsOnDoc/TraineeQuestionsOnDoc.asmx/getTraineeRaisedQuestionsonDocumentToTrainee")%>',
            data: jsonData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                loadTraineeQuestionsForTrainee(response);
            },
            failure: function (response) {
                // custAlertMsg('Internal Error Occured on Adding Questions to FAQ', 'error');
            },
            error: function (response) {
                // custAlertMsg('Internal Error Occured on Adding Questions to FAQ', 'error');
            }
        });
    }

    //For Trainee
    function loadTraineeQuestionsForTrainee(response) {
        var TraineeAskQuestionsObj = response.d;
        var result = $.parseJSON(TraineeAskQuestionsObj);
        var AnsweredQuestions = result['AnsweredQuestions'];
        var UnAnsweredQuestions = result['UnAnsweredQuestions'];
        //For Answered Questions
        var divAnswered = $("#divAnsweredQuestions div").eq(0).clone(true);
        $("#divAnsweredQuestions div").eq(0).remove();
        $("#divAnsweredQuestions").empty();
        for (var i = 0; i < AnsweredQuestions.length; i++) {
            var divAnsweredQuestionsdata = "<div class='card'><div class='card-title' id='divAQuestion" + AnsweredQuestions[i].QuestionID + "'><div class='title_faq question_header'>" +
                "<span class='collapsed' data-toggle='collapse' href='#collapseTQA_" + AnsweredQuestions[i].QuestionID + "' href='#collapseTQA_" + AnsweredQuestions[i].QuestionID + "' onclick='GetTQ_Answer(" + AnsweredQuestions[i].QuestionID + ");'>" + AnsweredQuestions[i].QuestionDescription + "</span></div>" +
                "<div id='collapseTQA_" + AnsweredQuestions[i].QuestionID + "' class='collapse' data-parent='#divAnsweredQuestions'><div class='card-body'>" +
                "<div class='padding-none float-left float-left col-lg-12'><div class='question_answer bottom float-left float-left col-lg-12'>" +
                "Loading..." +
                "</div><div class='float-left col-lg-12 padding-none top' style='padding-top: 10px; border-bottom: 1px solid #07889a;'><span class='total_comments_view float-left' style='right: -9px !important; margin-top: 0px !important; width: 100%;'>" +
                "Answered By : <span class='count_comments'>" + AnsweredQuestions[i].QuestionAnsweredBy + ", " + AnsweredQuestions[i].QuestionAnsweredDate + "</span>" +
                "<a class='float-right content_conversation' onclick='ATQ_OpenConversation(" + AnsweredQuestions[i].QuestionID + ");'>Conversation</a>" +
                "<a class='float-right content_conversation' style='display: " + AnsweredQuestions[i].IsPreviousQuestionsShow + ";' onclick='AT_OpenPreviousAnswer(" + AnsweredQuestions[i].QuestionID + ");'>Previous Answers</a>" +
                "</div></div></div>";
            $("#divAnsweredQuestions").append(divAnsweredQuestionsdata);
            divUnAnswered = $("#divAnsweredQuestions div").eq(0).clone(true);
        }
        // For Unanswered Questions to trainee
        var divUnAnswered = $("#divUnAnsweredQuestions div").eq(0).clone(true);
        $("#divUnAnsweredQuestions div").eq(0).remove();
        $("#divUnAnsweredQuestions").empty();
        for (var i = 0; i < UnAnsweredQuestions.length; i++) {
            //alert(UnAnsweredQuestions[i].QuestionID + ', ' + UnAnsweredQuestions[i].QuestionDescription + ', ' + UnAnsweredQuestions[i].QuestionRaisedDate);
            var divUnAnsweredQuestionsdata =
                "<div class='col-xs-12' id='divUAQuestion" + UnAnsweredQuestions[i].QuestionID + "'><div class='question_header float-left col-lg-12'><span>" +
                UnAnsweredQuestions[i].QuestionDescription + "</span ></div><div class='float-left col-lg-12 padding-none top' style='border-bottom:1px solid #38b8ba'><span class='total_comments_view float-left' style='width: 100%;right: -9px !important;margin-bottom: 5px; margin-top: 0px !important;'>" +
                "Raised On : <span class='count_comments'>" + UnAnsweredQuestions[i].QuestionRaisedDate + "</span><a class='float-right content_conversation' onclick='UAT_OpenConversation(" + UnAnsweredQuestions[i].QuestionID + ");' >Conversation</a>" +
                //"<a class='comments_count_new  float-right' style='top: -14px !important; right: -9px !important;'>04</a>" +
                //"<a class='float-right content_conversation' onclick='openAnswer("+ UnAnsweredQuestions[i].QuestionID+");'>Answer</a>" +
                "</div></div>";
            $("#divUnAnsweredQuestions").append(divUnAnsweredQuestionsdata);
            divUnAnswered = $("#divUnAnsweredQuestions div").eq(0).clone(true);
        }
        if (UnAnsweredQuestions.length == 0) {
            $("#divUnAnsweredQuestions").prepend("<div class='question_none'>No Unanswered Questions To Show</div>");
        }
        if (AnsweredQuestions.length == 0) {
            $("#divAnsweredQuestions").prepend("<div class='question_none'>No Answered Questions To Show</div>");
        }
        SetViewQuestionsDefaultState();
    }
</script>

<!--View Questions For Trainer-->
<script>
    function GetTraineeQuestionsForTrainerByAjaxCall() {
        UserSessionCheck();
        var DocID = $("#<%=hdnDocID.ClientID%>")[0].value;
        var TrainerID = $("#<%=hdnEmpID.ClientID%>")[0].value;
        var objStr = { "TrainerID": TrainerID, "DocID": DocID };
        var jsonData = JSON.stringify(objStr);
        $.ajax({
            type: "POST",
            url: '<%= Page.ResolveUrl("~/TMS/WebServices/TraineeQuestionsOnDoc/TraineeQuestionsOnDoc.asmx/getTraineeRaisedQuestionsonDocumentToTrainer")%>',
            data: jsonData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                loadTraineeQuestionsForTrainer(response);
            },
            failure: function (response) {
                // custAlertMsg('Internal Error Occured on Adding Questions to FAQ', 'error');
            },
            error: function (response) {
                // custAlertMsg('Internal Error Occured on Adding Questions to FAQ', 'error');
            }
        });
    }

    function GetTraineeAskedQuestionsForTraineeByAjaxCall() {
        UserSessionCheck();
        var DocID = $("#<%=hdnDocID.ClientID%>")[0].value;
        var TraineeID = $("#<%=hdnEmpID.ClientID%>")[0].value;
        var objStr = { "TraineeID": TraineeID, "DocID": DocID };
        var jsonData = JSON.stringify(objStr);
        $.ajax({
            type: "POST",
            url: '<%= Page.ResolveUrl("~/TMS/WebServices/TrainerAnswersOnDoc/TrainerAnswersOnDoc.asmx/GetTraineeAskedQuestionsOnDocumentToTrainee")%>',
            data: jsonData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                loadTraineeQuestionsForTrainee(response);
            },
            failure: function (response) {
                // custAlertMsg('Internal Error Occured on Adding Questions to FAQ', 'error');
            },
            error: function (response) {
                // custAlertMsg('Internal Error Occured on Adding Questions to FAQ', 'error');
            }
        });
    }

    // For Trainer
    function loadTraineeQuestionsForTrainer(response) {
        var TraineeAskQuestionsObj = response.d;
        var result = $.parseJSON(TraineeAskQuestionsObj);
        var AnsweredQuestions = result['AnsweredQuestions'];
        var UnAnsweredQuestions = result['UnAnsweredQuestions'];
        //For Answered Questions
        var divAnswered = $("#divAnsweredQuestions div").eq(0).clone(true);
        $("#divAnsweredQuestions div").eq(0).remove();
        $("#divAnsweredQuestions").empty();
        var QuestionsCount = 0;
        var MarkAsFaqShown = false;
        for (var i = 0; i < AnsweredQuestions.length; i++) {
            QuestionsCount++;
            if (AnsweredQuestions[i].IsQuestionMarkedAsFAQ == "block") {
                MarkAsFaqShown = true;
            }
            var divAnsweredQuestionsdata = "<div class='card'><div class='card-title ' id='divAQuestion" + AnsweredQuestions[i].QuestionID + "'><div class='title_faq question_header'>" +
                "<span class='collapsed' data-toggle='collapse'  href='#collapseTQA_" + AnsweredQuestions[i].QuestionID + "' onclick='GetTQ_Answer(" + AnsweredQuestions[i].QuestionID + ");'>" + AnsweredQuestions[i].QuestionDescription + "</span>" +
                "<label class='float-right' style='display: " + AnsweredQuestions[i].IsQuestionMarkedAsFAQ + ";'>" +
                "<input type='checkbox' class='cbMarkedFAQs' value='" + AnsweredQuestions[i].QuestionID + "'><span class='checkmark'></span> Make as FAQ</label></div></div>" +
                "<div id='collapseTQA_" + AnsweredQuestions[i].QuestionID + "' class='panel-collapse collapse' data-parent='#accordionTQ_Answered'><div class='card-body'>" +
                "<div class='padding-none float-left float-left col-lg-12'><div class='question_answer bottom float-left float-left col-lg-12'>" +
                "Loading..." +
                "</div><div class='float-left col-lg-12 padding-none bottom' style='padding: 10px 0px; border-bottom: 1px solid #38b8ba;'><span class='total_comments_view float-left' style='right: -9px !important; margin-top: 4px !important; width: 100%;'>" +
                "Answered By : <span class='count_comments'>" + AnsweredQuestions[i].QuestionAnsweredBy + ", " + AnsweredQuestions[i].QuestionAnsweredDate + "</span>" +
                "<a class='float-right content_conversation' onclick='ATQ_OpenConversation(" + AnsweredQuestions[i].QuestionID + ");'>Conversation</a>" +
                "<a class='float-right content_conversation' style='display: " + AnsweredQuestions[i].IsPreviousQuestionsShow + ";' onclick='AT_OpenPreviousAnswer(" + AnsweredQuestions[i].QuestionID + ");'>Previous Answers</a>" +
                "</div></div></div></div>";
            $("#divAnsweredQuestions").append(divAnsweredQuestionsdata);
            divUnAnswered = $("#divAnsweredQuestions div").eq(0).clone(true);
        }

        $("#divbtnTQ_ToFAQ").hide();

        if (QuestionsCount > 0 && MarkAsFaqShown) {
            $("#divbtnTQ_ToFAQ").show();
        }

        //For Unanswered Questions
        var div = $("#divUnAnsweredQuestions div").eq(0).clone(true);
        $("#divUnAnsweredQuestions div").eq(0).remove();
        $("#divUnAnsweredQuestions").empty();
        for (var i = 0; i < UnAnsweredQuestions.length; i++) {
            var divUnAsweredQuestions =
                "<div class='col-xs-12' id='divUAQuestion" + UnAnsweredQuestions[i].QuestionID + "'><div class='question_header float-left col-lg-12'><span>" +
                UnAnsweredQuestions[i].QuestionDescription +
                "</span ></div><div class='float-left col-lg-12 padding-none top'><span class='total_comments_view float-left' style='width: 100%;right: -9px !important; margin-top: 4px !important;border-bottom: 1px solid #38b8ba;'>" +
                "Raised By : <span class='count_comments'>" + UnAnsweredQuestions[i].QuestionRaisedBy + ", " + UnAnsweredQuestions[i].QuestionRaisedDate +
                "</span><a class='float-right content_conversation' onclick='UAT_OpenConversation(" + UnAnsweredQuestions[i].QuestionID + ");' >Conversation</a>" +
                // "<a class='comments_count_new float-right' style='top: -14px !important; right:-9px !important;'>04</a>" +
                "<a class='float-right content_conversation' onclick='UAT_OpenAnswer(" + UnAnsweredQuestions[i].QuestionID + ");'>Answer</a>" +
                "</div></div>";
            $("#divUnAnsweredQuestions").append(divUnAsweredQuestions);
            div = $("#divUnAnsweredQuestions div").eq(0).clone(true);
        }
        if (UnAnsweredQuestions.length == 0) {
            $("#divUnAnsweredQuestions").prepend("<div class='question_none' style='text-align:center;font-weight: bold;'>No UnAnswered Questions To Show</div>");
        }
        if (AnsweredQuestions.length == 0) {
            $("#divAnsweredQuestions").prepend("<div class='question_none' style='text-align:center;font-weight: bold;'>No Answered Questions To Show</div>");
        }
        SetViewQuestionsDefaultState();
    }
</script>

<!--To Show, hide Answer or Conversation Div-->
<script>
    function SetViewQuestionsDefaultState() {
        $("#divAnswerToTraineeQuestion").hide();
        $('#divUnAnswerdQueConversation').hide();
        $("#divAnswerdQueConversation").hide();
        $("#divUAQ_Conversation").empty();
        $("#divAQ_Conversation").empty();
        $('#divUnAnswered').removeClass('float-left col-lg-8');
        $('#divUnAnswered').addClass('float-left col-lg-12');
        $('#divAnswered').removeClass('float-left col-lg-8');
        $('#divAnswered').addClass('float-left col-lg-12');
    }
    function UAT_CloseAnswerOrConversationPopup(Type) {
        if (Type == "UAT_Answer") {
            $("#divAnswerToTraineeQuestion").hide();
            document.getElementById("<%= txtTrainerAnswer.ClientID%>").value = "";
        }
        else {
            $('#divUnAnswerdQueConversation').hide();
        }
        $('#divUnAnswered').removeClass('float-left col-lg-8');
        $('#divUnAnswered').addClass('float-left col-lg-12');
    }
    function UAT_OpenConversation(QuestionID) {
        $("#<%=hdnQuestionID.ClientID%>").val(QuestionID);
        document.getElementById("<%= txtUATQ_Comment.ClientID%>").value = "";
        $('#divUnAnswered').removeClass('float-left col-lg-12');
        $('#divUnAnswered').addClass('float-left col-lg-8');
        var txtQuestion = $('div#divUAQuestion' + QuestionID + '> div > span').html();
        $('div#divUnAnswerdQueConversation> div:eq(0) >div:eq(1)').html(txtQuestion);
        //Ajax call for loading the Conversation
        $("#<%=hdnCommentType.ClientID%>").val('UAQ');
        GetTraineeQuestionsConversationByAjaxCall();
        $('#divUnAnswerdQueConversation').show();
        $("#divAnswerToTraineeQuestion").hide();
        ATQ_CloseConversation();
    }
    function UAT_OpenAnswer(QuestionID) {
        $("#<%=hdnQuestionID.ClientID%>").val(QuestionID);
        document.getElementById("<%= txtTrainerAnswer.ClientID%>").value = "";
        $('#divUnAnswered').removeClass('float-left col-lg-12');
        $('#divUnAnswered').addClass('float-left col-lg-8');
        var txtQuestion = $('div#divUAQuestion' + QuestionID + '> div > span').html();
        $('div#divAnswerToTraineeQuestion> div:eq(0) >div:eq(1)').html(txtQuestion);
        $("#divAnswerToTraineeQuestion").show();
        $('#divUnAnswerdQueConversation').hide();
        ATQ_CloseConversation();
    }
    function ATQ_OpenConversation(QuestionID) {
        //alert(QuestionID);
        $("#<%=hdnQuestionID.ClientID%>").val(QuestionID);
        document.getElementById("<%= txtATQ_Comment.ClientID%>").value = "";
        UAT_CloseAnswerOrConversationPopup('UAT_Answer');
        UAT_CloseAnswerOrConversationPopup('UAT_Conversation');
        var txtAQuestion = $('div#divAQuestion' + QuestionID + '> div > span').html();
        //alert(txtAQuestion);
        $('div#divAnswerdQueConversation> div:eq(0) >div:eq(1)').html(txtAQuestion);
        $("#<%=hdnCommentType.ClientID%>").val('AQ');
        GetTraineeQuestionsConversationByAjaxCall();
        $('#divAnswered').removeClass('float-left col-lg-12');
        $('#divAnswered').addClass('float-left col-lg-8');
        $("#divAnswerdQueConversation").show();
    }
    function ATQ_CloseConversation() {
        $("#divAnswerdQueConversation").hide();
        $('#divAnswered').removeClass('float-left col-lg-8');
        $('#divAnswered').addClass('float-left col-lg-12');
        document.getElementById("<%= txtATQ_Comment.ClientID%>").value = "";
    }

    function ReloadDocumentQuestions() {
        ReloadTraineeQuestionForTrainer();
    }
</script>

<!--Get Conversation Data From DB-->
<script>   
    function GetTraineeQuestionsConversationByAjaxCall() {
        UserSessionCheck();
        var Que_ID = $("#<%=hdnQuestionID.ClientID%>")[0].value;
        var objStr = { "TQ_ID": Que_ID };
        var jsonData = JSON.stringify(objStr);
        $.ajax({
            type: "POST",
            url: '<%= Page.ResolveUrl("~/TMS/WebServices/TraineeQuestionsOnDoc/TraineeQuestionsOnDoc.asmx/GetTQ_Conversation")%>',
            data: jsonData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                loadTraineeQuestionsConversation(response);
            },
            failure: function (response) {
                // custAlertMsg('Internal Error Occured on Adding Questions to FAQ', 'error');
            },
            error: function (response) {
                // custAlertMsg('Internal Error Occured on Adding Questions to FAQ', 'error');
            }
        });
    }
    function loadTraineeQuestionsConversation(response) {
        var result = $.parseJSON(response.d);
        var objConversation = result["TQ_Conversation"];
        var CommentType = $("#<%=hdnCommentType.ClientID%>")[0].value;
        var divConversationdiv = "";
        var divConversation = "";
        if (CommentType == "UAQ") {
            divConversationdiv = $("#divUAQ_Conversation div").eq(0).clone(true);
            $("#divUAQ_Conversation div").eq(0).remove();
            $("#divUAQ_Conversation").empty();
            divConversation = "#divUAQ_Conversation";
        }
        else {
            divConversationdiv = $("#divAQ_Conversation div").eq(0).clone(true);
            $("#divAQ_Conversation div").eq(0).remove();
            $("#divAQ_Conversation").empty();
            divConversation = "#divAQ_Conversation";
        }
        for (var i = 0; i < objConversation.length; i++) {
            //alert(objConversation[i].CommentID +','+ objConversation[i].Comment + ','+objConversation[i].Commentor +','+ objConversation[i].CommentorRole +','+ objConversation[i].MarkedAsAnswer);
            var divClass = "";
            var IsChecked = "";
            var CommentAlign = "right";
            if (objConversation[i].CommentorRole == "6") {
                divClass = " question_header_reply";
                CommentAlign = "left";
            }
            if (objConversation[i].MarkedAsAnswer == "True") {
                IsChecked = "checked='checked' disabled";
            }
            var ViewBy = $("#<%=hdnViewerRole.ClientID%>")[0].value;
            var divConversationAppend = "";
            if (ViewBy == "6") {// by trainee
                divConversationAppend = "<div class='question_header float-left col-lg-12 padding-none trainer_reply" + divClass + "'><div class='float-left col-lg-12'>" +
                    "<p class='conversation_text' style='text-align:" + CommentAlign + ";'>" + objConversation[i].Comment + "</p>" +
                    "<span class='float-left total_comments'>Comment By :</span> <span class='float-left total_comments'>"
                    + objConversation[i].Commentor + " </span>" +
                    "</div></div>";
            }
            else {
                divConversationAppend = "<div class='question_header float-left col-lg-12 padding-none trainer_reply" + divClass + "'><div class='float-left col-lg-12'>" +
                    "<p class='conversation_text' style='text-align:" + CommentAlign + ";'>" + objConversation[i].Comment + "</p>" +
                    "<label class='float-right'>" +
                    "<input type='checkbox' " + IsChecked + " value='" + objConversation[i].CommentID + "' class='cbMarkAsAnswer' onchange='cbMarkAsAnswerChange(this)'>" +
                    "<span class='checkmark'></span>Mark As Answer</label>" +
                    "<span class='float-left total_comments'>Comment By :</span> <span class='float-left total_comments'>"
                    + objConversation[i].Commentor + " </span></div></div>";
            }
            $(divConversation).append(divConversationAppend);

        }
        if (objConversation.length == 0) {
            $(divConversation).prepend("<div class='question_none' style='text-align:center;font-weight: bold;'>No Conversation To Show</div>");
        }
    }
</script>

<!--Validations For View Question Actions-->
<script>
    function ValidateSubmitComment(type) {
        var Comment = "";
        if (type == "AQ") {
            Comment = document.getElementById("<%= txtATQ_Comment.ClientID%>").value.trim();
        }
        else {
            Comment = document.getElementById("<%= txtUATQ_Comment.ClientID%>").value.trim();
        }
        errors = [];
        if (Comment == "") {
            errors.push("Enter Comment");
        }
        if (errors.length > 0) {
            if (type == "AQ") {
                custAlertMsg(errors.join("<br/>"), "error", "focusOnAQ_Comment()");
            }
            else {
                custAlertMsg(errors.join("<br/>"), "error", "focusOnUAQ_Comment()");
            }
            return false;
        }
        else {
            return true;
        }
    }
    function focusOnUAQ_Comment() {
        $('#<%=txtUATQ_Comment.ClientID %>').focus();
    }
    function focusOnAQ_Comment() {
        $('#<%=txtATQ_Comment.ClientID %>').focus();
    }
    function ValidateSubmitAnswer() {
        var Answer = document.getElementById("<%= txtTrainerAnswer.ClientID%>").value.trim();
        errors = [];
        if (Answer == "") {
            errors.push("Enter Answer");
        }
        if (errors.length > 0) {
            custAlertMsg(errors.join("<br/>"), "error", "focusOnAnswer()");
            return false;
        }
        else {
            return true;
        }
    }
    function focusOnAnswer() {
        $('#<%=txtTrainerAnswer.ClientID %>').focus();
    }
    function custAlertMsgOnViewQuestionsUC(msgDescription, msgType, MSgFor) {
        if (msgType == 'success') {
            if (MSgFor == 'Answer') {
                custAlertMsg(msgDescription, msgType, 'OnAnswerSucess()');
            }
            else {
                //No need of Sucess msg for Conversation submit .
                OnCommentSucess();
            }
        }
        else {
            custAlertMsg(msgDescription, msgType, true);
        }
    }
    function OnAnswerSucess() {
        UAT_CloseAnswerOrConversationPopup('UAT_Answer');
        GetTraineeQuestionsForTrainerByAjaxCall();
    }
    function OnCommentSucess() {
        document.getElementById("<%= txtUATQ_Comment.ClientID%>").value = "";
        document.getElementById("<%= txtATQ_Comment.ClientID%>").value = "";
        GetTraineeQuestionsConversationByAjaxCall();
    }
</script>

<!--For Previous Answers to Trainee Question-->
<script>
    function AT_OpenPreviousAnswer(TQ_ID) {
        var txtAQuestion = $('div#divAQuestion' + TQ_ID + '> div > span').html();
        $('div#modalTQ_PreviousAnswers> div > div > div >h4').html(txtAQuestion);
        loadPreviousAnswers(TQ_ID);
        $('#modalTQ_PreviousAnswers').modal({ show: true, backdrop: 'static', keyboard: false });
    }
    function loadPreviousAnswers(TQ_ID) {
        UserSessionCheck();
        $("#divPreviousAnswerMain").empty();
        $.ajax({
            type: "POST",
            url: '<%= Page.ResolveUrl("~/TMS/WebServices/TraineeQuestionsOnDoc/TraineeQuestionsOnDoc.asmx/GetTQ_PreviousAnswer")%>',
            data: JSON.stringify({ "TQ_ID": TQ_ID }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var result = $.parseJSON(response.d);
                var objPreviousAnswers = result["PreviousAnswers"];
                for (var i = 0; i < objPreviousAnswers.length; i++) {
                    var divPreviousAnswers =
                        "<div class='form-group padding-none'>" +
                        "<label class='float-left col-lg-12 padding-none control-label custom_label_answer'>" +
                        "Answer " + (i + 1) + "<span class='total_comments float-right'>" +
                        "Created By : <span class='count_comments'>" + objPreviousAnswers[i].Employee + ", " + objPreviousAnswers[i].MarkedOn + "</span></span></label>" +
                        "<div class='padding-none'>" +
                        "<textarea class='form-control text_height' rows='7' disabled>" + objPreviousAnswers[i].Answer + "</textarea></div></div>";
                    $("#divPreviousAnswerMain").append(divPreviousAnswers);
                }
            },
            failure: function (response) {
                // custAlertMsg('Internal Error Occured on Adding Questions to FAQ', 'error');
            },
            error: function (response) {
                // custAlertMsg('Internal Error Occured on Adding Questions to FAQ', 'error');
            }
        });
    }
</script>

<!--Get the Latest Answer to Trainee Question-->
<script>
    function GetTQ_Answer(Que_ID) {
        UserSessionCheck();
        var txtAnswer = $('div#collapseTQA_' + Que_ID + ' > div > div > div').html();
        if (txtAnswer == 'Loading...') {
            // Ajax call for loading the Latest Answer to a Question
            var objStr = { "TQ_ID": Que_ID };
            var jsonData = JSON.stringify(objStr);
            $.ajax({
                type: "POST",
                url: '<%= Page.ResolveUrl("~/TMS/WebServices/TraineeQuestionsOnDoc/TraineeQuestionsOnDoc.asmx/GetLatestAnswertoTQ")%>',
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var Answerresult = $.parseJSON(response.d);
                    $('div#collapseTQA_' + Que_ID + ' > div > div > div:eq(0)').html(Answerresult);
                },
                failure: function (response) {
                    // custAlertMsg('Internal Error Occured on Adding Questions to FAQ', 'error');
                },
                error: function (response) {
                    custAlertMsg('Internal Error Occured on Adding Questions to FAQ', 'error');
                }
            });
        }
    }    
</script>

<!--For Mark As FAQ-->
<script>
    $('#btnSubmitTQ_ToFAQ').click(function () {
        UserSessionCheck();
        var MarkedFAQs = new Array();
        $('input.cbMarkedFAQs[type=checkbox]').each(function () {
            if (this.checked)
                MarkedFAQs.push($(this).val());
        });
        var data = { "strTQ_IDs": MarkedFAQs, "EmpID": $(<%=hdnEmpID.ClientID %>).val() };
        var jsonData = JSON.stringify(data);
        if (MarkedFAQs.length > 0) {
            $.ajax({
                type: "POST",
                url: '<%= Page.ResolveUrl("~/TMS/WebServices/TraineeQuestionsOnDoc/TraineeQuestionsOnDoc.asmx/PostTQ_ToFAQ")%>',
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var resultMsg = response.d;
                    if (resultMsg == "0") {
                        custAlertMsg('Question(s) already exists in FAQs', 'error');
                    }
                    else if (resultMsg == "1") {
                        custAlertMsg('Some of your Selected Question(s) are Failed on Adding to FAQs', 'info', 'GetTraineeQuestionsForTrainerByAjaxCall()');
                    }
                    else if (resultMsg == "2") {
                        custAlertMsg('All the Selected Question(s) are Added to FAQs', 'success', 'GetTraineeQuestionsForTrainerByAjaxCall()');
                    }
                    else {
                        custAlertMsg('Unknown', 'warning', 'GetTraineeQuestionsForTrainerByAjaxCall()');
                    }
                },
                failure: function (response) {
                    // custAlertMsg('Internal Error Occured on Adding Questions to FAQ', 'error');
                },
                error: function (response) {
                    custAlertMsg('Internal Error Occured on Adding Questions to FAQ', 'error', 'GetTraineeQuestionsForTrainerByAjaxCall()');
                }
            });
        }
        else {
            custAlertMsg('Select atleast one Question to Add on FAQs', 'warning', 'GetTraineeQuestionsForTrainerByAjaxCall()');
        }
    });
</script>

<!--For Mark Comment As Answer-->
<script>   
    function cbMarkAsAnswerChange(control) {
        UserSessionCheck();
        $(".cbMarkAsAnswer").prop('checked', false);
        $(".cbMarkAsAnswer").removeAttr("disabled");
        var markedCommentID = $(control).val();
        var data = { "MarkedCommentID": markedCommentID, "EmpID": $(<%=hdnEmpID.ClientID %>).val(), "TQ_ID": $(<%=hdnQuestionID.ClientID %>).val() };
        var jsonData = JSON.stringify(data)
        $.ajax({
            type: "POST",
            url: '<%= Page.ResolveUrl("~/TMS/WebServices/TraineeQuestionsOnDoc/TraineeQuestionsOnDoc.asmx/MarkCommentAsAnswer")%>',
            data: jsonData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var resultMsg = response.d;
                if (resultMsg == "1") {
                    custAlertMsg('Comment Marked As Answer', 'success', 'GetTraineeQuestionsForTrainerByAjaxCall()');
                    $(control).prop('checked', true);
                    $(control).attr("disabled", true);
                }
                else {
                    custAlertMsg('Unknown', 'warning', 'GetTraineeQuestionsForTrainerByAjaxCall()');
                }
            },
            failure: function (response) {
                // custAlertMsg('Internal Error Occured on Adding Questions to FAQ', 'error');
            },
            error: function (response) {
                custAlertMsg('Internal Error Occured on Mark Comment as Answer', 'error', 'GetTraineeQuestionsForTrainerByAjaxCall()');
            }
        });
    };
</script>


