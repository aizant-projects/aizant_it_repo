﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucJrTrainedRecords.ascx.cs" Inherits="AizantIT_PharmaApp.UserControls.TMS_UserControls.ucJrTrainedRecords" %>
<%@ Register Src="~/UserControls/TMS_UserControls/Exam/ucExamResultSheet.ascx" TagPrefix="uc1" TagName="ucExamResultSheet" %>

<asp:HiddenField ID="hdnJR_ID" runat="server" Value="0" />
<asp:HiddenField ID="hdnTrainerID" runat="server" Value="0" />
<asp:HiddenField runat="server" ID="hdnDocID" Value="0" />
<asp:HiddenField runat="server" ID="hdnIsEvaluation" Value="false" />

<!---Emp JR Training Records--->
<div id="ModalTrainingRecords" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="min-width:85%;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    <asp:Label ID="lblJRTrainingRecords" runat="server" Text="JR Training Records"></asp:Label>
                </h4>
                <button type="button" class="close tmsModalTR_CloseBtn" onclick="CloseTrainedDocModalPopUp();" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body">
                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12">
                    <ul id="ulTrainingRecordsTabs" class="nav nav-tabs tab_grid">
                        <li id="liRegDoc" class=" nav-item ">
                            <a class="RegularTab nav-link active" data-toggle="tab" href="#RegularList">Regular</a></li>
                        <li id="liRetrospective" class=" nav-item ">
                            <a class="RetrospectiveTab nav-link" data-toggle="tab" href="#RetrospectiveList" onclick="ReloadRetrospectiveRecords();">Retrospective</a></li>
                    </ul>
                    <div class="tab-content float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none top">
                        <div id="RegularList" class="RegularTabBody tab-pane fade show active">
                            
                                <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                    <table id="tblJrRegularTrainingRecords" class="padding-none datatable_cust tblJrRegularTrainingRecordClass display breakword">
                                        <thead>
                                            <tr>
                                                <th>DocumentID</th>
                                                <th>Document</th>
                                                <th>DocType</th>
                                                <th>Version</th>
                                                <th>Department</th>
                                                <th>ExamType</th>
                                                <th>Attempts</th>
                                                <th>MOE</th>
                                                <th>Status</th>
                                                <th>Read Duration</th>
                                            </tr>
                                        </thead>
                                    </table>

                                    <div class="float-left col-lg-12 padding-none  pull-left">
                                        <div class="grid_legend_tms pull-left" style="font-weight: bold; margin-right: 10px;">'MOE' : Mode of Evaluation</div>
                                        <span class="AnswerSheet_tms" style="padding: 2px 12px;"></span><b class="legends_grid_tms">Written</b><span class="OralEvaluation_tms" style="margin-left: 5px; padding: 2px 12px !important"></span><b class="legends_grid_tms">Oral</b></span>
                                    </div>
                                </div>
                            
                        </div>
                        <div id="RetrospectiveList" class="RetrospectiveTabBody tab-pane fade">
                            <div class=" float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                <table id="tblJrRetrospectiveRecords" class="display datatable_cust tblJrRetrospectiveRecordsClass breakword" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th>Document</th>
                                            <th>DocType</th>
                                            <th>Version</th>
                                            <th>Department</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
</div>
<!---End Emp JR Training Records--->

<!--------Trainer Evaluation Opinion---------->
<div id="modelTrainerEvaluationOpinion" role="dialog" class="modal department fade">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
               
                <h4 class="modal-title">Trainer Opinion</h4>
                 <button type="button" class="close TabFocus" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body float-left col-lg-12 col-sm-12 col-12 col-md-12">
                <div id="divOpinion">
                    <div class=" padding-none col-sm-12 col-12 float-left col-lg-12 col-md-12 " data-toggle="buttons">
                        <label for="inputPassword3" class=" float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none control-label">Opinion</label>
                        <label class="btn radio_check btn-primary_radio col-6 float-left col-lg-6 col-md-6 col-sm-6 active" id="lblRadioSubmitSatisfactory" runat="server">
                            <asp:RadioButton ID="rbtnSatisfactory" value="Yes" runat="server" GroupName="A" Text="Satisfactory" Checked="true" />
                        </label>
                        <label class="radio_check btn btn-primary_radio col-6 float-left col-lg-6 col-md-6 col-sm-6" id="lblRadioSubmitNotSatisfactory" runat="server">
                            <asp:RadioButton ID="rbtnNotSatisfactory" value="No" runat="server" GroupName="A" Text="Not Satisfactory" />
                        </label>
                    </div>
                </div>
                <div class="form-group float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                    <label for="inputPassword3" class=" float-left col-lg-12 col-sm-12 col-12 col-md-12 control-label padding-none custom_label_answer">Comments</label>
                    <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                        <asp:TextBox ID="txtEvaluationComments" runat="server" TextMode="MultiLine" Rows="5" MaxLength="250" placeholder="Enter Comments" CssClass="form-control feed_text capitalize"></asp:TextBox>
                    </div>
                </div>
               
            </div>
            <div class="modal-footer">
                 <div class="form-group float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                    <button type="button" class=" btn-cancel_popup float-right" style="margin-left: 10px !important " data-dismiss="modal">Cancel</button>
                    <asp:UpdatePanel ID="upEvaluateOralPractical" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnEvaluateOralPractical" OnClientClick="ShowPleaseWait('show');" runat="server" CssClass="btn-signup_popup float-right" Text="Evaluate" OnClick="btnEvaluateOralPractical_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</div>
<!---- Trainer Evaluation Opinion ---->

<!--------View Trainer Evaluation Opinion---------->
<div id="modelViewTrainerEvaluationOpinion" role="dialog" class="modal department fade">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Trainer Opinion</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <asp:UpdatePanel ID="upEvaluationOpinion" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-body float-left col-lg-12 col-sm-12 col-12 col-md-12">
                        <div id="divViewOpinion">
                            <div class="padding-none col-sm-12 col-12 float-left col-lg-12 col-md-12 " style="cursor:not-allowed" data-toggle="buttons">
                                <label for="inputPassword3" class=" float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none control-label">Opinion</label>
                                <label class="btn btn-primary_radio col-6 float-left col-lg-6 col-md-6 col-sm-6 active radio_check" id="lblRadioSatisfactory" runat="server" disabled="disabled" style="pointer-events: none">
                                    <asp:RadioButton ID="rbtnYes" runat="server" GroupName="A" Text="Satisfactory" Enabled="false" />
                                </label>
                                <label class="btn btn-primary_radio col-6 float-left col-lg-6 col-md-6 col-sm-6 radio_check" id="lblRadioNotSatisfactory" runat="server" disabled="disabled" style="pointer-events: none">
                                    <asp:RadioButton ID="rbtn_No" runat="server" GroupName="A" Text="Not Satisfactory" Enabled="false" />
                                </label>
                            </div>
                        </div>
                        <div class="form-group float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                            <label for="inputPassword3" class=" float-left col-lg-12 col-sm-12 col-12 col-md-12 control-label padding-none custom_label_answer">Comments</label>
                            <div class="float-left col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                <asp:TextBox ID="txtViewEvaluationComments" runat="server" TextMode="MultiLine" Rows="5" MaxLength="250" placeholder="Enter Comments" Enabled="false" CssClass="form-control feed_text capitalize"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!---- End Trainer Opinion---->

<div id="divServerBtn" style="display: none">
    <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnViewAnswerSheet" runat="server" Text="viewAnswerSheet" OnClick="btnViewAnswerSheet_Click1" />
            <asp:Button ID="btnGetTrainerOpinion" runat="server" Text="viewTrainerOpinion" OnClick="btnGetTrainerOpinion_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

<uc1:ucExamResultSheet runat="server" ID="ucExamResultSheet" />

<!--To get Emp JR Regular Records for Emp Reports and Trainer Evaluation-->
<script>
    function GetRegularRecords(JR_ID, TrainerID, IsEvaluation) {
        $("#<%=hdnJR_ID.ClientID%>").val(JR_ID);
        $("#<%=hdnTrainerID.ClientID%>").val(TrainerID);
        $("#<%=hdnIsEvaluation.ClientID%>").val(IsEvaluation);
        OpenTrainingRecordsModal();
    }

    function viewAnswerSheet(DocID) {
        $("#<%=hdnDocID.ClientID%>").val(DocID);
        $("#<%=btnViewAnswerSheet.ClientID%>").click();
    }

    function ReloadTrainedRecords() {
        $('#modelTrainerEvaluationOpinion').modal('hide');
        loadJrRegularRecords();
        ShowPleaseWait('hide');
    }
    function ClearTrainerOpinion() {
        document.getElementById("<%= txtEvaluationComments.ClientID%>").value = '';
        $("#<%=lblRadioSubmitSatisfactory.ClientID%>").addClass("active");
            $("#<%=lblRadioSubmitNotSatisfactory.ClientID%>").removeClass("active");
    }

    function CloseTrainedDocModalPopUp() {
        reloadoTableJR_TrainerEvaluationDetails();
        $('#ModalTrainingRecords').modal('hide');
    }

    function viewTrainerOpinion(DocID) {
        $("#<%=hdnDocID.ClientID%>").val(DocID);
        $('#modelTrainerEvaluationOpinion').modal({ show: true, backdrop: 'static', keyboard: false });
        ClearTrainerOpinion();
    }

    function GetTrainerOpinion(DocID) {
        $("#<%=hdnDocID.ClientID%>").val(DocID);
        $("#<%=btnGetTrainerOpinion.ClientID%>").click();
        $('#modelViewTrainerEvaluationOpinion').modal({ show: true, backdrop: 'static', keyboard: false });
    }

</script>

<!--Emp JR Regular jQuery DataTable-->
<script>
    var oTableJrRegularRecords;
    function loadJrRegularRecords() {
        if (oTableJrRegularRecords != null) {
            oTableJrRegularRecords.destroy();
        }

        oTableJrRegularRecords = $('#tblJrRegularTrainingRecords').DataTable({
            columns: [
                { 'data': 'DocumentID' },
                { 'data': 'Document' },
                { 'data': 'DocumentType' },
                { 'data': 'VersionNumber' },
                { 'data': 'DepartmentName' },
                { 'data': 'ExamType' },
                { 'data': 'Attempts' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) {
                        if (o.StatusName == "InProgress") {
                            return 'N/A';
                        }
                        else if (o.StatusName == "Completed") {
                            if (<%=hdnIsEvaluation.ClientID%>.value == "true") {
                                if (o.ExamType == 2) {
                                    return '<a class="OralEvaluation_tms" href="#" data-toggle="modal" data-target="#modelTrainerEvaluationOpinion" onclick="viewTrainerOpinion(' + o.DocumentID + ');"></a>';
                                }
                                else {
                                    return '<a class="AnswerSheet_tms" href="#" data-toggle="modal" data-target="#modelExamResultSheet"  onclick="viewAnswerSheet(' + o.DocumentID + ');"></a>';
                                }
                            }
                            else if (<%=hdnIsEvaluation.ClientID%>.value == "false") {
                                if (o.ExamType == 2) {
                                    return 'Oral';
                                }
                                else {
                                    return 'written(' + o.Attempts + ')';
                                }
                            }
                            else {
                                return '';
                            }
                        }
                        else if (o.StatusName == "Evaluated By Trainer") {
                            if (o.ExamType == 2) {
                                return '<a class="OralEvaluation_tms" href="#" title="Oral" onclick="GetTrainerOpinion(' + o.DocumentID + ');"></a>';
                            }
                            else {
                                return '<a class="AnswerSheet_tms" href="#" title="Written" onclick="viewAnswerSheet(' + o.DocumentID + ');"></a>';
                            }
                        }
                        else {
                            return '';
                        }
                    }
                },
                { 'data': 'StatusName' },
                { 'data': 'ReadDuration' }
            ],
            "scrollY": "374px",
            "aoColumnDefs": [{ "targets": [0, 5, 6], "visible": false }, { "targets": [3, 7], "orderable": false }, { className: "textAlignLeft", "targets": [1,2,4,8,9] }],
            "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/JrTrainedRecords/JrTrainedRecordService.asmx/GetJrRegularRecords")%>",
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "JR_ID", "value": <%=hdnJR_ID.ClientID%>.value },
                    { "name": "TrainerID", "value": <%=hdnTrainerID.ClientID%>.value });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        var IsRetrospective = json["iIsRetrospective"];
                        if (IsRetrospective == 0) {
                            $('#ulTrainingRecordsTabs').hide();
                        }
                        else {
                            $('#ulTrainingRecordsTabs').show();
                        }
                        fnCallback(json);
                        UserSessionCheck();
                        $("#tblJrRegularTrainingRecords").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        $(".tblJrRegularTrainingRecordClass").css({ "width": "100%" });
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
    }

    function OpenTrainingRecordsModal() {
        $('#ModalTrainingRecords').modal('show');
    }

    $('#ModalTrainingRecords').on('shown.bs.modal', function (e) {
        loadJrRegularRecords();
    });

</script>

<!--To get Emp JR Retrospective Records-->
<script>
    function GetRetrospectiveRecords(JR_ID, TrainerID) {
        $("#<%=hdnJR_ID.ClientID%>").val(JR_ID);
        $("#<%=hdnTrainerID.ClientID%>").val(TrainerID);
    }
</script>


<!--Emp JR Retrospective jQuery DataTable-->
<script>
    var oTableJrRetrospectiveRecords;
    function loadJrRetrospectiveRecords() {
        if (oTableJrRetrospectiveRecords != null) {
            oTableJrRetrospectiveRecords.destroy();
        }
        oTableJrRetrospectiveRecords = $('#tblJrRetrospectiveRecords').DataTable({
            columns: [
                { 'data': 'Document' },
                { 'data': 'DocumentType' },
                { 'data': 'VersionNumber' },
                { 'data': 'DepartmentName' }
            ],
            "order": [[1, "desc"]],
            "scrollY": "250px",
            "aoColumnDefs": [{ "targets": [2], "orderable": false }, { className: "textAlignLeft", "targets": [0, 1, 3] }],
            "sAjaxSource": "<%=ResolveUrl("~/TMS/WebServices/JrTrainedRecords/JrTrainedRecordService.asmx/GetJrRetrospectiveRecords")%>",
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "JR_ID", "value": <%=hdnJR_ID.ClientID%>.value }, { "name": "TrainerID", "value": <%=hdnTrainerID.ClientID%>.value });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        var IsRetrospective = json["iIsRetrospective"];
                        if (IsRetrospective == 0) {
                            $('#ulTrainingRecordsTabs').hide();
                        }
                        else {
                            $('#ulTrainingRecordsTabs').show();
                        }
                        fnCallback(json);
                        UserSessionCheck();
                        $("#tblJrRetrospectiveRecords").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        $(".tblJrRetrospectiveRecordsClass").css({ "width": "100%" });
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
           
        });
    }

    function ReloadRetrospectiveRecords() {
        if (oTableJrRetrospectiveRecords == null) {
            loadJrRetrospectiveRecords();
        }
    }

    // On Training Records Modal Closed.
    $(".tmsModalTR_CloseBtn").on("click", function () {
        SetRegularTabToActive();
        if (oTableJrRegularRecords != null) {
            oTableJrRegularRecords.destroy();
            oTableJrRegularRecords = null;
        }
        if (oTableJrRetrospectiveRecords != null) {
            oTableJrRetrospectiveRecords.destroy();
            oTableJrRetrospectiveRecords = null;
        }
    });

</script>

<script>
    function SetRegularTabToActive() {
        $('.RegularTab').addClass('active');
        $('.RegularTab').addClass('show');
        $('.RetrospectiveTab').removeClass('active');
        $('.RetrospectiveTab').removeClass('show');

        $('.RegularTabBody').addClass('active');
        $('.RegularTabBody').addClass('show');
        $('.RetrospectiveTabBody').removeClass('active');
        $('.RetrospectiveTabBody').removeClass('show');
    }
</script>

<script>
    $(document).ready(function () {
        $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
            var anchor = $(e.target).attr('href');
            if (anchor == "#RegularList") {
                loadJrRegularRecords();
            }
            else if (anchor == "#RetrospectiveList") {
                loadJrRetrospectiveRecords();
            }
        })
    });
</script>


