﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucFeedback.ascx.cs" Inherits="AizantIT_PharmaApp.UserControls.TMS_UserControls.ucFeedback" %>
<%-- Feedback form css--%>
<style>
    .rating {
        float: left;
    }
    .question_list_name {
        font-size:13pt;
        font-family: seguisb;
    }
        .rating:not(:checked) > input {
            position: absolute;
            /*top: -9999px;*/
            clip: rect(0,0,0,0);
        }

        .rating:not(:checked) > label {
            float: right;
            width: 40px;
            padding: 5px 0px 0px 14px;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 200%;
            line-height: 1.2;
            color: #acacac;
        }

            .rating:not(:checked) > label:before {
                content: '★ ';
            }

        .rating > input:checked ~ label {
            color: #ed9c78;
        }

        .rating:not(:checked) > label:hover,
        .rating:not(:checked) > label:hover ~ label {
            color: #ed9c78;
        }

        .rating > input:checked + label:hover,
        .rating > input:checked + label:hover ~ label,
        .rating > input:checked ~ label:hover,
        .rating > input:checked ~ label:hover ~ label,
        .rating > label:hover ~ input:checked ~ label {
            color: #ed9c78;
        }

        .rating > label:active {
            position: relative;
            top: 2px;
            left: 2px;
        }

    .btn-feedback {
        margin-left: 4px;
        background: #07889a;
        border: 1px solid #07889a;
        border-radius: 4px;
        padding: 3px 22px;
        font-size: 10pt;
        color: #fff;
        margin-bottom: 0px;
        font-family: seguisb;
    }

    .btn-feedback_skip {
        margin-left: 4px;
        background: #ee6a30;
        border: 1px solid #ee6a30;
        border-radius: 4px;
        padding: 3px 22px;
        font-size: 10pt;
        color: #fff;
        margin-bottom: 0px;
        font-family: seguisb;
    }
    #gvFeedbackQuestions tr td {background:#fff !important;}
    .emplo_feed {
        max-height: 300px;
        overflow: auto;
    }

    .feedbackCaptionText {
        color: #000;
        background: #d4d4d4;
        padding: 10px !important;
        font-family: seguisb;
        font-size: 12pt;
        font-weight: normal;
    }
</style>
<%--End Feedback form css--%>
<asp:UpdatePanel runat="server" UpdateMode="Always" ID="UpdatePanel1">
    <ContentTemplate>
        <asp:HiddenField ID="hdnEmpFeedbackID" runat="server" Value="0" />
        <asp:HiddenField ID="hdnFeedbackType" runat="server" Value="0" />
        <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
        <asp:HiddenField ID="hdnBaseID" runat="server" Value="0" />
    </ContentTemplate>
</asp:UpdatePanel>

<!--------Feedback Form Modal For Creation-------------->
<div id="FeedbackCreation" class="modal department fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content ">
            <asp:UpdatePanel runat="server" UpdateMode="Always" ID="upFeedbackModel">
                <ContentTemplate>
                    <div class="modal-body">
                        <div class="form-group">
                            <asp:Label ID="lblFeedbackMainTitle" runat="server"></asp:Label>
                            <asp:Label ID="lblFeedbackTitle" CssClass="float-left col-lg-12 col-sm-12 col-12 col-md-12 feedbackCaptionText padding-none" runat="server"></asp:Label>
                        </div>
                        <div class="form-group padding-none col-lg-12 col-sm-12 col-12 col-md-12  float-left emplo_feed">
                            <asp:GridView ID="gvFeedbackQuestions" EmptyDataText="No Record's" ShowHeader="false"
                                runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover AspGrid"
                                EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                                <Columns>
                                    <asp:TemplateField HeaderText="QuestionID" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblQuestionID" runat="server" Text='<%# Bind("QuestionID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <div class="col-lg-12 col-sm-12 col-12 col-md-12">
                                                <asp:Label ID="lblFeedbackQuestion" CssClass="question_list_name" runat="server" Text='<%# Bind("Question") %>'>></asp:Label>
                                            </div>
                                            <div class="col-lg-12 col-sm-12 col-12 col-md-12">
                                                <div>
                                                    <asp:RadioButtonList ID="rdblstRating" CssClass="rating " runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                        <asp:ListItem Value="5" Text="Worst"></asp:ListItem>
                                                        <asp:ListItem Value="4" Text="Fair"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="Average"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="Good"></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="Best"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div class="form-group col-lg-12 col-sm-12 col-12 col-md-12 top float-left padding-none">
                            <label for="inputPassword3" class=" col-lg-12 col-sm-12 col-12 col-md-12 padding-none control-label">Comments</label>
                            <div class="col-lg-12 col-sm-12 col-12 col-md-12 padding-none">
                                <asp:TextBox ID="txtFeedbackComments" runat="server" TextMode="MultiLine" Rows="5" CssClass="form-control feed_text capitalize"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-lg-12 col-sm-12 col-12 col-md-12 float-left  padding-none">
                              <button type="button" class="btn-feedback_skip float-right" onclick="ClearFeedbackComments()" data-dismiss="modal">Skip</button>
                            <asp:Button ID="btnF_Rating_Submit" runat="server" Text="Submit" CssClass="btn btn-feedback float-right" OnClick="btnF_Rating_Submit_Click" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="modal-footer" style="border: none;">
            </div>
        </div>
    </div>
</div>
<!-------- EndFeedback Form Rating-------------->

<!--------Feedback Form For View Purpose-------------->
<div id="ViewFeedbackForm" class="modal department fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content ">
            <div class="modal-body">
                <asp:UpdatePanel ID="upFeedbackForm" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="col-lg-12 col-sm-12 col-12 col-md-12 float-left">
                             <button type="button" class="close close_feed_view  float-right" data-dismiss="modal">&times;</button>
                            <asp:Label ID="lblViewFeedbackMainTitle" CssClass="" runat="server"></asp:Label>  
                           <asp:Label ID="lblViewFeedbackTitle" CssClass="float-left col-sm-12 feedbackCaptionText padding-none" runat="server"></asp:Label>
                        </div>
                        <div class="form-group  col-lg-12 col-sm-12 col-12 col-md-12  float-left emplo_feed">
                            <asp:GridView ID="gv_ViewFeedbackQuestions" EmptyDataText="No Record's" ShowHeader="false"
                                runat="server" AutoGenerateColumns="False" CssClass="table feedback_table table-hover"
                                EmptyDataRowStyle-CssClass="GV_EmptyDataStyle" BorderStyle="none">
                                <Columns>
                                    <asp:TemplateField ItemStyle-CssClass="feedback_table_td">
                                        <ItemTemplate>
                                            <div class="col-lg-12 col-sm-12 col-12 float-left col-md-12 padding-none">
                                                <asp:Label ID="lblFeedbackQuestion" runat="server" Text='<%# Bind("Question") %>'></asp:Label>
                                            </div>
                                            <div class="col-lg-12 col-sm-12 col-12 float-left col-md-12 padding-none">
                                                <div class="star_rating">
                                                    <asp:Literal ID="ltrlFeedbackRating" runat="server" Text='<%# Bind("Rating") %>'></asp:Literal>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>

                        <div class="form-group col-lg-12 col-sm-12 col-12 col-md-12 top float-left">
                            <label for="inputPassword3" class=" col-sm-12 padding-none control-label">Comments</label>
                            <div class="col-sm-12 padding-none">
                                <asp:TextBox ID="txtViewFeedbackComments" Enabled="false" runat="server" TextMode="MultiLine" Rows="5" CssClass="form-control feed_text capitalize"></asp:TextBox>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
            <div class="modal-footer" style="border: none;">
            </div>
        </div>
    </div>
</div>
<!-------- EndFeedback Form For View Purpose-------------->

<!--Feedback Script-->
<script>
    function ucCloseFeedback() {
        $('#FeedbackCreation').modal('hide');
    }
    function ucOpenFeedBackForm() {
        $('#FeedbackCreation').modal({ backdrop: 'static', keyboard: false });
    }
    function ucFeedbackFormViewopen() {
        $('#ViewFeedbackForm').modal({ backdrop: 'static', keyboard: false });
    }
    function feedbackValidation() {
        custAlertMsg('Please Give Feedback for All Questions', 'error');
    }

    function feedbackSuccess() {
        ucCloseFeedback();
        if ($("#<%=hdnFeedbackType.ClientID%>").val() == "2") {
            custAlertMsg('Thank You! For Your Feedback', 'success', 'ReloadSessionDatatables()');
            ClearFeedbackComments();
        }
        else {
            custAlertMsg('Thank You! For Your Feedback', 'success');
        }
    }
    function feedbackNotSubmit() {
        custAlertMsg('Feedback Not Submited, Due to Some Internal Error', 'error');
    }
    function ClearFeedbackComments() {
        var FeedBackComments = document.getElementById("<%=txtFeedbackComments.ClientID%>").value = '';
    }
</script>
