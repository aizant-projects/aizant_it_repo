﻿using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BusinessLayer;


namespace AizantIT_PharmaApp.UserControls.TMS_UserControls
{
    public partial class ucFeedback : System.Web.UI.UserControl
    {
        TMS_BAL objTMS_Bal;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }

        // FeedbackType 1 for "JR" 2 for "TTS", FeedbackStatus 1 for Active and 2 for Inactive. 
        public void CreateFeedbackForm(int FeedbackType, int BaseID, int EmpID)
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                int FeedbackStatus;
                hdnFeedbackType.Value = FeedbackType.ToString();
                hdnBaseID.Value = BaseID.ToString();
                hdnEmpID.Value = EmpID.ToString();
               
                lblFeedbackMainTitle.Text = FeedbackFormText(FeedbackType);

                DataTable dtF_Questions = objTMS_Bal.FeedbackForCreation(FeedbackType, out FeedbackStatus);
                if (FeedbackStatus != 2)
                {
                    if (dtF_Questions.Rows.Count > 0)
                    {
                        lblFeedbackTitle.Text = dtF_Questions.Rows[0]["Title"].ToString();
                        gvFeedbackQuestions.DataSource = dtF_Questions;
                        gvFeedbackQuestions.DataBind();
                    }
                    if (dtF_Questions.Rows.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "modal", "ucOpenFeedBackForm();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FB-1:" + strline + "  " + strMsg, "error");
            }
        }

        private static string FeedbackFormText(int FeedbackType,bool IsCreation=true)
        {
            string FeedbackMainTitle;
            string FeedbackFormText = "Feedback Form";
            if (FeedbackType == 1 || FeedbackType == 2)
            {
                FeedbackFormText = "Training Feedback Form";
            }
            if (IsCreation)
            {
                FeedbackMainTitle = "<h2 class='col-lg-10 col-sm-10 col-10 col-10-12 padding-none pop_headerfeed'>" + FeedbackFormText + "</h2>";
            }
            else
            {
                FeedbackMainTitle = "<h2 class='col-sm-10 pop_headerfeed' style='padding-left: 158px'>"+ FeedbackFormText + "</h2>";
            }
            return FeedbackMainTitle;
        }

        public void ViewFeedbackForm(int EmpFeedbackID, int FeedbackType)
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                hdnEmpFeedbackID.Value = EmpFeedbackID.ToString();
                hdnFeedbackType.Value = FeedbackType.ToString();
                DataSet ds = objTMS_Bal.ViewEmployeeFeedback(EmpFeedbackID, FeedbackType);

                lblViewFeedbackMainTitle.Text = FeedbackFormText(FeedbackType);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    lblViewFeedbackTitle.Text = ds.Tables[0].Rows[0]["Title"].ToString();
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    gv_ViewFeedbackQuestions.DataSource = ds.Tables[1];
                    gv_ViewFeedbackQuestions.DataBind();
                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    txtViewFeedbackComments.Text = ds.Tables[2].Rows[0]["Comments"].ToString();
                }
                upFeedbackForm.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "modal", "ucFeedbackFormViewopen();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FB-2:" + strline + "  " + strMsg, "error");
            }
        }
        
        protected void btnF_Rating_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                bool FeedbackValid = true;
                foreach (GridViewRow row in gvFeedbackQuestions.Rows)
                {
                    RadioButtonList rbtnlstRating = (RadioButtonList)row.FindControl("rdblstRating");
                    if (rbtnlstRating.SelectedValue == "")
                    {
                        FeedbackValid = false;
                    }
                }
                if (FeedbackValid)
                {
                    DataTable dtFeedback = new DataTable();
                    dtFeedback.Columns.Add("QuestionID");
                    dtFeedback.Columns.Add("Rating");
                    DataRow dr;
                    foreach (GridViewRow row in gvFeedbackQuestions.Rows)
                    {
                        Label lblQuestionID = row.FindControl("lblQuestionID") as Label;
                        RadioButtonList rblst = (RadioButtonList)row.FindControl("rdblstRating");
                        string F_QuestionID = lblQuestionID.Text;
                        dr = dtFeedback.NewRow();
                        dr["QuestionID"] = F_QuestionID;
                        dr["Rating"] = rblst.SelectedValue;
                        dtFeedback.Rows.Add(dr);
                    }
                    objTMS_Bal = new TMS_BAL();

                    int c = objTMS_Bal.InsertFeedbackForm(Convert.ToInt32(hdnEmpID.Value),
                        Convert.ToInt32(hdnBaseID.Value),
                        Convert.ToInt32(hdnFeedbackType.Value),
                        dtFeedback, txtFeedbackComments.Text.Trim() == "" ? "N/A": txtFeedbackComments.Text.Trim(), out int FeedbackResult);
                    if (FeedbackResult == 1)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "validation", "feedbackSuccess();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "errorModal", "feedbackNotSubmit();", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "validation", "feedbackValidation();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "FB-3:" + strline + "  " + strMsg, "error");
            }
        }
    }
}