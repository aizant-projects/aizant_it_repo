﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Examination.ascx.cs" Inherits="AizantIT_PharmaApp.UserControls.TMS_UserControls.Examination" %>
<%@ Register Src="~/UserControls/TMS_UserControls/Exam/ucExamResultSheet.ascx" TagPrefix="uc1" TagName="ucExamResultSheet" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" style="margin: 10px" UpdateMode="Always">
    <ContentTemplate>
        <asp:HiddenField ID="hdnBaseType" runat="server" Value="0" />
        <asp:HiddenField ID="hdnDocID" runat="server" Value="0" />
        <asp:HiddenField ID="hdnBaseID" runat="server" Value="0" />
        <asp:HiddenField ID="hdnMarksGained" runat="server" Value="0" />
        <asp:HiddenField ID="hdnIsAskFeedback" runat="server" Value="N" />
    </ContentTemplate>
</asp:UpdatePanel>
<!-- Modal popup for exam paper-->
<div class="modal fade" id="modelUcExamination" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 90%; height: 600px">
        <div class="modal-content">
            <div class="modal-header tmsModalHeader">
                <button type="button" class="close tmsModalCloseBtn" data-dismiss="modal">&times;</button>
                <h4 class="modal-title Panel_Title" style="color: aliceblue">Examination</h4>
            </div>
            <div class="modal-body tmsModalBody">
                <div class="row tmsModalContent" style="overflow: auto; padding: 0px;">
                    <asp:UpdatePanel ID="upExamQuestionnaire" runat="server" style="margin: 10px" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView ID="gvExamQuestionnaire" OnRowDataBound="gvExamQuestionnaire_RowDataBound" OnPageIndexChanging="gvExamQuestionnaire_PageIndexChanging"
                                DataKeyNames="QuestionID" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered question_list AspGrid"
                                EmptyDataText="No Questionnaire's" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle" ShowHeader="false"
                                PageSize="1" AllowPaging="true">
                                <PagerSettings Mode="NextPreviousFirstLast" PageButtonCount="4" FirstPageText="First" PreviousPageText="Previous" NextPageText="Next" LastPageText="Last" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Question ID" SortExpression="QueID" Visible="false">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtGVQueID" runat="server" Text='<%# Bind("QuestionID") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGVQueID" runat="server" Text='<%# Bind("QuestionID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <table class="table">
                                                <tr>
                                                    <td style="color: #2e2e2e; background: #c0d4d7; font-size: 12pt; font-weight: bold;">
                                                        <asp:Label ID="lblGVQuestion" runat="server" Text='<%# Bind("QuestionTitle") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="QuestionOptions">
                                                        <asp:RadioButtonList ID="rblOptions" Font-Bold="false" AutoPostBack="true" runat="server" OnSelectedIndexChanged="rblOptions_SelectedIndexChanged"></asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Left" CssClass="GridPager" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="modal-footer tmsModalContent">
                <div class="row" style="text-align: center">
                    <asp:UpdatePanel ID="upSubmitExamClick" runat="server" style="margin: 10px" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Button ID="btnSubmitExam" runat="server" CssClass="btn btn-approve_popup" Text="Submit Exam" title="Click To Submit the Exam" OnClick="btnSubmitExam_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Exam Passed-->
<div id="modalUcExamPass" class="modal" role="dialog">
    <div class=" modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
            <div class="modal-header tmsModalHeader">
                <h4 class="modal-title Panel_Title">You have been Qualified !</h4>
            </div>
            <div class="modal-body" style="padding: 0px">
                <div class="padding-none" style="padding: 10px">
                    <div class="success_icon col-lg-12 col-sm-12 col-xs-12 col-md-12">
                        <img src="<%=ResolveUrl("~/Images/TMS_Images/ExamPass.gif")%>" alt="Smiley face" style="margin-left: 4pc;" />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnInfoOk" class="btn TMS_Button" data-dismiss="modal" runat="server" onclick="OpenDocExamResultModal();">Ok</button>
            </div>
        </div>
    </div>
</div>
<!-- Exam Fail-->
<div id="modalUcExamFail" class="modal" role="dialog">
    <div class=" modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none">
            <div class="modal-header tmsModalHeader">
                <h4 class="modal-title Panel_Title">Sorry ! You were Failed.</h4>
            </div>
            <div class="modal-body" style="padding: 0px">
                <div class="padding-none" style="padding: 10px">
                    <div class="success_icon col-lg-12 col-sm-12 col-xs-12 col-md-12">
                        <img src="<%=ResolveUrl("~/Images/TMS_Images/ExamFail.gif")%>" alt="Smiley face" style="margin-left: 1pc;" />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="Button1" class="btn TMS_Button" data-dismiss="modal" runat="server">Ok</button>
            </div>
        </div>
    </div>
</div>
<uc1:ucExamResultSheet runat="server" ID="ucExamResultSheet" />

<div id="divServerBtn" style="display: none">
    <asp:UpdatePanel ID="upActionButtons" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnViewExamResult" runat="server" Text="Button" OnClick="btnViewExamResult_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

<script>
    function OpenSopExamModel() {
        // CloseReadDocModelAndCloseDocDetailsModel();
        $('#modelUcExamination').modal({ show: 'true', backdrop: 'static', keyboard: false });
    }
    function CloseExamModelAndOpenCongratulationsPopup() {
        $('#modelUcExamination').modal('hide');
        $('#modalUcExamPass').modal({ backdrop: 'static', keyboard: false });
    }
    function CloseExamModelAndOpenFailurePopup() {
        $('#modelUcExamination').modal('hide');
        $('#modalUcExamFail').modal({ backdrop: 'static', keyboard: false });
    }
    function OpenDocExamResultModal() {
        $("#<%=btnViewExamResult.ClientID%>").click();
    }
    function showNoQuestionsmessage() {
        //DocReadDurationTimer('stop');
        custAlertMsg('Currently there is no Question on this Document to take exam, Ask for Trainer to take exam', 'info', true);
    }
    function messageinCustomPopups() {
        custAlertMsg('You have to Answer all the Questions.', 'error', true);
    }

</script>
