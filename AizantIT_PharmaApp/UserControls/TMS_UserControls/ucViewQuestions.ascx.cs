﻿using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.Web.UI;
using TMS_BusinessLayer;

namespace AizantIT_PharmaApp.UserControls.TMS_UserControls
{
    public partial class ucViewQuestions : System.Web.UI.UserControl
    {
        TMS_BAL objTMS_Bal;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                string msg = "ucViewQuestions_1:" + strline + "  " + strMsg;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "custAlertMsgOnViewQuestionsUC('" + msg + "','error');", true);
            }
        }
        protected void btnSubmitAnswerbyTrainee_Click(object sender, EventArgs e)
        {
            try
            {
                string msg = "", MsgType = "", MsgFor = "Answer";
                objTMS_Bal = new TMS_BAL();
                string QuestionID = hdnQuestionID.Value;
                string EmpID = "0";
                if (HelpClass.IsUserAuthenticated())
                {
                    EmpID = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                    if (txtTrainerAnswer.Value != "" || txtTrainerAnswer.Value != string.Empty)
                    {
                        objTMS_Bal.SubmitAnswertoQuestionbyTrainer(hdnQuestionID.Value, EmpID, txtTrainerAnswer.Value, out int Result);
                        if (Result == 1)
                        {
                            msg = "Answer Submitted Successfully!"; MsgType = "success";
                        }
                        else if (Result == 2)
                        {
                            msg = "Failed to Submit, Since it is already submitted."; MsgType = "info";
                        }
                        else
                        {
                            msg = "Failed to Submit, Please contact Admin."; MsgType = "error";
                        }
                    }
                    else
                    {
                        msg = "Enter Answer";
                        MsgType = "error";
                    }
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "custAlertMsgOnViewQuestionsUC('" + msg + "','" + MsgType + "','" + MsgFor + "');", true);
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }

            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                string msg = "ucViewQuestions_2:" + strline + "  " + strMsg;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "custAlertMsgOnViewQuestionsUC('" + msg + "','error');", true);
            }
        }

        protected void btnSubmitComment_ServerClick(object sender, EventArgs e)
        {
            try
            {
                string MsgFor = "Comment";
                if (txtUATQ_Comment.Value.Trim() != "" || txtUATQ_Comment.Value.Trim() != string.Empty)
                {
                    SubmitComment(txtUATQ_Comment.Value.Trim());
                }
                else
                {
                    string msg = "Enter Comment";
                    string MsgType = "error";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "custAlertMsgOnViewQuestionsUC('" + msg + "','" + MsgType + "','" + MsgFor + "');", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                string msg = "ucViewQuestions_3:" + strline + "  " + strMsg;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "custAlertMsgOnViewQuestionsUC('" + msg + "','error');", true);
            }
        }
        public void SubmitComment(string Comment)
        {
            try
            {
                string msg = "", MsgType = "", MsgFor = "Comment";
                objTMS_Bal = new TMS_BAL();
                string EmpID = "0";
                if (HelpClass.IsUserAuthenticated())
                {
                    EmpID = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                    objTMS_Bal.SubmitCommentToTQ(hdnQuestionID.Value, EmpID, hdnViewerRole.Value, Comment, out int Result);
                    if (Result == 1)
                    {
                        msg = "Comment Submitted Successfully!"; MsgType = "success";
                    }
                    else
                    {
                        msg = "Failed to Submit, Please contact Admin."; MsgType = "error";
                    }
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "custAlertMsgOnViewQuestionsUC('" + msg + "','" + MsgType + "','" + MsgFor + "');", true);
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                string msg = "ucViewQuestions_3:" + strline + "  " + strMsg;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "custAlertMsgOnViewQuestionsUC('" + msg + "','error');", true);
            }
           
        }
        protected void btnAQ_SubmitComment_Click(object sender, EventArgs e)
        {
            string MsgFor = "Comment";
            if (txtATQ_Comment.Value.Trim() != "" || txtATQ_Comment.Value.Trim() != string.Empty)
            {
                SubmitComment(txtATQ_Comment.Value.Trim());
            }
            else
            {
                string msg = "Enter Comment";
                string MsgType = "error";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "custAlertMsgOnViewQuestionsUC('" + msg + "','" + MsgType + "','" + MsgFor + "');", true);
            }
        }
    }
}