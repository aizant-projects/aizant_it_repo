﻿using AizantIT_PharmaApp.Common;
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Data;
using System.Printing;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.UserControls.TMS_UserControls
{
    public partial class JR_TrainingFile : System.Web.UI.UserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (HelpClass.IsUserAuthenticated())
            {
                if (!IsPostBack)
                {
                    DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                    DataRow[] drUMS = dtx.Select("ModuleID=" + (int)Modules.TMS);
                    if (dtx.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0 || dtx.Select("RoleID=" + (int)TMS_UserRole.QA_TMS).Length > 0)
                    {
                        divPrint.Visible = true;
                    }
                    else
                    {
                        divPrint.Visible = false;
                    }
                }
            }
            else
            {
                Response.Redirect("~/UserLogin.aspx", false);
            }
        }
       
        public void PrintTrainingFile(ReportDocument objRpt)
        {
            string dPrinter = "";

            dPrinter = LocalPrintServer.GetDefaultPrintQueue().FullName;

            if (dPrinter == "Microsoft Print to PDF" || dPrinter == "Microsoft XPS Document Writer" || dPrinter.Contains("Send To OneNote"))
            {
                HelpClass.custAlertMsg(this, this.GetType(), "Sorry ! There is no Printer configured to print.", "error");
            }
            else
            {
                objRpt.PrintOptions.PrinterName = dPrinter;
                objRpt.PrintOptions.PrinterDuplex = CrystalDecisions.Shared.PrinterDuplex.Simplex;
                objRpt.PrintToPrinter(1, false, 0, 0);
            }
        }
              
    }
}