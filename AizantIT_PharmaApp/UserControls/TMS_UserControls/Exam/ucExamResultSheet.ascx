﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucExamResultSheet.ascx.cs" Inherits="AizantIT_PharmaApp.UserControls.Exam.ucExamResultSheet" %>
<%@ Register Src="~/UserControls/TMS_UserControls/ucFeedback.ascx" TagPrefix="uc1" TagName="ucFeedback" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
    <ContentTemplate>
        <asp:HiddenField ID="hdnBaseType" runat="server" />
        <asp:HiddenField ID="hdnBaseID" runat="server" Value="0" />
        <asp:HiddenField ID="hdnDocID" runat="server" Value="0" />
        <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
        <asp:HiddenField ID="hdnTraineeID" runat="server" Value="0" />
        <asp:HiddenField ID="hdnJR_ID" runat="server" Value="0" />
        <asp:HiddenField ID="hdnIsFeedbacktoAsk" runat="server" Value="N" />
        <asp:HiddenField ID="hdnIsrbtnEnabled" runat="server" Value="No" />
    </ContentTemplate>
</asp:UpdatePanel>
<div class="modal fade department" id="modelExamResultSheet" role="dialog">
    <div class="modal-dialog" style="min-width: 90%; height: 600px">
        <div class="modal-content">
            <div class="modal-header tmsModalHeader">
                <h4 class="modal-title Panel_Title" style="color: aliceblue">Exam Result</h4>
                <button type="button" class="close" data-dismiss="modal" onclick="OpenFeedBackForm();">&times;</button>
            </div>
            <div class="modal-body tmsModalBody">
                <asp:UpdatePanel ID="upExamResultcontent" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="col-12 float-left form-horizontal padding-none">

                            <div class="col-sm-4 float-left padding-none">
                                <span style="font-weight: bold">Exam Attempted Date:</span>
                                <label class="control-label" style="font-weight: normal" runat="server" id="lblExamDate" title="Exam Date and Time"></label>
                            </div>
                            <div class="col-sm-4 float-left" style="text-align: right; padding-right: 32px">
                                <span style="font-weight: bold">Attempts:</span>
                                <label class="control-label" style="font-weight: normal" runat="server" id="lblExamAttempt" title="Exam Attempts on this Document"></label>
                            </div>
                            <div class="col-sm-4 float-left" style="text-align: right; padding-right: 32px">
                                <span style="font-weight: bold">Marks:</span>
                                <label class="control-label" style="font-weight: normal" runat="server" id="lblMarks" title="Marks Obtained In this Document"></label>
                            </div>

                        </div>
                        <div class="tmsModalContent col-12 float-left" style="overflow: auto; padding: 0px; max-height: 450px;">
                            <asp:GridView ID="gvExamResultSheet" OnRowDataBound="gvExamResultSheet_RowDataBound"
                                DataKeyNames="QuestionID" runat="server" AutoGenerateColumns="False"
                                CssClass="table table-striped AspGrid question_list col-12 float-left"
                                EmptyDataText="No Questionnaire records" EmptyDataRowStyle-CssClass="col-12 float-left   GV_EmptyDataStyle" ShowHeader="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Question ID" SortExpression="QueID" Visible="false">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtGVQueID" runat="server" Text='<%# Bind("QuestionID") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <%--<asp:Label ID="lblGVQueID" runat="server" Text='<%# Bind("QuestionID") %>'></asp:Label>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <table class="table col-12 float-left">
                                                <tr>
                                                    <td style="color: #fff; background: #38b8ba; font-size: 12pt; font-weight: bold;">
                                                        <asp:Label ID="lblGVQuestion" runat="server" Text='<%# Bind("QuestionTitle") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="QuestionOptions">
                                                        <asp:RadioButtonList ID="rblOptions" Font-Bold="false" AutoPostBack="true" runat="server" CssClass="radio_disable"></asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <div class="modal-footer tmsModalContent">
               
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 float-left padding-none">
                        <div class="col-sm-12 float-left ">
                            <div class="col-sm-5 float-left" style="padding-left: 0px">
                                <table>
                                    <tr>
                                        <td style="width: 10px">
                                            <span class="label label-success" style="display: block; padding: 6px; background-color: green;"></span>
                                        </td>
                                        <td>&nbsp;Correct Answer
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-sm-5 float-left">
                                <table>
                                    <tr>
                                        <td style="width: 10px">
                                            <span class="label label-success" style="display: block; padding: 5px; background-color: red;"></span>
                                        </td>
                                        <td>&nbsp;Wrong Answer
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 float-left">
                    <asp:UpdatePanel ID="upExplanation" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 float-left" id="divTrainerExplanation" runat="server">
                                <div class="col-4 float-left" style="padding: 10px">
                                    <asp:Label ID="lblExplanationText" Font-Bold="true" CssClass="capitalize" runat="server" Text="Label"></asp:Label>
                                </div>
                                <div class="float-left " style="padding: 10px">
                                    <asp:RadioButtonList ID="rblWrongAnsExplained" Font-Bold="false" CssClass="rblOptions" CellSpacing="1" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Style="padding: 0px 28px 0px 0px;" Text="Yes" Value="Y"></asp:ListItem>
                                        <asp:ListItem Style="padding: 0px 28px 0px 0px;" Text="No" Value="N" Selected="True"></asp:ListItem>
                                        <asp:ListItem Style="padding: 0px 28px 0px 0px;" Text="N/A" Value="NA"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 float-left bottom" id="divTrainerEvaluate" runat="server" style="text-align: center" visible="false">
                                <input type="button" class="top btn-signup_popup" id="btnTrainerEvaluate" title="Evaluate" onclick="Tocheckradiobtnselected();" value="Evaluate">
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    </div>
                </div>
           
        </div>
    </div>
<asp:UpdatePanel ID="upbtnEvaluate" runat="server" UpdateMode="Always">
    <ContentTemplate>
        <div style="display: none">
            <asp:Button ID="btnYesClick" runat="server" OnClick="btnTrainerEvaluate_Click" Text="Button" />
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

<script>
    function showExamResult(){
        $('#modelExamResultSheet').modal({ show: true, backdrop: 'static', keyboard: false });
    }
    function OpenFeedBackForm() {
        var AskFeedback = $("#<%=hdnIsFeedbacktoAsk.ClientID%>").val();
        if ($("#<%=hdnBaseType.ClientID%>").val() == "2") {
            ReloadSessionDatatables();
        }
        if (AskFeedback == "Y") {
            AskFeedBackOnTraining("Y");
        }
        else {
            AskFeedBackOnTraining("N");
        }
    }
    function ucExamResultSheetShowError(msg, msgType) {
        custAlertMsg(msg, msgType, true);
    }
    function CloseAS_ReloadTrainedDocs() {
        $('#modelExamResultSheet').modal('hide');
        loadJrRegularRecords();
        ShowPleaseWait('hide');
    }
    function CloseAS_ReloadTraineeSessions() {
        $('#modelExamResultSheet').modal('hide');
        ReloadTraineeSessions();
        ShowPleaseWait('hide');
    }
    function trainerJrEvaluated() {
        custAlertMsg('Evaluated Successfully', 'success', 'CloseAS_ReloadTrainedDocs()');
    }
    function trainerTargetEvaluated() {
        custAlertMsg('Evaluated Successfully', 'success', 'CloseAS_ReloadTraineeSessions()');
    }
    function btnclick() {
        $("#<%=btnYesClick.ClientID%>").click();
        ShowPleaseWait('show');
    }
    function Tocheckradiobtnselected() {
        if ($("#<%=hdnIsrbtnEnabled.ClientID %>").val() == "Yes") {
            if ($("input[name='<%=rblWrongAnsExplained.UniqueID%>']:radio:checked").val() != "Y") {
                custAlertMsg("Have you explained wrongly answered questions ?", "confirm", "btnclick();");
            }
            else {
                btnclick();
            }
        }
        else {
            btnclick();
        }
    }   
</script>
