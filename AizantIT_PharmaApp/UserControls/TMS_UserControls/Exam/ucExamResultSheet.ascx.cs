﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.UserControls.Exam
{
    public partial class ucExamResultSheet : System.Web.UI.UserControl
    {
        TMS_BAL objTMS_Bal;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }

        public void showExamResult(int JrID, int BaseShowType, int DocID, bool IsEvaluation = false, int EmpID=0)
        {
            hdnBaseID.Value = JrID.ToString();           
            hdnBaseType.Value = ((int)TmsBaseType.JR_Training).ToString();
            
            hdnDocID.Value = DocID.ToString();
            hdnEmpID.Value = EmpID.ToString();

            objTMS_Bal = new TMS_BAL();
            DataSet dsResult = objTMS_Bal.getExamResult(JrID, BaseShowType, DocID);
            DataTable dtResult = dsResult.Tables[0];
            if (dtResult.Rows.Count > 0)
            {
                DataRow dr = dtResult.Rows[0];
                lblExamDate.InnerText = dr["ExamAttemptedDateAndTime"].ToString();
                lblMarks.InnerText = dr["Marks"].ToString();
                lblExamAttempt.InnerText = dr["Attempts"].ToString();
            }
            ViewState["Questions"] = dsResult.Tables[1];
            ViewState["Options"] = dsResult.Tables[2];
            bool NoWrongAnswers = true;
            foreach (DataRow itemDr in dsResult.Tables[1].Rows)
            {
                if (itemDr["Answer"].ToString() != itemDr["EmpAnswer"].ToString())
                {
                    NoWrongAnswers = false;
                }
            }
            if (NoWrongAnswers)
            {
                rblWrongAnsExplained.SelectedValue = "NA";
                rblWrongAnsExplained.Enabled = false;
                hdnIsrbtnEnabled.Value = "No";
            }
            else
            {
                rblWrongAnsExplained.Enabled = true;
                hdnIsrbtnEnabled.Value = "Yes";
            }
            if (dsResult.Tables[1].Rows.Count > 0)
            {
                gvExamResultSheet.DataSource = dsResult.Tables[1];
                gvExamResultSheet.DataBind();
            }
            else
            {
                gvExamResultSheet.DataSource = new List<object>();
                gvExamResultSheet.DataBind();
            }
            switch (BaseShowType)
            {              
                case 1:
                    if (IsEvaluation)
                    {
                        divTrainerExplanation.Visible = true;
                        divTrainerEvaluate.Visible = true;
                        lblExplanationText.Text = "Are You explained wrongly answered questions to the Trainee?";
                    }
                    else
                    {
                        divTrainerExplanation.Visible = false;
                    }
                    break;
                case 2:
                    if (IsEvaluation)
                    {
                        DataTable dt = dsResult.Tables[3];
                        if (dt.Rows.Count > 0)
                        {
                            lblExplanationText.Text = "'"+dt.Rows[0]["Trainer"].ToString() + "' explained wrongly answered questions to '"+ dt.Rows[0]["Trainee"].ToString() + "'";
                        }
                        rblWrongAnsExplained.SelectedValue = dsResult.Tables[4].Rows[0]["ExplainationStatus"].ToString();
                        divTrainerExplanation.Visible = true;
                        rblWrongAnsExplained.Enabled = false;
                        upExplanation.Update();
                    }
                    else
                    {
                        divTrainerExplanation.Visible = false;
                    }
                    break;               
            }
            upExamResultcontent.Update();
            upExplanation.Update();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "modal", "showExamResult();", true);
        }

        public void showExamResult(int TrainingSessionID,int TargetExamID, string TraineeID, 
            bool IsTrainerEvaluation = false, int EmpID=0,bool IsClassRoom=true)
        {
            hdnBaseID.Value = TrainingSessionID.ToString();            
            hdnBaseType.Value = ((int)TmsBaseType.Target_Training).ToString();
            hdnTraineeID.Value = TraineeID;
                                   
            hdnEmpID.Value = EmpID.ToString();

            objTMS_Bal = new TMS_BAL();
            int BaseShowType = IsTrainerEvaluation == true ? 3 : 4;
            DataSet dsResult = objTMS_Bal.getExamResult(TargetExamID, BaseShowType, 0); //3- Represents Target
            DataTable dtResult = dsResult.Tables[0];
            if (dtResult.Rows.Count > 0)
            {
                DataRow dr = dtResult.Rows[0];
                lblExamDate.InnerText = dr["ExamAttemptedDateAndTime"].ToString();
                lblMarks.InnerText = dr["Marks"].ToString();
                lblExamAttempt.InnerText = dr["Attempts"].ToString();
            }
            ViewState["Questions"] = dsResult.Tables[1];
            ViewState["Options"] = dsResult.Tables[2];
            bool NoWrongAnswers = true;
            foreach (DataRow itemDr in dsResult.Tables[1].Rows)
            {
                if (itemDr["Answer"].ToString() != itemDr["EmpAnswer"].ToString())
                {
                    NoWrongAnswers = false;
                }
            }
            if (NoWrongAnswers)
            {
                rblWrongAnsExplained.SelectedValue = "NA";
                rblWrongAnsExplained.Enabled = false;
                hdnIsrbtnEnabled.Value = "No";
            }
            else
            {
                rblWrongAnsExplained.Enabled = true;
                hdnIsrbtnEnabled.Value = "Yes";
            }
            if (dsResult.Tables[1].Rows.Count > 0)
            {
                gvExamResultSheet.DataSource = dsResult.Tables[1];
                gvExamResultSheet.DataBind();
            }
            else
            {
                gvExamResultSheet.DataSource = new List<object>();
                gvExamResultSheet.DataBind();
            }
            if (IsClassRoom)
            {
                if (IsTrainerEvaluation)
                {
                    divTrainerExplanation.Visible = true;
                    divTrainerEvaluate.Visible = true;
                    lblExplanationText.Text = "Are You explained wrongly answered questions to the Trainee?";
                }
                else
                {
                    DataTable dt = dsResult.Tables[3];
                    if (dt.Rows.Count > 0)
                    {
                        lblExplanationText.Text = "'" + dt.Rows[0]["Trainer"].ToString() + "' explained wrongly answered questions to '" + dt.Rows[0]["Trainee"].ToString() + "'";
                    }
                    rblWrongAnsExplained.SelectedValue = dsResult.Tables[4].Rows[0]["ExplanationStatus"].ToString();
                    divTrainerExplanation.Visible = true;
                    rblWrongAnsExplained.Enabled = false;
                    upExplanation.Update();
                }
            }
            else
            {
                divTrainerExplanation.Visible = false;
                divTrainerEvaluate.Visible = false;
            }              
            upExamResultcontent.Update();
            upExplanation.Update();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "modal", "showExamResult();", true);
        }       

        public void showExamResult(DataTable dtQuestions, DataTable dtOption, string marksObtained, string IsFeedbackToAsk,string BaseType,string Attempts)
        {
            hdnBaseType.Value = BaseType;
            lblExamDate.InnerText = DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
            lblExamAttempt.InnerText = Attempts;
            lblMarks.InnerText = marksObtained;
            ViewState["Questions"] = dtQuestions;
            ViewState["Options"] = dtOption;
            gvExamResultSheet.DataSource = dtQuestions;
            gvExamResultSheet.DataBind();
            divTrainerExplanation.Visible = false;
            rblWrongAnsExplained.Enabled = false;
            upExamResultcontent.Update();
            upExplanation.Update();
            if (IsFeedbackToAsk == "Y")
            {
                hdnIsFeedbacktoAsk.Value = "Y";
            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "modal", "showExamResult();", true);
        }

        protected void gvExamResultSheet_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string QuestionID = gvExamResultSheet.DataKeys[e.Row.RowIndex].Value.ToString();
                    RadioButtonList rblOptions = e.Row.FindControl("rblOptions") as RadioButtonList;
                    DataTable dtOptions = new DataTable();
                    dtOptions = (DataTable)ViewState["Options"];
                    DataView dvOptions = new DataView(dtOptions);
                    dvOptions.RowFilter = "QuestionID=" + QuestionID;

                    DataTable dtTemp = new DataTable();
                    dtTemp = dvOptions.ToTable();
                    rblOptions.DataSource = dtTemp;
                    rblOptions.DataTextField = "OptionTitle";
                    rblOptions.DataValueField = "Option";
                    rblOptions.DataBind();

                    DataTable dtQuestions = new DataTable();
                    dtQuestions = (DataTable)ViewState["Questions"];
                    foreach (DataRow dr in dtQuestions.Rows)
                    {
                        if (dr["QuestionID"].ToString() == QuestionID)
                        {
                            if (dr["EmpAnswer"].ToString() != "")
                            {
                                rblOptions.Items.FindByValue(dr["EmpAnswer"].ToString()).Selected = true;

                                if (dr["EmpAnswer"].ToString() == dr["Answer"].ToString())
                                {
                                    rblOptions.Items.FindByValue(dr["EmpAnswer"].ToString()).Attributes.Add("class", "correct");
                                }
                                else
                                {
                                    rblOptions.Items.FindByValue(dr["EmpAnswer"].ToString()).Attributes.Add("class", "wrong");
                                    rblOptions.Items.FindByValue(dr["Answer"].ToString()).Attributes.Add("class", "correct");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "modal", "ucExamResultSheetShowError('" + ex.Message + "','error');", true);
            }
        }

        public void TrainerEvaluation()
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                int EvaluateResult = 0;
                string BaseType = hdnBaseType.Value;

                if (BaseType == "1")
                {
                    objTMS_Bal.EvalutedJrByTrainer(Convert.ToInt32(hdnBaseID.Value), Convert.ToInt32(hdnDocID.Value),
                    Convert.ToInt32(hdnEmpID.Value), rblWrongAnsExplained.SelectedValue, out EvaluateResult);
                }
                else
                {
                    int Opinion = 1, ExplanationStatus = 0;
                    switch (rblWrongAnsExplained.SelectedValue)
                    {
                        case "Y":
                            ExplanationStatus = 1;
                            break;
                        case "N":
                            ExplanationStatus = 2;
                            break;
                        default:
                            ExplanationStatus = 0;
                            break;
                    }
                    string Remarks = "";
                    objTMS_Bal.EvaluateTargetTrainee(Convert.ToInt32(hdnBaseID.Value),
                        Convert.ToInt32(hdnTraineeID.Value), Convert.ToInt32(hdnEmpID.Value), out EvaluateResult, Opinion, ExplanationStatus, Remarks);
                }
                string OnEvaluationSucess = BaseType == "1" ? "trainerJrEvaluated();" : "trainerTargetEvaluated();";
                if (EvaluateResult == 1)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", OnEvaluationSucess, true);
                }
                else if (EvaluateResult == 2)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", "custAlertMsg('Already Evaluated','info');", true);
                }
                else if (EvaluateResult == 3)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", "custAlertMsg('Trainee is in-active','info');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", "custAlertMsg('System Error Occured','error');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "modal", "ucExamResultSheetShowError('" + ex.Message + "','error');", true);
            }
        }

        protected void btnTrainerEvaluate_Click(object sender, EventArgs e)
        {
            try
            {
                TrainerEvaluation();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "modal", "ucExamResultSheetShowError('" + ex.Message + "','error');", true);
            }
        }

    }
}