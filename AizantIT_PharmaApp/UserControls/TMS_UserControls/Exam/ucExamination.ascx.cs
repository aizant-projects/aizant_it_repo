﻿using AizantIT_PharmaApp.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.UserControls.TMS_UserControls.Exam
{
    public partial class ucExamination : System.Web.UI.UserControl
    {
        TMS_BAL objTMS_Bal;
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        #region Get Exam Paper
        public void GetExamPaper(string BaseID, string BaseType, string DocID = "0", string TraineeID = "0")
        {
            try
            {
                hdnBaseID.Value = BaseID;
                hdnBaseType.Value = BaseType;
                hdnDocID.Value = DocID;
                hdnTraineeID.Value = TraineeID;
                objTMS_Bal = new TMS_BAL();
                if (hdnBaseType.Value=="2")
                {
                    objTMS_Bal.GetTTS_StatusClose(hdnBaseID.Value, out int Result);
                    if (Result == 1)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "OpenTTSCloseValidation();", true);
                    }
                    else
                    {
                        GetDocQuestionnaire(DocID);
                    }
                }
                else
                {
                    GetDocQuestionnaire(DocID);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                string msg = "ucExam_2:" + strline + "  " + strMsg;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "messageinCustomPopups('" + msg + "','error');", true);
            }
        }

        private void GetDocQuestionnaire(string DocID)
        {
            DataSet ds = objTMS_Bal.getSopQuestionnaireForExam(DocID, out int ExamType);
            if (ExamType > 0)
            {
                gvExamQuestionnaire.DataSource = new List<object>();
                gvExamQuestionnaire.DataBind();
                DataTable dtSopQuestion = ds.Tables[0];
                if (dtSopQuestion.Rows.Count > 0)
                {
                    ViewState["Questions"] = ds.Tables[0];
                    ViewState["Options"] = ds.Tables[1];
                    if (dtSopQuestion.Rows.Count > 0)
                    {
                        gvExamQuestionnaire.DataSource = dtSopQuestion;
                        gvExamQuestionnaire.DataBind();
                    }
                    else
                    {
                        gvExamQuestionnaire.DataSource = new List<object>();
                        gvExamQuestionnaire.DataBind();
                    }
                    upExamQuestionnaire.Update();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "OpenSopExamModel();", true);
                }
                else
                {

                }
            }
            else
            {
                //code for confirmation and update DB table question type to 2 in Sop_Assigned to JR
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "showNoQuestionsmessage();", true);
            }
        }

        protected void gvExamQuestionnaire_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    objTMS_Bal = new TMS_BAL();
                    string QuestionID = gvExamQuestionnaire.DataKeys[e.Row.RowIndex].Value.ToString();
                    RadioButtonList rblOptions = e.Row.FindControl("rblOptions") as RadioButtonList;
                    DataTable dtOptions = new DataTable();
                    dtOptions = (DataTable)ViewState["Options"];
                    DataView dvOptions = new DataView(dtOptions);
                    dvOptions.RowFilter = "QuestionID=" + QuestionID;

                    rblOptions.DataSource = dvOptions.ToTable();
                    rblOptions.DataTextField = "OptionTitle";
                    rblOptions.DataValueField = "Option";
                    rblOptions.DataBind();

                    DataTable dtQuestions = new DataTable();
                    dtQuestions = (DataTable)ViewState["Questions"];
                    foreach (DataRow dr in dtQuestions.Rows)
                    {
                        if (dr["QuestionID"].ToString() == QuestionID)
                        {
                            if (dr["EmpAnswer"].ToString() != "")
                            {
                                rblOptions.Items.FindByValue(dr["EmpAnswer"].ToString()).Selected = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                string msg = "ucExam_3:" + strline + "  " + strMsg;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "messageinCustomPopups('" + msg + "','error');", true);
            }
        }
        protected void rblOptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow gvRow = (GridViewRow)((RadioButtonList)sender).NamingContainer;
                RadioButtonList rblOptions = (RadioButtonList)gvRow.FindControl("rblOptions");
                string option = rblOptions.SelectedValue;
                int selectRowIndex = gvRow.RowIndex;
                Label lbQueId = gvRow.FindControl("lblGVQueID") as Label;
                string QueID = lbQueId.Text;

                DataTable dtQuestions = ViewState["Questions"] as DataTable;
                foreach (DataRow dr in dtQuestions.Rows)
                {
                    if (dr["QuestionID"].ToString() == QueID)
                    {
                        dr["EmpAnswer"] = option;
                    }
                }
                ViewState["Questions"] = dtQuestions;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                string msg = "ucExam_4:" + strline + "  " + strMsg;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "messageinCustomPopups('" + msg + "','error');", true);
            }
        }
        protected void gvExamQuestionnaire_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvExamQuestionnaire.PageIndex = e.NewPageIndex;
                DataTable dtQuestions = ViewState["Questions"] as DataTable;
                gvExamQuestionnaire.DataSource = dtQuestions;
                gvExamQuestionnaire.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                string msg = "ucExam_5:" + strline + "  " + strMsg;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "messageinCustomPopups('" + msg + "','error');", true);
            }
        }
        #endregion

        //public event EventHandler buttonClick;
        protected void btnSubmitExam_Click(object sender, EventArgs e)
        {
            try
            {
                objTMS_Bal = new TMS_BAL();
                objTMS_Bal.GetTTS_StatusClose(hdnBaseID.Value, out int OutResult);
                if (hdnBaseType.Value=="2")
                {
                    if (OutResult == 1)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "OpenTTSCloseValidation();", true);
                    }
                    else
                    {
                        SubmitExam();
                    }
                }
                else
                {
                    SubmitExam();
                }
                
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                string msg = "ucExam_6:" + strline + "  " + strMsg;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "messageinCustomPopups('" + msg + "','error');", true);
            }
        }

        private void SubmitExam()
        {
            bool PassedOutStatus = false;
            int Marks = 0;
            int Attempts = 1;
            double Requiredresult = 0;
            objTMS_Bal = new TMS_BAL();
            DataTable dtTraineeAnswerSheet = new DataTable();
            dtTraineeAnswerSheet.Columns.Add("QuestionID");
            dtTraineeAnswerSheet.Columns.Add("TraineeAnswer");
            DataRow drTraineeAnswerSheet;
            DataTable dtSopQuestionnaire = new DataTable();
            dtSopQuestionnaire = (DataTable)ViewState["Questions"];
            if (dtSopQuestionnaire.Rows.Count > 0)
            {
                foreach (DataRow dr in dtSopQuestionnaire.Rows)
                {
                    if (dr["EmpAnswer"].ToString() != "")
                    {
                        drTraineeAnswerSheet = dtTraineeAnswerSheet.NewRow();
                        drTraineeAnswerSheet["QuestionID"] = dr["QuestionID"].ToString();
                        drTraineeAnswerSheet["TraineeAnswer"] = dr["EmpAnswer"].ToString();
                        dtTraineeAnswerSheet.Rows.Add(drTraineeAnswerSheet);
                    }
                    if (dr["Answer"].ToString() == dr["EmpAnswer"].ToString())
                    {
                        Marks = Marks + 1;
                    }
                }
                Requiredresult = 0.8 * dtSopQuestionnaire.Rows.Count;
                if (Requiredresult <= Marks)
                {
                    PassedOutStatus = true;
                }
                if (dtSopQuestionnaire.Rows.Count == dtTraineeAnswerSheet.Rows.Count)
                {
                    string msg = "";
                    if (PassedOutStatus == true)
                    {
                        msg = "Congratulations! You have been Qualified!";
                    }
                    else
                    {
                        msg = "Sorry! You Failed in this SOP.";
                    }
                    string ExplainedStatus = "";
                    if (dtSopQuestionnaire.Rows.Count == Marks)
                    {
                        ExplainedStatus = "NA";
                    }
                    else
                    {
                        ExplainedStatus = "N";
                    }

                    hdnMarksGained.Value = Marks.ToString();
                    objTMS_Bal.SubmitExamResult(hdnBaseID.Value, PassedOutStatus, Marks, dtTraineeAnswerSheet, ExplainedStatus, hdnBaseType.Value, out int Result, out Attempts, hdnDocID.Value, hdnTraineeID.Value);
                    hdnExamAttempts.Value = Attempts.ToString();
                    if (PassedOutStatus == true && Result!=4)
                    {
                        if (hdnBaseType.Value == ((int)TmsBaseType.JR_Training).ToString())
                        {
                            if (Result == 1) //Represents last sop of that JRTraining and Retrospective is submitted if there .
                            {
                                hdnIsAskFeedback.Value = "Y";
                            }
                            else
                            {
                                hdnIsAskFeedback.Value = "N";
                            }
                        }
                        else //for Target training always ask feed back
                        {
                            hdnIsAskFeedback.Value = "Y";
                        }
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "CloseExamModel('Success');", true);
                    }
                   else if (PassedOutStatus == false  )
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "CloseExamModel('Fail');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "CloseExamModel('Error');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "messageinCustomPopups('You have to Answer all the Questions.','error');", true);
                }
            }
        }

        protected void btnViewExamResult_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtDocQuestion = new DataTable();
                dtDocQuestion = (DataTable)ViewState["Questions"];
                DataTable dtDocOption = (DataTable)ViewState["Options"];
                if (dtDocQuestion.Rows.Count >= 0)
                {
                    ucExamResultSheet.showExamResult(dtDocQuestion, dtDocOption, hdnMarksGained.Value, hdnIsAskFeedback.Value,hdnBaseType.Value,hdnExamAttempts.Value);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                string msg = "ucExam_7:" + strline + "  " + strMsg;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "messageinCustomPopups('" + msg + "','error');", true);
            }
        }
    }
}