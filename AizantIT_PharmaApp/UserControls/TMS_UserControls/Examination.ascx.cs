﻿using AizantIT_PharmaApp.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BusinessLayer;

namespace AizantIT_PharmaApp.UserControls.TMS_UserControls
{
    public partial class Examination : System.Web.UI.UserControl
    {
        TMS_BAL objTMS_Bal;
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        #region Get Exam Paper
        public void GetExamPaper(string BaseID, string BaseType, string DocID = "0")
        {
            try
            {
                hdnBaseID.Value = BaseID;
                hdnBaseType.Value = BaseType;
                hdnDocID.Value = DocID;

                objTMS_Bal = new TMS_BAL();
                DataSet ds = objTMS_Bal.getSopQuestionnaireForExam(DocID, out int ExamType);
                if (ExamType > 0)
                {
                    gvExamQuestionnaire.DataSource = new List<object>();
                    gvExamQuestionnaire.DataBind();
                    DataTable dtSopQuestion = ds.Tables[0];
                    if (dtSopQuestion.Rows.Count > 0)
                    {
                        ViewState["Questions"] = ds.Tables[0];
                        ViewState["Options"] = ds.Tables[1];
                        if (dtSopQuestion.Rows.Count > 0)
                        {
                            gvExamQuestionnaire.DataSource = dtSopQuestion;
                            gvExamQuestionnaire.DataBind();
                        }
                        else
                        {
                            gvExamQuestionnaire.DataSource = new List<object>();
                            gvExamQuestionnaire.DataBind();
                        }
                        upExamQuestionnaire.Update();
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "OpenSopExamModel();", true);
                    }
                    else
                    {

                    }
                }
                else
                {
                    //code for confirmation and update DB table question type to 2 in Sop_Assigned to JR
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "showNoQuestionsModal();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "ExamUC-13:" + strline + "  " + strMsg, "error");
            }
        }
        protected void gvExamQuestionnaire_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    objTMS_Bal = new TMS_BAL();
                    string QuestionID = gvExamQuestionnaire.DataKeys[e.Row.RowIndex].Value.ToString();
                    RadioButtonList rblOptions = e.Row.FindControl("rblOptions") as RadioButtonList;
                    DataTable dtOptions = new DataTable();
                    dtOptions = (DataTable)ViewState["Options"];
                    DataView dvOptions = new DataView(dtOptions);
                    dvOptions.RowFilter = "QuestionID=" + QuestionID;

                    rblOptions.DataSource = dvOptions.ToTable();
                    rblOptions.DataTextField = "OptionTitle";
                    rblOptions.DataValueField = "Option";
                    rblOptions.DataBind();

                    DataTable dtQuestions = new DataTable();
                    dtQuestions = (DataTable)ViewState["Questions"];
                    foreach (DataRow dr in dtQuestions.Rows)
                    {
                        if (dr["QuestionID"].ToString() == QuestionID)
                        {
                            if (dr["EmpAnswer"].ToString() != "")
                            {
                                rblOptions.Items.FindByValue(dr["EmpAnswer"].ToString()).Selected = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_T-13:" + strline + "  " + strMsg, "error");
            }
        }
        protected void rblOptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow gvRow = (GridViewRow)((RadioButtonList)sender).NamingContainer;
                RadioButtonList rblOptions = (RadioButtonList)gvRow.FindControl("rblOptions");
                string option = rblOptions.SelectedValue;
                int selectRowIndex = gvRow.RowIndex;
                Label lbQueId = gvRow.FindControl("lblGVQueID") as Label;
                string QueID = lbQueId.Text;

                DataTable dtQuestions = ViewState["Questions"] as DataTable;
                foreach (DataRow dr in dtQuestions.Rows)
                {
                    if (dr["QuestionID"].ToString() == QueID)
                    {
                        dr["EmpAnswer"] = option;
                    }
                }
                ViewState["Questions"] = dtQuestions;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_T-14:" + strline + "  " + strMsg, "error");
            }
        }
        protected void gvExamQuestionnaire_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvExamQuestionnaire.PageIndex = e.NewPageIndex;
                DataTable dtQuestions = ViewState["Questions"] as DataTable;
                gvExamQuestionnaire.DataSource = dtQuestions;
                gvExamQuestionnaire.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_T-15:" + strline + "  " + strMsg, "error");
            }
        }
        #endregion

        //public event EventHandler buttonClick;
        protected void btnSubmitExam_Click(object sender, EventArgs e)
        {
            try
            {
                bool PassedOutStatus = false;
                int Marks = 0;
                double Requiredresult = 0;
                objTMS_Bal = new TMS_BAL();
                DataTable dtTraineeAnswerSheet = new DataTable();
                dtTraineeAnswerSheet.Columns.Add("QuestionID");
                dtTraineeAnswerSheet.Columns.Add("TraineeAnswer");
                DataRow drTraineeAnswerSheet;
                DataTable dtSopQuestionnaire = new DataTable();
                dtSopQuestionnaire = (DataTable)ViewState["Questions"];
                if (dtSopQuestionnaire.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtSopQuestionnaire.Rows)
                    {
                        if (dr["EmpAnswer"].ToString() != "")
                        {
                            drTraineeAnswerSheet = dtTraineeAnswerSheet.NewRow();
                            drTraineeAnswerSheet["QuestionID"] = dr["QuestionID"].ToString();
                            drTraineeAnswerSheet["TraineeAnswer"] = dr["EmpAnswer"].ToString();
                            dtTraineeAnswerSheet.Rows.Add(drTraineeAnswerSheet);
                        }
                        if (dr["Answer"].ToString() == dr["EmpAnswer"].ToString())
                        {
                            Marks = Marks + 1;
                        }
                    }
                    Requiredresult = 0.8 * dtSopQuestionnaire.Rows.Count;
                    if (Requiredresult <= Marks)
                    {
                        PassedOutStatus = true;
                    }
                    if (dtSopQuestionnaire.Rows.Count == dtTraineeAnswerSheet.Rows.Count)
                    {
                        string msg = "";
                        if (PassedOutStatus == true)
                        {
                            msg = "Congratulations! You have been Qualified!";
                        }
                        else
                        {
                            msg = "Sorry! You Failed in this SOP.";
                        }
                        string ExplainedStatus = "";
                        if (dtSopQuestionnaire.Rows.Count == Marks)
                        {
                            ExplainedStatus = "NA";
                        }
                        else
                        {
                            ExplainedStatus = "N";
                        }
                        string JR_ID = hdnBaseID.Value; //if bsetype =JR
                        hdnMarksGained.Value = Marks.ToString();
                        int Count = objTMS_Bal.SubmitExamResult(JR_ID, hdnDocID.Value, PassedOutStatus, Marks, dtTraineeAnswerSheet, ExplainedStatus, out int Result);
                        //string msgType = "", Outmsg = "";
                        //switch (Result)
                        //{
                        //    case 1:
                        //        msg = "Completed all the Documents along with Retrospective.";
                        //        msgType = "success";
                        //        break;
                        //    case 2:
                        //        msg = "Completed all the Documents, Retrospective is in pending.";
                        //        msgType = "success";
                        //        break;
                        //    case 3:
                        //        msg = "Submitted Sucessfully!, There are some more Documents to complete";
                        //        msgType = "info";
                        //        break;
                        //    default:
                        //        msg = "Unable to perform the action, Contact Admin.";
                        //        msgType = "info";
                        //        break;
                        //}
                        //HelpClass.custAlertMsg(this, this.GetType(), msg, msgType);
                        if (Result == 1)
                        {
                            // txtJR_Status.Text = "JR at Trainer Evaluation";

                         // Open the Feedback of JR if it is in active mode
                        }
                        else if (Result == 2) 
                        {
                            // Completed all the Documents , But retrospective is in pending.
                        }
                        else if (Result == 3)
                        {
                            //Still there are some more documents to complete.
                        }
                        if (PassedOutStatus == true)
                        {
                            if (Result == 1)
                            {
                                hdnIsAskFeedback.Value = "Y";
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "CloseExamModelAndOpenCongratulationsPopup();", true);
                            }

                        }
                        if (PassedOutStatus == false)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "CloseExamModelAndOpenFailurePopup();", true);
                        }
                    }
                    else
                    {
                        // ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('You have to Attempt all the Question.');", true);
                        //HelpClass.custAlertMsg(this, this.GetType(), "You have to Answer all the Questions.", "error");
                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "custAlertMsg('You have to Answer all the Questions.','error');", true);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "messageinCustomPopups();", true);
                    }
                }
                else
                {

                }
              
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "JR_T-16:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnViewExamResult_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtDocQuestion = new DataTable();
                dtDocQuestion = (DataTable)ViewState["Questions"];
                DataTable dtDocOption = (DataTable)ViewState["Options"];
                if (dtDocQuestion.Rows.Count >= 0)
                {
                    ucExamResultSheet.showExamResult(dtDocQuestion, dtDocOption, hdnMarksGained.Value, hdnIsAskFeedback.Value);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}