﻿using System;

namespace AizantIT_PharmaApp.UserControls
{
    public partial class SucessAlert : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public event EventHandler buttonClick1,buttonNoClick;

       
        protected void btnYes_Click1(object sender, EventArgs e)
        {
            buttonClick1(sender, e);
        }

        protected void btnNo_Click(object sender, EventArgs e)
        {
            buttonNoClick(sender, e);
        }
    }
}