﻿using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using UMS_BusinessLayer;

namespace AizantIT_PharmaApp.UserControls
{
    public partial class ElectronicSign : System.Web.UI.UserControl
    {
        UMS_BAL objUMS_BAL = new UMS_BAL();
        private bool _Pwd;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserDetails"]!=null)
                {
                    DataTable dtx = (Session["UserDetails"] as DataSet).Tables[0];
                    lblLoginID.Text = dtx.Rows[0]["EmpLoginID"].ToString();
                }
            }
        }

        public event EventHandler buttonClick;
        
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            buttonClick(sender, e);
        }

        public bool IsPasswordValid {
            get { return CheckPwd(); }

            set { _Pwd = value; }
        }

        private bool CheckPwd()
        {
            int EmpID=0;
            if (HelpClass.IsUserAuthenticated())
            {
               EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            }

            objUMS_BAL.ElectronicSignVerification(txtPassword.Text.Trim(), EmpID, out int SignVerified);
            if(SignVerified==1)
            {
                return true;
            }
            else
            {
                return false;
            }
           
        }
    }
}