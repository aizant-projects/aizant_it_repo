﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ElectronicSign.ascx.cs" Inherits="AizantIT_PharmaApp.UserControls.ElectronicSign" %>

<style>
    .button_UcSign {
        background-color: #098293;
        border: none;
        color: white;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 14px;
        -webkit-transition-duration: 0.4s; /* Safari */
        transition-duration: 0.4s;
        cursor: pointer;
    }

        .button_UcSign:hover {
            background-color: #0173b2;
            border: none;
            color: white;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 14px;
            -webkit-transition-duration: 0.4s; /* Safari */
            transition-duration: 0.4s;
            cursor: pointer;
        }
</style>

<script>
    function focusOnElecPwd() {
        $('#<%=txtPassword.ClientID%>').focus();
    }
    function openUC_ElectronicSign() {
        document.getElementById("<%=txtPassword.ClientID%>").value = '';
        $('#usES_Modal').modal({ backdrop: 'static', keyboard: false });
        $('#usES_Modal').on('shown.bs.modal', function () {
            $('#<%=txtPassword.ClientID%>').focus();
        });
    }
</script>
<div class="col-12 float-left padding-none" style="z-index:-1;">
<input type="search" tabindex="-1" style="opacity: 0;" />
</div>

<!-- Modal -->
<div class="modal fade" id="usES_Modal" role="dialog">
    <div class="modal-dialog modal-md modal-dialog-centered">
        <div class="modal-content">
            <asp:UpdatePanel ID="upE_Sign" UpdateMode="Always" runat="server">
                <ContentTemplate>
                    <div class="modal-header" style="padding: 10px; background-color: #098293; color: #ffeded; border: 2px solid #07889a;">
                        
                        <h5 class="modal-title">Electronic Signature</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" style="padding: 10px">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-horizontal" style="text-align: right">
                                    <div class="form-group bottom">
                                        <label class=" col-sm-3 float-left" style="text-align: right !important;">Login Id:</label>
                                        <div class="col-sm-8 float-left" style="text-align: left">
                                            <asp:Label ID="lblLoginID" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                    <div class="form-group top" style="margin-bottom: 0px">
                                        <label class=" col-sm-3  float-left" style="text-align: right !important;">Password:</label>
                                        <div class="col-sm-8  float-left">
                                            <asp:TextBox ID="txtPassword" onpaste="return false;" CssClass="login_input_sign_up form-control" TabIndex="100" TextMode="Password" runat="server" onkeypress="return PasswordKeypress(event);" placeholder="Enter Password" ToolTip="Signature"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer text-right" style="padding: 10px">
                        <asp:Button ID="btnSubmit" runat="server" TabIndex="101" OnClick="btnSubmit_Click" OnClientClick="return validatePwd();" CssClass=" btn-signup_popup" Text="Confirm" />
                        <button type="button" id="ElectnicsCancel" tabindex="102" class="float-right  btn-cancel_popup" data-dismiss="modal">Cancel</button>
                        
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
<script>
          //onkeydown = "return (event.keyCode!=13);"
          function PasswordKeypress(event) {
              if (event.keyCode == 13) {
                  $("#<%=btnSubmit.ClientID%>").click();
                  return false;
              }
              else {
                  return true;
              }
          }
</script>
<script>
        function validatePwd() {
            var txtPwd = document.getElementById("<%=txtPassword.ClientID%>");
            if (txtPwd.value != "") {
                setTimeout(function () { $('#<%=btnSubmit.ClientID %>').attr("disabled", true); }, 0);
                return true;
            }
            else {
                custAlertMsg("Enter Password.", "error", "focusOnElecPwd();");
                //$('#<%=txtPassword.ClientID%>').focus();
                return false;
            }
        }

</script>


