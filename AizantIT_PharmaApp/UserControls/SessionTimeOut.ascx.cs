﻿using System;
using System.Web.UI;
using System.Data;
using UMS_BusinessLayer;

namespace AizantIT_PharmaApp.UserControls
{
    public partial class SessionTimeOut : System.Web.UI.UserControl
    {
        // TMS_BAL objTMS_BAL = new TMS_BAL();

        UMS_BAL objUMS_BAL=new UMS_BAL(); 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["SuperUser"] == null)
                {
                    SessionTimeOutWarningMessage();
                }
            }            
        }
        protected void btnSessionKill_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("~/UserLogin.aspx");
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            UpdateSessionTimeOut();
        }

        public void UpdateSessionTimeOut()
        {
            Session.Timeout = Convert.ToInt32(hdfResult.Value);
            //hdfResult.Value = dt.Rows[0]["SessionTime"].ToString();

            //Adding SessionTimeout value from db to cookie.
            // HttpCookie SessionTO_Cookie = new HttpCookie("CookieIdleSessionTimeout");
            //SessionTO_Cookie.Value = hdfResult.Value;
            //HttpCookie SessionTO_Cookie = new HttpCookie("SessionTimeOut", hdfResult.Value);
            //Response.Cookies.Add(SessionTO_Cookie);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionAlert", "SessionTimer('restart');", true);
        }
        
        private void SessionTimeOutWarningMessage()
        {
            objUMS_BAL = new UMS_BAL();
            DataTable dt = objUMS_BAL.GetSessionTime();
            hdfResult.Value = dt.Rows[0]["SessionTime"].ToString();           
            Session.Timeout = Convert.ToInt32(dt.Rows[0]["SessionTime"]);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimer('start');", true);
           
        }
    }
}