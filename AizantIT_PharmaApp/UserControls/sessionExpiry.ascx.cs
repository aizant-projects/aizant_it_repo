﻿using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.Web;
using System.Web.Mvc;

namespace AizantIT_PharmaApp.UserControls
{
    public partial class sessionExpiry : ViewUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserDetails"]!=null)
            {
                if (Request.Cookies.Get("CookieExpire") != null && Session["UserDetails"] != null)
                {
                    Session.Timeout = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["SessionTimeOut"]);
                    HttpCookie CookieExpire = new HttpCookie("CookieExpire", Session.Timeout.ToString());
                    HttpContext.Current.Response.Cookies.Add(CookieExpire);
                }
                else
                {
                    HelpClass.RemoveActiveUserSession();
                    Response.Redirect("~/UserLogin.aspx");
                }
            }            
        }       
    }
}