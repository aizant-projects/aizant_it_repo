﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AizantIT_PharmaApp.Common
{
    public partial class ViewFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string[] arrfile = Request.QueryString["fname"].Split('.');
            string FilePath = MapPath(@"~/ReferenceFiles/" + Request.QueryString["fname"]);
            if(Request.QueryString["dol"]=="1")
                FilePath = MapPath(@"~/DownloadReferrences/" + Request.QueryString["fname"]);
            switch (arrfile[1].ToLower())
            {
                case "jpeg":
                    Image.Visible = true;
                    Spreadsheet.Visible = false;
                    RichEdit.Visible = false;
                    Splitter.Visible = false;
                    string[] path = FilePath.Split('\\');
                    Image.ImageUrl = "~/" + path[path.Length - 2] + @"/" + path[path.Length - 1];

                    break;
                case "jpg":
                    Image.Visible = true;
                    Spreadsheet.Visible = false;
                    RichEdit.Visible = false;
                    Splitter.Visible = false;
                    string[] path1 = FilePath.Split('\\');
                    Image.ImageUrl = "~/" + path1[path1.Length - 2] + @"/" + path1[path1.Length - 1];

                    break;
                case "png":
                    Image.Visible = true;
                    Spreadsheet.Visible = false;
                    RichEdit.Visible = false;
                    Splitter.Visible = false;
                    string[] path2 = FilePath.Split('\\');
                    Image.ImageUrl = "~/" + path2[path2.Length - 2] + @"/" + path2[path2.Length - 1];

                    break;
                case "gif":
                    Image.Visible = true;
                    Spreadsheet.Visible = false;
                    RichEdit.Visible = false;
                    Splitter.Visible = false;
                    string[] path3 = FilePath.Split('\\');
                    Image.ImageUrl = "~/" + path3[path3.Length - 2] + @"/" + path3[path3.Length - 1];

                    break;
                case "bmp":
                    Image.Visible = true;
                    Spreadsheet.Visible = false;
                    RichEdit.Visible = false;
                    Splitter.Visible = false;
                    string[] path4 = FilePath.Split('\\');
                    Image.ImageUrl = "~/" + path4[path4.Length - 2] + @"/" + path4[path4.Length - 1];

                    break;
                case "pdf":
                    Splitter.Visible = true;
                    Spreadsheet.Visible = false;
                    RichEdit.Visible = false;
                    Image.Visible = false;
                    string[] path5 = FilePath.Split('\\');
                    Splitter.Panes[0].ContentUrl = "~/" + path5[path5.Length - 2] + @"/" + path5[path5.Length - 1];

                    break;
                case "xls":
                    Spreadsheet.Visible = true;
                    RichEdit.Visible = false;
                    Image.Visible = false;
                    Splitter.Visible = false;
                    Spreadsheet.Open(FilePath);

                    break;
                case "xlsx":
                    Spreadsheet.Visible = true;
                    RichEdit.Visible = false;
                    Image.Visible = false;
                    Splitter.Visible = false;
                    Spreadsheet.Open(FilePath);

                    break;
                case "rtf":
                    RichEdit.Visible = true;
                    Spreadsheet.Visible = false;
                    Image.Visible = false;
                    Splitter.Visible = false;
                    RichEdit.Open(FilePath);

                    break;
                case "doc":
                    RichEdit.Visible = true;
                    Spreadsheet.Visible = false;
                    Image.Visible = false;
                    Splitter.Visible = false;
                    RichEdit.Open(FilePath);

                    break;
                case "docx":
                    RichEdit.Visible = true;
                    Spreadsheet.Visible = false;
                    Image.Visible = false;
                    Splitter.Visible = false;
                    RichEdit.Open(FilePath);

                    break;
                case "txt":
                    RichEdit.Visible = true;
                    Spreadsheet.Visible = false;
                    Image.Visible = false;
                    Splitter.Visible = false;
                    RichEdit.Open(FilePath);

                    break;
                case "htm":
                    Response.ContentType = "text/HTML";

                    break;
                case "html":
                    Response.ContentType = "text/HTML";

                    break;
            }
            // Response.ContentType = "Application/pdf";
            //Get the physical path to the file.

            //Write the file directly to the HTTP content output stream.
            //Response.WriteFile(FilePath);
            //Response.End();
        }
    }
}