﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Threading;

namespace AizantIT_PharmaApp.Common
{
    public class DynamicFolder
    {
        public static string CreateDynamicFolder(int subFolderNUm)
        {
            string strMainFolderPath = string.Empty;
            switch (subFolderNUm)
            {
                case 1:
                    strMainFolderPath= HttpContext.Current.Server.MapPath(@"~/DMSTempFolder/DocCreate/");
                    break;
                case 2:
                    strMainFolderPath = HttpContext.Current.Server.MapPath(@"~/DMSTempFolder/DOCPDFConvert/");
                    break;
                case 3:
                    strMainFolderPath = HttpContext.Current.Server.MapPath(@"~/DMSTempFolder/DocPrint/");
                    break;
                case 4:
                    strMainFolderPath = HttpContext.Current.Server.MapPath(@"~/DMSTempFolder/DocRTF/");
                    break;
                case 5:
                    strMainFolderPath = HttpContext.Current.Server.MapPath(@"~/DMSTempFolder/FormCreate/");
                    break;
                case 6:
                    strMainFolderPath = HttpContext.Current.Server.MapPath(@"~/DMSTempFolder/FormPrint/");
                    break;
                case 7:
                    strMainFolderPath = HttpContext.Current.Server.MapPath(@"~/DMSTempFolder/FormRTF/");
                    break;
                case 8:
                    strMainFolderPath = HttpContext.Current.Server.MapPath(@"~/DMSTempFolder/TemplateUpload/");
                    break;
                case 11:
                    strMainFolderPath = HttpContext.Current.Server.MapPath(@"~/QMSTempReports/");
                    break;
                case 12:
                    strMainFolderPath = HttpContext.Current.Server.MapPath(@"~/DMSPDFFolder/web/");
                    break;
            }

            string directoryPath = strMainFolderPath + DateTime.Today.ToString("yyyyMMdd");
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
            //DeleteDynamicFolder(subFolderNUm);
            return directoryPath;
        }
        public static bool DeleteDynamicFolder(int subFolderNUm)
        {
            string strMainFolderPath = string.Empty;
            switch (subFolderNUm)
            {
                case 1:
                    strMainFolderPath = HttpContext.Current.Server.MapPath(@"~/DMSTempFolder/DocCreate/");
                    break;
                case 2:
                    strMainFolderPath = HttpContext.Current.Server.MapPath(@"~/DMSTempFolder/DOCPDFConvert/");
                    break;
                case 3:
                    strMainFolderPath = HttpContext.Current.Server.MapPath(@"~/DMSTempFolder/DocPrint/");
                    break;
                case 4:
                    strMainFolderPath = HttpContext.Current.Server.MapPath(@"~/DMSTempFolder/DocRTF/");
                    break;
                case 5:
                    strMainFolderPath = HttpContext.Current.Server.MapPath(@"~/DMSTempFolder/FormCreate/");
                    break;
                case 6:
                    strMainFolderPath = HttpContext.Current.Server.MapPath(@"~/DMSTempFolder/FormPrint/");
                    break;
                case 7:
                    strMainFolderPath = HttpContext.Current.Server.MapPath(@"~/DMSTempFolder/FormRTF/");
                    break;
                case 8:
                    strMainFolderPath = HttpContext.Current.Server.MapPath(@"~/DMSTempFolder/TemplateUpload/");
                    break;
                case 11:
                    strMainFolderPath = HttpContext.Current.Server.MapPath(@"~/QMSTempReports/");
                    break;
                case 12:
                    strMainFolderPath = HttpContext.Current.Server.MapPath(@"~/DMSPDFFolder/web/");
                   break;
            }
            string directoryPath = strMainFolderPath + DateTime.Today.AddDays(-2).ToString("yyyyMMdd");
            if (Directory.Exists(directoryPath))
            {
                try
                {
                    Directory.Delete(directoryPath, recursive: true);               //throws if directory doesn't exist.
                    return true;
                }
                catch
                {
                    //HACK because the recursive delete will throw with an "Directory is not empty." 
                    //exception after it deletes all the contents of the diretory if the directory
                    //is open in the left nav of Windows's explorer tree.  This appears to be a caching
                    //or queuing latency issue.  Waiting 2 secs for the recursive delete of the directory's
                    //contents to take effect solved the issue for me.  Hate it I do, but it was the only
                    //way I found to work around the issue.
                    Thread.Sleep(2000);     //wait 2 seconds
                    Directory.Delete(directoryPath, recursive: true);
                    return true;
                }
            }
            return false;
        }
        public static string CreateTempDocumentFolder(string DocumentNumber,string VersionNumber)
        {
            string directoryPath = HttpContext.Current.Server.MapPath((string.Format(@"~/DMSTempDocuments/{0}/{1}/", DocumentNumber, VersionNumber)));
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
            return directoryPath;
        }
    }
}