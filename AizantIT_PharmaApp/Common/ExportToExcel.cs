﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Drawing;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Text.RegularExpressions;
using Microsoft.Win32;
using UMS_BO;
using UMS_BusinessLayer;

namespace AizantIT_PharmaApp.Common
{
    public class ExportToExcel
    {
        public bool CheckWhetherOfficeInstalled()
        {
            // Checking whether excel is installed on system
            RegistryKey TargetKey = default(RegistryKey);
            TargetKey = Registry.ClassesRoot.OpenSubKey("excel.application");
            if (TargetKey == null)
            {

                return false;

            }
            else
            {
                return true;
            }
        }
        public string ExportExcel(DataTable dtExcel, string rptName, object dtFromDate, object dtToDate, int inFirstRow = 0, int inFirstCol = 0, string strPath = "")//, string credit, string debit, string closing)
        {
            string strResult = string.Empty;
            try
            {
                if (CheckWhetherOfficeInstalled())
                {
                    int inColN = 1;

                    string strName = "", strAddress = "", strPhone = "";

                    Excel.Range range = null;
                    Excel.Application excel = new Excel.Application();

                    Excel.Workbook wb = excel.Workbooks.Add(Excel.XlSheetType.xlWorksheet);
                    Excel.Worksheet ws = (Excel.Worksheet)excel.ActiveSheet;
                    DataTable dtblCompany = fillCompany();

                    strAddress = dtblCompany.Rows[0]["CompanyAddress"].ToString().Replace("\r\n", " ");
                    strPhone = dtblCompany.Rows[0]["CompanyPhoneNo1"].ToString();
                    strName = dtblCompany.Rows[0]["CompanyName"].ToString();


                    //**************Report Header ***************************************
                    //range = (Excel.Range)ws.Cells[1, 1];
                    range = ws.get_Range("A1", "I1");
                    range.MergeCells = true;
                    range.Font.Size = 15;
                    range.RowHeight = 27;
                    range.Interior.Color = ColorTranslator.ToWin32(Color.LightGray);
                    range.Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    range.Cells.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                    range.Value2 = strName;

                    range = ws.get_Range("A2", "I2");
                    range.MergeCells = true;
                    range.Font.Size = 10;

                    range.Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    range.Value2 = strAddress;

                    range = ws.get_Range("A3", "I3");
                    range.MergeCells = true;
                    range.Font.Size = 10;

                    range.Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    range.Value2 = "Phone No :" + strPhone;

                    range = ws.get_Range("A5", "G5");
                    range.MergeCells = true;
                    range.Font.Size = 11;
                    range.Value2 = rptName;
                    range.Font.Underline = true;
                    range.Font.Bold = true;
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                    range = ws.get_Range("A6", "G6");
                    range.MergeCells = true;
                    //range.Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    range.Font.Size = 11;
                    if (dtFromDate != null && dtToDate != null)
                    {
                        range.Value2 = "(" + DateTime.Parse(dtFromDate.ToString()).Date.ToString("dd MMM yyyy") + "  To  " + DateTime.Parse(dtToDate.ToString()).Date.ToString("dd MMM yyyy") + ")";
                    }
                    else if (dtFromDate != null)
                    {
                        range.Value2 = DateTime.Parse(dtFromDate.ToString()).Date.ToString("dd MMM yyyy");
                    }
                    range.Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    range.MergeCells = true;
                    range.Font.Bold = true;


                    range = ws.get_Range("H5", "H5");
                    range.Value2 = "Date :";
                    range.Font.Bold = true;

                    range = ws.get_Range("I5", "I5");
                    range.Value2 = DateTime.Now.ToString("dd MMM yyyy");
                    range.Font.Bold = true;




                    int inNewRow = 0;
                    inNewRow = inFirstRow;


                    for (int inRow = inFirstRow; inRow < dtExcel.Rows.Count; inRow++)
                    {

                        for (int inCol = inFirstCol; inCol < dtExcel.Columns.Count; inCol++)
                        {
                            if (inRow == 0)
                            {

                                range = (Excel.Range)ws.Cells[inNewRow + 8, inColN];
                                range.Font.Bold = true;
                                range.Interior.Color = ColorTranslator.ToWin32(Color.LightGray);
                                range.Value2 = dtExcel.Columns[inCol].ColumnName;

                            }
                            range = (Excel.Range)ws.Cells[inNewRow + 9, inColN];


                            range.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlHairline, Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                            if (dtExcel.Rows[inRow][inCol].ToString() != null)
                            {
                                string str = dtExcel.Rows[inRow][inCol].ToString();


                                try
                                {
                                    decimal.Parse(str);
                                    decimal dc = 0;

                                    //if (dtExcel.Columns[inCol].ColumnName.ToLower() == "numericvalueColumn" || dtExcel.Columns[inCol].ColumnName.ToLower() == "numericvalueColumn")
                                    //{
                                    //    dc = Math.Round(decimal.Parse(str), 2);
                                    //    range.NumberFormat = "#00.00#";
                                    //}

                                    {
                                        dc = Math.Round(decimal.Parse(str), 2);
                                        range.NumberFormat = "General";
                                    }
                                    str = dc.ToString();

                                }
                                catch (Exception)
                                {
                                    try
                                    {
                                        DateTime.Parse(str);
                                        range.NumberFormat = "dd MMM yyyy";
                                        range.NumberFormat = "General";
                                    }
                                    catch (Exception)
                                    {
                                        range.NumberFormat = "@";
                                    }
                                }
                                //if (str.Contains("Dr") || str.Contains("Cr"))

                                //    range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;


                                range.Value2 = str;

                            }
                            inColN++;

                        }
                        inColN = 1;
                        inNewRow++;

                    }
                    inNewRow = inNewRow + 10;


                    ws.Columns.AutoFit();
                    if (strPath.Length > 15)
                    {
                        string filename = strPath;//"C:\\Myfiles\\" + rptName + "_" + DateTime.Now.ToString("ddMMMyyyy") + ".xlsx";
                        ws.SaveAs(filename);
                        // excel.Visible = true;
                        strResult = "File saved at " + filename;
                    }

                }
                else
                {
                    strResult = "Install office";
                }
            }
            catch (Exception ex)
            {
                strResult = ex.Message;
            }
            return strResult;
        }

        DataTable fillCompany()
        {
            DataTable dtcompany = new DataTable();
            CompanyBO objCompanyBO = new CompanyBO();
            objCompanyBO.Mode = 2;
            objCompanyBO.CompanyID = 0;
            objCompanyBO.CompanyCode = "";
            objCompanyBO.CompanyName = "";
            objCompanyBO.CompanyDescription = "";
            objCompanyBO.CompanyPhoneNo1 = "";
            objCompanyBO.CompanyPhoneNo2 = "";
            objCompanyBO.CompanyFaxNo1 = "";
            objCompanyBO.CompanyFaxNo2 = ""; //txtFaxNo2.Value;
            objCompanyBO.CompanyEmailID = "";
            objCompanyBO.CompanyWebUrl = "";
            objCompanyBO.CompanyAddress = "";
            objCompanyBO.CompanyLocation = "";// txtLocat.Value;
            objCompanyBO.CompanyCity = "";
            objCompanyBO.CompanyState = "";
            objCompanyBO.CompanyCountry = "";
            objCompanyBO.CompanyPinCode = "";
            objCompanyBO.CompanyStartDate = "";
            objCompanyBO.CompanyLogo = new byte[0];
            UMS_BAL objUMS_BAL = new UMS_BAL();
            dtcompany = objUMS_BAL.CompanyQuery(objCompanyBO);
            return dtcompany;

        }
    }
}