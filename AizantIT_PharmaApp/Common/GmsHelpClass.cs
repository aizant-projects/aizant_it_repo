﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using VMS_BAL;
using VMS_BO;
using AizantIT_PharmaApp.Common;
using System.Data.SqlClient;


namespace AizantIT_PharmaApp.Common
{
    public class GmsHelpClass
    {
        public static DataTable GetRolePrivileges(int roleId=0 )
        {
            roleId = roleId == 0 ? Convert.ToInt32(HttpContext.Current.Session["sesGMSRoleID"].ToString()) : roleId;
            VMSRolePrivilegesBAL objVMSRolePrivilegesBAL = new VMSRolePrivilegesBAL();
            RolePrivilegesBO objRolePrivilegesBO = new RolePrivilegesBO();
            objRolePrivilegesBO.RoleID =roleId ;
            DataTable dt = new DataTable();
            dt = objVMSRolePrivilegesBAL.GetRolePrivileges(objRolePrivilegesBO);
            return dt;
        }
    }
}