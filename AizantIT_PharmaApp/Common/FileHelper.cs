﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.Common
{
    public class FileHelper
    {
        public static byte[] ConvertHexToBytes(string hex)
        {
            byte[] bytes = new byte[hex.Length / 2];
            for (int i = 0; i < hex.Length; i += 2)
            {
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            }
            return bytes;
        }
        public static byte[] byteImg(string imgPath)
        {
            using (FileStream FS = new FileStream(imgPath, FileMode.Open, FileAccess.Read))            {
               
                byte[] img = new byte[FS.Length];
                FS.Read(img, 0, Convert.ToInt32(FS.Length));
                FS.Close();
                return img;
            }
        }
    }
}