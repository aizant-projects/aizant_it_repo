﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.Common
{
    public static class PDFConverterInfo
    {
         
        public static bool PDFConverterValidation()
        {
            try
            {                //try to get the remoting-object
                Word2PDFConverter.RemoteConverter converter = (Word2PDFConverter.RemoteConverter)Activator.GetObject(typeof(Word2PDFConverter.RemoteConverter), "http://localhost:"+ System.Configuration.ConfigurationManager.AppSettings["Word2PdfPort"].ToString() + "/RemoteConverter");

                if (!converter.WordIsAvailable())
                {
                    // lbl_error.Text = "Word 2007 not available on server!";
                    return false;
                }
                else
                    return true;
            }
            catch
            {
                //Remoteserver not available
                //lbl_error.Text = "PDFConverter is not running!";
                return false;
            }
        }
    }
    
}