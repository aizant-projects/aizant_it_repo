﻿using AizantIT_PharmaApp.Common.UMS;
using AizantIT_PharmaApp.VMS.MaterialRegister;
using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace AizantIT_PharmaApp.Common
{
    public class HelpClass
    {
        public static decimal attachSize = 0;
        public static int NoOfDecimalPlaces = 2;
        public static string dateFormat = "dd MMM yyyy";
        public static string dateTimeFormat = "dd MMM yyyy HH:mm:ss";
        public static bool IsUserAuthenticated()
        {
            if (HttpContext.Current.Session["UserDetails"] != null && HttpContext.Current.Request.Cookies.Get("CookieUserIn") != null)
            {
                if (UserSessionCheck.IsValidSession((HttpContext.Current.Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpLoginID"].ToString(),
                    (HttpContext.Current.Session["UserDetails"] as DataSet).Tables["UserDetails"].Rows[0]["ActiveUserSession"].ToString()))
                {
                    return true;
                }
                else
                {
                    RemoveActiveUserSession();
                    return false;
                }
            }
            else
            {
                if (HttpContext.Current.Session["UserDetails"] != null || HttpContext.Current.Request.Cookies.Get("CookieUserIn") != null)
                {
                    RemoveActiveUserSession();
                }
                return false;
            }
        }

        public static void RemoveActiveUserSession()
        {
            HttpContext.Current.Response.Cookies["CookieExpire"].Expires =
            HttpContext.Current.Response.Cookies["CookieUserIn"].Expires = DateTime.Now.AddDays(-1);
            if (HttpContext.Current.Session["UserDetails"] != null)
            {
                UserSessionCheck.ResetActiveUserSession((HttpContext.Current.Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpLoginID"].ToString(),
                (HttpContext.Current.Session["UserDetails"] as DataSet).Tables["UserDetails"].Rows[0]["ActiveUserSession"].ToString());
            }           
            HttpContext.Current.Session.Abandon();
        }

        public static void custAlertMsg(System.Web.UI.Control obj, Type t, String message, string typeOfMsg)
        {
            System.Web.UI.ScriptManager.RegisterStartupScript(obj, t, DateTime.Now.ToString("hh.mm.ss.fff"), "custAlertMsg('" + message + "','" + typeOfMsg + "');", true);
        }
        public static void custAlertMsgWithFunc(System.Web.UI.Control obj, Type t, String message, string typeOfMsg, string func)
        {
            System.Web.UI.ScriptManager.RegisterStartupScript(obj, t, DateTime.Now.ToString("hh.mm.ss.fff"), "custAlertMsg('" + message + "','" + typeOfMsg + "','" + func + "');", true);
        }
        public static void showMsg(System.Web.UI.Control obj, Type t, String message)
        {
            System.Web.UI.ScriptManager.RegisterStartupScript(obj, t, DateTime.Now.ToString("hh.mm.ss.fff"), "alert('" + message + "');", true);
        }
        public static void CallJS_Func(System.Web.UI.Control obj, Type t, string function)
        {
            System.Web.UI.ScriptManager.RegisterStartupScript(obj, t, DateTime.Now.ToString("hh.mm.ss.fff"), function, true);
        }
        public static string SQLEscapeString(string usString)
        {

            if (usString == null)
            {

                return null;

            }

            // it escapes \r, \n, \x00, \x1a, \, ', and "

            return System.Text.RegularExpressions.Regex.Replace(usString, @"[\r\n\x00\x1a\\'""]", @"\$0");

        }


        public static string SQLEscapeString(string usString, [Optional, DefaultParameterValue(false)] bool blnOptNumFlag)
        {
            //public string SQLEscapeString(string usString, [Optional, DefaultParameterValue(false)] bool blnOptNumFlag )
            if ((usString == "") && (blnOptNumFlag == true))
            {

                return "0";
            }


            if (usString == null)
            {

                return null;

            }

            // it escapes \r, \n, \x00, \x1a, \, ', and "

            return System.Text.RegularExpressions.Regex.Replace(usString, @"[\r\n\x00\x1a\\'""]", @"\$0");

        }
        #region Exceptions
        public static string LineNo(Exception ex)
        {
            string _lnNo = string.Empty;
            // Get stack trace for the exception with source file information
            var st = new StackTrace(ex, true);
            // Get the top stack frame
            var frame = st.GetFrame(st.FrameCount - 1);
            // Get the line number from the stack frame
            var line = frame.GetFileLineNumber();
            _lnNo = line.ToString();
            return _lnNo;

        }
        #endregion Exceptions       
        #region datatable DecimalPlaces
        public static DataTable GetNoOfDecimalPlacesDataTable(DataTable table)
        {
            //try
            //{
            //}
            //catch (SQLException sqe)
            //{
            //    string strmsgF = HelpClass.SQLEscapeString(sqe.Message);
            //    HelpClass.showMsg(this, this.GetType(), strmsgF);
            //}
            //catch (Exception ex)
            //{
            //    string strline = HelpClass.LineNo(ex);
            //    string strMsg = HelpClass.SQLEscapeString(ex.Message);
            //    HelpClass.showMsg(this, this.GetType(), "APP1:" + strline + ", " + strMsg);
            //}

            DataTable table1 = new DataTable();
            ArrayList decimalColumns = new ArrayList();
            ArrayList dateColumns = new ArrayList();

            for (int j = 0; j < table.Columns.Count; j++)
            {
                if (table.Columns[j].DataType.Equals(typeof(System.DateTime)))
                {
                    dateColumns.Add(j);
                    table1.Columns.Add(table.Columns[j].ColumnName, typeof(System.String));
                }
                else
                {

                    if (table.Columns[j].DataType.Equals(typeof(System.Decimal)))
                        decimalColumns.Add(j);
                    if (table.Columns[j].DataType.Equals(typeof(System.Double)))
                        decimalColumns.Add(j);
                    table1.Columns.Add(table.Columns[j].ColumnName, table.Columns[j].DataType);
                }
            }

            for (int i = 0; i < table.Rows.Count; i++)
            {
                DataRow dr = table1.NewRow();
                table1.Rows.Add(dr);
                for (int j = 0; j < table.Columns.Count; j++)
                {
                    if (decimalColumns.Contains(j))
                    {
                        if (!string.IsNullOrEmpty(table.Rows[i][j].ToString()))
                            table1.Rows[i][j] = Math.Round(Convert.ToDecimal(table.Rows[i][j].ToString()), HelpClass.NoOfDecimalPlaces).ToString();
                        else
                            table1.Rows[i][j] = table.Rows[i][j];
                    }
                }
            }
            return table1;
        }
        public static decimal GetNoOfDecimalPlacesValue(decimal decValue)
        {
            return Math.Round(decValue, HelpClass.NoOfDecimalPlaces);
        }
        public static string GetDecimalStringFormat()
        {
            string strFormat = "{0:0.00}";
            switch (HelpClass.NoOfDecimalPlaces)
            {
                case 0:
                    strFormat = "{0:0}";
                    break;
                case 1:
                    strFormat = "{0:0.0}";
                    break;
                case 2:
                    strFormat = "{0:0.00}";
                    break;
                case 3:
                    strFormat = "{0:0.000}";
                    break;
                case 4:
                    strFormat = "{0:0.0000}";
                    break;
                case 5:
                    strFormat = "{0:0.00000}";
                    break;
                case 6:
                    strFormat = "{0:0.000000}";
                    break;
                case 7:
                    strFormat = "{0:0.0000000}";
                    break;
                case 8:
                    strFormat = "{0:0.00000000}";
                    break;
                case 9:
                    strFormat = "{0:0.000000000}";
                    break;
                case 10:
                    strFormat = "{0:0.0000000000}";
                    break;
            }
            return strFormat;
        }
        #endregion datatable DecimalPlaces

        public static string GetMonthName(string month,bool IsLarge=true)
        {
            string[] mL = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            string[] mS = { "Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec" };
            switch (month)
            {
                case "1":
                    return IsLarge ? mL[0] : mS[0];
                case "2":
                    return IsLarge ? mL[1] : mS[1];
                case "3":
                    return IsLarge ? mL[2] : mS[2];
                case "4":
                    return IsLarge ? mL[3] : mS[3];
                case "5":
                    return IsLarge ? mL[4] : mS[4];
                case "6":
                    return IsLarge ? mL[5] : mS[5];
                case "7":
                    return IsLarge ? mL[6] : mS[6];
                case "8":
                    return IsLarge ? mL[7] : mS[7];
                case "9":
                    return IsLarge ? mL[8] : mS[8];
                case "10":
                    return IsLarge ? mL[9] : mS[9];
                case "11":
                    return IsLarge ? mL[10] : mS[10];
                case "12":
                    return IsLarge ? mL[11] : mS[11];
                default:
                    return "Not Defined";
            }
        }

        #region dataVariables #region dataVariables null
        public static Int32 ToInt32(object obj)
        {
            if (obj == null || obj == DBNull.Value || obj.ToString() == String.Empty)
            {
                return 0;
            }
            try
            {
                //return Convert.ToInt32(obj, NumberStyles.Any);
                int result = 0;
                int.TryParse(obj.ToString(), NumberStyles.Any, NumberFormatInfo.CurrentInfo, out result);
                return result;
            }
            catch { return 0; }
        }

        public static decimal ToDecimal(object obj)
        {
            if (obj == null || obj == DBNull.Value)
            {
                return 0;
            }
            try
            {
                //return Convert.ToDecimal(obj);
                decimal result = 0m;
                decimal.TryParse(obj.ToString(), NumberStyles.Any, NumberFormatInfo.CurrentInfo, out result);
                return result;
            }
            catch { return 0; }
        }
        public static double ToDouble(object obj)
        {
            if (obj == null || obj == DBNull.Value)
            {
                return 0;
            }
            try
            {
                double result = 0;
                double.TryParse(obj.ToString(), NumberStyles.Any, NumberFormatInfo.CurrentInfo, out result);
                return result;
            }
            catch { return 0; }
        }

        public static Boolean ToBoolean(object obj)
        {
            if (obj == null || obj == DBNull.Value)
            {
                return false;
            }
            try
            {
                return Convert.ToBoolean(obj);
            }
            catch { return false; }
        }

        public static string ToStr(object obj)
        {
            if (obj == null || obj == DBNull.Value)
            {
                return string.Empty;
            }
            try
            {
                return Convert.ToString(obj);
            }
            catch { return string.Empty; }
        }
        public static string GetCustomDate(object obj,bool _IsShort)
        {
            if (obj == null || obj == DBNull.Value || obj.ToString().Contains("1900")||obj.ToString()==string.Empty)
            {
                return "-NA-";
            }
                if(!_IsShort)
                return Convert.ToDateTime(obj).ToString(dateTimeFormat); 
                else
                    return Convert.ToDateTime(obj).ToString(dateFormat);
        }
        public static DateTime ToDate(object ob)
        {
            DateTime dtMIN = DateTime.Parse("01/01/1900");
            if (ob == null || ob == DBNull.Value)
            {
                return dtMIN;
            }
            try
            {
                return Convert.ToDateTime(ob);
            }
            catch
            {
                return dtMIN;
            }

        }
       
        public static bool IsDecimal(object obj)
        {
            string str = obj.ToString();
            string[] arr = str.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr.Length > 1)
            {
                if (Convert.ToInt32(arr[1]) > 0)
                {
                    return true;
                }
            }
            return false;
        }
        #endregion dataVariables null
        #region encrypt decrypt
        public static string Encrypt(string encryptString)
        {
            string EncryptionKey = "GIM01J2O3AQBYZCLND4H56WX79EFKPRS8TUV";
            byte[] clearBytes = Encoding.Unicode.GetBytes(encryptString);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] {
            0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76
        });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    encryptString = Convert.ToBase64String(ms.ToArray());
                }
            }
            return encryptString;
        }

        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "GIM01J2O3AQBYZCLND4H56WX79EFKPRS8TUV";
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] {
            0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76
        });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        internal static void custAlertMsg(Button btnInsert, Type type, string v)
        {
            throw new NotImplementedException();
        }

        internal static void custAlertMsg(MaterialRegister materialRegister, Type type, string v)
        {
            throw new NotImplementedException();
        }
        #endregion encrypt decrypt


    }
}