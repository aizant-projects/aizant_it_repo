﻿namespace AizantIT_PharmaApp.Common
{
    public class NotificationData
    {
        public string NotificationID { get; set; }
        public string Description { get; set; }
        public string Module { get; set; }
        public string Status { get; set; }
        public string ReferenceLink { get; set; }
        public string Date { get; set; }
        public string UnseenNotificationCount { get; set; }
        public string TotalNotifyCount { get; set; }
    }
}