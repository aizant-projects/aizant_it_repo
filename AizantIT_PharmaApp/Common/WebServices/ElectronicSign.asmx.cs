﻿using System;
using System.Data;
using System.Web.Services;
using UMS_BusinessLayer;

namespace AizantIT_PharmaApp.Common.WebServices
{
    /// <summary>
    /// Summary description for ElectronicSign
    /// </summary>
    [WebService(Namespace = "http://AizantIT.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class ElectronicSign : System.Web.Services.WebService
    {
        UMS_BAL objUMS_BAL = new UMS_BAL();
        [WebMethod(EnableSession = true)]
        public string VerifyElectronicSign(string Password,string EmpID)
        {
            EmpID = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
            objUMS_BAL.ElectronicSignVerification(Password.Trim(), Convert.ToInt32(EmpID), out int SignVerified);
            if (SignVerified == 1)
            {
                return "True";
            }
            else
            {
                return "False";
            }
        }
    }
}
