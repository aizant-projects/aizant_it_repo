﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Web.Script.Services;
using System.Text;

namespace AizantIT_PharmaApp.Common.WebServices
{
    /// <summary>
    /// Summary description for Email_Service
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Email_Service : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        [System.Web.Script.Services.ScriptMethod()]
        public void Send_Email(MailBO _objMailBO)
        {
            SmtpClient smtpClient = new SmtpClient("domain.a2hosted.com", 25);

            smtpClient.Credentials = new System.Net.NetworkCredential("user@example.com", "password");
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            MailMessage mailMessage = new MailMessage();
            MailAddress FromAddress = new MailAddress(_objMailBO.MailFrom);
            mailMessage.From = FromAddress;
            foreach (string strMailTo in _objMailBO.MailTo)
            {
                mailMessage.To.Add(strMailTo);
            }
            foreach (string strMailCC in _objMailBO.MailCC)
            {
                mailMessage.CC.Add(strMailCC);
            }
            foreach (string strMailBCC in _objMailBO.MailBCC)
            {
                mailMessage.Bcc.Add(strMailBCC);
            }

            mailMessage.Subject = _objMailBO.MailSubject;
            if (_objMailBO.IsHTML)

                mailMessage.Body = _objMailBO.HTMLMessage.ToString();

            else
                mailMessage.Body = _objMailBO.TextMessage;

            foreach (string strFilePath in _objMailBO.AttachmentsPath)
            {
                Attachment attachment = new Attachment(strFilePath);
                mailMessage.Attachments.Add(attachment);
            }            
           
            try
            {
                smtpClient.Send(mailMessage);
                
            }
            catch
            {
               
            }

        }
    }
    public class MailBO
    {
        private string _MailFrom;

        public string MailFrom
        {
            get { return _MailFrom; }
            set { _MailFrom = value; }
        }
        
        private List<string> _MailTo;

        public List<string> MailTo
        {
            get { return _MailTo; }
            set { _MailTo = value; }
        }
        private List<string> _MailCC;
        public List<string> MailCC
        {
            get { return _MailCC; }
            set { _MailCC = value; }
        }
        private List<string> _MailBCC;

        public List<string> MailBCC
        {
            get { return _MailBCC; }
            set { _MailBCC = value; }
        }

        private string _MailSubject;

        public string MailSubject
        {
            get { return _MailSubject; }
            set { _MailSubject = value; }
        }
        private bool _IsHTML;

        public bool IsHTML
        {
            get { return _IsHTML; }
            set { _IsHTML = value; }
        }
        private string _TextMessage;

        public string TextMessage
        {
            get { return _TextMessage; }
            set { _TextMessage = value; }
        }

        private StringBuilder _HTMLMessage;

        public StringBuilder HTMLMessage
        {
            get { return _HTMLMessage; }
            set { _HTMLMessage = value; }
        }

        private List<string> _AttachmentsPath;

        public List<string> AttachmentsPath
        {
            get { return _AttachmentsPath; }
            set { _AttachmentsPath = value; }
        }


    }
}
