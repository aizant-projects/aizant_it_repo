﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace AizantIT_PharmaApp.Common.WebServices
{
    /// <summary>
    /// Summary description for SessionTime
    /// </summary>
    [WebService(Namespace = "http://aizantitanalytics.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class SessionTime : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public void UpdateSessionTimeOut()
        {
            if (Session["UserDetails"]!=null)
            {
            Session.Timeout =  Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["SessionTimeOut"]);
            }
                //Session.Timeout;
            if (HttpContext.Current.Request.Cookies.Get("CookieExpire") != null)
            {
            HttpCookie CookieExpire = new HttpCookie("CookieExpire",Session.Timeout.ToString());
            HttpContext.Current.Response.Cookies.Add(CookieExpire);
            }
            //else
            //{
            //    HttpContext.Current.Response.Redirect("~/UserLogin.aspx");
            //}
        }
    }
}
