﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Services;
using UMS_BusinessLayer;

namespace AizantIT_PharmaApp.Common
{
    /// <summary>
    /// Summary description for Notification
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class Notification : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public List<NotificationData> GetNotifications(string Emp_ID = "0", string ShowLess = "No")
        {
            List<NotificationData> Notification = new List<NotificationData>();
            try
            {
                UMS_BAL objUMS_BAL = new UMS_BAL();
                Emp_ID = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                DataTable dtNotification = objUMS_BAL.getNotificationsData(Emp_ID);
                int ActualCount = dtNotification.Rows.Count;
                int numberOfUnSeenRecords = dtNotification.AsEnumerable().Where(x => x["NotificationStatus"].ToString() == "1").ToList().Count;
                if (ShowLess == "Yes")
                {
                    if (dtNotification.Rows.Count > 4)
                    {
                        dtNotification = dtNotification.Rows.Cast<System.Data.DataRow>().Take(4).CopyToDataTable();
                        dtNotification.AcceptChanges();
                    }
                }

                foreach (DataRow drresult in dtNotification.Rows)
                {
                    string RefLink = "#";
                    if (drresult["NotificationLink"].ToString().Contains('?'))
                    {
                        RefLink = drresult["NotificationLink"].ToString() + "&Notification_ID=" + drresult["NotificationID"].ToString();
                    }
                    else
                    {
                        RefLink = drresult["NotificationLink"].ToString() + "?Notification_ID=" + drresult["NotificationID"].ToString();
                    }
                    if (drresult["Module"].ToString() == "QMS")
                    {
                        RefLink = "/" + RefLink;
                        if (drresult["NotificationLink"].ToString().Contains('#'))
                        {
                            RefLink = "#";
                        }
                    }
                    Notification.Add(new NotificationData
                    {
                        NotificationID = drresult["NotificationID"].ToString(),
                        Description = drresult["Description"].ToString(),
                        Module = drresult["Module"].ToString(),
                        Status = drresult["NotificationStatus"].ToString(),
                        ReferenceLink = RefLink,
                        Date = drresult["GeneratedDateStr"].ToString(),
                        UnseenNotificationCount = numberOfUnSeenRecords.ToString(),
                        TotalNotifyCount = ActualCount.ToString(),
                    });
                }
                return Notification;
            }
            catch
            {
                return new List<NotificationData>();
            }
        }
        [WebMethod(EnableSession = true)]
        public void SubmitNotifications(string Notification_ID, string Emp_ID)
        {
            UMS_BAL objUMS_BAL = new UMS_BAL();
            int Notify_status = 3;
            Emp_ID= (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
            int EmpID =Convert.ToInt32(Emp_ID);
            int[] empIDs = new int[] { EmpID };
            objUMS_BAL.submitNotifications(Notification_ID,Notify_status, empIDs);
        }
    }
}
