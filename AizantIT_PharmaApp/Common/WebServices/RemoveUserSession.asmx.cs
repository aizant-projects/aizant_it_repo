﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using UMS_BusinessLayer;

namespace AizantIT_PharmaApp.Common.WebServices
{
    /// <summary>
    /// To Remove User Login Session from database and browser
    /// </summary>
    [WebService(Namespace = "")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class RemoveUserSession : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public void Remove_UserSession()
        {
            try
            {
                // record logout history
                UMS_BAL UMBAL = new UMS_BAL();
                UMBAL.UserLogoutUpdate((HttpContext.Current.Session["UserDetails"] as DataSet).Tables[3].Rows[0]["LogPkID"].ToString());
                for (int i = 0; i < 500; i++)
                {
                    //Elapse Time for logout execution
                }
            }
            catch
            {


            }
            HelpClass.RemoveActiveUserSession();
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void CheckUserSession()
        {                       
            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(HelpClass.IsUserAuthenticated()));
        }
    }
}
