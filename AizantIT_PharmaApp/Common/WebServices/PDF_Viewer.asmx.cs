﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using System;
using System.Data;
using System.IO;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace AizantIT_PharmaApp.Common.WebServices
{
    /// <summary>
    /// Summary description for PDF_Viewer
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class PDF_Viewer : System.Web.Services.WebService
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();

        [WebMethod(EnableSession = true)]
        public string GetPDF_FilePath(int TypeID,int DocOrVersionID,int OfEmpID,int OfRoleID)
        {
            try
            {
                string Msg = "", MsgType = "";
                DocObjects docObjects = new DocObjects();
                docObjects.Mode = TypeID;
                docObjects.DocRefID = DocOrVersionID;
                docObjects.RoleID = OfRoleID;
                docObjects.EmpID = OfEmpID;
                DataTable dt = DMS_Bal.DMS_GetPhysicalPathforViewing(docObjects);
                //string filePath = Server.MapPath("~/Scripts/pdf.js/web/20190408101726372.pdf");
                string filePath = Server.MapPath("~/Scripts/pdf.js/web/"+ dt.Rows[0]["DyFileName"].ToString());
                if (File.Exists(filePath)) 
                {
                    string Path = "<iframe src=\"" + System.Web.VirtualPathUtility.ToAbsolute("~/Scripts/pdf.js/web/viewer.html?file=" + dt.Rows[0]["DyFileName"].ToString()) + "#toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" frameborder=\"0\" height=\"680px\" ></iframe>";
                    Msg = Path;
                    MsgType = "success";
                }
                else
                {
                    Msg = "File does not exist.";
                    MsgType = "error";
                }

                var result = new 
                {
                    msg = Msg,
                    msgType = MsgType
                };
                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
             catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                var result = new
                {
                    msg = "PDF_Viewer:" + strline + "  " + strMsg,
                    msgType = "error"
                };
                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetPDF_Of_File(string FileName)
        {
            try
            {
                string Path = "<iframe src=\"" + System.Web.VirtualPathUtility.ToAbsolute("~/Scripts/pdf.js/web/viewer.html?file=" + FileName) + "#toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" frameborder=\"0\" height=\"700px\" ></iframe>";
                var result = new
                {
                    msg = Path,
                    msgType = "success"
                };
                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                var result = new
                {
                    msg = "PDF_Viewer:" + strline + "  " + strMsg,
                    msgType = "error"
                };
                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(result);
            }
        }
    }
}