﻿using System;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace AizantIT_PharmaApp.Common.CustomFilters
{
    public class CustAuthentication : ActionFilterAttribute, IAuthenticationFilter
    {
        bool IsValidUser;
        //Before method Execution
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            //To skip the Authentication
            //var skipAuthentication = filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true) ||
            //                    filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(
            //                        typeof(AllowAnonymousAttribute), true);
            //if (skipAuthentication)
            //{
            //    return;
            //}

            //throw new NotImplementedException();
            //If there's .Result in this method then OnAuthenticationChallenge will execute.

            //Commented(Modified to check the Valid Session or not for Single Instance)
            //if (filterContext.HttpContext.Request.Cookies.Get("CookieUserIn") != null
            //    && HttpContext.Current.Session["UserDetails"] != null && HelpClass.IsUserAuthenticated())
            if (HelpClass.IsUserAuthenticated())
            {
                IsValidUser = true;
            }
            else
            {
                IsValidUser = false;
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }
        // On Method Executing
        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            //throw new NotImplementedException();
            //filterContext.Result = new HttpUnauthorizedResult();
            //code for Authenticating the USer

            //Commented on  13-03-2019 (For Authenticating the Valid session to Json requests also)
            //if (!IsValidUser)
            //{
            //    filterContext.Result = new ViewResult { ViewName = "UnAuthenticatedView" };
            //}

            if (!IsValidUser)
            {
                // if (((ReflectedActionDescriptor)filterContext.ActionDescriptor).MethodInfo.ReturnType == typeof(JsonResult))
                //if (filterContext.Controller.GetType().GetMethod(filterContext.RouteData.Values["action"].ToString(),
                //    BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance).ReturnType == typeof(JsonResult))

                //Getting the Type of request wether post or get (since we are using same name for get and post methods)
                Type typeOfRequest = filterContext.HttpContext.Request.RequestType.ToLower() == "get" ? typeof(HttpGetAttribute) : typeof(HttpPostAttribute);
                //Retriving the current method by controller and its details.
                var cntMethods = filterContext.Controller.GetType().GetMethods()
                 .Where(m =>
                    m.Name == filterContext.RouteData.Values["action"].ToString() &&
                    ((typeOfRequest == typeof(HttpPostAttribute) &&
                          m.CustomAttributes.Where(a => a.AttributeType == typeOfRequest).Count() > 0
                       )
                       ||
                       (typeOfRequest == typeof(HttpGetAttribute) &&
                          m.CustomAttributes.Where(a => a.AttributeType == typeof(HttpPostAttribute)).Count() == 0
                       )
                    )
                );
                // Getting the method information.
                MethodInfo actionMethodInfo = actionMethodInfo = cntMethods != null && cntMethods.Count() == 1 ? cntMethods.ElementAt(0) : null;
                //Checking the return type of the current method(Eg: ActionResult, Json, Javascript,View, Partialview)
                if (actionMethodInfo != null)//Condition if cshtml is post and .cs method is null(Default Get)
                {
                    if (actionMethodInfo.ReturnType == typeof(JsonResult))
                    {
                        filterContext.Result = new JsonResult
                        {
                            Data = new { msg = "Not an Authenticated User", msgRedirecttoLoginPage = true },
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet
                        };
                    }
                    else
                    {
                        filterContext.Result = new ViewResult { ViewName = "UnAuthenticatedView" };
                    }
                }
                else
                {
                    filterContext.Result = new ViewResult { ViewName = "UnAuthenticatedView" };
                }
            }

        }
    }
}