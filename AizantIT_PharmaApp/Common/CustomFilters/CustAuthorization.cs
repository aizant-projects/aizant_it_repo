﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AizantIT_PharmaApp.Common.CustomFilters
{
    public class CustAuthorization : AuthorizeAttribute
    {
        private int[] _AllowedRoles;
        private int _ModuleID;
        private bool _hasAccess;

        public CustAuthorization(int ModuleID, params int[] AllowedRoles)
        {
            this._ModuleID = ModuleID;
            this._AllowedRoles = AllowedRoles;
        }
        
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (SkipAuthorization(filterContext))
                return;

            _hasAccess = false;
            if (HttpContext.Current.Session["UserDetails"] != null)
            {
                DataSet ds = HttpContext.Current.Session["UserDetails"] as DataSet;
                DataTable dtRoles = ds.Tables[1];
                if (dtRoles.Rows.Count > 0)
                {
                    List<String> ModuleRoles = new List<String>();
                    DataRow[] drUserRoles = dtRoles.Select("ModuleID=" + _ModuleID);
                    foreach (DataRow drAssignedRoles in drUserRoles)
                    {
                        if (_AllowedRoles.Contains((int)drAssignedRoles["RoleID"]))
                        {
                            _hasAccess = true;
                        }
                        ModuleRoles.Add(drAssignedRoles["RoleID"].ToString());
                    }
                    //filterContext.Controller.TempData["EmpModuleRoles"] = ModuleRoles.ToArray();
                    HttpContext.Current.Session["EmpModuleRoles"]= ModuleRoles.ToArray();
                    //filterContext.Controller.TempData.Keep("EmpModuleRoles");
                    //if (filterContext.Controller.TempData["EmpModuleRoles"]!=null)
                    //{
                    //    string[] data = (string[])filterContext.Controller.TempData["EmpModuleRoles"];
                    //}
                }
            }

            if (!_hasAccess)
            {
                switch (_ModuleID)
                {
                    case 0:
                        filterContext.Result = new ViewResult { ViewName = "MainUnAuthorisedView" };
                        //filterContext.Controller.TempData.Keep("EmpModuleRoles");
                        break;
                    case 1:
                        filterContext.Result = new ViewResult { ViewName = "~/Areas/AdminSetup/Views/Shared/_AdminSetupUnAuthorisedView.cshtml" };
                        break;
                    case 2:
                        filterContext.Result = new ViewResult { ViewName = "DMS_UnAuthorisedView" };
                        break;
                    case 3:
                        filterContext.Result = new ViewResult { ViewName = "TMS_UnAuthorisedView" };
                        break;
                    case 4:
                        filterContext.Result = new ViewResult { ViewName = "QMS_UnAuthorisedView" };
                       // filterContext.Controller.TempData.Keep("EmpModuleRoles");
                        break;
                    case 7:
                        filterContext.Result = new ViewResult { ViewName = "_RegulatoryUnAuthorizedView" };
                        break;
                        //default:
                        //    filterContext.HttpContext.Response.Cookies["CookieUserIn"].Expires = DateTime.Now.AddDays(-1);
                        //    filterContext.Result = new RedirectResult("~//UserLogin.aspx");
                        //    break;
                }
            }
            else
            {
                if (HttpContext.Current.Session["UserDetails"] != null)
                {
                    filterContext.Controller.TempData["EmpID"] = (HttpContext.Current.Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                    filterContext.Controller.TempData["EmpLoginID"] = (HttpContext.Current.Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpLoginID"].ToString();
                    //string LoginID = filterContext.Controller.TempData["EmpLoginID"].ToString();
                }
            }
        }
        private static bool SkipAuthorization(AuthorizationContext filterContext)
        {
            Contract.Assert(filterContext != null);

            return filterContext.ActionDescriptor.GetCustomAttributes(typeof(AllowAnonymousAttribute), true).Any()
                   || filterContext.ActionDescriptor.ControllerDescriptor.GetCustomAttributes(typeof(AllowAnonymousAttribute), true).Any();
        }
       
    }
}