﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.Common
{
    public class jQueryDataTableParamModel
    {
        public string sSortDir_0 { get; set; }

        public string sSearch { get; set; }

        public int iDisplayLength { get; set; }

        public int iDisplayStart { get; set; }

        public int iColumns { get; set; }

        public int iSortCol_0 { get; set; }

        public string param1 { get; set; }

    }
}