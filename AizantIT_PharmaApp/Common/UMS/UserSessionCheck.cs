﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.Common.UMS
{
    public static class UserSessionCheck
    {
        public static bool IsValidSession(string LoginID, string CurrentUserSession)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_UserSessionCheck]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@LoginID", LoginID);
                cmd.Parameters.AddWithValue("@UserSessionID", CurrentUserSession);
                conn.Open();
                bool result= cmd.ExecuteScalar() != null ? Convert.ToBoolean(cmd.ExecuteScalar()) : false;
                return result;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }

        public static void ResetActiveUserSession(string loginID, string userSessionID)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
            try
            {
                SqlCommand cmd = new SqlCommand("[UMS].[AizantIT_SP_ResetLoggedInSession]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@LoginID", loginID);
                cmd.Parameters.AddWithValue("@UserSessionID", userSessionID);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
    }
}