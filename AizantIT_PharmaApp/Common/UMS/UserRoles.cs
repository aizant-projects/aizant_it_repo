﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.Common.UMS
{
    public class UserRoles
    {
        public static DataTable GetEmpRoles(int ModuelID)
        {
            DataTable dtx = (HttpContext.Current.Session["UserDetails"] as DataSet).Tables[1];
            DataTable dtTemp = new DataTable();
            DataRow[] drTmsRoles = dtx.Select("ModuleID=" + ModuelID);
            dtTemp = drTmsRoles.CopyToDataTable();
            return dtTemp;
        }
    }
}