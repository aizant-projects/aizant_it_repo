export interface DepartmentsListStructure{
    Department_ID:number
    DepartmentName:string
}

export interface QualityEventsListStructure{
    QualityEvent_TypeID:number
    QualityEvent_TypeName:string
}

export const QualityEventIDs={
    IncidentReport:1,
    ChangeControlNote:2,
    Errata:3,
    NoteToFile:4
}

export const QualityEvents={
    ChangeControlNote:'Change Control Note',
    IncidentReport:'Incident Report',
    Errata:'Errata',
    NoteToFile:'Note To File'
}

export const QualityEventStatus={
    Approved:'Approved',
    Closed:'Closed',
    OverDue:'OverDue',
    Pending:'Pending',
    Rejected:'Rejected',
    OverdueLessThanOneWk:"< 1wk",
    OverdueOneWkToOneMn:"1wk-1mn",
    OverdueOneMnToThreeMn:"1mn-3mn",
    OverdueThreeMnToSixMn:"3mn-6mn",
    OverdueGreaterThanSixMn:">6mn"
}

export interface QualityEventsDataStructure{
    DepartmentName: string
    TotalRecordCCN: number
    TotalRecordIncident: number
    TotalRecordNTF: number
    TotalRecordsErrata: number
}

export interface AllDepartmentQualityEventsFilters{
     DeptID:number[],
     QualityEventType:number[],
    FromDate:string,
    ToDate:string
}

export interface QualityEventDataOfOneDept{
    department:number,
    qualityEvent:number,
    fromDate:string,
    toDate:string
}

export interface CCNFilters{
    departments:number[],
    fromDate:string,
    toDate:string,
    changeCategory:number[],
    changeClassification:number[]
}

export interface IncidentFilters{
    departments:number[],
    fromDate:string,
    toDate:string,
    incidentCategory:number[],
    incidentClassification:number[]
}

export interface CCNOverdueFilters{
    departments:number[],
    fromDate:string,
    toDate:string,
    changeCategory:number[],
    changeClassification:number[]
}

export interface IncidentOverdueFilters{
    departments:number[],
    fromDate:string,
    toDate:string,
    incidentCategory:number[],
    incidentClassification:number[]
}


export interface StatusChartFilters{
    DeptID: number[]
    QualityEventType: number
    CCNClassification:number[] | number
    CCNCategory:number
    IncidentClassification:number
    IncidentCategory:number
    FromDate:string
    ToDate:string
}

export interface ChangeCategoriesListStructure{
    changeCategoryID:number
    changeCategoryName:string
}

export interface ChangeClassificationListStructure{
    changeClassificationID:number|number[]
    changeClassificationName:string
}

export const ChangeCategoriesListValues={
    permanent:"Permanent",
    temporary:"Temporary"
}

export const ChangeClassificationListValues={
    major:"Major",
    minor:"Minor",
    unclassified:"Unclassified"
}

export const CCNClassificationStatusCodes={
    Major:1,
    Minor:2,
    Unclassified:7,
    All:0
}

export const CCNCategoryStatusCodes={
    Permanent:3,
    Temporary:4,
    Both:0
}

export interface IncidentCategoriesListStructure{
    TypeofCategoryID: number
    TypeofIncident: string
}

export interface IncidentClassificationListStructure{
    incidentClassificationID:number
    incidentClassificationName:string
}

export const IncidentClassificationListValues={
    qualityImpacting:"Quality Impacting",
    qualityNonImpacting:"Quality Non-Impacting",
    unclassified:"Unclassified"
}

export const IncidentClassificationStatusCodes={
    QualityImpacting:5,
    QualityNonImpacting:6,
    Unclassified:8,
    All:0
}

export interface StatusChartFilterValuesReturnStructure{
    EventName:string,
    TotalRecords:number
}

export const QualityEventIds={
    IncidentReport:1,
    CCN:2,
    Errata:3,
    NoteToFile:4
}

export interface OverdueDataFilters{
    DeptID: number[]
    QualityEventType: number
    CCNClassification:number[] | number
    CCNCategory:number
    IncidentClassification:number
    IncidentCategory:number
    FromDate:string
    ToDate:string
}

export interface OverdueListStructure{
    EventName: string
    TotalRecords: number
}

export interface DeptPerformanceDataFilters{
    DeptID: number
    QualityEventType: number
    CCNClassification:number|number[]
    CCNCategory:number
    IncidentClassification:number
    IncidentCategory:number
    FromDate:string
    ToDate:string
}

export interface DeptPerformanceDataStructure{
    Count: number
    DeptAvg: number
    Monthname: string
    OrgAvg: number
    MonthnameAndYear?:string
}

export interface MetricSelectionDropdownStructure{
    metricId:number,
    metricName:string
}
export const MetricsListValues={
    CountMetricID:0,
    countMetricName:"Count",
    TATMetricID:1,
    tatMetricName:"Turn Around Time"
}

export interface SampleRecords{
    ccnNumber: string;
    department: string;
    createdBy: string;
    createdDate: string;
    dueDate: string;
    classification: string;
    status: string;
}

export interface CCNListParams{
    DeptID: number[]
    CCNClassification:number[] | number
    CCNCategory:number
    FromDate:string
    ToDate:string
    showtype:number
}

export interface CCNListStructure{
    CCN_No: string    ​​​
    ChangeClassification: string​​​
    CreatedBy: string​​​
    CreatedDate: string​​​
    DepartmentName: string​​​
    DueDate: string​​​
    NumberofDueDays: number​​​
    StatusName: string​​​
    assignTo: string
} 

export interface IncidentListParams{
    DeptID: number[]
    IncidentClassification:number[] | number
    IncidentCategory:number
    FromDate:string
    ToDate:string
    showtype:number
}

export interface IncidentListStructure{
    TypeofIncident: string
    Category:string
    CreatedBy: string
    DateOfOccurance: string
    DateOfReport: string
    DepartmentName: string
    DueDate: string
    Incident_No: string
    NumberofDueDays: number
    StatusName: string
    assignTo: string
}

export interface ErrataListParams{
    DeptID: number[]
    FromDate:string
    ToDate:string
    showtype:number
}

export interface ErrataListStructure{
    CreatedDate: string
    Docname: string
    Errata_No: string
    referencedocno: string
    CreatedBy: string
    DepartmentName: string
    StatusName: string
    assignTo: string
}

export interface NTFListParams{
    DeptID: number[]
    FromDate:string
    ToDate:string
    showtype:number
}

export interface NTFListStructure{
    CreatedBy: string
    CreatedDate: string
    DepartmentName: string
    Docname: string
    NTF_No: string
    StatusName: string
    assignTo: string
    referencedocno: string
}
export interface CCNListFiltersForDeptPerformance{
    DeptID:number
    CCNClassification:number|number[]
    CCNCategory :number
    FromDate:string
    ToDate:string
}

export interface CCNListStructureForDeptPerformance{
    Month:string,
    CCN_No: string,    ​​​
    ChangeClassification: string,
    CreatedBy: string,
    CreatedDate: string,
    DepartmentName: string,
    DueDate: string,
    CompletedDate: string,
    TAT: number,
    monthlyTAT?:number,
    totalRecordsInMonth?:number,
    MonthnameAndYear?:string
}

export interface IncidentListFiltersForDeptPerformance{
    DeptID:number
    IncidentClassification:number
    IncidentCategory :number
    FromDate:string
    ToDate:string
}

export interface IncidentListStructureForDeptPerformance{
    Category: string
    CompletedDate: string
    CreatedBy: string
    CreatedDate: string
    DateOfOccurance: string
    DateOfReport: string
    DepartmentName: string
    DueDate:string
    Incident_No: string
    Month: string
    TAT: number
    TypeofIncident: string,
    monthlyTAT?:number,
    totalRecordsInMonth?:number
    MonthnameAndYear?:string
}

export interface ErrataListFiltersForDeptPerformance{
    DeptID:number
    FromDate:string
    ToDate:string
}

export interface ErrataListStructureForDeptPerformance{
    CompletedDate: string
    CreatedBy: string
    CreatedDate: string
    DepartmentName: string
    DocName: string
    Errata_No: string
    Month: string
    TAT: number
    refdocno: string,
    monthlyTAT?:number,
    totalRecordsInMonth?:number
    MonthnameAndYear?:string
}

export interface NTFListFiltersForDeptPerformance{
    DeptID:number
    FromDate:string
    ToDate:string
}

export interface NTFListStructureForDeptPerformance{
    CompletedDate: string
    CreatedBy: string
    CreatedDate: string
    DepartmentName: string
    DocName: string
    Month: string
    NTF_No: string
    TAT: number
    refdocno: string,
    monthlyTAT?:number,
    totalRecordsInMonth?:number
    MonthnameAndYear?:string
}

export const ShowTypeList={
    OverallList:1,
    Approved:2,
    Pending:3,
    Closed:4,
    Rejected:5,
    OverallOverdueList:6,
    OverdueLessThanOneWk:7,
    OverdueOneWkToOneMn:8,
    OverdueOneMnToThreeMn:9,
    OverdueThreeMnToSixMn:10,
    OverdueGreaterThanSixMn:11
}

export interface rowDetailsForExcelCustomization{
    startingCellNum:string,
    rowNum:string
}

//CAPA

export interface CAPAChartFilters{
    DeptID:number[] 
    QualityEventType:number[] 
    FromDate :string
    ToDate :string
}

export interface CAPAMainChartDataStructure{
    DepartmentName:string
    TotalRecordNTF:number
    TotalRecordCCN:number
    TotalRecordIncident:number
    TotalRecordOOS:number
    TotalRecordOOT:number
    TotalRecordMC:number
}

export interface TypeOfActionsListStrcture{
    typeOfActionId:number,
    typeOfActionName:string
}

export const TypeOfActionNames={
    preventive:'Preventive',
    corrective:'Corrective',
    correctiveAndPreventive:'Corrective & Preventive'
}

export const TypeOfActionStatusCodes={
    Preventive:1,
    Corrective:2,
    CorrectiveAndPreventive:3
}

export interface CAPAStatusChartFilters{
    DeptID:number[],
    QualityEventType :number,
    TypeOfAction:number,
    FromDate:string,
    ToDate:string
}

export interface CAPAOverdueChartFilters{
    DeptID :number[],
    QualityEventType :number,
    TypeOfAction:number,
    FromDate:string,
    ToDate:string
}

export const TrimmedDepartmentNames={
    "Analytical Development":'AD',
    "Business Development":'BD',
    "Clinical Pharmacology":'CP',
    "Engineering":'Engg',
    "Executive Managements":'EM',
    "Human Resources":'HR',
    "Production":'Prod.',
    "Quality Assurance":'QA',
    "Quality Control":'QC',
    "Regulatory Affairs":'Reg. Aff',
    "Warehouse":'WH'
}