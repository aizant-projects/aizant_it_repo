import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { DashboardsModule } from './dashboards/dashboards.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    DashboardsModule,
    BrowserAnimationsModule,
  ],
  providers: [
    {
      provide: APP_BASE_HREF,
    useValue: '/QMS/DashBoard/QMS_Dashboard'
  },
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
