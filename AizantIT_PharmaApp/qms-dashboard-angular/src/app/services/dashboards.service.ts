import { Injectable } from '@angular/core';
import { OverdueListStructure } from '../models/dashboards';

@Injectable({
  providedIn: 'root'
})


export class DashboardsService {
public statusChartColorPalette = ["#003f5c","#665191","#d45087","#f95d6a"];
public colorPalette1=["#003f5c","#2f4b7c","#665191","#a05195","#d45087","#f95d6a","#ff7c43","#ffa600"];
public colorPalette2=["#1565c0", "#f9a825","#2e7d32","#ef6c00","#4e342e","#37474f","#178b7e","#042347","#424242","#4d8d36"]
months:string[]=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

  constructor() { }

  getColor(QualityEventID):string{
    return this.colorPalette1[QualityEventID];
  }

  getMonth(monthNum:number){
    return this.months[monthNum-1];
  }

getModifiedOverdueData(overdueData):OverdueListStructure[]{
//   var localData = overdueData.sort(function (a, b) {
//     return b.count - a.count;
// });
var localData = overdueData;
var totalCount = localData.reduce(function (prevValue, item) {
    return prevValue + item.TotalRecords;
}, 0),
cumulativeCount = 0;
return localData.map(function (item, index) {
cumulativeCount += item.TotalRecords;
return {
    EventName: item.EventName,
    TotalRecords: item.TotalRecords,
    cumulativePercent: Math.round(cumulativeCount * 100 / totalCount)
};
});
}
}
