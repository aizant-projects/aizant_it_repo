import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AllDepartmentQualityEventsFilters } from '../models/dashboards';

@Injectable({
  providedIn: 'root'
})
export class ServerService {

  constructor(private httpClient:HttpClient) { }

  getAllDepartmentsList():Observable<any>{
    return this.httpClient.get(`${environment.serverUrl}/GetdepartmentList`);
  }

  getQualityEventsList():Observable<any>{
    return this.httpClient.get(`${environment.serverUrl}/GetQualityEventList`);
  }

  getIncidentCategoryList():Observable<any>{
    return this.httpClient.get(`${environment.serverUrl}/BindIncidentCategory`);
  }

  getDepartmentsQualityEventsData(departmentsQualityEventsForData?:AllDepartmentQualityEventsFilters):Observable<any>{
    return this.httpClient.post(`${environment.serverUrl}/ManagementChart`,departmentsQualityEventsForData);
  }

  getQualityEventStatusOfOneDept(qualityEventDataOfOneDept):Observable<any>{
    return this.httpClient.post(`${environment.serverUrl}/StatusChartForQualityEvents`,qualityEventDataOfOneDept);
  }

  getOverdueData(overdueDataFilters):Observable<any>{
    return this.httpClient.post(`${environment.serverUrl}/OverdueChart`,overdueDataFilters);
  }

  getDeptPerformanceDataOfCountMetric(deptPerformanceDataFilters):Observable<any>{
    return this.httpClient.post(`${environment.serverUrl}/GetPerformanceChartCount`,deptPerformanceDataFilters);
  }

  getDeptPerformanceDataOfTATMetric(deptPerformanceDataFilters):Observable<any>{
    return this.httpClient.post(`${environment.serverUrl}/GetPerformanceChartTAT`,deptPerformanceDataFilters);
  }

  getCCNList(ccnListFilters):Observable<any>{
    return this.httpClient.post(`${environment.serverUrl}/GetCCNList`,ccnListFilters)
  }

  getIncidentList(incidentListFilters):Observable<any>{
    return this.httpClient.post(`${environment.serverUrl}/GetIncidentList`,incidentListFilters)
  }
  getErrataList(errataListFilters):Observable<any>{
    return this.httpClient.post(`${environment.serverUrl}/GetErrataList`,errataListFilters)
  }
  getNTFList(noteToFileListFilters):Observable<any>{
    return this.httpClient.post(`${environment.serverUrl}/GetNTFList`,noteToFileListFilters)
  }

  getCCNListForDeptPerformance(ccnListFilters):Observable<any>{
    return this.httpClient.post(`${environment.serverUrl}/GetCCNListViewForPerformanceChart`,ccnListFilters)
  }
  getIncidentListForDeptPerformance(incidentListFilters):Observable<any>{
    return this.httpClient.post(`${environment.serverUrl}/GetIncidentListViewForPerformanceChart`,incidentListFilters)
  }
  getErrataListForDeptPerformance(errataListFilters):Observable<any>{
    return this.httpClient.post(`${environment.serverUrl}/GetErrataListViewForPerformanceChart`,errataListFilters)
  }
  getNTFListForDeptPerformance(noteToFileListFilters):Observable<any>{
    return this.httpClient.post(`${environment.serverUrl}/GetNTFListViewForPerformanceChart`,noteToFileListFilters)
  }

  getQualityEventsListForCAPA():Observable<any>{
    return this.httpClient.get(`${environment.serverUrl}/GetQualityEventListforCAPAChart`);
  }

  getDeptQualityEventsDataForCAPA(departmentsQualityEventsFiltersForCAPA):Observable<any>{
    return this.httpClient.post(`${environment.serverUrl}/CapaBarChart`,departmentsQualityEventsFiltersForCAPA);
  }

  getQualityEventStatusOfOneDeptForCAPA(departmentsQualityEventStatusFiltersForCAPA):Observable<any>{
    return this.httpClient.post(`${environment.serverUrl}/StatusChartForCAPA`,departmentsQualityEventStatusFiltersForCAPA);
  }

  
}
