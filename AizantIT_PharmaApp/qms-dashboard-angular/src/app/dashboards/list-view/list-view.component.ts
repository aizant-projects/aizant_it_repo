import { Component, EventEmitter, Input, OnInit, Output,ChangeDetectorRef,AfterViewInit, ViewChild, Renderer2 } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DataTableDirective } from 'angular-datatables';
import { combineLatest,Subject,Subscription } from 'rxjs';
import { CCNListStructure, ErrataListStructure, IncidentListStructure, NTFListStructure, QualityEvents, SampleRecords } from 'src/app/models/dashboards';

@Component({
  selector: 'app-list-view',

template:`
<div class="modal-header">
       <h4 id="dialog-sizes-name1" class="modal-title pull-left">{{currentDrillDownHeaderText}}</h4>
       <button type="button" class="close pull-right" (click)="onCloseListView()" aria-label="Close">
         <span aria-hidden="true">&times;</span>
       </button>
     </div>
     <div class="modal-body">
   <table datatable [dtOptions]="dtOptions" class="table datatable_cust table-striped table-bordered table-sm row-border hover">
           <thead>
               <tr >
                 <th *ngFor="let eachCol of dtOptions.columns">{{eachCol.title}}</th>
               </tr>
               
             </thead>
             <tbody *ngIf="qualityEvent===qualityEventsNames.ChangeControlNote">
               <tr *ngFor="let record of records">
                 <td>{{ record.CCN_No }}</td>
                 <td>{{ record.DepartmentName }}</td>
                 <td>{{ record.CreatedBy }}</td>
                 <td>{{ record.CreatedDate }}</td>
                 <td>{{ record.DueDate }}</td>
                 <td>{{ record.NumberofDueDays }}</td>
                 <td>{{ record.ChangeClassification }}</td>
                 <td>{{ record.StatusName }}</td>
                 <td>{{ record.assignTo }}</td>
               </tr>
             </tbody>
             <tbody *ngIf="qualityEvent===qualityEventsNames.IncidentReport">
               <tr *ngFor="let record of records">
                 <td>{{ record.Incident_No }}</td>
                 <td>{{ record.DepartmentName }}</td>
                 <td>{{ record.TypeofIncident }}</td>
                 <td>{{ record.Category }}</td>
                 <td>{{ record.CreatedBy }}</td>
                 <td>{{ record.DateOfOccurance }}</td>
                 <td>{{ record.DateOfReport }}</td>
                 <td>{{ record.DueDate }}</td>
                 <td>{{ record.NumberofDueDays }}</td>
                 <td>{{ record.StatusName }}</td>
                 <td>{{ record.assignTo }}</td>
               </tr>
             </tbody>
             <tbody *ngIf="qualityEvent===qualityEventsNames.Errata">
               <tr *ngFor="let record of records">
                 <td>{{ record.Errata_No }}</td>
                 <td>{{ record.DepartmentName }}</td>
                 <td>{{ record.Docname }}</td>
                 <td>{{ record.referencedocno }}</td>
                 <td>{{ record.CreatedBy }}</td>
                 <td>{{ record.CreatedDate }}</td>
                 <td>{{ record.StatusName }}</td>
                 <td>{{ record.assignTo }}</td>
               </tr>
             </tbody>
             <tbody *ngIf="qualityEvent===qualityEventsNames.NoteToFile">
               <tr *ngFor="let record of records">
                 <td>{{ record.NTF_No }}</td>
                 <td>{{ record.DepartmentName }}</td>
                 <td>{{ record.Docname }}</td>
                 <td>{{ record.referencedocno }}</td>
                 <td>{{ record.CreatedBy }}</td>
                 <td>{{ record.CreatedDate }}</td>
                 <td>{{ record.StatusName }}</td>
                 <td>{{ record.assignTo }}</td>
               </tr>
             </tbody>
             <tfoot>
             <tr >
             <td *ngFor="let eachC of dtOptions.columns"><input type="text" placeholder="Search"></td>
             </tr>
             </tfoot>
       </table>
     </div>`  ,
     styleUrls: ['../management-level-chart/management-level-chart.component.scss']
})
export class ListViewComponent implements OnInit,AfterViewInit {

  dtOptions: any = {};
  CCNdtOptions:any={};
  incidentDtOptions:any={};
  errataDtOptions:any={};
  noteToFileDtOptions:any={};
  currentDrillDownHeaderText: string;
  records:CCNListStructure[] | IncidentListStructure[] | ErrataListStructure[] | NTFListStructure[];
  ccnRecords:CCNListStructure[];
  incidentRecords:IncidentListStructure[];
  errataRecords:ErrataListStructure[];
  noteToFileRecords:NTFListStructure[];
  qualityEvent:string;
  fileName:string;
  qualityEventsNames=QualityEvents;
  showType:number;

  
  events: Subscription[] = [];
  
  @ViewChild(DataTableDirective)
  datatableElement: DataTableDirective;

  constructor(public bsModalRef: BsModalRef,public modalService:BsModalService,private changeDetection: ChangeDetectorRef,
    private renderer:Renderer2) { }


  ngOnInit(): void {

    this.CCNdtOptions = {
      
      columns: [{
        title: 'CCN Number',
        data: 'ccnNumber'
      }, {
        title: 'Department',
        data: 'department'
      }, {
        title: 'Created By',
        data: 'createdBy'
      },
      {
        title: 'Created Date',
        data: 'createdDate'
      },
      {
        title: 'Due Date',
        data: 'dueDate'
      },
      {
        title: 'No. of Due Days',
        data: 'dueDays',
      },
      {
        title: 'Classification',
        data: 'classification'
      },
      {
        title: 'Status',
        data: 'status'
      },
      {
        title: 'Assign To',
        data: 'assignTo'
      }
    ],
    columnDefs: [
      { "visible": (this.showType>5 && this.showType<12), "targets": 5 }
    ],
    responsive:true,
    scrollX:true,
      // Declare the use of the extension in the dom parameter
      dom: 'lBfrtip',
      // Configure the buttons
      buttons: [
        {
          extend: 'excelHtml5',
          
          text:'<img src="/Images/QMS_Dashboard_Angular/excel.png">',
          title:null
      }
      ],
    };
    
    this.incidentDtOptions = {
      
      columns: [{
        title: 'Incident_No',
        data: 'incidentNumber'
      }, {
        title: 'Department',
        data: 'department'
      }, 
      {
        title: 'Type of Incident',
        data: 'typeOfIncident'
      },
      {
        title:'Category',
        data:'category'
      },
      {
        title: 'Created By',
        data: 'createdBy'
      },
      {
        title: 'Date of Occurence',
        data: 'dateOfOccurance'
      },
      {
        title: 'Date of Report',
        data: 'dateOfReport'
      },
      {
        title: 'Due Date',
        data: 'dueDate'
      },
      {
        title: 'No. of Due Days',
        data: 'dueDays',
      },
      {
        title: 'Status',
        data: 'status'
      },
      {
        title: 'Assign To',
        data: 'assignTo'
      }
    ],
    columnDefs: [
      { "visible": (this.showType>5 && this.showType<12), "targets": 8 }
    ],
    responsive:true,
    scrollX:true,
      // Declare the use of the extension in the dom parameter
      dom: 'lBfrtip',
      // Configure the buttons
      buttons: [
        {
          extend: 'excel',
          text:'<img src="/Images/QMS_Dashboard_Angular/excel.png">',
          title:null
      }
      ],
    };

    this.errataDtOptions = {
      columns: [{
        title: 'Errata Number',
        data: 'errataNumber'
      }, {
        title: 'Department',
        data: 'department'
      },
      {
        title: 'Document',
        data: 'document'
      },
      {
        title: 'Ref. Doc No.',
        data: 'refDocNum'
      }, {
        title: 'Created By',
        data: 'createdBy'
      },
      {
        title: 'Created Date',
        data: 'createdDate'
      },
      {
        title: 'Status',
        data: 'status'
      },
      {
        title: 'Assign To',
        data: 'assignTo'
      }
    ],
    responsive:true,
    scrollX:true,
      // Declare the use of the extension in the dom parameter
      dom: 'lBfrtip',
      // Configure the buttons
      buttons: [
        {
          extend: 'excelHtml5',
          
          text:'<img src="/Images/QMS_Dashboard_Angular/excel.png">',
          title:null
      }
      ],
    };

    this.noteToFileDtOptions = {
      columns: [{
        title: 'NTF Number',
        data: 'noteToFileNumber'
      }, {
        title: 'Department',
        data: 'department'
      },
      {
        title: 'Document',
        data: 'document'
      },
      {
        title: 'Ref. Doc No.',
        data: 'refDocNum'
      }, {
        title: 'Created By',
        data: 'createdBy'
      },
      {
        title: 'Created Date',
        data: 'createdDate'
      },
      {
        title: 'Status',
        data: 'status'
      },
      {
        title: 'Assign To',
        data: 'assignTo'
      }
    ],
    responsive:true,
    scrollX:true,
      // Declare the use of the extension in the dom parameter
      dom: 'lBfrtip',
      // Configure the buttons
      buttons: [
        {
          extend: 'excelHtml5',
          
          text:'<img src="/Images/QMS_Dashboard_Angular/excel.png">',
          title:null
      }
      ],
    };

    var trimmedQualityEventName='';
    if(this.qualityEvent===QualityEvents.ChangeControlNote){
      trimmedQualityEventName='CCN';
      this.dtOptions=this.CCNdtOptions;
      this.records=this.ccnRecords;
    }
    else if(this.qualityEvent===QualityEvents.IncidentReport){
      trimmedQualityEventName='IR';
      this.dtOptions=this.incidentDtOptions;
      this.records=this.incidentRecords;
    }
    else if(this.qualityEvent===QualityEvents.NoteToFile){
      trimmedQualityEventName='NTF';
      this.dtOptions=this.noteToFileDtOptions;
      this.records=this.noteToFileRecords;
    }
    else{
      trimmedQualityEventName='Errata';
      this.dtOptions=this.errataDtOptions;
      this.records=this.errataRecords;
    }
    if(this.showType>5 && this.showType<12){
      this.dtOptions.buttons[0].filename='OverDue' +' - '+ trimmedQualityEventName + this.fileName;
    }else{
      this.dtOptions.buttons[0].filename=trimmedQualityEventName + this.fileName;
    }
    // let excelButton = document.getElementsByClassName('buttons-html5');
    // console.log(excelButton);
    //     this.renderer.addClass(excelButton[0],'btn btn-outline');
  }

  ngAfterViewInit():void{

    
    const _combine = combineLatest(
      [this.modalService.onShow,
      this.modalService.onShown,
      this.modalService.onHide,
      this.modalService.onHidden]
    ).subscribe(() => this.changeDetection.markForCheck());
    
    this.events.push(
      this.modalService.onShown.subscribe((reason: string) => {
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.columns().every(function () {
            const that = this;
            $('input', this.footer()).on('keyup change', function () {
              if (that.search() !== this['value']) {
                that
                  .search(this['value'])
                  .draw();
              }
            });
          });
        });
      // }
      // else{
        // this.dtTrigger.next();
      // }
      })
    );
    this.events.push(_combine);

  }
onCloseListView(){
  this.bsModalRef.hide();
}


ngOnDestroy():void{
  this.events.forEach((subscription: Subscription) => {
    subscription.unsubscribe();
  });
  this.events = [];
}
}
