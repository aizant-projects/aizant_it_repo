import { Component, OnInit, Renderer2 } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CAPAChartFilters, CAPAMainChartDataStructure, CAPAOverdueChartFilters, CAPAStatusChartFilters, CCNCategoryStatusCodes, CCNClassificationStatusCodes, 
  ChangeCategoriesListStructure, ChangeCategoriesListValues, ChangeClassificationListStructure, 
  ChangeClassificationListValues, DepartmentsListStructure, IncidentCategoriesListStructure, 
  IncidentClassificationListStructure, IncidentClassificationListValues, IncidentClassificationStatusCodes, 
  OverdueDataFilters, 
  OverdueListStructure, 
  QualityEventIDs, 
  QualityEventIds, QualityEventsListStructure, QualityEventStatus, StatusChartFilters, StatusChartFilterValuesReturnStructure,
  TrimmedDepartmentNames, 
  TypeOfActionNames, 
  TypeOfActionsListStrcture,
  TypeOfActionStatusCodes} from '../../models/dashboards';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { DashboardsService } from 'src/app/services/dashboards.service';
import { ServerService } from 'src/app/services/server.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { FlatpickrOptions } from 'ng2-flatpickr';
import * as moment from 'moment';


@Component({
  selector: 'app-capa-chart',
  templateUrl: './capa-chart.component.html',
  styleUrls: ['./capa-chart.component.scss']
})
export class CapaChartComponent implements OnInit {
  CAPAfiltersForm:FormGroup;

  departmentsList :DepartmentsListStructure[]=[];
  selectedDepartments :DepartmentsListStructure[]=[];
  departmentDropdownSettings:IDropdownSettings = {};

  eventsDropdownSettings:IDropdownSettings={};
  qualityEventsList:QualityEventsListStructure[]=[];
  selectedQualityEvents:QualityEventsListStructure[]=[];

  typeOfActionsList:TypeOfActionsListStrcture[]=[];
  selectedTypeOfAction:TypeOfActionsListStrcture[]=[];
  typeOfActionDropdownSettings:IDropdownSettings={};

  changeClassificationList:ChangeClassificationListStructure[]=[];
  selectedChangeClassification:ChangeClassificationListStructure[]=[];
  changeClassificationDropdownSettings:IDropdownSettings={};
  selectedCCNClassificationIDs:any[]=[];

  incidentCategoriesList:IncidentCategoriesListStructure[]=[];
  selectedIncidentCategory:IncidentCategoriesListStructure[]=[];
  incidentCategoryDropdownSettings:IDropdownSettings={};

  incidentClassificationList:IncidentClassificationListStructure[]=[];
  selectedIncidentClassification:IncidentClassificationListStructure[]=[];
  incidentClassificationDropdownSettings:IDropdownSettings={};

  initialFromDate;
  initialToDate;
  fromDateValue:string;
  toDateValue:string;

  fromDateOptions: FlatpickrOptions = {
    dateFormat:"d-M-Y"
  };
  toDateOptions: FlatpickrOptions = {
    dateFormat:"d-M-Y"
  };

  qualityEventDataOfOneDept:CAPAStatusChartFilters;
  previousDataFiltersForStatusChart:CAPAStatusChartFilters;
  filteredQualityEventData:CAPAStatusChartFilters;
  currentQualityEventId = QualityEventIds;

  overallStatuslevel:number=0;
  allDepartmentQualityEventsFiltersForCAPA:CAPAChartFilters;
  previousDataFiltersForStackedBarChart:CAPAChartFilters;
  dataSource: CAPAMainChartDataStructure[];
  selectedFromDate:string;
  selectedToDate:string;
  selectedQualityEventIdForStatusChart:number;
  selectedDeptIds:number[]=[];
  selectedQualityEventIds:number[];
  subtitleForStackedBarChart: string;
  currentDrillDownHeaderText:string;
  prevCurrentDrillDownHeaderText:string;
  selectedQualityEventName:string;
  statusListData:StatusChartFilterValuesReturnStructure[];

  overdueDataFilters:CAPAOverdueChartFilters;
  overdueDataCAPA:OverdueListStructure[]=[];

  constructor(private dashboardService:DashboardsService, private fb:FormBuilder, private serverService:ServerService,
    public modalService:BsModalService, private renderer:Renderer2) {
    this.overallStatuslevel=0;

    this.setTypeOfActionDropdown();

    this.CAPAfiltersForm=this.fb.group({
      fromDate:[''],
      toDate:['']
    });
    this.getDepartmentsList();
    this.getQualityEventsListForCAPAChart();

    this.fromDateOptions.defaultDate = this.initialFromDate = moment().subtract(1,'years').format('DD-MMM-YYYY');
    this.toDateOptions.defaultDate= this.initialToDate=moment().format('DD-MMM-YYYY');

    this.getAllDepartmentQualityEventsCAPADataOnInitialLoad();
    this.subtitleForStackedBarChart = '( '+this.allDepartmentQualityEventsFiltersForCAPA.FromDate + ' to ' + this.allDepartmentQualityEventsFiltersForCAPA.ToDate +' )';
    
   }

  ngOnInit(): void {
    this.departmentDropdownSettings = {
      singleSelection: false,
      idField: 'Department_ID',
      textField: 'DepartmentName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 1,
      allowSearchFilter: true,
    };

    this.eventsDropdownSettings = {
      singleSelection: true,
      idField: 'QualityEvent_TypeID',
      textField: 'QualityEvent_TypeName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 1,
      allowSearchFilter: true,
    };
  }

  setTypeOfActionDropdown(){
    this.typeOfActionDropdownSettings={
      singleSelection: true,
      idField: 'typeOfActionID',
      textField: 'typeOfActionName',
      itemsShowLimit: 1,
      closeDropDownOnSelection:true
    }
    this.typeOfActionsList=[
      {typeOfActionId:TypeOfActionStatusCodes.Corrective,typeOfActionName:TypeOfActionNames.corrective},
      {typeOfActionId:TypeOfActionStatusCodes.Preventive,typeOfActionName:TypeOfActionNames.preventive},
      {typeOfActionId:TypeOfActionStatusCodes.CorrectiveAndPreventive,typeOfActionName:TypeOfActionNames.correctiveAndPreventive}
    ]
    this.selectedTypeOfAction[0]=this.typeOfActionsList[2];

  }

  getAllDepartmentQualityEventsCAPADataOnInitialLoad(){
    this.allDepartmentQualityEventsFiltersForCAPA={
      DeptID:this.getDepartmentListIds(),
      QualityEventType:this.getQualityEventIds(),
      FromDate:this.initialFromDate,
      ToDate:this.initialToDate
    }
    console.log("CAPA Chart filters",this.allDepartmentQualityEventsFiltersForCAPA);
    this.previousDataFiltersForStackedBarChart=this.allDepartmentQualityEventsFiltersForCAPA;
    this.getDepartmentsQualityEventsDataFromFiltersOfCAPA(this.allDepartmentQualityEventsFiltersForCAPA);
  }

  onSelectAllDepartments(items: any){
    this.selectedDepartments=items;
      this.onDeptQualityEventsCAPADataChartFilterChange(items);
  }

  onSelectAllQualityEvents(items:any){
    this.selectedQualityEvents=items;
      this.onDeptQualityEventsCAPADataChartFilterChange(items);
  }

  getColorOFQE(QualityEventNumber){
    return this.dashboardService.getColor(QualityEventNumber);
  }

  getColorPaletteForStatusChart():string[]{
    return this.dashboardService.colorPalette2;
  }

  //Extracting Department IDs from departmentsList Object array
  getDepartmentListIds():number[]{
    let departmentIds=[];
    for(let i=0;i<this.departmentsList.length;i++){
      departmentIds.push(this.departmentsList[i].Department_ID)
    }
    return departmentIds;
  }


  //Extracting Quality Event IDs from qualityEventsList Object array
  getQualityEventIds():number[]{
    let qualityEventIds=[];
    for(let i=0;i<this.qualityEventsList.length;i++){
      qualityEventIds.push(this.qualityEventsList[i].QualityEvent_TypeID)
    }
    return qualityEventIds;
  }

  //fetches chart data based on filters passed
  getDepartmentsQualityEventsDataFromFiltersOfCAPA(allDepartmentQualityEventsFiltersForCAPA?:CAPAChartFilters){
    this.serverService.getDeptQualityEventsDataForCAPA(allDepartmentQualityEventsFiltersForCAPA).subscribe((res)=>{
      console.log("capa data",res);
      this.dataSource=res.CapaCount;
      for(let i=0;i<this.dataSource.length;i++){
        this.dataSource[i].DepartmentName=TrimmedDepartmentNames[this.dataSource[i].DepartmentName];
      }
      this.dataSource = this.dataSource.sort(function(x,y){
        if(x.DepartmentName < y.DepartmentName) return -1;
        if(x.DepartmentName > y.DepartmentName) return 1;
        return 0;
      });
      // this.dataSource=this.dataSource;
      console.log("stacked bar data",this.dataSource);

    },(err)=>{
      console.error(err);
    });
  }

  //fetches departmentsList from DB and sorts it alphabetically
  getDepartmentsList(){
    // let roleAndEventID={
    //   roleID:2,
    //   eventId:3
    // }
    this.serverService.getAllDepartmentsList().subscribe((resultantDepartments)=>{
      this.departmentsList=resultantDepartments.DeptFevent_list;
      this.departmentsList = this.departmentsList.sort(function(x,y){
        if(x.DepartmentName < y.DepartmentName) return -1;
        if(x.DepartmentName > y.DepartmentName) return 1;
        return 0;
      });
      this.selectedDepartments=this.departmentsList;
      this.previousDataFiltersForStackedBarChart.DeptID=this.getDepartmentListIds();
    },(err)=>{
      console.log(err)
    });
  }

  //fetches qualityEventsList from DB and sorts it alphabetically
  getQualityEventsListForCAPAChart(){
    this.serverService.getQualityEventsListForCAPA().subscribe((resultantQualityEvents)=>{
      this.qualityEventsList = resultantQualityEvents.sort(function(x,y){
        if(x.QualityEvent_TypeName < y.QualityEvent_TypeName) return -1;
        if(x.QualityEvent_TypeName > y.QualityEvent_TypeName) return 1;
        return 0;
      });
      // this.selectedQualityEvents=this.qualityEventsList;
      this.selectedQualityEvents=[];
      this.previousDataFiltersForStackedBarChart.QualityEventType=this.getQualityEventIds();
    },(err)=>{
      console.error(err)
    });
  }

  //returns department ID from Dept name
  getDeptIdFromDeptName=(deptName):number=>{
    for(let i=0;i<this.departmentsList.length;i++){
      if(this.departmentsList[i].DepartmentName===deptName){
        return this.departmentsList[i].Department_ID;
      }
    }
  }

  getDeptNameFromDeptID(deptId):string{
    for(let i=0;i<this.departmentsList.length;i++){
      if(this.departmentsList[i].Department_ID===deptId){
        return this.departmentsList[i].DepartmentName;
      }
    }
  }

  getQualityEventNameFromQualityEventId(qualityEventID):string{
    for(let i=0;i<this.qualityEventsList.length;i++){
      if(this.qualityEventsList[i].QualityEvent_TypeID===qualityEventID){
        return this.qualityEventsList[i].QualityEvent_TypeName;
      }
    }
  }

  //returns qualityEvent ID from qualityEvent name
  getQualityEventIdFromQualityEventName=(qualityEventName):number=>{
    for(let i=0;i<this.qualityEventsList.length;i++){
      if(this.qualityEventsList[i].QualityEvent_TypeName===qualityEventName){
        return this.qualityEventsList[i].QualityEvent_TypeID;
      }
    }
  }

  //converts fetched date into DB compatible date format
  getRequiredDateFormat=(selectedDate):string=>{
    var tempDate = new Date(selectedDate);
    let sampleFromDate = moment(tempDate.toISOString());
    return sampleFromDate.format('DD-MMM-YYYY');
  }

  setFromAndToDates(){
    this.selectedFromDate = this.CAPAfiltersForm.get('fromDate').value[0];
    this.selectedToDate=this.CAPAfiltersForm.get('toDate').value[0];
  }

  getSelectedDeptIDs(){
    this.selectedDeptIds=[];
    for(let i=0;i<this.selectedDepartments.length;i++){
      this.selectedDeptIds.push(this.selectedDepartments[i].Department_ID);
    }
  }

  getSelectedQualityEventIDs(){
    this.selectedQualityEventIds=[];
    for(let i=0;i<this.selectedQualityEvents.length;i++){
      this.selectedQualityEventIds.push(this.selectedQualityEvents[i].QualityEvent_TypeID);
    }
  }

  getSelectedCCNClassificationIDs(){
    this.selectedCCNClassificationIDs=[];
    for(let i=0;i<this.selectedChangeClassification.length;i++){
      this.selectedCCNClassificationIDs.push(this.selectedChangeClassification[i].changeClassificationID);
    }
  }

  onDeptQualityEventsCAPADataChartFilterChange(event){
    this.dataSource=[];

    this.setFromAndToDates();


    if(this.overallStatuslevel===0){
     this.getSelectedDeptIDs();
     this.getSelectedQualityEventIDs();

    this.allDepartmentQualityEventsFiltersForCAPA={
      DeptID:this.selectedDepartments.length===0 ? []: this.selectedDeptIds,
      QualityEventType:((this.selectedQualityEvents.length===0)||(this.selectedQualityEvents.length===this.qualityEventsList.length)) ?[] : this.selectedQualityEventIds,
      FromDate : this.selectedFromDate !== undefined ? this.getRequiredDateFormat(this.selectedFromDate):this.initialFromDate,
      ToDate : this.selectedToDate !== undefined ? this.getRequiredDateFormat(this.selectedToDate) : this.initialToDate
    }
    console.log("on filter change",this.allDepartmentQualityEventsFiltersForCAPA);
    this.previousDataFiltersForStackedBarChart=this.allDepartmentQualityEventsFiltersForCAPA;

    this.subtitleForStackedBarChart = '( '+this.allDepartmentQualityEventsFiltersForCAPA.FromDate + ' to ' + this.allDepartmentQualityEventsFiltersForCAPA.ToDate +' )';

    this.getDepartmentsQualityEventsDataFromFiltersOfCAPA(this.allDepartmentQualityEventsFiltersForCAPA);
    }
  }

  getUntrimmedDeptName(trimmedDeptName):string{
    for(let i=0;i<this.departmentsList.length;i++){
      if(trimmedDeptName===TrimmedDepartmentNames[this.departmentsList[i].DepartmentName]){
        return this.departmentsList[i].DepartmentName;
      }
    }
  }

  onDeptQualityEventCAPAPointClick(event){

    this.setFromAndToDates();
    this.selectedDepartments=[];
    let unTrimmedDeptName = this.getUntrimmedDeptName(event.target.originalArgument);
    // event.target.originalArgument = event.target.originalArgument === 'Executive Management'? 'Executive Managements' : event.target.originalArgument;
    this.selectedDepartments.push({
      Department_ID:this.getDeptIdFromDeptName(unTrimmedDeptName),
      DepartmentName:unTrimmedDeptName
    });

    this.selectedQualityEvents=[];
    this.selectedQualityEvents.push({
      QualityEvent_TypeID:this.getQualityEventIdFromQualityEventName(event.target.series.name),
      QualityEvent_TypeName:event.target.series.name
    });

    this.prevCurrentDrillDownHeaderText = this.currentDrillDownHeaderText=event.target.series.name;
    this.selectedQualityEventName = event.target.series.name;

      this.getQualityEventDataOfOneDept(this.selectedDepartments[0].Department_ID,event.target.series.name);

  }

  onStackedBarChartExporting(event){
    event.fileName="Total CAPA Count of QE of Depts " + this.subtitleForStackedBarChart;
  }

  customizeTooltip(arg: any) {
    return {
        text: arg.seriesName + ' : ' + arg.valueText
    };
}

getQualityEventDataOfOneDept(deptId:number,qualityEventName:string){
  let numToArrConversion=[];
  numToArrConversion.push(deptId);
  this.qualityEventDataOfOneDept={
    DeptID: numToArrConversion,
    QualityEventType: this.getQualityEventIdFromQualityEventName(qualityEventName),
    TypeOfAction:this.selectedTypeOfAction[0].typeOfActionId,
    FromDate:this.selectedFromDate !== undefined ? this.getRequiredDateFormat(this.selectedFromDate):this.initialFromDate,
    ToDate:this.selectedToDate !== undefined ? this.getRequiredDateFormat(this.selectedToDate) : this.initialToDate
  }

  this.selectedQualityEventIdForStatusChart=this.qualityEventDataOfOneDept.QualityEventType;
  this.previousDataFiltersForStatusChart=this.qualityEventDataOfOneDept;

  this.serverService.getQualityEventStatusOfOneDeptForCAPA(this.qualityEventDataOfOneDept).subscribe((res)=>{
    console.log("result of status chart capa",res);
    this.statusListData=res.QeStatus_List;
    this.statusListData=this.excludeEmptyRecordsOfStatusChart(this.statusListData);
    this.overallStatuslevel++;
  },(err)=>{
    console.error(err);
  });
}

excludeEmptyRecordsOfStatusChart(statusListData:StatusChartFilterValuesReturnStructure[]):StatusChartFilterValuesReturnStructure[]{
  let sampleListData:StatusChartFilterValuesReturnStructure[]=[];
  for(let i=0;i<statusListData.length;i++){
    if(statusListData[i].TotalRecords!==0){
      sampleListData.push(statusListData[i]);
    }
  }
  return sampleListData;
}

customizeLabel(arg) {
  return  arg.argumentText + ' : ' + arg.valueText;
}

  onShowOverallCAPAListView(){

  }

  getFilteredStatusChartData(){
    if(this.overallStatuslevel===1){
    this.getSelectedDeptIDs();
    this.filteredQualityEventData={
      DeptID: this.selectedDeptIds,
      QualityEventType: this.selectedQualityEventIdForStatusChart,
      TypeOfAction:this.selectedTypeOfAction[0].typeOfActionId,
      FromDate:this.selectedFromDate !== undefined ? this.getRequiredDateFormat(this.selectedFromDate):this.initialFromDate,
      ToDate:this.selectedToDate !== undefined ? this.getRequiredDateFormat(this.selectedToDate) : this.initialToDate
    }
    this.previousDataFiltersForStatusChart=this.filteredQualityEventData;
    this.getFilteredQualityEventStatusChart(this.filteredQualityEventData);


  }
  else if (this.overallStatuslevel===2){
    // this.getFilteredOverdueData();
  }
  }

  getFilteredQualityEventStatusChart(filteredQualityEventData:CAPAStatusChartFilters){
    this.serverService.getQualityEventStatusOfOneDeptForCAPA(filteredQualityEventData).subscribe((res)=>{
      this.statusListData=res.QeStatus_List;
      this.statusListData=this.excludeEmptyRecordsOfStatusChart(this.statusListData);
    },(err)=>{
      console.error(err);
    });
  }

  onPreviousLevelClick(){

    if(this.overallStatuslevel===1){
      this.overallStatuslevel--;
      this.getDepartmentsQualityEventsDataFromFiltersOfCAPA(this.previousDataFiltersForStackedBarChart);
      this.setSelectedDepartmentsValues(this.previousDataFiltersForStackedBarChart.DeptID);
      this.setSelectedQualityEventValues(this.previousDataFiltersForStackedBarChart.QualityEventType,'qualityEventsChart');
      this.setSelectedDateValues(this.previousDataFiltersForStackedBarChart.FromDate,this.previousDataFiltersForStackedBarChart.ToDate);
    }
    else if(this.overallStatuslevel==2){
      this.overallStatuslevel--;
      this.currentDrillDownHeaderText=this.prevCurrentDrillDownHeaderText;
      this.getFilteredQualityEventStatusChart(this.previousDataFiltersForStatusChart);
      this.setSelectedDepartmentsValues(this.previousDataFiltersForStatusChart.DeptID);
      this.setSelectedQualityEventValues(this.previousDataFiltersForStatusChart.QualityEventType,'statusChart');
      this.setSelectedDateValues(this.previousDataFiltersForStatusChart.FromDate,this.previousDataFiltersForStatusChart.ToDate);
    }
  }

  setSelectedDepartmentsValues(deptIds:number[]){
    this.selectedDepartments=[];
    for(let i=0;i<deptIds.length;i++){
      this.selectedDepartments.push({
        Department_ID:deptIds[i],
        DepartmentName:this.getDeptNameFromDeptID(deptIds[i])
      });
    }
  }

  setSelectedQualityEventValues(qualityEventIds,source:string){
    this.selectedQualityEvents=[];
    if(source==='qualityEventsChart'){
      for(let i=0;i<qualityEventIds.length;i++){
        this.selectedQualityEvents.push({
          QualityEvent_TypeID:qualityEventIds[i],
          QualityEvent_TypeName:this.getQualityEventNameFromQualityEventId(qualityEventIds[i])
        });
      }
    }else if(source==='statusChart'){
      this.selectedQualityEvents.push({
        QualityEvent_TypeID:qualityEventIds,
        QualityEvent_TypeName:this.getQualityEventNameFromQualityEventId(qualityEventIds)
      });
    }

  }

  setSelectedDateValues(fromDate,toDate){
      this.fromDateValue=fromDate;
      this.toDateValue=toDate;
  }

  getChangeClassificationNameFromChangeClassificationID(changeClassificationId):string{
    for(let i=0;i<this.changeClassificationList.length;i++){
      if(this.changeClassificationList[i].changeClassificationID===changeClassificationId){
        return this.changeClassificationList[i].changeClassificationName;
      }
    }
  }

  getIncidentCatNameFromIncidentCatID(incidentCategoryId):string{
    for(let i=0;i<this.incidentCategoriesList.length;i++){
      if(this.incidentCategoriesList[i].TypeofCategoryID===incidentCategoryId){
        return this.incidentCategoriesList[i].TypeofIncident;
      }
    }
  }

  getIncidentClassificationNameFromIncidentClassificationID(incidentclassificationId):string{
    for(let i=0;i<this.incidentClassificationList.length;i++){
      if(this.incidentClassificationList[i].incidentClassificationID===incidentclassificationId){
        return this.incidentClassificationList[i].incidentClassificationName;
      }
    }
  }

  onStatusChartPointClick(event){
    if ((event.target.argument).toLowerCase()===(QualityEventStatus.OverDue).toLowerCase()){
      this.setFromAndToDates();
      this.getSelectedDeptIDs();
      this.currentDrillDownHeaderText=event.target.argument + ' - '+ this.selectedQualityEventName;
      this.overallStatuslevel++;
      this.getFilteredOverdueData();
    }
    else if(this.selectedQualityEventIdForStatusChart===QualityEventIDs.ChangeControlNote){
      // this.getCCNStatusListOfAppliedFilters(event.target.argument);
    }
    else if(this.selectedQualityEventIdForStatusChart===QualityEventIDs.IncidentReport){
      // this.getIncidentStatusListOfAppliedFilters(event.target.argument);
    }
    else if (this.selectedQualityEventIdForStatusChart===QualityEventIDs.Errata){
      // this.getErrataStatusListOfAppliedFilters(event.target.argument);
    }
    else{
      // this.getNTFStatusListOfAppliedFilters(event.target.argument);
    }
  
  }

  getFilteredOverdueData(){
    this.getSelectedDeptIDs();

    this.overdueDataFilters={
      DeptID: this.selectedDeptIds,
      QualityEventType: this.selectedQualityEventIdForStatusChart,
      TypeOfAction:this.selectedTypeOfAction[0].typeOfActionId,
      FromDate:this.selectedFromDate !== undefined ? this.getRequiredDateFormat(this.selectedFromDate):this.initialFromDate,
      ToDate:this.selectedToDate !== undefined ? this.getRequiredDateFormat(this.selectedToDate) : this.initialToDate
    }

    this.serverService.getOverdueData(this.overdueDataFilters).subscribe((res)=>{
      this.overdueDataCAPA = this.dashboardService.getModifiedOverdueData(res.Overdue_List);
    },(err)=>{
      console.error(err);
    });
  }

  onCAPAOverdueStatusChartPointClick(event){
    if(this.selectedQualityEventIdForStatusChart===QualityEventIDs.ChangeControlNote){
      // this.getCCNStatusListOfAppliedFilters(event.target.argument);
    }
    else if(this.selectedQualityEventIdForStatusChart===QualityEventIDs.IncidentReport){
      // this.getIncidentStatusListOfAppliedFilters(event.target.argument);
    }
  }

  customizeTooltipForOverdue = (info: any) => {
    return {
        html: "<div><div class='tooltip-header'>" +
            info.argumentText + "</div>" +
            "<div class='tooltip-body'><div class='series-name'>" +
            "<span class='top-series-name'>" + info.points[0].seriesName + "</span>" +
            ": </div><div class='value-text'>" +
            "<span class='top-series-value'>" + info.points[0].valueText + "</span>" +
            "</div><div class='series-name'>" +
            "<span class='bottom-series-name'>" + info.points[1].seriesName + "</span>" +
            ": </div><div class='value-text'>" +
            "<span class='bottom-series-value'>" + info.points[1].valueText + "</span>" +
            "% </div></div></div>"
    };
  }

  customizeLabelText = (info: any) => {
    return info.valueText + "%";
  }

}
