import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CapaChartComponent } from './capa-chart.component';

describe('CapaChartComponent', () => {
  let component: CapaChartComponent;
  let fixture: ComponentFixture<CapaChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CapaChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CapaChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
