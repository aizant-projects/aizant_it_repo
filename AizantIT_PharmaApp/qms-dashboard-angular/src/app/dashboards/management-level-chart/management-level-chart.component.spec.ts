import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementLevelChartComponent } from './management-level-chart.component';

describe('ManagementLevelChartComponent', () => {
  let component: ManagementLevelChartComponent;
  let fixture: ComponentFixture<ManagementLevelChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementLevelChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementLevelChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
