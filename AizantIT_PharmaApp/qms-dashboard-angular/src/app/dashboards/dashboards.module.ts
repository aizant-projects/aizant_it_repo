import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardsComponent } from './dashboards.component';
import { ManagementLevelChartComponent } from './management-level-chart/management-level-chart.component';
import { DxButtonModule, DxChartModule, DxCheckBoxModule, DxPieChartModule } from 'devextreme-angular';
import { Ng2FlatpickrModule } from 'ng2-flatpickr';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { DepartmentPerformanceChartComponent } from './department-performance-chart/department-performance-chart.component';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { DataTablesModule } from 'angular-datatables';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ListViewComponent } from './list-view/list-view.component';
import { ListViewDeptPerformanceComponent } from './list-view-dept-performance/list-view-dept-performance.component';
import { CapaChartComponent } from './capa-chart/capa-chart.component';


@NgModule({
  declarations: [DashboardsComponent,ManagementLevelChartComponent, DepartmentPerformanceChartComponent,ListViewComponent, ListViewDeptPerformanceComponent, CapaChartComponent],
  imports: [
    CommonModule,
    DxChartModule,
    DxButtonModule,
    Ng2FlatpickrModule,
    AngularMultiSelectModule,
    FormsModule,
    ReactiveFormsModule,
    DxPieChartModule,
    DxCheckBoxModule,
    NgMultiSelectDropDownModule.forRoot(),
    // NgxSmartModalModule.forRoot(),
    DataTablesModule,
    ModalModule.forRoot()
  ],
  providers: [
    // NgxSmartModalService
  ],
  entryComponents: [ListViewComponent]
})
export class DashboardsModule { }
