import { ChangeDetectorRef, Component, OnInit, Renderer2, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { combineLatest, Subscription } from 'rxjs';
import { CCNListStructureForDeptPerformance, ErrataListStructureForDeptPerformance, IncidentListStructureForDeptPerformance, NTFListStructureForDeptPerformance, QualityEvents, rowDetailsForExcelCustomization } from 'src/app/models/dashboards';
import { DashboardsService } from 'src/app/services/dashboards.service';

@Component({
  selector: 'app-list-view-dept-performance',
  template:`
  <div class="modal-header">
       <h4 id="dialog-sizes-name1" class="modal-title pull-left">{{currentDrillDownHeaderText}}</h4>
       <button type="button" class="close pull-right" (click)="onCloseListView()" aria-label="Close">
         <span aria-hidden="true">&times;</span>
       </button>
     </div>
     <div class="modal-body">
   <table datatable [dtOptions]="dtOptions" class="table datatable_cust table-striped table-bordered table-sm row-border hover">
           <thead>
               <tr >
                 <th *ngFor="let eachCol of dtOptions.columns">{{eachCol.title}}</th>
               </tr>
               
             </thead>
             <tbody *ngIf="qualityEvent===qualityEventsNames.ChangeControlNote">
             
               <tr *ngFor="let record of records; let i=index">
                
                 <td>{{ record.CCN_No }}</td>
                 <td>{{ record.DepartmentName }}</td>
                 <td>{{ record.CreatedBy }}</td>
                 <td>{{ record.MonthnameAndYear }}</td>
                 <td>{{ record.CreatedDate }}</td>
                 <td>{{ record.DueDate }}</td>
                 <td>{{ record.CompletedDate }}</td>
                 <td>{{ record.ChangeClassification }}</td>
                 <td>{{ record.TAT }}</td>
                 <td>{{ record.TAT }}</td>
               </tr>
               
             </tbody>
             <tbody *ngIf="qualityEvent===qualityEventsNames.IncidentReport">
               <tr *ngFor="let record of records">
                 <td>{{ record.Incident_No }}</td>
                 <td>{{ record.DepartmentName }}</td>
                 <td>{{ record.TypeofIncident }}</td>
                 <td>{{ record.MonthnameAndYear }}</td>
                 <td>{{ record.Category }}</td>
                 <td>{{ record.CreatedBy }}</td>
                 <td>{{ record.DateOfOccurance }}</td>
                 <td>{{ record.DateOfReport }}</td>
                 <td>{{ record.DueDate }}</td>
                 <td>{{ record.CompletedDate }}</td>
                 <td>{{ record.TAT }}</td>
                 <td>{{ record.TAT }}</td>
               </tr>
             </tbody>
             <tbody *ngIf="qualityEvent===qualityEventsNames.Errata">
               <tr *ngFor="let record of records">
                 <td>{{ record.Errata_No }}</td>
                 <td>{{ record.DepartmentName }}</td>
                 <td>{{ record.DocName }}</td>
                 <td>{{ record.MonthnameAndYear }}</td>
                 <td>{{ record.refdocno }}</td>
                 <td>{{ record.CreatedBy }}</td>
                 <td>{{ record.CreatedDate }}</td>
                 <td>{{ record.CompletedDate }}</td>
                 <td>{{ record.TAT }}</td>
                 <td>{{ record.TAT }}</td>
               </tr>
             </tbody>
             <tbody *ngIf="qualityEvent===qualityEventsNames.NoteToFile">
               <tr *ngFor="let record of records">
                 <td>{{ record.NTF_No }}</td>
                 <td>{{ record.DepartmentName }}</td>
                 <td>{{ record.DocName }}</td>
                 <td>{{ record.MonthnameAndYear }}</td>
                 <td>{{ record.refdocno }}</td>
                 <td>{{ record.CreatedBy }}</td>
                 <td>{{ record.CreatedDate }}</td>
                 <td>{{ record.CompletedDate }}</td>
                 <td>{{ record.TAT }}</td>
                 <td>{{ record.TAT }}</td>
               </tr>
             </tbody>
             <tfoot>
             <tr >
             <td *ngFor="let eachC of dtOptions.columns"><input type="text" placeholder="Search"></td>
             </tr>
             </tfoot>
       </table>
     </div>`  ,
     styleUrls: ["../management-level-chart/management-level-chart.component.scss"
  ]
})
export class ListViewDeptPerformanceComponent implements OnInit {

  dtOptions: any = {};
  CCNdtOptions:any={};
  incidentDtOptions:any={};
  errataDtOptions:any={};
  noteToFileDtOptions:any={};
  currentDrillDownHeaderText: string;
  records:CCNListStructureForDeptPerformance[] | IncidentListStructureForDeptPerformance[] | ErrataListStructureForDeptPerformance[] | NTFListStructureForDeptPerformance[];
  ccnRecords:CCNListStructureForDeptPerformance[];
  incidentRecords:IncidentListStructureForDeptPerformance[];
  errataRecords:ErrataListStructureForDeptPerformance[];
  noteToFileRecords:NTFListStructureForDeptPerformance[];
  qualityEvent:string;
  fileName:string;
  qualityEventsNames=QualityEvents;
  showType:number;
  limits:{upperLimitForListView:number,lowerLimitForListView:number};
  dates:{fromDate:string,toDate:string}

  
  events: Subscription[] = [];
  ccnRecordsPerformance={};
  
  @ViewChild(DataTableDirective)
  datatableElement: DataTableDirective;

  constructor(private dashboardService:DashboardsService,public bsModalRef: BsModalRef,public modalService:BsModalService,private changeDetection: ChangeDetectorRef,
    private renderer:Renderer2) { }


  ngOnInit(): void {
    let limitsRecvd=this.limits;
    this.setCCNDtOptions(limitsRecvd);

    this.setIncidentDtOptions(limitsRecvd);
    this.setErrataDtOptions(limitsRecvd);
    this.setNTFDtOptions(limitsRecvd);

    var trimmedQualityEventName='';
    if(this.qualityEvent===QualityEvents.ChangeControlNote){
      trimmedQualityEventName='CCN';
      this.dtOptions=this.CCNdtOptions;
      this.records=this.ccnRecords;
    }
    else if(this.qualityEvent===QualityEvents.IncidentReport){
      trimmedQualityEventName='IR';
      this.dtOptions=this.incidentDtOptions;
      this.records=this.incidentRecords;
    }
    else if(this.qualityEvent===QualityEvents.NoteToFile){
      trimmedQualityEventName='NTF';
      this.dtOptions=this.noteToFileDtOptions;
      this.records=this.noteToFileRecords;
    }
    else{
      trimmedQualityEventName='Errata';
      this.dtOptions=this.errataDtOptions;
      this.records=this.errataRecords;
    }

    this.getMonthsInASequenceForListView(this.records);
    
      this.dtOptions.buttons[0].filename="Dept. Performance - "+trimmedQualityEventName + this.fileName;
    // let excelButton = document.getElementsByClassName('buttons-html5');
    // console.log(excelButton);
    //     this.renderer.addClass(excelButton[0],'btn btn-outline');
  }

  ngAfterViewInit():void{

    
    const _combine = combineLatest(
      [this.modalService.onShow,
      this.modalService.onShown,
      this.modalService.onHide,
      this.modalService.onHidden]
    ).subscribe(() => this.changeDetection.markForCheck());
    
    this.events.push(
      this.modalService.onShown.subscribe((reason: string) => {
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.columns().every(function () {
            const that = this;
            $('input', this.footer()).on('keyup change', function () {
              if (that.search() !== this['value']) {
                that
                  .search(this['value'])
                  .draw();
              }
            });
          });
        });
      })
    );
    this.events.push(_combine);

  }

  setCCNDtOptions(limitsRecvd){
    this.CCNdtOptions = {
      
      columns: [
        {
        title: 'CCN Number',
        data: 'ccnNumber'
      }, {
        title: 'Department',
        data: 'department'
      }, {
        title: 'Created By',
        data: 'createdBy'
      },
      {
        title: 'Month',
        data: 'month'
      },
      {
        title: 'Created Date',
        data: 'createdDate'
      },
      {
        title: 'Due Date',
        data: 'dueDate'
      },
      {
        title: 'Completed Date',
        data: 'completedDate'
      },
      {
        title: 'Classification',
        data: 'classification'
      },
      {
        title: 'TAT',
        data: 'turnAroundTime'
      },
      {
        title: 'Performance',
        data:'performance'
      }
    ],
    order: [],
    rowGroup: {
      dataSrc:"month"
    },
    responsive:true,
    // pagination:true,
    scrollX:true,
      // Declare the use of the extension in the dom parameter
      dom: 'lBfrtip',
      
      columnDefs: [
        { "visible": false, "targets": 3 },
        {
          "targets": 9,
          "data": null,
          "render": function ( data, type, row, meta ) {
            if(data>=limitsRecvd.upperLimitForListView){
              if (type==='display'){
              return '<img src="/Images/QMS_Dashboard_Angular/Poor_45x45.png">'
              }
              else{
                return 'Poor'
              }
            }
            else if(data<limitsRecvd.upperLimitForListView){
              if(data<limitsRecvd.lowerLimitForListView){
                if (type==='display'){
                  return '<img src="/Images/QMS_Dashboard_Angular/Good_45x45.png">'
                }
                else{
                  return 'Good'
                } 
              }
              else{
                if (type==='display'){
                  return '<img src="/Images/QMS_Dashboard_Angular/Moderate_45x45.png">'
                }
                else{
                  return 'Moderate'
                }

              }
            }
          }
        } 
      ],
      // Configure the buttons
      buttons: [
        {
          extend: 'excelHtml5',
          
          text:'<img src="/Images/QMS_Dashboard_Angular/excel.png">',
          title:null,
          exportOptions: {
            // Any other settings used
            grouped_array_index: 'month',
            columns: ':visible',
            format:{
              body:function(data, row, column, node){
              if(column===8){
                if(data==='<img src="/Images/QMS_Dashboard_Angular/Poor_45x45.png">'){
                  return 'Poor';
                }
                else if(data==='<img src="/Images/QMS_Dashboard_Angular/Moderate_45x45.png">'){
                  return 'Moderate';
                }else if(data==='<img src="/Images/QMS_Dashboard_Angular/Good_45x45.png">'){
                  return 'Good';
                }
                else return data;
                }
                else return data;
                
                
              }
            }
        },
      customize:function(xlsx){
        var sheet = xlsx.xl.worksheets['sheet1.xml'];
      console.log(sheet);

      // this will be used to determine addresses of cells being merged later (on export) in excel
      let lettersAndIndexes = { 'A': 1, 'B': 2, 'C': 3, 'D': 4, 'E': 5, 'F': 6, 'G': 7, 'H': 8, 'I': 9, 'J': 10, 'K': 11, 'L': 12, 'M': 13, 'N': 14, 'O': 15, 'P': 16, 'Q': 17, 'R': 18, 'S': 19, 'T': 20, 'U': 21, 'V': 22, 'W': 23, 'X': 24, 'Y': 25, 'Z': 26 }
      let indexesAndLetters = { 1: 'A', 2: 'B', 3: 'C', 4: 'D', 5: 'E', 6: 'F', 7: 'G', 8: 'H', 9: 'I', 10: 'J', 11: 'K', 12: 'L', 13: 'M', 14: 'N', 15: 'O', 16: 'P', 17: 'Q', 18: 'R', 19: 'S', 20: 'T', 21: 'U', 22: 'V', 23: 'W', 24: 'X', 25: 'Y', 26: 'Z' }
      
      

    var cols = sheet.getElementsByTagName('col');
    let numOfCols=cols.length;
    let lastColName= getLetterCodeByNumIndex(numOfCols);
    // let perfColName=getLetterCodeByNumIndex(numOfCols-1);
    var rows = $('row', sheet);
    console.log("rows",rows);
    let totalRows = rows.length;
    for(let i=0;i<totalRows;i++){
      if(rows[i].childNodes.length===1){

        let rowDet = getRowNodeHavingRowGroupHeader(rows[i]);
        let refString  = rowDet.startingCellNum + ':'+lastColName+rowDet.rowNum;
        $(`c[r=${rowDet.startingCellNum}]`,sheet).attr('s','7');
        // $(`c[r=${rowDet.startingCellNum}]`,sheet).attr('s','51');
        mergeFunction(refString);
      }
    }

    // for updating performance column data
    $(`row c[r^=${lastColName}]`,sheet).each(function(){
      if($('is t',this).text()==='Poor'){
        $(this).attr('s','11');
      }
      else if($('is t',this).text()==='Good'){
        $(this).attr('s','16');
      }
      else if($('is t',this).text()==='Moderate'){
        $(this).attr('s','20');
      }
    });
    // for(let i=0;i<totalRows;i++){
    //   if(i!==0){
    //     if(rows[i].childNodes.length>1){
    //       let cellName=lastColName+i;
    //       if((rows[i].lastChild.textContent)==='Poor'){
            
    //         $(`row c[r=${cellName}] is t`,sheet).css('color','red');
    //       }
    //       else if((rows[i].lastChild.textContent)==='Good'){
    //         $(`row c[r=${cellName}]`,sheet).css('color','green');
    //       }
    //       else if((rows[i].lastChild.textContent)==='Moderate'){
    //         $(`row c[r=${cellName}]`,sheet).css('color','Yellow');
    //       }
    //     }
    //   }
    // }

    console.log("after updating sheet",sheet);

      function getLetterCodeByNumIndex(numIndex) {
        let letterCode;
        if (numIndex > 702) { // 3 symbols
            let lastLetter, left, firstLetterNumber;
            if (numIndex % 26 === 0) {
                lastLetter = indexesAndLetters[26];
                left = numIndex - 26;
            } else {
                lastLetter = indexesAndLetters[numIndex % 26];
                left = numIndex - (numIndex % 26);
            }
    
            if ((left / 676) % 1 === 0) {
                firstLetterNumber = Math.floor(left / 676) - 1;
            } else {
                firstLetterNumber = Math.floor(left / 676);
            }
    
            let firstLetter = indexesAndLetters[firstLetterNumber];
            left = left - firstLetterNumber * 676;
            let middleLetter = indexesAndLetters[(left / 26).toFixed(0)];
    
            letterCode = firstLetter + middleLetter + lastLetter;
        } else if (numIndex > 26) { // 2 symbols
            let lastLetter, left;
            if (numIndex % 26 === 0) {
                lastLetter = indexesAndLetters[26];
                left = numIndex - 26;
            } else {
                lastLetter = indexesAndLetters[numIndex % 26];
                left = numIndex - (numIndex % 26);
            }
            let firstLetter = indexesAndLetters[(left / 26).toFixed(0)];
            letterCode = firstLetter + lastLetter;
        } else { // 1 symbol
            letterCode = indexesAndLetters[numIndex];
        }
        return letterCode;
    }
    function getNumIndexByLetterCode(letterCode) {
        letterCode = letterCode.toUpperCase();
        let numIndex;
        switch (letterCode.length) {
            case 1: {
                numIndex = lettersAndIndexes[letterCode]
                break;
            }
            case 2: {
                let [firstLetter, secondLetter] = letterCode.split('');
                numIndex = lettersAndIndexes[firstLetter] * 26 + lettersAndIndexes[secondLetter];
                break;
            }
            case 3: {
                let [firstLetter, secondLetter, thirdLetter] = letterCode.split('');
                numIndex = lettersAndIndexes[firstLetter] * 676 + lettersAndIndexes[secondLetter] * 26 + lettersAndIndexes[thirdLetter];
                break;
            }
            default:
                console.log(`unhandled letterCode.length: ${letterCode.length} ${letterCode}`);
                break;
        }
        return numIndex;
    }
        function _createNode(doc, nodeName, opts) {
          var tempNode = doc.createElement(nodeName);
          if (opts) {
              if (opts.attr) {
                  $(tempNode).attr(opts.attr);
              }
              if (opts.children) {
                  $.each(opts.children, function (key, value) {
                      tempNode.appendChild(value);
                  });
              }
              if (opts.text !== null && opts.text !== undefined) {
                  tempNode.appendChild(doc.createTextNode(opts.text));
              }
          }
          return tempNode;
      }
      
      function mergeFunction(referenceString) {
        // reference string format: 'B1:E1'
        var mergeCells = $('mergeCells', sheet);
        console.log("mergeCells",mergeCells);
        if (!referenceString) {
            alert('no reference string on merge function call');
            return
        }
        mergeCells[0].appendChild(_createNode(sheet, 'mergeCell', {
            attr: {
                ref: referenceString
            }
        }));
        mergeCells.attr('count', mergeCells.attr('count') + 1);
    }

    function getRowNodeHavingRowGroupHeader(currentRow):rowDetailsForExcelCustomization{
      let rowDetails={
        startingCellNum:currentRow.lastChild.attributes[1].value,
        rowNum:currentRow.attributes[0].value
      }
      return rowDetails;
    }
      },
    }
      ],
      
    };
  }

  setIncidentDtOptions(limitsRecvd){
    this.incidentDtOptions = {
      columns: [{
        title: 'Incident_No',
        data: 'incidentNumber'
      }, {
        title: 'Department',
        data: 'department'
      }, 
      {
        title: 'Type of Incident',
        data: 'typeOfIncident'
      },
      {
        title:'Month',
        data:'month'
      },
      {
        title:'Category',
        data:'category'
      },
      {
        title: 'Created By',
        data: 'createdBy'
      },
      {
        title: 'Date of Occurence',
        data: 'dateOfOccurance'
      },
      {
        title: 'Date of Report',
        data: 'dateOfReport'
      },
      {
        title: 'Due Date',
        data: 'dueDate'
      },
      {
        title: 'Completed Date',
        data: 'completedDate',
      },
      {
        title: 'TAT',
        data: 'turnAroundTime'
      },
      {
        title: 'Performance',
        data: 'performance'
      }
    ],
    columnDefs: [
      { "visible": false, "targets": 3 },
      {
        "targets": 11,
        "data": null,
        "render": function ( data, type, row, meta ) {
          if(data>=limitsRecvd.upperLimitForListView){
            if (type==='display'){
            return '<img src="/Images/QMS_Dashboard_Angular/Poor_45x45.png">'
            }
            else{
              return 'Poor'
            }
          }
          else if(data<limitsRecvd.upperLimitForListView){
            if(data<limitsRecvd.lowerLimitForListView){
              if (type==='display'){
                return '<img src="/Images/QMS_Dashboard_Angular/Good_45x45.png">'
              }
              else{
                return 'Good'
              } 
            }
            else{
              if (type==='display'){
                return '<img src="/Images/QMS_Dashboard_Angular/Moderate_45x45.png">'
              }
              else{
                return 'Moderate'
              }

            }
          }
        }
      } 
    ],
    order: [],
    rowGroup: {
      dataSrc:"month"
    },
    responsive:true,
    scrollX:true,
      // Declare the use of the extension in the dom parameter
      dom: 'lBfrtip',
      // Configure the buttons
      buttons: [
        {
          extend: 'excel',
          text:'<img src="/Images/QMS_Dashboard_Angular/excel.png">',
          title:null,
          exportOptions: {
            // Any other settings used
            grouped_array_index: 'month',
            columns: ':visible',
            format:{
              body:function(data, row, column, node){
              if(column===10){
                if(data==='<img src="/Images/QMS_Dashboard_Angular/Poor_45x45.png">'){
                  return 'Poor';
                }
                else if(data==='<img src="/Images/QMS_Dashboard_Angular/Moderate_45x45.png">'){
                  return 'Moderate';
                }else if(data==='<img src="/Images/QMS_Dashboard_Angular/Good_45x45.png">'){
                  return 'Good';
                }
                else return data;
                }
                else return data;
                
                
              }
            }
        },
      customize:function(xlsx){
        var sheet = xlsx.xl.worksheets['sheet1.xml'];
      console.log(sheet);

      // this will be used to determine addresses of cells being merged later (on export) in excel
      let lettersAndIndexes = { 'A': 1, 'B': 2, 'C': 3, 'D': 4, 'E': 5, 'F': 6, 'G': 7, 'H': 8, 'I': 9, 'J': 10, 'K': 11, 'L': 12, 'M': 13, 'N': 14, 'O': 15, 'P': 16, 'Q': 17, 'R': 18, 'S': 19, 'T': 20, 'U': 21, 'V': 22, 'W': 23, 'X': 24, 'Y': 25, 'Z': 26 }
      let indexesAndLetters = { 1: 'A', 2: 'B', 3: 'C', 4: 'D', 5: 'E', 6: 'F', 7: 'G', 8: 'H', 9: 'I', 10: 'J', 11: 'K', 12: 'L', 13: 'M', 14: 'N', 15: 'O', 16: 'P', 17: 'Q', 18: 'R', 19: 'S', 20: 'T', 21: 'U', 22: 'V', 23: 'W', 24: 'X', 25: 'Y', 26: 'Z' }

    var cols = sheet.getElementsByTagName('col');
    let numOfCols=cols.length;
    let lastColName= getLetterCodeByNumIndex(numOfCols);
    var rows = $('row', sheet);
    console.log("rows",rows);
    let totalRows = rows.length;
    for(let i=0;i<totalRows;i++){
      if(rows[i].childNodes.length===1){

        let rowDet = getRowNodeHavingRowGroupHeader(rows[i]);
        let refString  = rowDet.startingCellNum + ':'+lastColName+rowDet.rowNum;
        $(`c[r=${rowDet.startingCellNum}]`,sheet).attr('s','7');
        // $(`c[r=${rowDet.startingCellNum}]`,sheet).attr('s','51');
        mergeFunction(refString);
      }
    }

    // for updating performance column data styles
    $(`row c[r^=${lastColName}]`,sheet).each(function(){
      if($('is t',this).text()==='Poor'){
        $(this).attr('s','11');
      }
      else if($('is t',this).text()==='Good'){
        $(this).attr('s','16');
      }
      else if($('is t',this).text()==='Moderate'){
        $(this).attr('s','20');
      }
    });

    console.log("after updating sheet",sheet);

      function getLetterCodeByNumIndex(numIndex) {
        let letterCode;
        if (numIndex > 702) { // 3 symbols
            let lastLetter, left, firstLetterNumber;
            if (numIndex % 26 === 0) {
                lastLetter = indexesAndLetters[26];
                left = numIndex - 26;
            } else {
                lastLetter = indexesAndLetters[numIndex % 26];
                left = numIndex - (numIndex % 26);
            }
    
            if ((left / 676) % 1 === 0) {
                firstLetterNumber = Math.floor(left / 676) - 1;
            } else {
                firstLetterNumber = Math.floor(left / 676);
            }
    
            let firstLetter = indexesAndLetters[firstLetterNumber];
            left = left - firstLetterNumber * 676;
            let middleLetter = indexesAndLetters[(left / 26).toFixed(0)];
    
            letterCode = firstLetter + middleLetter + lastLetter;
        } else if (numIndex > 26) { // 2 symbols
            let lastLetter, left;
            if (numIndex % 26 === 0) {
                lastLetter = indexesAndLetters[26];
                left = numIndex - 26;
            } else {
                lastLetter = indexesAndLetters[numIndex % 26];
                left = numIndex - (numIndex % 26);
            }
            let firstLetter = indexesAndLetters[(left / 26).toFixed(0)];
            letterCode = firstLetter + lastLetter;
        } else { // 1 symbol
            letterCode = indexesAndLetters[numIndex];
        }
        return letterCode;
    }
    function getNumIndexByLetterCode(letterCode) {
        letterCode = letterCode.toUpperCase();
        let numIndex;
        switch (letterCode.length) {
            case 1: {
                numIndex = lettersAndIndexes[letterCode]
                break;
            }
            case 2: {
                let [firstLetter, secondLetter] = letterCode.split('');
                numIndex = lettersAndIndexes[firstLetter] * 26 + lettersAndIndexes[secondLetter];
                break;
            }
            case 3: {
                let [firstLetter, secondLetter, thirdLetter] = letterCode.split('');
                numIndex = lettersAndIndexes[firstLetter] * 676 + lettersAndIndexes[secondLetter] * 26 + lettersAndIndexes[thirdLetter];
                break;
            }
            default:
                console.log(`unhandled letterCode.length: ${letterCode.length} ${letterCode}`);
                break;
        }
        return numIndex;
    }
        function _createNode(doc, nodeName, opts) {
          var tempNode = doc.createElement(nodeName);
          if (opts) {
              if (opts.attr) {
                  $(tempNode).attr(opts.attr);
              }
              if (opts.children) {
                  $.each(opts.children, function (key, value) {
                      tempNode.appendChild(value);
                  });
              }
              if (opts.text !== null && opts.text !== undefined) {
                  tempNode.appendChild(doc.createTextNode(opts.text));
              }
          }
          return tempNode;
      }
      
      function mergeFunction(referenceString) {
        // reference string format: 'B1:E1'
        var mergeCells = $('mergeCells', sheet);
        console.log("mergeCells",mergeCells);
        if (!referenceString) {
            alert('no reference string on merge function call');
            return
        }
        mergeCells[0].appendChild(_createNode(sheet, 'mergeCell', {
            attr: {
                ref: referenceString
            }
        }));
        mergeCells.attr('count', mergeCells.attr('count') + 1);
    }

    function getRowNodeHavingRowGroupHeader(currentRow):rowDetailsForExcelCustomization{
      let rowDetails={
        startingCellNum:currentRow.lastChild.attributes[1].value,
        rowNum:currentRow.attributes[0].value
      }
      return rowDetails;
    }
      },
      }
      ],
    };
  }

  setErrataDtOptions(limitsRecvd){
    this.errataDtOptions = {
      
      columns: [{
        title: 'Errata Number',
        data: 'errataNumber'
      }, {
        title: 'Department',
        data: 'department'
      },
      {
        title: 'Document',
        data: 'document'
      },
      {
        title:'Month',
        data:'month'
      },
      {
        title: 'Ref. Doc No.',
        data: 'refDocNum'
      }, {
        title: 'Created By',
        data: 'createdBy'
      },
      {
        title: 'Created Date',
        data: 'createdDate'
      },
      {
        title: 'Completed Date',
        data: 'completedDate',
      },
      {
        title: 'TAT',
        data: 'turnAroundTime'
      },
      {
        title: 'Performance',
        data: 'performance'
      }
    ],
    columnDefs: [
      { "visible": false, "targets": 3 },
      {
        "targets": 9,
        "data": null,
        "render": function ( data, type, row, meta ) {
          if(data>=limitsRecvd.upperLimitForListView){
            if (type==='display'){
              return '<img src="/Images/QMS_Dashboard_Angular/Poor_45x45.png">'
            }
            else{
              return 'Poor'
            }
          }
          else if(data<limitsRecvd.upperLimitForListView){
            if(data<limitsRecvd.lowerLimitForListView){
              if (type==='display'){
                return '<img src="/Images/QMS_Dashboard_Angular/Good_45x45.png">'
              }
              else{
                return 'Good'
              } 
            }
            else{
              if (type==='display'){
                return '<img src="/Images/QMS_Dashboard_Angular/Moderate_45x45.png">'
              }
              else{
                return 'Moderate'
              }

            }
          }
        }
      } 
    ],
    // order: [[3, 'asc']],
    order: [],
    rowGroup: {
      dataSrc:"month"
    },
    responsive:true,
    scrollX:true,
      // Declare the use of the extension in the dom parameter
      dom: 'lBfrtip',
      // Configure the buttons
      buttons: [
        {
          extend: 'excelHtml5',
          
          text:'<img src="/Images/QMS_Dashboard_Angular/excel.png">',
          title:null,
          exportOptions: {
            // Any other settings used
            grouped_array_index: 'month',
            columns: ':visible',
            format:{
              body:function(data, row, column, node){
              if(column===8){
                if(data==='<img src="/Images/QMS_Dashboard_Angular/Poor_45x45.png">'){
                  return 'Poor';
                }
                else if(data==='<img src="/Images/QMS_Dashboard_Angular/Moderate_45x45.png">'){
                  return 'Moderate';
                }else if(data==='<img src="/Images/QMS_Dashboard_Angular/Good_45x45.png">'){
                  return 'Good';
                }
                else return data;
                }
                else return data;
                
                
              }
            }
        },
      customize:function(xlsx){
        var sheet = xlsx.xl.worksheets['sheet1.xml'];
      console.log(sheet);

      // this will be used to determine addresses of cells being merged later (on export) in excel
      let lettersAndIndexes = { 'A': 1, 'B': 2, 'C': 3, 'D': 4, 'E': 5, 'F': 6, 'G': 7, 'H': 8, 'I': 9, 'J': 10, 'K': 11, 'L': 12, 'M': 13, 'N': 14, 'O': 15, 'P': 16, 'Q': 17, 'R': 18, 'S': 19, 'T': 20, 'U': 21, 'V': 22, 'W': 23, 'X': 24, 'Y': 25, 'Z': 26 }
      let indexesAndLetters = { 1: 'A', 2: 'B', 3: 'C', 4: 'D', 5: 'E', 6: 'F', 7: 'G', 8: 'H', 9: 'I', 10: 'J', 11: 'K', 12: 'L', 13: 'M', 14: 'N', 15: 'O', 16: 'P', 17: 'Q', 18: 'R', 19: 'S', 20: 'T', 21: 'U', 22: 'V', 23: 'W', 24: 'X', 25: 'Y', 26: 'Z' }
      
      

    var cols = sheet.getElementsByTagName('col');
    let numOfCols=cols.length;
    let lastColName= getLetterCodeByNumIndex(numOfCols);
    // let perfColName=getLetterCodeByNumIndex(numOfCols-1);
    var rows = $('row', sheet);
    console.log("rows",rows);
    let totalRows = rows.length;
    for(let i=0;i<totalRows;i++){
      if(rows[i].childNodes.length===1){

        let rowDet = getRowNodeHavingRowGroupHeader(rows[i]);
        let refString  = rowDet.startingCellNum + ':'+lastColName+rowDet.rowNum;
        $(`c[r=${rowDet.startingCellNum}]`,sheet).attr('s','7');
        // $(`c[r=${rowDet.startingCellNum}]`,sheet).attr('s','51');
        mergeFunction(refString);
      }
    }

    // for updating performance column data
    $(`row c[r^=${lastColName}]`,sheet).each(function(){
      if($('is t',this).text()==='Poor'){
        $(this).attr('s','11');
      }
      else if($('is t',this).text()==='Good'){
        $(this).attr('s','16');
      }
      else if($('is t',this).text()==='Moderate'){
        $(this).attr('s','20');
      }
    });

    console.log("after updating sheet",sheet);

      function getLetterCodeByNumIndex(numIndex) {
        let letterCode;
        if (numIndex > 702) { // 3 symbols
            let lastLetter, left, firstLetterNumber;
            if (numIndex % 26 === 0) {
                lastLetter = indexesAndLetters[26];
                left = numIndex - 26;
            } else {
                lastLetter = indexesAndLetters[numIndex % 26];
                left = numIndex - (numIndex % 26);
            }
    
            if ((left / 676) % 1 === 0) {
                firstLetterNumber = Math.floor(left / 676) - 1;
            } else {
                firstLetterNumber = Math.floor(left / 676);
            }
    
            let firstLetter = indexesAndLetters[firstLetterNumber];
            left = left - firstLetterNumber * 676;
            let middleLetter = indexesAndLetters[(left / 26).toFixed(0)];
    
            letterCode = firstLetter + middleLetter + lastLetter;
        } else if (numIndex > 26) { // 2 symbols
            let lastLetter, left;
            if (numIndex % 26 === 0) {
                lastLetter = indexesAndLetters[26];
                left = numIndex - 26;
            } else {
                lastLetter = indexesAndLetters[numIndex % 26];
                left = numIndex - (numIndex % 26);
            }
            let firstLetter = indexesAndLetters[(left / 26).toFixed(0)];
            letterCode = firstLetter + lastLetter;
        } else { // 1 symbol
            letterCode = indexesAndLetters[numIndex];
        }
        return letterCode;
    }
    function getNumIndexByLetterCode(letterCode) {
        letterCode = letterCode.toUpperCase();
        let numIndex;
        switch (letterCode.length) {
            case 1: {
                numIndex = lettersAndIndexes[letterCode]
                break;
            }
            case 2: {
                let [firstLetter, secondLetter] = letterCode.split('');
                numIndex = lettersAndIndexes[firstLetter] * 26 + lettersAndIndexes[secondLetter];
                break;
            }
            case 3: {
                let [firstLetter, secondLetter, thirdLetter] = letterCode.split('');
                numIndex = lettersAndIndexes[firstLetter] * 676 + lettersAndIndexes[secondLetter] * 26 + lettersAndIndexes[thirdLetter];
                break;
            }
            default:
                console.log(`unhandled letterCode.length: ${letterCode.length} ${letterCode}`);
                break;
        }
        return numIndex;
    }
        function _createNode(doc, nodeName, opts) {
          var tempNode = doc.createElement(nodeName);
          if (opts) {
              if (opts.attr) {
                  $(tempNode).attr(opts.attr);
              }
              if (opts.children) {
                  $.each(opts.children, function (key, value) {
                      tempNode.appendChild(value);
                  });
              }
              if (opts.text !== null && opts.text !== undefined) {
                  tempNode.appendChild(doc.createTextNode(opts.text));
              }
          }
          return tempNode;
      }
      
      function mergeFunction(referenceString) {
        // reference string format: 'B1:E1'
        var mergeCells = $('mergeCells', sheet);
        console.log("mergeCells",mergeCells);
        if (!referenceString) {
            alert('no reference string on merge function call');
            return
        }
        mergeCells[0].appendChild(_createNode(sheet, 'mergeCell', {
            attr: {
                ref: referenceString
            }
        }));
        mergeCells.attr('count', mergeCells.attr('count') + 1);
    }

    function getRowNodeHavingRowGroupHeader(currentRow):rowDetailsForExcelCustomization{
      let rowDetails={
        startingCellNum:currentRow.lastChild.attributes[1].value,
        rowNum:currentRow.attributes[0].value
      }
      return rowDetails;
    }
      },
      }
      ],
    };
  }

  setNTFDtOptions(limitsRecvd){
    this.noteToFileDtOptions = {
      columns: [{
        title: 'NTF Number',
        data: 'noteToFileNumber'
      }, {
        title: 'Department',
        data: 'department'
      },
      {
        title: 'Document',
        data: 'document'
      },
      {
        title:'Month',
        data:'month'
      },
      {
        title: 'Ref. Doc No.',
        data: 'refDocNum'
      }, {
        title: 'Created By',
        data: 'createdBy'
      },
      {
        title: 'Created Date',
        data: 'createdDate'
      },
      {
        title: 'Completed Date',
        data: 'completedDate',
      },
      {
        title: 'TAT',
        data: 'turnAroundTime'
      },
      {
        title: 'Performance',
        data: 'performance'
      }
    ],
    columnDefs: [
      { "visible": false, "targets": 3 },
      {
        "targets": 9,
        "data": null,
        "render": function ( data, type, row, meta ) {
          if(data>=limitsRecvd.upperLimitForListView){
            if (type==='display'){
              return '<img src="/Images/QMS_Dashboard_Angular/Poor_45x45.png">'
            }
            else{
              return 'Poor'
            }
          }
          else if(data<limitsRecvd.upperLimitForListView){
            if(data<limitsRecvd.lowerLimitForListView){
              if (type==='display'){
                return '<img src="/Images/QMS_Dashboard_Angular/Good_45x45.png">'
              }
              else{
                return 'Good'
              } 
            }
            else{
              if (type==='display'){
                return '<img src="/Images/QMS_Dashboard_Angular/Moderate_45x45.png">'
              }
              else{
                return 'Moderate'
              }
  
            }
          }
        }
      } 
    ],
    // order: [[3, 'asc']],
    order: [],
    rowGroup: {
      dataSrc:"month"
    },
      responsive:true,
      scrollX:true,
        // Declare the use of the extension in the dom parameter
        dom: 'lBfrtip',
        // Configure the buttons
        buttons: [
          {
            extend: 'excelHtml5',
            
            text:'<img src="/Images/QMS_Dashboard_Angular/excel.png">',
            title:null,
            exportOptions: {
              // Any other settings used
              grouped_array_index: 'month',
              columns: ':visible',
              format:{
                body:function(data, row, column, node){
                if(column===8){
                  if(data==='<img src="/Images/QMS_Dashboard_Angular/Poor_45x45.png">'){
                    return 'Poor';
                  }
                  else if(data==='<img src="/Images/QMS_Dashboard_Angular/Moderate_45x45.png">'){
                    return 'Moderate';
                  }else if(data==='<img src="/Images/QMS_Dashboard_Angular/Good_45x45.png">'){
                    return 'Good';
                  }
                  else return data;
                  }
                  else return data;
                  
                  
                }
              }
          },
        customize:function(xlsx){
          var sheet = xlsx.xl.worksheets['sheet1.xml'];
        console.log(sheet);
  
        // this will be used to determine addresses of cells being merged later (on export) in excel
        let lettersAndIndexes = { 'A': 1, 'B': 2, 'C': 3, 'D': 4, 'E': 5, 'F': 6, 'G': 7, 'H': 8, 'I': 9, 'J': 10, 'K': 11, 'L': 12, 'M': 13, 'N': 14, 'O': 15, 'P': 16, 'Q': 17, 'R': 18, 'S': 19, 'T': 20, 'U': 21, 'V': 22, 'W': 23, 'X': 24, 'Y': 25, 'Z': 26 }
        let indexesAndLetters = { 1: 'A', 2: 'B', 3: 'C', 4: 'D', 5: 'E', 6: 'F', 7: 'G', 8: 'H', 9: 'I', 10: 'J', 11: 'K', 12: 'L', 13: 'M', 14: 'N', 15: 'O', 16: 'P', 17: 'Q', 18: 'R', 19: 'S', 20: 'T', 21: 'U', 22: 'V', 23: 'W', 24: 'X', 25: 'Y', 26: 'Z' }
        
        
  
      var cols = sheet.getElementsByTagName('col');
      let numOfCols=cols.length;
      let lastColName= getLetterCodeByNumIndex(numOfCols);
      var rows = $('row', sheet);
      console.log("rows",rows);
      let totalRows = rows.length;
      for(let i=0;i<totalRows;i++){
        if(rows[i].childNodes.length===1){
  
          let rowDet = getRowNodeHavingRowGroupHeader(rows[i]);
          let refString  = rowDet.startingCellNum + ':'+lastColName+rowDet.rowNum;
          $(`c[r=${rowDet.startingCellNum}]`,sheet).attr('s','7');
          // $(`c[r=${rowDet.startingCellNum}]`,sheet).attr('s','51');
          mergeFunction(refString);
        }
      }
  
      // for updating performance column data styles
      $(`row c[r^=${lastColName}]`,sheet).each(function(){
        if($('is t',this).text()==='Poor'){
          $(this).attr('s','11');
        }
        else if($('is t',this).text()==='Good'){
          $(this).attr('s','16');
        }
        else if($('is t',this).text()==='Moderate'){
          $(this).attr('s','20');
        }
      });
  
      console.log("after updating sheet",sheet);
  
        function getLetterCodeByNumIndex(numIndex) {
          let letterCode;
          if (numIndex > 702) { // 3 symbols
              let lastLetter, left, firstLetterNumber;
              if (numIndex % 26 === 0) {
                  lastLetter = indexesAndLetters[26];
                  left = numIndex - 26;
              } else {
                  lastLetter = indexesAndLetters[numIndex % 26];
                  left = numIndex - (numIndex % 26);
              }
      
              if ((left / 676) % 1 === 0) {
                  firstLetterNumber = Math.floor(left / 676) - 1;
              } else {
                  firstLetterNumber = Math.floor(left / 676);
              }
      
              let firstLetter = indexesAndLetters[firstLetterNumber];
              left = left - firstLetterNumber * 676;
              let middleLetter = indexesAndLetters[(left / 26).toFixed(0)];
      
              letterCode = firstLetter + middleLetter + lastLetter;
          } else if (numIndex > 26) { // 2 symbols
              let lastLetter, left;
              if (numIndex % 26 === 0) {
                  lastLetter = indexesAndLetters[26];
                  left = numIndex - 26;
              } else {
                  lastLetter = indexesAndLetters[numIndex % 26];
                  left = numIndex - (numIndex % 26);
              }
              let firstLetter = indexesAndLetters[(left / 26).toFixed(0)];
              letterCode = firstLetter + lastLetter;
          } else { // 1 symbol
              letterCode = indexesAndLetters[numIndex];
          }
          return letterCode;
      }
      function getNumIndexByLetterCode(letterCode) {
          letterCode = letterCode.toUpperCase();
          let numIndex;
          switch (letterCode.length) {
              case 1: {
                  numIndex = lettersAndIndexes[letterCode]
                  break;
              }
              case 2: {
                  let [firstLetter, secondLetter] = letterCode.split('');
                  numIndex = lettersAndIndexes[firstLetter] * 26 + lettersAndIndexes[secondLetter];
                  break;
              }
              case 3: {
                  let [firstLetter, secondLetter, thirdLetter] = letterCode.split('');
                  numIndex = lettersAndIndexes[firstLetter] * 676 + lettersAndIndexes[secondLetter] * 26 + lettersAndIndexes[thirdLetter];
                  break;
              }
              default:
                  console.log(`unhandled letterCode.length: ${letterCode.length} ${letterCode}`);
                  break;
          }
          return numIndex;
      }
          function _createNode(doc, nodeName, opts) {
            var tempNode = doc.createElement(nodeName);
            if (opts) {
                if (opts.attr) {
                    $(tempNode).attr(opts.attr);
                }
                if (opts.children) {
                    $.each(opts.children, function (key, value) {
                        tempNode.appendChild(value);
                    });
                }
                if (opts.text !== null && opts.text !== undefined) {
                    tempNode.appendChild(doc.createTextNode(opts.text));
                }
            }
            return tempNode;
        }
        
        function mergeFunction(referenceString) {
          // reference string format: 'B1:E1'
          var mergeCells = $('mergeCells', sheet);
          console.log("mergeCells",mergeCells);
          if (!referenceString) {
              alert('no reference string on merge function call');
              return
          }
          mergeCells[0].appendChild(_createNode(sheet, 'mergeCell', {
              attr: {
                  ref: referenceString
              }
          }));
          mergeCells.attr('count', mergeCells.attr('count') + 1);
      }
  
      function getRowNodeHavingRowGroupHeader(currentRow):rowDetailsForExcelCustomization{
        let rowDetails={
          startingCellNum:currentRow.lastChild.attributes[1].value,
          rowNum:currentRow.attributes[0].value
        }
        return rowDetails;
      }
        },
        }
        ],
      };
  }

  getMonthsInASequenceForListView(qualityEventRecords:CCNListStructureForDeptPerformance[] | IncidentListStructureForDeptPerformance[] | ErrataListStructureForDeptPerformance[] | NTFListStructureForDeptPerformance[]){

    for(let i=0;i<qualityEventRecords.length;i++){
      qualityEventRecords[i].Month = this.dashboardService.getMonth(parseInt(qualityEventRecords[i].Month));
    }
    let fromDateMonth=this.dates.fromDate.substr(3,3);
    let fromDateYear=this.dates.fromDate.substr(7,4);
    let toDateYear=this.dates.toDate.substr(7,4);
    let seqArrayWithStartingMonth=[];
    
    let i=0;
    let startingMonthIndexNumber:number;
    let otherHalfseqArray=[];
    
    for(i=0;i<qualityEventRecords.length;i++){
      if(qualityEventRecords[i]?.Month===fromDateMonth){
        startingMonthIndexNumber=i;
        seqArrayWithStartingMonth.push(qualityEventRecords[i]);
        qualityEventRecords[i].MonthnameAndYear=qualityEventRecords[i].Month+' - '+fromDateYear;
        for(i=startingMonthIndexNumber+1;i<qualityEventRecords.length;i++){
          seqArrayWithStartingMonth.push(qualityEventRecords[i]);
          qualityEventRecords[i].MonthnameAndYear=qualityEventRecords[i].Month+' - '+fromDateYear;
        }
      }
      else{
        otherHalfseqArray.push(qualityEventRecords[i]);
        qualityEventRecords[i].MonthnameAndYear=qualityEventRecords[i].Month+' - '+toDateYear;
      }
    }
    let modifiedDeptPerformancedataForList=[...seqArrayWithStartingMonth,...otherHalfseqArray];
    qualityEventRecords=modifiedDeptPerformancedataForList;
      this.records=qualityEventRecords;
  }
onCloseListView(){
  this.bsModalRef.hide();
}


ngOnDestroy():void{
  this.events.forEach((subscription: Subscription) => {
    subscription.unsubscribe();
  });
  this.events = [];
}
}
