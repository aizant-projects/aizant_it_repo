import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListViewDeptPerformanceComponent } from './list-view-dept-performance.component';

describe('ListViewDeptPerformanceComponent', () => {
  let component: ListViewDeptPerformanceComponent;
  let fixture: ComponentFixture<ListViewDeptPerformanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListViewDeptPerformanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListViewDeptPerformanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
