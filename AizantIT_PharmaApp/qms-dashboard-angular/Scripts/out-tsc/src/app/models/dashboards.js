export const QualityEventIDs = {
    IncidentReport: 1,
    ChangeControlNote: 2,
    Errata: 3,
    NoteToFile: 4
};
export const QualityEvents = {
    ChangeControlNote: 'Change Control Note',
    IncidentReport: 'Incident Report',
    Errata: 'Errata',
    NoteToFile: 'Note To File'
};
export const QualityEventStatus = {
    Approved: 'Approved',
    Closed: 'Closed',
    OverDue: 'OverDue',
    Pending: 'Pending',
    Rejected: 'Rejected',
    OverdueLessThanOneWk: "< 1wk",
    OverdueOneWkToOneMn: "1wk-1mn",
    OverdueOneMnToThreeMn: "1mn-3mn",
    OverdueThreeMnToSixMn: "3mn-6mn",
    OverdueGreaterThanSixMn: ">6mn"
};
export const ChangeCategoriesListValues = {
    permanent: "Permanent",
    temporary: "Temporary"
};
export const ChangeClassificationListValues = {
    major: "Major",
    minor: "Minor",
    unclassified: "Unclassified"
};
export const CCNClassificationStatusCodes = {
    Major: 1,
    Minor: 2,
    Unclassified: 7,
    All: 0
};
export const CCNCategoryStatusCodes = {
    Permanent: 3,
    Temporary: 4,
    Both: 0
};
export const IncidentClassificationListValues = {
    qualityImpacting: "Quality Impacting",
    qualityNonImpacting: "Quality Non-Impacting",
    unclassified: "Unclassified"
};
export const IncidentClassificationStatusCodes = {
    QualityImpacting: 5,
    QualityNonImpacting: 6,
    Unclassified: 8,
    All: 0
};
export const QualityEventIds = {
    IncidentReport: 1,
    CCN: 2,
    Errata: 3,
    NoteToFile: 4
};
export const MetricsListValues = {
    CountMetricID: 0,
    countMetricName: "Count",
    TATMetricID: 1,
    tatMetricName: "Turn Around Time"
};
export const ShowTypeList = {
    OverallList: 1,
    Approved: 2,
    Pending: 3,
    Closed: 4,
    Rejected: 5,
    OverallOverdueList: 6,
    OverdueLessThanOneWk: 7,
    OverdueOneWkToOneMn: 8,
    OverdueOneMnToThreeMn: 9,
    OverdueThreeMnToSixMn: 10,
    OverdueGreaterThanSixMn: 11
};
export const TrimmedDepartmentNames = {
    "Analytical Development": 'AD',
    "Business Development": 'BD',
    "Clinical Pharmacology": 'CP',
    "Engineering": 'Engg',
    "Executive Managements": 'EM',
    "Human Resources": 'HR',
    "Production": 'Prod.',
    "Quality Assurance": 'QA',
    "Quality Control": 'QC',
    "Regulatory Affairs": 'Reg. Aff',
    "Warehouse": 'WH'
};
//# sourceMappingURL=dashboards.js.map