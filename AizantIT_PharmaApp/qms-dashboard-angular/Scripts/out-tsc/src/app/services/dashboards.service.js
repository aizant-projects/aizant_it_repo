import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
let DashboardsService = class DashboardsService {
    constructor() {
        this.statusChartColorPalette = ["#003f5c", "#665191", "#d45087", "#f95d6a"];
        this.colorPalette1 = ["#003f5c", "#2f4b7c", "#665191", "#a05195", "#d45087", "#f95d6a", "#ff7c43", "#ffa600"];
        this.colorPalette2 = ["#1565c0", "#f9a825", "#2e7d32", "#ef6c00", "#4e342e", "#37474f", "#178b7e", "#042347", "#424242", "#4d8d36"];
        this.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    }
    getColor(QualityEventID) {
        return this.colorPalette1[QualityEventID];
    }
    getMonth(monthNum) {
        return this.months[monthNum - 1];
    }
    getModifiedOverdueData(overdueData) {
        //   var localData = overdueData.sort(function (a, b) {
        //     return b.count - a.count;
        // });
        var localData = overdueData;
        var totalCount = localData.reduce(function (prevValue, item) {
            return prevValue + item.TotalRecords;
        }, 0), cumulativeCount = 0;
        return localData.map(function (item, index) {
            cumulativeCount += item.TotalRecords;
            return {
                EventName: item.EventName,
                TotalRecords: item.TotalRecords,
                cumulativePercent: Math.round(cumulativeCount * 100 / totalCount)
            };
        });
    }
};
DashboardsService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    })
], DashboardsService);
export { DashboardsService };
//# sourceMappingURL=dashboards.service.js.map