import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
let ServerService = class ServerService {
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    getAllDepartmentsList() {
        return this.httpClient.get(`${environment.serverUrl}/GetdepartmentList`);
    }
    getQualityEventsList() {
        return this.httpClient.get(`${environment.serverUrl}/GetQualityEventList`);
    }
    getIncidentCategoryList() {
        return this.httpClient.get(`${environment.serverUrl}/BindIncidentCategory`);
    }
    getDepartmentsQualityEventsData(departmentsQualityEventsForData) {
        return this.httpClient.post(`${environment.serverUrl}/ManagementChart`, departmentsQualityEventsForData);
    }
    getQualityEventStatusOfOneDept(qualityEventDataOfOneDept) {
        return this.httpClient.post(`${environment.serverUrl}/StatusChartForQualityEvents`, qualityEventDataOfOneDept);
    }
    getOverdueData(overdueDataFilters) {
        return this.httpClient.post(`${environment.serverUrl}/OverdueChart`, overdueDataFilters);
    }
    getDeptPerformanceDataOfCountMetric(deptPerformanceDataFilters) {
        return this.httpClient.post(`${environment.serverUrl}/GetPerformanceChartCount`, deptPerformanceDataFilters);
    }
    getDeptPerformanceDataOfTATMetric(deptPerformanceDataFilters) {
        return this.httpClient.post(`${environment.serverUrl}/GetPerformanceChartTAT`, deptPerformanceDataFilters);
    }
    getCCNList(ccnListFilters) {
        return this.httpClient.post(`${environment.serverUrl}/GetCCNList`, ccnListFilters);
    }
    getIncidentList(incidentListFilters) {
        return this.httpClient.post(`${environment.serverUrl}/GetIncidentList`, incidentListFilters);
    }
    getErrataList(errataListFilters) {
        return this.httpClient.post(`${environment.serverUrl}/GetErrataList`, errataListFilters);
    }
    getNTFList(noteToFileListFilters) {
        return this.httpClient.post(`${environment.serverUrl}/GetNTFList`, noteToFileListFilters);
    }
    getCCNListForDeptPerformance(ccnListFilters) {
        return this.httpClient.post(`${environment.serverUrl}/GetCCNListViewForPerformanceChart`, ccnListFilters);
    }
    getIncidentListForDeptPerformance(incidentListFilters) {
        return this.httpClient.post(`${environment.serverUrl}/GetIncidentListViewForPerformanceChart`, incidentListFilters);
    }
    getErrataListForDeptPerformance(errataListFilters) {
        return this.httpClient.post(`${environment.serverUrl}/GetErrataListViewForPerformanceChart`, errataListFilters);
    }
    getNTFListForDeptPerformance(noteToFileListFilters) {
        return this.httpClient.post(`${environment.serverUrl}/GetNTFListViewForPerformanceChart`, noteToFileListFilters);
    }
    getQualityEventsListForCAPA() {
        return this.httpClient.get(`${environment.serverUrl}/GetQualityEventListforCAPAChart`);
    }
    getDeptQualityEventsDataForCAPA(departmentsQualityEventsFiltersForCAPA) {
        return this.httpClient.post(`${environment.serverUrl}/CapaBarChart`, departmentsQualityEventsFiltersForCAPA);
    }
    getQualityEventStatusOfOneDeptForCAPA(departmentsQualityEventStatusFiltersForCAPA) {
        return this.httpClient.post(`${environment.serverUrl}/StatusChartForCAPA`, departmentsQualityEventStatusFiltersForCAPA);
    }
};
ServerService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    })
], ServerService);
export { ServerService };
//# sourceMappingURL=server.service.js.map