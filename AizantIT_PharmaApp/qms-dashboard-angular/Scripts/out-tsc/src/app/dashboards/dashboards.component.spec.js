import * as tslib_1 from "tslib";
import { TestBed } from '@angular/core/testing';
import { DashboardsComponent } from './dashboards.component';
describe('DashboardsComponent', () => {
    let component;
    let fixture;
    beforeEach(() => tslib_1.__awaiter(this, void 0, void 0, function* () {
        yield TestBed.configureTestingModule({
            declarations: [DashboardsComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(DashboardsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=dashboards.component.spec.js.map