import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { CCNCategoryStatusCodes, CCNClassificationStatusCodes, ChangeCategoriesListValues, ChangeClassificationListValues, IncidentClassificationListValues, IncidentClassificationStatusCodes, QualityEventIDs, QualityEventIds, QualityEventStatus, TrimmedDepartmentNames } from '../../models/dashboards';
import * as moment from 'moment';
let CapaChartComponent = class CapaChartComponent {
    constructor(dashboardService, fb, serverService, modalService, renderer) {
        this.dashboardService = dashboardService;
        this.fb = fb;
        this.serverService = serverService;
        this.modalService = modalService;
        this.renderer = renderer;
        this.departmentsList = [];
        this.selectedDepartments = [];
        this.departmentDropdownSettings = {};
        this.eventsDropdownSettings = {};
        this.qualityEventsList = [];
        this.selectedQualityEvents = [];
        this.changeCategoriesList = [];
        this.selectedChangeCategory = [];
        this.changeCategoryDropdownSettings = {};
        this.changeClassificationList = [];
        this.selectedChangeClassification = [];
        this.changeClassificationDropdownSettings = {};
        this.selectedCCNClassificationIDs = [];
        this.incidentCategoriesList = [];
        this.selectedIncidentCategory = [];
        this.incidentCategoryDropdownSettings = {};
        this.incidentClassificationList = [];
        this.selectedIncidentClassification = [];
        this.incidentClassificationDropdownSettings = {};
        this.fromDateOptions = {
            dateFormat: "d-M-Y"
        };
        this.toDateOptions = {
            dateFormat: "d-M-Y"
        };
        this.currentQualityEventId = QualityEventIds;
        this.overallStatuslevel = 0;
        this.selectedDeptIds = [];
        this.overdueDataCAPA = [];
        //returns department ID from Dept name
        this.getDeptIdFromDeptName = (deptName) => {
            for (let i = 0; i < this.departmentsList.length; i++) {
                if (this.departmentsList[i].DepartmentName === deptName) {
                    return this.departmentsList[i].Department_ID;
                }
            }
        };
        //returns qualityEvent ID from qualityEvent name
        this.getQualityEventIdFromQualityEventName = (qualityEventName) => {
            for (let i = 0; i < this.qualityEventsList.length; i++) {
                if (this.qualityEventsList[i].QualityEvent_TypeName === qualityEventName) {
                    return this.qualityEventsList[i].QualityEvent_TypeID;
                }
            }
        };
        //converts fetched date into DB compatible date format
        this.getRequiredDateFormat = (selectedDate) => {
            var tempDate = new Date(selectedDate);
            let sampleFromDate = moment(tempDate.toISOString());
            return sampleFromDate.format('DD-MMM-YYYY');
        };
        this.customizeTooltipForOverdue = (info) => {
            return {
                html: "<div><div class='tooltip-header'>" +
                    info.argumentText + "</div>" +
                    "<div class='tooltip-body'><div class='series-name'>" +
                    "<span class='top-series-name'>" + info.points[0].seriesName + "</span>" +
                    ": </div><div class='value-text'>" +
                    "<span class='top-series-value'>" + info.points[0].valueText + "</span>" +
                    "</div><div class='series-name'>" +
                    "<span class='bottom-series-name'>" + info.points[1].seriesName + "</span>" +
                    ": </div><div class='value-text'>" +
                    "<span class='bottom-series-value'>" + info.points[1].valueText + "</span>" +
                    "% </div></div></div>"
            };
        };
        this.customizeLabelText = (info) => {
            return info.valueText + "%";
        };
        this.overallStatuslevel = 0;
        this.setChangeCategoryAndClassificationDropdown();
        this.setIncidentCategoryAndClassificationDropdown();
        this.CAPAfiltersForm = this.fb.group({
            fromDate: [''],
            toDate: ['']
        });
        this.getDepartmentsList();
        this.getQualityEventsListForCAPAChart();
        this.fromDateOptions.defaultDate = this.initialFromDate = moment().subtract(1, 'years').format('DD-MMM-YYYY');
        this.toDateOptions.defaultDate = this.initialToDate = moment().format('DD-MMM-YYYY');
        this.getAllDepartmentQualityEventsCAPADataOnInitialLoad();
        this.subtitleForStackedBarChart = '( ' + this.allDepartmentQualityEventsFiltersForCAPA.FromDate + ' to ' + this.allDepartmentQualityEventsFiltersForCAPA.ToDate + ' )';
    }
    ngOnInit() {
        this.departmentDropdownSettings = {
            singleSelection: false,
            idField: 'Department_ID',
            textField: 'DepartmentName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
        };
        this.eventsDropdownSettings = {
            singleSelection: true,
            idField: 'QualityEvent_TypeID',
            textField: 'QualityEvent_TypeName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
        };
    }
    setChangeCategoryAndClassificationDropdown() {
        this.changeCategoryDropdownSettings = {
            singleSelection: false,
            idField: 'changeCategoryID',
            textField: 'changeCategoryName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: false,
        };
        this.changeClassificationDropdownSettings = {
            singleSelection: false,
            idField: 'changeClassificationID',
            textField: 'changeClassificationName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: false,
        };
        this.changeCategoriesList = [
            { changeCategoryID: CCNCategoryStatusCodes.Permanent, changeCategoryName: ChangeCategoriesListValues.permanent },
            { changeCategoryID: CCNCategoryStatusCodes.Temporary, changeCategoryName: ChangeCategoriesListValues.temporary }
        ];
        this.selectedChangeCategory = this.changeCategoriesList;
        this.changeClassificationList = [
            { changeClassificationID: CCNClassificationStatusCodes.Major, changeClassificationName: ChangeClassificationListValues.major },
            { changeClassificationID: CCNClassificationStatusCodes.Minor, changeClassificationName: ChangeClassificationListValues.minor },
            { changeClassificationID: CCNClassificationStatusCodes.Unclassified, changeClassificationName: ChangeClassificationListValues.unclassified }
        ];
        this.selectedChangeClassification = this.changeClassificationList;
    }
    setIncidentCategoryAndClassificationDropdown() {
        this.incidentCategoryDropdownSettings = {
            singleSelection: false,
            idField: 'TypeofCategoryID',
            textField: 'TypeofIncident',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            closeDropDownOnSelection: false
        };
        this.serverService.getIncidentCategoryList().subscribe((res) => {
            this.incidentCategoriesList = res.data;
            this.selectedIncidentCategory = this.incidentCategoriesList;
        }, (err) => {
            console.error(err);
        });
        this.incidentClassificationDropdownSettings = {
            singleSelection: false,
            idField: 'incidentClassificationID',
            textField: 'incidentClassificationName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true
        };
        this.incidentClassificationList = [
            {
                "incidentClassificationID": IncidentClassificationStatusCodes.QualityImpacting,
                "incidentClassificationName": IncidentClassificationListValues.qualityImpacting
            },
            {
                "incidentClassificationID": IncidentClassificationStatusCodes.QualityNonImpacting,
                "incidentClassificationName": IncidentClassificationListValues.qualityNonImpacting
            },
            {
                "incidentClassificationID": IncidentClassificationStatusCodes.Unclassified,
                "incidentClassificationName": IncidentClassificationListValues.unclassified
            }
        ];
        this.selectedIncidentClassification = this.incidentClassificationList;
    }
    getAllDepartmentQualityEventsCAPADataOnInitialLoad() {
        this.allDepartmentQualityEventsFiltersForCAPA = {
            DeptID: this.getDepartmentListIds(),
            QualityEventType: this.getQualityEventIds(),
            FromDate: this.initialFromDate,
            ToDate: this.initialToDate
        };
        console.log("CAPA Chart filters", this.allDepartmentQualityEventsFiltersForCAPA);
        this.previousDataFiltersForStackedBarChart = this.allDepartmentQualityEventsFiltersForCAPA;
        this.getDepartmentsQualityEventsDataFromFiltersOfCAPA(this.allDepartmentQualityEventsFiltersForCAPA);
    }
    onSelectAllDepartments(items) {
        this.selectedDepartments = items;
        this.onDeptQualityEventsCAPADataChartFilterChange(items);
    }
    onSelectAllQualityEvents(items) {
        this.selectedQualityEvents = items;
        this.onDeptQualityEventsCAPADataChartFilterChange(items);
    }
    onSelectAllChangeCategories(items) {
        this.selectedChangeCategory = items;
        // this.getFilteredStatusChartData();
    }
    onSelectAllChangeClassifications(items) {
        this.selectedChangeClassification = items;
        // this.getFilteredStatusChartData();
    }
    onSelectAllIncidentCategories(items) {
        this.selectedIncidentCategory = items;
        // this.getFilteredStatusChartData();
    }
    onSelectAllIncidentClassification(items) {
        this.selectedIncidentClassification = items;
        // this.getFilteredStatusChartData();
    }
    getColorOFQE(QualityEventNumber) {
        return this.dashboardService.getColor(QualityEventNumber);
    }
    getColorPaletteForStatusChart() {
        return this.dashboardService.colorPalette2;
    }
    //Extracting Department IDs from departmentsList Object array
    getDepartmentListIds() {
        let departmentIds = [];
        for (let i = 0; i < this.departmentsList.length; i++) {
            departmentIds.push(this.departmentsList[i].Department_ID);
        }
        return departmentIds;
    }
    //Extracting Quality Event IDs from qualityEventsList Object array
    getQualityEventIds() {
        let qualityEventIds = [];
        for (let i = 0; i < this.qualityEventsList.length; i++) {
            qualityEventIds.push(this.qualityEventsList[i].QualityEvent_TypeID);
        }
        return qualityEventIds;
    }
    //fetches chart data based on filters passed
    getDepartmentsQualityEventsDataFromFiltersOfCAPA(allDepartmentQualityEventsFiltersForCAPA) {
        this.serverService.getDeptQualityEventsDataForCAPA(allDepartmentQualityEventsFiltersForCAPA).subscribe((res) => {
            console.log("capa data", res);
            this.dataSource = res.CapaCount;
            for (let i = 0; i < this.dataSource.length; i++) {
                this.dataSource[i].DepartmentName = TrimmedDepartmentNames[this.dataSource[i].DepartmentName];
            }
            this.dataSource = this.dataSource.sort(function (x, y) {
                if (x.DepartmentName < y.DepartmentName)
                    return -1;
                if (x.DepartmentName > y.DepartmentName)
                    return 1;
                return 0;
            });
            // this.dataSource=this.dataSource;
            console.log("stacked bar data", this.dataSource);
        }, (err) => {
            console.error(err);
        });
    }
    //fetches departmentsList from DB and sorts it alphabetically
    getDepartmentsList() {
        // let roleAndEventID={
        //   roleID:2,
        //   eventId:3
        // }
        this.serverService.getAllDepartmentsList().subscribe((resultantDepartments) => {
            this.departmentsList = resultantDepartments.DeptFevent_list;
            this.departmentsList = this.departmentsList.sort(function (x, y) {
                if (x.DepartmentName < y.DepartmentName)
                    return -1;
                if (x.DepartmentName > y.DepartmentName)
                    return 1;
                return 0;
            });
            this.selectedDepartments = this.departmentsList;
            this.previousDataFiltersForStackedBarChart.DeptID = this.getDepartmentListIds();
        }, (err) => {
            console.log(err);
        });
    }
    //fetches qualityEventsList from DB and sorts it alphabetically
    getQualityEventsListForCAPAChart() {
        this.serverService.getQualityEventsListForCAPA().subscribe((resultantQualityEvents) => {
            this.qualityEventsList = resultantQualityEvents.sort(function (x, y) {
                if (x.QualityEvent_TypeName < y.QualityEvent_TypeName)
                    return -1;
                if (x.QualityEvent_TypeName > y.QualityEvent_TypeName)
                    return 1;
                return 0;
            });
            // this.selectedQualityEvents=this.qualityEventsList;
            this.selectedQualityEvents = [];
            this.previousDataFiltersForStackedBarChart.QualityEventType = this.getQualityEventIds();
        }, (err) => {
            console.error(err);
        });
    }
    getDeptNameFromDeptID(deptId) {
        for (let i = 0; i < this.departmentsList.length; i++) {
            if (this.departmentsList[i].Department_ID === deptId) {
                return this.departmentsList[i].DepartmentName;
            }
        }
    }
    getQualityEventNameFromQualityEventId(qualityEventID) {
        for (let i = 0; i < this.qualityEventsList.length; i++) {
            if (this.qualityEventsList[i].QualityEvent_TypeID === qualityEventID) {
                return this.qualityEventsList[i].QualityEvent_TypeName;
            }
        }
    }
    setFromAndToDates() {
        this.selectedFromDate = this.CAPAfiltersForm.get('fromDate').value[0];
        this.selectedToDate = this.CAPAfiltersForm.get('toDate').value[0];
    }
    getSelectedDeptIDs() {
        this.selectedDeptIds = [];
        for (let i = 0; i < this.selectedDepartments.length; i++) {
            this.selectedDeptIds.push(this.selectedDepartments[i].Department_ID);
        }
    }
    getSelectedQualityEventIDs() {
        this.selectedQualityEventIds = [];
        for (let i = 0; i < this.selectedQualityEvents.length; i++) {
            this.selectedQualityEventIds.push(this.selectedQualityEvents[i].QualityEvent_TypeID);
        }
    }
    getSelectedCCNClassificationIDs() {
        this.selectedCCNClassificationIDs = [];
        for (let i = 0; i < this.selectedChangeClassification.length; i++) {
            this.selectedCCNClassificationIDs.push(this.selectedChangeClassification[i].changeClassificationID);
        }
    }
    onDeptQualityEventsCAPADataChartFilterChange(event) {
        this.dataSource = [];
        this.setFromAndToDates();
        if (this.overallStatuslevel === 0) {
            this.getSelectedDeptIDs();
            this.getSelectedQualityEventIDs();
            this.allDepartmentQualityEventsFiltersForCAPA = {
                DeptID: this.selectedDepartments.length === 0 ? [] : this.selectedDeptIds,
                QualityEventType: ((this.selectedQualityEvents.length === 0) || (this.selectedQualityEvents.length === this.qualityEventsList.length)) ? [] : this.selectedQualityEventIds,
                FromDate: this.selectedFromDate !== undefined ? this.getRequiredDateFormat(this.selectedFromDate) : this.initialFromDate,
                ToDate: this.selectedToDate !== undefined ? this.getRequiredDateFormat(this.selectedToDate) : this.initialToDate
            };
            console.log("on filter change", this.allDepartmentQualityEventsFiltersForCAPA);
            this.previousDataFiltersForStackedBarChart = this.allDepartmentQualityEventsFiltersForCAPA;
            this.subtitleForStackedBarChart = '( ' + this.allDepartmentQualityEventsFiltersForCAPA.FromDate + ' to ' + this.allDepartmentQualityEventsFiltersForCAPA.ToDate + ' )';
            this.getDepartmentsQualityEventsDataFromFiltersOfCAPA(this.allDepartmentQualityEventsFiltersForCAPA);
        }
    }
    getUntrimmedDeptName(trimmedDeptName) {
        for (let i = 0; i < this.departmentsList.length; i++) {
            if (trimmedDeptName === TrimmedDepartmentNames[this.departmentsList[i].DepartmentName]) {
                return this.departmentsList[i].DepartmentName;
            }
        }
    }
    onDeptQualityEventCAPAPointClick(event) {
        this.setFromAndToDates();
        this.selectedDepartments = [];
        let unTrimmedDeptName = this.getUntrimmedDeptName(event.target.originalArgument);
        // event.target.originalArgument = event.target.originalArgument === 'Executive Management'? 'Executive Managements' : event.target.originalArgument;
        this.selectedDepartments.push({
            Department_ID: this.getDeptIdFromDeptName(unTrimmedDeptName),
            DepartmentName: unTrimmedDeptName
        });
        this.selectedQualityEvents = [];
        this.selectedQualityEvents.push({
            QualityEvent_TypeID: this.getQualityEventIdFromQualityEventName(event.target.series.name),
            QualityEvent_TypeName: event.target.series.name
        });
        this.selectedChangeCategory = this.changeCategoriesList;
        this.selectedChangeClassification = this.changeClassificationList;
        this.selectedIncidentCategory = this.incidentCategoriesList;
        this.selectedIncidentClassification = this.incidentClassificationList;
        this.prevCurrentDrillDownHeaderText = this.currentDrillDownHeaderText = event.target.series.name;
        this.selectedQualityEventName = event.target.series.name;
        this.getQualityEventDataOfOneDept(this.selectedDepartments[0].Department_ID, event.target.series.name);
    }
    onStackedBarChartExporting(event) {
        event.fileName = "Total CAPA Count of QE of Depts " + this.subtitleForStackedBarChart;
    }
    customizeTooltip(arg) {
        return {
            text: arg.seriesName + ' : ' + arg.valueText
        };
    }
    getQualityEventDataOfOneDept(deptId, qualityEventName) {
        let numToArrConversion = [];
        numToArrConversion.push(deptId);
        this.qualityEventDataOfOneDept = {
            DeptID: numToArrConversion,
            QualityEventType: this.getQualityEventIdFromQualityEventName(qualityEventName),
            CCNClassification: CCNClassificationStatusCodes.All,
            CCNCategory: CCNCategoryStatusCodes.Both,
            IncidentClassification: IncidentClassificationStatusCodes.All,
            IncidentCategory: 0,
            FromDate: this.selectedFromDate !== undefined ? this.getRequiredDateFormat(this.selectedFromDate) : this.initialFromDate,
            ToDate: this.selectedToDate !== undefined ? this.getRequiredDateFormat(this.selectedToDate) : this.initialToDate
        };
        this.selectedQualityEventIdForStatusChart = this.qualityEventDataOfOneDept.QualityEventType;
        this.previousDataFiltersForStatusChart = this.qualityEventDataOfOneDept;
        this.serverService.getQualityEventStatusOfOneDeptForCAPA(this.qualityEventDataOfOneDept).subscribe((res) => {
            console.log("result of status chart capa", res);
            this.statusListData = res.QeStatus_List;
            this.statusListData = this.excludeEmptyRecordsOfStatusChart(this.statusListData);
            this.overallStatuslevel++;
        }, (err) => {
            console.error(err);
        });
    }
    excludeEmptyRecordsOfStatusChart(statusListData) {
        let sampleListData = [];
        for (let i = 0; i < statusListData.length; i++) {
            if (statusListData[i].TotalRecords !== 0) {
                sampleListData.push(statusListData[i]);
            }
        }
        return sampleListData;
    }
    customizeLabel(arg) {
        return arg.argumentText + ' : ' + arg.valueText;
    }
    onShowOverallCAPAListView() {
    }
    getFilteredStatusChartData() {
        if (this.overallStatuslevel === 1) {
            this.getSelectedDeptIDs();
            this.getSelectedCCNClassificationIDs();
            this.filteredQualityEventData = {
                DeptID: this.selectedDeptIds,
                QualityEventType: this.selectedQualityEventIdForStatusChart,
                // CCNClassification:(this.selectedChangeClassification.length===0) ? CCNClassificationStatusCodes.All: this.selectedChangeClassification[0].changeClassificationID,
                CCNClassification: ((this.selectedChangeClassification.length === 0) || (this.selectedChangeClassification.length > 1)) ? CCNClassificationStatusCodes.All : this.selectedChangeClassification[0].changeClassificationID,
                CCNCategory: (this.selectedChangeCategory.length === 0 || this.selectedChangeCategory.length > 1) ? CCNCategoryStatusCodes.Both : this.selectedChangeCategory[0].changeCategoryID,
                IncidentClassification: (this.selectedIncidentClassification.length === 0 || this.selectedIncidentClassification.length > 1) ? IncidentClassificationStatusCodes.All : this.selectedIncidentClassification[0].incidentClassificationID,
                IncidentCategory: 0,
                FromDate: this.selectedFromDate !== undefined ? this.getRequiredDateFormat(this.selectedFromDate) : this.initialFromDate,
                ToDate: this.selectedToDate !== undefined ? this.getRequiredDateFormat(this.selectedToDate) : this.initialToDate
            };
            this.previousDataFiltersForStatusChart = this.filteredQualityEventData;
            this.getFilteredQualityEventStatusChart(this.filteredQualityEventData);
        }
        else if (this.overallStatuslevel === 2) {
            // this.getFilteredOverdueData();
        }
    }
    getFilteredQualityEventStatusChart(filteredQualityEventData) {
        this.serverService.getQualityEventStatusOfOneDeptForCAPA(filteredQualityEventData).subscribe((res) => {
            this.statusListData = res.QeStatus_List;
            this.statusListData = this.excludeEmptyRecordsOfStatusChart(this.statusListData);
        }, (err) => {
            console.error(err);
        });
    }
    onPreviousLevelClick() {
        if (this.overallStatuslevel === 1) {
            this.overallStatuslevel--;
            this.getDepartmentsQualityEventsDataFromFiltersOfCAPA(this.previousDataFiltersForStackedBarChart);
            this.setSelectedDepartmentsValues(this.previousDataFiltersForStackedBarChart.DeptID);
            this.setSelectedQualityEventValues(this.previousDataFiltersForStackedBarChart.QualityEventType, 'qualityEventsChart');
            this.setSelectedDateValues(this.previousDataFiltersForStackedBarChart.FromDate, this.previousDataFiltersForStackedBarChart.ToDate);
        }
        else if (this.overallStatuslevel == 2) {
            this.overallStatuslevel--;
            this.currentDrillDownHeaderText = this.prevCurrentDrillDownHeaderText;
            this.getFilteredQualityEventStatusChart(this.previousDataFiltersForStatusChart);
            this.setSelectedDepartmentsValues(this.previousDataFiltersForStatusChart.DeptID);
            this.setSelectedQualityEventValues(this.previousDataFiltersForStatusChart.QualityEventType, 'statusChart');
            this.setSelectedDateValues(this.previousDataFiltersForStatusChart.FromDate, this.previousDataFiltersForStatusChart.ToDate);
            this.setChangeCategoryAndClassficationValues(this.previousDataFiltersForStatusChart.CCNCategory, this.previousDataFiltersForStatusChart.CCNClassification);
            this.setIncidentCategoryAndClassificationValues(this.previousDataFiltersForStatusChart.IncidentCategory, this.previousDataFiltersForStatusChart.IncidentClassification);
        }
    }
    setSelectedDepartmentsValues(deptIds) {
        this.selectedDepartments = [];
        for (let i = 0; i < deptIds.length; i++) {
            this.selectedDepartments.push({
                Department_ID: deptIds[i],
                DepartmentName: this.getDeptNameFromDeptID(deptIds[i])
            });
        }
    }
    setSelectedQualityEventValues(qualityEventIds, source) {
        this.selectedQualityEvents = [];
        if (source === 'qualityEventsChart') {
            for (let i = 0; i < qualityEventIds.length; i++) {
                this.selectedQualityEvents.push({
                    QualityEvent_TypeID: qualityEventIds[i],
                    QualityEvent_TypeName: this.getQualityEventNameFromQualityEventId(qualityEventIds[i])
                });
            }
        }
        else if (source === 'statusChart') {
            this.selectedQualityEvents.push({
                QualityEvent_TypeID: qualityEventIds,
                QualityEvent_TypeName: this.getQualityEventNameFromQualityEventId(qualityEventIds)
            });
        }
    }
    setSelectedDateValues(fromDate, toDate) {
        this.fromDateValue = fromDate;
        this.toDateValue = toDate;
    }
    setChangeCategoryAndClassficationValues(changeCategoryId, changeClassificationId) {
        this.selectedChangeCategory = [];
        this.selectedChangeClassification = [];
        if (changeCategoryId === 0) {
            for (let i = 0; i < this.changeCategoriesList.length; i++) {
                this.selectedChangeCategory.push({
                    changeCategoryID: this.changeCategoriesList[i].changeCategoryID,
                    changeCategoryName: this.changeCategoriesList[i].changeCategoryName
                });
            }
        }
        else if (changeCategoryId !== 0) {
            this.selectedChangeCategory.push({
                changeCategoryID: changeCategoryId,
                changeCategoryName: this.getChangeCatNameFromChangeCatID(changeCategoryId)
            });
        }
        if (changeClassificationId === 0) {
            for (let i = 0; i < this.changeClassificationList.length; i++) {
                this.selectedChangeClassification.push({
                    changeClassificationID: this.changeClassificationList[i].changeClassificationID,
                    changeClassificationName: this.changeClassificationList[i].changeClassificationName
                });
            }
        }
        else if (changeClassificationId !== 0) {
            this.selectedChangeClassification.push({
                changeClassificationID: changeClassificationId,
                changeClassificationName: this.getChangeClassificationNameFromChangeClassificationID(changeClassificationId)
            });
        }
    }
    setIncidentCategoryAndClassificationValues(incidentCategoryId, incidentClassificationId) {
        this.selectedIncidentCategory = [];
        this.selectedIncidentClassification = [];
        if (incidentCategoryId === 0) {
            for (let i = 0; i < this.incidentCategoriesList.length; i++) {
                this.selectedIncidentCategory.push({
                    TypeofCategoryID: this.incidentCategoriesList[i].TypeofCategoryID,
                    TypeofIncident: this.incidentCategoriesList[i].TypeofIncident
                });
            }
        }
        else if (incidentCategoryId !== 0) {
            this.selectedIncidentCategory.push({
                TypeofCategoryID: incidentCategoryId,
                TypeofIncident: this.getIncidentCatNameFromIncidentCatID(incidentCategoryId)
            });
        }
        if (incidentClassificationId === 0) {
            for (let i = 0; i < this.incidentClassificationList.length; i++) {
                this.selectedIncidentClassification.push({
                    incidentClassificationID: this.incidentClassificationList[i].incidentClassificationID,
                    incidentClassificationName: this.incidentClassificationList[i].incidentClassificationName
                });
            }
        }
        else if (incidentClassificationId !== 0) {
            this.selectedIncidentClassification.push({
                incidentClassificationID: incidentClassificationId,
                incidentClassificationName: this.getIncidentClassificationNameFromIncidentClassificationID(incidentClassificationId)
            });
        }
    }
    getChangeCatNameFromChangeCatID(changeCategoryId) {
        for (let i = 0; i < this.changeCategoriesList.length; i++) {
            if (this.changeCategoriesList[i].changeCategoryID === changeCategoryId) {
                return this.changeCategoriesList[i].changeCategoryName;
            }
        }
    }
    getChangeClassificationNameFromChangeClassificationID(changeClassificationId) {
        for (let i = 0; i < this.changeClassificationList.length; i++) {
            if (this.changeClassificationList[i].changeClassificationID === changeClassificationId) {
                return this.changeClassificationList[i].changeClassificationName;
            }
        }
    }
    getIncidentCatNameFromIncidentCatID(incidentCategoryId) {
        for (let i = 0; i < this.incidentCategoriesList.length; i++) {
            if (this.incidentCategoriesList[i].TypeofCategoryID === incidentCategoryId) {
                return this.incidentCategoriesList[i].TypeofIncident;
            }
        }
    }
    getIncidentClassificationNameFromIncidentClassificationID(incidentclassificationId) {
        for (let i = 0; i < this.incidentClassificationList.length; i++) {
            if (this.incidentClassificationList[i].incidentClassificationID === incidentclassificationId) {
                return this.incidentClassificationList[i].incidentClassificationName;
            }
        }
    }
    onStatusChartPointClick(event) {
        if ((event.target.argument).toLowerCase() === (QualityEventStatus.OverDue).toLowerCase()) {
            this.setFromAndToDates();
            this.getSelectedDeptIDs();
            this.currentDrillDownHeaderText = event.target.argument + ' - ' + this.selectedQualityEventName;
            this.overallStatuslevel++;
            this.getFilteredOverdueData();
        }
        else if (this.selectedQualityEventIdForStatusChart === QualityEventIDs.ChangeControlNote) {
            // this.getCCNStatusListOfAppliedFilters(event.target.argument);
        }
        else if (this.selectedQualityEventIdForStatusChart === QualityEventIDs.IncidentReport) {
            // this.getIncidentStatusListOfAppliedFilters(event.target.argument);
        }
        else if (this.selectedQualityEventIdForStatusChart === QualityEventIDs.Errata) {
            // this.getErrataStatusListOfAppliedFilters(event.target.argument);
        }
        else {
            // this.getNTFStatusListOfAppliedFilters(event.target.argument);
        }
    }
    getFilteredOverdueData() {
        this.getSelectedDeptIDs();
        this.overdueDataFilters = {
            DeptID: this.selectedDeptIds,
            QualityEventType: this.selectedQualityEventIdForStatusChart,
            CCNClassification: ((this.selectedChangeClassification.length === 0) || (this.selectedChangeClassification.length > 1)) ? CCNClassificationStatusCodes.All : this.selectedChangeClassification[0].changeClassificationID,
            CCNCategory: (this.selectedChangeCategory.length === 0 || this.selectedChangeCategory.length > 1) ? CCNCategoryStatusCodes.Both : this.selectedChangeCategory[0].changeCategoryID,
            IncidentClassification: (this.selectedIncidentClassification.length === 0 || this.selectedIncidentClassification.length > 1) ? IncidentClassificationStatusCodes.All : this.selectedIncidentClassification[0].incidentClassificationID,
            IncidentCategory: 0,
            FromDate: this.selectedFromDate !== undefined ? this.getRequiredDateFormat(this.selectedFromDate) : this.initialFromDate,
            ToDate: this.selectedToDate !== undefined ? this.getRequiredDateFormat(this.selectedToDate) : this.initialToDate
        };
        this.serverService.getOverdueData(this.overdueDataFilters).subscribe((res) => {
            this.overdueDataCAPA = this.dashboardService.getModifiedOverdueData(res.Overdue_List);
        }, (err) => {
            console.error(err);
        });
    }
    onCAPAOverdueStatusChartPointClick(event) {
        if (this.selectedQualityEventIdForStatusChart === QualityEventIDs.ChangeControlNote) {
            // this.getCCNStatusListOfAppliedFilters(event.target.argument);
        }
        else if (this.selectedQualityEventIdForStatusChart === QualityEventIDs.IncidentReport) {
            // this.getIncidentStatusListOfAppliedFilters(event.target.argument);
        }
    }
};
CapaChartComponent = tslib_1.__decorate([
    Component({
        selector: 'app-capa-chart',
        templateUrl: './capa-chart.component.html',
        styleUrls: ['./capa-chart.component.scss']
    })
], CapaChartComponent);
export { CapaChartComponent };
//# sourceMappingURL=capa-chart.component.js.map