import * as tslib_1 from "tslib";
import { TestBed } from '@angular/core/testing';
import { CapaChartComponent } from './capa-chart.component';
describe('CapaChartComponent', () => {
    let component;
    let fixture;
    beforeEach(() => tslib_1.__awaiter(this, void 0, void 0, function* () {
        yield TestBed.configureTestingModule({
            declarations: [CapaChartComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(CapaChartComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=capa-chart.component.spec.js.map