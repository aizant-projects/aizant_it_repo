import * as tslib_1 from "tslib";
import { TestBed } from '@angular/core/testing';
import { ListViewDeptPerformanceComponent } from './list-view-dept-performance.component';
describe('ListViewDeptPerformanceComponent', () => {
    let component;
    let fixture;
    beforeEach(() => tslib_1.__awaiter(this, void 0, void 0, function* () {
        yield TestBed.configureTestingModule({
            declarations: [ListViewDeptPerformanceComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(ListViewDeptPerformanceComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=list-view-dept-performance.component.spec.js.map