import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { CCNCategoryStatusCodes, CCNClassificationStatusCodes, ChangeCategoriesListValues, ChangeClassificationListValues, IncidentClassificationStatusCodes, MetricsListValues, QualityEventIDs, QualityEventIds, } from '../../models/dashboards';
import * as moment from 'moment';
import monthSelectPlugin from 'flatpickr/dist/plugins/monthSelect';
import { ListViewDeptPerformanceComponent } from '../list-view-dept-performance/list-view-dept-performance.component';
let DepartmentPerformanceChartComponent = class DepartmentPerformanceChartComponent {
    constructor(dashboardService, fb, serverService, modalService) {
        this.dashboardService = dashboardService;
        this.fb = fb;
        this.serverService = serverService;
        this.modalService = modalService;
        this.departmentsList = [];
        this.selectedDepartments = [];
        this.departmentDropdownSettings = {};
        this.eventsDropdownSettings = {};
        this.qualityEventsList = [];
        this.selectedQualityEvents = [];
        this.changeCategoriesList = [];
        this.selectedChangeCategory = [];
        this.changeCategoryDropdownSettings = {};
        this.changeClassificationList = [];
        this.selectedChangeClassification = [];
        this.changeClassificationDropdownSettings = {};
        this.incidentCategoriesList = [];
        this.selectedIncidentCategory = [];
        this.incidentCategoryDropdownSettings = {};
        this.incidentClassificationList = [];
        this.selectedIncidentClassification = [];
        this.incidentClassificationDropdownSettings = {};
        this.metricSelectionDropdownSettings = {};
        this.metricSelectionList = [];
        this.selectedMetric = [];
        this.isLimitsShown = false;
        this.isAveragesShown = false;
        this.fromDateOptions = {
            // defaultDate: '2021-05-15',
            // minDate:'2021-05-15',
            dateFormat: "d-M-Y",
            plugins: [
                monthSelectPlugin({
                    shorthand: true,
                    dateFormat: "M Y",
                    altFormat: "M Y",
                    theme: "light" // defaults to "light"
                })
            ]
        };
        this.toDateOptions = {
            // defaultDate: '2021-05-15',
            // minDate:'2021-05-15',
            dateFormat: "d-M-Y",
            plugins: [
                monthSelectPlugin({
                    shorthand: true,
                    dateFormat: "M Y",
                    altFormat: "M Y",
                    theme: "light" // defaults to "light"
                })
            ]
        };
        this.currentQualityEventId = QualityEventIds;
        this.selectedDeptIds = [];
        this.initialVisitCount = 0;
        this.currentDeptAvg = 0;
        this.currentOrgAvg = 0;
        this.upperLimit = 0;
        this.lowerLimit = 0;
        this.upperLimitColor = "#ff9b52";
        this.lowerLimitColor = "#6199e6";
        this.currentMetricId = MetricsListValues.CountMetricID;
        this.CCNRecords = [];
        this.incidentRecords = [];
        this.errrataRecords = [];
        this.noteToFileRecords = [];
        this.customizePoint = (arg) => {
            if (arg.value > this.upperLimit) {
                return { color: this.upperLimitColor };
            }
            else if (arg.value < this.lowerLimit) {
                return { color: this.lowerLimitColor };
            }
        };
        this.customizeLabel = (arg) => {
            if (arg.value > this.upperLimit) {
                return this.getLabelsSettings(this.upperLimitColor);
            }
            else if (arg.value < this.lowerLimit) {
                return this.getLabelsSettings(this.lowerLimitColor);
            }
        };
        //returns department ID from Dept name
        this.getDeptIdFromDeptName = (deptName) => {
            for (let i = 0; i < this.departmentsList.length; i++) {
                if (this.departmentsList[i].DepartmentName === deptName) {
                    return this.departmentsList[i].Department_ID;
                }
            }
        };
        //returns qualityEvent ID from qualityEvent name
        this.getQualityEventIdFromQualityEventName = (qualityEventName) => {
            for (let i = 0; i < this.qualityEventsList.length; i++) {
                if (this.qualityEventsList[i].QualityEvent_TypeName === qualityEventName) {
                    return this.qualityEventsList[i].QualityEvent_TypeID;
                }
            }
        };
        //converts fetched date into DB compatible date format
        this.getRequiredDateFormat = (selectedDate) => {
            var tempDate = new Date(selectedDate);
            let sampleDate = moment(tempDate.toISOString());
            return sampleDate;
        };
        this.isListViewShown = false;
        this.setChangeCategoryAndClassificationDropdown();
        this.setIncidentCategoryAndClassificationDropdown();
        this.filtersForm = this.fb.group({
            fromDate: [''],
            toDate: ['']
        });
        // this loads dept,quality events list sequentially and fetches data from db
        this.loadInitialDeptPerformanceData();
        this.setMetricDropdown();
    }
    ngOnInit() {
        this.departmentDropdownSettings = {
            singleSelection: true,
            idField: 'Department_ID',
            textField: 'DepartmentName',
            allowSearchFilter: true,
            closeDropDownOnSelection: true
        };
        this.eventsDropdownSettings = {
            singleSelection: true,
            idField: 'QualityEvent_TypeID',
            textField: 'QualityEvent_TypeName',
            closeDropDownOnSelection: true,
            allowSearchFilter: true
        };
        this.CCNRecords = [{
                Month: 'June',
                CCN_No: "1",
                ChangeClassification: "Major",
                CreatedBy: "Sandeep",
                CreatedDate: "12 Jun 2020",
                DepartmentName: "Quality Assurance",
                DueDate: "15 Jan 2021",
                CompletedDate: "20 May 2021",
                TAT: 24,
            },
            {
                Month: 'June',
                CCN_No: "2",
                ChangeClassification: "Minor",
                CreatedBy: "Mandeep",
                CreatedDate: "12 Jan 2020",
                DepartmentName: "Quality Control",
                DueDate: "15 Jun 2021",
                CompletedDate: "20 Apr 2021",
                TAT: 20,
            },
            {
                Month: 'June',
                CCN_No: "5",
                ChangeClassification: "Major",
                CreatedBy: "Mandeep",
                CreatedDate: "01 Jan 2020",
                DepartmentName: "Quality Control",
                DueDate: "15 Jun 2021",
                CompletedDate: "20 Apr 2021",
                TAT: 20,
            },
            {
                Month: 'July',
                CCN_No: "3",
                ChangeClassification: "Major",
                CreatedBy: "Jaideep",
                CreatedDate: "15 Jun 2020",
                DepartmentName: "Quality Assurance",
                DueDate: "15 Apr 2021",
                CompletedDate: "20 Dec 2021",
                TAT: 14,
            },
            {
                Month: 'July',
                CCN_No: "4",
                ChangeClassification: "Minor",
                CreatedBy: "Singh",
                CreatedDate: "10 Jun 2021",
                DepartmentName: "Quality Assurance",
                DueDate: "15 Jan 2022",
                CompletedDate: "20 May 2021",
                TAT: 4,
            }];
    }
    ngAfterViewInit() {
        let flatpickrToDateElem = document.getElementById('toDate');
        let inptElem = flatpickrToDateElem.getElementsByTagName('input')[0];
        inptElem.setAttribute("disabled", "true");
    }
    setMetricDropdown() {
        this.metricSelectionDropdownSettings = {
            singleSelection: true,
            idField: 'metricId',
            textField: 'metricName',
            closeDropDownOnSelection: true,
        };
        this.metricSelectionList = [
            { metricId: MetricsListValues.CountMetricID, metricName: MetricsListValues.countMetricName },
            { metricId: MetricsListValues.TATMetricID, metricName: MetricsListValues.tatMetricName }
        ];
        this.selectedMetric = [{
                metricId: MetricsListValues.CountMetricID, metricName: MetricsListValues.countMetricName
            }];
    }
    onMetricSelectionChange(event) {
        this.currentMetricId = event.metricId;
        this.getDeptPerformanceDataFromFilters();
    }
    getLabelsSettings(backgroundColor) {
        return {
            visible: true,
            backgroundColor: backgroundColor,
            customizeText: this.customizeText
        };
    }
    customizeText(arg) {
        return arg.valueText;
    }
    setChangeCategoryAndClassificationDropdown() {
        this.changeCategoryDropdownSettings = {
            singleSelection: false,
            idField: 'changeCategoryID',
            textField: 'changeCategoryName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: false,
            closeDropDownOnSelection: true
        };
        this.changeClassificationDropdownSettings = {
            singleSelection: true,
            idField: 'changeClassificationID',
            textField: 'changeClassificationName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: false,
            closeDropDownOnSelection: true
        };
        this.changeCategoriesList = [
            { changeCategoryID: CCNCategoryStatusCodes.Permanent, changeCategoryName: ChangeCategoriesListValues.permanent },
            { changeCategoryID: CCNCategoryStatusCodes.Temporary, changeCategoryName: ChangeCategoriesListValues.temporary }
        ];
        this.selectedChangeCategory = this.changeCategoriesList;
        this.changeClassificationList = [
            { changeClassificationID: CCNClassificationStatusCodes.Major, changeClassificationName: ChangeClassificationListValues.major },
            { changeClassificationID: CCNClassificationStatusCodes.Minor, changeClassificationName: ChangeClassificationListValues.minor }
        ];
        this.selectedChangeClassification = this.changeClassificationList;
        console.log("change classific", this.selectedChangeClassification);
    }
    setIncidentCategoryAndClassificationDropdown() {
        this.incidentCategoryDropdownSettings = {
            singleSelection: false,
            idField: 'TypeofCategoryID',
            textField: 'TypeofIncident',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            closeDropDownOnSelection: false
        };
        this.serverService.getIncidentCategoryList().subscribe((res) => {
            this.incidentCategoriesList = res.data;
            this.selectedIncidentCategory = this.incidentCategoriesList;
        }, (err) => {
            console.error(err);
        });
        this.incidentClassificationDropdownSettings = {
            singleSelection: false,
            idField: 'incidentClassificationID',
            textField: 'incidentClassificationName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: false,
            closeDropDownOnSelection: false
        };
        this.incidentClassificationList = [
            {
                "incidentClassificationID": IncidentClassificationStatusCodes.QualityImpacting,
                "incidentClassificationName": 'Quality Impacting'
            },
            {
                "incidentClassificationID": IncidentClassificationStatusCodes.QualityNonImpacting,
                "incidentClassificationName": 'Quality Non Impacting'
            }
        ];
        this.selectedIncidentClassification = this.incidentClassificationList;
    }
    loadInitialDeptPerformanceData() {
        this.initialVisitCount = 0;
        this.getDepartmentsList(); // this method also contains getQualityEventsList()
    }
    //fetches departmentsList from DB and sorts it alphabetically
    getDepartmentsList() {
        // let roleAndEventID={
        //   roleID:2,
        //   eventId:3
        // }
        this.serverService.getAllDepartmentsList().subscribe((resultantDepartments) => {
            this.departmentsList = resultantDepartments.DeptFevent_list;
            this.departmentsList = this.departmentsList.sort(function (x, y) {
                if (x.DepartmentName < y.DepartmentName)
                    return -1;
                if (x.DepartmentName > y.DepartmentName)
                    return 1;
                return 0;
            });
            this.selectedDepartments = [{
                    Department_ID: this.departmentsList[0].Department_ID,
                    DepartmentName: this.departmentsList[0].DepartmentName
                }];
            // The below method also contains getDeptPerformanceDataFromFilters() 
            //method which fetches data from db with given filters
            this.getQualityEventsList();
        }, (err) => {
            console.log(err);
        });
    }
    //fetches qualityEventsList from DB and sorts it alphabetically
    //also sets Initial Date values and calls getDeptPerformanceDataFromFilters Method sequentially.
    getQualityEventsList() {
        this.serverService.getQualityEventsList().subscribe((resultantQualityEvents) => {
            this.qualityEventsList = resultantQualityEvents.sort(function (x, y) {
                if (x.QualityEvent_TypeName < y.QualityEvent_TypeName)
                    return -1;
                if (x.QualityEvent_TypeName > y.QualityEvent_TypeName)
                    return 1;
                return 0;
            });
            this.selectedQualityEvents = [{
                    QualityEvent_TypeID: this.qualityEventsList[0].QualityEvent_TypeID,
                    QualityEvent_TypeName: this.qualityEventsList[0].QualityEvent_TypeName
                }];
            this.setInitialDateValues();
            this.getDeptPerformanceDataFromFilters();
        }, (err) => {
            console.log(err);
        });
    }
    setInitialDateValues() {
        let sampleDay = parseInt(moment().format('DD'));
        let prevSampleDay = sampleDay - 1;
        let firstOfMonth = 0 + (sampleDay - prevSampleDay).toString();
        this.fromDateValue = moment().date(parseInt(firstOfMonth)).subtract(1, 'years').format('MMM YYYY');
        let fromDateISOString = moment().date(parseInt(firstOfMonth)).subtract(1, 'years').toISOString();
        this.selectedFromDate = moment().date(parseInt(firstOfMonth)).subtract(1, 'years').format('DD-MMM-YYYY');
        this.setToDate(fromDateISOString);
    }
    setToDate(fromDateISOString) {
        var tempToDate = new Date(fromDateISOString);
        this.toDateValue = moment(tempToDate.toISOString()).add(1, 'years').subtract(1, 'days').format('MMM YYYY');
        let mon = this.toDateValue.substr(0, 3);
        if (mon === moment().month(11).format('MMM')) {
            //adding 1 year to fromdate -> subtracting 1 day-> results in last day of previous month -> subtracting 1 year if the todate month is dec
            this.selectedToDate = moment(tempToDate).add(1, 'years').subtract(1, 'days').subtract(1, 'years').format('DD-MMM-YYYY');
        }
        else {
            this.selectedToDate = moment(tempToDate).add(1, 'years').subtract(1, 'days').format('DD-MMM-YYYY');
        }
    }
    getDeptNameFromDeptID(deptId) {
        for (let i = 0; i < this.departmentsList.length; i++) {
            if (this.departmentsList[i].Department_ID === deptId) {
                return this.departmentsList[i].DepartmentName;
            }
        }
    }
    getQualityEventNameFromQualityEventId(qualityEventID) {
        for (let i = 0; i < this.qualityEventsList.length; i++) {
            if (this.qualityEventsList[i].QualityEvent_TypeID === qualityEventID) {
                return this.qualityEventsList[i].QualityEvent_TypeName;
            }
        }
    }
    setFromAndToDates() {
        let changedFromDate = this.filtersForm.get('fromDate').value[0];
        let fromDateISOString = this.getRequiredDateFormat(changedFromDate);
        this.setToDate(fromDateISOString);
    }
    // getSelectedDeptIDs(){
    //   this.selectedDeptIds=[];
    //   for(let i=0;i<this.selectedDepartments.length;i++){
    //     this.selectedDeptIds.push(this.selectedDepartments[i].Department_ID);
    //   }
    // }
    // getSelectedQualityEventIDs(){
    //   this.selectedQualityEventIds=[];
    //   for(let i=0;i<this.selectedQualityEvents.length;i++){
    //     this.selectedQualityEventIds.push(this.selectedQualityEvents[i].QualityEvent_TypeID);
    //   }
    // }
    //Extracting Department IDs from departmentsList Object array
    getDepartmentListIds() {
        let departmentIds = [];
        for (let i = 0; i < this.departmentsList.length; i++) {
            departmentIds.push(this.departmentsList[i].Department_ID);
        }
        return departmentIds;
    }
    //Extracting Quality Event IDs from qualityEventsList Object array
    getQualityEventIds() {
        let qualityEventIds = [];
        for (let i = 0; i < this.qualityEventsList.length; i++) {
            qualityEventIds.push(this.qualityEventsList[i].QualityEvent_TypeID);
        }
        return qualityEventIds;
    }
    onDeptPerformanceDataChartFilterChange(event) {
        this.initialVisitCount++;
        if (this.initialVisitCount > 2) { // to avoid recurring triggering of change event from datepickers on initial load
            this.setFromAndToDates();
            this.getDeptPerformanceDataFromFilters();
        }
        else {
            return;
        }
    }
    getDeptPerformanceDataFromFilters() {
        this.deptPerformanceDataFilters = {
            DeptID: this.selectedDepartments[0].Department_ID,
            QualityEventType: this.selectedQualityEvents[0].QualityEvent_TypeID,
            CCNClassification: this.selectedChangeClassification[0].changeClassificationID,
            CCNCategory: (this.selectedChangeCategory.length === 0 || this.selectedChangeCategory.length > 1) ? CCNCategoryStatusCodes.Both : this.selectedChangeCategory[0].changeCategoryID,
            IncidentClassification: (this.selectedIncidentClassification.length === 0 || this.selectedIncidentClassification.length > 1) ? IncidentClassificationStatusCodes.All : this.selectedIncidentClassification[0].incidentClassificationID,
            IncidentCategory: 0,
            FromDate: this.selectedFromDate,
            ToDate: this.selectedToDate
        };
        if (this.currentMetricId === MetricsListValues.CountMetricID) {
            this.isListViewShown = false;
            this.getCountMetricDataOfSelectedFilters();
        }
        else {
            this.isListViewShown = true;
            this.getTATMetricDataOfSelectedFilters();
        }
    }
    getCountMetricDataOfSelectedFilters() {
        this.serverService.getDeptPerformanceDataOfCountMetric(this.deptPerformanceDataFilters).subscribe((res) => {
            this.deptPerformanceData = res.Performance_List;
            this.setMonthsInASequence();
            this.setDeptAndOrgAverages();
            this.setUpperAndLowerLimits();
        }, (err) => {
            console.error(err);
        });
    }
    setMonthsInASequence() {
        let fromDateMonth = this.deptPerformanceDataFilters.FromDate.substr(3, 3);
        let fromDateYear = this.deptPerformanceDataFilters.FromDate.substr(7, 4);
        let toDateYear = this.deptPerformanceDataFilters.ToDate.substr(7, 4);
        console.log("fromdate year", fromDateYear);
        let seqArrayWithStartingMonth = [];
        let i = 0;
        let startingMonthIndexNumber;
        let otherHalfseqArray = [];
        for (i = 0; i < this.deptPerformanceData.length; i++) {
            if (this.deptPerformanceData[i] ? .Monthname === fromDateMonth : ) {
                startingMonthIndexNumber = i;
                seqArrayWithStartingMonth.push(this.deptPerformanceData[i]);
                this.deptPerformanceData[i].MonthnameAndYear = this.deptPerformanceData[i].Monthname + ' - ' + fromDateYear;
                for (i = startingMonthIndexNumber + 1; i < this.deptPerformanceData.length; i++) {
                    seqArrayWithStartingMonth.push(this.deptPerformanceData[i]);
                    this.deptPerformanceData[i].MonthnameAndYear = this.deptPerformanceData[i].Monthname + ' - ' + fromDateYear;
                }
            }
            else {
                otherHalfseqArray.push(this.deptPerformanceData[i]);
                this.deptPerformanceData[i].MonthnameAndYear = this.deptPerformanceData[i].Monthname + ' - ' + toDateYear;
            }
        }
        let modifiedDeptPerformancedata = [...seqArrayWithStartingMonth, ...otherHalfseqArray];
        this.deptPerformanceData = modifiedDeptPerformancedata;
    }
    setDeptAndOrgAverages() {
        this.currentDeptAvg = this.deptPerformanceData[0].DeptAvg;
        this.currentOrgAvg = this.deptPerformanceData[0].OrgAvg;
    }
    setUpperAndLowerLimits() {
        let deptAvg = this.deptPerformanceData[0].DeptAvg;
        let sum = 0;
        for (let i = 0; i < this.deptPerformanceData.length; i++) {
            sum = sum + (Math.pow((this.deptPerformanceData[i].Count - deptAvg), 2));
        }
        let sd = Math.sqrt((sum / this.deptPerformanceData.length));
        this.upperLimit = deptAvg + sd;
        this.lowerLimit = (deptAvg - sd) > 0 ? (deptAvg - sd) : 0;
    }
    getTATMetricDataOfSelectedFilters() {
        this.serverService.getDeptPerformanceDataOfTATMetric(this.deptPerformanceDataFilters).subscribe((res) => {
            console.log("tat metric data", res);
            this.deptPerformanceData = res.Performance_List;
            this.setMonthsInASequence();
            this.setDeptAndOrgAverages();
            this.setUpperAndLowerLimits();
        }, (err) => {
            console.error(err);
        });
    }
    onSelectAllChangeCategories(items) {
        this.selectedChangeCategory = items;
        this.getDeptPerformanceDataFromFilters();
    }
    onSelectAllChangeClassifications(items) {
        this.selectedChangeClassification = items;
        this.getDeptPerformanceDataFromFilters();
    }
    onSelectAllIncidentCategories(items) {
        this.selectedIncidentCategory = items;
        this.getDeptPerformanceDataFromFilters();
    }
    onSelectAllIncidentClassification(items) {
        this.selectedIncidentClassification = items;
        this.getDeptPerformanceDataFromFilters();
    }
    onSelectAllQualityEvents(event) { }
    onSelectAllDepartments(event) { }
    onShowListView() {
        // this.getOverallListOfAppliedFilters();
        if (this.selectedQualityEvents[0].QualityEvent_TypeID === QualityEventIDs.ChangeControlNote) {
            this.getOverallCCNListOfAppliedFilters();
        }
        else if (this.selectedQualityEvents[0].QualityEvent_TypeID === QualityEventIDs.IncidentReport) {
            this.getOverallIncidentListOfAppliedFilters();
        }
        else if (this.selectedQualityEvents[0].QualityEvent_TypeID === QualityEventIDs.Errata) {
            this.getOverallErrataListOfAppliedFilters();
        }
        else {
            this.getOverallNTFListOfAppliedFilters();
        }
    }
    getUpperAndLowerLimitsForListView(qualityEventRecords) {
        let sumOfAllTATs = 0;
        let earlierMonth = '';
        let count = 0;
        for (let i = 0; i < qualityEventRecords.length; i++) {
            qualityEventRecords[i].monthlyTAT = 0;
            qualityEventRecords[i].totalRecordsInMonth = 0;
        }
        if (qualityEventRecords.length !== 0) {
            for (let i = 0; i <= qualityEventRecords.length; i++) {
                //in the below code - calculation is being performed on the previous record, 
                //hence to perform on all records the index is made equal to the length of array
                if (i === 0) {
                    earlierMonth = qualityEventRecords[i].Month;
                    count = count + 1;
                    qualityEventRecords[i].totalRecordsInMonth = count;
                }
                else if (i === 1) {
                    if (qualityEventRecords[i] ? .Month === earlierMonth : ) {
                        qualityEventRecords[i].monthlyTAT = qualityEventRecords[i - 1].TAT + qualityEventRecords[i].TAT;
                        // count++;
                        qualityEventRecords[i].totalRecordsInMonth = qualityEventRecords[i - 1].totalRecordsInMonth + 1;
                        earlierMonth = qualityEventRecords[i].Month;
                    }
                    else {
                        sumOfAllTATs = sumOfAllTATs + qualityEventRecords[i - 1].TAT;
                        earlierMonth = qualityEventRecords[i] ? .Month : ;
                        qualityEventRecords[i - 1].totalRecordsInMonth = 1;
                    }
                }
                else {
                    if (i !== qualityEventRecords.length) {
                        if (qualityEventRecords[i] ? .Month === earlierMonth : ) {
                            if (qualityEventRecords[i - 1].monthlyTAT === 0) {
                                qualityEventRecords[i].monthlyTAT = qualityEventRecords[i - 1].TAT + qualityEventRecords[i].TAT;
                                // count++;
                                qualityEventRecords[i - 1].totalRecordsInMonth = 1;
                                qualityEventRecords[i].totalRecordsInMonth = qualityEventRecords[i - 1].totalRecordsInMonth + 1;
                                earlierMonth = qualityEventRecords[i].Month;
                            }
                            else {
                                qualityEventRecords[i].monthlyTAT = qualityEventRecords[i - 1].monthlyTAT + qualityEventRecords[i].TAT;
                                // count++;
                                qualityEventRecords[i].totalRecordsInMonth = qualityEventRecords[i - 1].totalRecordsInMonth + 1;
                                earlierMonth = qualityEventRecords[i].Month;
                            }
                        }
                        else {
                            if (qualityEventRecords[i - 1].monthlyTAT !== 0) {
                                sumOfAllTATs = sumOfAllTATs + (qualityEventRecords[i - 1].monthlyTAT / qualityEventRecords[i - 1].totalRecordsInMonth);
                                earlierMonth = qualityEventRecords[i].Month;
                                qualityEventRecords[i].totalRecordsInMonth = 1;
                            }
                            else {
                                sumOfAllTATs = sumOfAllTATs + qualityEventRecords[i - 1].TAT;
                                earlierMonth = qualityEventRecords[i] ? .Month : ;
                                qualityEventRecords[i].totalRecordsInMonth = 1;
                            }
                        }
                    }
                    else {
                        sumOfAllTATs = sumOfAllTATs + qualityEventRecords[i - 1].TAT;
                    }
                }
            }
            console.log("qualityEventRecords", qualityEventRecords);
            console.log("sum of all tats", sumOfAllTATs);
            let meanOfAllTATs = sumOfAllTATs / 12;
            let sum = 0;
            for (let i = 0; i < qualityEventRecords.length; i++) {
                sum = sum + (Math.pow((qualityEventRecords[i].TAT - meanOfAllTATs), 2));
            }
            let sd = Math.sqrt((sum / qualityEventRecords.length));
            let upperLimitForListView = meanOfAllTATs + sd;
            let lowerLimitForListView = (meanOfAllTATs - sd) > 0 ? (meanOfAllTATs - sd) : 0;
            return { upperLimitForListView: upperLimitForListView, lowerLimitForListView: lowerLimitForListView };
        }
        else {
            return { upperLimitForListView: 0, lowerLimitForListView: 0 };
        }
    }
    getOverallCCNListOfAppliedFilters() {
        this.ccnListFiltersForDeptPerformance = {
            DeptID: this.selectedDepartments[0].Department_ID,
            CCNClassification: this.selectedChangeClassification[0].changeClassificationID,
            CCNCategory: (this.selectedChangeCategory.length === 0 || this.selectedChangeCategory.length > 1) ? CCNCategoryStatusCodes.Both : this.selectedChangeCategory[0].changeCategoryID,
            FromDate: this.selectedFromDate,
            ToDate: this.selectedToDate
        };
        console.log("list filters for dept per", this.ccnListFiltersForDeptPerformance);
        this.serverService.getCCNListForDeptPerformance(this.ccnListFiltersForDeptPerformance).subscribe((res) => {
            console.log("ccn list for dept preformance", res.Cchart_List);
            this.CCNRecords = res.Cchart_List;
            for (let i = 0; i < this.CCNRecords.length; i++) {
                this.CCNRecords[i].TAT = parseInt(res.Cchart_List[i].TAT);
            }
            let limitsForListView = this.getUpperAndLowerLimitsForListView(this.CCNRecords);
            console.log("limits for list view", limitsForListView);
            const initialState = {
                currentDrillDownHeaderText: "Department Performance - CCN" + ' - ' + 'Overall List',
                ccnRecords: this.CCNRecords,
                qualityEvent: this.selectedQualityEvents[0].QualityEvent_TypeName,
                fileName: ' - ' + 'Overall List' + ' (' + this.ccnListFiltersForDeptPerformance.FromDate + ' to ' + this.ccnListFiltersForDeptPerformance.ToDate + ')',
                limits: limitsForListView,
                dates: { fromDate: this.selectedFromDate, toDate: this.selectedToDate }
            };
            this.bsModalRef = this.modalService.show(ListViewDeptPerformanceComponent, { initialState, class: 'gray modal-xl', backdrop: 'static', });
        }, (err) => {
            console.error(err);
        });
    }
    getOverallIncidentListOfAppliedFilters() {
        this.incidentListFilters = {
            DeptID: this.selectedDepartments[0].Department_ID,
            IncidentClassification: (this.selectedIncidentClassification.length === 0 || this.selectedIncidentClassification.length > 1) ? IncidentClassificationStatusCodes.All : this.selectedIncidentClassification[0].incidentClassificationID,
            IncidentCategory: 0,
            FromDate: this.selectedFromDate,
            ToDate: this.selectedToDate
        };
        this.serverService.getIncidentListForDeptPerformance(this.incidentListFilters).subscribe((res) => {
            console.log("Incident records for dept perf", res);
            this.incidentRecords = res.Pchart_List;
            for (let i = 0; i < this.incidentRecords.length; i++) {
                this.incidentRecords[i].TAT = parseInt(res.Pchart_List[i].TAT);
            }
            let limitsForListView = this.getUpperAndLowerLimitsForListView(this.incidentRecords);
            const initialState = {
                currentDrillDownHeaderText: "Department Performance - IR" + ' - ' + 'Overall List',
                incidentRecords: this.incidentRecords,
                qualityEvent: this.getQualityEventNameFromQualityEventId(this.selectedQualityEvents[0].QualityEvent_TypeID),
                fileName: ' - ' + 'Overall List' + ' (' + this.incidentListFilters.FromDate + ' to ' + this.incidentListFilters.ToDate + ')',
                limits: limitsForListView,
                dates: { fromDate: this.selectedFromDate, toDate: this.selectedToDate }
            };
            this.bsModalRef = this.modalService.show(ListViewDeptPerformanceComponent, { initialState, class: 'gray modal-xl', backdrop: 'static', });
        }, (err) => {
            console.error(err);
        });
    }
    getOverallErrataListOfAppliedFilters() {
        this.errataListFilters = {
            DeptID: this.selectedDepartments[0].Department_ID,
            FromDate: this.selectedFromDate,
            ToDate: this.selectedToDate
        };
        this.serverService.getErrataListForDeptPerformance(this.errataListFilters).subscribe((res) => {
            console.log("errata records fr dept perf", res);
            this.errrataRecords = res.Echart_List;
            for (let i = 0; i < this.errrataRecords.length; i++) {
                this.errrataRecords[i].TAT = parseInt(res.Echart_List[i].TAT);
            }
            let limitsForListView = this.getUpperAndLowerLimitsForListView(this.errrataRecords);
            const initialState = {
                currentDrillDownHeaderText: "Department Performance - Errata" + ' - ' + 'Overall List',
                errataRecords: this.errrataRecords,
                qualityEvent: this.getQualityEventNameFromQualityEventId(this.selectedQualityEvents[0].QualityEvent_TypeID),
                fileName: ' - ' + 'Overall List' + ' (' + this.errataListFilters.FromDate + ' to ' + this.errataListFilters.ToDate + ')',
                limits: limitsForListView,
                dates: { fromDate: this.selectedFromDate, toDate: this.selectedToDate }
            };
            this.bsModalRef = this.modalService.show(ListViewDeptPerformanceComponent, { initialState, class: 'gray modal-xl', backdrop: 'static', });
        }, (err) => {
            console.error(err);
        });
    }
    getOverallNTFListOfAppliedFilters() {
        this.noteToFileListFilters = {
            DeptID: this.selectedDepartments[0].Department_ID,
            FromDate: this.selectedFromDate,
            ToDate: this.selectedToDate
        };
        this.serverService.getNTFListForDeptPerformance(this.noteToFileListFilters).subscribe((res) => {
            console.log("NTF records for dept perf", res);
            this.noteToFileRecords = res.Nchart_List;
            for (let i = 0; i < this.noteToFileRecords.length; i++) {
                this.noteToFileRecords[i].TAT = parseInt(res.Nchart_List[i].TAT);
            }
            let limitsForListView = this.getUpperAndLowerLimitsForListView(this.noteToFileRecords);
            const initialState = {
                currentDrillDownHeaderText: "Department Performance - NTF" + ' - ' + 'Overall List',
                noteToFileRecords: this.noteToFileRecords,
                qualityEvent: this.getQualityEventNameFromQualityEventId(this.selectedQualityEvents[0].QualityEvent_TypeID),
                fileName: ' - ' + 'Overall List' + ' (' + this.noteToFileListFilters.FromDate + ' to ' + this.noteToFileListFilters.ToDate + ')',
                limits: limitsForListView,
                dates: { fromDate: this.selectedFromDate, toDate: this.selectedToDate }
            };
            this.bsModalRef = this.modalService.show(ListViewDeptPerformanceComponent, { initialState, class: 'gray modal-xl', backdrop: 'static', });
        }, (err) => {
            console.error(err);
        });
    }
};
DepartmentPerformanceChartComponent = tslib_1.__decorate([
    Component({
        selector: 'app-department-performance-chart',
        templateUrl: './department-performance-chart.component.html',
        styleUrls: ['./department-performance-chart.component.scss']
    })
], DepartmentPerformanceChartComponent);
export { DepartmentPerformanceChartComponent };
//# sourceMappingURL=department-performance-chart.component.js.map