import * as tslib_1 from "tslib";
import { TestBed } from '@angular/core/testing';
import { DepartmentPerformanceChartComponent } from './department-performance-chart.component';
describe('DepartmentPerformanceChartComponent', () => {
    let component;
    let fixture;
    beforeEach(() => tslib_1.__awaiter(this, void 0, void 0, function* () {
        yield TestBed.configureTestingModule({
            declarations: [DepartmentPerformanceChartComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(DepartmentPerformanceChartComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=department-performance-chart.component.spec.js.map