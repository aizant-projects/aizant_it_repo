import * as tslib_1 from "tslib";
import { TestBed } from '@angular/core/testing';
import { ManagementLevelChartComponent } from './management-level-chart.component';
describe('ManagementLevelChartComponent', () => {
    let component;
    let fixture;
    beforeEach(() => tslib_1.__awaiter(this, void 0, void 0, function* () {
        yield TestBed.configureTestingModule({
            declarations: [ManagementLevelChartComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(ManagementLevelChartComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=management-level-chart.component.spec.js.map