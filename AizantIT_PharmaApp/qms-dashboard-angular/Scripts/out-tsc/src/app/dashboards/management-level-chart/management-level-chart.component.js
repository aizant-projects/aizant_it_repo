import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { CCNCategoryStatusCodes, CCNClassificationStatusCodes, ChangeCategoriesListValues, ChangeClassificationListValues, IncidentClassificationListValues, IncidentClassificationStatusCodes, QualityEventIDs, QualityEventIds, QualityEventStatus, ShowTypeList, TrimmedDepartmentNames } from '../../models/dashboards';
import * as moment from 'moment';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { ListViewComponent } from '../list-view/list-view.component';
let ManagementLevelChartComponent = class ManagementLevelChartComponent {
    constructor(dashboardService, fb, serverService, modalService, changeDetection, renderer) {
        this.dashboardService = dashboardService;
        this.fb = fb;
        this.serverService = serverService;
        this.modalService = modalService;
        this.changeDetection = changeDetection;
        this.renderer = renderer;
        this.events = [];
        this.dtOptions = {};
        this.dtTrigger = new Subject();
        this.statusChartDataSource = [];
        this.departmentsList = [];
        this.selectedDepartments = [];
        this.departmentDropdownSettings = {};
        this.eventsDropdownSettings = {};
        this.qualityEventsList = [];
        this.selectedQualityEvents = [];
        this.changeCategoriesList = [];
        this.selectedChangeCategory = [];
        this.changeCategoryDropdownSettings = {};
        this.changeClassificationList = [];
        this.selectedChangeClassification = [];
        this.changeClassificationDropdownSettings = {};
        this.selectedCCNClassificationIDs = [];
        this.incidentCategoriesList = [];
        this.selectedIncidentCategory = [];
        this.incidentCategoryDropdownSettings = {};
        this.incidentClassificationList = [];
        this.selectedIncidentClassification = [];
        this.incidentClassificationDropdownSettings = {};
        this.fromDateOptions = {};
        //   fromDateOptions: Partial<BaseOptions> = {
        //   dateFormat:"d-M-Y",
        //   onClose: this.closeFromDate.bind(this)
        // };
        this.toDateOptions = {
        // dateFormat:"d-M-Y",
        //     // onOpen:this.openTodate.bind(this)
        };
        this.subtitleForStackedBarChart = '';
        this.currentFileName = '';
        this.currentQualityEventId = QualityEventIds;
        this.selectedDeptIds = [];
        this.overdueData = [];
        this.selectedQualityEventName = '';
        this.CCNRecords = [];
        this.incidentRecords = [];
        this.errrataRecords = [];
        this.noteToFileRecords = [];
        //returns department ID from Dept name
        this.getDeptIdFromDeptName = (deptName) => {
            for (let i = 0; i < this.departmentsList.length; i++) {
                if (this.departmentsList[i].DepartmentName === deptName) {
                    return this.departmentsList[i].Department_ID;
                }
            }
        };
        //returns qualityEvent ID from qualityEvent name
        this.getQualityEventIdFromQualityEventName = (qualityEventName) => {
            for (let i = 0; i < this.qualityEventsList.length; i++) {
                if (this.qualityEventsList[i].QualityEvent_TypeName === qualityEventName) {
                    return this.qualityEventsList[i].QualityEvent_TypeID;
                }
            }
        };
        //converts fetched date into DB compatible date format
        this.getRequiredDateFormat = (selectedDate) => {
            var tempDate = new Date(selectedDate);
            let sampleFromDate = moment(tempDate.toISOString());
            return sampleFromDate.format('DD-MMM-YYYY');
        };
        this.customizeTooltipForOverdue = (info) => {
            return {
                html: "<div><div class='tooltip-header'>" +
                    info.argumentText + "</div>" +
                    "<div class='tooltip-body'><div class='series-name'>" +
                    "<span class='top-series-name'>" + info.points[0].seriesName + "</span>" +
                    ": </div><div class='value-text'>" +
                    "<span class='top-series-value'>" + info.points[0].valueText + "</span>" +
                    "</div><div class='series-name'>" +
                    "<span class='bottom-series-name'>" + info.points[1].seriesName + "</span>" +
                    ": </div><div class='value-text'>" +
                    "<span class='bottom-series-value'>" + info.points[1].valueText + "</span>" +
                    "% </div></div></div>"
            };
        };
        this.customizeLabelText = (info) => {
            return info.valueText + "%";
        };
        this.overallStatuslevel = 0;
        this.setChangeCategoryAndClassificationDropdown();
        this.setIncidentCategoryAndClassificationDropdown();
        this.filtersForm = this.fb.group({
            fromDate: [''],
            toDate: ['']
        });
        this.getDepartmentsList();
        this.getQualityEventsList();
        this.fromDateOptions.dateFormat = 'd-M-Y';
        this.fromDateOptions.defaultDate = moment().subtract(1, 'years').format('DD-MMM-YYYY');
        this.toDateOptions.dateFormat = 'd-M-Y';
        this.toDateOptions.defaultDate = moment().format('DD-MMM-YYYY');
    }
    ngOnInit() {
        this.departmentDropdownSettings = {
            singleSelection: false,
            idField: 'Department_ID',
            textField: 'DepartmentName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
        };
        this.eventsDropdownSettings = {
            singleSelection: false,
            idField: 'QualityEvent_TypeID',
            textField: 'QualityEvent_TypeName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
        };
        // let fromDateElem = document.getElementById('fromDatePickrElement');
        // this.fromDateOptionsNew=flatpickr(fromDateElem,{
        //   dateFormat:'d-M-Y',
        //   defaultDate : moment().subtract(1,'years').format('DD-MMM-YYYY'),
        //   onClose: this.closeFromDate.bind(this)
        // });
        // let toDateElem = document.getElementById('toDatePickrElement');
        // this.toDateOptionsNew=flatpickr(toDateElem,{
        //   dateFormat:"d-M-Y",
        //   defaultDate: moment().format('DD-MMM-YYYY')
        // });
        // this.fromDateOptions={
        //     dateFormat:'d-M-Y',
        //   defaultDate : moment().subtract(1,'years').format('DD-MMM-YYYY'),
        //   onClose: this.closeFromDate.bind(this,this.toDateOptionsNew)
        // }
        this.initialFromDate = moment().subtract(1, 'years').format('DD-MMM-YYYY');
        this.initialToDate = moment().format('DD-MMM-YYYY');
        this.allDepartmentQualityEventsFilters = {
            DeptID: this.getDepartmentListIds(),
            QualityEventType: this.getQualityEventIds(),
            FromDate: this.initialFromDate,
            ToDate: this.initialToDate
        };
        this.previousDataFiltersForStackedBarChart = this.allDepartmentQualityEventsFilters;
        this.getDepartmentsQualityEventsDataFromFilters(this.allDepartmentQualityEventsFilters);
        this.subtitleForStackedBarChart = '( ' + this.allDepartmentQualityEventsFilters.FromDate + ' to ' + this.allDepartmentQualityEventsFilters.ToDate + ' )';
    }
    ngAfterViewInit() {
        let flatpickrInput = document.getElementsByClassName('ng2-flatpickr-input');
        for (let i = 0; i < flatpickrInput.length; i++) {
            this.renderer.addClass(flatpickrInput[i], 'form-control');
        }
    }
    setChangeCategoryAndClassificationDropdown() {
        this.changeCategoryDropdownSettings = {
            singleSelection: false,
            idField: 'changeCategoryID',
            textField: 'changeCategoryName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: false,
        };
        this.changeClassificationDropdownSettings = {
            singleSelection: false,
            idField: 'changeClassificationID',
            textField: 'changeClassificationName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: false,
        };
        this.changeCategoriesList = [
            { changeCategoryID: CCNCategoryStatusCodes.Permanent, changeCategoryName: ChangeCategoriesListValues.permanent },
            { changeCategoryID: CCNCategoryStatusCodes.Temporary, changeCategoryName: ChangeCategoriesListValues.temporary }
        ];
        this.selectedChangeCategory = this.changeCategoriesList;
        this.changeClassificationList = [
            { changeClassificationID: CCNClassificationStatusCodes.Major, changeClassificationName: ChangeClassificationListValues.major },
            { changeClassificationID: CCNClassificationStatusCodes.Minor, changeClassificationName: ChangeClassificationListValues.minor },
            { changeClassificationID: CCNClassificationStatusCodes.Unclassified, changeClassificationName: ChangeClassificationListValues.unclassified }
        ];
        this.selectedChangeClassification = this.changeClassificationList;
    }
    setIncidentCategoryAndClassificationDropdown() {
        this.incidentCategoryDropdownSettings = {
            singleSelection: false,
            idField: 'TypeofCategoryID',
            textField: 'TypeofIncident',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            closeDropDownOnSelection: false
        };
        this.serverService.getIncidentCategoryList().subscribe((res) => {
            this.incidentCategoriesList = res.data;
            this.selectedIncidentCategory = this.incidentCategoriesList;
        }, (err) => {
            console.error(err);
        });
        this.incidentClassificationDropdownSettings = {
            singleSelection: false,
            idField: 'incidentClassificationID',
            textField: 'incidentClassificationName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true
        };
        this.incidentClassificationList = [
            {
                "incidentClassificationID": IncidentClassificationStatusCodes.QualityImpacting,
                "incidentClassificationName": IncidentClassificationListValues.qualityImpacting
            },
            {
                "incidentClassificationID": IncidentClassificationStatusCodes.QualityNonImpacting,
                "incidentClassificationName": IncidentClassificationListValues.qualityNonImpacting
            },
            {
                "incidentClassificationID": IncidentClassificationStatusCodes.Unclassified,
                "incidentClassificationName": IncidentClassificationListValues.unclassified
            }
        ];
        this.selectedIncidentClassification = this.incidentClassificationList;
    }
    getColorPaletteForStatusChart() {
        return this.dashboardService.statusChartColorPalette;
    }
    getColorOFQE(QualityEventNumber) {
        return this.dashboardService.getColor(QualityEventNumber);
    }
    onPreviousLevelClick() {
        if (this.overallStatuslevel === 1) {
            this.overallStatuslevel--;
            this.getDepartmentsQualityEventsDataFromFilters(this.previousDataFiltersForStackedBarChart);
            this.setSelectedDepartmentsValues(this.previousDataFiltersForStackedBarChart.DeptID);
            this.setSelectedQualityEventValues(this.previousDataFiltersForStackedBarChart.QualityEventType, 'qualityEventsChart');
            this.setSelectedDateValues(this.previousDataFiltersForStackedBarChart.FromDate, this.previousDataFiltersForStackedBarChart.ToDate);
        }
        else if (this.overallStatuslevel == 2) {
            this.overallStatuslevel--;
            this.currentDrillDownHeaderText = this.prevCurrentDrillDownHeaderText;
            this.getFilteredQualityEventStatusChart(this.previousDataFiltersForStatusChart);
            this.setSelectedDepartmentsValues(this.previousDataFiltersForStatusChart.DeptID);
            this.setSelectedQualityEventValues(this.previousDataFiltersForStatusChart.QualityEventType, 'statusChart');
            this.setSelectedDateValues(this.previousDataFiltersForStatusChart.FromDate, this.previousDataFiltersForStatusChart.ToDate);
            this.setChangeCategoryAndClassficationValues(this.previousDataFiltersForStatusChart.CCNCategory, this.previousDataFiltersForStatusChart.CCNClassification);
            this.setIncidentCategoryAndClassificationValues(this.previousDataFiltersForStatusChart.IncidentCategory, this.previousDataFiltersForStatusChart.IncidentClassification);
        }
    }
    setSelectedDepartmentsValues(deptIds) {
        this.selectedDepartments = [];
        for (let i = 0; i < deptIds.length; i++) {
            this.selectedDepartments.push({
                Department_ID: deptIds[i],
                DepartmentName: this.getDeptNameFromDeptID(deptIds[i])
            });
        }
    }
    setSelectedQualityEventValues(qualityEventIds, source) {
        this.selectedQualityEvents = [];
        if (source === 'qualityEventsChart') {
            for (let i = 0; i < qualityEventIds.length; i++) {
                this.selectedQualityEvents.push({
                    QualityEvent_TypeID: qualityEventIds[i],
                    QualityEvent_TypeName: this.getQualityEventNameFromQualityEventId(qualityEventIds[i])
                });
            }
        }
        else if (source === 'statusChart') {
            this.selectedQualityEvents.push({
                QualityEvent_TypeID: qualityEventIds,
                QualityEvent_TypeName: this.getQualityEventNameFromQualityEventId(qualityEventIds)
            });
        }
    }
    setSelectedDateValues(fromDate, toDate) {
        this.fromDateValue = fromDate;
        this.toDateValue = toDate;
    }
    setChangeCategoryAndClassficationValues(changeCategoryId, changeClassificationId) {
        this.selectedChangeCategory = [];
        this.selectedChangeClassification = [];
        if (changeCategoryId === 0) {
            for (let i = 0; i < this.changeCategoriesList.length; i++) {
                this.selectedChangeCategory.push({
                    changeCategoryID: this.changeCategoriesList[i].changeCategoryID,
                    changeCategoryName: this.changeCategoriesList[i].changeCategoryName
                });
            }
        }
        else if (changeCategoryId !== 0) {
            this.selectedChangeCategory.push({
                changeCategoryID: changeCategoryId,
                changeCategoryName: this.getChangeCatNameFromChangeCatID(changeCategoryId)
            });
        }
        if (changeClassificationId === 0) {
            for (let i = 0; i < this.changeClassificationList.length; i++) {
                this.selectedChangeClassification.push({
                    changeClassificationID: this.changeClassificationList[i].changeClassificationID,
                    changeClassificationName: this.changeClassificationList[i].changeClassificationName
                });
            }
        }
        else if (changeClassificationId !== 0) {
            this.selectedChangeClassification.push({
                changeClassificationID: changeClassificationId,
                changeClassificationName: this.getChangeClassificationNameFromChangeClassificationID(changeClassificationId)
            });
        }
    }
    setIncidentCategoryAndClassificationValues(incidentCategoryId, incidentClassificationId) {
        this.selectedIncidentCategory = [];
        this.selectedIncidentClassification = [];
        if (incidentCategoryId === 0) {
            for (let i = 0; i < this.incidentCategoriesList.length; i++) {
                this.selectedIncidentCategory.push({
                    TypeofCategoryID: this.incidentCategoriesList[i].TypeofCategoryID,
                    TypeofIncident: this.incidentCategoriesList[i].TypeofIncident
                });
            }
        }
        else if (incidentCategoryId !== 0) {
            this.selectedIncidentCategory.push({
                TypeofCategoryID: incidentCategoryId,
                TypeofIncident: this.getIncidentCatNameFromIncidentCatID(incidentCategoryId)
            });
        }
        if (incidentClassificationId === 0) {
            for (let i = 0; i < this.incidentClassificationList.length; i++) {
                this.selectedIncidentClassification.push({
                    incidentClassificationID: this.incidentClassificationList[i].incidentClassificationID,
                    incidentClassificationName: this.incidentClassificationList[i].incidentClassificationName
                });
            }
        }
        else if (incidentClassificationId !== 0) {
            this.selectedIncidentClassification.push({
                incidentClassificationID: incidentClassificationId,
                incidentClassificationName: this.getIncidentClassificationNameFromIncidentClassificationID(incidentClassificationId)
            });
        }
    }
    getChangeCatNameFromChangeCatID(changeCategoryId) {
        for (let i = 0; i < this.changeCategoriesList.length; i++) {
            if (this.changeCategoriesList[i].changeCategoryID === changeCategoryId) {
                return this.changeCategoriesList[i].changeCategoryName;
            }
        }
    }
    getChangeClassificationNameFromChangeClassificationID(changeClassificationId) {
        for (let i = 0; i < this.changeClassificationList.length; i++) {
            if (this.changeClassificationList[i].changeClassificationID === changeClassificationId) {
                return this.changeClassificationList[i].changeClassificationName;
            }
        }
    }
    getIncidentCatNameFromIncidentCatID(incidentCategoryId) {
        for (let i = 0; i < this.incidentCategoriesList.length; i++) {
            if (this.incidentCategoriesList[i].TypeofCategoryID === incidentCategoryId) {
                return this.incidentCategoriesList[i].TypeofIncident;
            }
        }
    }
    getIncidentClassificationNameFromIncidentClassificationID(incidentclassificationId) {
        for (let i = 0; i < this.incidentClassificationList.length; i++) {
            if (this.incidentClassificationList[i].incidentClassificationID === incidentclassificationId) {
                return this.incidentClassificationList[i].incidentClassificationName;
            }
        }
    }
    //Extracting Department IDs from departmentsList Object array
    getDepartmentListIds() {
        let departmentIds = [];
        for (let i = 0; i < this.departmentsList.length; i++) {
            departmentIds.push(this.departmentsList[i].Department_ID);
        }
        return departmentIds;
    }
    //Extracting Quality Event IDs from qualityEventsList Object array
    getQualityEventIds() {
        let qualityEventIds = [];
        for (let i = 0; i < this.qualityEventsList.length; i++) {
            qualityEventIds.push(this.qualityEventsList[i].QualityEvent_TypeID);
        }
        return qualityEventIds;
    }
    //fetches chart data based on filters passed
    getDepartmentsQualityEventsDataFromFilters(allDepartmentQualityEventsFilters) {
        this.serverService.getDepartmentsQualityEventsData(allDepartmentQualityEventsFilters).subscribe((res) => {
            this.dataSource = res.Mchart_bind;
            for (let i = 0; i < this.dataSource.length; i++) {
                this.dataSource[i].DepartmentName = TrimmedDepartmentNames[this.dataSource[i].DepartmentName];
            }
            this.dataSource = this.dataSource.sort(function (x, y) {
                if (x.DepartmentName < y.DepartmentName)
                    return -1;
                if (x.DepartmentName > y.DepartmentName)
                    return 1;
                return 0;
            });
            // this.dataSource=this.dataSource;
        }, (err) => {
            console.error(err);
        });
    }
    //fetches departmentsList from DB and sorts it alphabetically
    getDepartmentsList() {
        // let roleAndEventID={
        //   roleID:2,
        //   eventId:3
        // }
        this.serverService.getAllDepartmentsList().subscribe((resultantDepartments) => {
            this.departmentsList = resultantDepartments.DeptFevent_list;
            this.departmentsList = this.departmentsList.sort(function (x, y) {
                if (x.DepartmentName < y.DepartmentName)
                    return -1;
                if (x.DepartmentName > y.DepartmentName)
                    return 1;
                return 0;
            });
            this.selectedDepartments = this.departmentsList;
            console.log("dept names", this.departmentsList);
            this.previousDataFiltersForStackedBarChart.DeptID = this.getDepartmentListIds();
        }, (err) => {
            console.error(err);
        });
    }
    //fetches qualityEventsList from DB and sorts it alphabetically
    getQualityEventsList() {
        this.serverService.getQualityEventsList().subscribe((resultantQualityEvents) => {
            this.qualityEventsList = resultantQualityEvents.sort(function (x, y) {
                if (x.QualityEvent_TypeName < y.QualityEvent_TypeName)
                    return -1;
                if (x.QualityEvent_TypeName > y.QualityEvent_TypeName)
                    return 1;
                return 0;
            });
            this.selectedQualityEvents = this.qualityEventsList;
            this.previousDataFiltersForStackedBarChart.QualityEventType = this.getQualityEventIds();
        }, (err) => {
            console.error(err);
        });
    }
    getDeptNameFromDeptID(deptId) {
        for (let i = 0; i < this.departmentsList.length; i++) {
            if (this.departmentsList[i].Department_ID === deptId) {
                return this.departmentsList[i].DepartmentName;
            }
        }
    }
    getQualityEventNameFromQualityEventId(qualityEventID) {
        for (let i = 0; i < this.qualityEventsList.length; i++) {
            if (this.qualityEventsList[i].QualityEvent_TypeID === qualityEventID) {
                return this.qualityEventsList[i].QualityEvent_TypeName;
            }
        }
    }
    setFromAndToDates() {
        this.selectedFromDate = this.filtersForm.get('fromDate').value[0];
        this.selectedToDate = this.filtersForm.get('toDate').value[0];
    }
    closeFromDate(otherArg, selectedDates, dateStr, instance) {
        console.log("selected dates", selectedDates);
        console.log("date str", dateStr);
        console.log("instance", instance);
        console.log("other arg", otherArg);
        otherArg.set('minDate', '01-Jul-2021');
        // this.toDateOptions.minDate='01-Jul-2021';
        // console.log('modified to date options',this.toDateOptions);
    }
    // openTodate(selectedDates, dateStr, instance){
    //   console.log("selected dates",selectedDates);
    //   console.log("to date str",dateStr);
    //   console.log("to date instance",instance);
    //   // instance.set('minDate','01-Jul-2021');
    //   // console.log("instance modified",instance);
    // }
    isDatesValid(fromDate, toDate) {
        // let requiredFormatOfFromDate:string = this.getRequiredDateFormat(fromDate);
        // this.toDateOptions.minDate='01-Jul-2021';
        // console.log('min to date',this.toDateOptions.minDate);
        // let requiredFormatOfToDate=this.getRequiredDateFormat(toDate);
        // return moment(requiredFormatOfFromDate).isBefore(requiredFormatOfToDate);
    }
    getSelectedDeptIDs() {
        this.selectedDeptIds = [];
        for (let i = 0; i < this.selectedDepartments.length; i++) {
            this.selectedDeptIds.push(this.selectedDepartments[i].Department_ID);
        }
    }
    getSelectedQualityEventIDs() {
        this.selectedQualityEventIds = [];
        for (let i = 0; i < this.selectedQualityEvents.length; i++) {
            this.selectedQualityEventIds.push(this.selectedQualityEvents[i].QualityEvent_TypeID);
        }
    }
    getSelectedCCNClassificationIDs() {
        this.selectedCCNClassificationIDs = [];
        for (let i = 0; i < this.selectedChangeClassification.length; i++) {
            this.selectedCCNClassificationIDs.push(this.selectedChangeClassification[i].changeClassificationID);
        }
    }
    //The following method executes on every filter change
    onDeptQualityEventsDataChartFilterChange(item) {
        this.dataSource = [];
        this.setFromAndToDates();
        this.isDatesValid(this.selectedFromDate, this.selectedToDate);
        if (this.overallStatuslevel === 0) {
            this.getSelectedDeptIDs();
            this.getSelectedQualityEventIDs();
            this.allDepartmentQualityEventsFilters = {
                DeptID: this.selectedDepartments.length === 0 ? [] : this.selectedDeptIds,
                QualityEventType: this.selectedQualityEvents.length === 0 ? [] : this.selectedQualityEventIds,
                FromDate: this.selectedFromDate !== undefined ? this.getRequiredDateFormat(this.selectedFromDate) : this.initialFromDate,
                ToDate: this.selectedToDate !== undefined ? this.getRequiredDateFormat(this.selectedToDate) : this.initialToDate
            };
            this.previousDataFiltersForStackedBarChart = this.allDepartmentQualityEventsFilters;
            this.subtitleForStackedBarChart = '( ' + this.allDepartmentQualityEventsFilters.FromDate + ' to ' + this.allDepartmentQualityEventsFilters.ToDate + ' )';
            this.getDepartmentsQualityEventsDataFromFilters(this.allDepartmentQualityEventsFilters);
        }
        else if (this.overallStatuslevel === 1) {
            this.getFilteredStatusChartData();
        }
        else if (this.overallStatuslevel === 2) {
            this.getFilteredOverdueData();
        }
    }
    onSelectAllDepartments(items) {
        this.selectedDepartments = items;
        this.onDeptQualityEventsDataChartFilterChange(items);
    }
    onSelectAllQualityEvents(items) {
        this.selectedQualityEvents = items;
        this.onDeptQualityEventsDataChartFilterChange(items);
    }
    onSelectAllChangeCategories(items) {
        this.selectedChangeCategory = items;
        this.getFilteredStatusChartData();
    }
    onSelectAllChangeClassifications(items) {
        this.selectedChangeClassification = items;
        this.getFilteredStatusChartData();
    }
    onSelectAllIncidentCategories(items) {
        this.selectedIncidentCategory = items;
        this.getFilteredStatusChartData();
    }
    onSelectAllIncidentClassification(items) {
        this.selectedIncidentClassification = items;
        this.getFilteredStatusChartData();
    }
    getUntrimmedDeptName(trimmedDeptName) {
        for (let i = 0; i < this.departmentsList.length; i++) {
            if (trimmedDeptName === TrimmedDepartmentNames[this.departmentsList[i].DepartmentName]) {
                return this.departmentsList[i].DepartmentName;
            }
        }
    }
    //on Department's Quality event Stack is clicked on graph
    onDeptQualityEventPointClick(event) {
        this.setFromAndToDates();
        this.selectedDepartments = [];
        let unTrimmedDeptName = this.getUntrimmedDeptName(event.target.originalArgument);
        // event.target.originalArgument = event.target.originalArgument === 'Executive Management'? 'Executive Managements' : event.target.originalArgument;
        this.selectedDepartments.push({
            Department_ID: this.getDeptIdFromDeptName(unTrimmedDeptName),
            DepartmentName: unTrimmedDeptName
        });
        this.selectedQualityEvents = [];
        this.selectedQualityEvents.push({
            QualityEvent_TypeID: this.getQualityEventIdFromQualityEventName(event.target.series.name),
            QualityEvent_TypeName: event.target.series.name
        });
        this.selectedChangeCategory = this.changeCategoriesList;
        this.selectedChangeClassification = this.changeClassificationList;
        this.selectedIncidentCategory = this.incidentCategoriesList;
        this.selectedIncidentClassification = this.incidentClassificationList;
        this.prevCurrentDrillDownHeaderText = this.currentDrillDownHeaderText = event.target.series.name;
        this.selectedQualityEventName = event.target.series.name;
        this.getQualityEventDataOfOneDept(this.selectedDepartments[0].Department_ID, event.target.series.name);
    }
    getQualityEventDataOfOneDept(deptId, qualityEventName) {
        let numToArrConversion = [];
        numToArrConversion.push(deptId);
        this.qualityEventDataOfOneDept = {
            DeptID: numToArrConversion,
            QualityEventType: this.getQualityEventIdFromQualityEventName(qualityEventName),
            CCNClassification: CCNClassificationStatusCodes.All,
            CCNCategory: CCNCategoryStatusCodes.Both,
            IncidentClassification: IncidentClassificationStatusCodes.All,
            IncidentCategory: 0,
            FromDate: this.selectedFromDate !== undefined ? this.getRequiredDateFormat(this.selectedFromDate) : this.initialFromDate,
            ToDate: this.selectedToDate !== undefined ? this.getRequiredDateFormat(this.selectedToDate) : this.initialToDate
        };
        this.selectedQualityEventIdForStatusChart = this.qualityEventDataOfOneDept.QualityEventType;
        this.previousDataFiltersForStatusChart = this.qualityEventDataOfOneDept;
        this.serverService.getQualityEventStatusOfOneDept(this.qualityEventDataOfOneDept).subscribe((res) => {
            this.statusListData = res.QeStatus_List;
            this.statusListData = this.excludeEmptyRecordsOfStatusChart(this.statusListData);
            this.overallStatuslevel++;
        }, (err) => {
            console.error(err);
        });
    }
    excludeEmptyRecordsOfStatusChart(statusListData) {
        let sampleListData = [];
        for (let i = 0; i < statusListData.length; i++) {
            if (statusListData[i].TotalRecords !== 0) {
                sampleListData.push(statusListData[i]);
            }
        }
        return sampleListData;
    }
    getFilteredStatusChartData() {
        if (this.overallStatuslevel === 1) {
            this.getSelectedDeptIDs();
            this.getSelectedCCNClassificationIDs();
            this.filteredQualityEventData = {
                DeptID: this.selectedDeptIds,
                QualityEventType: this.selectedQualityEventIdForStatusChart,
                // CCNClassification:(this.selectedChangeClassification.length===0) ? CCNClassificationStatusCodes.All: this.selectedChangeClassification[0].changeClassificationID,
                CCNClassification: ((this.selectedChangeClassification.length === 0) || (this.selectedChangeClassification.length > 1)) ? CCNClassificationStatusCodes.All : this.selectedChangeClassification[0].changeClassificationID,
                CCNCategory: (this.selectedChangeCategory.length === 0 || this.selectedChangeCategory.length > 1) ? CCNCategoryStatusCodes.Both : this.selectedChangeCategory[0].changeCategoryID,
                IncidentClassification: (this.selectedIncidentClassification.length === 0 || this.selectedIncidentClassification.length > 1) ? IncidentClassificationStatusCodes.All : this.selectedIncidentClassification[0].incidentClassificationID,
                IncidentCategory: 0,
                FromDate: this.selectedFromDate !== undefined ? this.getRequiredDateFormat(this.selectedFromDate) : this.initialFromDate,
                ToDate: this.selectedToDate !== undefined ? this.getRequiredDateFormat(this.selectedToDate) : this.initialToDate
            };
            this.previousDataFiltersForStatusChart = this.filteredQualityEventData;
            this.getFilteredQualityEventStatusChart(this.filteredQualityEventData);
        }
        else if (this.overallStatuslevel === 2) {
            this.getFilteredOverdueData();
        }
    }
    getFilteredQualityEventStatusChart(filteredQualityEventData) {
        this.serverService.getQualityEventStatusOfOneDept(filteredQualityEventData).subscribe((res) => {
            this.statusListData = res.QeStatus_List;
            this.statusListData = this.excludeEmptyRecordsOfStatusChart(this.statusListData);
        }, (err) => {
            console.error(err);
        });
    }
    onStatusChartPointClick(event) {
        if (event.target.argument === QualityEventStatus.OverDue) {
            this.setFromAndToDates();
            this.getSelectedDeptIDs();
            this.currentDrillDownHeaderText = event.target.argument + ' - ' + this.selectedQualityEventName;
            this.overallStatuslevel++;
            this.getFilteredOverdueData();
        }
        else if (this.selectedQualityEventIdForStatusChart === QualityEventIDs.ChangeControlNote) {
            this.getCCNStatusListOfAppliedFilters(event.target.argument);
        }
        else if (this.selectedQualityEventIdForStatusChart === QualityEventIDs.IncidentReport) {
            this.getIncidentStatusListOfAppliedFilters(event.target.argument);
        }
        else if (this.selectedQualityEventIdForStatusChart === QualityEventIDs.Errata) {
            this.getErrataStatusListOfAppliedFilters(event.target.argument);
        }
        else {
            this.getNTFStatusListOfAppliedFilters(event.target.argument);
        }
    }
    getFilteredOverdueData() {
        this.getSelectedDeptIDs();
        this.overdueDataFilters = {
            DeptID: this.selectedDeptIds,
            QualityEventType: this.selectedQualityEventIdForStatusChart,
            CCNClassification: ((this.selectedChangeClassification.length === 0) || (this.selectedChangeClassification.length > 1)) ? CCNClassificationStatusCodes.All : this.selectedChangeClassification[0].changeClassificationID,
            CCNCategory: (this.selectedChangeCategory.length === 0 || this.selectedChangeCategory.length > 1) ? CCNCategoryStatusCodes.Both : this.selectedChangeCategory[0].changeCategoryID,
            IncidentClassification: (this.selectedIncidentClassification.length === 0 || this.selectedIncidentClassification.length > 1) ? IncidentClassificationStatusCodes.All : this.selectedIncidentClassification[0].incidentClassificationID,
            IncidentCategory: 0,
            FromDate: this.selectedFromDate !== undefined ? this.getRequiredDateFormat(this.selectedFromDate) : this.initialFromDate,
            ToDate: this.selectedToDate !== undefined ? this.getRequiredDateFormat(this.selectedToDate) : this.initialToDate
        };
        this.serverService.getOverdueData(this.overdueDataFilters).subscribe((res) => {
            this.overdueData = this.dashboardService.getModifiedOverdueData(res.Overdue_List);
        }, (err) => {
            console.error(err);
        });
    }
    onStackedBarChartExporting(event) {
        event.fileName = "Total Quality Events of Depts " + this.subtitleForStackedBarChart;
    }
    customizeLabel(arg) {
        return arg.argumentText + ' : ' + arg.valueText;
    }
    customizeTooltip(arg) {
        return {
            text: arg.seriesName + ' : ' + arg.valueText
        };
    }
    onOverdueStatusChartPointClick(event) {
        if (this.selectedQualityEventIdForStatusChart === QualityEventIDs.ChangeControlNote) {
            this.getCCNStatusListOfAppliedFilters(event.target.argument);
        }
        else if (this.selectedQualityEventIdForStatusChart === QualityEventIDs.IncidentReport) {
            this.getIncidentStatusListOfAppliedFilters(event.target.argument);
        }
    }
    onShowOverallListView() {
        if (this.selectedQualityEventIdForStatusChart === QualityEventIDs.ChangeControlNote) {
            this.getOverallCCNListOfAppliedFilters();
        }
        else if (this.selectedQualityEventIdForStatusChart === QualityEventIDs.IncidentReport) {
            this.getOverallIncidentListOfAppliedFilters();
        }
        else if (this.selectedQualityEventIdForStatusChart === QualityEventIDs.Errata) {
            this.getOverallErrataListOfAppliedFilters();
        }
        else {
            this.getOverallNTFListOfAppliedFilters();
        }
    }
    getOverallCCNListOfAppliedFilters() {
        if (this.overallStatuslevel !== 2) {
            this.ccnListFilters = {
                DeptID: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.DeptID : this.filteredQualityEventData.DeptID,
                CCNClassification: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.CCNClassification : this.filteredQualityEventData.CCNClassification,
                CCNCategory: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.CCNCategory : this.filteredQualityEventData.CCNCategory,
                FromDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.FromDate : this.filteredQualityEventData.FromDate,
                ToDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.ToDate : this.filteredQualityEventData.ToDate,
                showtype: ShowTypeList.OverallList
            };
        }
        else {
            this.ccnListFilters = {
                DeptID: this.overdueDataFilters.DeptID,
                CCNClassification: this.overdueDataFilters.CCNClassification,
                CCNCategory: this.overdueDataFilters.CCNCategory,
                FromDate: this.overdueDataFilters.FromDate,
                ToDate: this.overdueDataFilters.ToDate,
                showtype: ShowTypeList.OverallOverdueList
            };
        }
        this.serverService.getCCNList(this.ccnListFilters).subscribe((res) => {
            this.CCNRecords = res.CCN_List;
            const initialState = {
                currentDrillDownHeaderText: this.currentDrillDownHeaderText + ' - ' + 'Overall List',
                ccnRecords: this.CCNRecords,
                qualityEvent: this.getQualityEventNameFromQualityEventId(this.selectedQualityEventIdForStatusChart),
                fileName: ' - ' + 'Overall List' + ' (' + this.ccnListFilters.FromDate + ' to ' + this.ccnListFilters.ToDate + ')',
                showType: this.ccnListFilters.showtype
            };
            this.bsModalRef = this.modalService.show(ListViewComponent, { initialState, class: 'gray modal-xl', backdrop: 'static', });
        }, (err) => {
            console.error(err);
        });
    }
    getShowTypeListID(statusName) {
        if (this.overallStatuslevel === 2) {
            if (statusName === QualityEventStatus.OverdueLessThanOneWk) {
                return ShowTypeList.OverdueLessThanOneWk;
            }
            else if (statusName === QualityEventStatus.OverdueOneWkToOneMn) {
                return ShowTypeList.OverdueOneWkToOneMn;
            }
            else if (statusName === QualityEventStatus.OverdueOneMnToThreeMn) {
                return ShowTypeList.OverdueOneMnToThreeMn;
            }
            else if (statusName === QualityEventStatus.OverdueThreeMnToSixMn) {
                return ShowTypeList.OverdueThreeMnToSixMn;
            }
            else if (statusName === QualityEventStatus.OverdueGreaterThanSixMn) {
                return ShowTypeList.OverdueGreaterThanSixMn;
            }
        }
        else {
            return ShowTypeList[statusName];
        }
    }
    getCCNStatusListOfAppliedFilters(CCNStatusName) {
        if (this.overallStatuslevel !== 2) {
            this.ccnListFilters = {
                DeptID: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.DeptID : this.filteredQualityEventData.DeptID,
                CCNClassification: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.CCNClassification : this.filteredQualityEventData.CCNClassification,
                CCNCategory: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.CCNCategory : this.filteredQualityEventData.CCNCategory,
                FromDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.FromDate : this.filteredQualityEventData.FromDate,
                ToDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.ToDate : this.filteredQualityEventData.ToDate,
                showtype: this.getShowTypeListID(CCNStatusName)
            };
        }
        else {
            this.ccnListFilters = {
                DeptID: this.overdueDataFilters.DeptID,
                CCNClassification: this.overdueDataFilters.CCNClassification,
                CCNCategory: this.overdueDataFilters.CCNCategory,
                FromDate: this.overdueDataFilters.FromDate,
                ToDate: this.overdueDataFilters.ToDate,
                showtype: this.getShowTypeListID(CCNStatusName)
            };
        }
        this.serverService.getCCNList(this.ccnListFilters).subscribe((res) => {
            this.CCNRecords = res.CCN_List;
            const initialState = {
                currentDrillDownHeaderText: this.currentDrillDownHeaderText + ' - ' + CCNStatusName,
                ccnRecords: this.CCNRecords,
                qualityEvent: this.getQualityEventNameFromQualityEventId(this.selectedQualityEventIdForStatusChart),
                fileName: ' - ' + CCNStatusName + ' (' + this.ccnListFilters.FromDate + ' to ' + this.ccnListFilters.ToDate + ')',
                showType: this.ccnListFilters.showtype
            };
            this.bsModalRef = this.modalService.show(ListViewComponent, { initialState, class: 'gray modal-xl', backdrop: 'static', });
        }, (err) => {
            console.error(err);
        });
    }
    getOverallIncidentListOfAppliedFilters() {
        if (this.overallStatuslevel !== 2) {
            this.incidentListFilters = {
                DeptID: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.DeptID : this.filteredQualityEventData.DeptID,
                IncidentClassification: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.IncidentClassification : this.filteredQualityEventData.IncidentClassification,
                IncidentCategory: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.IncidentCategory : this.filteredQualityEventData.IncidentCategory,
                FromDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.FromDate : this.filteredQualityEventData.FromDate,
                ToDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.ToDate : this.filteredQualityEventData.ToDate,
                showtype: ShowTypeList.OverallList
            };
        }
        else {
            this.incidentListFilters = {
                DeptID: this.overdueDataFilters.DeptID,
                IncidentClassification: this.overdueDataFilters.IncidentClassification,
                IncidentCategory: this.overdueDataFilters.IncidentCategory,
                FromDate: this.overdueDataFilters.FromDate,
                ToDate: this.overdueDataFilters.ToDate,
                showtype: ShowTypeList.OverallOverdueList
            };
        }
        this.serverService.getIncidentList(this.incidentListFilters).subscribe((res) => {
            this.incidentRecords = res.Incident_List;
            const initialState = {
                currentDrillDownHeaderText: this.currentDrillDownHeaderText + ' - ' + 'Overall List',
                incidentRecords: this.incidentRecords,
                qualityEvent: this.getQualityEventNameFromQualityEventId(this.selectedQualityEventIdForStatusChart),
                fileName: ' - ' + 'Overall List' + ' (' + this.incidentListFilters.FromDate + ' to ' + this.incidentListFilters.ToDate + ')',
                showType: this.incidentListFilters.showtype
            };
            this.bsModalRef = this.modalService.show(ListViewComponent, { initialState, class: 'gray modal-xl', backdrop: 'static', });
        }, (err) => {
            console.error(err);
        });
    }
    getIncidentStatusListOfAppliedFilters(incidentStatusName) {
        if (this.overallStatuslevel !== 2) {
            this.incidentListFilters = {
                DeptID: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.DeptID : this.filteredQualityEventData.DeptID,
                IncidentClassification: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.IncidentClassification : this.filteredQualityEventData.IncidentClassification,
                IncidentCategory: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.IncidentCategory : this.filteredQualityEventData.IncidentCategory,
                FromDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.FromDate : this.filteredQualityEventData.FromDate,
                ToDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.ToDate : this.filteredQualityEventData.ToDate,
                showtype: this.getShowTypeListID(incidentStatusName)
            };
        }
        else {
            this.incidentListFilters = {
                DeptID: this.overdueDataFilters.DeptID,
                IncidentClassification: this.overdueDataFilters.IncidentClassification,
                IncidentCategory: this.overdueDataFilters.IncidentCategory,
                FromDate: this.overdueDataFilters.FromDate,
                ToDate: this.overdueDataFilters.ToDate,
                showtype: this.getShowTypeListID(incidentStatusName)
            };
        }
        this.serverService.getIncidentList(this.incidentListFilters).subscribe((res) => {
            this.incidentRecords = res.Incident_List;
            const initialState = {
                currentDrillDownHeaderText: this.currentDrillDownHeaderText + ' - ' + incidentStatusName,
                incidentRecords: this.incidentRecords,
                qualityEvent: this.getQualityEventNameFromQualityEventId(this.selectedQualityEventIdForStatusChart),
                fileName: ' - ' + incidentStatusName + ' (' + this.incidentListFilters.FromDate + ' to ' + this.incidentListFilters.ToDate + ')',
                showType: this.incidentListFilters.showtype
            };
            this.bsModalRef = this.modalService.show(ListViewComponent, { initialState, class: 'gray modal-xl', backdrop: 'static', });
        }, (err) => {
            console.error(err);
        });
    }
    getOverallErrataListOfAppliedFilters() {
        this.errataListFilters = {
            DeptID: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.DeptID : this.filteredQualityEventData.DeptID,
            FromDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.FromDate : this.filteredQualityEventData.FromDate,
            ToDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.ToDate : this.filteredQualityEventData.ToDate,
            showtype: ShowTypeList.OverallList
        };
        this.serverService.getErrataList(this.errataListFilters).subscribe((res) => {
            this.errrataRecords = res.Errata_List;
            const initialState = {
                currentDrillDownHeaderText: this.currentDrillDownHeaderText + ' - ' + 'Overall List',
                errataRecords: this.errrataRecords,
                qualityEvent: this.getQualityEventNameFromQualityEventId(this.selectedQualityEventIdForStatusChart),
                fileName: ' - ' + 'Overall List' + ' (' + this.errataListFilters.FromDate + ' to ' + this.errataListFilters.ToDate + ')',
                showType: this.errataListFilters.showtype
            };
            this.bsModalRef = this.modalService.show(ListViewComponent, { initialState, class: 'gray modal-xl', backdrop: 'static', });
        }, (err) => {
            console.error(err);
        });
    }
    getErrataStatusListOfAppliedFilters(errataStatusName) {
        this.errataListFilters = {
            DeptID: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.DeptID : this.filteredQualityEventData.DeptID,
            FromDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.FromDate : this.filteredQualityEventData.FromDate,
            ToDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.ToDate : this.filteredQualityEventData.ToDate,
            showtype: this.getShowTypeListID(errataStatusName)
        };
        this.serverService.getErrataList(this.errataListFilters).subscribe((res) => {
            this.errrataRecords = res.Errata_List;
            const initialState = {
                currentDrillDownHeaderText: this.currentDrillDownHeaderText + ' - ' + errataStatusName,
                errataRecords: this.errrataRecords,
                qualityEvent: this.getQualityEventNameFromQualityEventId(this.selectedQualityEventIdForStatusChart),
                fileName: ' - ' + errataStatusName + ' (' + this.errataListFilters.FromDate + ' to ' + this.errataListFilters.ToDate + ')',
                showType: this.errataListFilters.showtype
            };
            this.bsModalRef = this.modalService.show(ListViewComponent, { initialState, class: 'gray modal-xl', backdrop: 'static', });
        }, (err) => {
            console.error(err);
        });
    }
    getOverallNTFListOfAppliedFilters() {
        this.noteToFileListFilters = {
            DeptID: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.DeptID : this.filteredQualityEventData.DeptID,
            FromDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.FromDate : this.filteredQualityEventData.FromDate,
            ToDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.ToDate : this.filteredQualityEventData.ToDate,
            showtype: ShowTypeList.OverallList
        };
        this.serverService.getNTFList(this.noteToFileListFilters).subscribe((res) => {
            this.noteToFileRecords = res.NTF_List;
            const initialState = {
                currentDrillDownHeaderText: this.currentDrillDownHeaderText + ' - ' + 'Overall List',
                noteToFileRecords: this.noteToFileRecords,
                qualityEvent: this.getQualityEventNameFromQualityEventId(this.selectedQualityEventIdForStatusChart),
                fileName: ' - ' + 'Overall List' + ' (' + this.noteToFileListFilters.FromDate + ' to ' + this.noteToFileListFilters.ToDate + ')',
                showType: this.noteToFileListFilters.showtype
            };
            this.bsModalRef = this.modalService.show(ListViewComponent, { initialState, class: 'gray modal-xl', backdrop: 'static', });
        }, (err) => {
            console.error(err);
        });
    }
    getNTFStatusListOfAppliedFilters(noteToFileStatusName) {
        this.noteToFileListFilters = {
            DeptID: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.DeptID : this.filteredQualityEventData.DeptID,
            FromDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.FromDate : this.filteredQualityEventData.FromDate,
            ToDate: this.filteredQualityEventData === undefined ? this.qualityEventDataOfOneDept.ToDate : this.filteredQualityEventData.ToDate,
            showtype: this.getShowTypeListID(noteToFileStatusName)
        };
        this.serverService.getNTFList(this.noteToFileListFilters).subscribe((res) => {
            this.noteToFileRecords = res.NTF_List;
            const initialState = {
                currentDrillDownHeaderText: this.currentDrillDownHeaderText + ' - ' + noteToFileStatusName,
                noteToFileRecords: this.noteToFileRecords,
                qualityEvent: this.getQualityEventNameFromQualityEventId(this.selectedQualityEventIdForStatusChart),
                fileName: ' - ' + noteToFileStatusName + ' (' + this.noteToFileListFilters.FromDate + ' to ' + this.noteToFileListFilters.ToDate + ')',
                showType: this.noteToFileListFilters.showtype
            };
            this.bsModalRef = this.modalService.show(ListViewComponent, { initialState, class: 'gray modal-xl', backdrop: 'static', });
        }, (err) => {
            console.error(err);
        });
    }
    onCloseListView() {
        this.bsModalRef.hide();
    }
    // rerender(): void {
    //   this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
    //     // Destroy the table first
    //     dtInstance.destroy();
    //     // Call the dtTrigger to rerender again
    //     this.dtTrigger.next();
    //   });
    // }
    unsubscribe() {
        this.events.forEach((subscription) => {
            subscription.unsubscribe();
        });
        this.events = [];
    }
    ngOnDestroy() {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
    }
};
tslib_1.__decorate([
    ViewChild(DataTableDirective)
], ManagementLevelChartComponent.prototype, "datatableElement", void 0);
tslib_1.__decorate([
    ViewChild('fromDatePickrElement')
], ManagementLevelChartComponent.prototype, "fromDatePickr", void 0);
ManagementLevelChartComponent = tslib_1.__decorate([
    Component({
        selector: 'app-management-level-chart',
        templateUrl: './management-level-chart.component.html',
        styleUrls: ['./management-level-chart.component.scss']
    })
], ManagementLevelChartComponent);
export { ManagementLevelChartComponent };
//# sourceMappingURL=management-level-chart.component.js.map