﻿using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.Web.UI.WebControls;
using UMS_BusinessLayer;

namespace AizantIT_PharmaApp.UserManagement
{
    public partial class UnlockEmployee : System.Web.UI.Page
    {
        UMS_BAL objUMS_BAL;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblMessage.Text = "";
                if (HelpClass.IsUserAuthenticated())
                {
                    
                    DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                    DataTable dtTemp = new DataTable();
                    DataRow[] drUMS = dt.Select("ModuleID=1");
                    if (drUMS.Length > 0)
                    {
                        dtTemp = drUMS.CopyToDataTable();

                        BindGV();
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx");
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx");
                }
            }
        }
        public void BindGV()
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                DataTable dt = objUMS_BAL.GVLoginActivation();
                gvUnlockEmployee.DataSource = dt;
                gvUnlockEmployee.DataBind();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }
        protected void btnUnLock_Click(object sender, EventArgs e)
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                foreach (GridViewRow gvrow in gvUnlockEmployee.Rows)
                {
                   

                    CheckBox chck = gvrow.FindControl("CheckBox1") as CheckBox;

                    if (chck.Checked)
                    {
                        int UnlockedBy = 0;
                        if (HelpClass.IsUserAuthenticated())
                        {
                            UnlockedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        }
                        var Label = gvrow.FindControl("lblGVEmpID") as Label;
                        int s = objUMS_BAL.UnLock(int.Parse(Label.Text), UnlockedBy);
                        if (s > 0)
                        {
                            BindGV();
                            lblMessage.Text = "Employee(s) Unlocked Sucessfully";
                            lblMessage.ForeColor = System.Drawing.Color.Green;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UserManagement/UnlockEmployee.aspx");
        }
    }
}