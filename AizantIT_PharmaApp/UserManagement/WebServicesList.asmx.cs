﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using UMS_BusinessLayer;
using UMS_BO;
using AizantIT_PharmaApp.Common;
using AizantIT_PharmaApp.UserManagement.UMSModel;

namespace AizantIT_PharmaApp.UserManagement
{
    /// <summary>
    /// Summary description for WebServicesList
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 

    [System.Web.Script.Services.ScriptService]
    public class WebServicesList : System.Web.Services.WebService
    {
        static int EmpID = 0;
        string Dashboard = "";
        string UnAssignRoles = "";
        UMS_BAL UMS_ObjBAL = new UMS_BAL();
        SigUPObjects objSignUp = new SigUPObjects();
        //EmployeeList

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetEmployeesList1()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString();

            //var sSearch_Photo = HttpContext.Current.Request.Params["sSearch_1"];
            var sSearch_EmpCode = HttpContext.Current.Request.Params["sSearch_3"];
            var sSearch_Name = HttpContext.Current.Request.Params["sSearch_4"];
            var sSearch_DepartmentName = HttpContext.Current.Request.Params["sSearch_5"];
            var sSearch_DesignationName = HttpContext.Current.Request.Params["sSearch_6"];
            var sSearch_StartDate = HttpContext.Current.Request.Params["sSearch_9"];
            var sSearch_Status = HttpContext.Current.Request.Params["sSearch_10"];
            //var sSearch_LoginID = HttpContext.Current.Request.Params["sSearch_11"];
            //var sSearch_Locked = HttpContext.Current.Request.Params["sSearch_12"];
            //var sSearch_RoleCount = HttpContext.Current.Request.Params["sSearch_13"];
            
            var DeptCode = HttpContext.Current.Request.Params["DeptCode"].ToString();
            var RoleID = int.Parse(HttpContext.Current.Request.Params["RoleID"]);
            var ShowType = int.Parse(HttpContext.Current.Request.Params["ShowType"]);
            List<EmployeeLists> listEmployees = new List<EmployeeLists>();
            int filteredCount = 0;
            string Operation = "Employee";

            DataTable dt = UMS_ObjBAL.GetUMSEmployeeLIST(iDisplayLength, iDisplayStart, iSortCol_0,
             sSortDir_0, sSearch,sSearch_EmpCode,sSearch_Name,sSearch_DepartmentName,sSearch_DesignationName,
             sSearch_StartDate,sSearch_Status,Operation, ShowType, DeptCode, RoleID);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    EmployeeLists employee = new EmployeeLists();
                    employee.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    employee.RoleCount = rdr["RoleCount"].ToString();
                    employee.EmpCode = (rdr["EmpCode"].ToString());
                    employee.Name = (rdr["Name"].ToString());
                    employee.DepartmentName = (rdr["DepartmentName"].ToString());
                    employee.DesignationName = (rdr["DesignationName"].ToString());
                    employee.StartDate = (rdr["StartDate"].ToString());
                    employee.Status = (rdr["Status"].ToString());
                    employee.LoginID = (rdr["LoginID"].ToString());
                    employee.Locked = (rdr["Locked"].ToString());
                    employee.EmpID = Convert.ToInt32(rdr["EmpID"]);
                    employee.MobileNo = (rdr["MobileNo"].ToString());
                    employee.DeptID = Convert.ToInt32(rdr["DeptID"]);
                    employee.OfficialEmailID = (rdr["OfficialEmailID"].ToString());
                    if ((rdr["Photo"].ToString()) != "")
                    {
                        string base64ImageString = ConvertBytesToBase64((byte[])(rdr["Photo"]));
                        employee.Photo = base64ImageString;
                    }
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listEmployees.Add(employee);
                }
            }
            var result = new
            {
                iTotalRecords = UMS_ObjBAL.GetUMSTotalCount("EmpList"),
                iTotalDisplayRecords = filteredCount,
                aaData = listEmployees
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }

        public string ConvertBytesToBase64(byte[] imageBytes)
        {
            return Convert.ToBase64String(imageBytes);
        }

        //DesignationList      
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetDesignationList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_Code = HttpContext.Current.Request.Params["sSearch_1"];
            var sSearch_DesignationName = HttpContext.Current.Request.Params["sSearch_2"];

            var DeptCode = HttpContext.Current.Request.Params["DeptCode"].ToString();
            var RoleID = int.Parse(HttpContext.Current.Request.Params["RoleID"]);
            var ShowType = int.Parse(HttpContext.Current.Request.Params["ShowType"]);
            List<DesignationList> listDesignation = new List<DesignationList>();
            int filteredCount = 0;
            string Operation = "Designation";
            DataTable dt = UMS_ObjBAL.GetUMSLIST(iDisplayLength, iDisplayStart, iSortCol_0,
            sSortDir_0, sSearch, sSearch_Code, sSearch_DesignationName, Operation, ShowType, DeptCode, RoleID);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    DesignationList Designation = new DesignationList();
                    Designation.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    Designation.DesignationID = Convert.ToInt32(rdr["DesignationID"]);
                    Designation.DesignationName = (rdr["DesignationName"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listDesignation.Add(Designation);
                }
            }
            var result = new
            {
                iTotalRecords = UMS_ObjBAL.GetUMSTotalCount("DesigList"),
                iTotalDisplayRecords = filteredCount,
                aaData = listDesignation
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }

        //DepartmentList
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetDepartmentList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_DeptCode = HttpContext.Current.Request.Params["sSearch_2"];
            var sSearch_DepartmentName = HttpContext.Current.Request.Params["sSearch_3"];

            var DeptCode = HttpContext.Current.Request.Params["DeptCode"].ToString();
            var RoleID = int.Parse(HttpContext.Current.Request.Params["RoleID"]);
            var ShowType = int.Parse(HttpContext.Current.Request.Params["ShowType"]);
            List<DepartmentList> listDepartment = new List<DepartmentList>();
            int filteredCount = 0;
            string Operation = "Department";
            DataTable dt = UMS_ObjBAL.GetUMSLIST(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, sSearch_DeptCode, sSearch_DepartmentName, Operation, ShowType, DeptCode, RoleID);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    DepartmentList Department = new DepartmentList();
                    Department.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    Department.DepartmentID = Convert.ToInt32(rdr["DeptID"]);
                    Department.DepartmentName = (rdr["DepartmentName"].ToString());
                    Department.DeptCode = (rdr["DeptCode"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listDepartment.Add(Department);
                }
            }
            var result = new
            {
                iTotalRecords = UMS_ObjBAL.GetUMSTotalCount("DeptList"),
                iTotalDisplayRecords = filteredCount,
                aaData = listDepartment
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }


        //SecuritySetting (Change Password) Web Method for getting EmpID
        //Modified by pradeep
        [System.Web.Services.WebMethod(EnableSession = true)]
        [System.Web.Script.Services.ScriptMethod()]
        public string GetSessionEmployeeID()
        {
            if (HelpClass.IsUserAuthenticated())
            {
                EmpID = Convert.ToInt32((HttpContext.Current.Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            }

            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(EmpID);
        }

        //SecuritySetting (Change Password) Web Method for getting CurrentPassword and NewPassword
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string SubmitSecurityPassword(string CurrentPassword, string NewPassword)
        {
            if (HelpClass.IsUserAuthenticated())
            {
                EmpID = Convert.ToInt32((HttpContext.Current.Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            }

            string strResult = string.Empty;
            //strResult = "2";
            UMS_BAL objUMS_BAL = new UMS_BAL();
            DataSet dt = objUMS_BAL.GetDataToMyProfile(EmpID);
            if (dt != null && dt.Tables[0].Rows.Count > 0)
            {
                string CurrentPwd = dt.Tables[0].Rows[0]["Password"].ToString();
                string TxtCurrentPwd = objUMS_BAL.Encrypt(CurrentPassword);
                if (CurrentPwd == TxtCurrentPwd)
                {
                    DataTable dt3 = objUMS_BAL.MyprofileChangePassword(EmpID, NewPassword, "Password changed by Logged in user himself.");
                    if (dt3.Rows.Count > 0)
                    {
                        DataRow dr = dt3.Rows[0];
                        int Result = Convert.ToInt32(dr["Status"].ToString());
                        if (Result == 4)
                        {
                            strResult = "1";

                        }
                        else
                        {
                            strResult = "2";

                        }
                    }

                    else
                    {
                        strResult = "3";

                    }
                }
                else
                {
                    strResult = "4";

                }
            }
            else
            {

                strResult = "5";
            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(strResult);

        }

        //SecuritySetting (Update SecurityQues and SecurityAns) to Prepopulate the data on textbox
        [WebMethod(EnableSession = true)]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [System.Web.Script.Services.ScriptMethod()]
        public string SecurityQuesAnsPrepopulate()
        {

            if (HelpClass.IsUserAuthenticated())
            {
                EmpID = Convert.ToInt32((HttpContext.Current.Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            }
            SigUPObjects objSinUp = new SigUPObjects();
            UMS_BAL objUMS_BAL = new UMS_BAL();

            DataSet dt = objUMS_BAL.GetDataToMyProfile(EmpID);

            string secQuestion = "", SecAnswer = "";
            if (dt.Tables[0].Rows.Count > 0)
            {
                //objSinUp.SecurityQuestion = dt.Tables[0].Rows[0]["Question"].ToString();
                secQuestion = dt.Tables[0].Rows[0]["Question"].ToString();
                SecAnswer = dt.Tables[0].Rows[0]["Answer"].ToString();

                objSinUp.SecurityAnswer = dt.Tables[0].Rows[0]["Answer"].ToString();
            }
            var result = new
            {
                securityQuestion = secQuestion,
                SecurityAnswer = SecAnswer,
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }

        //SecuritySetting (Update SecurityQues and SecurityAns) to Update SecurityQues and SecurityAns
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string UpdateSecurityQuesAnsw(string SecurityQuestion, string SecurityAnswer)
        {
            string strResult = string.Empty;
            if (HelpClass.IsUserAuthenticated())
            {
                EmpID = Convert.ToInt32((HttpContext.Current.Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            }

            UMS_BAL objUMS_BAL = new UMS_BAL();
            int count = objUMS_BAL.MyprofileSecurityUpdate(EmpID, SecurityQuestion, SecurityAnswer);
            if (count > 0)
            {
                strResult = "1";
            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(strResult);
        }

        //MyProfile Web-service 
        //MyProfile to Prepopulate the Employee details on Label
        [WebMethod(EnableSession = true)]
        //[System.Web.Script.Services.ScriptMethod()]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string MyProfileGetDataToMyProfilePrepoulateLabel()
        {
            int _empID = 0;

            if (HttpContext.Current.Session["UserDetails"] != null)
            {
                _empID = Convert.ToInt32((HttpContext.Current.Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);

            }

            UMS_BAL objUMS_BAL = new UMS_BAL();
            DataSet dt = objUMS_BAL.GetDataToMyProfile(_empID);

            string employeeCode = "", firstName = "", lastName = "", dob = "", gender = "", department = "", designation = "", bloodGroup = "";
            string startDate = "", email = "", address = "", mobile = "", country = "", state = "", image = "";

            if (dt.Tables[0].Rows.Count > 0)
            {
                employeeCode = dt.Tables[0].Rows[0]["EmpCode"].ToString();
                firstName = dt.Tables[0].Rows[0]["FirstName"].ToString();
                lastName = dt.Tables[0].Rows[0]["LastName"].ToString();
                dob = dt.Tables[0].Rows[0]["Dateofbirth"].ToString();
                gender = dt.Tables[0].Rows[0]["Gender"].ToString();
                department = dt.Tables[0].Rows[0]["DepartmentName"].ToString();
                designation = dt.Tables[0].Rows[0]["DesignationName"].ToString();
                if (!string.IsNullOrEmpty(dt.Tables[0].Rows[0]["BloodGroup"].ToString()))
                {
                    bloodGroup = dt.Tables[0].Rows[0]["BloodGroup"].ToString();
                }
                else
                {
                    bloodGroup = "- NA -";
                }
                if (!string.IsNullOrEmpty(dt.Tables[0].Rows[0]["StartDate"].ToString()))
                {
                    startDate = dt.Tables[0].Rows[0]["StartDate"].ToString();
                }
                else
                {
                    startDate = "- NA -";
                }
                email = dt.Tables[0].Rows[0]["OfficialEmailID"].ToString();
                address = dt.Tables[0].Rows[0]["Address"].ToString();
                mobile = dt.Tables[0].Rows[0]["MobileNo"].ToString();
                country = dt.Tables[0].Rows[0]["CountryName"].ToString();
                state = dt.Tables[0].Rows[0]["StateName"].ToString();

                if (!string.IsNullOrEmpty(dt.Tables[0].Rows[0]["Photo"].ToString()))
                {
                    image = objUMS_BAL.BindApplicantImage((byte[])dt.Tables[0].Rows[0]["Photo"]);
                }
                else
                {
                    image = "/Images/UserLogin/DefaultImage.jpg";
                }
            }

            var result = new
            {
                EmployeeCode = employeeCode,
                FirstName = firstName,
                LastName = lastName,
                Dob = dob,
                Gender = gender,
                Department = department,
                Designation = designation,
                BloodGroup = bloodGroup,
                StartDate = startDate,
                Email = email,
                Address = address,
                Mobile = mobile,
                Country = country,
                State = state,
                Image = image

            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }


        //MyRoles WebService 
        //MyRoles to Prepopulate the Modules Drop-down list
        [WebMethod(EnableSession = true)]
        [System.Web.Script.Services.ScriptMethod()]
        public string MyRolesDropDownBindPrePopulateLoadModules()
        {
            if (HelpClass.IsUserAuthenticated())
            {
                EmpID = Convert.ToInt32((HttpContext.Current.Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            }
            List<RolesObjects> Modules = new List<RolesObjects>();
            UMS_BAL objUMS_BAL = new UMS_BAL();
            DataSet dt = objUMS_BAL.GetDataToMyProfile(EmpID);
            foreach (DataRow drresult in dt.Tables[1].Rows)
            {
                Modules.Add(new RolesObjects
                {
                    ModuleID = drresult["ModuleID"].ToString(),
                    ModuleName = drresult["ModuleName"].ToString(),
                });
            }
            var result = new
            {
                records = Modules
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }

        //MyRoles WebService
        //MyRoles to get Roles and Department from selected Module
        [WebMethod(EnableSession = true)]
        [System.Web.Script.Services.ScriptMethod()]
        public string myroles(string SelectedModuleID)
        {
            if (HelpClass.IsUserAuthenticated())
            {
                EmpID = Convert.ToInt32((HttpContext.Current.Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            }
            List<RolesObjects> Roles = new List<RolesObjects>();
            List<RolesObjects> Depts = new List<RolesObjects>();
            UMS_BAL objUMS_BAL = new UMS_BAL();

            DataSet ds = objUMS_BAL.GetModuleRoles(EmpID.ToString(), SelectedModuleID);
            DataTable dtRoles = ds.Tables[0];
            DataTable dtDept = ds.Tables[1];

            foreach (DataRow drresult in dtRoles.Rows)
            {
                Roles.Add(new RolesObjects
                {
                    RoleID = drresult["RoleID"].ToString(),
                    RoleName = drresult["RoleName"].ToString(),
                });
            }

            foreach (DataRow drresult in dtDept.Rows)
            {
                Depts.Add(new RolesObjects
                {
                    DeptID = drresult["DeptID"].ToString(),
                    DepartmentName = drresult["DepartmentName"].ToString(),
                });
            }

            var result = new
            {
                records1 = Roles,
                records2 = Depts
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }

        #region Employee Assign Roles
        //Get RoleWise Departments
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetRolewiseDepartments(int EmpID, int ModuleID, int RoleID)
        {

            bool isAccessibleDepts = true;
            List<UMSModel.UmsDepartmentList> DepartmentLists = new List<UMSModel.UmsDepartmentList>();
            List<UMSModel.UmsRoleList> RoleLists = new List<UMSModel.UmsRoleList>();
            List<UMSModel.UmsModuleList> ModuleLists = new List<UMSModel.UmsModuleList>();

            DataTable dtDepartments = UMS_ObjBAL.GetUMSEmployeeRole(EmpID, ModuleID, RoleID, out isAccessibleDepts);

            if (dtDepartments.Rows.Count != 0)
            {
                foreach (DataRow rdr in dtDepartments.Rows)
                {
                    UMSModel.UmsDepartmentList Departments = new UMSModel.UmsDepartmentList();
                    UMSModel.UmsRoleList Role = new UMSModel.UmsRoleList();
                    UMSModel.UmsModuleList Module = new UmsModuleList();

                    //Department List
                    if (!DBNull.Value.Equals(rdr["DeptID"]))
                    {
                        Departments.DeptID = Convert.ToInt32(rdr["DeptID"]);
                        Departments.DepartmentName = (rdr["DepartmentName"].ToString());
                    }

                    Departments.RoleId = Convert.ToInt32(rdr["RoleId"]);
                    Departments.RoleName = Convert.ToString(rdr["RoleName"]);
                    Departments.IsAccessible = Convert.ToBoolean(rdr["IsAccessibleDepartments"]);
                    Departments.ModuleID = Convert.ToInt32(rdr["ModuleID"]);
                    Departments.ModuleName = Convert.ToString(rdr["ModuleName"]);

                    //roleList
                    Role.RoleID = Convert.ToInt32(rdr["RoleId"]);
                    Role.RoleName = Convert.ToString(rdr["RoleName"]);
                    Role.IsAccessible = Convert.ToBoolean(rdr["IsAccessibleDepartments"]);
                    Role.ModuleID = Convert.ToInt32(rdr["ModuleID"]);
                    Role.ModuleName = Convert.ToString(rdr["ModuleName"]);

                    //moduleList
                    Module.ModuleID = Convert.ToInt32(rdr["ModuleID"]);
                    Module.ModuleName = Convert.ToString(rdr["ModuleName"]);

                    RoleLists.Add(Role);
                    DepartmentLists.Add(Departments);
                    ModuleLists.Add(Module);
                }
            }

            var myRoleList = RoleLists.GroupBy(i => i.RoleID)
                                      .Select(g => g.First()).ToList();//grouping apply in roleList 

            var myModuleList = ModuleLists.GroupBy(i => i.ModuleID)
                                          .Select(g => g.First()).ToList();//Grouping  apply in ModuleList

            var result = new JsonResponce
            {
                DepartmentsList = DepartmentLists,
                RolesList = myRoleList,
                ModuleList = myModuleList,
                Message = "success",
                IsAccessDept = isAccessibleDepts
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }


        //Employee Role Deletion
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string EmployeeRoleDelete(int EmpID, int ModuleID, int RoleID)
        {

            int createdBy = 0;
            if (HelpClass.IsUserAuthenticated())
            {
                createdBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            }
            string data = UMS_ObjBAL.EmployeeRoleDelete(EmpID, ModuleID, RoleID, createdBy);
            var result = new JsonResponce
            {
                Message = data
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);

        }
        //Employee Role Accessible Departments Deletion
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string RoleDepartmentsDelete(int EmpID, int ModuleID, int RoleID, int DeptID)
        {

            int CreatedBy = 0;
            if (HelpClass.IsUserAuthenticated())
            {
                CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            }
            string data = UMS_ObjBAL.RoleDepartmentsDelete(EmpID, ModuleID, RoleID, DeptID, CreatedBy);


            var result = new JsonResponce
            {
                Message = data
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);

        }
        //employee Roles Assign or Updating
        [WebMethod(EnableSession = true)]
        [System.Web.Script.Services.ScriptMethod()]
        public string EmpRolesAssign(int EmpID, int ModuleID, int RoleID, string DeptList)
        {
            var strDept = DeptList.Split(',');
            UMS_BAL objUMS_BAL = new UMS_BAL();
            DataTable dtDepts = new DataTable();
            dtDepts.Columns.Add("DeptID");
            DataRow drDepts;
            int createdBy = 0;
            if (HelpClass.IsUserAuthenticated())
            {
                createdBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            }

            if (DeptList != "")
            {
                foreach (var item in strDept)
                {
                    drDepts = dtDepts.NewRow();
                    drDepts["DeptID"] = item;
                    dtDepts.Rows.Add(drDepts);
                }
            }

            int retuendata = objUMS_BAL.EmpRolesAssign(EmpID, ModuleID, dtDepts, RoleID, createdBy);
            var result = new JsonResponce
            {
                Message = retuendata == 1 ? "Updated" : "Added"

            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        //Bind the Accessible Departments 
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetDepartmentsList()
        {
            List<UMSModel.UmsDepartmentList> AccessibleDepartmentLists = new List<UMSModel.UmsDepartmentList>();

            UMS_BAL objUMS_BAL = new UMS_BAL();
            DataTable dt = objUMS_BAL.BindDepartmentsBAL();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    UMSModel.UmsDepartmentList DepartmentList = new UMSModel.UmsDepartmentList();
                    DepartmentList.DeptID = Convert.ToInt32(rdr["DeptID"]);
                    DepartmentList.DepartmentName = (rdr["DepartmentName"].ToString());
                    AccessibleDepartmentLists.Add(DepartmentList);
                }
            }
            var result = new JsonResponce
            {
                DepartmentsList = AccessibleDepartmentLists
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);

        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string RoleDepartmentsChecking(int EmpID, int ModuleID, int RoleID, int DeptID)
        {

            UMS_BAL objUMS_BAL = new UMS_BAL();
            string retuendata = objUMS_BAL.RoleDepartmentsChecking(EmpID, ModuleID, RoleID, DeptID);

            var result = new JsonResponce
            {
                Message = retuendata
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string RoleDeleteChecking(int EmpID, int ModuleID, int RoleID)
        {

            UMS_BAL objUMS_BAL = new UMS_BAL();
            string retuendata = objUMS_BAL.RoleDeleteChecking(EmpID, ModuleID, RoleID);

            var result = new JsonResponce
            {
                Message = retuendata
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion

        //To Get Employee Login History
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetEmpLoginHistory()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString();

            var sSearch_EmpCode = HttpContext.Current.Request.Params["sSearch_0"].ToString();
            var sSearch_EmpName = HttpContext.Current.Request.Params["sSearch_1"].ToString();
            var sSearch_LoginID = HttpContext.Current.Request.Params["sSearch_2"].ToString();
            var sSearch_Login_Time = HttpContext.Current.Request.Params["sSearch_3"].ToString();
            var sSearch_Logout_Time = HttpContext.Current.Request.Params["sSearch_4"].ToString();
            var sSearch_UserHostAddress = HttpContext.Current.Request.Params["sSearch_5"].ToString();
            var sSearch_UserClientAddress = HttpContext.Current.Request.Params["sSearch_6"].ToString();

            List<EmployeeListHistory> listEmployees = new List<EmployeeListHistory>();

            int filteredCount = 0;
            DataTable dt = UMS_ObjBAL.GetUMS_BAL_LoginHistoryLIST(iDisplayLength, iDisplayStart, iSortCol_0,
             sSortDir_0, sSearch, sSearch_EmpCode, sSearch_EmpName, sSearch_LoginID, sSearch_Login_Time, sSearch_Logout_Time, sSearch_UserHostAddress, sSearch_UserClientAddress);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    EmployeeListHistory empHistory = new EmployeeListHistory();
                    empHistory.EmpCode = (rdr["EmpCode"].ToString());
                    empHistory.Employee = (rdr["EmployeeName"].ToString());
                    empHistory.LoginID = (rdr["LoginID"].ToString());
                    empHistory.Login_Time = (rdr["Login_Time"].ToString());
                    empHistory.Logout_Time = (rdr["Logout_Time"].ToString());
                    empHistory.UserHostAddress = (rdr["UserHostAddress"].ToString());
                    empHistory.UserClientAddress = (rdr["UserClientAddress"].ToString());

                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listEmployees.Add(empHistory);
                }
            }
            var result = new
            {
                //iTotalRecords = UMS_ObjBAL.GetUMSTotalCount("EmpList"),
                iTotalDisplayRecords = filteredCount,
                aaData = listEmployees
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetDepartmentsBasedOnRoleID(int EmpID, int ModuleID, int RoleID)
        {

            DataTable dtDepartments = UMS_ObjBAL.GetDepartmentsBasedOnRoleID(EmpID, ModuleID, RoleID);

            List<int> DeptIDs = new List<int>();
            if (dtDepartments.Rows.Count != 0)
            {
                foreach (DataRow rdr in dtDepartments.Rows)
                {

                    DeptIDs.Add(Convert.ToInt32(rdr["DeptID"]));
                }
            }
            var result = new
            {
                DepartmentsIDs = DeptIDs,
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
    }
}
