﻿using AizantIT_PharmaApp.Common;
using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using UMS_BusinessLayer;

namespace AizantIT_PharmaApp.UserManagement
{
    public partial class EmpRoles : System.Web.UI.Page
    {
        UMS_BAL objUMS_BAL;
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
       
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
              
                ConfirmCustomAlert.buttonClick += new EventHandler(ConfirmCustomAlert_buttonClick);
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=1");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                    
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EmpRoles_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EmpRoles_M1:" + strline + "  " + strMsg, "error");
            }
        }
        private void InitializeThePage()
        {
            try
            {
                DropDownDefaults();
                btnAddRemoveCss();
                if (Request.QueryString["E"] != null && Request.QueryString["D"] != null)
                {
                    ddlDepartment.SelectedValue = Request.QueryString["D"].ToString();
                    DataTable dtemp = objUMS_BAL.GetEmployess(Request.QueryString["D"].ToString());
                    ddlEmployeeList.DataSource = dtemp;
                    ddlEmployeeList.DataValueField = "EmpID";
                    ddlEmployeeList.DataTextField = "Name";
                    ddlEmployeeList.DataBind();
                    ddlDepartment.Enabled = false;
                    ddlEmployeeList.SelectedValue = Request.QueryString["E"].ToString();
                    BindRoles(ddlEmployeeList.SelectedValue);
                }
                hdnPreviousModuleID.Value = "0";
                hdnUserMakeChanges.Value = "N";
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EmpRoles_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EmpRoles_M2:" + strline + "  " + strMsg, "error");
            }
        }
        public void btnAddRemoveCss()
        {
            try
            {
                btnAddRoles.Enabled = btnRemoveRoles.Enabled = false;
                btnAddRoles.CssClass = btnRemoveRoles.CssClass = "btn btn-default";
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EmpRoles_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EmpRoles_M3:" + strline + "  " + strMsg, "error");
            }
        }
        public void DropDownDefaults()
        {
            try
            {
                BindDepartments();
                bindModules();
                ddlEmployeeList.Enabled = false;
               
                ddlEmployeeList.Items.Clear();
                ddlEmployeeList.Items.Insert(0, new ListItem("-- Select Employee --", "0"));
                ddlModulesList.Enabled = false;
                cblDepartments.Items.Clear();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EmpRoles_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EmpRoles_M4:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindDepartments()
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                DataTable dt = objUMS_BAL.BindDepartmentsBAL();
                ddlDepartment.DataSource = dt;
                ddlDepartment.DataTextField = "DepartmentName";
                ddlDepartment.DataValueField = "DeptID";
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("-- Select Department --", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Emp_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Emp_M3:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindCBLDepartmentsList(string selectedDepartment)
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                DataTable dt = objUMS_BAL.BindDepartmentsBAL();
                cblDepartments.DataSource = dt;
                cblDepartments.DataTextField = "DepartmentName";
                cblDepartments.DataValueField = "DeptID";
              
                cblDepartments.DataBind();
                
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EmpRoles_M6:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EmpRoles_M6:" + strline + "  " + strMsg, "error");
            }
        }
        protected void ddlDepartments_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblDepartments.Items.Clear();
                objUMS_BAL = new UMS_BAL();
                upEmpRoleDept.Update();
                ddlModulesList.SelectedIndex = 0;
                ddlModulesList.Enabled = false;
                btnAddRemoveCss();
                
                ddlEmployeeList.Enabled = false;
                ddlEmployeeList.Items.Clear();
                int SelectedDepartmentID = int.Parse(ddlDepartment.SelectedItem.Value);
                if (SelectedDepartmentID > 0)
                {
                    DataTable dtemp = objUMS_BAL.GetEmployess(SelectedDepartmentID.ToString());
                    ddlEmployeeList.DataSource = dtemp;
                    ddlEmployeeList.DataValueField = "EmpID";
                    ddlEmployeeList.DataTextField = "Name";
                    ddlEmployeeList.DataBind();
                    ddlEmployeeList.Items.Insert(0, new ListItem("- Select Employee -", "0"));
                    ddlEmployeeList.Enabled = true;
                }
                else
                {
                    ddlEmployeeList.Items.Insert(0, new ListItem("- Select Employee -", "0"));
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EmpRoles_M7:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EmpRoles_M7:" + strline + "  " + strMsg, "error");
            }
        }
        private void bindModules()
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                DataTable dt = objUMS_BAL.GetModules();
                if (dt.Rows.Count > 0)
                {
                    ddlModulesList.DataSource = dt;
                    ddlModulesList.DataTextField = "ModuleName";
                    ddlModulesList.DataValueField = "ModuleID";
                    ddlModulesList.DataBind();
                    ddlModulesList.Items.Insert(0, new ListItem("-- All Module --", "0"));
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EmpRoles_M8:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EmpRoles_M8:" + strline + "  " + strMsg, "error");
            }
        }
        #region ListBox Operations
        protected void btnAddRoles_Click(object sender, EventArgs e)
        {
            MovingListBoxItems(lbFirst, lbSecond);
            hdnUserMakeChanges.Value = "Y";
            if (lbSecond.Items.Count == 0)
            {
                cblDepartments.Enabled = false;
            }
            else
            {
                cblDepartments.Enabled = true;
            }
        }
        protected void btnRemoveRoles_Click(object sender, EventArgs e)
        {
            MovingListBoxItems(lbSecond, lbFirst);
            hdnUserMakeChanges.Value = "Y";
            if (lbSecond.Items.Count == 0)
            {
                cblDepartments.Enabled = false;
                for (int items = 0; items < cblDepartments.Items.Count; items++)
                {
                    cblDepartments.ClearSelection();
                }
            }
            else
            {
                cblDepartments.Enabled = true;
            }
        }
        protected void cblDepartments_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnUserMakeChanges.Value = "Y";
            bindCheckedDepartmentToTopIndex();
        }
        private void bindCheckedDepartmentToTopIndex()
        {
            for (int i = 0; i < cblDepartments.Items.Count; i++)
            {
                if (cblDepartments.Items[i].Selected == true)
                {
                    cblDepartments.Items.Insert(0, new ListItem(cblDepartments.Items[i].Text.ToString(), cblDepartments.Items[i].Value));
                    //cblDepartments.Items.FindByValue(cblDepartments.Items[i].Value).Selected = true;
                    cblDepartments.Items.RemoveAt(i + 1);
                    cblDepartments.Items.FindByValue(cblDepartments.Items[0].Value).Selected = true;
                }
            }
        }
        private void MovingListBoxItems(ListBox source, ListBox destination)
        {
            for (int i = source.Items.Count - 1; i >= 0; i--)
            {
                if (source.Items[i].Selected)
                {
                    destination.Items.Add(source.Items[i]);
                    source.Items.Remove(source.Items[i]);
                }
            }
        }
        #endregion
        protected void btnReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UserManagement/EmpRoles.aspx");
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtDepts = getSelectedDepartments();
                int selectedCount = chkAssignRoles.Items.Cast<ListItem>().Count(li => li.Selected);
                if (ddlEmployeeList.SelectedValue == "0" || ddlModulesList.SelectedValue == "0" || (dtDepts.Rows.Count == 0) /*|| (selectedCount==0)*/)
                {

                    ArrayList Mandatory = new ArrayList();
                    if (ddlDepartment.SelectedValue == "0")
                    {
                        Mandatory.Add("Select Department");
                    }
                    if (ddlEmployeeList.SelectedValue == "0")
                    {
                       
                        Mandatory.Add("Select Employee");
                    }
                    if (ddlModulesList.SelectedValue == "0")
                    {
                       
                        Mandatory.Add("Select Module");
                    }
                    if (dtDepts.Rows.Count == 0)
                    {
                       
                        Mandatory.Add("Select Accessible Departments");
                    }
                   
                    StringBuilder s = new StringBuilder();
                    foreach (string x in Mandatory)
                    {
                        s.Append(x);
                        s.Append("<br/>");
                    }
                    HelpClass.custAlertMsg(this, this.GetType(), s.ToString(), "error");
                    
                }
                else
                {
                    hdnUserMakeChanges.Value = "btnSubmit";
                    HelpClass.custAlertMsg(this, this.GetType(), "Do you want to submit", "confirm");
                    
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "EmpRoles_M9:" + strline + "  " + strMsg, "error");
            }
        }
        public void SubmitModuleWiseEmpRolesAndDepts(DataTable dtDepts)
        {
            try
            {
                hdnUserMakeChanges.Value = "N";
                AssignRoles(chkAssignRoles, ddlEmployeeList.SelectedValue, ddlModulesList.SelectedValue, dtDepts);
                hdnSaveChanges.Value = "No";
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EmpRoles_M10:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EmpRoles_M10:" + strline + "  " + strMsg, "error");
            }
        }
        private DataTable getSelectedDepartments()
        {
            DataTable dtDepts = new DataTable();
            dtDepts.Columns.Add("DeptID");
            DataRow drDepts;
            foreach (ListItem Item in cblDepartments.Items)
            {
                if (Item.Selected)
                {
                    drDepts = dtDepts.NewRow();
                    drDepts["DeptID"] = Item.Value;
                    dtDepts.Rows.Add(drDepts);
                }
            }
            return dtDepts;
        }
        private void AssignRoles(CheckBoxList lbRoles, string EmpID, string ModuleID, DataTable dtDepts)
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                int CreatedBy = 0;
                if (HelpClass.IsUserAuthenticated())
                {
                    CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                }
                DataTable dt = new DataTable();
                dt.Columns.Add("RoleID");
                DataRow dr;
                foreach (ListItem item in lbRoles.Items)
                {
                    if (item.Selected)
                    {
                        dr = dt.NewRow();
                        dr["RoleID"] = item.Value;
                        dt.Rows.Add(dr);
                    }
                }
               
                DataTable dtTemp = new DataTable();
                DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                DataRow[] drUMS = dtx.Select("ModuleID=1");
                dtTemp = drUMS.CopyToDataTable();
                int RoleID = Convert.ToInt32(dtTemp.Rows[0]["RoleID"].ToString());
                string Result = objUMS_BAL.AssignEmpRoles(dt, EmpID, CreatedBy, ModuleID, dtDepts, RoleID);
                int selectedCount = chkAssignRoles.Items.Cast<ListItem>().Count(li => li.Selected);
                if (selectedCount > 0)
                {
                    if (hdnSaveChanges.Value == "Yes")
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Roles Assigned in " + ddlModulesList.Items.FindByValue(hdnPreviousModuleID.Value).Text + " Module", "success");
                        lblMsg.Text = "";
                       
                    }
                    else
                    {
                        if (ddlModulesList.SelectedValue != "0")
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Roles Assigned in " + ddlModulesList.SelectedItem.Text + " Module", "success");
                            lblMsg.Text = "";
                        }
                        else
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Roles Assigned to Employee", "success");
                            lblMsg.Text = "";
                        }
                    }
                }
                else
                {
                    if (hdnSaveChanges.Value == "Yes")
                    {
                        lblMsg.Text = "Warning : No Roles Assigned in " + ddlModulesList.Items.FindByValue(hdnPreviousModuleID.Value).Text + " Module";
                       
                    }
                    else
                    {
                        if (ddlModulesList.SelectedValue != "0")
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Roles UnAssigned in " + ddlModulesList.SelectedItem.Text + " Module", "warning");
                            lblMsg.Text = " Warning : No Roles Assigned in " + ddlModulesList.SelectedItem.Text + " Module";
                        }
                        else
                        {
                            lblMsg.Text = "Warning : No Roles Assigned to Employee";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EmpRoles_M11:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EmpRoles_M11:" + strline + "  " + strMsg, "error");
            }
        }
        private void ErrorMsg(Exception ex)
        {
            lblMsg.Text = ex.Message;
            lblMsg.ForeColor = Color.Red;
        }
        public void BindRoles(string value)
        {
            try
            {
                if (value != "0")
                {
                    lbFirst.Enabled = lbSecond.Enabled = true;
                    ddlModulesList.Enabled = true;
                    objUMS_BAL = new UMS_BAL();
                    DataSet ds = objUMS_BAL.GetModuleRoles(value, ddlModulesList.SelectedValue);
                    DataTable dtResult = ds.Tables[0];
                    if (dtResult.Rows.Count >= 0)
                    {
                        DataTable dtRoles = objUMS_BAL.GetEmpModuleUnAssignedRoles(dtResult, ddlModulesList.SelectedValue);
                        chkAssignRoles.DataSource = dtRoles;
                        chkAssignRoles.DataTextField = "RoleName";
                        chkAssignRoles.DataValueField = "RoleID";
                        chkAssignRoles.DataBind();
                        if (dtRoles.Rows.Count > 0)
                        {
                            if (dtResult.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtRoles.Rows.Count; i++)
                                {
                                    for (int j = 0; j < dtResult.Rows.Count; j++)
                                    {
                                        string A = dtResult.Rows[j]["RoleName"].ToString();
                                        string B = dtRoles.Rows[i]["RoleName"].ToString();

                                        if (A == B)
                                        {
                                            chkAssignRoles.Items[i].Selected = true;
                                        }                                       
                                    }
                                }
                            }
                        }
                        upAssignRoles.Update();
                    }
                    else
                    {
                    }
                }
                else
                {
                    ddlModulesList.Enabled = false;
                }
                upEmpRoleDept.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EmpRoles_M12:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EmpRoles_M12:" + strline + "  " + strMsg, "error");
            }
        }
        protected void ddlEmployeeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlModulesList.SelectedIndex = 0;
                cblDepartments.Items.Clear();
                btnAddRemoveCss();
                hdnUserMakeChanges.Value = "N";
                BindRoles(ddlEmployeeList.SelectedValue);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EmpRoles_M13:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EmpRoles_M13:" + strline + "  " + strMsg, "error");
            }
        }
      
        protected void ddlModulesList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
               
                bindModuleWiseDeptAndRoles();
                upEmpRoleDept.Update();
                


            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EmpRoles_M15:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EmpRoles_M15:" + strline + "  " + strMsg, "error");
            }
        }
        private void bindModuleWiseDeptAndRoles()
        {
            try
            {
                cblDepartments.Items.Clear();
                //binding all departments to Checkbox list
                BindCBLDepartmentsList(ddlDepartment.SelectedValue.ToString());
                objUMS_BAL = new UMS_BAL();
                // getting Roles and Departments based on Employee
                DataSet ds = objUMS_BAL.GetModuleRoles(ddlEmployeeList.SelectedValue, ddlModulesList.SelectedValue);
                DataTable dtAssignedRoles = ds.Tables[0];
                if (Convert.ToInt32(ddlModulesList.SelectedValue) > 0)
                {
                    DataTable dtDeptResult = ds.Tables[1];
                    for (int i = 0; i < dtDeptResult.Rows.Count; i++)
                    {
                        cblDepartments.Items.FindByValue(dtDeptResult.Rows[i]["DeptID"].ToString()).Selected = true;
                    }
                    bindCheckedDepartmentToTopIndex();
                }
                if (dtAssignedRoles.Rows.Count > 0)
                {
                    chkAssignRoles.Items.Clear();
                    upAssignRoles.Update();
                    DataTable dtAvailableRoles = objUMS_BAL.GetEmpModuleUnAssignedRoles(dtAssignedRoles, ddlModulesList.SelectedValue);
                    chkAssignRoles.DataSource = dtAvailableRoles;
                    chkAssignRoles.DataTextField = "RoleName";
                    chkAssignRoles.DataValueField = "RoleID";
                    chkAssignRoles.DataBind();
                    if (dtAvailableRoles.Rows.Count > 0)
                    {
                        if (dtAssignedRoles.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtAvailableRoles.Rows.Count; i++)
                            {
                                for (int j = 0; j < dtAssignedRoles.Rows.Count; j++)
                                {
                                    string A = dtAssignedRoles.Rows[j]["RoleName"].ToString();
                                    string B = dtAvailableRoles.Rows[i]["RoleName"].ToString();

                                    if (A == B)
                                    {
                                        chkAssignRoles.Items[i].Selected = true;
                                    }
                                }
                            }
                        }
                    }
                    upAssignRoles.Update();
                }
                else
                {
                    chkAssignRoles.Items.Clear();
                    upAssignRoles.Update();
                    DataTable dt = objUMS_BAL.GetModuleRolesonModuleId(ddlModulesList.SelectedValue);
                    if (dt.Rows.Count > 0)
                    {
                        chkAssignRoles.DataSource = dt;
                        chkAssignRoles.DataTextField = "RoleName";
                        chkAssignRoles.DataValueField = "RoleID";
                        chkAssignRoles.DataBind();
                        upAssignRoles.Update();
                    }
                }
                hdnUserMakeChanges.Value = "N";
                int selectedCount = chkAssignRoles.Items.Cast<ListItem>().Count(li => li.Selected);
                if (selectedCount == 0 || ddlModulesList.SelectedValue == "0")
                {
                    lblMsg.Text = "Warning : No Roles Assigned to the Employee";
                }
                cblDepartments.Enabled = true;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EmpRoles_M16:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EmpRoles_M16:" + strline + "  " + strMsg, "error");
            }
        }
        
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }

        protected void btnDelete1_Click(object sender, EventArgs e)
        {
            hdnUserMakeChanges.Value = "btnDeleteClick";
            HelpClass.custAlertMsg(this, this.GetType(), "Do you want to Remove Roles for this Module", "confirm");
        }
        protected void ConfirmCustomAlert_buttonClick(object sender, EventArgs e)
        {
            if (hdnUserMakeChanges.Value == "btnDeleteClick")
            {
                objUMS_BAL = new UMS_BAL();
                int CreatedBy = 0;
                if (HelpClass.IsUserAuthenticated())
                {
                    CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                }
                DataTable dt = new DataTable();
                dt.Columns.Add("RoleID");

                DataTable dtTemp = new DataTable();
                DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                DataRow[] drUMS = dtx.Select("ModuleID=1");
                dtTemp = drUMS.CopyToDataTable();
                int RoleID = Convert.ToInt32(dtTemp.Rows[0]["RoleID"].ToString());
                objUMS_BAL.DeleteRolesToOdule(ddlEmployeeList.SelectedValue, CreatedBy, ddlModulesList.SelectedValue, RoleID);
                string strModuleName = ddlModulesList.SelectedItem.Text;
                ddlModulesList.SelectedIndex = 0;
                ddlModulesList_SelectedIndexChanged(null, null);
                HelpClass.custAlertMsg(this, this.GetType(), "Roles for " + strModuleName + " are removed. ", "success");
            }
            if(hdnUserMakeChanges.Value == "btnSubmit")
            {
                DataTable dtDepts = getSelectedDepartments();
                SubmitModuleWiseEmpRolesAndDepts(dtDepts);
            }
        }
    }
}