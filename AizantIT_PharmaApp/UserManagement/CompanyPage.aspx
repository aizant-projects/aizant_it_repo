﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UMS_Master/NestedUMS_Master.master" AutoEventWireup="true" CodeBehind="CompanyPage.aspx.cs" Inherits="AizantIT_PharmaApp.UserManagement.CompanyPage" %>

<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <script type="text/javascript">
        function emailURLvalidation()
        {
            var comUrl = document.getElementById("<%=txtWebsite.ClientID%>").value.trim();
            var EmpEmailID = document.getElementById("<%=txtEmailID.ClientID%>").value.trim();
            var CompanyName = document.getElementById("<%=txtCompanyName.ClientID%>").value.trim();
            var compLogo = document.getElementById("<%=hdfLogo.ClientID%>").value.trim();
            errors = [];
           ShowPleaseWait('show');	
           
        if (CompanyName == '') {
            errors.push("Please enter Company Name");
            }
        if (compLogo != '1') {
            errors.push("Please upload Company logo");
        }
        if (EmpEmailID.length > 0) {
            if (!IsValidEmail(EmpEmailID)) {
                errors.push("Invalid Email ID");
            }
        }
        if (comUrl.length > 0) {
            if (!IsValidURL(comUrl)) {
                errors.push("Invalid Website address");
            }
        }
            if (errors.length > 0) {
                ShowPleaseWait('hide');
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else
            {
                 ShowPleaseWait('hide');
                 return true;
            }
        }
        function ReloadCurrentPage() {
            window.open("<%=ResolveUrl("~/UserManagement/CompanyPage.aspx")%>", "_self");
        }
        function showimagepreview(input) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.readAsDataURL(input.files[0]);
                filerdr.onload = function (e) {
                    var image = new Image();
                    //Set the Base64 string return from FileReader as source.
                    image.src = e.target.result;
                    //Validate the File Height and Width.
                    image.onload = function () {
                        var height = this.height;
                        var width = this.width;
                        errors = [];
                        if (height > width) {
                            errors.push("Image Height should be less than width of the Image");
                            document.getElementById("<%=FileUpload1.ClientID %>").value = "";

                        }
                        if (errors.length > 0) {
                            custAlertMsg(errors.join("<br/>"), "error");
                            return false;
                        }
                        else {
                            $('#<%=imgEmpImage.ClientID%>').attr('src', e.target.result);
                            document.getElementById("<%=hdfLogo.ClientID%>").value = "1";
                            return true;
                            }
                        
                      }
                  }
            }

        }
    </script>
    <asp:HiddenField ID="hdfLogo" runat="server" Value="0" />
      <div class=" col-lg-3 padding-none">
        <div class="closebtn"><span>&#9776;</span></div>
        <div style="display: none;" class="closebtn1">
            <span>&#9776;</span>
        </div>
    </div>
    <div class=" float-left col-lg-12 padding-none">

        <div class=" col-lg-12 padding-none float-left">
            <div class="col-lg-12 padding-none float-left">

               
                <%--                <div id="dvTab">
                      <ul class="nav nav-tabs Panel_Header" id="myTab" >
       
                   <li class="active"><a data-toggle="pill" href="#home" >Employee</a></li>
                    <li><a data-toggle="pill" href="#menu1">Documents</a></li>

                </ul>--%>
                <div class="grid_scrollbar padding-none float-left">
                     <div id="header">
                    <div class="col-sm-12 dashboard_panel_header">
                       
                           Company Info 
                      
                    </div>
                </div>
                            <div class="col-lg-10  label_panel float-left">
                              
                                <div class="col-sm-12 padding-none float-left top" style="border: 1px solid #07889a;">
                                      <div class="col-sm-12 dashboard_panel_header float-left  bottom">
                                    
                                        Company Details
                                 
                                </div>
                                    <div class="form-group col-sm-4 float-left">
                                        <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_ums">Company code</label>
                                        <div class="col-sm-12 padding-none">
                                            <input type="text" class="form-control login_input_sign_up" id="txtCompanyCode" placeholder="" runat="server" tabindex="1" maxlength="20">
                                        </div>
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <div class="form-group col-sm-4 float-left">
                                        <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">Company Name<span class="smallred_label">*</span></label>
                                        <div class="col-sm-12 padding-none">
                                          
                                            <%--<input type="text" class="form-control login_input_sign_up"  id="txtCompanyName" placeholder="" runat="server" tabindex="2" onkeypress="return ValidateAlpha(event);">--%>
                                              
                                            <asp:TextBox class="form-control login_input_sign_up" ID="txtCompanyName" placeholder="" runat="server" tabindex="2" onkeypress="return ValidateAlpha(event);" MaxLength="200"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-4  padding-none float-left">
                                        <div class="form-group col-sm-12">
                                            <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">Start Date</label>
                                            <div class="col-sm-12 padding-none">
                                                <input type="text" class="form-control login_input_sign_up" id="txtStartDate" placeholder="" runat="server" tabindex="3">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                             
                                <div class="col-sm-12 padding-none float-left top" style="border: 1px solid #07889a; margin-bottom: 10px;">
                                       <div class="col-sm-12 dashboard_panel_header  float-left">
                                    
                                        About Company 
                                </div>
                                    <div class="col-sm-12 padding-none float-left">


                                        <div class="form-group col-sm-12 float-left">
                                            <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">Description</label>
                                            <div class="col-sm-12 padding-none">
                                                <input type="text" class="form-control login_input_sign_up" id="txtCompanyDescription" placeholder="" runat="server" tabindex="4" onkeypress="return ValidateAlpha(event);" maxlength="300">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-sm-12 padding-none float-left">

                                        <div class="form-group col-sm-12">
                                            <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">Address</label>
                                            <div class="col-sm-12 padding-none">
                                                <textarea type="text" class="form-control login_input_sign_up" id="txtAddress" placeholder="" runat="server"  rows="1" tabindex="5" maxlength="20"></textarea>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-sm-12 padding-none float-left">

                                        <div class="form-group col-sm-4 float-left">
                                            <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">Contact No</label>
                                            <div class="col-sm-12 padding-none">

                                                <asp:TextBox class="form-control login_input_sign_up" ID="txtCompanyPhoneNo1" placeholder="" runat="server" TabIndex="6" maxlength="20"></asp:TextBox>
                                            </div>

                                        </div>

                                        <div class="form-group float-left col-sm-4">
                                            <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">Alternate Number</label>
                                            <div class="col-sm-12 padding-none">
                                                <asp:TextBox ID="txtCompanyPhoneNo2" runat="server" class="form-control login_input_sign_up" placeholder="" TabIndex="7" maxlength="20"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group float-left col-sm-4">
                                            <label for="inputEmail3" class=" padding-none float-left col-sm-12 control-label custom_label_answer">FAX No</label>
                                            <div class="col-sm-12 padding-none">

                                                <asp:TextBox class="form-control login_input_sign_up" ID="txtFaxNo1" placeholder="" runat="server" TabIndex="8" maxlength="20"></asp:TextBox>
                                            </div>

                                        </div>

                                        <%-- <div class="form-group float-left col-sm-3">
                                            <label for="inputEmail3" class=" padding-none float-left col-sm-12 control-label custom_label_answer">Alternate FAX No</label>
                                            <div class="col-sm-12 padding-none">
                                                <asp:TextBox ID="txtFaxNo2" runat="server" class="form-control " placeholder="" TabIndex="10" onkeypress="return ValidatePhoneNo(event);" MaxLength="10"></asp:TextBox>
                                            </div>
                                        </div>--%>
                                    </div>

                                    <div class="col-sm-12 padding-none">

                                        <%--<div class="form-group float-left col-sm-12">
                                            <label for="inputEmail3" class=" padding-none float-left col-sm-12 control-label custom_label_answer">Address</label>
                                            <div class="col-sm-12 padding-none">
                                                <textarea type="text" class="form-control login_input_sign_up" id="txtAddress" placeholder="" runat="server" tabindex="25" rows="1"></textarea>
                                            </div>
                                        </div>--%>
                                        <%-- <div class="form-group float-left col-sm-4">
                                        <label for="inputEmail3" class=" padding-none float-left col-sm-12 control-label custom_label_answer">Location</label>
                                        <div class="col-sm-12 padding-none">
                                            <input type="text" class="form-control login_input_sign_up" id="txtLocation" placeholder="" runat="server" tabindex="8" onkeypress="return nospaces(event);" onkeydown="return RestrictEnterKey(event);">
                                        </div>
                                    </div>--%>
                                    </div>
                                    <div class="col-sm-12 padding-none">
                                    </div>


                                    <div class="col-sm-12 padding-none">
                                        <div class="form-group float-left col-sm-4  padding-none">
                                            <div class="form-group float-left col-sm-12">
                                                <label for="inputEmail3" class=" padding-none float-left col-sm-12 control-label custom_label_answer">City</label>
                                                <div class="col-sm-12 padding-none">
                                                    <input type="text" class="form-control login_input_sign_up" id="txtCity" placeholder="" runat="server" tabindex="9" onkeypress="return nospaces(event);" onkeydown="return RestrictEnterKey(event);">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group float-left col-sm-4 padding-none">
                                            <div class="form-group float-left col-sm-12">
                                                <label for="inputEmail3" class=" padding-none float-left col-sm-12 control-label custom_label_answer">State</label>
                                                <div class="col-sm-12 padding-none">
                                                    <input type="text" class="form-control login_input_sign_up" id="txtState" placeholder="" runat="server" tabindex="10" onkeypress="return nospaces(event);" onkeydown="return RestrictEnterKey(event);">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group float-left col-sm-4  padding-none">
                                            <div class="form-group float-left col-sm-12">
                                                <label for="inputEmail3" class=" padding-none float-left col-sm-12 control-label custom_label_answer">Country</label>
                                                <div class="col-sm-12 padding-none">
                                                    <input type="text" class="form-control login_input_sign_up" id="txtCountry" placeholder="" runat="server" tabindex="11" onkeypress="return nospaces(event);" onkeydown="return RestrictEnterKey(event);">
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="col-sm-12 padding-none">
                                        <div class="form-group float-left col-sm-4  padding-none">
                                            <div class="form-group float-left col-sm-12">
                                                <label for="inputEmail3" class=" padding-none float-left col-sm-12 control-label custom_label_answer">Pincode</label>
                                                <div class="col-sm-12 padding-none">
                                                    <input type="text" class="form-control login_input_sign_up" id="txtPincode" placeholder="" runat="server" tabindex="12" onkeypress="return nospaces(event);" onkeydown="return RestrictEnterKey(event);" maxlength="20">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group float-left col-sm-4">
                                            <label for="inputEmail3" class=" padding-none float-left col-sm-12 control-label custom_label_answer">Email</label>
                                            <div class="col-sm-12 padding-none">
                                                <input type="text" class="form-control login_input_sign_up" id="txtEmailID" placeholder="" runat="server" tabindex="13" onkeypress="return nospaces(event);" onkeydown="return RestrictEnterKey(event);" maxlength="50">
                                            </div>
                                        </div>
                                        <div class="form-group float-left col-sm-4">
                                            <label for="inputEmail3" class=" padding-none float-left col-sm-12 control-label custom_label_answer">Website</label>
                                            <div class="col-sm-12 padding-none">
                                                <input type="text" class="form-control login_input_sign_up" id="txtWebsite" placeholder="" runat="server" tabindex="14" onkeypress="return nospaces(event);" onkeydown="return RestrictEnterKey(event);" maxlength="50">
                                            </div>
                                        </div>


                                    </div>

                                </div>

                            </div>
                            <div class="col-lg-2 float-left col-12 col-sm-12 col-md-12">

                                <div class="profile_bg float-left col-lg-12 col-12 col-sm-12 col-md-12">
                                    <asp:Image ID="imgEmpImage" runat="server" class="img-fluid" align="center" Width="200px" Height="50px" ToolTip="Company logo" /><%--<img src="../Images/UserLogin/profile_bg.png"  style="margin-top: 8px;"alt="Smiley face"/>--%>
                                </div>
                                <div class="file upload-button btn  btn-primary col-sm-12 col-12 col-lg-12 col-md-12" id="dvFup" runat="server">Upload  logo<asp:FileUpload class="upload-button_input" ID="FileUpload1" runat="server" onchange="showimagepreview(this)" accept=".png,.jpg,.jpeg,.gif" ToolTip="Click to Upload Photo" /></div>

                                <div>
                                </div>
                            </div>
                </div>
                <div class="col-lg-12 padding-none">
                    <div class="form-group">
                        <div class="col-sm-12 padding-none float-left text-right">
                           
                                <asp:UpdatePanel ID="UpdatePanelBtns" runat="server" UpdateMode="Always">
                                    <ContentTemplate>                                       
                                        <asp:Button ID="btnEdit" CssClass=" btn btn-signup_popup" runat="server" Text="Edit" ToolTip="click to Modify Company Details" Visible="false" OnClick="btnEdit_Click" />
                                         <asp:Button type="button" class=" btn btn-signup_popup" runat="server" ID="btnSubmit" Text="Submit" TabIndex="29" Visible="false" OnClick="btnSubmit_Click" OnClientClick="javascript:return emailURLvalidation();"></asp:Button>
                                       <asp:Button ID="btnUpdate" CssClass="btn btn-signup_popup" runat="server" Text="Update" ToolTip="click to Update Company Details" Visible="false" OnClick="btnUpdate_Click" OnClientClick="javascript:return emailURLvalidation();" />
                                        <asp:Button ID="btnCancel" CssClass=" btn btn-cancel_popup" runat="server" Text="Cancel" ToolTip="click to Cancel" Visible="false" OnClick="btnCancel_Click" />

                                        </ContentTemplate>

                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnUpdate" />
                                        <asp:PostBackTrigger ControlID="btnSubmit" />
                                         <asp:PostBackTrigger ControlID="btnEdit" />
                                    </Triggers>
                                </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="Panel_Footer_Message_Label">
            <asp:UpdatePanel ID="UPLblMsgMessage" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <asp:Label ID="lblMsgMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />
    

    <script>      
        $(function () {
            $('#<%=txtStartDate.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                useCurrent: true,
            });
        });
    </script>

   
   
   <%-- </a>--%>
   
   
   
</asp:Content>
