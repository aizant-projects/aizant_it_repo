﻿using AizantIT_PharmaApp.Common;
using System;
using System.Web.UI.WebControls;
using System.Data;
using UMS_BusinessLayer;


namespace AizantIT_PharmaApp.UserManagement
{
    public partial class EmployeeAssignRoles : System.Web.UI.Page
    {
        UMS_BAL objUMS_BAL;
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitializeThePage();
            }
        }
        //DepartmentList binding 
        public void BindDepartments()
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                DataTable dt = objUMS_BAL.BindDepartmentsBAL();
                ddlDepartment.DataSource = dt;
                ddlDepartment.DataTextField = "DepartmentName";
                ddlDepartment.DataValueField = "DeptID";
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("-- Select Department --", "0"));
                ddlEmployeeList.Items.Insert(0, new ListItem("- Select Employee -", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DeptEmp_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DeptEmp_M1:" + strline + "  " + strMsg, "error");
            }
        }
        //modulesList binding
        private void bindModules()
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                DataTable dt = objUMS_BAL.GetModules();
                if (dt.Rows.Count > 0)
                {
                    ddlModulesList.DataSource = dt;
                    ddlModulesList.DataTextField = "ModuleName";
                    ddlModulesList.DataValueField = "ModuleID";
                    ddlModulesList.DataBind();
                    ddlModulesList.Items.Insert(0, new ListItem("-- All Module --", "0"));
                    ddlRoleList.Items.Insert(0, new ListItem("--All Roles--", "0"));
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EmpModule_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EmpModule_M2:" + strline + "  " + strMsg, "error");
            }
        }
        //EmployeeList binding Based on Dept'ID
        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                ddlEmployeeList.Enabled = false;
                int IsActive = 1;//it's using in-Active employee Stop lodging
                int SelectedDepartmentID = int.Parse(ddlDepartment.SelectedItem.Value);
                if (SelectedDepartmentID > 0)
                {
                    DataTable dtemp = objUMS_BAL.GetEmployess(SelectedDepartmentID.ToString(),IsActive);
                    ddlEmployeeList.DataSource = dtemp;
                    ddlEmployeeList.DataValueField = "EmpID";
                    ddlEmployeeList.DataTextField = "Name";
                    ddlEmployeeList.DataBind();
                    ddlEmployeeList.Items.Insert(0, new ListItem("- Select Employee -", "0"));
                    ddlEmployeeList.Enabled = true;
                    upModuleRole.Update();
                    ddlModulesList.SelectedIndex = 0;
                    ddlModulesList.Enabled = false;
                    ddlRoleList.SelectedIndex = 0;
                    ddlRoleList.Enabled = false;
                }
                else
                {
                    upModuleRole.Update();
                    ddlEmployeeList.SelectedIndex = 0;
                    ddlModulesList.SelectedIndex = 0;
                    ddlModulesList.Enabled = false;
                    ddlRoleList.SelectedIndex = 0;
                    ddlRoleList.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Employee_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Employee_M3:" + strline + "  " + strMsg, "error");
            }
        }
        //Particular Employee based on  ModuleList will be Visible
        protected void ddlEmployeeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlModulesList.Enabled = false;
            int SelectedEmployeeID = int.Parse(ddlEmployeeList.SelectedItem.Value);
            if (SelectedEmployeeID > 0)
            {
                upModuleRole.Update();
                ddlModulesList.SelectedIndex = 0;
                ddlModulesList.Enabled = true;
                ddlRoleList.SelectedIndex = 0;
                ddlRoleList.Enabled = false;
            }
            else
            {
                upModuleRole.Update();
                ddlModulesList.SelectedIndex = 0;
                ddlModulesList.Enabled = false;
                ddlRoleList.SelectedIndex = 0;
                ddlRoleList.Enabled = false;
            }
        }
        //Module Selection 
        protected void ddlModulesList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                ddlRoleList.Enabled = false;
                int SelectedModuleID = int.Parse(ddlModulesList.SelectedValue);
                if (SelectedModuleID > 0)
                {
                    DataTable dtemp = objUMS_BAL.GetModuleRolesonModuleId(SelectedModuleID.ToString());
                    ddlRoleList.DataSource = dtemp;
                    ddlRoleList.DataValueField = "RoleID";
                    ddlRoleList.DataTextField = "RoleName";
                    ddlRoleList.DataBind();
                    ddlRoleList.Items.Insert(0, new ListItem("--All Roles--", "0"));
                    ddlRoleList.SelectedValue = "0";
                    ddlRoleList.Enabled = true;
                }
                else
                {                  
                    ddlRoleList.SelectedValue = "0";
                    ddlRoleList.Enabled = false;                   
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EmpRoles_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EmpRoles_M4:" + strline + "  " + strMsg, "error");
            }
        }
        //Clear all fields
        protected void btnReset_Click(object sender, EventArgs e)
        {
            int id =Convert.ToInt32(hdEmployeeId.Value==""?"0": hdEmployeeId.Value);
            if (id > 0)
            {
                upModuleRole.Update();
                upDeptEmp.Update();
                ddlModulesList.SelectedIndex = 0;
                ddlModulesList.Enabled = true;
                ddlRoleList.SelectedIndex = 0;
                ddlRoleList.Enabled = false;
                
            }
            else
            {
                upModuleRole.Update();
                upDeptEmp.Update();
                ddlDepartment.SelectedIndex = 0;
                ddlEmployeeList.SelectedIndex = 0;
                ddlEmployeeList.Enabled = false;
                ddlModulesList.SelectedIndex = 0;
                ddlModulesList.Enabled = false;
                ddlRoleList.SelectedIndex = 0;
                ddlRoleList.Enabled = false;
            }

        }
        //Initialize method Get and Bind Employeelist elements 
        private void InitializeThePage()
        {
            try
            {
                BindDepartments();
                bindModules();

                if (Request.QueryString["E"] != null && Request.QueryString["D"] != null&& Request.QueryString["S"] != null)
                {
                    ddlDepartment.SelectedValue = Request.QueryString["D"].ToString();

                    var employeeId = Request.QueryString["E"].ToString();
                    DataTable dtemp = objUMS_BAL.GetEmployess(Request.QueryString["D"].ToString());

                    ddlEmployeeList.DataValueField = "EmpID";
                    ddlEmployeeList.DataTextField = "Name";
                    ddlEmployeeList.DataSource = dtemp;
                    ddlEmployeeList.DataBind();

                    ddlDepartment.Enabled = false;
                    ddlEmployeeList.SelectedValue = Request.QueryString["E"].ToString();
                    ddlEmployeeList.Enabled = false;
                    ddlModulesList.SelectedIndex = 0;
                    ddlModulesList.Enabled = true;
                    ddlRoleList.SelectedIndex = 0;
                    ddlRoleList.Enabled = false;
                    hdEmployeeId.Value = ddlEmployeeList.SelectedValue;
                    hdEmployeeStatus.Value= Request.QueryString["S"].ToString();
                    if (Request.QueryString["S"].ToString() == "In-Active")
                    {
                        btnReset.Visible = false;
                        ddlModulesList.SelectedIndex = 0;
                        ddlModulesList.Enabled = false;
                    }
                }
                else
                {
                    ddlEmployeeList.SelectedIndex = 0;
                    ddlEmployeeList.Enabled = false;
                    ddlModulesList.SelectedIndex = 0;
                    ddlModulesList.Enabled = false;
                    ddlRoleList.SelectedIndex = 0;
                    ddlRoleList.Enabled = false;
                }

            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Employee_M5:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Employee_M5:" + strline + "  " + strMsg, "error");
            }
        }   
    }
}