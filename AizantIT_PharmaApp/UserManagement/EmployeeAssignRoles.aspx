﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeeAssignRoles.aspx.cs" Inherits="AizantIT_PharmaApp.UserManagement.EmployeeAssignRoles" MasterPageFile="~/MasterPages/UMS_Master/NestedUMS_Master.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .divRolesDeptView {
            border-top: 1px solid #07889a;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hdEmployeeId" runat="server" />
     <asp:HiddenField ID="hdEmployeeStatus" runat="server" Value="Active" />

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class=" col-lg-3 padding-none">
        <div class="closebtn"><span>&#9776;</span></div>
        <div style="display: none;" class="closebtn1"><span>&#9776;</span></div>
    </div>
    <asp:Button ID="btnBackToEmployeeList" runat="server" class=" float-right  btn-signup_popup" PostBackUrl="~/UserManagement/EmployeeList.aspx" Text="Employee List" />
    <div class=" col-12 col-lg-12 col-md-12 col-sm-12 col-12  padding-none float-left">
        <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-12 float-left padding-none grid_assgin_nav ">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 padding-none grid_border float-left">
                <div class="col-lg-12 dashboard_panel_header">Assign Employee Role</div>
                <asp:UpdatePanel ID="upDeptEmp" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="col-lg-4 bottom top  float-left">
                            <asp:Label ID="lbldepartment" CssClass="label-style" runat="server">Department<span class="smallred_label">*</span></asp:Label>
                            <asp:DropDownList ID="ddlDepartment" CssClass="form-control drop_down selectpicker regulatory_dropdown_style" data-size="7" data-live-search="true" runat="server" AutoPostBack="true" Width="100%" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" onchange="DeptSelect();"></asp:DropDownList>
                        </div>
                        <div class="col-lg-4 bottom  top  float-left">
                            <asp:Label ID="lblEmployeelist" CssClass="label-style" runat="server">Employee<span class="smallred_label">*</span></asp:Label>
                            <asp:DropDownList ID="ddlEmployeeList" CssClass="form-control drop_down selectpicker regulatory_dropdown_style" data-size="7" data-live-search="true" runat="server" AutoPostBack="true" Width="100%" OnSelectedIndexChanged="ddlEmployeeList_SelectedIndexChanged" onchange="EmployeeSelect();"></asp:DropDownList>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <asp:UpdatePanel ID="upModuleRole" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="col-lg-4 bottom top  float-left" style="margin-top: 11px;">
                            <asp:Label ID="lblModulesList" CssClass="label-style" runat="server">Module<span class="smallred_label">*</span></asp:Label>
                            <asp:DropDownList ID="ddlModulesList" CssClass="form-control  drop_down selectpicker regulatory_dropdown_style" data-size="7" data-live-search="true" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlModulesList_SelectedIndexChanged" onchange="ModuleSelect();"></asp:DropDownList>
                        </div>
                        <div class="col-lg-4 bottom   float-left">
                            <span class="label-style">Role<span class="smallred_label">*</span></span>
                            <asp:DropDownList ID="ddlRoleList" CssClass="form-control drop_down selectpicker regulatory_dropdown_style" data-size="7" data-live-search="true" runat="server" Width="100%" AutoPostBack="false" onchange="RoleSelect();"></asp:DropDownList>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <div class="col-lg-4 bottom  float-left" id="divDeptsList">
                    <span class="label-style">Access Departments<span class="smallred_label">*</span></span>
                    <select id="lstAccessDepartment" multiple class="form-control drop_down selectpicker regulatory_dropdown_style" data-size="7" data-live-search="true" title="--Select Department--">
                    </select>
                </div>


                <asp:UpdatePanel ID="upbtn" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="col-lg-4 bottom top  float-left">
                            <button id="btnSubmit" class=" btn-signup_popup top"   type="button" onclick="AssignRole();">Add</button>
                            <asp:Button class=" btn-revert_popup   top" runat="server"  ID="btnReset" Text="Reset" OnClick="btnReset_Click" OnClientClick="clearFields();" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="col-12 col-lg-12 col-md-12 col-sm-12 float-left  padding-none" style="display: none" id="divEmployeeStatus" >
                   <p style="color:red" class="col-12 text-center">Employee is In-Active, Roles cannot be Assigned</p>
                </div>
                <div class="col-12 col-lg-12 col-md-12 col-sm-12 float-left divRolesDeptView padding-none" style="display: none">
                  
                    <div id="divEmployeeAssignRoles">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--On Init-->
    <script src="../AppScripts/EmployeeRoles.js"></script>
    <script>
        //Global Variables RoleId,ModuleId,DepartmentId
        var deleteRoleId = 0;
        var deleteModuleId = 0;
        var deleteDeptId = 0;
        var visible = true;
        var divEmployeeAssignRole ='#divEmployeeAssignRoles';//Display the List of Role in Assign to this Div
        var IsAccessibleDepartments = 0;//it used  check  accessible Department Validation Checking
        var empDepartmentList = [];//Empty Employee DepartmentList
        var empRoleList = [];//Empty Employee RoleList
        var empModuleList = [];//Empty Employee ModuleList
        var ArrayDeptIds=[];
        var DepartmentsBasedOnRoleID;
        $(function () {
            var hdEmployeeId = 0
            hdEmployeeId = $('#<%=hdEmployeeId.ClientID%>').val();//Hidden Field Value get 
            //Hide and Show the EmployeeList Button
            if (hdEmployeeId > 0) {
                $('#<%=btnBackToEmployeeList.ClientID%>').show();
            }
            else {
                $('#<%=btnBackToEmployeeList.ClientID%>').hide();
            }
            if ($('#<%=hdEmployeeStatus.ClientID%>').val() == 'Active') {
                $("#btnSubmit").show();
            }
            else {
                $("#btnSubmit").hide();
                $("#divEmployeeStatus").show();
                visible = false;
            }
            $("#lstAccessDepartment").prop("disabled", true);
            $(".btn[data-id='lstAccessDepartment']").addClass("disabled");
            $('#lstAccessDepartment').selectpicker('val', [0]);
            var ddlAccessDepartmentList = $("#lstAccessDepartment");
            $.ajax({
                type: "GET",
                url: "/UserManagement/WebServicesList.asmx/GetDepartmentsList",//GetRolesBasedOnEmployee ActionMethod in UMSRoles Controller
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var data = JSON.parse(result.d);
                    var deptList = data.DepartmentsList;
                    $("#lstAccessDepartment").html(""); // clear before appending new list
                    for (var i = 0; i < deptList.length; i++) {
                        ddlAccessDepartmentList.append(
                            $('<option></option>').val(deptList[i].DeptID).text(deptList[i].DepartmentName));
                    }
                    $('#lstAccessDepartment').selectpicker('refresh');
                },
                error: function (jqXHR, exception) {
                    alert(exception);
                }
            });
            if (hdEmployeeId > 0) {//if Employee Id Exist When Load the Employee Role Based on EMployeeId
                var moduleId = 0;
                var roleId = 0;
                GetEmployeeRoles(hdEmployeeId, moduleId, roleId, visible, divEmployeeAssignRole);//Bind Employee module Role  wise Departments
                $(".divRolesDeptView").show();
            }
        });

        //Employee Departments Dropdown Select
        function DeptSelect() {
            $(".divRolesDeptView").hide();
            var ddlDepartmentId = $('#<%=ddlDepartment.ClientID%>').val();
            var accessbleDepartmetnList = 0;
            if (ddlDepartmentId == 0) {
                $(divEmployeeAssignRole).empty();
                $(".divRolesDeptView").hide();
                AccessibleDepartmentRefresh(accessbleDepartmetnList);
            }
            else {
                $('#divDeptsList').show();
                $(divEmployeeAssignRole).empty();
                AccessibleDepartmentRefresh(accessbleDepartmetnList);
                $("#lstAccessDepartment").prop("disabled", true);
                $(".btn[data-id='lstAccessDepartment']").addClass("disabled");
            }
        }
        //Employee DropdownList selection
        function EmployeeSelect() {
            var roleID = 0;
            var moduleID = 0;
            var ddlEmployeeId = $('#<%=ddlEmployeeList.ClientID%>').val();
            if (ddlEmployeeId == 0) {
                $('#divDeptsList').show();
                $(".divRolesDeptView").hide();
                $(divEmployeeAssignRole).empty();
                $("#lstAccessDepartment").prop("disabled", true);
                $(".btn[data-id='lstAccessDepartment']").addClass("disabled");
                $('#lstAccessDepartment').selectpicker('val', [0]);
            }
            else {
                $(divEmployeeAssignRole).empty();
                GetEmployeeRoles(ddlEmployeeId, moduleID, roleID, visible, divEmployeeAssignRole);//Bind Employee module Role  wise Departments
                $(".divRolesDeptView").show();
                $("#lstAccessDepartment").prop("disabled", true);
                $(".btn[data-id='lstAccessDepartment']").addClass("disabled");
                $('#lstAccessDepartment').selectpicker('val', [0]);
            }
        }
        //Module DropdownList selection
        function ModuleSelect() {
            var departmentsList = 0;
            $(divEmployeeAssignRole).empty();//clear div tag
            var roleID = 0;//RoleId inti Zero
            var empID = $('#<%=ddlEmployeeList.ClientID%>').val();
            var moduleID = $('#<%=ddlModulesList.ClientID%>').val();
            $('#divDeptsList').show();
            if (moduleID == 0) {
                GetEmployeeRoles(empID, moduleID, roleID, visible, divEmployeeAssignRole);//Bind Employee module Role  wise Departments
                AccessibleDepartmentRefresh(departmentsList);
                $("#lstAccessDepartment").prop("disabled", true);
                $(".btn[data-id='lstAccessDepartment']").addClass("disabled");
            }
            else {
                GetEmployeeRoles(empID, moduleID, roleID, visible, divEmployeeAssignRole);//Bind Employee module Role  wise Departments
                $(".divRolesDeptView").show();
                AccessibleDepartmentRefresh(departmentsList);
                $("#lstAccessDepartment").prop("disabled", true);
                $(".btn[data-id='lstAccessDepartment']").addClass("disabled");
            }
        }
        //Role DropdownList selection
        function RoleSelect() {
            var empID = $('#<%=ddlEmployeeList.ClientID%>').val();
            var moduleID = $('#<%=ddlModulesList.ClientID%>').val();
            var roleID = $('#<%=ddlRoleList.ClientID%>').val();
            var departmentsList = 0;
            if (roleID == 0) {
                $(divEmployeeAssignRole).empty();
                GetEmployeeRoles(empID, moduleID, roleID, visible, divEmployeeAssignRole);//Bind Employee module Role  wise Departments
                AccessibleDepartmentRefresh(departmentsList);
                if ($('#<%=hdEmployeeStatus.ClientID%>').val() == 'Active') {
                    $("#btnSubmit").text("Add");
                } else {
                    $("#btnSubmit").hide();
                }
                $("#lstAccessDepartment").prop("disabled", true);
                $(".btn[data-id='lstAccessDepartment']").addClass("disabled");
            }
            else {
           $(divEmployeeAssignRole).empty();
           GetEmployeeRoles(empID, moduleID, roleID, visible, divEmployeeAssignRole);//Bind Employee module Role  wise Departments
                $("#lstAccessDepartment").prop("disabled", false);
                $(".btn[data-id='lstAccessDepartment']").addClass("disabled");

                var jsonSend = { 'EmpID': parseInt(empID), 'ModuleID': parseInt(moduleID), 'RoleID': parseInt(roleID) };
                AjaxGetDepartmentsBasedOnRoleID(jsonSend);
            }
        }
        //Employee Role Assign Or Update
        function AssignRole() {
            var empID = $('#<%=ddlEmployeeList.ClientID%>').val();
            var moduleID = $('#<%=ddlModulesList.ClientID%>').val();
            var roleID = $('#<%=ddlRoleList.ClientID%>').val();
            var deptIds = $('#lstAccessDepartment').val();
            var deptId = $('#<%=ddlDepartment.ClientID%>').val();
            var errors = [];
                ArrayDeptIds = deptIds.map(function (item) {
                    return parseInt(item);
                });;
             //validation with out any change throw the info msg
             if($("#btnSubmit").text()=='Update')
             {
                 if ((JSON.stringify(ArrayDeptIds) == JSON.stringify(DepartmentsBasedOnRoleID))) 
                    {
                        custAlertMsg("There Are No Changes To Update", "warning");
                        return false;
                    }
                  else
                     {
                   DepartmentsBasedOnRoleID=ArrayDeptIds;
                     }
             }
            
            if (deptId == 0 || deptId == null) {
                errors.push("Select Department.");
            }

            if (empID == 0 || empID == null) {
                errors.push("Select Employee.");
             }

            if (moduleID == 0) {
                errors.push("Select Module.");
            }

            if (roleID == 0 || roleID == null) {
                errors.push("Select Role.");
            }
            //check IsAccessibleDepartments true or false
            if (IsAccessibleDepartments == 0)
            {
                if (deptIds.length == 0)
                {
                    errors.push("Select at least One Access Department.");
                }
            }
            if (errors.length>0)
            {
                 custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            var deptList = [];//empty Department arrray
            deptList = deptIds;//it accesable departments Assign to departmentList
            var strAry = deptList.toString();
            var jsonData = ({
                EmpID: empID,
                ModuleID: moduleID,
                RoleID: roleID,
                DeptList: strAry
            })//json  object
            var moduleName = $('#<%=ddlModulesList.ClientID%> option:selected').text();
            var roleName = $("#<%=ddlRoleList.ClientID%> option:selected").text();
            ShowPleaseWait('show');	
            $.ajax({
                url: "/UserManagement/WebServicesList.asmx/EmpRolesAssign",
                type: "POST",
                data: JSON.stringify(jsonData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result)
                {
                  ShowPleaseWait('hide');	
                    var data = JSON.parse(result.d);//json result store in ''
                    if (data.Message == "Updated")
                    {
                        if ($('#<%=ddlEmployeeList.ClientID%>').val() == '<%=(Session["UserDetails"] as System.Data.DataSet).Tables[0].Rows[0]["EmpID"].ToString()%>')
                        {
                            custAlertMsg("<b>" + roleName + "</b>" + " Role  is  " + data.Message + " on " + "<b>" + moduleName + "</b>" + " Module <br>You will redirect  to  Login page.", "success","RoleModificationReloadPage();");
                        }
                        else {
                            custAlertMsg("<b>" + roleName + "</b>" + " Role  is  " + data.Message + " on " + "<b>" + moduleName + "</b>" + " Module ", "success", "GetEmployeeRoles(" + empID + "," + moduleID + "," + roleID + "," + visible + ",'"+divEmployeeAssignRole+"');");
                        }
                    }
                    else
                    {
                        DepartmentsBasedOnRoleID=ArrayDeptIds;
                        if ($('#<%=ddlEmployeeList.ClientID%>').val() == '<%=(Session["UserDetails"] as System.Data.DataSet).Tables[0].Rows[0]["EmpID"].ToString()%>')
                        {
                            custAlertMsg("<b>" + roleName + "</b>" + " Role  is  " + data.Message + " on " + "<b>" + moduleName + "</b>" + " Module.<br>You will redirect  to  Login page.", "success", "RoleModificationReloadPage();");
                        }
                        else {
                            custAlertMsg("<b>" + roleName + "</b>" + " Role is   " + data.Message + "  on  " + "<b>" + moduleName + " </b>" + " Module ", "success", "GetEmployeeRoles(" + empID + "," + moduleID + "," + roleID + "," + visible + ",'"+divEmployeeAssignRole +"');");
                        }
                    }
                },
                error: function (jqXHR, exception)
                {
                    alert(exception);
                }
            });
        }
        //Delete Role Function
        function RoleDelete(roleId, moduleId) {
         var empID = $('#<%=ddlEmployeeList.ClientID%>').val();
            if (roleId == 1 && moduleId==1 && ($('#<%=ddlEmployeeList.ClientID%>').val() == '<%=(Session["UserDetails"] as System.Data.DataSet).Tables[0].Rows[0]["EmpID"].ToString()%>' )) {//admin role in ums module con't be deleted
                custAlertMsg("Admin Role Removal is denied ","error");
                return false;
            }
          
            deleteRoleId = roleId;
            deleteModuleId = moduleId;
            var roleName = RoleName(deleteRoleId) //RoleName function it used to get the Role Name
            var moduleName = ModuleName(deleteModuleId);
            var jsonSend = ({ 'EmpID': parseInt(empID), 'ModuleID': parseInt(moduleId), 'RoleID': parseInt(roleId) })
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                data: jsonSend,
                dataType: "json",
                url: "/UserManagement/WebServicesList.asmx/RoleDeleteChecking", //RoleDepartmentsDelete webMethod in /UserManagement/WebServicesList.asmx
                success: function (result) {
                    var data = JSON.parse(result.d);
                    if (data.Message == "Success") {
                        custAlertMsg("Are you sure do you want to Remove the " + "<strong>" + roleName + "</strong>" + " Role on " + "<strong>" + moduleName + "</strong>" + "  Module?", "confirm", "ConfirmRoleDelete(" + deleteRoleId + "," + deleteModuleId + ");");
                    }
                    else {
                        custAlertMsg("<b>" + deptName + "</b>" + " Department already Removed on " + "<b>" + roleName + "</b >" + "  Role ", "confirm", "ReloadDepartmentAfterConfirm();");
                    }
                }
            });
        }
        //Confirmation Delete Role
        function ConfirmRoleDelete(roleId, moduleId) {
            deleteRoleId = roleId;
            deleteModuleId = moduleId;
            var empID = $('#<%=ddlEmployeeList.ClientID%>').val();
            var jsonSend = ({ 'EmpID': parseInt(empID), 'ModuleID': parseInt(moduleId), 'RoleID': parseInt(roleId) })
            var roleName = RoleName(roleId);//Get RoleName Based on RoleId
            var moduleName = ModuleName(moduleId);
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                data: jsonSend,
                dataType: "json",
                url: "/UserManagement/WebServicesList.asmx/EmployeeRoleDelete",
                success: function (result) {
                    var data = JSON.parse(result.d);
                    if (data.Message == "Success") {
                        if ($('#<%=ddlEmployeeList.ClientID%>').val() == '<%=(Session["UserDetails"] as System.Data.DataSet).Tables[0].Rows[0]["EmpID"].ToString()%>')
                        {
                            custAlertMsg("<b>" + roleName + " </b>" + " Role  removed on <b>" + " " + moduleName + "</b> " + " Module.<br>You will redirect  to  Login page.", "success","RoleModificationReloadPage();");
                        }
                        else
                        {
                            custAlertMsg("<b>" + roleName + " </b>" + " Role  removed on <b>" + " " + moduleName + "</b> " + " Module", "success", "AfterDeleteRoleGetModuleWiseDepartments(" + empID + ");");//Custome Success Alert message
                        }
                    }
                    else {
                        custAlertMsg("<b>" + roleName + " </b>" + " Role deletion failed", "error");
                    }
                },
            });
        }
        //After Deleteing the Role Get All data Module Wise
        function AfterDeleteRoleGetModuleWiseDepartments(empId) {
            $(divEmployeeAssignRole).empty();
            var roleHeaderMainID = '#moduleHeader' + deleteModuleId + 'roleHeader' + deleteRoleId;
            if (roleHeaderMainID != "") {
                $(roleHeaderMainID).remove();
            }
            var roleId = $('#<%=ddlRoleList.ClientID%>').val();//Get selected Role Id
            var moduleID = $('#<%=ddlModulesList.ClientID%>').val();//Get Selected ModuleId
            var deptList = 0;
            GetEmployeeRoles(empId, moduleID, roleId, visible, divEmployeeAssignRole);//Bind Employee module Role  wise Departments
            AccessibleDepartmentRefresh(deptList);//refresh the Accesble departmentList
        }
        //Role Accessble Departments Deleted
        function DeleteRoleDepartments(deptId, roleId, moduleId) {
            deleteModuleId = moduleId;
            deleteDeptId = deptId;
            deleteRoleId = roleId;
            var empID = $('#<%=ddlEmployeeList.ClientID%>').val();
            var deptName = DepartmentName(deptId);
            var roleName = RoleName(roleId);
            var roleDeptMainID = '#ulModule' + deleteModuleId + 'ulRoleDepts' + deleteRoleId; //RolesDepartment Ul Id
            var roleDeptCount = 0;//roleDepcount is used to count the Departments
            var liRoleDept = 0;
            $(roleDeptMainID).each(function () {// RoleDepartmentMainId of Ul
                liRoleDept = $(this).find('li')//get each roleDepartments in RoleDepartmentMainId
            })
            roleDeptCount = liRoleDept.length;
            if (roleDeptCount == 1) {
                custAlertMsg("Select At Least one Department before Remove Departments", "error");
                return false;
            }
            else {
                //Department Exist or Not 
                var jsonSend = ({ 'EmpID': parseInt(empID), 'ModuleID': parseInt(moduleId), 'RoleID': parseInt(roleId), 'DeptID': parseInt(deptId) })
                $.ajax({
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    data: jsonSend,
                    dataType: "json",
                    url: "/UserManagement/WebServicesList.asmx/RoleDepartmentsChecking", //RoleDepartmentsDelete webMethod in /UserManagement/WebServicesList.asmx
                    success: function (result) {
                        var data = JSON.parse(result.d);
                        if (data.Message == "Success") {
                            custAlertMsg("Are you sure do you want to Remove the  " + "<b>" + deptName + "</b>" + " Department ?", "confirm","ConfirmDeleteRoleDepartments();");
                        }
                        else {
                            custAlertMsg("<b>" + deptName + "</b>" + " Department already Removed on " + "<b>" + roleName + "</b >" + "  Role ", "confirm", "ReloadDepartmentAfterConfirm();");
                        }
                    }
                });
            }
        }
        //Confirm Delete Role Departments
        function ConfirmDeleteRoleDepartments() {
            var roleModuleId = deleteModuleId;
            var roleroleId = deleteRoleId;
            var empID = $('#<%=ddlEmployeeList.ClientID%>').val();
            var deptName = DepartmentName(deleteDeptId);//DepartmentName function is used to get the Department Name based on DeptId
            var roleName = RoleName(deleteRoleId);//RoleName function is used to get the Department Name based on RoleId
            var jsonSend = ({ 'EmpID': parseInt(empID), 'ModuleID': parseInt(roleModuleId), 'RoleID': parseInt(roleroleId), 'DeptID': parseInt(deleteDeptId) })
            var roleID = $('#<%=ddlRoleList.ClientID%>').val();
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                data: jsonSend,
                dataType: "json",
                url: "/UserManagement/WebServicesList.asmx/RoleDepartmentsDelete", //RoleDepartmentsDelete webMethod in /UserManagement/WebServicesList.asmx
                success: function (result) {
                    var data = JSON.parse(result.d);
                    if (data.Message == "Success") {
                        var jsonSend = { 'EmpID': parseInt(empID), 'ModuleID': parseInt(roleModuleId), 'RoleID': parseInt(roleroleId) };
                        AjaxGetDepartmentsBasedOnRoleID(jsonSend);
                        if ($('#<%=ddlEmployeeList.ClientID%>').val() == '<%=(Session["UserDetails"] as System.Data.DataSet).Tables[0].Rows[0]["EmpID"].ToString()%>')
                        {
                            custAlertMsg("<b>" + deptName + "</b>" + " Department Removed  on " + "<b>" + roleName + "</b >" + "  Role.</br>You will redirect  to  Login page.", "success","RoleModificationReloadPage();");
                        }
                        else
                        {
                            custAlertMsg("<b>" + deptName + "</b>" + " Department Removed  on " + "<b>" + roleName + "</b >" + "  Role ", "success", "AfterDeleteRoleDepartmentsRemoveLi();");
                        }
                    }
                    if (roleID != 0) {
                        $(divEmployeeAssignRole).empty();
                        GetEmployeeRoles(empID, roleModuleId, roleroleId, visible, divEmployeeAssignRole);
                    }
                }
            });
        }
        //RoleDepartment Delete succesfully Remove Li element
        function AfterDeleteRoleDepartmentsRemoveLi() {
            var deptElementsID = '#liModule' + deleteModuleId + 'liRole' + deleteRoleId + 'liDept' + deleteDeptId;//Role Departments Li Id
            if (deptElementsID != "") {
                $(deptElementsID).remove();//this function is used to delete the role departments
            }
        }
        //delete RoleDepartment After Confirm 
        function ReloadDepartmentAfterConfirm() {
            var empID = $('#<%=ddlEmployeeList.ClientID%>').val();
            var moduleID = $('#<%=ddlModulesList.ClientID%>').val();
            var roleID = $('#<%=ddlRoleList.ClientID%>').val();
            $(divEmployeeAssignRole).empty();//clear div tag
            GetEmployeeRoles(empID, moduleID, roleID, visible, divEmployeeAssignRole);
        }

        //Get DepartmentName Based on DeptId Id
        function DepartmentName(deptId) {
            var deptName = '';
            $.each(empDepartmentList, function (i, item) {
                if (deptId == item.DeptID) {
                    deptName = item.DepartmentName;//if condition is true Department Name store in 'deptName' variable
                }
            });
            return deptName;
        }
        //Get RoleName Based on Role Id
        function RoleName(roleId) {
            var roleName = '';
            $.each(empRoleList, function (i, item) {
                if (roleId == item.RoleID) {
                    roleName = item.RoleName;//if condition is true Role Name store in 'roleName' variable
                }
            });
            return roleName;
        }
        //Get Module Name Based on Module Id
        function ModuleName(moduleId) {
            var moduleName = '';
            $.each(empRoleList, function (i, item) {
                if (moduleId == item.ModuleID) {
                    moduleName = item.ModuleName;//if condition is true Role Name store in 'MOdule Name' variable
                }
            });
            return moduleName;
        }
        //Clear the All the fields
        function clearFields() {
            var lstdept = 0;
            $(divEmployeeAssignRole).empty();
            AccessibleDepartmentRefresh(lstdept);
            $("#lstAccessDepartment").prop("disabled", true);
            $('#divDeptsList').show();
            $(".divRolesDeptView").hide();
            var empId = $('#<%=hdEmployeeId.ClientID%>').val();
            var moduleId = 0;
            var roleId = 0;
            if (empId != 0) {
                GetEmployeeRoles(empId, moduleId, roleId, visible, divEmployeeAssignRole);
                $(".divRolesDeptView").show();
            }
        }
        function RoleModificationReloadPage() {
            var Url = '<%= ResolveUrl("~/Common/WebServices/RemoveUserSession.asmx/Remove_UserSession")%>';
            LogOut(Url);
        }
         function AjaxGetDepartmentsBasedOnRoleID(jsonSend) {
            $.ajax({
                type: "GET",
                url: "/UserManagement/WebServicesList.asmx/GetDepartmentsBasedOnRoleID",//GetDepartmentsBasedOnRoleID 
                data: jsonSend,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                   var  DeptIds = JSON.parse(result.d);
                    DepartmentsBasedOnRoleID = DeptIds.DepartmentsIDs;
                    }
            });
        }
    </script>

</asp:Content>

