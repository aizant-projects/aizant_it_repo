﻿using System;
using System.Web.UI;
using System.Data;
using UMS_BusinessLayer;

namespace AizantIT_PharmaApp.UserManagement
{
    public partial class ForgotPassword : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                lblMsg.Text = "";
                txtLoginID.Focus();
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {   
            try
            {
                UMS_BAL objUMS_BAL = new UMS_BAL();
               
                if (txtEmailID.Value.Trim() == ""||txtLoginID.Value.Trim() == "")
                {
                    lblMsg.Text = "EmailID and LoginID Should not be empty";
                }
                else
                {
                    string LoginID = "";
                    string EmailID = "";
                    int EmpID =0;
                    string SecurityAnswer = "";
                    string NewPassword = "";
                    switch (hdnStep.Value)
                    {
                        case "0": //step1
                            LoginID= txtLoginID.Value.Trim();
                            EmailID = txtEmailID.Value.Trim();

                            DataTable dt = objUMS_BAL.ForgotPassword(LoginID, EmailID, "Step1", EmpID, SecurityAnswer, NewPassword);
                            if (dt.Rows.Count > 0)
                            {
                                DataRow dr = dt.Rows[0];
                                HFEmpID.Value = dr["EmpID"].ToString();
                                txtSecurityQues.Value = dr["Question"].ToString();

                                hdnStep.Value = "1";
                                txtLoginID.Disabled = true;
                                txtEmailID.Disabled = true;
                                divStep2.Visible = true;
                                lblMsg.Text = "";
                            }
                            else
                            {
                                lblMsg.Text = "Invalid EmaiID or LoginID";
                            }
                                break;
                            
                    case "1": //step2
                            EmpID = Convert.ToInt32(HFEmpID.Value);
                            SecurityAnswer = txtUserAnswer.Value.Trim();
                            DataTable dt2 = objUMS_BAL.ForgotPassword(LoginID, EmailID, "Step2", EmpID, SecurityAnswer, NewPassword);
                            if (dt2.Rows.Count > 0)
                            {
                                DataRow dr = dt2.Rows[0];
                                int Result =Convert.ToInt32(dr["Status"].ToString());
                                if (Result == 1)
                                {
                                    divStep3.Visible = true;
                                    hdnStep.Value = "2";
                                    btnSubmit.Text = "Submit";
                                    txtUserAnswer.Disabled = true;
                                    lblMsg.Text = "";
                                }
                                else if(Result==0)
                                {
                                    lblMsg.Text = "Not a valid answer";
                                }
                            }
                            break;
                        case "2": //step3
                            EmpID = Convert.ToInt32(HFEmpID.Value);
                            NewPassword = txtNewPwd.Value.Trim();
                            DataTable dt3 = objUMS_BAL.ForgotPassword(LoginID, EmailID, "Step3", EmpID, SecurityAnswer, NewPassword);
                            if (dt3.Rows.Count > 0)
                            {
                                DataRow dr = dt3.Rows[0];
                                int Result = Convert.ToInt32(dr["Status"].ToString());
                                if (Result == 4)
                                {
                                    lblMsg.Text = "New Password Shouldn't be a Previous 3 Password's";
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Script", "Sucessmsg();", true);
                                    lblMsg.Text = "";
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }                               
            }
            catch (Exception ex)
            {
                ErrorMsg(ex);
            }
        }
        private void JsMethod(string msg)
        {
            msg = msg.Replace("'", "");
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "alert", "alert('" + msg + "');", true);
        }
        private void ErrorMsg(Exception ex)
        {
            lblMsg.Text = ex.Message;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UserLogin.aspx");
        }
    }
}