﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UMS_Master/NestedUMS_Master.master" AutoEventWireup="true" CodeBehind="EmployeeHistoryLogin.aspx.cs" Inherits="AizantIT_PharmaApp.UserManagement.EmployeeHistoryLogin" %>
  
 
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%-- <link href="<%=ResolveUrl("~/AppCSS/PMS/columndatatablebuttons.css")%>" rel="stylesheet" />--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <style>
        .container {
            width: 1390px;
        }

        .table-text-center th, .table-text-center td {
            text-align: center;
        }

        .dataTables_scroll {
            overflow: auto;
        }
      
    </style>
<%--  <script src="<%=ResolveUrl("~/AppScripts/PMS/colvisdatatable.js")%>"></script>--%>
 
<!--For Viewing Grid-->
      <div class=" col-lg-3 padding-none">
        <div class="closebtn"><span>&#9776;</span></div>
        <div style="display: none;" class="closebtn1">
            <span>&#9776;</span>
        </div>
    </div>
   <div>
        <div class=" col-lg-12 col-sm-12 col-md-12 col-12 padding-none float-left">
            <div class=" col-lg-12 col-sm-12 col-md-12 col-12 grid_area padding-none float-left">
                <div class="col-lg-12 col-sm-12 col-md-12 col-12 padding-none float-left">
                
                        <div id="header">
                            <div class="col-lg-12 col-sm-12 col-md-12 col-12 dashboard_panel_header float-left">
                                   Employee Login History
                            </div>
                        </div>
                        <div id="body" class="col-lg-12 col-sm-12 col-md-12 col-12 float-left " style="overflow: auto; margin-top: 10px">

                            <table id="dtEmpLoginHistoryList" class="table datatable_cust table-striped table-bordered top breakword dtEmpLoginHistoryListClass" cellspacing="0" style="width: 100%">
                                <thead>
                                    <tr id="tablerow">
                                        <th>Emp Code</th> <!--0-->
                                        <th>Employee Name</th><!--1-->
                                        <th>Login ID</th><!--2-->
                                        <th>Login Time</th><!--3-->
                                        <th>Logout Time</th><!--4-->
                                        <th>Server Address</th><!--5-->
                                        <th>Client Address</th><!--6-->
                                    </tr>
                                </thead>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">

    //For Coloumn wise search on Jquery Grid
    $('#dtEmpLoginHistoryList thead tr').clone(true).appendTo('#dtEmpLoginHistoryList thead');
    $('#dtEmpLoginHistoryList thead tr:eq(1) th').each(function (i)
    {
        if (i < 7) {
            var title = $(this).text();
            if (i == 0) {//Emp Code
                $(this).html('<input type="text" placeholder="Search" style="Width:70px;" maxLength="10" />');
            }
            else if (i == 2){//Login ID
                $(this).html('<input type="text" placeholder="Search" style="Width:80px;" maxLength="10" />');
            }
            else if (i == 3) {//Login Time
                $(this).html('<input type="text" placeholder="Search" style="Width:150px;" maxLength="20" />');
            }
            else {
                $(this).html('<input type="text" placeholder="Search" />');
            }

            $('input', this).on('keyup change', function () {
                if (EmployeeLoginHistorytable.column(i).search() !== this.value) { 
                    EmployeeLoginHistorytable
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
            if (i == 4 ||i==5) {
                $(this).html('');
            }
        }
        
    });
    //end


    $('#dtEmpLoginHistoryList').wrap('<div class="dataTables_scroll" />');
    var EmployeeLoginHistorytable = $('#dtEmpLoginHistoryList').DataTable({
        
        columns: [
            { 'data': 'EmpCode' },//0
            { 'data': 'Employee' },//1
            { 'data': 'LoginID'},//2
            { 'data': 'Login_Time' },//3
            { 'data': 'Logout_Time' },//4
            { 'data': 'UserHostAddress' },//5
            { 'data': 'UserClientAddress'}//6
        ],
        "paging": true,
        "search": {
            "regex": true
        },
        "orderCellsTop": true,
        "scrollY": "500px",
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        "aoColumnDefs": [{ "targets": [4,5], "searchable": false }, { "bSortable": false, "aTargets": [4,5] },
        {
            "className": "dt-body-left", "targets": [2, 3, 4]
        },
        
        ],
       "responsive": true,
        //"bAutoWidth": true,
        "sServerMethod": 'Get',
        "sAjaxSource": '/UserManagement/WebServicesList.asmx/GetEmpLoginHistory',
        "fnServerData": function (sSource, aoData, fnCallback) {
                 $.ajax({
                     "dataType": 'json',
                     "contentType": "application/json; charset=utf-8",
                     "type": "GET",
                     "url": sSource,
                     "data": aoData,
                     "success": function (msg) {
                         var json = jQuery.parseJSON(msg.d);
                         fnCallback(json);
                         $(".dtEmpLoginHistoryListClass").css({ "width": "100%" });
                         $(".dtEmpLoginHistoryListClass").css({ "height": "100%" });

                         UserSessionCheck();
                     },
                     error: function (xhr, textStatus, error) {
                     }
                 });
             }
         });
</script>

<!--End-->
</asp:Content>
