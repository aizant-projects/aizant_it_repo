﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using UMS_BusinessLayer;

namespace AizantIT_PharmaApp.UserManagement
{
    /// <summary>
    /// Summary description for ViewDocument1
    /// </summary>
    public class ViewDocument1 : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            UMS_BAL uMS_BAL = new UMS_BAL();
            string DocID = context.Request.QueryString["DocID"];
           

            DataTable dtSop = uMS_BAL.GetEmployeeDocments(DocID,0, "V");
            if (dtSop.Rows.Count > 0)
            {
                DataRow dr = dtSop.Rows[0];
                //Response.Clear();
                context.Response.Buffer = true;
                context.Response.Charset = "";
                context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                if (dr["FileType"].ToString() == "pdf")
                {
                    context.Response.ContentType = "application/pdf";
                }
                context.Response.BinaryWrite((byte[])dr["DocumentContent"]);
                context.Response.Flush();
                context.Response.End();
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}