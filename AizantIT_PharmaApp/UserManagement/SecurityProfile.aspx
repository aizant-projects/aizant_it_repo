﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UMS_Master/NestedUMS_Master.Master" AutoEventWireup="true" CodeBehind="SecurityProfile.aspx.cs" Inherits="AizantIT_PharmaApp.UserManagement.SecurityProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <link href="<%=ResolveUrl("~/AppCSS/CreateUserStyles.css")%>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/Scripts/bootstrap-multiselect.css") %>" rel="stylesheet" />
     <link href="<%=ResolveUrl("~/Scripts/bootstrap-multiselect.js") %>" rel="stylesheet" />

    <div class=" col-lg-12 col-sm-12  col-md-12 col-12 padding-none">
        <div class="closebtn"><span>&#9776;</span></div>
        <div style="display: none;" class="closebtn1">
            <span>&#9776;</span>

        </div>
    </div>

    <div class="col-sm-12 col-lg-8 col-md-12 col-12 offset-2">
        <div class="row Panel_Frame">
           
                <div class="col-lg-12 col-sm-12  col-md-12 col-12 dashboard_panel_header float-left">
                    Security Profile
                    
                  <%--  <div class="col-sm-6">
                        <asp:HiddenField ID="HFDeptID" runat="server" />
                    </div>--%>
                </div>
            
            <div class="col-12 float-left">
                <div class="row" style="margin-left: 10px; margin-right: 10px">
                    <div class="form-horizontal col-12 float-left">
                        <div class="form-group col-12 float-left">
                            <label for="username" class="col-sm-4 control-label top float-left">Minimum Tenure of Password<span class="smallred_label">*</span></label>
                            <div class="col-sm-5 top float-left">
                                <asp:TextBox CssClass="float-left form-control " style="width:60%" ID="txtTenureofPassword" runat="server" placeholder="Number of Days" onkeypress="return ValidatePhoneNo(event)" TabIndex="1"></asp:TextBox>
                            
                            <div class="col-sm-0 float-left" style="margin-left: 16px;float: left;">
                                Days
                            </div>
                                </div>
                        </div>
                        <div class="form-group  col-12 float-left">
                            <label for="name" class="col-sm-4 float-left control-label">Password Expiry Alert Before<span class="smallred_label">*</span></label>
                            <div class="col-sm-5 float-left">
                                <asp:TextBox  CssClass=" float-left form-control" style="width:60%" name="name" ID="txtPasswordExpiryAlertBefore" runat="server" placeholder="Number of Days" onkeypress="return ValidatePhoneNo(event)" TabIndex="2" onchange="return validate();"></asp:TextBox>
                          
                            <div class="col-sm-0" style="margin-left: 16px;float: left;">
                                Days  
                            </div>
                                  </div>
                        </div>
                        <div class="form-group  col-12 float-left">
                            <label for="name" class="col-sm-4 float-left control-label">No of Invalid Attempts<span class="smallred_label">*</span></label>
                            <div class="col-sm-5 float-left">
                                <asp:TextBox  CssClass=" float-left form-control" style="width:60%" name="name" ID="txtAccountLockOut" runat="server" placeholder="Number" onkeypress="return ValidatePhoneNo(event)" TabIndex="3"></asp:TextBox>
                            
                            <div class="col-sm-0" style="margin-left: 16px;float: left;">
                            </div>
                                </div>
                        </div>
                        <div class="form-group  col-12 float-left">
                            <label for="name" class="col-sm-4 float-left control-label">Session Timeout<span class="smallred_label">*</span></label>
                            <div class="col-sm-5 float-left">
                                <asp:TextBox  CssClass=" float-left form-control" style="width:60%" name="name" ID="txtSessionTimeOut" MaxLength="50" runat="server" placeholder="Minutes" onkeypress="return ValidatePhoneNo(event)" TabIndex="4"></asp:TextBox>
                          
                            <div class="col-sm-0" style="margin-left: 16px;float: left;">
                                Mintues   
                            </div>
                                  </div>
                        </div>
                         <div class="form-group  col-12 float-left" ID="dvComments" runat="server">
                            <label for="name" class="col-sm-4 float-left control-label">Comments<span class="smallred_label">*</span></label>
                            <div class="col-sm-5 float-left">
                            <%--<TextArea  CssClass=" float-left form-control"  ID="txtComments" runat="server"  placeholder="Enter Comments"  style="height: 360px;" TabIndex="5"></TextArea>--%>

                                <asp:TextBox ID="txtComments" TextMode="MultiLine" Rows="3" CssClass="form-control  disablePast" runat="server" TabIndex="5" onkeypress="return this.value.length<=250" ToolTip=" Comments" placeholder="Enter  Comments"></asp:TextBox>
                            <div class="col-sm-0" style="margin-left: 16px;float: left;">
                            </div>
                                </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-sm-12 modal-footer text-right">
                <div class="Panel_Footer_Buttons">
               
                        <asp:UpdatePanel ID="upSecprofile" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <asp:Button ID="btnSubmit" Class="btn-signup_popup" runat="server" Text="Submit" OnClientClick="javascript:return Requiredvaidate();" OnClick="btnSubmit_Click" TabIndex="6" />
                                <asp:Button ID="btnCancel" Class="btn-cancel_popup" runat="server" Text="Cancel" OnClick="btnCancel_Click" TabIndex="7" />
                                <asp:Button ID="btnEdit"  Class="btn-revert_popup" runat="server" Text="Edit" OnClick="btnEdit_Click" TabIndex="8" />
                                <asp:Button ID="btnHistory" Class="btn-signup_popup" runat="server" Text="History"  TabIndex="9" OnClick="btnHistory_Click" />
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnSubmit" />
                                <asp:PostBackTrigger ControlID="btnEdit" />
                            </Triggers>
                        </asp:UpdatePanel>
                    
                </div>
                <div class="Panel_Footer_Message_Label">
                    <asp:Label ID="lblDepartmentError" runat="server" Text="" ForeColor="Red"></asp:Label>
                </div>
            </div>
        </div>
         </div>
     <!--------Comment History View-------------->
    <div id="myCommentHistory" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg" style="width:80%">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel ID="upcomment" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-header">
                            <span id="span4" runat="server"></span>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body" style="max-height: 600px; overflow-y: auto">
                            <div class=" history_grid" style="overflow-y: auto;">
                                <asp:GridView ID="gv_CommentHistory" runat="server" CssClass="table table-hover table-responsive" ClientIDMode="Static" 
                                    EmptyDataText="No Records Found" AutoGenerateColumns="False" PagerSettings-Mode="NumericFirstLast" BackColor="White" 
                                    BorderColor="#d2d2d2" BorderStyle="None" BorderWidth="1px" OnRowDataBound="gv_CommentHistory_RowDataBound" 
                                     AllowPaging="true" PageSize="5" OnPageIndexChanging="gv_CommentHistory_PageIndexChanging" >
                                    <Columns>
                                        <asp:TemplateField HeaderText="User"  ItemStyle-Width="140px" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_GivenBy" runat="server" Text='<%# Eval("GivenBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role" ItemStyle-Width="70"  ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Role" runat="server" Text='<%# Eval("CommentedRole") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" ItemStyle-Width="150px" >
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Date" runat="server" Text='<%# Eval("CommentedDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                       
                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Action" runat="server" Text='<%# Eval("Action") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Submitted Data" ItemStyle-Width="300px" >
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_SubmittedData" runat="server" Text='<%# Eval("SubmittedData") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_shortComments" runat="server" Text='<%# Eval("Comments") %>'></asp:Label>                                               
                                                <asp:TextBox ID="txt_Comments" runat="server" ReadOnly="true" Rows="4" TextMode="MultiLine" Text='<%# Eval("Comments") %>'></asp:TextBox>
                                                <asp:LinkButton ID="lbcommentsmore" runat="server" CommandArgument='<%# Eval("Comments")%>' CommandName="SM" >Show More</asp:LinkButton>
                                                <asp:LinkButton ID="lbcommentsless" runat="server" CommandArgument='<%# Eval("Comments") %>' CommandName="SL" >Show Less</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                     <PagerStyle cssClass="gridpager"  HorizontalAlign="Right" />
                                </asp:GridView>
                            </div>
                        </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!--------End Comment History View-------------->
    <script type="text/javascript">

        //Disable past operation in  Comments 
            $('.disablePast').bind('paste', function (e) {
                e.preventDefault(); //disable cut,copy,paste
            });
         function ShowPleaseWaitGifDisable() {
            ShowPleaseWait('hide');
        }
        function validate() {
            var TenureofPassword = document.getElementById("<%=txtTenureofPassword.ClientID%>").value.trim();
            var PasswordExpiryAlertBefore = document.getElementById("<%=txtPasswordExpiryAlertBefore.ClientID%>").value.trim();
            errors = [];
            if (parseInt(PasswordExpiryAlertBefore) == parseInt(TenureofPassword)) {
                errors.push("Password Expiry Alert Before Less then the Tenure of Password");
            }
            if (parseInt(PasswordExpiryAlertBefore) > parseInt(TenureofPassword)) {
                errors.push("Password Expiry Alert Before Less then the Tenure of Password");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }
        //for required fields
        function Requiredvaidate() {
            var TenureofPassword = document.getElementById("<%=txtTenureofPassword.ClientID%>").value.trim();
            var PasswordExpiryAlertBefore = document.getElementById("<%=txtPasswordExpiryAlertBefore.ClientID%>").value.trim();
            var SessionTimeOut = document.getElementById("<%=txtSessionTimeOut.ClientID%>").value.trim();
            var AccountLockOut = document.getElementById("<%=txtAccountLockOut.ClientID%>").value.trim();
            var Comments = document.getElementById("<%=txtComments.ClientID%>").value.trim();
            errors = [];
            // alert(SessionTimeOut);
            if (TenureofPassword == '') {
                errors.push("Enter Minimum Tenure of Password ");

            }
            if (PasswordExpiryAlertBefore == '') {
                errors.push("Enter Password Expiry Alert Before");

            }
            if (AccountLockOut == '') {
                errors.push("Enter No of Invalid Attempts");

            }
            if (SessionTimeOut == '') {
                errors.push("Enter Session Timeout");

            }
            if (PasswordExpiryAlertBefore != '' && TenureofPassword != '') {
                if (parseInt(PasswordExpiryAlertBefore) > parseInt(TenureofPassword)) {

                    errors.push("Password Expiry Alert Before Less then the Tenure of Password");
                }
                if (TenureofPassword == PasswordExpiryAlertBefore) {
                    errors.push("Password Expiry Alert Before Less then the Tenure of Password");
                }
                if (TenureofPassword < 10 || TenureofPassword > 365) {
                    errors.push("Minimum Tenure of Password should be Minimum 10 days Maximum 365 days.");

                } if (PasswordExpiryAlertBefore < 2) {
                    errors.push("Password Expiry Alert Before should be Minimum 2 days and less than the Password Tenure");

                }
            }

            if (AccountLockOut != '') {

                if (AccountLockOut < 3 || AccountLockOut > 10) {
                    errors.push("Number of Invalid Attempts Should be Minimum 3  and Maximum 10 ");
                }
            }
            if (SessionTimeOut != '') {
                if (SessionTimeOut < 5 || SessionTimeOut > 60) {
                    errors.push("Session Timeout Minimum 5 Minutes and Maximum 60 Minutes");

                }
            }
            if (Comments == '') {
                errors.push("Please Enter Comments.");

            }
            if (errors.length > 0) {
                    custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else
            {
                ShowPleaseWait('show');
                return true;
            }
        }
    </script>

    <script>
        //for clearing the Fields
        function Clear() {
            document.getElementById("<%=txtAccountLockOut.ClientID%>").value = "";
            document.getElementById("<%=txtPasswordExpiryAlertBefore.ClientID%>").value = "";
            document.getElementById("<%=txtSessionTimeOut.ClientID%>").value = "";
            document.getElementById("<%=txtTenureofPassword.ClientID%>").value = "";
        }
        function ReloadSelf() {
            window.open("<%=ResolveUrl("~/UserManagement/SecurityProfile.aspx.aspx")%>", "_self");
        }
    </script>

    <script>
        $("#NavLnkDepartment").attr("class", "active");
        $("#NavLnkCreateDept").attr("class", "active");
    </script>
 <script>
     $(document).ready(function () {
   window.history.replaceState('','',window.location.href)
   });
</script>
<style>
    .gridpager table
    {
        float:right;
    }
    .gridpager, .gridpager td
    {    
        text-align: right;
        color: #38b8ba;  
        background:#efffff;

        font-weight: bold;
        text-decoration: none;
    }
    .gridpager a
    {
        color: White;
        font-weight: normal;
    }
     .gridpager tr td {
                padding: 5px 13px;
    background: #38b8ba;
    border:1px solid #fff;
    color:#000;
        }
</style>
</asp:Content>

