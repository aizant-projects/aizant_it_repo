﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UMS_Master/NestedUMS_Master.master" AutoEventWireup="true" CodeBehind="DashboardUMS.aspx.cs" Inherits="AizantIT_PharmaApp.DashboardUMS" %>
<%@ Register Assembly="DevExpress.Web.Bootstrap.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.Bootstrap" TagPrefix="dx" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<%@ Register Assembly="DevExpress.Web.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="<%=ResolveUrl("~/AppCSS/dashboardstyle.css")%>" rel="stylesheet" />
    <script src="<%=ResolveUrl("~/DevexScripts/jquery-3.2.1.min.js")%>"></script>
    <link href="<%=ResolveUrl("~/DevexScripts/jquery-ui.css")%>" rel="stylesheet" type="text/css" />
    <script src="<%=ResolveUrl("~/DevexScripts/jquery-ui.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/cldr.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/cldr/event.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/cldr/supplemental.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/globalize.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/globalize/currency.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/globalize/date.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/globalize/message.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/globalize/number.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/knockout.js")%>"></script>
    <script type="text/javascript">
        function onClientCustomizePoint(args) {
            var colorValue;
            var arrColors = ["#98AFC7", "#4863A0", "#2B547E", "#0020C2", "#00FFFF", "#307D7E", "#008080", "#728C00", "#254117", "#347235", "#CD7F32", "#317ECC", "#806517",
                "#F75D59", "#FF0000", "#E41B17", "#8C001A", "#810541", "#058246", "#F52887", "#C04000", "#E9AB17", "#3BB9FF", "#7D1B7E", "#7F38EC", "#800517", "#3EA055", "#3090C7", "#98AFC7",
                "#2B65EC", "#7BCCB5", "#54C571", "#B2C248", "#E2A76F", "#7F462C", "#C85A17", "#E77471", "#7D0552", "#E38AAE", "#4E387E", "#C45AEC", "#893BFF", "#B93B8F", "#E67451"];
            switch (args.index) {
                case 0:
                    colorValue = arrColors[10];
                    break;
                case 1:
                    colorValue = arrColors[1];
                    break;
                case 2:
                    colorValue = arrColors[20];
                    break;
                case 3:
                    colorValue = arrColors[5];
                    break;
                case 4:
                    colorValue = arrColors[15];
                    break;
                case 5:
                    colorValue = arrColors[25];
                    break;
                case 6:
                    colorValue = arrColors[30];
                    break;
                case 7:
                    colorValue = arrColors[2];
                    break;
                case 8:
                    colorValue = arrColors[12];
                    break;
                case 9:
                    colorValue = arrColors[22];
                    break;
                case 10:
                    colorValue = arrColors[3];
                    break;
                case 11:
                    colorValue = arrColors[13];
                    break;
                case 12:
                    colorValue = arrColors[23];
                case 13:
                    colorValue = arrColors[4];
                    break;
                case 14:
                    colorValue = arrColors[14];
                    break;
                case 15:
                    colorValue = arrColors[24];
                    break;
                case 16:
                    colorValue = arrColors[6];
                    break;
                case 17:
                    colorValue = arrColors[16];
                case 18:
                    colorValue = arrColors[26];
                    break;
                case 19:
                    colorValue = arrColors[7];
                    break;
                case 20:
                    colorValue = arrColors[17];
                    break;
                case 21:
                    colorValue = arrColors[27];
                    break;
                case 22:
                    colorValue = arrColors[8];
                    break;
                case 23:
                    colorValue = arrColors[18];
                    break;
                case 24:
                    colorValue = arrColors[28];
                    break;
                case 25:
                    colorValue = arrColors[9];
                    break;
                case 26:
                    colorValue = arrColors[19];
                    break;
                case 27:
                    colorValue = arrColors[29];
                    break;
                case 28:
                    colorValue = arrColors[0];
                    break;
                case 29:
                    colorValue = arrColors[31];
                    break;
                case 30:
                    colorValue = arrColors[32];
                    break;
                    break;
                    break;
            }
            return {
                color: colorValue
            }
        }        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class=" col-lg-3 padding-none">
        <div class="closebtn"><span>&#9776;</span></div>
        <div style="display: none;" class="closebtn1">
            <span>&#9776;</span>
        </div>
        <asp:HiddenField ID="hdModuleId" runat="server" />
    </div>
    <div class=" col-lg-12 col-xl-12 col-sm-12 col-md-12 col-12 col-sm-12 col-md-12 padding-none">
        <div class=" col-lg-12 col-12 col-sm-12 col-md-12 col-xl-12 col-sm-12 col-md-12    outer_border_profile <%--outer_border--%>">
            <div class=" col-lg-12 col-xl-12 col-sm-12 col-md-12 col-12 padding-none">
                <div class=" col-sm-6 col-md-3 col-lg-3 col-xl-3 col-6 float-left padding-right_div fade-in one" id="Div2" runat="server">
                    <div class="col-12 padding-none  outer_dashboard float-left">
                        <div class="d-md-none d-sm-none d-none d-lg-block d-xl-block float-left" style="position: absolute; right: 10px; opacity: 0.1; top: 20px;">
                            <img src="<%=ResolveUrl("~/Images/UMSDashBoard/user_big.png")%>" class="" alt="Approve" />
                        </div>
                        <div class="col-12 header_text_dshboard float-left">Active Employees</div>
                        <div class="col-12 padding-none float-left">
                            <div class="col-lg-6 col-6 col-sm-6 col-md-6 padding-none d-md-none d-sm-none d-none d-lg-block d-xl-block float-left">
                                <img src="<%=ResolveUrl("~/Images/UMSDashBoard/user_bsmall.png")%>" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6  col-12 float-right">
                                <span class="link_dashboard">
                                    <%--<a href="EmployeeList.aspx?L=T">--%>
                                    <a href="EmployeeList.aspx?T=1">
                                        <asp:Label ID="lblTotalUSers" CssClass="link_dashboard" style="text-decoration:underline;" runat="server" Text="0"></asp:Label></a></span>
                            </div>

                        </div>
                    </div>
                </div>




                <div class="col-sm-6 col-md-3 col-lg-3 col-xl-3 col-6 float-left padding-right_div fade-in one" id="div1" runat="server">
                    <div class="col-12 padding-none outer_dashboard1 float-left">
                        <div class="d-md-none d-sm-none d-none d-lg-block d-xl-block float-left" style="position: absolute; right: 10px; opacity: 0.1; top: 20px;">
                            <img src="<%=ResolveUrl("~/Images/UMSDashBoard/locked_big.png")%>" class="" alt="Approve" />
                        </div>
                        <div class="col-12 header_text_dshboard float-left">Locked Employees</div>
                        <div class="col-12 padding-none float-left">
                            <div class="col-lg-6 col-6 col-sm-6 col-md-6 padding-none d-md-none d-sm-none d-none d-lg-block d-xl-block float-left">
                                <img src="<%=ResolveUrl("~/Images/UMSDashBoard/locked_small.png")%>" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6  col-12 float-right">
                                <span class="link_dashboard">
                                    <%--        <a href="EmployeeList.aspx?L=Y">--%>
                                    <a href="EmployeeList.aspx?L=2">
                                        <asp:Label ID="lblLockedUsers" CssClass="link_dashboard" runat="server"  style="text-decoration:underline;" Text="0"></asp:Label></a></span>
                            </div>

                        </div>
                    </div>
                </div>




                <div class="col-sm-6 col-md-3 col-lg-3 col-xl-3 col-6 float-left padding-right_div fade-in one" id="dvEfective" runat="server">
                    <div class="col-12 padding-none  outer_dashboard3  float-left">
                        <div class="d-md-none d-sm-none d-none d-lg-block d-xl-block float-left" style="position: absolute; right: 10px; opacity: 0.1; top: 20px;">
                            <img src="<%=ResolveUrl("~/Images/UMSDashBoard/depart_big.png")%>" class="" alt="Approve" />
                        </div>
                        <div class="col-12 header_text_dshboard float-left">Total Departments</div>
                        <div class="col-12 padding-none float-left">
                            <div class="col-lg-6 col-6 col-sm-6 col-md-6 padding-none d-md-none d-sm-none d-none d-lg-block d-xl-block float-left">
                                <img src="<%=ResolveUrl("~/Images/UMSDashBoard/depart_small.png")%>" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6  col-12 float-right">
                                <span class="link_dashboard">
                                    <a href="Department/DepartmentList.aspx?L=T">
                                        <asp:Label ID="lblTotalDepartment" CssClass="link_dashboard" runat="server" Text="0" style="text-decoration:underline;"></asp:Label></a></span>
                            </div>

                        </div>
                    </div>
                </div>




                <div class="col-sm-6 col-md-3 col-lg-3 col-xl-3 col-6 float-left padding-right_div fade-in one" id="divCreatorRevert" runat="server">
                    <div class="col-12 padding-none  outer_dashboard2  float-left">
                        <div class="d-md-none d-sm-none d-none d-lg-block d-xl-block float-left" style="position: absolute; right: 10px; top: 20px; opacity: 0.1">
                            <img src="<%=ResolveUrl("~/Images/UMSDashBoard/no_roles_big.png")%>" class="" alt="Approve" />
                        </div>
                        <div class="col-12 header_text_dshboard float-left">Employee with No Role</div>
                        <div class="col-12 padding-none float-left">
                            <div class="col-lg-6 col-6 col-sm-6 col-md-6 d-md-none d-sm-none d-none d-lg-block d-xl-block padding-none float-left">
                                <img src="<%=ResolveUrl("~/Images/UMSDashBoard/no_roles_small.png")%>" class="dashboard-image" alt="Approve" />
                            </div>
                            <div class="col-lg-6  col-12 float-right">
                                <span class="link_dashboard">
                                    <%-- <a href="EmployeeList.aspx?A=U">--%>
                                    <a href="EmployeeList.aspx?A=3">
                                        <asp:Label ID="lblUnAssignRoles" CssClass="link_dashboard" runat="server" Text="0" style="text-decoration:underline;"></asp:Label></a></span>
                            </div>
                        </div>
                    </div>
                </div>






                <%--                <div class="col-lg-3 padding-right_div fade-in one">
				<div class="col-lg-12 col-12 col-sm-12 col-md-12  outer_dashboard outerbg_dashboard1  float-left">
									<div class="col-lg-12 col-12 col-sm-12 col-md-12 header_text_dshboard">Total Employees </div>
									<div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none">
										<div class="col-lg-6 padding-none "> <img src="../Images/UMSDashBoard/total_user.png" width="72%" class="dashboard-image" /></div>
										<div class="col-lg-6 ">
											<span class="link_dashboard"> <a href="EmployeeList.aspx?L=T">
                                                <asp:Label ID="lblTotalUSers" runat="server"></asp:Label> </a></span>
										</div>
									</div>
								</div>
								</div>
                 <div class="col-lg-3 col-3 col-sm-3 col-md-3 padding-right_div fade-in two">
									<div class="col-lg-12 col-12 col-sm-12 col-md-12 col-12 col-sm-12 col-md-12  outer_dashboard outerbg_dashboard2 float-left">
									<div class="col-lg-12 col-12 col-sm-12 col-md-12 col-12 col-sm-12 col-md-12 header_text_dshboard">Locked Employees </div>
									<div class="col-lg-12 col-12 col-sm-12 col-md-12 col-12 col-sm-12 col-md-12 padding-none">
										<div class="col-lg-6 padding-none "> <img src="../Images/UMSDashBoard/locked_users.png" width="78%"  class="dashboard-image"/></div>
										<div class="col-lg-6 ">
											<span class="link_dashboard"> <a href="EmployeeList.aspx?L=Y">
                                                <asp:Label ID="lblLockedUsers" runat="server"></asp:Label> </a></span>
										</div>
									</div>
								</div>
								</div>
                <div class="col-lg-3 col-3 col-sm-3 col-md-3 padding-right_div fade-in three">
									<div class="col-lg-12 col-12 col-sm-12 col-md-12 col-12 col-sm-12 col-md-12  outer_dashboard outerbg_dashboard3 float-left">
									<div class="col-lg-12 col-12 col-sm-12 col-md-12 col-12 col-sm-12 col-md-12 header_text_dshboard">Total Departments </div>
									<div class="col-lg-12 col-12 col-sm-12 col-md-12 col-12 col-sm-12 col-md-12 padding-none">
										<div class="col-lg-6 padding-none "> <img src="../Images/UMSDashBoard/department.png" width="77%" class="dashboard-image"/></div>
										<div class="col-lg-6 ">
											<span class="link_dashboard"> <a href="Department/DepartmentList.aspx?L=T">
                                                   <asp:Label ID="lblTotalDepartment" runat="server"></asp:Label> </a></span>
										</div>
									</div>
								</div>
								</div>
                
                            <div class="col-lg-3 col-3 col-sm-3 col-md-3 padding-right_div fade-in four">
									<div class="col-lg-12 col-12 col-sm-12 col-md-12 col-12 col-sm-12 col-md-12  outer_dashboard outerbg_dashboard4 float-left">
									<div class="col-lg-12 col-12 col-sm-12 col-md-12 col-12 col-sm-12 col-md-12 header_text_dshboard">Employee with No Roles </div>
									<div class="col-lg-12 col-12 col-sm-12 col-md-12 col-12 col-sm-12 col-md-12 padding-none">
										<div class="col-lg-6 padding-none "><img src="../Images/UMSDashBoard/grid-icons_18.png" width="79%"  class="dashboard-image"/></div>
										<div class="col-lg-6 ">
											<span class="link_dashboard">  <a href="EmployeeList.aspx?A=U">
                                  <asp:Label ID="lblUnAssignRoles" runat="server"></asp:Label> </a></span>
										</div>
									</div>
								</div>
								</div>
          
                --%>
                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                <div class="col-sm-12 padding-none dashboard_panel_full float-left">
                    <div class="col-lg-12 col-12 col-sm-12 col-md-12 col-12 col-sm-12 col-md-12 dashboard_panel_header float-left">Department Wise Employees</div>
                    <%--   <div style="text-align:center; font-weight:bold;">Total No of Employees</div>--%>
                    <div class="col-lg-12 col-12 col-sm-12 col-md-12 col-12 col-sm-12 col-md-12 float-left top bottom">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <dx:BootstrapChart ID="DeptChart" runat="server" ClientInstanceName="Deptbarchart" EncodeHtml="True" OnClientCustomizePoint="onClientCustomizePoint">
                                    <SettingsToolTip OnClientCustomizeTooltip="function(pointInfo) {
	                                            return { html: &quot;&lt;div&gt;&quot; + &quot;&lt;h6&gt;&quot; + pointInfo.argument +&quot; : &quot;+pointInfo.value+ &quot;&lt;/h6&gt;&quot; + &quot;&lt;/div&gt;&quot; };
                                            }">
                                    </SettingsToolTip>
                                    <ValueAxisCollection>
                                        <dx:BootstrapChartValueAxis TickInterval="1" />
                                    </ValueAxisCollection>
                                    <ClientSideEvents PointClick="function(s, e) {
	                                                                                e.target.select();
                                                                                }"
                                        PointSelectionChanged='function(s, e) {
                                               var deptCode=e.target &amp;&amp; e.target.originalArgument;              
                                               window.open("EmployeeList.aspx?DWECode="+deptCode, "_self");

                                                                                }' />
                                </dx:BootstrapChart>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="col-sm-12 padding-none dashboard_panel_full float-left">
                    <div class="col-lg-12 col-12 col-sm-12 col-md-12 col-12 col-sm-12 col-md-12 dashboard_panel_header float-left">Role Employees on Departments</div>
                    <div class="form-group padding-none col-lg-12 col-12 col-sm-12 col-md-12 col-12 col-sm-12 col-md-12 top float-left">
                        <div class="col-lg-6 float-left">
                            <label class="padding-none col-lg-6">Select Module</label>
                            <asp:UpdatePanel ID="upSelectModule" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="Moduelddl" CssClass="form-control SearchDropDown selectpicker regulatory_dropdown_style " data-live-search="true"  data-size="7" AutoPostBack="true" Width="100%" title="Select Module" OnSelectedIndexChanged="Moduleddl_SelectedIndexChanged" runat="server"></asp:DropDownList>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="Moduelddl" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>

                        </div>
                        <div class="col-lg-6 float-left">
                            <label class="padding-none col-lg-6">Select Roles</label>
                            <asp:UpdatePanel ID="upselectRoles" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlRoles" CssClass="form-control SearchDropDown selectpicker regulatory_dropdown_style" data-live-search="true"  data-size="7" AutoPostBack="true" Width="100%" title="Select Role" OnSelectedIndexChanged="ddlRoles_SelectedIndexChanged" runat="server"></asp:DropDownList>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlRoles" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>

                        </div>
                    </div>
                    <div class="col-lg-12 col-12 col-sm-12 col-md-12 col-12 col-sm-12 col-md-12 float-left top bottom">

                        <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                        <!--start commented by pradeep-->
                        <asp:UpdatePanel ID="upModuleChart" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <dx:BootstrapChart ID="ModuleChart" runat="server" ClientInstanceName="Modulebarchart" EncodeHtml="True" OnClientCustomizePoint="onClientCustomizePoint">
                                    <SettingsToolTip OnClientCustomizeTooltip="function(pointInfo) {
	return { html: &quot;&lt;div&gt;&quot; + &quot;&lt;h6&gt;&quot; + pointInfo.argument +&quot; : &quot;+pointInfo.value+ &quot;&lt;/h6&gt;&quot; + &quot;&lt;/div&gt;&quot; };
}">
                                    </SettingsToolTip>

                                    <ValueAxisCollection>
                                        <dx:BootstrapChartValueAxis TickInterval="1" />
                                    </ValueAxisCollection>

                                    <ClientSideEvents PointClick="function(s, e) {
	e.target.select();
}"
                                        PointSelectionChanged='function(s, e) {
               var deptCode=e.target &amp;&amp; e.target.originalArgument;   
                      var ModuleID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_Moduelddl").value; 
               var RoleID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddlRoles").value;
 window.open("EmployeeList.aspx?DeptCode="+deptCode+"&RoleID="+RoleID+"", "_self");
}' />
                                </dx:BootstrapChart>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <!--End commented by pradeep-->
                    </div>

                </div>
            </div>

        </div>
    </div>
    <script>

    </script>
</asp:Content>
