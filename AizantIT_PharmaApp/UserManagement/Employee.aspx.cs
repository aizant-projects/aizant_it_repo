﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_BusinessLayer;
using UMS_BO;
using System.IO;
using AizantIT_PharmaApp.Common;
using AizantIT_PharmaApp.UserControls;
using System.Data.SqlClient;

namespace AizantIT_PharmaApp.UserManagement
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        UMS_BAL objUMS_BAL = new UMS_BAL();
        UserObjects UOB;
        DataTable dtDuration = new DataTable();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ConfirmCustomAlert.buttonClick += new EventHandler(ConfirmCustomAlert_buttonClick);                
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=1");

                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }                    
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Emp_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Emp_M2:" + strline + "  " + strMsg, "error");
            }
        }

        protected void ConfirmCustomAlert_buttonClick(object sender, EventArgs e)
        {
            try
            {
                if (hdnDeleteRow.Value != "")
                {
                    string DocID = hdnDeleteRow.Value;
                    if (Request.QueryString["EmpID"] != null)
                    {
                        
                        int CreateBy = 0;
                        if (HelpClass.IsUserAuthenticated())
                        {
                            CreateBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        }

                        DataTable dt1 = new DataTable();
                        dt1 = objUMS_BAL.GetEmployeeDocments((DocID), CreateBy,"D");
                        BindUploadDoc();
                        InitializeThePage();
                    }
                    else
                    {
                        DataTable dt = ViewState["DeptDuration"] as DataTable;
                        DataRow[] drr = dt.Select("ID=" + DocID + "");
                        foreach (var drow in drr)
                        {
                            drow.Delete();
                        }
                        
                        dt.AcceptChanges();
                        gvDocumentUpload.DataSource = dt;
                        gvDocumentUpload.DataBind();
                        gvDocumentUpload.Columns[7].Visible = false; gvDocumentUpload.Columns[5].Visible = false;
                        ViewState["DeptDuration"] = dt;
                    }
                    BindPhoto();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Emp_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Emp_M1:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindTempData()
        {
            DataTable dtDuration = new DataTable();
            dtDuration.TableName = "Durations";
            dtDuration.Columns.Add("ID");
            dtDuration.Columns.Add("DocID");
            dtDuration.Columns.Add("DocTypeID");
            dtDuration.Columns.Add("FileName");
            dtDuration.Columns.Add("Bytes", typeof(byte[]));
            ViewState["DeptDuration"] = dtDuration;
        }
        protected void UploadFile(object sender, EventArgs e)
        {
            try
            {
                DataTable dtDuration = ViewState["DeptDuration"] as DataTable;
                string fileName = Path.GetFileName(FileUploadDoc.PostedFile.FileName);
                string filetype = Path.GetExtension(FileUploadDoc.PostedFile.FileName);
                Stream fs = FileUploadDoc.PostedFile.InputStream;
                BinaryReader br = new BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                
                string mime = MimeType.GetMimeType(bytes, fileName);
                if (mime == "application/pdf")
                {
                   
                    if (dtDuration.Rows.Count > 0)
                    {
                        DataRow[] drFilename = dtDuration.Select("DocTypeID='" + fileName + "'");
                        if (drFilename.Length > 0)//Document Name Already Exist
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Document Name Already Exists", "info");
                            return;
                        }
                    }

                    if (Request.QueryString["EmpID"] != null)
                    {
                       
                        int CreateBy = 0;
                        if (HelpClass.IsUserAuthenticated())
                        {
                            CreateBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        }


                        //insert the file into database
                        int inserDocument = objUMS_BAL.EmployeeDoc((txtEmpCode.Value), Convert.ToInt32(ddlDocumenType.SelectedValue), bytes, filetype, fileName, CreateBy, out int Count);
                        if (inserDocument == 2)
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Before Adding the Documents Please add the Employee", "error");
                            return;
                        }
                        if (inserDocument == 1)
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Already Resume Inserted", "error");
                            return;
                        }
                        if (inserDocument == 3)//Document Name Already Exist
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Document Name Already Exists", "info");
                            return;
                        }
                        BindUploadDoc();
                    }
                    else
                    {
                        
                        DataRow dr = dtDuration.NewRow();
                        int count = 0;
                        int rowcount = dtDuration.Rows.Count;
                        for (int i = 0; i < dtDuration.Rows.Count; i++)
                        {
                            if (Convert.ToInt32(dtDuration.Rows[i]["DocID"]) == Convert.ToInt32(ddlDocumenType.SelectedValue))
                            {
                                if (ddlDocumenType.SelectedValue == "1")
                                {
                                    count = 1;
                                }
                            }
                        }
                        if (count == 1)
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Already Resume Inserted", "error");
                            return;
                        }
                        else
                        {

                            //int max = Convert.ToInt32(dtDuration.AsEnumerable().Max(row => row["ID"]));
                            int max = rowcount;
                            dr["ID"] = max + 1;
                            dr["DocID"] = ddlDocumenType.SelectedValue;
                            dr["DocTypeID"] = fileName;//Upload file name
                            if (ddlDocumenType.SelectedValue == "1")
                            { dr["FileName"] = "Resume"; }
                            else if (ddlDocumenType.SelectedValue == "2") { dr["FileName"] = "Academic Certificate"; } else if (ddlDocumenType.SelectedValue == "3") { dr["FileName"] = "Other Document"; }
                            dr["Bytes"] = bytes;
                            dtDuration.Rows.Add(dr);
                            ViewState["DeptDuration"] = dtDuration;
                           
                            gvDocumentUpload.DataSource = dtDuration;
                            gvDocumentUpload.DataBind();
                            gvDocumentUpload.Columns[1].Visible = gvDocumentUpload.Columns[7].Visible = false; gvDocumentUpload.Columns[5].Visible = false;
                        }
                    }
                }
                else
                {
                    //  file is Invalid  
                    HelpClass.custAlertMsg(this, this.GetType(), "Please upload pdf Files only", "error");
                   

                }
               
               
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Emp_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Emp_M3:" + strline + "  " + strMsg, "error");
            }
            finally
            {
                upSelectDoc.Update();
                BindPhoto();
                if (HFDOB.Value != "")
                {
                    txtEmpDob.Text = HFDOB.Value;
                }
            }

        }
        public void BindPhoto()
        {
            byte[] img1 = EmpImage();
            if (img1 != null)
            {
                BindApplicantImage((byte[])img1);
            }
        }
        protected void DownloadFile(object sender, EventArgs e)
        {
            try
            {
                RetriveEmployeeDetails();
                string DocID = ((sender as LinkButton).CommandArgument);
                GridViewRow row = ((Control)sender).Parent.Parent as GridViewRow;
                literalViewDocment.Text = "<embed src=\"" + ResolveUrl("~/UserManagement/ViewDoc.ashx") + "?DocID=" + DocID + "#toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" height=\"700px\" />";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", " $('#ViewTrainingRecord').modal({ show: true, backdrop: 'static', keyboard: false });", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Emp_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Emp_M4:" + strline + "  " + strMsg, "error");
            }
        }
        //there is no Empcode in Database page navigate Employeecode
        private void AutoIncrement()
        {
            try
            {
                DataSet ds = objUMS_BAL.AutoNumber(out string Status);
                if (Status == "")
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Please create employee code before creating employee.", "warning", "ReloadEmployeeCodePage()");
                    //HelpClass.custAlertMsg(this, this.GetType(), "There is No EmployeeCode", "warning");

                    //HttpContext.Current.Response.Redirect("~/UserManagement/EmployeeCode.aspx", false);
                    //HttpContext.Current.ApplicationInstance.CompleteRequest();
                    //txtEmpCode.Disabled = false;
                }
                else
                {
                    txtEmpCode.Value = Status;
                    txtEmpCode.Disabled = true;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Emp_M5:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Emp_M5:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindUploadDoc()
        {
            try
            {
                if (txtEmpCode.Value != "")
                {
                    DataTable dt = new DataTable();
                    dt = objUMS_BAL.GetEmployeeDocments((txtEmpCode.Value),0,"S");
                    gvDocumentUpload.DataSource = dt;
                    gvDocumentUpload.DataBind();
                    if (Request.QueryString["EmpID"] != null)
                    {
                        //gvDocumentUpload.Columns[1].Visible = false;
                    }
                    else
                    {
                        gvDocumentUpload.Columns[4].Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Emp_M6:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Emp_M6:" + strline + "  " + strMsg, "error");
            }
        }
        protected void DeleteFile(object sender, EventArgs e)
        {
        }
        private void InitializeThePage()
        {
            try
            {               
               
                BindTempData();
                
                FileUploadDoc.Disabled = true;
                btnUpload.Enabled = false;
                btnUpload.CssClass = "file upload-button btn btn-primary col-sm-12 col-xs-12 col-lg-12 col-md-12";
                BindUploadDoc();
                txtEmpCode.Focus();
                lblDesignationError.Text = lblDepartmentError.Text = lblMsgMessage.Text = "";
                BindCountries();
                BindDepartments();
                BindAllDesignations();
                if (txtEmpDob.Text != "")
                {
                    Session["Dob"] = txtEmpDob.Text;
                    txtEmpDob.Text = Session["Dob"].ToString();
                }
                else
                {
                    //DateTime maxDate = DateTime.Now.AddYears(-10);
                    //hdfMaxDateForDOB.Value = maxDate.ToString("dd MMM yyyy");
                }
                if (Request.QueryString["EmpID"] != null)
                {
                    btnLogincheck.Visible = false;
                    divDoc.Visible = true;
                    RetriveEmployeeDetails();
                    btnCancelSubmit.Visible = false;
                }
                else
                {
                    AutoIncrement();
                   
                    imgEmpImage.ImageUrl = "~/Images/UserLogin/DefaultImage.jpg";
                    divrblockstatus.Style.Add("pointer-events", "none");
                    divrbActiveStatus.Style.Add("pointer-events", "none");
                    rbbtnLocked.Checked = rbbtnActive.Checked = rbMale.Checked = true;
                    txtEndDate.Disabled = true;
                    btnCancel.Visible = btnUpdate.Visible = false;
                   
                    ddlCity.Enabled = false;
                    ddlCity.Items.Clear();
                    ddlCity.Items.Insert(0, new ListItem("-- Select City --", "0"));
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Emp_M7:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Emp_M7:" + strline + "  " + strMsg, "error");
            }
        }
        private void RetriveEmployeeDetails()
        {
            try
            {
                UOB = new UserObjects();
                txtEmpCode.Disabled = txtEmpLoginID.Disabled = true; btnLogincheck.Enabled = false; btnReset.Visible = btnSubmit.Visible = false; btnResetPwd.Visible = true;

                int EmpID = Convert.ToInt32(Request.QueryString["EmpID"].ToString());
                DataTable dt = objUMS_BAL.GetUserDetails(EmpID);
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    HFEmpID.Value = EmpID.ToString();
                    txtEmpCode.Value = dr["EmpCode"].ToString();
                    txtEmpFirstName.Value = dr["FirstName"].ToString();
                    txtEmpLastName.Value = dr["LastName"].ToString();
                    txtEmpLoginID.Value = dr["LoginID"].ToString();
                    txtEmpEmailID.Value = dr["OfficialEmailID"].ToString();
                    txtEmpMobileNo.Text = dr["MobileNo"].ToString();

                    txtEmpTelNo.Text = dr["Tel1"].ToString();
                    txtEmpEmgNo.Text = dr["EmergencyContact"].ToString();
                    txtEmpAddress.Value = dr["Address"].ToString();
                    DataTable dt1 = new DataTable();
                    dt1 = objUMS_BAL.GetEmployeeDocments((txtEmpCode.Value),0,"S");
                    gvDocumentUpload.DataSource = dt1;
                    gvDocumentUpload.DataBind();
                    gvDocumentUpload.Columns[0].Visible = gvDocumentUpload.Columns[6].Visible = false;
                    if (dr["DateOfBirth"].ToString() != "")
                    {
                        DateTime DOB = (DateTime)dr["DateOfBirth"];
                        txtEmpDob.Text = DOB.ToString("dd MMM yyyy");
                        HFDOB.Value = DOB.ToString("dd MMM yyyy");
                    }
                    else
                        txtEmpDob.Text = "0";
                    if (dr["StartDate"].ToString() != "")
                    {
                        DateTime StartDate = (DateTime)dr["StartDate"];
                        txtStartDate.Value = StartDate.ToString("dd MMM yyyy");
                    }
                    else
                        txtStartDate.Value = "";

                    if (dr["EndDate"].ToString() != "")
                    {
                        DateTime EndDate = (DateTime)dr["EndDate"];
                        txtEndDate.Value = EndDate.ToString("dd MMM yyyy");
                        //hdnEndDateExist.Value =Convert.ToString(1);//date exist already
                    }
                    else
                        txtEndDate.Value = "";
                    if (dr["BloodGroup"].ToString() == "")
                    {
                        ddlBloodGroup.SelectedValue = "0";
                    }
                    else
                    {
                        //ddlBloodGroup.SelectedItem.Text = dr["BloodGroup"].ToString();
                        int value = 0;
                        if (dr["BloodGroup"].ToString() == "A+")
                            value = 1;
                        else if (dr["BloodGroup"].ToString() == "A-")
                            value = 2;
                        else if (dr["BloodGroup"].ToString() == "AB+")
                            value = 3;
                        else if (dr["BloodGroup"].ToString() == "AB-")
                            value = 4;
                        else if (dr["BloodGroup"].ToString() == "B+")
                            value = 5;
                        else if (dr["BloodGroup"].ToString() == "B-")
                            value = 6;
                        else if (dr["BloodGroup"].ToString() == "O+")
                            value = 7;
                        else
                            value = 8;
                        ddlBloodGroup.SelectedValue = value.ToString();
                    }
                    ddlDepartments.SelectedValue = dr["DefaultDeptID"].ToString();
                   
                    ddlDesignation.SelectedValue = dr["CurrentDesignation"].ToString();
                    ddlCountry.SelectedValue = dr["CountryID"].ToString();
                    BindStates(Convert.ToInt32(ddlCountry.SelectedItem.Value));
                    ddlState.SelectedValue = dr["StateID"].ToString();
                    BindCity(Convert.ToInt32(ddlState.SelectedItem.Value));
                    if (dr["CityID"].ToString() != "")
                    {
                        ddlCity.SelectedValue = dr["CityID"].ToString();
                    }

                    if (dr["Locked"].ToString() == "Y")
                    {
                        if (dr["LockedStatus"].ToString() != "")
                        {
                            if (Convert.ToInt32(dr["LockedStatus"]) == 1)
                            {
                                divrblockstatus.Style.Add("pointer-events", "none");
                                lblrblocked.Attributes.Add("class", "btn btn-primary_radio radio_check  col-6 col-lg-6 col-md-6 col-sm-6 float-left  active");
                                lblrbunlocked.Attributes.Add("class", "btn btn-primary_radio radio_check  col-6 col-lg-6 col-md-6 col-sm-6 float-left ");

                            }
                        }
                        else
                        {
                            lblrblocked.Attributes.Add("class", "btn btn-primary_radio radio_check  col-6 col-lg-6 col-md-6 col-sm-6 float-left  active");
                            lblrbunlocked.Attributes.Add("class", "btn btn-primary_radio radio_check  col-6 col-lg-6 col-md-6 col-sm-6 float-left ");
                            rbbtnLocked.Checked = true;
                            rbbtnUnlocked.Checked = false;
                           
                        }
                    }
                    if (dr["Locked"].ToString() == "N")
                    {

                        lblrbunlocked.Attributes.Add("class", "btn btn-primary_radio radio_check  col-6 col-lg-6 col-md-6 col-sm-6 float-left  active");
                        lblrblocked.Attributes.Add("class", "btn btn-primary_radio radio_check  col-6 col-lg-6 col-md-6 col-sm-6 float-left ");
                        rbbtnUnlocked.Checked = true;
                        rbbtnLocked.Checked = false;
                        
                    }
                    if (dr["Gender"].ToString() == "M")
                    {
                        lblrbMale.Attributes.Add("class", "btn btn-primary_radio radio_check col-sm-6 float-left  col-6 col-lg-6 col-md-6 active");
                        lblrbFemale.Attributes.Add("class", "btn btn-primary_radio radio_check col-sm-6 float-left  col-6 col-lg-6 col-md-6 ");
                        rbMale.Checked = true;
                        rbbtnFemale.Checked = false;
                    }
                    else
                    {
                        lblrbFemale.Attributes.Add("class", "btn btn-primary_radio radio_check col-sm-6 float-left  col-6 col-lg-6 col-md-6 active");
                        lblrbMale.Attributes.Add("class", "btn btn-primary_radio radio_check col-sm-6 float-left  col-6 col-lg-6 col-md-6 ");
                        rbbtnFemale.Checked = true;
                        rbMale.Checked = false;
                    }
                    if (dr["Status"].ToString() == "A")
                    {
                        //class added
                        lblrbActive.Attributes.Add("class", "btn btn-primary_radio radio_check  col-6 col-lg-6 col-md-6 col-sm-6 float-left  active focus");
                        lblrbInActive.Attributes.Add("class", "btn btn-primary_radio radio_check  col-6 col-lg-6 col-md-6 col-sm-6 float-left  ");
                        rbbtnActive.Checked = true;
                        rbbtnInActive.Checked = false;
                    }
                    else
                    {
                        lblrbInActive.Attributes.Add("class", "btn btn-primary_radio radio_check  col-6 col-lg-6 col-md-6 col-sm-6 float-left  active focus");
                        lblrbActive.Attributes.Add("class", "btn btn-primary_radio radio_check  col-6 col-lg-6 col-md-6 col-sm-6 float-left  ");
                        rbbtnInActive.Checked = true;
                        rbbtnActive.Checked = false;
                    }
                    if (dr["Photo"].ToString() == "")
                        imgEmpImage.ImageUrl = "~/Images/UserLogin//DefaultImage.jpg";
                    else
                    {
                        BindApplicantImage((byte[])dr["Photo"]);
                        // HFImage.Value = dr["Photo"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Emp_M8:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Emp_M8:" + strline + "  " + strMsg, "error");
            }
        }
        private void BindApplicantImage(byte[] applicantImage)
        {
            string base64ImageString = ConvertBytesToBase64(applicantImage);

            imgEmpImage.ImageUrl = "data:image/jpg;base64," + base64ImageString;
        }
        public string ConvertBytesToBase64(byte[] imageBytes)
        {
            return Convert.ToBase64String(imageBytes);
        }
        public byte[] EmpImage()
        {
            byte[] imgbyte = null;
            if (FileUpload1.HasFile)
            {
                //getting length of uploaded file  
                int length = FileUpload1.PostedFile.ContentLength;
                //create a byte array to store the binary image data  
                imgbyte = new byte[length];
                //store the currently selected file in memeory  
                HttpPostedFile img = FileUpload1.PostedFile;
                //set the binary data  
                img.InputStream.Read(imgbyte, 0, length);
            }
            Session["Img"] = imgbyte;
            
            return imgbyte;
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            SqlTransaction _transaction = null;
            SqlConnection _Conn = null;
            try
            {
                if (txtEmpCode.Value.Trim() == "" || txtEmpFirstName.Value.Trim() == "" || txtEmpLastName.Value.Trim() == "" || txtEmpDob.Text.Trim() == "" ||
                  ddlDepartments.SelectedValue == "0" || ddlDesignation.SelectedValue == "0" || txtEmpLoginID.Value.Trim() == "" || txtEmpMobileNo.Text.Trim() == "" ||
                  ddlCountry.SelectedValue == "0" || ddlState.SelectedValue == "0" || txtEmpAddress.Value.Trim() == "" || txtEmpEmailID.Value.Trim() == "")
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "All fields represented with * are mandatory", "error");
                }
                else
                {


                    if (txtEmpLoginID.Value.Trim() == "" || txtEmpLoginID.Value.Trim().Length <= 3)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "LoginID should contain Minimum of 4 Characters", "error");
                        return;
                    }

                    else
                    {
                        if (LoginIDCheck())
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "LoginID Already exists please give another LoginID!", "error");
                            return;
                        }
                        UOB = new UserObjects();
                        if (rbMale.Checked)
                            UOB.EmpGender = 'M';
                        if (rbbtnFemale.Checked)
                            UOB.EmpGender = 'F';
                        if (rbbtnActive.Checked)
                            UOB.EmpActiveStatus = 'A';
                        if (rbbtnInActive.Checked)
                            UOB.EmpActiveStatus = 'I';
                        if (rbbtnLocked.Checked)
                            UOB.EmpLockStatus = 'Y';
                        if (rbbtnUnlocked.Checked)
                            UOB.EmpLockStatus = 'N';

                        UOB.EmpCode = txtEmpCode.Value.Trim();
                        UOB.EmpFirstName = txtEmpFirstName.Value.Trim();
                        UOB.EmpLastName = txtEmpLastName.Value.Trim();
                        UOB.EmpDOB = Convert.ToDateTime(txtEmpDob.Text.Trim());
                        HFDOB.Value = txtEmpDob.Text.Trim();
                        UOB.EmpDepartment = Convert.ToInt32(ddlDepartments.SelectedValue);
                        UOB.EmpDesignation = Convert.ToInt32(ddlDesignation.SelectedValue);
                        UOB.EmpLoginID = txtEmpLoginID.Value.Trim();
                        if (ddlBloodGroup.SelectedValue == "0")
                            UOB.EmpBloodGroup = "";
                        else
                            UOB.EmpBloodGroup = ddlBloodGroup.SelectedItem.Text;
                        UOB.EmpEmailID = txtEmpEmailID.Value.Trim();
                        UOB.EmpMobileNo = txtEmpMobileNo.Text.Trim();
                        UOB.EmpEmgNo = txtEmpEmgNo.Text.Trim();
                        UOB.EmpTelNo = txtEmpTelNo.Text.Trim();
                        UOB.EmpCountry = Convert.ToInt32(ddlCountry.SelectedValue);
                        UOB.EmpState = Convert.ToInt32(ddlState.SelectedValue);
                        UOB.EmpCity = Convert.ToInt32(ddlCity.SelectedValue);
                        UOB.EmpAddress = txtEmpAddress.Value.Trim();
                        if (Session["Img"] != null)
                        {
                            //byte[] barrImg = (byte[])(hdnImage.Value.ToString());
                            UOB.EmpPhoto = (byte[])(Session["Img"]);
                        }
                        else
                        {
                            UOB.EmpPhoto = EmpImage();
                        }
                        //UOB.EmpPhoto = EmpImage();


                        UOB.EmpStartDate = txtStartDate.Value.Trim();
                        UOB.EmpEndDate = txtEndDate.Value.Trim();
                        int CreatedBy = 0;
                        if (HelpClass.IsUserAuthenticated())
                        {
                            CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        }
                        DataTable dtTemp = new DataTable();
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataRow[] drUMS = dtx.Select("ModuleID=1");
                        dtTemp = drUMS.CopyToDataTable();
                        int RoleID = Convert.ToInt32(dtTemp.Rows[0]["RoleID"].ToString());
                        UOB.RoleID = RoleID;
                        int outEmpId = objUMS_BAL.AddUserDetailsBAL(UOB, CreatedBy, ref _Conn, ref _transaction);
                        if (ViewState["DeptDuration"] != null)
                        {
                            DataTable dtDuration = ViewState["DeptDuration"] as DataTable;
                            if (dtDuration.Rows.Count > 0)
                            {
                                int inserDocument = objUMS_BAL.EmployeeDocCreationBal(outEmpId, CreatedBy, dtDuration, ref _Conn, ref _transaction);
                            }
                        }
                        _transaction.Commit();
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "User Created Successfully", "success", "ReloadCurrentPage()");

                    }
                }
             }
            catch (Exception ex)
            {
                if (_transaction != null)
                {
                    _transaction.Rollback();
                }
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Emp_M9:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Emp_M9:" + strline + "  " + strMsg, "error");
            }
            finally
            {
                if (_Conn != null)
                {
                    _Conn.Close();
                    _Conn.Dispose();
                }
            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", "ShowPleaseWaitGifDisableImage();", true);
        }
        private bool LoginIDCheck()
        {
            bool isDuplicate = false;
            try
            {
                if (txtEmpLoginID.Value.Trim() == "" || txtEmpLoginID.Value.Trim().Length <= 3)
                {
                    lblLoginCheck.Text = "LoginID should contain Minimum of 4 Characters";
                    lblLoginCheck.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    int count = objUMS_BAL.LoginIDCheckBAL(txtEmpLoginID.Value.Trim());

                    if (count > 0)
                    {
                        lblLoginCheck.Text = "This LoginID Already exists!";
                        lblLoginCheck.ForeColor = System.Drawing.Color.Red;
                        isDuplicate = true;
                    }
                    else
                    {
                        lblLoginCheck.Text = "LoginID is Accepted";
                        lblLoginCheck.ForeColor = System.Drawing.Color.Green;
                    }

                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Emp_M10:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Emp_M10:" + strline + "  " + strMsg, "error");
            }
            return isDuplicate;
        }
        protected void btnLogincheck_Click(object sender, EventArgs e)
        {
            LoginIDCheck();
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/UserManagement/Employee.aspx");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Emp_M11:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Emp_M11:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnDeptCreate_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtDeptCode.Value.Trim() == "" || txtDeptName.Value.Trim() == "")
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "All fields represented with * are mandatory", "error");
                }
                string DeptName = txtDeptName.Value.Trim();
                string DeptCode = txtDeptCode.Value.Trim().ToUpper();
                string Operation = "Insert";
                int Deptid = 0;
                int EmpID = 0;
                if (HelpClass.IsUserAuthenticated())
                {
                    EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                }
                DataTable dtTemp = new DataTable();
                DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                DataRow[] drUMS = dtx.Select("ModuleID=1");
                dtTemp = drUMS.CopyToDataTable();
                int RoleID = Convert.ToInt32(dtTemp.Rows[0]["RoleID"].ToString());
                int s = objUMS_BAL.DepartmentBAL(Operation, EmpID, Deptid, DeptName, DeptCode, out int VerifyDeptName, RoleID);
                if (VerifyDeptName == 1)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Department Name already Exists", "error");
                }
                else if (VerifyDeptName == 2)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Department Code already Exists", "error");
                }
                else if (VerifyDeptName == 3)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Department Name and Code already Exists", "error");
                }
                else
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Department Created Successfully", "success", "HideModDepartment()");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Emp_M12:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Emp_M12:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnCreateDesignation_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtDesignationName.Value.Trim() == "")
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "All fields represented with * are mandatory", "error");
                    //lblDesignationError.Text = "All fields represented with * are mandatory";
                }
                else
                {
                    int EmpID = 0;
                    if (HelpClass.IsUserAuthenticated())
                    {
                        EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    }
                    DataTable dtTemp = new DataTable();
                    DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                    DataRow[] drUMS = dtx.Select("ModuleID=1");
                    dtTemp = drUMS.CopyToDataTable();
                    int RoleID = Convert.ToInt32(dtTemp.Rows[0]["RoleID"].ToString());
                    int Desgid = 0;
                    string Operation = "Insert";
                    string DesignationName = txtDesignationName.Value.Trim();
                    int s = objUMS_BAL.DesignationtBAL(Operation, EmpID, Desgid, DesignationName, out int VerifyDesigName, RoleID);
                    if (VerifyDesigName == 1)
                    {
                       
                        txtDesignationName.Value = "";

                        HelpClass.custAlertMsg(this, this.GetType(), "Designation Name already Exists", "error");

                    }
                    else
                    {
                        if (s > 0)
                        {
                           
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Designation Created successfully", "success", "HideModDesignation()");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Emp_M13:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Emp_M13:" + strline + "  " + strMsg, "error");
            }
        }
        private void JsMethod(string msg)
        {
            //msg = msg.Replace("'", "");
            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "alert", "alert('" + msg + "');", true);
        }
        private void JsMethodForReload(string msg)
        {
            
        }
        public void BindCountries()
        {
            try
            {
                
                ddlCountry.Items.Insert(0, new ListItem("-- Select Country --", "0"));
                DataTable dt = objUMS_BAL.BindCountriesBAL();
                ddlCountry.DataSource = dt;
                ddlCountry.DataTextField = "CountryName";
                ddlCountry.DataValueField = "CountryID";
                ddlCountry.DataBind();
                ddlCountry.Items.Insert(0, new ListItem("-- Select Country --", "0"));
                ddlCountry.SelectedValue = "101";
                UOB = new UserObjects();
                UOB.SelectedCountryID = Convert.ToInt32("101");
                DataTable dt1 = new DataTable();
                if (UOB.SelectedCountryID > 0)
                {
                    dt1 = objUMS_BAL.BindStatesBAL(UOB.SelectedCountryID);
                    ddlState.DataSource = dt1;
                    ddlState.DataTextField = "StateName";
                    ddlState.DataValueField = "StateID";
                    ddlState.DataBind();
                    ddlState.Items.Insert(0, new ListItem("-- Select State --", "0"));
                    ddlState.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Emp_M14:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Emp_M14:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindStates(int SelectedCountryID)
        {
            try
            {
                DataTable dt = objUMS_BAL.BindStatesBAL(SelectedCountryID);
                ddlState.DataSource = dt;
                ddlState.DataTextField = "StateName";
                ddlState.DataValueField = "StateID";
                ddlState.DataBind();
                ddlState.Items.Insert(0, new ListItem("-- Select State --", "0"));
                ddlState.Enabled = true;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Emp_M15:" + strline + "  " + strMsg);
                HelpClass.showMsg(this, this.GetType(), "Emp_M15:" + strline + "  " + strMsg);
            }

        }
        public void BindCity(int SelectedCountryID)
        {
            try
            {
                DataTable dt = objUMS_BAL.BindCityBAL(SelectedCountryID);
                ddlCity.DataSource = dt;
                ddlCity.DataTextField = "CityName";
                ddlCity.DataValueField = "CityID";
                ddlCity.DataBind();
                ddlCity.Items.Insert(0, new ListItem("-- Select City --", "0"));
                ddlCity.Enabled = true;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Emp_M16:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Emp_M16:" + strline + "  " + strMsg, "error");
            }

        }
        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                UOB = new UserObjects();
                ddlState.Enabled = false;
                ddlState.Items.Clear();
                ddlCity.Enabled = false;
                ddlCity.Items.Clear();
               
                UOB.SelectedCountryID = Convert.ToInt32(ddlCountry.SelectedItem.Value);
                if (UOB.SelectedCountryID > 0)
                {
                    DataTable dt = objUMS_BAL.BindStatesBAL(UOB.SelectedCountryID);
                    ddlState.DataSource = dt;
                    ddlState.DataTextField = "StateName";
                    ddlState.DataValueField = "StateID";
                    ddlState.DataBind();
                    ddlState.Items.Insert(0, new ListItem("-- Select State --", "0"));
                    ddlState.Enabled = true;
                    ddlCity.Items.Insert(0, new ListItem("-- Select City --", "0"));
                    //upDob.Update();
                    //UPCountries.Update();
                }
                else
                {
                    ddlState.Items.Insert(0, new ListItem("-- Select State --", "0"));
                    ddlCity.Items.Insert(0, new ListItem("-- Select City --", "0"));

                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Emp_M17:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Emp_M17:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindDepartments()
        {
            try
            {
                DataTable dt = objUMS_BAL.BindDepartmentsBAL();
                ddlDepartments.DataSource = dt;
                ddlDepartments.DataTextField = "DepartmentName";
                ddlDepartments.DataValueField = "DeptID";
                ddlDepartments.DataBind();
                ddlDepartments.Items.Insert(0, new ListItem("-- Select Department --", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Emp_M18:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Emp_M18:" + strline + "  " + strMsg, "error");
            }

        }
        public void BindAllDesignations()
        {
            try
            {
                DataTable dt = objUMS_BAL.BindAllDesignationsBAL();
                ddlDesignation.DataSource = dt;
                ddlDesignation.DataTextField = "DesignationName";
                ddlDesignation.DataValueField = "DesignationID";
                ddlDesignation.DataBind();
                ddlDesignation.Items.Insert(0, new ListItem("-- Select Designation --", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Emp_M19:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Emp_M19:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                // if condition for checking the Update or Create based on EmpID
                if (txtEmpFirstName.Value.Trim() == "" || txtEmpLastName.Value.Trim() == "" || txtEmpDob.Text.Trim() == "" ||
                ddlDepartments.SelectedValue == "0" || ddlDesignation.SelectedValue == "0" || txtEmpMobileNo.Text.Trim() == "" ||
                ddlCountry.SelectedValue == "0" || ddlState.SelectedValue == "0" || txtEmpAddress.Value.Trim() == "" ||
                txtEmpEmailID.Value.Trim() == "")
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "All fields represented with * are mandatory", "error", "btnUpdateEnable()");
                    
                }
                else
                {
                    UOB = new UserObjects();

                    if (rbMale.Checked)
                        UOB.EmpGender = 'M';
                    if (rbbtnFemale.Checked)
                        UOB.EmpGender = 'F';
                    if (rbbtnActive.Checked)
                        UOB.EmpActiveStatus = 'A';
                    if (rbbtnInActive.Checked)
                        UOB.EmpActiveStatus = 'I';
                    if (rbbtnLocked.Checked)
                        UOB.EmpLockStatus = 'Y';
                    if (rbbtnUnlocked.Checked)
                        UOB.EmpLockStatus = 'N';
                    UOB.EmpID = Convert.ToInt32(HFEmpID.Value);
                    UOB.EmpFirstName = txtEmpFirstName.Value.Trim();
                    UOB.EmpLastName = txtEmpLastName.Value.Trim();
                    UOB.EmpDOB = Convert.ToDateTime(txtEmpDob.Text.Trim());
                    UOB.EmpDepartment = Convert.ToInt32(ddlDepartments.SelectedValue);
                    UOB.EmpDesignation = Convert.ToInt32(ddlDesignation.SelectedValue);
                    if (ddlBloodGroup.SelectedValue == "0")
                        UOB.EmpBloodGroup = "";
                    else
                        UOB.EmpBloodGroup = ddlBloodGroup.SelectedItem.Text;
                    UOB.EmpEmailID = txtEmpEmailID.Value.Trim();
                    UOB.EmpMobileNo = txtEmpMobileNo.Text.Trim();
                    UOB.EmpEmgNo = txtEmpEmgNo.Text.Trim();
                    UOB.EmpTelNo = txtEmpTelNo.Text.Trim();
                    UOB.EmpCountry = Convert.ToInt32(ddlCountry.SelectedValue);
                    UOB.EmpState = Convert.ToInt32(ddlState.SelectedValue);
                    UOB.EmpCity = Convert.ToInt32(ddlCity.SelectedValue);
                    UOB.EmpAddress = txtEmpAddress.Value.Trim();
                    if (Session["Img"] != null)
                    {
                       
                        UOB.EmpPhoto = (byte[])(Session["Img"]);
                    }
                    else
                    {
                        UOB.EmpPhoto = EmpImage();
                    }
                    Session["Img"] = null;
                    UOB.EmpStartDate = txtStartDate.Value.Trim();
                    UOB.EmpEndDate = txtEndDate.Value.Trim();
                    int EmpId = 0;
                    if (HelpClass.IsUserAuthenticated())
                    {
                        EmpId = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    }
                   
                    DataTable dtTemp = new DataTable();
                    DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                    DataRow[] drUMS = dtx.Select("ModuleID=1");
                    dtTemp = drUMS.CopyToDataTable();
                    int RoleID = Convert.ToInt32(dtTemp.Rows[0]["RoleID"].ToString());
                    UOB.RoleID = RoleID;
                   int count = objUMS_BAL.EditUserDetailsBAL(UOB, EmpId);
                    if (count > 0)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "User Updated Successfully", "success", "ReloadListPage()");
                        if (UOB.EmpPhoto != null)
                            BindApplicantImage((byte[])UOB.EmpPhoto);
                        HFDOB.Value = UOB.EmpDOB.ToString("dd MMMM yyyy");
                    }
                    
                }
              }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Emp_M20:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Emp_M20:" + strline + "  " + strMsg, "error");
            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", "ShowPleaseWaitGifDisableImage();", true);
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/UserManagement/EmployeeList.aspx",false);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Emp_M21:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Emp_M21:" + strline + "  " + strMsg, "error");
            }
        }
        protected void ddlDocumenType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDocumenType.SelectedValue == "0")
            {
                FileUploadDoc.Disabled = true;
                btnUpload.Enabled = false;
            }
            else
            {
                FileUploadDoc.Disabled = false;
                btnUpload.Enabled = true;
            }
            upSelectDoc.Update();
        }
        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                UOB = new UserObjects();
                ddlCity.Enabled = false;
                ddlCity.Items.Clear();
               
                UOB.SelectedCountryID = Convert.ToInt32(ddlState.SelectedItem.Value);
                if (UOB.SelectedCountryID > 0)
                {
                    DataTable dt = objUMS_BAL.BindCityBAL(UOB.SelectedCountryID);
                    ddlCity.DataSource = dt;
                    ddlCity.DataTextField = "CityName";
                    ddlCity.DataValueField = "CityID";
                    ddlCity.DataBind();
                    ddlCity.Items.Insert(0, new ListItem("-- Select City --", "0"));
                    ddlCity.Enabled = true;
                }
                else
                {
                    ddlCity.Items.Insert(0, new ListItem("-- Select City --", "0"));
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "Emp_M22:" + strline + "  " + strMsg, "error");
            }
        }
        protected void txtEmpDob_TextChanged(object sender, EventArgs e)
        {
            HFDOB.Value = txtEmpDob.Text;
        }
        protected void btnsubmitResetPwd_Click(object sender, EventArgs e)
        {
            try
            {
                int EmpID=0;
                if (Request.QueryString["EmpID"]!=null)
                {
                    EmpID =Convert.ToInt32(Request.QueryString["EmpID"]);
                }
                int LogEmpId = 0;
                if (HelpClass.IsUserAuthenticated())
                {
                    LogEmpId = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                }

               

                DataTable dtTemp = new DataTable();
                DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                DataRow[] drUMS = dtx.Select("ModuleID=1");
                dtTemp = drUMS.CopyToDataTable();
                int RoleID = Convert.ToInt32(dtTemp.Rows[0]["RoleID"].ToString());
                UOB = new UserObjects();
                UOB.RoleID = RoleID;
                UOB.EmpID = LogEmpId;
                UMS_BAL objUMS_BAL = new UMS_BAL();
                int Result;
                objUMS_BAL.ResetPassword(EmpID, txtNewPassword.Value,out Result, UOB,1);
                    if (Result == 1)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Password Reset Successfully", "success", "ClearChangePwd();");
                    }
                    else if(Result==2)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "New Password Should not be Previous 3 Passwords", "error", "ErrorClearChangePwd();");
                    }
                    else if (Result == 3)
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "SignUp is not created by User", "error", "ErrorClearChangePwd();");
                    }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EMP_M23:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EMP_M23:" + strline + "  " + strMsg, "error");
            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", "ShowPleaseWaitGifDisable();", true);
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }

        protected void btnResetDepartment_Click(object sender, EventArgs e)
        {
            txtDeptCode.Value = txtDeptName.Value = string.Empty;
            UPDepartmentCreationModal.Update();
            BindDepartments();
            UPddlDepartments1.Update();
            lblDepartmentError.Text = "";
        }

        protected void btnResetDesignation_Click(object sender, EventArgs e)
        {
            txtDesignationName.Value = string.Empty;
            UPtxtDesignationName.Update();
            BindAllDesignations();
            UPddldesignation1.Update();
            lblDesignationError.Text = "";
        }
    }
}
