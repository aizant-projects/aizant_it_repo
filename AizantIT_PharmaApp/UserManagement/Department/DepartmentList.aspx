﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UMS_Master/NestedUMS_Master.Master" AutoEventWireup="true" CodeBehind="DepartmentList.aspx.cs" Inherits="AizantIT_PharmaApp.Department.DepartmentList" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">





    <style>
        .container {
            width: 1390px;
        }

        .table-text-center th, .table-text-center td {
            text-align: center;
        }

        .gridpager tr td {
            padding: 5px 13px;
            background: #38b8ba;
            border: 1px solid #fff;
            color: #000;
        }

        .gridpager table {
            float: right;
        }

        .gridpager, .gridpager td {
            text-align: right;
            color: #38b8ba;
            background: #efffff;
            font-weight: bold;
            text-decoration: none;
        }

            .gridpager a {
                color: White;
                font-weight: normal;
            }
        /*.modal-backdrop {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 1040;
        background-color: #000;
    }
    .modal1 {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 9999;
        display: none;
        overflow: hidden;
        -webkit-overflow-scrolling: touch;
        outline: 0;
    }*/
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>
    <asp:HiddenField ID="HFDeptID" runat="server" />
    <asp:HiddenField ID="hdfVID" runat="server" />
    <asp:HiddenField ID="hdfDname" runat="server" />

    <asp:HiddenField ID="HDDeptCode" runat="server" />
    <asp:HiddenField ID="HDDeptName" runat="server" />
    <%--<div id="DeptList" runat="server">--%>
    <div class=" col-lg-3 padding-none">
        <div class="closebtn"><span>&#9776;</span></div>
        <div style="display: none;" class="closebtn1">
            <span>&#9776;</span>
        </div>
    </div>

    <div style="margin-right: 0px; margin-top: 3px">
        <asp:UpdatePanel ID="upserver" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <%--    <asp:Button ID="btnReview" runat="server" class="float-right btn btn-signup_popup" OnClick="btnSubmit_ServerClick" Text="Create Department"></asp:Button>--%>
                <asp:Button ID="btnReview" runat="server" class="float-right btn btn-signup_popup" OnClick="btnSubmit_ServerClick" OnClientClick="return DepartmentAdded();" Text="Create Department"></asp:Button>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" Style="display: none" />
                <asp:Button ID="btnCommentsHistoryView" runat="server" Text="Submit" Style="display: none" OnClick="btnCommentsHistoryView_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    <div class=" col-lg-12 col-sm-12  col-md-12 col-12 padding-none">
        <div class=" col-lg-12 col-sm-12  col-md-12 col-12 grid_area padding-none">
            <div class="col-lg-12 col-sm-12  col-md-12 col-12 padding-none">
                <div>
                    <div id="header">
                        <div class="col-lg-12 dashboard_panel_header ">
                            Department List
                        </div>
                    </div>
                </div>

                <div id="body" class="col-sm-12" style="overflow: auto;" <%--max-height:400px; max-height:400px;"--%>>
                    <div class="col-sm-12" style="padding-right: 0px; margin-top: 10px">
                        <table id="dtDepartmentList" class="display datatable_cust" cellspacing="0" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Department ID</th>
                                    <th>Department code</th>
                                    <th>Department name</th>
                                    <th>Edit</th>
                                    <th>History</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--------Comment History View-------------->
    <div id="myCommentHistoryDpt" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg modal-dialog-centered" style="width: 80%">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel ID="upcomment" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-header">
                            <span id="span4" runat="server"></span>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>

                        </div>
                        <div class="modal-body" style="max-height: 600px; overflow-y: auto">
                            <div class=" history_grid" style="overflow-y: auto;">
                                <asp:GridView ID="gv_CommentHistory" runat="server" CssClass="table table-hover table-responsive" ClientIDMode="Static" 
                                    EmptyDataText="No Records Found" AutoGenerateColumns="False" PagerSettings-Mode="NumericFirstLast" BackColor="White" 
                                    BorderColor="#d2d2d2" BorderStyle="None" BorderWidth="1px" AllowPaging="true" OnPageIndexChanging="gv_CommentHistory_PageIndexChanging" PageSize="25">
                                    <Columns>
                                        <asp:TemplateField HeaderText="User" ItemStyle-Width="140px" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_GivenBy" runat="server" Text='<%# Eval("GivenBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role" ItemStyle-Width="70px" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Role" runat="server" Text='<%# Eval("CommentedRole") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" ItemStyle-Width="150px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Date" runat="server" Text='<%# Eval("CommentedDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Action" runat="server" Text='<%# Eval("Action") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Submitted Data" ItemStyle-Width="200px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_SubmittedData" runat="server" Text='<%# Eval("SubmittedData") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <PagerStyle  CssClass="gridpager" HorizontalAlign="Right" />
                                </asp:GridView>
                            </div>
                        </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!--------End Comment History View-------------->
    <asp:HiddenField ID="hdnDeptCode" runat="server" Value="" />
    <asp:HiddenField ID="hdnRoleID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnShowType" runat="server" Value="0" />


    <div id="DeptModal" class="modal department fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header ">

                    <h4 class="modal-title">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <asp:Label ID="lblDeptName" runat="server"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" style="padding: 0px;">
                    <asp:UpdatePanel ID="UPDepartmentCreationModal" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="form-group col-sm-12" style="float: none !important">
                                <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">Department Code<span class="smallred_label">*</span></label>
                                <div class="col-sm-12 padding-none" style="float: none !important">
                                    <asp:Label ID="lblDeptid" runat="server" Visible="false"></asp:Label>
                                    <input type="text" class="form-control login_input_sign_up" name="name" id="txtDeptCode" maxlength="5" runat="server" placeholder="New Department Code" onkeypress="return ValidateAlphaNumeric(event)" onpaste="return false" tabindex="7" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12" style="float: none !important">
                                <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">Department Name<span class="smallred_label">*</span></label>
                                <div class="col-sm-12 padding-none" style="float: none !important">
                                    <input type="text" class="form-control login_input_sign_up" name="name" id="txtDeptName" maxlength="50" runat="server" placeholder="New Department Name" onkeypress="return ValidateAlphaNumericSpace(event)" onpaste="return false" tabindex="8" />
                                </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">

                    <asp:UpdatePanel ID="UPbtndeptCreate" runat="server" UpdateMode="Conditional" style="display: inline-block">
                        <ContentTemplate>
                            <asp:Button ID="btnDeptCreate" CssClass="btn-signup_popup" runat="server" Text="Create" data-number="1" OnClientClick="return DepartmentRequired();" TabIndex="9" OnClick="btnDeptCreate_Click" />
                            <button data-dismiss="modal" id="btnDeptCancel" class="btn-cancel_popup" runat="server" tabindex="10">Cancel</button>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
            </div>
            <div class="Panel_Footer_Message_Label Model_Msg" hidden="hidden">
                <asp:UpdatePanel ID="UPDepartmentError" runat="server" UpdateMode="Always" style="padding: 5px">
                    <ContentTemplate>
                        <asp:Label ID="lblDepartmentError" runat="server" Text="" ForeColor="Red"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <script>
    $('#dtDepartmentList thead tr').clone(true).appendTo('#dtDepartmentList thead');
    $('#dtDepartmentList thead tr:eq(1) th').each(function (i) {
        if (i < 4) {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="Search " />');

            $('input', this).on('keyup change', function () {
                if (DepartmentTbl.column(i).search() !== this.value) {
                    DepartmentTbl
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
            if (i == 0 || i == 1)
            {
                $(this).html('');
            }
        }
        else {
            $(this).html('');
        }
    });
        
        var DepartmentTbl = $('#dtDepartmentList').DataTable({
            columns: [
                { 'data': 'RowNumber' },
                { 'data': 'DepartmentID' },
                { 'data': 'DeptCode' },
                { 'data': 'DepartmentName' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) {
                        return '<a class="Edit"  title="Edit" data-target="#DeptModal" data-toggle="modal" onclick="EditEmployeeunlock(' + o.DepartmentID + ',\'' + o.DeptCode + '\',\'' + o.DepartmentName + '\');"></a>';
                    }
                }
                , {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) {
                        { return '<a class="summary_latest" data-target="#myCommentHistoryDpt" data-toggle="modal" title="History"   onclick="ViewCommentHistory(' + o.DepartmentID + ', \'' + o.DepartmentName + '\' )">' + '' + '</a>'; }
                    }
                }
            ],
            "columnDefs": [

                {
                    "targets": [0],
                    "orderable": false,
                },
                {
                    "targets": [1],
                    "visible": false,
                    "searchable": false

                },
                {
                    "className": "dt-body-left", "targets": [2, 3]
                }
            ],
            "orderCellsTop": true,
            "order": [1, "desc"],
            "paging": true,
            "lengthMenu": [[10, 15, 700], [10, 15, "All"]],

            sServerMethod: 'post',
            "sAjaxSource": '/UserManagement/WebServicesList.asmx/GetDepartmentList',
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "DeptCode", "value": $('#<%=hdnDeptCode.ClientID%>').val() }, { "name": "RoleID", "value": $('#<%=hdnRoleID.ClientID%>').val() }, { "name": "ShowType", "value": $('#<%=hdnShowType.ClientID%>').val() });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#dtDepartmentList").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        //$(".DepartmentTblTrainingSessionsClass").css({ "width": "100%" });
                        UserSessionCheck();
                    },
                    error: function (xhr, textStatus, error) {
                        //if (typeof console == "object") {
                        //    console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        //}

                        custAlertMsg("System Error Occurred, Please contact Admin.", "error");
                    }
                });
            }
        });

        function Reloadtable() {
            DepartmentTbl.ajax.reload();
            $('#DeptModal').modal('hide');
        }
        // function ReloadUpdatetable() {
        //    DepartmentTbl.ajax.reload();
        //    $('#DeptModal').modal('hide');
        //}
    </script>


    <script>
        function ViewCommentHistory(PK_ID, Dname) {
            document.getElementById("<%=hdfVID.ClientID%>").value = PK_ID;
                document.getElementById("<%=hdfDname.ClientID%>").value = Dname;

                var btnCV = document.getElementById("<%=btnCommentsHistoryView.ClientID %>");
            btnCV.click();
        }
        function EditEmployeeunlock(E_ID, E_Code, E_Name) {
            document.getElementById("<%=HFDeptID.ClientID%>").value = E_ID;
            document.getElementById("<%=HDDeptCode.ClientID%>").value = E_Code;
            document.getElementById("<%=HDDeptName.ClientID%>").value = E_Name;
            var clickButton = document.getElementById("<%=btnSubmit.ClientID %>");
            clickButton.click();
        }

    </script>
    <script type="text/javascript">   
        function DepartmentRequired() {
            var DeptName = document.getElementById("<%=txtDeptName.ClientID%>").value.trim();
            var DeptCode = document.getElementById("<%=txtDeptCode.ClientID%>").value.trim();
            errors = [];
            if (DeptCode == '') {
                errors.push("Enter Department Code");
                document.getElementById("<%=txtDeptCode.ClientID%>").focus();
            }
            if (DeptName == '') {
                errors.push("Enter Department Name");
                document.getElementById("<%=txtDeptName.ClientID%>").focus();
            }
            if (errors.length > 0)
            {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else
            {
                return true;
            }
        }
    </script>

    <script>
        $(document).keydown(function (event) {
            if (event.keyCode == 27) {
                var clickButton = document.getElementById("<%=btnDeptCancel.ClientID %>");
                clickButton.click();
            }
        });
    </script>

    <script>
        function DepartmentCreatedSucess() {
            $('#ModalError').modal('hide');
            $('#DeptModal').modal('{ show: true}');

        }
    </script>

    <script>

        function ReloadCurrentPage() {

            window.open("<%=ResolveUrl("~/UserManagement/Department/DepartmentList.aspx")%>", "_self");
        }
    </script>
    <script>
        //DepartmentAdded click Before Clear HFDeptID the value  
        function DepartmentAdded() {
            document.getElementById("<%=HFDeptID.ClientID%>").value = "";
        }
    </script>
    <script>
    $(function () {
        $('#DeptModal').on('shown.bs.modal', function () {
            $('#ContentPlaceHolder1_ContentPlaceHolder1_txtDeptCode').focus();
        });
    });
</script>
</asp:Content>


