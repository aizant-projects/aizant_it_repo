﻿using System;
using System.Data;
using System.Web.UI;
using UMS_BusinessLayer;
using AizantIT_PharmaApp.Common;

namespace AizantIT_PharmaApp.Department
{
    public partial class DepartmentList : System.Web.UI.Page
    {
        UMS_BAL objUMS_BAL;
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {              
               
                 if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {                       
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=1");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                   
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DeptList_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DeptList_M1:" + strline + "  " + strMsg, "error");              
            }
        }
        protected void btnDeptCreate_Click(object sender, EventArgs e)
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                if (txtDeptCode.Value.Trim() == "" || txtDeptName.Value.Trim() == "")
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "All Fields Represented With* Are Mandatory", "error");
                 }
                else
                {
                    string DP_Code = HDDeptCode.Value.Trim();
                    string DP_Name = HDDeptName.Value.Trim();

                    string DeptName = txtDeptName.Value.Trim();
                    string DeptCode = txtDeptCode.Value.Trim().ToUpper();
                    int EmpID = 0;
                    if (HelpClass.IsUserAuthenticated())
                    {
                        EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    }
                    DataTable dtTemp = new DataTable();
                    DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                    DataRow[] drUMS = dtx.Select("ModuleID=1");
                    dtTemp = drUMS.CopyToDataTable();
                    int RoleID = Convert.ToInt32(dtTemp.Rows[0]["RoleID"].ToString());
                    if (HFDeptID.Value != "")
                    {
                        string Operation = "Update";
                        int Deptid = Convert.ToInt32(HFDeptID.Value);
                        if (DP_Code == DeptCode && DP_Name == DeptName)
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "There Are No Changes To Update.", "info");
                        }
                        else
                        {
                            int s = objUMS_BAL.DepartmentBAL(Operation, EmpID, Deptid, DeptName, DeptCode, out int VerifyDept, RoleID);

                            if (VerifyDept == 1)
                            {
                                HelpClass.custAlertMsg(this, this.GetType(), "Department Name already Exists", "error");
                            }
                            else if (VerifyDept == 2)
                            {
                                HelpClass.custAlertMsg(this, this.GetType(), "Department Code already Exists", "error");
                            }
                            else if (VerifyDept == 3)
                            {
                                HelpClass.custAlertMsg(this, this.GetType(), "Department Name and Code already Exists", "error");
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", "ShowPleaseWaitdisable();", true);
                            }
                            else
                            {
                                if (s > 0)
                                {
                                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Department Updated Successfully", "success", "Reloadtable();");

                                }
                                else
                                {
                                    HelpClass.custAlertMsg(this, this.GetType(), "Unable to update a department", "error");
                                }
                            }
                        }
                      //  ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", "ShowPleaseWaitdisable();", true);
                    }
                    else if (HFDeptID.Value == "")
                    {
                        string Operation = "Insert";
                        int Deptid = 0;
                        int s = objUMS_BAL.DepartmentBAL(Operation, EmpID, Deptid, DeptName, DeptCode, out int VerifyDept, RoleID);
                        if (VerifyDept == 1)
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Department Name already Exists", "error");
                        }
                        else if (VerifyDept == 2)
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Department Code already Exists", "error");
                        }
                        else if (VerifyDept == 3)
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Department Name and Code already Exists", "error");
                        }
                        else
                        {
                            if (s > 0)
                            {
                                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Department Created Successfully", "success", "Reloadtable()");

                            }
                            else
                            {
                                HelpClass.custAlertMsg(this, this.GetType(), "Unable to create a department", "error");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DeptList_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DeptList_M2:" + strline + "  " + strMsg, "error");
            }
           // ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", "ShowPleaseWaitdisable();", true);

        }
        private void JsMethod(string msg)
        {
            //msg = msg.Replace("'", "");
            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "alert", "alert('" + msg + "')", true);
        }
        protected void btnCommentsHistoryView_Click(object sender, EventArgs e)
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                DataTable DMS_DT = objUMS_BAL.GetDepartmentHistory(Convert.ToInt32(hdfVID.Value));
                gv_CommentHistory.DataSource = DMS_DT;
                gv_CommentHistory.DataBind();
                ViewState["dtCommonHistory"]= DMS_DT;
                if (DMS_DT.Rows.Count > 0)
                {
                    span4.Attributes.Add("title", DMS_DT.Rows[0]["Department"].ToString());
                    if (DMS_DT.Rows[0]["Department"].ToString().Length > 60)
                    {
                        span4.InnerHtml = DMS_DT.Rows[0]["Department"].ToString().Substring(0, 60) + "...";
                    }
                    else
                    {
                        span4.InnerHtml = DMS_DT.Rows[0]["Department"].ToString();
                    }

                }
                else
                {
                    span4.InnerText = hdfDname.Value.ToString();//"History";
                }
                upcomment.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myCommentHistoryDpt').modal({ show: true, backdrop: 'static', keyboard: false });", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MDL_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MDL_M3:" + strline + "  " + "History viewing failed", "error");
            }
        }
        protected void btnSubmit_ServerClick(object sender, EventArgs e)
        {
            try
            {
                lblDeptName.Text = "Add Department";
                btnDeptCreate.Text = "Create";
                UPbtndeptCreate.Update();
                txtDeptCode.Value = "";
                txtDeptName.Value = "";
                UPDepartmentCreationModal.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", " $('#DeptModal').modal({ show: true, backdrop: 'static', keyboard: false });", true);
            }
            catch (Exception ex)
            {
                lblDepartmentError.Text = ex.Message;
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                btnDeptCreate.Text = "Update";
                lblDeptName.Text = "Edit Department";
                UPbtndeptCreate.Update();
                UMS_BAL objUMS_BAL = new UMS_BAL();
                DataTable dt = objUMS_BAL.GetDepartmentDetails(Convert.ToInt32(HFDeptID.Value));
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    txtDeptCode.Value = dr["DeptCode"].ToString();
                    txtDeptName.Value = dr["DepartmentName"].ToString();
                    UPDepartmentCreationModal.Update();
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", " $('#DeptModal').modal({ show: true, backdrop: 'static', keyboard: false });", true);
            }
            catch (Exception ex)
            {
                lblDepartmentError.Text = ex.Message;
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }

        protected void gv_CommentHistory_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            gv_CommentHistory.PageIndex = e.NewPageIndex;
            gv_CommentHistory.DataSource =(DataTable)ViewState["dtCommonHistory"];
            gv_CommentHistory.DataBind();
        }
    }
}