﻿

namespace AizantIT_PharmaApp.UserManagement.UMSModel
{
    public class UmsModuleList
    {
        public int ModuleID { get; set; }
        public string ModuleName { get; set; }
    }
}