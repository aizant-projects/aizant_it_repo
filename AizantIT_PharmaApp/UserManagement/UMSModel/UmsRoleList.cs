﻿using System;


namespace AizantIT_PharmaApp.UserManagement.UMSModel
{
    public class UmsRoleList
    {
        public int? RoleID { get; set; }
        public string RoleName { get; set; }
        public Boolean IsAccessible { get; set; }
        public int ModuleID { get; set; }
        public string ModuleName { get; set; }
    }
}