﻿
using System.Collections.Generic;


namespace AizantIT_PharmaApp.UserManagement.UMSModel
{
    public class JsonResponce
    {


        public string Message { get; set; }
        public bool Status { get; set; }
        public int Data { get; set; }

        public List<UmsRoleList> RolesList { get; set; }
        public List<UmsDepartmentList> DepartmentsList { get; set; }
        public List<UmsModuleList> ModuleList { get; set; }
        public bool IsAccessDept { get; set; }


    }
}