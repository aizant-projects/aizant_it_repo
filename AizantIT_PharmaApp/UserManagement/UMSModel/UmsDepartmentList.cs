﻿using System;


namespace AizantIT_PharmaApp.UserManagement.UMSModel
{
    public class UmsDepartmentList
    {
        public int DeptID { get; set; }
        public string DepartmentName { get; set; }

        public int RoleId { get; set; }
        public string RoleName { get; set; }

        public int ModuleID { get; set; }
        public string ModuleName { get; set; }
        public Boolean IsAccessible { get; set; }
    }
}