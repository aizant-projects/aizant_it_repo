﻿using System;
using System.Web;
using UMS_BO;
using UMS_BusinessLayer;
using System.Data;
using AizantIT_PharmaApp.Common;
using System.Text;

namespace AizantIT_PharmaApp.UserManagement
{
    public partial class CompanyPage : System.Web.UI.Page
    {
        UMS_BAL objUMS_BAL;
        CompanyBO objCompanyBO;
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //log.Debug(strMsg);,log.Info(strMsg);,log.Warn(strMsg);,log.Error(strMsg);,log.Fatal(strMsg);
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            btnUpdate.Visible = true;
            btnCancel.Visible = true;
            btnEdit.Visible = false;
            enableCompany();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (HelpClass.IsUserAuthenticated())
            {
                if (!IsPostBack)
                {
                    Initialize();

                }
            }
            else
            {
                Response.Redirect("~/UserLogin.aspx", false);
            }
        }
        public void Initialize()
        {
            try
            {
            
            DataTable dtcompany = new DataTable();
            objCompanyBO = new CompanyBO();
            objCompanyBO.Mode = 3;
            objCompanyBO.CompanyID = 0;
            objCompanyBO.CompanyCode = txtCompanyCode.Value;
            objCompanyBO.CompanyName = txtCompanyName.Text;
            objCompanyBO.CompanyDescription = txtCompanyDescription.Value;
            objCompanyBO.CompanyPhoneNo1 = txtCompanyPhoneNo1.Text;
            objCompanyBO.CompanyPhoneNo2 = txtCompanyPhoneNo2.Text;
            objCompanyBO.CompanyFaxNo1 = txtFaxNo1.Text;
            objCompanyBO.CompanyFaxNo2 = ""; //txtFaxNo2.Value;
            objCompanyBO.CompanyEmailID = txtEmailID.Value;
            objCompanyBO.CompanyWebUrl = txtWebsite.Value;
            objCompanyBO.CompanyAddress = txtAddress.Value;
            objCompanyBO.CompanyLocation = "";// txtLocat.Value;
            objCompanyBO.CompanyCity = txtCity.Value;
            objCompanyBO.CompanyState = txtState.Value;
            objCompanyBO.CompanyCountry = txtCountry.Value;
            objCompanyBO.CompanyPinCode = txtPincode.Value;
            objCompanyBO.CompanyStartDate =  txtStartDate.Value == "" ? "" : Convert.ToDateTime(txtStartDate.Value).ToString("yyyyMMdd");
                objCompanyBO.CompanyLogo = new byte[0];
            objUMS_BAL = new UMS_BAL();
            dtcompany = objUMS_BAL.CompanyQuery(objCompanyBO);
            if (Convert.ToInt32(dtcompany.Rows[0][0].ToString())>0)
            {
                ViewState["isUpdate"] = "1";
                btnEdit.Visible = true;
                fillCompany();
                    disableCompany();
            }
            else
            {
                ViewState["isUpdate"] = "0";
                btnSubmit.Visible = true;
                btnCancel.Visible = true;
            }
            }
            catch (Exception ex)
            {

                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("CompanyPage_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "CompanyPage_M1:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UserManagement/CompanyPage.aspx");

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder sbErrorMsg = new StringBuilder();
                if (txtCompanyName.Text.Trim() == "")
                {
                    sbErrorMsg.Append(" Enter company name." + "<br/>");
                }
                if (sbErrorMsg.Length > 8)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), sbErrorMsg.ToString(), "error");
                    return;
                }
                DataTable dtcompany = new DataTable();
                objCompanyBO = new CompanyBO();
                objCompanyBO.Mode = 0;
                objCompanyBO.CompanyID = 0;
                objCompanyBO.CompanyCode = txtCompanyCode.Value.Trim();
                objCompanyBO.CompanyName = txtCompanyName.Text.Trim();
                objCompanyBO.CompanyDescription = txtCompanyDescription.Value.Trim();
                objCompanyBO.CompanyPhoneNo1 = txtCompanyPhoneNo1.Text.Trim();
                objCompanyBO.CompanyPhoneNo2 = txtCompanyPhoneNo2.Text.Trim();
                objCompanyBO.CompanyFaxNo1 = txtFaxNo1.Text.Trim();
                objCompanyBO.CompanyFaxNo2 = ""; //txtFaxNo2.Value;
                objCompanyBO.CompanyEmailID = txtEmailID.Value.Trim();
                objCompanyBO.CompanyWebUrl = txtWebsite.Value.Trim();
                objCompanyBO.CompanyAddress = txtAddress.Value.Trim();
                objCompanyBO.CompanyLocation = "";// txtLocat.Value;
                objCompanyBO.CompanyCity = txtCity.Value.Trim();
                objCompanyBO.CompanyState = txtState.Value.Trim();
                objCompanyBO.CompanyCountry = txtCountry.Value.Trim();
                objCompanyBO.CompanyPinCode = txtPincode.Value.Trim();
                objCompanyBO.CompanyStartDate =txtStartDate.Value.Trim()== ""?"": Convert.ToDateTime(txtStartDate.Value.Trim()).ToString("yyyyMMdd");
                if (HelpClass.IsUserAuthenticated())
                {
                    objCompanyBO.createdBY = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                }
                else
                    objCompanyBO.createdBY = 0;
                if (ViewState["compImg"] != null)
                {
                    //byte[] barrImg = (byte[])(hdnImage.Value.ToString());
                    objCompanyBO.CompanyLogo = (byte[])(ViewState["compImg"]);
                                    }
                else
                {
                    objCompanyBO.CompanyLogo = EmpImage();
                    if (ViewState["compImg"] == null)
                    {
                        sbErrorMsg.Append(" Please upload Company logo." + "<br/>");
                       // HelpClass.custAlertMsg(this, this.GetType(), "Please upload Company logo", "error");
                       // return;
                    }
                }
               
                objUMS_BAL = new UMS_BAL();
                dtcompany = objUMS_BAL.CompanyQuery(objCompanyBO);
                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Company details created Successfully", "success", "ReloadCurrentPage()");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("CMP_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "CMP_M2:" + strline + "  " + strMsg, "error");
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder sbErrorMsg = new StringBuilder();
                if (txtCompanyName.Text.Trim() == "")
                {
                    sbErrorMsg.Append(" Enter company name." + "<br/>");
                }
                if (sbErrorMsg.Length > 8)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), sbErrorMsg.ToString(), "error");
                    return;
                }
                DataTable dtcompany = new DataTable();
                objCompanyBO = new CompanyBO();
                objCompanyBO.Mode = 1;
                objCompanyBO.CompanyID = 0;
                objCompanyBO.CompanyCode = txtCompanyCode.Value.Trim();
                objCompanyBO.CompanyName = txtCompanyName.Text.Trim();
                objCompanyBO.CompanyDescription = txtCompanyDescription.Value.Trim();
                objCompanyBO.CompanyPhoneNo1 = txtCompanyPhoneNo1.Text.Trim();
                objCompanyBO.CompanyPhoneNo2 = txtCompanyPhoneNo2.Text.Trim();
                objCompanyBO.CompanyFaxNo1 = txtFaxNo1.Text.Trim();
                objCompanyBO.CompanyFaxNo2 = ""; //txtFaxNo2.Value;
                objCompanyBO.CompanyEmailID = txtEmailID.Value.Trim();
                objCompanyBO.CompanyWebUrl = txtWebsite.Value.Trim();
                objCompanyBO.CompanyAddress = txtAddress.Value.Trim();
                objCompanyBO.CompanyLocation = "";// txtLocat.Value;
                objCompanyBO.CompanyCity = txtCity.Value.Trim();
                objCompanyBO.CompanyState = txtState.Value.Trim();
                objCompanyBO.CompanyCountry = txtCountry.Value.Trim();
                objCompanyBO.CompanyPinCode = txtPincode.Value.Trim();
                objCompanyBO.CompanyStartDate = txtStartDate.Value.Trim() == "" ? "" : Convert.ToDateTime(txtStartDate.Value.Trim()).ToString("yyyyMMdd");
                if (HelpClass.IsUserAuthenticated())
                {
                    objCompanyBO.createdBY = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                }
                else
                    objCompanyBO.createdBY = 0;
                if (FileUpload1.HasFile)
                {
                    objCompanyBO.CompanyLogo = EmpImage();
                   
                }
                else  if (ViewState["compImg"] != null)
                {
                    //byte[] barrImg = (byte[])(hdnImage.Value.ToString());
                    objCompanyBO.CompanyLogo = (byte[])(ViewState["compImg"]);
                }
                
                objUMS_BAL = new UMS_BAL();
                dtcompany = objUMS_BAL.CompanyQuery(objCompanyBO);
                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Company details modified successfully", "success", "ReloadCurrentPage()");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("CMP_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "CMP_M3:" + strline + "  " + strMsg, "error");
            }
        }
        void fillCompany()
        {
            DataTable dtcompany = new DataTable();
            objCompanyBO = new CompanyBO();
            objCompanyBO.Mode = 2;
            objCompanyBO.CompanyID = 0;
            objCompanyBO.CompanyCode = txtCompanyCode.Value;
            objCompanyBO.CompanyName = txtCompanyName.Text;
            objCompanyBO.CompanyDescription = txtCompanyDescription.Value;
            objCompanyBO.CompanyPhoneNo1 = txtCompanyPhoneNo1.Text;
            objCompanyBO.CompanyPhoneNo2 = txtCompanyPhoneNo2.Text;
            objCompanyBO.CompanyFaxNo1 = txtFaxNo1.Text;
            objCompanyBO.CompanyFaxNo2 = ""; //txtFaxNo2.Value;
            objCompanyBO.CompanyEmailID = txtEmailID.Value;
            objCompanyBO.CompanyWebUrl = txtWebsite.Value;
            objCompanyBO.CompanyAddress = txtAddress.Value;
            objCompanyBO.CompanyLocation = "";// txtLocat.Value;
            objCompanyBO.CompanyCity = txtCity.Value;
            objCompanyBO.CompanyState = txtState.Value;
            objCompanyBO.CompanyCountry = txtCountry.Value;
            objCompanyBO.CompanyPinCode = txtPincode.Value;
            objCompanyBO.CompanyStartDate = txtStartDate.Value == "" ? "" : Convert.ToDateTime(txtStartDate.Value).ToString("yyyyMMdd");
            objCompanyBO.CompanyLogo = new byte[0];

            objUMS_BAL = new UMS_BAL();
            dtcompany = objUMS_BAL.CompanyQuery(objCompanyBO);
            if (dtcompany.Rows.Count > 0)
            {
                txtCompanyCode.Value = dtcompany.Rows[0]["CompanyCode"].ToString();
                txtCompanyName.Text = dtcompany.Rows[0]["CompanyName"].ToString();
                txtCompanyDescription.Value = dtcompany.Rows[0]["CompanyDescription"].ToString();
                txtCompanyPhoneNo1.Text = dtcompany.Rows[0]["CompanyPhoneNo1"].ToString();
                txtCompanyPhoneNo2.Text = dtcompany.Rows[0]["CompanyPhoneNo2"].ToString();
                txtFaxNo1.Text = dtcompany.Rows[0]["CompanyFaxNo1"].ToString();
                // txtFaxNo2.value = dtcompany.Rows[0]["CompanyFaxNo2"].ToString();
                txtEmailID.Value = dtcompany.Rows[0]["CompanyEmailID"].ToString();
                txtWebsite.Value = dtcompany.Rows[0]["CompanyWebUrl"].ToString();
                txtAddress.Value = dtcompany.Rows[0]["CompanyAddress"].ToString();
               // txtLocation.Text = dtcompany.Rows[0]["CompanyLocation"].ToString();
                txtCity.Value = dtcompany.Rows[0]["CompanyCity"].ToString();
                txtState.Value = dtcompany.Rows[0]["CompanyState"].ToString();
                txtCountry.Value = dtcompany.Rows[0]["CompanyCountry"].ToString();
                txtPincode.Value = dtcompany.Rows[0]["CompanyPinCode"].ToString();
                if (dtcompany.Rows[0]["CompanyStartDate"].ToString() != "")
                {
                    if (dtcompany.Rows[0]["CompanyStartDate"].ToString().Contains("1900"))
                    { txtStartDate.Value = ""; }
                    else
                    {
                        DateTime StartDate = (DateTime)dtcompany.Rows[0]["CompanyStartDate"];
                        txtStartDate.Value = StartDate.ToString("dd MMM yyyy");
                    }
                }
                else
                    txtStartDate.Value = "";

                if (dtcompany.Rows[0]["CompanyLogo"] != null)
                {
                    BindApplicantImage((byte[])dtcompany.Rows[0]["CompanyLogo"]);
                    ViewState["compImg"] = (byte[])dtcompany.Rows[0]["CompanyLogo"];
                    hdfLogo.Value = "1";
                }
            }
        }
        public byte[] EmpImage()
        {
            byte[] imgbyte = new byte[0];
            ViewState["compImg"] = null;
            if (FileUpload1.HasFile)
            {
                //getting length of uploaded file  
                int length = FileUpload1.PostedFile.ContentLength;
                //create a byte array to store the binary image data  
                imgbyte = new byte[length];
                //store the currently selected file in memeory  
                HttpPostedFile img = FileUpload1.PostedFile;
                //set the binary data  
                img.InputStream.Read(imgbyte, 0, length);
                ViewState["compImg"] = imgbyte;
            }           
           
            return imgbyte;
        }
        private void BindApplicantImage(byte[] applicantImage)
        {
            string base64ImageString =  ConvertBytesToBase64(applicantImage);
            if(base64ImageString.Length>0)
            {
                imgEmpImage.ImageUrl = "data:image/jpg;base64," + base64ImageString;
            }
            else
            {
                imgEmpImage.ImageUrl = "~/Images/Landing/logo.png";
            }
        }
        public string ConvertBytesToBase64(byte[] imageBytes)
        {
            return Convert.ToBase64String(imageBytes);
        }
        void disableCompany()
        {
            txtCompanyCode.Attributes.Add("readonly", "readonly");
            txtCompanyName.Attributes.Add("readonly", "readonly"); 
            txtCompanyDescription.Attributes.Add("readonly", "readonly");
            //txtCompanyPhoneNo1.Enabled = false; 
            //txtCompanyPhoneNo2.Enabled = false;
            //txtFaxNo1.Enabled = false;

            txtCompanyPhoneNo1.Attributes.Add("readonly", "readonly");
            txtCompanyPhoneNo2.Attributes.Add("readonly", "readonly");
            txtFaxNo1.Attributes.Add("readonly", "readonly");

            // txtFaxNo2.Attributes.Add("readonly", "readonly"); 
            txtEmailID.Attributes.Add("readonly", "readonly"); 
            txtWebsite.Attributes.Add("readonly", "readonly"); 
            txtAddress.Attributes.Add("readonly", "readonly"); 
            // txtLocation.Text 
            txtCity.Attributes.Add("readonly", "readonly"); 
            txtState.Attributes.Add("readonly", "readonly"); 
            txtCountry.Attributes.Add("readonly", "readonly"); 
            txtPincode.Attributes.Add("readonly", "readonly"); 
           
                txtStartDate.Attributes.Add("readonly", "readonly");
            dvFup.Visible = false;
        }
        void enableCompany()
        {
            txtCompanyCode.Attributes.Remove("readonly");
            txtCompanyName.Attributes.Remove("readonly");
            txtCompanyDescription.Attributes.Remove("readonly");
            //txtCompanyPhoneNo1.Enabled = true;
            //txtCompanyPhoneNo2.Enabled = true;
            //txtFaxNo1.Enabled = true;


            txtCompanyPhoneNo1.Attributes.Remove("readonly");
            txtCompanyPhoneNo2.Attributes.Remove("readonly");
            txtFaxNo1.Attributes.Remove("readonly");
            // txtFaxNo2.Attributes.Add("readonly", "readonly"); 
            txtEmailID.Attributes.Remove("readonly");
            txtWebsite.Attributes.Remove("readonly");
            txtAddress.Attributes.Remove("readonly");
            // txtLocation.Text 
            txtCity.Attributes.Remove("readonly");
            txtState.Attributes.Remove("readonly");
            txtCountry.Attributes.Remove("readonly");
            txtPincode.Attributes.Remove("readonly");

            txtStartDate.Attributes.Remove("readonly");
            dvFup.Visible = true;
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}