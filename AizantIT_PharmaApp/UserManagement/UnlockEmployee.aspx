﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UMS_Master/UMS_Master.Master" AutoEventWireup="true" CodeBehind="UnlockEmployee.aspx.cs" Inherits="AizantIT_PharmaApp.UserManagement.UnlockEmployee" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style>
        .txtCenter {
           text-align:center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div class="row Panel_Frame">
            <div id="header">
                <div class="col-sm-12 Panel_Header" >
                    <div class="col-sm-6" style="padding-left: 5px">
                        <span class="Panel_Title">Un-Lock Employee List</span>
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div>
            <div id="body" style="margin-left:10px;margin-right:10px">
                <asp:GridView  ID="gvUnlockEmployee" 
                    runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered"
                    EmptyDataText="No More Employee's to UnLock" EmptyDataRowStyle-CssClass="GV_EmptyDataStyle">
                     <Columns>                                                
                         <asp:TemplateField ItemStyle-CssClass="txtCenter" HeaderStyle-CssClass="txtCenter">
                             <HeaderTemplate>
                                 <asp:CheckBox ID="checkAll" runat="server" onclick = "checkAll(this);"/>
                             </HeaderTemplate>
                             <ItemTemplate>
                                 <asp:CheckBox ID="CheckBox1" runat="server" onclick = "Check_Click(this)"/>
                             </ItemTemplate>
                         </asp:TemplateField>
                         <asp:TemplateField HeaderText="Employee ID" SortExpression="EmpID" Visible="false">
                             <EditItemTemplate>
                                 <asp:TextBox ID="txtGVEmpID" runat="server" Text='<%# Bind("EmpID") %>'></asp:TextBox>
                             </EditItemTemplate>
                             <ItemTemplate>
                                 <asp:Label ID="lblGVEmpID" runat="server" Text='<%# Bind("EmpID") %>'></asp:Label>
                             </ItemTemplate>
                         </asp:TemplateField>
                         <asp:TemplateField HeaderText="Employee Name" SortExpression="Name">
                             <EditItemTemplate>
                                 <asp:TextBox ID="txtGVName" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                             </EditItemTemplate>
                             <ItemTemplate>
                                 <asp:Label ID="lblGVName" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                             </ItemTemplate>
                         </asp:TemplateField>
                         <asp:TemplateField HeaderText="Department" SortExpression="DepartmentName">
                             <EditItemTemplate>
                                 <asp:TextBox ID="txtGVDepartmentName" runat="server" Text='<%# Bind("DepartmentName") %>'></asp:TextBox>
                             </EditItemTemplate>
                             <ItemTemplate>
                                 <asp:Label ID="lblGVDepartmentName" runat="server" Text='<%# Bind("DepartmentName") %>'></asp:Label>
                             </ItemTemplate>
                         </asp:TemplateField>
                          <asp:TemplateField HeaderText="Designation" SortExpression="DesignationName">
                             <EditItemTemplate>
                                 <asp:TextBox ID="txtGVDesignationName" runat="server" Text='<%# Bind("DesignationName") %>'></asp:TextBox>
                             </EditItemTemplate>
                             <ItemTemplate>
                                 <asp:Label ID="lblGVDesignationName" runat="server" Text='<%# Bind("DesignationName") %>'></asp:Label>
                             </ItemTemplate>
                         </asp:TemplateField>
                          <asp:TemplateField HeaderText="Start Date" SortExpression="StartDate">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtGVStartDate" runat="server" Text='<%# Bind("StartDate") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblGVStartDate" runat="server" Text='<%#Eval("StartDate","{0:dd/MMM/yyyy}")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                         <asp:TemplateField HeaderText="Locked" SortExpression="Locked" Visible="false">
                             <EditItemTemplate>
                                 <asp:TextBox ID="txtGVLocked" runat="server" Text='<%# Bind("Locked") %>'></asp:TextBox>
                             </EditItemTemplate>
                             <ItemTemplate>
                                 <asp:Label ID="lblGVLocked" runat="server" Text='<%#Eval("Locked")%>'></asp:Label>
                             </ItemTemplate>
                         </asp:TemplateField>
                         <asp:TemplateField HeaderText="LoginID" SortExpression="LoginID">
                             <EditItemTemplate>
                                 <asp:TextBox ID="txtGVLoginID" runat="server" Text='<%# Bind("LoginID") %>'></asp:TextBox>
                             </EditItemTemplate>
                             <ItemTemplate>
                                 <asp:Label ID="lblgvLoginID" runat="server" Text='<%#Eval("LoginID")%>'></asp:Label>
                             </ItemTemplate>
                         </asp:TemplateField>
                        
                     </Columns>
                    
                 </asp:GridView>
                </div>
             <div class="col-sm-12 modal-footer Panel_Footer">
                 <div class="Panel_Footer_Buttons">
                  <asp:Button ID="btnUnLock" CssClass="button" runat="server" Text="UnLock" OnClick="btnUnLock_Click" ToolTip="Click to Unlock Selected Employee(s)"/>
                <asp:Button ID="btnReset" CssClass="button" runat="server" Text="Reset" OnClick="btnReset_Click" ToolTip="Click to Reset"/>
                     </div>
                  <div class="Panel_Footer_Message_Label">
                       <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
                      </div>
             </div>
         </div>
     </div>
  

    <script type = "text/javascript">
        function Check_Click(objRef) {
            //Get the Row based on checkbox
            var row = objRef.parentNode.parentNode;
           
            //Get the reference of GridView
            var GridView = row.parentNode;

            //Get all input elements in Gridview
            var inputList = GridView.getElementsByTagName("input");
           
            for (var i = 0; i < inputList.length; i++) {
                //The First element is the Header Checkbox
                var headerCheckBox = inputList[0];
               
                //Based on all or none checkboxes
                //are checked check/uncheck Header Checkbox
                var checked = true;
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                        inputList[i].removeAttribute("checked", "checked");
                        break;
                    }
                    else {
                        checked = true;
                        inputList[i].setAttribute("checked", "checked");
                    }
                }
            }
            headerCheckBox.checked = checked;
        }
</script>

    <script type = "text/javascript">
        function checkAll(objRef) {
            var GridView = objRef.parentNode.parentNode.parentNode;
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                //Get the Cell To find out ColumnIndex
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                    if (objRef.checked) {
                        //If the header checkbox is checked
                        //check all checkboxes                        
                        inputList[i].checked = true;
                        inputList[i].setAttribute("checked", "checked");
                    }
                    else {
                        //If the header checkbox is checked
                        //uncheck all checkboxes                       
                        inputList[i].checked = false;
                        inputList[i].removeAttribute("checked", "checked");
                    }
                }
            }
        }
</script> 

    <script>
        $("#NavLnkActiveEmp").attr("class","active");
    </script>
</asp:Content>
