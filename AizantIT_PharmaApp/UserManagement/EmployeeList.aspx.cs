﻿
using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_BusinessLayer;
namespace AizantIT_PharmaApp.UserManagement
{
    public partial class EmployeeList : System.Web.UI.Page
    {
        UMS_BAL objUMS_BAL = new UMS_BAL();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        hdModuleId.Value = "1";//1  UMS Module

                        if ((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString() == "0")
                        {
                            if (Request.QueryString["A"] == "3")//UnAssignRoles --3
                            {
                                Session["UnAssignRoles"] = 3;
                            }
                            else
                            {
                                Session["UnAssignRoles"] = null;
                            }
                            if (Request.QueryString["L"] == "2")//Locked Employees--2
                            {
                                Session["DashBoard"] = 2;
                            }
                            else
                            {
                                Session["DashBoard"] = null;
                            }
                        }
                        else
                        {
                            DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                            DataTable dtTemp = new DataTable();
                            DataRow[] drUMS = dt.Select("ModuleID=1");
                            if (drUMS.Length > 0)
                            {
                                dtTemp = drUMS.CopyToDataTable();
                                Session["UnAssingRoles"] = null;
                                Session["DashBoard"] = null;

                                if ((Request.QueryString["T"] != null))
                                {
                                    if (Request.QueryString["T"].ToString() == "1")//Total Employees--1
                                    {
                                        hdnShowType.Value = "1";
                                    }
                                    else
                                    {
                                        hdnShowType.Value = "0";
                                    }
                                }
                                if ((Request.QueryString["L"] != null))
                                {
                                    if (Request.QueryString["L"].ToString() == "2")//LockedEmployees--2
                                    {
                                        hdnShowType.Value = "2";
                                    }
                                    else
                                    {
                                        hdnShowType.Value = "0";
                                    }
                                }

                                if ((Request.QueryString["A"] != null))
                                {
                                    if (Request.QueryString["A"].ToString() == "3")//Employe with no Roles--3
                                    {
                                        hdnShowType.Value = "3";
                                    }
                                    else
                                    {
                                        hdnShowType.Value = "0";
                                    }
                                }
                                //Department Wise Employees Chart
                                if (Request.QueryString["DWECode"] != null)
                                {
                                    hdnDeptCode.Value = Request.QueryString["DWECode"];
                                    hdnShowType.Value = "4";
                                }
                                // Roles Employees on  Department
                                if (Request.QueryString["DeptCode"] != null)
                                {
                                    hdnDeptCode.Value= Request.QueryString["DeptCode"]; 
                                    Session["DashBoard"] = Request.QueryString["DeptCode"];
                                    hdnShowType.Value = "5"; 
                                }
                                if (Request.QueryString["RoleID"] != null)
                                {
                                    hdnRoleID.Value = Request.QueryString["RoleID"];
                                    hdnShowType.Value = "5";
                                }
                            }
                            else
                            {
                                Response.Redirect("~/UserLogin.aspx", false);
                            }
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EmpList_M1:" + strline + "  " + strMsg);
                HelpClass.showMsg(this, this.GetType(), "EmpList_M1:" + strline + "  " + strMsg);
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                int EmpID = 0;
                if (HelpClass.IsUserAuthenticated())
                {
                    EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                }
                DataTable dtTemp = new DataTable();
                DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                DataRow[] drUMS = dtx.Select("ModuleID=1");
                dtTemp = drUMS.CopyToDataTable();
                int RoleID = Convert.ToInt32(dtTemp.Rows[0]["RoleID"].ToString());
                int Count = objUMS_BAL.UpdateEmpLockBal(txtEmpID.Value, txtComments.Value, out int status, EmpID, RoleID);

                if (status == 1)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "User will be UnLocked after UserLogin Registration", "info");
                }
                if (status == 2)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Locked Successfully", "success", "Reloadtable();");
                }
                if (status == 3)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Unlocked Successfully", "success", "Reloadtable();");
                }
                
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EmpList_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EmpList_M2:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnEmployee_Click(object sender, EventArgs e)
        {

        }
        protected void btnCommentsHistoryView_Click(object sender, EventArgs e)
        {
            try
            {
                gv_CommentHistory.Visible = true;
                gvUserLoginHistory.Visible = true;
                var umsModuleID = Convert.ToInt32(hdModuleId.Value);
                UMS_BAL objUMS_BAL = new UMS_BAL();
                DataTable DMS_DT = objUMS_BAL.GetEmployeeAuditHistory(Convert.ToInt32(hdfVID.Value),umsModuleID);
                gv_CommentHistory.DataSource = DMS_DT;
                gv_CommentHistory.DataBind();
                ViewState["dtCommentHistory"] = DMS_DT;
                DataTable DMS_DTU = objUMS_BAL.GetUserLoginHistory(Convert.ToInt32(hdfVID.Value));
                gvUserLoginHistory.DataSource = DMS_DTU;
                gvUserLoginHistory.DataBind();
                ViewState["dtLoginHistory"] = DMS_DTU;
                if (DMS_DT.Rows.Count > 0)
                {
                    span4.Attributes.Add("title",DMS_DT.Rows[0]["Employee"].ToString());
                    if (DMS_DT.Rows[0]["Employee"].ToString().Length > 60)
                    {
                        span4.InnerHtml = DMS_DT.Rows[0]["Employee"].ToString().Substring(0, 60) + "...";
                    }
                    else
                    {
                        span4.InnerHtml = DMS_DT.Rows[0]["Employee"].ToString();
                    }


                }
                else
                {
                    //span4.InnerText = "History";
                }
                gvUserLoginHistory.Visible = false;
                upcomment.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myCommentHistory').modal({ show: true, backdrop: 'static', keyboard: false });", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MDL_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MDL_M3:" + strline + "  " + "History viewing failed", "error");
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }

        protected void btnUserLoginHistory_Click(object sender, EventArgs e)
        {
            try
            {
                if (gv_CommentHistory.Visible)
                {
                    gvUserLoginHistory.Visible = true;
                    gv_CommentHistory.Visible = false;
                }
                else
                {
                    gvUserLoginHistory.Visible = false;
                    gv_CommentHistory.Visible = true;
                }

                upcomment.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myCommentHistory').modal({ show: true, backdrop: 'static', keyboard: false });", true);
            }
            catch (Exception)
            {

                throw;
            }
        }

        protected void gv_CommentHistory_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            gv_CommentHistory.PageIndex = e.NewPageIndex;
            gv_CommentHistory.DataSource = (DataTable)ViewState["dtCommentHistory"]; 
            gv_CommentHistory.DataBind();
        }

        protected void gvUserLoginHistory_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            gvUserLoginHistory.PageIndex = e.NewPageIndex;
            gvUserLoginHistory.DataSource = (DataTable)ViewState["dtLoginHistory"];
            gvUserLoginHistory.DataBind();
        }
    }
}