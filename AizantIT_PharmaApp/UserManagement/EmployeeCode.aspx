﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UMS_Master/NestedUMS_Master.Master" AutoEventWireup="true" CodeBehind="EmployeeCode.aspx.cs" Inherits="AizantIT_PharmaApp.UserManagement.EmployeeCode" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>
    <link href="<%=ResolveUrl("~/AppCSS/CreateUserStyles.css")%>" rel="stylesheet" />
    <div class=" col-lg-3 padding-none float-left">
        <div class="closebtn"><span>&#9776;</span></div>
        <div style="display: none;" class="closebtn1">
            <span>&#9776;</span>

        </div>
    </div>
    <div class="col-sm-12 col-lg-8 col-md-12 col-12 offset-2 float-left ">
        <div class="row Panel_Frame col-12 float-left padding-none">
           
        
                <div class="col-lg-12 col-sm-12  col-md-12 col-12 dashboard_panel_header float-left">
                    <div class="col-sm-6 float-left">
                   Employee Code
                        </div>
                    <div class="col-sm-6 float-left">
                        <asp:HiddenField ID="HFDesgID" runat="server" />
                    </div>
                </div>
       

            <div class="modal-body">
                <div class="row" style="margin-left: 10px; margin-right: 10px">
                    <div class="form-group col-12">
                        <label for="name" class="col-sm-4 float-left control-label">Character</label>
                        <div class="col-sm-7 float-left">
                            <input type="text" class="form-control"  style="" name="name" id="txtEmployeeCharacter" maxlength="50" runat="server" placeholder="Enter Character" onkeypress="return ValidateAlphaNumericSpace(event)" onpaste="return false" tabindex="1" title="Designation Name" autocomplete="off" />
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-left: 10px; margin-right: 10px; margin-top: 10px">
                    <div class="form-group col-12">
                        <label for="name" class="col-sm-4 float-left control-label">Employee code<span class="smallred_label">*</span></label>
                        <div class="col-sm-7 float-left">
                            <input type="text" class="form-control" style="" name="name" id="txtEmployeeCodeName" runat="server" placeholder="Enter Employee Code" onkeypress="return ValidatePhoneNo(event)" onpaste="return false" tabindex="2" title="Designation Name" maxlength="20" autocomplete="off" />
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-sm-12 modal-footer Panel_Footer float-left text-right " id="divFooter" runat="server">
                <div class="Panel_Footer_Buttons">
                   
                                            <asp:Button ID="btnSubmit" class=" btn btn-signup_popup" runat="server" Text="Submit" OnClientClick="javascript:return EmployeeRequired();" TabIndex="2" OnClick="btnSubmit_Click" />

                        <button type="button" id="btnEmployeeClear" runat="server" class="  btn btn-cancel_popup" tabindex="3" title="click to Clear" onclientclick="javascript:return ResetValues();">Clear</button>
                        <%--<asp:Button ID="btnUpdate" CssClass="button" runat="server" Text="Update" OnClick="btnUpdate_Click"/>--%>
                    
                </div>
                <div class="Panel_Footer_Message_Label">
                    <asp:Label ID="lblEmployeeError" runat="server" Text="" ForeColor="Red"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    
    <script>
       //for clearing the Employee
       $('#<%=btnEmployeeClear.ClientID%>').click(function () {
           $('#<%=txtEmployeeCodeName.ClientID%>').val("");
           $('#<%=txtEmployeeCharacter.ClientID%>').val("");
           document.getElementById("<%=lblEmployeeError.ClientID%>").innerHTML = "";
       });
       //for mandatory fields in Employee Modal popup
       function EmployeeRequired() {
           errors = [];
           var NewDesig = document.getElementById("<%=txtEmployeeCodeName.ClientID%>").value.trim();

           if (NewDesig == "") {
               errors.push("Enter Employee Code");            
             
             }
             
           if (errors.length > 0) {
               custAlertMsg(errors.join("<br/>"), "error");
               return false;
           }
           else {
               return true;
           }
       }
    </script>
    <script type="text/javascript">
        function ResetValues() {
            document.getElementById('<%=txtEmployeeCharacter.ClientID%>') = '';
            document.getElementById('<%=txtEmployeeCodeName.ClientID%>') = '';
        }
        function Reload() {
            window.open("<%=ResolveUrl("~/UserManagement/EmployeeCode.aspx")%>", "_self");
        }
    </script>

    <script>
        $("#NavLnkAppStarts").attr("class", "active");
        $("#NavLnkEmpCode").attr("class", "active");
    </script>
</asp:Content>
