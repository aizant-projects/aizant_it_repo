﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UMS_Master/NestedUMS_Master.Master"
    AutoEventWireup="true" CodeBehind="DesignationList.aspx.cs" Inherits="AizantIT_PharmaApp.Designation.DesignationList" %>

<%@ Register Src="~/UserControls/SucessAlert.ascx" TagPrefix="uc1" TagName="SucessAlert" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .container {
            width: 1390px;
        }

        .table-text-center th, .table-text-center td {
            text-align: center;
        }


           .gridpager tr td {
            padding: 5px 13px;
            background: #38b8ba;
            border: 1px solid #fff;
            color: #000;
        }

        .gridpager table {
            float: right;
        }

        .gridpager, .gridpager td {
            text-align: right;
            color: #38b8ba;
            background: #efffff;
            font-weight: bold;
            text-decoration: none;
        }

            .gridpager a {
                color: White;
                font-weight: normal;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%@ Import Namespace="System.Data" %>
    <%@ Import Namespace="System.Data.SqlClient" %>
    <%@ Import Namespace="UMS_BusinessLayer" %>


    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <uc1:SucessAlert runat="server" ID="SucessAlert" />
    <div class=" col-lg-3 padding-none">
        <div class="closebtn">
            <span>&#9776;
            </span>
        </div>
        <div style="display: none;" class="closebtn1">
            <span>&#9776;</span>
        </div>
    </div>
    <asp:HiddenField ID="HFDeptID" runat="server" />
    <div style="margin-right: 0px; margin-top: 3px">
        <asp:UpdatePanel ID="upserver" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <%--<asp:Button ID="btnReview" runat="server" class="float-right btn btn-signup_popup" OnClick="btnSubmit_ServerClick" Text="Create Designation"></asp:Button>--%>
                <asp:Button ID="btnReview" runat="server" class="float-right btn btn-signup_popup" OnClick="btnSubmit_ServerClick" OnClientClick="javascript:return DesignationAdded();" Text="Create Designation"></asp:Button>

                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" Style="display: none" />
                <asp:Button ID="btnCommentsHistoryView" runat="server" Text="Submit" Style="display: none" OnClick="btnCommentsHistoryView_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>


    <div class=" col-lg-12 col-sm-12  col-md-12 col-xs-12 float-right padding-none">
        <div class="col-lg-12 col-sm-12  col-md-12 col-xs-12  grid_area padding-none">
            <div class=" col-lg-12 col-sm-12  col-md-12 col-xs-12 padding-none">

                <div>
                    <div id="header">
                        <div class="col-lg-12 dashboard_panel_header">Designation List</div>

                    </div>
                </div>
                <div id="body" class="col-sm-12" style="overflow: auto; margin-top: 10px;">
                    <div class="col-sm-12" style="padding-right: 0px; margin-top: 10px">
                        <table id="dtDesignationList" class="display datatable_cust" cellspacing="0" style="width: 100%" table-layout="fixed">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Designation ID</th>
                                    <th>Designation name</th>
                                    <th>Edit</th>
                                    <th>History</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <asp:HiddenField ID="hdnDeptCode" runat="server" Value="" />
    <asp:HiddenField ID="hdnRoleID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnShowType" runat="server" Value="0" />
    <!--------Comment History View-------------->
    <div id="myCommentHistory" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel ID="upcomment" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-header">
                            <span id="span4" runat="server"></span>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>

                        </div>
                        <div class="modal-body" style="max-height: 600px; overflow-y: auto">
                            <div class=" history_grid" style="overflow-y: auto;">
                                <asp:GridView ID="gv_CommentHistory" runat="server" CssClass="table table-hover table-responsive" ClientIDMode="Static" 
                                    EmptyDataText="No Records Found" AutoGenerateColumns="False" PagerSettings-Mode="NumericFirstLast" BackColor="White" 
                                    BorderColor="#d2d2d2" BorderStyle="None" BorderWidth="1px" AllowPaging="true" OnPageIndexChanging="gv_CommentHistory_PageIndexChanging" PageSize="25">
                                    <Columns>
                                        <asp:TemplateField HeaderText="User" ItemStyle-Width="140px" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_GivenBy" runat="server" Text='<%# Eval("GivenBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role" ItemStyle-Width="70px" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Role" runat="server" Text='<%# Eval("CommentedRole") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" ItemStyle-Width="150px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Date" runat="server" Text='<%# Eval("CommentedDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Action" runat="server" Text='<%# Eval("Action") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Submitted Data" ItemStyle-Width="200px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_SubmittedData" runat="server" Text='<%# Eval("SubmittedData") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <PagerStyle CssClass="gridpager" HorizontalAlign="Right" />
                                </asp:GridView>
                            </div>
                        </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!--------End Comment History View-------------->
    <asp:HiddenField ID="hdfVID" runat="server" value="0" />
    <asp:HiddenField ID="hdfDesgname" runat="server"  Value=""/>
    <script>
        var tblDesignationList;
        $(function () {

    $('#dtDesignationList thead tr').clone(true).appendTo('#dtDesignationList thead');
    $('#dtDesignationList thead tr:eq(1) th').each(function (i) {
        if (i < 3) {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="Search " />');

            $('input', this).on('keyup change', function () {
                if (tblDesignationList.column(i).search() !== this.value) {
                    tblDesignationList
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
            if (i == 0 || i == 1)
            {
                $(this).html('');
            }
        }
        else {
            $(this).html('');
        }
    });

            tblDesignationList = $('#dtDesignationList').DataTable({
                columns: [
                    { 'data': 'RowNumber' },
                    { 'data': 'DesignationID' },
                    { 'data': 'DesignationName' },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            return '<a class="Edit" title="Edit" data-target="#myModalDesignation" data-toggle="modal"  onclick="EditDesignation(' + o.DesignationID + ',\'' + o.DesignationName + '\')"></a>';
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) { return '<a  class="summary_latest" title="History"  data-target="#myCommentHistory" data-toggle="modal" onclick="ViewDesignationCommentHistory(' + o.DesignationID + ', \'' + o.DesignationName + '\' )">' + '' + '</a>'; }

                    }
                ],
                "columnDefs": [
                    {
                        "targets": [0],
                        "orderable": false,
                    },
                    {
                        "className": "dt-body-left", "targets": [2]
                    },
                    {
                        "bSortable": false, "targets": [3]
                    },
                    {
                        "targets": [1],
                        "visible": false,
                        "searchable": false
                    }
                ],
                "orderCellsTop": true,
                "order": [1, "desc"],
                "paging": true,
                "lengthMenu": [[10,15, 700], [10,15, "All"]],

                sServerMethod: 'post',
                "sAjaxSource": '/UserManagement/WebServicesList.asmx/GetDesignationList',
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "DeptCode", "value": $('#<%=hdnDeptCode.ClientID%>').val() }, { "name": "RoleID", "value": $('#<%=hdnRoleID.ClientID%>').val() }, { "name": "ShowType", "value": $('#<%=hdnShowType.ClientID%>').val() });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#dtDesignationList").show();
                            $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                            //$(".tblDesignationListTrainingSessionsClass").css({ "width": "100%" });
                            UserSessionCheck();
                        },
                        error: function (xhr, textStatus, error) {
                            //if (typeof console == "object") {
                            //    console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            //}

                            custAlertMsg("System Error Occurred, Please contact Admin.", "error");
                        }
                    });
                }
            });

            //$('.dtDesignationList_filterinput[type="search"]').css(
            //    { 'width': '550px' }
            //);
            //$('#dtDesignationList_filter input').addClass('form-control');

        });
        function Reloadtable()
        {
            tblDesignationList.ajax.reload();
            $('#myModalDesignation').modal('hide');
        }
        // function ReloadUpdatetable()
        //{
        //    tblDesignationList.ajax.reload();
        //    $('#myModalDesignation').modal('hide');
        //}
    </script>
    <!-- Modal popup For Designation-->
    <script runat="server">        
        protected void btnSubmit_ServerClick(object sender, EventArgs e)
        {
            try
            {
                btnCreateDesignation.Text = "Create";
                lblDesignationName.Text = "Add Designation";
                txtDesignationName.Value = "";
                UPtxtDesignationName.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", " $('#myModalDesignation').modal({ show: true, backdrop: 'static', keyboard: false });", true);

            }
            catch (Exception ex)
            {
                lblDesignationError.Text = ex.Message;
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                btnCreateDesignation.Text = "Update";
                lblDesignationName.Text = "Edit Designation";
                UMS_BAL objUMS_BAL = new UMS_BAL();
                //HFDesgID.Value = txtDesignationName.Value;
                DataTable dt = objUMS_BAL.GetDesigantionDetails(Convert.ToInt32(HFDesgID.Value));
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    txtDesignationName.Value = dr["DesignationName"].ToString();
                    UPtxtDesignationName.Update();
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", " $('#myModalDesignation').modal({ show: true, backdrop: 'static', keyboard: false });", true);

            }
            catch (Exception ex)
            {
                lblDesignationError.Text = ex.Message;
            }
        }

    </script>
    <script>

        function EditDesignation(E_ID, E_DesigName) {
            document.getElementById("<%=HFDesgID.ClientID%>").value = E_ID;
            document.getElementById("<%=hdfDesgname.ClientID%>").value = E_DesigName;
            var clickButton = document.getElementById("<%=btnSubmit.ClientID%>");
            clickButton.click();
        }
        function ViewDesignationCommentHistory(PK_ID, Desgname) {
            document.getElementById("<%=hdfVID.ClientID%>").value = PK_ID;
            document.getElementById("<%=hdfDesgname.ClientID%>").value = Desgname;
            var btnCV = document.getElementById("<%=btnCommentsHistoryView.ClientID %>");
            btnCV.click();
        }
    </script>
    <%-- designation start--%>
    <div id='OpenDilog'></div>
    <div id="myModalDesignation" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <asp:Label ID="lblDesignationName" runat="server"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" style="padding: 0px;">
                    <div class="form-group col-sm-12" style="float: none !important">
                        <div class="col-sm-6">
                            <asp:HiddenField ID="HFDesgID" runat="server"  Value="0"/>
                        </div>
                        <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">Designation Name<span class="smallred_label">*</span></label>
                        <div class="col-sm-12 padding-none" style="float: none !important">
                            <asp:UpdatePanel ID="UPtxtDesignationName" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <input type="text" class="form-control login_input_sign_up" id="txtDesignationName" runat="server" placeholder="Designation Name" onkeypress="return ValidateAlphaNumericSpace(event)" onpaste="return false" tabindex="13" autofocus="autofocus" maxlength="100" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>

                </div>
                <div class="col-12 float-left text-right modal-footer">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <asp:Button ID="btnCreateDesignation" Class="btn-signup_popup" runat="server" OnClick="btnCreateDesignation_Click" OnClientClick="javascript:return DesignationRequired();" TabIndex="14" Text="Create" />
                                <asp:Button data-dismiss="modal" runat="server" ID="btnCancel" class="btn-cancel_popup" TabIndex="15" Text="Cancel"></asp:Button>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    
                    <%-- <button type="button" class="button button2" data-dismiss="modal" style="width: 120px;background-color:#4949ff;border-color:#4949ff;color:white">Clear</button>  --%>
                </div>
                <div class="Panel_Footer_Message_Label Model_Msg">
                    <asp:UpdatePanel ID="UPdesigMessage" runat="server" UpdateMode="Always" style="padding: 5px">
                        <ContentTemplate>
                            <asp:Label ID="lblDesignationError" runat="server" Text="" ForeColor="Red"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>

            </div>

        </div>
    </div>

    <script>   
        function DesignationRequired() {

            var NewDesig = document.getElementById("<%=txtDesignationName.ClientID%>").value.trim();
            errors = [];
            if (NewDesig == "") {
                errors.push("Enter Designation Name");
                document.getElementById("<%=txtDesignationName.ClientID%>").focus();

            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                         return true;
            }
        }
    </script>
    <script>
        $(document).keydown(function (event) {
            if (event.keyCode == 27) {
                var clickButton = document.getElementById("<%=btnCancel.ClientID %>");
                clickButton.click();
            }
        });
    </script>
    <script>
        $(document).keydown(function (event) {
            if (event.keyCode == 27) {
                var clickButton = document.getElementById("<%= btnCancel.ClientID %>");
                clickButton.click();
            }
        });
    </script>
    <script>
        function ReloadCurrentPage1() {
            window.open("<%=ResolveUrl("~/UserManagement/Designation/DesignationList.aspx")%>", "_self");
        }
    </script>
    <script>
        //DesignationAdded click Before Clear HFDeptID the value  
        function DesignationAdded() {
            document.getElementById("<%=HFDesgID.ClientID%>").value = "";
        }
    </script>
      <script>
    $(function () {
        $('#myModalDesignation').on('shown.bs.modal', function () {
            $('#ContentPlaceHolder1_ContentPlaceHolder1_txtDesignationName').focus();
        });
    });
</script>
</asp:Content>


