﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_BusinessLayer;
using UMS_BO;
using AizantIT_PharmaApp.Common;

namespace AizantIT_PharmaApp.Designation
{
    public partial class Designation : System.Web.UI.Page
    {
        UMS_BAL objUMS_BAL;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblDesignationError.Text = "";
                

                 if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                       
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=1");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            InitializeThePage();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                    else
                    {
                        SessionTimeOut.UpdateSessionTimeOut();
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "Design1:" + strline + " " + strMsg);
            }
        }
        private void InitializeThePage()
        {
            //lblDesignationError.Text = "";
            //txtDesignationName.Focus();
            if (Request.QueryString["DesgID"] != null)
            {
                btnSubmit.Text = "Update";
                btnSubmit.Attributes.Add("title", "Click to Update");
                btnDesignationClear.Visible = false;
                RetriveDesignationDetails();
            }
            else
            {
                btnCancel.Visible = false;
                btnSubmit.Attributes.Add("title", "Click to Submit");
            }
        }
        //private void JsMethod(string msg)
        //{
        //    msg = msg.Replace("'", "");
        //    ScriptManager.RegisterStartupScript(Page, this.GetType(), "alert", "alert('" + msg + "')", true);
        //}
        private void RetriveDesignationDetails()
        {
            objUMS_BAL = new UMS_BAL();
            int DesgID = Convert.ToInt32(Request.QueryString["DesgID"].ToString());
            DataTable dt = objUMS_BAL.GetDesigantionDetails(DesgID);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                HFDesgID.Value = DesgID.ToString();
                txtDesignationName.Value = dr["DesignationName"].ToString();
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                if (txtDesignationName.Value.Trim() == "")
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "All fields represented with * are mandatory", "error");
                    //lblDesignationError.Text = "All fields represented with * are mandatory";
                    //lblDesignationError.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    int EmpID = 0;
                    if (HelpClass.IsUserAuthenticated())
                    {
                        EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    }
                    if (HFDesgID.Value == "")
                    {
                        int Desgid = 0;
                        string Operation = "Insert";
                        string DesignationName = txtDesignationName.Value.Trim();
                        int s = objUMS_BAL.DesignationtBAL(Operation, EmpID, Desgid, DesignationName, out int VerifyDesigName);
                        if (VerifyDesigName == 1)
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Designation Name already Exists", "error");
                            // lblDesignationError.Text = "Designation Name already Exist's";
                        }
                        else
                        {
                            if (s > 0)
                            {
                                HelpClass.custAlertMsg(this, this.GetType(), "Designation Updated sucessfully", "success");
                                //lblDesignationError.Text = "Designation created sucessfully";
                                //lblDesignationError.ForeColor = System.Drawing.Color.Green;
                                //JsMethod("Designation Created sucessfully");
                                //txtDesignationName.Value = string.Empty;
                                //lblDesignationError.Text = "";
                            }
                        }
                    }
                    else
                    {
                        string Operation = "Update";
                        int Desgid = Convert.ToInt32(HFDesgID.Value);
                        string DesignationName = txtDesignationName.Value.Trim();

                        int s = objUMS_BAL.DesignationtBAL(Operation, EmpID, Desgid, DesignationName, out int VerifyDesigName);
                        if (VerifyDesigName == 1)
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Designation Name already Exists", "error");
                           // lblDesignationError.Text = "Designation Name already Exist's";
                        }
                        else
                        {
                            if (s > 0)
                            {
                                HelpClass.custAlertMsg(this, this.GetType(), "Designation Updated sucessfully", "success");
                                //JsMethod("Designation Updated sucessfully");
                                //lblDesignationError.Text = "";
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "Design2:" + strline + " " + strMsg);
            }

        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/UserManagement/Designation/DesignationList.aspx");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.showMsg(this, this.GetType(), "Design3:" + strline + " " + strMsg);
            }

        }
    }
}