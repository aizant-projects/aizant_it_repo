﻿using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.Web.UI;
using UMS_BusinessLayer;

namespace AizantIT_PharmaApp.Designation
{
    public partial class DesignationList : System.Web.UI.Page
    {
        UMS_BAL objUMS_BAL;
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void SucessAlert_buttonClick1(object sender, EventArgs e)
        {
            Response.Redirect("/UserManagement/Designation/DesignationList.aspx");
            // ScriptManager.RegisterStartupScript(Page, this.GetType(), "alert", "Reload();", true);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SucessAlert.buttonClick1 += new EventHandler(SucessAlert_buttonClick1);
                
                 if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                      
                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=1");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            //this.GetDesignationPageWise(1);
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                    
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Designlist_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Designlist_M1:" + strline + "  " + strMsg, "error");
            }           
        }        

        protected void btnAddDesignation_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", " $('#myModalDesignation').modal({ show: true, backdrop: 'static', keyboard: false });", true);
        }
        private void RetriveDesignationDetails()
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                int DesgID = Convert.ToInt32(Request.QueryString["DesgID"].ToString());
                DataTable dt = objUMS_BAL.GetDesigantionDetails(DesgID);
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    HFDesgID.Value = DesgID.ToString();
                    txtDesignationName.Value = dr["DesignationName"].ToString();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Designlist_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Designlist_M2:" + strline + "  " + strMsg, "error");              
            }
           
        }
        protected void btnCreateDesignation_Click(object sender, EventArgs e)
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                if (txtDesignationName.Value.Trim() == "")
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "All fields represented with * are mandatory", "error");
                 }
                else
                {
                    int EmpID = 0;
                    if (HelpClass.IsUserAuthenticated())
                    {
                        EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    }
                    DataTable dtTemp = new DataTable();
                    DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                    DataRow[] drUMS = dtx.Select("ModuleID=1");
                    dtTemp = drUMS.CopyToDataTable();
                    int RoleID = Convert.ToInt32(dtTemp.Rows[0]["RoleID"].ToString());
                    if (HFDesgID.Value == "")
                    {
                        int Desgid = 0;
                        string Operation = "Insert";
                        string DesignationName = txtDesignationName.Value.Trim();
                        int s = objUMS_BAL.DesignationtBAL(Operation, EmpID, Desgid, DesignationName, out int VerifyDesigName, RoleID);
                      
                        if (VerifyDesigName == 1)
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Designation Name already Exists", "error");                 
                        }
                        else
                        {
                            if (s > 0)
                            {
                                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Designation created successfully", "success", "Reloadtable();");
                                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", " $('#myModalDesignation').modal('hide');", true);
                            }
                        }
                    }
                    else
                    {
                        string Operation = "Update";
                        int Desgid = Convert.ToInt32(HFDesgID.Value);
                        string DesignationName = txtDesignationName.Value.Trim();
                        string Designation_Name = hdfDesgname.Value.Trim();
                        if (DesignationName == Designation_Name)
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "There Are No Changes To Update.", "info");
                        }
                        else
                        {
                            int s = objUMS_BAL.DesignationtBAL(Operation, EmpID, Desgid, DesignationName, out int VerifyDesigName, RoleID);
                            if (VerifyDesigName == 1)
                            {
                                HelpClass.custAlertMsg(this, this.GetType(), "Designation Name already Exists", "error");
                             }
                            else
                            {
                               if (s > 0)
                                {
                                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Designation Updated Successfully", "success", "Reloadtable();");
                                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", " $('#myModalDesignation').modal('hide');", true);
                                }
                            }
                        }
                    }
                }
               // ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", "ShowPleaseWaitdisable();", true);

            }
            catch (Exception ex)
            {
               // ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", "ShowPleaseWaitdisable();", true);
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Designlist_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Designlist_M3:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnCommentsHistoryView_Click(object sender, EventArgs e)
        {
            try
            {
                UMS_BAL objUMS_BAL = new UMS_BAL();
                DataTable DMS_DT = objUMS_BAL.GetDesignationHistory(Convert.ToInt32(hdfVID.Value)); 
                gv_CommentHistory.DataSource = DMS_DT;
                gv_CommentHistory.DataBind();
                ViewState["dtCommonHistory"]= DMS_DT;
                if (DMS_DT.Rows.Count > 0)
                {
                    span4.Attributes.Add("title", DMS_DT.Rows[0]["Designation"].ToString());
                    if (DMS_DT.Rows[0]["Designation"].ToString().Length > 60)
                    {
                        span4.InnerHtml = DMS_DT.Rows[0]["Designation"].ToString().Substring(0, 60) + "...";
                    }
                    else
                    {
                        span4.InnerHtml = DMS_DT.Rows[0]["Designation"].ToString();
                    }
                    
                    
                }
                else
                {
                    span4.InnerText = hdfDesgname.Value.ToString();  //"History";
                }
                upcomment.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myCommentHistory').modal({ show: true, backdrop: 'static', keyboard: false });", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MDL_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MDL_M3:" + strline + "  " + "History viewing failed", "error");
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }

        protected void gv_CommentHistory_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            gv_CommentHistory.PageIndex = e.NewPageIndex;
            gv_CommentHistory.DataSource = (DataTable)ViewState["dtCommonHistory"];
            gv_CommentHistory.DataBind();
        }
    }
}
