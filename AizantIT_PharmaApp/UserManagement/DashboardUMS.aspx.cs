﻿using AizantIT_PharmaApp.Common;
using DevExpress.Web.Bootstrap;
using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Web.UI.WebControls;
using UMS_BusinessLayer;

namespace AizantIT_PharmaApp
{
    public partial class DashboardUMS : System.Web.UI.Page
    {
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        ArrayList arrColors = new ArrayList() { "#98AFC7", "#4863A0", "#2B547E", "#0020C2", "#00FFFF", "#307D7E", "#008080", "#728C00", "#254117", "#347235", "#CD7F32", "#317ECC", "#806517",
            "#F75D59", "#FF0000", "#E41B17", "#8C001A", "#810541", "#058246", "#F52887", "#C04000", "#E9AB17", "#3BB9FF", "#7D1B7E", "#7F38EC", "#800517", "#3EA055", "#3090C7", "#98AFC7",
            "#2B65EC", "#7BCCB5", "#54C571", "#B2C248", "#E2A76F", "#7F462C", "#C85A17", "#E77471", "#7D0552", "#E38AAE", "#4E387E", "#C45AEC", "#893BFF", "#B93B8F", "#E67451" };
        ArrayList DBReportcolors = new ArrayList();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HelpClass.IsUserAuthenticated())
            {
                if (!IsPostBack)
                {
                    hdModuleId.Value = "1";//1  UMS Module
                    BindActiveUsers();
                    BindModuels1();
                    ddlRoles.SelectedIndex = 0;
                    ddlRoles.Enabled = false;
                    BindDepartmentChart();
                    UpdatePanel1.Update();
                }
              
            }
            else
            {
                Response.Redirect("~/UserLogin.aspx", false);
            }
        }
        //Total Active Users
        private void BindActiveUsers()
        {
            UMS_BAL ObjUMS_BAL = new UMS_BAL();
            string operation = "";
            //Active Employees
            operation = "TotalUsers";
            lblTotalUSers.Text = ObjUMS_BAL.GetTotalUsers(operation).ToString();
            //locked employees
            operation = "LockedUsers";
            lblLockedUsers.Text = ObjUMS_BAL.GetTotalUsers(operation).ToString();
            //total departments
            operation = "TotalDepartment";
            lblTotalDepartment.Text = ObjUMS_BAL.GetTotalUsers(operation).ToString();
            //UnAssign employees
            operation = "UnAssignRoles";
            lblUnAssignRoles.Text = ObjUMS_BAL.GetTotalUsers(operation).ToString();


        }

        //bind Moduels Dropdown
        public void BindModuels1()
        {
            try
            {
                UMS_BAL ObjUMS_BAL = new UMS_BAL();
                DataTable dt = ObjUMS_BAL.GetModules();
                Moduelddl.DataSource = dt;
                Moduelddl.DataTextField = "ModuleName";
                Moduelddl.DataValueField = "ModuleID";
                Moduelddl.DataBind();
                Moduelddl.Items.Insert(0, new ListItem("-- Select Module --", "0"));
                Moduelddl.SelectedIndex = 0;
                
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Dbd_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Dbd_M1:" + strline + "  " + strMsg, "error");
            }

        }
        //bind roles
        public void BinddropdownRoles()
        {
            try
            {
                int ModuleID = Convert.ToInt32(Moduelddl.SelectedValue);
                if(ModuleID>0)
                {
                    ddlRoles.Items.Clear();
                    UMS_BAL ObjUMS_BAL = new UMS_BAL();
                    DataTable dt = ObjUMS_BAL.BindDropDownRoles(ModuleID);
                    ddlRoles.DataSource = dt;
                    ddlRoles.DataTextField = "RoleName";
                    ddlRoles.DataValueField = "RoleID";
                    ddlRoles.DataBind();
                    ddlRoles.Items.Insert(0, new ListItem("-- select Roles --", "0"));
                    ddlRoles.SelectedIndex = 0;
                    ddlRoles.Enabled = true;
                    upSelectModule.Update();
                    upselectRoles.Update();
                    upModuleChart.Update();


                }
                else
                {
                    upSelectModule.Update();
                    upselectRoles.Update();
                    ddlRoles.SelectedIndex = 0;
                    ddlRoles.Enabled = false;
                    upModuleChart.Update();
                }
               
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Dbd_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Dbd_M2:" + strline + "  " + strMsg, "error");
            }
        }
        public void BindDepartmentChart()
        {
            try
            {
                UMS_BAL ObjUMS_BAL = new UMS_BAL();
                DataTable dt = ObjUMS_BAL.BindDepartmetChart();
                if (dt.Rows.Count > 0)
                {
                    lblMsg.Text = "";                    
                    DeptChart.SeriesCollection.Clear();
                    DeptChart.DataSource = dt;
                    DeptChart.SettingsCommonSeries.ArgumentField = "DepartmentName";
                    DeptChart.SeriesCollection.Add(new BootstrapChartBarSeries() { ValueField = "Total", Name = "Total" });
                    DeptChart.SettingsLegend.Visible = false;
                    DeptChart.SettingsCommonSeries.Label.Visible = true;
                    DeptChart.SettingsToolTip.Enabled = true;
                    DeptChart.SettingsToolTip.Format.Type = FormatType.None;
                    DeptChart.DataBind();                    
                    DeptChart.Visible = true;
                }
                else
                {
                    lblMsg.Text = "No Departments in this roles";
                }
            }

            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Dbd_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Dbd_M3:" + strline + "  " + strMsg, "error");
            }
        }
        private Color[] chartColorCodes()
        {
            Color[] customPalette = new Color[44];
            customPalette[0] = ColorTranslator.FromHtml("#CCCC66");
            customPalette[1] = ColorTranslator.FromHtml("#669933");
            customPalette[2] = ColorTranslator.FromHtml("#0099CC");
            customPalette[3] = ColorTranslator.FromHtml("#3366FF");
            customPalette[4] = ColorTranslator.FromHtml("#3333CC");
            customPalette[5] = ColorTranslator.FromHtml("#FFCC00");
            customPalette[6] = ColorTranslator.FromHtml("#66CC66");
            customPalette[7] = ColorTranslator.FromHtml("#0099FF");
            customPalette[8] = ColorTranslator.FromHtml("#3366CC");
            customPalette[9] = ColorTranslator.FromHtml("#333399");
            customPalette[10] = ColorTranslator.FromHtml("#c1cdc1");
            customPalette[11] = ColorTranslator.FromHtml("#cdb79e");
            customPalette[12] = ColorTranslator.FromHtml("#8fbc8f");
            customPalette[13] = ColorTranslator.FromHtml("#87cefa");
            customPalette[14] = ColorTranslator.FromHtml("#d8bfd8");
            customPalette[15] = ColorTranslator.FromHtml("#0099CC");
            customPalette[16] = ColorTranslator.FromHtml("#339999");
            customPalette[17] = ColorTranslator.FromHtml("#336699");
            customPalette[18] = ColorTranslator.FromHtml("#FFCC99");
            customPalette[19] = ColorTranslator.FromHtml("#FFFFCC");
            customPalette[20] = ColorTranslator.FromHtml("#999999");
            customPalette[21] = ColorTranslator.FromHtml("#333333");
            customPalette[22] = ColorTranslator.FromHtml("#666633");
            customPalette[23] = ColorTranslator.FromHtml("#9966FF");
            customPalette[24] = ColorTranslator.FromHtml("#FF6633");
            customPalette[25] = ColorTranslator.FromHtml("#800517");
            customPalette[26] = ColorTranslator.FromHtml("#3EA055");
            customPalette[27] = ColorTranslator.FromHtml("#3090C7");
            customPalette[28] = ColorTranslator.FromHtml("#98BFC1");
            customPalette[29] = ColorTranslator.FromHtml("#2B65EC");
            customPalette[30] = ColorTranslator.FromHtml("#7BCCB5");
            customPalette[31] = ColorTranslator.FromHtml("#54C571");
            customPalette[32] = ColorTranslator.FromHtml("#B2C248");
            customPalette[33] = ColorTranslator.FromHtml("#E2A76F");
            customPalette[34] = ColorTranslator.FromHtml("#7F462C");
            customPalette[35] = ColorTranslator.FromHtml("#C85A17");
            customPalette[36] = ColorTranslator.FromHtml("#E77471");
            customPalette[37] = ColorTranslator.FromHtml("#7D0552");
            customPalette[38] = ColorTranslator.FromHtml("#E38AAE");
            customPalette[39] = ColorTranslator.FromHtml("#4E387E");
            customPalette[40] = ColorTranslator.FromHtml("#C45AEC");
            customPalette[41] = ColorTranslator.FromHtml("#893BFF");
            customPalette[42] = ColorTranslator.FromHtml("#B93B8F");
            customPalette[43] = ColorTranslator.FromHtml("#E67451");
            return customPalette;
        }
        
        public void ModuelsWithRolesChart()
        {
            try
            {
                UMS_BAL ObjUMS_BAL = new UMS_BAL();
                int ModuelID = Convert.ToInt32(Moduelddl.SelectedValue);
                int RoleID = Convert.ToInt32(ddlRoles.SelectedValue);
                DataTable dt = ObjUMS_BAL.ModulesWithRolesChart(ModuelID, RoleID);
                if (dt.Rows.Count > 0)
                {
                    lblMsg.Text = "";                   
                    ModuleChart.SeriesCollection.Clear();
                    ModuleChart.DataSource = dt;
                    ModuleChart.SettingsCommonSeries.ArgumentField = "DepartmentName";
                    ModuleChart.SeriesCollection.Add(new BootstrapChartBarSeries() { ValueField = "Total", Name = "Total" });
                    ModuleChart.SettingsLegend.Visible = false;
                    ModuleChart.SettingsCommonSeries.Label.Visible = true;
                    ModuleChart.SettingsToolTip.Enabled = true;
                    ModuleChart.SettingsToolTip.Format.Type = FormatType.None;
                    ModuleChart.DataBind();
                    //Updating Update panel for getting charts without Refreshing 
                    upSelectModule.Update();
                    upselectRoles.Update();
                    upModuleChart.Update();
                }
                else
                {
                    lblMsg.Text = "No Departments in this roles";

                }
               
            }

            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("Dbd_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "Dbd_M4:" + strline + "  " + strMsg, "error");
            }
        }
        

        protected void Moduleddl_SelectedIndexChanged(object sender, EventArgs e)
        {
            BinddropdownRoles();
        }

        protected void ddlRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            ModuelsWithRolesChart();
            upModuleChart.Update();

        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }

       
    }
}