﻿using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_BusinessLayer;

namespace AizantIT_PharmaApp.UserManagement
{
    public partial class SecurityProfile : System.Web.UI.Page
    {
        UMS_BAL objUMS_BAL;
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblDepartmentError.Text = "";

                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {

                        DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dt.Select("ModuleID=1");
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            Initilizae();
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }

                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "SecurityPro_M1:" + strline + "  " + strMsg, "error");
            }
        }

        public void Initilizae()
        {

            try
            {

                UMS_BAL uMS_BAL = new UMS_BAL();
                DataTable dt = uMS_BAL.SecurityProfileBal1(0, 0, 0, 0, "Select");
                if (dt.Rows.Count > 0)
                {
                    txtTenureofPassword.Text = dt.Rows[0]["PasswordTenure"].ToString();
                    txtPasswordExpiryAlertBefore.Text = dt.Rows[0]["ExpiryAlertBefore"].ToString();
                    txtAccountLockOut.Text = dt.Rows[0]["LockOutAttempts"].ToString();
                    txtSessionTimeOut.Text = dt.Rows[0]["SessionTime"].ToString();
                }
                txtComments.Text = "";
                txtTenureofPassword.Enabled = false;
                
                txtPasswordExpiryAlertBefore.Enabled = false;
                txtAccountLockOut.Enabled = false;
                txtSessionTimeOut.Enabled = false;
                dvComments.Visible = false;
                btnCancel.Visible = false;
                btnSubmit.Visible = false;
                btnHistory.Visible = true;

            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("SecurityPro_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "SecurityPro_M2:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                int EmpID = 0;
                if (HelpClass.IsUserAuthenticated())
                {
                    EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                }
                DataTable dtTemp = new DataTable();
                DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                DataRow[] drUMS = dtx.Select("ModuleID=1");
                dtTemp = drUMS.CopyToDataTable();
                int RoleID = Convert.ToInt32(dtTemp.Rows[0]["RoleID"].ToString());
                UMS_BAL uMS_BAL = new UMS_BAL();
                DataTable count = uMS_BAL.SecurityProfileBal(Convert.ToInt32(txtTenureofPassword.Text.Trim()), Convert.ToInt32(txtAccountLockOut.Text.Trim()), Convert.ToInt32(txtPasswordExpiryAlertBefore.Text.Trim()), Convert.ToInt32(txtSessionTimeOut.Text.Trim()), "Add", EmpID, txtComments.Text.Trim(), RoleID);
                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Updated Successfully", "success","ReloadSelf");
                Initilizae();
                btnEdit.Visible = true;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("SecurityPro_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "SecurityPro_M3:" + strline + "  " + strMsg, "error");
            }
           ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", "ShowPleaseWaitGifDisable();", true);
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {

            txtTenureofPassword.Enabled = true;
            txtPasswordExpiryAlertBefore.Enabled = true;
            txtAccountLockOut.Enabled = true;
            txtSessionTimeOut.Enabled = true;
            dvComments.Visible = true;
            txtComments.Text = "";
            btnSubmit.Visible = true;
            btnCancel.Visible = true;
            btnEdit.Visible = false;
            btnHistory.Visible = false;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UserManagement/SecurityProfile.aspx");
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }

        protected void btnHistory_Click(object sender, EventArgs e)
        {
            try
            {
                UMS_BAL uMS_BAL = new UMS_BAL();
                DataTable DMS_DT = uMS_BAL.GetSecurityProfileHistory();
                gv_CommentHistory.DataSource = DMS_DT;
                gv_CommentHistory.DataBind();

               
                    span4.InnerText = "Security Profile History";
               
                upcomment.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myCommentHistory').modal({ show: true, backdrop: 'static', keyboard: false });", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("SecurityPro_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "SecurityPro_M3:" + strline + "  " + "History viewing failed", "error");
            }
        }
        protected void gv_CommentHistory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string _fullcomments = (e.Row.Cells[5].FindControl("txt_Comments") as TextBox).Text;
                    LinkButton _lnkmorecomments = (e.Row.Cells[5].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (e.Row.Cells[5].FindControl("lbcommentsless") as LinkButton);
                    if (_fullcomments.Length > 70)
                    {
                        (e.Row.Cells[5].FindControl("lbl_shortComments") as Label).Visible = true;
                        TextBox FullTbx = (e.Row.Cells[5].FindControl("txt_Comments") as TextBox);
                        (e.Row.Cells[5].FindControl("lbl_shortComments") as Label).Text = _fullcomments.Substring(0, 70) + "..";
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = true;

                        if (_fullcomments.Length > 250 && _fullcomments.Length <= 500)
                        {
                            FullTbx.Rows = 6;
                        }
                        else if (_fullcomments.Length > 500 && _fullcomments.Length <= 1000)
                        {
                            FullTbx.Rows = 9;
                        }
                        else if (_fullcomments.Length > 1000 && _fullcomments.Length <= 1500)
                        {
                            FullTbx.Rows = 12;
                        }
                        else if (_fullcomments.Length > 1500 && _fullcomments.Length <= 2000)
                        {
                            FullTbx.Rows = 15;
                        }
                        else if (_fullcomments.Length > 2000)
                        {
                            FullTbx.Rows = 20;
                        }
                        FullTbx.Visible = false;
                    }
                    else
                    {
                        (e.Row.Cells[5].FindControl("lbl_shortComments") as Label).Visible = true;
                        (e.Row.Cells[5].FindControl("txt_Comments") as TextBox).Visible = false;
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("SecurityPro_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "SecurityPro_M4:" + strline + "  " + "Comment History row databound failed", "error");
            }
        }
        //protected void gv_CommentHistory_RowCommand(object sender, GridViewCommandEventArgs e)// OnRowCommand="gv_CommentHistory_RowCommand"
        //{
        //    try
        //    {
        //        GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
        //        LinkButton _lnkmorecomments = (row.Cells[5].FindControl("lbcommentsmore") as LinkButton);
        //        LinkButton _lnklesscomments = (row.Cells[5].FindControl("lbcommentsless") as LinkButton);
        //        if (e.CommandName == "SM")
        //        {
        //            (row.Cells[5].FindControl("lbl_shortComments") as Label).Visible = false;
        //            (row.Cells[5].FindControl("txt_Comments") as TextBox).Visible = true;
        //            _lnklesscomments.Visible = true;
        //            _lnkmorecomments.Visible = false;
        //        }
        //        else if (e.CommandName == "SL")
        //        {
        //            (row.Cells[5].FindControl("lbl_shortComments") as Label).Visible = true;
        //            (row.Cells[5].FindControl("txt_Comments") as TextBox).Visible = false;
        //            _lnklesscomments.Visible = false;
        //            _lnkmorecomments.Visible = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string strline = HelpClass.LineNo(ex);
        //        string strMsg = HelpClass.SQLEscapeString(ex.Message);
        //        Aizant_log.Error("SecurityPro_M5:" + strline + "  " + strMsg);
        //        HelpClass.custAlertMsg(this, this.GetType(), "SecurityPro_M5:" + strline + "  " + "Comment History row command failed", "error");
        //    }
        //}

        protected void gv_CommentHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            UMS_BAL uMS_BAL = new UMS_BAL();
            DataTable DMS_DT = uMS_BAL.GetSecurityProfileHistory();

            gv_CommentHistory.PageIndex = e.NewPageIndex;
            gv_CommentHistory.DataSource = DMS_DT;
            gv_CommentHistory.DataBind();
        }
    }
}