﻿using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using UMS_BO;
using UMS_BusinessLayer;

namespace AizantIT_PharmaApp.UserManagement
{
    public partial class EmployeeCode : System.Web.UI.Page
    {
        UserObjects UOB;
        UMS_BAL UMBAL;
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HelpClass.IsUserAuthenticated())
            {
                if (!IsPostBack)
                {
                    GetEmpCode();
                }
            }
            else
            {
                Response.Redirect("~/UserLogin.aspx", false);
            }

        }
        public void GetEmpCode()
        {
            try
            {
                UMBAL = new UMS_BAL();
                DataSet dt = UMBAL.GetEmpCode(out int Status);
                if (dt.Tables[0].Rows.Count > 0)
                {
                    txtEmployeeCodeName.Value = dt.Tables[0].Rows[0]["EmpCode"].ToString();
                    txtEmployeeCharacter.Value = dt.Tables[0].Rows[0]["EmpCharacter"].ToString();
                    if (Status != 0)
                    {
                        btnSubmit.Visible = false;
                        btnEmployeeClear.Visible = false;
                        txtEmployeeCharacter.Disabled = true;
                        txtEmployeeCodeName.Disabled = true;
                        divFooter.Visible = false;
                    }

                }
                else
                {
                    txtEmployeeCodeName.Disabled = false;
                    txtEmployeeCharacter.Disabled = false;
                    btnSubmit.Enabled = true;
                    btnEmployeeClear.Disabled = false;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EmpCode_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EmpCode_M1:" + strline + "  " + strMsg, "error");
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                UMBAL = new UMS_BAL();
                UOB = new UserObjects();
                UOB.EmpCharacter = txtEmployeeCharacter.Value.Trim();
                UOB.EmpCode =txtEmployeeCodeName.Value.Trim();
                int i = UMBAL.AddEmpCode(UOB);
                if (i == 1)
                {
                   
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Created Successfully", "success", "Reload();");
                 
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EmpCode_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EmpCode_M2:" + strline + "  " + strMsg, "error");
            }
        }

        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}