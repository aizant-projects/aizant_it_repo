﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using AizantIT_PharmaApp.UserControls;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.DMS.DMSAdmin
{
    public partial class Form_Creation : System.Web.UI.Page
    {
        private Word2PDFConverter.RemoteConverter converter;
        private string storefolder = HttpContext.Current.Server.MapPath(@"~/Scripts/pdf.js/web/");
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        UMS_BAL objUMS_BAL;
        DocObjects docObjects = new DocObjects();
        DocObjects docObjects1 = new DocObjects();
        DocObjects docObjects2 = new DocObjects();
        ReviewObjects reviewObjects = new ReviewObjects();
        DocUploadBO objectDocUploadBO;
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    converter = (Word2PDFConverter.RemoteConverter)Activator.GetObject(typeof(Word2PDFConverter.RemoteConverter), "http://localhost:" + System.Configuration.ConfigurationManager.AppSettings["Word2PdfPort"].ToString() + "/RemoteConverter");
                    if (!IsPostBack)
                    {
                        Session["vwdocObjects1F"] = null;
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=2").Length > 0 || dtTemp.Select("RoleID=30").Length > 0)//2-admin,30-controller
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop3", "showFormDetails();", true);
                        ElectronicSign.buttonClick += new EventHandler(ElectronicSign_buttonClick);
                        ConfirmCustomAlert.buttonClick += new EventHandler(ConfirmCustomAlert_buttonClick);
                        if (Request.QueryString.Count > 0)
                        {
                            if (Request.QueryString["FormVersionID"] != null)
                            {
                                if (Request.QueryString["from"] == "c")
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "show7", "showUpload();", true);
                                }
                                if (Request.QueryString["from"] == "r")
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "show9", "showUpload();", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "show7", "showUpload();", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "show7", "showUpload();", true);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M1:" + strline + "  " + "Document Load failed.", "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        private void InitializeThePage()
        {
            try
            {
                DMS_GetDocumentTypeList();
                DMS_GetDepartmentList();
                DMS_GetCreatorList();
                ddl_Creator.Enabled = false;
                div2.InnerHtml = "Create Form";
                ViewState["PKID"] = 0;
                ViewState["WordPath"] = 0;
                ViewState["PDFPath"] = 0;
                ViewState["DyWordName"] = 0;
                ViewState["DyPDFName"] = 0;
                ViewState["FormContent"] = 0;
                ViewState["FormExt"] = 0;
                ViewState["WordName"] = 0;
                ViewState["Action"] = "";
                ViewState["CreationStatusID"] = 0;
                if (Request.QueryString.Count > 0)
                {
                    if (Request.QueryString["FormVersionID"] != null)
                    {
                        StringBuilder sbWarningMsg = new StringBuilder();
                        ViewState["PKID"] = Request.QueryString["FormVersionID"];
                        RetriveFormDetails();
                        comments.Visible = true;
                        DocObjects docObjects = new DocObjects();
                        docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
                        DataTable dt = DMS_Bal.DMS_GetFormDetails(docObjects);

                        foreach (ListItem _LiAuth in ddl_Creator.Items)
                        {
                            if (_LiAuth.Value == dt.Rows[0]["CreatorID"].ToString())
                            {
                                ddl_Creator.SelectedValue = dt.Rows[0]["CreatorID"].ToString();
                                break;
                            }
                        }

                        if (ddl_Creator.SelectedValue.ToString().Trim() == "0" && dt.Rows[0]["CreatorID"].ToString() != "")
                        {
                            sbWarningMsg.Append("Assigned Form Creator - <b>" + dt.Rows[0]["CreatedBy"].ToString() + "</b> is InActive." + "<br/>");
                        }
                        
                        //ddl_Creator.SelectedValue = dt.Rows[0]["CreatorID"].ToString();
                        btnFormNumcheck.Visible = false;
                        btnFormNamecheck.Visible = false;
                        docObjects.Version = Convert.ToInt32(ViewState["PKID"]);
                        docObjects.RoleID = 0;
                        docObjects.EmpID = 0;
                        docObjects.FileType1 = dt.Rows[0]["FormExtension"].ToString();
                        docObjects.Mode = 0;
                        System.Data.DataTable table = DMS_Bal.DMS_GetFormLocationList(docObjects);
                        if (table.Rows[0]["FormFileName"].ToString().Length > 15)
                        {
                            lb_Upload.Attributes.Add("title", table.Rows[0]["FormFileName"].ToString());
                            lb_Upload.Text = table.Rows[0]["FormFileName"].ToString().Substring(0, 15) + "...";
                        }
                        else
                        {
                            lb_Upload.Text = table.Rows[0]["FormFileName"].ToString();
                        }
                        //lb_Upload.Text = table.Rows[0]["FormFileName"].ToString();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "show3", "showViewForm();", true);
                        if (Request.QueryString["from"] == "c")
                        {
                            DMS_GetControllerList();
                            btn_Submit.Value = "Update";
                            docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
                            if (dt.Rows[0]["FormRequestTypeID"].ToString() == "1")
                            {
                                div2.InnerHtml = "Edit New Form";
                                btnReset.Visible = false;
                            }
                            else if (dt.Rows[0]["FormRequestTypeID"].ToString() == "2")
                            {
                                div2.InnerHtml = "Edit Revision Form";
                                DisableFormDetails();
                                ddl_Approver.Enabled = true;
                                btnReset.Visible = false;
                            }
                            else if (dt.Rows[0]["FormRequestTypeID"].ToString() == "3")
                            {

                                div2.InnerHtml = "Edit Withdraw Form";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop5", "showStatusChangeDiv();", true);
                                DisableFormDetails();
                                ddl_Approver.Enabled = true;
                                btnReset.Visible = false;
                            }
                            btn_Download.Visible = true;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "show6", "showUpload();", true);
                        }
                        else if (Request.QueryString["from"] == "a")
                        {
                            DisableFormDetails();
                            DMS_GetApproverList();
                            btn_Submit.Visible = false;
                            btn_Reject.Visible = true;
                            btn_Revert.Visible = true;
                            btn_approve.Visible = true;
                            btnReset.Visible = false;
                            if (dt.Rows[0]["FormRequestTypeID"].ToString() == "1")
                            {
                                div2.InnerHtml = "Approve New Form";
                            }
                            else if (dt.Rows[0]["FormRequestTypeID"].ToString() == "2")
                            {
                                div2.InnerHtml = "Approve Form Revision";
                            }
                            else if (dt.Rows[0]["FormRequestTypeID"].ToString() == "3")
                            {
                                div2.InnerHtml = "Approve Form Withdrawal";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop4", "showStatusChangeDiv();", true);
                                divOpinion.Style.Add("pointer-events", "none");
                                rbtnActive.Checked = false;
                                rbtnInActive.Checked = true;
                                btn_Revert.Visible = false;
                            }
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "hide1", "hideUpload();", true);

                            btn_Download.Visible = false;
                            if (Request.QueryString["Pending"] == "1")
                            {
                                if (dt.Rows[0]["FormRequestTypeID"].ToString() == "2")
                                {
                                    btn_approve.Visible = false;
                                }
                                else if (dt.Rows[0]["FormRequestTypeID"].ToString() == "3")
                                {
                                    btn_approve.Visible = false;
                                }
                            }
                        }
                        else if (Request.QueryString["from"] == "s")
                        {
                            btn_Submit.Value = "Withdraw";
                            div2.InnerHtml = "Form Withdraw";
                            DMS_GetControllerList();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop5", "showStatusChangeDiv();", true);
                            divOpinion.Style.Add("pointer-events", "none");
                            rbtnActive.Checked = true;
                            rbtnInActive.Checked = false;
                            btnReset.Visible = false;
                            DisableFormDetails();
                            ddl_Approver.Enabled = true;
                            comments.Visible = true;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "hide2", "hideUpload();", true);
                            btn_Download.Visible = false;
                            ddl_Approver.SelectedValue = "0";
                        }
                        else if (Request.QueryString["from"] == "r")
                        {
                            btn_Submit.Value = "Revise";
                            div2.InnerHtml = "Form Revision";
                            DMS_GetControllerList();
                            DisableFormDetails();
                            ddl_Approver.Enabled = true;
                            txt_Description.Disabled = false;
                            btnReset.Visible = false;
                            btn_Submit.Visible = false;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "show8", "showUpload();", true);
                            ddl_Approver.SelectedValue = "0";
                        }
                        else if (Request.QueryString["from"] == "m")
                        {
                            btn_Submit.Visible = false;
                            btn_Update.Visible = true;
                            if (dt.Rows[0]["FormRequestTypeID"].ToString() == "1")
                            {
                                div2.InnerHtml = "Manage New Form Reviewers";
                            }
                            else if (dt.Rows[0]["FormRequestTypeID"].ToString() == "2")
                            {
                                div2.InnerHtml = "Manage Revision Form Reviewers";
                            }
                            else if (dt.Rows[0]["FormRequestTypeID"].ToString() == "3")
                            {
                                div2.InnerHtml = "Manage Withdraw Form Reviewers";
                            }
                            DisableFormDetails();
                            DMS_GetApproverList();
                            ddl_Approver.Enabled = true;
                            btn_Download.Visible = false;
                            btnReset.Visible = false;
                            btn_Reject.Visible = true;
                            if(dt.Rows[0]["CreationStatusID"].ToString()== "5")
                            {
                                ddl_Creator.Enabled = true;
                                ViewState["CreationStatusID"] = 1;
                            }
                        }
                        if (Request.QueryString["from"] == "r" || Request.QueryString["from"] == "s")
                        {
                        }
                        else
                        {
                            foreach (ListItem _LiAuth in ddl_Approver.Items)
                            {
                                if (_LiAuth.Value == dt.Rows[0]["ApproverID"].ToString())
                                {
                                    ddl_Approver.SelectedValue = dt.Rows[0]["ApproverID"].ToString();
                                    break;
                                }
                            }

                            if (ddl_Approver.SelectedValue.ToString().Trim() == "0" && dt.Rows[0]["ApproverID"].ToString() != "")
                            {
                                sbWarningMsg.Append("Assigned Form Approver - <b>" + dt.Rows[0]["ApprovedBy"].ToString() + "</b> is InActive." + "<br/>");
                            }
                            //ddl_Approver.SelectedValue = dt.Rows[0]["ApproverID"].ToString();
                        }
                        if (sbWarningMsg.Length > 8)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbWarningMsg.ToString() + "','warning');", true);
                        }
                    }
                }
                else
                {
                    btn_Download.Visible = false;
                    DMS_GetControllerList();
                    btn_Submit.Visible = false;
                    ddl_Creator.SelectedValue = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "show5", "showUpload();", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "hide3", "hideViewForm();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M2:" + strline + "  " + "Page Initialization failed.", "error");
            }
        }
        public void DMS_GetDocumentTypeList()
        {
            try
            {
                JQDataTableBO objJQDataTableBO = new JQDataTableBO();
                objJQDataTableBO.iMode = 0;
                objJQDataTableBO.pkid = 0;
                objJQDataTableBO.iDisplayLength = 0;
                objJQDataTableBO.iDisplayStart = 0;
                objJQDataTableBO.iSortCol = 0;
                objJQDataTableBO.sSortDir = "";
                objJQDataTableBO.sSearch = "";
                DataTable DMS_Dt = DMS_Bal.DMS_GetDocumentTypeList(objJQDataTableBO);
                ddl_DocumentType.DataValueField = DMS_Dt.Columns["DocumentTypeID"].ToString();
                ddl_DocumentType.DataTextField = DMS_Dt.Columns["DocumentType"].ToString();
                ddl_DocumentType.DataSource = DMS_Dt;
                ddl_DocumentType.DataBind();
                ddl_DocumentType.Items.Insert(0, new ListItem("--Select Document Type--", "0"));
                ddl_DocumentType.Items.Insert(DMS_Dt.Rows.Count + 1, new ListItem("Others", "-1"));
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop2", "showRefDocNumDropdown();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M3:" + strline + "  " + "Document Type load failed.", "error");
            }
        }
        public void DMS_GetDepartmentList()
        {
            try
            {
                DataTable DMS_Dt = DMS_BalDM.DMS_GetDepartmentList();
                ddl_DeptName.DataValueField = DMS_Dt.Columns["DeptID"].ToString();
                ddl_DeptName.DataTextField = DMS_Dt.Columns["DepartmentName"].ToString();
                ddl_DeptName.DataSource = DMS_Dt;
                ddl_DeptName.DataBind();
                ddl_DeptName.Items.Insert(0, new ListItem("--Select Department--", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M4:" + strline + "  " + "Department load failed.", "error");
            }
        }
        public void getdocumentlist(string dept, string doctype)
        {
            try
            {
                DocumentManagmentBAL DMS_Bal = new DocumentManagmentBAL();
                docObjects.dept = dept;
                docObjects.DocumentType = doctype;
                docObjects.RType = "N/A";
                DataTable DMS_dt = DMS_Bal.DMS_GetDocumentNameList(docObjects);
                ddl_RefDocName.DataSource = DMS_dt;
                ddl_RefDocName.DataValueField = DMS_dt.Columns["DocumentID"].ToString();
                ddl_RefDocName.DataTextField = DMS_dt.Columns["DocumentName"].ToString();
                ddl_RefDocName.DataBind();
                ddl_RefDocName.Items.Insert(0, new ListItem("--Select Document--", "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void ddl_DeptName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl_DeptName.SelectedValue != "0" || ddl_DocumentType.SelectedValue != "-1")
            {
                getdocumentlist(ddl_DeptName.SelectedValue, ddl_DocumentType.SelectedValue);
            }
            else
            {
                ddl_RefDocName.Items.Clear();
                ddl_RefDocName.Items.Insert(0, new ListItem("--Select Document--", "0"));
            }
            if (ddl_DocumentType.SelectedValue == "-1")
            {
                ddl_RefDocName.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop2", "showRefDocNumTextBox1();", true);
            }
            else
            {
                ddl_RefDocName.Visible = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop2", "showRefDocNumDropdown();", true);
            }
            upRefDocNum.Update();
        }
        protected void ddl_DocumentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl_DeptName.SelectedValue != "0" && ddl_DocumentType.SelectedValue != "-1")
            {
                getdocumentlist(ddl_DeptName.SelectedValue, ddl_DocumentType.SelectedValue);
            }
            else
            {
                ddl_RefDocName.Items.Clear();
                ddl_RefDocName.Items.Insert(0, new ListItem("--Select Document--", "0"));
            }
            if (ddl_DocumentType.SelectedValue == "-1")
            {
                ddl_RefDocName.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop2", "showRefDocNumTextBox();", true);
            }
            else
            {
                ddl_RefDocName.Visible = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop2", "showRefDocNumDropdown();", true);
            }
            upRefDocNum.Update();
        }
        public void DMS_GetControllerList()
        {
            try
            {
                DataTable DMS_Dt = DMS_Bal.DMS_GetControllersList();
                if (DMS_Dt.Rows.Count > 0)
                {
                    DataRow[] filtered = DMS_Dt.Select("EmpID <> ('" + (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString() + "')");
                    DMS_Dt = filtered.CopyToDataTable();
                    ddl_Approver.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                    ddl_Approver.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                    ddl_Approver.DataSource = DMS_Dt;
                    ddl_Approver.DataBind();
                    ddl_Approver.Items.Insert(0, new ListItem("-- Select Form Approver --", "0"));
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop6", "custAlertMsg('There are no employees with Controller role.','error');", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M6:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M6:" + strline + "  " + "Controller Name drop down Loading failed.", "error");
            }
        }
        public void DMS_GetApproverList()
        {
            try
            {
                DataTable DMS_Dt = DMS_Bal.DMS_GetControllersList();
                if (DMS_Dt.Rows.Count > 0)
                {
                    ddl_Approver.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                    ddl_Approver.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                    ddl_Approver.DataSource = DMS_Dt;
                    ddl_Approver.DataBind();
                    ddl_Approver.Items.Insert(0, new ListItem("-- Select Form Approver --", "0"));
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop6", "custAlertMsg('There are no employees with controller role.','error');", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M7:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M7:" + strline + "  " + "Form Approver drop down Loading failed.", "error");
            }
        }
        public void DMS_GetCreatorList()
        {
            try
            {
                docObjects.UserRole = "2";
                DataTable DMS_Dt = DMS_Bal.DMS_GetApproverList(docObjects);
                if (DMS_Dt.Rows.Count > 0)
                {
                    //DataRow[] filtered = DMS_Dt.Select("EmpID <> ('" + (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString() + "')");
                    //DMS_Dt = filtered.CopyToDataTable();
                    ddl_Creator.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                    ddl_Creator.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                    ddl_Creator.DataSource = DMS_Dt;
                    ddl_Creator.DataBind();
                    ddl_Creator.Items.Insert(0, new ListItem("-- Select Form Creator --", "0"));
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop6", "custAlertMsg('There are no employees with Admin role.','error');", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M6:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M6:" + strline + "  " + "Controller Name drop down Loading failed.", "error");
            }
        }
        protected void btn_Reset_Click(object sender, EventArgs e)
        {
            btnReset.Enabled = false;
            try
            {

                DMS_Reset();
                EnableFormDetails();
                btnReset.Visible = true;
                ddl_RefDocName.Visible = true;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M10:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M10:" + strline + "  " + "Form Details Reset failed.", "error");
            }
            finally
            {
                btnReset.Enabled = true;
            }
        }
        public void DMS_Reset()
        {
            try
            {
                txt_FormName.Value = "";
                txt_FormNumber.Text = "";
                txt_Version.Text = "";
                txt_Description.Value = "";
                txt_RefDocNum.Text = "";
                ddl_DeptName.SelectedIndex = -1;
                ddl_DocumentType.SelectedIndex = -1;
                ddl_RefDocName.SelectedIndex = -1;
                ddl_Approver.SelectedIndex = -1;
                btnFormNumcheck.Visible = true;
                btnFormNamecheck.Visible = true;
                lbl_number.Visible = false;
                lblStatus.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hide4", "hideViewForm();", true);
                btn_Submit.Visible = false;
                txt_Comments.Value = "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void RetriveFormDetails()
        {
            try
            {
                DocObjects docObjects = new DocObjects();
                docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
                DataTable dt = DMS_Bal.DMS_GetFormDetails(docObjects);
                ddl_DeptName.SelectedValue = dt.Rows[0]["DeptID"].ToString();
                txt_FormNumber.Text = dt.Rows[0]["FormNumber"].ToString();
                ViewState["FormNumber"]= dt.Rows[0]["FormNumber"].ToString();
                if (Request.QueryString["from"] == "r")
                {
                    if (Convert.ToInt32(dt.Rows[0]["FormVersionNumber"]) <= 9)
                    {
                        txt_Version.Text = '0' + (Convert.ToInt32(dt.Rows[0]["FormVersionNumber"]) + 1).ToString();
                    }
                    else
                    {
                        txt_Version.Text = (Convert.ToInt32(dt.Rows[0]["FormVersionNumber"]) + 1).ToString();
                    }                  
                }
                else
                {
                    txt_Version.Text = dt.Rows[0]["FormVersionNumber"].ToString();                   
                }
                txt_FormName.Value = dt.Rows[0]["FormName"].ToString();
                ViewState["FormName"]= dt.Rows[0]["FormName"].ToString();
                if(Request.QueryString["from"] == "r")
                {
                    txt_Description.Value = "";
                }
                else
                {
                    if(dt.Rows[0]["FormDescription"].ToString() != "")
                    {
                        txt_Description.Value = dt.Rows[0]["FormDescription"].ToString();
                    }
                    else
                    {
                        txt_Description.Value = "N/A";
                    }
                }
                
                ddl_DocumentType.SelectedValue = dt.Rows[0]["Ref_DocTypeID"].ToString();
                getdocumentlist(ddl_DeptName.SelectedValue, ddl_DocumentType.SelectedValue);
                if (dt.Rows[0]["Ref_DocVersionID"].ToString() != "" && dt.Rows[0]["Ref_DocVersionID"].ToString() != "0")
                {
                    ddl_RefDocName.SelectedValue = dt.Rows[0]["Ref_DocVersionID"].ToString();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "show1", "showRefDocNumDropdown();", true);
                }
                else if (ddl_DocumentType.SelectedValue == "-1" && dt.Rows[0]["OtherDocumentNum"].ToString() != "")
                {
                    txt_RefDocNum.Text = dt.Rows[0]["OtherDocumentNum"].ToString();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "show2", "showRefDocNumTextBox1();", true);
                }
                else
                {
                    ddl_RefDocName.SelectedValue = "0";
                    txt_RefDocNum.Text = "";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "show1", "showRefDocNumDropdown();", true);
                }
                
                upRefDocNum.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M11:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M11:" + strline + "  " + "Form Details Retrieval failed.", "error");
            }
        }
        private void DisableFormDetails()
        {
            try
            {
                ddl_DeptName.Enabled = false;
                txt_FormNumber.ReadOnly = true;
                txt_Version.ReadOnly = true;
                txt_FormName.Disabled = true;
                txt_Description.Disabled = true;
                ddl_DocumentType.Enabled = false;
                ddl_RefDocName.Enabled = false;
                ddl_Approver.Enabled = false;
                txt_RefDocNum.Enabled = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M12:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M12:" + strline + "  " + "Form Details Disable failed.", "error");
            }
        }
        protected void btnFormNumcheck_Click(object sender, EventArgs e)
        {
            btnFormNumcheck.Enabled = false;
            try
            {
                if (!string.IsNullOrEmpty(txt_FormNumber.Text))
                {
                    docObjects.FormNumber = txt_FormNumber.Text.Trim();
                    DataTable DMS_dt = DMS_Bal.DMS_FormNumberSearch(docObjects);
                    if (DMS_dt.Rows.Count > 0)
                    {
                        lbl_number.Visible = true;
                        lbl_number.Text = "Form Number Already Taken";
                        lbl_number.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        lbl_number.Visible = true;
                        lbl_number.Text = "Form Number Available";
                        lbl_number.ForeColor = System.Drawing.Color.Green;
                    }
                }
                else
                {
                    lbl_number.Visible = false;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M18:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M18:" + strline + "  " + "Form Number Text Changed failed.", "error");
            }
            finally
            {
                btnFormNumcheck.Enabled = true;
            }
        }
        protected void btnFormNamecheck_Click(object sender, EventArgs e)
        {
            btnFormNamecheck.Enabled = false;
            try
            {
                if (ddl_DeptName.SelectedIndex == 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>  select Department.','warning');", true);
                    txt_FormName.Value = "";
                }
                else
                {
                    if (!string.IsNullOrEmpty(txt_FormName.Value))
                    {
                        docObjects.FormName = txt_FormName.Value.Trim();
                        docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName.SelectedValue);
                        DataTable DMS_dt = DMS_Bal.DMS_FormNameSearch(docObjects);
                        if (DMS_dt.Rows.Count > 0)
                        {
                            lblStatus.Visible = true;
                            lblStatus.Text = "Form Name Already Taken";
                            lblStatus.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            lblStatus.Visible = true;
                            lblStatus.Text = "Form Name Available";
                            lblStatus.ForeColor = System.Drawing.Color.Green;
                        }
                    }
                    else
                    {
                        lblStatus.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M19:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M19:" + strline + "  " + "Form Name Text Changed failed.", "error");
            }
            finally
            {
                btnFormNamecheck.Enabled = true;
            }
        }
        protected void btn_Cancel_Click(object sender, EventArgs e)
        {
            btn_Cancel.Enabled = false;
            try
            {
                if (Request.QueryString.Count > 0)
                {
                    if (Request.QueryString["FormVersionID"] != null)
                    {
                        if (Request.QueryString["from"] == "c")
                        {
                            Response.Redirect("~/DMS/DMSAdmin/FormCreatorList.aspx", false);
                        }
                        else if (Request.QueryString["from"] == "a")
                        {
                            Response.Redirect("~/DMS/DocumentController/FormApproverList.aspx", false);
                        }
                        else if (Request.QueryString["from"] == "p")
                        {
                            Response.Redirect("~/DMS/DMSAdmin/FormPendingList.aspx", false);
                        }
                        else if(Request.QueryString["from"] == "m")
                        {
                            Response.Redirect("~/DMS/DMSAdmin/FormManageList.aspx", false);
                        }
                        else
                        {
                            Response.Redirect("~/DMS/MainFormList.aspx", false);
                        }
                    }
                    else
                    {
                        Response.Redirect("~/DMS/MainFormList.aspx", false);
                    }
                }
                else
                {
                    Response.Redirect("~/DMS/MainFormList.aspx");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M20:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M20:" + strline + "  " + "Form Cancel Click failed.", "error");
            }
            finally
            {
                btn_Cancel.Enabled = true;
            }
        }
        protected void ElectronicSign_buttonClick(object sender, EventArgs e)
        {
            try
            {
                bool b = ElectronicSign.IsPasswordValid;
                if (b == true)
                {
                    if (hdnAction.Value == "Approve")
                    {
                        Approve();
                    }
                    else if (hdnAction.Value == "Revert")
                    {
                        Revert();
                    }
                    else if (hdnAction.Value == "Reject")
                    {
                        Reject();
                    }
                    else if (hdnAction.Value == "Submit" || ViewState["Action"].ToString() == "Submit")
                    {
                        Submit();
                    }
                    else if (hdnAction.Value == "Withdraw")
                    {
                        Withdraw();
                    }
                    else if(hdnAction.Value == "Update")
                    {
                        Update();
                    }
                }
                if (b == false)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Password is Incorrect!", "error");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M21:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M21:" + strline + "  " + "Electronic Signature validation failed.", "error");
            }
        }
        public void Submit()
        {
            try
            {
                StringBuilder sbErrorMsg = new StringBuilder();
                sbErrorMsg.Clear();
                objectDocUploadBO = new DocUploadBO();
                if (ddl_DeptName.SelectedValue.ToString().Trim() == "0")
                {
                    sbErrorMsg.Append(" Select Department." + "<br/>");
                }
                if (txt_FormNumber.Text.Trim() == "")
                {
                    sbErrorMsg.Append(" Enter Form Number." + "<br/>");
                }
                if (Request.QueryString.Count == 0)
                {
                    if (lbl_number.Visible == true)
                    {
                        if (lbl_number.Text.Trim() == "Form Number Already Taken")
                        {
                            if (!string.IsNullOrEmpty(txt_FormNumber.Text.Trim()))
                            {
                                if (Convert.ToInt32(ViewState["PKID"].ToString()) == 0)
                                {
                                    docObjects.FormNumber = txt_FormNumber.Text.Trim();
                                    DataTable DMS_dt = DMS_Bal.DMS_FormNumberSearch(docObjects);
                                    if (DMS_dt.Rows.Count > 0)
                                    {
                                        sbErrorMsg.Append("Form Number Already exists, Please Enter another Number." + "<br/>");
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(txt_FormNumber.Text.Trim()))
                        {
                            if (Convert.ToInt32(ViewState["PKID"].ToString()) == 0)
                            {
                                docObjects.FormNumber = txt_FormNumber.Text.Trim();
                                DataTable DMS_dt = DMS_Bal.DMS_FormNumberSearch(docObjects);
                                if (DMS_dt.Rows.Count > 0)
                                {
                                    sbErrorMsg.Append("Form Number Already exists, Please Enter another Number." + "<br/>");
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(txt_FormNumber.Text.Trim()))
                    {
                        if (ViewState["FormNumber"].ToString() != txt_FormNumber.Text.Trim())
                        {
                            docObjects.FormNumber = txt_FormNumber.Text.Trim();
                            DataTable DMS_dt = DMS_Bal.DMS_FormNumberSearch(docObjects);
                            if (DMS_dt.Rows.Count > 0)
                            {
                                sbErrorMsg.Append("Form Number Already exists, Please Enter another Number." + "<br/>");
                            }
                        }
                    }
                }
                if (txt_Version.Text.Trim() == "")
                {
                    sbErrorMsg.Append(" Enter Version Number." + "<br/>");
                }
                if (txt_FormName.Value.Trim() == "")
                {
                    sbErrorMsg.Append(" Enter Form Name." + "<br/>");
                }
                if (Request.QueryString.Count == 0)
                {
                    if (lblStatus.Visible == true)
                    {
                        if (lblStatus.Text.Trim() == "Form Name Already Taken")
                        {
                            if (!string.IsNullOrEmpty(txt_FormName.Value.Trim()))
                            {
                                if (Convert.ToInt32(ViewState["PKID"].ToString()) == 0)
                                {
                                    docObjects.FormName = txt_FormName.Value.Trim();
                                    docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName.SelectedValue);
                                    DataTable DMS_dt = DMS_Bal.DMS_FormNameSearch(docObjects);
                                    if (DMS_dt.Rows.Count > 0)
                                    {
                                        sbErrorMsg.Append("Form Name Already exists, Please Enter another Name." + "<br/>");
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(txt_FormName.Value.Trim()))
                        {
                            if (Convert.ToInt32(ViewState["PKID"].ToString()) == 0)
                            {
                                docObjects.FormName = txt_FormName.Value.Trim();
                                docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName.SelectedValue);
                                DataTable DMS_dt = DMS_Bal.DMS_FormNameSearch(docObjects);
                                if (DMS_dt.Rows.Count > 0)
                                {
                                    sbErrorMsg.Append("Form Name Already exists, Please Enter another Name." + "<br/>");
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(txt_FormName.Value.Trim()))
                    {
                        if (ViewState["FormName"].ToString() != txt_FormName.Value.Trim())
                        {
                            docObjects.FormName = txt_FormName.Value.Trim();
                            docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName.SelectedValue);
                            DataTable DMS_dt = DMS_Bal.DMS_FormNameSearch(docObjects);
                            if (DMS_dt.Rows.Count > 0)
                            {
                                sbErrorMsg.Append("Form Name Already exists, Please Enter another Name." + "<br/>");
                            }
                        }
                    }
                }
                if (ddl_Approver.SelectedValue.ToString().Trim() == "0")
                {
                    sbErrorMsg.Append(" Select Form Approver." + "<br/>");
                }
                if (lb_Upload.Text == "")
                {
                    sbErrorMsg.Append(" Upload Form." + "<br/>");
                }
                if (sbErrorMsg.Length > 8)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
                    return;
                }
                docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName.SelectedValue);
                docObjects.FormNumber = Regex.Replace(txt_FormNumber.Text.Trim(), @"\s+", " ");
                docObjects.Version = Convert.ToInt32(txt_Version.Text.Trim());
                docObjects.FormName = Regex.Replace(txt_FormName.Value.Trim(), @"\s+", " ");
                docObjects.Purpose = Regex.Replace(txt_Description.Value.Trim(), @"\s+", " ");
                docObjects.Remarks = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                if (ddl_DocumentType.SelectedValue != "-1")
                {
                    docObjects.DocRefID = Convert.ToInt32(ddl_RefDocName.SelectedValue);
                    docObjects.documentNUmber = "";
                    docObjects.action = 1;
                }
                else
                {
                    docObjects.documentNUmber = Regex.Replace(txt_RefDocNum.Text.Trim(), @"\s+", " ");
                    docObjects.DocRefID = 0;
                    docObjects.action = 0;
                }
                docObjects.DMS_CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                docObjects.PreparedDate = DateTime.Now;
                docObjects.DMSDocControllerID = Convert.ToInt32(ddl_Approver.SelectedValue);
                docObjects.FileType1 = ViewState["FormExt"].ToString();
                docObjects.Filename = ViewState["WordName"].ToString();
                if (ViewState["FormExt"].ToString() != "0")
                {
                    docObjects.Content = (byte[])ViewState["FormContent"];
                }
                else
                {
                    docObjects.Content = new byte[0];
                }
                if (btn_Submit.Value == "Submit")
                {
                    if (Convert.ToInt32(ViewState["PKID"].ToString()) > 0)
                    {
                        // update
                        docObjects.Status = 0;
                        docObjects.FormVersionID = Convert.ToInt32(ViewState["PKID"].ToString());
                        //int DMS_ins = DMS_Bal.DMS_FormUpdation(docObjects);
                        if (Session["vwdocObjects1F"] != null)
                        {
                            DMS_Bal.DMS_FormHistoryList((DocObjects)(Session["vwdocObjects1F"]));
                        }
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "New Form with Form Number <b>" + txt_FormNumber.Text + "</b> Created Successfully.", "success", "ReloadRevertedPage()");
                    }
                    else
                    {
                        // insert
                        
                        //ViewState["PKID"] = DMS_ins;

                        //docObjects2.Version = Convert.ToInt32(ViewState["PKID"]);
                        docObjects2.RoleID = 2;//admin
                        docObjects2.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        docObjects2.FilePath1 = ViewState["WordPath"].ToString();
                        docObjects2.FilePath2 = ViewState["PDFPath"].ToString();
                        docObjects2.FileType1 = ViewState["FormExt"].ToString();
                        docObjects2.FileType2 = ".pdf";
                        docObjects2.FileName1 = ViewState["DyWordName"].ToString();
                        docObjects2.FileName2 = ViewState["DyPDFName"].ToString();
                        objectDocUploadBO.docLocation = docObjects2;
                        //int DOC_Ins = DMS_Bal.DMS_InsertFormLocation(docObjects2);
                        if (Session["vwdocObjects1F"] != null)
                        {
                            DocObjects _objHistory = (DocObjects)(Session["vwdocObjects1F"]);
                            //_objHistory.Version = Convert.ToInt32(ViewState["PKID"]);
                            objectDocUploadBO.docComments = _objHistory;
                            //DMS_Bal.DMS_FormHistoryList(_objHistory);
                        }

                        docObjects.Status = 0;
                        objectDocUploadBO.formCreate = docObjects;
                        int DMS_ins = DMS_Bal.DMS_FormInsertion(objectDocUploadBO);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "New Form with Form Number <b>" + txt_FormNumber.Text + "</b> Created Successfully.", "success", "ReloadInitiationPage()");
                    }
                }
                else if (div2.InnerHtml == "Edit New Form")
                {
                    if (ViewState["FormExt"].ToString() != "0")
                    {
                        docObjects2.Version = Convert.ToInt32(ViewState["PKID"]);
                        docObjects2.RoleID = 2;//admin
                        docObjects2.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        docObjects2.FilePath1 = ViewState["WordPath"].ToString();
                        docObjects2.FilePath2 = ViewState["PDFPath"].ToString();
                        docObjects2.FileType1 = ViewState["FormExt"].ToString();
                        docObjects2.FileType2 = ".pdf";
                        docObjects2.FileName1 = ViewState["DyWordName"].ToString();
                        docObjects2.FileName2 = ViewState["DyPDFName"].ToString();
                        objectDocUploadBO.docLocation = docObjects2;
                        //int DOC_Ins = DMS_Bal.DMS_InsertFormLocation(docObjects2);
                    }
                    if (Session["vwdocObjects1F"] != null)
                    {
                        objectDocUploadBO.docComments = (DocObjects)(Session["vwdocObjects1F"]);
                        //DMS_Bal.DMS_FormHistoryList((DocObjects)(Session["vwdocObjects1F"]));
                    }

                    docObjects.Status = 3;
                    docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
                    objectDocUploadBO.formCreate = docObjects;
                    int DMS_ins = DMS_Bal.DMS_FormUpdation(objectDocUploadBO);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "New Form with Form Number <b>" + txt_FormNumber.Text + "</b> Updated Successfully.", "success", "ReloadRevertedPage()");
                }
                else if (btn_Submit.Value == "Revise")
                {
                    int MultiCount = DMS_Bal.DMS_MultipleEditRestriction(Convert.ToInt32(Request.QueryString["FormVersionID"].ToString()));
                    if (MultiCount == 0)
                    {
                        

                        //docObjects2.Version = Convert.ToInt32(ViewState["PKID"]);
                        docObjects2.RoleID = 2;//admin
                        docObjects2.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        docObjects2.FilePath1 = ViewState["WordPath"].ToString();
                        docObjects2.FilePath2 = ViewState["PDFPath"].ToString();
                        docObjects2.FileType1 = ViewState["FormExt"].ToString();
                        docObjects2.FileType2 = ".pdf";
                        docObjects2.FileName1 = ViewState["DyWordName"].ToString();
                        docObjects2.FileName2 = ViewState["DyPDFName"].ToString();
                        objectDocUploadBO.docLocation = docObjects2;
                        //int DOC_Ins = DMS_Bal.DMS_InsertFormLocation(docObjects2);
                        if (Session["vwdocObjects1F"] != null)
                        {
                            DocObjects _objHistory = (DocObjects)(Session["vwdocObjects1F"]);
                            //_objHistory.Version = Convert.ToInt32(ViewState["PKID"]);
                            objectDocUploadBO.docComments = _objHistory;
                            //DMS_Bal.DMS_FormHistoryList(_objHistory);
                        }

                        docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
                        objectDocUploadBO.formCreate = docObjects;
                        int DMS_ins = DMS_Bal.DMS_FormRevision(objectDocUploadBO);
                        //ViewState["PKID"] = DMS_ins;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Revision of Form with Form Number <b>" + txt_FormNumber.Text + "</b> Created Successfully.", "success", "ReloadInitiationPage()");
                    }
                    else
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), " Form has already been modified.", "info", "ReloadInitiationPage()");
                    }
                }
                else if (div2.InnerHtml == "Edit Revision Form")
                {
                    if (ViewState["FormExt"].ToString() != "0")
                    {
                        docObjects2.Version = Convert.ToInt32(ViewState["PKID"]);
                        docObjects2.RoleID = 2;//admin
                        docObjects2.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        docObjects2.FilePath1 = ViewState["WordPath"].ToString();
                        docObjects2.FilePath2 = ViewState["PDFPath"].ToString();
                        docObjects2.FileType1 = ViewState["FormExt"].ToString();
                        docObjects2.FileType2 = ".pdf";
                        docObjects2.FileName1 = ViewState["DyWordName"].ToString();
                        docObjects2.FileName2 = ViewState["DyPDFName"].ToString();
                        objectDocUploadBO.docLocation = docObjects2;
                        //int DOC_Ins = DMS_Bal.DMS_InsertFormLocation(docObjects2);
                    }

                    if (Session["vwdocObjects1F"] != null)
                    {
                        objectDocUploadBO.docComments = (DocObjects)(Session["vwdocObjects1F"]);
                        //DMS_Bal.DMS_FormHistoryList((DocObjects)(Session["vwdocObjects1F"]));
                    }

                    docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
                    objectDocUploadBO.formCreate = docObjects;
                    int DMS_ins = DMS_Bal.DMS_FormRevisionUpdate(objectDocUploadBO);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Revision of Form with Form Number <b>" + txt_FormNumber.Text + "</b> Updated Successfully.", "success", "ReloadRevertedPage()");
                }

            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M22:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M22:" + strline + "  " + "Form Submission failed.", "error");
            }
        }
        public void Withdraw()
        {
            try
            {
                StringBuilder sbErrorMsg = new StringBuilder();
                sbErrorMsg.Clear();
                if (ddl_Approver.SelectedValue.ToString().Trim() == "0")
                {
                    sbErrorMsg.Append(" Select Form Approver." + "<br/>");
                }
                if (sbErrorMsg.Length > 8)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
                    return;
                }
                docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
                docObjects.Purpose = Regex.Replace(txt_Description.Value.Trim(), @"\s+", " ");
                docObjects.DMS_CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                docObjects.DMSDocControllerID = Convert.ToInt32(ddl_Approver.SelectedValue);
                if (btn_Submit.Value == "Withdraw")
                {
                    int MultiCount = DMS_Bal.DMS_MultipleEditRestriction(Convert.ToInt32(Request.QueryString["FormVersionID"].ToString()));
                    if (MultiCount == 0)
                    {
                        docObjects.Remarks = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                        int DMS_ins = DMS_Bal.DMS_FormWithdraw(docObjects);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), " Withdraw of Form with Form Number <b>" + txt_FormNumber.Text + "</b> Created Successfully.", "success", "ReloadInitiationPage()");
                    }
                    else
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), " Form has already been modified.", "info", "ReloadInitiationPage()");
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M22:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M22:" + strline + "  " + "Form Submission failed.", "error");
            }
        }
        public void Approve()
        {
            try
            {
                docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
                docObjects.DMSDocControllerID = Convert.ToInt32(ddl_Approver.SelectedValue);
                int FormPCount = DMS_Bal.DMS_PendingFormRequestCount(Convert.ToInt32(Request.QueryString["FormVersionID"].ToString()),1);
                if (FormPCount == 0)
                {
                    if (div2.InnerHtml == "Approve New Form")
                    {
                        docObjects.Remarks = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                        int DMS_ins = DMS_BalDM.DMS_ApproveFormbyController(docObjects);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "New Form with Form Number <b>" + txt_FormNumber.Text + "</b> has been Approved Successfully.", "success", "ReloadApproverPage()");
                    }
                    else if (div2.InnerHtml == "Approve Form Revision")
                    {
                        docObjects.Remarks = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                        int DMS_ins = DMS_BalDM.DMS_ApproveFormRevisionbyController(docObjects);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Revision of Form with Form Number <b>" + txt_FormNumber.Text + "</b> has been Approved Successfully.", "success", "ReloadApproverPage()");
                    }
                    else if (div2.InnerHtml == "Approve Form Withdrawal")
                    {
                        docObjects.Remarks = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                        int DMS_ins = DMS_BalDM.DMS_WithdrawFormbyController(docObjects);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Withdraw of Form with Form Number <b>" + txt_FormNumber.Text + "</b> has been Approved Successfully.", "success", "ReloadApproverPage()");
                    }
                }
                else
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "There is/are form distribution request/s pending, form cannot be approved.", "error", "ReloadApproverPage()");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M23:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M23:" + strline + "  " + "Form Approval failed.", "error");
            }
        }
        public void Revert()
        {
            try
            {
                docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
                docObjects.DMSDocControllerID = Convert.ToInt32(ddl_Approver.SelectedValue);
                if (div2.InnerHtml == "Approve New Form")
                {
                    docObjects.Remarks = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                    int DMS_ins = DMS_BalDM.DMS_RevertFormbyController(docObjects);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "New Form with Form Number <b>" + txt_FormNumber.Text + "</b> has been Reverted Successfully.", "success", "ReloadApproverPage()");
                }
                else if (div2.InnerHtml == "Approve Form Revision")
                {
                    docObjects.Remarks = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                    int DMS_ins = DMS_BalDM.DMS_RevertFormbyController(docObjects);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Revision of Form with Form Number <b>" + txt_FormNumber.Text + "</b> has been Reverted Successfully.", "success", "ReloadApproverPage()");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M24:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M24:" + strline + "  " + "Form Revert failed.", "error");
            }
        }
        public void Reject()
        {
            try
            {
                docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());               
                if (div2.InnerHtml == "Approve New Form")
                {
                    docObjects.DMSDocControllerID = Convert.ToInt32(ddl_Approver.SelectedValue);
                    docObjects.Remarks = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                    docObjects.Status = 0;
                    int DMS_ins = DMS_BalDM.DMS_RejectFormbyController(docObjects);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "New Form with Form Number <b>" + txt_FormNumber.Text + "</b> has been Rejected by Doc Controller Successfully.", "success", "ReloadApproverPage()");
                }
                else if (div2.InnerHtml == "Approve Form Revision")
                {
                    docObjects.DMSDocControllerID = Convert.ToInt32(ddl_Approver.SelectedValue);
                    docObjects.Remarks = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                    docObjects.Status = 0;
                    int DMS_ins = DMS_BalDM.DMS_RejectFormbyController(docObjects);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Revision of Form with Form Number <b>" + txt_FormNumber.Text + "</b> has been Rejected by Doc Controller Successfully.", "success", "ReloadApproverPage()");
                }
                else if (div2.InnerHtml == "Approve Form Withdrawal")
                {
                    docObjects.DMSDocControllerID = Convert.ToInt32(ddl_Approver.SelectedValue);
                    docObjects.Remarks = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                    docObjects.Status = 1;
                    int DMS_ins = DMS_BalDM.DMS_RejectFormbyController(docObjects);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Withdraw of Form with Form Number <b>" + txt_FormNumber.Text + "</b> has been Rejected by Doc Controller Successfully.", "success", "ReloadApproverPage()");
                }
                else if (div2.InnerHtml == "Manage New Form Reviewers")
                {
                    docObjects.DMSDocControllerID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                    docObjects.Remarks = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                    docObjects.Status = 0;
                    int DMS_ins = DMS_BalDM.DMS_RejectFormbyController(docObjects);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "New Form with Form Number <b>" + txt_FormNumber.Text + "</b> has been Rejected by Admin Successfully.", "success", "ReloadManagePage()");
                }
                else if (div2.InnerHtml == "Manage Revision Form Reviewers")
                {
                    docObjects.DMSDocControllerID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                    docObjects.Remarks = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                    docObjects.Status = 0;
                    int DMS_ins = DMS_BalDM.DMS_RejectFormbyController(docObjects);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Revision of Form with Form Number <b>" + txt_FormNumber.Text + "</b> has been Rejected by Admin Successfully.", "success", "ReloadManagePage()");
                }
                else if (div2.InnerHtml == "Manage Withdraw Form Reviewers")
                {
                    docObjects.DMSDocControllerID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                    docObjects.Remarks = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                    docObjects.Status = 1;
                    int DMS_ins = DMS_BalDM.DMS_RejectFormbyController(docObjects);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Withdraw of Form with Form Number <b>" + txt_FormNumber.Text + "</b> has been Rejected by Admin Successfully.", "success", "ReloadManagePage()");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M25:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M25:" + strline + "  " + "Form Reject failed.", "error");
            }
        }
        private void EnableFormDetails()
        {
            try
            {
                ddl_DeptName.Enabled = true;
                txt_FormNumber.ReadOnly = false;
                txt_Version.ReadOnly = false;
                txt_FormName.Disabled = false;
                txt_Description.Disabled = false;
                ddl_DocumentType.Enabled = true;
                ddl_RefDocName.Enabled = true;
                ddl_Approver.Enabled = true;
                txt_RefDocNum.Enabled = true;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M12:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M12:" + strline + "  " + "Form Details Disable failed.", "error");
            }
        }
        protected void ConfirmCustomAlert_buttonClick(object sender, EventArgs e)
        {
            try
            {
                if (hdnAction.Value == "Submit")
                {
                    ViewState["Action"] = "Submit";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop4", "openUC_ElectronicSign();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M22:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M22:" + strline + "  " + "Form Submission failed.", "error");
            }
        }
        private string GetRefdocNo()
        {
            string strRefNo = "N/A";
            if (ddl_RefDocName.SelectedValue != "0")
            {
                string[] strRefNames = ddl_RefDocName.SelectedItem.Text.Trim().Split('-');
                strRefNo = strRefNames[0];
            }
            return strRefNo;
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            btnUpload.Enabled = false;
            try
            {
                Session["vwdocObjects1F"] = null;
                if (file_word.HasFile)
                {
                    Stream fs = file_word.PostedFile.InputStream;
                    BinaryReader br = new BinaryReader(fs);
                    byte[] Formbyte = br.ReadBytes((Int32)fs.Length);
                    if (Formbyte.Length < 1000)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' Not able to read content,form upload failed','warning');", true);
                        return;
                    }
                    string ext = System.IO.Path.GetExtension(file_word.FileName).ToLower();
                    string mime = MimeType.GetMimeType(Formbyte, file_word.FileName);
                    string[] filemimes1 = new string[] { "application/pdf", "application/x-msdownload", "application/unknown", "application/octet-stream" };
                    string[] filemimes = new string[] { "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/x-zip-compressed" };
                    if (filemimes1.Contains(mime))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' Please upload .doc or .docx files only ! <br/> The uploaded file is not valid or corrupted','warning');", true);
                        return;
                    }
                    if (!filemimes.Contains(mime))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' Please upload .doc or .docx files only ! <br/> The uploaded file is not valid or corrupted','warning');", true);
                        return;
                    }
                    if (ext == ".doc" || ext == ".docx")
                    {
                        string strWordMainFolderPath = HttpContext.Current.Server.MapPath(@"~/DMSTempForms/");
                        if (!Directory.Exists(strWordMainFolderPath))
                        {
                            Directory.CreateDirectory(strWordMainFolderPath);
                        }                        
                        string File1Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ext;
                        string strSourcePath = (strWordMainFolderPath + File1Path);
                        string File2Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                        string strTargPath = (storefolder + File2Path);

                        //Save original file
                        file_word.SaveAs(strSourcePath);

                        //call the converter method
                        bool isValid = converter.convert(strSourcePath, strTargPath);
                        if (isValid)
                        {
                            if (file_word.FileName.Trim().Length > 15)
                            {
                                lb_Upload.Attributes.Add("title", file_word.FileName.Trim());
                                lb_Upload.Text = file_word.FileName.Trim().Substring(0, 15) + "...";
                            }
                            else
                            {
                                lb_Upload.Text = file_word.FileName.Trim();
                            }
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "show4", "showViewForm();", true);
                            ViewState["WordPath"] = string.Format(@"~/DMSTempForms/") + File1Path;
                            ViewState["PDFPath"] = string.Format(@"~/Scripts/pdf.js/web/") + File2Path;
                            ViewState["DyWordName"] = File1Path;
                            ViewState["DyPDFName"] = File2Path;
                            ViewState["FormContent"] = Formbyte;
                            ViewState["FormExt"] = ext;
                            ViewState["WordName"] = file_word.FileName;

                            btn_Submit.Visible = true;
                            btn_Download.Visible = false;

                            docObjects1 = new DocObjects();
                            docObjects1.Version = Convert.ToInt32(ViewState["PKID"]);
                            docObjects1.RoleID = 2;//2-Admin
                            docObjects1.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            docObjects1.Remarks = "N/A";
                            docObjects1.action = 17;
                            docObjects1.CommentType = 1;
                            docObjects1.AssignedTo = 0;
                            docObjects1.AssignedRole = 0;
                            // DMS_Bal.DMS_FormHistoryList(docObjects1);
                            docObjects1.PreparedDate = DateTime.Now;
                            Session["vwdocObjects1F"] = docObjects1;
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "custAlertMsg(' <br/> Server error , contact admin.','error');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/> Please upload  .doc or .docx files only.','warning');", true);
                        return;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/> Uploaded form is empty.','warning');", true);
                    return;
                }
                www.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M23:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M23:" + strline + "  " + "Form Upload failed.", "error");
            }
            finally
            {
                btnUpload.Enabled = true;
            }
        }
        protected void lb_Upload_Click(object sender, EventArgs e)
        {
            lb_Upload.Enabled = false;
            try
            {
                if (ViewState["DyPDFName"].ToString() != "0")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view1", "ViewTempForm();", true);
                }
                else
                {
                    hdfPkID.Value = ViewState["PKID"].ToString();
                    hdfViewType.Value = "3";
                    hdfViewEmpID.Value = "0";
                    hdfViewRoleID.Value = "0";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view1", "ViewForm();", true);
                }
                if (txt_FormName.Value != "")
                {
                    span2.InnerText = txt_FormName.Value;
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#mpop_FileViewer').modal({ show: true, backdrop: 'static', keyboard: false });", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M24:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M24:" + strline + "  " + "Form Upload label click failed.", "error");
            }
            finally
            {
                lb_Upload.Enabled = true;
            }
        }
        protected void btn_Download_Click(object sender, EventArgs e)
        {
            btn_Download.Enabled = false;
            try
            {
                docObjects.Version = Convert.ToInt32(ViewState["PKID"]);
                docObjects.RoleID = 0;
                docObjects.EmpID = 0;
                docObjects.FileType1 = ViewState["FormExt"].ToString();
                docObjects.Mode = 0;
                System.Data.DataTable DMS_DT = DMS_Bal.DMS_GetFormLocationList(docObjects);
                string DocPath = null;
                Response.Clear();
                if (DMS_DT.Rows.Count > 0)
                {
                    if (ViewState["DyWordName"].ToString() != "0")
                    {
                        DocPath = ViewState["WordPath"].ToString();
                        if (ViewState["FormExt"].ToString() == ".doc")
                        {
                            Response.ContentType = "application/msword";
                        }
                        else if (ViewState["FormExt"].ToString() == ".docx")
                        {
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                        }
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + ViewState["WordName"].ToString());
                    }
                    else
                    {
                        DocPath = Server.MapPath(@DMS_DT.Rows[0]["FilePath"].ToString());
                        if (DMS_DT.Rows[0]["FileType"].ToString() == ".doc")
                        {
                            Response.ContentType = "application/msword";
                        }
                        else if (DMS_DT.Rows[0]["FileType"].ToString() == ".docx")
                        {
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                        }
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + DMS_DT.Rows[0]["FormFileName"].ToString());
                    }
                }
                else
                {
                    if (ViewState["DyWordName"].ToString() != "0")
                    {
                        DocPath = Server.MapPath(@DMS_DT.Rows[0]["FilePath"].ToString());
                        if (DMS_DT.Rows[0]["FileType"].ToString() == ".doc")
                        {
                            Response.ContentType = "application/msword";
                        }
                        else if (DMS_DT.Rows[0]["FileType"].ToString() == ".docx")
                        {
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                        }
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + DMS_DT.Rows[0]["FormFileName"].ToString());
                    }
                    else
                    {
                        DocPath = ViewState["WordPath"].ToString();
                        if (ViewState["FormExt"].ToString() == ".doc")
                        {
                            Response.ContentType = "application/msword";
                        }
                        else if (ViewState["FormExt"].ToString() == ".docx")
                        {
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                        }
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + ViewState["WordName"].ToString());
                    }

                }
                Response.TransmitFile(DocPath);
                DownloadRecord();
                Response.End();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M25:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M25:" + strline + "  " + "Form Download failed.", "error");
            }
            finally
            {
                btn_Download.Enabled = true;
            }
        }
        public void DownloadRecord()
        {
            docObjects1.Version = Convert.ToInt32(ViewState["PKID"]);
            docObjects1.RoleID = 2;//2-Admin
            docObjects1.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            docObjects1.Remarks = "N/A";
            docObjects1.action = 16;
            docObjects1.CommentType = 1;
            docObjects1.AssignedTo = 0;
            docObjects1.AssignedRole = 0;
            docObjects1.PreparedDate = DateTime.Now;
            DMS_Bal.DMS_FormHistoryList(docObjects1);
        }
        public void Update()
        {
            try
            {
                StringBuilder sbErrorMsg = new StringBuilder();
                sbErrorMsg.Clear();
                if (ViewState["CreationStatusID"].ToString() == "1")
                {
                    if (ddl_Creator.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append(" Select Form Approver." + "<br/>");
                    }
                }
                
                if (ddl_Approver.SelectedValue.ToString().Trim() == "0")
                {
                    sbErrorMsg.Append(" Select Form Approver." + "<br/>");
                }
                if (txt_Comments.Value.ToString().Trim() == "")
                {
                    sbErrorMsg.Append(" Enter Comments." + "<br/>");
                }
                if (sbErrorMsg.Length > 8)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
                    return;
                }
                else
                {
                    docObjects.pkid = Convert.ToInt32(ViewState["PKID"]);
                    docObjects.DMS_ModifiedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    if (ddl_Creator.SelectedValue == "")
                    {
                        docObjects.DMS_CreatedBy = 0;
                    }
                    else
                    {
                        docObjects.DMS_CreatedBy = Convert.ToInt32(ddl_Creator.SelectedValue);
                    }
                    docObjects.DMSDocControllerID = Convert.ToInt32(ddl_Approver.SelectedValue);
                    docObjects.Remarks = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                    int DMS_ins = DMS_Bal.DMS_UpdateFormUsers(docObjects);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Form with Form Number <b>" + txt_FormNumber.Text + "</b> Updated by Admin Successfully.", "success", "ReloadManagePage()");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M26:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M26:" + strline + "  " + "Form Updation failed.", "error");
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}