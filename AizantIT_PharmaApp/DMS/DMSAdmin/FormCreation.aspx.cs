﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using DevExpress.Web.Office;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AizantIT_PharmaApp.DMS
{
    public partial class FormCreation : System.Web.UI.Page
    {
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocObjects docObjects = new DocObjects();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=2").Length > 0 || dtTemp.Select("RoleID=30").Length > 0)//2-admin,30-controller
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                    else
                    {
                        ElectronicSign.buttonClick += new EventHandler(ElectronicSign_buttonClick);
                        ConfirmCustomAlert.buttonClick += new EventHandler(ConfirmCustomAlert_buttonClick);
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M1:" + strline + "  " + "Document Load failed", "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        private void InitializeThePage()
        {
            try
            {
                DMS_GetDocumentTypeList();
                DMS_GetDepartmentList();
                div2.InnerHtml = "Create Form";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop1", "showFormDetails();", true);
                ViewState["PKID"] = 0;
                if (Request.QueryString.Count > 0)
                {
                    if (Request.QueryString["FormVersionID"] != null)
                    {
                        ViewState["PKID"] = Request.QueryString["FormVersionID"];
                        RetriveFormDetails();
                        //btn_Reset.Visible = false;
                        comments.Visible = true;
                        DocObjects docObjects = new DocObjects();
                        docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
                        DataTable dt = DMS_Bal.DMS_GetFormDetails(docObjects);
                        btnFormNumcheck.Visible = false;
                        btnFormNamecheck.Visible = false;
                        btn_Save.Visible = false;
                        if (Request.QueryString["from"] == "c")
                        {

                            DMS_GetControllerList();
                            btn_Submit.Value = "Update";
                            docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
                            DataTable DMS_DT = DMS_BalDM.DMS_GetLatestFormComment(docObjects);
                            if (dt.Rows[0]["FormRequestTypeID"].ToString() == "1" || dt.Rows[0]["FormRequestTypeID"].ToString() == "2")
                            {
                                if (dt.Rows[0]["FormRequestTypeID"].ToString() == "1")
                                {
                                    div2.InnerHtml = "Edit New Form";
                                }
                                else if (dt.Rows[0]["FormRequestTypeID"].ToString() == "2")
                                {
                                    div2.InnerHtml = "Edit Revision Form";
                                    DisableFormDetails();
                                    ddl_Approver.Enabled = true;
                                    btnReset.Visible = false;
                                }
                                spancomment.Visible = false;
                                btnRefresh.Visible = false;
                                if (DMS_DT.Rows.Count > 0)
                                {
                                    txt_Comments.Text = DMS_DT.Rows[0]["Comments"].ToString();
                                }
                                else
                                {
                                    txt_Comments.Text = "";
                                }
                                txt_Comments.ReadOnly = true;
                            }
                            else
                            {
                                div2.InnerHtml = "Edit Withdraw Form";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop5", "showStatusChangeDiv();", true);
                                txt_statuschangecomments.Text = "";
                                btnSubmit.Value = "Update";
                                btnNext.Visible = false;
                                btnReset.Visible = false;
                                if (DMS_DT.Rows.Count > 0)
                                {
                                    txt_statuschangecomments.Text = DMS_DT.Rows[0]["Comments"].ToString();
                                }
                                else
                                {
                                    txt_statuschangecomments.Text = "";
                                }
                                txt_statuschangecomments.ReadOnly = true;
                            }
                        }
                        else if (Request.QueryString["from"] == "a")
                        {
                            DisableFormDetails();
                            ASPxRichEdit2.ReadOnly = true;
                            DMS_GetApproverList();
                            if (dt.Rows[0]["FormRequestTypeID"].ToString() == "1" || dt.Rows[0]["FormRequestTypeID"].ToString() == "2")
                            {
                                txt_Comments.ReadOnly = false;
                                btn_Submit.Visible = false;
                                btn_Reject.Visible = true;
                                btn_Revert.Visible = true;
                                btn_approve.Visible = true;
                                btnRefresh.Visible = false;
                                btnReset.Visible = false;
                                if (dt.Rows[0]["FormRequestTypeID"].ToString() == "1")
                                {
                                    //btn_approve.Value = "Approve";
                                    //btn_Revert.Value = "Revert";
                                    //btn_Reject.Value = "Reject";
                                    div2.InnerHtml = "Approve New Form";
                                }
                                else if (dt.Rows[0]["FormRequestTypeID"].ToString() == "2")
                                {
                                    //btn_approve.Value = "Approve Revision";
                                    //btn_Revert.Value = "Revert Revision";
                                    //btn_Reject.Value = "Reject Revision";
                                    div2.InnerHtml = "Approve Form Revision";
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop4", "showStatusChangeDiv();", true);
                                divOpinion.Style.Add("pointer-events", "none");
                                rbtnActive.Checked = false;
                                rbtnInActive.Checked = true;
                                txt_statuschangecomments.Text = "";
                                btnSubmit.Visible = false;
                                btnNext.Visible = false;
                                btnReject.Visible = true;
                                //btnRevert.Visible = true;
                                btnapprove.Visible = true;
                                btnReset.Visible = false;
                                //btn_approve.Value = "";
                                //btn_Revert.Value = "";
                                //btn_Reject.Value = "";
                                div2.InnerHtml = "Approve Form Withdrawal";
                            }
                            if (Request.QueryString["Pending"] == "1")
                            {
                                if (dt.Rows[0]["FormRequestTypeID"].ToString() == "2")
                                {
                                    btn_approve.Visible = false;
                                }
                                else if (dt.Rows[0]["FormRequestTypeID"].ToString() == "3")
                                {
                                    btnapprove.Visible = false;
                                }
                            }
                        }
                        else if (Request.QueryString["from"] == "s")
                        {
                            div2.InnerHtml = "Form Withdraw";
                            DMS_GetControllerList();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop5", "showStatusChangeDiv();", true);
                            divOpinion.Style.Add("pointer-events", "none");
                            rbtnActive.Checked = true;
                            rbtnInActive.Checked = false;
                            txt_statuschangecomments.Text = "";
                            btnSubmit.Visible = true;
                            btnNext.Visible = false;
                            btnReset.Visible = false;
                            DisableFormDetails();
                            ddl_Approver.Enabled = true;
                        }
                        else if (Request.QueryString["from"] == "r")
                        {
                            btn_Submit.Value = "Revise";
                            div2.InnerHtml = "Form Revision";
                            DMS_GetControllerList();
                            DisableFormDetails();
                            ddl_Approver.Enabled = true;
                            txt_Description.Enabled = true;
                            btnRefresh.Visible = false;
                            btnReset.Visible = false;
                            comments.Visible = false;
                            //btn_Reset.Visible = false;
                        }
                        else if (Request.QueryString["from"] == "p")
                        {
                            DMS_GetControllerList();
                            comments.Visible = false;
                            btn_Save.Visible = true;
                            DisableFormDetails();
                            btnReset.Visible = false;
                            btnRefresh.Visible = false;
                        }
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop3", "hideEditButton();", true);
                    }
                }
                else
                {
                    DMS_GetControllerList();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M2:" + strline + "  " + "Page Initialization failed", "error");
            }
        }
        public void DMS_GetDocumentTypeList()
        {
            try
            {
                JQDataTableBO objJQDataTableBO = new JQDataTableBO();
                objJQDataTableBO.iMode = 0;
                objJQDataTableBO.pkid = 0;
                objJQDataTableBO.iDisplayLength = 0;
                objJQDataTableBO.iDisplayStart = 0;
                objJQDataTableBO.iSortCol = 0;
                objJQDataTableBO.sSortDir = "";
                objJQDataTableBO.sSearch = "";
                DataTable DMS_Dt = DMS_Bal.DMS_GetDocumentTypeList(objJQDataTableBO);
                ddl_DocumentType.DataValueField = DMS_Dt.Columns["DocumentTypeID"].ToString();
                ddl_DocumentType.DataTextField = DMS_Dt.Columns["DocumentType"].ToString();
                ddl_DocumentType.DataSource = DMS_Dt;
                ddl_DocumentType.DataBind();
                ddl_DocumentType.Items.Insert(0, new ListItem("--Select Document Type--", "0"));
                ddl_DocumentType.Items.Insert(DMS_Dt.Rows.Count + 1, new ListItem("Others", "-1"));
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop2", "showRefDocNumDropdown();", true);
                //ddl_DocumentType.Items.Insert(0, new ListItem("--Select Document Type--", "0"));               
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M3:" + strline + "  " + "Document Type load failed", "error");
            }
        }
        public void DMS_GetDepartmentList()
        {
            try
            {
                DataTable DMS_Dt = DMS_BalDM.DMS_GetDepartmentList();
                ddl_DeptName.DataValueField = DMS_Dt.Columns["DeptID"].ToString();
                ddl_DeptName.DataTextField = DMS_Dt.Columns["DepartmentName"].ToString();
                ddl_DeptName.DataSource = DMS_Dt;
                ddl_DeptName.DataBind();
                ddl_DeptName.Items.Insert(0, new ListItem("--Select Department--", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M4:" + strline + "  " + "Department load failed", "error");
            }
        }
        public void getdocumentlist(string dept, string doctype)
        {
            try
            {
                DocumentManagmentBAL DMS_Bal = new DocumentManagmentBAL();
                docObjects.dept = dept;
                docObjects.DocumentType = doctype;
                docObjects.RType = "NA";
                DataTable DMS_dt = DMS_Bal.DMS_GetDocumentNameList(docObjects);
                ddl_RefDocName.DataSource = DMS_dt;
                ddl_RefDocName.DataValueField = DMS_dt.Columns["DocumentID"].ToString();
                ddl_RefDocName.DataTextField = DMS_dt.Columns["DocumentName"].ToString();
                ddl_RefDocName.DataBind();
                ddl_RefDocName.Items.Insert(0, new ListItem("--Select Document--", "0"));
            }
            catch (Exception ex)
            {
                throw ex;
                //string strline = HelpClass.LineNo(ex);
                //string strMsg = HelpClass.SQLEscapeString(ex.Message);
                //Aizant_log.Error("FC_M5:" + strline + "  " + strMsg);
                //HelpClass.custAlertMsg(this, this.GetType(), "FC_M5:" + strline + "  " + "Document Name Dropdown Loading failed", "error");
            }
        }
        protected void ddl_DeptName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl_DeptName.SelectedValue != "0" && ddl_DocumentType.SelectedValue != "-1")
            {
                getdocumentlist(ddl_DeptName.SelectedValue, ddl_DocumentType.SelectedValue);
            }
            else
            {
                ddl_RefDocName.Items.Clear();
                ddl_RefDocName.Items.Insert(0, new ListItem("--Select Document--", "0"));
            }
            upRefDocNum.Update();
        }
        protected void ddl_DocumentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl_DeptName.SelectedValue != "0" && ddl_DocumentType.SelectedValue != "-1")
            {
                getdocumentlist(ddl_DeptName.SelectedValue, ddl_DocumentType.SelectedValue);
            }
            else
            {
                ddl_RefDocName.Items.Clear();
                ddl_RefDocName.Items.Insert(0, new ListItem("--Select Document--", "0"));
            }
            if (ddl_DocumentType.SelectedValue == "-1")
            {
                ddl_RefDocName.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop2", "showRefDocNumTextBox();", true);
            }
            else
            {
                ddl_RefDocName.Visible = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop2", "showRefDocNumDropdown();", true);
            }
            upRefDocNum.Update();
        }
        public void DMS_GetControllerList()
        {
            try
            {
                DataTable DMS_Dt = DMS_Bal.DMS_GetControllersList();
                DataRow[] filtered = DMS_Dt.Select("EmpID NOT IN ('" + (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString() + "')");
                DMS_Dt = filtered.CopyToDataTable();
                ddl_Approver.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                ddl_Approver.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                ddl_Approver.DataSource = DMS_Dt;
                ddl_Approver.DataBind();
                ddl_Approver.Items.Insert(0, new ListItem("-- Select Form Approver --", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M6:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M6:" + strline + "  " + "Controller Name Dropdown Loading failed", "error");
            }
        }
        public void DMS_GetApproverList()
        {
            try
            {
                DataTable DMS_Dt = DMS_Bal.DMS_GetControllersList();
                ddl_Approver.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                ddl_Approver.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                ddl_Approver.DataSource = DMS_Dt;
                ddl_Approver.DataBind();
                ddl_Approver.Items.Insert(0, new ListItem("-- Select Form Approver --", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M7:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M7:" + strline + "  " + "Form Approver Dropdown Loading failed", "error");
            }
        }
        protected void Btn_Upload_Click(object sender, EventArgs e)
        {
            Btn_Upload.Enabled = false;
            try
            {
                if (file_word.HasFile)
                {
                    Session["CreateFileUpload"] = null;
                    Session["word"] = null;
                    byte[] imgbyte = null;
                    if (file_word.PostedFile.FileName.Length > 5)
                    {
                        string ext = System.IO.Path.GetExtension(file_word.FileName);
                        Stream fs = file_word.PostedFile.InputStream;
                        BinaryReader br = new BinaryReader(fs);
                        imgbyte = br.ReadBytes((Int32)fs.Length);
                        fs.Close();
                        string strFileName = file_word.FileName;
                        string strFileUpload = DynamicFolder.CreateDynamicFolder(5);
                        string File1Path = DateTime.Now.ToString("yyyyMMddHHmmss") + ext;
                        string strTargPath = (strFileUpload + "\\" + File1Path);
                        File.WriteAllBytes(strTargPath, imgbyte);
                        string mime = MimeType.GetMimeType(imgbyte, file_word.FileName);
                        string[] filemimes1 = new string[] { "application/pdf", "application/x-msdownload", "application/unknown", "application/octet-stream" };
                        string[] filemimes = new string[] { "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/x-zip-compressed" };
                        if (filemimes1.Contains(mime))
                        {
                            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop1", "ShowDocDetails1();", true);
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' Please upload .doc or .docx files only ! <br/> The uploaded file is not valid or corrupted','warning');", true);
                            return;
                        }
                        if (!filemimes.Contains(mime))
                        {
                            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop1", "ShowDocDetails1();", true);
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' Please upload .doc or .docx files only ! <br/> The uploaded file is not valid or corrupted','warning');", true);
                            return;
                        }
                        if (ext == ".doc" || ext == ".docx")
                        {
                            if (ext == ".doc")
                            {
                                RichEditDocumentServer server = new RichEditDocumentServer();
                                Document document = server.Document;
                                server.LoadDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Doc);
                                Section firstSection = document.Sections[0];
                                RichEditDocumentServer server1 = new RichEditDocumentServer();
                                Document document1 = server1.Document;
                                StringBuilder sbHFErrorMsg = new StringBuilder();
                                if (firstSection.HasHeader(HeaderFooterType.Primary) || firstSection.HasHeader(HeaderFooterType.First) || firstSection.HasHeader(HeaderFooterType.Even) || firstSection.HasHeader(HeaderFooterType.Odd))
                                {
                                    sbHFErrorMsg.Append("Upload the .doc file without header!" + "<br/>");
                                }
                                if (firstSection.HasFooter(HeaderFooterType.Primary) || firstSection.HasFooter(HeaderFooterType.First) || firstSection.HasFooter(HeaderFooterType.Even) || firstSection.HasFooter(HeaderFooterType.Odd))
                                {
                                    sbHFErrorMsg.Append("Upload the .doc file without footer!" + "<br/>");
                                }
                                if (sbHFErrorMsg.Length > 8)
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('" + sbHFErrorMsg.ToString() + "','warning');", true);
                                    return;
                                }
                                else
                                {
                                    document1.AppendDocumentContent(Server.MapPath(@"~/Files/FormDetailsTeplate.docx"), DevExpress.XtraRichEdit.DocumentFormat.OpenXml, Server.MapPath(@"~/Files/FormDetailsTeplate.docx"), InsertOptions.KeepSourceFormatting);
                                    document1.AppendDocumentContent(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Doc, strTargPath, InsertOptions.KeepSourceFormatting);
                                    Section firstSection1 = document1.Sections[0];
                                    SubDocument myHeader = firstSection1.BeginUpdateHeader(HeaderFooterType.Primary);
                                    DocumentRange[] deptrange = myHeader.FindAll("formdept1", SearchOptions.CaseSensitive, myHeader.Range);
                                    myHeader.InsertText(deptrange[0].Start, ddl_DeptName.SelectedItem.Text.Trim());
                                    myHeader.Delete(deptrange[0]);
                                    DocumentRange[] refsoprange = myHeader.FindAll("refsopnum1", SearchOptions.CaseSensitive, myHeader.Range);
                                    if (ddl_DocumentType.SelectedValue != "-1" && ddl_RefDocName.SelectedValue != "0")
                                    {

                                        myHeader.InsertText(refsoprange[0].Start, GetRefdocNo());
                                        myHeader.Delete(refsoprange[0]);
                                    }
                                    else if (ddl_DocumentType.SelectedValue == "-1" && txt_RefDocNum.Text.Trim() != "")
                                    {
                                        myHeader.InsertText(refsoprange[0].Start, txt_RefDocNum.Text.Trim());
                                        myHeader.Delete(refsoprange[0]);
                                    }
                                    else
                                    {
                                        myHeader.InsertText(refsoprange[0].Start, "NA");
                                        myHeader.Delete(refsoprange[0]);
                                    }
                                    DocumentRange[] formnamerange = myHeader.FindAll("formname1", SearchOptions.CaseSensitive, myHeader.Range);
                                    myHeader.InsertText(formnamerange[0].Start, txt_FormName.Text.Trim());
                                    myHeader.Delete(formnamerange[0]);
                                    DocumentRange[] formnumrange = myHeader.FindAll("formnum1", SearchOptions.CaseSensitive, myHeader.Range);
                                    if (Convert.ToInt32(txt_Version.Text.Trim()) <= 9)
                                    {
                                        myHeader.InsertText(formnumrange[0].Start, txt_FormNumber.Text.Trim() + "-0" + txt_Version.Text.Trim());
                                        myHeader.Delete(formnumrange[0]);
                                    }
                                    else
                                    {
                                        myHeader.InsertText(formnumrange[0].Start, txt_FormNumber.Text.Trim() + "-" + txt_Version.Text.Trim());
                                        myHeader.Delete(formnumrange[0]);
                                    }
                                    firstSection1.EndUpdateHeader(myHeader);
                                }
                                document1.SaveDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Doc);
                                server.Dispose();
                                server1.Dispose();
                                //Marshal.ReleaseComObject(server);
                                //Marshal.ReleaseComObject(server1);
                                ASPxRichEdit2.Open(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Doc);
                            }
                            else if (ext == ".docx")
                            {
                                RichEditDocumentServer server = new RichEditDocumentServer();
                                Document document = server.Document;
                                server.LoadDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                                Section firstSection = document.Sections[0];
                                RichEditDocumentServer server1 = new RichEditDocumentServer();
                                Document document1 = server1.Document;
                                StringBuilder sbHFErrorMsg = new StringBuilder();
                                if (firstSection.HasHeader(HeaderFooterType.Primary))
                                {
                                    sbHFErrorMsg.Append("Upload the .docx file without header!" + "<br/>");
                                }
                                if (firstSection.HasFooter(HeaderFooterType.Primary))
                                {
                                    sbHFErrorMsg.Append("Upload the .docx file without footer!" + "<br/>");
                                }
                                if (sbHFErrorMsg.Length > 8)
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('" + sbHFErrorMsg.ToString() + "','warning');", true);
                                    return;
                                }
                                else
                                {
                                    document1.AppendDocumentContent(Server.MapPath(@"~/Files/FormDetailsTeplate.docx"), DevExpress.XtraRichEdit.DocumentFormat.OpenXml, Server.MapPath(@"~/Files/FormDetailsTeplate.docx"), InsertOptions.KeepSourceFormatting);
                                    document1.AppendDocumentContent(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml, strTargPath, InsertOptions.KeepSourceFormatting);
                                    Section firstSection1 = document1.Sections[0];
                                    SubDocument myHeader = firstSection1.BeginUpdateHeader(HeaderFooterType.Primary);
                                    DocumentRange[] deptrange = myHeader.FindAll("formdept1", SearchOptions.CaseSensitive, myHeader.Range);
                                    myHeader.InsertText(deptrange[0].Start, ddl_DeptName.SelectedItem.Text.Trim());
                                    myHeader.Delete(deptrange[0]);
                                    DocumentRange[] refsoprange = myHeader.FindAll("refsopnum1", SearchOptions.CaseSensitive, myHeader.Range);
                                    if (ddl_DocumentType.SelectedValue != "-1" && ddl_RefDocName.SelectedValue != "0")
                                    {
                                        myHeader.InsertText(refsoprange[0].Start, GetRefdocNo());
                                        myHeader.Delete(refsoprange[0]);
                                    }
                                    else if (ddl_DocumentType.SelectedValue == "-1" && txt_RefDocNum.Text.Trim() != "")
                                    {
                                        myHeader.InsertText(refsoprange[0].Start, txt_RefDocNum.Text.Trim());
                                        myHeader.Delete(refsoprange[0]);
                                    }
                                    else
                                    {
                                        myHeader.InsertText(refsoprange[0].Start, "NA");
                                        myHeader.Delete(refsoprange[0]);
                                    }
                                    DocumentRange[] formnamerange = myHeader.FindAll("formname1", SearchOptions.CaseSensitive, myHeader.Range);
                                    myHeader.InsertText(formnamerange[0].Start, txt_FormName.Text.Trim());
                                    myHeader.Delete(formnamerange[0]);
                                    DocumentRange[] formnumrange = myHeader.FindAll("formnum1", SearchOptions.CaseSensitive, myHeader.Range);
                                    if (Convert.ToInt32(txt_Version.Text.Trim()) <= 9)
                                    {
                                        myHeader.InsertText(formnumrange[0].Start, txt_FormNumber.Text.Trim() + "-0" + txt_Version.Text.Trim());
                                        myHeader.Delete(formnumrange[0]);
                                    }
                                    else
                                    {
                                        myHeader.InsertText(formnumrange[0].Start, txt_FormNumber.Text.Trim() + "-" + txt_Version.Text.Trim());
                                        myHeader.Delete(formnumrange[0]);
                                    }
                                    firstSection1.EndUpdateHeader(myHeader);
                                }
                                document1.SaveDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                                server.Dispose();
                                server1.Dispose();
                                //Marshal.ReleaseComObject(server);
                                //Marshal.ReleaseComObject(server1);
                                ASPxRichEdit2.Open(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/> Please upload  .doc or .docx files only','warning');", true);
                            return;
                        }
                    }
                }
                www.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M8:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M8:" + strline + "  " + "Form Upload failed", "error");
            }
            finally
            {
                Btn_Upload.Enabled = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop2", "showFormContent();", true);
            }
        }
        protected void btn_Reset_Click(object sender, EventArgs e)
        {
            btnReset.Enabled = false;
            try
            {
                DMS_Reset();
                EnableFormDetails();
                btnCancel.Visible = true;
                btnReset.Visible = true;
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop1", "showFormDetails();", true);
                ddl_RefDocName.Visible = true;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M10:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M10:" + strline + "  " + "Form Details Reset failed", "error");
            }
            finally
            {
                btnReset.Enabled = true;
            }
        }
        //protected void btn_Submit_Click(object sender, EventArgs e)
        //{
        //    btn_Submit.Enabled = false;
        //    try
        //    {
        //        ViewState["hdnAction"] = "Submit";
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop4", "openElectronicSignModal();", true);
        //    }
        //    catch (Exception ex)
        //    {
        //        string strline = HelpClass.LineNo(ex);
        //        string strMsg = HelpClass.SQLEscapeString(ex.Message);
        //        Aizant_log.Error("FC_M9:" + strline + "  " + strMsg);
        //        HelpClass.custAlertMsg(this, this.GetType(), "FC_M9:" + strline + "  " + "Form Submission failed", "error");
        //    }
        //    finally
        //    {
        //        btn_Submit.Enabled = true;
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop1", "showFormDetails();", true);
        //    }
        //}
        public void DMS_Reset()
        {
            try
            {
                txt_FormName.Text = "";
                txt_FormNumber.Text = "";
                txt_Version.Text = "";
                txt_Description.Text = "";
                txt_RefDocNum.Text = "";
                ddl_DeptName.SelectedIndex = -1;
                ddl_DocumentType.SelectedIndex = -1;
                ddl_RefDocName.SelectedIndex = -1;
                ddl_Approver.SelectedIndex = -1;
                DocumentManager.CloseDocument(Guid.NewGuid().ToString());
                //string strTargPath = Server.MapPath(@"~/Files/EmptyFile.docx");
                //ASPxRichEdit2.Open(strTargPath);
                btnNext.Visible = true;
                btnFormNumcheck.Visible = true;
                btnFormNamecheck.Visible = true;
                lbl_number.Visible = false;
                lblStatus.Visible = false;
            }
            catch (Exception ex)
            {
                throw ex;
                //string strline = HelpClass.LineNo(ex);
                //string strMsg = HelpClass.SQLEscapeString(ex.Message);
                //Aizant_log.Error("FC_M10:" + strline + "  " + strMsg);
                //HelpClass.custAlertMsg(this, this.GetType(), "FC_M10:" + strline + "  " + "Form Details Reset failed", "error");
            }
        }
        private void RetriveFormDetails()
        {
            try
            {
                DocObjects docObjects = new DocObjects();
                docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
                DataTable dt = DMS_Bal.DMS_GetFormDetails(docObjects);
                ddl_DeptName.SelectedValue = dt.Rows[0]["DeptID"].ToString();
                txt_FormNumber.Text = dt.Rows[0]["FormNumber"].ToString();
                if (Request.QueryString["from"] == "r")
                {
                    if (Convert.ToInt32(dt.Rows[0]["FormVersionNumber"]) <= 9)
                    {
                        txt_Version.Text = '0' + (Convert.ToInt32(dt.Rows[0]["FormVersionNumber"]) + 1).ToString();
                    }
                    else
                    {
                        txt_Version.Text = (Convert.ToInt32(dt.Rows[0]["FormVersionNumber"]) + 1).ToString();
                    }
                }
                else
                {
                    txt_Version.Text = dt.Rows[0]["FormVersionNumber"].ToString();
                }
                txt_FormName.Text = dt.Rows[0]["FormName"].ToString();
                txt_Description.Text = dt.Rows[0]["FormDescription"].ToString();
                ddl_DocumentType.SelectedValue = dt.Rows[0]["Ref_DocTypeID"].ToString();
                getdocumentlist(ddl_DeptName.SelectedValue, ddl_DocumentType.SelectedValue);
                if (dt.Rows[0]["Ref_DocVersionID"].ToString() != "" && dt.Rows[0]["Ref_DocVersionID"].ToString() != "0")
                {
                    ddl_RefDocName.SelectedValue = dt.Rows[0]["Ref_DocVersionID"].ToString();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "show1", "showRefDocNumDropdown();", true);
                }
                else if (ddl_DocumentType.SelectedValue == "-1" && dt.Rows[0]["OtherDocumentNum"].ToString() != "")
                {
                    txt_RefDocNum.Text = dt.Rows[0]["OtherDocumentNum"].ToString();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "show2", "showRefDocNumTextBox();", true);
                }
                else
                {
                    ddl_RefDocName.SelectedValue = "0";
                    txt_RefDocNum.Text = "";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "show1", "showRefDocNumDropdown();", true);
                }
                ddl_Approver.SelectedValue = dt.Rows[0]["ApproverID"].ToString();
                WordUpload.Visible = false;
                DataTable DMS_DT = DMS_Bal.DMS_GetFormContent(docObjects);
                ASPxRichEdit2.Open(
                               Guid.NewGuid().ToString(),
                               DevExpress.XtraRichEdit.DocumentFormat.Rtf,
                               () =>
                               {
                                   byte[] docBytes = (byte[])DMS_DT.Rows[0]["FormContent"];
                                   return new MemoryStream(docBytes);
                               }
                               );
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M11:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M11:" + strline + "  " + "Form Details Retrieval failed", "error");
            }

        }
        private void DisableFormDetails()
        {
            try
            {
                ddl_DeptName.Enabled = false;
                txt_FormNumber.ReadOnly = true;
                txt_Version.ReadOnly = true;
                txt_FormName.ReadOnly = true;
                txt_Description.Enabled = false;
                ddl_DocumentType.Enabled = false;
                ddl_RefDocName.Enabled = false;
                ddl_Approver.Enabled = false;
                txt_RefDocNum.Enabled = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M12:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M12:" + strline + "  " + "Form Details Disable failed", "error");
            }
        }
        //protected void btn_Reject_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ViewState["hdnAction"] = "Reject";
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop4", "openElectronicSignModal();", true);
        //    }
        //    catch (Exception ex)
        //    {
        //        string strline = HelpClass.LineNo(ex);
        //        string strMsg = HelpClass.SQLEscapeString(ex.Message);
        //        Aizant_log.Error("FC_M13:" + strline + "  " + strMsg);
        //        HelpClass.custAlertMsg(this, this.GetType(), "FC_M13:" + strline + "  " + "Form Rejection failed", "error");
        //    }
        //}
        //protected void btn_Revert_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ViewState["hdnAction"] = "Revert";
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop4", "openElectronicSignModal();", true);
        //    }
        //    catch (Exception ex)
        //    {
        //        string strline = HelpClass.LineNo(ex);
        //        string strMsg = HelpClass.SQLEscapeString(ex.Message);
        //        Aizant_log.Error("FC_M14:" + strline + "  " + strMsg);
        //        HelpClass.custAlertMsg(this, this.GetType(), "FC_M14:" + strline + "  " + "Form Revertion failed", "error");
        //    }
        //}
        //protected void btn_approve_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ViewState["hdnAction"] = "Approve";
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop4", "openElectronicSignModal();", true);
        //    }
        //    catch (Exception ex)
        //    {
        //        string strline = HelpClass.LineNo(ex);
        //        string strMsg = HelpClass.SQLEscapeString(ex.Message);
        //        Aizant_log.Error("FC_M15:" + strline + "  " + strMsg);
        //        HelpClass.custAlertMsg(this, this.GetType(), "FC_M15:" + strline + "  " + "Form Approval failed", "error");
        //    }
        //}
        protected void btnNext_Click(object sender, EventArgs e)
        {
            btnNext.Enabled = false;
            try
            {
                StringBuilder sbErrorMsg = new StringBuilder();
                sbErrorMsg.Clear();
                if (ddl_DeptName.SelectedValue.ToString().Trim() == "0")
                {
                    sbErrorMsg.Append(" Select Department" + "<br/>");
                }
                if (txt_FormNumber.Text.Trim() == "")
                {
                    sbErrorMsg.Append(" Enter Form Number" + "<br/>");
                }
                if (Request.QueryString.Count == 0)
                {
                    if (!string.IsNullOrEmpty(txt_FormNumber.Text.Trim()))
                    {
                        docObjects.FormNumber = txt_FormNumber.Text.Trim();
                        DataTable DMS_dt = DMS_Bal.DMS_FormNumberSearch(docObjects);
                        if (DMS_dt.Rows.Count > 0)
                        {
                            sbErrorMsg.Append("Form Number Already exists Please Enter another Number" + "<br/>");
                        }
                    }
                }
                if (txt_Version.Text.Trim() == "")
                {
                    sbErrorMsg.Append(" Enter Version Number" + "<br/>");
                }
                else
                {
                    try
                    {
                        if (Convert.ToInt32(txt_Version.Text.Trim()) < 0)
                        {
                            sbErrorMsg.Append("Version Number Starts From 0" + "<br/>");
                        }
                        if (txt_Version.Text.Trim().ToLower().Contains("e"))
                        {
                            sbErrorMsg.Append("Version Number Should be Integer" + "<br/>");
                        }
                    }
                    catch
                    {
                    }
                }
                if (txt_FormName.Text.Trim() == "")
                {
                    sbErrorMsg.Append(" Enter Form Name" + "<br/>");
                }
                if (Request.QueryString.Count == 0)
                {
                    if (!string.IsNullOrEmpty(txt_FormName.Text.Trim()))
                    {
                        docObjects.FormName = txt_FormName.Text.Trim();
                        docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName.SelectedValue);
                        DataTable DMS_dt = DMS_Bal.DMS_FormNameSearch(docObjects);
                        if (DMS_dt.Rows.Count > 0)
                        {
                            sbErrorMsg.Append("Form Name Already exists Please Enter another Name" + "<br/>");
                        }
                    }
                }
                if (ddl_Approver.SelectedValue.ToString().Trim() == "0")
                {
                    sbErrorMsg.Append(" Select a Form Approver" + "<br/>");
                }
                if (sbErrorMsg.Length > 8)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop2", "showFormDetails();", true);
                    return;
                }
                else
                {
                    if (Request.QueryString.Count == 0)
                    {
                        RichEditDocumentServer server = new RichEditDocumentServer();
                        server.LoadDocument(Server.MapPath(@"~/Files/FormDetailsTeplate.docx"), DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                        Document document = server.Document;
                        Section firstSection = document.Sections[0];
                        SubDocument myHeader = firstSection.BeginUpdateHeader(HeaderFooterType.Primary);
                        DocumentRange[] deptrange = myHeader.FindAll("formdept1", SearchOptions.CaseSensitive, myHeader.Range);
                        myHeader.InsertText(deptrange[0].Start, ddl_DeptName.SelectedItem.Text.Trim());
                        myHeader.Delete(deptrange[0]);
                        DocumentRange[] refsoprange = myHeader.FindAll("refsopnum1", SearchOptions.CaseSensitive, myHeader.Range);
                        if (ddl_DocumentType.SelectedValue != "-1" && ddl_RefDocName.SelectedValue != "0")
                        {
                            myHeader.InsertText(refsoprange[0].Start, GetRefdocNo());
                            myHeader.Delete(refsoprange[0]);
                        }
                        else if (ddl_DocumentType.SelectedValue == "-1" && txt_RefDocNum.Text.Trim() != "")
                        {
                            myHeader.InsertText(refsoprange[0].Start, txt_RefDocNum.Text.Trim());
                            myHeader.Delete(refsoprange[0]);
                        }
                        else
                        {
                            myHeader.InsertText(refsoprange[0].Start, "NA");
                            myHeader.Delete(refsoprange[0]);
                        }
                        DocumentRange[] formnamerange = myHeader.FindAll("formname1", SearchOptions.CaseSensitive, myHeader.Range);
                        myHeader.InsertText(formnamerange[0].Start, txt_FormName.Text.Trim());
                        myHeader.Delete(formnamerange[0]);
                        DocumentRange[] formnumrange = myHeader.FindAll("formnum1", SearchOptions.CaseSensitive, myHeader.Range);
                        if (Convert.ToInt32(txt_Version.Text.Trim()) <= 9)
                        {
                            if (Convert.ToInt32(txt_Version.Text.Trim()) == 0)
                            {
                                myHeader.InsertText(formnumrange[0].Start, txt_FormNumber.Text.Trim() + "-00");
                                myHeader.Delete(formnumrange[0]);
                            }
                            else
                            {
                                myHeader.InsertText(formnumrange[0].Start, txt_FormNumber.Text.Trim() + "-0" + txt_Version.Text.Trim());
                                myHeader.Delete(formnumrange[0]);
                            }
                        }
                        else
                        {
                            myHeader.InsertText(formnumrange[0].Start, txt_FormNumber.Text.Trim() + "-" + txt_Version.Text.Trim());
                            myHeader.Delete(formnumrange[0]);
                        }
                        firstSection.EndUpdateHeader(myHeader);
                        string strFileUpload = DynamicFolder.CreateDynamicFolder(5);
                        string File1Path = DateTime.Now.ToString("yyyyMMddHHmmss") + ".docx";
                        string strTargPath = (strFileUpload + "\\" + File1Path);
                        document.SaveDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                        server.Dispose();
                        //Marshal.ReleaseComObject(server);

                        ASPxRichEdit2.DocumentId = Guid.NewGuid().ToString();
                        ASPxRichEdit2.Open(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                        DocumentManager.CloseDocument(Guid.NewGuid().ToString());

                    }
                    else
                    {
                        if (Request.QueryString["from"] == "c")
                        {
                            docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
                            DataTable dt = DMS_Bal.DMS_GetFormDetails(docObjects);
                            DataTable DMS_DT = DMS_Bal.DMS_GetFormContent(docObjects);
                            byte[] docBytes = (byte[])DMS_DT.Rows[0]["FormContent"];
                            string strFileUpload = DynamicFolder.CreateDynamicFolder(5);
                            string File1Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".rtf";
                            string strTargPath = (strFileUpload + "\\" + File1Path);
                            File.WriteAllBytes(strTargPath, docBytes);
                            RichEditDocumentServer server = new RichEditDocumentServer();
                            server.LoadDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
                            Document document = server.Document;
                            Section firstSection = document.Sections[0];
                            SubDocument myHeader = firstSection.BeginUpdateHeader(HeaderFooterType.Primary);
                            DocumentRange[] deptrange = myHeader.FindAll(dt.Rows[0]["DeptName"].ToString(), SearchOptions.CaseSensitive, myHeader.Range);
                            myHeader.InsertText(deptrange[0].Start, ddl_DeptName.SelectedItem.Text.Trim());
                            myHeader.Delete(deptrange[0]);
                            if (dt.Rows[0]["Ref_DocNumber"].ToString() != "")
                            {
                                DocumentRange[] refsoprange = myHeader.FindAll(dt.Rows[0]["Ref_DocNumber"].ToString(), SearchOptions.CaseSensitive, myHeader.Range);
                                if (ddl_DocumentType.SelectedValue != "-1" && ddl_RefDocName.SelectedValue != "0")
                                {
                                    myHeader.InsertText(refsoprange[0].Start, GetRefdocNo());
                                    myHeader.Delete(refsoprange[0]);
                                }
                                else if (ddl_DocumentType.SelectedValue == "-1" && txt_RefDocNum.Text.Trim() != "")
                                {
                                    myHeader.InsertText(refsoprange[0].Start, txt_RefDocNum.Text.Trim());
                                    myHeader.Delete(refsoprange[0]);
                                }
                                else
                                {
                                    myHeader.InsertText(refsoprange[0].Start, "NA");
                                    myHeader.Delete(refsoprange[0]);
                                }
                            }
                            else if (dt.Rows[0]["OtherDocumentNum"].ToString() != "")
                            {
                                DocumentRange[] refsoprange = myHeader.FindAll(dt.Rows[0]["OtherDocumentNum"].ToString(), SearchOptions.CaseSensitive, myHeader.Range);
                                if (ddl_DocumentType.SelectedValue != "-1" && ddl_RefDocName.SelectedValue != "0")
                                {
                                    myHeader.InsertText(refsoprange[0].Start, GetRefdocNo());
                                    myHeader.Delete(refsoprange[0]);
                                }
                                else if (ddl_DocumentType.SelectedValue == "-1" && txt_RefDocNum.Text.Trim() != "")
                                {
                                    myHeader.InsertText(refsoprange[0].Start, txt_RefDocNum.Text.Trim());
                                    myHeader.Delete(refsoprange[0]);
                                }
                                else
                                {
                                    myHeader.InsertText(refsoprange[0].Start, "NA");
                                    myHeader.Delete(refsoprange[0]);
                                }
                            }
                            else
                            {
                                DocumentRange[] refsoprange = myHeader.FindAll("NA", SearchOptions.CaseSensitive, myHeader.Range);
                                if (ddl_DocumentType.SelectedValue != "-1" && ddl_RefDocName.SelectedValue != "0")
                                {
                                    myHeader.InsertText(refsoprange[0].Start, GetRefdocNo());
                                    myHeader.Delete(refsoprange[0]);
                                }
                                else if (ddl_DocumentType.SelectedValue == "-1" && txt_RefDocNum.Text.Trim() != "")
                                {
                                    myHeader.InsertText(refsoprange[0].Start, txt_RefDocNum.Text.Trim());
                                    myHeader.Delete(refsoprange[0]);
                                }
                                else
                                {
                                    myHeader.InsertText(refsoprange[0].Start, "NA");
                                    myHeader.Delete(refsoprange[0]);
                                }
                            }
                            DocumentRange[] formnamerange = myHeader.FindAll(dt.Rows[0]["FormName"].ToString(), SearchOptions.CaseSensitive, myHeader.Range);
                            myHeader.InsertText(formnamerange[0].Start, txt_FormName.Text.Trim());
                            myHeader.Delete(formnamerange[0]);
                            DocumentRange[] formnumrange = myHeader.FindAll(dt.Rows[0]["FormNumber"].ToString() + "-" + dt.Rows[0]["FormVersionNumber"].ToString(), SearchOptions.CaseSensitive, myHeader.Range);
                            myHeader.InsertText(formnumrange[0].Start, txt_FormNumber.Text.Trim() + "-" + txt_Version.Text.Trim());
                            myHeader.Delete(formnumrange[0]);
                            firstSection.EndUpdateHeader(myHeader);
                            document.SaveDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                            server.Dispose();
                            //Marshal.ReleaseComObject(server);

                            ASPxRichEdit2.DocumentId = Guid.NewGuid().ToString();
                            ASPxRichEdit2.Open(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                            DocumentManager.CloseDocument(Guid.NewGuid().ToString());
                        }
                        else if (Request.QueryString["from"] == "r")
                        {
                            docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
                            DataTable dt = DMS_Bal.DMS_GetFormDetails(docObjects);
                            DataTable DMS_DT = DMS_Bal.DMS_GetFormContent(docObjects);
                            byte[] docBytes = (byte[])DMS_DT.Rows[0]["FormContent"];
                            string strFileUpload = DynamicFolder.CreateDynamicFolder(5);
                            string File1Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".rtf";
                            string strTargPath = (strFileUpload + "\\" + File1Path);
                            File.WriteAllBytes(strTargPath, docBytes);
                            RichEditDocumentServer server = new RichEditDocumentServer();
                            server.LoadDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
                            Document document = server.Document;
                            Section firstSection = document.Sections[0];
                            SubDocument myHeader = firstSection.BeginUpdateHeader(HeaderFooterType.Primary);
                            //DocumentRange[] deptrange = myHeader.FindAll(dt.Rows[0]["DeptName"].ToString(), SearchOptions.CaseSensitive, myHeader.Range);
                            //myHeader.InsertText(deptrange[0].Start, ddl_DeptName.SelectedItem.Text.Trim());
                            //myHeader.Delete(deptrange[0]);
                            //if (dt.Rows[0]["Ref_DocNumber"].ToString() != "")
                            //{
                            //    DocumentRange[] refsoprange = myHeader.FindAll(dt.Rows[0]["Ref_DocNumber"].ToString(), SearchOptions.CaseSensitive, myHeader.Range);
                            //    if (ddl_DocumentType.SelectedValue != "-1" && ddl_RefDocName.SelectedValue != "0")
                            //    {
                            //        myHeader.InsertText(refsoprange[0].Start, ddl_RefDocName.SelectedItem.Text.Trim());
                            //        myHeader.Delete(refsoprange[0]);
                            //    }
                            //    else if (ddl_DocumentType.SelectedValue == "-1" && txt_RefDocNum.Text.Trim() != "")
                            //    {
                            //        myHeader.InsertText(refsoprange[0].Start, txt_RefDocNum.Text.Trim());
                            //        myHeader.Delete(refsoprange[0]);
                            //    }
                            //    else
                            //    {
                            //        myHeader.InsertText(refsoprange[0].Start, "NA");
                            //        myHeader.Delete(refsoprange[0]);
                            //    }
                            //}
                            //else if (dt.Rows[0]["OtherDocumentNum"].ToString() != "")
                            //{
                            //    DocumentRange[] refsoprange = myHeader.FindAll(dt.Rows[0]["OtherDocumentNum"].ToString(), SearchOptions.CaseSensitive, myHeader.Range);
                            //    if (ddl_DocumentType.SelectedValue != "-1" && ddl_RefDocName.SelectedValue != "0")
                            //    {
                            //        myHeader.InsertText(refsoprange[0].Start, ddl_RefDocName.SelectedItem.Text.Trim());
                            //        myHeader.Delete(refsoprange[0]);
                            //    }
                            //    else if (ddl_DocumentType.SelectedValue == "-1" && txt_RefDocNum.Text.Trim() != "")
                            //    {
                            //        myHeader.InsertText(refsoprange[0].Start, txt_RefDocNum.Text.Trim());
                            //        myHeader.Delete(refsoprange[0]);
                            //    }
                            //    else
                            //    {
                            //        myHeader.InsertText(refsoprange[0].Start, "NA");
                            //        myHeader.Delete(refsoprange[0]);
                            //    }
                            //}
                            //else
                            //{
                            //    DocumentRange[] refsoprange = myHeader.FindAll("NA", SearchOptions.CaseSensitive, myHeader.Range);
                            //    if (ddl_DocumentType.SelectedValue != "-1" && ddl_RefDocName.SelectedValue != "0")
                            //    {
                            //        myHeader.InsertText(refsoprange[0].Start, ddl_RefDocName.SelectedItem.Text.Trim());
                            //        myHeader.Delete(refsoprange[0]);
                            //    }
                            //    else if (ddl_DocumentType.SelectedValue == "-1" && txt_RefDocNum.Text.Trim() != "")
                            //    {
                            //        myHeader.InsertText(refsoprange[0].Start, txt_RefDocNum.Text.Trim());
                            //        myHeader.Delete(refsoprange[0]);
                            //    }
                            //    else
                            //    {
                            //        myHeader.InsertText(refsoprange[0].Start, "NA");
                            //        myHeader.Delete(refsoprange[0]);
                            //    }
                            //}
                            //DocumentRange[] formnamerange = myHeader.FindAll(dt.Rows[0]["FormName"].ToString(), SearchOptions.CaseSensitive, myHeader.Range);
                            //myHeader.InsertText(formnamerange[0].Start, txt_FormName.Text.Trim());
                            //myHeader.Delete(formnamerange[0]);
                            DocumentRange[] formnumrange = myHeader.FindAll(dt.Rows[0]["FormNumber"].ToString() + "-" + dt.Rows[0]["FormVersionNumber"].ToString(), SearchOptions.CaseSensitive, myHeader.Range);
                            myHeader.InsertText(formnumrange[0].Start, txt_FormNumber.Text.Trim() + "-" + txt_Version.Text.Trim());
                            myHeader.Delete(formnumrange[0]);
                            firstSection.EndUpdateHeader(myHeader);
                            document.SaveDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                            server.Dispose();
                            //Marshal.ReleaseComObject(server);

                            ASPxRichEdit2.DocumentId = Guid.NewGuid().ToString();
                            ASPxRichEdit2.Open(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                            DocumentManager.CloseDocument(Guid.NewGuid().ToString());
                        }
                    }
                    btnNext.Visible = false;
                    btnCancel.Visible = false;
                    btnReset.Visible = false;
                    btnFormNumcheck.Visible = false;
                    btnFormNamecheck.Visible = false;
                    lbl_number.Visible = false;
                    lblStatus.Visible = false;
                    DisableFormDetails();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop2", "showFormContent();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M16:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M16:" + strline + "  " + "Next Click failed", "error");
            }
            finally
            {
                btnNext.Enabled = true;
            }
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            btnRefresh.Enabled = false;
            try
            {
                RichEditDocumentServer server = new RichEditDocumentServer();
                server.LoadDocument(Server.MapPath(@"~/Files/FormDetailsTeplate.docx"), DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                Document document = server.Document;
                Section firstSection = document.Sections[0];
                SubDocument myHeader = firstSection.BeginUpdateHeader(HeaderFooterType.Primary);
                DocumentRange[] deptrange = myHeader.FindAll("formdept1", SearchOptions.CaseSensitive, myHeader.Range);
                myHeader.InsertText(deptrange[0].Start, ddl_DeptName.SelectedItem.Text.Trim());
                myHeader.Delete(deptrange[0]);
                DocumentRange[] refsoprange = myHeader.FindAll("refsopnum1", SearchOptions.CaseSensitive, myHeader.Range);
                if (ddl_RefDocName.SelectedValue != "" && ddl_RefDocName.SelectedValue != "0")
                {
                    myHeader.InsertText(refsoprange[0].Start, GetRefdocNo());
                    myHeader.Delete(refsoprange[0]);
                }
                else if (ddl_DocumentType.SelectedValue == "-1" && txt_RefDocNum.Text.Trim() != "")
                {
                    myHeader.InsertText(refsoprange[0].Start, txt_RefDocNum.Text.Trim());
                    myHeader.Delete(refsoprange[0]);
                }
                else
                {
                    myHeader.InsertText(refsoprange[0].Start, "NA");
                    myHeader.Delete(refsoprange[0]);
                }
                DocumentRange[] formnamerange = myHeader.FindAll("formname1", SearchOptions.CaseSensitive, myHeader.Range);
                myHeader.InsertText(formnamerange[0].Start, txt_FormName.Text.Trim());
                myHeader.Delete(formnamerange[0]);
                DocumentRange[] formnumrange = myHeader.FindAll("formnum1", SearchOptions.CaseSensitive, myHeader.Range);
                if (Convert.ToInt32(txt_Version.Text.Trim()) <= 9)
                {
                    myHeader.InsertText(formnumrange[0].Start, txt_FormNumber.Text.Trim() + "-0" + txt_Version.Text.Trim());
                    myHeader.Delete(formnumrange[0]);
                }
                else
                {
                    myHeader.InsertText(formnumrange[0].Start, txt_FormNumber.Text.Trim() + "-" + txt_Version.Text.Trim());
                    myHeader.Delete(formnumrange[0]);
                }
                firstSection.EndUpdateHeader(myHeader);
                string strFileUpload = DynamicFolder.CreateDynamicFolder(5);
                string File1Path = DateTime.Now.ToString("yyyyMMddHHmmss") + ".docx";
                string strTargPath = (strFileUpload + "\\" + File1Path);
                document.SaveDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                server.Dispose();
                //Marshal.ReleaseComObject(server);

                ASPxRichEdit2.DocumentId = Guid.NewGuid().ToString();
                ASPxRichEdit2.Open(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                DocumentManager.CloseDocument(Guid.NewGuid().ToString());

            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M17:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M17:" + strline + "  " + "Refresh Click failed", "error");
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop2", "showFormContent();", true);
                btnRefresh.Enabled = true;
            }
        }
        protected void btnFormNumcheck_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txt_FormNumber.Text))
                {
                    docObjects.FormNumber = txt_FormNumber.Text.Trim();
                    DataTable DMS_dt = DMS_Bal.DMS_FormNumberSearch(docObjects);
                    if (DMS_dt.Rows.Count > 0)
                    {
                        lbl_number.Visible = true;
                        lbl_number.Text = "Form Number Already Taken";
                        lbl_number.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        lbl_number.Visible = true;
                        lbl_number.Text = "Form Number Available";
                        lbl_number.ForeColor = System.Drawing.Color.Green;
                    }
                }
                else
                {
                    lbl_number.Visible = false;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M18:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M18:" + strline + "  " + "Form Number Text Changed failed", "error");
            }
        }
        protected void btnFormNamecheck_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddl_DeptName.SelectedIndex == 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>  select Department','warning');", true);
                    txt_FormName.Text = "";
                }
                else
                {
                    if (!string.IsNullOrEmpty(txt_FormName.Text))
                    {
                        docObjects.FormName = txt_FormName.Text.Trim();
                        docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName.SelectedValue);
                        DataTable DMS_dt = DMS_Bal.DMS_FormNameSearch(docObjects);
                        if (DMS_dt.Rows.Count > 0)
                        {
                            lblStatus.Visible = true;
                            lblStatus.Text = "Form Name Already Taken";
                            lblStatus.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            lblStatus.Visible = true;
                            lblStatus.Text = "Form Name Available";
                            lblStatus.ForeColor = System.Drawing.Color.Green;
                        }
                    }
                    else
                    {
                        lblStatus.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M19:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M19:" + strline + "  " + "Form Name Text Changed failed", "error");
            }
        }
        protected void btn_Cancel_Click(object sender, EventArgs e)
        {
            btnCancel.Enabled = false;
            try
            {
                if (Request.QueryString.Count > 0)
                {
                    if (Request.QueryString["FormVersionID"] != null)
                    {
                        if (Request.QueryString["from"] == "c")
                        {
                            Response.Redirect("~/DMS/DMSAdmin/FormCreatorList.aspx", false);
                        }
                        else if (Request.QueryString["from"] == "a")
                        {
                            Response.Redirect("~/DMS/DocumentController/FormApproverList.aspx", false);
                        }
                        else if (Request.QueryString["from"] == "p")
                        {
                            Response.Redirect("~/DMS/DMSAdmin/FormPendingList.aspx", false);
                        }
                        else
                        {
                            Response.Redirect("~/DMS/MainFormList.aspx", false);
                        }
                    }
                    else
                    {
                        Response.Redirect("~/DMS/MainFormList.aspx", false);
                    }
                }
                else
                {
                    Response.Redirect("~/DMS/MainFormList.aspx");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M20:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M20:" + strline + "  " + "Form Cancel Click failed", "error");
            }
            finally
            {
                btnCancel.Enabled = true;
            }
        }
        protected void ElectronicSign_buttonClick(object sender, EventArgs e)
        {
            try
            {
                bool b = ElectronicSign.IsPasswordValid;
                if (b == true)
                {
                    if (hdnAction.Value == "Approve")
                    {
                        Approve();
                    }
                    else if (hdnAction.Value == "Revert")
                    {
                        Revert();
                    }
                    else if (hdnAction.Value == "Reject")
                    {
                        Reject();
                    }
                    else if (hdnAction.Value == "Submit")
                    {
                        Submit();
                    }
                    //else if (hdnAction.Value == "Save")
                    //{
                    //    Save();
                    //}
                    else if (hdnAction.Value == "Withdraw")
                    {
                        Withdraw();
                    }
                }
                if (b == false)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Password is Incorrect!", "error");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M21:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M21:" + strline + "  " + "Electronic Signature validation failed", "error");
            }
        }
        public void Submit()
        {
            try
            {
                StringBuilder sbErrorMsg = new StringBuilder();
                sbErrorMsg.Clear();
                byte[] Doc_data;
                Doc_data = ASPxRichEdit2.SaveCopy(DevExpress.XtraRichEdit.DocumentFormat.Rtf);
                int Form_length = Doc_data.Length;
                if (ddl_DeptName.SelectedValue.ToString().Trim() == "0")
                {
                    sbErrorMsg.Append(" Select Department" + "<br/>");
                }
                if (txt_FormNumber.Text.Trim() == "")
                {
                    sbErrorMsg.Append(" Enter Form Number" + "<br/>");
                }
                if (Request.QueryString.Count == 0)
                {
                    if (lbl_number.Visible == true)
                    {
                        if (lbl_number.Text.Trim() == "Form Number Already Taken")
                        {
                            sbErrorMsg.Append("Form Number Already exists Please Enter another Number" + "<br/>");
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(txt_FormNumber.Text.Trim()))
                        {
                            if (Convert.ToInt32(ViewState["PKID"].ToString()) == 0)
                            {
                                docObjects.FormNumber = txt_FormNumber.Text.Trim();
                                DataTable DMS_dt = DMS_Bal.DMS_FormNumberSearch(docObjects);
                                if (DMS_dt.Rows.Count > 0)
                                {
                                    sbErrorMsg.Append("Form Number Already exists Please Enter another Number" + "<br/>");
                                }
                            }
                        }
                    }
                }
                if (txt_Version.Text.Trim() == "")
                {
                    sbErrorMsg.Append(" Enter Version Number" + "<br/>");
                }
                if (txt_FormName.Text.Trim() == "")
                {
                    sbErrorMsg.Append(" Enter Form Name" + "<br/>");
                }
                if (Request.QueryString.Count == 0)
                {
                    if (lblStatus.Visible == true)
                    {
                        if (lblStatus.Text.Trim() == "Form Name Already Taken")
                        {
                            sbErrorMsg.Append("Form Name Already exists Please Enter another Name" + "<br/>");
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(txt_FormName.Text.Trim()))
                        {
                            if (Convert.ToInt32(ViewState["PKID"].ToString()) == 0)
                            {
                                docObjects.FormName = txt_FormName.Text.Trim();
                                docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName.SelectedValue);
                                DataTable DMS_dt = DMS_Bal.DMS_FormNameSearch(docObjects);
                                if (DMS_dt.Rows.Count > 0)
                                {
                                    sbErrorMsg.Append("Form Name Already exists Please Enter another Name" + "<br/>");
                                }
                            }
                        }
                    }
                }
                //if (txt_Description.Text.Trim() == "")
                //{
                //    sbErrorMsg.Append(" Enter Form Comments" + "<br/>");
                //}
                if (ddl_Approver.SelectedValue.ToString().Trim() == "0")
                {
                    sbErrorMsg.Append(" Select a Form Approver" + "<br/>");
                }
                if (Form_length < 28300)
                {
                    sbErrorMsg.Append(" Please Design Complete Form" + "<br/>");
                }
                if (sbErrorMsg.Length > 8)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
                    return;
                }
                docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName.SelectedValue);
                docObjects.FormNumber = txt_FormNumber.Text.Trim();
                docObjects.Version = Convert.ToInt32(txt_Version.Text.Trim());
                docObjects.FormName = txt_FormName.Text.Trim();
                docObjects.Purpose = txt_Description.Text.Trim();
                if (ddl_DocumentType.SelectedValue != "-1")
                {
                    docObjects.DocRefID = Convert.ToInt32(ddl_RefDocName.SelectedValue);
                    docObjects.documentNUmber = "";
                    docObjects.action = 1;
                }
                else
                {
                    docObjects.documentNUmber = txt_RefDocNum.Text.Trim();
                    docObjects.DocRefID = 0;
                    docObjects.action = 0;
                }
                docObjects.DMS_CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                docObjects.DMSDocControllerID = Convert.ToInt32(ddl_Approver.SelectedValue);
                docObjects.Content = Doc_data;
                if (btn_Submit.Value == "Submit")
                {
                    if (Convert.ToInt32(ViewState["PKID"].ToString()) > 0)
                    {
                        // update
                        docObjects.Status = 0;
                        docObjects.FormVersionID = Convert.ToInt32(ViewState["PKID"].ToString());
                        int DMS_ins = DMS_Bal.DMS_FormUpdation(docObjects);
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "New Form with Form Number " + txt_FormNumber.Text + " Created Successfully", "success", "ReloadRevertedPage()");
                    }
                    else
                    {
                        // insert
                        docObjects.Status = 0;
                        int DMS_ins = DMS_Bal.DMS_FormInsertion(docObjects);
                        ViewState["PKID"] = DMS_ins;
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "New Form with Form Number " + txt_FormNumber.Text + " Created Successfully", "success", "ReloadInitiationPage()");
                    }
                    //int DMS_ins = DMS_Bal.DMS_FormInsertion(docObjects);
                    //HelpClass.custAlertMsgWithFunc(this, this.GetType(), " New Form with Form Number " + txt_FormNumber.Text + " Created Successfully", "success", "ReloadInitiationPage()");
                }
                else if (div2.InnerHtml == "Edit New Form")
                {
                    docObjects.Status = 3;
                    docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
                    int DMS_ins = DMS_Bal.DMS_FormUpdation(docObjects);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "New Form with Form Number " + txt_FormNumber.Text + " Updated Successfully", "success", "ReloadRevertedPage()");
                }
                else if (btn_Submit.Value == "Revise")
                {
                    docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
                    int DMS_ins = DMS_Bal.DMS_FormRevision(docObjects);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Revision of Form with Form Number " + txt_FormNumber.Text + " Created Successfully", "success", "ReloadInitiationPage()");
                }
                else if (div2.InnerHtml == "Edit Revision Form")
                {
                    docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
                    int DMS_ins = DMS_Bal.DMS_FormRevisionUpdate(docObjects);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Revision of Form with Form Number " + txt_FormNumber.Text + " Updated Successfully", "success", "ReloadRevertedPage()");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M22:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M22:" + strline + "  " + "Form Submission failed", "error");
            }
        }
        public void Withdraw()
        {
            try
            {
                StringBuilder sbErrorMsg = new StringBuilder();
                sbErrorMsg.Clear();
                if (ddl_Approver.SelectedValue.ToString().Trim() == "0")
                {
                    sbErrorMsg.Append(" Select a Form Approver" + "<br/>");
                }
                if (txt_statuschangecomments.Text.Trim() == "")
                {
                    sbErrorMsg.Append(" Enter Comments" + "<br/>");
                }
                if (sbErrorMsg.Length > 8)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
                    return;
                }
                docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
                docObjects.Purpose = txt_Description.Text.Trim();
                docObjects.DMS_CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                docObjects.DMSDocControllerID = Convert.ToInt32(ddl_Approver.SelectedValue);

                if (btnSubmit.Value == "Withdraw")
                {
                    docObjects.Remarks = txt_statuschangecomments.Text.Trim();
                    int DMS_ins = DMS_Bal.DMS_FormWithdraw(docObjects);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), " Withdraw of Form with Form Number " + txt_FormNumber.Text + " Created Successfully", "success", "ReloadInitiationPage()");
                }
                else if (btnSubmit.Value == "Update")
                {
                    docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
                    int DMS_ins = DMS_Bal.DMS_FormUpdation(docObjects);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "New Form with Form Number " + txt_FormNumber.Text + " Updated Successfully", "success", "ReloadRevertedPage()");
                }

            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M22:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M22:" + strline + "  " + "Form Submission failed", "error");
            }
        }
        public void Approve()
        {
            try
            {
                docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
                docObjects.DMSDocControllerID = Convert.ToInt32(ddl_Approver.SelectedValue);

                if (div2.InnerHtml == "Approve New Form")
                {
                    docObjects.Remarks = txt_Comments.Text.Trim();
                    int DMS_ins = DMS_BalDM.DMS_ApproveFormbyController(docObjects);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "New Form with Form Number " + txt_FormNumber.Text + " has been Approved Successfully", "success", "ReloadApproverPage()");
                }
                else if (div2.InnerHtml == "Approve Form Revision")
                {
                    docObjects.Remarks = txt_Comments.Text.Trim();
                    int DMS_ins = DMS_BalDM.DMS_ApproveFormRevisionbyController(docObjects);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Revision of Form with Form Number " + txt_FormNumber.Text + " has been Approved Successfully", "success", "ReloadApproverPage()");
                }

                else if (div2.InnerHtml == "Approve Form Withdrawal")
                {
                    docObjects.Remarks = txt_statuschangecomments.Text.Trim();
                    int DMS_ins = DMS_BalDM.DMS_WithdrawFormbyController(docObjects);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Withdraw of Form with Form Number " + txt_FormNumber.Text + " has been Approved Successfully", "success", "ReloadApproverPage()");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M23:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M23:" + strline + "  " + "Form Approval failed", "error");
            }
        }
        public void Revert()
        {
            try
            {
                docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
                docObjects.DMSDocControllerID = Convert.ToInt32(ddl_Approver.SelectedValue);
                if (div2.InnerHtml == "Approve New Form")
                {
                    docObjects.Remarks = txt_Comments.Text.Trim();
                    int DMS_ins = DMS_BalDM.DMS_RevertFormbyController(docObjects);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "New Form with Form Number " + txt_FormNumber.Text + " has been Reverted Successfully", "success", "ReloadApproverPage()");
                }
                else if (div2.InnerHtml == "Approve Form Revision")
                {
                    docObjects.Remarks = txt_Comments.Text.Trim();
                    int DMS_ins = DMS_BalDM.DMS_RevertFormbyController(docObjects);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Revision of Form with Form Number " + txt_FormNumber.Text + " has been Reverted Successfully", "success", "ReloadApproverPage()");
                }
                else if (div2.InnerHtml == "Approve Form Withdrawal")
                {
                    docObjects.Remarks = txt_statuschangecomments.Text.Trim();
                    int DMS_ins = DMS_BalDM.DMS_RevertFormbyController(docObjects);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Withdraw of Form with Form Number " + txt_FormNumber.Text + " has been Reverted Successfully", "success", "ReloadApproverPage()");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M24:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M24:" + strline + "  " + "Form Revert failed", "error");
            }
        }
        public void Reject()
        {
            try
            {
                docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
                docObjects.DMSDocControllerID = Convert.ToInt32(ddl_Approver.SelectedValue);
                if (div2.InnerHtml == "Approve New Form")
                {
                    docObjects.Remarks = txt_Comments.Text.Trim();
                    docObjects.Status = 0;
                    int DMS_ins = DMS_BalDM.DMS_RejectFormbyController(docObjects);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "New Form with Form Number " + txt_FormNumber.Text + " has been Rejected Successfully", "success", "ReloadApproverPage()");
                }
                else if (div2.InnerHtml == "Approve Form Revision")
                {
                    docObjects.Remarks = txt_Comments.Text.Trim();
                    docObjects.Status = 0;
                    int DMS_ins = DMS_BalDM.DMS_RejectFormbyController(docObjects);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Revision of Form with Form Number " + txt_FormNumber.Text + " has been Rejected Successfully", "success", "ReloadApproverPage()");
                }
                else if (div2.InnerHtml == "Approve Form Withdrawal")
                {
                    docObjects.Remarks = txt_statuschangecomments.Text.Trim();
                    docObjects.Status = 1;
                    int DMS_ins = DMS_BalDM.DMS_RejectFormbyController(docObjects);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Withdraw of Form with Form Number " + txt_FormNumber.Text + " has been Rejected Successfully", "success", "ReloadApproverPage()");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M25:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M25:" + strline + "  " + "Form Reject failed", "error");
            }
        }
        private void EnableFormDetails()
        {
            try
            {
                ddl_DeptName.Enabled = true;
                txt_FormNumber.ReadOnly = false;
                txt_Version.ReadOnly = false;
                txt_FormName.ReadOnly = false;
                txt_Description.Enabled = true;
                ddl_DocumentType.Enabled = true;
                ddl_RefDocName.Enabled = true;
                ddl_Approver.Enabled = true;
                txt_RefDocNum.Enabled = true;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M12:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M12:" + strline + "  " + "Form Details Disable failed", "error");
            }
        }
        //protected void btnSubmit_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        StringBuilder sbErrorMsg = new StringBuilder();
        //        sbErrorMsg.Clear();               
        //        if (ddl_Approver.SelectedValue.ToString().Trim() == "0")
        //        {
        //            sbErrorMsg.Append(" Select a Form Approver" + "<br/>");
        //        }
        //        if (txt_statuschangecomments.Text.Trim() == "")
        //        {
        //            sbErrorMsg.Append(" Enter Comments" + "<br/>");
        //        }
        //        if (sbErrorMsg.Length > 8)
        //        {
        //            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
        //            return;
        //        }
        //        docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
        //        docObjects.Purpose = txt_Description.Text.Trim();
        //        docObjects.DMS_CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
        //        docObjects.DMSDocControllerID = Convert.ToInt32(ddl_Approver.SelectedValue);
        //        if (btn_Submit.Value == "Submit")
        //        {
        //            int DMS_ins = DMS_Bal.DMS_FormInsertion(docObjects);
        //            HelpClass.custAlertMsgWithFunc(this, this.GetType(), " Form Number " + txt_FormNumber.Text + " Created Successfully", "success", "ReloadInitiationPage()");
        //        }
        //        else if (btn_Submit.Value == "Update")
        //        {
        //            docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
        //            int DMS_ins = DMS_Bal.DMS_FormUpdation(docObjects);
        //            HelpClass.custAlertMsgWithFunc(this, this.GetType(), " Form Number " + txt_FormNumber.Text + " Updated Successfully", "success", "ReloadRevertedPage()");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string strline = HelpClass.LineNo(ex);
        //        string strMsg = HelpClass.SQLEscapeString(ex.Message);
        //        Aizant_log.Error("FC_M12:" + strline + "  " + strMsg);
        //        HelpClass.custAlertMsg(this, this.GetType(), "FC_M12:" + strline + "  " + "Form Details Disable failed", "error");
        //    }
        //}
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            EnableFormDetails();
            btnNext.Visible = true;
            btnReset.Visible = true;
            btnCancel.Visible = true;
            //btnEdit.Visible = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop2", "hideEditButton();", true);
        }
        //public void Save()
        //{
        //    try
        //    {
        //        StringBuilder sbErrorMsg = new StringBuilder();
        //        sbErrorMsg.Clear();
        //        byte[] Doc_data;
        //        Doc_data = ASPxRichEdit2.SaveCopy(DevExpress.XtraRichEdit.DocumentFormat.Rtf);
        //        int Form_length = Doc_data.Length;
        //        if (ddl_DeptName.SelectedValue.ToString().Trim() == "0")
        //        {
        //            sbErrorMsg.Append(" Select Department" + "<br/>");
        //        }
        //        if (txt_FormNumber.Text.Trim() == "")
        //        {
        //            sbErrorMsg.Append(" Enter Form Number" + "<br/>");
        //        }
        //        if (Request.QueryString.Count == 0)
        //        {
        //            if (lbl_number.Visible == true)
        //            {
        //                if (lbl_number.Text.Trim() == "Form Number Already Taken")
        //                {
        //                    sbErrorMsg.Append("Form Number Already exists Please Enter another Number" + "<br/>");
        //                }
        //            }
        //            else
        //            {
        //                if (!string.IsNullOrEmpty(txt_FormNumber.Text.Trim()))
        //                {
        //                    docObjects.FormNumber = txt_FormNumber.Text.Trim();
        //                    DataTable DMS_dt = DMS_Bal.DMS_FormNumberSearch(docObjects);
        //                    if (DMS_dt.Rows.Count > 0)
        //                    {
        //                        sbErrorMsg.Append("Form Number Already exists Please Enter another Number" + "<br/>");
        //                    }
        //                }
        //            }
        //        }
        //        if (txt_Version.Text.Trim() == "")
        //        {
        //            sbErrorMsg.Append(" Enter Version Number" + "<br/>");
        //        }
        //        if (txt_FormName.Text.Trim() == "")
        //        {
        //            sbErrorMsg.Append(" Enter Form Name" + "<br/>");
        //        }
        //        if (Request.QueryString.Count == 0)
        //        {
        //            if (lblStatus.Visible == true)
        //            {
        //                if (lblStatus.Text.Trim() == "Form Name Already Taken")
        //                {
        //                    sbErrorMsg.Append("Form Name Already exists Please Enter another Name" + "<br/>");
        //                }
        //            }
        //            else
        //            {
        //                if (!string.IsNullOrEmpty(txt_FormName.Text.Trim()))
        //                {

        //                    docObjects.FormName = txt_FormName.Text.Trim();
        //                    docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName.SelectedValue);
        //                    DataTable DMS_dt = DMS_Bal.DMS_FormNameSearch(docObjects);
        //                    if (DMS_dt.Rows.Count > 0)
        //                    {
        //                        sbErrorMsg.Append("Form Name Already exists Please Enter another Name" + "<br/>");
        //                    }
        //                }
        //            }
        //        }
        //        if (ddl_Approver.SelectedValue.ToString().Trim() == "0")
        //        {
        //            sbErrorMsg.Append(" Select a Form Approver" + "<br/>");
        //        }
        //        if (Form_length == 0)
        //        {
        //            sbErrorMsg.Append("Do not Save Empty Form" + "<br/>");
        //        }
        //        if (sbErrorMsg.Length > 8)
        //        {
        //            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
        //            return;
        //        }
        //        docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName.SelectedValue);
        //        docObjects.FormNumber = txt_FormNumber.Text.Trim();
        //        docObjects.Version = Convert.ToInt32(txt_Version.Text.Trim());
        //        docObjects.FormName = txt_FormName.Text.Trim();
        //        docObjects.Purpose = txt_Description.Text.Trim();
        //        if (ddl_DocumentType.SelectedValue != "-1")
        //        {
        //            docObjects.DocRefID = Convert.ToInt32(ddl_RefDocName.SelectedValue);
        //            docObjects.documentNUmber = "";
        //            docObjects.action = 1;
        //        }
        //        else
        //        {
        //            docObjects.documentNUmber = txt_RefDocNum.Text.Trim();
        //            docObjects.DocRefID = 0;
        //            docObjects.action = 0;
        //        }
        //        docObjects.DMS_CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
        //        docObjects.DMSDocControllerID = Convert.ToInt32(ddl_Approver.SelectedValue);
        //        docObjects.Content = Doc_data;
        //        docObjects.Status = 1;
        //        docObjects.FormVersionID = Convert.ToInt32(ViewState["PKID"].ToString());
        //        if (Convert.ToInt32(ViewState["PKID"].ToString()) > 0)
        //        {
        //            // update
        //            docObjects.FormVersionID = Convert.ToInt32(ViewState["PKID"].ToString());
        //            int DMS_ins = DMS_Bal.DMS_FormUpdation(docObjects);
        //            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "New Form with Form Number " + txt_FormNumber.Text + " Saved Successfully", "success", "ReloadRevertedPage()");
        //        }
        //        else
        //        {
        //            // insert
        //            int DMS_ins = DMS_Bal.DMS_FormInsertion(docObjects);
        //            ViewState["PKID"] = DMS_ins;
        //            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "New Form with Form Number " + txt_FormNumber.Text + " Saved Successfully", "success", "ReloadInitiationPage()");
        //        }
        //        //if (btn_Save.Value == "Submit")
        //        //{

        //        //    int DMS_ins = DMS_Bal.DMS_FormInsertion(docObjects);
        //        //    ViewState["PKID"] = DMS_ins;
        //        //    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "New Form with Form Number " + txt_FormNumber.Text + " Saved Successfully", "success", "ReloadInitiationPage()");
        //        //}
        //        //else if (btn_Save.Value == "Update")
        //        //{
        //        //    docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
        //        //    int DMS_ins = DMS_Bal.DMS_FormUpdation(docObjects);
        //        //    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "New Form with Form Number " + txt_FormNumber.Text + " Saved Successfully", "success", "ReloadRevertedPage()");
        //        //}

        //    }
        //    catch (Exception ex)
        //    {
        //        string strline = HelpClass.LineNo(ex);
        //        string strMsg = HelpClass.SQLEscapeString(ex.Message);
        //        Aizant_log.Error("FC_M22:" + strline + "  " + strMsg);
        //        HelpClass.custAlertMsg(this, this.GetType(), "FC_M22:" + strline + "  " + "Form Submission failed", "error");
        //    }
        //}
        protected void ConfirmCustomAlert_buttonClick(object sender, EventArgs e)
        {
            try
            {
                if (hdnConfirm.Value == "Save")
                {
                    StringBuilder sbErrorMsg = new StringBuilder();
                    sbErrorMsg.Clear();
                    byte[] Doc_data;
                    Doc_data = ASPxRichEdit2.SaveCopy(DevExpress.XtraRichEdit.DocumentFormat.Rtf);
                    int Form_length = Doc_data.Length;
                    if (ddl_DeptName.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append(" Select Department" + "<br/>");
                    }
                    if (txt_FormNumber.Text.Trim() == "")
                    {
                        sbErrorMsg.Append(" Enter Form Number" + "<br/>");
                    }
                    if (Request.QueryString.Count == 0)
                    {
                        if (lbl_number.Visible == true)
                        {
                            if (lbl_number.Text.Trim() == "Form Number Already Taken")
                            {
                                sbErrorMsg.Append("Form Number Already exists Please Enter another Number" + "<br/>");
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(txt_FormNumber.Text.Trim()))
                            {
                                docObjects.FormNumber = txt_FormNumber.Text.Trim();
                                DataTable DMS_dt = DMS_Bal.DMS_FormNumberSearch(docObjects);
                                if (DMS_dt.Rows.Count > 0)
                                {
                                    sbErrorMsg.Append("Form Number Already exists Please Enter another Number" + "<br/>");
                                }
                            }
                        }
                    }
                    if (txt_Version.Text.Trim() == "")
                    {
                        sbErrorMsg.Append(" Enter Version Number" + "<br/>");
                    }
                    if (txt_FormName.Text.Trim() == "")
                    {
                        sbErrorMsg.Append(" Enter Form Name" + "<br/>");
                    }
                    if (Request.QueryString.Count == 0)
                    {
                        if (lblStatus.Visible == true)
                        {
                            if (lblStatus.Text.Trim() == "Form Name Already Taken")
                            {
                                sbErrorMsg.Append("Form Name Already exists Please Enter another Name" + "<br/>");
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(txt_FormName.Text.Trim()))
                            {

                                docObjects.FormName = txt_FormName.Text.Trim();
                                docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName.SelectedValue);
                                DataTable DMS_dt = DMS_Bal.DMS_FormNameSearch(docObjects);
                                if (DMS_dt.Rows.Count > 0)
                                {
                                    sbErrorMsg.Append("Form Name Already exists Please Enter another Name" + "<br/>");
                                }
                            }
                        }
                    }
                    if (ddl_Approver.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append(" Select a Form Approver" + "<br/>");
                    }
                    if (Form_length < 28300)
                    {
                        sbErrorMsg.Append(" Form is Incomplete, add more data" + "<br/>");
                    }
                    if (sbErrorMsg.Length > 8)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
                        return;
                    }
                    docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName.SelectedValue);
                    docObjects.FormNumber = txt_FormNumber.Text.Trim();
                    docObjects.Version = Convert.ToInt32(txt_Version.Text.Trim());
                    docObjects.FormName = txt_FormName.Text.Trim();
                    docObjects.Purpose = txt_Description.Text.Trim();
                    if (ddl_DocumentType.SelectedValue != "-1")
                    {
                        docObjects.DocRefID = Convert.ToInt32(ddl_RefDocName.SelectedValue);
                        docObjects.documentNUmber = "";
                        docObjects.action = 1;
                    }
                    else
                    {
                        docObjects.documentNUmber = txt_RefDocNum.Text.Trim();
                        docObjects.DocRefID = 0;
                        docObjects.action = 0;
                    }
                    docObjects.DMS_CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    docObjects.DMSDocControllerID = Convert.ToInt32(ddl_Approver.SelectedValue);
                    docObjects.Content = Doc_data;
                    docObjects.Status = 1;
                    docObjects.FormVersionID = Convert.ToInt32(ViewState["PKID"].ToString());
                    if (Convert.ToInt32(ViewState["PKID"].ToString()) > 0)
                    {
                        // update
                        docObjects.FormVersionID = Convert.ToInt32(ViewState["PKID"].ToString());
                        int DMS_ins = DMS_Bal.DMS_FormUpdation(docObjects);
                        HelpClass.custAlertMsg(this, this.GetType(), "New Form with Form Number " + txt_FormNumber.Text + " Saved Successfully", "success");
                    }
                    else
                    {
                        // insert
                        int DMS_ins = DMS_Bal.DMS_FormInsertion(docObjects);
                        ViewState["PKID"] = DMS_ins;
                        HelpClass.custAlertMsg(this, this.GetType(), "New Form with Form Number " + txt_FormNumber.Text + " Saved Successfully", "success");
                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop2", "showFormContent();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FC_M22:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FC_M22:" + strline + "  " + "Form Submission failed", "error");
            }
        }
        //protected void ConfirmCustomAlert_buttonClick(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (hdnConfirm.Value == "Next")
        //        {
        //            StringBuilder sbErrorMsg = new StringBuilder();
        //            sbErrorMsg.Clear();
        //            if (ddl_DeptName.SelectedValue.ToString().Trim() == "0")
        //            {
        //                sbErrorMsg.Append(" Select Department" + "<br/>");
        //            }
        //            if (txt_FormNumber.Text.Trim() == "")
        //            {
        //                sbErrorMsg.Append(" Enter Form Number" + "<br/>");
        //            }
        //            if (Request.QueryString.Count == 0)
        //            {
        //                if (lbl_number.Visible == true)
        //                {
        //                    if (lbl_number.Text.Trim() == "Form Number Already Taken")
        //                    {
        //                        sbErrorMsg.Append("Form Number Already exists Please Enter another Number" + "<br/>");
        //                    }
        //                }
        //                else
        //                {
        //                    if (!string.IsNullOrEmpty(txt_FormNumber.Text.Trim()))
        //                    {
        //                        docObjects.FormNumber = txt_FormNumber.Text.Trim();
        //                        DataTable DMS_dt = DMS_Bal.DMS_FormNumberSearch(docObjects);
        //                        if (DMS_dt.Rows.Count > 0)
        //                        {
        //                            sbErrorMsg.Append("Form Number Already exists Please Enter another Number" + "<br/>");
        //                        }
        //                    }
        //                }
        //            }
        //            if (txt_Version.Text.Trim() == "")
        //            {
        //                sbErrorMsg.Append(" Enter Version Number" + "<br/>");
        //            }
        //            else
        //            {
        //                try
        //                {
        //                    if (Convert.ToInt32(txt_Version.Text.Trim()) < 0)
        //                    {
        //                        sbErrorMsg.Append("Version Number Starts From 0" + "<br/>");
        //                    }
        //                    if (txt_Version.Text.Trim().ToLower().Contains("e"))
        //                    {
        //                        sbErrorMsg.Append("Version Number Should be Integer" + "<br/>");
        //                    }
        //                }
        //                catch
        //                {
        //                }
        //            }
        //            if (txt_FormName.Text.Trim() == "")
        //            {
        //                sbErrorMsg.Append(" Enter Form Name" + "<br/>");
        //            }
        //            if (Request.QueryString.Count == 0)
        //            {
        //                if (lblStatus.Visible == true)
        //                {
        //                    if (lblStatus.Text.Trim() == "Form Name Already Taken")
        //                    {
        //                        sbErrorMsg.Append("Form Name Already exists Please Enter another Name" + "<br/>");
        //                    }
        //                }
        //                else
        //                {
        //                    if (!string.IsNullOrEmpty(txt_FormName.Text.Trim()))
        //                    {
        //                        docObjects.FormName = txt_FormName.Text.Trim();
        //                        docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName.SelectedValue);
        //                        DataTable DMS_dt = DMS_Bal.DMS_FormNameSearch(docObjects);
        //                        if (DMS_dt.Rows.Count > 0)
        //                        {
        //                            sbErrorMsg.Append("Form Name Already exists Please Enter another Name" + "<br/>");
        //                        }
        //                    }
        //                }
        //            }
        //            //if (txt_Description.Text.Trim() == "")
        //            //{
        //            //    sbErrorMsg.Append(" Enter Form Comments" + "<br/>");
        //            //}
        //            if (ddl_Approver.SelectedValue.ToString().Trim() == "0")
        //            {
        //                sbErrorMsg.Append(" Select a Form Approver" + "<br/>");
        //            }
        //            if (sbErrorMsg.Length > 8)
        //            {
        //                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop2", "showFormDetails();", true);
        //                return;
        //            }
        //            else
        //            {
        //                if (Request.QueryString.Count == 0)
        //                {
        //                    RichEditDocumentServer server = new RichEditDocumentServer();
        //                    server.LoadDocument(Server.MapPath(@"~/Files/FormDetailsTeplate.docx"), DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
        //                    Document document = server.Document;
        //                    Section firstSection = document.Sections[0];
        //                    SubDocument myHeader = firstSection.BeginUpdateHeader(HeaderFooterType.Primary);
        //                    DocumentRange[] deptrange = myHeader.FindAll("formdept1", SearchOptions.CaseSensitive, myHeader.Range);
        //                    myHeader.InsertText(deptrange[0].Start, ddl_DeptName.SelectedItem.Text.Trim());
        //                    myHeader.Delete(deptrange[0]);
        //                    DocumentRange[] refsoprange = myHeader.FindAll("refsopnum1", SearchOptions.CaseSensitive, myHeader.Range);
        //                    if (ddl_DocumentType.SelectedValue != "-1" && ddl_RefDocName.SelectedValue != "0")
        //                    {
        //                        myHeader.InsertText(refsoprange[0].Start, ddl_RefDocName.SelectedItem.Text.Trim());
        //                        myHeader.Delete(refsoprange[0]);
        //                    }
        //                    else if (ddl_DocumentType.SelectedValue == "-1" && txt_RefDocNum.Text.Trim() != "")
        //                    {
        //                        myHeader.InsertText(refsoprange[0].Start, txt_RefDocNum.Text.Trim());
        //                        myHeader.Delete(refsoprange[0]);
        //                    }
        //                    else
        //                    {
        //                        myHeader.InsertText(refsoprange[0].Start, "NA");
        //                        myHeader.Delete(refsoprange[0]);
        //                    }
        //                    DocumentRange[] formnamerange = myHeader.FindAll("formname1", SearchOptions.CaseSensitive, myHeader.Range);
        //                    myHeader.InsertText(formnamerange[0].Start, txt_FormName.Text.Trim());
        //                    myHeader.Delete(formnamerange[0]);
        //                    DocumentRange[] formnumrange = myHeader.FindAll("formnum1", SearchOptions.CaseSensitive, myHeader.Range);
        //                    if (Convert.ToInt32(txt_Version.Text.Trim()) <= 9)
        //                    {
        //                        if (Convert.ToInt32(txt_Version.Text.Trim()) == 0)
        //                        {
        //                            myHeader.InsertText(formnumrange[0].Start, txt_FormNumber.Text.Trim() + "-00");
        //                            myHeader.Delete(formnumrange[0]);
        //                        }
        //                        else
        //                        {
        //                            myHeader.InsertText(formnumrange[0].Start, txt_FormNumber.Text.Trim() + "-0" + txt_Version.Text.Trim());
        //                            myHeader.Delete(formnumrange[0]);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        myHeader.InsertText(formnumrange[0].Start, txt_FormNumber.Text.Trim() + "-" + txt_Version.Text.Trim());
        //                        myHeader.Delete(formnumrange[0]);
        //                    }
        //                    firstSection.EndUpdateHeader(myHeader);
        //                    string strFileUpload = DynamicFolder.CreateDynamicFolder(5);
        //                    string File1Path = DateTime.Now.ToString("yyyyMMddHHmmss") + ".docx";
        //                    string strTargPath = (strFileUpload + "\\" + File1Path);
        //                    document.SaveDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
        //                    server.Dispose();
        //                    //Marshal.ReleaseComObject(server);

        //                    ASPxRichEdit2.DocumentId = Guid.NewGuid().ToString();
        //                    ASPxRichEdit2.Open(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
        //                    DocumentManager.CloseDocument(Guid.NewGuid().ToString());

        //                }
        //                else
        //                {
        //                    if (Request.QueryString["from"] == "c" || Request.QueryString["from"] == "p")
        //                    {
        //                        docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
        //                        DataTable dt = DMS_Bal.DMS_GetFormDetails(docObjects);
        //                        DataTable DMS_DT = DMS_Bal.DMS_GetFormContent(docObjects);
        //                        byte[] docBytes = (byte[])DMS_DT.Rows[0]["FormContent"];
        //                        string strFileUpload = DynamicFolder.CreateDynamicFolder(5);
        //                        string File1Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".rtf";
        //                        string strTargPath = (strFileUpload + "\\" + File1Path);
        //                        File.WriteAllBytes(strTargPath, docBytes);
        //                        RichEditDocumentServer server = new RichEditDocumentServer();
        //                        server.LoadDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
        //                        Document document = server.Document;
        //                        Section firstSection = document.Sections[0];
        //                        SubDocument myHeader = firstSection.BeginUpdateHeader(HeaderFooterType.Primary);
        //                        DocumentRange[] deptrange = myHeader.FindAll(dt.Rows[0]["DeptName"].ToString(), SearchOptions.CaseSensitive, myHeader.Range);
        //                        myHeader.InsertText(deptrange[0].Start, ddl_DeptName.SelectedItem.Text.Trim());
        //                        myHeader.Delete(deptrange[0]);
        //                        if (dt.Rows[0]["Ref_DocNumber"].ToString() != "")
        //                        {
        //                            DocumentRange[] refsoprange = myHeader.FindAll(dt.Rows[0]["Ref_DocNumber"].ToString(), SearchOptions.CaseSensitive, myHeader.Range);
        //                            if (ddl_DocumentType.SelectedValue != "-1" && ddl_RefDocName.SelectedValue != "0")
        //                            {
        //                                myHeader.InsertText(refsoprange[0].Start, ddl_RefDocName.SelectedItem.Text.Trim());
        //                                myHeader.Delete(refsoprange[0]);
        //                            }
        //                            else if (ddl_DocumentType.SelectedValue == "-1" && txt_RefDocNum.Text.Trim() != "")
        //                            {
        //                                myHeader.InsertText(refsoprange[0].Start, txt_RefDocNum.Text.Trim());
        //                                myHeader.Delete(refsoprange[0]);
        //                            }
        //                            else
        //                            {
        //                                myHeader.InsertText(refsoprange[0].Start, "NA");
        //                                myHeader.Delete(refsoprange[0]);
        //                            }
        //                        }
        //                        else if (dt.Rows[0]["OtherDocumentNum"].ToString() != "")
        //                        {
        //                            DocumentRange[] refsoprange = myHeader.FindAll(dt.Rows[0]["OtherDocumentNum"].ToString(), SearchOptions.CaseSensitive, myHeader.Range);
        //                            if (ddl_DocumentType.SelectedValue != "-1" && ddl_RefDocName.SelectedValue != "0")
        //                            {
        //                                myHeader.InsertText(refsoprange[0].Start, ddl_RefDocName.SelectedItem.Text.Trim());
        //                                myHeader.Delete(refsoprange[0]);
        //                            }
        //                            else if (ddl_DocumentType.SelectedValue == "-1" && txt_RefDocNum.Text.Trim() != "")
        //                            {
        //                                myHeader.InsertText(refsoprange[0].Start, txt_RefDocNum.Text.Trim());
        //                                myHeader.Delete(refsoprange[0]);
        //                            }
        //                            else
        //                            {
        //                                myHeader.InsertText(refsoprange[0].Start, "NA");
        //                                myHeader.Delete(refsoprange[0]);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            DocumentRange[] refsoprange = myHeader.FindAll("NA", SearchOptions.CaseSensitive, myHeader.Range);
        //                            if (ddl_DocumentType.SelectedValue != "-1" && ddl_RefDocName.SelectedValue != "0")
        //                            {
        //                                myHeader.InsertText(refsoprange[0].Start, ddl_RefDocName.SelectedItem.Text.Trim());
        //                                myHeader.Delete(refsoprange[0]);
        //                            }
        //                            else if (ddl_DocumentType.SelectedValue == "-1" && txt_RefDocNum.Text.Trim() != "")
        //                            {
        //                                myHeader.InsertText(refsoprange[0].Start, txt_RefDocNum.Text.Trim());
        //                                myHeader.Delete(refsoprange[0]);
        //                            }
        //                            else
        //                            {
        //                                myHeader.InsertText(refsoprange[0].Start, "NA");
        //                                myHeader.Delete(refsoprange[0]);
        //                            }
        //                        }
        //                        DocumentRange[] formnamerange = myHeader.FindAll(dt.Rows[0]["FormName"].ToString(), SearchOptions.CaseSensitive, myHeader.Range);
        //                        myHeader.InsertText(formnamerange[0].Start, txt_FormName.Text.Trim());
        //                        myHeader.Delete(formnamerange[0]);
        //                        DocumentRange[] formnumrange = myHeader.FindAll(dt.Rows[0]["FormNumber"].ToString() + "-" + dt.Rows[0]["FormVersionNumber"].ToString(), SearchOptions.CaseSensitive, myHeader.Range);
        //                        myHeader.InsertText(formnumrange[0].Start, txt_FormNumber.Text.Trim() + "-" + txt_Version.Text.Trim());
        //                        myHeader.Delete(formnumrange[0]);
        //                        firstSection.EndUpdateHeader(myHeader);
        //                        document.SaveDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
        //                        server.Dispose();
        //                        //Marshal.ReleaseComObject(server);

        //                        ASPxRichEdit2.DocumentId = Guid.NewGuid().ToString();
        //                        ASPxRichEdit2.Open(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
        //                        DocumentManager.CloseDocument(Guid.NewGuid().ToString());
        //                    }
        //                    else if (Request.QueryString["from"] == "r")
        //                    {
        //                        docObjects.FormVersionID = Convert.ToInt32(Request.QueryString["FormVersionID"].ToString());
        //                        DataTable dt = DMS_Bal.DMS_GetFormDetails(docObjects);
        //                        DataTable DMS_DT = DMS_Bal.DMS_GetFormContent(docObjects);
        //                        byte[] docBytes = (byte[])DMS_DT.Rows[0]["FormContent"];
        //                        string strFileUpload = DynamicFolder.CreateDynamicFolder(5);
        //                        string File1Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".rtf";
        //                        string strTargPath = (strFileUpload + "\\" + File1Path);
        //                        File.WriteAllBytes(strTargPath, docBytes);
        //                        RichEditDocumentServer server = new RichEditDocumentServer();
        //                        server.LoadDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
        //                        Document document = server.Document;
        //                        Section firstSection = document.Sections[0];
        //                        SubDocument myHeader = firstSection.BeginUpdateHeader(HeaderFooterType.Primary);
        //                        //DocumentRange[] deptrange = myHeader.FindAll(dt.Rows[0]["DeptName"].ToString(), SearchOptions.CaseSensitive, myHeader.Range);
        //                        //myHeader.InsertText(deptrange[0].Start, ddl_DeptName.SelectedItem.Text.Trim());
        //                        //myHeader.Delete(deptrange[0]);
        //                        //if (dt.Rows[0]["Ref_DocNumber"].ToString() != "")
        //                        //{
        //                        //    DocumentRange[] refsoprange = myHeader.FindAll(dt.Rows[0]["Ref_DocNumber"].ToString(), SearchOptions.CaseSensitive, myHeader.Range);
        //                        //    if (ddl_DocumentType.SelectedValue != "-1" && ddl_RefDocName.SelectedValue != "0")
        //                        //    {
        //                        //        myHeader.InsertText(refsoprange[0].Start, ddl_RefDocName.SelectedItem.Text.Trim());
        //                        //        myHeader.Delete(refsoprange[0]);
        //                        //    }
        //                        //    else if (ddl_DocumentType.SelectedValue == "-1" && txt_RefDocNum.Text.Trim() != "")
        //                        //    {
        //                        //        myHeader.InsertText(refsoprange[0].Start, txt_RefDocNum.Text.Trim());
        //                        //        myHeader.Delete(refsoprange[0]);
        //                        //    }
        //                        //    else
        //                        //    {
        //                        //        myHeader.InsertText(refsoprange[0].Start, "NA");
        //                        //        myHeader.Delete(refsoprange[0]);
        //                        //    }
        //                        //}
        //                        //else if (dt.Rows[0]["OtherDocumentNum"].ToString() != "")
        //                        //{
        //                        //    DocumentRange[] refsoprange = myHeader.FindAll(dt.Rows[0]["OtherDocumentNum"].ToString(), SearchOptions.CaseSensitive, myHeader.Range);
        //                        //    if (ddl_DocumentType.SelectedValue != "-1" && ddl_RefDocName.SelectedValue != "0")
        //                        //    {
        //                        //        myHeader.InsertText(refsoprange[0].Start, ddl_RefDocName.SelectedItem.Text.Trim());
        //                        //        myHeader.Delete(refsoprange[0]);
        //                        //    }
        //                        //    else if (ddl_DocumentType.SelectedValue == "-1" && txt_RefDocNum.Text.Trim() != "")
        //                        //    {
        //                        //        myHeader.InsertText(refsoprange[0].Start, txt_RefDocNum.Text.Trim());
        //                        //        myHeader.Delete(refsoprange[0]);
        //                        //    }
        //                        //    else
        //                        //    {
        //                        //        myHeader.InsertText(refsoprange[0].Start, "NA");
        //                        //        myHeader.Delete(refsoprange[0]);
        //                        //    }
        //                        //}
        //                        //else
        //                        //{
        //                        //    DocumentRange[] refsoprange = myHeader.FindAll("NA", SearchOptions.CaseSensitive, myHeader.Range);
        //                        //    if (ddl_DocumentType.SelectedValue != "-1" && ddl_RefDocName.SelectedValue != "0")
        //                        //    {
        //                        //        myHeader.InsertText(refsoprange[0].Start, ddl_RefDocName.SelectedItem.Text.Trim());
        //                        //        myHeader.Delete(refsoprange[0]);
        //                        //    }
        //                        //    else if (ddl_DocumentType.SelectedValue == "-1" && txt_RefDocNum.Text.Trim() != "")
        //                        //    {
        //                        //        myHeader.InsertText(refsoprange[0].Start, txt_RefDocNum.Text.Trim());
        //                        //        myHeader.Delete(refsoprange[0]);
        //                        //    }
        //                        //    else
        //                        //    {
        //                        //        myHeader.InsertText(refsoprange[0].Start, "NA");
        //                        //        myHeader.Delete(refsoprange[0]);
        //                        //    }
        //                        //}
        //                        //DocumentRange[] formnamerange = myHeader.FindAll(dt.Rows[0]["FormName"].ToString(), SearchOptions.CaseSensitive, myHeader.Range);
        //                        //myHeader.InsertText(formnamerange[0].Start, txt_FormName.Text.Trim());
        //                        //myHeader.Delete(formnamerange[0]);
        //                        DocumentRange[] formnumrange = myHeader.FindAll(dt.Rows[0]["FormNumber"].ToString() + "-" + dt.Rows[0]["FormVersionNumber"].ToString(), SearchOptions.CaseSensitive, myHeader.Range);
        //                        myHeader.InsertText(formnumrange[0].Start, txt_FormNumber.Text.Trim() + "-" + txt_Version.Text.Trim());
        //                        myHeader.Delete(formnumrange[0]);
        //                        firstSection.EndUpdateHeader(myHeader);
        //                        document.SaveDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
        //                        server.Dispose();
        //                        //Marshal.ReleaseComObject(server);

        //                        ASPxRichEdit2.DocumentId = Guid.NewGuid().ToString();
        //                        ASPxRichEdit2.Open(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
        //                        DocumentManager.CloseDocument(Guid.NewGuid().ToString());
        //                    }
        //                }
        //                btnNext.Visible = false;
        //                btnCancel.Visible = false;
        //                btnReset.Visible = false;
        //                btnFormNumcheck.Visible = false;
        //                btnFormNamecheck.Visible = false;
        //                DisableFormDetails();
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop2", "showFormContent();", true);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string strline = HelpClass.LineNo(ex);
        //        string strMsg = HelpClass.SQLEscapeString(ex.Message);
        //        Aizant_log.Error("DI_M4:" + strline + "  " + strMsg);
        //        HelpClass.custAlertMsg(this, this.GetType(), "DI_M4:" + strline + "  " + "Document submit/update failed", "error");
        //    }
        //}
        private string GetRefdocNo()
        {
            string strRefNo = "NA";
            if (ddl_RefDocName.SelectedValue != "0")
            {
                string[] strRefNames = ddl_RefDocName.SelectedItem.Text.Trim().Split('-');
                strRefNo = strRefNames[0];
            }
            return strRefNo;
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
        
    }
}