﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.DMS.DMSAdmin
{
    public partial class FormReqManageList : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        DocPrintRequest DocReqObjects = new DocPrintRequest();
        DocObjects docObjects = new DocObjects();
        UMS_BAL objUMS_BAL;
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        ViewState["ReqStatusID"] = "";
                        hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=2").Length > 0)//2-DMSAdmin
                            {
                                if (Request.QueryString["Notification_ID"] != null)
                                {
                                    objUMS_BAL = new UMS_BAL();
                                    int[] empIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, empIDs); //2 represents NotifyEmp had visited the page through notification link.
                                    DataTable dt = DMS_Bal.DMS_NotificationFormRequestID(Convert.ToInt32(Request.QueryString["Notification_ID"]));
                                    hdfVID.Value = dt.Rows[0]["FormRequestID"].ToString();
                                    hdfPkID.Value = dt.Rows[0]["FormVersionID"].ToString();
                                    btnRequestControl_Click(null, null);
                                }
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                    else
                    {
                        ElectronicSign.buttonClick += new EventHandler(ElectronicSign_buttonClick);
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRCL_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRCL_M1:" + strline + "  " + "Document Load failed.", "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        protected void btnRequestControl_Click(object sender, EventArgs e)
        {
            btnRequestControl.Enabled = false;
            try
            {
                StringBuilder sbWarningMsg = new StringBuilder();
                docObjects.FormReqID = Convert.ToInt32(hdfVID.Value);
                DataSet DMS_DT = DMS_Bal.DMS_GetFormPrintReqDetails(docObjects);
                docObjects.FormVersionID = Convert.ToInt32(DMS_DT.Tables[0].Rows[0]["FormVersionID"].ToString());
                DataTable DMS_DT1 = DMS_Bal.DMS_GetFormDetails(docObjects);
                hdfDeptID.Value = DMS_DT1.Rows[0]["DeptID"].ToString();
                //hdfReqStatusID.Value = DMS_DT.Rows[0]["RequestStatusID"].ToString();
                DMS_GetReviewerList();
                DMS_GetControllerList();
                if (DMS_DT.Tables[0].Rows[0]["RequestStatusID"].ToString() == "2" || DMS_DT.Tables[0].Rows[0]["RequestStatusID"].ToString() == "3")
                {
                    ddl_ReviewedBy.Enabled = false;
                }
                txt_RequestedBy.Text = DMS_DT.Tables[0].Rows[0]["RequestBy"].ToString();
                txt_ProjectNumber.Text = string.IsNullOrEmpty(DMS_DT.Tables[0].Rows[0]["ProjectNumber"].ToString()) ? "N/A" : DMS_DT.Tables[0].Rows[0]["ProjectNumber"].ToString();
                txt_NoofCopies.Text = DMS_DT.Tables[0].Rows[0]["NooFCopies"].ToString();
                txt_DocumentName.Text = DMS_DT.Tables[0].Rows[0]["FormName"].ToString();
                txt_Purpose.Text = DMS_DT.Tables[0].Rows[0]["RequestPurpose"].ToString();
                txt_FormNumber.Text = DMS_DT.Tables[0].Rows[0]["FormNumber"].ToString();
                txt_VersionNumber.Text = DMS_DT.Tables[0].Rows[0]["FormVersionNumber"].ToString();
                foreach (ListItem _LiAuth in ddl_ReviewedBy.Items)
                {
                    if (_LiAuth.Value == DMS_DT.Tables[0].Rows[0]["ReviewerID"].ToString())
                    {
                        ddl_ReviewedBy.SelectedValue = DMS_DT.Tables[0].Rows[0]["ReviewerID"].ToString();
                        break;
                    }
                }

                if (ddl_ReviewedBy.SelectedValue.ToString().Trim() == "0" && DMS_DT.Tables[0].Rows[0]["ReviewerID"].ToString() != "")
                {
                    sbWarningMsg.Append("Assigned Request Reviewer - <b>" + DMS_DT.Tables[0].Rows[0]["ReviewedBy"].ToString() + "</b> <br>is either InActive or does not have access to department." + "<br/>");
                }
                foreach (ListItem _LiAuth in ddl_DocController.Items)
                {
                    if (_LiAuth.Value == DMS_DT.Tables[0].Rows[0]["DocControlerID"].ToString())
                    {
                        ddl_DocController.SelectedValue = DMS_DT.Tables[0].Rows[0]["DocControlerID"].ToString();
                        break;
                    }
                }

                if (ddl_DocController.SelectedValue.ToString().Trim() == "0" && DMS_DT.Tables[0].Rows[0]["DocControlerID"].ToString() != "")
                {
                    sbWarningMsg.Append("Assigned Document Controller - <b>" + DMS_DT.Tables[0].Rows[0]["DocController"].ToString() + "</b> is InActive." + "<br/>");
                }
                if (sbWarningMsg.Length > 8)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbWarningMsg.ToString() + "','warning');", true);
                }
                //ddl_ReviewedBy.SelectedValue = DMS_DT.Tables[0].Rows[0]["ReviewerID"].ToString();
                //ddl_DocController.SelectedValue = DMS_DT.Tables[0].Rows[0]["DocControlerID"].ToString();
                txt_Comments.Value = "";
                UpGridButtons.Update();
                ViewState["ReqStatusID"] = DMS_DT.Tables[0].Rows[0]["RequestStatusID"].ToString();
                span_FileTitle.InnerText = "Request Number-" + DMS_DT.Tables[0].Rows[0]["RequestNo"].ToString();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_request').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRCL_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRCL_M4:" + strline + "  " + "Form Request modal loading failed.", "error");
            }
            finally
            {
                btnRequestControl.Enabled = true;
            }
        }
        public void DMS_GetReviewerList()
        {
            try
            {
                UMS_BAL objUMS_Bal = new UMS_BAL();
                DataTable DMS_Dt = objUMS_Bal.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(hdfDeptID.Value), (int)DMS_UserRole.PrintReviewer_DMS, true);
                if (DMS_Dt.Rows.Count > 0)
                {
                    DataRow[] filtered = DMS_Dt.Select("EmpID NOT IN ('" + (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString() + "')");
                    if (filtered.Any())
                    {
                        DMS_Dt = filtered.CopyToDataTable();
                        ddl_ReviewedBy.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                        ddl_ReviewedBy.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                        ddl_ReviewedBy.DataSource = DMS_Dt;
                        ddl_ReviewedBy.DataBind();
                        ddl_ReviewedBy.Items.Insert(0, new System.Web.UI.WebControls.ListItem("-- Select Request Reviewer --", "0"));
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>   There are no employees with print reviewer role for the document department.','warning');", true);
                    //literalSop1.Text = "There are no employees with print reviewer role.";
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DRCL_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DRCL_M2:" + strline + "  " + "Reviewer drop-down loading failed.", "error");
            }
        }
        public void DMS_GetControllerList()
        {
            try
            {
                DataTable DMS_Dt = DMS_Bal.DMS_GetControllersList();
                if (DMS_Dt.Rows.Count > 0)
                {
                    if (ddl_ReviewedBy.SelectedValue != "")
                    {
                        DataRow[] filtered = DMS_Dt.Select("EmpID NOT IN ('" + (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString() + "','" + ddl_ReviewedBy.SelectedValue + "')");
                        if (filtered.Any())
                        {
                            DMS_Dt = filtered.CopyToDataTable();
                            ddl_DocController.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                            ddl_DocController.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                            ddl_DocController.DataSource = DMS_Dt;
                            ddl_DocController.DataBind();
                            ddl_DocController.Items.Insert(0, new System.Web.UI.WebControls.ListItem("-- Select Document Controller --", "0"));
                        }
                    }
                    else
                    {
                        DataRow[] filtered = DMS_Dt.Select("EmpID NOT IN ('" + (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString() + "')");
                        if (filtered.Any())
                        {
                            DMS_Dt = filtered.CopyToDataTable();
                            ddl_DocController.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                            ddl_DocController.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                            ddl_DocController.DataSource = DMS_Dt;
                            ddl_DocController.DataBind();
                            ddl_DocController.Items.Insert(0, new System.Web.UI.WebControls.ListItem("-- Select Document Controller --", "0"));
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>   There are no employees with controller role.','warning');", true);
                    //literalSop1.Text = "There are no employees with controller role.";
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DRCL_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DRCL_M2:" + strline + "  " + "Controller drop-down loading failed.", "error");
            }
        }
        protected void btnCommentsHistoryView_Click(object sender, EventArgs e)
        {
            btnCommentsHistoryView.Enabled = false;
            try
            {
                docObjects.FormReqID = Convert.ToInt32(hdfVID.Value);
                DataTable DMS_DT = DMS_Bal.DMS_GetFormPrintReqHistory(docObjects);
                gv_CommentHistory.DataSource = DMS_DT;
                ViewState["HistoryDataTable"] = DMS_DT;
                gv_CommentHistory.DataBind();
                UpGridButtons.Update();
                span4.InnerHtml = DMS_DT.Rows[0]["FormReqTitle"].ToString();
                upcomment.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myCommentHistory').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRCL_M5:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRCL_M5:" + strline + "  " + "Form Request History loading failed.", "error");
            }
            finally
            {
                btnCommentsHistoryView.Enabled = true;
            }
        }
        protected void gv_CommentHistory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string _fullcomments = (e.Row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox).Text;
                    LinkButton _lnkmorecomments = (e.Row.Cells[3].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (e.Row.Cells[3].FindControl("lbcommentsless") as LinkButton);
                    if (_fullcomments.Length > 70)
                    {
                        (e.Row.Cells[3].FindControl("lbl_shortComments") as Label).Visible = true;
                        System.Web.UI.WebControls.TextBox FullTbx = (e.Row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox);
                        (e.Row.Cells[3].FindControl("lbl_shortComments") as Label).Text = _fullcomments.Substring(0, 70) + "..";
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = true;

                        if (_fullcomments.Length > 100 && _fullcomments.Length <= 200)
                        {
                            FullTbx.Rows = 3;
                        }
                        else if (_fullcomments.Length > 200 && _fullcomments.Length <= 300)
                        {
                            FullTbx.Rows = 4;
                        }
                        FullTbx.Visible = false;
                    }
                    else
                    {
                        (e.Row.Cells[3].FindControl("lbl_shortComments") as Label).Visible = true;
                        (e.Row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox).Visible = false;
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRCL_M6:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRCL_M6:" + strline + "  " + "Comment History row databound failed.", "error");
            }
        }
        protected void gv_CommentHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "SM")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    LinkButton _lnkmorecomments = (row.Cells[3].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (row.Cells[3].FindControl("lbcommentsless") as LinkButton);
                    (row.Cells[3].FindControl("lbl_shortComments") as Label).Visible = false;
                    (row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox).Visible = true;
                    _lnklesscomments.Visible = true;
                    _lnkmorecomments.Visible = false;
                }
                else if (e.CommandName == "SL")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    LinkButton _lnkmorecomments = (row.Cells[3].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (row.Cells[3].FindControl("lbcommentsless") as LinkButton);
                    (row.Cells[3].FindControl("lbl_shortComments") as Label).Visible = true;
                    (row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox).Visible = false;
                    _lnklesscomments.Visible = false;
                    _lnkmorecomments.Visible = true;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRCL_M7:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRCL_M7:" + strline + "  " + "Comment History row command failed.", "error");
            }
        }
        protected void ElectronicSign_buttonClick(object sender, EventArgs e)
        {
            try
            {
                bool b = ElectronicSign.IsPasswordValid;
                if (b == true)
                {
                    if (hdnAction.Value == "Update")
                    {
                        Update();
                    }
                    else if (hdnAction.Value == "Reject")
                    {
                        Reject();
                    }
                }
                if (b == false)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Password is Incorrect!", "error");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRCL_M8:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRCL_M8:" + strline + "  " + "Electronic Signature validation failed.", "error");
            }
        }
        public void Update()
        {
            try
            {
                StringBuilder sbErrorMsg = new StringBuilder();
                sbErrorMsg.Clear();
                if (ViewState["ReqStatusID"].ToString() == "1")
                {
                    if (ddl_ReviewedBy.SelectedValue.Trim() == "0")
                    {
                        sbErrorMsg.Append(" Select Request Reviewer." + "<br/>");
                    }
                }
                if (ddl_DocController.SelectedValue.Trim() == "0")
                {
                    sbErrorMsg.Append(" Select Document Controller." + "<br/>");
                }
                if (txt_Comments.Value.Trim() == "")
                {
                    sbErrorMsg.Append(" Enter Comments." + "<br/>");
                }
                if (sbErrorMsg.Length > 8)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
                    return;
                }
                docObjects.pkid = Convert.ToInt32(hdfVID.Value);
                docObjects.DMS_ModifiedBy = Convert.ToInt32(hdnEmpID.Value);
                if (ddl_ReviewedBy.SelectedValue == "")
                {
                    docObjects.DMSReviewedByID = 0;
                }
                else
                {
                    docObjects.DMSReviewedByID = Convert.ToInt32(ddl_ReviewedBy.SelectedValue);
                }
                docObjects.DMSDocControllerID = Convert.ToInt32(ddl_DocController.SelectedValue);
                docObjects.Remarks = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                int DMS_AP = DMS_Bal.DMS_UpdateFormPrintRequestUsers(docObjects);
                docObjects.FormReqID = Convert.ToInt32(hdfVID.Value);
                DataSet DMS_DT = DMS_Bal.DMS_GetFormPrintReqDetails(docObjects);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Form Request Number <b>" + DMS_DT.Tables[0].Rows[0]["RequestNo"].ToString() + "</b> Updated by Admin Successfully.", "success", "reloadtable();");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRCL_M9:" + strline + "  " + strMsg);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                HelpClass.custAlertMsg(this, this.GetType(), "FRCL_M9:" + strline + "  " + "Form Request Updation failed.", "error");
            }
        }
        public void Reject()
        {
            try
            {
                StringBuilder sbErrorMsg = new StringBuilder();
                sbErrorMsg.Clear();
                if (txt_Comments.Value.Trim() == "")
                {
                    sbErrorMsg.Append(" Enter Comments." + "<br/>");
                }
                if (sbErrorMsg.Length > 8)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
                    return;
                }
                DocReqObjects.DocReqID = Convert.ToInt32(hdfVID.Value);
                DocReqObjects.ReviewedByID = Convert.ToInt32(hdnEmpID.Value);
                DocReqObjects.Comments = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                int DMS_AP = DMS_BalDM.DMS_FormPrintReqReject(DocReqObjects);
                docObjects.FormReqID = Convert.ToInt32(hdfVID.Value);
                DataSet DMS_DT = DMS_Bal.DMS_GetFormPrintReqDetails(docObjects);
                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Form Request Number <b>" + DMS_DT.Tables[0].Rows[0]["RequestNo"].ToString() + "</b> Rejected by Admin Successfully.", "success", "reloadtable();");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRCL_M10:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRCL_M10:" + strline + "  " + "Form Request Reject failed.", "error");
            }
        }
        protected void btn_View_Click(object sender, EventArgs e)
        {
            btn_View.Enabled = false;
            try
            {
                docObjects.FormReqID = Convert.ToInt32(hdfVID.Value);
                DataSet DMS_DT1 = DMS_Bal.DMS_GetFormPrintReqDetails(docObjects);
                DocObjects docObjects1 = new DocObjects();
                docObjects1.FormVersionID = Convert.ToInt32(DMS_DT1.Tables[0].Rows[0]["FormVersionID"]);
                hdfPkID.Value = DMS_DT1.Tables[0].Rows[0]["FormVersionID"].ToString();
                hdfViewType.Value = "3";
                hdfViewEmpID.Value = "0";
                hdfViewRoleID.Value = "0";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view1", "ViewForm();", true);

                DataTable DMS_DT = DMS_Bal.DMS_GetFormHistory(docObjects1);
                span1.Attributes.Add("title", DMS_DT.Rows[0]["FormTitle"].ToString());
                if (DMS_DT.Rows[0]["FormTitle"].ToString().Length > 60)
                {
                    span1.InnerHtml = DMS_DT.Rows[0]["FormTitle"].ToString().Substring(0, 60) + "...";
                }
                else
                {
                    span1.InnerHtml = DMS_DT.Rows[0]["FormTitle"].ToString();
                }
                UpHeading.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRCL_M11:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRCL_M11:" + strline + "  " + "Form viewing failed.", "error");
            }
            finally
            {
                btn_View.Enabled = true;
            }
        }
        protected void gv_CommentHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_CommentHistory.PageIndex = e.NewPageIndex;
                gv_CommentHistory.DataSource = (DataTable)(ViewState["HistoryDataTable"]);
                gv_CommentHistory.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRCL_M11:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRCL_M11:" + strline + "  " + "grid view page index changing failed.", "error");
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}