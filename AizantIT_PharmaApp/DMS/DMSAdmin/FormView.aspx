﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormView.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.FormView" %>

<%@ Register Assembly="DevExpress.Web.ASPxRichEdit.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRichEdit" TagPrefix="dx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script>
        function RestrictMinus(e) {
            if (e.keyCode === 45 || e.keyCode === 109 || e.keyCode === 189) {
                return false;
            }
            else {
                return true;
            }
        };
        function restrictAlphabets(e) {
            var x = e.which || e.keycode;
            if ((x >= 48 && x <= 57) || x == 8 ||
                (x >= 35 && x <= 40))
                return true;
            else
                return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="new-password">
        <link href="<%=ResolveUrl("~/AppCSS/Bootstrap/css/bootstrap4.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/AppCSS/dms_styles/dms.css")%>" rel= "stylesheet" />
        <link href="<%=ResolveUrl("~/AppCSS/fontscss/fonts_segoe_gly.css")%>" rel= "stylesheet" />
         <link href="<%=ResolveUrl("~/AppCSS/CustAlerts/SingleCustAlert.css")%>" rel= "stylesheet" />
       <script src="<%=ResolveUrl("~/Scripts/jquery-3.2.1.js")%>"></script>
        <script src="<%=ResolveUrl("~/Scripts/popper.js")%>"></script>
        <link href="<%=ResolveUrl("~/Content/bootstrap-select.css")%>" rel="stylesheet" />
        <script src="<%=ResolveUrl("~/AppCSS/Bootstrap/scripts/bootstrap4.min.js")%>"></script>
         <script src="<%=ResolveUrl("~/Scripts/bootstrap-select.min.js")%>"></script>
       <%-- <link href="<%=ResolveUrl("~/AppCSS/DMS/style.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/AppCSS/DMS/DMS_StyleSheet1.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/AppCSS/Main_style.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/AppCSS/Editor/site.css")%>" rel="stylesheet" />
        <link href="<%=ResolveUrl("~/AppCSS/CustAlerts/custAlert.css")%>" rel="stylesheet" />--%>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="col-12 top bottom text-right">
            <asp:Button ID="Btn_Request" runat="server" Text="Request to Print" CssClass="  btn-signup_popup" OnClick="Btn_Request_Click" Visible="false" Style="z-index: 99" />
        </div>
        <div id="divliteral" runat="server" visible="false">
            <asp:Literal ID="literalSop1" runat="server"></asp:Literal>
        </div>
        <div id="divPDF_Viewer">
            <noscript>
                                                Please Enable JavaScript.
                                            </noscript>
        </div>
        <!-------- Request VIEW-------------->
        <div id="myModal_request" class="modal department fade" role="dialog">
            <div class="modal-dialog modal-lg" style="min-width: 98%">
                <!-- Modal content-->
                <div class="modal-content col-lg-12 padding-none">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div class="modal-header">
                                
                                <span id="span_FileTitle" runat="server">Request to Print</span>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <div id="div_RequestedBy" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group padding-right_div float-left">
                                    <asp:Label ID="lblRequestedBy" runat="server" Text="Requested By" CssClass="label-style"></asp:Label>
                                    <asp:TextBox ID="txt_RequestedBy" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Requested By" ReadOnly="true" MaxLength="100"></asp:TextBox>
                                </div>
                                <div id="div_NoofCopies" class="col-md-6 col-lg-2 col-12 col-sm-12 form-group float-left">
                                    <asp:Label ID="lblNoofCopies" runat="server" Text="No.of Copies" CssClass="label-style"></asp:Label><span class="smallred_label">*</span>
                                    <asp:TextBox ID="txt_NoofCopies" runat="server" CssClass="form-control login_input_sign_up" TextMode="Number" onkeypress="return restrictAlphabets(event);" min="0" max="999" MaxLength="3" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"></asp:TextBox>
                                </div>
                                <div id="div_Project" class="col-md-6 col-lg-4 col-12 col-sm-12 form-group padding-left_div float-left">
                                    <asp:Label ID="Label1" runat="server" Text="Project Number" CssClass="label-style"></asp:Label>
                                    <asp:TextBox ID="txt_ProjectNumber" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Project Number" MaxLength="50"></asp:TextBox>
                                </div>
                                <div id="div_formNo" class="col-md-6 col-lg-6 col-12 col-sm-12 padding-right_div form-group float-left">
                                    <asp:Label ID="lblFormNum" runat="server" Text="Form Number" CssClass="label-style"></asp:Label>
                                    <asp:TextBox ID="txt_FormNumber" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true" ></asp:TextBox>
                                </div>
                                <div id="div_versionno" class="col-md-6 col-lg-3 col-12 col-sm-12 form-group float-left ">
                                    <asp:Label ID="lblVersionNum" runat="server" Text="Version Number" CssClass="label-style"></asp:Label>
                                    <asp:TextBox ID="txt_VersionNumber" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true" ></asp:TextBox>
                                </div>
                                <div id="div_DocName" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group padding-right_div float-left">
                                    <asp:Label ID="Label4" runat="server" Text="Form Name" CssClass="label-style"></asp:Label>
                                    <asp:TextBox ID="txt_DocumentName" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="Enter Form Name" Rows="2" MaxLength="300" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div id="div_Purpose" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group padding-left_div float-left">
                                    <asp:Label ID="lblComments" runat="server" Text="Purpose" CssClass="label-style"></asp:Label><span class="smallred_label">*</span>
                                    <textarea id="txt_Purpose" runat="server" class="form-control" placeholder="Enter Purpose" maxlength="300"></textarea>
                                </div>
                                <div id="div_ReviewedBy" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group padding-right_div float-left">
                                    <asp:Label ID="Label6" runat="server" Text="Request Reviewer" CssClass="label-style"></asp:Label><span class="smallred_label">*</span>
                                    <asp:DropDownList ID="ddl_ReviewedBy" data-size="9" runat="server" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left selectpicker1 drop_down form-control regulatory_dropdown_style" data-live-search="true" placeholder="Select Request Reviewer" AutoPostBack="true" OnSelectedIndexChanged="ddl_ReviewedBy_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                                <div id="div_DocController" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group padding-left_div float-left">
                                    <asp:Label ID="Label7" runat="server" Text="Document Controller" CssClass="label-style"></asp:Label><span class="smallred_label">*</span>
                                    <asp:DropDownList ID="ddl_DocController" data-size="9" runat="server" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 padding-none selectpicker1 drop_down form-control regulatory_dropdown_style" data-live-search="true" placeholder="Select Document Controller" Enabled="false"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="modal-footer">
                                <asp:Label ID="lblerror" runat="server"></asp:Label>
                                <asp:Button ID="btn_Submit" Text="Submit" CssClass=" btn-signup_popup" runat="server" ValidationGroup="DMS_DC" OnClick="btn_Submit_Click" />
                                <asp:Button ID="btn_Reset" Text="Reset" CssClass=" btn-revert_popup" runat="server" CausesValidation="false" OnClick="btn_Reset_Click" />
                                <asp:Button ID="btn_Cancel" Text="Cancel" data-dismiss="modal" CssClass=" btn-cancel_popup" runat="server" CausesValidation="false" OnClick="btn_Cancel_Click" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <!-------- End Request VIEW-------------->

       <!---Error popup--->
       <div id="ModalError" class="modal ModalDanger error_popup" role="dialog" style="">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content modalMainContent">
                    <div class="modal-header ModalHeaderPart">
                        <div class="error_icon float-left">                           
                            <h4 class="modal-title float-left popup_title">Error</h4>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="col-lg-12 padding-none">
                            <asp:UpdatePanel ID="upError" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <p id="msgError" class="col-lg-12" style="padding: 10px 22px; max-height: 300px; overflow-x: auto; font-family: arial; color: #f95a5a; font-weight: bold;" runat="server"></p>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                   <div class="modal-footer ModalFooterPart">
                        <button type="button" id="btnErrorOk" class="btn-cancel_popup" style="margin-top: 10px !important" data-dismiss="modal">Ok</button>
                    </div>                      
                </div>
            </div>
        </div>
        <!-- End Error Popup-->

         <!-- Success content-->
        <div id="ModalSuccess" class="modal ModalSucess success_popup" role="dialog" style="z-index: 9999">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content modalMainContent ">
                    <div class="modal-header ModalHeaderPart">
                        <div class="success_icon float-left">
                            
                            <h4 class="modal-title popup_title float-left">Success</h4>
                        </div>

                    </div>
                    <div class="modal-body" style="padding: 11px 0px;">
                        <asp:UpdatePanel ID="Upsucsess" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="col-lg-12 padding-none">
                                    <p id="msgSuccess" class="col-lg-12" style="padding: 0px 22px; max-height: 128px; overflow-x: auto;" runat="server"></p>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                       
                    </div>
                     <div class="modal-footer ModalFooterPart">
                        <button type="button" id="btnSuccessOk" class="btn-signup_popup" data-dismiss="modal" style="margin-top: 10px !important" runat="server">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Success content-->

        <!-- Information content-->
        <div id="ModalInfo" class="modal info_popup ModalConfirm" role="dialog" style="z-index: 9999">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content modalMainContent">
                    <div class="modal-header ModalHeaderPart">
                        <div class="success_icon float-left">
                            <h4 class="modal-title popup_title float-left">Information</h4>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="col-lg-12">
                            <p id="msgInfo" class="col-lg-12 " style="padding: 0px 22px; max-height: 128px; overflow-x: auto;" runat="server"></p>
                        </div>
                    </div>
                    <div class="modal-footer ModalFooterPart">
                        <button type="button" id="btnInfoOk" class="btn-info_popup" data-dismiss="modal" style="margin-top: 10px !important" runat="server">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Information content-->

        <asp:HiddenField ID="hdfVID" runat="server" />
        <asp:HiddenField ID="hdfDocNum" runat="server" />
        <asp:HiddenField ID="hdfEmpName" runat="server" />
        <asp:HiddenField ID="hdfDeptID" runat="server" />
        <asp:HiddenField ID="hdfViewType" runat="server" />
        <asp:HiddenField ID="hdfViewEmpID" runat="server" />
        <asp:HiddenField ID="hdfViewRoleID" runat="server" />
        <asp:HiddenField ID="hdfViewDivID" runat="server" />
        <script src='<%= Page.ResolveUrl("~/AppScripts/PDF_Viewer.min.js")%>'></script>
         <script>
             $(function () {
                 $(".selectpicker1").selectpicker();
             });
             </script>
        <script>
            function ViewForm() {
                PdfViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>', $('#<%=hdfViewType.ClientID%>').val(), $('#<%=hdfVID.ClientID%>').val(), $('#<%=hdfViewEmpID.ClientID%>').val(), $('#<%=hdfViewRoleID.ClientID%>').val(), "#divPDF_Viewer");
            }
            function CloseBrowser() {
                window.close();
            }
        </script>
    </form>
</body>
</html>
