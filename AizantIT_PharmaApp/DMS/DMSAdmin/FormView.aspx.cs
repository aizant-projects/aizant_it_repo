﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.DMS
{
    public partial class FormView : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        DocObjects docObjects = new DocObjects();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        if (Request.QueryString.Count > 0)
                        {
                            string vid = Request.QueryString[0];
                            hdfVID.Value = vid;
                            if (Request.QueryString["reader"] != null)
                            {
                                Btn_Request.Visible = true;
                                hdfEmpName.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString();
                                msgSuccess.InnerHtml = "New Form Request has been Initiated Successfully.";
                            }
                            ToViewForm(vid);
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Uclose", "CloseBrowser();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FV_M1:" + strline + "  " + strMsg);
                string strError = "FV_M1:" + strline + "  " + "Page loading failed.";
                divliteral.Visible = true;
                literalSop1.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        private void ToViewForm(string vid)
        {
            try
            {
                docObjects.FormVersionID = Convert.ToInt32(vid);
                DataTable DMS_DT = DMS_Bal.DMS_GetFormDetails(docObjects);
                hdfDocNum.Value = DMS_DT.Rows[0]["FormNumber"].ToString();
                hdfDeptID.Value = DMS_DT.Rows[0]["DeptID"].ToString();                
                DataTable dt = DMS_Bal.DMS_GetFormContent(docObjects);
                divliteral.Visible = false;
                if (dt.Rows[0]["FormContent"].ToString().Length > 0)
                {
                    hdfVID.Value = vid;
                    hdfViewType.Value = "3";
                    hdfViewEmpID.Value = "0";
                    hdfViewRoleID.Value = "0";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view1", "ViewForm();", true);
                }
                else
                {
                    divliteral.Visible = true;
                    literalSop1.Text = @" <b style=""color:red;"">" + "Form Not Available To View." + "</b> ";
                }
            }
            catch (Exception)
            {
                divliteral.Visible = true;
                literalSop1.Text = "Form Not Available To View, Bad data.";
            }
        }
        public void DMS_GetReviewerList()
        {
            try
            {
                UMS_BAL objUMS_Bal = new UMS_BAL();
                DataTable DMS_Dt = objUMS_Bal.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(hdfDeptID.Value), (int)DMS_UserRole.PrintReviewer_DMS, true);
                if (DMS_Dt.Rows.Count > 0)
                {
                    DataRow[] filtered = DMS_Dt.Select("EmpID NOT IN ('" + (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString() + "')");
                    if (filtered.Any())
                    {
                        DMS_Dt = filtered.CopyToDataTable();
                        ddl_ReviewedBy.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                        ddl_ReviewedBy.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                        ddl_ReviewedBy.DataSource = DMS_Dt;
                        ddl_ReviewedBy.DataBind();
                        ddl_ReviewedBy.Items.Insert(0, new ListItem("-- Select Request Reviewer --", "0"));
                    }
                }
                else
                {
                    msgError.InnerHtml = "There are no employees with print reviewer role for the form department.";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#ModalError').modal({ backdrop: 'static', keyboard: false });", true);
                    upError.Update();
                    //literalSop1.Text = "There are no employees with print reviewer role.";
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FV_M2:" + strline + "  " + strMsg);
                string strError = "FV_M2:" + strline + "  " + "Request Reviewer list loading failed.";
                divliteral.Visible = true;
                literalSop1.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        public void DMS_GetControllerList()
        {
            try
            {
                DataTable DMS_Dt = DMS_Bal.DMS_GetControllersList();
                if (DMS_Dt.Rows.Count > 0)
                {
                    DataRow[] filtered = DMS_Dt.Select("EmpID NOT IN ('" + (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString() + "','" + ddl_ReviewedBy.SelectedValue + "')");
                    if (filtered.Any())
                    {
                        DMS_Dt = filtered.CopyToDataTable();
                        ddl_DocController.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                        ddl_DocController.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                        ddl_DocController.DataSource = DMS_Dt;
                        ddl_DocController.DataBind();
                        ddl_DocController.Items.Insert(0, new ListItem("-- Select Document Controller --", "0"));
                    }
                }
                else
                {
                    msgError.InnerHtml = "There are no employees with controller role.";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#ModalError').modal({ backdrop: 'static', keyboard: false });", true);
                    upError.Update();
                    //literalSop1.Text = "There are no employees with controller role.";
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FV_M3:" + strline + "  " + strMsg);
                string strError = "FV_M3:" + strline + "  " + "Controller list loading failed.";
                divliteral.Visible = true;
                literalSop1.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        protected void Btn_Request_Click(object sender, EventArgs e)
        {
            Btn_Request.Enabled = false;
            try
            {
                DataTable dt = DMS_Bal.DMS_LatestActiveForm(Convert.ToInt32(hdfVID.Value));
                DMS_GetReviewerList();
                if (hdfVID.Value == dt.Rows[0]["FormVersionID"].ToString())
                {
                    int PendingFormRequestsByUserCount = DMS_Bal.DMS_PendingFormRequestsByUserCount(Convert.ToInt32(hdfVID.Value), Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString()));
                    if (PendingFormRequestsByUserCount == 0)
                    {
                        docObjects.FormVersionID = Convert.ToInt32(hdfVID.Value);
                        DataTable DMS_DT = DMS_Bal.DMS_GetFormDetails(docObjects);
                        txt_DocumentName.Text = DMS_DT.Rows[0]["FormName"].ToString();
                        //span_FileTitle.InnerHtml = "Form Number-" + hdfDocNum.Value;
                        txt_RequestedBy.Text = hdfEmpName.Value;
                        txt_FormNumber.Text= DMS_DT.Rows[0]["FormNumber"].ToString();
                        txt_VersionNumber.Text= DMS_DT.Rows[0]["FormVersionNumber"].ToString();
                        DMS_Reset();
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_request').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                        hdfViewType.Value = "3";
                        hdfViewEmpID.Value = "0";
                        hdfViewRoleID.Value = "0";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view2", "ViewForm();", true);
                    }
                    else
                    {
                        docObjects.FormVersionID = Convert.ToInt32(hdfVID.Value);
                        DataTable DMS_DT = DMS_Bal.DMS_GetFormDetails(docObjects);
                        txt_DocumentName.Text = DMS_DT.Rows[0]["FormName"].ToString();
                        //span_FileTitle.InnerHtml = "Form Number-" + hdfDocNum.Value;
                        txt_RequestedBy.Text = hdfEmpName.Value;
                        txt_FormNumber.Text = DMS_DT.Rows[0]["FormNumber"].ToString();
                        txt_VersionNumber.Text = DMS_DT.Rows[0]["FormVersionNumber"].ToString();
                        DMS_Reset();
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_request').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                        msgInfo.InnerHtml = "Previous Form Print Requests are not yet returned, Please return them!";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "$('#ModalInfo').modal({ backdrop: 'static', keyboard: false });", true);
                        hdfViewType.Value = "3";
                        hdfViewEmpID.Value = "0";
                        hdfViewRoleID.Value = "0";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view2", "ViewForm();", true);
                        upError.Update();
                        return;
                    }
                }
                else
                {
                    msgError.InnerHtml = "The form with this version is not being used anymore";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#ModalError').modal({ backdrop: 'static', keyboard: false });", true);
                    hdfViewType.Value = "3";
                    hdfViewEmpID.Value = "0";
                    hdfViewRoleID.Value = "0";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view2", "ViewForm();", true);
                    upError.Update();
                    return;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FV_M4:" + strline + "  " + strMsg);
                string strError = "FV_M4:" + strline + "  " + "Request Pop up loading failed.";
                divliteral.Visible = true;
                literalSop1.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
            finally
            {
                Btn_Request.Enabled = true;
            }
        }
        public void DMS_Reset()
        {
            try
            {
                ddl_DocController.Enabled = false;
                ddl_DocController.SelectedIndex = -1;
                ddl_ReviewedBy.SelectedIndex = -1;
                txt_Purpose.Value = "";
                txt_NoofCopies.Text = "";
                txt_ProjectNumber.Text = "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btn_Reset_Click(object sender, EventArgs e)
        {
            btn_Reset.Enabled = false;
            try
            {
                DMS_Reset();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "select1", "$('.selectpicker1').selectpicker();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FV_M5:" + strline + "  " + strMsg);
                string strError = "FV_M5:" + strline + "  " + "Fields Reset failed.";
                divliteral.Visible = true;
                literalSop1.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
            finally
            {
                btn_Reset.Enabled = true;
            }
        }
        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            btn_Submit.Enabled = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "select1", "$('.selectpicker1').selectpicker();", true);
            try
            {
                StringBuilder sbErrorMsg = new StringBuilder();
                sbErrorMsg.Clear();
                if (txt_NoofCopies.Text.Trim() == "")
                {
                    sbErrorMsg.Append("Enter Number Of Copies." + "<br/>");
                }
                else if (Convert.ToInt32(txt_NoofCopies.Text.Trim()) <= 0)
                {
                    sbErrorMsg.Append("Number Of Copies must be greater than zero." + "<br/>");
                }
                if (txt_Purpose.Value.Trim() == "")
                {
                    sbErrorMsg.Append("Enter Purpose." + "<br/>");
                }
                if (ddl_ReviewedBy.SelectedValue.ToString().Trim() == "0")
                {
                    sbErrorMsg.Append("Select Request Reviewer." + "<br/>");
                }
                if (ddl_DocController.SelectedValue.ToString().Trim() == "0" || ddl_DocController.SelectedValue.ToString().Trim() == "")
                {
                    sbErrorMsg.Append("Select Document Controller." + "<br/>");
                }
                if (sbErrorMsg.Length > 8)
                {
                    msgError.InnerHtml = sbErrorMsg.ToString();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#ModalError').modal({ backdrop: 'static', keyboard: false });", true);
                    upError.Update();
                    return;
                }
                DataTable dt = DMS_Bal.DMS_LatestActiveForm(Convert.ToInt32(hdfVID.Value));
                if (hdfVID.Value == dt.Rows[0]["FormVersionID"].ToString())
                {
                    docObjects.pkid = Convert.ToInt32(hdfVID.Value);
                    docObjects.Purpose = Regex.Replace(txt_Purpose.Value.Trim(), @"\s+", " ");
                    docObjects.NoofCopies = Convert.ToInt32(txt_NoofCopies.Text.Trim());
                    docObjects.ProjectNumber = Regex.Replace(txt_ProjectNumber.Text.Trim(), @"\s+", " ");
                    docObjects.DMSRequestByID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    docObjects.DMSReviewedByID = Convert.ToInt32(ddl_ReviewedBy.SelectedValue);
                    if (ddl_DocController.SelectedValue.ToString().Trim() == "0")
                    {
                        docObjects.DMSDocControllerID = 0;
                    }
                    else
                    {
                        docObjects.DMSDocControllerID = Convert.ToInt32(ddl_DocController.SelectedValue);
                    }
                    int DMS_ins = DMS_Bal.DMS_FormPrintRequestSubmission(docObjects);
                    msgSuccess.InnerHtml = "New Form Request with no. <b>" + DMS_ins.ToString() + "</b> has been Initiated Successfully.";
                    Upsucsess.Update();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "$('#ModalSuccess').modal({ backdrop: 'static', keyboard: false });", true);
                    DMS_Reset();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop3", "$('#myModal_request').modal('hide');", true);
                }
                else
                {
                    msgError.InnerHtml = "The form with this version is not being used anymore";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#ModalError').modal({ backdrop: 'static', keyboard: false });", true);
                    hdfViewType.Value = "3";
                    hdfViewEmpID.Value = "0";
                    hdfViewRoleID.Value = "0";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view2", "ViewForm();", true);
                    upError.Update();
                    return;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FV_M6:" + strline + "  " + strMsg);
                string strError = "FV_M6:" + strline + "  " + "Form Request submission failed.";
                divliteral.Visible = true;
                literalSop1.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
            finally
            {
                btn_Submit.Enabled = true;
            }
        }
        protected void btn_Cancel_Click(object sender, EventArgs e)
        {
            btn_Cancel.Enabled = false;
            try
            {
                Response.Redirect("~/DMS/FormPrintReqMainList.aspx", false);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FV_M7:" + strline + "  " + strMsg);
                string strError = "FV_M7:" + strline + "  " + "Form Request Cancel failed.";
                divliteral.Visible = true;
                literalSop1.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
            finally
            {
                btn_Cancel.Enabled = true;
            }
        }
        protected void ddl_ReviewedBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddl_DocController.Enabled = true;
                DMS_GetControllerList();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "select1", "$('.selectpicker1').selectpicker();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FV_M8:" + strline + "  " + strMsg);
                string strError = "FV_M8:" + strline + "  " + "request reviewer dropdown selected index changed failed.";
                divliteral.Visible = true;
                literalSop1.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
        
    }
}