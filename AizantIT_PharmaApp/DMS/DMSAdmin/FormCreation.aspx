﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="FormCreation.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.FormCreation" %>

<%@ Register Assembly="DevExpress.Web.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxRichEdit.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRichEdit" TagPrefix="dx" %>
<%@ Register Src="~/UserControls/ElectronicSign.ascx" TagPrefix="uc1" TagName="ElectronicSign" %>
<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .dxreView {
            float: right;
            overflow: auto;
            padding: 0 10px;
            -webkit-overflow-scrolling: touch;
            -webkit-appearance: none;
            -ms-touch-action: pan-y pinch-zoom;
            touch-action: pan-y pinch-zoom;
            width: 100%;
        }

        .dxreControl {
            height: 850px !important;
        }
    </style>
    <script>

        $('#divFContent').hide();
        function showFormDetails() {
            $('#divFDetails').show();
            $('#divFContent').hide();
            $('#btnFormDetails').hide();
            var dt = $('#<%=ddl_DocumentType.ClientID %> option:selected').text();
            if (dt == 'Others') {
                showRefDocNumTextBox();
            }
            else {
                showRefDocNumDropdown();
            }
            //var FormDetails = document.getElementById("divFDetails");
            //var FormContent = document.getElementById("divFContent");
            //var FormDetailsbutton = document.getElementById("btnFormDetails");
            //FormDetails.style.display = "block";
            //FormContent.style.display = "none";
            //FormDetailsbutton.style.display = "none";
        }
        function showFormContent() {
            $('#divFDetails').hide();
            $('#divFContent').show();
            //var FormDetails = document.getElementById("divFDetails");
            //var FormContent = document.getElementById("divFContent");
            //FormDetails.style.display = "none";
            //FormContent.style.display = "block";
            showFormDetailsbutton();
        }
        function showFormDetailsClick() {
            var x = document.getElementById("btnFormDetails");
            if (x.value === "Show Details") {
                x.value = "Hide Details";
                showForm();
            } else {
                x.value = "Show Details";
                showFormContent();
            }
        }
        function showRefDocNumTextBox() {
            $('.divRefDocNumddl').hide();
            $('.divRefDocNumText').show();
            //var RefDocNumddl = document.getElementById("divRefDocNumddl");
            //var RefDocNumText = document.getElementById("divRefDocNumText");
            //RefDocNumddl.style.display = "none";
            //RefDocNumText.style.display = "block";
        }
        function showRefDocNumDropdown() {
            $('.divRefDocNumddl').show();
            $('.divRefDocNumText').hide();
            //var RefDocNumddl = document.getElementById("divRefDocNumddl");
            //var RefDocNumText = document.getElementById("divRefDocNumText");
            //RefDocNumddl.style.display = "block";
            //RefDocNumText.style.display = "none";
        }
        function showForm() {
            $('#divFDetails').show();
            $('#divFContent').show();
            <%--$('#<%=btnEdit.ClientID %>').css("display", "block");--%>
            var dt = $('#<%=ddl_DocumentType.ClientID %> option:selected').text();
            if (dt == 'Others') {
                showRefDocNumTextBox();
            }
            else {
                showRefDocNumDropdown();
            }
            //var FormDetails = document.getElementById("divFDetails");
            //var FormContent = document.getElementById("divFContent");
            //FormDetails.style.display = "block";
            //FormContent.style.display = "block";
        }
        function showFormDetailsbutton() {
            $('#btnFormDetails').show();
            $('#btnFormDetails').prop('value', 'Show Details');
            //var FormDetailsbutton = document.getElementById("btnFormDetails");
            //FormDetailsbutton.style.display = "block";
            //FormDetailsbutton.value = "Show Details";
        }
        function showStatusChangeDiv() {
            // $('#divStatuschange').show();
            //$('#divStatuschange').attr('style', 'display:block');
            //$('#divStatuschange').css("display", "");
            //$('#divStatuschange').attr.('style', 'display:block');
            $("#divStatuschange").css({ display: "block" });
        }
        function hideEditButton() {
            $('#<%=btnEdit.ClientID %>').css("display", "none");
            }
    </script>

    <asp:ScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ScriptManager>
    <div class="padding-none">
        <asp:Button ID="btnAdd" runat="server" class="pull-right btn btn-approve_popup" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
        <input id="btnFormDetails" type="button" class="pull-right btn btn-approve_popup" onclick="showFormDetailsClick();" style="display: none" />
    </div>
    <div class=" col-md-12 col-lg-12 col-xs-12 col-sm-12 dms_outer_border">
        <div class="col-lg-12 col-lg-12 col-xs-12 col-md-12 col-sm-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div class=" col-xs-12  padding-none pull-right">
                <div class="panel-heading panel-heading_title" id="div2" runat="server">
                </div>
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" id="divFDetails">
                    <div class="panel-heading header_acc">
                        Form Details
                    </div>
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none  grid_panel_hod">
                        <div class="form-group col-md-6 col-lg-6 col-xs-6 col-sm-6 top">
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none">
                                <label class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none">Department<span class="smallred">*</span></label>
                                <asp:DropDownList ID="ddl_DeptName" runat="server" data-size="9" data-live-search="true" CssClass="form-control col-md-12 col-lg-12 col-xs-12 col-sm-12 selectpicker drop_down" Style="width: 100%;" TabIndex="1" OnSelectedIndexChanged="ddl_DeptName_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group col-sm-3 top">
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none">
                                <label>Form Number<span class="smallred">*</span></label>
                                <asp:Button ID="btnFormNumcheck" CssClass="check_button pull-right" runat="server" Text="Check!" OnClick="btnFormNumcheck_Click" ToolTip="Form Number Availability" />
                            </div>
                            <asp:TextBox ID="txt_FormNumber" runat="server" CssClass="form-control" placeholder="Enter Form Number" TabIndex="2" autocomplete="off" MaxLength="20"></asp:TextBox>
                            <asp:Label ID="lbl_number" runat="server" CssClass="label_status" Visible="false"></asp:Label>
                        </div>
                        <div class="form-group col-sm-3 top">
                            <label class="padding-none col-sm-12 control-label custom_label_answer">Version Number<span class="smallred">*</span></label>
                            <div class="col-sm-12 padding-none">
                                <asp:TextBox ID="txt_Version" runat="server" CssClass="form-control" placeholder="Enter Version Number" TextMode="Number" onkeypress="return RestrictMinus(event);" TabIndex="3" min="0"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-sm-6">

                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none">
                                <label>Form Name<span class="smallred">*</span></label>
                                <asp:Button ID="btnFormNamecheck" CssClass="check_button pull-right" runat="server" Text="Check!" OnClick="btnFormNamecheck_Click" ToolTip="Form Name Availability" />
                            </div>
                            <asp:TextBox ID="txt_FormName" runat="server" Rows="2" placeholder="Enter Form Name" CssClass="form-control" TextMode="MultiLine" TabIndex="4" MaxLength="300"></asp:TextBox>
                            <asp:Label ID="lblStatus" runat="server" CssClass="label_status" Visible="false"></asp:Label>
                        </div>

                        <div class="form-group col-md-6 col-lg-6 col-xs-6 col-sm-6">
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none">
                                <label class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none">Description</label>
                                <asp:TextBox ID="txt_Description" runat="server" Rows="2" placeholder="Enter Form Description" CssClass="form-control" TextMode="MultiLine" TabIndex="5" MaxLength="300"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none">
                            <div class="form-group col-md-3 col-lg-3 col-xs-6 col-sm-6 " id="Approver" runat="server">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none">
                                    <label class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none">Approver<span class="smallred">*</span></label>
                                    <asp:DropDownList ID="ddl_Approver" runat="server" CssClass="form-control col-md-12 col-lg-12 col-xs-12 col-sm-12 SearchDropDown selectpicker" Style="width: 100%;" TabIndex="6"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group col-md-3 col-lg-3 col-xs-6 col-sm-6">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none">
                                    <label class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none">Document Type</label>
                                    <asp:DropDownList ID="ddl_DocumentType" runat="server" data-size="9" data-live-search="true" Style="width: 100%;" CssClass="form-control col-md-12 col-lg-12 col-xs-12 col-sm-12 selectpicker drop_down" TabIndex="7" OnSelectedIndexChanged="ddl_DocumentType_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none">
                                    <label class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none">Reference Document Number</label>
                                    <asp:UpdatePanel runat="server" ID="upRefDocNum" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddl_RefDocName" runat="server" data-size="9" data-live-search="true" Style="width: 100%;" CssClass="form-control col-md-12 col-lg-12 col-xs-12 col-sm-12 selectpicker drop_down divRefDocNumddl" TabIndex="8"></asp:DropDownList>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddl_DeptName" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="ddl_DocumentType" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:TextBox ID="txt_RefDocNum" runat="server" Rows="2" placeholder="Enter Reference Document Number" CssClass="form-control divRefDocNumText" TextMode="MultiLine" TabIndex="8" MaxLength="300" Style="display: none;"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" id="divStatuschange" style="display: none">
                            <%-- <div class="form-group col-lg-3 padding-none">
                                  <asp:Label ID="lblRadiobutton" runat="server" Text="Status:"></asp:Label><span class="smallred">*</span>
                                      </div>
                                <div class="form-group col-md-6 col-lg-3 col-xs-6 col-sm-6">
                                    <asp:RadioButtonList runat="server" ID="RadioButtonList1" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="Active" Value="yes"></asp:ListItem>
                                                <asp:ListItem Text="InActive" Value="no"></asp:ListItem>
                                            </asp:RadioButtonList>
                                </div>--%>
                            <div class="btn-group padding-right_div col-sm-12 col-xs-12 col-lg-6 col-md-12 bottom" data-toggle="buttons" id="divOpinion" runat="server">
                                <label for="inputPassword3" class=" col-lg-12 col-sm-12 col-xs-12 col-md-12 padding-none control-label">Status<span class="smallred">*</span></label>
                                <label class="btn btn-primary_radio col-xs-6 col-lg-6 col-md-6 col-sm-6 active" id="lblRadioTrainingYes">
                                    <asp:RadioButton ID="rbtnActive" value="Yes" runat="server" GroupName="A" Text="Active" Checked="true" />
                                </label>
                                <label class="btn btn-primary_radio col-xs-6 col-lg-6 col-md-6 col-sm-6" id="lblRadioTrainingNo">
                                    <asp:RadioButton ID="rbtnInActive" value="No" runat="server" GroupName="A" Text="InActive" />
                                </label>
                            </div>
                            <div class="form-group col-md-6 col-lg-6 col-xs-6 col-sm-6 padding-left_div">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none">
                                    <label class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none">Comments<span class="smallred" id="spanstatuschangecomment" runat="server">*</span></label>
                                    <asp:TextBox ID="txt_statuschangecomments" runat="server" Rows="2" placeholder="Enter comments" CssClass="form-control" TextMode="MultiLine" TabIndex="2" MaxLength="300"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-xs-12 col-sm-12 top">
                            <asp:Button ID="btnCancel" Text="Cancel" Class="pull-right btn btn-canceldms_popup" runat="server" TabIndex="9" OnClick="btn_Cancel_Click" />
                            <asp:Button ID="btnReset" Text="Reset" Class="pull-right btn btn-revert_popup" runat="server" TabIndex="10" OnClick="btn_Reset_Click" />
                            <asp:Button ID="btnNext" Text="Next" Class="pull-right btn btn-approve_popup" runat="server" TabIndex="11" OnClick="btnNext_Click" />
                            <%--<input type="button" id="btnNext" runat="server" class="pull-right btn btn-approve_popup" onclick="RequriedFieldsNextClick();" value="Next" TabIndex="11"/>--%>
                            <asp:Button ID="btnEdit" Text="Edit" Class="pull-right btn btn-track_edit" runat="server" TabIndex="11" OnClick="btnEdit_Click" Style="display: none" />
                            <%--<asp:Button ID="btnSubmit" Text="Withdraw" Class="pull-right btn btn-approve_popup" runat="server" TabIndex="11" OnClick="btnSubmit_Click" Visible="false"/>--%>
                            <input type="button" id="btnSubmit" runat="server" class="pull-right btn btn-approve_popup" onclick="return commentRequriedWithdrawInitiate();" value="Withdraw" visible="false" tabindex="16" />
                            <input type="button" id="btnReject" runat="server" class="pull-right btn btn-revert_popup" onclick="return commentRequriedWithdrawReject();" value="Reject" visible="false" tabindex="18" />
                            <input type="button" id="btnRevert" runat="server" class="pull-right btn btn-revert_popup" onclick="return commentRequriedWithdrawRevert();" value="Revert" visible="false" tabindex="17" />
                            <input type="button" id="btnapprove" runat="server" class="pull-right btn btn-approve_popup" onclick="return commentRequriedWithdraw();" value="Approve" visible="false" tabindex="16" />
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 " id="divFContent" style="display: none">
                    <div class="panel-heading header_acc">
                        Form Content
                    </div>
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none grid_panel_hod">
                        <div class="top col-lg-8" id="WordUpload" runat="server" style="padding-left: 10px;">
                            <div class="col-lg-3 padding-none" id="lbl_Upload" runat="server"><b class="grid_icon_legend">Upload your word File:</b></div>
                            <div class="col-lg-5 padding-none">
                                <asp:FileUpload ID="file_word" runat="server" />
                            </div>
                            <asp:UpdatePanel ID="www" runat="server" UpdateMode="Conditional" style="text-align: right">
                                <ContentTemplate>
                                    <asp:Button ID="Btn_Upload" runat="server" Text="Upload" CssClass="btn btn-approve_popup" ValidationGroup="edit" OnClientClick="return Required();" OnClick="Btn_Upload_Click" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="Btn_Upload" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                        <div class="edit_botton_reviewer top pull-right col-lg-1 text-right">
                            <asp:Button ID="btnRefresh" runat="server" Text="Reset" CssClass="btn btn-approve_popup_edit" OnClick="btnRefresh_Click" />
                        </div>
                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 pull-left" style="overflow: auto;" runat="server" id="divRichEdit">
                            <dx:ASPxRichEdit ID="ASPxRichEdit2" runat="server" Width="100%" Height="600px" WorkDirectory="~\App_Data\WorkDirectory" ClientInstanceName="cr" ShowConfirmOnLosingChanges="false">
                                <Settings>
                                    <SpellChecker Enabled="true" Culture="en-US">
                                        <OptionsSpelling IgnoreEmails="true" IgnoreUri="true" />
                                    </SpellChecker>
                                    <Behavior CreateNew="Hidden" Drag="Hidden" Drop="Hidden" Open="Hidden" Printing="Hidden" Save="Hidden" SaveAs="Hidden" />
                                    <HorizontalRuler Visibility="Hidden" />
                                </Settings>
                                <SettingsDocumentSelector UploadSettings-ValidationSettings-DisableHttpHandlerValidation="true" UploadSettings-Enabled="true" UploadSettings-AdvancedModeSettings-TemporaryFolder="~\App_Data\WorkDirectory"></SettingsDocumentSelector>
                            </dx:ASPxRichEdit>
                        </div>
                        <div class="form-group col-md-6 col-lg-12 col-xs-6 col-sm-6" id="comments" runat="server" visible="false">
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none">
                                <label class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none">Comments<span class="smallred" id="spancomment" runat="server">*</span></label>
                                <asp:TextBox ID="txt_Comments" runat="server" CssClass="form-control" placeholder="Enter Comments" TextMode="MultiLine" TabIndex="5" MaxLength="500" ReadOnly="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-xs-12 col-sm-12 top">
                            <asp:Button ID="btn_Cancel" Text="Cancel" Class="pull-right btn btn-canceldms_popup" runat="server" TabIndex="18" OnClick="btn_Cancel_Click" />
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <%--                                    <asp:Button ID="btn_Reset" Text="Reset" Class="pull-right btn btn-revert_popup" runat="server" TabIndex="17" OnClick="btn_Reset_Click" />--%>
                                    <%--<asp:Button ID="btn_Submit" Text="Submit" Class="pull-right btn btn-approve_popup" runat="server" ValidationGroup="save" TabIndex="16" OnClick="btn_Submit_Click" />
                                    <asp:Button ID="btn_Reject" Text="Reject" Class="pull-right btn btn-revert_popup" runat="server" TabIndex="18" Visible="false" OnClick="btn_Reject_Click" OnClientClick="return commentRequriedReject();" />
                                    <asp:Button ID="btn_Revert" Text="Revert" Class="pull-right btn btn-revert_popup" runat="server" TabIndex="17" Visible="false" OnClick="btn_Revert_Click" OnClientClick="return commentRequriedRevert();" />--%>
                                    <%-- <asp:Button ID="btn_approve" Text="Approve" Class="pull-right btn btn-approve_popup" runat="server" TabIndex="16" Visible="false" OnClick="btn_approve_Click" OnClientClick="return commentRequriedApprove();" />--%>
                                    <input type="button" id="btn_Save" runat="server" class="pull-left btn btn-approve_popup" onclick="ConformAlertSave();" value="Save" tabindex="15" />
                                    <input type="button" id="btn_Submit" runat="server" class="pull-right btn btn-approve_popup" onclick="SubmitAction();" value="Submit" tabindex="16" />
                                    <input type="button" id="btn_Reject" runat="server" class="pull-right btn btn-revert_popup" onclick="return commentRequriedReject();" value="Reject" visible="false" tabindex="18" />
                                    <input type="button" id="btn_Revert" runat="server" class="pull-right btn btn-revert_popup" onclick="return commentRequriedRevert();" value="Revert" visible="false" tabindex="17" />
                                    <input type="button" id="btn_approve" runat="server" class="pull-right btn btn-approve_popup" onclick="return commentRequriedApprove();" value="Approve" visible="false" tabindex="16" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btn_Submit" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <uc1:ElectronicSign runat="server" ID="ElectronicSign" />
    <uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />
    <asp:HiddenField runat="server" ID="hdnAction" />
    <asp:HiddenField runat="server" ID="hdnConfirm" />
    <script type="text/javascript">
        function handleFileSelect(event) {
            var files = event.target.files;
            var file = files[0];
            cr.commands.fileOpen.execute(file.name);
        }
    </script>
    <script type="text/javascript">
        function commentRequriedApprove() {
            var comment = document.getElementById("<%=txt_Comments.ClientID%>").value;
            errors = [];
            if (comment == "") {
                errors.push("Enter comments");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                ApproveAction();
                return true;
            }
        }
        function commentRequriedWithdraw() {
            var comment = document.getElementById("<%=txt_statuschangecomments.ClientID%>").value;
            errors = [];
            if (comment == "") {
                errors.push("Enter comments");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                ApproveAction();
                return true;
            }
        }
        function commentRequriedWithdrawInitiate() {
            var comment = document.getElementById("<%=txt_statuschangecomments.ClientID%>").value;
            errors = [];
            if (comment == "") {
                errors.push("Enter comments");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                WithdrawAction();
                return true;
            }
        }
        function commentRequriedRevert() {
            var comment = document.getElementById("<%=txt_Comments.ClientID%>").value;
            errors = [];
            if (comment == "") {
                errors.push("Enter comments");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                RevertAction();
                return true;
            }
        }
        function commentRequriedReject() {
            var comment = document.getElementById("<%=txt_Comments.ClientID%>").value;
            errors = [];
            if (comment == "") {
                errors.push("Enter comments");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                RejectAction();
                return true;
            }
        }
        function commentRequriedWithdrawRevert() {
            var comment = document.getElementById("<%=txt_statuschangecomments.ClientID%>").value;
            errors = [];
            if (comment == "") {
                errors.push("Enter comments");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                RevertAction();
                return true;
            }
        }
        function commentRequriedWithdrawReject() {
            var comment = document.getElementById("<%=txt_statuschangecomments.ClientID%>").value;
            errors = [];
            if (comment == "") {
                errors.push("Enter comments");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                RejectAction();
                return true;
            }
        }
        <%--function RequriedFieldsNextClick() {
            errors = [];
            if ($('#<%=ddl_DeptName.ClientID %> option:selected').val() == "0") {
                errors.push("Select a Department");
            }
            if ($('#<%=txt_FormNumber.ClientID %>').val()  == "") {
                errors.push("Enter Form Number");
            }
            if ($('#<%=txt_Version.ClientID %>').val()  == "") {
                errors.push("Enter Version Number");
            }
            if ($('#<%=txt_FormName.ClientID %>').val() == "") {
                errors.push("Enter Form Name");
            }
            if ($('#<%=ddl_Approver.ClientID %> option:selected').val() == "0") {
                errors.push("Select a Form Approver");
            }
            if(errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                ConformAlertNext();
                return true;
            }
        }--%>
</script>
    <script>
      <%--  function SaveAction() {
            document.getElementById("<%=hdnAction.ClientID%>").value = "Save";
            openElectronicSignModal();
        }--%>
        function SubmitAction() {
            document.getElementById("<%=hdnAction.ClientID%>").value = "Submit";
            openElectronicSignModal();
        }
        function WithdrawAction() {
            document.getElementById("<%=hdnAction.ClientID%>").value = "Withdraw";
            openElectronicSignModal();
        }
        function RejectAction() {
            document.getElementById("<%=hdnAction.ClientID%>").value = "Reject";
            openElectronicSignModal();
        }
        function RevertAction() {
            document.getElementById("<%=hdnAction.ClientID%>").value = "Revert";
            openElectronicSignModal();
        }
        function ApproveAction() {
            document.getElementById("<%=hdnAction.ClientID%>").value = "Approve";
            openElectronicSignModal();
        }
       <%-- function ConformAlertNext() {
            document.getElementById("<%=hdnConfirm.ClientID%>").value = 'Next';
            custAlertMsg('Do you want to save the form details ? They cannot be changed again!', 'confirm', true);
        }--%>

        function ConformAlertSave() {
            document.getElementById("<%=hdnConfirm.ClientID%>").value = 'Save';
            custAlertMsg('Do you want to save the form details and form content ?', 'confirm', true);
        }

    </script>
    <script>
        function ReloadInitiationPage() {
            window.open("<%=ResolveUrl("~/DMS/DMSAdmin/FormCreation.aspx")%>", "_self");
            $('#usES_Modal').modal('hide');
        }
        function ReloadRevertedPage() {
            window.open("<%=ResolveUrl("~/DMS/DMSAdmin/FormCreatorList.aspx")%>", "_self");
            $('#usES_Modal').modal('hide');
        }
        function ReloadApproverPage() {
            window.open("<%=ResolveUrl("~/DMS/DocumentController/FormApproverList.aspx")%>", "_self");
            $('#usES_Modal').modal('hide');
        }
    </script>
    <script>
        function RestrictMinus(e) {
            if (e.keyCode === 45 || e.keyCode === 109 || e.keyCode === 189) {
                return false;
            }
            else {
                return true;
            }
        };
    </script>
    <script>
        function openElectronicSignModal() {
            openUC_ElectronicSign();
        }
    </script>
    <script>
        function Required() {
            var file = document.getElementById("<%=file_word.ClientID%>").value;
            errors = [];
            if (file == "") {
                errors.push("Please Upload file");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }
    </script>
</asp:Content>
