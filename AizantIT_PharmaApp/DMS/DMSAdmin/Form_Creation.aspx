﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="Form_Creation.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DMSAdmin.Form_Creation" %>

<%@ Register Src="~/UserControls/ElectronicSign.ascx" TagPrefix="uc1" TagName="ElectronicSign" %>
<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        $('#divFContent').hide();
        function showFormDetails() {
            $('#divFDetails').show();
            $('#divFContent').hide();
            $('#btnFormDetails').hide();
            var dt = $('#<%=ddl_DocumentType.ClientID %> option:selected').text();
            if (dt == 'Others') {
                showRefDocNumTextBox1();
            }
            else {
                showRefDocNumDropdown();
            }
        }
        function showFormContent() {
            $('#divFDetails').hide();
            $('#divFContent').show();
            showFormDetailsbutton();
        }
        function showFormDetailsClick() {
            var x = document.getElementById("btnFormDetails");
            if (x.value === "Show Details") {
                x.value = "Hide Details";
                showForm();
            } else {
                x.value = "Show Details";
                showFormContent();
            }
        }
        function showRefDocNumTextBox1() {
            $('.divRefDocNumddl').hide();
            $('.divRefDocNumText').show();
        }
        function showRefDocNumTextBox() {
            $('.divRefDocNumddl').hide();
            $('.divRefDocNumText').show();
            $('.divRefDocNumText').val("");
        }
        function showRefDocNumDropdown() {
            $('.divRefDocNumddl').show();
            $('.divRefDocNumText').hide();
        }
        function showForm() {
            $('#divFDetails').show();
            var dt = $('#<%=ddl_DocumentType.ClientID %> option:selected').text();
            if (dt == 'Others') {
                showRefDocNumTextBox();
            }
            else {
                showRefDocNumDropdown();
            }
        }
        function showFormDetailsbutton() {
            $('#btnFormDetails').show();
            $('#btnFormDetails').prop('value', 'Show Details');
        }
        function showStatusChangeDiv() {
            $("#divStatuschange").css({ display: "block" });
        }
    </script>
    <asp:ScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ScriptManager>
    <div class="padding-none">
        <asp:Button ID="btnAdd" runat="server" class="float-right  btn-signup_popup" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
    </div>
    <div class=" col-md-12 col-lg-12 col-12 col-sm-12 dms_outer_border float-left padding-none">
        <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div class=" col-12  padding-none float-right">
                <div class="panel-heading panel-heading_title" id="div2" runat="server">
                </div>
                
                <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left grid_panel_full padding-none">
                    <div class="form-group col-md-6 col-lg-6 col-6 col-sm-6 top float-left ">
                        <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left">
                            <label class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none ">Department<span class="smallred_label">*</span></label>
                            <asp:DropDownList ID="ddl_DeptName" runat="server" data-size="9" data-live-search="true" CssClass="form-control col-md-12 col-lg-12 col-12 col-sm-12 selectpicker  regulatory_dropdown_style drop_down" Style="width: 100%;" TabIndex="1" OnSelectedIndexChanged="ddl_DeptName_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="UpFormNub" runat="server">
                                            <ContentTemplate>
                    <div class="form-group col-sm-3 top float-left">
                        <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left">
                            <label>Form Number<span class="smallred_label">*</span></label>
                            <asp:Button ID="btnFormNumcheck" CssClass="check_button float-right" runat="server" Text="Check!" OnClick="btnFormNumcheck_Click" ToolTip="Form Number Availability" />
                        </div>
                        <asp:TextBox ID="txt_FormNumber" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Form Number" TabIndex="2" autocomplete="off" MaxLength="30" onkeypress="return RestrictDirectoryCharacters(event);" onpaste="return RestrictDirectoryCharactersOnPaste(event);"></asp:TextBox>
                        <asp:Label ID="lbl_number" runat="server" CssClass="label_status" Visible="false"></asp:Label>
                    </div>
</ContentTemplate>
                        </asp:UpdatePanel>
                    <div class="form-group col-sm-3 top float-left">
                        <label class="padding-none col-sm-12 control-label custom_label_answer">Version Number<span class="smallred_label">*</span></label>
                        <div class="col-sm-12 padding-none">
                            <asp:TextBox ID="txt_Version" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Version Number" TextMode="Number" onkeypress="return restrictAlphabets(event);" TabIndex="3" min="0" max="999" MaxLength="3" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"></asp:TextBox>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="UpFormName" runat="server">
                                            <ContentTemplate>
                    <div class="form-group col-sm-6 float-left">
                        <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left">
                            <label>Form Name<span class="smallred_label">*</span></label>
                            <asp:Button ID="btnFormNamecheck" CssClass="check_button float-right" runat="server" Text="Check!" OnClick="btnFormNamecheck_Click" ToolTip="Form Name Availability" />
                        </div>
                        <textarea ID="txt_FormName" runat="server" placeholder="Enter Form Name" class="form-control" TabIndex="4" MaxLength="300" onkeypress="return RestrictStarFrontSlashSquareBraces(event);" onpaste="ValidateStarFrontSlashSquareBracesOnPaste();"></textarea>
                        <asp:Label ID="lblStatus" runat="server" CssClass="label_status" Visible="false"></asp:Label>
                    </div>
                         </ContentTemplate>
                        </asp:UpdatePanel>
                    <div class="form-group col-md-6 col-lg-6 col-6 col-sm-6 float-left">
                        <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left">
                            <label class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none">Description</label>
                            <textarea ID="txt_Description" runat="server" placeholder="Enter Form Description" class="form-control" TabIndex="5" MaxLength="300"></textarea>
                        </div>
                    </div>                    
                        <div class="form-group col-md-6 col-lg-6 col-6 col-sm-6 float-left">
                            <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left">
                                <label class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none">Document Type</label>
                                <asp:DropDownList ID="ddl_DocumentType" runat="server" data-size="9" data-live-search="true" Style="width: 100%;" CssClass="form-control col-md-12 col-lg-12 col-12 col-sm-12 selectpicker  regulatory_dropdown_style drop_down" TabIndex="7" OnSelectedIndexChanged="ddl_DocumentType_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group col-md-6 col-lg-6 col-6 col-sm-6 float-left">
                            <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left">
                                <label class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none">Reference Document Number</label>
                                <asp:UpdatePanel runat="server" ID="upRefDocNum" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddl_RefDocName" runat="server" data-size="9" data-live-search="true" Style="width: 100%;" CssClass="form-control col-md-12 col-lg-12 col-12 col-sm-12 selectpicker  regulatory_dropdown_style drop_down divRefDocNumddl" TabIndex="8"></asp:DropDownList>
                                        <asp:TextBox ID="txt_RefDocNum" runat="server" placeholder="Enter Reference Document Number" CssClass="form-control divRefDocNumText" TabIndex="8" MaxLength="50" Style="display: none;"></asp:TextBox>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddl_DeptName" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="ddl_DocumentType" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>

                            </div>
                        </div>
                    <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left">
                        <div class="form-group col-md-6 col-lg-6 col-6 col-sm-6 float-left" id="Div1" runat="server">
                            <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none">
                                <label class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none">Creator<span class="smallred_label">*</span></label>
                                <asp:DropDownList ID="ddl_Creator" runat="server" CssClass="form-control col-md-12 col-lg-12 col-12 col-sm-12 SearchDropDown selectpicker  regulatory_dropdown_style" Style="width: 100%;" TabIndex="6"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group col-md-6 col-lg-6 col-6 col-sm-6 float-left" id="Approver" runat="server">
                            <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none">
                                <label class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none">Approver<span class="smallred_label">*</span></label>
                                <asp:DropDownList ID="ddl_Approver" runat="server" CssClass="form-control col-md-12 col-lg-12 col-12 col-sm-12 SearchDropDown selectpicker  regulatory_dropdown_style" Style="width: 100%;" TabIndex="6"></asp:DropDownList>
                            </div>
                        </div>                        
                    </div>
                    <div class="col-md-12 col-lg-12 col-12 col-sm-12 float-left top" id="divStatuschange" style="display: none">
                        <div class="btn-group padding-right_div col-sm-12 col-12 col-lg-6 col-md-12 bottom" data-toggle="buttons" id="divOpinion" runat="server">
                            <label for="inputPassword3" class=" padding-right_div control-label">Status<span class="smallred_label">*</span></label>
                            <label class="btn btn-primary_radio col-6 col-lg-6 col-md-6 col-sm-6 active radio_check" id="lblRadioTrainingYes">
                                <asp:RadioButton ID="rbtnActive" value="Yes" runat="server" GroupName="A" Text="Active" Checked="true" />
                            </label>
                            <label class="btn btn-primary_radio col-6 col-lg-6 col-md-6 col-sm-6 radio_check" id="lblRadioTrainingNo">
                                <asp:RadioButton ID="rbtnInActive" value="No" runat="server" GroupName="A" Text="InActive" />
                            </label>
                        </div>
                    </div>
                    <%--<div class="form-group col-md-12 col-lg-12 col-12 col-sm-12 top float-left">
                    </div>--%>
                    <div class="col-lg-3 float-left top" id="uploadForm" style="padding-left: 10px; display: none;">
                        <asp:UpdatePanel ID="www" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="file upload-button btn btn-primary col-sm-12 col-12 col-lg-12 col-md-12" id="dvFup" runat="server" style="margin-top: 0px !important;">Upload<asp:FileUpload class="upload-button_input" ID="file_word" runat="server" onchange="UploadClicK()" accept=".doc,.docx" ToolTip="Click to Upload Form" /></div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="col-lg-8 float-left top" id="divViewForm">
                        <label class="float-left" style="margin-top: 2px;">View Form : </label>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:LinkButton ID="lb_Upload" runat="server" CssClass="float-left" OnClick="lb_Upload_Click" ForeColor="OrangeRed" Font-Bold="true"></asp:LinkButton>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="form-group col-md-6 col-lg-12 col-6 col-sm-6 float-left" id="comments" runat="server">
                        <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none top">
                            <label class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none">Comments<span class="smallred_label" id="spancomment" runat="server">*</span></label>
                            <textarea ID="txt_Comments" runat="server" class="form-control" placeholder="Enter Comments" TabIndex="15" MaxLength="300"></textarea>
                        </div>
                    </div>
                    <div class="form-group col-md-12 col-lg-12 col-12 col-sm-12 top float-left bottom">

                        <asp:Button ID="btn_Cancel" Text="Cancel" Class="float-right  btn-cancel_popup bottom" runat="server" TabIndex="19" OnClick="btn_Cancel_Click" style="margin-left:2px;"/>
                        
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Button ID="btn_Download" Text="Download" Class="float-left  btn-signup_popup bottom" runat="server" TabIndex="15" OnClick="btn_Download_Click" OnClientClick="DoubleClickHandleReport();" />
                                <asp:Button ID="btnReset" Text="Reset" Class="float-right  btn-revert_popup bottom" runat="server" TabIndex="18" OnClick="btn_Reset_Click" style="margin-left:2px;"/>
                                <input type="button" id="btn_Submit" runat="server" class="float-right bottom  btn-signup_popup" onclick="SubmitAction();" value="Submit" tabindex="17" />
                                <input type="button" id="btn_Reject" runat="server" class="float-right bottom btn-revert_popup" onclick="return commentRequriedReject();" value="Reject" visible="false" tabindex="18" style="margin-left:2px;"/>
                                <input type="button" id="btn_Revert" runat="server" class="float-right bottom btn-revert_popup" onclick="return commentRequriedRevert();" value="Revert" visible="false" tabindex="17" style="margin-left:2px;"/>
                                <input type="button" id="btn_approve" runat="server" class="float-right bottom btn-signup_popup" onclick="return commentRequriedApprove();" value="Approve" visible="false" tabindex="16" />
                                <input type="button" id="btn_Update" runat="server" class="float-right  bottom btn-signup_popup" onclick="UpdateAction();" value="Update" visible="false" tabindex="17" />
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btn_Submit" />
                                <asp:PostBackTrigger ControlID="BtnReset" />
                                <asp:PostBackTrigger ControlID="btn_Download" />
                                <asp:PostBackTrigger ControlID="btn_Update" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sticky-popup open" id="divcomments" visible="false">
                    <div class="popup-wrap">
                        <div class="popup-header">
                            <span class="popup-title">Comments<div class="popup-image"></div>
                            </span>
                        </div>
                        <div class="popup-content">
                            <div class="popup-content-pad">
                                <div class="popup-form" id="popup-1560">
                                    <div class="popup-form--part-title-font-weight-normal" id="popup-form-1560">
                                        <div class="popup-form__part popup-part popup-part--multi_line_text popup-part--width-full popup-part--label-above"
                                            id="popup-1560_multi_line_text_4-part" data-popup-type="multi_line_text">
                                            <div class="popup-part-wrap">
                                                <div class="popup-part__el">
                                                    <div class="col-md-12 col-lg-12 col-12 col-sm-12 form-group top bottom">
                                                        <div class="col-md-12 col-lg-12 col-12 col-sm-12 bottom padding-none top" style="border: 1px solid #2e2e2e; max-height: 338px; min-height: 250px; overflow-y: auto;" id="divgvcomments">
                                                            loading..
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    <!--------Progress Bar VIEW-------------->
    <div id="modalProgressbar" data-backdrop="static" class="modal department fade" role="dialog">
        <div class=" modal-dialog modal-md" style="min-width: 47%">
            <!-- Modal content-->
            <div class="modal-content ">
                <div class="modal-header">
                    <span id="span1" runat="server">Progress Bar</span>
                </div>
                  <div class="modal-body">
                <div class="col-md-12 col-lg-12 col-12 col-sm-12 float-left" style="background-color: #fff">
                   
                                <img alt="" src="<%=ResolveUrl("~/Images/TMS_Images/please_wait.gif")%>" />
                          
                </div>
                      </div>
            </div>
        </div>
    </div>
    <!--------End Progress Bar VIEW-------------->

    <!--------File VIEW-------------->
    <div id="mpop_FileViewer" class="modal department fade" role="dialog">
        <div class=" modal-dialog modal-lg" style="min-width: 90%">
            <!-- Modal content-->
            <div class="modal-content ">
                 <div class="modal-header">
                     <asp:UpdatePanel runat="server">
                         <ContentTemplate>
                             <span id="span2" runat="server">Form View</span>
                         </ContentTemplate>
                     </asp:UpdatePanel>                  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="col-md-12 col-lg-12 col-12 col-sm-12">

                    <div class="col-sm-12 padding-none">
                        
                        <div id="divPDF_Viewer">
                            <noscript>
                                Please Enable JavaScript.
                            </noscript>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--------End File VIEW-------------->

    <uc1:ElectronicSign runat="server" ID="ElectronicSign" />
    <uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />
    <asp:HiddenField runat="server" ID="hdnAction" />
    <asp:HiddenField runat="server" ID="hdnConfirm" />
    <asp:HiddenField ID="hdfPkID" runat="server" />
    <asp:HiddenField ID="hdfViewType" runat="server" />
    <asp:HiddenField ID="hdfViewEmpID" runat="server" />
    <asp:HiddenField ID="hdfViewRoleID" runat="server" />
    <asp:HiddenField ID="hdfViewDivID" runat="server" />
    <asp:HiddenField ID="hdfFileName" runat="server" />
    <asp:Button ID="btnUpload" runat="server" Text="Button" OnClick="btnUpload_Click" OnClientClick="return Required();" Style="display: none" />
    <script type="text/javascript">
        function DoubleClickHandleReport() {
            var FormNumber = document.getElementById("<%=txt_FormNumber.ClientID%>").value;
            var FormVersion = document.getElementById("<%=txt_Version.ClientID%>").value;
            custAlertMsg("Form <b>"+FormNumber+"-"+FormVersion+"</b> is downloading please wait,Click OK after download is completed", "info");
        }
        function handleFileSelect(event) {
            var files = event.target.files;
            var file = files[0];
            cr.commands.fileOpen.execute(file.name);
        }
    </script>
    <script type="text/javascript">
        function commentRequriedApprove() {
            var comment = document.getElementById("<%=txt_Comments.ClientID%>").value.trim();
            errors = [];
            if (comment == "") {
                errors.push("Enter Comments.");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                ApproveAction();
                return true;
            }
        }
        function commentRequriedWithdrawInitiate() {
            var comment = document.getElementById("<%=txt_Comments.ClientID%>").value.trim();
            errors = [];
            if (comment == "") {
                errors.push("Enter Comments.");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                WithdrawAction();
                return true;
            }
        }
        function commentRequriedRevert() {
            var comment = document.getElementById("<%=txt_Comments.ClientID%>").value.trim();
            errors = [];
            if (comment == "") {
                errors.push("Enter Comments.");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                RevertAction();
                return true;
            }
        }
        function commentRequriedReject() {
            var comment = document.getElementById("<%=txt_Comments.ClientID%>").value.trim();
            errors = [];
            if (comment == "") {
                errors.push("Enter Comments.");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                RejectAction();
                return true;
            }
        }
    </script>
    <script>
        function SubmitAction() {
            errors = [];
            if ($('#<%=ddl_DeptName.ClientID %> option:selected').val() == "0") {
                errors.push("Select Department.");
            }
            if ($('#<%=txt_FormNumber.ClientID %>').val().trim() == "") {
                errors.push("Enter Form Number.");
            }
            if ($('#<%=txt_Version.ClientID %>').val().trim() == "") {
                errors.push("Enter Version Number.");
            }
            if ($('#<%=txt_FormName.ClientID %>').val().trim() == "") {
                errors.push("Enter Form Name.");
            }
            if ($('#<%=ddl_Approver.ClientID %> option:selected').val() == "0") {
                errors.push("Select Form Approver.");
            }
            if ($('#<%=txt_Comments.ClientID %>').val().trim() == "") {
                errors.push("Enter Comments.");
            }
            var DyPDFName = '<%=this.ViewState["DyPDFName"].ToString()%>';
            var FormVersionID = '<%=this.Request.QueryString["FormVersionID"]%>';
            var from = '<%=this.Request.QueryString["from"]%>';
            if (DyPDFName == 0) {
                if (FormVersionID == "" || from == "r") {
                    errors.push("Upload Form.");
                }
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                if (from == "s") {
                    document.getElementById("<%=hdnAction.ClientID%>").value = "Withdraw";
                }
                else {
                    document.getElementById("<%=hdnAction.ClientID%>").value = "Submit";
                }

                if (FormVersionID == "" || from == "r#no-back-button") {
                    openElectronicSignModal();
                }
                else {
                    if (DyPDFName == 0 && from != "s") {
                        custAlertMsg('Do you want to update without uploading form?', 'confirm', true);
                    }
                    else {
                        openElectronicSignModal();
                    }
                }
                //setTimeout(function () { $('#<%=btn_Submit.ClientID %>').attr("disabled", true); }, 0);
                return true;
            }
        }
        function WithdrawAction() {
            document.getElementById("<%=hdnAction.ClientID%>").value = "Withdraw";
            openElectronicSignModal();
        }
        function RejectAction() {
            document.getElementById("<%=hdnAction.ClientID%>").value = "Reject";
            openElectronicSignModal();
        }
        function RevertAction() {
            document.getElementById("<%=hdnAction.ClientID%>").value = "Revert";
            openElectronicSignModal();
        }
        function ApproveAction() {
            document.getElementById("<%=hdnAction.ClientID%>").value = "Approve";
            openElectronicSignModal();
        }
        function ConformAlertSave() {
            document.getElementById("<%=hdnConfirm.ClientID%>").value = 'Save';
            custAlertMsg('Do you want to save the form details and form content?', 'confirm', true);
        }
    </script>
    <script>
        function ReloadInitiationPage() {
            window.open("<%=ResolveUrl("~/DMS/DMSAdmin/Form_Creation.aspx")%>", "_self");
            //$('#usES_Modal').modal('hide');
        }
        function ReloadRevertedPage() {
            window.open("<%=ResolveUrl("~/DMS/DMSAdmin/FormCreatorList.aspx")%>", "_self");
            //$('#usES_Modal').modal('hide');
        }
        function ReloadApproverPage() {
            window.open("<%=ResolveUrl("~/DMS/DocumentController/FormApproverList.aspx")%>", "_self");
            //$('#usES_Modal').modal('hide');
        }
        function ReloadManagePage() {
            window.open("<%=ResolveUrl("~/DMS/DMSAdmin/FormManageList.aspx")%>", "_self");
            //$('#usES_Modal').modal('hide');
        }
    </script>
    <script>
        function RestrictMinus(e) {
            if (e.keyCode === 45 || e.keyCode === 109 || e.keyCode === 189) {
                return false;
            }
            else {
                return true;
            }
        };
    </script>
    <script>
        function openElectronicSignModal() {
            openUC_ElectronicSign();
        }
    </script>
    <script src='<%= Page.ResolveUrl("~/AppScripts/PDF_Viewer.min.js")%>'></script>
    <script>
        function Required() {
            var fullPath = document.getElementById("<%=file_word.ClientID%>").value;
            var filename = "";
            if (fullPath) {
                var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                var filename = fullPath.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                }
            }
            errors = [];
            if (filename == "") {
                errors.push("Please Choose a file to upload.");
            }
            if (filename.length > 95) {
                errors.push("Uploaded file length should not be greater than 95 characters");
            }
            if (filename.includes(",") || filename.includes("/") || filename.includes(":") || filename.includes("*") || filename.includes("?") ||
                filename.includes("<") || filename.includes(">") || filename.includes("|") || filename.includes("\"") || filename.includes("\\") ||
                filename.includes(";")) {
                errors.push("Uploaded file name can contain only the following special characters !@#()-_'+=$^%&~` ");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                $('#modalProgressbar').modal({ show: true });
                return true;
            }
        }
        function UploadClicK() {
            var btnUpload1 = document.getElementById("<%=btnUpload.ClientID%>");
            btnUpload1.click();
        }
        function CloseBrowser() {
            window.close();
        }
        function ViewForm() {
            var pkID = '<%=this.ViewState["PKID"].ToString()%>';
            PdfViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>', "3", pkID, "0", "0", "#divPDF_Viewer", "func1");
        }
        function ViewTempForm() {
            var dyPDF = '<%=this.ViewState["DyPDFName"].ToString()%>';
            PdfFormViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_Of_File")%>', dyPDF, "#divPDF_Viewer");
        }
    </script>
    <script>
        $(function () {
            if (/*@cc_on!@*/true) {
                var ieclass = 'ie' + document.documentMode;
                $(".popup-wrap").addClass(ieclass);
            }
            $(".sticky-popup").addClass('sticky-popup-right');
            var contwidth = $(".popup-content").outerWidth() + 2;
            $(".sticky-popup").css("right", 0);
            var FormVersionID = '<%=this.Request.QueryString["FormVersionID"]%>';
            var from = '<%=this.Request.QueryString["from"]%>';
            if (FormVersionID == "" || from == "r" || from == "s") {
                $(".sticky-popup").css("visibility", "hidden");
            }
            else {
                $(".sticky-popup").css("visibility", "visible");
            }
            $(".popup-header").click(function () {
                if ($('.sticky-popup').hasClass("open")) {
                    $('.sticky-popup').removeClass("open");
                    $(".sticky-popup").css("right", "-" + contwidth + "px");
                }
                else {
                    $('.sticky-popup').addClass("open");
                    $(".sticky-popup").css("right", 0);
                }
            });
            $("#closeButton").click(function () {
                $('.sticky-popup').removeClass("open");
                $(".sticky-popup").css("right", "-" + contwidth + "px");
            });
        });
        function func1() {

        }
    </script>
    <script>      
        $(document).ready(function () {
            function GetParameterValues(param) {
                var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                for (var i = 0; i < url.length; i++) {
                    var urlparam = url[i].split('=');
                    if (urlparam[0] == param) {
                        return urlparam[1];
                    }
                }
            }
            var from = GetParameterValues("from");
            var FormVersionID1 = GetParameterValues("FormVersionID");
            var pending = GetParameterValues("Pending");
            var jsPath = '<%=ResolveUrl("~/AppScripts/ReviewCommentsDiv.js")%>';
            var security = document.createElement("script");
            security.type = "text/javascript";
            security.src = jsPath;
            $("#divgvcomments").append(security);
            GetFormComments('<%=Page.ResolveUrl("~//DMS//WebServices//DMSService.asmx//GetFormComments")%>', FormVersionID1);
        });
    </script>
    <script>
        function showUpload() {
            $('#uploadForm').show();
        }
        function hideUpload() {
            $('#uploadForm').hide();
        }
        function showViewForm() {
            $('#divViewForm').show();
        }
        function hideViewForm() {
            $('#divViewForm').hide();
        }
    </script>
    <script>
         function getEveryoneCommentsData(FormVersionID) {
             GetFormComments('<%= Page.ResolveUrl("~//DMS//WebServices//DMSService.asmx//GetFormComments" )%>', FormVersionID);
         }
         function restrictAlphabets(e) {
             var x = e.which || e.keycode;
             if ((x >= 48 && x <= 57) || x == 8 ||
                 (x >= 35 && x <= 40))
                 return true;
             else
                 return false;
         }
    </script>
    <script src='<%=Page.ResolveUrl("~/AppScripts/ReviewCommentsDiv.js")%>'></script>
    <script>
         function hideImg() {
             ShowPleaseWait('hide');
         }
    </script>
     <script>
         $(function () {
             InsertFormNumTitle();
         });

         var prm = Sys.WebForms.PageRequestManager.getInstance();
         prm.add_endRequest(function () {
             InsertFormNumTitle();
         });
         function InsertFormNumTitle() {
             var text_val;
             $("#<%=txt_FormNumber.ClientID%>").on('keyup', function () {
                text_val = $(this).val();
                $("#<%=txt_FormNumber.ClientID%>").attr('title', text_val);
            });
         }
         function HidePopup() {
             $('#usES_Modal').modal('hide');
         }
         function UpdateAction() {
             errors = [];
             var CreationStatusID = '<%=this.ViewState["CreationStatusID"].ToString()%>';
             if (CreationStatusID == '1') {
                 if ($('#<%=ddl_Creator.ClientID %> option:selected').val() == "0") {
                     errors.push("Select Form Creator.");
                 }
             }
             
             if ($('#<%=ddl_Approver.ClientID %> option:selected').val() == "0") {
                 errors.push("Select Form Approver.");
             }
             if ($('#<%=txt_Comments.ClientID %>').val().trim() == "") {
                 errors.push("Enter Comments.");
             }
             if (errors.length > 0) {
                 custAlertMsg(errors.join("<br/>"), "error");
                 return false;
             }
             else {
                 document.getElementById("<%=hdnAction.ClientID%>").value = "Update";
                 openElectronicSignModal();
             }
         }
  </script>
</asp:Content>
