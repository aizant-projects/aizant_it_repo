﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using AizantIT_PharmaApp.DMS.WebServices;
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AizantIT_PharmaApp.DMS
{
    public partial class MainFormList : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocObjects docObjects = new DocObjects();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=2").Length > 0)//admin role
                            {
                                hdfRole.Value = "2";
                                ///btnMainFormListReport.Visible = true;
                            }
                            else if (dtTemp.Select("RoleID=30").Length > 0)//controller role
                            {
                                hdfRole.Value = "30";
                                //btnMainFormListReport.Visible = true;
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show1", "getadmincolumn();", true);
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MFL_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MFL_M1:" + strline + "  " + "Document Load failed.", "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        protected void btnFormView_Click(object sender, EventArgs e)
        {
            btnFormView.Enabled = false;
            try
            {
                DocObjects docObjects = new DocObjects();
                docObjects.FormVersionID = Convert.ToInt32(hdfVID.Value);
                DataTable DMS_DT = DMS_Bal.DMS_GetFormHistory(docObjects);
                hdfViewType.Value = "3";
                hdfViewEmpID.Value = "0";
                hdfViewRoleID.Value = "0";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view1", "ViewPDFForm();", true);
                span1.Attributes.Add("title", DMS_DT.Rows[0]["FormTitle"].ToString());
                if (DMS_DT.Rows[0]["FormTitle"].ToString().Length > 60)
                {
                    span1.InnerHtml = DMS_DT.Rows[0]["FormTitle"].ToString().Substring(0, 60) + "...";
                }
                else
                {
                    span1.InnerHtml = DMS_DT.Rows[0]["FormTitle"].ToString();
                }
                UpHeading.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MFL_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MFL_M2:" + strline + "  " + "Form viewing failed.", "error");
            }
            finally
            {
                btnFormView.Enabled = true;
            }
        }
        protected void btnCommentsHistoryView_Click(object sender, EventArgs e)
        {
            btnCommentsHistoryView.Enabled = false;
            try
            {
                DocObjects docObjects = new DocObjects();
                docObjects.FormVersionID = Convert.ToInt32(hdfVID.Value);
                DataTable DMS_DT = DMS_Bal.DMS_GetFormHistory(docObjects);
                gv_CommentHistory.DataSource = DMS_DT;
                ViewState["HistoryDataTable"] = DMS_DT;
                gv_CommentHistory.DataBind();
                UpGridButtons.Update();
                if (DMS_DT.Rows.Count > 0)
                {
                    span4.Attributes.Add("title", DMS_DT.Rows[0]["FormTitle"].ToString());
                    if (DMS_DT.Rows[0]["FormTitle"].ToString().Length > 60)
                    {
                        span4.InnerHtml = DMS_DT.Rows[0]["FormTitle"].ToString().Substring(0, 60) + "...";
                    }
                    else
                    {
                        span4.InnerHtml = DMS_DT.Rows[0]["FormTitle"].ToString();
                    }
                    upcomment.Update();
                }
                else
                {
                    span4.InnerText = "Comment History";
                    upcomment.Update();
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myCommentHistory').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MFL_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MFL_M3:" + strline + "  " + "Form History loading failed.", "error");
            }
            finally
            {
                btnCommentsHistoryView.Enabled = true;
            }
        }
        protected void gv_CommentHistory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string _fullcomments = (e.Row.Cells[3].FindControl("txt_Comments") as TextBox).Text;
                    LinkButton _lnkmorecomments = (e.Row.Cells[3].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (e.Row.Cells[3].FindControl("lbcommentsless") as LinkButton);
                    if (_fullcomments.Length > 70)
                    {
                        (e.Row.Cells[3].FindControl("lbl_shortComments") as Label).Visible = true;
                        TextBox FullTbx = (e.Row.Cells[3].FindControl("txt_Comments") as TextBox);
                        (e.Row.Cells[3].FindControl("lbl_shortComments") as Label).Text = _fullcomments.Substring(0, 70) + "..";
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = true;

                        if (_fullcomments.Length > 100 && _fullcomments.Length <= 200)
                        {
                            FullTbx.Rows = 3;
                        }
                        else if (_fullcomments.Length > 200 && _fullcomments.Length <= 300)
                        {
                            FullTbx.Rows = 4;
                        }

                        FullTbx.Visible = false;
                    }
                    else
                    {
                        (e.Row.Cells[3].FindControl("lbl_shortComments") as Label).Visible = true;
                        (e.Row.Cells[3].FindControl("txt_Comments") as TextBox).Visible = false;
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MFL_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MFL_M4:" + strline + "  " + "Comment History row data-bound failed.", "error");
            }
        }
        protected void gv_CommentHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                
                if (e.CommandName == "SM")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    LinkButton _lnkmorecomments = (row.Cells[3].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (row.Cells[3].FindControl("lbcommentsless") as LinkButton);
                    (row.Cells[3].FindControl("lbl_shortComments") as Label).Visible = false;
                    (row.Cells[3].FindControl("txt_Comments") as TextBox).Visible = true;
                    _lnklesscomments.Visible = true;
                    _lnkmorecomments.Visible = false;
                }
                else if (e.CommandName == "SL")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    LinkButton _lnkmorecomments = (row.Cells[3].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (row.Cells[3].FindControl("lbcommentsless") as LinkButton);
                    (row.Cells[3].FindControl("lbl_shortComments") as Label).Visible = true;
                    (row.Cells[3].FindControl("txt_Comments") as TextBox).Visible = false;
                    _lnklesscomments.Visible = false;
                    _lnkmorecomments.Visible = true;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MFL_M5:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MFL_M5:" + strline + "  " + "Comment History row command failed.", "error");
            }
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            btnEdit.Enabled = false;
            try
            {
                DocObjects docObjects = new DocObjects();
                docObjects.FormVersionID = Convert.ToInt32(hdfVID.Value);
                DataTable DMS_DT = DMS_Bal.DMS_GetFormHistory(docObjects);
                if (DMS_DT.Rows.Count > 0)
                {
                    span2.Attributes.Add("title", DMS_DT.Rows[0]["FormTitle"].ToString());
                    if (DMS_DT.Rows[0]["FormTitle"].ToString().Length > 60)
                    {
                        span2.InnerHtml = DMS_DT.Rows[0]["FormTitle"].ToString().Substring(0, 60) + "...";
                    }
                    else
                    {
                        span2.InnerHtml = DMS_DT.Rows[0]["FormTitle"].ToString();
                    }
                }
                else
                {
                    span2.InnerText = "Edit Form";
                }
                rbtnRevision.Checked = false;
                rbtnStatusChange.Checked = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myFormEdit').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MDL_M5:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MDL_M5:" + strline + "  " + "Page Initialization failed.", "error");
            }
            finally
            {
                btnEdit.Enabled = true;
            }
        }
        protected void gv_CommentHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_CommentHistory.PageIndex = e.NewPageIndex;
                gv_CommentHistory.DataSource = (DataTable)(ViewState["HistoryDataTable"]);
                gv_CommentHistory.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MDL_M6:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MDL_M6:" + strline + "  " + "grid view page index changing failed.", "error");
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }

        //protected void btnMainFormListReport_Click(object sender, EventArgs e)
        //{
        //    DMSService service = new DMSService();
        //    string EmpName = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString();
        //    string base64String = service.ReportMainFormList("N", EmpName);
        //    byte[] bytes = Convert.FromBase64String(base64String);
        //    System.IO.MemoryStream ms = new System.IO.MemoryStream(bytes);
        //    Response.Clear();
        //    Response.Buffer = true;
        //    Response.ContentType = "application/pdf";
        //    string _dwnFileName = "FormMainList_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".pdf";
        //    Response.AddHeader("content-disposition", "attachment;filename=" + _dwnFileName);
        //    ms.WriteTo(Response.OutputStream);
        //    Response.End();
        //}
    }
}