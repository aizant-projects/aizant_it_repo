﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="Document_Type.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentIntiator.Document_Type" %>

<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="<%=ResolveUrl("~/AppCSS/DMS/DMS_StyleSheet1.css")%>" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class=" col-lg-6 float-right padding-none">
        <asp:UpdatePanel runat="server" ID="upAddDocument" UpdateMode="Conditional">
        <ContentTemplate>
        <asp:Button ID="btnAddDocument" runat="server" CssClass=" btn-signup_popup float-right launch-modal" Text="Add Document Type" OnClientClick="ShowAddpanel();" OnClick="btnAddDocument_Click" />
            </ContentTemplate>
    </asp:UpdatePanel>
    </div>
    <div class=" col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 float-left padding-none">
        <div class=" col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 dms_outer_border float-left padding-none">
            <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
                <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
            </div>
            <div id="divMainContainer" runat="server" visible="true">
                <div class=" col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12  padding-none float-right " visible="false">
                    <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12  float-left padding-none">
                        <div class="grid_header col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12  float-left">Document Type</div>
                        <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 grid_panel_full border_top_none bottom  float-left">
                           <div class="col-12 float-left padding-none top bottom">
                            <table id="dtDocType" class="display datatable_cust" cellspacing="0" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Document Type Id</th>
                                        <th>Document Type</th>
                                        <th>Review Process Type</th>
                                        <th>Training Process</th>
                                        <th>Review Period</th>
                                        <th>Edit</th>
                                        <th>History</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-------- Add Panel VIEW-------------->
    <div id="myModal_lock" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" style="min-width:40%;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    
                    <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Always">
                        <ContentTemplate>
                            <span id="span_FileTitle" runat="server">Add Document</span>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <button type="button" class="close" data-dismiss="modal" style="width: 50px;">&times;</button>
                </div>
                <div class="modal-body bottom float-left col-lg-12 col-12 col-md-12 col-sm-12 padding-none" style="padding-bottom: 0px; ">
                    <asp:UpdatePanel runat="server" ID="upDocTypeModelBody" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="form-group input-group col-lg-12 col-12 col-md-12 col-sm-12 float-left padding-none" runat="server">
                                <asp:Label ID="lbl_requestType" runat="server" Text="Document Type" CssClass="col-5 profile_label top required"></asp:Label>
                                <div class="col-7 float-left">
                                <asp:TextBox ID="txt_DocumentType" runat="server" AutoPostBack="true" OnTextChanged="txt_DocumentType_TextChanged" AutoCompleteType="Disabled" CssClass="form-control  login_input_sign_up" placeholder="Enter Document Type" onkeypress="return RestrictPercentageSquareBraces(event);" MaxLength="100" TabIndex="1" onblur=""></asp:TextBox>
                            </div>
                                </div>
                            <div id="DivLoading" class="form-group input-group col-lg-12 col-12 col-md-12 col-sm-12 float-left padding-none" style="display:none">
                                <div class="col-5"></div>
                                <label Class="col-7 profile_label top required" style="color:red">Document Type is being verified , Please wait..</label>
                            </div>
                            <div class="form-group input-group col-lg-12 col-12 col-md-12 col-sm-12 float-left padding-none " id="ProcessType" runat="server">
                                <asp:Label ID="Label1" runat="server" Text="Review Process Type" CssClass="col-5 profile_label top required"></asp:Label>
                                <div class="col-7 float-left">
                                <asp:DropDownList ID="ddl_ProcessType" runat="server" CssClass="form-control selectpicker drop_down showtick regulatory_dropdown_style" TabIndex="2">
                                </asp:DropDownList>
                                    </div>
                            </div>
                            <asp:CheckBox ID="cbDeclaration" runat="server" Text="Training" Style="display: none" />
                            <div class="form-group input-group col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 float-left padding-none">
                                <asp:Label ID="lblRadiobutton" runat="server" Text="Training Process Required" CssClass="col-5 profile_label  required top"></asp:Label>
                                <div class="col-7 float-left">
                                <asp:RadioButtonList runat="server" ID="RadioButtonList1" RepeatDirection="Horizontal" TabIndex="4">
                                    <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="no"></asp:ListItem>
                                </asp:RadioButtonList>
                                    </div>
                            </div>
                             <div class="form-group input-group col-lg-12 col-12 col-md-12 col-sm-12 float-left padding-none " id="Div1" runat="server">
                                <asp:Label ID="Label3" runat="server" Text="Review Period" CssClass="col-5 profile_label top required"></asp:Label>
                                <div class="col-7 float-left">
                                <asp:DropDownList ID="ddlReviewPeriod" data-size="7" runat="server" CssClass="form-control selectpicker drop_down showtick regulatory_dropdown_style" TabIndex="3">
                                </asp:DropDownList>
                                    </div>
                            </div>
                            <div class="form-group input-group col-lg-12 col-12 col-md-12 col-sm-12 float-left " id="dvComments" runat="server" visible="false">
                                <asp:Label ID="Label2" runat="server" Text="Comments" CssClass=" label_name  required"></asp:Label>
                                <div class="col-12 float-left padding-none">
                                <textarea ID="txtComments" runat="server" rows="3" class="col-12 form-control" placeholder="Enter Comments" MaxLength="300" TabIndex="4"></textarea>
                            </div>
                                </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="txt_DocumentType" EventName="TextChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer" style="border-bottom: 1px solid #d4d4d4 !important;">
                    <asp:UpdatePanel ID="UpSubmit" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Button type="button" ID="btn_RADD" runat="server" Text="Submit" CssClass=" btn-signup_popup" OnClick="btn_RADD_Click" OnClientClick="return EditDocument();" TabIndex="4" />
                            <asp:Button type="button" ID="btn_Close" runat="server" Text="Cancel" CssClass=" btn-cancel_popup" OnClick="btn_Close_Click" TabIndex="5" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <!-------- End Add Panel-------------->

    <uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />

    <!--------Comment History View-------------->
    <div id="myCommentHistory" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 85%">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel ID="upcomment" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-header">
                           
                            <span id="span4" runat="server"></span>
                             <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body" style="max-height: 600px; overflow-y: auto">
                            <div class=" history_grid" style="overflow-y: auto;">
                                <asp:GridView ID="gv_CommentHistory" runat="server" AllowPaging="true" PageSize="10" CssClass="table table-hover" ClientIDMode="Static" EmptyDataText="No Records Found" AutoGenerateColumns="False" PagerSettings-Mode="NumericFirstLast" BackColor="White" BorderColor="#d2d2d2" BorderStyle="None" BorderWidth="1px" OnRowDataBound="gv_CommentHistory_RowDataBound" OnRowCommand="gv_CommentHistory_RowCommand" OnPageIndexChanging="gv_CommentHistory_PageIndexChanging" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="User" ItemStyle-Width="11%" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_GivenBy" runat="server" Text='<%# Eval("GivenBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role" ItemStyle-Width="11%" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Role" runat="server" Text='<%# Eval("CommentedRole") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" ItemStyle-Width="12%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Date" runat="server" Text='<%# Eval("CommentedDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="23%" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Action" runat="server" Text='<%# Eval("Action") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="39%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_shortComments" runat="server" Text='<%# Eval("Comments") %>'></asp:Label>
                                                <asp:TextBox ID="txt_Comments" runat="server" class="col-12" ReadOnly="true" Rows="4" TextMode="MultiLine" Text='<%# Eval("Comments") %>' Width="100%"></asp:TextBox>
                                                <asp:LinkButton ID="lbcommentsmore" runat="server" CommandArgument='<%# Eval("Comments")%>' CommandName="SM">Show More</asp:LinkButton>
                                                <asp:LinkButton ID="lbcommentsless" runat="server" CommandArgument='<%# Eval("Comments") %>' CommandName="SL">Show Less</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                     <PagerStyle cssClass="gridpager"  HorizontalAlign="Right" />
                                </asp:GridView>
                            </div>
                        </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!--------End Comment History View-------------->


    <asp:HiddenField ID="hdfPKID" runat="server" />
    <asp:UpdatePanel runat="server" ID="upBtns" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnDocTypeEdit" runat="server" Text="Submit" Style="display: none" OnClick="btnDocTypeEdit_Click" OnClientClick="showImg();"/>
            <asp:Button ID="btnCommentsHistoryView" runat="server" Text="Submit" Style="display: none" OnClick="btnCommentsHistoryView_Click" OnClientClick="showImg();"/>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>
        $('#dtDocType thead tr').clone(true).appendTo('#dtDocType thead');
        $('#dtDocType thead tr:eq(1) th').each(function (i) {
            if (i < 6) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder=" Search " />');

                $('input', this).on('keyup change', function () {
                    if (tblReload.column(i).search() !== this.value) {
                        tblReload
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
                if (i == 1) {
                    $(this).html('');
                }
            }
            else {
                $(this).html('');
            }
        });
        $('#dtDocType').wrap('<div class="dataTables_scroll" />');
        var tblReload = $('#dtDocType').DataTable({
            "autoWidth": true,
            columns: [
                { 'data': 'RowNumber' },
                { 'data': 'DocumentTypeID' },
                { 'data': 'DocumentType' },
                { 'data': 'ProcessType' },
                { 'data': 'IsTraining' },
                 {'data': 'ReviewPeriod'},
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="Edit" title="Edit" data-toggle="modal" data-target="#myModal_lock" onclick=ViewDocument(' + o.DocumentTypeID + ')></a>'; }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="summary_latest" title="History" data-target="#myCommentHistory" data-toggle="modal" onclick=ViewCommentHistory(' + o.DocumentTypeID + ')></a>'; }
                }
            ],
            "aoColumnDefs": [{ "targets": [0,1], "visible": false, "searchable": false }, { "bSortable": false, "aTargets": [0, 1] }, { "className": "dt-body-left", "targets": [2, 3, 4,5] },
            ],
            "orderCellsTop": true,
            "order": [[1, "desc"]],
            'bAutoWidth': true,
            "responsive": true,
            sServerMethod: 'Post',
            "sAjaxSource": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetDocumentTypes" )%>',
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push();
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#dtDocType").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        UserSessionCheck();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
        function reloadtable() {
            tblReload.ajax.reload();
            $("#myModal_lock").modal('hide');
        }
    </script>
    <script>       
        function ShowAddpanel() {
            resetAdd();
            document.getElementById("<%=span_FileTitle.ClientID%>").innerHTML = "<h4 class='modal-title'>Add Document</h4>";

            $("#myModal_lock").modal({
                backdrop: 'static',
                keyboard: false,
            });
        }
        function HideAddPanel() {
            $("#myModal_lock .close").click();
        }
        function resetAdd() {
            document.getElementById('<%=txt_DocumentType.ClientID%>').value = '';
            document.getElementById('<%=ddl_ProcessType.ClientID%>').value = 0;
            document.getElementById('<%=btn_RADD.ClientID%>').value = 'Submit';
            document.getElementById('<%=ddl_ProcessType.ClientID%>').disabled = false;
        }
        function ViewDocument(PK_ID) {
            document.getElementById("<%=hdfPKID.ClientID%>").value = PK_ID;

            var btnFV = document.getElementById("<%=btnDocTypeEdit.ClientID %>");
            document.getElementById("<%=btn_RADD.ClientID%>").value = 'Update';
            document.getElementById("<%=span_FileTitle.ClientID%>").value = 'Edit Document';
            btnFV.click();
        }
    </script>
    <script type="text/javascript">     
        function EditDocument() {
            var documenType = document.getElementById("<%=txt_DocumentType.ClientID%>").value;
            var processType = document.getElementById("<%=ddl_ProcessType.ClientID%>").value;
            var radio = rb.getElementsByTagName("input");
            var isChecked = false;
            for (var i = 0; i < radio.length; i++) {
                if (radio[i].checked) {
                    isChecked = true;
                    break;
                }
            }
            errors = [];
            if (documenType == "") {
                errors.push("Enter Document Type.");
            }
            if (processType == 0) {
                errors.push("Select Review Process Type.");
            }
            if (!isChecked) {
                errors.push("Select an Training option.");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                showImg();
                return true;
            }
        }
    </script>
    <script>
        function RefreshPage() {
            window.open("<%=ResolveUrl("~/DMS/DocumentIntiator/Document_Type.aspx")%>", "_self");
        }
        function ConformAlert() {
            custAlertMsg('The Review Process Type and Training Process Required Cannot be Changed again, Do You Want to Save?', 'confirm', true);
        }
        function ViewCommentHistory(PK_ID) {
            document.getElementById("<%=hdfPKID.ClientID%>").value = PK_ID;
            var btnCV = document.getElementById("<%=btnCommentsHistoryView.ClientID %>");
            btnCV.click();
        }
    </script>
    <script>
        function showPopup() {
            $('#myModal_lock').modal({ show: true, backdrop: 'static', keyboard: false });
            $('#myModal_lock').on('shown.bs.modal', function () {
                document.getElementById("<%=txt_DocumentType.ClientID%>").focus();
            });
        }
    </script>
    <script>
        function disableRadioTraining() {
            $('#lblRadioTrainingYes').removeClass('active');
            $('#lblRadioTrainingNo').addClass('active');
        }
        function enableRadioTraining() {
            $('#lblRadioTrainingYes').addClass('active');
            $('#lblRadioTrainingNo').removeClass('active');
        }
        $('.cancel-click').on('click', function (e) {
            if (e.target.nodeName === 'LABEL') e.preventDefault();
        });
    </script>
    <script>
        function showImg() {
            ShowPleaseWait('show');
        }
        function hideImg() {
            ShowPleaseWait('hide');
        }
        function showLoading(){
            $('#DivLoading').show();
        }
    </script>
</asp:Content>
