﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using System.Text;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;
using System.Text.RegularExpressions;

namespace AizantIT_PharmaApp.DMS.DocumentIntiator
{
    public partial class DocumentIntiation : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        DocObjects docObjects = new DocObjects();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            ElectronicSign.buttonClick += new EventHandler(ElectronicSign_buttonClick);
            ConfirmCustomAlert.buttonClick += new EventHandler(ConfirmCustomAlert_buttonClick);
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        CreateViewState();
                        DisplayHeading();
                        ViewState["DocNameChange"] = "0";
                        ViewState["RenewAuthorShow"] = "0";
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if ((dtTemp.Select("RoleID=12").Length > 0) || (dtTemp.Select("RoleID=2").Length > 0))//12-Initiator,2-DMSAdmin
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M1:" + strline + "  " + "Document Load failed.", "error");
            }
        }
        public void DisplayHeading()
        {
            try
            {
                if ((Request.QueryString["DocumentID"] != null && Request.QueryString["From"] == "M"))
                {
                    lblheding.Text = "Manage Document Reviewers";
                }
                if ((Request.QueryString["DocumentID"] != null && Request.QueryString["From"] == "R"))
                {
                    lblheding.Text = "Reverted Document";
                }
                if (Request.QueryString["DocumentID"] == null)
                {
                    lblheding.Text = "Initiate Document";
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M2:" + strline + "  " + "Display Heading failed.", "error");
            }
        }
        public void CreateViewState()
        {
            try
            {
                DataTable dtlOCAL = new DataTable();
                dtlOCAL.Columns.Add("EmpID");
                dtlOCAL.Columns.Add("Control");
                dtlOCAL.Columns.Add("Role");
                ViewState["dtRemoveEmpID"] = dtlOCAL;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M3:" + strline + "  " + "ViewState dtRemoveEmpID Creation failed.", "error");
            }
        }
        protected void ElectronicSign_buttonClick(object sender, EventArgs e)
        {
            try
            {
                bool b = ElectronicSign.IsPasswordValid;
                if (b == true)
                {
                    if (ViewState["Action"].ToString() == "Reject")
                    {
                        Reject();
                    }
                }
                if (b == false)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Password is Incorrect!", "error");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M2:" + strline + "  " + "Electronic Signature validation failed.", "error");
            }
        }
        protected void ConfirmCustomAlert_buttonClick(object sender, EventArgs e)
        {
            try
            {
                if (hdnConfirm.Value == "Delete")
                {
                    int EmpID = Convert.ToInt32(hdnDeleteEmpIDRow.Value);
                    string Role = hdnDeleteRoleRow.Value.ToString().Trim();
                    DataTable dtApprover = ViewState["ApproverData"] as DataTable;
                    if (ViewState["dtRemoveEmpID"] is DataTable dtTemp)
                    {
                        if (dtApprover.Rows.Count >= 0)
                        {
                            for (int i = 0; i < dtApprover.Rows.Count; i++)
                            {
                                int TempEmpID = Convert.ToInt32(dtApprover.Rows[i]["EmpID"].ToString());
                                string TempRole = dtApprover.Rows[i]["Role"].ToString();
                                if (TempEmpID == EmpID && TempRole == Role)
                                {
                                    dtApprover.Rows.RemoveAt(i);
                                }
                            }
                            for (int i = 0; i < dtTemp.Rows.Count; i++)
                            {
                                int RemoveEmpID = Convert.ToInt32(dtTemp.Rows[i]["EmpID"].ToString());
                                string RemoveRole = dtTemp.Rows[i]["Role"].ToString();
                                if (RemoveEmpID == EmpID && RemoveRole == Role)
                                {
                                    dtTemp.Rows.RemoveAt(i);
                                }
                            }
                            ViewState["ApproverData"] = dtApprover;
                            gvApprover.DataSource = dtApprover;
                            gvApprover.DataBind();
                        }
                    }
                    ShowDcoumentList();
                }
                else if (hdnConfirm.Value == "Submit")
                {
                    int DMS_ins;
                    StringBuilder sbErrorMsg = new StringBuilder();
                    if (ddl_requestType.SelectedValue == "1")
                    {
                        docObjects.DMS_DocumentName = Regex.Replace(txt_DocumentName.Value.Trim(), @"\s+", " ");
                        docObjects.DMS_DocumentType = Convert.ToInt32(ddl_DocumentType.SelectedValue);
                        docObjects.DMS_IntiatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        docObjects.DMS_CreatedBy = Convert.ToInt32(ddl_CreatedBy.SelectedValue);
                        List<Int_EmployeeBy> _lstQA = new List<Int_EmployeeBy>();
                        List<Int_EmployeeBy> _lstReviewBy = new List<Int_EmployeeBy>();
                        List<Int_EmployeeBy> _lstApproverBy = new List<Int_EmployeeBy>();
                        foreach (GridViewRow row in gvApprover.Rows)
                        {
                            if ((row.Cells[0].Text) == "Initiation Request Approver")
                            {
                                Int_EmployeeBy _Objint_EmployeeBy = new Int_EmployeeBy();
                                _Objint_EmployeeBy.EmpID = Convert.ToInt32((row.Cells[3].FindControl("lblEmpId") as TextBox).Text);
                                _Objint_EmployeeBy.DeptID = Convert.ToInt32((row.Cells[3].FindControl("lblDeptID") as TextBox).Text);
                                _lstQA.Add(_Objint_EmployeeBy);
                            }
                            else if ((row.Cells[0].Text) == "Document Reviewer")
                            {
                                Int_EmployeeBy _Objint_EmployeeBy = new Int_EmployeeBy();
                                _Objint_EmployeeBy.EmpID = Convert.ToInt32((row.Cells[3].FindControl("lblEmpId") as TextBox).Text);
                                _Objint_EmployeeBy.DeptID = Convert.ToInt32((row.Cells[3].FindControl("lblDeptID") as TextBox).Text);
                                _lstReviewBy.Add(_Objint_EmployeeBy);
                            }
                            else if ((row.Cells[0].Text) == "Document Approver")
                            {
                                Int_EmployeeBy _Objint_EmployeeBy = new Int_EmployeeBy();
                                _Objint_EmployeeBy.EmpID = Convert.ToInt32((row.Cells[3].FindControl("lblEmpId") as TextBox).Text);
                                _Objint_EmployeeBy.DeptID = Convert.ToInt32((row.Cells[3].FindControl("lblDeptID") as TextBox).Text);
                                _lstApproverBy.Add(_Objint_EmployeeBy);
                            }
                        }
                        docObjects.lstQA = _lstQA;
                        docObjects.lstReviewBy = _lstReviewBy;
                        docObjects.lstApproveBy = _lstApproverBy;
                        docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName.SelectedValue);
                        docObjects.RequestType = Convert.ToInt32(ddl_requestType.SelectedValue);
                        docObjects.authorBy = Convert.ToInt32(ddl_authorizedBy.SelectedValue);
                        docObjects.documentNUmber = Regex.Replace(txt_DocumentNumber.Text.Trim(), @"\s+", " ");
                        docObjects.Purpose = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                        DMS_ins = DMS_Bal.DMS_DocumentCreation(docObjects, (RadioButtonList1.SelectedValue == "yes" ? true : false));
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), " New  Document Number <b>" + txt_DocumentNumber.Text + "</b> Initiated Successfully.", "success", "ReloadInitiationPage()");
                    }
                    else if (ddl_requestType.SelectedValue == "2")
                    {
                        if(txt_DocumentName.Value.Trim() == "")
                        {
                            string[] OnlyDocName = ddl_AutoDocName.SelectedItem.Text.Split('-');
                            docObjects.DMS_DocumentName = Regex.Replace(OnlyDocName[1].Trim(), @"\s+", " ");
                        }
                        else
                        {
                            docObjects.DMS_DocumentName = Regex.Replace(txt_DocumentName.Value.Trim(), @"\s+", " ");
                        }
                        docObjects.DMS_DocumentType = Convert.ToInt32(ddl_DocumentType.SelectedValue);
                        docObjects.DMS_IntiatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        docObjects.DMS_CreatedBy = Convert.ToInt32(ddl_CreatedBy.SelectedValue);
                        List<Int_EmployeeBy> _lstQA = new List<Int_EmployeeBy>();
                        List<Int_EmployeeBy> _lstReviewBy = new List<Int_EmployeeBy>();
                        List<Int_EmployeeBy> _lstApproverBy = new List<Int_EmployeeBy>();
                        foreach (GridViewRow row in gvApprover.Rows)
                        {
                            if ((row.Cells[0].Text) == "Initiation Request Approver")
                            {
                                Int_EmployeeBy _Objint_EmployeeBy = new Int_EmployeeBy();
                                _Objint_EmployeeBy.EmpID = Convert.ToInt32((row.Cells[3].FindControl("lblEmpId") as TextBox).Text);
                                _Objint_EmployeeBy.DeptID = Convert.ToInt32((row.Cells[3].FindControl("lblDeptID") as TextBox).Text);
                                _lstQA.Add(_Objint_EmployeeBy);
                            }
                            else if ((row.Cells[0].Text) == "Document Reviewer")
                            {
                                Int_EmployeeBy _Objint_EmployeeBy = new Int_EmployeeBy();
                                _Objint_EmployeeBy.EmpID = Convert.ToInt32((row.Cells[3].FindControl("lblEmpId") as TextBox).Text);
                                _Objint_EmployeeBy.DeptID = Convert.ToInt32((row.Cells[3].FindControl("lblDeptID") as TextBox).Text);
                                _lstReviewBy.Add(_Objint_EmployeeBy);
                            }
                            else if ((row.Cells[0].Text) == "Document Approver")
                            {
                                Int_EmployeeBy _Objint_EmployeeBy = new Int_EmployeeBy();
                                _Objint_EmployeeBy.EmpID = Convert.ToInt32((row.Cells[3].FindControl("lblEmpId") as TextBox).Text);
                                _Objint_EmployeeBy.DeptID = Convert.ToInt32((row.Cells[3].FindControl("lblDeptID") as TextBox).Text);
                                _lstApproverBy.Add(_Objint_EmployeeBy);
                            }
                        }
                        docObjects.lstQA = _lstQA;
                        docObjects.lstReviewBy = _lstReviewBy;
                        docObjects.lstApproveBy = _lstApproverBy;
                        docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName.SelectedValue);
                        docObjects.RequestType = Convert.ToInt32(ddl_requestType.SelectedValue);
                        docObjects.authorBy = Convert.ToInt32(ddl_authorizedBy.SelectedValue);
                        docObjects.DocumentID = ddl_AutoDocName.SelectedValue;
                        docObjects.Purpose = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                        DMS_ins = DMS_Bal.DMS_DocumentUpdation(docObjects, (RadioButtonList1.SelectedValue == "yes" ? true : false));
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), " Revision of  Document Number <b>" + txt_DocumentNumber.Text + "</b> Initiated Successfully.", "success", "ReloadInitiationPage()");
                    }
                    else if (ddl_requestType.SelectedValue == "3")
                    {
                        docObjects.DocumentID = (ddl_AutoDocName.SelectedValue);
                        docObjects.RequestType = Convert.ToInt32(ddl_requestType.SelectedValue);
                        docObjects.DMS_IntiatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        docObjects.DMSDocControllerID = Convert.ToInt32(ddl_Controller.SelectedValue);
                        docObjects.Purpose = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                        docObjects.isManage = 1;
                        docObjects.action = 0;
                        List<Int_EmployeeBy> _lstQA = new List<Int_EmployeeBy>();
                        foreach (GridViewRow row in gvApprover.Rows)
                        {
                            if ((row.Cells[0].Text) == "Initiation Request Approver")
                            {
                                Int_EmployeeBy _Objint_EmployeeBy = new Int_EmployeeBy();
                                _Objint_EmployeeBy.EmpID = Convert.ToInt32((row.Cells[3].FindControl("lblEmpId") as TextBox).Text);
                                _Objint_EmployeeBy.DeptID = Convert.ToInt32((row.Cells[3].FindControl("lblDeptID") as TextBox).Text);
                                _lstQA.Add(_Objint_EmployeeBy);
                            }
                        }
                        docObjects.lstQA = _lstQA;
                        DMS_ins = DMS_Bal.DMS_DocumentUpdationWithdrawn(docObjects);
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), " Withdrawal of  Document Number <b>" + txt_DocumentNumber.Text + "</b> Initiated Successfully.", "success", "ReloadInitiationPage()");
                    }
                    else if (ddl_requestType.SelectedValue == "5")
                    {
                        docObjects.DocumentID = (ddl_AutoDocName.SelectedValue);
                        docObjects.RequestType = Convert.ToInt32(ddl_requestType.SelectedValue);
                        docObjects.DMS_IntiatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        docObjects.DMSDocControllerID = Convert.ToInt32(ddl_Controller.SelectedValue);
                        if (ViewState["RenewAuthorShow"].ToString() == "1")
                        {
                            docObjects.DMS_CreatedBy = Convert.ToInt32(ddl_CreatedBy.SelectedValue);
                        }
                        docObjects.Purpose = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                        docObjects.isManage = 1;
                        docObjects.action = 0;
                        List<Int_EmployeeBy> _lstQA = new List<Int_EmployeeBy>();
                        List<Int_EmployeeBy> _lstReviewBy = new List<Int_EmployeeBy>();
                        List<Int_EmployeeBy> _lstApproverBy = new List<Int_EmployeeBy>();
                        foreach (GridViewRow row in gvApprover.Rows)
                        {
                            if ((row.Cells[0].Text) == "Initiation Request Approver")
                            {
                                Int_EmployeeBy _Objint_EmployeeBy = new Int_EmployeeBy();
                                _Objint_EmployeeBy.EmpID = Convert.ToInt32((row.Cells[3].FindControl("lblEmpId") as TextBox).Text);
                                _Objint_EmployeeBy.DeptID = Convert.ToInt32((row.Cells[3].FindControl("lblDeptID") as TextBox).Text);
                                _lstQA.Add(_Objint_EmployeeBy);
                            }
                            else if ((row.Cells[0].Text) == "Document Reviewer")
                            {
                                Int_EmployeeBy _Objint_EmployeeBy = new Int_EmployeeBy();
                                _Objint_EmployeeBy.EmpID = Convert.ToInt32((row.Cells[3].FindControl("lblEmpId") as TextBox).Text);
                                _Objint_EmployeeBy.DeptID = Convert.ToInt32((row.Cells[3].FindControl("lblDeptID") as TextBox).Text);
                                _lstReviewBy.Add(_Objint_EmployeeBy);
                            }
                            else if ((row.Cells[0].Text) == "Document Approver")
                            {
                                Int_EmployeeBy _Objint_EmployeeBy = new Int_EmployeeBy();
                                _Objint_EmployeeBy.EmpID = Convert.ToInt32((row.Cells[3].FindControl("lblEmpId") as TextBox).Text);
                                _Objint_EmployeeBy.DeptID = Convert.ToInt32((row.Cells[3].FindControl("lblDeptID") as TextBox).Text);
                                _lstApproverBy.Add(_Objint_EmployeeBy);
                            }
                        }
                        docObjects.lstQA = _lstQA;
                        docObjects.lstReviewBy = _lstReviewBy;
                        docObjects.lstApproveBy = _lstApproverBy;
                        DMS_ins = DMS_Bal.DMS_DocumentInitiationRenew(docObjects);
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), " Renewal of  Document Number <b>" + txt_DocumentNumber.Text + "</b> Initiated Successfully.", "success", "ReloadInitiationPage()");
                    }
                    ShowDcoumentList();
                }
                else if (hdnConfirm.Value == "Update")
                {
                    StringBuilder sbErrorMsg = new StringBuilder();
                    DataTable dt = DMS_Bal.DMS_GetDocByID(Request.QueryString["DocumentID"].ToString());
                    if (ddl_requestType.SelectedValue == "1")
                    {
                        if (Request.QueryString["From"] == "R")
                        {
                            docObjects.Status = 1;
                            docObjects.isManage = 1;
                            docObjects.Remarks = "N/A";
                        }
                        else if (Request.QueryString["From"] == "M")
                        {
                            docObjects.Status = 0;
                            docObjects.isManage = 0;
                            docObjects.Remarks = Regex.Replace(txt_Remarks.Value.Trim(), @"\s+", " ");
                        }
                        docObjects.documentNUmber = Regex.Replace(txt_DocumentNumber.Text.Trim(), @"\s+", " ");
                        docObjects.DocumentID = (Request.QueryString["DocumentID"].ToString());
                        docObjects.DMS_DocumentName = Regex.Replace(txt_DocumentName.Value.Trim(), @"\s+", " ");
                        docObjects.DMS_DocumentType = Convert.ToInt32(ddl_DocumentType.SelectedValue);
                        docObjects.DMS_ModifiedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        if (ddl_Initiator.SelectedValue == "0")
                        {
                            docObjects.DMS_IntiatedBy = 0;
                        }
                        //else if(ddl_Initiator.SelectedValue == dt.Rows[0]["IntiatorID"].ToString())
                        //{
                        //    docObjects.DMS_IntiatedBy = 0;
                        //}
                        else
                        {
                            docObjects.DMS_IntiatedBy = Convert.ToInt32(ddl_Initiator.SelectedValue);
                        }
                        if (ddl_CreatedBy.SelectedValue == "0")
                        {
                            docObjects.DMS_CreatedBy = 0;
                        }
                        //else if (ddl_CreatedBy.SelectedValue == dt.Rows[0]["authorID"].ToString())
                        //{
                        //    docObjects.DMS_CreatedBy = 0;
                        //}
                        else
                        {
                            docObjects.DMS_CreatedBy = Convert.ToInt32(ddl_CreatedBy.SelectedValue);
                        }
                        
                        List<Int_EmployeeBy> _lstQA = new List<Int_EmployeeBy>();
                        List<Int_EmployeeBy> _lstReviewBy = new List<Int_EmployeeBy>();
                        List<Int_EmployeeBy> _lstApproverBy = new List<Int_EmployeeBy>();
                        foreach (GridViewRow row in gvApprover.Rows)
                        {
                            if ((row.Cells[0].Text) == "Initiation Request Approver")
                            {
                                Int_EmployeeBy _Objint_EmployeeBy = new Int_EmployeeBy();
                                _Objint_EmployeeBy.EmpID = Convert.ToInt32((row.Cells[3].FindControl("lblEmpId") as TextBox).Text);
                                _Objint_EmployeeBy.DeptID = Convert.ToInt32((row.Cells[3].FindControl("lblDeptID") as TextBox).Text);
                                _lstQA.Add(_Objint_EmployeeBy);
                            }
                            else if ((row.Cells[0].Text) == "Document Reviewer")
                            {
                                Int_EmployeeBy _Objint_EmployeeBy = new Int_EmployeeBy();
                                _Objint_EmployeeBy.EmpID = Convert.ToInt32((row.Cells[3].FindControl("lblEmpId") as TextBox).Text);
                                _Objint_EmployeeBy.DeptID = Convert.ToInt32((row.Cells[3].FindControl("lblDeptID") as TextBox).Text);
                                _lstReviewBy.Add(_Objint_EmployeeBy);
                            }
                            else if ((row.Cells[0].Text) == "Document Approver")
                            {
                                Int_EmployeeBy _Objint_EmployeeBy = new Int_EmployeeBy();
                                _Objint_EmployeeBy.EmpID = Convert.ToInt32((row.Cells[3].FindControl("lblEmpId") as TextBox).Text);
                                _Objint_EmployeeBy.DeptID = Convert.ToInt32((row.Cells[3].FindControl("lblDeptID") as TextBox).Text);
                                _lstApproverBy.Add(_Objint_EmployeeBy);
                            }
                        }
                        docObjects.lstQA = _lstQA;
                        docObjects.lstReviewBy = _lstReviewBy;
                        docObjects.lstApproveBy = _lstApproverBy;
                        docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName.SelectedValue);
                        docObjects.RequestType = Convert.ToInt32(ddl_requestType.SelectedValue);
                        docObjects.authorBy = Convert.ToInt32(ddl_authorizedBy.SelectedValue);
                        docObjects.Purpose = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                        int DMS_ins = DMS_Bal.DMS_DocumentEdit(docObjects, (RadioButtonList1.SelectedValue == "yes" ? true : false));
                        if (Request.QueryString["From"] == "M")
                        {
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "New Document Number <b>" + txt_DocumentNumber.Text + "</b> Updated Successfully.", "success", "ReloadManagePage()");
                        }
                        else if (Request.QueryString["From"] == "R")
                        {
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "New  Document Number <b>" + txt_DocumentNumber.Text + "</b> Updated Successfully.", "success", "ReloadRevertPage()");
                        }
                    }
                    else if (ddl_requestType.SelectedValue == "2")
                    {
                        if (Request.QueryString["From"] == "R")
                        {
                            docObjects.Status = 0;
                            docObjects.isManage = 1;
                            docObjects.Remarks = "N/A";
                        }
                        else if (Request.QueryString["From"] == "M")
                        {
                            docObjects.Status = 1;
                            docObjects.isManage = 0;
                            docObjects.Remarks = Regex.Replace(txt_Remarks.Value.Trim(), @"\s+", " ");
                        }
                        docObjects.DocumentID = (Request.QueryString["DocumentID"].ToString());
                        docObjects.DMS_DocumentName = Regex.Replace(txt_DocumentName.Value.Trim(), @"\s+", " ");
                        docObjects.DMS_ModifiedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        if (ddl_Initiator.SelectedValue == "0")
                        {
                            docObjects.DMS_IntiatedBy = 0;
                        }
                        //else if (ddl_Initiator.SelectedValue == dt.Rows[0]["IntiatorID"].ToString())
                        //{
                        //    docObjects.DMS_IntiatedBy = 0;
                        //}
                        else
                        {
                            docObjects.DMS_IntiatedBy = Convert.ToInt32(ddl_Initiator.SelectedValue);
                        }
                        if (ddl_CreatedBy.SelectedValue == "0")
                        {
                            docObjects.DMS_CreatedBy = 0;
                        }
                        //else if (ddl_CreatedBy.SelectedValue == dt.Rows[0]["authorID"].ToString())
                        //{
                        //    docObjects.DMS_CreatedBy = 0;
                        //}
                        else
                        {
                            docObjects.DMS_CreatedBy = Convert.ToInt32(ddl_CreatedBy.SelectedValue);
                        }
                        List<Int_EmployeeBy> _lstQA = new List<Int_EmployeeBy>();
                        List<Int_EmployeeBy> _lstReviewBy = new List<Int_EmployeeBy>();
                        List<Int_EmployeeBy> _lstApproverBy = new List<Int_EmployeeBy>();
                        foreach (GridViewRow row in gvApprover.Rows)
                        {
                            if ((row.Cells[0].Text) == "Initiation Request Approver")
                            {
                                Int_EmployeeBy _Objint_EmployeeBy = new Int_EmployeeBy();
                                _Objint_EmployeeBy.EmpID = Convert.ToInt32((row.Cells[3].FindControl("lblEmpId") as TextBox).Text);
                                _Objint_EmployeeBy.DeptID = Convert.ToInt32((row.Cells[3].FindControl("lblDeptID") as TextBox).Text);
                                _lstQA.Add(_Objint_EmployeeBy);
                            }
                            else if ((row.Cells[0].Text) == "Document Reviewer")
                            {
                                Int_EmployeeBy _Objint_EmployeeBy = new Int_EmployeeBy();
                                _Objint_EmployeeBy.EmpID = Convert.ToInt32((row.Cells[3].FindControl("lblEmpId") as TextBox).Text);
                                _Objint_EmployeeBy.DeptID = Convert.ToInt32((row.Cells[3].FindControl("lblDeptID") as TextBox).Text);
                                _lstReviewBy.Add(_Objint_EmployeeBy);
                            }
                            else if ((row.Cells[0].Text) == "Document Approver")
                            {
                                Int_EmployeeBy _Objint_EmployeeBy = new Int_EmployeeBy();
                                _Objint_EmployeeBy.EmpID = Convert.ToInt32((row.Cells[3].FindControl("lblEmpId") as TextBox).Text);
                                _Objint_EmployeeBy.DeptID = Convert.ToInt32((row.Cells[3].FindControl("lblDeptID") as TextBox).Text);
                                _lstApproverBy.Add(_Objint_EmployeeBy);
                            }
                        }
                        docObjects.lstQA = _lstQA;
                        docObjects.lstReviewBy = _lstReviewBy;
                        docObjects.lstApproveBy = _lstApproverBy;
                        docObjects.authorBy = Convert.ToInt32(ddl_authorizedBy.SelectedValue);
                        docObjects.Purpose = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                        int DMS_ins = DMS_Bal.DMS_DocumentUpdationEdit(docObjects, (RadioButtonList1.SelectedValue == "yes" ? true : false));
                        if (Request.QueryString["From"] == "M")
                        {
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Revision of Document Number <b>" + txt_DocumentNumber.Text + "</b> Updated Successfully.", "success", "ReloadManagePage()");
                        }
                        else if (Request.QueryString["From"] == "R")
                        {
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Revision  Document Number <b>" + txt_DocumentNumber.Text + "</b> Updated Successfully.", "success", "ReloadRevertPage()");
                        }
                    }
                    else if (ddl_requestType.SelectedValue == "3")
                    {
                        int DMS_ins;
                        docObjects.DocumentID = ViewState["DocID"].ToString();
                        docObjects.RequestType = Convert.ToInt32(ddl_requestType.SelectedValue);                                              
                        docObjects.DMS_IntiatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        docObjects.DMSDocControllerID = Convert.ToInt32(ddl_Controller.SelectedValue);
                        docObjects.Purpose = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                        docObjects.isManage = 2;
                        docObjects.action = 1;
                        docObjects.Remarks = "N/A";
                        List<Int_EmployeeBy> _lstQA = new List<Int_EmployeeBy>();
                        foreach (GridViewRow row in gvApprover.Rows)
                        {
                            if ((row.Cells[0].Text) == "Initiation Request Approver")
                            {
                                Int_EmployeeBy _Objint_EmployeeBy = new Int_EmployeeBy();
                                _Objint_EmployeeBy.EmpID = Convert.ToInt32((row.Cells[3].FindControl("lblEmpId") as TextBox).Text);
                                _Objint_EmployeeBy.DeptID = Convert.ToInt32((row.Cells[3].FindControl("lblDeptID") as TextBox).Text);
                                _lstQA.Add(_Objint_EmployeeBy);
                            }
                        }
                        docObjects.lstQA = _lstQA;
                        if(Request.QueryString["From"] == "M")
                        {
                            docObjects.isManage = 0;
                            docObjects.action = 2;
                            docObjects.Remarks = Regex.Replace(txt_Remarks.Value.Trim(), @"\s+", " ");
                            if (ddl_Initiator.SelectedValue == "0")
                            {
                                docObjects.DMS_ModifiedBy = 0;
                            }
                            else if (ddl_Initiator.SelectedValue == dt.Rows[0]["IntiatorID"].ToString())
                            {
                                docObjects.DMS_ModifiedBy = 0;
                            }
                            else
                            {
                                docObjects.DMS_ModifiedBy = Convert.ToInt32(ddl_Initiator.SelectedValue);
                            }                            
                            DMS_ins = DMS_Bal.DMS_DocumentUpdationWithdrawn(docObjects);
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Withdrawal of Document Number <b>" + txt_DocumentNumber.Text + "</b> Updated Successfully.", "success", "ReloadManagePage()");
                        }
                        else
                        {
                            DMS_ins = DMS_Bal.DMS_DocumentUpdationWithdrawn(docObjects);
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Withdrawal of Document Number <b>" + txt_DocumentNumber.Text + "</b> Updated Successfully.", "success", "ReloadRevertPage()");
                        }
                        
                        
                    }
                    else if (ddl_requestType.SelectedValue == "5")
                    {
                        int DMS_ins;
                        docObjects.DocumentID = ViewState["DocID"].ToString();
                        docObjects.RequestType = Convert.ToInt32(ddl_requestType.SelectedValue);
                        docObjects.DMS_IntiatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        docObjects.DMSDocControllerID = Convert.ToInt32(ddl_Controller.SelectedValue);
                        if (ViewState["RenewAuthorShow"].ToString() == "1")
                        {
                            docObjects.DMS_CreatedBy = Convert.ToInt32(ddl_CreatedBy.SelectedValue);
                        }
                        else
                        {
                            docObjects.DMS_CreatedBy = 0;
                        }
                        docObjects.Purpose = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                        docObjects.isManage = 1;
                        docObjects.action = 1;
                        docObjects.Remarks = "N/A";
                        List<Int_EmployeeBy> _lstQA = new List<Int_EmployeeBy>();
                        List<Int_EmployeeBy> _lstReviewBy = new List<Int_EmployeeBy>();
                        List<Int_EmployeeBy> _lstApproverBy = new List<Int_EmployeeBy>();
                        foreach (GridViewRow row in gvApprover.Rows)
                        {
                            if ((row.Cells[0].Text) == "Initiation Request Approver")
                            {
                                Int_EmployeeBy _Objint_EmployeeBy = new Int_EmployeeBy();
                                _Objint_EmployeeBy.EmpID = Convert.ToInt32((row.Cells[3].FindControl("lblEmpId") as TextBox).Text);
                                _Objint_EmployeeBy.DeptID = Convert.ToInt32((row.Cells[3].FindControl("lblDeptID") as TextBox).Text);
                                _lstQA.Add(_Objint_EmployeeBy);
                            }
                            else if ((row.Cells[0].Text) == "Document Reviewer")
                            {
                                Int_EmployeeBy _Objint_EmployeeBy = new Int_EmployeeBy();
                                _Objint_EmployeeBy.EmpID = Convert.ToInt32((row.Cells[3].FindControl("lblEmpId") as TextBox).Text);
                                _Objint_EmployeeBy.DeptID = Convert.ToInt32((row.Cells[3].FindControl("lblDeptID") as TextBox).Text);
                                _lstReviewBy.Add(_Objint_EmployeeBy);
                            }
                            else if ((row.Cells[0].Text) == "Document Approver")
                            {
                                Int_EmployeeBy _Objint_EmployeeBy = new Int_EmployeeBy();
                                _Objint_EmployeeBy.EmpID = Convert.ToInt32((row.Cells[3].FindControl("lblEmpId") as TextBox).Text);
                                _Objint_EmployeeBy.DeptID = Convert.ToInt32((row.Cells[3].FindControl("lblDeptID") as TextBox).Text);
                                _lstApproverBy.Add(_Objint_EmployeeBy);
                            }
                        }
                        docObjects.lstQA = _lstQA;
                        docObjects.lstReviewBy = _lstReviewBy;
                        docObjects.lstApproveBy = _lstApproverBy;
                        if (Request.QueryString["From"] == "M")
                        {
                            docObjects.isManage = 0;
                            docObjects.action = 2;
                            docObjects.Remarks = Regex.Replace(txt_Remarks.Value.Trim(), @"\s+", " ");
                            if (ddl_Initiator.SelectedValue == "0")
                            {
                                docObjects.DMS_ModifiedBy = 0;
                            }
                            else if (ddl_Initiator.SelectedValue == dt.Rows[0]["IntiatorID"].ToString())
                            {
                                docObjects.DMS_ModifiedBy = 0;
                            }
                            else
                            {
                                docObjects.DMS_ModifiedBy = Convert.ToInt32(ddl_Initiator.SelectedValue);
                            }
                            DMS_ins = DMS_Bal.DMS_DocumentUpdationRenew(docObjects);
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Renewal of Document Number <b>" + txt_DocumentNumber.Text + "</b> Updated Successfully.", "success", "ReloadManagePage()");
                        }
                        else
                        {
                            DMS_ins = DMS_Bal.DMS_DocumentUpdationRenew(docObjects);
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Renewal of Document Number <b>" + txt_DocumentNumber.Text + "</b> Updated Successfully.", "success", "ReloadRevertPage()");
                        }
                    }
                    ShowDcoumentList();
                }
                else if (hdnConfirm.Value == "Reject")
                {
                    ViewState["Action"] = "Reject";
                    ShowDcoumentList();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop4", "openUC_ElectronicSign();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M4:" + strline + "  " + "Document submit/update failed.", "error");
            }
        }
        public void DMS_GetDocumentTypeList()
        {
            try
            {
                JQDataTableBO objJQDataTableBO = new JQDataTableBO();
                objJQDataTableBO.iMode = 0;
                objJQDataTableBO.pkid = 0;
                objJQDataTableBO.iDisplayLength = 0;
                objJQDataTableBO.iDisplayStart = 0;
                objJQDataTableBO.iSortCol = 0;
                objJQDataTableBO.sSortDir = "";
                objJQDataTableBO.sSearch = "";
                DataTable DMS_Dt = DMS_Bal.DMS_GetDocumentTypeList(objJQDataTableBO);
                if (DMS_Dt.Rows.Count > 0)
                {
                    ddl_DocumentType.DataValueField = DMS_Dt.Columns["DocumentTypeID"].ToString();
                    ddl_DocumentType.DataTextField = DMS_Dt.Columns["DocumentType"].ToString();
                    ddl_DocumentType.DataSource = DMS_Dt;
                    ddl_DocumentType.DataBind();
                    ddl_DocumentType.Items.Insert(0, new ListItem("--Select Document Type--", "0"));
                }
                else
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), " Document Types not available , please contact admin", "error", "ReloadDashboard();");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M5:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M5:" + strline + "  " + "Document Type load failed.", "error");
            }
        }
        public void DMS_GetApproverreviewerList()
        {
            try
            {
                docObjects.dept = "0";
                docObjects.UserRole = "R";
                DataTable DMS_Dt = DMS_Bal.DMS_GetApproverReviewerList(docObjects);
                docObjects.DMS_Deparment = 0;
                docObjects.UserRole = "A";
                DataTable DMS_Dt1 = DMS_Bal.DMS_GetApproverReviewerList(docObjects);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M6:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M6:" + strline + "  " + "Approver/reviewer list load failed.", "error");
            }
        }
        public void DMS_GetRequestTypeList()
        {
            try
            {
                //DataRow[] dRows = DMS_Bal.DMS_GetRequestTypeList().Select("RequestTypeID<>4 and RequestTypeID<>3");
                DataTable DMS_Dt = DMS_Bal.DMS_GetRequestTypeList();
                if (DMS_Dt.Rows.Count > 0)
                {
                    ddl_requestType.DataValueField = DMS_Dt.Columns["RequestTypeID"].ToString();
                    ddl_requestType.DataTextField = DMS_Dt.Columns["RequestType"].ToString();
                    ddl_requestType.DataSource = DMS_Dt;
                    ddl_requestType.DataBind();
                    ddl_requestType.Items.Insert(0, new ListItem("--Select Request Type--", "0"));
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop5", "custAlertMsg('There are no request types.','error');", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M7:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M7:" + strline + "  " + "Request Type load failed.", "error");
            }
        }
        public void DDl_Authorize()
        {
            if (Request.QueryString["DocumentID"] == null)
            {
                ddlAuthor();
            }
            if (Request.QueryString["DocumentID"] != null)
            {
            }
        }
        public void ddlAuthor()
        {
            try
            {
                if (ViewState["dtRemoveEmpID"] != null)
                {
                    DataTable dtTemp = ViewState["dtRemoveEmpID"] as DataTable;
                    if (dtTemp.Rows.Count > 0)
                    {
                        DataRow[] rows;
                        rows = dtTemp.Select("Control = 'AU'");  //'UserName' is ColumnName
                        foreach (DataRow row in rows)
                            dtTemp.Rows.Remove(row);
                    }
                    docObjects.UserRole = "13";
                    DataTable DMS_Dt = DMS_Bal.DMS_GetApproverList(docObjects);
                    if (DMS_Dt.Rows.Count > 0)
                    {
                        if (dtTemp.Rows.Count > 0)
                        {
                            if (DMS_Dt.Rows.Count > 0)
                            {
                                for (int j = 0; j < dtTemp.Rows.Count; j++)
                                {
                                    for (int i = 0; i < DMS_Dt.Rows.Count; i++)
                                    {
                                        int EmpID = Convert.ToInt32(DMS_Dt.Rows[i]["EmpID"].ToString());
                                        int RemoveID = Convert.ToInt32(dtTemp.Rows[j]["EmpID"].ToString());
                                        string Role = dtTemp.Rows[j]["Role"].ToString();
                                        if (EmpID == RemoveID && Role != "Initiation Request Approver")
                                        {
                                            DMS_Dt.Rows.RemoveAt(i);
                                        }
                                    }
                                }
                            }
                            ddl_authorizedBy.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                            ddl_authorizedBy.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                            ddl_authorizedBy.DataSource = DMS_Dt;
                            ddl_authorizedBy.DataBind();
                            ddl_authorizedBy.Items.Insert(0, new ListItem("--Select Authorizer--", "0"));
                        }
                        ddl_authorizedBy.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                        ddl_authorizedBy.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                        ddl_authorizedBy.DataSource = DMS_Dt;
                        ddl_authorizedBy.DataBind();
                        ddl_authorizedBy.Items.Insert(0, new ListItem("--Select Authorizer--", "0"));
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M8:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M8:" + strline + "  " + "Authorizer load failed.", "error");
            }
        }
        public void getuserroles(string AuthorizerID="")
        {
            try
            {
                docObjects.UserRole = "11";
                DataTable DMS_List = DMS_Bal.DMS_GetApproverList(docObjects);
                if (DMS_List.Rows.Count > 0)
                {
                    ddl_CreatedBy.DataSource = DMS_List;
                    ddl_CreatedBy.DataValueField = DMS_List.Columns["EmpID"].ToString();
                    ddl_CreatedBy.DataTextField = DMS_List.Columns["EmpName"].ToString();
                    ddl_CreatedBy.DataBind();
                    ddl_CreatedBy.Items.Insert(0, new ListItem("--Select Author--", "0"));
                    docObjects.UserRole = "13";
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop6", "custAlertMsg('There are no employees for author role.','error');", true);
                }
                DataTable DMS_Dt = DMS_Bal.DMS_GetApproverList(docObjects);
                if (DMS_Dt.Rows.Count > 0)
                {
                    ddl_authorizedBy.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                    ddl_authorizedBy.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                    ddl_authorizedBy.DataSource = DMS_Dt;
                    ddl_authorizedBy.DataBind();
                    ddl_authorizedBy.Items.Insert(0, new ListItem("--Select Authorizer--", "0"));
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop7", "custAlertMsg('There are no employees for authorizer role.','error');", true);
                }
                if (Session["uniqueemployee"] != null)
                {
                    DataTable dt = (DataTable)Session["uniqueemployee"];
                    foreach (DataRow row in dt.Rows)
                    {
                        string value = row["Role"].ToString();
                        if (value == "Author")
                        {
                            ddl_CreatedBy.Items.Remove(ddl_CreatedBy.Items.FindByValue(row["EmpID"].ToString()));
                        }
                        if (value == "Creator")
                        {
                            ddl_authorizedBy.Items.Remove(ddl_authorizedBy.Items.FindByValue(row["EmpID"].ToString()));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M9:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M9:" + strline + "  " + "Author/Authorizer load failed.", "error");
            }
        }
        public void DMS_GetDepartmentList()
        {
            try
            {
                docObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                if (Request.QueryString["From"] == "M")
                {
                    docObjects.RoleID = 2;//2-Admin
                }
                else
                {
                    docObjects.RoleID = 12;//12-Initiator
                }
                DataTable DMS_Dt = DMS_BalDM.DMS_EmpModuleDepartmentFilling(docObjects);
                ddl_DeptName.DataValueField = DMS_Dt.Columns["DeptID"].ToString();
                ddl_DeptName.DataTextField = DMS_Dt.Columns["DepartmentName"].ToString();
                ddl_DeptName.DataSource = DMS_Dt;
                ddl_DeptName.DataBind();
                ddl_DeptName.Items.Insert(0, new ListItem("--Select Department--", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M10:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M10:" + strline + "  " + "Department load failed.", "error");
            }
        }
        // Reset Button Click Event
        protected void btn_Reset_Click(object sender, EventArgs e)
        {
            btn_Reset.Enabled = false;
            try
            {
                Reset();
                ShowDcoumentList();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M11:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M11:" + strline + "  " + "Reset failed.", "error");
            }
            finally
            {
                btn_Reset.Enabled = true;
            }
        }
        public void Reset()
        {
            Response.Redirect("DocumentIntiation.aspx", false);
        }
        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            btn_Submit.Enabled = false;
            try
            {
                StringBuilder sbErrorMsg = new StringBuilder();
                if (ddl_requestType.SelectedValue == "1")
                {
                    sbErrorMsg.Clear();
                    if (ddl_DeptName.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append(" Select Department." + "<br/>");
                    }
                    if (ddl_DocumentType.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append(" Select Document Type." + "<br/>");
                    }
                    else if(ddl_DocumentType.SelectedValue.ToString().Trim() == "" || ddl_DocumentType.SelectedValue.ToString().Trim() == null)
                    {
                        sbErrorMsg.Append(" No Document Type/s present." + "<br/>");
                    }
                    if (txt_DocumentNumber.Text.Trim() == "")
                    {
                        sbErrorMsg.Append(" Enter Document Number." + "<br/>");
                    }
                    if (!string.IsNullOrEmpty(txt_DocumentNumber.Text.Trim()))
                    {
                        docObjects.documentNUmber = txt_DocumentNumber.Text.Trim();
                        DataTable DMS_dt = DMS_Bal.DMS_DocumentNumberSearch(docObjects);
                        if (DMS_dt.Rows.Count > 0)
                        {
                            sbErrorMsg.Append("Document Number Already exists, Please Enter another Number." + "<br/>");
                        }
                    }
                    if (txt_DocumentName.Value.Trim() == "")
                    {
                        sbErrorMsg.Append(" Enter Document Name." + "<br/>");
                    }
                    //if (!string.IsNullOrEmpty(txt_DocumentName.Value.Trim()))
                    //{

                    //    docObjects.DMS_DocumentName = txt_DocumentName.Value.Trim();
                    //    docObjects.dept = ddl_DeptName.SelectedValue;
                    //    DataTable DMS_dt = DMS_Bal.DMS_DocumentNameSearch(docObjects);
                    //    if (DMS_dt.Rows.Count > 0)
                    //    {
                    //        sbErrorMsg.Append("Document Name Already exists, Please Enter another Name." + "<br/>");
                    //    }
                    //}
                    if (txt_Comments.Value.Trim() == "")
                    {
                        sbErrorMsg.Append(" Enter Purpose of Initiation." + "<br/>");
                    }
                    if (ddl_CreatedBy.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append(" Select Author." + "<br/>");
                    }
                    int QACount = 0, ReviewerCount = 0, ApproverCount = 0;
                    foreach (GridViewRow row in gvApprover.Rows)
                    {
                        if ((row.Cells[0].Text) == "Initiation Request Approver")
                        {
                            QACount = QACount + 1;
                        }
                        else if ((row.Cells[0].Text) == "Document Reviewer")
                        {
                            ReviewerCount = ReviewerCount + 1;
                        }
                        else if ((row.Cells[0].Text) == "Document Approver")
                        {
                            ApproverCount = ApproverCount + 1;
                        }
                    }
                    if (QACount == 0)
                    {
                        sbErrorMsg.Append(" Select at-least one Initiation Request Approver." + "<br/>");
                    }
                    if (ReviewerCount == 0)
                    {
                        sbErrorMsg.Append(" Select at-least one Document Reviewer." + "<br/>");
                    }
                    if (ApproverCount == 0)
                    {
                        sbErrorMsg.Append(" Select at-least one Document Approver.");
                    }
                    if (sbErrorMsg.Length > 8)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
                        return;
                    }
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop1", "ConformAlertSubmit();", true);
                }
                else if (ddl_requestType.SelectedValue == "2")
                {
                    sbErrorMsg.Clear();
                    if (ddl_DeptName.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append(" Select Department." + "<br/>");
                    }
                    if (ddl_DocumentType.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append(" Select Document Type." + "<br/>");
                    }
                    else if (ddl_DocumentType.SelectedValue.ToString().Trim() == "" || ddl_DocumentType.SelectedValue.ToString().Trim() == null)
                    {
                        sbErrorMsg.Append(" No Document Type/s present." + "<br/>");
                    }
                    if (ViewState["DocNameChange"].ToString() != "1" && (ddl_AutoDocName.SelectedValue.ToString().Trim() == "0" || ddl_AutoDocName.SelectedValue.ToString().Trim() == ""))
                    {
                        sbErrorMsg.Append(" Select Document Name." + "<br/>");
                    }
                    else if (ViewState["DocNameChange"].ToString() == "1")
                    {
                        if (txt_DocumentName.Value.Trim() == "")
                        {
                            sbErrorMsg.Append(" Enter Document Name." + "<br/>");
                        }
                        //else if (!string.IsNullOrEmpty(txt_DocumentName.Value.Trim()))
                        //{

                        //    docObjects.DMS_DocumentName = txt_DocumentName.Value.Trim();
                        //    docObjects.dept = ddl_DeptName.SelectedValue;
                        //    DataTable DMS_dt = DMS_Bal.DMS_DocumentNameSearch(docObjects);
                        //    if (DMS_dt.Rows.Count > 0)
                        //    {
                        //        sbErrorMsg.Append("Document Name Already exists, Please Enter another Name." + "<br/>");
                        //    }
                        //}
                    }
                    
                    if (txt_Comments.Value.Trim() == "")
                    {
                        sbErrorMsg.Append(" Enter Reason for Revision." + "<br/>");
                    }
                    if (ddl_CreatedBy.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append(" Select Author." + "<br/>");
                    }
                    int QACount = 0, ReviewerCount = 0, ApproverCount = 0;
                    foreach (GridViewRow row in gvApprover.Rows)
                    {
                        if ((row.Cells[0].Text) == "Initiation Request Approver")
                        {
                            QACount = QACount + 1;
                        }
                        else if ((row.Cells[0].Text) == "Document Reviewer")
                        {
                            ReviewerCount = ReviewerCount + 1;
                        }
                        else if ((row.Cells[0].Text) == "Document Approver")
                        {
                            ApproverCount = ApproverCount + 1;
                        }
                    }
                    if (QACount == 0)
                    {
                        sbErrorMsg.Append(" Select at-least one Initiation Request Approver." + "<br/>");
                    }
                    if (ReviewerCount == 0)
                    {
                        sbErrorMsg.Append(" Select at-least one Document Reviewer." + "<br/>");
                    }
                    if (ApproverCount == 0)
                    {
                        sbErrorMsg.Append(" Select at-least one Document Approver.");
                    }
                    if (sbErrorMsg.Length > 8)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
                        return;
                    }
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop1", "ConformAlertSubmit();", true);
                }
                else if (ddl_requestType.SelectedValue == "3")
                {
                    sbErrorMsg.Clear();
                    if (ddl_DeptName.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append(" Select Department." + "<br/>");
                    }
                    if (ddl_DocumentType.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append(" Select Document Type." + "<br/>");
                    }
                    else if (ddl_DocumentType.SelectedValue.ToString().Trim() == "" || ddl_DocumentType.SelectedValue.ToString().Trim() == null)
                    {
                        sbErrorMsg.Append(" No Document Type/s present." + "<br/>");
                    }
                    if (ddl_AutoDocName.SelectedValue.ToString().Trim() == "0" || ddl_AutoDocName.SelectedValue.ToString().Trim() == "")
                    {
                        sbErrorMsg.Append(" Select Document Name." + "<br/>");
                    }                    
                    if (txt_Comments.Value.Trim() == "")
                    {
                        sbErrorMsg.Append(" Enter Reason for Withdrawal." + "<br/>");
                    }
                    if (ddl_Controller.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append(" Select Controller." + "<br/>");
                    }
                    int QACount = 0;
                    foreach (GridViewRow row in gvApprover.Rows)
                    {
                        if ((row.Cells[0].Text) == "Initiation Request Approver")
                        {
                            QACount = QACount + 1;
                        }
                    }
                    if (QACount == 0)
                    {
                        sbErrorMsg.Append(" Select at-least one Initiation Request Approver." + "<br/>");
                    }
                    if (sbErrorMsg.Length > 8)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
                        return;
                    }
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop1", "ConformAlertSubmit();", true);
                }
                else if (ddl_requestType.SelectedValue == "5")
                {
                    sbErrorMsg.Clear();
                    if (ddl_DeptName.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append(" Select Department." + "<br/>");
                    }
                    if (ddl_DocumentType.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append(" Select Document Type." + "<br/>");
                    }
                    else if (ddl_DocumentType.SelectedValue.ToString().Trim() == "" || ddl_DocumentType.SelectedValue.ToString().Trim() == null)
                    {
                        sbErrorMsg.Append(" No Document Type/s present." + "<br/>");
                    }
                    if (ddl_AutoDocName.SelectedValue.ToString().Trim() == "0" || ddl_AutoDocName.SelectedValue.ToString().Trim() == "")
                    {
                        sbErrorMsg.Append(" Select Document Name." + "<br/>");
                    }                                    
                    if (txt_Comments.Value.Trim() == "")
                    {
                        sbErrorMsg.Append(" Enter Reason for Renew." + "<br/>");
                    }
                    if (ViewState["RenewAuthorShow"].ToString() == "1")
                    {
                        if (ddl_CreatedBy.SelectedValue.ToString().Trim() == "0")
                        {
                            sbErrorMsg.Append(" Select Author." + "<br/>");
                        }
                    }
                    if (ddl_Controller.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append(" Select Controller." + "<br/>");
                    }
                    int QACount = 0, ReviewerCount = 0, ApproverCount = 0;
                    foreach (GridViewRow row in gvApprover.Rows)
                    {
                        if ((row.Cells[0].Text) == "Initiation Request Approver")
                        {
                            QACount = QACount + 1;
                        }
                        else if ((row.Cells[0].Text) == "Document Reviewer")
                        {
                            ReviewerCount = ReviewerCount + 1;
                        }
                        else if ((row.Cells[0].Text) == "Document Approver")
                        {
                            ApproverCount = ApproverCount + 1;
                        }
                    }
                    if (QACount == 0)
                    {
                        sbErrorMsg.Append(" Select at-least one Initiation Request Approver." + "<br/>");
                    }
                    if (ReviewerCount == 0)
                    {
                        sbErrorMsg.Append(" Select at-least one Document Reviewer." + "<br/>");
                    }
                    if (ApproverCount == 0)
                    {
                        sbErrorMsg.Append(" Select at-least one Document Approver.");
                    }
                    if (sbErrorMsg.Length > 8)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
                        return;
                    }
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop1", "ConformAlertSubmit();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M12:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M12:" + strline + "  " + "Initiation/Revision/Withdraw validation failed.", "error");
            }
            finally
            {
                ShowDcoumentList();
                btn_Submit.Enabled = true;
            }
            //when exception occurs page align not change
            ShowDcoumentList();
        }
        public void ShowDcoumentList()
        {
            try
            {
                if (Request.QueryString.Count > 0)
                {
                    if (ddl_requestType.SelectedValue == "1" && Request.QueryString["From"] == "M")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "NewDocMan();", true);
                    }
                    if (ddl_requestType.SelectedValue == "2" && Request.QueryString["From"] == "M")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "RevisionDocMan();", true);
                        RadioButtonList1.Enabled = true;
                    }
                    if (ddl_requestType.SelectedValue == "3" && Request.QueryString["From"] == "M")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "WithdrawDocMan();", true);
                    }
                    if (ddl_requestType.SelectedValue == "5" && Request.QueryString["From"] == "M" && ViewState["RenewAuthorShow"].ToString() != "1" )
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "WithdrawDocMan();", true);
                    }
                    if (ddl_requestType.SelectedValue == "5" && Request.QueryString["From"] == "M" && ViewState["RenewAuthorShow"].ToString() == "1")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ExistRenewDocMan();", true);
                    }
                    if (ddl_requestType.SelectedValue == "1" && Request.QueryString["From"] == "R")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "NewDocRev();", true);
                    }
                    if (ddl_requestType.SelectedValue == "2" && Request.QueryString["From"] == "R")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "RevisionDocRev();", true);
                    }
                    if (ddl_requestType.SelectedValue == "3" && Request.QueryString["From"] == "R")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "WithdrawDocRev();", true);
                    }
                    if (ddl_requestType.SelectedValue == "5" && Request.QueryString["From"] == "R" && ViewState["RenewAuthorShow"].ToString() != "1")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "WithdrawDocRev();", true);
                    }
                    if (ddl_requestType.SelectedValue == "5" && Request.QueryString["From"] == "R" && ViewState["RenewAuthorShow"].ToString() == "1")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ExistRenewDocRev();", true);
                    }
                }
                else
                {
                     if (ddl_requestType.SelectedValue == "1")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "NewDocInt();", true);
                        txt_VersionID.Text = "00";
                    }
                    if (ddl_requestType.SelectedValue == "3")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "WithdrawDocInt();", true);
                    }
                    if (ddl_requestType.SelectedValue == "2" && ViewState["DocNameChange"].ToString() != "1")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "RevisionDocInt();", true);
                    }
                    else if (ddl_requestType.SelectedValue == "2" && ViewState["DocNameChange"].ToString() == "1")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "RevisionDocIntNameChange();", true);
                    }
                    if (ddl_requestType.SelectedValue == "5" && ViewState["RenewAuthorShow"].ToString()!="1")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "WithdrawDocInt();", true);
                    }
                    if (ddl_requestType.SelectedValue == "5" && ViewState["RenewAuthorShow"].ToString() == "1")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ExistRenewDocInt();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M13:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M13:" + strline + "  " + "Initiation div styles loading failed.", "error");
            }
        }
        //protected void btnDocNamecheck_Click(object sender, EventArgs e)
        //{
        //    btnDocNamecheck.Enabled = false;
        //    try
        //    {
        //        if (ddl_DeptName.SelectedIndex == 0)
        //        {
        //            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>  select Department.','warning');", true);
        //            ShowDcoumentList();
        //            txt_DocumentName.Value = "";
        //        }
        //        else
        //        {
        //            if (!string.IsNullOrEmpty(txt_DocumentName.Value))
        //            {
        //                docObjects.DMS_DocumentName = txt_DocumentName.Value.Trim();
        //                docObjects.dept = ddl_DeptName.SelectedValue;
        //                DataTable DMS_dt = DMS_Bal.DMS_DocumentNameSearch(docObjects);
        //                if (DMS_dt.Rows.Count > 0)
        //                {
        //                    lblStatus.Visible = true;
        //                    lblStatus.Text = "Document Name Already Taken";
        //                    lblStatus.ForeColor = System.Drawing.Color.Red;
        //                }
        //                else
        //                {
        //                    lblStatus.Visible = true;
        //                    lblStatus.Text = "Document Name Available";
        //                    lblStatus.ForeColor = System.Drawing.Color.Green;
        //                }
        //                ShowDcoumentList();
        //            }
        //            else
        //            {
        //                ShowDcoumentList();
        //                lblStatus.Visible = false;
        //            }
        //        }
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
        //    }
        //    catch (Exception ex)
        //    {
        //        string strline = HelpClass.LineNo(ex);
        //        string strMsg = HelpClass.SQLEscapeString(ex.Message);
        //        Aizant_log.Error("DI_M14:" + strline + "  " + strMsg);
        //        HelpClass.custAlertMsg(this, this.GetType(), "DI_M14:" + strline + "  " + "Document Name Text Changed failed.", "error");
        //    }
        //    finally
        //    {
        //        btnDocNamecheck.Enabled = true;
        //    }
        //}
        public void DMS_Reset()
        {
            try
            {
                txt_DocumentNumber.Text = "";
                ddl_DeptName.SelectedIndex = -1;
                ddl_authorizedBy.SelectedIndex = -1;
                ddl_AutoDocName.SelectedIndex = -1;
                ddl_CreatedBy.SelectedIndex = -1;
                txt_DocumentName.Value = "";
                lblStatus.Visible = false;
                ddl_DocumentType.SelectedIndex = -1;
                txt_Comments.Value = "";
                txt_VersionID.Text = "";
                DMS_GetApproverreviewerList();
                ViewState["RenewAuthorShow"] = "0";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop3", "enableRadioPublic();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M15:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M15:" + strline + "  " + "DMS reset failed.", "error");
            }
        }
        public void ClearGridview()//clear approver and reviewer gridview
        {
            ViewState["ApproverData"] = null;
            gvApprover.DataSource = null;
            gvApprover.DataBind();
        }
        protected void ddl_requestType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddl_DocumentType.SelectedValue == "" || ddl_DocumentType.SelectedValue == null)
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), " Document Types not available , please contact admin", "error", "ReloadDashboard();");
                }
                if (ddl_requestType.SelectedValue == "0")
                {
                    DMS_Reset();
                    ClearGridview();
                    ClearDropDown();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "selectDocInit();", true);
                    div_buttons1.Visible = false;
                    lbl_number.Text = string.Empty;
                    CreateViewState();
                }
                else if (ddl_requestType.SelectedValue == "1")
                {
                        DMS_Reset();
                        ClearGridview();
                        ClearDropDown();
                        DMS_GetRoleName();
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "NewDocInt();", true);
                        div_buttons1.Visible = true;
                        txt_DocumentNumber.Attributes.Clear();
                        txt_VersionID.Text = "00";
                        txt_DocumentName.Disabled = true;
                        txt_DocumentNumber.Enabled = false;
                        lblComments.Text = "Purpose of Initiation";
                        CreateViewState();
                        RadioButtonList1.Enabled = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "enableRadioPublic();", true);
                        btnDocNumcheck.Visible = true;
                }
                else if (ddl_requestType.SelectedValue == "2")
                {                   
                        DMS_Reset();
                        ClearGridview();
                        ClearDropDown();
                        DMS_GetRoleName();
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "RevisionDocInt();", true);
                        txt_DocumentNumber.Attributes.Add("readonly", "readonly");
                        lbl_number.Text = "";
                        ddl_DocumentType.Visible = true;
                        div_buttons1.Visible = true;
                        lblComments.Text = "Reason for Revision";
                        CreateViewState();
                        RadioButtonList1.Enabled = true;
                        btnDocNumcheck.Visible = false;
                        btnNewDocName.Visible = false;
                        ddl_AutoDocName.Items.Clear();
                }
                else if (ddl_requestType.SelectedValue == "3")
                {                 
                        DMS_Reset();
                        ClearDropDown();
                        ClearGridview();
                        DMS_GetRoleName();
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "WithdrawDocInt();", true);
                        txt_DocumentNumber.Attributes.Add("readonly", "readonly");
                        ddl_DocumentType.Visible = true;
                        div_buttons1.Visible = true;
                        lbl_number.Text = "";
                        lblComments.Text = "Reason for Withdrawal";
                        CreateViewState();
                        RadioButtonList1.Enabled = false;
                        btnDocNumcheck.Visible = false;
                        btnNewDocName.Visible = false;
                        ddl_AutoDocName.Items.Clear();
                    ddl_Controller.SelectedValue = "0";
                }
                else if (ddl_requestType.SelectedValue == "5")
                {
                    DMS_Reset();
                    ClearGridview();
                    ClearDropDown();
                    DMS_GetRoleName();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "WithdrawDocInt();", true);
                    txt_DocumentNumber.Attributes.Add("readonly", "readonly");
                    lbl_number.Text = "";
                    ddl_DocumentType.Visible = true;
                    div_buttons1.Visible = true;
                    lblComments.Text = "Reason for Renew";
                    CreateViewState();
                    RadioButtonList1.Enabled = false;
                    btnDocNumcheck.Visible = false;
                    btnNewDocName.Visible = false;
                    ddl_AutoDocName.Items.Clear();
                    ddl_Controller.SelectedValue = "0";
                }
                ViewState["RenewAuthorShow"] = "0";
                RadioButtonList1.SelectedValue = "yes";
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M16:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M16:" + strline + "  " + "Request Type Div Hiding failed.", "error");
            }
        }
        public void getdocumentlist(string type, string dept, string Rtype)
        {
            try
            {
                DocumentManagmentBAL DMS_Bal = new DocumentManagmentBAL();
                docObjects.dept = dept;
                docObjects.DocumentType = type;
                docObjects.RType = Rtype;
                DataTable DMS_dt = DMS_Bal.DMS_GetDocumentNameList(docObjects);
                ddl_AutoDocName.DataSource = DMS_dt;
                if(Rtype=="R" || Rtype == "W" || Rtype == "Re")
                {
                    ddl_AutoDocName.DataValueField = DMS_dt.Columns["DocumentID"].ToString();
                    ddl_AutoDocName.DataTextField = DMS_dt.Columns["DocumentName"].ToString();
                }
                ddl_AutoDocName.DataBind();
                ddl_AutoDocName.Items.Insert(0, new ListItem("--Select Document Name--", "0"));
                txt_DocumentNumber.Text = string.Empty;
                txt_VersionID.Text = string.Empty;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M17:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M17:" + strline + "  " + "Document Name Drop-down Loading failed.", "error");
            }
        }
        public bool ddlCreatorList(string authorID = "")
        {
            try
            {
                DataTable dtTemp = (ViewState["dtRemoveEmpID"]) as DataTable;// remove employee Id
                if (dtTemp.Rows.Count > 0)
                {
                    DataRow[] rows;
                    rows = dtTemp.Select("Control = 'C'");  //'UserName' is ColumnName
                    foreach (DataRow row in rows)
                        dtTemp.Rows.Remove(row);
                }
                UMS_BAL objUMS_Bal = new UMS_BAL();
                DataTable DMS_List = objUMS_Bal.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(ddl_DeptName.SelectedValue), (int)DMS_UserRole.Author_DMS, true);
                ddl_CreatedBy.DataSource = DMS_List;
                ddl_CreatedBy.DataValueField = DMS_List.Columns["EmpID"].ToString();
                ddl_CreatedBy.DataTextField = DMS_List.Columns["EmpName"].ToString();
                ddl_CreatedBy.DataBind();
                ddl_CreatedBy.Items.Insert(0, new ListItem("--Select Author--", "0"));
                if (authorID != "")
                {
                    DataRow[] drAuthorID = DMS_List.Select("EmpID=" + authorID);
                    if (drAuthorID.Length > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M18:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M18:" + strline + "  " + "Author list Dropdown Loading failed.", "error");
                return false;
            }
        }
        protected void ddl_DeptName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddl_DeptName.SelectedValue != "0")
                {
                    ddlCreatorList();
                    ddlInitiatorList(ddl_DeptName.SelectedValue);
                    ddl_Initiator.SelectedValue = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                    txt_DocumentName.Disabled = false;
                    txt_DocumentNumber.Enabled = true;
                }
                if (ddl_DeptName.SelectedIndex <= 0)
                {
                    ddlCreatorList();
                    ddl_Initiator.Items.Clear();
                    txt_DocumentName.Disabled = true;
                    txt_DocumentNumber.Enabled= false;
                }
                ddl_Initiator.Enabled = false;
                DMS_GetDocumentTypeList();
                ddl_AutoDocName.Items.Clear();
                txt_DocumentName.Value = string.Empty;
                lblStatus.Text = string.Empty;
                txt_VersionID.Text = "";
                if (ddl_requestType.SelectedValue == "2" || ddl_requestType.SelectedValue == "3")
                {
                    txt_DocumentNumber.Text = string.Empty;
                    ViewState["DocNameChange"] = "0";
                    btnNewDocName.Visible = false;
                }
                ShowDcoumentList();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M19:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M19:" + strline + "  " + "Author based on Department loading failed.", "error");
            }
        }
        protected void btnDocNumcheck_Click(object sender, EventArgs e)
        {
            btnDocNumcheck.Enabled = false;
            try
            {
                if (!string.IsNullOrEmpty(txt_DocumentNumber.Text))
                {
                    docObjects.documentNUmber = txt_DocumentNumber.Text.Trim();
                    DataTable DMS_dt = DMS_Bal.DMS_DocumentNumberSearch(docObjects);
                    if (DMS_dt.Rows.Count > 0)
                    {
                        lbl_number.Visible = true;
                        lbl_number.Text = "Document Number Already Taken";
                        lbl_number.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        lbl_number.Visible = true;
                        lbl_number.Text = "Document Number Available";
                        lbl_number.ForeColor = System.Drawing.Color.Green;
                    }
                    ShowDcoumentList();
                }
                else
                {
                    ShowDcoumentList();
                    lbl_number.Visible = false;
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M20:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M20:" + strline + "  " + "Document Number Text Changed failed.", "error");
            }
            finally
            {
                btnDocNumcheck.Enabled = true;
            }
        }
        public void DDl_createdBY()
        {
            try
            {
                bool ISExist = CheckRemoveEmpID(Convert.ToInt32(ddl_CreatedBy.SelectedValue));
                if (ISExist == true)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>  Employee Already Selected Please Select another Employee.','warning');", true);
                    DataTable dt = ViewState["dtRemoveEmpID"] as DataTable;
                    DataRow[] drUMS = dt.Select("Control='C'");
                    if (drUMS.Length > 0)
                    {
                        ddl_CreatedBy.SelectedValue = drUMS[0]["EmpID"].ToString();
                    }
                    else
                    {
                        ddl_CreatedBy.SelectedIndex = 0;
                    }
                    return;
                }
                if (ddl_CreatedBy.SelectedIndex <= 0)
                {
                    DataTable dtTemp = (ViewState["dtRemoveEmpID"]) as DataTable;// remove employee Id
                    if (dtTemp.Rows.Count > 0)
                    {
                        DataRow[] rows;
                        rows = dtTemp.Select("Control = 'C'");  //'UserName' is ColumnName
                        foreach (DataRow row in rows)
                            dtTemp.Rows.Remove(row);
                    }
                    DDl_Authorize();
                }
                if (ddl_CreatedBy.SelectedIndex > 0)
                {
                    DataTable dtTemp = (ViewState["dtRemoveEmpID"]) as DataTable;
                    if (dtTemp.Rows.Count > 0)
                    {
                        DataRow[] rows;
                        rows = dtTemp.Select("Control = 'C'");  //'UserName' is ColumnName
                        foreach (DataRow row in rows)
                            dtTemp.Rows.Remove(row);
                    }
                    DataRow drRemoveEmpID = (dtTemp.NewRow());
                    drRemoveEmpID["EmpID"] = ddl_CreatedBy.SelectedValue;
                    drRemoveEmpID["Control"] = "C";
                    dtTemp.Rows.Add(drRemoveEmpID);
                    string value = txt_DocumentName.Value;
                    if (Session["uniqueemployee"] != null)
                    {
                        DataTable dt = (DataTable)Session["uniqueemployee"];
                        for (int i = dt.Rows.Count - 1; i >= 0; i--)
                        {
                            if (dt.Rows[i].ToString() == "Creator")
                            {
                                dt.Rows[i].Delete();
                            }
                        }
                        dt.Rows.Add("Creator", ddl_CreatedBy.SelectedValue);
                    }
                    else
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.Add("Role", typeof(string));
                        dt.Columns.Add("EmpID", typeof(string));
                        dt.Rows.Add("Creator", ddl_CreatedBy.SelectedValue);
                        Session["uniqueemployee"] = dt;
                    }
                    DataTable dt1 = (DataTable)Session["uniqueemployee"];
                    DDl_Authorize();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M21:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M21:" + strline + "  " + "Author employee restriction failed.", "error");
            }
        }
        protected void ddl_CreatedBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["DocumentID"] == null)
                {
                    DDl_createdBY();
                    ClearDropDown();//approver and reviewer dropDown clear
                    ShowDcoumentList();
                }
                if (Request.QueryString["DocumentID"] != null)
                {
                    DDl_createdBY();
                    ClearDropDown();//approver and reviewer dropDown clear
                    ShowDcoumentList();
                    if (Request.QueryString["From"] == "M")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show1", "showInactive();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M22:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M22:" + strline + "  " + "CreatedBy Selected Index Changed failed.", "error");
            }
        }
        //this code purpose roles Restriction
        public bool CheckRemoveEmpID(int EmpID)
        {
            bool ISExist = false;
            if (ViewState["dtRemoveEmpID"] != null)
            {
                DataTable dtRemoveEmpID = ViewState["dtRemoveEmpID"] as DataTable;
                if (dtRemoveEmpID.Rows.Count > 0)
                {
                    for (int i = 0; i < dtRemoveEmpID.Rows.Count; i++)
                    {
                        int RemoveEmpID = Convert.ToInt32(dtRemoveEmpID.Rows[i]["EmpID"].ToString());
                        string Role = dtRemoveEmpID.Rows[i]["Role"].ToString();
                        if (EmpID == RemoveEmpID && Role != "Initiation Request Approver")
                        {
                            ISExist = true;
                            break;
                        }
                    }
                }
            }
            return ISExist;
        }
        protected void ddl_AutoDocName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddl_AutoDocName.SelectedValue != "0")
                {
                    btnNewDocName.Visible = true;
                    docObjects.DocumentID = ddl_AutoDocName.SelectedValue.ToString();
                    DataTable DMS_dt = DMS_Bal.DMS_GetVersionID(docObjects);
                    if (ddl_requestType.SelectedValue == "2" || ddl_requestType.SelectedValue == "3" || ddl_requestType.SelectedValue == "5")
                    {
                        txt_VersionID.Text = DMS_dt.Rows[0]["VersionNumber"].ToString();
                        txt_DocumentNumber.Text = DMS_dt.Rows[0]["DocumentNumber"].ToString();
                        txt_DocumentName.Value= DMS_dt.Rows[0]["DocumentName"].ToString();
                        if (Convert.ToBoolean(DMS_dt.Rows[0]["isPublic"].ToString()) == true)
                        {
                            RadioButtonList1.SelectedValue = "yes";
                        }
                        else
                        {
                            RadioButtonList1.SelectedValue = "no";
                        }
                    }
                    if (ddl_requestType.SelectedValue == "3" || ddl_requestType.SelectedValue == "5")
                    {
                        btnNewDocName.Visible = false;
                    }
                    if (ddl_requestType.SelectedValue == "5")
                    {
                        if (DMS_dt.Rows[0]["ActiveRequestTypeID"].ToString()=="4")
                        {
                            ViewState["RenewAuthorShow"] = "1";
                        }
                        else
                        {
                            ViewState["RenewAuthorShow"] = "0";
                        }
                    }
                    else{
                        ViewState["RenewAuthorShow"] = "0";
                    }
                }
                else
                {
                    btnNewDocName.Visible = false;
                    txt_VersionID.Text = "";
                    txt_DocumentNumber.Text = "";
                    ViewState["RenewAuthorShow"] = "0";
                }
                ShowDcoumentList();
                UpDocumentName.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M23:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M23:" + strline + "  " + "Version loading  failed.", "error");
            }
        }
        private void InitializeThePage()
        {
            try
            {
                DMS_GetDepartmentList();
                DMS_GetDocumentTypeList();
                DMS_GetRequestTypeList();
                DMS_GetRoleName();
                QA_GetDepartmentList();
                DMS_GetApproverreviewerList();
                getuserroles();
                ShowDcoumentList();
                DMS_GetControllerList();
                if (Request.QueryString["DocumentID"] != null)
                {
                    RetriveDocumentDetails();
                    div_buttons1.Visible = true;
                    btn_Reset.Visible = false;
                    btn_Submit.Visible = false;

                    //if (Request.QueryString["From"] == "M")
                    //{
                    //    btn_Reject.Visible = true;
                    //}
                    //else
                    //{
                    //    btn_Reject.Visible = false;
                    //}
                }
                else
                {
                    btn_Update.Visible = false;
                    btn_Cancel.Visible = false;
                    btn_Reject.Visible = false;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "selectDocInit();", true);
                }
                if (ddl_DeptName.SelectedValue == "0")
                {
                    txt_DocumentName.Disabled = true;
                }
                else
                {
                    txt_DocumentName.Disabled = false;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M24:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M24:" + strline + "  " + "Page Initialization failed.", "error");
            }
        }
        private void RetriveDocumentDetails()
        {
            try
            {
                StringBuilder sbWarningMsg = new StringBuilder();
                sbWarningMsg.Clear();
                DataTable dtTemp = ((ViewState["dtRemoveEmpID"]) as DataTable);//for storing author creator approver values
                DataRow drRemoveEmpID1 = dtTemp.NewRow();
                string DocumentID = (Request.QueryString["DocumentID"].ToString());
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(DocumentID);
                string effe = DMS_DT.Rows[0]["EffectiveDate"].ToString();
                string exp = DMS_DT.Rows[0]["ExpirationDate"].ToString();
                effe = effe.Replace(" 00:00:00", "");
                exp = exp.Replace(" 00:00:00", "");
                txt_DocumentID.Text = DMS_DT.Rows[0]["DocumentID"].ToString();
                ViewState["DocID"] = DMS_DT.Rows[0]["docnum"].ToString();
                ddl_requestType.SelectedValue = DMS_DT.Rows[0]["RequestTypeID"].ToString();
                //txt_AutoDocName.Text = DMS_DT.Rows[0]["DocumentName"].ToString();

                txt_DocumentName.Value = DMS_DT.Rows[0]["DocumentName"].ToString();
                ViewState["DocumentName"]= DMS_DT.Rows[0]["DocumentName"].ToString();
                lblStatus.Visible = false;
                txt_DocumentNumber.Text = DMS_DT.Rows[0]["DocumentNumber"].ToString();
                ViewState["DocumentNumber"]= DMS_DT.Rows[0]["DocumentNumber"].ToString();                
                bool IsInitiatorID=ddlInitiatorList(DMS_DT.Rows[0]["DeptID"].ToString(), DMS_DT.Rows[0]["IntiatorID"].ToString());
                if (!IsInitiatorID)
                {
                    sbWarningMsg.Append("Assigned initiator - <b>" + DMS_DT.Rows[0]["IntiatedBy"].ToString() + "</b> <br>is either InActive or does not have access to department." + "<br/>");
                }
                else
                {
                    ddl_Initiator.SelectedValue = DMS_DT.Rows[0]["IntiatorID"].ToString();
                }
                if(Request.QueryString["From"] == "R")
                {
                    ddl_Initiator.Enabled = false;
                }
                ddl_DeptName.SelectedValue = DMS_DT.Rows[0]["DeptID"].ToString();
                ddl_DocumentType.SelectedValue = DMS_DT.Rows[0]["DocumentTypeID"].ToString();
                string rev_data = DMS_DT.Rows[0]["ReviewedBy"].ToString();
                string Author_name = DMS_DT.Rows[0]["CreatorID"].ToString();
                if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "1" || DMS_DT.Rows[0]["RequestTypeID"].ToString() == "2")
                {
                    DMS_GetRoleName();
                    bool IsAuthorID = ddlCreatorList(DMS_DT.Rows[0]["authorID"].ToString());
                    if (IsAuthorID)
                    {
                        ddl_CreatedBy.SelectedValue = DMS_DT.Rows[0]["authorID"].ToString();
                        drRemoveEmpID1["EmpID"] = ddl_CreatedBy.SelectedValue;//To store Creator ID
                        drRemoveEmpID1["Control"] = "C";
                        ((ViewState["dtRemoveEmpID"]) as DataTable).Rows.Add(drRemoveEmpID1);
                    }
                    else
                    {
                        sbWarningMsg.Append("Assigned author - " + Author_name + "</b> <br>is either InActive or does not have access to department." + "<br/>");
                        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop2", "custAlertMsg(' <br/> Author is InActive/Accessible department has been removed for the assigned author - " + Author_name + ".','warning');", true);
                    }
                    txt_Comments.Value = DMS_DT.Rows[0]["CreatePurpose"].ToString();
                }
                else if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "3" || DMS_DT.Rows[0]["RequestTypeID"].ToString() == "5")
                {
                    DMS_GetRoleName();
                    txt_Comments.Value = DMS_DT.Rows[0]["WithdrawPurpose"].ToString();
                    ddl_Controller.SelectedValue= DMS_DT.Rows[0]["ControllerID"].ToString();
                    if(DMS_DT.Rows[0]["authorID"].ToString()!="0")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "show4", "showDivAuthor();", true);
                        ddl_CreatedBy.SelectedValue = DMS_DT.Rows[0]["authorID"].ToString();
                        ViewState["RenewAuthorShow"] = "1";
                    }
                }
                if (DMS_DT.Rows[0]["AuthorizedByID"].ToString() == "")
                {
                    ddl_authorizedBy.SelectedIndex = -1;
                }
                else
                {

                    foreach (ListItem _LiAuth in ddl_authorizedBy.Items)
                    {
                        if(_LiAuth.Value== DMS_DT.Rows[0]["AuthorizedByID"].ToString())
                        {
                            ddl_authorizedBy.SelectedValue = DMS_DT.Rows[0]["AuthorizedByID"].ToString();
                            break;
                        }
                    }

                    if (ddl_authorizedBy.SelectedValue.ToString().Trim() == "0" && DMS_DT.Rows[0]["AuthorizedByID"].ToString() != "")
                    {
                        sbWarningMsg.Append("Assigned authorizer - <b>" + DMS_DT.Rows[0]["AuthorizedBy"].ToString() + "</b> <br>is InActive." + "<br/>");
                    }
                    
                    DataRow drRemoveEmpID2 = dtTemp.NewRow();
                    drRemoveEmpID2["EmpID"] = ddl_authorizedBy.SelectedValue;//To store Authorize ID
                    drRemoveEmpID2["Control"] = "AU";
                    ((ViewState["dtRemoveEmpID"]) as DataTable).Rows.Add(drRemoveEmpID2);
                }
                if (sbWarningMsg.Length > 8)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbWarningMsg.ToString() + "','warning');", true);
                }
                docObjects.DocumentID = DMS_DT.Rows[0]["DocumentID"].ToString();
                if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "2" || DMS_DT.Rows[0]["RequestTypeID"].ToString() == "3" || DMS_DT.Rows[0]["RequestTypeID"].ToString() == "5")
                {
                    txt_VersionID.Text = DMS_DT.Rows[0]["VersionNumber"].ToString();
                }
                if (Convert.ToBoolean(DMS_DT.Rows[0]["isPublic"].ToString()) == true)
                {
                    RadioButtonList1.SelectedValue = "yes";
                }
                else
                {
                    RadioButtonList1.SelectedValue = "no";
                }
                getselectedapproverslist();
                Rejected_comments();
                if (ddl_requestType.SelectedValue == "1")
                {
                    if (Request.QueryString["From"] == "M")
                    {
                        ddl_requestType.Enabled = false;
                        txt_DocumentNumber.Attributes.Add("readonly", "readonly");
                        ddl_DeptName.Enabled = false;
                        txt_DocumentName.Attributes.Add("readonly", "readonly");
                        ddl_DocumentType.Enabled = false;
                        txt_VersionID.Text = "00";
                        txt_Comments.Attributes.Add("readonly", "readonly");
                        lblComments.Text = "Purpose of Initiation";
                        if(DMS_DT.Rows[0]["Status"].ToString() == "IQA" || DMS_DT.Rows[0]["Status"].ToString() == "0" || DMS_DT.Rows[0]["Status"].ToString() == "1R" || DMS_DT.Rows[0]["Status"].ToString() == "1A" || DMS_DT.Rows[0]["Status"].ToString() == "1Au")
                        {
                            ddl_Initiator.Enabled = false;
                        }
                        else if (DMS_DT.Rows[0]["Status"].ToString() == "1" || DMS_DT.Rows[0]["Status"].ToString() == "2" || DMS_DT.Rows[0]["Status"].ToString() == "3")
                        {
                            ddl_CreatedBy.Enabled = false;
                            ddl_Initiator.Enabled = false;
                        }
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "NewDocMan();", true);
                        lstQA.Visible = false;
                        SinglelstQA.Visible = true;
                        btnQA.Visible = false;
                        ddlRoles.Enabled = false;
                        ddlDepartmentQA.Enabled = false;
                        btn_Reject.Visible = true;
                    }
                    else if (Request.QueryString["From"] == "R")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "NewDocRev();", true);
                        ddl_requestType.Enabled = false;
                        ddl_DeptName.Enabled = false;
                        ddl_DocumentType.Enabled = false;
                        txt_VersionID.Text = "00";
                        lblComments.Text = "Purpose of Initiation";
                        btn_Reject.Visible = false;
                    }
                }
                if (ddl_requestType.SelectedValue == "2")
                {
                    if (Request.QueryString["From"] == "M")
                    {
                        ddl_requestType.Enabled = false;
                        txt_DocumentNumber.Attributes.Add("readonly", "readonly");
                        ddl_DocumentType.Enabled = false;
                        ddl_DeptName.Enabled = false;
                        txt_DocumentName.Attributes.Add("readonly", "readonly");
                        txt_Comments.Disabled = true;
                        lblComments.Text = "reason for revision";
                        if (DMS_DT.Rows[0]["Status"].ToString() == "IQA" || DMS_DT.Rows[0]["Status"].ToString() == "0" || DMS_DT.Rows[0]["Status"].ToString() == "1R" || DMS_DT.Rows[0]["Status"].ToString() == "1A" || DMS_DT.Rows[0]["Status"].ToString() == "1Au")
                        {
                            ddl_Initiator.Enabled = false;
                        }
                        else if (DMS_DT.Rows[0]["Status"].ToString() == "1" || DMS_DT.Rows[0]["Status"].ToString() == "2" || DMS_DT.Rows[0]["Status"].ToString() == "3")
                        {
                            ddl_CreatedBy.Enabled = false;
                            ddl_Initiator.Enabled = false;
                        }
                        RadioButtonList1.Enabled = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "RevisionDocMan();", true);
                        lstQA.Visible = false;
                        SinglelstQA.Visible = true;
                        ddlRoles.Enabled = false;
                        ddlDepartmentQA.Enabled = false;
                        btn_Reject.Visible = true;
                    }
                    else if (Request.QueryString["From"] == "R")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "RevisionDocRev();", true);
                        ddl_requestType.Enabled = false;
                        txt_DocumentNumber.Attributes.Add("readonly", "readonly");
                        ddl_DocumentType.Enabled = false;
                        ddl_DeptName.Enabled = false;
                        ddl_AutoDocName.Enabled = false;
                        txt_Comments.Disabled = false;
                        lblComments.Text = "reason for revision";
                        RadioButtonList1.Enabled = false;
                        btn_Reject.Visible = false;
                    }
                }
                if (ddl_requestType.SelectedValue == "3")
                {
                    
                    ddl_requestType.Enabled = false;
                    txt_DocumentNumber.Attributes.Add("readonly", "readonly");
                    ddl_DocumentType.Enabled = false;
                    ddl_DeptName.Enabled = false;
                    txt_DocumentName.Attributes.Add("readonly", "readonly");
                    ddl_AutoDocName.Enabled = false;
                    ddl_DocumentType.Visible = true;
                    lblComments.Text = "Reason for Withdrawal";
                    RadioButtonList1.Enabled = false;
                    btn_Reject.Visible = false;
                    if (Request.QueryString["From"] == "M")
                    {
                        txt_Comments.Disabled = true;                       
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "WithdrawDocMan();", true);
                        if (DMS_DT.Rows[0]["Status"].ToString() == "WR" || DMS_DT.Rows[0]["Status"].ToString() == "5")
                        {
                            ddl_Initiator.Enabled = false;
                        }
                        lstQA.Visible = false;
                        SinglelstQA.Visible = true;
                        ddlRoles.Enabled = false;
                        ddlDepartmentQA.Enabled = false;
                        btn_Reject.Visible = true;
                    }
                    else if (Request.QueryString["From"] == "R")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "WithdrawDocRev();", true);
                    }
                }
                if (ddl_requestType.SelectedValue == "5")
                {

                    ddl_requestType.Enabled = false;
                    txt_DocumentNumber.Attributes.Add("readonly", "readonly");
                    ddl_DocumentType.Enabled = false;
                    ddl_DeptName.Enabled = false;
                    txt_DocumentName.Attributes.Add("readonly", "readonly");
                    ddl_AutoDocName.Enabled = false;
                    ddl_DocumentType.Visible = true;
                    lblComments.Text = "Reason for Renew";
                    RadioButtonList1.Enabled = false;
                    btn_Reject.Visible = false;
                    if (Request.QueryString["From"] == "M")
                    {
                        txt_Comments.Disabled = true;
                        if (DMS_DT.Rows[0]["authorID"].ToString() != "0")
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ExistRenewDocMan();", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "WithdrawDocMan();", true);
                        }
                        if (DMS_DT.Rows[0]["Status"].ToString() == "AR")
                        {
                            ddl_Initiator.Enabled = true;
                        }
                        else if (DMS_DT.Rows[0]["Status"].ToString() == "1" || DMS_DT.Rows[0]["Status"].ToString() == "2" || DMS_DT.Rows[0]["Status"].ToString() == "5")
                        {
                            ddl_CreatedBy.Enabled = false;
                            ddl_Initiator.Enabled = false;
                        }
                        else if(DMS_DT.Rows[0]["Status"].ToString() == "IRN" || DMS_DT.Rows[0]["Status"].ToString() == "0")
                        {
                            ddl_Initiator.Enabled = false;
                        }
                        lstQA.Visible = false;
                        SinglelstQA.Visible = true;
                        ddlRoles.Enabled = false;
                        ddlDepartmentQA.Enabled = false;
                        btn_Reject.Visible = true;
                    }
                    else if (Request.QueryString["From"] == "R")
                    {
                        if (ViewState["RenewAuthorShow"].ToString() != "1")
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "WithdrawDocRev();", true);
                        }
                        if (ViewState["RenewAuthorShow"].ToString() == "1")
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ExistRenewDocRev();", true);
                        }
                    }
                }
                //btnDocNamecheck.Visible = false;
                btnDocNumcheck.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M25:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M25:" + strline + "  " + "Retrieve Document Details failed.", "error");
            }
        }
        public void getselectedapproverslist()
        {
            try
            {
                DataTable rev = DMS_Bal.DMS_GetApproverReviewerSelectedList(txt_DocumentID.Text, 1);
                DataTable aprrove = DMS_Bal.DMS_GetApproverReviewerSelectedList(txt_DocumentID.Text, 2);
                DataTable rev_list = DMS_Bal.DMS_GetApproverReviewerSelectedList(txt_DocumentID.Text, 5);
                DataTable aprrove_list = DMS_Bal.DMS_GetApproverReviewerSelectedList(txt_DocumentID.Text, 4);
                ViewState["ApproverData"] = rev_list;
                ViewState["Data"] = rev_list;
                gvApprover.DataSource = ViewState["Data"];
                gvApprover.DataBind();
                RolesDivGV.Visible = true;
                DataTable dtTemp = ((ViewState["dtRemoveEmpID"]) as DataTable);
                if (rev_list.Rows.Count > 0)
                {
                    for (int i = 0; i < rev_list.Rows.Count; i++)
                    {
                        int EmpID = Convert.ToInt32(rev_list.Rows[i]["EmpID"].ToString());
                        string Role = rev_list.Rows[i]["Role"].ToString();
                        if (EmpID != 0 && Role == "Initiation Request Approver")
                        {
                            DataRow drRemoveEmpID3 = dtTemp.NewRow();
                            drRemoveEmpID3["EmpID"] = EmpID;
                            drRemoveEmpID3["Role"] = Role;
                            drRemoveEmpID3["Control"] = "IA";
                            dtTemp.Rows.Add(drRemoveEmpID3);
                        }
                        else if (EmpID != 0 && Role == "Document Reviewer")
                        {
                            DataRow drRemoveEmpID3 = dtTemp.NewRow();
                            drRemoveEmpID3["EmpID"] = EmpID;
                            drRemoveEmpID3["Role"] = Role;
                            drRemoveEmpID3["Control"] = "R";
                            dtTemp.Rows.Add(drRemoveEmpID3);
                        }
                        else if (EmpID != 0 && Role == "Document Approver")
                        {
                            DataRow drRemoveEmpID3 = dtTemp.NewRow();
                            drRemoveEmpID3["EmpID"] = EmpID;
                            drRemoveEmpID3["Role"] = Role;
                            drRemoveEmpID3["Control"] = "AP";
                            dtTemp.Rows.Add(drRemoveEmpID3);
                        }
                    }
                    if (Request.QueryString["From"] == "M")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show1", "showInactive();", true);
                    }
                }
                DMS_GetApproverreviewerList();

            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M26:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M26:" + strline + "  " + "selected reviewers/approvers loading failed.", "error");
            }
        }
        protected void btn_Update_Click(object sender, EventArgs e)
        {
            btn_Update.Enabled = false;
            try
            {
                StringBuilder sbErrorMsg = new StringBuilder();
                string DocumentID = (Request.QueryString["DocumentID"].ToString());
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(DocumentID);
                if (ddl_requestType.SelectedValue == "1")
                {
                    sbErrorMsg.Clear();
                    if (txt_DocumentNumber.Text.Trim() == "")
                    {
                        sbErrorMsg.Append("Please Enter Document Number." + "<br/>");
                    }
                    if (!string.IsNullOrEmpty(txt_DocumentNumber.Text.Trim()))
                    {
                        if (txt_DocumentNumber.Text != ViewState["DocumentNumber"].ToString())
                        {
                            docObjects.documentNUmber = txt_DocumentNumber.Text.Trim();
                            DataTable DMS_dt = DMS_Bal.DMS_DocumentNumberSearch(docObjects);
                            if (DMS_dt.Rows.Count > 0)
                            {
                                sbErrorMsg.Append("Document Number Already exists, Please Enter another Number." + "<br/>");
                            }
                        }
                    }
                    if (ddl_DocumentType.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append("Please Select Document Type." + "<br/>");
                    }
                    else if (ddl_DocumentType.SelectedValue.ToString().Trim() == "" || ddl_DocumentType.SelectedValue.ToString().Trim() == null)
                    {
                        sbErrorMsg.Append(" No Document Type/s present." + "<br/>");
                    }
                    if (ddl_DeptName.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append("Please Select Department." + "<br/>");
                    }
                    if (txt_DocumentName.Value.Trim() == "")
                    {
                        sbErrorMsg.Append("Please Enter Document Name." + "<br/>");
                    }
                    //if (!string.IsNullOrEmpty(txt_DocumentName.Value.Trim()))
                    //{
                    //    if (txt_DocumentName.Value != ViewState["DocumentName"].ToString())
                    //    {
                    //        docObjects.DMS_DocumentName = txt_DocumentName.Value.Trim();
                    //        docObjects.dept = ddl_DeptName.SelectedValue;
                    //        DataTable DMS_dt = DMS_Bal.DMS_DocumentNameSearch(docObjects);
                    //        if (DMS_dt.Rows.Count > 0)
                    //        {
                    //            sbErrorMsg.Append("Document Name Already exists, Please Enter another Name." + "<br/>");
                    //        }
                    //    }
                    //}
                    if (txt_Comments.Value.Trim() == "")
                    {
                        sbErrorMsg.Append("Please Enter Purpose of Initiation." + "<br/>");
                    }
                    if (Request.QueryString["From"] == "M")
                    {
                        if (ddl_Initiator.SelectedValue.ToString().Trim() == "0" && DMS_DT.Rows[0]["Status"].ToString() == "AR")
                        {
                            sbErrorMsg.Append("Select an Initiator." + "<br/>");
                        }
                    }
                    if (ddl_CreatedBy.SelectedValue.ToString().Trim() == "0" && DMS_DT.Rows[0]["Status"].ToString() != "1" && DMS_DT.Rows[0]["Status"].ToString() != "2" && DMS_DT.Rows[0]["Status"].ToString() != "3")
                    {
                        sbErrorMsg.Append("Please Select Author." + "<br/>");
                    }
                    if (Request.QueryString["From"] == "M")
                    {
                        
                        if (ddl_authorizedBy.SelectedValue.ToString().Trim() != "0" && DMS_DT.Rows[0]["AuthorizedByID"].ToString() == "")
                        {
                            sbErrorMsg.Append("Authorizer cannot be added." + "<br/>");
                        }
                        else if (ddl_authorizedBy.SelectedValue.ToString().Trim() == "0" && DMS_DT.Rows[0]["AuthorizedByID"].ToString() != "")
                        {
                            sbErrorMsg.Append("Select an Authorizer." + "<br/>");
                        }
                    }
                    int QACount = 0, ReviewerCount = 0, ApproverCount = 0;
                    foreach (GridViewRow row in gvApprover.Rows)
                    {
                        if ((row.Cells[0].Text) == "Initiation Request Approver")
                        {
                            QACount = QACount + 1;
                        }
                        else if ((row.Cells[0].Text) == "Document Reviewer")
                        {
                            ReviewerCount = ReviewerCount + 1;
                        }
                        else if ((row.Cells[0].Text) == "Document Approver")
                        {
                            ApproverCount = ApproverCount + 1;
                        }
                    }
                    if (QACount == 0)
                    {
                        sbErrorMsg.Append(" Select at-least one Initiation Request Approver." + "<br/>");
                    }
                    if (ReviewerCount == 0)
                    {
                        sbErrorMsg.Append(" Select at-least one Document Reviewer." + "<br/>");
                    }
                    if (ApproverCount == 0)
                    {
                        sbErrorMsg.Append(" Select at-least one Document Approver." + "<br/>");
                    }
                    if (Request.QueryString["From"] == "M")
                    {
                        if (txt_Remarks.Value.Trim() == "")
                        {
                            sbErrorMsg.Append("Please Enter Remarks." + "<br/>");
                        }                                             
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show3", "showInactive();", true);
                    }
                    if (sbErrorMsg.Length > 8)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
                        return;
                    }
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop1", "ConformAlertUpdate();", true);
                }
                else if (ddl_requestType.SelectedValue == "2")
                {
                    sbErrorMsg.Clear();
                    if (ddl_DocumentType.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append("Please Select Document Type." + "<br/>");
                    }
                    else if (ddl_DocumentType.SelectedValue.ToString().Trim() == "" || ddl_DocumentType.SelectedValue.ToString().Trim() == null)
                    {
                        sbErrorMsg.Append(" No Document Type/s present." + "<br/>");
                    }
                    if (ddl_DeptName.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append("Please Select Department." + "<br/>");
                    }
                    if (txt_DocumentName.Value.Trim() == "")
                    {
                        sbErrorMsg.Append("Please Enter Document Name." + "<br/>");
                    }
                    //if (!string.IsNullOrEmpty(txt_DocumentName.Value.Trim()))
                    //{
                    //    if (txt_DocumentName.Value != ViewState["DocumentName"].ToString())
                    //    {
                    //        docObjects.DMS_DocumentName = txt_DocumentName.Value.Trim();
                    //        docObjects.dept = ddl_DeptName.SelectedValue;
                    //        DataTable DMS_dt = DMS_Bal.DMS_DocumentNameSearch(docObjects);
                    //        if (DMS_dt.Rows.Count > 0)
                    //        {
                    //            sbErrorMsg.Append("Document Name Already exists, Please Enter another Name." + "<br/>");
                    //        }
                    //    }
                    //}
                    if (txt_Comments.Value.Trim() == "")
                    {
                        sbErrorMsg.Append("Please enter reason for revision." + "<br/>");
                    }
                    if (Request.QueryString["From"] == "M")
                    {
                        if (ddl_Initiator.SelectedValue.ToString().Trim() == "0" && DMS_DT.Rows[0]["Status"].ToString() == "AR")
                        {
                            sbErrorMsg.Append("Select an Initiator." + "<br/>");
                        }
                    }
                    if (ddl_CreatedBy.SelectedValue.ToString().Trim() == "0" && DMS_DT.Rows[0]["Status"].ToString() != "1" && DMS_DT.Rows[0]["Status"].ToString() != "2" && DMS_DT.Rows[0]["Status"].ToString() != "3")
                    {
                        sbErrorMsg.Append("Please Select Author." + "<br/>");
                    }
                    if (Request.QueryString["From"] == "M")
                    {
                        //string DocumentID = (Request.QueryString["DocumentID"].ToString());
                        //DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(DocumentID);
                        if (ddl_authorizedBy.SelectedValue.ToString().Trim() != "0" && DMS_DT.Rows[0]["AuthorizedByID"].ToString() == "")
                        {
                            sbErrorMsg.Append("Authorizer cannot be added." + "<br/>");
                        }
                        else if (ddl_authorizedBy.SelectedValue.ToString().Trim() == "0" && DMS_DT.Rows[0]["AuthorizedByID"].ToString() != "")
                        {
                            sbErrorMsg.Append("Select an Authorizer." + "<br/>");
                        }
                    }
                    int QACount = 0, ReviewerCount = 0, ApproverCount = 0;
                    foreach (GridViewRow row in gvApprover.Rows)
                    {
                        if ((row.Cells[0].Text) == "Initiation Request Approver")
                        {
                            QACount = QACount + 1;
                        }
                        else if ((row.Cells[0].Text) == "Document Reviewer")
                        {
                            ReviewerCount = ReviewerCount + 1;
                        }
                        else if ((row.Cells[0].Text) == "Document Approver")
                        {
                            ApproverCount = ApproverCount + 1;
                        }
                    }
                    if (QACount == 0)
                    {
                        sbErrorMsg.Append(" Select at-least one Initiation Request Approver." + "<br/>");
                    }
                    if (ReviewerCount == 0)
                    {
                        sbErrorMsg.Append(" Select at-least one Document Reviewer." + "<br/>");
                    }
                    if (ApproverCount == 0)
                    {
                        sbErrorMsg.Append(" Select at-least one Document Approver." + "<br/>");
                    }
                    if (Request.QueryString["From"] == "M")
                    {
                        if (txt_Remarks.Value.Trim() == "")
                        {
                            sbErrorMsg.Append("Please Enter Remarks." + "<br/>");
                        }                                               
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show3", "showInactive();", true);
                    }
                    if (sbErrorMsg.Length > 8)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
                        return;
                    }
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop1", "ConformAlertUpdate();", true);
                }
                else if (ddl_requestType.SelectedValue == "3")
                {
                    sbErrorMsg.Clear();
                    if (ddl_DocumentType.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append("Please Select Document Type." + "<br/>");
                    }
                    else if (ddl_DocumentType.SelectedValue.ToString().Trim() == "" || ddl_DocumentType.SelectedValue.ToString().Trim() == null)
                    {
                        sbErrorMsg.Append(" No Document Type/s present." + "<br/>");
                    }
                    if (ddl_DeptName.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append("Please Select Department." + "<br/>");
                    }
                    if (txt_DocumentName.Value.Trim() == "")
                    {
                        sbErrorMsg.Append("Please Select Document." + "<br/>");
                    }
                    if (txt_Comments.Value.Trim() == "")
                    {
                        sbErrorMsg.Append("Please enter Reason for Withdrawal." + "<br/>");
                    }
                    if (ddl_Controller.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append("Please Select Controller." + "<br/>");
                    }
                    int QACount = 0;
                    foreach (GridViewRow row in gvApprover.Rows)
                    {
                        if ((row.Cells[0].Text) == "Initiation Request Approver")
                        {
                            QACount = QACount + 1;
                        }
                    }
                    if (QACount == 0)
                    {
                        sbErrorMsg.Append(" Select at-least one Initiation Request Approver." + "<br/>");
                    }
                    if (Request.QueryString["From"] == "M")
                    {
                        if (ddl_Initiator.SelectedValue.ToString().Trim() == "0" && (DMS_DT.Rows[0]["Status"].ToString() == "AR"))
                        {
                            sbErrorMsg.Append("Select an Initiator." + "<br/>");
                        }
                        if (txt_Remarks.Value.Trim() == "")
                        {
                            sbErrorMsg.Append("Please Enter Remarks." + "<br/>");
                        }
                    }
                    if (sbErrorMsg.Length > 8)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
                        return;
                    }
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop1", "ConformAlertUpdate();", true);
                }
                else if (ddl_requestType.SelectedValue == "5")
                {
                    sbErrorMsg.Clear();
                    if (ddl_DocumentType.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append("Please Select Document Type." + "<br/>");
                    }
                    else if (ddl_DocumentType.SelectedValue.ToString().Trim() == "" || ddl_DocumentType.SelectedValue.ToString().Trim() == null)
                    {
                        sbErrorMsg.Append(" No Document Type/s present." + "<br/>");
                    }
                    if (ddl_DeptName.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append("Please Select Department." + "<br/>");
                    }
                    if (txt_DocumentName.Value.Trim() == "")
                    {
                        sbErrorMsg.Append("Please Select Document." + "<br/>");
                    }
                    if (txt_Comments.Value.Trim() == "")
                    {
                        sbErrorMsg.Append("Please enter Reason for Renew." + "<br/>");
                    }
                    if(ViewState["RenewAuthorShow"].ToString()=="1")
                    {
                        if(ddl_CreatedBy.SelectedValue.ToString().Trim() == "0")
                        {
                            sbErrorMsg.Append("Please Select author." + "<br/>");
                        }
                    }
                    if (ddl_Controller.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append("Please Select Controller." + "<br/>");
                    }
                    int QACount = 0, ReviewerCount = 0, ApproverCount = 0;
                    foreach (GridViewRow row in gvApprover.Rows)
                    {
                        if ((row.Cells[0].Text) == "Initiation Request Approver")
                        {
                            QACount = QACount + 1;
                        }
                        else if ((row.Cells[0].Text) == "Document Reviewer")
                        {
                            ReviewerCount = ReviewerCount + 1;
                        }
                        else if ((row.Cells[0].Text) == "Document Approver")
                        {
                            ApproverCount = ApproverCount + 1;
                        }
                    }
                    if (QACount == 0)
                    {
                        sbErrorMsg.Append(" Select at-least one Initiation Request Approver." + "<br/>");
                    }
                    if (ReviewerCount == 0)
                    {
                        sbErrorMsg.Append(" Select at-least one Document Reviewer." + "<br/>");
                    }
                    if (ApproverCount == 0)
                    {
                        sbErrorMsg.Append(" Select at-least one Document Approver." + "<br/>");
                    }
                    if (Request.QueryString["From"] == "M")
                    {
                        if (ddl_Initiator.SelectedValue.ToString().Trim() == "0" && (DMS_DT.Rows[0]["Status"].ToString() == "AR"))
                        {
                            sbErrorMsg.Append("Select an Initiator." + "<br/>");
                        }
                        if (txt_Remarks.Value.Trim() == "")
                        {
                            sbErrorMsg.Append("Please Enter Remarks." + "<br/>");
                        }
                    }
                    if (sbErrorMsg.Length > 8)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
                        return;
                    }
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop1", "ConformAlertUpdate();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M27:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M27:" + strline + "  " + "Document Update failed.", "error");
            }
            finally
            {
                ShowDcoumentList();
                btn_Update.Enabled = true;
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        protected void ddl_DocumentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lblStatus.Text = "";
                if (ddl_requestType.SelectedValue == "2" || ddl_requestType.SelectedValue == "3" || ddl_requestType.SelectedValue == "5")
                {
                    string Rtype = "";
                    if (ddl_requestType.SelectedValue == "2")
                    {
                        Rtype = "R";
                        ViewState["DocNameChange"] = "0";
                    }
                    if (ddl_requestType.SelectedValue == "3")
                    {
                        Rtype = "W";
                    }
                    if (ddl_requestType.SelectedValue == "5")
                    {
                        Rtype = "Re";
                    }
                    getdocumentlist(ddl_DocumentType.SelectedValue, ddl_DeptName.SelectedValue, Rtype);
                    btnNewDocName.Visible = false;
                }
                else
                {
                    ShowDcoumentList();
                    txt_DocumentName.Value = "";
                    lblStatus.Text = "";
                }
                ViewState["RenewAuthorShow"] = "0";
                ShowDcoumentList();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M28:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M28:" + strline + "  " + "Documents loading based on Department,document type,request type failed.", "error");
            }
        }
        //Clear Approver and Reviewe DropDowns
        public void ClearDropDown()
        {
            lstQA.Items.Clear();
            SinglelstQA.Items.Clear();
            QA_GetDepartmentList();            
        }
        protected void btn_Cancel_Click(object sender, EventArgs e)
        {
            btn_Cancel.Enabled = false;
            try
            {
                if (Request.QueryString.Count > 1)
                {
                    if (Request.QueryString["From"] != null)
                    {
                        if (Request.QueryString["From"] == "M")
                        {
                            Response.Redirect("~/DMS/DocumentIntiator/Manage_Documents.aspx", false);
                        }
                        else if (Request.QueryString["From"] == "R")
                        {
                            Response.Redirect("~/DMS/DocumentIntiator/Reverted_Documents.aspx", false);
                        }
                    }
                    else
                    {
                        Response.Redirect("~/DMS/DocumentIntiator/DocumentIntiation.aspx", false);
                    }
                }
                else
                {
                    Response.Redirect("~/DMS/DocumentIntiator/DocumentIntiation.aspx", false);
                }
                ShowDcoumentList();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M29:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M29:" + strline + "  " + "Cancel failed.", "error");
            }
            finally
            {
                btn_Cancel.Enabled = true;
            }
        }
        protected void ddl_authorizedBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddl_authorizedBy.SelectedIndex > 0)
                {
                    bool ISExist = CheckRemoveEmpID(Convert.ToInt32(ddl_authorizedBy.SelectedValue));
                    if (ISExist == true)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>  Employee Already Selected Please Select another Employee.','warning');", true);
                        DataTable dt = ViewState["dtRemoveEmpID"] as DataTable;
                        DataRow[] drUMS = dt.Select("Control='AU'");
                        if (drUMS.Length > 0)
                        {
                            ddl_authorizedBy.SelectedValue = drUMS[0]["EmpID"].ToString();
                        }
                        else
                        {
                            ddl_authorizedBy.SelectedIndex = 0;
                        }
                        return;
                    }
                    if (ViewState["dtRemoveEmpID"] != null)
                    {
                        DataTable dtTemp = ((ViewState["dtRemoveEmpID"]) as DataTable);
                        if (dtTemp.Rows.Count > 0)
                        {
                            DataRow[] rows;
                            rows = dtTemp.Select("Control = 'AU'");  //'UserName' is ColumnName
                            foreach (DataRow row in rows)
                                dtTemp.Rows.Remove(row);
                        }
                        DataRow drRemoveEmpID = dtTemp.NewRow();
                        drRemoveEmpID["EmpID"] = ddl_authorizedBy.SelectedValue;
                        drRemoveEmpID["Control"] = "AU";
                        ((ViewState["dtRemoveEmpID"]) as DataTable).Rows.Add(drRemoveEmpID);
                    }
                }
                if (ddl_authorizedBy.SelectedIndex <= 0)
                {
                    DataTable dtTemp = (ViewState["dtRemoveEmpID"]) as DataTable;
                    if (dtTemp.Rows.Count > 0)
                    {
                        DataRow[] rows;
                        rows = dtTemp.Select("Control = 'AU'");  //'UserName' is ColumnName
                        foreach (DataRow row in rows)
                            dtTemp.Rows.Remove(row);
                    }
                }
                if (Request.QueryString["From"] == "M")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show1", "showInactive();", true);
                }
                ClearDropDown();//approver and reviewer dropDown clear
                ShowDcoumentList();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M30:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M30:" + strline + "  " + "AuthorizedBy SelectedIndexChanged failed.", "error");
            }
        }
        protected void ddlDepartmentQA_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlDepartmentQA.SelectedIndex > 0)
                {
                    if (ddlRoles.SelectedValue == "0")
                    {
                    }
                    else if (ddlRoles.SelectedValue == "1")//1=New Document
                    {
                        UMS_BAL objUMS_Bal = new UMS_BAL();
                        DataTable DMS_Dt = objUMS_Bal.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(ddlDepartmentQA.SelectedValue), (int)DMS_UserRole.Approver_DMS, true);
                        if (DMS_Dt.Rows.Count > 0)
                        {
                            //DataRow[] filtered = DMS_Dt.Select("EmpID NOT IN ('" + (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString() + "')");
                            //if (filtered.Any())
                            //{
                            //    DMS_Dt = filtered.CopyToDataTable();
                                if (ViewState["dtRemoveEmpID"] != null)
                                {
                                    DataTable dtTemp = ViewState["dtRemoveEmpID"] as DataTable;
                                    if (dtTemp.Rows.Count > 0)
                                    {
                                        for (int i = 0; i < dtTemp.Rows.Count; i++)
                                        {
                                            for (int j = 0; j < DMS_Dt.Rows.Count; j++)
                                            {
                                                int DMSEmpID = Convert.ToInt32(DMS_Dt.Rows[j]["EmpID"].ToString());
                                                int RemoveID = Convert.ToInt32(dtTemp.Rows[i]["EmpID"].ToString());
                                                string DMSRole = dtTemp.Rows[i]["Role"].ToString();
                                                if (RemoveID == DMSEmpID && DMSRole != "Document Approver" && DMSRole != "Document Reviewer")
                                                {
                                                    DMS_Dt.Rows.RemoveAt(j);
                                                }
                                            }
                                        }
                                    }
                                    if (Request.QueryString["From"] == "M")
                                    {
                                        SinglelstQA.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                                        SinglelstQA.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                                        SinglelstQA.DataSource = DMS_Dt;
                                        SinglelstQA.DataBind();
                                    }
                                    else
                                    {
                                        lstQA.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                                        lstQA.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                                        lstQA.DataSource = DMS_Dt;
                                        lstQA.DataBind();
                                    }
                                    if (DMS_Dt.Rows.Count > 0)
                                    {
                                        ViewState["ApproverDeptcode"] = DMS_Dt.Rows[0]["DeptCode"].ToString();
                                    }
                                }
                            //}
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>   There are no Initiation Request Approvers for this Department.','warning');", true);
                            ClearDropDown();
                        }
                    }
                    else if (ddlRoles.SelectedValue == "2")//2=Revision of Existing Document
                    {
                        UMS_BAL objUMS_Bal = new UMS_BAL();
                        DataTable DMS_Dt = objUMS_Bal.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(ddlDepartmentQA.SelectedValue), (int)DMS_UserRole.Reviewer_DMS, true);
                        if (DMS_Dt.Rows.Count > 0)
                        {
                            if (ViewState["dtRemoveEmpID"] != null)
                            {
                                DataTable dtTemp = ViewState["dtRemoveEmpID"] as DataTable;
                                if (dtTemp.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dtTemp.Rows.Count; i++)
                                    {
                                        for (int j = 0; j < DMS_Dt.Rows.Count; j++)
                                        {
                                            int DMSEmpID = Convert.ToInt32(DMS_Dt.Rows[j]["EmpID"].ToString());
                                            int RemoveID = Convert.ToInt32(dtTemp.Rows[i]["EmpID"].ToString());
                                            string DMSRole = dtTemp.Rows[i]["Role"].ToString();
                                            if (RemoveID == DMSEmpID && DMSRole != "Initiation Request Approver")
                                            {
                                                DMS_Dt.Rows.RemoveAt(j);
                                            }
                                        }
                                    }
                                }
                                if (Request.QueryString["From"] == "M")
                                {
                                    SinglelstQA.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                                    SinglelstQA.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                                    SinglelstQA.DataSource = DMS_Dt;
                                    SinglelstQA.DataBind();
                                }
                                else
                                {
                                    lstQA.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                                    lstQA.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                                    lstQA.DataSource = DMS_Dt;
                                    lstQA.DataBind();
                                }
                                if (DMS_Dt.Rows.Count > 0)
                                {
                                    ViewState["ApproverDeptcode"] = DMS_Dt.Rows[0]["DeptCode"].ToString();
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>   There are no Document Reviewers for this Department.','warning');", true);
                            ClearDropDown();
                        }
                    }
                    else if (ddlRoles.SelectedValue == "3")//3=Withdraw Document
                    {
                        UMS_BAL objUMS_Bal = new UMS_BAL();
                        DataTable DMS_Dt = objUMS_Bal.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(ddlDepartmentQA.SelectedValue), (int)DMS_UserRole.Approver_DMS, true);
                        if (DMS_Dt.Rows.Count > 0)
                        {
                            if (ViewState["dtRemoveEmpID"] != null)
                            {
                                DataTable dtTemp = ViewState["dtRemoveEmpID"] as DataTable;
                                if (dtTemp.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dtTemp.Rows.Count; i++)
                                    {
                                        for (int j = 0; j < DMS_Dt.Rows.Count; j++)
                                        {
                                            int DMSEmpID = Convert.ToInt32(DMS_Dt.Rows[j]["EmpID"].ToString());
                                            int RemoveID = Convert.ToInt32(dtTemp.Rows[i]["EmpID"].ToString());
                                            string DMSRole = dtTemp.Rows[i]["Role"].ToString();
                                            if (RemoveID == DMSEmpID && DMSRole != "Initiation Request Approver")
                                            {
                                                DMS_Dt.Rows.RemoveAt(j);
                                            }
                                        }
                                    }
                                }
                                if (Request.QueryString["From"] == "M")
                                {
                                    SinglelstQA.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                                    SinglelstQA.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                                    SinglelstQA.DataSource = DMS_Dt;
                                    SinglelstQA.DataBind();
                                }
                                else
                                {
                                    lstQA.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                                    lstQA.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                                    lstQA.DataSource = DMS_Dt;
                                    lstQA.DataBind();
                                }
                                if (DMS_Dt.Rows.Count > 0)
                                {
                                    ViewState["ApproverDeptcode"] = DMS_Dt.Rows[0]["DeptCode"].ToString();
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>   There are no Document Approvers for this Department.','warning');", true);
                        }
                    }
                }
                ShowDcoumentList();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M31:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M31:" + strline + "  " + " Initiation Request Approver/Document reviewer/Document approver binding Exception.", "error");
            }
        }
        protected void btnQA_Click(object sender, EventArgs e)
        {
            btnQA.Enabled = false;
            try
            {
                DataTable dtTemp = new DataTable();
                string Deptcode = (string)ViewState["ApproverDeptcode"];
                bool isManageFirstRow = false;
                int rowIndex = -1;
                if (ViewState["ApproverData"] != null)
                {
                    dtTemp = ViewState["ApproverData"] as DataTable;
                    if (Request.QueryString["From"] == "M")
                    {
                        for (int x = 0; x < SinglelstQA.Items.Count; x++)
                        {
                            if (SinglelstQA.Items[x].Selected)
                            {
                                bool IsEmpExists = false;
                                for (int i = 0; i < dtTemp.Rows.Count; i++)
                                {
                                    int GvEmpID = Convert.ToInt32(dtTemp.Rows[i]["EmpID"].ToString());
                                    int DMSEmpID = Convert.ToInt32(SinglelstQA.Items[x].Value);
                                    string DMSRole = dtTemp.Rows[i]["Role"].ToString();
                                    if (GvEmpID == DMSEmpID && DMSRole != "Initiation Request Approver" && DMSRole != "Document Approver" && DMSRole != "Document Reviewer")
                                    {
                                        IsEmpExists = true;
                                    }
                                }
                                if (!IsEmpExists)
                                {
                                    if (ViewState["EditRowIndex"] != null)
                                    {
                                        rowIndex = Convert.ToInt32(ViewState["EditRowIndex"].ToString());
                                        dtTemp.Rows.RemoveAt(rowIndex);
                                        DataTable dtremove = ((ViewState["dtRemoveEmpID"]) as DataTable);
                                        DataRow[] rows = dtremove.AsEnumerable().Where(guser => guser.Field<string>("EmpID") == ViewState["EditEmpID"].ToString()).ToArray();
                                        foreach (DataRow row in rows) dtremove.Rows.Remove(row);
                                        dtremove.AcceptChanges();
                                        ViewState["dtRemoveEmpID"] = dtremove;
                                        // data base inactive code.
                                        isManageFirstRow = true;
                                    }
                                    DataRow dr = dtTemp.NewRow();
                                    dr["EmpName"] = SinglelstQA.Items[x].Text;
                                    dr["EmpID"] = SinglelstQA.Items[x].Value;
                                    dr["DeptCode"] = Deptcode;
                                    dr["DeptID"] = ddlDepartmentQA.SelectedValue;
                                    dr["Role"] = ddlRoles.SelectedItem.Text.Trim();
                                    dr["Status"] = null;
                                    dr["DocStatus"] = null;
                                    if (isManageFirstRow == true)
                                    {
                                        dtTemp.Rows.InsertAt(dr, rowIndex);
                                        isManageFirstRow = false;
                                    }
                                    else
                                        dtTemp.Rows.Add(dr);
                                    DataRow drRemoveEmpID = ((ViewState["dtRemoveEmpID"]) as DataTable).NewRow();
                                    drRemoveEmpID["EmpID"] = SinglelstQA.Items[x].Value;
                                    drRemoveEmpID["Control"] = "AP";
                                    drRemoveEmpID["Role"] = ddlRoles.SelectedItem.Text.Trim();
                                    ((ViewState["dtRemoveEmpID"]) as DataTable).Rows.Add(drRemoveEmpID);
                                }
                            }
                        }
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show2", "showInactive();", true);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show3", "hideInactiveStyle();", true);
                    }
                    else
                    {
                        for (int x = 0; x < lstQA.Items.Count; x++)
                        {
                            if (lstQA.Items[x].Selected)
                            {
                                bool IsEmpExists = false;
                                for (int i = 0; i < dtTemp.Rows.Count; i++)
                                {
                                    int GvEmpID = Convert.ToInt32(dtTemp.Rows[i]["EmpID"].ToString());
                                    int DMSEmpID = Convert.ToInt32(lstQA.Items[x].Value);
                                    string DMSRole = dtTemp.Rows[i]["Role"].ToString();
                                    if (GvEmpID == DMSEmpID && DMSRole != "Initiation Request Approver" && DMSRole != "Document Approver" && DMSRole != "Document Reviewer")
                                    {
                                        IsEmpExists = true;
                                    }
                                }
                                if (!IsEmpExists)
                                {
                                    if (ViewState["EditRowIndex"] != null)
                                    {
                                        rowIndex = Convert.ToInt32(ViewState["EditRowIndex"].ToString());
                                        dtTemp.Rows.RemoveAt(rowIndex);
                                        DataTable dtremove = ((ViewState["dtRemoveEmpID"]) as DataTable);
                                        DataRow[] rows = dtremove.AsEnumerable().Where(guser => guser.Field<string>("EmpID") == ViewState["EditEmpID"].ToString()).ToArray();
                                        foreach (DataRow row in rows) dtremove.Rows.Remove(row);
                                        dtremove.AcceptChanges();
                                        ViewState["dtRemoveEmpID"] = dtremove;
                                        // data base inactive code.
                                        isManageFirstRow = true;
                                    }
                                    DataRow dr = dtTemp.NewRow();
                                    dr["EmpName"] = lstQA.Items[x].Text;
                                    dr["EmpID"] = lstQA.Items[x].Value;
                                    dr["DeptCode"] = Deptcode;
                                    dr["DeptID"] = ddlDepartmentQA.SelectedValue;
                                    dr["Role"] = ddlRoles.SelectedItem.Text.Trim();
                                    dr["Status"] = null;
                                    dr["DocStatus"] = null;
                                    if (isManageFirstRow == true)
                                    {
                                        dtTemp.Rows.InsertAt(dr, rowIndex);
                                        isManageFirstRow = false;
                                    }
                                    else
                                        dtTemp.Rows.Add(dr);
                                    DataRow drRemoveEmpID = ((ViewState["dtRemoveEmpID"]) as DataTable).NewRow();
                                    drRemoveEmpID["EmpID"] = lstQA.Items[x].Value;
                                    drRemoveEmpID["Control"] = "AP";
                                    drRemoveEmpID["Role"] = ddlRoles.SelectedItem.Text.Trim();
                                    ((ViewState["dtRemoveEmpID"]) as DataTable).Rows.Add(drRemoveEmpID);
                                }
                            }
                        }
                    }
                }
                else
                {
                    dtTemp.Columns.Add("EmpName");
                    dtTemp.Columns.Add("DeptCode");
                    dtTemp.Columns.Add("EmpID");
                    dtTemp.Columns.Add("DeptID");
                    dtTemp.Columns.Add("Role");
                    dtTemp.Columns.Add("Status");
                    dtTemp.Columns.Add("DocStatus");
                    dtTemp.Columns.Add("ActiveStatus");
                    if (Request.QueryString["From"] == "M")
                    {
                        for (int x = 0; x < SinglelstQA.Items.Count; x++)
                        {
                            if (SinglelstQA.Items[x].Selected.Equals(true))
                            {
                                DataRow dr = dtTemp.NewRow();
                                dr["EmpName"] = SinglelstQA.Items[x].Text;
                                dr["EmpID"] = SinglelstQA.Items[x].Value;
                                int EmpID = Convert.ToInt32(SinglelstQA.Items[x].Value);
                                dr["DeptCode"] = Deptcode;
                                dr["DeptID"] = ddlDepartmentQA.SelectedValue;
                                dr["Role"] = ddlRoles.SelectedItem.Text.Trim();
                                dr["Status"] = null;
                                dr["DocStatus"] = null;
                                dtTemp.Rows.Add(dr);
                                DataRow drRemoveEmpID = ((ViewState["dtRemoveEmpID"]) as DataTable).NewRow();
                                drRemoveEmpID["EmpID"] = SinglelstQA.Items[x].Value;
                                drRemoveEmpID["Control"] = "AP";
                                drRemoveEmpID["Role"] = ddlRoles.SelectedItem.Text.Trim();
                                ((ViewState["dtRemoveEmpID"]) as DataTable).Rows.Add(drRemoveEmpID);
                            }
                        }
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show2", "showInactive();", true);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show3", "hideInactiveStyle();", true);
                    }
                    else
                    {
                        for (int x = 0; x < lstQA.Items.Count; x++)
                        {
                            if (lstQA.Items[x].Selected.Equals(true))
                            {
                                DataRow dr = dtTemp.NewRow();
                                dr["EmpName"] = lstQA.Items[x].Text;
                                dr["EmpID"] = lstQA.Items[x].Value;
                                int EmpID = Convert.ToInt32(lstQA.Items[x].Value);
                                dr["DeptCode"] = Deptcode;
                                dr["DeptID"] = ddlDepartmentQA.SelectedValue;
                                dr["Role"] = ddlRoles.SelectedItem.Text.Trim();
                                dr["Status"] = null;
                                dr["DocStatus"] = null;
                                dr["ActiveStatus"] = null;
                                dtTemp.Rows.Add(dr);
                                DataRow drRemoveEmpID = ((ViewState["dtRemoveEmpID"]) as DataTable).NewRow();
                                drRemoveEmpID["EmpID"] = lstQA.Items[x].Value;
                                drRemoveEmpID["Control"] = "AP";
                                drRemoveEmpID["Role"] = ddlRoles.SelectedItem.Text.Trim();
                                ((ViewState["dtRemoveEmpID"]) as DataTable).Rows.Add(drRemoveEmpID);
                            }
                        }
                    }
                }
                ViewState["ApproverData"] = dtTemp;
                RolesDivGV.Visible = true;
                gvApprover.DataSource = dtTemp;
                gvApprover.DataBind();
                ClearDropDown();
                ShowDcoumentList();
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M32:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M32:" + strline + "  " + "Add Initiation request approver/Document reviewer/Document approver failed.", "error");
            }
            finally
            {
                btnQA.Enabled = true;
            }
        }
        public void QA_GetDepartmentList()
        {
            try
            {
                DataTable DMS_Dt = DMS_BalDM.DMS_GetDepartmentList();
                ddlDepartmentQA.DataValueField = DMS_Dt.Columns["DeptID"].ToString();
                ddlDepartmentQA.DataTextField = DMS_Dt.Columns["DepartmentName"].ToString();
                ddlDepartmentQA.DataSource = DMS_Dt;
                ddlDepartmentQA.DataBind();
                ddlDepartmentQA.Items.Insert(0, new ListItem("--Select Department--", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M33:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M33:" + strline + "  " + "Roles Department load failed.", "error");
            }
        }
        protected void ddlRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearDropDown();
            ShowDcoumentList();
        }
        protected void DMS_GetRoleName()
        {
            try
            {
                ddlRoles.Items.Clear();
                if (ddl_requestType.SelectedValue == "1" || ddl_requestType.SelectedValue == "2" || ddl_requestType.SelectedValue == "5")
                {
                    ddlRoles.Items.Insert(0, new ListItem("--Select Role--", "0"));
                    ddlRoles.Items.Insert(1, new ListItem("Initiation Request Approver", "1"));
                    ddlRoles.Items.Insert(2, new ListItem("Document Reviewer", "2"));
                    ddlRoles.Items.Insert(3, new ListItem("Document Approver", "3"));
                }
                else if (ddl_requestType.SelectedValue == "3")
                {
                    ddlRoles.Items.Insert(0, new ListItem("--Select Role--", "0"));
                    ddlRoles.Items.Insert(1, new ListItem("Initiation Request Approver", "1"));
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M34:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M34:" + strline + "  " + "Roles loading based on request-type failed.", "error");
            }
        }
        public void Rejected_comments()
        {
            try
            {
                docObjects.Version = Convert.ToInt32(Request.QueryString["DocumentID"].ToString());
                docObjects.Mode = 2;
                DataTable DMS_Commets = DMS_Bal.DMS_GetTempRevertedReviewersDetails(docObjects);
                if (DMS_Commets.Rows.Count > 0)
                {
                    txt_RevertComments.Text = DMS_Commets.Rows[0][0].ToString();
                }
                else
                {
                    txt_RevertComments.Text = "";
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M36:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M36:" + strline + "  " + "reject comments loading failed.", "error");
            }
        }
        protected void gvApprover_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (Request.QueryString["From"] != null)
                {
                    if (Request.QueryString["From"] == "M")
                    {
                        if (e.Row.RowType == DataControlRowType.DataRow)
                        {
                            TextBox txtStatus = (TextBox)(e.Row.FindControl("lblStatus"));
                            TextBox txtDocStatus = (TextBox)(e.Row.FindControl("lblDocStatus"));
                            LinkButton lbtnDelete = (LinkButton)(e.Row.FindControl("lnkDelete"));
                            LinkButton lbtnEdit = (LinkButton)(e.Row.FindControl("lnkEdit"));
                            TextBox txtActiveStatus = (TextBox)(e.Row.FindControl("lblActiveStatus"));
                            string Role = e.Row.Cells[0].Text;
                            //0,1R,1A,1Au(CR,R,AP,AU),1(R,AP,AU),2(AP,AU),3(AU)
                            bool isVisible = true;
                            lbtnDelete.Visible = false;
                            lbtnEdit.Visible = false;
                            switch (txtDocStatus.Text)
                            {
                                case "1":
                                    if (Role == "Initiation Request Approver")
                                    {
                                        isVisible = false;
                                    }
                                    break;
                                case "2":
                                    if (Role == "Initiation Request Approver" || Role == "Document Reviewer")
                                    {
                                        isVisible = false;
                                    }
                                    break;
                                case "3":
                                    if (Role == "Initiation Request Approver" || Role == "Document Reviewer" || Role == "Document Approver")
                                    {
                                        isVisible = false;
                                    }
                                    break;
                                case "1R":
                                    if (Role == "Initiation Request Approver")
                                    {
                                        isVisible = false;
                                    }
                                    break;
                                case "1A":
                                    if (Role == "Initiation Request Approver")
                                    {
                                        isVisible = false;
                                    }
                                    break;
                                case "1Au":
                                    if (Role == "Initiation Request Approver")
                                    {
                                        isVisible = false;
                                    }
                                    break;
                            }
                            if (isVisible)
                            {
                                if (txtStatus.Text == "" || (txtDocStatus.Text == "1R" || txtDocStatus.Text == "1A" || txtDocStatus.Text == "1Au" || txtDocStatus.Text == "AR"))
                                    lbtnEdit.Visible = true;
                            }
                            if (txtActiveStatus.Text == "I")
                            {
                                e.Row.Attributes.Add("Class", "inactive_emp");
                            }
                        }
                        ShowDcoumentList();

                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M37:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M37:" + strline + "  " + "Row Data-bound for edit button failed.", "error");
            }
        }
        protected void gvApprover_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "edt")
                {
                    //Determine the RowIndex of the Row whose Button was clicked.
                    int rowIndex = Convert.ToInt32(e.CommandArgument);

                    //Reference the GridView Row.
                    GridViewRow row = gvApprover.Rows[rowIndex];

                    //Fetch value of Name.
                    int EmpID = Convert.ToInt32((row.FindControl("lblEmpId") as TextBox).Text);
                    int DeptID = Convert.ToInt32((row.FindControl("lblDeptID") as TextBox).Text);
                    string ActiveStatus = (row.FindControl("lblActiveStatus") as TextBox).Text;
                    ViewState["EditEmpID"] = EmpID;
                    ViewState["EditDeptID"] = DeptID;
                    string Role = row.Cells[0].Text;
                    string EmpName = row.Cells[1].Text;
                    if (Role == "Initiation Request Approver")
                    {
                        ddlRoles.SelectedValue = "1";
                    }
                    else if (Role == "Document Reviewer")
                    {
                        ddlRoles.SelectedValue = "2";
                    }
                    else if (Role == "Document Approver")
                    {
                        ddlRoles.SelectedValue = "3";
                    }
                    else
                    {
                        ddlRoles.SelectedValue = "0";
                    }
                    ddlDepartmentQA.SelectedValue = DeptID.ToString();
                    if (ddlDepartmentQA.SelectedIndex > 0)
                    {
                        if (ddlRoles.SelectedValue == "0")
                        {
                        }
                        else if (ddlRoles.SelectedValue == "1")
                        {
                            UMS_BAL objUMS_Bal = new UMS_BAL();
                            DataTable DMS_Dt = objUMS_Bal.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(ddlDepartmentQA.SelectedValue), (int)DMS_UserRole.Approver_DMS, true);
                            if (DMS_Dt.Rows.Count > 0)
                            {
                                if (ViewState["dtRemoveEmpID"] != null)
                                {
                                    DataTable dtTemp = ViewState["dtRemoveEmpID"] as DataTable;
                                    if (dtTemp.Rows.Count > 0)
                                    {
                                        for (int i = 0; i < dtTemp.Rows.Count; i++)
                                        {
                                            for (int j = 0; j < DMS_Dt.Rows.Count; j++)
                                            {
                                                int DMSEmpID = Convert.ToInt32(DMS_Dt.Rows[j]["EmpID"].ToString());
                                                int RemoveID = Convert.ToInt32(dtTemp.Rows[i]["EmpID"].ToString());
                                                string DMSRole = dtTemp.Rows[i]["Role"].ToString();
                                                if (RemoveID == DMSEmpID && DMSRole != "Document Approver" && DMSRole != "Document Reviewer" && DMSEmpID != EmpID)
                                                {
                                                    DMS_Dt.Rows.RemoveAt(j);
                                                }
                                            }
                                        }
                                    }
                                    if (Request.QueryString["From"] == "M")
                                    {
                                        SinglelstQA.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                                        SinglelstQA.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                                        SinglelstQA.DataSource = DMS_Dt;
                                        SinglelstQA.DataBind();
                                    }
                                    else
                                    {
                                        lstQA.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                                        lstQA.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                                        lstQA.DataSource = DMS_Dt;
                                        lstQA.DataBind();
                                    }
                                    if (DMS_Dt.Rows.Count > 0)
                                    {
                                        ViewState["ApproverDeptcode"] = DMS_Dt.Rows[0]["DeptCode"].ToString();
                                    }
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>   There are no Initiation Request Approvers for this Department.','warning');", true);
                                ClearDropDown();
                            }
                        }
                        else if (ddlRoles.SelectedValue == "2")
                        {
                            UMS_BAL objUMS_Bal = new UMS_BAL();
                            DataTable DMS_Dt = objUMS_Bal.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(ddlDepartmentQA.SelectedValue), (int)DMS_UserRole.Reviewer_DMS, true);
                            if (DMS_Dt.Rows.Count > 0)
                            {
                                if (ViewState["dtRemoveEmpID"] != null)
                                {
                                    DataTable dtTemp = ViewState["dtRemoveEmpID"] as DataTable;
                                    if (dtTemp.Rows.Count > 0)
                                    {
                                        for (int i = 0; i < dtTemp.Rows.Count; i++)
                                        {
                                            for (int j = 0; j < DMS_Dt.Rows.Count; j++)
                                            {
                                                int DMSEmpID = Convert.ToInt32(DMS_Dt.Rows[j]["EmpID"].ToString());
                                                int RemoveID = Convert.ToInt32(dtTemp.Rows[i]["EmpID"].ToString());
                                                string DMSRole = dtTemp.Rows[i]["Role"].ToString();
                                                if (RemoveID == DMSEmpID && DMSRole != "Initiation Request Approver" && DMSEmpID != EmpID)
                                                {
                                                    DMS_Dt.Rows.RemoveAt(j);
                                                }
                                            }
                                        }
                                    }
                                    if (Request.QueryString["From"] == "M")
                                    {
                                        SinglelstQA.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                                        SinglelstQA.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                                        SinglelstQA.DataSource = DMS_Dt;
                                        SinglelstQA.DataBind();
                                    }
                                    else
                                    {
                                        lstQA.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                                        lstQA.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                                        lstQA.DataSource = DMS_Dt;
                                        lstQA.DataBind();
                                    }
                                    if (DMS_Dt.Rows.Count > 0)
                                    {
                                        ViewState["ApproverDeptcode"] = DMS_Dt.Rows[0]["DeptCode"].ToString();
                                    }
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>   There are no Document Reviewers for this Department.','warning');", true);
                                ClearDropDown();
                            }
                        }
                        else if (ddlRoles.SelectedValue == "3")
                        {
                            UMS_BAL objUMS_Bal = new UMS_BAL();
                            DataTable DMS_Dt = objUMS_Bal.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(ddlDepartmentQA.SelectedValue), (int)DMS_UserRole.Approver_DMS, true);
                            if (DMS_Dt.Rows.Count > 0)
                            {
                                if (ViewState["dtRemoveEmpID"] != null)
                                {
                                    DataTable dtTemp = ViewState["dtRemoveEmpID"] as DataTable;
                                    if (dtTemp.Rows.Count > 0)
                                    {
                                        for (int i = 0; i < dtTemp.Rows.Count; i++)
                                        {
                                            for (int j = 0; j < DMS_Dt.Rows.Count; j++)
                                            {
                                                int DMSEmpID = Convert.ToInt32(DMS_Dt.Rows[j]["EmpID"].ToString());
                                                int RemoveID = Convert.ToInt32(dtTemp.Rows[i]["EmpID"].ToString());
                                                string DMSRole = dtTemp.Rows[i]["Role"].ToString();
                                                if (RemoveID == DMSEmpID && DMSRole != "Initiation Request Approver" && DMSEmpID != EmpID)
                                                {
                                                    DMS_Dt.Rows.RemoveAt(j);
                                                }
                                            }
                                        }
                                    }
                                    if (Request.QueryString["From"] == "M")
                                    {
                                        SinglelstQA.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                                        SinglelstQA.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                                        SinglelstQA.DataSource = DMS_Dt;
                                        SinglelstQA.DataBind();
                                    }
                                    else
                                    {
                                        lstQA.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                                        lstQA.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                                        lstQA.DataSource = DMS_Dt;
                                        lstQA.DataBind();
                                    }
                                    if (DMS_Dt.Rows.Count > 0)
                                    {
                                        ViewState["ApproverDeptcode"] = DMS_Dt.Rows[0]["DeptCode"].ToString();
                                    }
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>   There are no Document Approvers for this Department.','warning');", true);
                            }
                        }
                    }
                    if (Request.QueryString["From"] == "M")
                    {
                        if(ActiveStatus == "A" || ActiveStatus == "")
                        {
                            SinglelstQA.SelectedValue = EmpID.ToString();
                        }
                        else
                        {
                            //SinglelstQA.Items.Insert(0, new ListItem("--Select Employee--", "0"));
                            //SinglelstQA.SelectedValue = "0";
                            //SinglelstQA.Items[0].Attributes.Add("Class", "regulatory_dropdown_inactive");

                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Employee <b>" + EmpName + "<b> <br>is In-Active , select another employee.", "info", "showInactiveStyle();");                           
                        }                       
                        btnQA.Visible = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show3", "showInactive();", true);
                    }
                    else
                    {
                        lstQA.SelectedValue = EmpID.ToString();
                    }
                    ViewState["EditRowIndex"] = rowIndex;
                }
                ShowDcoumentList();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M38:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M38:" + strline + "  " + "Row Command for edit button failed.", "error");
            }
        }
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            ShowDcoumentList();
        }
        protected void lstQA_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int count = -1;
                for (int i = 0; i < lstQA.Items.Count; i++)
                {

                    if (lstQA.Items[i].Selected == true)
                    {
                        count++;
                        lstQA.Items.Insert(count, new ListItem(lstQA.Items[i].Text.ToString(), lstQA.Items[i].Value));
                        lstQA.Items.RemoveAt(i + 1);
                        lstQA.Items.FindByValue(lstQA.Items[count].Value).Selected = true;
                    }
                }
                ShowDcoumentList();
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop4", "MakeDropdownOpen();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M39:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M39:" + strline + "  " + "lstQA Selected Index Changed failed.", "error");
            }
        }
        public void Reject()
        {
            try
            {
                docObjects.Version = Convert.ToInt32(Request.QueryString["DocumentID"]);
                docObjects.DMSDocControllerID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                docObjects.Remarks = Regex.Replace(txt_Remarks.Value.Trim(), @"\s+", " ");
                int DMS_AP = DMS_BalDM.DMS_RejectDocByAdmin(docObjects);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document with Document Number <b>" + txt_DocumentNumber.Text + "</b> has been Rejected Successfully.", "success", "ReloadManagePage()");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M40:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M40:" + strline + "  " + "Document Reject failed.", "error");
            }
        }
        protected void btnNewDocName_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["DocNameChange"] = "1";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "RevisionDocIntNameChange();", true);
                UpDocumentName.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M41:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M41:" + strline + "  " + "New Document Update failed.", "error");
            }
        }
        public bool ddlInitiatorList(string DeptID,string InitiatorID="")
        {
            try
            {              
                UMS_BAL objUMS_Bal = new UMS_BAL();
                DataTable DMS_List = objUMS_Bal.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(DeptID), (int)DMS_UserRole.Initiator_DMS, true);
                ddl_Initiator.DataSource = DMS_List;
                ddl_Initiator.DataValueField = DMS_List.Columns["EmpID"].ToString();
                ddl_Initiator.DataTextField = DMS_List.Columns["EmpName"].ToString();
                ddl_Initiator.DataBind();
                ddl_Initiator.Items.Insert(0, new ListItem("--Select Initiator--", "0"));
                if (InitiatorID != "")
                {
                    DataRow[] drInitiatorID = DMS_List.Select("EmpID=" + InitiatorID);
                    if (drInitiatorID.Length > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M42:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M42:" + strline + "  " + "Initiator list Dropdown Loading failed.", "error");
                return false;
            }
        }
        public void DMS_GetControllerList()
        {
            try
            {
                DataTable DMS_Dt = DMS_Bal.DMS_GetControllersList();
                if (DMS_Dt.Rows.Count > 0)
                {
                    //DataRow[] filtered = DMS_Dt.Select("EmpID NOT IN ('" + (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString() + "','" + ddl_ReviewedBy.SelectedValue + "')");
                    //if (filtered.Any())
                    //{
                        //DMS_Dt = filtered.CopyToDataTable();
                        ddl_Controller.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                        ddl_Controller.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                        ddl_Controller.DataSource = DMS_Dt;
                        ddl_Controller.DataBind();
                        ddl_Controller.Items.Insert(0, new ListItem("-- Select Document Controller --", "0"));
                    //}
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>   There are no Employees with Controller Role.','warning');", true);
                    //literalSop1.Text = "There are no employees with controller role.";
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DI_M43:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DI_M43:" + strline + "  " + "Controller list loading failed.", "error");
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
        
    }
}