﻿using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.UserControls;
using System.Text.RegularExpressions;

namespace AizantIT_PharmaApp.DMS.DocumentIntiator
{
    public partial class Document_Type : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocObjects docObjects = new DocObjects();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            ConfirmCustomAlert.buttonClick += new EventHandler(ConfirmAdd_Click);
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=2").Length > 0)//2-DMSAdmin
                            {
                                DMS_GetProcessTypeList();
                                DMS_GetRenewYearsList();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DT_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DT_M1:" + strline + "  " + "Document Load failed.", "error");
            }
        }
        public void DMS_GetProcessTypeList()
        {
            try
            {
                DataTable DMS_Dt = DMS_Bal.DMS_GetProcessList();
                ddl_ProcessType.DataValueField = DMS_Dt.Columns["ProcessTypeID"].ToString();
                ddl_ProcessType.DataTextField = DMS_Dt.Columns["ProcessType"].ToString();
                ddl_ProcessType.DataSource = DMS_Dt;
                ddl_ProcessType.DataBind();
                ddl_ProcessType.Items.Insert(0, new ListItem("--Select Review Process Type--", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DT_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DT_M2:" + strline + "  " + "Process Type Load failed.", "error");
            }
        }
        public void DMS_GetRenewYearsList()
        {
            try
            {
                DataTable DMS_Dt = DMS_Bal.DMS_GetRenewYearsBal();
                ddlReviewPeriod.DataValueField = DMS_Dt.Columns["RenewYearID"].ToString();
                ddlReviewPeriod.DataTextField = DMS_Dt.Columns["RenewYear"].ToString();
                ddlReviewPeriod.DataSource = DMS_Dt;
                ddlReviewPeriod.DataBind();
                //ddlReviewPeriod.Items.Insert(0, new ListItem("--Select Review Year--", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DT_RY1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DT_RY1:" + strline + "  " + "Review Year Load failed.", "error");
            }
        }
        protected void ConfirmAdd_Click(object sender, EventArgs e)
        {
            try
            {
                docObjects.DocumentType = Regex.Replace(txt_DocumentType.Text.Trim(), @"\s+", " ");
                docObjects.ProcessType = ddl_ProcessType.SelectedValue;
                docObjects.ReviewYear = ddlReviewPeriod.SelectedValue;
                docObjects.DMS_CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                if (btn_RADD.Text.Trim().ToLower() == "submit")
                {
                    int DMS_Ins = DMS_Bal.DMS_OtherDocumentTypeInsertion(docObjects, (RadioButtonList1.SelectedValue == "yes" ? true : false));
                    reset();
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "  Document Type created successfully.", "success", "RefreshPage()");
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop1", "$('#myModal_lock .close').click();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DT_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DT_M3:" + strline + "  " + "Document Type Insertion failed.", "error");
            }
        }
        protected void btn_RADD_Click(object sender, EventArgs e)
        {
            btn_RADD.Enabled = false;
            try
            {
                StringBuilder sbErrorMsg = new StringBuilder();
                if (txt_DocumentType.Text.Trim() == "")
                {
                    sbErrorMsg.Append("Enter Document Type." + "<br/>");
                }
                if (ddl_ProcessType.SelectedValue.ToString().Trim() == "0")
                {
                    sbErrorMsg.Append("Select Review Process Type." + "<br/>");
                }
                if(RadioButtonList1.SelectedValue == "yes")
                {
                    if (ddlReviewPeriod.SelectedValue.ToString().Trim() == "0")
                    {
                        sbErrorMsg.Append("Select Review Period." + "<br/>");
                    }
                }
                
                if (btn_RADD.Text.Trim().ToLower() == "update")
                {
                    if (txtComments.Value.Trim() == "")
                    {
                        sbErrorMsg.Append("Enter comments." + "<br/>");
                    }
                }
                if (sbErrorMsg.Length > 8)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('" + sbErrorMsg.ToString() + "','error');", true);
                    return;
                }
                docObjects.DocumentType = Regex.Replace(txt_DocumentType.Text.Trim(), @"\s+", " ");
                DataTable DMS_Dt = DMS_Bal.DMS_GetDocumentTypeList(docObjects);
               
                    if (DMS_Dt.Rows.Count > 0 && (ddlReviewPeriod.SelectedValue == DMS_Dt.Rows[0]["ReviewYears"].ToString()))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop2", "custAlertMsg(' <br />  Document Type Already Exists.','warning');", true);
                        return;
                    }
                else
                {
                    if (btn_RADD.Text.Trim().ToLower() == "submit")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ConformAlert();", true);
                    }
                    else if (btn_RADD.Text.Trim().ToLower() == "update")
                    {
                        docObjects.Remarks = Regex.Replace(txtComments.Value.Trim(), @"\s+", " ");
                        docObjects.DMS_CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        docObjects.TypeID = hdfPKID.Value;
                        docObjects.ReviewYear = ddlReviewPeriod.SelectedValue;
                        int DMS_Ins = DMS_Bal.DMS_OtherDocumentTypeInsertionUpdate(docObjects, (RadioButtonList1.SelectedValue == "yes" ? true : false));
                        reset();
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document Type Updated Successfully.", "success", "RefreshPage()");
                    }
                }
                UpSubmit.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop1", "$('#myModal_lock .close').click()", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DT_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DT_M4:" + strline + "  " + "Document Type Insertion confirm/Update failed.", "error");
            }
            finally
            {
                btn_RADD.Enabled = true;
            }
        }
        public void reset()
        {
            try
            {
                ddl_ProcessType.SelectedIndex = -1;
                txt_DocumentType.Text = "";
                dvComments.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DT_M5:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DT_M5:" + strline + "  " + "reset failed.", "error");
            }
        }
        //protected void btn_Reset_Click(object sender, EventArgs e)
        //{
        //    reset();
        //}
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        protected void btn_Close_Click(object sender, EventArgs e)
        {
            btn_Close.Enabled = false;
            try
            {
                span_FileTitle.InnerText = "Add Document";
                btn_RADD.Text = "Submit";
                ddl_ProcessType.Enabled = true;
                dvComments.Visible = false;
                upDocTypeModelBody.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HideAddPanel();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DT_M6:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DT_M6:" + strline + "  " + "Close failed.", "error");
            }
            finally
            {
                btn_Close.Enabled = true;
            }
        }
        protected void txt_DocumentType_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txt_DocumentType.Text.Trim().Length > 0)
                {
                    docObjects.DocumentType = txt_DocumentType.Text.Trim();
                    DataTable DMS_Dt = DMS_Bal.DMS_GetDocumentTypeList(docObjects);
                    if (DMS_Dt.Rows.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br />  Document Type  Already Exists.','warning');", true);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DT_M7:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DT_M7:" + strline + "  " + "Document Type TextChanged Failed.", "error");
            }
        }
        protected void btnDocTypeEdit_Click(object sender, EventArgs e)
        {
            btnDocTypeEdit.Enabled = false;
            try
            {
                btn_RADD.Text = "Update";
                dvComments.Visible = true;
                span_FileTitle.InnerHtml = "<h4 class='modal-title'>Edit Document</h4>";
                DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
                JQDataTableBO objJQDataTableBO = new JQDataTableBO();
                objJQDataTableBO.iMode = 0;
                objJQDataTableBO.pkid = Convert.ToInt32(hdfPKID.Value);
                objJQDataTableBO.iDisplayLength = 0;
                objJQDataTableBO.iDisplayStart = 0;
                objJQDataTableBO.iSortCol = 0;
                objJQDataTableBO.sSortDir = "";
                objJQDataTableBO.sSearch = "";
                DataTable dt = DMS_Bal.DMS_GetDocumentTypeList(objJQDataTableBO);
                ddl_ProcessType.SelectedValue = dt.Rows[0]["pt"].ToString();
                ddlReviewPeriod.SelectedValue = dt.Rows[0]["ReviewYears"].ToString();
                txt_DocumentType.Text = dt.Rows[0]["DocumentType"].ToString();
                if (dt.Rows[0]["Training"].ToString() == "Yes")
                {
                    RadioButtonList1.SelectedValue = "yes";
                } 
                else
                {
                    RadioButtonList1.SelectedValue = "no";
                }
                ddl_ProcessType.Enabled = false;
                RadioButtonList1.Enabled = false;
                txtComments.Value = "";
                upDocTypeModelBody.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "showPopup();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DT_M8:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DT_M8:" + strline + "  " + "Edit Document Click failed.", "error");
            }
            finally
            {
                btnDocTypeEdit.Enabled = true;
            }
        }
        protected void btnAddDocument_Click(object sender, EventArgs e)
        {
            btnAddDocument.Enabled = false;
            try
            {
                span_FileTitle.InnerText = "Add Document";
                dvComments.Visible = false;
                txt_DocumentType.Text = "";
                ddl_ProcessType.SelectedValue = "0";
                ddl_ProcessType.Enabled = true;
                RadioButtonList1.Enabled = true;
                RadioButtonList1.SelectedValue = "no";
                ddlReviewPeriod.SelectedValue = "0";
                btn_RADD.Text = "Submit";
                upAddDocument.Update();
                upDocTypeModelBody.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "$('#myModal_lock').modal({ show: true, backdrop: 'static', keyboard: false });", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DT_M9:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DT_M9:" + strline + "  " + "Add Document Click failed.", "error");
            }
            finally
            {
                btnAddDocument.Enabled = true;
            }
        }
        protected void btnCommentsHistoryView_Click(object sender, EventArgs e)
        {
            btnCommentsHistoryView.Enabled = false;
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocumentTypeComments(hdfPKID.Value);
                gv_CommentHistory.DataSource = DMS_DT;
                ViewState["HistoryDataTable"]= DMS_DT;
                gv_CommentHistory.DataBind();
                upBtns.Update();
                if (DMS_DT.Rows.Count > 0)
                {
                    span4.Attributes.Add("title", DMS_DT.Rows[0]["DocumentTitle"].ToString());
                    if (DMS_DT.Rows[0]["DocumentTitle"].ToString().Length > 60)
                    {
                        span4.InnerHtml = DMS_DT.Rows[0]["DocumentTitle"].ToString().Substring(0, 60) + "...";
                    }
                    else
                    {
                        span4.InnerHtml = DMS_DT.Rows[0]["DocumentTitle"].ToString();
                    }
                }
                else
                {
                    span4.InnerText = "Comment History";
                }
                upcomment.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myCommentHistory').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DT_M10:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DT_M10:" + strline + "  " + "History viewing failed.", "error");
            }
            finally
            {
                btnCommentsHistoryView.Enabled = true;
            }
        }
        protected void gv_CommentHistory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string _fullcomments = (e.Row.Cells[4].FindControl("txt_Comments") as TextBox).Text;
                    LinkButton _lnkmorecomments = (e.Row.Cells[4].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (e.Row.Cells[4].FindControl("lbcommentsless") as LinkButton);
                    if (_fullcomments.Length > 70)
                    {
                        (e.Row.Cells[4].FindControl("lbl_shortComments") as Label).Visible = true;
                        TextBox FullTbx = (e.Row.Cells[4].FindControl("txt_Comments") as TextBox);
                        (e.Row.Cells[4].FindControl("lbl_shortComments") as Label).Text = _fullcomments.Substring(0, 70) + "..";
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = true;

                        if (_fullcomments.Length > 250 && _fullcomments.Length <= 500)
                        {
                            FullTbx.Rows = 6;
                        }
                        else if (_fullcomments.Length > 500 && _fullcomments.Length <= 1000)
                        {
                            FullTbx.Rows = 9;
                        }
                        else if (_fullcomments.Length > 1000 && _fullcomments.Length <= 1500)
                        {
                            FullTbx.Rows = 12;
                        }
                        else if (_fullcomments.Length > 1500 && _fullcomments.Length <= 2000)
                        {
                            FullTbx.Rows = 15;
                        }
                        else if (_fullcomments.Length > 2000)
                        {
                            FullTbx.Rows = 20;
                        }
                        FullTbx.Visible = false;
                    }
                    else
                    {
                        (e.Row.Cells[4].FindControl("lbl_shortComments") as Label).Visible = true;
                        (e.Row.Cells[4].FindControl("txt_Comments") as TextBox).Visible = false;
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DT_M11:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DT_M11:" + strline + "  " + "Comment History row data-bound failed.", "error");
            }
        }
        protected void gv_CommentHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {                
                if (e.CommandName == "SM")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    LinkButton _lnkmorecomments = (row.Cells[4].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (row.Cells[4].FindControl("lbcommentsless") as LinkButton);
                    (row.Cells[4].FindControl("lbl_shortComments") as Label).Visible = false;
                    (row.Cells[4].FindControl("txt_Comments") as TextBox).Visible = true;
                    _lnklesscomments.Visible = true;
                    _lnkmorecomments.Visible = false;
                }
                else if (e.CommandName == "SL")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    LinkButton _lnkmorecomments = (row.Cells[4].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (row.Cells[4].FindControl("lbcommentsless") as LinkButton);
                    (row.Cells[4].FindControl("lbl_shortComments") as Label).Visible = true;
                    (row.Cells[4].FindControl("txt_Comments") as TextBox).Visible = false;
                    _lnklesscomments.Visible = false;
                    _lnkmorecomments.Visible = true;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DT_M12:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DT_M12:" + strline + "  " + "Comment History row command failed.", "error");
            }
        }
        protected void gv_CommentHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_CommentHistory.PageIndex = e.NewPageIndex;
                gv_CommentHistory.DataSource = (DataTable)(ViewState["HistoryDataTable"]);
                gv_CommentHistory.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DT_M13:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DT_M13:" + strline + "  " + "grid view page index changing failed.", "error");
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}