﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="FormReturnReportPage.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.Reports.FormReturnPage" %>

<%@ Register Assembly="DevExpress.XtraReports.v18.1.Web.WebForms, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .hidden {
            display: none;
        }

        dropdown-menu {
            max-height: auto !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ScriptManager>
    <link href="<%=ResolveUrl("~/DevexScripts/jquery-ui.css")%>" rel="stylesheet" type="text/css" />
    <script src="<%=ResolveUrl("~/DevexScripts/jquery-ui.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/cldr.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/cldr/event.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/cldr/supplemental.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/globalize.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/globalize/currency.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/globalize/date.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/globalize/message.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/globalize/number.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/knockout.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/ace.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/ext-language_tools.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/moment.min.js")%>"></script>
    <div class="padding-none">
        <asp:Button ID="btnAdd" runat="server" class="float-right  btn-signup_popup" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
    </div>
    <div class=" col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left">
        <div class="col-lg-12 col-sm-12 col-12 col-md-12 padding-none dms_outer_border float-left">
            <div class="col-md-12 col-lg-12 col-12 col-sm-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server" visible="false">
                <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left" id="divMainContainer" runat="server" visible="true">
                <div class=" col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-right ">
                    <div class="col-md-12 col-lg-12 col-12 col-sm-12  padding-none float-left">
                        <div class="grid_header col-md-12 col-lg-12 col-12 col-sm-12 float-left">Form Reconciliation Report</div>
                      <div class="col-12 float-left grid_panel_full border_top_none padding-none">
                        <div class="col-lg-12 col-sm-12 col-12 col-md-12 top padding-none float-left">
                            <div class="col-lg-3 padding-none form-group float-left" runat="server" id="div_Dept">
                                <label class="control-label col-sm-12 lbl" id="lblDepartment">Department<span class="mandatoryStar">*</span></label>
                                <div class="col-sm-12">
                                    <asp:DropDownList ID="ddlDepartment" runat="server" data-size="9" data-live-search="true" CssClass="form-control col-md-12 col-lg-12 col-12 col-sm-12 drop_down selectpicker regulatory_dropdown_style" AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-3 padding-none form-group float-left" runat="server" id="div_FormNum">
                                <label class="control-label col-sm-12 lbl" id="lblFormNum">Form Number<span class="mandatoryStar">*</span></label>
                                <div class="col-sm-12">
                                    <asp:DropDownList ID="ddlFormNum" runat="server" data-size="9" data-live-search="true" CssClass="form-control col-md-12 col-lg-12 col-12 col-sm-12 drop_down selectpicker regulatory_dropdown_style"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-3 padding-none form-group float-left" runat="server" id="div_FromDate">
                                <label class="control-label col-sm-12 lbl" id="lblFromDate">From Date</label>
                                <div class="col-sm-12">
                                    <asp:TextBox ID="txtSearchReturnsFromDate" CssClass="form-control login_input_sign_up" runat="server" placeholder="Select From Date" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-3 padding-none form-group float-left" runat="server" id="div_ToDate">
                                <label class="control-label col-sm-12  lbl" id="lblToDate">To Date</label>
                                <div class="col-sm-12">
                                    <asp:TextBox ID="txtSearchReturnsToDate" runat="server" CssClass="form-control login_input_sign_up" placeholder="Select To Date" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-sm-12 col-12 col-md-12  top float-left text-right bottom">
                           
                               <asp:LinkButton ID="btnFind" runat="server" CssClass="  btn-signup_popup" OnClick="btnFind_Click">Submit</asp:LinkButton>

                                <asp:Button ID="btnReset" class=" btn-revert_popup  "  Text="Reset" runat="server" OnClientClick="ReloadFormReturnReport();" OnClick="btnReset_Click" />
                            
                        </div>
                        <div class="form-group col-lg-12 float-left">
                            <dx:ASPxWebDocumentViewer ID="ASPxWebDocumentViewer1" runat="server" OnCacheReportDocument="ASPxDocumentViewer1_CacheReportDocument" OnRestoreReportDocumentFromCache="ASPxDocumentViewer1_RestoreReportDocumentFromCache">
                                <ClientSideEvents Init="function(s, e) {
                                s.previewModel.reportPreview.zoom(1);
                            }" />
                            </dx:ASPxWebDocumentViewer>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!----To Load Calendar ---->

    <script>
        $(document).ready(function () {
            //Binding Code
            myfunction();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                //Binding Code Again
                myfunction();
            }
        });
    </script>
    <!----End To Load Calendar ---->
    <script>
        function myfunction() {
            //for from date and to date validations fromdate should be < todate
            $(function () {
                $('#<%=txtSearchReturnsFromDate.ClientID%>').datetimepicker({
                  format: 'DD MMM YYYY',
                  useCurrent: false,
              });
              $('#<%=txtSearchReturnsToDate.ClientID%>').datetimepicker({
                  format: 'DD MMM YYYY',
                  useCurrent: false,
              });
              $('#<%=txtSearchReturnsFromDate.ClientID%>').on("dp.change", function (e) {
                  $('#<%=txtSearchReturnsToDate.ClientID%>').data("DateTimePicker").minDate(moment(e.date).format('DD MMM YYYY'));
                  var _mindate = new Date(moment(e.date).format('DD MMM YYYY'));
                    _mindate.setDate(_mindate.getDate() + 6)
                    $('#<%=txtSearchReturnsToDate.ClientID%>').val(_mindate.format('dd MMM yyyy'));
                  $('#<%=txtSearchReturnsToDate.ClientID%>').data("DateTimePicker").maxDate(moment().add(7, 'days'));
                });
            });
        }
    </script>
    <script>
        function Clearfromtodate() {
            document.getElementById('<%=txtSearchReturnsFromDate.ClientID%>').value = '';
            document.getElementById('<%=txtSearchReturnsToDate.ClientID%>').value = '';
        }
        function ReloadFormReturnReport() {
            window.open("<%=ResolveUrl("~/DMS/Reports/FormReturnReportPage.aspx")%>", "_self");
        }
    </script>
</asp:Content>
