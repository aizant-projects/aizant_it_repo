﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using DevExpress.XtraReports.UI;
using System;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using AizantIT_PharmaApp.DMS.DevXtraReports;
using UMS_BO;
using UMS_BusinessLayer;

namespace AizantIT_PharmaApp.DMS.Reports
{
    public partial class FormReturnPage : System.Web.UI.Page
    {
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        FormPrintRequest formPrint = new FormPrintRequest();
        DocObjects docObjects = new DocObjects();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=30").Length > 0)//controller role
                            {
                                DMS_GetDepartmentList();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRRP_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRRP_M1:" + strline + "  " + "Page loading failed.", "error");
            }
        }
        #region documentviewer
        protected void ASPxDocumentViewer1_CacheReportDocument(object sender, DevExpress.XtraReports.Web.CacheReportDocumentEventArgs e)
        {
            try
            {
                e.Key = Guid.NewGuid().ToString();
                Page.Session[e.Key] = e.SaveDocumentToMemoryStream();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRRP_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRRP_M2:" + strline + "  " + "Cache Report Document failed.", "error");
            }
        }
        protected void ASPxDocumentViewer1_RestoreReportDocumentFromCache(object sender, DevExpress.XtraReports.Web.RestoreReportDocumentFromCacheEventArgs e)
        {
            try
            {
                Stream stream = Page.Session[e.Key] as Stream;
                if (stream != null)
                    e.RestoreDocumentFromStream(stream);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRRP_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRRP_M3:" + strline + "  " + "Restore Report Document From Cache failed.", "error");
            }
        }
        #endregion documentviewer
        protected void btnFind_Click(object sender, EventArgs e)
        {
            btnFind.Enabled = false;
            try
            {
                StringBuilder sbErrorMsg = new StringBuilder();
                sbErrorMsg.Clear();
                if (ddlDepartment.SelectedValue.ToString().Trim() == "0")
                {
                    sbErrorMsg.Append(" Select Department." + "<br/>");
                }
                if (ddlFormNum.SelectedValue.ToString().Trim() == "0" || ddlFormNum.SelectedValue.ToString().Trim() == "")
                {
                    sbErrorMsg.Append(" Select Form Number." + "<br/>");
                }
                bool isFromDate = false, isToDate = false;

                if (string.IsNullOrEmpty(txtSearchReturnsFromDate.Text.Trim()))
                {
                    isFromDate = true;
                    //sbErrorMsg.Append(" Enter From Date." + "<br/>");
                }
                if (string.IsNullOrEmpty(txtSearchReturnsToDate.Text.Trim()))
                {
                    isToDate = true;
                    //sbErrorMsg.Append(" Select Dates." + "<br/>");
                }
                if((isFromDate==true && isToDate==false) ||(isFromDate==false && isToDate == true))
                {
                    var ValidationMsg = isFromDate == true ? "Select From Date." + " <br/>" : "Select To Date" + "<br/>";
                    sbErrorMsg.Append(ValidationMsg);
                }
               
                if (sbErrorMsg.Length > 8)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
                    return;
                }
                else
                {
                    formPrint.FormVersionID = Convert.ToInt32(ddlFormNum.SelectedValue);
                    formPrint.FromDate =  string.IsNullOrEmpty(txtSearchReturnsFromDate.Text) ? (DateTime?)null : Convert.ToDateTime(txtSearchReturnsFromDate.Text);
                    formPrint.ToDate =  string.IsNullOrEmpty(txtSearchReturnsToDate.Text) ? (DateTime?)null : Convert.ToDateTime(txtSearchReturnsToDate.Text);
                    DataTable dt = DMS_Bal.DMS_GetFormReturnReportDetails(formPrint);
                    if (dt.Rows.Count > 0)
                    {

                        FormReturnReport formReturnReport = new FormReturnReport();
                        ((XRLabel)(formReturnReport.FindControl("xrLblFrom", false))).Text = "From Date :" +(txtSearchReturnsFromDate.Text.Trim()==""?"N/A": DateTime.Parse(txtSearchReturnsFromDate.Text.Trim()).ToString("dd MMM yyyy"));
                        ((XRLabel)(formReturnReport.FindControl("xrLblTo", false))).Text = "To Date :" + (txtSearchReturnsToDate.Text.Trim() == "" ? "N/A" : DateTime.Parse(txtSearchReturnsToDate.Text.Trim()).ToString("dd MMM yyyy"));
                        ((XRLabel)(formReturnReport.FindControl("lblPrintedBy", false))).Text = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString();
                        DataTable _dtCompany = fillCompany();
                        if (!string.IsNullOrEmpty(_dtCompany.Rows[0]["CompanyLogo"].ToString()))
                        {
                            ((XRPictureBox)(formReturnReport.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                        }
                        formReturnReport.DataSource = dt;
                        formReturnReport.DataMember = dt.TableName;
                        formReturnReport.CreateDocument();
                        ASPxWebDocumentViewer1.OpenReport(formReturnReport);
                        ASPxWebDocumentViewer1.DataBind();
                    }
                    else
                        HelpClass.custAlertMsg(this, this.GetType(), "Form reconciliation data is not available.", "info");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRRP_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRRP_M4:" + strline + "  " + "From Return Report finding failed.", "error");
            }
            finally
            {
                btnFind.Enabled = true;
            }
        }
        public void DMS_GetDepartmentList()
        {
            try
            {
                DataTable DMS_Dt = DMS_BalDM.DMS_GetDepartmentList();
                ddlDepartment.DataValueField = DMS_Dt.Columns["DeptID"].ToString();
                ddlDepartment.DataTextField = DMS_Dt.Columns["DepartmentName"].ToString();
                ddlDepartment.DataSource = DMS_Dt;
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("--Select Department--", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRRP_M5:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRRP_M5:" + strline + "  " + "Department load failed.", "error");
            }
        }
        public void DMS_GetFormNumberList(int DeptID)
        {
            try
            {
                docObjects.DMS_Deparment = DeptID;
                DataTable DMS_Dt = DMS_BalDM.DMS_GetFormNumberList(docObjects);
                ddlFormNum.DataValueField = DMS_Dt.Columns["FormID"].ToString();
                ddlFormNum.DataTextField = DMS_Dt.Columns["FormNumber"].ToString();
                ddlFormNum.DataSource = DMS_Dt;
                ddlFormNum.DataBind();
                ddlFormNum.Items.Insert(0, new ListItem("--Select Form Number--", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRRP_M6:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRRP_M6:" + strline + "  " + "Form Number load failed.", "error");
            }
        }
        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDepartment.SelectedValue != "0")
            {
                DMS_GetFormNumberList(Convert.ToInt32(ddlDepartment.SelectedValue));
                txtSearchReturnsFromDate.Text = "";
                txtSearchReturnsToDate.Text = "";
            }
        }
        DataTable fillCompany()
        {
            try
            {
                CompanyBO objCompanyBO;
                UMS_BAL objUMS_BAL;
                DataTable dtcompany = new DataTable();
                objCompanyBO = new CompanyBO();
                objCompanyBO.Mode = 2;
                objCompanyBO.CompanyID = 0;
                objCompanyBO.CompanyCode = "";
                objCompanyBO.CompanyName = "";
                objCompanyBO.CompanyDescription = "";
                objCompanyBO.CompanyPhoneNo1 = "";
                objCompanyBO.CompanyPhoneNo2 = "";
                objCompanyBO.CompanyFaxNo1 = "";
                objCompanyBO.CompanyFaxNo2 = ""; //txtFaxNo2.Value;
                objCompanyBO.CompanyEmailID = "";
                objCompanyBO.CompanyWebUrl = "";
                objCompanyBO.CompanyAddress = "";
                objCompanyBO.CompanyLocation = "";// txtLocat.Value;
                objCompanyBO.CompanyCity = "";
                objCompanyBO.CompanyState = "";
                objCompanyBO.CompanyCountry = "";
                objCompanyBO.CompanyPinCode = "";
                objCompanyBO.CompanyStartDate = "";
                objCompanyBO.CompanyLogo = new byte[0];
                objUMS_BAL = new UMS_BAL();
                dtcompany = objUMS_BAL.CompanyQuery(objCompanyBO);
                return dtcompany;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private System.Drawing.Image BindApplicantImage(byte[] applicantImage)
        {
            using (var ms = new MemoryStream(applicantImage))
            {
                return System.Drawing.Image.FromStream(ms);
            }
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            btnReset.Enabled = false;
            try
            {
                ddlDepartment.SelectedValue = "0";
                ddlFormNum.SelectedValue = "0";
                txtSearchReturnsFromDate.Text = "";
                txtSearchReturnsToDate.Text = "";
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRRP_M8:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRRP_M8:" + strline + "  " + "search fields reset failed.", "error");
            }
            finally
            {
                btnReset.Enabled = true;
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}