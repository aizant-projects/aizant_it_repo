﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="DocReqReviewerList.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentReviewer.DocReqReviewerList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class=" col-lg-6 padding-none">
        <div class="closebtn">
            <span>&#9776;
            </span>
        </div>
        <div style="display: none;" class="closebtn1">
            <span>&#9776;</span>
        </div>
    </div>
    <div class=" col-lg-6 padding-none">
        <asp:Button ID="btnAdd" runat="server" class="pull-right btn btn-approve_popup" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
    </div>
    <div class=" col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none">
        <div class=" col-md-12 col-lg-12 col-xs-12 col-sm-12 dms_outer_border">
    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
        <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
    </div>
    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none" id="divMainContainer" runat="server" visible="true">
        <div class=" col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none pull-right ">
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 grid_panel_full padding-none">
                        <div class="grid_header col-md-12 col-lg-12 col-xs-12 col-sm-12 ">Doc Distribution Request Reviewer List</div>
                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 top bottom">
                            <table id="dtDocList" class="display" cellspacing="0" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>DocRequestID</th>
                                        <th>Request Number</th>
                                        <th>Document Name</th>
                                        <th>Print Purpose</th>
                                        <th>Page Numbers</th>
                                        <th>Review Request</th>
                                        <th>History</th>
                                    </tr>
                                </thead>
                            </table>                           
                        </div>
                    </div>
                </div>
        </div>
            </div>
        </div>

        <!-------- Request VIEW-------------->
        <div id="myModal_request" class="modal department fade" role="dialog">
            <div class="modal-dialog modal-lg" style="width: 98%">
                <!-- Modal content-->
                <div class="modal-content col-lg-12 padding-none">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <span id="span_FileTitle" runat="server">View Request</span>
                            </div>
                            <div class="modal-body">
                                <%--<div class="form-group  col-md-4 col-lg-3 col-xs-6 col-sm-6" id="RequestNumber">
                                    <asp:Label ID="Label3" runat="server" Text="Request Number"></asp:Label>
                                    <asp:TextBox ID="txt_RequestNumber" runat="server" CssClass="form-control" placeholder="Enter Request Number" ReadOnly="true" AutoCompleteType="Disabled" MaxLength="50"></asp:TextBox>
                                </div>--%>
                                <div id="div_RequestedBy" class="col-md-6 col-lg-6 col-xs-12 col-sm-12 form-group">
                                    <asp:Label ID="lblRequestedBy" runat="server" Text="Requested By"></asp:Label>
                                    <asp:TextBox ID="txt_RequestedBy" runat="server" CssClass="form-control" placeholder="Enter Requested By" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div id="divSoftCopy" class="form-group col-sm-12 col-lg-2 col-xs-12 col-md-12 col-sm-12" style="padding-left:50px">
                                    <asp:Label ID="lblRadiobutton" runat="server" Text="Type of Request"></asp:Label>
                                    <asp:RadioButtonList runat="server" ID="RadioButtonList1" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="SoftCopy" Value="yes"></asp:ListItem>
                                        <asp:ListItem Text="Print" Value="no"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                                <div id="div_NoofCopies" class="col-md-6 col-lg-1 col-xs-12 col-sm-12 form-group">
                                    <asp:Label ID="lblNoofCopies" runat="server" Text="No of Copies"></asp:Label>
                                    <asp:TextBox ID="txt_NoofCopies" runat="server" CssClass="form-control" TextMode="Number" Text="1" ReadOnly="true"></asp:TextBox>
                                </div>
                                  <div id="div_PageNo" class="col-md-6 col-lg-3 col-xs-12 col-sm-12 form-group">
                                    <asp:Label ID="lblPageNo" runat="server" Text="Page Numbers"></asp:Label>
                                    <asp:TextBox ID="txt_PageNo" runat="server" CssClass="form-control" placeholder="Enter Page Numbers" ReadOnly="true"></asp:TextBox>
                                   <%-- <asp:RegularExpressionValidator ID="revPageNo" runat="server" ErrorMessage="Invalid Character encountered !" ControlToValidate="txt_PageNo" ValidationExpression="^[0-9,-]*$" Display="Static" ForeColor="red"></asp:RegularExpressionValidator>--%>
                                </div>
                                <div id="div_DocName" class="col-md-6 col-lg-6 col-xs-12 col-sm-12 form-group">
                                    <asp:Label ID="Label4" runat="server" Text="Document Name"></asp:Label>
                                    <asp:TextBox ID="txt_DocumentName" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="Enter Document Name" AutoPostBack="true" Rows="2" MaxLength="500" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div id="div_Purpose" class="col-md-6 col-lg-6 col-xs-12 col-sm-12 form-group">
                                    <asp:Label ID="lblComments" runat="server" Text="Purpose"></asp:Label>
                                    <asp:TextBox ID="txt_Purpose" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="Enter Purpose" Width="100%" Rows="2" ReadOnly="true"></asp:TextBox>
                                </div>                                                            
                               <%-- <div id="div_ValidDays" class="col-md-6 col-lg-2 col-xs-12 col-sm-12 form-group">
                                    <asp:Label ID="lblValidDays" runat="server" Text="Validity(In Days)"></asp:Label><span class="smallred">*</span>
                                    <asp:TextBox ID="txt_ValidDays" runat="server" CssClass="form-control" placeholder="Enter Valid Days" TextMode="Number"></asp:TextBox>
                                </div>--%>
                                <div id="div_ReviewedBy" class="col-md-6 col-lg-6 col-xs-12 col-sm-12 form-group">
                                    <asp:Label ID="Label6" runat="server" Text="Reviewed By"></asp:Label>
                                    <asp:TextBox ID="txt_ReviewedBy" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div id="div_DocController" class="col-md-6 col-lg-6 col-xs-12 col-sm-12 form-group">
                                    <asp:Label ID="Label7" runat="server" Text="Document Controller"></asp:Label>
                                    <asp:TextBox ID="txt_DocController" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div id="div_Comments" class="col-md-12 col-lg-12 col-xs-12 col-sm-12 form-group" runat="server">
                                    <asp:Label ID="Label1" runat="server" Text="Comments"></asp:Label>
                                    <asp:TextBox ID="txt_Comments" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="Enter Comments" Width="100%" Rows="2"></asp:TextBox>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="modal-footer" id="buttons" runat="server">                             
                                <asp:Button ID="btn_Approve" Text="Approve" CssClass="btn btn-approve_popup" runat="server" ValidationGroup="DMS_DC" OnClick="btn_Approve_Click"/>
                                <asp:Button ID="btn_Reject" Text="Reject" CssClass="btn btn-revert_popup" runat="server" CausesValidation="false" OnClick="btn_Reject_Click" OnClientClick="return commentRequried();"/>
                                <asp:Button ID="btn_Cancel" Text="Cancel" data-dismiss="modal" CssClass="btn btn-canceldms_popup" runat="server" CausesValidation="false" />
                            </div>                         
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <!-------- End Request VIEW-------------->

    <!--------Comment History View-------------->
    <div id="myCommentHistory" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg" style="width:80%">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <span id="span4" runat="server">Comments History</span>
                        </div>
                        <div class="modal-body" style="max-height: 600px; overflow-y: auto">
                            <div class=" history_grid" style="overflow-y: auto;">
                                <asp:GridView ID="gv_CommentHistory" runat="server" CssClass="table table-hover table-responsive" ClientIDMode="Static" EmptyDataText="No Records Found" AutoGenerateColumns="False" PagerSettings-Mode="NumericFirstLast" BackColor="White" BorderColor="#d2d2d2" BorderStyle="None" BorderWidth="1px">
                                    <Columns>
                                        <asp:TemplateField HeaderText="User" ItemStyle-Width="118px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_GivenBy" runat="server" Text='<%# Eval("ActionBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" ItemStyle-Width="158px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Date" runat="server" Text='<%# Eval("ActionDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="218px">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Action" runat="server" Text='<%# Eval("Action") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments">
                                            <ItemTemplate>
                                                <%--<asp:Label ID="lbl_DocumentID" runat="server" Text='<%# Eval("Comments") %>'></asp:Label>--%>
                                                <%--<asp:textbox id="TextBox1" runat="server" Text='<%# Eval("Comments") %>' TextMode="MultiLine" ReadOnly="true"></asp:textbox>--%>
                                                <textarea id="TextArea1" runat="server" value='<%# Eval("Comments") %>' readonly="readonly"></textarea>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!--------End Comment History View-------------->
    <asp:HiddenField ID="hdfVID" runat="server" />
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
       <asp:UpdatePanel ID="UpGridButtons" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnRequestReview" runat="server" Text="Submit" OnClick="btnRequestReview_Click" Style="display: none" />
            <asp:Button ID="btnCommentsHistoryView" runat="server" Text="Submit" Style="display: none" OnClick="btnCommentsHistoryView_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>
        $('#dtDocList').wrap('<div class="dataTables_scroll" />');
        var oTable = $('#dtDocList').DataTable({
            columns: [
                { 'data': 'RowNumber' },
                { 'data': 'DocReqID' },
                { 'data': 'RequestNumber' },
                { 'data': 'DocumentName' },
                { 'data': 'PrintPurpose' },
                { 'data': 'PageNumbers' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="view" title="View" href="#" onclick=ReviewRequest(' + o.DocReqID + ')></a>'; }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="summary_latest" title="History" href="#" onclick=ViewCommentHistory(' + o.DocReqID + ')></a>'; }
                }               
            ],
            "fixedHeader": {
                "header": true
            },
            "responsive": true,
            "scrollCollapse": true,
            "paging": true,
            "search": {
                "regex": true
            },
            "lengthMenu": [[10, 15, 25, 100], [10, 15, 25, 100]],
            "aoColumnDefs": [{ "targets": [1], "visible": false, "searchable": false }, { "bSortable": false, "aTargets": [0] }, { "className": "dt-body-left", "targets": [3, 4,5] },
            ],
            "order": [[1, "desc"]],
            'bAutoWidth': true,
            "bServerSide": true,
            "language": {
                searchPlaceholder: "Search Records", "infoFiltered": "",
                "language": {
                    searchPlaceholder: "Search Records",
                    "infoFiltered": "",
                    emptyTable: "No Records Available"
                },
            },
            sServerMethod: 'Post',
            "sAjaxSource": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetDocPrintReqReviewerList" )%>',
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#dtDocList").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
        function reloadtable() {
            oTable.ajax.reload();
            $('#myModal_request').modal('hide');
            //$('#usES_Modal').modal('hide');
        }
    </script>
    <script>
        function ReviewRequest(V_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = V_ID;
             var btnRV = document.getElementById("<%=btnRequestReview.ClientID %>");
            btnRV.click();
        }
        function ViewCommentHistory(PK_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = PK_ID;
            var btnCV = document.getElementById("<%=btnCommentsHistoryView.ClientID %>");
            btnCV.click();
        }
    </script>
     <script type="text/javascript">
         function commentRequried() {
             var comment = document.getElementById("<%=txt_Comments.ClientID%>").value;
             errors = [];
             if (comment == "") {
                 errors.push("Enter comments");
             }
             if (errors.length > 0) {
                 custAlertMsg(errors.join("<br/>"), "error");
                 return false;
             }
             else {
                 return true;
             }
         }
    </script>
</asp:Content>
