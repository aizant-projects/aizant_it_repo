﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AizantIT_DMSBAL;
using AizantIT_PharmaApp.Common;
using DevExpress.XtraRichEdit;
namespace AizantIT_PharmaApp.DMS.DocumentReviewer
{
    public partial class CreatedSOPView : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString.Count > 0)
                    {
                        string vid = Request.QueryString[0];
                        ToCreateSopDocument(vid);
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("CSV_M1:" + strline + "  " + strMsg);
                string strError = "CSV_M1:" + strline + "  " + "Page loading failed";
                lblError.Visible = true;
                lblError.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        private void ToCreateSopDocument(string vid)
        {
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(vid);
                if (DMS_DT.Rows[0]["Content"] != DBNull.Value)
                {
                    ASPxRichEdit1.Open(
                    Guid.NewGuid().ToString(),
                    DevExpress.XtraRichEdit.DocumentFormat.Rtf,
                    () =>
                    {
                        byte[] docBytes = (byte[])DMS_DT.Rows[0]["Content"];
                        return new MemoryStream(docBytes);
                    }
                );
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("CSV_M2:" + strline + "  " + strMsg);
                string strError = "CSV_M2:" + strline + "  " + "Create SOP loading failed";
                lblError.Visible = true;
                lblError.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}