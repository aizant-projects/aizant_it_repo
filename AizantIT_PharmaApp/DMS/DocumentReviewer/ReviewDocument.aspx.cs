﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AizantIT_PharmaApp.UserControls;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using UMS_BusinessLayer;
using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using System.Net;

namespace AizantIT_PharmaApp.DMS.DocumentReviewer
{
    public partial class ReviewDocument : System.Web.UI.Page
    {
        private Word2PDFConverter.RemoteConverter converter;
        private string storefolder = HttpContext.Current.Server.MapPath(@"~/Scripts/pdf.js/web/");
        UMS_BAL objUMS_BAL = new UMS_BAL();
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        ReviewObjects reviewObjects = new ReviewObjects();
        DocObjects docObjects = new DocObjects();
        DocObjects docObjects1 = new DocObjects();
        DocUploadBO objectDocUploadBO;
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            ConfirmCustomAlert.buttonClick += new EventHandler(ConfirmCustomAlert_buttonClick);
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    converter = (Word2PDFConverter.RemoteConverter)Activator.GetObject(typeof(Word2PDFConverter.RemoteConverter), "http://localhost:" + System.Configuration.ConfigurationManager.AppSettings["Word2PdfPort"].ToString() + "/RemoteConverter");
                    if (!IsPostBack)
                    {
                        Session["vwdocObjectsR"] = null;
                        Session["vwreviewObjectsR"] = null;
                        Session["vwdocObjects1R"] = null;
                        ViewState["PDFPhysicalName"] = "";
                        System.Data.DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        hdfRName.Value = "Changed By " + (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString();
                        System.Data.DataTable dtTemp = new System.Data.DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=10").Length > 0)//10-Reviewer
                            {
                                if (Request.QueryString["Notification_ID"] != null)
                                {
                                    objUMS_BAL = new UMS_BAL();
                                    int[] empIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, empIDs); //2 represents NotifyEmp had visited the page through notification link.                                                                                                                                                          
                                }
                                InitializeThePage();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                    else
                    {
                        ElectronicSign.buttonClick += new EventHandler(ElectronicSign_buttonClick);
                        getTime();
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop", "OpenReadSopModal();", true);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "reload();", true);
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("RRD_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "RRD_M1:" + strline + "  " + "Document Load failed.", "error");
            }
        }
        protected void ConfirmCustomAlert_buttonClick(object sender, EventArgs e)
        {
            try
            {
                if (hdnAction.Value == "Save Comments")
                {
                    reviewObjects.Id = Convert.ToInt32(hdfPKID.Value);
                    reviewObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    reviewObjects.DocStatus = "R";
                    reviewObjects.Comments = txt_Reject.Value.Trim();
                    reviewObjects.ReviewedTime = hdnHours.Value + ":" + hdnMinutes.Value + ":" + hdnSeconds.Value;
                    int DMS_update = DMS_BalDM.DMS_SaveReviewComments(reviewObjects);
                    if (DMS_update > 0)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Reviewer Remarks Saved Successfully", "success");
                    }
                }
                else if (hdnAction.Value == "Revert")
                {
                    ViewState["Action"] = "Revert";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop4", "openUC_ElectronicSign();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("RRD_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "RRD_M2:" + strline + "  " + "Reviewer Comments saving failed.", "error");
            }
        }
        protected void ElectronicSign_buttonClick(object sender, EventArgs e)
        {
            try
            {
                bool b = ElectronicSign.IsPasswordValid;
                if (b == true)
                {
                    if (hdnAction.Value == "Approve")
                    {
                        Approve();
                    }
                    else if (hdnAction.Value == "Revert" || ViewState["Action"].ToString() == "Revert")
                    {
                        Revert();
                    }
                }
                if (b == false)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Password is Incorrect!", "error");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("RRD_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "RRD_M3:" + strline + "  " + "Electronic Signature validation failed.", "error");
            }
        }
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod(EnableSession = true)]
        public static void InsertCurrentDuration(string ID, string Hours, string Minutes, string Seconds)
        {
            try
            {
                DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
                DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
                ReviewObjects reviewObjects = new ReviewObjects();
                reviewObjects.Id = Convert.ToInt32(ID);
                reviewObjects.EmpID = Convert.ToInt32((HttpContext.Current.Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                reviewObjects.DocStatus = "R";
                reviewObjects.ReviewedTime = Hours + ":" + Minutes + ":" + Seconds;
                int DMS_Update = DMS_BalDM.DMS_UpdateReviewedTimeToReviewer(reviewObjects);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                log4net.ILog Aizant_log1 = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                Aizant_log1.Error("RRD_M35:" + strline + "  " + strMsg);
            }
        }
        public void getTime()
        {
            try
            {
                reviewObjects.Id = Convert.ToInt32(hdfPKID.Value);
                reviewObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                reviewObjects.DocStatus = "R";
                System.Data.DataTable DMS_Dt = DMS_Bal.DMS_GetsavedTimeByID(reviewObjects);
                if (DMS_Dt.Rows[0]["ReviewedTime"].ToString() != "")
                {
                    DataRow row = DMS_Dt.Rows[0];
                    string[] arr = Regex.Split(row["ReviewedTime"].ToString(), ":");
                    hdnHours.Value = arr[0];
                    hdnMinutes.Value = arr[1];
                    hdnSeconds.Value = arr[2];
                }
                else
                {
                    string[] arr = Regex.Split("0:0:0", ":");
                    hdnHours.Value = arr[0];
                    hdnMinutes.Value = arr[1];
                    hdnSeconds.Value = arr[2];
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("RRD_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "RRD_M4:" + strline + "  " + "Timer Time getting failed.", "error");
            }
        }
        protected void gvAssignedDocuments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvAssignedDocuments.PageIndex = e.NewPageIndex;
                gvAssignedDocuments.DataSource = (System.Data.DataTable)Session["dtAssignedDocuments"];
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowRefferal();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("RRD_M5:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "RRD_M5:" + strline + "  " + "Internal Documents Page index changing failed.", "error");
            }
        }
        protected void lnk_ViewInternal_Click(object sender, EventArgs e)
        {
            try
            {
                hdnGrdiReferesh.Value = "Internal";
                hdnNewDocTab.Value = "Refferal";
                LinkButton lnk = (LinkButton)sender;
                GridViewRow i = (GridViewRow)lnk.NamingContainer;
                Label lbl_text = (Label)i.FindControl("lbl_DocumentID");

                ViewState["isDetails"] = false;
                System.Data.DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(lbl_text.Text);
                span4.InnerHtml = "<b>" + DMS_DT.Rows[0]["DocumentName"].ToString() + "</b>";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "ShowInternalFile();", true);
                hdfVID.Value = lbl_text.Text;
                hdfViewType.Value = "0";
                hdfViewEmpID.Value = "0";
                hdfViewRoleID.Value = "0";
                if (hdnGrdiReferesh.Value == "Internal" && hdnNewDocTab.Value == "Refferal")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "TabRefClick();", true);
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view1", "ViewInternalDocument('#InternalRefDiv','" + lbl_text.Text + "');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("RRD_M6:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "RRD_M6:" + strline + "  " + "Internal Documents viewing failed.", "error");
            }
        }
        protected void lnkDownload_Click(object sender, EventArgs e)
        {
            try
            {
                hdnGrdiReferesh.Value = "External";
                hdnNewDocTab.Value = "Refferal";
                string filePath = (sender as LinkButton).CommandArgument;
                ExternalRefDiv.InnerHtml = "&nbsp";
                ExternalRefDiv.InnerHtml = "<iframe src=\"" + ResolveUrl("~/DMS/DocumentCreator/CreateRefferalInIFrame.aspx") + "?FilePath=" + filePath + "#toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" height=\"700px\"></iframe>";
                UpdatePanel10.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "ShowExternalFile();", true);
                if (hdnGrdiReferesh.Value == "External" && hdnNewDocTab.Value == "Refferal")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "TabRefClick();", true);
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("RRD_M7:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "RRD_M7:" + strline + "  " + "External Documents viewing failed.", "error");
            }
        }
        public void Revert()
        {
            try
            {
                System.Data.DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                objectDocUploadBO = new DocUploadBO();
                if (DMS_DT.Rows[0]["Status"].ToString() != "1R" && DMS_DT.Rows[0]["Status"].ToString() != "RJ")
                {
                    int isUpload = 0;

                    if (Session["vwdocObjectsR"] != null)
                    {
                        isUpload = 1;
                        objectDocUploadBO.docLocation = (DocObjects)(Session["vwdocObjectsR"]);
                        //int DOC_Ins = DMS_Bal.DMS_InsertDocumentLocation((DocObjects)(Session["vwdocObjectsR"]));
                    }
                    if (Session["vwreviewObjectsR"] != null)
                    {
                        isUpload = 1;
                        objectDocUploadBO.docContent = (ReviewObjects)(Session["vwreviewObjectsR"]);
                        //int DMS_Status = DMS_Bal.DMS_UpdateContentByID((ReviewObjects)(Session["vwreviewObjectsR"]));
                    }
                    if (Session["vwdocObjects1R"] != null)
                    {
                        isUpload = 1;
                        objectDocUploadBO.docComments = (DocObjects)(Session["vwdocObjects1R"]);
                        //DMS_Bal.DMS_DocComments((DocObjects)(Session["vwdocObjects1R"]));
                    }

                    reviewObjects.Id = Convert.ToInt32(hdfPKID.Value);
                    reviewObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    reviewObjects.DocStatus = "R";
                    reviewObjects.Comments = Regex.Replace(txt_Reject.Value.Trim(), @"\s+", " ");
                    if(DMS_DT.Rows[0]["RequestTypeID"].ToString() == "5")
                    {
                        reviewObjects.Process = "1";
                        reviewObjects.ReviewedTime = "N/A";
                    }
                    else
                    {
                        reviewObjects.Process = hdfPID.Value;
                        reviewObjects.ReviewedTime = hdnHours.Value + ":" + hdnMinutes.Value + ":" + hdnSeconds.Value;
                    }                
                    if (isUpload == 1)
                    {
                        reviewObjects.Status = 1;
                    }
                    else
                    {
                        reviewObjects.Status = 0;
                    }
                    objectDocUploadBO.docReviwer = reviewObjects;
                    objectDocUploadBO.docStatus= reviewObjects;
                    int DMS_Update = DMS_BalDM.DMS_UpdateCommetaToReviewer(objectDocUploadBO);
                    //int DMS_Update = DMS_BalDM.DMS_UpdateCommetaToReviewer(reviewObjects);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document Number <b>" + txt_DocumentNumber.Text + "</b> Reverted Successfully.", "success", "CancelReset()");
                }
                else if (DMS_DT.Rows[0]["Status"].ToString() == "1R")
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document with DocumentNumber <b>" + txt_DocumentNumber.Text + "</b> has been already Reverted.", "info", "CancelReset()");
                }
                else if (DMS_DT.Rows[0]["Status"].ToString() == "RJ")
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document with Document Number <b>" + txt_DocumentNumber.Text + "</b> has been already Rejected.", "info", "CancelReset()");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("RRD_M8:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "RRD_M8:" + strline + "  " + "Review status Reverted failed.", "error");
            }
        }
        public void Approve()
        {
            try
            {
                System.Data.DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                if (DMS_DT.Rows[0]["Status"].ToString() != "1R" && DMS_DT.Rows[0]["Status"].ToString() != "RJ")
                {
                    reviewObjects.Id = Convert.ToInt32(hdfPKID.Value);
                    reviewObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    reviewObjects.DocStatus = "R";
                    reviewObjects.Comments = Regex.Replace(txt_Reject.Value.Trim(), @"\s+", " ");
                    if(DMS_DT.Rows[0]["RequestTypeID"].ToString() == "5")
                    {
                        reviewObjects.Process = "1";
                        reviewObjects.ReviewedTime = "N/A";
                    }
                    else
                    {
                        reviewObjects.Process = hdfPID.Value;
                        reviewObjects.ReviewedTime = hdnHours.Value + ":" + hdnMinutes.Value + ":" + hdnSeconds.Value;
                    }                    
                    
                    int DMS_Update = DMS_BalDM.DMS_ApproveDocByReviewer(reviewObjects);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document with Number <b>" + txt_DocumentNumber.Text + "</b> Approved Successfully.", "success", "CancelReset()");
                }
                else if (DMS_DT.Rows[0]["Status"].ToString() == "1R")
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document with DocumentNumber <b>" + txt_DocumentNumber.Text + "</b> has been already Reverted.", "info", "CancelReset()");
                }
                else if (DMS_DT.Rows[0]["Status"].ToString() == "RJ")
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document with Document Number <b>" + txt_DocumentNumber.Text + "</b> has been already Rejected.", "info", "CancelReset()");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("RRD_M9:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "RRD_M9:" + strline + "  " + "Review status Approved failed.", "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        private void RetriveDocumentDetails(int DocumentID, int ProcessID)
        {
            try
            {
                hdfPKID.Value = DocumentID.ToString();
                System.Data.DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                if (DMS_DT.Rows.Count > 0)
                {
                    txt_requestType.Text = DMS_DT.Rows[0]["RequestType"].ToString();
                    txt_documentType.Text = DMS_DT.Rows[0]["DocumentType"].ToString();
                    txt_Department.Text = DMS_DT.Rows[0]["Department"].ToString();
                    txt_DocumentNumber.Text = DMS_DT.Rows[0]["DocumentNumber"].ToString();
                    txt_DocumentName.Text = DMS_DT.Rows[0]["DocumentName"].ToString();
                    if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "1")
                    {
                        lblComments.InnerHtml = "Purpose of Initiation";
                        txt_Comments.Text = DMS_DT.Rows[0]["CreatePurpose"].ToString();
                        txt_VersionID.Text = "00";
                    }
                    else if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "2")
                    {
                        lblComments.InnerHtml = "Reason for Revision";
                        txt_Comments.Text = DMS_DT.Rows[0]["CreatePurpose"].ToString();
                        txt_VersionID.Text = DMS_DT.Rows[0]["versionNumber"].ToString();
                    }
                    else if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "5")
                    {
                        lblComments.InnerHtml = "Reason for Renew";
                        txt_Comments.Text = DMS_DT.Rows[0]["WithdrawPurpose"].ToString();
                        txt_VersionID.Text = DMS_DT.Rows[0]["versionNumber"].ToString();
                        btn_Download.Visible = false;
                        if (DMS_DT.Rows[0]["CreatorID"].ToString() != "")
                        {
                            Btn_Revert.Visible = true;
                        }
                        else
                        {
                            Btn_Revert.Visible = false;
                        }                        
                        Btn_SaveRemarks.Visible = false;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "hide1", "hidecontrols();", true);
                    }
                    if (Convert.ToBoolean(DMS_DT.Rows[0]["isPublic"].ToString()) == true)
                    {
                        RadioButtonList1.SelectedValue = "yes";
                    }
                    else
                    {
                        RadioButtonList1.SelectedValue = "no";
                    }
                    RadioButtonList1.Enabled = false;
                    txt_Author.Text = DMS_DT.Rows[0]["CreatorID"].ToString();
                    if (txt_Author.Text == "")
                    {
                        divauthor.Visible = false;
                    }
                    else
                    {
                        divauthor.Visible = true;
                    }
                    txt_Authorizer.Text = DMS_DT.Rows[0]["AuthorizedBy"].ToString();
                    if (txt_Authorizer.Text == "")
                    {
                        divauthorizer.Visible = false;
                    }
                    else
                    {
                        divauthorizer.Visible = true;
                    }
                    txt_Controller.Text= DMS_DT.Rows[0]["ControlledBy"].ToString();
                    if (txt_Controller.Text == "")
                    {
                        divcontroller.Visible = false;
                    }
                    else
                    {
                        divcontroller.Visible = true;
                    }
                    System.Data.DataTable aprrove_list = DMS_Bal.DMS_GetApproverReviewerSelectedList(hdfPKID.Value, 5);
                    ViewState["ApproverData"] = aprrove_list;
                    gvApprover.DataSource = aprrove_list;
                    gvApprover.DataBind();
                    if (DMS_DT.Rows[0]["ReferralCount"].ToString() == "0" || DMS_DT.Rows[0]["RequestTypeID"].ToString() == "5")
                    {
                        liReferrals.Visible = false;
                    }
                    else
                    {
                        liReferrals.Visible = true;
                    }
                    hdfFromEmpID.Value = DMS_DT.Rows[0]["authorID"].ToString();
                    docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                    System.Data.DataTable dt = DMS_Bal.DMS_GetTempRevertedReviewersDetails(docObjects);
                    if (dt.Rows.Count > 0 && hdfFromEmpID.Value !="0")
                    {
                        divcomments.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("RRD_M10:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "RRD_M10:" + strline + "  " + "Document Details loading failed.", "error");
            }
        }
        private void RetrieveDocumentContent(int DocumentID, int ProcessID)
        {
            try
            {
                hdfPID.Value = ProcessID.ToString();
                hdfPKID.Value = DocumentID.ToString();
                hdfViewType.Value = "0";
                hdfViewEmpID.Value = "0";
                hdfViewRoleID.Value = "11";
                if (hdfPID.Value == "3")
                {
                    reviewObjects.Id = Convert.ToInt32(hdfPKID.Value);
                    reviewObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    reviewObjects.DocStatus = "R";
                    int DMS_update = DMS_BalDM.DMS_UpdateReviewstaus(reviewObjects);
                    if (DMS_update == 0)
                    {
                        btn_Download.Visible = false;
                        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    This Document is under Review  By Another Reviewer.Thank you.','info');", true);
                        HelpClass.custAlertMsgWithFunc(this.Page, this.Page.GetType(), "This Document is under Review  By Another Reviewer.Thank you.", "info", "CancelReset()");
                    }
                }
                System.Data.DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                reviewObjects.Id = Convert.ToInt32(hdfPKID.Value);
                reviewObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                reviewObjects.DocStatus = "R";
                System.Data.DataTable DMS_Commets = DMS_Bal.DMS_GetsavedCommentsByID(reviewObjects);
                txt_Reject.Value = DMS_Commets.Rows[0]["ReviewComments"].ToString();
                getTime();
                if (DMS_DT.Rows[0]["RequestTypeID"].ToString() != "5")
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Please Download Document to view Track Changes!", "info");
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop", "OpenReadSopModal();", true);
                }
                else if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "5" && DMS_DT.Rows[0]["authorID"].ToString() != "0")
                {
                    hdfViewType.Value = "5";
                    hdfViewEmpID.Value = "0";
                    hdfViewRoleID.Value = "11";
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view2", "ViewDocument('#divPDF_Viewer','2','');", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("RRD_M11:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "RRD_M11:" + strline + "  " + "Document Content loading failed.", "error");
            }
        }
        private void RetrieveRefferalContent(int DocumentID)
        {
            try
            {
                hdfPKID.Value = DocumentID.ToString();
                hdnGrdiReferesh.Value = "Link";
                System.Data.DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                string DMS_DocumentID = hdfPKID.Value;
                txtReferrences.Text = DMS_Bal.DMS_GetLinkReferences(Convert.ToInt32(DMS_DocumentID)).Replace("<a", "<a  target='_blank' ");
                System.Data.DataTable dtInternalDocs = DMS_Bal.DMS_GetReferalInternalDocs(Convert.ToInt32(DMS_DocumentID));
                Session["dtAssignedDocuments"] = dtInternalDocs;
                gvAssignedDocuments.DataSource = dtInternalDocs;
                gvAssignedDocuments.DataBind();
                System.Data.DataTable dtExternalDocs = DMS_Bal.DMS_GetReferalExternalFiles(Convert.ToInt32(DMS_DocumentID));
                gvExternalFiles.DataSource = dtExternalDocs;
                gvExternalFiles.DataBind();
                ViewState["dtExternalDocuments"] = dtExternalDocs;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("RRD_M12:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "RRD_M12:" + strline + "  " + "Referral Content loading failed.", "error");
            }
        }
        private void InitializeThePage()
        {
            try
            {
                if (Request.QueryString["DocumentID"] != null && Request.QueryString["PID"] != null)
                {
                    int DocumentID = Convert.ToInt32(Request.QueryString["DocumentID"]);
                    int ProcessID = Convert.ToInt32(Request.QueryString["PID"]);
                    RetriveDocumentDetails(DocumentID, ProcessID);
                    RetrieveDocumentContent(DocumentID, ProcessID);
                    RetrieveRefferalContent(DocumentID);
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("RRD_M13:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "RRD_M13:" + strline + "  " + "Page Initialization failed.", "error");
            }
        }
        protected void btn_Download_Click(object sender, EventArgs e)
        {
            btn_Download.Enabled = false;
            try
            {
                System.Data.DataTable dt = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                if (Btn_ShowOriginal.Visible == false && Btn_ShowModified.Visible == false)
                {
                    docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                    docObjects.RoleID = 11;//author
                    docObjects.EmpID = 0;
                    docObjects.FileType1 = dt.Rows[0]["FileType"].ToString();
                    docObjects.Mode = 0;
                }
                else if (Btn_ShowOriginal.Visible == true && Btn_ShowModified.Visible == false)
                {
                    docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                    docObjects.RoleID = 0;
                    docObjects.EmpID = 0;
                    docObjects.FileType1 = dt.Rows[0]["FileType"].ToString();
                    docObjects.Mode = 0;
                }
                else if (Btn_ShowOriginal.Visible == false && Btn_ShowModified.Visible == true)
                {
                    docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                    docObjects.RoleID = 11;//Author
                    docObjects.EmpID = 0;
                    docObjects.FileType1 = dt.Rows[0]["FileType"].ToString();
                    docObjects.Mode = 0;
                }

                System.Data.DataTable DMS_DT = DMS_Bal.DMS_GetDocumentLocationList(docObjects);
                string DocPath = Server.MapPath(@DMS_DT.Rows[0]["FilePath"].ToString());

                if (DMS_DT.Rows[0]["FileType"].ToString() == ".doc")
                {
                    Response.ContentType = "application/msword";
                }
                else if (DMS_DT.Rows[0]["FileType"].ToString() == ".docx")
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                }
                //WebClient webClient = new WebClient();
                //webClient.DownloadFile(DocPath, @"C:\Downloads\myfile.doc");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + DMS_DT.Rows[0]["FileName"].ToString());
                Response.TransmitFile(DocPath);
                DownloadRecord();
                Response.End();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("RRD_M14:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "RRD_M14:" + strline + "  " + "Download click failed.", "error");
            }
            finally
            {
                btn_Download.Enabled = true;
            }
        }
        public void DownloadRecord()
        {
            docObjects1.Version = Convert.ToInt32(hdfPKID.Value);
            docObjects1.RoleID = 10;//10-Reviewer
            docObjects1.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            docObjects1.Remarks = "N/A";
            docObjects1.Time = "N/A";
            docObjects1.action = 42;
            docObjects1.CommentType = 1;
            DMS_Bal.DMS_DocComments(docObjects1);
        }
        protected void Btn_Upload_Click(object sender, EventArgs e)
        {
            Btn_Upload.Enabled = false;
            try
            {
                ViewState["PDFPhysicalName"] = "";
                Session["vwdocObjectsR"] = null;
                Session["vwreviewObjectsR"] = null;
                Session["vwdocObjects1R"] = null;
                byte[] Filebyte = null;
                if (file_word.HasFile)
                {
                    System.Data.DataTable dt = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                    Stream fs = file_word.PostedFile.InputStream;
                    BinaryReader br = new BinaryReader(fs);
                    Filebyte = br.ReadBytes((Int32)fs.Length);
                    if (Filebyte.Length < 1000)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' Not able to read content,document upload failed','warning');", true);
                        return;
                    }
                    string ext = System.IO.Path.GetExtension(file_word.FileName).ToLower();
                    string mime = MimeType.GetMimeType(Filebyte, file_word.FileName);
                    string[] filemimes1 = new string[] { "application/pdf", "application/x-msdownload", "application/unknown", "application/octet-stream" };
                    string[] filemimes = new string[] { "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/x-zip-compressed" };
                    if (filemimes1.Contains(mime))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop3", "custAlertMsg(' Please upload .doc or .docx files only ! <br/> The uploaded file is not valid or corrupted.','warning');", true);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "hide2", "hideApproveDownload();", true);
                        return;
                    }
                    if (!filemimes.Contains(mime))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop3", "custAlertMsg(' Please upload .doc or .docx files only ! <br/> The uploaded file is not valid or corrupted.','warning');", true);
                        return;
                    }
                    if (ext == ".doc" || ext == ".docx")
                    {
                        string strWordMainFolderPath = DynamicFolder.CreateTempDocumentFolder(dt.Rows[0]["DocumentNumber"].ToString(), dt.Rows[0]["VersionNumber"].ToString());
                        string File1Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ext;
                        string strSourcePath = (strWordMainFolderPath + "\\" + File1Path);
                        string File2Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                        string strTargPath = (storefolder + File2Path);

                        //Save original file
                        file_word.SaveAs(strSourcePath);

                        //call the converter method
                        bool isValid = converter.convert(strSourcePath, strTargPath);
                        if (isValid)
                        {
                            docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                            docObjects.RoleID = 10;//reviewer
                            docObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            docObjects.FilePath1 = string.Format(@"~/DMSTempDocuments/{0}/{1}/", dt.Rows[0]["DocumentNumber"].ToString(), dt.Rows[0]["VersionNumber"].ToString()) + File1Path;
                            docObjects.FilePath2 = string.Format(@"~/Scripts/pdf.js/web/") + File2Path;
                            docObjects.isTemp = 0;
                            docObjects.FileType1 = ext;
                            docObjects.FileType2 = ".pdf";
                            docObjects.FileName1 = File1Path;
                            docObjects.FileName2 = File2Path;
                            ViewState["PDFPhysicalName"] = File2Path;
                            Session["vwdocObjectsR"] = docObjects;
                            //if (hdfPID.Value == "2" || hdfPID.Value == "3")
                            //{
                            //    reviewObjects.Id = Convert.ToInt32(hdfPKID.Value);
                            //    //reviewObjects.Content = Filebyte;
                            //    reviewObjects.Ext = ext;
                            //    reviewObjects.TemplateName = file_word.FileName;
                            //    Session["vwreviewObjectsR"] = reviewObjects;
                            //}

                            docObjects1.Version = Convert.ToInt32(hdfPKID.Value);
                            docObjects1.RoleID = 10;//10-Reviewer
                            docObjects1.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            docObjects1.Remarks = "N/A";
                            docObjects1.Time = "N/A";
                            docObjects1.action = 43;
                            docObjects1.CommentType = 1;
                            Session["vwdocObjects1R"] = docObjects1;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show1", "showApproveDownload();", true);
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view3", "ViewDocument('#divPDF_Viewer','1','" + File2Path + "');", true);
                            Btn_ShowOriginal.Visible = true;
                            Btn_ShowModified.Visible = false;
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "custAlertMsg(' <br/> Server error , contact administrator.','error');", true);
                            return;
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop3", "custAlertMsg(' <br/> Please upload  .doc or .docx files only.','warning');", true);
                        return;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop3", "custAlertMsg(' <br/> Please upload  .doc or .docx files only.','warning');", true);
                    return;
                }
                upUpload.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("RRD_M15:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "RRD_M15:" + strline + "  " + "Upload click failed.", "error");
            }
            finally
            {
                Btn_Upload.Enabled = true;              
            }
        }
        protected void Btn_ShowOriginal_Click(object sender, EventArgs e)
        {
            Btn_ShowOriginal.Enabled = false;
            try
            {
                hdfViewType.Value = "0";
                hdfViewEmpID.Value = "0";
                hdfViewRoleID.Value = "11";//author

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view4", "ViewDocument('#divPDF_Viewer',2,'');", true);
                Btn_ShowOriginal.Visible = false;
                Btn_ShowModified.Visible = true;

                docObjects1.Version = Convert.ToInt32(hdfPKID.Value);
                docObjects1.RoleID = 10;//10-Reviewer
                docObjects1.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                docObjects1.Remarks = "N/A";
                docObjects1.Time = "N/A";
                docObjects1.action = 0;
                docObjects1.CommentType = 2;
                System.Data.DataTable table = DMS_Bal.DMS_DocComments(docObjects1);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show2", "showApproveDownload();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("RRD_M16:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "RRD_M16:" + strline + "  " + "Show Original click failed.", "error");
            }
            finally
            {
                Btn_ShowOriginal.Enabled = true;
            }
        }
        protected void Btn_ShowModified_Click(object sender, EventArgs e)
        {
            Btn_ShowModified.Enabled = false;
            try
            {
                hdfViewType.Value = "0";
                hdfViewEmpID.Value = "0";
                hdfViewRoleID.Value = "0";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view5", "ViewDocument('#divPDF_Viewer','1','" + ViewState["PDFPhysicalName"].ToString() + "');", true);
                Btn_ShowOriginal.Visible = true;
                Btn_ShowModified.Visible = false;

                docObjects1.Version = Convert.ToInt32(hdfPKID.Value);
                docObjects1.RoleID = 10;//10-Reviewer
                docObjects1.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                docObjects1.Remarks = "N/A";
                docObjects1.Time = "N/A";
                docObjects1.action = 0;
                docObjects1.CommentType = 2;
                System.Data.DataTable table = DMS_Bal.DMS_DocComments(docObjects1);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show3", "showApproveDownload();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("RRD_M17:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "RRD_M17:" + strline + "  " + "Show Modified click failed.", "error");
            }
            finally
            {
                Btn_ShowModified.Enabled = true;
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}