﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AizantIT_PharmaApp.DMS.DocumentReviewer
{
    public partial class DocReqReviewerList : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        DocPrintRequest DocReqObjects = new DocPrintRequest();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=10").Length > 0)//10-Reviewer
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DRRL_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DRL_M1:" + strline + "  " + "Document Load failed", "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        protected void btnRequestReview_Click(object sender, EventArgs e)
        {
            DataTable DMS_DT = DMS_Bal.DMS_GetDocPrintReqDetails(hdfVID.Value);
            txt_RequestedBy.Text = DMS_DT.Rows[0]["RequestBy"].ToString();
            if (Convert.ToBoolean(DMS_DT.Rows[0]["isSoftCopy"].ToString()) == true)
            {
                RadioButtonList1.SelectedValue = "yes";
            }
            else
            {
                RadioButtonList1.SelectedValue = "no";
            }
            RadioButtonList1.Enabled = false;
            txt_PageNo.Text = DMS_DT.Rows[0]["PageNumbers"].ToString();
            txt_DocumentName.Text = DMS_DT.Rows[0]["DocumentName"].ToString();
            txt_Purpose.Text = DMS_DT.Rows[0]["PrintPurpose"].ToString();
            txt_ReviewedBy.Text = DMS_DT.Rows[0]["ReviewedBy"].ToString();
            txt_DocController.Text = DMS_DT.Rows[0]["DocController"].ToString();
            UpGridButtons.Update();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_request').modal({ show: 'true', backdrop: 'static', keyboard: false });", true);
        }

        protected void btnCommentsHistoryView_Click(object sender, EventArgs e)
        {
            DataTable DMS_DT = DMS_Bal.DMS_GetDocDistributionHistory(hdfVID.Value);
            gv_CommentHistory.DataSource = DMS_DT;
            gv_CommentHistory.DataBind();
            UpGridButtons.Update();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myCommentHistory').modal({ show: 'true', backdrop: 'static', keyboard: false });", true);
        }

        protected void btn_Approve_Click(object sender, EventArgs e)
        {
            DocReqObjects.DocReqID = Convert.ToInt32(hdfVID.Value);
            DocReqObjects.ReviewedByID = Convert.ToInt32(hdnEmpID.Value);
            DocReqObjects.Comments = txt_Comments.Text.Trim();
            int DMS_AP = DMS_BalDM.DMS_DocReqApprovedByReviewer(DocReqObjects);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "reloadtable();", true);
            HelpClass.custAlertMsg(this, this.GetType(), "Document Request ID " + hdfVID.Value + " Approved by Reviewer Successfully", "success");
        }

        protected void btn_Reject_Click(object sender, EventArgs e)
        {
            DocReqObjects.DocReqID = Convert.ToInt32(hdfVID.Value);
            DocReqObjects.ReviewedByID = Convert.ToInt32(hdnEmpID.Value);
            DocReqObjects.Comments = txt_Comments.Text.Trim();
            int DMS_AP = DMS_BalDM.DMS_DocReqRejectedByReviewer(DocReqObjects);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "reloadtable();", true);
            HelpClass.custAlertMsg(this, this.GetType(), "Document Request ID " + hdfVID.Value + " Rejected by Reviewer Successfully", "success");
        }
        private void InitializeThePage()
        {
            //try
            //{
            //    if (Request.QueryString.Count > 0)
            //    {
            //        div_Comments.Visible = true;
            //        buttons.Visible = true;
            //    }
            //    else
            //    {
            //        div_Comments.Visible = false;
            //        buttons.Visible = false;
            //    }
            //}
            //catch(Exception ex)
            //{

            //}
        }
    }
}