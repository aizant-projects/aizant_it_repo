﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using DevExpress.Web.Bootstrap;
using System;
using System.Data;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AizantIT_PharmaApp.DMS
{
    public partial class DMSHomePage : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_bal = new DocumentCreationBAL();
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        ReviewObjects reviewObjects = new ReviewObjects();
        DocObjects docObjects = new DocObjects();
        int countE = 0;
        int countRD = 0;
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        ViewState["emp_id_DMS"] = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                        ViewState["ReaderPage"] = "false";
                        ViewState["EffDepartment"] = 0;
                        ViewState["EffDocType"] = 0;
                        ViewState["RecRevisedFDate"] = DateTime.Today.AddDays(-30).ToString("dd MMM yyyy");
                        ViewState["RecRevisedTDate"] = DateTime.Today.ToString("dd MMM yyyy");
                        ViewState["ATEDepartment"] = 0;
                        ViewState["ATEDocType"] = 0;
                        ViewState["ATEFromDate"] = DateTime.Today.ToString("dd MMM yyyy");
                        ViewState["ATEToDate"] = DateTime.Today.AddDays(+30).ToString("dd MMM yyyy");
                        ViewState["DocStatusDepartment"] = 0;
                        ViewState["DocStatusDocType"] = 0;
                        hdfReader.Value = "0";
                        bool IsReaderRole = false;
                        EffectiveChart.Visible = false;
                        RecentlyRevisedChart.Visible = false;
                        AbouttoExpireChart.Visible = false;
                        DocumentStatusChart.Visible = false;
                        bool IsDashboardVisible = false;
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        if (drUMS.Length > 0)
                        {
                            int _NoRole = 0;
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=23").Length > 0)//reader role
                            {
                                EffectiveChart.Visible = true;
                                IsReaderRole = true;
                                ReadyToPrint();
                                _NoRole++;
                                ViewState["ReaderPage"] = "true";
                                hdfReader.Value = "1";
                                IsDashboardVisible = true;
                            }
                            if (dtTemp.Select("RoleID=30").Length > 0)//controller role
                            {
                                QAApprover();
                                EffeectiveDoucment(30);
                                EffectiveChart.Visible = true;
                                RecentlyRevisedChart.Visible = true;
                                AbouttoExpireChart.Visible = true;
                                DocumentStatusChart.Visible = true;
                                ReadyToPrint();
                                _NoRole++;
                                if (IsReaderRole)
                                {
                                    ViewState["ReaderPage"] = "true";
                                    hdfReader.Value = "1";
                                }
                                IsDashboardVisible = true;
                            }
                            if (dtTemp.Select("RoleID=32").Length > 0)//print reviewer role
                            {
                                QAApprover();
                                _NoRole++;
                                if (IsReaderRole)
                                {
                                    ViewState["ReaderPage"] = "true";
                                    hdfReader.Value = "1";
                                }
                            }
                            if (dtTemp.Select("RoleID=11").Length > 0)//author role
                            {
                                QAApprover();
                                InitiateReverted();
                                EffectiveChart.Visible = true;
                                RecentlyRevisedChart.Visible = true;
                                _NoRole++;
                                ViewState["ReaderPage"] = "false";
                                hdfReader.Value = "0";
                                IsDashboardVisible = true;
                            }
                            if (dtTemp.Select("RoleID=9").Length > 0)//approver role
                            {
                                //EffeectiveDoucment(9);
                                QAApprover();
                                EffectiveChart.Visible = true;
                                RecentlyRevisedChart.Visible = true;
                                AbouttoExpireChart.Visible = true;
                                DocumentStatusChart.Visible = true;
                                _NoRole++;
                                ViewState["ReaderPage"] = "false";
                                hdfReader.Value = "0";
                                IsDashboardVisible = true;
                            }
                            if (dtTemp.Select("RoleID=10").Length > 0)//reviewer role
                            {
                                QAApprover();
                                EffectiveChart.Visible = true;
                                RecentlyRevisedChart.Visible = true;
                                AbouttoExpireChart.Visible = true;
                                DocumentStatusChart.Visible = true;
                                _NoRole++;
                                ViewState["ReaderPage"] = "false";
                                hdfReader.Value = "0";
                                IsDashboardVisible = true;
                            }
                            if (dtTemp.Select("RoleID=12").Length > 0)//initiator role
                            {
                                InitiateReverted();
                                EffectiveChart.Visible = true;
                                RecentlyRevisedChart.Visible = true;
                                AbouttoExpireChart.Visible = true;
                                DocumentStatusChart.Visible = true;
                                _NoRole++;
                                ViewState["ReaderPage"] = "false";
                                hdfReader.Value = "0";
                                IsDashboardVisible = true;
                            }
                            if (dtTemp.Select("RoleID=13").Length > 0)//authorizer role
                            {
                                QAApprover();
                                EffectiveChart.Visible = true;
                                RecentlyRevisedChart.Visible = true;
                                AbouttoExpireChart.Visible = true;
                                DocumentStatusChart.Visible = true;
                                _NoRole++;
                                ViewState["ReaderPage"] = "false";
                                hdfReader.Value = "0";
                                IsDashboardVisible = true;
                            }
                            if (dtTemp.Select("RoleID=2").Length > 0)//admin role
                            {
                                EffectiveChart.Visible = true;
                                RecentlyRevisedChart.Visible = true;
                                AbouttoExpireChart.Visible = true;
                                DocumentStatusChart.Visible = true;
                                InitiateReverted();
                                _NoRole++;
                                ViewState["ReaderPage"] = "false";
                                hdfReader.Value = "0";
                                IsDashboardVisible = true;
                            }

                            if (_NoRole == 0)
                            {
                                loadAuthorizeErrorMsg();
                            }
                            if (!IsDashboardVisible)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "show1", "hideDashboard();", true);
                            }
                            DMS_GetDepartmentList();
                            DMS_GetDocumentTypeList();
                            ChartReload();
                            Session["ChartStatus"] = 0;
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DHP_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DHP_M1:" + strline + "  " + "Document Load failed.", "error");
            }
        }
        public void ChartReload()
        {
            ChartFill(0);
            ChartFill3();
            ChartFill4(0);
            ChartFill6(0);
            ChartFill8(0);
            ChartFill10(0);
            ChartFill14(0);
        }
        public void QAApprover()
        {
            try
            {
                int EmpID = Convert.ToInt32(ViewState["emp_id_DMS"]);
                string StatusQ = "QA";
                int countQ = DMS_bal.RevertedInitator(EmpID, StatusQ);
                int RequestTypeIDQ = 1;
                int countQA;
                countQA = DMS_bal.StatusNoficationByRequestType(EmpID, StatusQ, RequestTypeIDQ);
                if (countQA >= 1)
                {
                    lblApproverQANew.Text = countQA.ToString();
                    divNewApproverQA.Visible = true;
                }
                RequestTypeIDQ = 2;
                countQA = DMS_bal.StatusNoficationByRequestType(EmpID, StatusQ, RequestTypeIDQ);
                if (countQA >= 1)
                {
                    lblApproverQARevision.Text = countQA.ToString();
                    divRevisionApproverQA.Visible = true;
                }
                RequestTypeIDQ = 3;
                countQA = DMS_bal.StatusNoficationByRequestType(EmpID, StatusQ, RequestTypeIDQ);
                if (countQA >= 1)
                {
                    lblApproverQAWithdraw.Text = countQA.ToString();
                    divWithdrawApproverQA.Visible = true;
                }
                RequestTypeIDQ = 5;
                countQA = DMS_bal.StatusNoficationByRequestType(EmpID, StatusQ, RequestTypeIDQ);
                if (countQA >= 1)
                {
                    lblApproverQARenew.Text = countQA.ToString();
                    divRenewApproverQA.Visible = true;
                }
                string StatusW = "W_Ap";
                int countW = DMS_bal.RevertedInitator(EmpID, StatusW);
                if (countW >= 1)
                {
                    lblControllerWithdraw.Text = countW.ToString();
                    divWithdrawController.Visible = true;
                }
                string StatusC = "C";
                int countC = DMS_bal.RevertedInitator(EmpID, StatusC);
                int RequestTypeIDC = 1;
                int countCA;
                countCA = DMS_bal.StatusNoficationByRequestType(EmpID, StatusC, RequestTypeIDC);
                if (countCA >= 1)
                {
                    lblApproverCreatorNew.Text = countCA.ToString();
                    divNewApproverCreator.Visible = true;
                }
                RequestTypeIDC = 2;
                countCA = DMS_bal.StatusNoficationByRequestType(EmpID, StatusC, RequestTypeIDC);
                if (countCA >= 1)
                {
                    lblApproverCreatorRevision.Text = countCA.ToString();
                    divRevisionApproverCreator.Visible = true;
                }
                RequestTypeIDC = 5;
                countCA = DMS_bal.StatusNoficationByRequestType(EmpID, StatusC, RequestTypeIDC);
                if (countCA >= 1)
                {
                    lblApproverCreatorRenew.Text = countCA.ToString();
                    divRenewApproverCreator.Visible = true;
                }

                string StatusR = "R_Ap";
                int countR = DMS_bal.RevertedInitator(EmpID, StatusR);
                int RequestTypeIDR = 1;
                int countRA;
                countRA = DMS_bal.StatusNoficationByRequestType(EmpID, StatusR, RequestTypeIDR);
                if (countRA >= 1)
                {
                    lblReviewerNew.Text = countRA.ToString();
                    divNewReviewer.Visible = true;
                }
                RequestTypeIDR = 2;
                countRA = DMS_bal.StatusNoficationByRequestType(EmpID, StatusR, RequestTypeIDR);
                if (countRA >= 1)
                {
                    lblReviewerRevision.Text = countRA.ToString();
                    divRevisionReviewer.Visible = true;
                }
                RequestTypeIDR = 5;
                countRA = DMS_bal.StatusNoficationByRequestType(EmpID, StatusR, RequestTypeIDR);
                if (countRA >= 1)
                {
                    lblReviewerRenew.Text = countRA.ToString();
                    divRenewReviewer.Visible = true;
                }
                string StatusA = "Ap_D";
                int countA = DMS_bal.RevertedInitator(EmpID, StatusA);
                int RequestTypeIDA = 1;
                int countAA;
                countAA = DMS_bal.StatusNoficationByRequestType(EmpID, StatusA, RequestTypeIDA);
                if (countAA >= 1)
                {
                    lblApproverDocNew.Text = countAA.ToString();
                    divNewApproverDoc.Visible = true;
                }
                RequestTypeIDA = 2;
                countAA = DMS_bal.StatusNoficationByRequestType(EmpID, StatusA, RequestTypeIDA);
                if (countAA >= 1)
                {
                    lblApproverDocRevision.Text = countAA.ToString();
                    divRevisionApproverDoc.Visible = true;
                }
                RequestTypeIDA = 5;
                countAA = DMS_bal.StatusNoficationByRequestType(EmpID, StatusA, RequestTypeIDA);
                if (countAA >= 1)
                {
                    lblApproverDocRenew.Text = countAA.ToString();
                    divRenewApproverDoc.Visible = true;
                }
                string StatusAU = "Au";
                int countAU = DMS_bal.RevertedInitator(EmpID, StatusAU);
                int RequestTypeIDAU = 1;
                int countAUA;
                countAUA = DMS_bal.StatusNoficationByRequestType(EmpID, StatusAU, RequestTypeIDAU);
                if (countAUA >= 1)
                {
                    lblAuthorizeNew.Text = countAUA.ToString();
                    divNewAuthorize.Visible = true;
                }
                RequestTypeIDAU = 2;
                countAUA = DMS_bal.StatusNoficationByRequestType(EmpID, StatusAU, RequestTypeIDAU);
                if (countAUA >= 1)
                {
                    lblAuthorizeRevision.Text = countAUA.ToString();
                    divRevisionAuthorize.Visible = true;
                }

                docObjects.EmpID = EmpID;
                docObjects.Status = 1;
                DataSet ds = DMS_bal.DMS_GetPrintDistributionNotificationCards(docObjects);
                int countCO = Convert.ToInt32(ds.Tables[5].Rows[0][0]);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) > 0)
                    {
                        lblControllerApproveFormCreation.Text = ds.Tables[0].Rows[0][0].ToString();
                        divControllerApproveFormCreation.Visible = true;
                    }
                    else
                    {
                        divControllerApproveFormCreation.Visible = false;
                    }
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    if (Convert.ToInt32(ds.Tables[1].Rows[0][0].ToString()) > 0)
                    {
                        lblControllerApproveFormRequest.Text = ds.Tables[1].Rows[0][0].ToString();
                        divControllerApproveFormRequest.Visible = true;
                    }
                    else
                    {
                        divControllerApproveFormRequest.Visible = false;
                    }
                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    if (Convert.ToInt32(ds.Tables[2].Rows[0][0].ToString()) > 0)
                    {
                        lblControllerApproveDocumentRequest.Text = ds.Tables[2].Rows[0][0].ToString();
                        divControllerApproveDocumentRequest.Visible = true;
                    }
                    else
                    {
                        divControllerApproveDocumentRequest.Visible = false;
                    }
                }
                if (ds.Tables[3].Rows.Count > 0)
                {
                    if (Convert.ToInt32(ds.Tables[3].Rows[0][0].ToString()) > 0)
                    {
                        lblControllerApproveRevisionFormCreation.Text = ds.Tables[3].Rows[0][0].ToString();
                        divControllerApproveRevisionFormCreation.Visible = true;
                    }
                    else
                    {
                        divControllerApproveRevisionFormCreation.Visible = false;
                    }
                }
                if (ds.Tables[4].Rows.Count > 0)
                {
                    if (Convert.ToInt32(ds.Tables[4].Rows[0][0].ToString()) > 0)
                    {
                        lblControllerApproveWithdrawFormCreation.Text = ds.Tables[4].Rows[0][0].ToString();
                        divControllerApproveWithdrawFormCreation.Visible = true;
                    }
                    else
                    {
                        divControllerApproveWithdrawFormCreation.Visible = false;
                    }
                }
                docObjects.Status = 2;
                DataSet ds1 = DMS_bal.DMS_GetPrintDistributionNotificationCards(docObjects);
                int countPR = Convert.ToInt32(ds1.Tables[2].Rows[0][0]);
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToInt32(ds1.Tables[0].Rows[0][0].ToString()) > 0)
                    {
                        lblPReviewerApproveFormRequest.Text = ds1.Tables[0].Rows[0][0].ToString();
                        divPReviewerApproveFormRequest.Visible = true;
                    }
                    else
                    {
                        divPReviewerApproveFormRequest.Visible = false;
                    }
                }
                if (ds1.Tables[1].Rows.Count > 0)
                {
                    if (Convert.ToInt32(ds1.Tables[1].Rows[0][0].ToString()) > 0)
                    {
                        lblPReviewerApproveDocumentRequest.Text = ds1.Tables[1].Rows[0][0].ToString();
                        divPReviewerApproveDocumentRequest.Visible = true;
                    }
                    else
                    {
                        divPReviewerApproveDocumentRequest.Visible = false;
                    }
                }

                docObjects.Status = 5;
                DataSet ds2 = DMS_bal.DMS_GetPrintDistributionNotificationCards(docObjects);
                int countCV = Convert.ToInt32(ds2.Tables[0].Rows[0][0]);
                if (Convert.ToInt32(ds2.Tables[0].Rows[0][0].ToString()) > 0)
                {
                    lblReturnVerifyForms.Text = ds2.Tables[0].Rows[0][0].ToString();
                    divReturnVerifyController.Visible = true;
                }
                if (countQ >= 1 || countW >= 1 || countC >= 1 || countR >= 1 || countA >= 1 || countAU >= 1 || countCO >= 1 || countPR >= 1 || countCV >= 1)
                {
                    lblApproverQA.Text = (countQ + countW + countC + countR + countA + countAU + countCO + countPR + countCV).ToString();
                    divApproverQA.Visible = true;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DHP_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DHP_M3:" + strline + "  " + "Request Approver Notification failed.", "error");
            }
        }
        public void InitiateReverted()
        {
            try
            {
                int EmpID = Convert.ToInt32(ViewState["emp_id_DMS"]);
                string StatusI = "I";
                int countI = DMS_bal.RevertedInitator(EmpID, StatusI);
                int RequestTypeIDI = 1;
                int countIR;
                countIR = DMS_bal.StatusNoficationByRequestType(EmpID, StatusI, RequestTypeIDI);
                if (countIR >= 1)
                {
                    lblRevertedinitiationNew.Text = countIR.ToString();
                    divNewRevertedInitiation.Visible = true;
                }
                RequestTypeIDI = 2;
                countIR = DMS_bal.StatusNoficationByRequestType(EmpID, StatusI, RequestTypeIDI);
                if (countIR >= 1)
                {
                    lblRevertedinitiationRevision.Text = countIR.ToString();
                    divRevisionRevertedInitiation.Visible = true;
                }
                RequestTypeIDI = 3;
                countIR = DMS_bal.StatusNoficationByRequestType(EmpID, StatusI, RequestTypeIDI);
                if (countIR >= 1)
                {
                    lblRevertedinitiationWithdaw.Text = countIR.ToString();
                    divWithdrawRevertedInitiation.Visible = true;
                }
                RequestTypeIDI = 5;
                countIR = DMS_bal.StatusNoficationByRequestType(EmpID, StatusI, RequestTypeIDI);
                if (countIR >= 1)
                {
                    lblRevertedinitiationRenew.Text = countIR.ToString();
                    divRenewRevertedInitiation.Visible = true;
                }
                string StatusC = "C_R";
                int countC = DMS_bal.RevertedInitator(EmpID, StatusC);
                int RequestTypeIDC = 1;
                int countCR;
                countCR = DMS_bal.StatusNoficationByRequestType(EmpID, StatusC, RequestTypeIDC);
                if (countCR >= 1)
                {
                    lblCreatorRevertNew.Text = countCR.ToString();
                    divNewCreatorRevert.Visible = true;
                }
                RequestTypeIDC = 2;
                countCR = DMS_bal.StatusNoficationByRequestType(EmpID, StatusC, RequestTypeIDC);
                if (countCR >= 1)
                {
                    lblCreatorRevertRevision.Text = countCR.ToString();
                    divRevisionCreatorRevert.Visible = true;
                }
                RequestTypeIDC = 5;
                countCR = DMS_bal.StatusNoficationByRequestType(EmpID, StatusC, RequestTypeIDC);
                if (countCR >= 1)
                {
                    lblCreatorRevertRenew.Text = countCR.ToString();
                    divRenewCreatorRevert.Visible = true;
                }

                docObjects.EmpID = EmpID;
                docObjects.Status = 3;
                DataSet ds = DMS_bal.DMS_GetPrintDistributionNotificationCards(docObjects);
                int countF = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
                if (ds.Tables[1].Rows.Count > 0)
                {
                    if (Convert.ToInt32(ds.Tables[1].Rows[0][0].ToString()) > 0)
                    {
                        lblAdminRevertForms.Text = ds.Tables[1].Rows[0][0].ToString();
                        divAdminRevertForms.Visible = true;
                    }
                    else
                    {
                        divAdminRevertForms.Visible = false;
                    }
                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    if (Convert.ToInt32(ds.Tables[2].Rows[0][0].ToString()) > 0)
                    {
                        lblAdminRevertRevisionForms.Text = ds.Tables[2].Rows[0][0].ToString();
                        divAdminRevertRevisionForms.Visible = true;
                    }
                }
                if (ds.Tables[3].Rows.Count > 0)
                {
                    if (Convert.ToInt32(ds.Tables[3].Rows[0][0].ToString()) > 0)
                    {
                        lblAdminRevertWithdrawForms.Text = ds.Tables[3].Rows[0][0].ToString();
                        divAdminRevertWithdrawForms.Visible = true;
                    }
                }
                if (countI >= 1 || countC >= 1 || countF >= 1)
                {
                    lblRevertedinitiation.Text = (countI + countC + countF).ToString();
                    divRevertedInitiation.Visible = true;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DHP_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DHP_M2:" + strline + "  " + "Initiator Reverted Notification failed.", "error");
            }
        }
        public void EffeectiveDoucment(int RoleID)
        {
            try
            {
                DocumentCreationBAL DMS_BalDC = new DocumentCreationBAL();
                JQDataTableBO objJQDataTableBO = new JQDataTableBO();
                objJQDataTableBO.iMode = 2;
                objJQDataTableBO.pkid = 0;
                objJQDataTableBO.iDisplayLength = 0;
                objJQDataTableBO.iDisplayStart = 0;
                objJQDataTableBO.iSortCol = 0;
                objJQDataTableBO.sSortDir = "";
                objJQDataTableBO.sSearch = "";
                objJQDataTableBO.EmpID = Convert.ToInt32(ViewState["emp_id_DMS"]);
                
                if (RoleID == 30)
                {
                    DataTable dt = DMS_BalDC.DMS_Listofeffectivedocuments(objJQDataTableBO);
                    countE = Convert.ToInt32(dt.Rows[0][0]);

                    string Status = "Tbe_D";
                    int RequestTypeID = 1;
                    DataTable DMS_DT= DMS_bal.StatusNoficationByRequestType_Dt(objJQDataTableBO.EmpID, Status, RequestTypeID);
                    if (Convert.ToInt32(DMS_DT.Rows[0][0].ToString()) >= 1)
                    {
                        lblEffectiveNew.Text = DMS_DT.Rows[0][0].ToString();
                        divNewEfective.Visible = true;
                    }
                    RequestTypeID = 2;
                    DMS_DT = DMS_bal.StatusNoficationByRequestType_Dt(objJQDataTableBO.EmpID, Status, RequestTypeID);
                    if (Convert.ToInt32(DMS_DT.Rows[0][0].ToString()) >= 1)
                    {
                        lblEffectiveRevision.Text = DMS_DT.Rows[0][0].ToString();
                        divRevisionEfective.Visible = true;
                    }
              
                    //DataTable dt1 = DMS_bal.DMS_ExtendDocument(objJQDataTableBO);
                     //countRD = Convert.ToInt32(dt1.Rows[0][0]);
                    int RequestTypeIDR = 5;
                    //int countR;
                    string StatusR = "Re_D";

                    DMS_DT = DMS_bal.StatusNoficationByRequestType_Dt(objJQDataTableBO.EmpID, StatusR, RequestTypeIDR);
                    if (DMS_DT.Rows.Count>0)
                    {
                        if (DMS_DT.Select("RequestTypeID=5").Length>0)
                        {
                            int RenewNewCount =Convert.ToInt32(DMS_DT.Select("RequestTypeID=5")[0]["RenewRequestCount"]);
                            countRD = countRD + RenewNewCount;
                            lblRenewDocumentNew.Text = RenewNewCount.ToString();
                            divNewRenewDocument.Visible = true;
                        }
                        //if (DMS_DT.Select("RequestTypeID=2").Length > 0)
                        //{
                        //    int RenewRevisionCount = Convert.ToInt32(DMS_DT.Select("RequestTypeID=2")[0]["RenewRequestCount"]);
                        //    countRD = countRD + RenewRevisionCount;
                        //    lblRenewDocumentRevision.Text = RenewRevisionCount.ToString();
                        //    divRevisionRenewDocument.Visible = true;
                        //}
                        //if (DMS_DT.Select("RequestTypeID=4").Length > 0)
                        //{
                        //    int RenewExistingCount = Convert.ToInt32(DMS_DT.Select("RequestTypeID=4")[0]["RenewRequestCount"]);
                        //    countRD = countRD + RenewExistingCount;
                        //    lblRenewDocumentExisting.Text = RenewExistingCount.ToString();
                        //    divExistingRenewDocument.Visible = true;
                        //}
                    }
                    //if (DMS_DT.Rows[1]["RequestType"].ToString()=="New Document" && Convert.ToInt32(DMS_DT.Rows[1]["RenewRequestCount"].ToString()) >= 1)
                    //{
                    //    lblRenewDocumentNew.Text = DMS_DT.Rows[1]["RenewRequestCount"].ToString();
                    //    divNewRenewDocument.Visible = true;
                    //}

                    ////RequestTypeIDR = 2;
                    ////DMS_DT = DMS_bal.StatusNoficationByRequestType(objJQDataTableBO.EmpID, StatusR, RequestTypeIDR);
                    //if (DMS_DT.Rows[2]["RequestType"].ToString() == "Revision of Existing Document" && Convert.ToInt32(DMS_DT.Rows[2]["RenewRequestCount"].ToString()) >= 1)
                    //{
                    //    lblRenewDocumentRevision.Text = DMS_DT.Rows[2]["RenewRequestCount"].ToString().ToString();
                    //    divRevisionRenewDocument.Visible = true;
                    //}
                    ////RequestTypeIDR = 4;
                    ////DMS_DT = DMS_bal.StatusNoficationByRequestType(objJQDataTableBO.EmpID, StatusR, RequestTypeIDR);
                    //if (DMS_DT.Rows[0]["RequestType"].ToString() == "Existing Document" && Convert.ToInt32(DMS_DT.Rows[0]["RenewRequestCount"].ToString()) >= 1)
                    //{
                    //    lblRenewDocumentExisting.Text = countR.ToString();
                    //    divExistingRenewDocument.Visible = true;
                    //}
                }
                if (countE >= 1 || countRD >= 1)
                {
                    lblEffective.Text = (countE + countRD).ToString();
                    dvEfective.Visible = true;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DHP_M9:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DHP_M9:" + strline + "  " + "Effective Document Notification failed.", "error");
            }
        }
        public void ReadyToPrint()
        {
            try
            {
                docObjects.EmpID = Convert.ToInt32(ViewState["emp_id_DMS"]);
                docObjects.Status = 4;
                DataSet ds = DMS_bal.DMS_GetPrintDistributionNotificationCards(docObjects);
                if (ds.Tables[3].Rows.Count > 0)
                {
                    if (Convert.ToInt32(ds.Tables[3].Rows[0][0].ToString()) > 0)
                    {
                        lblReadyToPrint.Text = ds.Tables[3].Rows[0][0].ToString();
                        divPrint.Visible = true;
                    }
                    else
                    {
                        divPrint.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) > 0)
                    {
                        lblReadyToPrintForms.Text = ds.Tables[0].Rows[0][0].ToString();
                        divPrintForms.Visible = true;
                    }
                    else
                    {
                        divPrintForms.Visible = false;
                    }
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    if (Convert.ToInt32(ds.Tables[1].Rows[0][0].ToString()) > 0)
                    {
                        lblReadyToPrintDocuments.Text = ds.Tables[1].Rows[0][0].ToString();
                        divPrintDocuments.Visible = true;
                    }
                    else
                    {
                        divPrintDocuments.Visible = false;
                    }
                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    if (Convert.ToInt32(ds.Tables[2].Rows[0][0].ToString()) > 0)
                    {
                        lblReturnForms.Text = ds.Tables[2].Rows[0][0].ToString();
                        divReturnForms.Visible = true;
                    }
                    else
                    {
                        divReturnForms.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DHP_M40:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DHP_M40:" + strline + "  " + "Ready To Print Notification failed.", "error");
            }
        }
        public void DMS_GetDepartmentList()
        {
            try
            {
                DataTable DMS_Dt = DMS_BalDM.DMS_GetDepartmentList();
                ddl_DeptName1.DataValueField = DMS_Dt.Columns["DeptID"].ToString();
                ddl_DeptName1.DataTextField = DMS_Dt.Columns["DepartmentName"].ToString();
                ddl_DeptName1.DataSource = DMS_Dt;
                ddl_DeptName1.DataBind();
                ddl_DeptName1.Items.Insert(0, new ListItem("All Departments", "0"));

                ddl_DeptName2.DataValueField = DMS_Dt.Columns["DeptID"].ToString();
                ddl_DeptName2.DataTextField = DMS_Dt.Columns["DepartmentName"].ToString();
                ddl_DeptName2.DataSource = DMS_Dt;
                ddl_DeptName2.DataBind();
                ddl_DeptName2.Items.Insert(0, new ListItem("All Departments", "0"));

                ddl_DeptName3.DataValueField = DMS_Dt.Columns["DeptID"].ToString();
                ddl_DeptName3.DataTextField = DMS_Dt.Columns["DepartmentName"].ToString();
                ddl_DeptName3.DataSource = DMS_Dt;
                ddl_DeptName3.DataBind();
                ddl_DeptName3.Items.Insert(0, new ListItem("All Departments", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DHP_M11:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DHP_M11:" + strline + "  " + "Department Drop-down loading failed.", "error");
            }
        }
        public void DMS_GetDocumentTypeList()
        {
            try
            {
                JQDataTableBO objJQDataTableBO = new JQDataTableBO();
                objJQDataTableBO.iMode = 0;
                objJQDataTableBO.pkid = 0;
                objJQDataTableBO.iDisplayLength = 0;
                objJQDataTableBO.iDisplayStart = 0;
                objJQDataTableBO.iSortCol = 0;
                objJQDataTableBO.sSortDir = "";
                objJQDataTableBO.sSearch = "";
                DataTable DMS_Dt = DMS_bal.DMS_GetDocumentTypeList(objJQDataTableBO);

                ddl_DocType1.DataValueField = DMS_Dt.Columns["DocumentTypeID"].ToString();
                ddl_DocType1.DataTextField = DMS_Dt.Columns["DocumentType"].ToString();
                ddl_DocType1.DataSource = DMS_Dt;
                ddl_DocType1.DataBind();
                ddl_DocType1.Items.Insert(0, new ListItem("All Document Types", "0"));

                ddl_DocType2.DataValueField = DMS_Dt.Columns["DocumentTypeID"].ToString();
                ddl_DocType2.DataTextField = DMS_Dt.Columns["DocumentType"].ToString();
                ddl_DocType2.DataSource = DMS_Dt;
                ddl_DocType2.DataBind();
                ddl_DocType2.Items.Insert(0, new ListItem("All Document Types", "0"));

                ddl_DocType3.DataValueField = DMS_Dt.Columns["DocumentTypeID"].ToString();
                ddl_DocType3.DataTextField = DMS_Dt.Columns["DocumentType"].ToString();
                ddl_DocType3.DataSource = DMS_Dt;
                ddl_DocType3.DataBind();
                ddl_DocType3.Items.Insert(0, new ListItem("All Document Types", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DHP_M12:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DHP_M12:" + strline + "  " + "DocumentType Drop-down loading failed.", "error");
            }
        }
        public void ChartFill(int DefaultValue = 0)
        {
            try
            {
                DataTable dt = new DataTable();
                if (DefaultValue == 0)
                {
                    if (ViewState["ReaderPage"].ToString() == "true")
                    {
                        dt = DMS_bal.DMS_HomeDocumentListChart(0, 0, 2, Convert.ToInt32(ViewState["emp_id_DMS"]));
                        ViewState["EffDepartment"] = 0;
                        ViewState["EffDocType"] = 0;
                    }
                    else
                    {
                        dt = DMS_bal.DMS_HomeDocumentListChart(0, 0, 0, 0);
                        ViewState["EffDepartment"] = 0;
                        ViewState["EffDocType"] = 0;
                    }
                }
                else if (DefaultValue == 1)
                {
                    if (ViewState["ReaderPage"].ToString() == "true")
                    {
                        dt = DMS_bal.DMS_HomeDocumentListChart(Convert.ToInt32(ddl_DeptName1.SelectedValue), Convert.ToInt32(ddl_DocType1.SelectedValue), 2, Convert.ToInt32(ViewState["emp_id_DMS"]));
                        ViewState["EffDepartment"] = ddl_DeptName1.SelectedValue;
                        ViewState["EffDocType"] = ddl_DocType1.SelectedValue;

                    }
                    else
                    {
                        dt = DMS_bal.DMS_HomeDocumentListChart(Convert.ToInt32(ddl_DeptName1.SelectedValue), Convert.ToInt32(ddl_DocType1.SelectedValue), 0, 0);
                        ViewState["EffDepartment"] = ddl_DeptName1.SelectedValue;
                        ViewState["EffDocType"] = ddl_DocType1.SelectedValue;
                    }
                }
                else if (DefaultValue == 2)
                {
                    if (ViewState["ReaderPage"].ToString() == "true")
                    {
                        dt = DMS_bal.DMS_HomeDocumentListChart(Convert.ToInt32(ViewState["EffDepartment"].ToString()), Convert.ToInt32(ViewState["EffDocType"].ToString()), 2, Convert.ToInt32(ViewState["emp_id_DMS"]));
                    }
                    else
                    {
                        dt = DMS_bal.DMS_HomeDocumentListChart(Convert.ToInt32(ViewState["EffDepartment"].ToString()), Convert.ToInt32(ViewState["EffDocType"].ToString()), 0, 0);
                    }
                }

                BPCEffective.SeriesCollection.Clear();
                BPCEffective.DataSource = dt;
                BPCEffective.SettingsCommonSeries.ArgumentField = "DocumentType";
                BPCEffective.SeriesCollection.Add(new BootstrapPieChartSeries() { ValueField = "NoofDocuments", Name = "NoofDocuments" });
                BPCEffective.SettingsCommonSeries.Label.Visible = true;
                BPCEffective.SettingsToolTip.Enabled = true;
                BPCEffective.SettingsToolTip.Format.Type = FormatType.None;
                BPCEffective.DataBind();
                BPCEffective.Visible = true;
                BCEffective.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DHP_M13:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DHP_M13:" + strline + "  " + "DocumentType Effective chart filling failed.", "error");
            }
        }
        public void ChartFill1(int DefaultValue = 0)
        {
            try
            {
                DataTable dt = new DataTable();
                if (DefaultValue == 1)
                {
                    if (ViewState["ReaderPage"].ToString() == "true")
                    {
                        dt = DMS_bal.DMS_HomeDocumentListChart(Convert.ToInt32(ViewState["EffDepartment"].ToString()), Convert.ToInt32(ViewState["EffDocType"].ToString()), 3, Convert.ToInt32(ViewState["emp_id_DMS"]));
                    }
                    else
                    {
                        dt = DMS_bal.DMS_HomeDocumentListChart(Convert.ToInt32(ViewState["EffDepartment"].ToString()), Convert.ToInt32(ViewState["EffDocType"].ToString()), 1, 0);
                    }
                }
                else
                {
                    if (ViewState["ReaderPage"].ToString() == "true")
                    {
                        dt = DMS_bal.DMS_HomeDocumentListChart(Convert.ToInt32(ddl_DeptName1.SelectedValue), Convert.ToInt32(ddl_DocType1.SelectedValue), 3, Convert.ToInt32(ViewState["emp_id_DMS"]));
                        ViewState["EffDepartment"] = ddl_DeptName1.SelectedValue;
                        ViewState["EffDocType"] = ddl_DocType1.SelectedValue;
                    }
                    else
                    {
                        dt = DMS_bal.DMS_HomeDocumentListChart(Convert.ToInt32(ddl_DeptName1.SelectedValue), Convert.ToInt32(ddl_DocType1.SelectedValue), 1, 0);
                        ViewState["EffDepartment"] = ddl_DeptName1.SelectedValue;
                        ViewState["EffDocType"] = ddl_DocType1.SelectedValue;
                    }
                }

                BCEffective.SeriesCollection.Clear();
                BCEffective.DataSource = dt;
                BCEffective.SettingsCommonSeries.ArgumentField = "DeptCode";
                BCEffective.SeriesCollection.Add(new BootstrapChartBarSeries() { ValueField = "NoofDocuments", Name = "NoofDocuments" });
                BCEffective.SettingsLegend.Visible = false;
                BCEffective.SettingsCommonSeries.Label.Visible = true;
                BCEffective.SettingsToolTip.Enabled = true;
                BCEffective.SettingsToolTip.Format.Type = FormatType.None;
                BCEffective.DataBind();
                BPCEffective.Visible = false;
                BCEffective.Visible = true;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DHP_M14:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DHP_M14:" + strline + "  " + "Department Effective chart filling failed.", "error");
            }
        }
        public void ChartFill2(int DefaultValue = 0)
        {
            try
            {
                if (DefaultValue == 1)
                {
                    docObjects.FromDate = Convert.ToDateTime(ViewState["RecRevisedFDate"].ToString());
                    docObjects.ToDate = Convert.ToDateTime(ViewState["RecRevisedTDate"].ToString());
                }
                else
                {
                    docObjects.FromDate = Convert.ToDateTime(txt_FromDate1.Text);
                    docObjects.ToDate = Convert.ToDateTime(txt_ToDate1.Text);
                    ViewState["RecRevisedFDate"] = txt_FromDate1.Text;
                    ViewState["RecRevisedTDate"] = txt_ToDate1.Text;
                }
                DataTable dt = DMS_bal.DMS_RecentlyRevisedDocumentsChart(docObjects);

                BPCRecentlyRevised.SeriesCollection.Clear();
                BPCRecentlyRevised.DataSource = dt;
                BPCRecentlyRevised.SettingsCommonSeries.ArgumentField = "DeptCode";
                BPCRecentlyRevised.SeriesCollection.Add(new BootstrapPieChartSeries() { ValueField = "No.ofRecentlyRevisedDocuments", Name = "No.ofRecentlyRevisedDocuments" });
                BPCRecentlyRevised.SettingsCommonSeries.Label.Visible = true;
                BPCRecentlyRevised.SettingsToolTip.Enabled = true;
                BPCRecentlyRevised.SettingsToolTip.Format.Type = FormatType.None;
                BPCRecentlyRevised.DataBind();
                upRecentlyRevised.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DHP_M15:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DHP_M15:" + strline + "  " + "Department RecentlyRevised chart filling failed.", "error");
            }
        }
        public void ChartFill3()
        {
            try
            {
                docObjects.FromDate = DateTime.Today.AddDays(-30);
                docObjects.ToDate = DateTime.Today;
                DataTable dt = DMS_bal.DMS_RecentlyRevisedDocumentsChart(docObjects);

                BPCRecentlyRevised.SeriesCollection.Clear();
                BPCRecentlyRevised.DataSource = dt;
                BPCRecentlyRevised.SettingsCommonSeries.ArgumentField = "DeptCode";
                BPCRecentlyRevised.SeriesCollection.Add(new BootstrapPieChartSeries() { ValueField = "No.ofRecentlyRevisedDocuments", Name = "No.ofRecentlyRevisedDocuments" });
                BPCRecentlyRevised.SettingsCommonSeries.Label.Visible = true;
                BPCRecentlyRevised.SettingsToolTip.Enabled = true;
                BPCRecentlyRevised.SettingsToolTip.Format.Type = FormatType.None;
                BPCRecentlyRevised.DataBind();
                upRecentlyRevised.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DHP_M16:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DHP_M16:" + strline + "  " + "Department Default RecentlyRevised chart filling failed.", "error");
            }
        }
        public void ChartFill4(int DefaultValue = 0)
        {
            try
            {
                if (DefaultValue == 0)
                {
                    docObjects.DMS_Deparment = 0;
                    docObjects.DMS_DocumentType = 0;
                    docObjects.FromDate = DateTime.Today;
                    docObjects.ToDate = DateTime.Today.AddDays(+30);
                    ViewState["ATEDepartment"] = 0;
                    ViewState["ATEDocType"] = 0;
                    ViewState["ATEFromDate"] = DateTime.Today;
                    ViewState["ATEToDate"] = DateTime.Today.AddDays(+30);
                }
                else if (DefaultValue == 1)
                {
                    docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName2.SelectedValue);
                    docObjects.DMS_DocumentType = Convert.ToInt32(ddl_DocType2.SelectedValue);
                    docObjects.FromDate = Convert.ToDateTime(txt_FromDate2.Text);
                    docObjects.ToDate = Convert.ToDateTime(txt_ToDate2.Text);
                    ViewState["ATEDepartment"] = ddl_DeptName2.SelectedValue;
                    ViewState["ATEDocType"] = ddl_DocType2.SelectedValue;
                    ViewState["ATEFromDate"] = txt_FromDate2.Text;
                    ViewState["ATEToDate"] = txt_ToDate2.Text;
                }
                else if (DefaultValue == 2)
                {
                    docObjects.DMS_Deparment = Convert.ToInt32(ViewState["ATEDepartment"].ToString());
                    docObjects.DMS_DocumentType = Convert.ToInt32(ViewState["ATEDocType"].ToString());
                    docObjects.FromDate = Convert.ToDateTime(ViewState["ATEFromDate"].ToString());
                    docObjects.ToDate = Convert.ToDateTime(ViewState["ATEToDate"].ToString());
                }
                docObjects.Mode = 0;
                DataTable dt = DMS_bal.DMS_ReachingReviewDateChart(docObjects);

                BPCAbouttoExpire.SeriesCollection.Clear();
                BPCAbouttoExpire.DataSource = dt;
                BPCAbouttoExpire.SettingsCommonSeries.ArgumentField = "DocumentType";
                BPCAbouttoExpire.SeriesCollection.Add(new BootstrapPieChartSeries() { ValueField = "NoofDocumentsToExpire", Name = "NoofDocumentsToExpire" });
                BPCAbouttoExpire.SettingsCommonSeries.Label.Visible = true;
                BPCAbouttoExpire.SettingsToolTip.Enabled = true;
                BPCAbouttoExpire.SettingsToolTip.Format.Type = FormatType.None;
                BPCAbouttoExpire.DataBind();
                BPCAbouttoExpire.Visible = true;
                BCAbouttoExpire.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DHP_M17:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DHP_M17:" + strline + "  " + "DocumentType DocumentsToExpire chart filling failed.", "error");
            }
        }
        public void ChartFill5(int DefaultValue = 0)
        {
            try
            {
                if (DefaultValue == 1)
                {
                    docObjects.DMS_Deparment = Convert.ToInt32(ViewState["ATEDepartment"].ToString());
                    docObjects.DMS_DocumentType = Convert.ToInt32(ViewState["ATEDocType"].ToString());
                    docObjects.FromDate = Convert.ToDateTime(ViewState["ATEFromDate"].ToString());
                    docObjects.ToDate = Convert.ToDateTime(ViewState["ATEToDate"].ToString());
                }
                else
                {
                    docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName2.SelectedValue);
                    docObjects.DMS_DocumentType = Convert.ToInt32(ddl_DocType2.SelectedValue);
                    docObjects.FromDate = Convert.ToDateTime(txt_FromDate2.Text);
                    docObjects.ToDate = Convert.ToDateTime(txt_ToDate2.Text);
                    ViewState["ATEDepartment"] = ddl_DeptName2.SelectedValue;
                    ViewState["ATEDocType"] = ddl_DocType2.SelectedValue;
                    ViewState["ATEFromDate"] = txt_FromDate2.Text;
                    ViewState["ATEToDate"] = txt_ToDate2.Text;
                }
                docObjects.Mode = 1;
                DataTable dt = DMS_bal.DMS_ReachingReviewDateChart(docObjects);

                BCAbouttoExpire.SeriesCollection.Clear();
                BCAbouttoExpire.DataSource = dt;
                BCAbouttoExpire.SettingsCommonSeries.ArgumentField = "DeptCode";
                BCAbouttoExpire.SeriesCollection.Add(new BootstrapChartBarSeries() { ValueField = "NoofDocumentsToExpire", Name = "NoofDocumentsToExpire" });
                BCAbouttoExpire.SettingsLegend.Visible = false;
                BCAbouttoExpire.SettingsCommonSeries.Label.Visible = true;
                BCAbouttoExpire.SettingsToolTip.Enabled = true;
                BCAbouttoExpire.SettingsToolTip.Format.Type = FormatType.None;
                BCAbouttoExpire.DataBind();
                BPCAbouttoExpire.Visible = false;
                BCAbouttoExpire.Visible = true;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DHP_M18:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DHP_M18:" + strline + "  " + "Department DocumentsToExpire chart filling failed.", "error");
            }
        }
        public void ChartFill6(int DefaultValue = 0)
        {
            try
            {
                if (DefaultValue == 0)
                {
                    docObjects.DMS_Deparment = 0;
                    docObjects.DMS_DocumentType = 0;
                    ViewState["DocStatusDepartment"] = 0;
                    ViewState["DocStatusDocType"] = 0;
                }
                else if (DefaultValue == 1)
                {
                    docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName3.SelectedValue);
                    docObjects.DMS_DocumentType = Convert.ToInt32(ddl_DocType3.SelectedValue);
                    ViewState["DocStatusDepartment"] = ddl_DeptName3.SelectedValue;
                    ViewState["DocStatusDocType"] = ddl_DocType3.SelectedValue;
                }
                else if (DefaultValue == 2)
                {
                    docObjects.DMS_Deparment = Convert.ToInt32(ViewState["DocStatusDepartment"].ToString());
                    docObjects.DMS_DocumentType = Convert.ToInt32(ViewState["DocStatusDocType"].ToString());
                }
                docObjects.DocStatus = "P";
                docObjects.Mode = 0;
                DataTable dt = DMS_bal.DMS_DocumentStatusChart(docObjects);

                BPCPending.SeriesCollection.Clear();
                BPCPending.DataSource = dt;
                BPCPending.SettingsCommonSeries.ArgumentField = "DocumentType";
                BPCPending.SeriesCollection.Add(new BootstrapPieChartSeries() { ValueField = "NoofDocuments", Name = "NoofDocuments" });
                BPCPending.SettingsCommonSeries.Label.Visible = true;
                BPCPending.SettingsToolTip.Enabled = true;
                BPCPending.SettingsToolTip.Format.Type = FormatType.None;
                BPCPending.DataBind();
                BPCPending.Visible = true;
                BCPending.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DHP_M19:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DHP_M19:" + strline + "  " + "DocumentType DocumentStatus Pending chart filling failed.", "error");
            }
        }
        public void ChartFill7(int DefaultValue = 0)
        {
            try
            {
                if (DefaultValue == 1)
                {
                    docObjects.DMS_Deparment = Convert.ToInt32(ViewState["DocStatusDepartment"].ToString());
                    docObjects.DMS_DocumentType = Convert.ToInt32(ViewState["DocStatusDocType"].ToString());
                }
                else
                {
                    docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName3.SelectedValue);
                    docObjects.DMS_DocumentType = Convert.ToInt32(ddl_DocType3.SelectedValue);
                    ViewState["DocStatusDepartment"] = ddl_DeptName3.SelectedValue;
                    ViewState["DocStatusDocType"] = ddl_DocType3.SelectedValue;
                }
                docObjects.DocStatus = "P";
                docObjects.Mode = 1;
                DataTable dt = DMS_bal.DMS_DocumentStatusChart(docObjects);

                BCPending.SeriesCollection.Clear();
                BCPending.DataSource = dt;
                BCPending.SettingsCommonSeries.ArgumentField = "DeptCode";
                BCPending.SeriesCollection.Add(new BootstrapChartBarSeries() { ValueField = "NoofDocuments", Name = "NoofDocuments" });
                BCPending.SettingsLegend.Visible = false;
                BCPending.SettingsCommonSeries.Label.Visible = true;
                BCPending.SettingsToolTip.Enabled = true;
                BCPending.SettingsToolTip.Format.Type = FormatType.None;
                BCPending.DataBind();
                BPCPending.Visible = false;
                BCPending.Visible = true;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DHP_M20:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DHP_M20:" + strline + "  " + "Department DocumentStatus Pending chart filling failed.", "error");
            }
        }
        public void ChartFill8(int DefaultValue = 0)
        {
            try
            {
                if (DefaultValue == 0)
                {
                    docObjects.DMS_Deparment = 0;
                    docObjects.DMS_DocumentType = 0;
                    ViewState["DocStatusDepartment"] = 0;
                    ViewState["DocStatusDocType"] = 0;
                }
                else if (DefaultValue == 1)
                {
                    docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName3.SelectedValue);
                    docObjects.DMS_DocumentType = Convert.ToInt32(ddl_DocType3.SelectedValue);
                    ViewState["DocStatusDepartment"] = ddl_DeptName3.SelectedValue;
                    ViewState["DocStatusDocType"] = ddl_DocType3.SelectedValue;
                }
                else if (DefaultValue == 2)
                {
                    docObjects.DMS_Deparment = Convert.ToInt32(ViewState["DocStatusDepartment"].ToString());
                    docObjects.DMS_DocumentType = Convert.ToInt32(ViewState["DocStatusDocType"].ToString());
                }
                docObjects.DocStatus = "W";
                docObjects.Mode = 0;
                DataTable dt = DMS_bal.DMS_DocumentStatusChart(docObjects);

                BPCWithdrawn.SeriesCollection.Clear();
                BPCWithdrawn.DataSource = dt;
                BPCWithdrawn.SettingsCommonSeries.ArgumentField = "DocumentType";
                BPCWithdrawn.SeriesCollection.Add(new BootstrapPieChartSeries() { ValueField = "NoofDocuments", Name = "NoofDocuments" });
                BPCWithdrawn.SettingsCommonSeries.Label.Visible = true;
                BPCWithdrawn.SettingsToolTip.Enabled = true;
                BPCWithdrawn.SettingsToolTip.Format.Type = FormatType.None;
                BPCWithdrawn.DataBind();
                BPCWithdrawn.Visible = true;
                BCWithdrawn.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DHP_M21:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DHP_M21:" + strline + "  " + "DocumentType DocumentStatus Withdrawn chart filling failed.", "error");
            }
        }
        public void ChartFill9(int DefaultValue = 0)
        {
            try
            {
                if (DefaultValue == 1)
                {
                    docObjects.DMS_Deparment = Convert.ToInt32(ViewState["DocStatusDepartment"].ToString());
                    docObjects.DMS_DocumentType = Convert.ToInt32(ViewState["DocStatusDocType"].ToString());
                }
                else
                {
                    docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName3.SelectedValue);
                    docObjects.DMS_DocumentType = Convert.ToInt32(ddl_DocType3.SelectedValue);
                    ViewState["DocStatusDepartment"] = ddl_DeptName3.SelectedValue;
                    ViewState["DocStatusDocType"] = ddl_DocType3.SelectedValue;
                }
                docObjects.DocStatus = "W";
                docObjects.Mode = 1;
                DataTable dt = DMS_bal.DMS_DocumentStatusChart(docObjects);

                BCWithdrawn.SeriesCollection.Clear();
                BCWithdrawn.DataSource = dt;
                BCWithdrawn.SettingsCommonSeries.ArgumentField = "DeptCode";
                BCWithdrawn.SeriesCollection.Add(new BootstrapChartBarSeries() { ValueField = "NoofDocuments", Name = "NoofDocuments" });
                BCWithdrawn.SettingsLegend.Visible = false;
                BCWithdrawn.SettingsCommonSeries.Label.Visible = true;
                BCWithdrawn.SettingsToolTip.Enabled = true;
                BCWithdrawn.SettingsToolTip.Format.Type = FormatType.None;
                BCWithdrawn.DataBind();
                BPCWithdrawn.Visible = false;
                BCWithdrawn.Visible = true;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DHP_M22:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DHP_M22:" + strline + "  " + "Department DocumentStatus Withdrawn chart filling failed.", "error");
            }
        }
        public void ChartFill10(int DefaultValue = 0)
        {
            try
            {
                if (DefaultValue == 0)
                {
                    docObjects.DMS_Deparment = 0;
                    docObjects.DMS_DocumentType = 0;
                    ViewState["DocStatusDepartment"] = 0;
                    ViewState["DocStatusDocType"] = 0;
                }
                else if (DefaultValue == 1)
                {
                    docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName3.SelectedValue);
                    docObjects.DMS_DocumentType = Convert.ToInt32(ddl_DocType3.SelectedValue);
                    ViewState["DocStatusDepartment"] = ddl_DeptName3.SelectedValue;
                    ViewState["DocStatusDocType"] = ddl_DocType3.SelectedValue;
                }
                else if (DefaultValue == 2)
                {
                    docObjects.DMS_Deparment = Convert.ToInt32(ViewState["DocStatusDepartment"].ToString());
                    docObjects.DMS_DocumentType = Convert.ToInt32(ViewState["DocStatusDocType"].ToString());
                }
                docObjects.DocStatus = "A";
                docObjects.Mode = 0;
                DataTable dt = DMS_bal.DMS_DocumentStatusChart(docObjects);

                BPCActive.SeriesCollection.Clear();
                BPCActive.DataSource = dt;
                BPCActive.SettingsCommonSeries.ArgumentField = "DocumentType";
                BPCActive.SeriesCollection.Add(new BootstrapPieChartSeries() { ValueField = "NoofDocuments", Name = "NoofDocuments" });
                BPCActive.SettingsCommonSeries.Label.Visible = true;
                BPCActive.SettingsToolTip.Enabled = true;
                BPCActive.SettingsToolTip.Format.Type = FormatType.None;
                BPCActive.DataBind();
                BPCActive.Visible = true;
                BCActive.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DHP_M23:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DHP_M23:" + strline + "  " + "DocumentType DocumentStatus Active chart filling failed.", "error");
            }
        }
        public void ChartFill11(int DefaultValue = 0)
        {
            try
            {
                if (DefaultValue == 1)
                {
                    docObjects.DMS_Deparment = Convert.ToInt32(ViewState["DocStatusDepartment"].ToString());
                    docObjects.DMS_DocumentType = Convert.ToInt32(ViewState["DocStatusDocType"].ToString());
                }
                else
                {
                    docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName3.SelectedValue);
                    docObjects.DMS_DocumentType = Convert.ToInt32(ddl_DocType3.SelectedValue);
                    ViewState["DocStatusDepartment"] = ddl_DeptName3.SelectedValue;
                    ViewState["DocStatusDocType"] = ddl_DocType3.SelectedValue;
                }
                docObjects.DocStatus = "A";
                docObjects.Mode = 1;
                DataTable dt = DMS_bal.DMS_DocumentStatusChart(docObjects);

                BCActive.SeriesCollection.Clear();
                BCActive.DataSource = dt;
                BCActive.SettingsCommonSeries.ArgumentField = "DeptCode";
                BCActive.SeriesCollection.Add(new BootstrapChartBarSeries() { ValueField = "NoofDocuments", Name = "NoofDocuments" });
                BCActive.SettingsLegend.Visible = false;
                BCActive.SettingsCommonSeries.Label.Visible = true;
                BCActive.SettingsToolTip.Enabled = true;
                BCActive.SettingsToolTip.Format.Type = FormatType.None;
                BCActive.DataBind();
                BPCActive.Visible = false;
                BCActive.Visible = true;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DHP_M24:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DHP_M24:" + strline + "  " + "Department DocumentStatus Active chart filling failed.", "error");
            }
        }
        public void ChartFill14(int DefaultValue = 0)
        {
            try
            {
                if (DefaultValue == 0)
                {
                    docObjects.DMS_Deparment = 0;
                    docObjects.DMS_DocumentType = 0;
                    ViewState["DocStatusDepartment"] = 0;
                    ViewState["DocStatusDocType"] = 0;
                }
                else if (DefaultValue == 1)
                {
                    docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName3.SelectedValue);
                    docObjects.DMS_DocumentType = Convert.ToInt32(ddl_DocType3.SelectedValue);
                    ViewState["DocStatusDepartment"] = ddl_DeptName3.SelectedValue;
                    ViewState["DocStatusDocType"] = ddl_DocType3.SelectedValue;
                }
                else if (DefaultValue == 2)
                {
                    docObjects.DMS_Deparment = Convert.ToInt32(ViewState["DocStatusDepartment"].ToString());
                    docObjects.DMS_DocumentType = Convert.ToInt32(ViewState["DocStatusDocType"].ToString());
                }
                docObjects.DocStatus = "O";
                docObjects.Mode = 0;
                DataTable dt = DMS_bal.DMS_DocumentStatusChart(docObjects);

                BPCOverdue.SeriesCollection.Clear();
                BPCOverdue.DataSource = dt;
                BPCOverdue.SettingsCommonSeries.ArgumentField = "DocumentType";
                BPCOverdue.SeriesCollection.Add(new BootstrapPieChartSeries() { ValueField = "NoofDocuments", Name = "NoofDocuments" });
                BPCOverdue.SettingsCommonSeries.Label.Visible = true;
                BPCOverdue.SettingsToolTip.Enabled = true;
                BPCOverdue.SettingsToolTip.Format.Type = FormatType.None;
                BPCOverdue.DataBind();
                BPCOverdue.Visible = true;
                BCOverdue.Visible = false;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DHP_M27:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DHP_M27:" + strline + "  " + "DocumentType DocumentStatus Overdue chart filling failed.", "error");
            }
        }
        public void ChartFill15(int DefaultValue = 0)
        {
            try
            {
                if (DefaultValue == 1)
                {
                    docObjects.DMS_Deparment = Convert.ToInt32(ViewState["DocStatusDepartment"].ToString());
                    docObjects.DMS_DocumentType = Convert.ToInt32(ViewState["DocStatusDocType"].ToString());
                }
                else
                {
                    docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName3.SelectedValue);
                    docObjects.DMS_DocumentType = Convert.ToInt32(ddl_DocType3.SelectedValue);
                    ViewState["DocStatusDepartment"] = ddl_DeptName3.SelectedValue;
                    ViewState["DocStatusDocType"] = ddl_DocType3.SelectedValue;
                }
                docObjects.DocStatus = "O";
                docObjects.Mode = 1;
                DataTable dt = DMS_bal.DMS_DocumentStatusChart(docObjects);

                BCOverdue.SeriesCollection.Clear();
                BCOverdue.DataSource = dt;
                BCOverdue.SettingsCommonSeries.ArgumentField = "DeptCode";
                BCOverdue.SeriesCollection.Add(new BootstrapChartBarSeries() { ValueField = "NoofDocuments", Name = "NoofDocuments" });
                BCOverdue.SettingsLegend.Visible = false;
                BCOverdue.SettingsCommonSeries.Label.Visible = true;
                BCOverdue.SettingsToolTip.Enabled = true;
                BCOverdue.SettingsToolTip.Format.Type = FormatType.None;
                BCOverdue.DataBind();
                BPCOverdue.Visible = false;
                BCOverdue.Visible = true;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DHP_M28:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DHP_M28:" + strline + "  " + "Department DocumentStatus Overdue chart filling failed.", "error");
            }
        }
        protected void btn_Effective_Click(object sender, EventArgs e)
        {
            if (ddl_DeptName1.SelectedValue == "0" && ddl_DocType1.SelectedValue == "0")
            {
                ChartFill(0);
            }
            else if (ddl_DeptName1.SelectedValue != "0" && ddl_DocType1.SelectedValue == "0")
            {
                ChartFill(1);
            }
            else
            {
                ChartFill1();
            }
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop", "dashboard1();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop1", "dashboard2();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop2", "dashboard3();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop3", "dashboard4();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "hide", "$('#filter_dropdown1').hide();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            upEffective.Update();
        }
        protected void btn_EffectiveClose_Click(object sender, EventArgs e)
        {
            if (ViewState["EffDepartment"].ToString() == "0" && ViewState["EffDocType"].ToString() == "0")
            {
                ChartFill(0);
            }
            else if (ViewState["EffDepartment"].ToString() != "0" && ViewState["EffDocType"].ToString() == "0")
            {
                ChartFill(2);
            }
            else
            {
                ChartFill1(1);
            }
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop", "dashboard1();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop1", "dashboard2();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop2", "dashboard3();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop3", "dashboard4();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "hide1", "$('#filter_dropdown1').hide();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            upEffective.Update();
        }
        protected void btnRefreshEffectiveChart_Click(object sender, EventArgs e)
        {
            ChartFill(0);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop", "dashboard1();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop1", "dashboard2();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop2", "dashboard3();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop3", "dashboard4();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "reset1", "ResetEffChart();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            upEffective.Update();
        }
        protected void btn_RecentlyRevised_Click(object sender, EventArgs e)
        {
            ChartFill2();
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop", "dashboard1();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop1", "dashboard2();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop2", "dashboard3();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop3", "dashboard4();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "hide1", "$('#filter_dropdown2').hide();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            upRecentlyRevised.Update();
        }
        protected void btn_RecentlyRevisedClose_Click(object sender, EventArgs e)
        {
            ChartFill2(1);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop", "dashboard1();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop1", "dashboard2();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop2", "dashboard3();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop3", "dashboard4();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "hide1", "$('#filter_dropdown2').hide();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            upRecentlyRevised.Update();
        }
        protected void btnRefreshRecentlyRevisedChart_Click(object sender, EventArgs e)
        {
            ChartFill3();
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop", "dashboard1();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop1", "dashboard2();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop2", "dashboard3();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop3", "dashboard4();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "refresh1", "myfunction();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            upRecentlyRevised.Update();
        }
        protected void btn_ReachingReviewDate_Click(object sender, EventArgs e)
        {
            if (ddl_DeptName2.SelectedValue == "0" && ddl_DocType2.SelectedValue == "0" && txt_FromDate2.Text == DateTime.Today.ToString("dd MMM yyyy") && txt_ToDate2.Text == DateTime.Today.AddDays(+30).ToString("dd MMM yyyy"))
            {
                ChartFill4(0);
            }
            else if (ddl_DeptName2.SelectedValue == "0" && ddl_DocType2.SelectedValue == "0" && (txt_FromDate2.Text != DateTime.Today.ToString("dd MMM yyyy") || txt_ToDate2.Text != DateTime.Today.AddDays(+30).ToString("dd MMM yyyy")))
            {
                ChartFill4(1);
            }
            else if (ddl_DeptName2.SelectedValue != "0" && ddl_DocType2.SelectedValue == "0")
            {
                ChartFill4(1);
            }
            else
            {
                ChartFill5();
            }
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop", "dashboard1();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop1", "dashboard2();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop2", "dashboard3();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop3", "dashboard4();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "hide1", "$('#filter_dropdown3').hide();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            upToExpire.Update();
        }
        protected void btn_ReachingReviewDateClose_Click(object sender, EventArgs e)
        {
            if (ViewState["ATEDepartment"].ToString() == "0" && ViewState["ATEDocType"].ToString() == "0" && Convert.ToDateTime(ViewState["ATEFromDate"]).ToString("dd MMM yyyy") == DateTime.Today.ToString("dd MMM yyyy") && Convert.ToDateTime(ViewState["ATEToDate"]).ToString("dd MMM yyyy") == DateTime.Today.AddDays(+30).ToString("dd MMM yyyy"))
            {
                ChartFill4(0);
            }
            else if (ViewState["ATEDepartment"].ToString() == "0" && ViewState["ATEDocType"].ToString() == "0" && (Convert.ToDateTime(ViewState["ATEFromDate"]).ToString("dd MMM yyyy") != DateTime.Today.ToString("dd MMM yyyy") || Convert.ToDateTime(ViewState["ATEToDate"]).ToString("dd MMM yyyy") != DateTime.Today.AddDays(+30).ToString("dd MMM yyyy")))
            {
                ChartFill4(2);
            }
            else if (ViewState["ATEDepartment"].ToString() != "0" && ViewState["ATEDocType"].ToString() == "0")
            {
                ChartFill4(2);
            }
            else
            {
                ChartFill5(1);
            }
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop", "dashboard1();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop1", "dashboard2();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop2", "dashboard3();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop3", "dashboard4();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "hide1", "$('#filter_dropdown3').hide();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            upToExpire.Update();
        }
        protected void btnRefreshAbouttoExpireChart_Click(object sender, EventArgs e)
        {
            ChartFill4(0);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop", "dashboard1();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop1", "dashboard2();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop2", "dashboard3();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop3", "dashboard4();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "refresh1", "myfunction();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            upToExpire.Update();
        }
        protected void btn_DocumentStatus_Click(object sender, EventArgs e)
        {
            if (ddl_DeptName3.SelectedValue == "0" && ddl_DocType3.SelectedValue == "0")
            {
                ChartFill6(0);
                ChartFill8(0);
                ChartFill10(0);
                ChartFill14(0);
            }
            else if (ddl_DeptName3.SelectedValue != "0" && ddl_DocType3.SelectedValue == "0")
            {
                ChartFill6(1);
                ChartFill8(1);
                ChartFill10(1);
                ChartFill14(1);
            }
            else
            {
                ChartFill7();
                ChartFill9();
                ChartFill11();
                ChartFill15();
            }
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop", "dashboard1();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop1", "dashboard2();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop2", "dashboard3();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop3", "dashboard4();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "hide1", "$('#filter_dropdown4').hide();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            upDocumentStatus.Update();
        }
        protected void btn_DocumentStatusClose_Click(object sender, EventArgs e)
        {
            if (ViewState["DocStatusDepartment"].ToString() == "0" && ViewState["DocStatusDocType"].ToString() == "0")
            {
                ChartFill6(0);
                ChartFill8(0);
                ChartFill10(0);
                ChartFill14(0);
            }
            else if (ViewState["DocStatusDepartment"].ToString() != "0" && ViewState["DocStatusDocType"].ToString() == "0")
            {
                ChartFill6(2);
                ChartFill8(2);
                ChartFill10(2);
                ChartFill14(2);
            }
            else
            {
                ChartFill7(1);
                ChartFill9(1);
                ChartFill11(1);
                ChartFill15(1);
            }
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop", "dashboard1();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop1", "dashboard2();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop2", "dashboard3();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop3", "dashboard4();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "hide1", "$('#filter_dropdown4').hide();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            upDocumentStatus.Update();
        }
        protected void btnRefreshDocumentStatusChart_Click(object sender, EventArgs e)
        {
            ChartFill6(0);
            ChartFill8(0);
            ChartFill10(0);
            ChartFill14(0);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop", "dashboard1();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop1", "dashboard2();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop2", "dashboard3();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop3", "dashboard4();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "reset1", "ResetDocStatusChart();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            upDocumentStatus.Update();
        }
        private Color[] chartColorCodes()
        {
            Color[] customPalette = new Color[44];
            customPalette[0] = ColorTranslator.FromHtml("#CCCC66");
            customPalette[1] = ColorTranslator.FromHtml("#669933");
            customPalette[2] = ColorTranslator.FromHtml("#0099CC");
            customPalette[3] = ColorTranslator.FromHtml("#3366FF");
            customPalette[4] = ColorTranslator.FromHtml("#3333CC");
            customPalette[5] = ColorTranslator.FromHtml("#FFCC00");
            customPalette[6] = ColorTranslator.FromHtml("#66CC66");
            customPalette[7] = ColorTranslator.FromHtml("#0099FF");
            customPalette[8] = ColorTranslator.FromHtml("#3366CC");
            customPalette[9] = ColorTranslator.FromHtml("#333399");
            customPalette[10] = ColorTranslator.FromHtml("#c1cdc1");
            customPalette[11] = ColorTranslator.FromHtml("#cdb79e");
            customPalette[12] = ColorTranslator.FromHtml("#8fbc8f");
            customPalette[13] = ColorTranslator.FromHtml("#87cefa");
            customPalette[14] = ColorTranslator.FromHtml("#d8bfd8");
            customPalette[15] = ColorTranslator.FromHtml("#0099CC");
            customPalette[16] = ColorTranslator.FromHtml("#339999");
            customPalette[17] = ColorTranslator.FromHtml("#336699");
            customPalette[18] = ColorTranslator.FromHtml("#FFCC99");
            customPalette[19] = ColorTranslator.FromHtml("#FFFFCC");
            customPalette[20] = ColorTranslator.FromHtml("#999999");
            customPalette[21] = ColorTranslator.FromHtml("#333333");
            customPalette[22] = ColorTranslator.FromHtml("#666633");
            customPalette[23] = ColorTranslator.FromHtml("#9966FF");
            customPalette[24] = ColorTranslator.FromHtml("#FF6633");
            customPalette[25] = ColorTranslator.FromHtml("#800517");
            customPalette[26] = ColorTranslator.FromHtml("#3EA055");
            customPalette[27] = ColorTranslator.FromHtml("#3090C7");
            customPalette[28] = ColorTranslator.FromHtml("#98BFC1");
            customPalette[29] = ColorTranslator.FromHtml("#2B65EC");
            customPalette[30] = ColorTranslator.FromHtml("#7BCCB5");
            customPalette[31] = ColorTranslator.FromHtml("#54C571");
            customPalette[32] = ColorTranslator.FromHtml("#B2C248");
            customPalette[33] = ColorTranslator.FromHtml("#E2A76F");
            customPalette[34] = ColorTranslator.FromHtml("#7F462C");
            customPalette[35] = ColorTranslator.FromHtml("#C85A17");
            customPalette[36] = ColorTranslator.FromHtml("#E77471");
            customPalette[37] = ColorTranslator.FromHtml("#7D0552");
            customPalette[38] = ColorTranslator.FromHtml("#E38AAE");
            customPalette[39] = ColorTranslator.FromHtml("#4E387E");
            customPalette[40] = ColorTranslator.FromHtml("#C45AEC");
            customPalette[41] = ColorTranslator.FromHtml("#893BFF");
            customPalette[42] = ColorTranslator.FromHtml("#B93B8F");
            customPalette[43] = ColorTranslator.FromHtml("#E67451");
            return customPalette;
        }
        protected void BPCEffective_Init(object sender, EventArgs e)
        {
            //BPCEffective.CustomPalette = chartColorCodes();
        }
        protected void BPCRecentlyRevised_Init(object sender, EventArgs e)
        {
            // BPCRecentlyRevised.CustomPalette = chartColorCodes();
        }
        protected void BPCAbouttoExpire_Init(object sender, EventArgs e)
        {
            //BPCAbouttoExpire.CustomPalette = chartColorCodes();
        }
        protected void BPCPending_Init(object sender, EventArgs e)
        {
            // BPCPending.CustomPalette = chartColorCodes();
        }
        protected void BPCWithdrawn_Init(object sender, EventArgs e)
        {
            //BPCWithdrawn.CustomPalette = chartColorCodes();
        }
        protected void BPCActive_Init(object sender, EventArgs e)
        {
            BPCActive.CustomPalette = chartColorCodes();
        }
        protected void BPCOverdue_Init(object sender, EventArgs e)
        {
            //BPCOverdue.CustomPalette = chartColorCodes();
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}
