﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="MainFormList.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.MainFormList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div id="divMainFormListReport" style="display:none">
        <%--<asp:Button ID="btnMainFormListReport" runat="server" class="float-right  report_btn" Text="Report" OnClick="btnMainFormListReport_Click" style="margin-left:4px;" OnClientClick="DoubleClickHandleReport()"/>--%>
       <input type="button" id="btnMainFormListReport"  class="float-right  btn-signup_popup report_btn" onclick="PrintMainFormList();" value="Reports"/>
   
        </div>
    <div class="padding-none">
        <asp:Button ID="btnAdd" runat="server" class="float-right  btn-signup_popup" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
    </div>
    <div class=" col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left">
        <div class=" col-md-12 col-lg-12 col-12 col-sm-12 dms_outer_border float-left padding-none">
            <div class="col-md-12 col-lg-12 col-12 col-sm-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server" visible="false">
                <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left" id="divMainContainer" runat="server" visible="true">
                <div class=" col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-right ">
                    <div class="col-md-12 col-lg-12 col-12 col-sm-12  padding-none float-left">
                        <div class="grid_header col-md-12 col-lg-12 col-12 col-sm-12 float-left">Form List</div>
                        <div class="col-md-12 col-lg-12 col-12 col-sm-12 grid_panel_full border_top_none bottom float-left">
                            <div class="col-12 float-left padding-none top bottom">
                            <table id="dtDocList" class="display datatable_cust" cellspacing="0" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>VersionID</th>
                                        <th>Form Number</th>
                                        <th>Form Name</th>
                                        <th>Form Description</th>
                                        <th>Version</th>
                                        <th>Department</th>
                                        <th>RefDocNumber</th>
                                        <th>Creator</th>
                                        <th>Approver</th>
                                        <th>Action</th>
                                        <th>View Form</th>
                                        <th>History</th>
                                        <th>Edit</th>
                                        <th>FormRevisionCount</th>
                                    </tr>
                                </thead>
                            </table>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-------- Form VIEW-------------->
    <div id="myFormView" class="modal department fade" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 80%; height: 500px">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpHeading">
                        <ContentTemplate>
                            <span id="span1" runat="server"></span>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="divPDF_Viewer">
                        <noscript>
                            Please Enable JavaScript.
                        </noscript>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--------End Form VIEW-------------->

    <!--------Comment History View-------------->
    <div id="myCommentHistory" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 85%">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel ID="upcomment" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-header">
                           
                            <span id="span4" runat="server"></span>
                             <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body" style="max-height: 600px; overflow-y: auto">
                            <div class=" history_grid" style="overflow-y: auto;">
                                <asp:GridView ID="gv_CommentHistory" runat="server" CssClass="table table-hover" ClientIDMode="Static" EmptyDataText="No Records Found" AutoGenerateColumns="False" PagerSettings-Mode="NumericFirstLast" BackColor="White" BorderColor="#d2d2d2" BorderStyle="None" BorderWidth="1px" OnRowDataBound="gv_CommentHistory_RowDataBound" OnRowCommand="gv_CommentHistory_RowCommand" AllowPaging="true" PageSize="10" OnPageIndexChanging="gv_CommentHistory_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="User" ItemStyle-Width="12%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_GivenBy" runat="server" Text='<%# Eval("ActionBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" ItemStyle-Width="11%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Date" runat="server" Text='<%# Eval("ActionDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role" ItemStyle-Width="13%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Role" runat="server" Text='<%# Eval("CommentedRole") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="20%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Action" runat="server" Text='<%# Eval("Action") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments" ItemStyle-Width="29%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_shortComments" runat="server" Text='<%# Eval("Comments") %>'></asp:Label>
                                                <asp:TextBox ID="txt_Comments" runat="server" ReadOnly="true" Rows="2" TextMode="MultiLine" Text='<%# Eval("Comments") %>' Width="100%"></asp:TextBox>
                                                <asp:LinkButton ID="lbcommentsmore" runat="server" CommandArgument='<%# Eval("Comments")%>' CommandName="SM">Show More</asp:LinkButton>
                                                <asp:LinkButton ID="lbcommentsless" runat="server" CommandArgument='<%# Eval("Comments") %>' CommandName="SL">Show Less</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Assigned To">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_AssignedTo" runat="server" Text='<%# Eval("AssignedTo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle cssClass="gridpager"  HorizontalAlign="Right" />
                                </asp:GridView>
                            </div>
                        </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!--------End Comment History View-------------->

    <!--------Edit Document Privacy View-------------->
    <div id="myFormEdit" class="modal department fade" role="dialog" style="top: 250px" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md" style="min-width: 21%; overflow: hidden">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <span id="span2" runat="server"></span>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" style="max-height: 680px;">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <label for="inputPassword3" class=" col-4 padding-none control-label required">Request Type:</label>
                            <div class="btn-group padding-none col-sm-12 col-12 col-lg-12 col-md-12 bottom " data-toggle="buttons" id="divOpinion" runat="server">
                                
                                <label class="btn btn-primary_radio radio_check col-6 col-lg-6 col-md-6 col-sm-6" id="lblRadioTrainingYes">
                                    <asp:RadioButton ID="rbtnRevision" value="Yes" runat="server" GroupName="A" Text="Revision" />
                                </label>
                                <label class="btn btn-primary_radio radio_check col-6 col-lg-6 col-md-6 col-sm-6" id="lblRadioTrainingNo">
                                    <asp:RadioButton ID="rbtnStatusChange" value="No" runat="server" GroupName="A" Text="Withdraw" />
                                </label>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer form-group">
                    <button type="button" id="btn_Submit" runat="server" onclick="return RequiredFields();" class=" btn-signup_popup">Submit</button>
                    <button type="button" data-dismiss="modal" runat="server" id="btnclose1" class=" btn-cancel_popup">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!--------End Edit Document Privacy View-------------->

    <!--Mani Document Fill View -->
        <div id="MyModelMainFormView" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
            <div id="divModalSize" class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content padding-none">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <div class="modal-header">
                        <h4 class="modal-title">Main Form List Report</h4>
                        <div class=" float-right padding-none">
                            <button type="button" class="close" data-dismiss="modal" onclick="closeWaitModelCCN();">&times;</button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="embed-responsive embed-responsive-16by9" id="dvmainformfileframe">
                        </div>
                    </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>                   
                </div>
            </div>
        </div>
  <!--End-->
        <!--Mani Document Fill View -->
        <div id="MyModelMainFormListView" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
            <div id="divModalSize" class="modal-dialog modal-lg" style="min-width:80%;">
                <!-- Modal content-->
                <div class="modal-content padding-none">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <div class="modal-header">
                        <h4 class="modal-title">Form List Report</h4>
                        <div class=" float-right padding-none">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="embed-responsive embed-responsive-16by9" id="dvfileframe">
                        </div>
                    </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>                   
                </div>
            </div>
        </div>
        <!--End-->

    <asp:HiddenField ID="hdfVID" runat="server" />
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
    <asp:HiddenField ID="hdfRole" runat="server" />
    <asp:HiddenField ID="hdfActionID" runat="server" />
    <asp:HiddenField ID="hdfViewType" runat="server" />
    <asp:HiddenField ID="hdfViewEmpID" runat="server" />
    <asp:HiddenField ID="hdfViewRoleID" runat="server" />
    <asp:HiddenField ID="hdfViewDivID" runat="server" />
    <asp:UpdatePanel ID="UpGridButtons" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnFormView" runat="server" Text="Submit" Style="display: none" OnClick="btnFormView_Click" OnClientClick="showImg();"/>
            <asp:Button ID="btnCommentsHistoryView" runat="server" Text="Submit" Style="display: none" OnClick="btnCommentsHistoryView_Click" OnClientClick="showImg();"/>
            <asp:Button ID="btnEdit" runat="server" Text="Submit" Style="display: none" OnClick="btnEdit_Click" OnClientClick="showImg();"/>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>
        $('#dtDocList thead tr').clone(true).appendTo('#dtDocList thead');
        $('#dtDocList thead tr:eq(1) th').each(function (i) {
            if (i < 11) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder=" Search " />');

                $('input', this).on('keyup change', function () {
                    if (oTable.column(i).search() !== this.value) {
                        oTable
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
                if (i == 1) {
                    $(this).html('');
                }
            }
            else {
                $(this).html('');
            }
        });
        $('#dtDocList').wrap('<div class="dataTables_scroll" />');
        var oTable = $('#dtDocList').DataTable({
            columns: [
                { 'data': 'RowNumber' },//0
                { 'data': 'FormVersionID' },//1
                { 'data': 'FormNumber' },//2
                { 'data': 'FormName' },//3
                { 'data': 'Purpose' },//4
                { 'data': 'VersionNumber' },//5
                { 'data': 'dept' },//6
                { 'data': 'documentNUmber' },//7
                { 'data': 'CreatedBy' },//8
                { 'data': 'oldApprovedBY' },//9
                { 'data': 'ActionName' },//10
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="view" title="View Doc" data-toggle="modal" data-target="#myFormView" onclick=ViewForm(' + o.FormVersionID + ')></a>'; }
                },//11
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="summary_latest" title="History" data-toggle="modal" data-target="#myCommentHistory" onclick=ViewCommentHistory(' + o.FormVersionID + ')></a>'; }
                },//12
                { 'data': 'action' },//13
                { 'data': 'PendingFormRevisionCount' },//14
            ],
            "responsive": true,
            "paging": true,
            "search": {
                "regex": true
            },
            "aoColumnDefs": [{ "targets": [0,1, 4,7,8,9, 13, 14], "visible": false, "searchable": false }, { "bSortable": false, "aTargets": [0, 13] }, { "className": "dt-body-left", "targets": [2, 3, 4, 6, 8, 9, 10] },
            {
                targets: [13], render: function (a, b, data, d) {
                    if (data.action == 3 && data.PendingFormRevisionCount == 0) {
                        return '<a class="Edit" title="Edit" data-toggle="modal" data-target="#myFormEdit"  onclick=EditAction(' + data.FormVersionID + ')>' + '' + '</a>';
                    } else {
                        return 'N/A';
                    }
                }
            },
            ],
            "orderCellsTop": true,
            "order": [[1, "desc"]],
            'bAutoWidth': true,
            sServerMethod: 'Post',
            "sAjaxSource": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetFormMainList" )%>',
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#dtDocList").show();
                        UserSessionCheck();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
    </script>
    <script>
        function DoubleClickHandleReport() {
            custAlertMsg("Document Report is downloading please wait,Click OK after download is completed","info")
        }
        var RoleID = document.getElementById("<%=hdfRole.ClientID%>").value;
        if (RoleID == 2 || RoleID==30)
        {
            $("#divMainFormListReport").show();
        }
        function ViewForm(V_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = V_ID;
            var btnRV = document.getElementById("<%=btnFormView.ClientID %>");
            btnRV.click();
        }
        function ViewCommentHistory(PK_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = PK_ID;
            var btnCV = document.getElementById("<%=btnCommentsHistoryView.ClientID %>");
            btnCV.click();
        }
        function EditAction(PK_ID) {

            document.getElementById("<%=hdfVID.ClientID%>").value = PK_ID;
            var btnDE = document.getElementById("<%=btnEdit.ClientID %>");
            btnDE.click();
        }
    </script>
    <script>
        function getadmincolumn() {
            var RoleID = document.getElementById("<%=hdfRole.ClientID%>").value;
            if (RoleID == "2") {
                oTable.column(13).visible(true);
            }
            else {
                oTable.column(13).visible(false);
            }
        }
    </script>
       <script src='<%= Page.ResolveUrl("~/AppScripts/PDF_Viewer.min.js")%>'></script>
    <script src='<%= Page.ResolveUrl("~/AppScripts/MainDocPDF_Viewer.js")%>'></script>
    <script type="text/javascript">
        function RequiredFields() {
            var rbtnStatusChange = document.getElementById("<%=rbtnStatusChange.ClientID%>");
            var rbtnRevision = document.getElementById("<%=rbtnRevision.ClientID%>");
             errors = [];
             if (rbtnStatusChange.checked == false && rbtnRevision.checked == false) {
                 errors.push("Select revision or withdraw.");
             }
             if (errors.length > 0) {
                 custAlertMsg(errors.join("<br/>"), "error");
                 return false;
             }
             else {
                 var fvID = document.getElementById("<%=hdfVID.ClientID%>").value;

                 var statuschange = "/DMS/DMSAdmin/Form_Creation.aspx?FormVersionID=" + fvID + "&from=s";
                 var Revision = "/DMS/DMSAdmin/Form_Creation.aspx?FormVersionID=" + fvID + "&from=r";

                 if (rbtnStatusChange.checked) {
                     window.location.href = statuschange;
                 }
                 else if (rbtnRevision.checked) {
                     window.location.href = Revision;
                 }
                 return true;
             }
        }
        function CloseBrowser() {
            window.close();
        }
        function ViewPDFForm() {
            $('#myFormView').modal({ show: true, backdrop: 'static', keyboard: false });
            PdfViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>', "3", $('#<%=hdfVID.ClientID%>').val(), "0", "0", "#divPDF_Viewer", '');
        }
    </script>
      <script>
        function showImg() {
            ShowPleaseWait('show');
        }
        function hideImg() {
            ShowPleaseWait('hide');
        }     
    
    function PrintMainFormList()
        {
         $.ajax({  
             type: "POST",  
             url: "WebServices/DMSService.asmx/ReportMainFormList",  
             contentType: "application/json; charset=utf-8",  
             dataType: "json",  
             data: JSON.stringify({IsJqueryDataList:"N"}),
             success: function (result) {
                 var message = JSON.stringify(result.d);
                 var parsePath = JSON.parse(message);
                 let finalPath = JSON.parse(parsePath);
                 if (finalPath=="NoData")
                 {
                     custAlertMsg("Records Not Found", "error");
                 }
                 else
                 {
                    var dvShow = document.getElementById("dvfileframe");
                    AdminMainDocPrintFilePath = finalPath;//Store File Path
                     PdfMainDocViewerContentLoading(AdminMainDocPrintFilePath, dvShow);
                     $("#MyModelMainFormListView").modal("show");
                 }
             },  
             failure: function (response) {  
                 alert(response.d);  
             }  
         }); 
        }
    </script>
</asp:Content>
