﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using AizantIT_PharmaApp.DMS.WebServices;
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_BusinessLayer;

namespace AizantIT_PharmaApp.DMS
{
    public partial class FormPrintReqMainList : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocObjects docObjects = new DocObjects();
        UMS_BAL objUMS_BAL;
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    hdnAdminRole.Value = "2";
                    if (!IsPostBack)
                    {
                        hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            Session["FormReturnVerify"] = "0";
                            if (Request.QueryString["Notification_ID"] != null)
                            {
                                objUMS_BAL = new UMS_BAL();
                                int[] empIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                                objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, empIDs); //2 represents NotifyEmp had visited the page through notification link.
                                DataTable dt = DMS_Bal.DMS_NotificationFormRequestID(Convert.ToInt32(Request.QueryString["Notification_ID"]));
                                hdfVID.Value = dt.Rows[0]["FormRequestID"].ToString();
                                btnEditFormReturn_Click(null, null);
                            }
                            if (Request.QueryString["verify"] != null)
                            {
                                Session["FormReturnVerify"] = Request.QueryString["verify"].ToString();
                            }
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "change1", "changeHeaderText();", true);
                            if (dtTemp.Select("RoleID=30").Length > 0)//30-DocController
                            {
                                hdnAdminRole.Value = "30";
                                hdfRole.Value = "30";
                                //btnFormPrintReqMainListReport.Visible = true;
                            }
                            else if (dtTemp.Select("RoleID=32").Length > 0)//32-PrintReviewer
                            {
                                hdnAdminRole.Value = "32";
                                //btnFormPrintReqMainListReport.Visible = false;
                            }
                            else if (dtTemp.Select("RoleID=23").Length > 0)//23-Reader
                            {
                                hdnAdminRole.Value = "0";
                                //btnFormPrintReqMainListReport.Visible = false;
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                    else
                    {
                        ElectronicSign.buttonClick += new EventHandler(ElectronicSign_buttonClick);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", "getadmincolumn();", true);
                    }
                    
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRML_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRML_M1:" + strline + "  " + "Document Load failed.", "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        protected void btnRequestView_Click(object sender, EventArgs e)
        {
            btnRequestView.Enabled = false;
            try
            {
                docObjects.FormReqID = Convert.ToInt32(hdfVID.Value);
                DataSet DMS_DT = DMS_Bal.DMS_GetFormPrintReqDetails(docObjects);
                txt_RequestedBy.Text = DMS_DT.Tables[0].Rows[0]["RequestBy"].ToString();
                txt_ProjectNumber.Text = string.IsNullOrEmpty(DMS_DT.Tables[0].Rows[0]["ProjectNumber"].ToString()) ? "N/A" : DMS_DT.Tables[0].Rows[0]["ProjectNumber"].ToString();
                txt_NoofCopies.Text = DMS_DT.Tables[0].Rows[0]["NooFCopies"].ToString();
                txt_DocumentName.Text = DMS_DT.Tables[0].Rows[0]["FormName"].ToString();
                txt_Purpose.Text = DMS_DT.Tables[0].Rows[0]["RequestPurpose"].ToString();
                txt_ReviewedBy.Text = DMS_DT.Tables[0].Rows[0]["ReviewedBy"].ToString();
                txt_DocController.Text = DMS_DT.Tables[0].Rows[0]["DocController"].ToString();
                txt_FormNumber.Text = DMS_DT.Tables[0].Rows[0]["FormNumber"].ToString();
                txt_VersionNumber.Text = DMS_DT.Tables[0].Rows[0]["FormVersionNumber"].ToString();
                span_FileTitle.InnerText = "Request Number-" + DMS_DT.Tables[0].Rows[0]["RequestNo"].ToString();
                UpGridButtons.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_request').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRML_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRML_M2:" + strline + "  " + "Request Details viewing failed.", "error");
            }
            finally
            {
                btnRequestView.Enabled = true;
            }
        }
        protected void btnCommentsHistoryView_Click(object sender, EventArgs e)
        {
            btnCommentsHistoryView.Enabled = false;
            try
            {
                docObjects.FormReqID = Convert.ToInt32(hdfVID.Value);
                DataTable DMS_DT = DMS_Bal.DMS_GetFormPrintReqHistory(docObjects);
                gv_CommentHistory.DataSource = DMS_DT;
                ViewState["HistoryDataTable"] = DMS_DT;
                gv_CommentHistory.DataBind();
                UpGridButtons.Update();
                span4.InnerHtml = DMS_DT.Rows[0]["FormReqTitle"].ToString();
                upcomment.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myCommentHistory').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRML_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRML_M3:" + strline + "  " + "Form Request History loading failed.", "error");
            }
            finally
            {
                btnCommentsHistoryView.Enabled = true;
            }
        }
        protected void btnEditFormReturn_Click(object sender, EventArgs e)
        {
            btnEditFormReturn.Enabled = false;
            try
            {
                docObjects.FormReqID = Convert.ToInt32(hdfVID.Value);
                DataSet DMS_DT = DMS_Bal.DMS_GetFormPrintReqDetails(docObjects);
                txt_RequestedByReturn.Text = DMS_DT.Tables[0].Rows[0]["RequestBy"].ToString();
                txt_ProjectNumberReturn.Text = DMS_DT.Tables[0].Rows[0]["ProjectNumber"].ToString();
                txt_NoofCopiesReturn.Text = DMS_DT.Tables[0].Rows[0]["NooFCopies"].ToString();
                txt_DocumentNameReturn.Text = DMS_DT.Tables[0].Rows[0]["FormName"].ToString();
                txt_PurposeReturn.Text = DMS_DT.Tables[0].Rows[0]["RequestPurpose"].ToString();
                txt_ReviewedByReturn.Text = DMS_DT.Tables[0].Rows[0]["ReviewedBy"].ToString();
                txt_DocControllerReturn.Text = DMS_DT.Tables[0].Rows[0]["DocController"].ToString();
                DataTable dt = DMS_Bal.DMS_GetFormReqReturnDetails(docObjects);
                txt_NoofUsedCopies.Text = (Convert.ToInt32(DMS_DT.Tables[0].Rows[0]["NooFCopies"].ToString()) - Convert.ToInt32(dt.Rows[0]["ReturnedCopies"].ToString())).ToString();
                txt_CopyNumbers.Text = dt.Rows[0]["ReturnedCopyNumbers"].ToString();
                txt_Remarks.Text = dt.Rows[0]["Remarks"].ToString();
                txt_NoofReturnedCopies.Text = dt.Rows[0]["ReturnedCopies"].ToString();
                txt_FormNumberReturn.Text = DMS_DT.Tables[0].Rows[0]["FormNumber"].ToString();
                txt_VersionNumberReturn.Text = DMS_DT.Tables[0].Rows[0]["FormVersionNumber"].ToString();
                span2.InnerText = "Request Number-" + DMS_DT.Tables[0].Rows[0]["RequestNo"].ToString();
                txt_Comments.Value = "";
                UpdatePanel2.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myFormReturnEdit').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRML_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRML_M4:" + strline + "  " + "Form Return Details loading failed.", "error");
            }
            finally
            {
                btnEditFormReturn.Enabled = true;
            }
        }
        protected void gv_CommentHistory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string _fullcomments = (e.Row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox).Text;
                    LinkButton _lnkmorecomments = (e.Row.Cells[3].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (e.Row.Cells[3].FindControl("lbcommentsless") as LinkButton);
                    if (_fullcomments.Length > 70)
                    {
                        (e.Row.Cells[3].FindControl("lbl_shortComments") as Label).Visible = true;
                        System.Web.UI.WebControls.TextBox FullTbx = (e.Row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox);
                        (e.Row.Cells[3].FindControl("lbl_shortComments") as Label).Text = _fullcomments.Substring(0, 70) + "..";
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = true;

                        if (_fullcomments.Length > 100 && _fullcomments.Length <= 200)
                        {
                            FullTbx.Rows = 3;
                        }
                        else if (_fullcomments.Length > 200 && _fullcomments.Length <= 300)
                        {
                            FullTbx.Rows = 4;
                        }

                        FullTbx.Visible = false;
                    }
                    else
                    {
                        (e.Row.Cells[3].FindControl("lbl_shortComments") as Label).Visible = true;
                        (e.Row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox).Visible = false;
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRML_M5:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRML_M5:" + strline + "  " + "Comment History row data-bound failed.", "error");
            }
        }
        protected void gv_CommentHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                
                if (e.CommandName == "SM")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    LinkButton _lnkmorecomments = (row.Cells[3].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (row.Cells[3].FindControl("lbcommentsless") as LinkButton);
                    (row.Cells[3].FindControl("lbl_shortComments") as Label).Visible = false;
                    (row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox).Visible = true;
                    _lnklesscomments.Visible = true;
                    _lnkmorecomments.Visible = false;
                }
                else if (e.CommandName == "SL")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    LinkButton _lnkmorecomments = (row.Cells[3].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (row.Cells[3].FindControl("lbcommentsless") as LinkButton);
                    (row.Cells[3].FindControl("lbl_shortComments") as Label).Visible = true;
                    (row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox).Visible = false;
                    _lnklesscomments.Visible = false;
                    _lnkmorecomments.Visible = true;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRML_M6:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRML_M6:" + strline + "  " + "Comment History row command failed.", "error");
            }
        }
        protected void ElectronicSign_buttonClick(object sender, EventArgs e)
        {
            try
            {
                bool b = ElectronicSign.IsPasswordValid;
                if (b == true)
                {
                    if (hdnAction.Value == "Verify")
                    {
                        Verify();
                    }
                }
                if (b == false)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Password is Incorrect!", "error");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRML_M8:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRML_M8:" + strline + "  " + "Electronic Signature validation failed.", "error");
            }
        }
        public void Verify()
        {
            try
            {
                docObjects.FormReqID = Convert.ToInt32(hdfVID.Value);
                docObjects.DMSDocControllerID = Convert.ToInt32(hdnEmpID.Value);
                docObjects.Remarks = txt_Comments.Value.Trim();
                int DMS_ins = DMS_Bal.DMS_VerifyFormReturn(docObjects);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Forms with Form Request ID <b>" + hdfVID.Value + "</b> Return has been Verified Successfully.", "success", "ReloadVerifyPage()");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRML_M9:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRML_M9:" + strline + "  " + "Form Request Return Verify failed.", "error");
            }
        }
        protected void gv_CommentHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_CommentHistory.PageIndex = e.NewPageIndex;
                gv_CommentHistory.DataSource = (DataTable)(ViewState["HistoryDataTable"]);
                gv_CommentHistory.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRML_M10:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRML_M10:" + strline + "  " + "grid view page index changing failed.", "error");
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }

        //protected void btnFormPrintReqMainListReport_Click(object sender, EventArgs e)
        //{
        //    DMSService service = new DMSService();
        //    string EmpName = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString();
        //    string base64String = service.FromPrintReqMainListReport("N", EmpName);
        //    byte[] bytes = Convert.FromBase64String(base64String);
        //    System.IO.MemoryStream ms = new System.IO.MemoryStream(bytes);
        //    Response.Clear();
        //    Response.Buffer = true;
        //    Response.ContentType = "application/pdf";
        //    string _dwnFileName = "FormPrintMainList_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".pdf";
        //    Response.AddHeader("content-disposition", "attachment;filename=" + _dwnFileName);
        //    ms.WriteTo(Response.OutputStream);
        //    Response.End();
        //}
    }
}