﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="DocPrintReqMainList.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocPrintReqMainList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div id="divDocPrintReqMainList" style="display:none">
        <%--<asp:Button ID="btnDocPrintReqMainListReport" runat="server" class="float-right  report_btn" Text="Report" OnClick="btnDocPrintReqMainListReport_Click" style="margin-left:4px;" OnClientClick="DoubleClickHandleReport();"/>--%>
        <input type="button" id="btnDocPrintReqMainListReport" class="float-right  btn-signup_popup report_btn" onclick="DocPrintReqMainList();" value="Reports"/>
    </div>
    <div class="padding-none">
        <asp:Button ID="btnAdd" runat="server" class="float-right  btn-signup_popup" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
    </div>
    <div class=" col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left">
        <div class=" col-md-12 col-lg-12 col-12 col-sm-12 dms_outer_border float-left padding-none">
            <div class="col-md-12 col-lg-12 col-12 col-sm-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server" visible="false">
                <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left" id="divMainContainer" runat="server" visible="true">
                <div class=" col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-right ">
                    <div class="col-md-12 col-lg-12 col-12 col-sm-12  padding-none float-left">
                        <div class="grid_header col-md-12 col-lg-12 col-12 col-sm-12 float-left">Document Print Request Main List</div>
                        <div class="col-md-12 col-lg-12 col-12 col-sm-12 grid_panel_full border_top_none bottom float-left">
                            <div class="col-12 float-left top padding-none bottom">
                            <table id="dtDocList" class="display datatable_cust" cellspacing="0" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>DocRequestID</th>
                                        <th>Request Number</th>
                                        <th>Document Name</th>
                                        <th>Document Number</th>
                                        <th>Document Version </th>
                                        <th>Department</th>
                                        <th>Print Purpose</th>
                                        <th>Page Numbers</th>
                                        <th>Approved By</th>
                                        <th>Request By</th>
                                        <th>Requested Date</th>
                                        <th>Action Status</th>
                                        <th>View Request</th>
                                        <th>History</th>
                                    </tr>
                                </thead>
                            </table>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-------- Request VIEW-------------->
    <div id="myModal_request" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 98%">
            <!-- Modal content-->
            <div class="modal-content col-lg-12 col-md-12 padding-none">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <span id="span_FileTitle" runat="server"></span>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            
                        </div>
                        <div class="modal-body">
                            <div id="div_RequestedBy" class="col-md-5 col-lg-6 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="lblRequestedBy" runat="server" Text="Requested By" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_RequestedBy" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Requested By" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="divSoftCopy" class="form-group col-sm-12 col-lg-3 col-12 col-md-3 col-sm-12 float-left" style="padding-left: 50px">
                                <asp:Label ID="lblRadiobutton" runat="server" Text="Type of Request" CssClass="label_name"></asp:Label>
                                <asp:RadioButtonList runat="server" ID="RadioButtonList1" RepeatDirection="Horizontal" Enabled="false">
                                    <asp:ListItem Text="Download" Value="yes"></asp:ListItem>
                                    <asp:ListItem Text="Print" Value="no"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                            <div id="div_NoofCopies" class="col-md-2 col-lg-3 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="lblNoofCopies" runat="server" Text="No.of Copies" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_NoofCopies" runat="server" CssClass="form-control login_input_sign_up" TextMode="Number" Text="1" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_PageNo" class="col-md-2 col-lg-4 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="lblPageNo" runat="server" Text="Page Numbers" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_PageNo" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Page Numbers" ReadOnly="true"></asp:TextBox>
                            </div>
                             <div id="div_docNo" class="col-md-6 col-lg-5 col-12 col-sm-12 form-group float-left">
                                    <asp:Label ID="lblDocNum" runat="server" Text="Document Number" CssClass="label-style"></asp:Label>
                                    <asp:TextBox ID="txt_DocumentNumber" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true" ></asp:TextBox>
                                </div>
                                <div id="div_versionnoNo" class="col-md-6 col-lg-3 col-12 col-sm-12 form-group float-left ">
                                    <asp:Label ID="lblVersionNum" runat="server" Text="Version Number" CssClass="label-style"></asp:Label>
                                    <asp:TextBox ID="txt_VersionNumber" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true" ></asp:TextBox>
                                </div>
                            <div id="div_DocName" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label4" runat="server" Text="Document Name" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_DocumentName" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="Enter Document Name" AutoPostBack="true" Rows="2" MaxLength="500" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_Purpose" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="lblComments" runat="server" Text="Purpose" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_Purpose" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="Enter Purpose" Width="100%" Rows="2" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_ReviewedBy" class="col-md-6 col-lg-3 col-12 col-sm-12 form-group bottom float-left">
                                <asp:Label ID="Label6" runat="server" Text="Reviewed By" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_ReviewedBy" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_DocController" class="col-md-6 col-lg-3 col-12 col-sm-12 form-group bottom float-left">
                                <asp:Label ID="Label7" runat="server" Text="Document Controller" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_DocController" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_CopyType" class="col-md-6 col-lg-4 col-12 col-sm-12 form-group  float-left">
                                <asp:Label ID="Label2" runat="server" Text="Type of Copy" CssClass="label_name"></asp:Label>
                                <asp:TextBox ID="txt_CopyType" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_ValidDays" class="col-md-6 col-lg-2 col-12 col-sm-12 form-group   float-left">
                                <asp:Label ID="lblValidDays" runat="server" Text="Validity(In Days)" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_ValidDays" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="col-12 text-right">
                    <button data-dismiss="modal" class=" btn-cancel_popup">Close</button>
                                </div>
                </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!-------- End Request VIEW-------------->

    <!--------Comment History View-------------->
    <div id="myCommentHistory" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 85%">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel ID="upcomment" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-header">
                             <span id="span4" runat="server"></span>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                           
                        </div>
                        <div class="modal-body" style="max-height: 600px; overflow-y: auto">
                            <div class=" history_grid" style="overflow-y: auto;">
                                <asp:GridView ID="gv_CommentHistory" runat="server" CssClass="table table-hover" ClientIDMode="Static" EmptyDataText="No Records Found" AutoGenerateColumns="False" PagerSettings-Mode="NumericFirstLast" BackColor="White" BorderColor="#d2d2d2" BorderStyle="None" BorderWidth="1px" OnRowDataBound="gv_CommentHistory_RowDataBound" OnRowCommand="gv_CommentHistory_RowCommand" AllowPaging="true" PageSize="10" OnPageIndexChanging="gv_CommentHistory_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="User" ItemStyle-Width="12%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_GivenBy" runat="server" Text='<%# Eval("ActionBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" ItemStyle-Width="11%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Date" runat="server" Text='<%# Eval("ActionDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role" ItemStyle-Width="13%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Role" runat="server" Text='<%# Eval("CommentedRole") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="20%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Action" runat="server" Text='<%# Eval("Action") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments" ItemStyle-Width="29%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_shortComments" runat="server" Text='<%# Eval("Comments") %>'></asp:Label>
                                                <asp:TextBox ID="txt_Comments" runat="server" ReadOnly="true" Rows="2" TextMode="MultiLine" Text='<%# Eval("Comments") %>' Width="100%"></asp:TextBox>
                                                <asp:LinkButton ID="lbcommentsmore" runat="server" CommandArgument='<%# Eval("Comments")%>' CommandName="SM">Show More</asp:LinkButton>
                                                <asp:LinkButton ID="lbcommentsless" runat="server" CommandArgument='<%# Eval("Comments") %>' CommandName="SL">Show Less</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Assigned To">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_AssignedTo" runat="server" Text='<%# Eval("AssignedTo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle cssClass="gridpager"  HorizontalAlign="Right" />
                                </asp:GridView>
                            </div>
                        </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!--------End Comment History View-------------->

    <!--Doc Print Req Main List Fill View -->
        <div id="MyModelDocPrintReqMainView" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
            <div id="divModalSize" class="modal-dialog modal-lg" style="min-width:80%;">
                <!-- Modal content-->
                <div class="modal-content padding-none">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <div class="modal-header">
                        <h4 class="modal-title">Document Print Request List Report</h4>
                        <div class=" float-right padding-none">
                            <button type="button" class="close" data-dismiss="modal" >&times;</button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="embed-responsive embed-responsive-16by9" id="dvfileframeDoc">
                        </div>
                    </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>                   
                </div>
            </div>
        </div>
    <!--End-->

    <asp:HiddenField ID="hdfVID" runat="server" />
    <asp:HiddenField ID="hdnRole" runat="server" />
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
    <asp:UpdatePanel ID="UpGridButtons" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnRequestView" runat="server" Text="Submit" OnClick="btnRequestView_Click" Style="display: none" OnClientClick="showImg();"/>
            <asp:Button ID="btnCommentsHistoryView" runat="server" Text="Submit" Style="display: none" OnClick="btnCommentsHistoryView_Click" OnClientClick="showImg();"/>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--<script src='<%= Page.ResolveUrl("~/AppScripts/MainDocPDF_Viewer.js")%>'></script>--%>
    <script>
        $('#dtDocList thead tr').clone(true).appendTo('#dtDocList thead');
        $('#dtDocList thead tr:eq(1) th').each(function (i) {
            if (i < 13) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder=" Search " />');

                $('input', this).on('keyup change', function () {
                    if (oTable.column(i).search() !== this.value) {
                        oTable
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
                if (i == 1) {
                    $(this).html('');
                }
            }
            else {
                $(this).html('');
            }
        });
        $('#dtDocList').wrap('<div class="dataTables_scroll" />');
        var oTable = $('#dtDocList').DataTable({
            columns: [
                { 'data': 'RowNumber' },
                { 'data': 'DocReqID' },
                { 'data': 'RequestNumber' },
                { 'data': 'DocumentName' },
                { 'data': 'DocumentNumber' },
                {'data':'DocumentVersionNumber'},//5
                { 'data': 'DeptName' },//6
                { 'data': 'PrintPurpose' },//7
                { 'data': 'PageNumbers' },//8
                { 'data': 'ApprovedBy' },//9
                { 'data': 'RequestBy' },//10
                { 'data': 'RequestedDate' },//11
                { 'data': 'ActionName' },//12
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="view" title="View" data-toggle="modal" data-target="#myModal_request"  onclick=ViewRequest(' + o.DocReqID + ')></a>'; }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="summary_latest" title="History" data-toggle="modal" data-target="#myCommentHistory"  onclick=ViewCommentHistory(' + o.DocReqID + ')></a>'; }
                },
            ],
            "scrollX": true,
            "reponsive": true,
             "scrollY":'50vh',
        "scrollCollapse": true,
            
            "paging": true,
            "search": {
                "regex": true
            },
            "aoColumnDefs": [{ "targets": [0,1,3,7], "visible": false, "searchable": false },
            { "bSortable": false, "aTargets": [0] }, { "className": "dt-body-left", "targets": [4, 6, 7, 9,10,12] },
            ],
            "orderCellsTop": true,
            "order": [[1, "desc"]],
            sServerMethod: 'Post',
            "sAjaxSource": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetDocPrintReqMainList" )%>',
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#dtDocList").show();
                        UserSessionCheck();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
    </script>
    <script>
        function DoubleClickHandleReport() {
            custAlertMsg("Document Report is downloading please wait,Click OK after download is completed","info")
        }
        var RoleID = document.getElementById("<%=hdnRole.ClientID%>").value;
        if (RoleID==2)
        {
            $("#divDocPrintReqMainList").show();
        }
        function ViewRequest(V_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = V_ID;
            var btnRV = document.getElementById("<%=btnRequestView.ClientID %>");
            btnRV.click();
        }
        function ViewCommentHistory(PK_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = PK_ID;
            var btnCV = document.getElementById("<%=btnCommentsHistoryView.ClientID %>");
            btnCV.click();
        }
    </script>
    <script src='<%= Page.ResolveUrl("~/AppScripts/PDF_Viewer.min.js")%>'></script>
    <script src='<%= Page.ResolveUrl("~/AppScripts/MainDocPDF_Viewer.js")%>'></script>
    <script>
        function showImg() {
            ShowPleaseWait('show');
        }
        function hideImg() {
            ShowPleaseWait('hide');
        }
         function DocPrintReqMainList()
        {
         $.ajax({  
             type: "POST",  
             url: "WebServices/DMSService.asmx/DocPrintReqMainListReport",  
             contentType: "application/json; charset=utf-8",  
             dataType: "json",  
             data: JSON.stringify({IsJqueryDataList:"N"}),
             success: function (result) {
                 var message = JSON.stringify(result.d);
                 var parsePath = JSON.parse(message);
                 let finalPath = JSON.parse(parsePath);
                 if (finalPath=="NoData")
                 {
                     custAlertMsg("Records Not Found", "error");
                 }
                 else
                 {
                    var dvShow = document.getElementById("dvfileframeDoc");
                    AdminMainDocPrintFilePath = finalPath;//Store File Path
                     PdfMainDocViewerContentLoading(AdminMainDocPrintFilePath, dvShow);
                    $("#MyModelDocPrintReqMainView").modal("show");
                 }
             },  
             failure: function (response) {  
                 alert(response.d);  
             }  
         }); 
        }
    </script>
</asp:Content>
