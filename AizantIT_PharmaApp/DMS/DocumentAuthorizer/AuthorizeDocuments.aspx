﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="AuthorizeDocuments.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentAuthorizer.AuthorizeDocuments" %>

<%@ Register Src="~/UserControls/ElectronicSign.ascx" TagPrefix="uc1" TagName="ElectronicSign" %>
<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--  <link href="<%=ResolveUrl("~/AppCSS/DMS/DMS_StyleSheet1.css")%>" rel="stylesheet" />--%>
    <style>
        table tbody tr th {
            background-color: #07889a;
            color: #fff;
            text-align: center;
            height: 36px;
        }

        table tbody tr td {
            text-align: center;
            height: 30px;
        }

        .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
            background-color: #07889a !important;
        }

        .datalistcss {
            overflow-x: hidden;
            height: 180px;
            border-color: #CCCCCC;
            border-width: 1px;
            border-style: Solid;
            border-collapse: collapse;
            padding: 5px;
            border-radius: 5px;
        }

        .header_profile {
            background: #07889a;
            padding: 10px;
            color: #fff;
            font-size: 12pt;
            font-family: seguisb;
        }

        iframe {
            overflow-y: hidden;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:HiddenField ID="hdnGrdiReferesh" runat="server" Value="Link" />
    <asp:HiddenField ID="hdnNewDocTab" runat="server" Value="Create" />
    <div class="padding-none">
        <asp:Button ID="btnAdd" runat="server" class="float-right  btn-signup_popup" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
    </div>
    <div class="col-md-12 col-lg-12 col-12 col-sm-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
        <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
    </div>
    <div class="col-md-12 col-lg-12 col-12 col-sm-12 dms_outer_border padding-none float-left" id="divMainContainer" runat="server" visible="true">
        <div id="accordion_author" class="accordion qms_accordion">
            <div class="card mb-0">
                <div class="card-header header_col  accordion-toggle" data-toggle="collapse" href="#collapse1">

                    <a class="card-title">Document Details
                    </a>
                </div>


                <div id="collapse1" class="collapse show grid_panel_full border_top_none" data-parent="#accordion_author1">
                    <div class="card-body acc_boby float-left ">

                        <asp:UpdatePanel ID="UpMyDocFiles" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="col-lg-12 content_bg float-left">
                                    <div class="col-lg-6 top bottom padding-none float-left" style="border-right: 1px solid #d4d4d4;">
                                        <div class="col-sm-12 form-group float-left">
                                            <label for="inputEmail3" class=" padding-none profile_label col-sm-4 float-left">Request Type</label>
                                            <div class="col-sm-8 padding-none profile_defined float-left">
                                                <asp:TextBox ID="txt_requestType" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 form-group float-left top">
                                            <label for="inputEmail3" class=" padding-none profile_label col-sm-4 float-left">Document Type</label>
                                            <div class="col-sm-8 padding-none profile_defined float-left">
                                                <asp:TextBox ID="txt_documentType" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class=" col-sm-12 form-group float-left top">
                                            <label for="inputEmail3" class=" padding-none profile_label col-sm-4 float-left">Department</label>
                                            <div class="col-sm-8 padding-none profile_defined float-left">
                                                <asp:TextBox ID="txt_Department" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class=" col-sm-12 form-group float-left top">
                                            <label for="inputEmail3" class=" padding-none profile_label col-sm-4 float-left">Document Number</label>
                                            <div class="col-sm-8 padding-none profile_defined float-left">
                                                <asp:TextBox ID="txt_DocumentNumber" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class=" col-sm-12 form-group float-left top">
                                            <label for="inputEmail3" class=" padding-none profile_label col-sm-4 float-left">Document Name</label>
                                            <div class="col-sm-8 padding-none profile_defined float-left">
                                                <asp:TextBox ID="txt_DocumentName" runat="server" TextMode="MultiLine" CssClass="form-control" Height="80px" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 top padding-none float-left">
                                        <div class="col-sm-12 form-group float-left">
                                            <label for="inputEmail3" class=" padding-none profile_label col-sm-4 float-left">Version</label>
                                            <div class="col-sm-8 padding-none profile_defined float-left">
                                                <asp:TextBox ID="txt_VersionID" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 form-group float-left">
                                            <label for="inputEmail3" class="profile_label col-sm-4 float-left" style="padding-top: 11px; padding-left: 0px;">Privacy</label>
                                            <div class="col-sm-8  profile_defined float-left">
                                                <asp:RadioButtonList runat="server" ID="RadioButtonList1" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="Public" Value="yes"></asp:ListItem>
                                                    <asp:ListItem Text="Private" Value="no"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class=" col-sm-12 form-group float-left">
                                            <label for="inputEmail3" class=" padding-none profile_label col-sm-4 float-left">Author</label>
                                            <div class="col-sm-8 padding-none profile_defined float-left">
                                                <asp:TextBox ID="txt_Author" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 form-group float-left top" runat="server" id="div1">
                                            <label for="inputEmail3" class=" padding-none profile_label col-sm-4 float-left">Authorizer</label>
                                            <div class="col-sm-8 padding-none profile_defined float-left">
                                                <asp:TextBox ID="txt_Authorizer" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class=" col-sm-12 form-group float-left top">
                                            <label for="inputEmail3" runat="server" id="lblComments" class=" padding-none profile_label col-sm-4 float-left"></label>
                                            <div class="col-sm-8 padding-none profile_defined float-left">
                                                <asp:TextBox ID="txt_Comments" runat="server" TextMode="MultiLine" CssClass="form-control" Width="100%" Height="80px" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-12 col-sm-12 grid_border padding-none float-left">
                                        <asp:UpdatePanel ID="UPgvApprover" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                <div class="panel-heading_title col-md-12 col-lg-12 col-12 col-sm-12 float-left">
                                                    Selected Employees
                                                </div>
                                                <div class="col-md-12 col-lg-12 col-12 col-sm-12  float-left grid_panel_full bottom ">
                                                    <asp:GridView ID="gvApprover" CssClass="padding-right_div col-md-12 col-lg-12 col-12 col-sm-12 cal top bottom" runat="server" AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:BoundField DataField="Role" HeaderText="Role" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" Visible="true" />
                                                            <asp:BoundField DataField="EmpName" HeaderText="Employee" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" Visible="true" />
                                                            <asp:BoundField DataField="DeptCode" HeaderText="DeptCode" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" Visible="true" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>

                <div class="card-header header_col  accordion-toggle" data-toggle="collapse" href="#collapse2">

                    <a class="card-title">Authorize Document</a>
                </div>

                <div id="collapse2" class="collapse show grid_panel_full border_top_none" data-parent="#accordion_author2">
                    <div class="card-body acc_boby float-left ">
                        <div class=" col-lg-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-12 padding-none  ">
                                <ul class="nav nav-tabs tab_grid reviewer_edit_btn top">

                                    <li class=" nav-item" runat="server" id="liCreate" onclick="TabCreateClick();"><a class="Createtab nav-link active" data-toggle="tab" href="#home">Authorize</a></li>
                                    <li class=" nav-item" runat="server" id="liReferrals" onclick="TabReferralClick();"><a class="Refferaltab nav-link" data-toggle="tab" href="#menu1">Referrals</a></li>
                                    <li class="float-right nav-item  ml-auto">
                                        <div class="edit_botton_reviewer float-right">
                                            <asp:Button ID="Btn_ShowOriginal" runat="server" Text="Show Original" CssClass=" btn-track_edit" OnClick="Btn_ShowOriginal_Click" Visible="false" OnClientClick="showImg();" />
                                            <asp:Button ID="Btn_ShowModified" runat="server" Text="Show Modified" CssClass=" btn-track_edit" OnClick="Btn_ShowModified_Click" Visible="false" OnClientClick="showImg();" />
                                            <asp:Button ID="btn_Download" runat="server" Text="Download" CssClass="float-left  btn-signup_popup btn-download" OnClick="btn_Download_Click" OnClientClick="hideApproveDownload();" />
                                        </div>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div id="home" class="Createtabbody tab-pane fade in active show">
                                        <div class="top col-lg-12  float-left  padding-none" id="WordUpload" style="display: none;">
                                            <div class="padding-none float-left" id="lbl_Upload" runat="server"><b class="grid_icon_legend padding-right_div">Upload your word File:</b></div>
                                            <div class="col-lg-9 padding-none float-left">
                                                <asp:FileUpload ID="file_word" CssClass="btn-file-upload" value="Choose File" runat="server" />
                                            </div>
                                            <asp:UpdatePanel ID="upUpload" runat="server" UpdateMode="Conditional" style="text-align: right">
                                                <ContentTemplate>
                                                    <asp:Button ID="Btn_Upload" runat="server" Text="Upload" CssClass=" btn-signup_popup" ValidationGroup="edit" OnClientClick="return Required();" OnClick="Btn_Upload_Click" />
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="Btn_Upload" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </div>
                                        <div class="col-lg-12 float-left padding-none">
                                            <div class="bottom col-lg-6 float-left padding-none" id="divinfotext" style="display: none">
                                                <span class="note_lbl">NOTE:</span>
                                                <asp:Label ID="lbl_InfoText" class="dms_info_text" runat="server" Text="Enable track changes option in word document before uploading"></asp:Label>
                                            </div>
                                            <div class="col-sm-6 bottom float-right padding-none" style="font-size: 15px; text-align: right; margin-top: 2px;">
                                                <span class="glyphicon glyphicon-time"></span>&nbsp;
                                                    <span id="Timer"></span>
                                            </div>
                                        </div>
                                        <div class="listPDF_Viewer" id="divPDF_Viewer">
                                            <noscript>
                                                Please Enable JavaScript.
                                            </noscript>
                                        </div>
                                        <label for="inputEmail3" class="form-group top padding-none col-sm-12">Remarks :<span class="smallred_label">*</span></label>
                                        <div style="padding-bottom: 5px;">
                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                <ContentTemplate>
                                                    <textarea id="txt_Reject" text='<%# Eval("Comments") %>' runat="server" class="form-control" rows="8" maxlength="3000"></textarea>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-12 col-sm-12  float-left padding-none top text-right bottom">
                                            <input type="button" id="Btn_Approve" runat="server" data-dismiss="modal" class=" btn-signup_popup" onclick="commentRequriedApprove();" value="Approve" />
                                            <input type="button" data-dismiss="modal" class=" btn-revert_popup" onclick="return commentRequriedRevert();" value="Revert" />
                                            <input type="button" id="btn_close" runat="server" data-dismiss="modal" class="  btn-cancel_popup" onclick="return CancelReset();" value="Close" />
                                            <input type="button" class="float-left  btn-signup_popup savecomments" onclick="return ConfirmAlert();" value="Save Remarks" />
                                        </div>
                                    </div>
                                    <div id="menu1" class="Refferaltabbody tab-pane fade ">
                                        <div class="col-lg-12">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-12 top padding-none">
                                                <ul class="nav nav-tabs tab_grid">
                                                    <li class=" nav-item"><a class="Linktab nav-link active" data-toggle="tab" href="#home1">Link Referrals</a></li>
                                                    <li class=" nav-item"><a class="internaltab nav-link" data-toggle="tab" href="#menu12">Internal Documents</a></li>
                                                    <li class=" nav-item"><a class="Externaltab nav-link" data-toggle="tab" href="#menu23">External Documents</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-12 padding-none tab-content">
                                                <div id="home1" class="Linktabbody tab-pane fade in active top show">
                                                    <div class="col-sm-12">
                                                        <br />
                                                        <div class="col-sm-12">
                                                            <asp:Literal ID="txtReferrences" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="menu12" class="internaltabbody tab-pane fade top">
                                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                        <ContentTemplate>
                                                            <div class="row">
                                                                <div class="col-sm-12 top">
                                                                    <asp:GridView ID="gvAssignedDocuments" runat="server" class="display" AutoGenerateColumns="false" Width="100%" EmptyDataText="No files uploaded" BorderColor="#07889a">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Document Number">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lbl_DocumentNumber" runat="server" Text='<%# Eval("DocumentNumber") %>'></asp:Label>
                                                                                    <asp:Label ID="lbl_DocumentID" runat="server" Text='<%# Eval("VersionID") %>' Visible="false"></asp:Label>
                                                                                    <asp:Label ID="lbl_DeptID" runat="server" Text='<%# Eval("DeptID") %>' Visible="false"></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Document Name">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lbl_DocumentName" runat="server" Text='<%# Eval("DocumentName") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="View Doc">
                                                                                <ItemTemplate>
                                                                                    <asp:UpdatePanel ID="jhj" runat="server">
                                                                                        <ContentTemplate>
                                                                                            <asp:LinkButton ID="lnk_ViewInternal" runat="server" class="view" OnClick="lnk_ViewInternal_Click" OnClientClick="showImg();"></asp:LinkButton>
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                                <div class="Externaltabbody tab-pane fade top" id="menu23">
                                                    <div class="col-sm-12 padding-none top bottom">
                                                        <div class="col-sm-12  padding-none">
                                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                                                <ContentTemplate>
                                                                    <asp:GridView ID="gvExternalFiles" runat="server" AutoGenerateColumns="false" EmptyDataText="No files uploaded" Width="100%" BorderColor="#07889a">
                                                                        <Columns>
                                                                            <asp:BoundField DataField="FileName" HeaderText="File Name" />
                                                                            <asp:TemplateField HeaderText="View Doc">
                                                                                <ItemTemplate>
                                                                                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                                                                        <ContentTemplate>
                                                                                            <asp:LinkButton ID="lnkDownload" class="view" CommandArgument='<%# Eval("FileName") +"@$"+Eval("Ref_FileID")+"@$"+Eval("pkid")%>' runat="server" OnClick="lnkDownload_Click" OnClientClick="showImg();"></asp:LinkButton>
                                                                                            <asp:Label ID="lbl_isSavedFile" runat="server" Text='<%# Eval("isSavedFile") %>' Visible="false"></asp:Label>
                                                                                            <asp:Label ID="lbl_Ref_FileID" runat="server" Text='<%# Eval("Ref_FileID") %>' Visible="false"></asp:Label>
                                                                                            <asp:Label ID="lbl_pkid" runat="server" Text='<%# Eval("pkid") %>' Visible="false"></asp:Label>
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-lg-12 col-12 col-sm-12 top padding-none">
                                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                        <ContentTemplate>
                                                            <!--add html button-->
                                                            <button type="button" data-dismiss="modal" runat="server" id="btn_Refferals_Cancel1" class="float-right  btn-cancel_popup top bottom" onclick="return accordion_tab();">Cancel</button>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sticky-popup open" id="divcomments" runat="server" visible="false">
        <div class="popup-wrap">
            <div class="popup-header">
                <span class="popup-title">Comments<div class="popup-image"></div>
                </span>
            </div>
            <div class="popup-content">
                <div class="popup-content-pad">
                    <div class="popup-form" id="popup-1560">
                        <div class="popup-form--part-title-font-weight-normal" id="popup-form-1560">
                            <div class="popup-form__part popup-part popup-part--multi_line_text popup-part--width-full popup-part--label-above"
                                id="popup-1560_multi_line_text_4-part" data-popup-type="multi_line_text">
                                <div class="popup-part-wrap">
                                    <div class="popup-part__el">
                                        <div class="col-md-12 col-lg-12 col-12 col-sm-12 form-group top bottom">
                                            <div class="col-md-12 col-lg-12 col-12 col-sm-12 bottom padding-none top" style="border: 1px solid #2e2e2e; max-height: 338px; min-height: 250px; overflow-y: auto;" id="divgvcomments">
                                                loading..
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--------Progress Bar VIEW-------------->
    <div id="modalProgressbar" data-backdrop="static" class="modal department fade" role="dialog">
        <div class=" modal-dialog " style="min-width: 47%">
            <!-- Modal content-->
            <div class="modal-content ">
                <div class="modal-header">
                    <span id="span1" runat="server">Progress Bar</span>
                </div>
                <div class="modal-body">
                    <div class="col-md-12 col-lg-12 col-12 col-sm-12 float-left" style="background-color: #fff">

                        <img alt="" src="<%=ResolveUrl("~/Images/TMS_Images/please_wait.gif")%>" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--------End Progress Bar VIEW-------------->

    <!--------Internal Refferal VIEW-------------->
    <div id="moppnlInternalView" class="modal department" role="dialog">
        <div class="modal-dialog modal-lg " style="min-width: 86%; height: 500px;">
            <!-- Modal content-->
            <div class="modal-content ">
                <div class="modal-header">

                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <span id="span4" runat="server"></span>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <button type="button" class="close" data-dismiss="modal" onclick="ShowRefferal();" style="width: 50px">&times;</button>
                </div>
                <div class="modal-body">
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                    <div id="InternalRefDiv">
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="Button11" runat="server" Text="Close" CssClass=" btn-success" BackColor="#4d85a4" Style="display: none;" />
                </div>
            </div>
        </div>
    </div>
    <!--------End Internal Refferal VIEW-------------->

    <!--------External Refferal VIEW-------------->
    <div id="mpop_FileViewer" class="modal department" role="dialog">
        <div class="modal-dialog modal-lg " style="min-width: 86%">
            <!-- Modal content-->
            <div class="modal-content ">
                <div class="modal-header">

                    <span id="span5" runat="server">External Referral</span>
                    <button type="button" class="close" data-dismiss="modal" onclick="ShowRefferal();">&times;</button>
                </div>
                <div class="col-md-12 col-lg-12 col-12 col-sm-12" style="background-color: #fff">
                    <div class="col-sm-12">
                        <asp:UpdatePanel ID="UpdatePanel10" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="ExternalRefDiv" runat="server">
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--------End External Refferal VIEW-------------->
    <uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />
    <uc1:ElectronicSign runat="server" ID="ElectronicSign" />
    <asp:HiddenField ID="hdfPKID" runat="server" />
    <asp:HiddenField ID="hdfVID" runat="server" />
    <asp:HiddenField ID="hdfDocumentID" runat="server" />
    <asp:HiddenField ID="hdfPID" runat="server" />
    <asp:HiddenField ID="hdnAction" runat="server" />
    <asp:HiddenField ID="hdfAuChange" runat="server" Value="0" />
    <asp:HiddenField ID="hdfAuName" runat="server" />
    <asp:HiddenField ID="hdfTraining" runat="server" />
    <asp:HiddenField runat="server" ID="hdnSeconds" Value="0" />
    <asp:HiddenField runat="server" ID="hdnHours" Value="0" />
    <asp:HiddenField runat="server" ID="hdnMinutes" Value="0" />
    <asp:HiddenField ID="hdfFromEmpID" runat="server" />
    <asp:HiddenField ID="hdfToRoleID" runat="server" />
    <asp:HiddenField ID="hdfDocStatus" runat="server" />
    <asp:HiddenField ID="hdfViewType" runat="server" />
    <asp:HiddenField ID="hdfViewEmpID" runat="server" />
    <asp:HiddenField ID="hdfViewRoleID" runat="server" />
    <input type="hidden" id="ISupload1" name="ISupload1" value="0" />
    <input type="hidden" id="UpFilename" name="UpFilename" value="" />
    <!--Ml Feature Checking-->
    <script src='<%=ResolveUrl("~/AppScripts/ML_FeaturesChecking.js")%>'></script>
    <script>
        GetMLFeatureChecking(2);//Passing Paramenter Module ID
    </script>
    <script>
        var startTimer;
        function AuthorizerSopReadDuration(type) {
            if (type == "start") {
                var sec = parseInt(document.getElementById("<%=hdnSeconds.ClientID%>").value);
                var h = parseInt(document.getElementById("<%=hdnHours.ClientID%>").value);
                var min = parseInt(document.getElementById("<%=hdnMinutes.ClientID%>").value);
                startTimer = setInterval(function () {
                    var seconds = sec + 1;
                    var minutes = min;
                    var hours = h;
                    SaveCurrentDuration(seconds, minutes, hours);
                    if (seconds >= 60) {
                        var minutes = min + 1;
                        seconds = 0;
                    }
                    if (minutes >= 60) {
                        var hours = h + 1;
                        minutes = 0;
                    }
                    sec = seconds;
                    min = minutes;
                    h = hours;
                    document.getElementById("Timer").innerHTML = hours + "h " + minutes + "m " + seconds + "s ";
                }, 1000);
            }
            else {
                clearInterval(startTimer);
            }
        }
        function OpenReadSopModal() {
            AuthorizerSopReadDuration('start');
        }
        function CloseReadSopModal() {
            AuthorizerSopReadDuration('stop');
        }
        function SaveCurrentDuration(seconds, minutes, hours) {
            var ID = document.getElementById('<%=hdfPKID.ClientID%>').value;
            var hours = hours;
            var min = minutes;
            var sec = seconds;
            document.getElementById('<%=hdnHours.ClientID%>').value = hours;
            document.getElementById('<%=hdnMinutes.ClientID%>').value = minutes;
            document.getElementById('<%=hdnSeconds.ClientID%>').value = seconds;
            var objStr = { "ID": ID, "Hours": hours, "Minutes": min, "Seconds": sec };
            var jsonData = JSON.stringify(objStr);
            $.ajax({
                type: "POST",
                url: "AuthorizeDocuments.aspx/InsertCurrentDuration",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function () {
                    UserSessionCheck();
                },
                failure: function () {
                    custAlertMsg("Internal Server Error Occurred. Sop Read Duration not Recorded.", "error");
                }
            });
        }
    </script>
    <script>
        function openElectronicSignModal() {
            openUC_ElectronicSign();
        }
    </script>
    <script type="text/javascript">
        function commentRequriedApprove() {
            var comment = document.getElementById("<%=txt_Reject.ClientID%>").value;
            var training = document.getElementById("<%=hdfTraining.ClientID%>").value;
            errors = [];
            if (comment.trim() == "") {
                errors.push("Enter Remarks.");
            }
            if (training != "0") {
                errors.push("Training's going on for this document.")
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                ApproveAction();
                return true;
            }
        }
        function commentRequriedRevert() {
            var comment = document.getElementById("<%=txt_Reject.ClientID%>").value;
            errors = [];
            if (comment.trim() == "") {
                errors.push("Enter Remarks.");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                RevertAction();
                return true;
            }
        }
        function RevertAction() {
            document.getElementById("<%=hdnAction.ClientID%>").value = "Revert";
            var f2name = '<%=this.ViewState["PDFPhysicalName"].ToString()%>';
            if (f2name == "") {
                custAlertMsg('Do you want to revert without uploading document?', 'confirm', true);
            }
            else {
                openElectronicSignModal();
            }
        }
        function ApproveAction() {
            document.getElementById("<%=hdnAction.ClientID%>").value = "Approve";
            openElectronicSignModal();
        }
        function CancelReset() {
            CloseReadSopModal();
            document.getElementById("<%=txt_Reject.ClientID%>").value = "";
            window.open("<%=ResolveUrl("~/DMS/DocumentAuthorizer/Authorize_Documents.aspx")%>", "_self");
        }
        function CancelReset1() {
            reload();
            window.open("<%=ResolveUrl("~/DMS/DocumentAuthorizer/AuthorizeDocuments.aspx?VersionID=" + hdfPKID.Value + "&PID=" + hdfPID.Value + "&Training=" + hdfTraining.Value)%>", "_self");
        }
        function CancelResetWithML() {
            CloseReadSopModal();
            document.getElementById("<%=txt_Reject.ClientID%>").value = "";
            window.open("<%=ResolveUrl("~/DMS/DocumentAuthorizer/Authorize_Documents.aspx")%>", "_self");
            //Start DMS_ML
            if (ML_FeatureIDs.indexOf(1) != -1) {
                var DOCID = $('#<%=hdfDocumentID.ClientID%>').val();
                $.get(AGA_ML_URL + "/Document", { "id": DOCID });
            }
            //End DMS_ML
        }
        function accordion_tab() {
            $('#<%=hdnNewDocTab.ClientID%>').val('Create');
              $('#<%=hdnGrdiReferesh.ClientID%>').val('Link');
              ReferalsTabState();
              var SUp1 = document.getElementById("ISupload1").value;
              var FN1 = document.getElementById("UpFilename").value;
              ViewDocument('#divPDF_Viewer', SUp1, FN1);
        }
        function ConfirmAlert() {
            CloseReadSopModal();
            var comment = document.getElementById("<%=txt_Reject.ClientID%>").value;
              errors = [];
              if (comment.trim() != "") {
                  document.getElementById("<%=hdnAction.ClientID%>").value = "Save Comments";
                  custAlertMsg('Do You Want To Save Remarks?', 'confirm', true);
              }
              if (comment.trim() == "") {
                  errors.push("Enter Remarks.");
              }
              if (errors.length > 0) {
                  custAlertMsg(errors.join("<br/>"), "error");
                  return false;
              }
              else {
                  return true;
              }
          }
          function ConfirmSaveAlert() {
              CloseReadSopModal();
              var comment = document.getElementById("<%=txt_Reject.ClientID%>").value;
              errors = [];
              if (comment.trim() == "") {
                  errors.push("Enter Remarks.");
              }
              if (errors.length > 0) {
                  custAlertMsg(errors.join("<br/>"), "error");
                  return false;
              }
              else {
                  document.getElementById("<%=hdnAction.ClientID%>").value = "Save";
                  custAlertMsg('Do You Want To Save Changes?', 'confirm', true);
                  return true;
              }
          }
          function HidePopup() {
              $('#usES_Modal').modal('hide');
          }
    </script>
    <script>
          $(document).ready(function () {
              ReferalsTabState();
              TabRefClick();
          });
          var prm = Sys.WebForms.PageRequestManager.getInstance();
          prm.add_endRequest(function () {
              ReferalsTabState();
              TabRefClick();
          });
          function ReferalsTabState() {
              if ($('#<%=hdnNewDocTab.ClientID%>').val() == 'Create') {
                  $('.internaltab').removeClass('active');
                  $('.Externaltab').removeClass('active');
                  $('.Createtab').addClass('active');
                  $('.Linktab').removeClass('active');
                  $('.Refferaltab').removeClass('active');
                  $('.Externaltabbody').removeClass('active');
                  $('.Externaltabbody').removeClass('show');
                  $('.internaltabbody').removeClass('active');
                  $('.internaltabbody').removeClass('show');
                  $('.Createtabbody').addClass('active');
                  $('.Createtabbody').addClass('show');
                  $('.Linktabbody').removeClass('active');
                  $('.Linktabbody').removeClass('show');
                  $('.Refferaltabbody').removeClass('active');
                  $('.Refferaltabbody').removeClass('show');
                  $('.edit_botton_reviewer').show();
              }
              if ($('#<%=hdnGrdiReferesh.ClientID%>').val() == '' && $('#<%=hdnNewDocTab.ClientID%>').val() == 'Refferal') {
                  $('.internaltab').removeClass('active');
                  $('.Externaltab').removeClass('active');
                  $('.Createtab').removeClass('active');
                  $('.Linktab').addClass('active');
                  $('.Refferaltab').addClass('active');
                  $('.Externaltabbody').removeClass('active');
                  $('.Externaltabbody').removeClass('show');
                  $('.internaltabbody').removeClass('active');
                  $('.internaltabbody').removeClass('show');
                  $('.Createtabbody').removeClass('active');
                  $('.Createtabbody').removeClass('show');
                  $('.Linktabbody').addClass('active');
                  $('.Linktabbody').addClass('show');
                  $('.Refferaltabbody').addClass('active');
                  $('.Refferaltabbody').addClass('show');
              }
              if ($('#<%=hdnGrdiReferesh.ClientID%>').val() == 'Link' && $('#<%=hdnNewDocTab.ClientID%>').val() == 'Refferal') {
                  $('.internaltab').removeClass('active');
                  $('.Externaltab').removeClass('active');
                  $('.Createtab').removeClass('active');
                  $('.Linktab').addClass('active');
                  $('.Refferaltab').addClass('active');
                  $('.Externaltabbody').removeClass('active');
                  $('.Externaltabbody').removeClass('show');
                  $('.internaltabbody').removeClass('active');
                  $('.internaltabbody').removeClass('show');
                  $('.Createtabbody').removeClass('active');
                  $('.Createtabbody').removeClass('show');
                  $('.Linktabbody').addClass('active');
                  $('.Linktabbody').addClass('show');
                  $('.Refferaltabbody').addClass('active');
                  $('.Refferaltabbody').addClass('show');
              }
              if ($('#<%=hdnGrdiReferesh.ClientID%>').val() == 'Internal' && $('#<%=hdnNewDocTab.ClientID%>').val() == 'Refferal') {
                  $('.Externaltab').removeClass('active');
                  $('.Linktab').removeClass('active');
                  $('.Createtab').removeClass('active');
                  $('.internaltab').addClass('active');
                  $('.Refferaltab').addClass('active');
                  $('.Externaltabbody').removeClass('active');
                  $('.Externaltabbody').removeClass('show');
                  $('.Linktabbody').removeClass('active');
                  $('.Linktabbody').removeClass('show');
                  $('.Createtabbody').removeClass('active');
                  $('.Createtabbody').removeClass('show');
                  $('.internaltabbody').addClass('active');
                  $('.internaltabbody').addClass('show');
                  $('.Refferaltabbody').addClass('active');
                  $('.Refferaltabbody').addClass('show');
              }
              if ($('#<%=hdnGrdiReferesh.ClientID%>').val() == 'External' && $('#<%=hdnNewDocTab.ClientID%>').val() == 'Refferal') {
                  $('.internaltab').removeClass('active');
                  $('.Linktab').removeClass('active');
                  $('.Createtab').removeClass('active');
                  $('.Externaltab').addClass('active');
                  $('.Refferaltab').addClass('active');
                  $('.internaltabbody').removeClass('active');
                  $('.internaltabbody').removeClass('show');
                  $('.Linktabbody').removeClass('active');
                  $('.Linktabbody').removeClass('show');
                  $('.Createtabbody').removeClass('active');
                  $('.Createtabbody').removeClass('show');
                  $('.Externaltabbody').addClass('active');
                  $('.Externaltabbody').addClass('show');
                  $('.Refferaltabbody').addClass('active');
                  $('.Refferaltabbody').addClass('show');
              }
          }
          function TabReferralClick() {
              $('#<%=hdnNewDocTab.ClientID%>').val('Refferal');
              $('.edit_botton_reviewer').hide();
              ReferalsTabState();
          }
          function TabCreateClick() {
              $('#<%=hdnNewDocTab.ClientID%>').val('Create');
              ReferalsTabState();
              var SUp1 = document.getElementById("ISupload1").value;
              var FN1 = document.getElementById("UpFilename").value;
              ViewDocument('#divPDF_Viewer', SUp1, FN1);
          }
    </script>
    <script>
          function TabRefClick() {
              $(".Linktab").click(function () {
                  $('#<%=hdnGrdiReferesh.ClientID%>').val('Link');
              });
              $(".internaltab").click(function () {
                  $('#<%=hdnGrdiReferesh.ClientID%>').val('Internal');
              });
              $(".Externaltab").click(function () {
                  $('#<%=hdnGrdiReferesh.ClientID%>').val('External');
              });
              $(".Createtab").click(function () {
                  $('#<%=hdnNewDocTab.ClientID%>').val('Create');
              });
              $(".Refferaltab").click(function () {
                  $('#<%=hdnNewDocTab.ClientID%>').val('Refferal');
              });
          }
          function reload() {
              $('#collapse1').collapse();
          }
    </script>
    <script>
          function Onkeydown() {
              var Auname = document.getElementById("<%=hdfAuName.ClientID%>").value;
              var Auchange = document.getElementById("<%=hdfAuChange.ClientID%>").value;
              var selection = au.core.selection.getLastSelectedInterval();
              au.commands.changeFontForeColor.execute("#E033FF");
              if (Auchange == "0") {
                  document.getElementById("<%=hdfAuChange.ClientID%>").value = "1";
              }
              else if (Auchange == "1") {

              }
          }
    </script>
    <script>
          function showsave() {
              $('.save').show();
              $('.savecomments').hide();
          }
          function showsavecomments() {
              $('.save').hide();
              $('.savecomments').show();
          }
          function showgvComments() {
              $('#divgvcomments').show();
          }
    </script>
    <script>
          $(function () {
              if (/*@cc_on!@*/true) {
                  var ieclass = 'ie' + document.documentMode;
                  $(".popup-wrap").addClass(ieclass);
              }
              $(".sticky-popup").addClass('sticky-popup-right');
              var contwidth = $(".popup-content").outerWidth() + 2;
              $(".sticky-popup").css("right", 0);
              $(window).scroll(function () {
                  var height = $(window).scrollTop();
                  if (height > 50) {
                      $(".sticky-popup").css("visibility", "visible");
                  } else {
                      $(".sticky-popup").css("visibility", "hidden");
                  }
              });

              $(".popup-header").click(function () {
                  if ($('.sticky-popup').hasClass("open")) {
                      $('.sticky-popup').removeClass("open");
                      $(".sticky-popup").css("right", "-" + contwidth + "px");
                  }
                  else {
                      $('.sticky-popup').addClass("open");
                      $(".sticky-popup").css("right", 0);
                  }
              });
              $("#closeButton").click(function () {

                  $('.sticky-popup').removeClass("open");
                  $(".sticky-popup").css("right", 0);
              });
          });
          function func1() {

          }
    </script>
    <script>
            function getEveryoneCommentsData() {
                getEveryoneComments('<%= Page.ResolveUrl("~//DMS//WebServices//DMSService.asmx//GetEveryoneComments" )%>', '<%=hdfPKID.Value%>');
            }
            $(document).ready(function () {
                var jsPath = '<%=ResolveUrl("~/AppScripts/ReviewCommentsDiv.js")%>';
                var security = document.createElement("script");
                security.type = "text/javascript";
                security.src = jsPath;
                $("#divgvcomments").append(security);
                getEveryoneCommentsData();
            });
    </script>
    <script>           
            function ShowExternalFile() {
                $('#mpop_FileViewer').modal({ show: true, backdrop: 'static', keyboard: false });
            }
            function ShowInternalFile() {
                $('#moppnlInternalView').modal({ show: true, backdrop: 'static', keyboard: false });
            }
            function Required() {
                var fullPath = document.getElementById("<%=file_word.ClientID%>").value;
                var filename = "";
                if (fullPath) {
                    var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                    var filename = fullPath.substring(startIndex);
                    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                        filename = filename.substring(1);
                    }
                }
                errors = [];
                if (filename == "") {
                    errors.push("Please Choose a file to upload.");
                }
                if (filename.length > 95) {
                    errors.push("Uploaded file length should not be greater than 95 characters");
                }
                if (filename.includes(",") || filename.includes("/") || filename.includes(":") || filename.includes("*") || filename.includes("?") ||
                    filename.includes("<") || filename.includes(">") || filename.includes("|") || filename.includes("\"") || filename.includes("\\") ||
                    filename.includes(";")) {
                    errors.push("Uploaded file name can contain only the following special characters !@#()-_'+=$^%&~` ");
                }
                if (errors.length > 0) {
                    custAlertMsg(errors.join("<br/>"), "error");
                    return false;
                }
                else {
                    $('#modalProgressbar').modal({ show: true });
                    return true;
                }
            }
            function CloseBrowser() {
                window.close();
            }
    </script>
    <script src='<%= Page.ResolveUrl("~/AppScripts/PDF_Viewer.min.js")%>'></script>
    <script>
            function ViewDocument(DivID, isupload, Filename1) {
                if (isupload == "1") {
                    PdfFormViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_Of_File")%>', Filename1, DivID);
                    document.getElementById("ISupload1").value = "1";
                    document.getElementById("UpFilename").value = Filename1;
                }
                else {
                    PdfViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>', $('#<%=hdfViewType.ClientID%>').val(), $('#<%=hdfPKID.ClientID%>').val(), $('#<%=hdfViewEmpID.ClientID%>').val(), $('#<%=hdfViewRoleID.ClientID%>').val(), DivID, "func1");
                    document.getElementById("ISupload1").value = "2";
                    document.getElementById("UpFilename").value = "";
                }
            }
            function ViewInternalDocument(DivID, VID) {
                PdfViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>', "0", VID, "0", "0", DivID, "func1");
            }
        function hideApproveDownload() {
            var DocNumber = document.getElementById("<%=txt_DocumentNumber.ClientID%>").value;
            var DocVersion = document.getElementById("<%=txt_VersionID.ClientID%>").value;
            custAlertMsg(" Document <b>"+DocNumber+"-"+DocVersion+"</b> is downloading please wait,Click OK after download is completed", "info");

                $('#<%=btn_Download.ClientID%>').hide();
                $('#<%=Btn_Approve.ClientID%>').hide();
                $('#WordUpload').show();
                $('#divinfotext').show();
            }
            function showApproveDownload() {
                $('#<%=btn_Download.ClientID%>').hide();
                $('#<%=Btn_Approve.ClientID%>').hide();
                $('#WordUpload').show();
                $('#divinfotext').show();
            }
    </script>
    <script>
            function showImg() {
                ShowPleaseWait('show');
            }
            function hideImg() {
                ShowPleaseWait('hide');
            }
    </script>
</asp:Content>
