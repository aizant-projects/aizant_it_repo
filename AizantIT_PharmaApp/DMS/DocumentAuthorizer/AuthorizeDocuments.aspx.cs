﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_BusinessLayer;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Text;
using UMS_BO;
using System.Data.SqlClient;

namespace AizantIT_PharmaApp.DMS.DocumentAuthorizer
{
    public partial class AuthorizeDocuments : System.Web.UI.Page
    {
        private Word2PDFConverter.RemoteConverter converter;
        private string storefolder = HttpContext.Current.Server.MapPath(@"~/Scripts/pdf.js/web/");
        UMS_BAL objUMS_BAL = new UMS_BAL();
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        ReviewObjects reviewObjects = new ReviewObjects();
        DocObjects docObjects = new DocObjects();
        DocObjects docObjects1 = new DocObjects();
        DocUploadBO objectDocUploadBO;
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            ConfirmCustomAlert.buttonClick += new EventHandler(ConfirmCustomAlert_buttonClick);
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    converter = (Word2PDFConverter.RemoteConverter)Activator.GetObject(typeof(Word2PDFConverter.RemoteConverter), "http://localhost:" + System.Configuration.ConfigurationManager.AppSettings["Word2PdfPort"].ToString() + "/RemoteConverter");
                    if (!IsPostBack)
                    {
                        Session["vwdocObjectsAz"] = null;
                        Session["vwreviewObjectsAz"] = null;
                        Session["vwdocObjects1Az"] = null;
                        ViewState["PDFPhysicalName"] = "";
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        hdfAuName.Value = "Changed By " + (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString();
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=13").Length > 0)//13-Authorizer
                            {
                                if (Request.QueryString["Notification_ID"] != null)
                                {
                                    objUMS_BAL = new UMS_BAL();
                                    int[] empIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, empIDs); //2 represents NotifyEmp had visited the page through notification link.                               
                                }
                                InitializeThePage();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                    else
                    {
                        ElectronicSign.buttonClick += new EventHandler(ElectronicSign_buttonClick);
                        getTime();
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop", "OpenReadSopModal();", true);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "reload();", true);
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("AUD_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "AUD_M1:" + strline + "  " + "Document Load failed.", "error");
            }
        }
        protected void ElectronicSign_buttonClick(object sender, EventArgs e)
        {
            try
            {
                bool b = ElectronicSign.IsPasswordValid;
                if (b == true)
                {
                    if (hdnAction.Value == "Approve")
                    {
                        Approve();
                    }
                    else if (hdnAction.Value == "Revert" || ViewState["Action"].ToString() == "Revert")
                    {
                        Revert();
                    }
                }
                if (b == false)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Password is Incorrect!", "error");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("AUD_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "AUD_M2:" + strline + "  " + "Electronic Signature validation failed.", "error");
            }
        }
        protected void ConfirmCustomAlert_buttonClick(object sender, EventArgs e)
        {
            try
            {
                if (hdnAction.Value == "Save Comments")
                {
                    reviewObjects.Id = Convert.ToInt32(hdfPKID.Value);
                    reviewObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    reviewObjects.DocStatus = "Au";
                    reviewObjects.Comments = txt_Reject.Value.Trim();
                    reviewObjects.ReviewedTime = hdnHours.Value + ":" + hdnMinutes.Value + ":" + hdnSeconds.Value;
                    int DMS_update = DMS_BalDM.DMS_SaveReviewComments(reviewObjects);
                    if (DMS_update > 0)
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Authorizer Remarks Saved Successfully.", "success");
                    }
                }
                else if (hdnAction.Value == "Revert")
                {
                    ViewState["Action"] = "Revert";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop4", "openUC_ElectronicSign();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("AUD_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "AUD_M3:" + strline + "  " + "Authorizer Comments saving failed.", "error");
            }
        }
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod(EnableSession = true)]
        public static void InsertCurrentDuration(string ID, string Hours, string Minutes, string Seconds)
        {
            try
            {
                DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
                DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
                ReviewObjects reviewObjects = new ReviewObjects();
                reviewObjects.Id = Convert.ToInt32(ID);
                reviewObjects.EmpID = Convert.ToInt32((HttpContext.Current.Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                reviewObjects.DocStatus = "Au";
                reviewObjects.ReviewedTime = Hours + ":" + Minutes + ":" + Seconds;
                int DMS_Update = DMS_BalDM.DMS_UpdateReviewedTimeToReviewer(reviewObjects);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                log4net.ILog Aizant_log1 = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                Aizant_log1.Error("AUD_M35:" + strline + "  " + strMsg);
            }
        }
        public void getTime()
        {
            try
            {
                reviewObjects.Id = Convert.ToInt32(hdfPKID.Value);
                reviewObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                reviewObjects.DocStatus = "Au";
                DataTable DMS_Dt = DMS_Bal.DMS_GetsavedTimeByID(reviewObjects);
                if (DMS_Dt.Rows[0]["AuthorizedTime"].ToString() != "")
                {
                    DataRow row = DMS_Dt.Rows[0];
                    string[] arr = Regex.Split(row["AuthorizedTime"].ToString(), ":");
                    hdnHours.Value = arr[0];
                    hdnMinutes.Value = arr[1];
                    hdnSeconds.Value = arr[2];
                }
                else
                {
                    string[] arr = Regex.Split("0:0:0", ":");
                    hdnHours.Value = arr[0];
                    hdnMinutes.Value = arr[1];
                    hdnSeconds.Value = arr[2];
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("AUD_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "AUD_M4:" + strline + "  " + "Timer Time getting failed.", "error");
            }
        }
        protected void gvAssignedDocuments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvAssignedDocuments.PageIndex = e.NewPageIndex;
                gvAssignedDocuments.DataSource = (DataTable)Session["dtAssignedDocuments"];
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowRefferal();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("AUD_M5:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "AUD_M5:" + strline + "  " + "Internal Documents Page index changing failed.", "error");
            }
        }
        protected void lnk_ViewInternal_Click(object sender, EventArgs e)
        {
            try
            {
                hdnGrdiReferesh.Value = "Internal";
                hdnNewDocTab.Value = "Refferal";
                LinkButton lnk = (LinkButton)sender;
                GridViewRow i = (GridViewRow)lnk.NamingContainer;
                Label lbl_text = (Label)i.FindControl("lbl_DocumentID");
                Session["isDetails"] = false;
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(lbl_text.Text);
                span4.InnerHtml = "<b>" + DMS_DT.Rows[0]["DocumentName"].ToString() + "</b>";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "$('#moppnlInternalView').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                hdfVID.Value = lbl_text.Text;
                hdfViewType.Value = "0";
                hdfViewEmpID.Value = "0";
                hdfViewRoleID.Value = "0";
                if (hdnGrdiReferesh.Value == "Internal" && hdnNewDocTab.Value == "Refferal")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "TabRefClick();", true);
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view1", "ViewInternalDocument('#InternalRefDiv','" + lbl_text.Text + "');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("AUD_M6:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "AUD_M6:" + strline + "  " + "Internal Documents viewing failed.", "error");
            }
        }
        protected void lnkDownload_Click(object sender, EventArgs e)
        {
            try
            {
                hdnGrdiReferesh.Value = "External";
                hdnNewDocTab.Value = "Refferal";
                string filePath = (sender as LinkButton).CommandArgument;
                ExternalRefDiv.InnerHtml = "&nbsp";
                ExternalRefDiv.InnerHtml = "<iframe src=\"" + ResolveUrl("~/DMS/DocumentCreator/CreateRefferalInIFrame.aspx") + "?FilePath=" + filePath + "#toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" height=\"676px\" ></iframe>";
                UpdatePanel10.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "$('#mpop_FileViewer').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                if (hdnGrdiReferesh.Value == "External" && hdnNewDocTab.Value == "Refferal")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "TabRefClick();", true);
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("AUD_M7:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "AUD_M7:" + strline + "  " + "External Documents viewing failed.", "error");
            }
        }
        public void Revert()
        {
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                if (DMS_DT.Rows[0]["Status"].ToString() != "RJ")
                {
                    int isUpload = 0;
                    objectDocUploadBO = new DocUploadBO();
                    if (Session["vwdocObjectsAz"] != null)
                    {
                        isUpload = 1;
                        objectDocUploadBO.docLocation = (DocObjects)(Session["vwdocObjectsAz"]);
                        //int DOC_Ins = DMS_Bal.DMS_InsertDocumentLocation((DocObjects)(Session["vwdocObjectsAz"]));
                    }
                    if (Session["vwreviewObjectsAz"] != null)
                    {
                        isUpload = 1;
                        objectDocUploadBO.docContent = (ReviewObjects)(Session["vwreviewObjectsAz"]);
                        //int DMS_Status = DMS_Bal.DMS_UpdateContentByID((ReviewObjects)(Session["vwreviewObjectsAz"]));
                    }
                    if (Session["vwdocObjects1Az"] != null)
                    {
                        isUpload = 1;
                        objectDocUploadBO.docComments = (DocObjects)(Session["vwdocObjects1Az"]);
                        //DMS_Bal.DMS_DocComments((DocObjects)(Session["vwdocObjects1Az"]));
                    }
                    reviewObjects.Id = Convert.ToInt32(hdfPKID.Value);
                    reviewObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    reviewObjects.DocStatus = "Au";
                    reviewObjects.Comments = Regex.Replace(txt_Reject.Value.Trim(), @"\s+", " ");
                    reviewObjects.Process = hdfPID.Value;
                    reviewObjects.ReviewedTime = hdnHours.Value + ":" + hdnMinutes.Value + ":" + hdnSeconds.Value;
                    if (isUpload == 1)
                    {
                        reviewObjects.Status = 1;
                    }
                    else
                    {
                        reviewObjects.Status = 0;
                    }
                    objectDocUploadBO.docReviwer = reviewObjects;
                    objectDocUploadBO.docStatus = reviewObjects;
                    int DMS_Update = DMS_BalDM.DMS_UpdateCommetaToReviewer(objectDocUploadBO);
                    //int DMS_Update = DMS_BalDM.DMS_UpdateCommetaToReviewer(reviewObjects);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document Number <b>" + txt_DocumentNumber.Text + "</b> Reverted Successfully.", "success", "CancelReset()");
                }
                else
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document with Document Number <b>" + txt_DocumentNumber.Text + "</b> has been already Rejected.", "info", "CancelReset()");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("AUD_M8:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "AUD_M8:" + strline + "  " + "Review status Reverted failed.", "error");
            }
        }
        public void Approve()
        {
            SqlTransaction transaction = null;
            SqlConnection EFFConn = null;
            string _DocStatus="";
            bool TrainingCount=false;
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                _DocStatus = DMS_DT.Rows[0]["Status"].ToString();
                if (DMS_DT.Rows[0]["Status"].ToString() != "RJ")
                {
                    TrainingCount = DMS_Bal.DMS_AllowDocumentActions(1, Convert.ToInt32(DMS_DT.Rows[0]["docnum"].ToString()), Convert.ToInt32(hdfPKID.Value));
                    if (TrainingCount == true)
                    {
                        DataTable _dtCompany = fillCompany();
                        if (!string.IsNullOrEmpty(_dtCompany.Rows[0]["CompanyLogo"].ToString()))
                        {
                            reviewObjects.Id = Convert.ToInt32(hdfPKID.Value);
                            reviewObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            reviewObjects.DocStatus = "Au";
                            reviewObjects.Comments = Regex.Replace(txt_Reject.Value.Trim(), @"\s+", " ");
                            reviewObjects.Process = hdfPID.Value;
                            reviewObjects.ReviewedTime = hdnHours.Value + ":" + hdnMinutes.Value + ":" + hdnSeconds.Value;
                            int DMS_Update = DMS_BalDM.DMS_ApproveDocByReviewer(reviewObjects, ref transaction, ref EFFConn);
                            if (DMS_Update == 1)
                            {
                                docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                                docObjects.RoleID = 11;//author
                                docObjects.EmpID = 0;
                                docObjects.FileType1 = ".pdf";
                                docObjects.Mode = 0;
                                System.Data.DataTable dt = DMS_Bal.DMS_GetDocumentLocationList(docObjects, ref transaction, ref EFFConn);
                                string SourcePath = Server.MapPath(@dt.Rows[0]["FilePath"].ToString());
                                string File2Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                                string TargetPath = (storefolder + File2Path);
                                PdfReader reader = new PdfReader(SourcePath);
                                PdfStamper stamper = new PdfStamper(reader, new FileStream(TargetPath, FileMode.Create));
                                int pages = reader.NumberOfPages;
                                stamper.InsertPage(1, iTextSharp.text.PageSize.A4);
                                PdfContentByte overContent = stamper.GetOverContent(1);

                                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                                image.Alignment = iTextSharp.text.Image.ALIGN_RIGHT;
                                image.SetAbsolutePosition(460, 800);
                                image.ScaleToFit(110, 55);
                                overContent.AddImage(image);

                                DataSet ds = DMS_Bal.DMS_ShowFirstPageDetails(hdfPKID.Value, ref transaction, ref EFFConn);
                                float[] HeadertablecolWidths = { 340f, 160f };
                                PdfPTable Headertab = new PdfPTable(HeadertablecolWidths);
                                Headertab.TotalWidth = 500F;
                                //row 1
                                string DocumentName = ds.Tables[0].Rows[0]["DocumentName"].ToString();
                                PdfPCell DocName = new PdfPCell(new Phrase(DocumentName,
                                                    new Font(Font.FontFamily.TIMES_ROMAN, 12F, Font.BOLD)));
                                DocName.Colspan = 2;
                                DocName.HorizontalAlignment = Element.ALIGN_CENTER; //0=Left, 1=Center, 2=Right
                                                                                    //Style
                                                                                    //cell.BorderColor = new BaseColor(System.Drawing.Color.LightGray);
                                                                                    //cell.BorderWidth = 3f;
                                DocName.Border = Rectangle.NO_BORDER;
                                Headertab.AddCell(DocName);
                                //row 2
                                string DocumentType = "Document Type     : " + ds.Tables[0].Rows[0]["DocumentType"].ToString();
                                PdfPCell DocType = new PdfPCell(new Phrase(DocumentType,
                                                   new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                DocType.Colspan = 2;
                                DocType.Border = Rectangle.NO_BORDER;
                                Headertab.AddCell(DocType);
                                //row 3
                                string DocNum = null;
                                if (Convert.ToInt32(ds.Tables[0].Rows[0]["VersionNumber"]) <= 9)
                                {
                                    DocNum = "Document Number: " + ds.Tables[0].Rows[0]["DocumentNumber"].ToString() + "-0" + ds.Tables[0].Rows[0]["VersionNumber"].ToString();
                                }
                                else
                                {
                                    DocNum = "Document Number: " + ds.Tables[0].Rows[0]["DocumentNumber"].ToString() + "-" + ds.Tables[0].Rows[0]["VersionNumber"].ToString();
                                }
                                PdfPCell DocNumber = new PdfPCell(new Phrase(DocNum,
                                                    new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                DocNumber.Border = Rectangle.NO_BORDER;
                                Headertab.AddCell(DocNumber);
                                string EffDate = "Effective Date: " + ds.Tables[0].Rows[0]["EffectiveDate"].ToString();
                                PdfPCell EffectiveDate = new PdfPCell(new Phrase(EffDate,
                                                    new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                EffectiveDate.Border = Rectangle.NO_BORDER;
                                Headertab.AddCell(EffectiveDate);
                                //row 4
                                string Dept = "Department            : " + ds.Tables[0].Rows[0]["DocDepartment"].ToString();
                                PdfPCell Department = new PdfPCell(new Phrase(Dept,
                                                    new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                Department.Border = Rectangle.NO_BORDER;
                                Headertab.AddCell(Department);
                                string RevDate = "Review Date   : " + ds.Tables[0].Rows[0]["ExpirationDate"].ToString();
                                PdfPCell ReviewDate = new PdfPCell(new Phrase(RevDate,
                                                    new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                ReviewDate.Border = Rectangle.NO_BORDER;
                                Headertab.AddCell(ReviewDate);
                                iTextSharp.text.Document Headerdocument = overContent.PdfDocument;
                                Headertab.WriteSelectedRows(0, -1, 40f, Headerdocument.Top - 20, overContent);

                                int tableCount = 0;
                                if (ds.Tables[4].Rows.Count > 0)
                                {
                                    tableCount = ds.Tables.Count - 1;
                                }
                                else
                                {
                                    tableCount = ds.Tables.Count - 2;
                                }
                                float[] tablecolWidths = { 80f, 170f, 80f, 170f };
                                PdfPTable Contenttab = new PdfPTable(tablecolWidths);
                                Contenttab.TotalWidth = 520F;
                                StringBuilder tblHeaderText = new StringBuilder();
                                for (int i = 1; i <= tableCount; i++)
                                {
                                    switch (i)
                                    {
                                        case 1:
                                            tblHeaderText.Clear();
                                            tblHeaderText.Append("Prepared By");
                                            break;
                                        case 2:
                                            tblHeaderText.Clear();
                                            tblHeaderText.Append("Reviewed By");
                                            break;
                                        case 3:
                                            tblHeaderText.Clear();
                                            tblHeaderText.Append("Approved By");
                                            break;
                                        case 4:
                                            tblHeaderText.Clear();
                                            tblHeaderText.Append("Authorized By");
                                            break;
                                        default:
                                            break;
                                    }
                                    PdfPCell tblHeader = new PdfPCell(new Phrase(tblHeaderText.ToString(),
                                    new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                    tblHeader.Colspan = 4;
                                    tblHeader.HorizontalAlignment = Element.ALIGN_MIDDLE; //0=Left, 1=Center, 2=Right
                                    tblHeader.Padding = 5;                                  //Style
                                    tblHeader.BackgroundColor = new BaseColor(System.Drawing.Color.LightGray);
                                    Contenttab.AddCell(tblHeader);
                                    for (int j = 1; j <= ds.Tables[i].Rows.Count; j++)
                                    {
                                        PdfPCell Namecell1 = new PdfPCell(new Phrase("Name         :",
                                        new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                        Namecell1.Border = Rectangle.LEFT_BORDER;
                                        Namecell1.Padding = 5;
                                        Contenttab.AddCell(Namecell1);
                                        PdfPCell Namecell2 = new PdfPCell(new Phrase(ds.Tables[i].Rows[j - 1][0].ToString(),
                                                            new Font(Font.FontFamily.TIMES_ROMAN, 12F, 0, BaseColor.BLUE)));
                                        Namecell2.Border = Rectangle.NO_BORDER;
                                        Namecell2.Padding = 5;
                                        Contenttab.AddCell(Namecell2);
                                        PdfPCell Descell1 = new PdfPCell(new Phrase("Designation   :",
                                                            new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                        Descell1.Border = Rectangle.NO_BORDER;
                                        Descell1.Padding = 5;
                                        Contenttab.AddCell(Descell1);
                                        PdfPCell Descell2 = new PdfPCell(new Phrase(ds.Tables[i].Rows[j - 1][3].ToString(),
                                                            new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                        Descell2.Border = Rectangle.RIGHT_BORDER;
                                        Descell2.Padding = 5;
                                        Contenttab.AddCell(Descell2);
                                        PdfPCell Datecell1 = new PdfPCell(new Phrase("Date           :",
                                           new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                        Datecell1.BorderWidthTop = 0;
                                        Datecell1.BorderWidthRight = 0;
                                        Datecell1.Padding = 5;
                                        Contenttab.AddCell(Datecell1);
                                        PdfPCell Datecell2 = new PdfPCell(new Phrase(ds.Tables[i].Rows[j - 1][1].ToString(),
                                                            new Font(Font.FontFamily.TIMES_ROMAN, 12F, 0, BaseColor.BLUE)));
                                        Datecell2.Border = Rectangle.BOTTOM_BORDER;
                                        Datecell2.Padding = 5;
                                        Contenttab.AddCell(Datecell2);
                                        PdfPCell Deptcell1 = new PdfPCell(new Phrase("Department    :",
                                                           new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                        Deptcell1.Border = Rectangle.BOTTOM_BORDER;
                                        Deptcell1.Padding = 5;
                                        Contenttab.AddCell(Deptcell1);
                                        PdfPCell Deptcell2 = new PdfPCell(new Phrase(ds.Tables[i].Rows[j - 1][2].ToString(),
                                                            new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                        Deptcell2.BorderWidthTop = 0;
                                        Deptcell2.BorderWidthLeft = 0;
                                        Deptcell2.Padding = 5;
                                        Contenttab.AddCell(Deptcell2);
                                    }
                                }
                                ColumnText ct = new ColumnText(stamper.GetOverContent(1));
                                ct.AddElement(Contenttab);
                                Rectangle rect = new Rectangle(0, 90, 0, 136);
                                ct.SetSimpleColumn(-25, 56, PageSize.A4.Width + 25, PageSize.A4.Height - 175);
                                ct.Go();

                                PdfContentByte middleContent = stamper.GetOverContent(1);
                                PdfPTable Declerationtab = new PdfPTable(1);
                                Declerationtab.TotalWidth = 520F;
                                //row 1
                                PdfPCell Decleration = new PdfPCell(new Phrase("This document is electronically signed and doesn’t require manual signature",
                                                    new Font(Font.FontFamily.TIMES_ROMAN, 10F)));
                                Decleration.HorizontalAlignment = Element.ALIGN_CENTER;
                                Decleration.BorderWidth = 0;
                                Declerationtab.AddCell(Decleration);
                                iTextSharp.text.Document Declarationdocument = middleContent.PdfDocument;
                                Declerationtab.WriteSelectedRows(0, -1, 40f, Declarationdocument.Bottom + 100f, middleContent);

                                PdfContentByte underContent = stamper.GetOverContent(1);
                                PdfPTable Footertab = new PdfPTable(1);
                                Footertab.TotalWidth = 520F;
                                //row 1
                                PdfPCell FootHeader = new PdfPCell(new Phrase("CONFIDENTIAL AND PROPRIETARY",
                                                    new Font(Font.FontFamily.TIMES_ROMAN, 12F, Font.BOLD, BaseColor.BLACK)));
                                FootHeader.HorizontalAlignment = Element.ALIGN_CENTER;
                                FootHeader.BorderWidthBottom = 0;
                                Footertab.AddCell(FootHeader);
                                //row 2
                                PdfPCell FootBody = new PdfPCell(new Phrase("The contents of this document are confidential and proprietary to Aizant Drug Research Solutions Pvt. Ltd. Any unauthorized use, disclosure or reproduction is strictly prohibited.",
                                                    new Font(Font.FontFamily.TIMES_ROMAN, 10F)));
                                FootBody.BorderWidthTop = 0;
                                Footertab.AddCell(FootBody);
                                iTextSharp.text.Document Footerdocument = underContent.PdfDocument;
                                Footertab.WriteSelectedRows(0, -1, 40f, Footerdocument.Bottom + 30f, underContent);
                                stamper.Close();

                                docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                                docObjects.RoleID = 13;//authorizer
                                docObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                docObjects.FilePath1 = "N/A";
                                docObjects.FilePath2 = string.Format(@"~/Scripts/pdf.js/web/") + File2Path;
                                docObjects.isTemp = 0;
                                docObjects.FileType1 = "N/A";
                                docObjects.FileType2 = ".pdf";
                                docObjects.FileName1 = "N/A";
                                docObjects.FileName2 = File2Path;
                                int DOC_Ins = DMS_Bal.DMS_InsertDocumentLocation(docObjects, ref transaction, ref EFFConn);
                                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MLalert", "LoadML_URL();", true);
                                if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "1")
                                {
                                    transaction.Commit();
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "New Document with Number <b>" + txt_DocumentNumber.Text + "</b> Made Active Successfully.", "success", "CancelResetWithML()");
                                }
                                else if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "2")
                                {
                                    transaction.Commit();
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Revision Document with Number <b>" + txt_DocumentNumber.Text + "</b> Made Active Successfully.", "success", "CancelResetWithML()");
                                }                           
                            }
                            else
                            {
                                transaction.Commit();
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "  Document Number <b>" + txt_DocumentNumber.Text + "</b> Approved Successfully.", "success", "CancelResetWithML()");
                            }
                        }
                    }
                    else
                    {
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document Under Training, Not able to active Revision Document.", "error", "CancelReset()");
                    }
                }
                else
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document with Document Number <b>" + txt_DocumentNumber.Text + "</b> has been already Rejected.", "info", "CancelReset()");
                }
            }
            catch (Exception ex)
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("AUD_M9:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "AUD_M9:" + strline + "  " + "Review status Approved failed.", "error");
            }
            finally
            {
                if(_DocStatus != "RJ" && (_DocStatus != "3" && TrainingCount==false))
                {
                    if (EFFConn != null)
                    {
                        EFFConn.Close();
                        EFFConn.Dispose();
                    }
                }               
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        private void RetriveDocumentDetails(int VersionID, int ProcessID, int Training,int DocumentID)
        {
            try
            {
                hdfPKID.Value = VersionID.ToString();
                hdfPID.Value = ProcessID.ToString();
                hdfTraining.Value = Training.ToString();
                hdfDocumentID.Value = DocumentID.ToString();
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                if (DMS_DT.Rows.Count > 0)
                {
                    txt_requestType.Text = DMS_DT.Rows[0]["RequestType"].ToString();
                    txt_documentType.Text = DMS_DT.Rows[0]["DocumentType"].ToString();
                    txt_Department.Text = DMS_DT.Rows[0]["Department"].ToString();
                    txt_DocumentNumber.Text = DMS_DT.Rows[0]["DocumentNumber"].ToString();
                    txt_DocumentName.Text = DMS_DT.Rows[0]["DocumentName"].ToString();
                    if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "1")
                    {
                        lblComments.InnerHtml = "Purpose of Initiation";
                        txt_Comments.Text = DMS_DT.Rows[0]["CreatePurpose"].ToString();
                        txt_VersionID.Text = "00";
                    }
                    else if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "2")
                    {
                        lblComments.InnerHtml = "Reason for Change";
                        txt_Comments.Text = DMS_DT.Rows[0]["CreatePurpose"].ToString();
                        txt_VersionID.Text = DMS_DT.Rows[0]["versionNumber"].ToString();
                    }
                    if (Convert.ToBoolean(DMS_DT.Rows[0]["isPublic"].ToString()) == true)
                    {
                        RadioButtonList1.SelectedValue = "yes";
                    }
                    else
                    {
                        RadioButtonList1.SelectedValue = "no";
                    }
                    RadioButtonList1.Enabled = false;
                    txt_Author.Text = DMS_DT.Rows[0]["CreatorID"].ToString();
                    txt_Authorizer.Text = DMS_DT.Rows[0]["AuthorizedBy"].ToString();
                    if (txt_Authorizer.Text == "")
                    {
                        div1.Visible = false;
                    }
                    else
                    {
                        div1.Visible = true;
                    }
                    DataTable aprrove_list = DMS_Bal.DMS_GetApproverReviewerSelectedList(hdfPKID.Value, 5);
                    ViewState["ApproverData"] = aprrove_list;
                    gvApprover.DataSource = aprrove_list;
                    gvApprover.DataBind();
                    if (DMS_DT.Rows[0]["ReferralCount"].ToString() == "0")
                    {
                        liReferrals.Visible = false;
                    }
                    else
                    {
                        liReferrals.Visible = true;
                    }
                    if (Training == 0)
                    {
                        Btn_Approve.Visible = true;
                    }
                    else
                    {
                        Btn_Approve.Visible = false;
                    }
                    hdfFromEmpID.Value = DMS_DT.Rows[0]["authorID"].ToString();
                    docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                    DataTable dt = DMS_Bal.DMS_GetTempRevertedReviewersDetails(docObjects);
                    if (dt.Rows.Count > 0)
                    {
                        divcomments.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("AUD_M10:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "AUD_M10:" + strline + "  " + "Document Details loading failed.", "error");
            }
        }
        private void RetrieveDocumentContent(int DocumentID)
        {
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                reviewObjects.Id = Convert.ToInt32(hdfPKID.Value);
                reviewObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                reviewObjects.DocStatus = "Au";
                DataTable DMS_Commets = DMS_Bal.DMS_GetsavedCommentsByID(reviewObjects);
                txt_Reject.Value = DMS_Commets.Rows[0]["ReviewComments"].ToString();
                getTime();
                HelpClass.custAlertMsg(this, this.GetType(), "Please Download Document to view Track Changes!", "info");
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop", "OpenReadSopModal();", true);
                hdfViewType.Value = "0";
                hdfViewEmpID.Value = "0";
                hdfViewRoleID.Value = "11";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view2", "ViewDocument('#divPDF_Viewer','2','');", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("AUD_M11:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "AUD_M11:" + strline + "  " + "Document Content loading failed.", "error");
            }
        }
        private void RetrieveRefferalContent(int DocumentID)
        {
            try
            {
                hdnGrdiReferesh.Value = "Link";
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                string DMS_DocumentID = hdfPKID.Value;
                txtReferrences.Text = DMS_Bal.DMS_GetLinkReferences(Convert.ToInt32(DMS_DocumentID)).Replace("<a", "<a  target='_blank' ");
                DataTable dtInternalDocs = DMS_Bal.DMS_GetReferalInternalDocs(Convert.ToInt32(DMS_DocumentID));
                Session["dtAssignedDocuments"] = dtInternalDocs;
                gvAssignedDocuments.DataSource = dtInternalDocs;
                gvAssignedDocuments.DataBind();
                DataTable dtExternalDocs = DMS_Bal.DMS_GetReferalExternalFiles(Convert.ToInt32(DMS_DocumentID));
                gvExternalFiles.DataSource = dtExternalDocs;
                gvExternalFiles.DataBind();
                ViewState["dtExternalDocuments"] = dtExternalDocs;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("AUD_M12:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "AUD_M12:" + strline + "  " + "Referral Content loading failed.", "error");
            }
        }
        private void InitializeThePage()
        {
            try
            {
                if (Request.QueryString["VersionID"] != null && Request.QueryString["PID"] != null && Request.QueryString["Training"] != null && Request.QueryString["DocumentID"] != null)
                {
                    int VersionID = Convert.ToInt32(Request.QueryString["VersionID"]);
                    int ProcessID = Convert.ToInt32(Request.QueryString["PID"]);
                    int Training = Convert.ToInt32(Request.QueryString["Training"]);
                    int DocumentID = Convert.ToInt32(Request.QueryString["DocumentID"]);
                    RetriveDocumentDetails(VersionID, ProcessID, Training, DocumentID);
                    RetrieveDocumentContent(VersionID);
                    RetrieveRefferalContent(VersionID);
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("AUD_M13:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "AUD_M13:" + strline + "  " + "Page Initialization failed.", "error");
            }
        }
        protected void btn_Download_Click(object sender, EventArgs e)
        {
            btn_Download.Enabled = false;
            try
            {
                System.Data.DataTable dt = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                if (Btn_ShowOriginal.Visible == false && Btn_ShowModified.Visible == false)
                {
                    docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                    docObjects.RoleID = 11;//author
                    docObjects.EmpID = 0;
                    docObjects.FileType1 = dt.Rows[0]["FileType"].ToString();
                    docObjects.Mode = 0;
                }
                else if (Btn_ShowOriginal.Visible == true && Btn_ShowModified.Visible == false)
                {
                    docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                    docObjects.RoleID = 0;
                    docObjects.EmpID = 0;
                    docObjects.FileType1 = dt.Rows[0]["FileType"].ToString();
                    docObjects.Mode = 0;
                }
                else if (Btn_ShowOriginal.Visible == false && Btn_ShowModified.Visible == true)
                {
                    docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                    docObjects.RoleID = 11;//Author
                    docObjects.EmpID = 0;
                    docObjects.FileType1 = dt.Rows[0]["FileType"].ToString();
                    docObjects.Mode = 0;
                }
                System.Data.DataTable DMS_DT = DMS_Bal.DMS_GetDocumentLocationList(docObjects);
                string DocPath = Server.MapPath(@DMS_DT.Rows[0]["FilePath"].ToString());
                Response.Clear();
                if (DMS_DT.Rows[0]["FileType"].ToString() == ".doc")
                {
                    Response.ContentType = "application/msword";
                }
                else if (DMS_DT.Rows[0]["FileType"].ToString() == ".docx")
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                }
                Response.AddHeader("Content-Disposition", "attachment; filename=" + DMS_DT.Rows[0]["FileName"].ToString());
                Response.TransmitFile(DocPath);
                DownloadRecord();
                Response.End();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("AUD_M14:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "AUD_M14:" + strline + "  " + "Download click failed.", "error");
            }
            finally
            {
                btn_Download.Enabled = true;
            }
        }
        public void DownloadRecord()
        {
            docObjects1.Version = Convert.ToInt32(hdfPKID.Value);
            docObjects1.RoleID = 13;//13-Authorizer
            docObjects1.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            docObjects1.Remarks = "N/A";
            docObjects1.Time = "N/A";
            docObjects1.action = 42;
            docObjects1.CommentType = 1;
            DMS_Bal.DMS_DocComments(docObjects1);
        }
        protected void Btn_Upload_Click(object sender, EventArgs e)
        {
            Btn_Upload.Enabled = false;
            try
            {
                Session["vwdocObjectsAz"] = null;
                Session["vwreviewObjectsAz"] = null;
                Session["vwdocObjects1Az"] = null;
                byte[] Filebyte = null;
                if (file_word.HasFile)
                {
                    System.Data.DataTable dt = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                    Stream fs = file_word.PostedFile.InputStream;
                    BinaryReader br = new BinaryReader(fs);
                    Filebyte = br.ReadBytes((Int32)fs.Length);
                    if (Filebyte.Length < 1000)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' Not able to read content,document upload failed','warning');", true);
                        return;
                    }
                    string ext = System.IO.Path.GetExtension(file_word.FileName).ToLower();
                    string mime = MimeType.GetMimeType(Filebyte, file_word.FileName);
                    string[] filemimes1 = new string[] { "application/pdf", "application/x-msdownload", "application/unknown", "application/octet-stream" };
                    string[] filemimes = new string[] { "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/x-zip-compressed" };
                    if (filemimes1.Contains(mime))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' Please upload .doc or .docx files only ! <br/> The uploaded file is not valid or corrupted.','warning','hideApproveDownload();');", true);
                        return;
                    }
                    if (!filemimes.Contains(mime))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' Please upload .doc or .docx files only ! <br/> The uploaded file is not valid or corrupted.','warning','hideApproveDownload();');", true);
                        return;
                    }
                    if (ext == ".doc" || ext == ".docx")
                    {
                        string strWordMainFolderPath = DynamicFolder.CreateTempDocumentFolder(dt.Rows[0]["DocumentNumber"].ToString(), dt.Rows[0]["VersionNumber"].ToString());
                        string File1Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ext;
                        string strSourcePath = (strWordMainFolderPath + "\\" + File1Path);
                        string File2Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                        string strTargPath = (storefolder + File2Path);

                        //Save original file
                        file_word.SaveAs(strSourcePath);

                        //call the converter method
                        bool isValid = converter.convert(strSourcePath, strTargPath);
                        if (isValid)
                        {
                            docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                            docObjects.RoleID = 13;//authorizer
                            docObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            docObjects.FilePath1 = string.Format(@"~/DMSTempDocuments/{0}/{1}/", dt.Rows[0]["DocumentNumber"].ToString(), dt.Rows[0]["VersionNumber"].ToString()) + File1Path;
                            docObjects.FilePath2 = string.Format(@"~/Scripts/pdf.js/web/") + File2Path;
                            docObjects.isTemp = 0;
                            docObjects.FileType1 = ext;
                            docObjects.FileType2 = ".pdf";
                            docObjects.FileName1 = File1Path;
                            docObjects.FileName2 = File2Path;
                            ViewState["PDFPhysicalName"] = File2Path;
                            Session["vwdocObjectsAz"] = docObjects;

                            //if (hdfPID.Value == "2" || hdfPID.Value == "3")
                            //{
                            //    reviewObjects.Id = Convert.ToInt32(hdfPKID.Value);
                            //    //reviewObjects.Content = Filebyte;
                            //    reviewObjects.Ext = ext;
                            //    reviewObjects.TemplateName = file_word.FileName;
                            //    Session["vwreviewObjectsAz"] = reviewObjects;
                            //}

                            docObjects1.Version = Convert.ToInt32(hdfPKID.Value);
                            docObjects1.RoleID = 9;//9-Approver
                            docObjects1.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            docObjects1.Remarks = "N/A";
                            docObjects1.Time = "N/A";
                            docObjects1.action = 43;
                            docObjects1.CommentType = 1;
                            Session["vwdocObjects1Az"] = docObjects1;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show1", "showApproveDownload();", true);
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view3", "ViewDocument('#divPDF_Viewer','1','" + File2Path + "');", true);
                            Btn_ShowOriginal.Visible = true;
                            Btn_ShowModified.Visible = false;
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "custAlertMsg(' <br/> Server error , contact admin.','error','hideApproveDownload();');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/> Please upload  .doc or .docx files only.','warning','hideApproveDownload();');", true);
                        return;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/> Please upload  .doc or .docx files only.','warning');", true);
                    return;
                }
                upUpload.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("AUD_M15:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "AUD_M15:" + strline + "  " + "Upload click failed.", "error");
            }
            finally
            {
                Btn_Upload.Enabled = true;
            }
        }
        protected void Btn_ShowOriginal_Click(object sender, EventArgs e)
        {
            Btn_ShowOriginal.Enabled = false;
            try
            {
                hdfViewType.Value = "0";
                hdfViewEmpID.Value = "0";
                hdfViewRoleID.Value = "11";//author
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view4", "ViewDocument('#divPDF_Viewer','2','');", true);
                Btn_ShowOriginal.Visible = false;
                Btn_ShowModified.Visible = true;

                docObjects1.Version = Convert.ToInt32(hdfPKID.Value);
                docObjects1.RoleID = 9;//9-Approver
                docObjects1.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                docObjects1.Remarks = "N/A";
                docObjects1.Time = "N/A";
                docObjects1.action = 0;
                docObjects1.CommentType = 2;
                System.Data.DataTable table = DMS_Bal.DMS_DocComments(docObjects1);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show2", "showApproveDownload();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("AUD_M16:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "AUD_M16:" + strline + "  " + "Show Original click failed.", "error");
            }
            finally
            {
                Btn_ShowOriginal.Enabled = true;
            }
        }
        protected void Btn_ShowModified_Click(object sender, EventArgs e)
        {
            Btn_ShowModified.Enabled = false;
            try
            {
                hdfViewType.Value = "0";
                hdfViewEmpID.Value = "0";
                hdfViewRoleID.Value = "0";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view5", "ViewDocument('#divPDF_Viewer','1','" + ViewState["PDFPhysicalName"].ToString() + "');", true);
                Btn_ShowOriginal.Visible = true;
                Btn_ShowModified.Visible = false;

                docObjects1.Version = Convert.ToInt32(hdfPKID.Value);
                docObjects1.RoleID = 9;//9-Approver
                docObjects1.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                docObjects1.Remarks = "N/A";
                docObjects1.Time = "N/A";
                docObjects1.action = 0;
                docObjects1.CommentType = 2;
                System.Data.DataTable table = DMS_Bal.DMS_DocComments(docObjects1);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show3", "showApproveDownload();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("AUD_M17:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "AUD_M17:" + strline + "  " + "Show Modified click failed.", "error");
            }
            finally
            {
                Btn_ShowModified.Enabled = true;
            }
        }
        DataTable fillCompany()
        {
            try
            {
                CompanyBO objCompanyBO;
                UMS_BAL objUMS_BAL;
                DataTable dtcompany = new DataTable();
                objCompanyBO = new CompanyBO();
                objCompanyBO.Mode = 2;
                objCompanyBO.CompanyID = 0;
                objCompanyBO.CompanyCode = "";
                objCompanyBO.CompanyName = "";
                objCompanyBO.CompanyDescription = "";
                objCompanyBO.CompanyPhoneNo1 = "";
                objCompanyBO.CompanyPhoneNo2 = "";
                objCompanyBO.CompanyFaxNo1 = "";
                objCompanyBO.CompanyFaxNo2 = ""; //txtFaxNo2.Value;
                objCompanyBO.CompanyEmailID = "";
                objCompanyBO.CompanyWebUrl = "";
                objCompanyBO.CompanyAddress = "";
                objCompanyBO.CompanyLocation = "";// txtLocat.Value;
                objCompanyBO.CompanyCity = "";
                objCompanyBO.CompanyState = "";
                objCompanyBO.CompanyCountry = "";
                objCompanyBO.CompanyPinCode = "";
                objCompanyBO.CompanyStartDate = "";
                objCompanyBO.CompanyLogo = new byte[0];
                objUMS_BAL = new UMS_BAL();
                dtcompany = objUMS_BAL.CompanyQuery(objCompanyBO);
                return dtcompany;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}