﻿using AizantIT_DMSBAL;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Pdf;
using System.Drawing.Imaging;
using DevExpress.Web;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;


namespace AizantIT_PharmaApp.DMS.DevReports
{
    public partial class frmDocumentViewer : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            
                if (Request.QueryString.Count > 0)
                {
                    string vid = Request.QueryString[0];
                    DocumentViewerPageFill(vid);
                }
            //DocumentViewerPageFill();
        }

        protected void DocumentViewerPageFill(string vid)
        {
            try
            {

                DocumentFirstPage firstPage = new DocumentFirstPage();
                DataSet ds = DMS_Bal.DMS_ShowFirstPageDetails(vid);


                if (ds.Tables[0].Rows.Count > 0)
                {
                    ((XRTableCell)(firstPage.FindControl("xrTCDocumentNumber", true))).Text = ds.Tables[0].Rows[0]["DocumentNumber"].ToString();
                    ((XRTableCell)(firstPage.FindControl("xrTCDocumentName", true))).Text = ds.Tables[0].Rows[0]["DocumentName"].ToString();
                    ((XRLabel)(firstPage.FindControl("xrLblDocumentType", true))).Text = ds.Tables[0].Rows[0]["DocumentType"].ToString();
                    ((XRTableCell)(firstPage.FindControl("xrTCEffectiveDate", true))).Text = ds.Tables[0].Rows[0]["EffectiveDate"].ToString();
                    ((XRTableCell)(firstPage.FindControl("xrTCReviewDate", true))).Text = ds.Tables[0].Rows[0]["ExpirationDate"].ToString();
                    ((XRTableCell)(firstPage.FindControl("xrTCDocumentDepartment", true))).Text = ds.Tables[0].Rows[0]["DocDepartment"].ToString();
                    ((XRTableCell)(firstPage.FindControl("xrTCDocumentVersion", true))).Text = ds.Tables[0].Rows[0]["VersionNumber"].ToString();
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    XRTable tblauthor = ((XRTable)(firstPage.FindControl("xrTblAuthor", true)));
                    tblauthor.BeginInit();
                    for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                    {
                        XRTableRow row1 = new XRTableRow();//row create

                        //int j = 0;
                        //foreach (DataColumn dc in ds1.Tables[2].Columns)
                        //{
                        XRTableCell cell1 = new XRTableCell(); 
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell1.CanGrow = true;
                        cell1.Width = 50;
                        cell1.Borders = DevExpress.XtraPrinting.BorderSide.Left;
                        cell1.Text = "Name:";
                        row1.Cells.Add(cell1);
                        XRTableCell cell2 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell2.CanGrow = true;
                        cell2.Width = 150;
                        //cell2.BorderColor = Color.Blue;
                        //cell2.ForeColor = Color.Indigo;
                        //cell2.Font = new Font("Times New Roman",12, FontStyle.Underline);
                        cell2.Text = ds.Tables[1].Rows[i]["AuthorName"].ToString();
                        row1.Cells.Add(cell2);
                        XRTableCell cell3 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell3.CanGrow = true;
                        cell3.Width = 50;
                        cell3.Text = "Designation:";
                        row1.Cells.Add(cell3);
                        XRTableCell cell4 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell4.CanGrow = true;
                        cell4.Width = 100;
                        cell4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
                        //cell4.ForeColor = Color.Indigo;
                        //cell4.Font = new Font("Times New Roman", 12, FontStyle.Underline);
                        cell4.Text = ds.Tables[1].Rows[i]["AuthorDesignation"].ToString();
                        row1.Cells.Add(cell4);
                        //   /* j*/++;
                        //}
                        tblauthor.Rows.Add(row1);

                        XRTableRow row2 = new XRTableRow();
                        XRTableCell cell5 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell5.CanGrow = true;
                        cell5.Width = 50;
                        cell5.Borders = DevExpress.XtraPrinting.BorderSide.Left;
                        cell5.Text = "Date:";
                        row2.Cells.Add(cell5);
                        XRTableCell cell6 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell6.CanGrow = true;
                        cell6.Width = 150;
                        //cell6.ForeColor = Color.Indigo;
                        //cell6.Font = new Font("Times New Roman", 12, FontStyle.Underline);
                        cell6.Text = ds.Tables[1].Rows[i]["CreatedDate"].ToString();
                        row2.Cells.Add(cell6);
                        XRTableCell cell7 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell7.CanGrow = true;
                        cell7.Width = 50;
                        cell7.Text = "Department:";
                        row2.Cells.Add(cell7);
                        XRTableCell cell8 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell8.CanGrow = true;
                        cell8.Width = 100;
                        //cell8.ForeColor = Color.Indigo;
                        cell8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
                        //cell8.Font = new Font("Times New Roman", 12, FontStyle.Underline);
                        cell8.Text = ds.Tables[1].Rows[i]["AuthorDepartment"].ToString();
                        row2.Cells.Add(cell8);
                        //   /* j*/++;
                        //}
                        tblauthor.Rows.Add(row2);

                        XRTableRow row3 = new XRTableRow();
                        tblauthor.Rows.Add(row3);
                    }
                    tblauthor.Rows.RemoveAt(0);
                    tblauthor.Rows.LastRow.Dispose();
                    tblauthor.EndInit();
                }
                else
                {
                    ((XRLabel)(firstPage.FindControl("xrLblPreparedby", true))).Visible = false;
                    ((XRTable)(firstPage.FindControl("xrTblAuthor", true))).Visible = false;
                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    XRTable tblreview = ((XRTable)(firstPage.FindControl("xrTblReviewer", true)));
                    tblreview.BeginInit();
                    for (int i = 0; i < ds.Tables[2].Rows.Count; i++)
                    {
                        XRTableRow row1 = new XRTableRow();

                        //int j = 0;
                        //foreach (DataColumn dc in ds1.Tables[2].Columns)
                        //{
                        XRTableCell cell1 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell1.CanGrow = true;
                        cell1.Width = 50;
                        cell1.Borders = DevExpress.XtraPrinting.BorderSide.Left;
                        cell1.Text = "Name:";
                        row1.Cells.Add(cell1);
                        XRTableCell cell2 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell2.CanGrow = true;
                        cell2.Width = 150;
                        //cell2.Font = new Font("Times New Roman", 12, FontStyle.Underline);
                        cell2.Text = ds.Tables[2].Rows[i]["ReviewerName"].ToString();
                        row1.Cells.Add(cell2);
                        XRTableCell cell3 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell3.CanGrow = true;
                        cell3.Width = 50;
                        cell3.Text = "Designation:";
                        row1.Cells.Add(cell3);
                        XRTableCell cell4 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell4.CanGrow = true;
                        cell4.Width = 100;
                        cell4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
                        //cell4.Font = new Font("Times New Roman", 12, FontStyle.Underline);
                        cell4.Text = ds.Tables[2].Rows[i]["ReviewerDesignation"].ToString();
                        row1.Cells.Add(cell4);
                        //   /* j*/++;
                        //}
                        tblreview.Rows.Add(row1);

                        XRTableRow row2 = new XRTableRow();
                        XRTableCell cell5 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell5.CanGrow = true;
                        cell5.Width = 50;
                        cell5.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom;
                        cell5.Text = "Date:";
                        row2.Cells.Add(cell5);
                        XRTableCell cell6 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell6.CanGrow = true;
                        cell6.Width = 150;
                        cell6.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                        //cell6.Font = new Font("Times New Roman", 12, FontStyle.Underline);
                        cell6.Text = ds.Tables[2].Rows[i]["ReviewedDate"].ToString();
                        row2.Cells.Add(cell6);
                        XRTableCell cell7 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell7.CanGrow = true;
                        cell7.Width = 50;
                        cell7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                        cell7.Text = "Department:";
                        row2.Cells.Add(cell7);
                        XRTableCell cell8 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell8.CanGrow = true;
                        cell8.Width = 100;
                        cell8.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                        //cell8.Font = new Font("Times New Roman", 12, FontStyle.Underline);
                        cell8.Text = ds.Tables[2].Rows[i]["ReviewerDepartment"].ToString();
                        row2.Cells.Add(cell8);
                        //   /* j*/++;
                        //}
                        tblreview.Rows.Add(row2);

                        //XRTableRow row3 = new XRTableRow();
                        ////row3.Borders= DevExpress.XtraPrinting.BorderSide.Bottom;
                        //tblreview.Rows.Add(row3);
                    }
                    tblreview.Rows.RemoveAt(0);
                    //tblreview.Rows.LastRow.Dispose();
                    tblreview.EndInit();
                    //((XRLabel)(rd.FindControl("xrTCReviewerName", true))).Text = ds.Tables[2].Rows[0]["ReviewerName"].ToString();
                    //((XRLabel)(rd.FindControl("xrTCReviewerDate", true))).Text = ds.Tables[2].Rows[0]["ReviewedDate"].ToString();
                    //((XRLabel)(rd.FindControl("xrTCReviewerDesignation", true))).Text = ds.Tables[2].Rows[0]["ReviewerDesignation"].ToString();
                    //((XRLabel)(rd.FindControl("xrTCReviewerDepartment", true))).Text = ds.Tables[2].Rows[0]["ReviewerDepartment"].ToString();
                }
                else
                {
                    ((XRLabel)(firstPage.FindControl("xrLblReviewedby", true))).Visible = false;
                    ((XRTable)(firstPage.FindControl("xrTblReviewer", true))).Visible = false;
                }
                if (ds.Tables[3].Rows.Count > 0)
                {
                    XRTable tblapprove = ((XRTable)(firstPage.FindControl("xrTblApprover", true)));
                    tblapprove.BeginInit();
                    for (int i = 0; i < ds.Tables[3].Rows.Count; i++)
                    {
                        XRTableRow row1 = new XRTableRow();

                        //int j = 0;
                        //foreach (DataColumn dc in ds1.Tables[2].Columns)
                        //{
                        XRTableCell cell1 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell1.CanGrow = true;
                        cell1.Width = 50;
                        cell1.Borders = DevExpress.XtraPrinting.BorderSide.Left;
                        cell1.Text = "Name:";
                        row1.Cells.Add(cell1);
                        XRTableCell cell2 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell2.CanGrow = true;
                        cell2.Width = 150;
                        //cell2.Font = new Font("Times New Roman", 12, FontStyle.Underline);
                        cell2.Text = ds.Tables[3].Rows[i]["ApproverName"].ToString();
                        row1.Cells.Add(cell2);
                        XRTableCell cell3 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell3.CanGrow = true;
                        cell3.Width = 50;
                        cell3.Text = "Designation:";
                        row1.Cells.Add(cell3);
                        XRTableCell cell4 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell4.CanGrow = true;
                        cell4.Width = 100;
                        cell4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
                        //cell4.Font = new Font("Times New Roman", 12, FontStyle.Underline);
                        cell4.Text = ds.Tables[3].Rows[i]["ApproverDesignation"].ToString();
                        row1.Cells.Add(cell4);
                        //   /* j*/++;
                        //}
                        tblapprove.Rows.Add(row1);

                        XRTableRow row2 = new XRTableRow();
                        XRTableCell cell5 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell5.CanGrow = true;
                        cell5.Width = 50;
                        cell5.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom;
                        cell5.Text = "Date:";
                        row2.Cells.Add(cell5);
                        XRTableCell cell6 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell6.CanGrow = true;
                        cell6.Width = 150;
                        cell6.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                        //cell6.Font = new Font("Times New Roman", 12, FontStyle.Underline);
                        cell6.Text = ds.Tables[3].Rows[i]["ApprovedDate"].ToString();
                        row2.Cells.Add(cell6);
                        XRTableCell cell7 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell7.CanGrow = true;
                        cell7.Width = 50;
                        cell7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                        cell7.Text = "Department:";
                        row2.Cells.Add(cell7);
                        XRTableCell cell8 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell8.CanGrow = true;
                        cell8.Width = 100;
                        cell8.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                        //cell8.Font = new Font("Times New Roman", 12, FontStyle.Underline);
                        cell8.Text = ds.Tables[3].Rows[i]["ApproverDepartment"].ToString();
                        row2.Cells.Add(cell8);
                        //   /* j*/++;
                        //}
                        tblapprove.Rows.Add(row2);

                        //XRTableRow row3 = new XRTableRow();
                        //tblapprove.Rows.Add(row3);
                        //((XRLabel)(rd.FindControl("xrTCApproverName", true))).Text = ds.Tables[3].Rows[i]["ApproverName"].ToString();
                        //((XRLabel)(rd.FindControl("xrTCApproverDate", true))).Text = ds.Tables[3].Rows[i]["ApprovedDate"].ToString();
                        //((XRLabel)(rd.FindControl("xrTCApproverDesignation", true))).Text = ds.Tables[3].Rows[i]["ApproverDesignation"].ToString();
                        //((XRLabel)(rd.FindControl("xrTCApproverDepartment", true))).Text = ds.Tables[3].Rows[i]["ApproverDepartment"].ToString();
                    }
                    tblapprove.Rows.RemoveAt(0);
                    //tblapprove.Rows.LastRow.Dispose();
                    tblapprove.EndInit();
                }
                else
                {
                    ((XRLabel)(firstPage.FindControl("xrLblApprovedby", true))).Visible = false;
                    ((XRTable)(firstPage.FindControl("xrTblApprover", true))).Visible = false;
                }
                if (ds.Tables[4].Rows.Count > 0)
                {
                    XRTable tblauthorize = ((XRTable)(firstPage.FindControl("xrTblAuthorizer", true)));
                    tblauthorize.BeginInit();
                    for (int i = 0; i < ds.Tables[4].Rows.Count; i++)
                    {
                        XRTableRow row1 = new XRTableRow();

                        //int j = 0;
                        //foreach (DataColumn dc in ds1.Tables[2].Columns)
                        //{
                        XRTableCell cell1 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell1.CanGrow = true;
                        cell1.Width = 50;
                        cell1.Borders = DevExpress.XtraPrinting.BorderSide.Left;
                        cell1.Text = "Name:";
                        row1.Cells.Add(cell1);
                        XRTableCell cell2 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell2.CanGrow = true;
                        cell2.Width = 150;
                        //cell2.Font = new Font("Times New Roman", 12, FontStyle.Underline);
                        cell2.Text = ds.Tables[4].Rows[i]["AuthorizerName"].ToString();
                        row1.Cells.Add(cell2);
                        XRTableCell cell3 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell3.CanGrow = true;
                        cell3.Width = 50;
                        cell3.Text = "Designation:";
                        row1.Cells.Add(cell3);
                        XRTableCell cell4 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell4.CanGrow = true;
                        cell4.Width = 100;
                        cell4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
                        //cell4.Font = new Font("Times New Roman", 12, FontStyle.Underline);
                        cell4.Text = ds.Tables[4].Rows[i]["AuthorizerDesignation"].ToString();
                        row1.Cells.Add(cell4);
                        //   /* j*/++;
                        //}
                        tblauthorize.Rows.Add(row1);

                        XRTableRow row2 = new XRTableRow();
                        XRTableCell cell5 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell5.CanGrow = true;
                        cell5.Width = 50;
                        cell5.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom;
                        //cell5.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                        cell5.Text = "Date:";
                        row2.Cells.Add(cell5);
                        XRTableCell cell6 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell6.CanGrow = true;
                        cell6.Width = 150;
                        cell6.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                        //cell6.Font = new Font("Times New Roman", 12, FontStyle.Underline);                       
                        cell6.Text = ds.Tables[4].Rows[i]["AuthorizedDate"].ToString();
                        row2.Cells.Add(cell6);
                        XRTableCell cell7 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell7.CanGrow = true;
                        cell7.Width = 50;
                        cell7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                        cell7.Text = "Department:";
                        row2.Cells.Add(cell7);
                        XRTableCell cell8 = new XRTableCell();
                        //XRBinding binding = new XRBinding("Text", ds1, ds1.Tables[2].Columns[j].ColumnName);
                        //cell.DataBindings.Add(binding);
                        cell8.CanGrow = true;
                        cell8.Width = 100;
                        cell8.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                        //cell8.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                        //cell8.Font = new Font("Times New Roman", 12, FontStyle.Underline);
                        cell8.Text = ds.Tables[4].Rows[i]["AuthorizerDepartment"].ToString();
                        row2.Cells.Add(cell8);
                        //   /* j*/++;
                        //}
                        tblauthorize.Rows.Add(row2);

                        XRTableRow row3 = new XRTableRow();
                        tblauthorize.Rows.Add(row3);
                        //((XRLabel)(rd.FindControl("xrTCApproverName", true))).Text = ds.Tables[3].Rows[i]["ApproverName"].ToString();
                        //((XRLabel)(rd.FindControl("xrTCApproverDate", true))).Text = ds.Tables[3].Rows[i]["ApprovedDate"].ToString();
                        //((XRLabel)(rd.FindControl("xrTCApproverDesignation", true))).Text = ds.Tables[3].Rows[i]["ApproverDesignation"].ToString();
                        //((XRLabel)(rd.FindControl("xrTCApproverDepartment", true))).Text = ds.Tables[3].Rows[i]["ApproverDepartment"].ToString();
                    }
                    tblauthorize.Rows.RemoveAt(0);
                    tblauthorize.Rows.LastRow.Dispose();
                    tblauthorize.EndInit();
                    //((XRLabel)(rd.FindControl("xrTCAuthorizerName", true))).Text = ds.Tables[4].Rows[0]["AuthorizerName"].ToString();
                    //((XRLabel)(rd.FindControl("xrTCAuthorizerDate", true))).Text = ds.Tables[4].Rows[0]["AuthorizedDate"].ToString();
                    //((XRLabel)(rd.FindControl("xrTCAuthorizerDesignation", true))).Text = ds.Tables[4].Rows[0]["AuthorizerDesignation"].ToString();
                    //((XRLabel)(rd.FindControl("xrTCAuthorizerDepartment", true))).Text = ds.Tables[4].Rows[0]["AuthorizerDepartment"].ToString();
                }
                else
                {
                    ((XRLabel)(firstPage.FindControl("xrLblAuthorizedby", true))).Visible = false;
                    ((XRTable)(firstPage.FindControl("xrTblAuthorizer", true))).Visible = false;
                }

                firstPage.CreateDocument();


                DocumentSecondPage secondPage = new DocumentSecondPage();
                DataTable dtSop = DMS_Bal.DMS_GetDocByID(vid);
                if (dtSop.Rows.Count > 0)
                {
                    ((XRTableCell)(secondPage.FindControl("xrTCDocumentName", true))).Text = dtSop.Rows[0]["DocumentName"].ToString();
                    ((XRTableCell)(secondPage.FindControl("xrTCDocumentNumber", true))).Text = dtSop.Rows[0]["DocumentNumber"].ToString();
                    ((XRTableCell)(secondPage.FindControl("xrTCDocumentDepartment", true))).Text = dtSop.Rows[0]["Department"].ToString();
                    ((XRLabel)(secondPage.FindControl("xrLblDocumentType", true))).Text = dtSop.Rows[0]["DocumentType"].ToString();
                    ((XRTableCell)(secondPage.FindControl("xrTCEffectiveDate", true))).Text = dtSop.Rows[0]["EffectiveDate"].ToString();
                    ((XRTableCell)(secondPage.FindControl("xrTCReviewDate", true))).Text = dtSop.Rows[0]["ExpirationDate"].ToString();
                    ((XRTableCell)(secondPage.FindControl("xrTCDocumentVersion", true))).Text = dtSop.Rows[0]["VersionNumber"].ToString();
                    DataRow drcontent = dtSop.Rows[0];
                    if (drcontent["FileType"].ToString().ToLower() == "rtf")
                    {
                        ((XRTable)(secondPage.FindControl("xrTable1", true))).Visible = false;
                        ((XRLabel)(secondPage.FindControl("xrLblDocumentType", true))).Visible = false;
                        ((XRPictureBox)(secondPage.FindControl("xrPictureBox1", true))).Visible = false;
                        MemoryStream stream = new MemoryStream((byte[])drcontent["Content"]);
                        StreamReader reader = new StreamReader(stream);
                        string text = reader.ReadToEnd();                       
                        //xrRichText1.SelectedRtf = @"{\rtf1 \par \page}";
                        ((XRRichText)(secondPage.FindControl("xrRichText1", true))).Rtf = text;
                       // ((XRRichText)(secondPage.FindControl("xrRichText1", true))).Visible = false;
            //            RichEditDocumentServer server = new RichEditDocumentServer();
            //            server.LoadDocument(stream, DocumentFormat.Rtf);
            //            DocumentRange[] ranges = server.Document.FindAll(DevExpress.Office.Characters.PageBreak.ToString(), DevExpress.XtraRichEdit.API.Native.SearchOptions.None);
            //            DocumentPosition dp = server.Document.Paragraphs[0].Range.Start;

            //            List<MyRtfObject> collection = new List<MyRtfObject>();
            //            foreach (DocumentRange dr in ranges)
            //            {
            //                DocumentRange tmpRange = server.Document.CreateRange(dp, dr.Start.ToInt() - dp.ToInt());
            //                collection.Add(new MyRtfObject() { RtfSplitContent = server.Document.GetRtfText(tmpRange) });
            //                dp = dr.End;
            //            }
            //            DocumentRange tmpRange2 = server.Document.CreateRange(dp, server.Document.Paragraphs[server.Document.Paragraphs.Count - 1].Range.End.ToInt() - dp.ToInt());
            //            collection.Add(new MyRtfObject() { RtfSplitContent = server.Document.GetRtfText(tmpRange2) });
            //            ((XRRichText)(secondPage.FindControl("xrRichText1", true))).DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            //new DevExpress.XtraReports.UI.XRBinding("Rtf", null, "RtfSplitContent")});
                      //  secondPage.DataSource = collection;
                        secondPage.CreateDocument();
                        firstPage.Pages.AddRange(secondPage.Pages);

                        //XtraReport1 report1 = new XtraReport1();
                        //((XRRichText)(report1.FindControl("xrRichText1", true))).Rtf = text;
                        //report1.CreateDocument();
                        //firstPage.Pages.AddRange(report1.Pages);
                        ASPxWebDocumentViewer1.OpenReport(firstPage);
                        ASPxWebDocumentViewer1.DataBind();
                    }
                    else if (drcontent["FileType"].ToString().ToLower() == "pdf")
                    {
                        //StringBuilder sbHtml = new StringBuilder();
                        //sbHtml.Append("<html><body>");
                        //sbHtml.Append("<embed src=\"" + ResolveUrl("~/ASPXHandlers/Handler1.ashx") + "?SopId=" + drcontent["DocumentID"].ToString() + "#toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" height=\"650px\" />");
                        //sbHtml.Append("</body></html>");
                        //((XRRichText)(secondPage.FindControl("xrRichText1", false))).Html = sbHtml.ToString();
                        // File.WriteAllBytes(Server.MapPath(@"~/Files/" + drcontent["FileName"]), (byte[])drcontent["Content"]);
                        //((XRRichText)(secondPage.FindControl("xrRichText1", false))).LoadFile(Server.MapPath(@"~/Files/" + drcontent["FileName"]));

                        //((XRRichText)(secondPage.FindControl("xrRichText1", false))).Text = Convert.ToBase64String((byte[])drcontent["Content"]);
                        ((XRRichText)(secondPage.FindControl("xrRichText1", true))).Visible = false;
                        ((XRTable)(secondPage.FindControl("xrTable1", true))).Visible = false;
                        ((XRLabel)(secondPage.FindControl("xrLblDocumentType", true))).Visible = false;                        
                        ((XRPictureBox)(secondPage.FindControl("xrPictureBox1", true))).Visible = false;
                        PdfDocumentProcessor _documentProcessor = new PdfDocumentProcessor();
                        using (MemoryStream stream = new MemoryStream((byte[])drcontent["Content"]))
                        {
                            _documentProcessor.LoadDocument(stream, true);

                        }
                        XRTable pdfTable = new XRTable();
                        pdfTable.WidthF = 810;
                        if (Directory.Exists(Server.MapPath(@"~/DMS/pdfImages")))
                        {
                            //Delete all files from the Directory
                            foreach (string file in Directory.GetFiles(Server.MapPath(@"~/DMS/pdfImages")))
                            {
                                File.Delete(file);
                            }
                        }
                            for (int pageNumber = 1; pageNumber <= _documentProcessor.Document.Pages.Count; pageNumber++)
                        {
                            using (Bitmap bitmap = _documentProcessor.CreateBitmap(pageNumber, 1080))
                            {
                                string outputFileName = Server.MapPath(@"~/DMS/pdfImages/" + pageNumber.ToString() + ".bmp");
                                using (MemoryStream memory = new MemoryStream())
                                {
                                    using (FileStream fs = new FileStream(outputFileName, FileMode.Create, FileAccess.ReadWrite))
                                    {
                                        XRTableRow r1 = new XRTableRow();
                                        XRTableCell c1 = new XRTableCell();
                                        XRPictureBox pdfImage = new XRPictureBox();
                                        bitmap.Save(memory, ImageFormat.Bmp);
                                        byte[] bytes = memory.ToArray();
                                        fs.Write(bytes, 0, bytes.Length);
                                        pdfImage.Name = "pdc" + pageNumber.ToString();
                                        pageNumber.ToString();
                                        pdfImage.ImageUrl = outputFileName;
                                        // pdfImage.Image= bitmap;
                                        // Set its size.
                                        pdfImage.SizeF = new SizeF(810, 950);
                                        // Set its size mode.
                                        pdfImage.Sizing = DevExpress.XtraPrinting.ImageSizeMode.AutoSize;
                                        c1.CanGrow = true;
                                        c1.Width = 810;
                                        c1.Controls.Add(pdfImage);
                                        r1.Cells.Add(c1);
                                        r1.SizeF = new SizeF(810, 950);
                                        pdfTable.Rows.Add(r1);

                                    }
                                }
                            }

                        }
                        secondPage.Bands[BandKind.Detail].Controls.Add(pdfTable);
                        // ((XRPictureBox)(secondPage.FindControl("xrPictureBox2", true))).Image = ByteToImage((byte[])drcontent["Content"]);
                        secondPage.CreateDocument();
                        //firstPage.Pages.AddRange(secondPage.Pages);
                        ASPxWebDocumentViewer1.OpenReport(secondPage);
                        ASPxWebDocumentViewer1.DataBind();
                    }
                    //xrRichText1

                }

            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }
        public static Bitmap ByteToImage(byte[] blob)
        {
            MemoryStream mStream = new MemoryStream();
            byte[] pData = blob;
            mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
            Bitmap bm = new Bitmap(mStream, false);
            mStream.Dispose();
            return bm;
        }
        #region documentviewer
        protected void ASPxDocumentViewer1_CacheReportDocument(object sender, DevExpress.XtraReports.Web.CacheReportDocumentEventArgs e)
        {
            e.Key = Guid.NewGuid().ToString();
            Page.Session[e.Key] = e.SaveDocumentToMemoryStream();
        }

        protected void ASPxDocumentViewer1_RestoreReportDocumentFromCache(object sender, DevExpress.XtraReports.Web.RestoreReportDocumentFromCacheEventArgs e)
        {
            Stream stream = Page.Session[e.Key] as Stream;
            if (stream != null)
                e.RestoreDocumentFromStream(stream);
        }
        #endregion documentviewer
    }

    public class MyRtfObject
    {
        public string RtfSplitContent { get; set; }
    }
}