﻿namespace AizantIT_PharmaApp.DMS.DevReports
{
    partial class DocumentFirstPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLblReviewedby = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTblAuthorizer = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTCAuthorizerDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTCAuthorizerDepartment = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLblAuthorizedby = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTblApprover = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLblApprovedby = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTblReviewer = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTblAuthor = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTCAuthorDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTCAuthorDepartment = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLblPreparedby = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLblDocumentType = new DevExpress.XtraReports.UI.XRLabel();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTCDocumentName = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTCDocumentNumber = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTCDocumentVersion = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTCEffectiveDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTCDocumentDepartment = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTCReviewDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTblAuthorizer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTblApprover)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTblReviewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTblAuthor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLblReviewedby,
            this.xrTblAuthorizer,
            this.xrLblAuthorizedby,
            this.xrTblApprover,
            this.xrLblApprovedby,
            this.xrTblReviewer,
            this.xrTblAuthor,
            this.xrLblPreparedby});
            this.Detail.HeightF = 380.2083F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLblReviewedby
            // 
            this.xrLblReviewedby.BackColor = System.Drawing.Color.LightGray;
            this.xrLblReviewedby.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLblReviewedby.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLblReviewedby.LocationFloat = new DevExpress.Utils.PointFloat(54.25355F, 74.87501F);
            this.xrLblReviewedby.Name = "xrLblReviewedby";
            this.xrLblReviewedby.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100F);
            this.xrLblReviewedby.SizeF = new System.Drawing.SizeF(747.92F, 30F);
            this.xrLblReviewedby.StylePriority.UseBackColor = false;
            this.xrLblReviewedby.StylePriority.UseBorders = false;
            this.xrLblReviewedby.StylePriority.UseFont = false;
            this.xrLblReviewedby.StylePriority.UsePadding = false;
            this.xrLblReviewedby.StylePriority.UseTextAlignment = false;
            this.xrLblReviewedby.Text = "Reviewed by";
            this.xrLblReviewedby.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTblAuthorizer
            // 
            this.xrTblAuthorizer.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTblAuthorizer.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTblAuthorizer.LocationFloat = new DevExpress.Utils.PointFloat(54.25003F, 225.875F);
            this.xrTblAuthorizer.Name = "xrTblAuthorizer";
            this.xrTblAuthorizer.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 10, 10, 100F);
            this.xrTblAuthorizer.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTblAuthorizer.SizeF = new System.Drawing.SizeF(747.92F, 30F);
            this.xrTblAuthorizer.StylePriority.UseBorders = false;
            this.xrTblAuthorizer.StylePriority.UseFont = false;
            this.xrTblAuthorizer.StylePriority.UsePadding = false;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell17,
            this.xrTCAuthorizerDate,
            this.xrTableCell19,
            this.xrTCAuthorizerDepartment});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseBorders = false;
            this.xrTableCell17.StylePriority.UseFont = false;
            this.xrTableCell17.Weight = 0.61142102794785036D;
            // 
            // xrTCAuthorizerDate
            // 
            this.xrTCAuthorizerDate.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTCAuthorizerDate.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Underline);
            this.xrTCAuthorizerDate.Name = "xrTCAuthorizerDate";
            this.xrTCAuthorizerDate.StylePriority.UseBorders = false;
            this.xrTCAuthorizerDate.StylePriority.UseFont = false;
            this.xrTCAuthorizerDate.Weight = 0.82868983175960842D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseBorders = false;
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.Weight = 0.61281429655888919D;
            // 
            // xrTCAuthorizerDepartment
            // 
            this.xrTCAuthorizerDepartment.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTCAuthorizerDepartment.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Underline);
            this.xrTCAuthorizerDepartment.Name = "xrTCAuthorizerDepartment";
            this.xrTCAuthorizerDepartment.StylePriority.UseBorders = false;
            this.xrTCAuthorizerDepartment.StylePriority.UseFont = false;
            this.xrTCAuthorizerDepartment.Weight = 0.947074843733652D;
            // 
            // xrLblAuthorizedby
            // 
            this.xrLblAuthorizedby.BackColor = System.Drawing.Color.LightGray;
            this.xrLblAuthorizedby.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLblAuthorizedby.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLblAuthorizedby.LocationFloat = new DevExpress.Utils.PointFloat(54.26027F, 195.875F);
            this.xrLblAuthorizedby.Name = "xrLblAuthorizedby";
            this.xrLblAuthorizedby.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100F);
            this.xrLblAuthorizedby.SizeF = new System.Drawing.SizeF(747.92F, 30F);
            this.xrLblAuthorizedby.StylePriority.UseBackColor = false;
            this.xrLblAuthorizedby.StylePriority.UseBorders = false;
            this.xrLblAuthorizedby.StylePriority.UseFont = false;
            this.xrLblAuthorizedby.StylePriority.UsePadding = false;
            this.xrLblAuthorizedby.StylePriority.UseTextAlignment = false;
            this.xrLblAuthorizedby.Text = "Authorized by";
            this.xrLblAuthorizedby.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTblApprover
            // 
            this.xrTblApprover.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTblApprover.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTblApprover.LocationFloat = new DevExpress.Utils.PointFloat(54.25356F, 165.875F);
            this.xrTblApprover.Name = "xrTblApprover";
            this.xrTblApprover.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 10, 10, 100F);
            this.xrTblApprover.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTblApprover.SizeF = new System.Drawing.SizeF(747.92F, 30F);
            this.xrTblApprover.StylePriority.UseBorders = false;
            this.xrTblApprover.StylePriority.UseFont = false;
            this.xrTblApprover.StylePriority.UsePadding = false;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell10,
            this.xrTableCell12,
            this.xrTableCell11});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.Text = "xrTableCell9";
            this.xrTableCell9.Weight = 1.5242710096659549D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Underline);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.Text = "xrTableCell10";
            this.xrTableCell10.Weight = 2.0660060250546293D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.Text = "xrTableCell12";
            this.xrTableCell12.Weight = 1.5277445612772291D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Underline);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseBorders = false;
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.Text = "xrTableCell11";
            this.xrTableCell11.Weight = 2.36114435859203D;
            // 
            // xrLblApprovedby
            // 
            this.xrLblApprovedby.BackColor = System.Drawing.Color.LightGray;
            this.xrLblApprovedby.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLblApprovedby.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLblApprovedby.LocationFloat = new DevExpress.Utils.PointFloat(54.26024F, 134.875F);
            this.xrLblApprovedby.Name = "xrLblApprovedby";
            this.xrLblApprovedby.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100F);
            this.xrLblApprovedby.SizeF = new System.Drawing.SizeF(747.92F, 30F);
            this.xrLblApprovedby.StylePriority.UseBackColor = false;
            this.xrLblApprovedby.StylePriority.UseBorders = false;
            this.xrLblApprovedby.StylePriority.UseFont = false;
            this.xrLblApprovedby.StylePriority.UsePadding = false;
            this.xrLblApprovedby.StylePriority.UseTextAlignment = false;
            this.xrLblApprovedby.Text = "Approved by";
            this.xrLblApprovedby.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTblReviewer
            // 
            this.xrTblReviewer.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTblReviewer.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTblReviewer.LocationFloat = new DevExpress.Utils.PointFloat(54.2569F, 104.875F);
            this.xrTblReviewer.Name = "xrTblReviewer";
            this.xrTblReviewer.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 10, 10, 100F);
            this.xrTblReviewer.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTblReviewer.SizeF = new System.Drawing.SizeF(747.92F, 30F);
            this.xrTblReviewer.StylePriority.UseBorders = false;
            this.xrTblReviewer.StylePriority.UseFont = false;
            this.xrTblReviewer.StylePriority.UsePadding = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.xrTableCell3,
            this.xrTableCell8,
            this.xrTableCell6});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 10, 10, 100F);
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UsePadding = false;
            this.xrTableCell2.Text = "xrTableCell2";
            this.xrTableCell2.Weight = 0.83774905355949925D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Underline);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.Text = "xrTableCell3";
            this.xrTableCell3.Weight = 1.1355333706462185D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.Text = "xrTableCell8";
            this.xrTableCell8.Weight = 0.83965743123836267D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Underline);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.Text = "xrTableCell6";
            this.xrTableCell6.Weight = 1.2977469293053974D;
            // 
            // xrTblAuthor
            // 
            this.xrTblAuthor.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTblAuthor.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTblAuthor.LocationFloat = new DevExpress.Utils.PointFloat(54.25003F, 44.87502F);
            this.xrTblAuthor.Name = "xrTblAuthor";
            this.xrTblAuthor.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 10, 10, 100F);
            this.xrTblAuthor.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTblAuthor.SizeF = new System.Drawing.SizeF(747.92F, 30F);
            this.xrTblAuthor.StylePriority.UseBorders = false;
            this.xrTblAuthor.StylePriority.UseFont = false;
            this.xrTblAuthor.StylePriority.UsePadding = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.xrTCAuthorDate,
            this.xrTableCell7,
            this.xrTCAuthorDepartment});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.Weight = 0.611421027950048D;
            // 
            // xrTCAuthorDate
            // 
            this.xrTCAuthorDate.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Underline);
            this.xrTCAuthorDate.Name = "xrTCAuthorDate";
            this.xrTCAuthorDate.StylePriority.UseFont = false;
            this.xrTCAuthorDate.Weight = 0.8286905662194981D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.Weight = 0.61281368450934748D;
            // 
            // xrTCAuthorDepartment
            // 
            this.xrTCAuthorDepartment.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTCAuthorDepartment.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Underline);
            this.xrTCAuthorDepartment.Name = "xrTCAuthorDepartment";
            this.xrTCAuthorDepartment.StylePriority.UseBorders = false;
            this.xrTCAuthorDepartment.StylePriority.UseFont = false;
            this.xrTCAuthorDepartment.Weight = 0.94707472132110637D;
            // 
            // xrLblPreparedby
            // 
            this.xrLblPreparedby.BackColor = System.Drawing.Color.LightGray;
            this.xrLblPreparedby.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLblPreparedby.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLblPreparedby.LocationFloat = new DevExpress.Utils.PointFloat(54.25018F, 14.87503F);
            this.xrLblPreparedby.Name = "xrLblPreparedby";
            this.xrLblPreparedby.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100F);
            this.xrLblPreparedby.SizeF = new System.Drawing.SizeF(747.92F, 30F);
            this.xrLblPreparedby.StylePriority.UseBackColor = false;
            this.xrLblPreparedby.StylePriority.UseBorders = false;
            this.xrLblPreparedby.StylePriority.UseFont = false;
            this.xrLblPreparedby.StylePriority.UsePadding = false;
            this.xrLblPreparedby.StylePriority.UseTextAlignment = false;
            this.xrLblPreparedby.Text = "Prepared by";
            this.xrLblPreparedby.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.ImageUrl = "C:\\Users\\cr011\\source\\repos\\AizantPharmaERP_RepoBareTMS\\AizantIT_PharmaApp\\Images" +
    "\\UserLogin\\Aizant_Logo.png";
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(645.3192F, 22.50001F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(137.9307F, 32.375F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // xrLblDocumentType
            // 
            this.xrLblDocumentType.Angle = 90F;
            this.xrLblDocumentType.Font = new System.Drawing.Font("Times New Roman", 30F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLblDocumentType.LocationFloat = new DevExpress.Utils.PointFloat(0.0102361F, 10.00001F);
            this.xrLblDocumentType.Name = "xrLblDocumentType";
            this.xrLblDocumentType.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblDocumentType.SizeF = new System.Drawing.SizeF(54.25003F, 160.8334F);
            this.xrLblDocumentType.StylePriority.UseFont = false;
            this.xrLblDocumentType.Text = "xrLblDocumentType";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.xrLblDocumentType,
            this.xrPictureBox1});
            this.PageHeader.HeightF = 170.8334F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(54.26027F, 70.83342F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow5,
            this.xrTableRow7});
            this.xrTable1.SizeF = new System.Drawing.SizeF(749.42F, 89.99998F);
            this.xrTable1.StylePriority.UseBorders = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTCDocumentName,
            this.xrTableCell4});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "Document Name:";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell1.Weight = 0.52715867135780359D;
            // 
            // xrTCDocumentName
            // 
            this.xrTCDocumentName.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTCDocumentName.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTCDocumentName.Name = "xrTCDocumentName";
            this.xrTCDocumentName.StylePriority.UseBorders = false;
            this.xrTCDocumentName.StylePriority.UseFont = false;
            this.xrTCDocumentName.StylePriority.UseTextAlignment = false;
            this.xrTCDocumentName.Text = "xrTCDocumentName";
            this.xrTCDocumentName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTCDocumentName.Weight = 2.0599113558265985D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2});
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "xrTableCell2";
            this.xrTableCell4.Weight = 0.412929972815598D;
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPageInfo2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 0F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(101.6389F, 30F);
            this.xrPageInfo2.StylePriority.UseBorders = false;
            this.xrPageInfo2.StylePriority.UseFont = false;
            this.xrPageInfo2.StylePriority.UseTextAlignment = false;
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrPageInfo2.TextFormatString = "Page {0} of {1}";
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTCDocumentNumber,
            this.xrTableCell16,
            this.xrTCDocumentVersion,
            this.xrTableCell14,
            this.xrTCEffectiveDate});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseBorders = false;
            this.xrTableCell13.StylePriority.UseFont = false;
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "Document Number:";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell13.Weight = 0.61015628353836582D;
            // 
            // xrTCDocumentNumber
            // 
            this.xrTCDocumentNumber.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTCDocumentNumber.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTCDocumentNumber.Name = "xrTCDocumentNumber";
            this.xrTCDocumentNumber.StylePriority.UseBorders = false;
            this.xrTCDocumentNumber.StylePriority.UseFont = false;
            this.xrTCDocumentNumber.StylePriority.UseTextAlignment = false;
            this.xrTCDocumentNumber.Text = "xrTCDocumentNumber";
            this.xrTCDocumentNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTCDocumentNumber.Weight = 0.82703119355654664D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.StylePriority.UseFont = false;
            this.xrTableCell16.StylePriority.UseTextAlignment = false;
            this.xrTableCell16.Text = "Version:";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell16.Weight = 0.26132661261313D;
            // 
            // xrTCDocumentVersion
            // 
            this.xrTCDocumentVersion.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTCDocumentVersion.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTCDocumentVersion.Name = "xrTCDocumentVersion";
            this.xrTCDocumentVersion.StylePriority.UseBorders = false;
            this.xrTCDocumentVersion.StylePriority.UseFont = false;
            this.xrTCDocumentVersion.StylePriority.UseTextAlignment = false;
            this.xrTCDocumentVersion.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTCDocumentVersion.Weight = 0.16264202027177743D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "Effective Date:";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell14.Weight = 0.50490952184763094D;
            // 
            // xrTCEffectiveDate
            // 
            this.xrTCEffectiveDate.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTCEffectiveDate.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTCEffectiveDate.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.xrTCEffectiveDate.Name = "xrTCEffectiveDate";
            this.xrTCEffectiveDate.StylePriority.UseBorders = false;
            this.xrTCEffectiveDate.StylePriority.UseFont = false;
            this.xrTCEffectiveDate.StylePriority.UseForeColor = false;
            this.xrTCEffectiveDate.StylePriority.UseTextAlignment = false;
            this.xrTCEffectiveDate.Text = "xrTCEffectiveDate";
            this.xrTCEffectiveDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTCEffectiveDate.Weight = 0.63393436817254889D;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15,
            this.xrTCDocumentDepartment,
            this.xrTableCell18,
            this.xrTCReviewDate});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "Department:";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell15.Weight = 0.61015622245597112D;
            // 
            // xrTCDocumentDepartment
            // 
            this.xrTCDocumentDepartment.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTCDocumentDepartment.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTCDocumentDepartment.Name = "xrTCDocumentDepartment";
            this.xrTCDocumentDepartment.StylePriority.UseBorders = false;
            this.xrTCDocumentDepartment.StylePriority.UseFont = false;
            this.xrTCDocumentDepartment.StylePriority.UseTextAlignment = false;
            this.xrTCDocumentDepartment.Text = "xrTCDocumentDepartment";
            this.xrTCDocumentDepartment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTCDocumentDepartment.Weight = 1.2509998264414539D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseBorders = false;
            this.xrTableCell18.StylePriority.UseFont = false;
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.Text = "Review Date:";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell18.Weight = 0.50490915535326264D;
            // 
            // xrTCReviewDate
            // 
            this.xrTCReviewDate.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTCReviewDate.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTCReviewDate.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.xrTCReviewDate.Name = "xrTCReviewDate";
            this.xrTCReviewDate.StylePriority.UseBorders = false;
            this.xrTCReviewDate.StylePriority.UseFont = false;
            this.xrTCReviewDate.StylePriority.UseForeColor = false;
            this.xrTCReviewDate.StylePriority.UseTextAlignment = false;
            this.xrTCReviewDate.Text = "xrTCReviewDate";
            this.xrTCReviewDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTCReviewDate.Weight = 0.63393479574931177D;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel11,
            this.xrLabel10});
            this.PageFooter.HeightF = 110.5F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrLabel11
            // 
            this.xrLabel11.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(54.25014F, 32.99999F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(747.9166F, 36.83332F);
            this.xrLabel11.StylePriority.UseBorders = false;
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.Text = "The contents of this document are confidential and proprietary to Aizant Drug Res" +
    "earch Solutions Pvt. Ltd. Any unauthorized use, disclosure or reproduction is st" +
    "rictly prohibited.";
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(54.24998F, 9.999974F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(747.9166F, 23F);
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "CONFIDENTIAL AND PROPRIETARY";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DocumentFirstPage
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.PageFooter});
            this.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.Margins = new System.Drawing.Printing.Margins(2, 2, 0, 0);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "17.2";
            this.Watermark.ForeColor = System.Drawing.Color.DarkGray;
            this.Watermark.Text = "Restricted circulation / For Reference";
            this.Watermark.TextTransparency = 176;
            ((System.ComponentModel.ISupportInitialize)(this.xrTblAuthorizer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTblApprover)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTblReviewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTblAuthor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRTable xrTblAuthorizer;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTCAuthorizerDate;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTCAuthorizerDepartment;
        private DevExpress.XtraReports.UI.XRLabel xrLblAuthorizedby;
        private DevExpress.XtraReports.UI.XRTable xrTblApprover;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRLabel xrLblApprovedby;
        private DevExpress.XtraReports.UI.XRTable xrTblReviewer;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRLabel xrLblReviewedby;
        private DevExpress.XtraReports.UI.XRTable xrTblAuthor;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTCAuthorDate;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTCAuthorDepartment;
        private DevExpress.XtraReports.UI.XRLabel xrLblPreparedby;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel xrLblDocumentType;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTCDocumentName;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTCDocumentVersion;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTCEffectiveDate;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTCDocumentDepartment;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTCReviewDate;
        private DevExpress.XtraReports.UI.XRTableCell xrTCDocumentNumber;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
    }
}
