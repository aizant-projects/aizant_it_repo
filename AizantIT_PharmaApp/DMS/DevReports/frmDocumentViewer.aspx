﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmDocumentViewer.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DevReports.frmDocumentViewer" %>
<%@ Register Assembly="DevExpress.XtraReports.v18.1.Web.WebForms, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        .dxrd-image-print{
            display:none;
        }
        .dxrd-image-print-page{
            display:none;
        }
        .dxrd-image-export-to{
            display:none;
        }
        .dx-menu .dx-menu-item-expanded{
            display:none;
        }
    </style>
</head>
<body oncontextmenu="return false">
    <form id="form1" runat="server">
        <div>
            <dx:ASPxWebDocumentViewer ID="ASPxWebDocumentViewer1" runat="server" OnCacheReportDocument="ASPxDocumentViewer1_CacheReportDocument" OnRestoreReportDocumentFromCache="ASPxDocumentViewer1_RestoreReportDocumentFromCache">
            </dx:ASPxWebDocumentViewer>
        </div>
        <script>
             //To disable ctrl+s
             document.addEventListener("keydown", function (e) {
                 if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
                     e.preventDefault();
                     //alert('captured');
                 }
             }, false);

             // To disable F12
            $(document).keydown(function (event) {
                if (event.keyCode == 123) {
                    return false;
                }
                //else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I        
                //    return false;
                //}
             });

            //To disable ctrl+P
            jQuery(document).bind("keyup keydown", function (e) {
                if (e.ctrlKey && e.keyCode == 80) {
                    return false;
                }
            });
        </script>
    </form>
</body>
</html>
