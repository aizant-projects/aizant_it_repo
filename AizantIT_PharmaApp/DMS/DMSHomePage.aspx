﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="DMSHomePage.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DMSHomePage" %>

<%@ Register Assembly="DevExpress.Web.Bootstrap.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.Bootstrap" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v18.1.Web, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v18.1.Web, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web.Designer" TagPrefix="dxchartdesigner" %>
<%@ Register Assembly="DevExpress.Web.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1">   
    <script src="<%=ResolveUrl("~/DevexScripts/jquery-ui.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/cldr.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/cldr/event.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/cldr/supplemental.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/globalize.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/globalize/currency.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/globalize/date.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/globalize/message.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/globalize/number.js")%>"></script>
    <script src="<%=ResolveUrl("~/DevexScripts/knockout.js")%>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
        <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
    </div>
    <div id="divMainContainer" runat="server" visible="true">
        <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none float-left">
            <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none panel_list_bg float-left" id="content" style="position: relative; z-index: 9;">
                <div class="col-lg-3 col-md-3 col-sm-6 col-6 float-left padding-right_div f1_container" id="divApproverQA" runat="server" visible="false" style="position: relative; z-index: 99;">
                    <div class=" f1_card col-lg-12  float-left">
                        <div class="front face outer_dashboard float-left">
                            <div style="position: absolute; right: 0; opacity: 0.2; top: 20px;">
                                <img src="<%=ResolveUrl("~/Images/common_dashboard/approve_big.png")%>" class="" alt="Approve" />
                            </div>
                            <div class="col-lg-12 col-12 col-sm-12 col-md-12 header_text_dshboard float-left">Approval Documents</div>
                            <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none float-left">
                                <div class="col-lg-6 col-6 col-sm-6 col-md-6  padding-none float-left">
                                    <img src="<%=ResolveUrl("~/Images/common_dashboard/approve_small.png")%>" class="dashboard-image" alt="Approve" />
                                </div>
                                <div class="col-lg-6 col-6 col-sm-6 col-md-6  float-left">
                                    <span class="link_dashboard dashboard_frontpanel_value ">
                                        <asp:Label ID="lblApproverQA" runat="server" Text="0"></asp:Label></span>
                                </div>
                            </div>
                        </div>
                        <div class="back face center outer_dashboard padding-none float-left" style="height: 120px !important;">
                            <div class="col-lg-12 col-12 col-sm-12 col-md-12 float-left padding-none">
                                <div class="col-lg-12 padding-none float-left">
                                    <div class="col-lg-12 padding-none float-left">
                                        <div class=" col-lg-12 padding-none  float-right ">
                                            <ul class="col-lg-12 col-12 col-sm-12 col-md-12 flip_list style-2 padding-none float-left">
                                                <li>
                                                    <a href="/DMS/DocumentApprover/Initiated_Documents.aspx?rtp=1" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divNewApproverQA" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Initiation Request - New</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblApproverQANew" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentApprover/Initiated_Documents.aspx?rtp=2" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divRevisionApproverQA" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Initiation Request - Revision</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblApproverQARevision" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentApprover/Initiated_Documents.aspx?rtp=3" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divWithdrawApproverQA" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Initiation Request - Withdraw</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblApproverQAWithdraw" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentApprover/Initiated_Documents.aspx?rtp=5" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divRenewApproverQA" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Initiation Request - Renew</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblApproverQARenew" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentController/DocWithdrawList.aspx" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divWithdrawController" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Approve Withdraw Document</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblControllerWithdraw" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentCreator/Create_Document.aspx?tab=1&rtp=1" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divNewApproverCreator" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Create New Document</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblApproverCreatorNew" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentCreator/Create_Document.aspx?tab=1&rtp=2" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divRevisionApproverCreator" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Document Revision</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblApproverCreatorRevision" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentCreator/Create_Document.aspx?tab=1&rtp=5" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divRenewApproverCreator" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Document Renew</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblApproverCreatorRenew" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentReviewer/Review_Document.aspx?rtp=1" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divNewReviewer" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Review New Document</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblReviewerNew" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentReviewer/Review_Document.aspx?rtp=2" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divRevisionReviewer" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Review Revision Document</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblReviewerRevision" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentReviewer/Review_Document.aspx?rtp=5" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divRenewReviewer" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Review Renew Document</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblReviewerRenew" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentApprover/Approve_Documents.aspx?rtp=1" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divNewApproverDoc" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Approve New Document</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblApproverDocNew" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentApprover/Approve_Documents.aspx?rtp=2" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divRevisionApproverDoc" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Approve Revision Document</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblApproverDocRevision" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentApprover/Approve_Documents.aspx?rtp=5" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divRenewApproverDoc" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Approve Renew Document</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblApproverDocRenew" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentAuthorizer/Authorize_Documents.aspx?rtp=1" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divNewAuthorize" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Authorize New Document</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblAuthorizeNew" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentAuthorizer/Authorize_Documents.aspx?rtp=2" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divRevisionAuthorize" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Authorize Revision Document</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblAuthorizeRevision" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentController/FormApproverList.aspx?rtp=1" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divControllerApproveFormCreation" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Approve New Form Creation</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblControllerApproveFormCreation" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentController/FormApproverList.aspx?rtp=2" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divControllerApproveRevisionFormCreation" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Approve Revision Form Creation</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblControllerApproveRevisionFormCreation" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentController/FormApproverList.aspx?rtp=3" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divControllerApproveWithdrawFormCreation" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Approve Withdraw Form Initiation</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblControllerApproveWithdrawFormCreation" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentController/FormReqControllerList.aspx" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divControllerApproveFormRequest" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Approve Form Print Request</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblControllerApproveFormRequest" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentController/DocReqControllerList.aspx" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divControllerApproveDocumentRequest" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Approve Document Print Request</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblControllerApproveDocumentRequest" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/PrintReviewer/FormReqPrintReviewerList.aspx" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divPReviewerApproveFormRequest" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Review Form Print Request</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblPReviewerApproveFormRequest" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/PrintReviewer/DocReqPrintReviewerList.aspx" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divPReviewerApproveDocumentRequest" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Review Document Print Request</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblPReviewerApproveDocumentRequest" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/FormPrintReqMainList.aspx?verify=t" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divReturnVerifyController" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Verify Form Return</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblReturnVerifyForms" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-6 float-left padding-right_div f1_container" id="divRevertedInitiation" runat="server" visible="false" style="position: relative; z-index: 9;">
                    <div class=" f1_card col-lg-12  float-left">
                        <div class="front face outer_dashboard1 float-left">
                            <div style="position: absolute; right: 0; top: 20px;">
                                <img src="<%=ResolveUrl("~/Images/common_dashboard/reverted.png")%>" class="" alt="Approve" />
                            </div>
                            <div class="col-lg-12 col-12 col-sm-12 col-md-12 header_text_dshboard">Reverted Documents</div>
                            <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none float-left">
                                <div class="col-lg-6 col-6 col-sm-6 col-md-6  padding-none float-left">
                                    <img src="<%=ResolveUrl("~/Images/common_dashboard/revetred_small.png")%>" class="dashboard-image" alt="Approve" />
                                </div>
                                <div class="col-lg-6 col-6 col-sm-6 col-md-6 float-left">
                                    <span class="link_dashboard dashboard_frontpanel_value">
                                        <asp:Label ID="lblRevertedinitiation" runat="server" Text="0"></asp:Label></span>
                                </div>
                            </div>
                        </div>
                        <div class="back face center outer_dashboard1 padding-none float-left">
                            <div class="col-lg-12 col-12 col-sm-12 col-md-12 float-left padding-none">
                                <div class="col-lg-12 padding-none float-left">
                                    <div class="col-lg-12 padding-none float-left">
                                        <div class=" col-lg-12 padding-none  float-right ">
                                            <ul class="col-lg-12 col-12 col-sm-12 col-md-12 flip_list style-2  padding-none">
                                                <li>
                                                    <a href="/DMS/DocumentIntiator/Reverted_Documents.aspx?rtp=1" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divNewRevertedInitiation" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">New Document Request</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblRevertedinitiationNew" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentIntiator/Reverted_Documents.aspx?rtp=2" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divRevisionRevertedInitiation" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Revision Document Request</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblRevertedinitiationRevision" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentIntiator/Reverted_Documents.aspx?rtp=3" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divWithdrawRevertedInitiation" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Withdraw Document Request</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblRevertedinitiationWithdaw" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentIntiator/Reverted_Documents.aspx?rtp=5" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divRenewRevertedInitiation" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Renew Document Request</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblRevertedinitiationRenew" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentCreator/Create_Document.aspx?tab=2&rtp=1" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divNewCreatorRevert" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">New Document</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblCreatorRevertNew" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentCreator/Create_Document.aspx?tab=2&rtp=2" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divRevisionCreatorRevert" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Revision Document</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblCreatorRevertRevision" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentCreator/Create_Document.aspx?tab=2&rtp=5" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divRenewCreatorRevert" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Renew Document</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblCreatorRevertRenew" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DMSAdmin/FormCreatorList.aspx?rtp=1" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divAdminRevertForms" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">New Forms</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblAdminRevertForms" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DMSAdmin/FormCreatorList.aspx?rtp=2" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divAdminRevertRevisionForms" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Revision Forms</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblAdminRevertRevisionForms" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DMSAdmin/FormCreatorList.aspx?rtp=3" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divAdminRevertWithdrawForms" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Withdraw Forms</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblAdminRevertWithdrawForms" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-6 float-left padding-right_div f1_container float-left" id="dvEfective" runat="server" visible="false" style="position: relative; z-index: 9;">
                    <div class=" f1_card col-lg-12  float-left">
                        <div class="front face outer_dashboard2 float-left">
                            <div style="position: absolute; right: 0; opacity: 0.4; top: 20px;">
                                <img src="<%=ResolveUrl("~/Images/common_dashboard/calender_big.png")%>" class="" alt="Approve" />
                            </div>
                            <div class="col-lg-12 col-12 col-sm-12 col-md-12 header_text_dshboard">Calendar Actions</div>
                            <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none float-left">
                                <div class="col-lg-6 col-6 col-sm-6 col-md-6  padding-none float-left">
                                    <img src="<%=ResolveUrl("~/Images/common_dashboard/calender_small.png")%>" class="dashboard-image" alt="Approve" />
                                </div>
                                <div class="col-lg-6 col-6 col-sm-6 col-md-6 float-left ">
                                    <span class="link_dashboard dashboard_frontpanel_value">
                                        <asp:Label ID="lblEffective" runat="server" Text="0"></asp:Label></span>
                                </div>
                            </div>
                        </div>
                        <div class="back face center outer_dashboard2 padding-none float-left">
                            <div class="col-lg-12 col-12 col-sm-12 col-md-12 float-left padding-none">
                                <div class="col-lg-12 padding-none float-left">
                                    <div class="col-lg-12 padding-none float-left">
                                        <div class=" col-lg-12 padding-none  float-right ">
                                            <ul class="col-lg-12 col-12 col-sm-12 col-md-12 flip_list style-2 padding-none ">
                                                <li>
                                                    <a href="/DMS/DocumentApprover/Effective_Documents.aspx?rtp=1" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divNewEfective" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Assign Effective Date - New Doc</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblEffectiveNew" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentApprover/Effective_Documents.aspx?rtp=2" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divRevisionEfective" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Assign Effective Date - Revision Doc</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblEffectiveRevision" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentApprover/Renew_Document.aspx" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divNewRenewDocument" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Renew Document</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblRenewDocumentNew" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <%--<li>
                                                    <a href="/DMS/DocumentApprover/Renew_Document.aspx?rtp=2" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divRevisionRenewDocument" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Renew Revision Document</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblRenewDocumentRevision" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentApprover/Renew_Document.aspx?rtp=4" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divExistingRenewDocument" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Renew Existing Document</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblRenewDocumentExisting" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>--%>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-6 float-left padding-right_div f1_container" id="divPrint" runat="server" style="position: relative; z-index: 9;" visible="false">
                    <div class=" f1_card col-lg-12  float-left">
                        <div class="front face outer_dashboard3 float-left">
                            <div style="position: absolute; right: 10px; opacity: 0.4; top: 30px;">
                                <img src="<%=ResolveUrl("~/Images/common_dashboard/print_big_dashboard.png")%>" class="" alt="Approve" />
                            </div>
                            <div class="col-lg-12 col-12 col-sm-12 col-md-12 header_text_dshboard">Ready To Print</div>
                            <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none float-left">
                                <div class="col-lg-6 col-6 col-sm-6 col-md-6  padding-none float-left">
                                    <img src="<%=ResolveUrl("~/Images/common_dashboard/print_small_dashboard.png")%>" class="dashboard-image" alt="Approve" />
                                </div>
                                <div class="col-lg-6 col-6 col-sm-6 col-md-6  float-left">
                                    <span class="link_dashboard dashboard_frontpanel_value">
                                        <asp:Label ID="lblReadyToPrint" runat="server" Text="0"></asp:Label></span>
                                </div>
                            </div>
                        </div>
                        <div class="back face center outer_dashboard3 float-left">
                            <div class="col-lg-12 col-12 col-sm-12 col-md-12 float-left">
                                <div class="col-lg-12 padding-none float-left">
                                    <div class="col-lg-12 padding-none">
                                        <div class=" col-lg-12 padding-none ">
                                            <ul class="col-lg-12 col-12 col-sm-12 col-md-12 flip_list style-2  padding-none">
                                                <li>
                                                    <a href="/DMS/DocumentsList/FormReqList.aspx?print=y" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divPrintForms" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Print Forms</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblReadyToPrintForms" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentsList/DocRequestList.aspx" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divPrintDocuments" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Print Documents</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblReadyToPrintDocuments" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/DMS/DocumentsList/FormReqList.aspx?print=n" class="float-left col-lg-12 col-12 col-sm-12 col-md-12" id="divReturnForms" runat="server" visible="false">
                                                        <div class="float-left padding-none col-lg-11">Return Forms</div>
                                                        <div class="float-right padding-none col-lg-1 dashboard_panel_value">
                                                            <asp:Label ID="lblReturnForms" runat="server" Text="0"></asp:Label>
                                                        </div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-12 col-sm-12 col-md-12 padding-none top float-left" id="DMSheader">
            <div class="grid_header  col-lg-12 col-12 col-sm-12 col-md-12 float-left">
                <div class="col-lg-6 col-6 col-sm-6 col-md-6  padding-none">DMS Dashboard</div>
                <div class="col-lg-6 col-6 col-sm-6 col-md-6  padding-none float-left">
                    <div class="panel_list float-right">
                        <ul>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="graphic" class="col-lg-12 col-12 col-sm-12 col-md-12 grid_panel_full bottom float-left">

            <div class="bottom top">
                <div class="col-lg-4 col-12 col-sm-12 col-md-6 float-left" id="EffectiveChart" runat="server">
                    <div class="grid_header col-lg-12 col-12 col-sm-12 col-md-12 float-left">
                        <div class="col-lg-7 col-6 col-sm-6 col-md-6  padding-none float-left">Effective Documents</div>
                        <div class="col-lg-5 col-6 col-sm-6 col-md-6  padding-none float-left">
                            <div class="panel_list float-right">
                                <ul>
                                    <li>
                                        <asp:LinkButton ID="btnRefreshEffectiveChart" runat="server" class="refresh" OnClick="btnRefreshEffectiveChart_Click" OnClientClick="showImg();"></asp:LinkButton>
                                    </li>
                                    <li>
                                        <a class="filter_icon filter_one"></a>
                                        <div class=" filter_dropdown " id="filter_dropdown1" style="display: none">
                                            <ul class="top">
                                                <li>
                                                    <div class="form-group  col-lg-12 col-12 col-sm-12 col-md-12 padding-none">
                                                        <asp:DropDownList ID="ddl_DeptName1" runat="server" data-size="6" CssClass="col-lg-12 col-12 col-sm-12 col-md-12 padding-none selectpicker drop_down regulatory_dropdown_style" data-live-search="true"></asp:DropDownList>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="form-group  col-lg-12 col-12 col-sm-12 col-md-12 padding-none">
                                                        <asp:DropDownList ID="ddl_DocType1" runat="server" data-size="6" CssClass="col-lg-12 col-12 col-sm-12 col-md-12 padding-none selectpicker drop_down regulatory_dropdown_style" data-live-search="true"></asp:DropDownList>
                                                    </div>
                                                </li>
                                                <li class="float-right">
                                                    <div class="form-group col-lg-12 col-12 col-sm-12 col-md-12" style="background: none; padding: 0px;" id="dashboard_btn1">
                                                        <asp:Button ID="btn_Effective" class="  btn-signup_popup  float-left" runat="server" Text="Submit" OnClick="btn_Effective_Click" OnClientClick="showImg();"/>
                                                        <asp:Button ID="btn_EffectiveClose" class=" btn-cancel_popup " runat="server" Text="Close" OnClick="btn_EffectiveClose_Click" style="margin-left:2px;" OnClientClick="showImg();"/>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12  grid_panel_full bottom float-left">
                        <asp:UpdatePanel ID="upEffective" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <dx:ASPxLabel ID="lblEffectiveChart" runat="server" ClientInstanceName="Effectivelabel" Text="No Documents Available" Font-Size="20px" ClientVisible="false" ForeColor="CadetBlue" Height="400px" Style="padding-top: 175px; padding-left: 50px"></dx:ASPxLabel>
                                <dx:BootstrapPieChart ID="BPCEffective" ClientInstanceName="Effectivepiechart" runat="server" EncodeHtml="True" OnInit="BPCEffective_Init" Palette="Ocean">
                                    <SeriesCollection>
                                        <dx:BootstrapPieChartSeries MaxLabelCount="10">
                                            <Label OnClientCustomizeText="function(pointInfo) {
	return { pointInfo.argument + &quot; : &quot;+ pointInfo.value };
}">
                                            </Label>
                                        </dx:BootstrapPieChartSeries>
                                    </SeriesCollection>
                                    <SettingsLegend Visible="False" />
                                    <SettingsToolTip OnClientCustomizeTooltip="function(pointInfo) {
	return { html: &quot;&lt;div&gt;&quot; + &quot;&lt;h6&gt;&quot; + pointInfo.argument +&quot; : &quot;+pointInfo.value+ &quot;&lt;/h6&gt;&quot; + &quot;&lt;/div&gt;&quot; };
}">
                                    </SettingsToolTip>
                                    <ClientSideEvents PointClick="function(s, e) {
	e.target.select();
}"
                                        PointSelectionChanged='function(s, e) {
               var doctype=e.target &amp;&amp; e.target.originalArgument;
               var docTypeID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddl_DocType1").value; 
               var deptID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddl_DeptName1").value;
              var reader=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_hdfReader").value;
if(reader=="0")
{
window.open("MainDocumentList.aspx?cstatus=1&amp;dtype="+doctype+"&amp;dtypeID="+docTypeID+"&amp;dID="+deptID, "_self");
}
else if(reader=="1")
{
window.open("DocumentsList/Document_List.aspx?cstatus=1&amp;dtype="+doctype+"&amp;dtypeID="+docTypeID+"&amp;dID="+deptID, "_self");
}
}'
                                        Init="function(s, e) {
    var chartMainElement = s.GetMainElement();
	var elements = chartMainElement.getElementsByClassName(&quot;dxbs-empty-piechart&quot;);
            if (elements.length &gt; 0) {
                Effectivelabel.SetVisible(true);
                s.SetVisible(false);
            }
}" />
                                </dx:BootstrapPieChart>

                                <dx:BootstrapChart ID="BCEffective" runat="server" ClientInstanceName="Effectivebarchart" EncodeHtml="True" Palette="Office">
                                    <SettingsCommonAxis>
                                        <ConstantLineStyle Color="#000000">
                                        </ConstantLineStyle>
                                    </SettingsCommonAxis>
                                    <SettingsToolTip OnClientCustomizeTooltip="function(pointInfo) {
	return { html: &quot;&lt;div&gt;&quot; + &quot;&lt;h6&gt;&quot; + pointInfo.argument +&quot; : &quot;+pointInfo.value+ &quot;&lt;/h6&gt;&quot; + &quot;&lt;/div&gt;&quot; };
}">
                                    </SettingsToolTip>
                                    <ArgumentAxis>
                                        <ConstantLineStyle Color="#000000">
                                        </ConstantLineStyle>
                                    </ArgumentAxis>
                                    <ClientSideEvents PointClick="function(s, e) {
	e.target.select();
}"
                                        PointSelectionChanged='function(s, e) {
               var deptcode=e.target &amp;&amp; e.target.originalArgument;
               var docTypeID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddl_DocType1").value; 
               var deptID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddl_DeptName1").value;
              var reader=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_hdfReader").value;
if(reader=="0")
{
window.open("MainDocumentList.aspx?cstatus=1&amp;dcode="+deptcode+"&amp;dtypeID="+docTypeID+"&amp;dID="+deptID, "_self");
}
else if(reader=="1")
{
window.open("DocumentsList/Document_List.aspx?cstatus=1&amp;dcode="+deptcode+"&amp;dtypeID="+docTypeID+"&amp;dID="+deptID, "_self");
}
}'
                                        Init="function(s, e) {
	        var chartMainElement = s.GetMainElement();
            var elements = chartMainElement.getElementsByClassName(&quot;dxc-markers&quot;)[0];
            var rectangles = elements.getElementsByTagName(&quot;rect&quot;);
            if (rectangles.length == 0) {
                Effectivelabel.SetVisible(true);
                s.SetVisible(false);
            }
}" />
                                </dx:BootstrapChart>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btn_Effective" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btn_EffectiveClose" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnRefreshEffectiveChart" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <!--/col-md--->
                </div>
            </div>
            <script>
                $(document).ready(function () {
                    //Binding Code
                    myfunction();
                    dashboard1();
                    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
                    function EndRequestHandler(sender, args) {
                        //Binding Code Again
                        dashboard1();
                    }
                });
            </script>


            <div class=" bottom">
                <div class="col-lg-4 col-12 col-sm-12 col-md-6 padding-right float-left" id="RecentlyRevisedChart" runat="server">
                    <div class="grid_header  col-lg-12 col-12 col-sm-12 col-md-12 float-left">
                        <div class="col-lg-6 col-6 col-sm-6 col-md-6  padding-none float-left">Recently Revised</div>
                        <div class="col-lg-6 col-6 col-sm-6 col-md-6  padding-none float-left">
                            <div class="panel_list float-right">
                                <ul>
                                    <li>
                                        <asp:LinkButton ID="btnRefreshRecentlyRevisedChart" runat="server" class="refresh" OnClick="btnRefreshRecentlyRevisedChart_Click" OnClientClick="showImg();"></asp:LinkButton>
                                    </li>
                                    <li>
                                        <a class="filter_icon filter_two"></a>
                                    </li>
                                    <div class="filter_dropdown" id="filter_dropdown2" style="display: none">
                                        <ul class="top">
                                            <li>
                                                <div class="form-group  col-lg-12 col-12 col-sm-12 col-md-12 padding-none">
                                                    <asp:TextBox ID="txt_FromDate1" runat="server" CssClass="form-control login_input_sign_up" placeholder="Select From Date" autocomplete="off"></asp:TextBox>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group col-lg-12 col-12 col-sm-12 col-md-12 padding-none">
                                                    <asp:TextBox ID="txt_ToDate1" runat="server" CssClass="form-control login_input_sign_up" placeholder="Select To Date" autocomplete="off"></asp:TextBox>
                                                </div>
                                            </li>
                                            <li class="float-right">
                                                <div href="#" class="form-group col-lg-12 col-12 col-sm-12 col-md-12" style="background: none; padding: 0px;" id="dashboard_btn2">
                                                    <asp:Button ID="btn_RecentlyRevised" class="  btn-signup_popup  float-left" runat="server" Text="Submit" OnClick="btn_RecentlyRevised_Click" OnClientClick="showImg();"/>
                                                    <asp:Button ID="btn_RecentlyRevisedClose" class=" btn-cancel_popup " runat="server" Text="Close" OnClick="btn_RecentlyRevisedClose_Click" style="margin-left:2px;" OnClientClick="showImg();"/>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12  grid_panel_full bottom float-left">
                        <asp:UpdatePanel ID="upRecentlyRevised" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <dx:ASPxLabel ID="lblRecentlyRevised" runat="server" ClientInstanceName="RecentlyRevisedlabel" Text="No Documents Available" Font-Size="20px" ClientVisible="false" ForeColor="CadetBlue" Height="400px" Style="padding-top: 175px; padding-left: 50px"></dx:ASPxLabel>
                                <dx:BootstrapPieChart ID="BPCRecentlyRevised" ClientInstanceName="RecentlyRevisedchart" runat="server" EncodeHtml="True" OnInit="BPCRecentlyRevised_Init" Palette="Material">
                                    <SeriesCollection>
                                        <dx:BootstrapPieChartSeries MaxLabelCount="10">
                                        </dx:BootstrapPieChartSeries>
                                    </SeriesCollection>
                                    <SettingsLegend Visible="False" />
                                    <SettingsToolTip OnClientCustomizeTooltip="function(pointInfo) {
	return { html: &quot;&lt;div&gt;&quot; + &quot;&lt;h6&gt;&quot; + pointInfo.argument +&quot; : &quot;+pointInfo.value+ &quot;&lt;/h6&gt;&quot; + &quot;&lt;/div&gt;&quot; };
}">
                                    </SettingsToolTip>
                                    <ClientSideEvents PointClick="function(s, e) {
	e.target.select();
}"
                                        PointSelectionChanged='function(s, e) {
               var deptcode=e.target &amp;&amp; e.target.originalArgument;
               var fromdate=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_txt_FromDate1").value; 
               var todate=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_txt_ToDate1").value;
              var reader=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_hdfReader").value;
if(reader=="0")
{
window.open("MainDocumentList.aspx?cstatus=3&amp;dcode="+deptcode+"&amp;fdate="+fromdate+"&amp;tdate="+todate, "_self");
}
else if(reader=="1")
{
window.open("DocumentsList/Document_List.aspx?cstatus=3&amp;dcode="+deptcode+"&amp;fdate="+fromdate+"&amp;tdate="+todate, "_self");
}
}'
                                        Init="function(s, e) {
    var chartMainElement = s.GetMainElement();
	var elements = chartMainElement.getElementsByClassName(&quot;dxbs-empty-piechart&quot;);
            if (elements.length &gt; 0) {
                s.SetVisible(false);
                RecentlyRevisedlabel.SetVisible(true);
            }
}" />
                                </dx:BootstrapPieChart>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btn_RecentlyRevised" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btn_RecentlyRevisedClose" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnRefreshRecentlyRevisedChart" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>

                    </div>
                </div>
                <!--/col-md--->
            </div>
            <script>
                $(document).ready(function () {
                    //Binding Code
                    myfunction();
                    dashboard2();
                    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
                    function EndRequestHandler(sender, args) {
                        //Binding Code Again
                        dashboard2();
                    }
                });
            </script>


            
                <div class="col-lg-4 col-12 col-sm-12 col-md-6 padding-right float-left" id="AbouttoExpireChart" runat="server">
                    <div class="grid_header  col-lg-12 col-12 col-sm-12 col-md-12 float-left">
                        <div class="col-lg-6 col-6 col-sm-6 col-md-6  padding-none float-left">About to Expire</div>
                        <div class="col-lg-6 col-6 col-sm-6 col-md-6  padding-none float-left">
                            <div class="panel_list float-right">
                                <ul>
                                    <li>
                                        <asp:LinkButton ID="btnRefreshAbouttoExpireChart" runat="server" class="refresh" OnClick="btnRefreshAbouttoExpireChart_Click" OnClientClick="showImg();"></asp:LinkButton>
                                    </li>
                                    <li>
                                        <a class="filter_icon filter_three"></a>
                                    </li>
                                    <div class=" filter_dropdown float-left" id="filter_dropdown3" style="display: none">
                                        <ul class="top">
                                            <li>
                                                <div class="form-group  col-lg-12 col-12 col-sm-12 col-md-12 padding-none float-left">
                                                    <asp:DropDownList ID="ddl_DeptName2" runat="server" data-size="6" CssClass="col-lg-12 col-12 col-sm-12 col-md-12 padding-none selectpicker drop_down regulatory_dropdown_style" data-live-search="true"></asp:DropDownList>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group  col-lg-12 col-12 col-sm-12 col-md-12 padding-none float-left">
                                                    <asp:DropDownList ID="ddl_DocType2" runat="server" data-size="6" CssClass="col-lg-12 col-12 col-sm-12 col-md-12 padding-none selectpicker drop_down regulatory_dropdown_style" data-live-search="true"></asp:DropDownList>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group  col-lg-12 col-12 col-sm-12 col-md-12 padding-none float-left">
                                                    <asp:TextBox ID="txt_FromDate2" runat="server" CssClass="form-control login_input_sign_up" placeholder="Select From Date" autocomplete="off"></asp:TextBox>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group  col-lg-12 col-12 col-sm-12 col-md-12 padding-none float-left">
                                                    <asp:TextBox ID="txt_ToDate2" runat="server" CssClass="form-control login_input_sign_up" placeholder="Select To Date" autocomplete="off"></asp:TextBox>
                                                </div>
                                            </li>
                                            <li class="float-right">
                                                <div href="#" class="form-group col-lg-12 col-12 col-sm-12 col-md-12" style="background: none; padding: 0px;" id="dashboard_btn3">
                                                    <asp:Button ID="btn_ReachingReviewDate" class="  btn-signup_popup  float-left" runat="server" Text="Submit" OnClick="btn_ReachingReviewDate_Click" OnClientClick="showImg();"/>
                                                    <asp:Button ID="btn_ReachingReviewDateClose" class=" btn-cancel_popup " runat="server" Text="Close" OnClick="btn_ReachingReviewDateClose_Click" style="margin-left:2px;" OnClientClick="showImg();"/>
                                                </div>
                                            </li>

                                        </ul>
                                    </div>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12  grid_panel_full bottom float-left">
                        <asp:UpdatePanel ID="upToExpire" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <dx:ASPxLabel ID="lblAbouttoExpire" runat="server" ClientInstanceName="AbouttoExpirelabel" Text="No Documents Available" Font-Size="20px" ClientVisible="false" ForeColor="CadetBlue" Height="400px" Style="padding-top: 175px; padding-left: 50px"></dx:ASPxLabel>
                                <dx:BootstrapPieChart ID="BPCAbouttoExpire" ClientInstanceName="AbouttoExpirepiechart" runat="server" EncodeHtml="True" OnInit="BPCAbouttoExpire_Init" Palette="Carmine">
                                    <SettingsLegend Visible="False" />
                                    <SettingsToolTip OnClientCustomizeTooltip="function(pointInfo) {
	return { html: &quot;&lt;div&gt;&quot; + &quot;&lt;h6&gt;&quot; + pointInfo.argument +&quot; : &quot;+pointInfo.value+ &quot;&lt;/h6&gt;&quot; + &quot;&lt;/div&gt;&quot; };
}">
                                    </SettingsToolTip>
                                    <ClientSideEvents PointClick="function(s, e) {
	e.target.select();
}"
                                        PointSelectionChanged='function(s, e) {
               var doctype=e.target &amp;&amp; e.target.originalArgument;
               var docTypeID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddl_DocType2").value; 
               var deptID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddl_DeptName2").value;
               var fromdate=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_txt_FromDate2").value; 
               var todate=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_txt_ToDate2").value;
window.open("MainDocumentList.aspx?cstatus=2&amp;dtype="+doctype+"&amp;dtypeID="+docTypeID+"&amp;dID="+deptID+"&amp;fdate="+fromdate+"&amp;tdate="+todate, "_self");

}'
                                        Init="function(s, e) {
    var chartMainElement = s.GetMainElement();
	var elements = chartMainElement.getElementsByClassName(&quot;dxbs-empty-piechart&quot;);
            if (elements.length &gt; 0) {
                AbouttoExpirelabel.SetVisible(true);
                s.SetVisible(false);
            }
}" />
                                </dx:BootstrapPieChart>
                                <dx:BootstrapChart ID="BCAbouttoExpire" runat="server" ClientInstanceName="Effectivebarchart" EncodeHtml="True">
                                    <SettingsToolTip OnClientCustomizeTooltip="function(pointInfo) {
	return { html: &quot;&lt;div&gt;&quot; + &quot;&lt;h6&gt;&quot; + pointInfo.argument +&quot; : &quot;+pointInfo.value+ &quot;&lt;/h6&gt;&quot; + &quot;&lt;/div&gt;&quot; };
}">
                                    </SettingsToolTip>
                                    <ClientSideEvents PointClick="function(s, e) {
	e.target.select();
}"
                                        PointSelectionChanged='function(s, e) {
               var deptcode=e.target &amp;&amp; e.target.originalArgument;
               var docTypeID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddl_DocType2").value; 
               var deptID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddl_DeptName2").value;
               var fromdate=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_txt_FromDate2").value; 
               var todate=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_txt_ToDate2").value;
window.open("MainDocumentList.aspx?cstatus=2&amp;dcode="+deptcode+"&amp;dtypeID="+docTypeID+"&amp;dID="+deptID+"&amp;fdate="+fromdate+"&amp;tdate="+todate, "_self");

}'
                                        Init="function(s, e) {
           var chartMainElement = s.GetMainElement();
            var elements = chartMainElement.getElementsByClassName(&quot;dxc-markers&quot;)[0];
            var rectangles = elements.getElementsByTagName(&quot;rect&quot;);
            if (rectangles.length == 0) {
                AbouttoExpirelabel.SetVisible(true);
                s.SetVisible(false);
            }
}" />
                                </dx:BootstrapChart>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btn_ReachingReviewDate" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btn_ReachingReviewDateClose" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnRefreshAbouttoExpireChart" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <!--/col-md--->
          
            <script>
                $(document).ready(function () {
                    //Binding Code
                    myfunction();
                    dashboard3();
                    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
                    function EndRequestHandler(sender, args) {
                        //Binding Code Again
                        dashboard3();
                    }
                });
            </script>


            <div class=" bottom float-left">
                <div class="col-md-12 padding-right float-left" id="DocumentStatusChart" runat="server">
                    <div class="grid_header  col-lg-12 col-12 col-sm-12 col-md-12 float-left">
                        <div class="col-lg-6 col-6 col-sm-6 col-md-6  padding-none float-left">Document Status</div>
                        <div class="col-lg-6 col-6 col-sm-6 col-md-6  padding-none float-left">
                            <div class="panel_list float-right">
                                <ul>
                                    <li>
                                        <asp:LinkButton ID="btnRefreshDocumentStatusChart" runat="server" class="refresh" OnClick="btnRefreshDocumentStatusChart_Click" OnClientClick="showImg();"></asp:LinkButton>
                                    </li>
                                    <li>
                                        <a class="filter_icon filter_four"></a>
                                    </li>
                                    <div class=" filter_dropdown" id="filter_dropdown4" style="display: none">
                                        <ul class="top">
                                            <li>
                                                <div class="form-group  col-lg-12 col-12 col-sm-12 col-md-12 padding-none">
                                                    <asp:DropDownList ID="ddl_DeptName3" runat="server" data-size="6" CssClass="col-lg-12 col-12 col-sm-12 col-md-12 padding-none selectpicker drop_down regulatory_dropdown_style" data-live-search="true"></asp:DropDownList>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group  col-lg-12 col-12 col-sm-12 col-md-12 padding-none">
                                                    <asp:DropDownList ID="ddl_DocType3" runat="server" data-size="6" CssClass="col-lg-12 col-12 col-sm-12 col-md-12 padding-none selectpicker drop_down regulatory_dropdown_style" data-live-search="true"></asp:DropDownList>
                                                </div>
                                            </li>
                                            <li class="float-right">
                                                <div class="form-group col-lg-12 col-12 col-sm-12 col-md-12" style="background: none; padding: 0px;" id="dashboard_btn4">
                                                    <asp:Button ID="btn_DocumentStatus" class="  btn-signup_popup  float-left" runat="server" Text="Submit" OnClick="btn_DocumentStatus_Click" OnClientClick="showImg();"/>
                                                    <asp:Button ID="btn_DocumentStatusClose" class=" btn-cancel_popup " runat="server" Text="Close" OnClick="btn_DocumentStatusClose_Click" style="margin-left:2px;" OnClientClick="showImg();"/>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12  grid_panel_full bottom float-left">
                        <asp:UpdatePanel ID="upDocumentStatus" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="col-md-6 top bottom float-left chart_border">
                                    <dx:ASPxLabel ID="lblPending" runat="server" ClientInstanceName="Pendinglabel" Text="No Pending Documents Available" Font-Size="20px" ClientVisible="false" ForeColor="CadetBlue" Height="400px" Style="padding-top: 175px; padding-left: 50px"></dx:ASPxLabel>
                                    <dx:BootstrapPieChart ID="BPCPending" ClientInstanceName="Pendingpiechart" TitleText="Work in Progress Documents" runat="server" width="500px" EncodeHtml="True" OnInit="BPCPending_Init" Palette="Bright">
                                        <SeriesCollection>
                                            <dx:BootstrapPieChartSeries MaxLabelCount="10">
                                            </dx:BootstrapPieChartSeries>
                                        </SeriesCollection>
                                        <SettingsToolTip OnClientCustomizeTooltip="function(pointInfo) {
	return { html: &quot;&lt;div&gt;&quot; + &quot;&lt;h6&gt;&quot; + pointInfo.argument +&quot; : &quot;+pointInfo.value+ &quot;&lt;/h6&gt;&quot; + &quot;&lt;/div&gt;&quot; };
}">
                                        </SettingsToolTip>
                                        <ClientSideEvents PointClick="function(s, e) {
	e.target.select();
}"
                                            PointSelectionChanged='function(s, e) {
               var doctype=e.target &amp;&amp; e.target.originalArgument;
               var docTypeID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddl_DocType3").value; 
               var deptID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddl_DeptName3").value;
window.open("MainDocumentList.aspx?cstatus=4&amp;dtype="+doctype+"&amp;dtypeID="+docTypeID+"&amp;dID="+deptID, "_self");
}'
                                            Init="function(s, e) {
    var chartMainElement = s.GetMainElement();
	var elements = chartMainElement.getElementsByClassName(&quot;dxbs-empty-piechart&quot;);
            if (elements.length &gt; 0) {
                Pendinglabel.SetVisible(true);
                s.SetVisible(false);
            }
}" />
                                    </dx:BootstrapPieChart>
                                    <dx:BootstrapChart ID="BCPending" runat="server" TitleText="Pending Documents" Width="500px" ClientInstanceName="Pendingbarchart" EncodeHtml="True">
                                        <SettingsToolTip OnClientCustomizeTooltip="function(pointInfo) {
	return { html: &quot;&lt;div&gt;&quot; + &quot;&lt;h6&gt;&quot; + pointInfo.argument +&quot; : &quot;+pointInfo.value+ &quot;&lt;/h6&gt;&quot; + &quot;&lt;/div&gt;&quot; };
}">
                                        </SettingsToolTip>
                                        <ClientSideEvents PointClick="function(s, e) {
	e.target.select();
}"
                                            PointSelectionChanged='function(s, e) {
               var deptcode=e.target &amp;&amp; e.target.originalArgument;
               var docTypeID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddl_DocType3").value; 
               var deptID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddl_DeptName3").value;
window.open("MainDocumentList.aspx?cstatus=4&amp;dcode="+deptcode+"&amp;dtypeID="+docTypeID+"&amp;dID="+deptID, "_self");
}'
                                            Init="function(s, e) {
	        var chartMainElement = s.GetMainElement();
            var elements = chartMainElement.getElementsByClassName(&quot;dxc-markers&quot;)[0];
            var rectangles = elements.getElementsByTagName(&quot;rect&quot;);
            if (rectangles.length == 0) {
                Pendinglabel.SetVisible(true);
                s.SetVisible(false);
            }
}" />
                                    </dx:BootstrapChart>
                                </div>
                                <div class="col-md-6 top bottom float-left chart_border">
                                    <dx:ASPxLabel ID="lblWithdrawn" runat="server" ClientInstanceName="Withdrawnlabel" Text="No Withdrawn Documents Available" Font-Size="20px" ClientVisible="false" ForeColor="CadetBlue" Height="400px" Style="padding-top: 175px; padding-left: 50px"></dx:ASPxLabel>
                                    <dx:BootstrapPieChart ID="BPCWithdrawn" ClientInstanceName="Withdrawnpiechart" TitleText="Withdrawn Documents" runat="server" width="500px" EncodeHtml="True" OnInit="BPCWithdrawn_Init" Palette="Ocean">
                                        <SeriesCollection>
                                            <dx:BootstrapPieChartSeries MaxLabelCount="10">
                                            </dx:BootstrapPieChartSeries>
                                        </SeriesCollection>
                                        <SettingsToolTip OnClientCustomizeTooltip="function(pointInfo) {
	return { html: &quot;&lt;div&gt;&quot; + &quot;&lt;h6&gt;&quot; + pointInfo.argument +&quot; : &quot;+pointInfo.value+ &quot;&lt;/h6&gt;&quot; + &quot;&lt;/div&gt;&quot; };
}">
                                        </SettingsToolTip>
                                        <ClientSideEvents PointClick="function(s, e) {
	e.target.select();
}"
                                            PointSelectionChanged='function(s, e) {
               var doctype=e.target &amp;&amp; e.target.originalArgument;
               var docTypeID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddl_DocType3").value; 
               var deptID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddl_DeptName3").value;
window.open("MainDocumentList.aspx?cstatus=5&amp;dtype="+doctype+"&amp;dtypeID="+docTypeID+"&amp;dID="+deptID, "_self");
}'
                                            Init="function(s, e) {
    var chartMainElement = s.GetMainElement();
	var elements = chartMainElement.getElementsByClassName(&quot;dxbs-empty-piechart&quot;);
            if (elements.length &gt; 0) {
                Withdrawnlabel.SetVisible(true);
                s.SetVisible(false);
            }
}" />
                                    </dx:BootstrapPieChart>
                                    <dx:BootstrapChart ID="BCWithdrawn" runat="server" ClientInstanceName="Withdrawnbarchart" width="500px" TitleText="Withdrawn Documents" EncodeHtml="True">
                                        <SettingsToolTip OnClientCustomizeTooltip="function(pointInfo) {
	return { html: &quot;&lt;div&gt;&quot; + &quot;&lt;h6&gt;&quot; + pointInfo.argument +&quot; : &quot;+pointInfo.value+ &quot;&lt;/h6&gt;&quot; + &quot;&lt;/div&gt;&quot; };
}">
                                        </SettingsToolTip>
                                        <ClientSideEvents PointClick="function(s, e) {
	e.target.select();
}"
                                            PointSelectionChanged='function(s, e) {
               var deptcode=e.target &amp;&amp; e.target.originalArgument;
               var docTypeID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddl_DocType3").value; 
               var deptID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddl_DeptName3").value;
window.open("MainDocumentList.aspx?cstatus=5&amp;dcode="+deptcode+"&amp;dtypeID="+docTypeID+"&amp;dID="+deptID, "_self");
}'
                                            Init="function(s, e) {
	        var chartMainElement = s.GetMainElement();
            var elements = chartMainElement.getElementsByClassName(&quot;dxc-markers&quot;)[0];
            var rectangles = elements.getElementsByTagName(&quot;rect&quot;);
            if (rectangles.length == 0) {
                Withdrawnlabel.SetVisible(true);
                s.SetVisible(false);
            }
}" />
                                    </dx:BootstrapChart>
                                </div>
                                <div class="col-md-6 top bottom float-left chart_border">
                                    <dx:ASPxLabel ID="lblActive" runat="server" ClientInstanceName="Activelabel" Text="No Active Documents Available" Font-Size="20px" ClientVisible="false" ForeColor="CadetBlue" Height="400px" Style="padding-top: 175px; padding-left: 50px"></dx:ASPxLabel>
                                    <dx:BootstrapPieChart ID="BPCActive" ClientInstanceName="Activepiechart" TitleText="To be Made Effective Documents" runat="server" EncodeHtml="True" OnInit="BPCActive_Init" Palette="Vintage">
                                        <SeriesCollection>
                                            <dx:BootstrapPieChartSeries MaxLabelCount="10">
                                            </dx:BootstrapPieChartSeries>
                                        </SeriesCollection>
                                        <SettingsToolTip OnClientCustomizeTooltip="function(pointInfo) {
	return { html: &quot;&lt;div&gt;&quot; + &quot;&lt;h6&gt;&quot; + pointInfo.argument +&quot; : &quot;+pointInfo.value+ &quot;&lt;/h6&gt;&quot; + &quot;&lt;/div&gt;&quot; };
}">
                                        </SettingsToolTip>
                                        <ClientSideEvents PointClick="function(s, e) {
	e.target.select();
}"
                                            PointSelectionChanged='function(s, e) {
               var doctype=e.target &amp;&amp; e.target.originalArgument;
               var docTypeID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddl_DocType3").value; 
               var deptID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddl_DeptName3").value;
window.open("MainDocumentList.aspx?cstatus=6&amp;dtype="+doctype+"&amp;dtypeID="+docTypeID+"&amp;dID="+deptID, "_self");
}'
                                            Init="function(s, e) {
    var chartMainElement = s.GetMainElement();
	var elements = chartMainElement.getElementsByClassName(&quot;dxbs-empty-piechart&quot;);
            if (elements.length &gt; 0) {
                Activelabel.SetVisible(true);
                s.SetVisible(false);
            }
}" />
                                    </dx:BootstrapPieChart>
                                    <dx:BootstrapChart ID="BCActive" runat="server" ClientInstanceName="Activebarchart" TitleText="Active Documents" EncodeHtml="True">
                                        <SettingsToolTip OnClientCustomizeTooltip="function(pointInfo) {
	return { html: &quot;&lt;div&gt;&quot; + &quot;&lt;h6&gt;&quot; + pointInfo.argument +&quot; : &quot;+pointInfo.value+ &quot;&lt;/h6&gt;&quot; + &quot;&lt;/div&gt;&quot; };
}">
                                        </SettingsToolTip>
                                        <ClientSideEvents PointClick="function(s, e) {
	e.target.select();
}"
                                            PointSelectionChanged='function(s, e) {
               var deptcode=e.target &amp;&amp; e.target.originalArgument;
               var docTypeID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddl_DocType3").value; 
               var deptID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddl_DeptName3").value;
window.open("MainDocumentList.aspx?cstatus=6&amp;dcode="+deptcode+"&amp;dtypeID="+docTypeID+"&amp;dID="+deptID, "_self");
}'
                                            Init="function(s, e) {
	        var chartMainElement = s.GetMainElement();
            var elements = chartMainElement.getElementsByClassName(&quot;dxc-markers&quot;)[0];
            var rectangles = elements.getElementsByTagName(&quot;rect&quot;);
            if (rectangles.length == 0) {
                Activelabel.SetVisible(true);
                s.SetVisible(false);
            }
}" />
                                    </dx:BootstrapChart>
                                </div>
                                <div class="col-md-6 top bottom float-left chart_border">
                                    <dx:ASPxLabel ID="lblOverdue" runat="server" ClientInstanceName="Overduelabel" Text="No Overdue Documents Available" Font-Size="20px" ClientVisible="false" ForeColor="CadetBlue" Height="400px" Style="padding-top: 175px; padding-left: 50px"></dx:ASPxLabel>
                                    <dx:BootstrapPieChart ID="BPCOverdue" ClientInstanceName="Overduepiechart" TitleText="Overdue Documents" runat="server" EncodeHtml="True" OnInit="BPCOverdue_Init" Palette="Soft">
                                        <SeriesCollection>
                                            <dx:BootstrapPieChartSeries MaxLabelCount="10">
                                            </dx:BootstrapPieChartSeries>
                                        </SeriesCollection>
                                        <SettingsToolTip OnClientCustomizeTooltip="function(pointInfo) {
	return { html: &quot;&lt;div&gt;&quot; + &quot;&lt;h6&gt;&quot; + pointInfo.argument +&quot; : &quot;+pointInfo.value+ &quot;&lt;/h6&gt;&quot; + &quot;&lt;/div&gt;&quot; };
}">
                                        </SettingsToolTip>
                                        <ClientSideEvents PointClick="function(s, e) {
	e.target.select();
}"
                                            PointSelectionChanged='function(s, e) {
               var doctype=e.target &amp;&amp; e.target.originalArgument;
               var docTypeID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddl_DocType3").value; 
               var deptID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddl_DeptName3").value;
window.open("MainDocumentList.aspx?cstatus=8&amp;dtype="+doctype+"&amp;dtypeID="+docTypeID+"&amp;dID="+deptID, "_self");
}'
                                            Init="function(s, e) {
    var chartMainElement = s.GetMainElement();
	var elements = chartMainElement.getElementsByClassName(&quot;dxbs-empty-piechart&quot;);
            if (elements.length &gt; 0) {
                Overduelabel.SetVisible(true);
                s.SetVisible(false);
            }
}" />
                                    </dx:BootstrapPieChart>
                                    <dx:BootstrapChart ID="BCOverdue" runat="server" ClientInstanceName="Overduebarchart" TitleText="Overdue Documents" EncodeHtml="True">
                                        <SettingsToolTip OnClientCustomizeTooltip="function(pointInfo) {
	return { html: &quot;&lt;div&gt;&quot; + &quot;&lt;h6&gt;&quot; + pointInfo.argument +&quot; : &quot;+pointInfo.value+ &quot;&lt;/h6&gt;&quot; + &quot;&lt;/div&gt;&quot; };
}">
                                        </SettingsToolTip>
                                        <ClientSideEvents PointClick="function(s, e) {
	e.target.select();
}"
                                            PointSelectionChanged='function(s, e) {
               var deptcode=e.target &amp;&amp; e.target.originalArgument;
               var docTypeID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddl_DocType3").value; 
               var deptID=document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_ddl_DeptName3").value;
window.open("MainDocumentList.aspx?cstatus=8&amp;dcode="+deptcode+"&amp;dtypeID="+docTypeID+"&amp;dID="+deptID, "_self");
}'
                                            Init="function(s, e) {
	        var chartMainElement = s.GetMainElement();
            var elements = chartMainElement.getElementsByClassName(&quot;dxc-markers&quot;)[0];
            var rectangles = elements.getElementsByTagName(&quot;rect&quot;);
            if (rectangles.length == 0) {
                Overduelabel.SetVisible(true);
                s.SetVisible(false);
            }
}" />
                                    </dx:BootstrapChart>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btn_DocumentStatus" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btn_DocumentStatusClose" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnRefreshDocumentStatusChart" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <!--/col-md--->
            </div>
            <script>
                $(document).ready(function () {
                    //Binding Code
                    myfunction();
                    dashboard4();
                    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
                    function EndRequestHandler(sender, args) {
                        //Binding Code Again
                        dashboard4();
                    }
                });
            </script>

        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdfReader"></asp:HiddenField>
    <!--/col-md--->
    <script>     
                function dashboard4() {
                    $(".filter_four").on("click", function () {
                        $("#filter_dropdown4").toggle();
                    });
                }
                function dashboard3() {
                    $(".filter_three").on("click", function () {
                        $("#filter_dropdown3").toggle();
                    });
                }
                function dashboard2() {
                    $(".filter_two").on("click", function () {
                        $("#filter_dropdown2").toggle();
                    });
                }
                function dashboard1() {
                    $(".filter_one").on("click", function () {
                        $("#filter_dropdown1").toggle();
                    });
                }
                function dashboard() {
                    $(".filter_zero").on("click", function () {
                        $("#filter_dropdown").toggle();
                    });
                }
    </script>
    <!----To Load Calendar ---->
    <script>
            function myfunction() {
                //for start date and end date validations startdate should be < enddate
                $(function () {
                    $('#<%=txt_FromDate1.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        useCurrent: false,
                        widgetPositioning: {
                            horizontal: 'right',
                            vertical: 'bottom'
                        }
                    });
                    $('#<%=txt_ToDate1.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        useCurrent: false,
                        widgetPositioning: {
                            horizontal: 'right',
                            vertical: 'bottom'
                        }
                    });
                    $('#<%=txt_FromDate2.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        useCurrent: false,
                        widgetPositioning: {
                            horizontal: 'right',
                            vertical: 'bottom'
                        }
                    });
                    $('#<%=txt_ToDate2.ClientID%>').datetimepicker({
                        format: 'DD MMM YYYY',
                        useCurrent: false,
                        widgetPositioning: {
                            horizontal: 'right',
                            vertical: 'bottom'
                        }
                    });
                    var fromdate1 = new Date();
                    fromdate1.setDate(fromdate1.getDate() - 30);
                    $('#<%=txt_FromDate1.ClientID%>').val(fromdate1.format('dd MMM yyyy'));
                        var todate1 = new Date();
                        $('#<%=txt_ToDate1.ClientID%>').val(todate1.format('dd MMM yyyy'));
                                var fromdate2 = new Date();
                                $('#<%=txt_FromDate2.ClientID%>').val(fromdate2.format('dd MMM yyyy'));
                                var todate2 = new Date();
                                todate2.setDate(todate2.getDate() + 30);
                                $('#<%=txt_ToDate2.ClientID%>').val(todate2.format('dd MMM yyyy'));
                                $('#<%=ddl_DeptName2.ClientID%>').val('0');
                                $('#<%=ddl_DeptName2.ClientID%>').selectpicker("refresh");
                        $('#<%=ddl_DocType2.ClientID%>').val('0');
                        $('#<%=ddl_DocType2.ClientID%>').selectpicker("refresh");
                });
            }
    </script>
    <script>
            $(function () {
                $('#<%=txt_FromDate1.ClientID%>').on("dp.change", function (e) {
                    $('#<%=txt_ToDate1.ClientID%>').data("DateTimePicker").minDate(e.date);
                });
            });
    </script>
    <script>
            $(function () {
                $('#<%=txt_ToDate1.ClientID%>').on("dp.change", function (e) {
                        var mindate = document.getElementById("<%=txt_FromDate1.ClientID%>").value;
                        $('#<%=txt_ToDate1.ClientID%>').data("DateTimePicker").minDate(mindate);
                });
            });
    </script>
    <script>
                $(function () {
                    $('#<%=txt_FromDate2.ClientID%>').on("dp.change", function (e) {
                                $('#<%=txt_ToDate2.ClientID%>').data("DateTimePicker").minDate(e.date);
                    });
                });
    </script>
    <script>
                        $(function () {
                            $('#<%=txt_ToDate2.ClientID%>').on("dp.change", function (e) {
                                var mindate = document.getElementById("<%=txt_FromDate2.ClientID%>").value;
                                $('#<%=txt_ToDate2.ClientID%>').data("DateTimePicker").minDate(mindate);
                            });
                        });
    </script>
    <script>
                        function showAllCharts() {
                            $("#EffectiveChart").show();
                            $("#RecentlyRevisedChart").show();
                            $("#AbouttoExpireChart").show();
                            $("#DocumentStatusChart").show();
                        }
                        function showEffectiveRecentlyRevisedCharts() {
                            $("#EffectiveChart").show();
                            $("#RecentlyRevisedChart").show();
                            $("#AbouttoExpireChart").hide();
                            $("#DocumentStatusChart").hide();
                        }
    </script>
    <script>
                        function showDashboard() {
                            $("#DMSheader").show();
                            $("#graphic").show();
                        }
                        function hideDashboard() {
                            $("#DMSheader").hide();
                            $("#graphic").hide();
                        }
    </script>
    <script>
                function ResetEffChart() {
                    $('#<%=ddl_DeptName1.ClientID%>').val('0');
                    $('#<%=ddl_DeptName1.ClientID%>').selectpicker("refresh");
                    $('#<%=ddl_DocType1.ClientID%>').val('0');
                    $('#<%=ddl_DocType1.ClientID%>').selectpicker("refresh");
                }
                function ResetDocStatusChart() {
                    $('#<%=ddl_DeptName3.ClientID%>').val('0');
                    $('#<%=ddl_DeptName3.ClientID%>').selectpicker("refresh");
                    $('#<%=ddl_DocType3.ClientID%>').val('0');
                    $('#<%=ddl_DocType3.ClientID%>').selectpicker("refresh");
                }
    </script>
    <script>
                function showImg() {
                    ShowPleaseWait('show');
                }
                function hideImg() {
                    ShowPleaseWait('hide');
                }
    </script>
</asp:Content>
