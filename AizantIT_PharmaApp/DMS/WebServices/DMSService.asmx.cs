﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using AizantIT_DMSBO;
using AizantIT_DMSBAL;
using System.Globalization;
using System.Web.Script.Services;
using System.Web.Caching;
using AizantIT_PharmaApp.DMS.Reports;
using DevExpress.XtraReports.UI;
using AizantIT_PharmaApp.Common;
using System.Net;
using System.IO;
using DevExpress.Utils.OAuth.Provider;
using UMS_BO;
using UMS_BusinessLayer;
using System.Collections;
using System.Text.RegularExpressions;

namespace AizantIT_PharmaApp.DMS.WebServices
{
    /// <summary>
    /// Summary description for DMSService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    [System.Web.Script.Services.ScriptService]
    public class DMSService : System.Web.Services.WebService
    {
        DocumentCreationBAL DMS_BalDC = new DocumentCreationBAL();
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        #region Existing DocumentList
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetDocumentList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_DocumentNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_VersionNumber = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentType = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Department = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_EffectiveDate = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ExpirationDate = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);

            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentNumber;
            objJQDataTableBO.sSearchDocumentName = sSearch_DocumentName;
            objJQDataTableBO.sSearchVersionNumber = sSearch_VersionNumber;
            objJQDataTableBO.sSearchDocumentType = sSearch_DocumentType;
            objJQDataTableBO.sSearchDepartment = sSearch_Department;
            objJQDataTableBO.sSearchEffectiveDate = sSearch_EffectiveDate;
            objJQDataTableBO.sSearchExpirationDate = sSearch_ExpirationDate;
            List<ExistingDocBO> listDocuments = new List<ExistingDocBO>();
            int filteredCount = 0;
            DataTable dt = DMS_BalDC.DMS_GetExistingDocuments(objJQDataTableBO);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    ExistingDocBO objDocObjects = new ExistingDocBO();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    objDocObjects.VersionID = Convert.ToInt32(rdr["VersionID"].ToString());
                    objDocObjects.Department = (rdr["Department"].ToString());
                    objDocObjects.DocumentName = (rdr["DocumentName"].ToString());
                    objDocObjects.DocumentType = (rdr["DocumentType"].ToString());
                    objDocObjects.DocumentNumber = (rdr["DocumentNumber"].ToString());
                    objDocObjects.EffectiveDate = (rdr["EffectiveDate"].ToString());
                    objDocObjects.ExpirationDate = (rdr["ExpirationDate"].ToString());
                    objDocObjects.VersionNumber = (rdr["VersionNumber"].ToString());
                    objDocObjects.expStatus = Convert.ToInt32(rdr["expStatus"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listDocuments.Add(objDocObjects);
                }
            }
            //JQDataTableBO _objJQDataTableBO = new JQDataTableBO();
            //_objJQDataTableBO.iMode = 2;
            //_objJQDataTableBO.pkid = 0;
            //_objJQDataTableBO.iDisplayLength = iDisplayLength;
            //_objJQDataTableBO.iDisplayStart = iDisplayStart;
            //_objJQDataTableBO.iSortCol = iSortCol_0;
            //_objJQDataTableBO.sSortDir = sSortDir_0;
            //_objJQDataTableBO.sSearch = sSearch;
            //DataTable dtcount = DMS_BalDC.DMS_GetExistingDocuments(_objJQDataTableBO);
            var result = new
            {
                iTotalRecords = filteredCount,
                iTotalDisplayRecords = filteredCount,
                aaData = listDocuments
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion Existing DocumentList

        #region GetDocumentTypes
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetDocumentTypes()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_DocumentType = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ProcessType = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_IsTraining = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ReviewPeriod = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);

            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchDocumentType = sSearch_DocumentType;
            objJQDataTableBO.sSearchProcessType = sSearch_ProcessType;
            objJQDataTableBO.sSearchIsTraining = sSearch_IsTraining;
            objJQDataTableBO.sSearch_ReviewPeriodP = sSearch_ReviewPeriod;
            List<DocumentTypeBO> listDocumentType = new List<DocumentTypeBO>();
            int filteredCount = 0;
            DataTable dt = DMS_BalDC.DMS_GetDocumentTypeList(objJQDataTableBO);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    DocumentTypeBO objDocObjects = new DocumentTypeBO();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    objDocObjects.DocumentTypeID = Convert.ToInt32(rdr["DocumentTypeID"].ToString());
                    objDocObjects.DocumentType = (rdr["DocumentType"].ToString());
                    objDocObjects.ProcessTypeID = Convert.ToInt32(rdr["ProcessTypeID"].ToString());
                    objDocObjects.ProcessType = (rdr["ProcessType"].ToString());
                    objDocObjects.ReviewPeriod = (rdr["ReviewPeriod"].ToString());
                    objDocObjects.IsTraining = (rdr["Training"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listDocumentType.Add(objDocObjects);
                }
            }
            //JQDataTableBO _objJQDataTableBO = new JQDataTableBO();
            //_objJQDataTableBO.iMode = 2;
            //_objJQDataTableBO.pkid = 0;
            //_objJQDataTableBO.iDisplayLength = iDisplayLength;
            //_objJQDataTableBO.iDisplayStart = iDisplayStart;
            //_objJQDataTableBO.iSortCol = iSortCol_0;
            //_objJQDataTableBO.sSortDir = sSortDir_0;
            //_objJQDataTableBO.sSearch = sSearch;
            //DataTable dtcount = DMS_BalDC.DMS_GetDocumentTypeList(_objJQDataTableBO);
            var result = new
            {
                iTotalRecords = filteredCount,
                iTotalDisplayRecords = filteredCount,
                aaData = listDocumentType
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetDocumentTypes

        #region GetTemplate
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetAddTemplates()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            List<AddTemplateBO> listTemplate = new List<AddTemplateBO>();
            int filteredCount = 0;
            DataTable dt = DMS_BalDC.DMS_GetTemplateNameList(objJQDataTableBO);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    AddTemplateBO objDocObjects = new AddTemplateBO();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    objDocObjects.TemplateID = Convert.ToInt32(rdr["TemplateID"].ToString());
                    objDocObjects.CreatedBy = Convert.ToInt32(rdr["CreatedBy"].ToString());
                    objDocObjects.TemplateName = (rdr["TemplateName"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listTemplate.Add(objDocObjects);
                }
            }
            JQDataTableBO _objJQDataTableBO = new JQDataTableBO();
            _objJQDataTableBO.iMode = 2;
            _objJQDataTableBO.pkid = 0;
            _objJQDataTableBO.iDisplayLength = iDisplayLength;
            _objJQDataTableBO.iDisplayStart = iDisplayStart;
            _objJQDataTableBO.iSortCol = iSortCol_0;
            _objJQDataTableBO.sSortDir = sSortDir_0;
            _objJQDataTableBO.sSearch = sSearch;
            DataTable dtcount = DMS_BalDC.DMS_GetTemplateNameList(_objJQDataTableBO);
            var result = new
            {
                iTotalRecords = dtcount.Rows.Count > 0 ? Convert.ToInt32(dtcount.Rows[0][0]) : 0,
                iTotalDisplayRecords = filteredCount,
                aaData = listTemplate
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetTemplate

        #region GetListOfDocuments
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetListOfDocuments()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_DocumentNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_VersionNumber = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentType = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Department = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_EffectiveDate = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ExpirationDate = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentNumber;
            objJQDataTableBO.sSearchDocumentName = sSearch_DocumentName;
            objJQDataTableBO.sSearchVersionNumber = sSearch_VersionNumber;
            objJQDataTableBO.sSearchDocumentType = sSearch_DocumentType;
            objJQDataTableBO.sSearchDepartment = sSearch_Department;
            objJQDataTableBO.sSearchEffectiveDate = sSearch_EffectiveDate;
            objJQDataTableBO.sSearchExpirationDate = sSearch_ExpirationDate;
            objJQDataTableBO.ChartStatus = 0;
            objJQDataTableBO.ChartDocTypeID = 0;
            objJQDataTableBO.ChartDeptID = 0;
            objJQDataTableBO.ChartFromDate = DateTime.Now;
            objJQDataTableBO.ChartToDate = DateTime.Now;
            objJQDataTableBO.ChartDeptCode = string.Empty;
            objJQDataTableBO.ChartDocTypeName = string.Empty;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            List<DocListBO> listofDocuments = new List<DocListBO>();
            int filteredCount = 0;
            DataTable dt = new DataTable();
            if (Session["ChartStatus"].ToString() != "0")
            {
                if (Session["ChartStatus"].ToString() == "1")
                {

                    objJQDataTableBO.ChartStatus = 1;
                    objJQDataTableBO.ChartDocTypeID = Convert.ToInt32(Session["ChartDocumentType"].ToString());
                    objJQDataTableBO.ChartDeptID = Convert.ToInt32(Session["ChartDepartment"].ToString());
                    objJQDataTableBO.ChartDeptCode = Session["ChartDeptCode"].ToString();
                    objJQDataTableBO.ChartDocTypeName = Session["ChartDocTypeName"].ToString();
                }
                if (Session["ChartStatus"].ToString() == "3")
                {

                    objJQDataTableBO.ChartStatus = 3;
                    objJQDataTableBO.ChartFromDate = Convert.ToDateTime(Session["ChartFromDate"].ToString());
                    objJQDataTableBO.ChartToDate = Convert.ToDateTime(Session["ChartToDate"].ToString());
                    objJQDataTableBO.ChartDeptCode = Session["ChartDeptCode"].ToString();
                }
            }
            dt = DMS_BalDC.DMS_Listofdocuments(objJQDataTableBO);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    DocListBO objDocObjects = new DocListBO();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    objDocObjects.VersionID = Convert.ToInt32(rdr["VersionID"].ToString());
                    objDocObjects.Department = (rdr["Department"].ToString());
                    objDocObjects.DocumentName = (rdr["DocumentName"].ToString());
                    objDocObjects.VersionNumber =(rdr["VersionNumber"].ToString());
                    objDocObjects.DocumentNumber = (rdr["DocumentNumber"].ToString());
                    objDocObjects.DocumentType = (rdr["DocumentType"].ToString());
                    objDocObjects.EffectiveDate = (rdr["EffectiveDate"].ToString());
                    objDocObjects.ExpirationDate = (rdr["ExpirationDate"].ToString());
                    objDocObjects.IsPublic = (rdr["isPublic"].ToString());
                    objDocObjects.expStatus = Convert.ToInt32(rdr["expStatus"].ToString());
                    objDocObjects.ReferralCount = Convert.ToInt32(rdr["ReferralCount"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofDocuments.Add(objDocObjects);
                }
            }
            //JQDataTableBO _objJQDataTableBO = new JQDataTableBO();
            //_objJQDataTableBO.iMode = 2;
            //_objJQDataTableBO.pkid = 0;
            //_objJQDataTableBO.iDisplayLength = iDisplayLength;
            //_objJQDataTableBO.iDisplayStart = iDisplayStart;
            //_objJQDataTableBO.iSortCol = iSortCol_0;
            //_objJQDataTableBO.sSortDir = sSortDir_0;
            //_objJQDataTableBO.sSearch = sSearch;
            //DataTable dtcount = DMS_BalDC.DMS_Listofdocuments(_objJQDataTableBO);
            var result = new
            {
                iTotalRecords = filteredCount,
                iTotalDisplayRecords = filteredCount,
                aaData = listofDocuments
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetListOfDocuments

        #region GetDates
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetDates()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_DocumentNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_VersionNumber = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentType = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Department = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);            
            var sSearch_EffectiveDate = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ExpirationDate = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);           

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            var ReqType = HttpContext.Current.Request.Params["ReqTypeID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentNumber;
            objJQDataTableBO.sSearchDocumentName = sSearch_DocumentName;
            objJQDataTableBO.sSearchVersionNumber = sSearch_VersionNumber;
            objJQDataTableBO.sSearchDocumentType = sSearch_DocumentType;
            objJQDataTableBO.sSearchDepartment = sSearch_Department;
            objJQDataTableBO.sSearchEffectiveDate = sSearch_EffectiveDate;
            objJQDataTableBO.sSearchExpirationDate = sSearch_ExpirationDate;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType = Convert.ToInt32(ReqType);
            List<DatesBO> listofDates = new List<DatesBO>();
            int filteredCount = 0;
            DataTable dt = DMS_BalDC.DMS_Listofeffectivedocuments(objJQDataTableBO);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    DatesBO objDocObjects = new DatesBO();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    objDocObjects.VersionID = Convert.ToInt32(rdr["VersionID"].ToString());
                    objDocObjects.Department = (rdr["Department"].ToString());
                    objDocObjects.DocumentName = (rdr["DocumentName"].ToString());
                    objDocObjects.DocumentNumber = (rdr["DocumentNumber"].ToString());
                    objDocObjects.EffectiveDate = (rdr["EffectiveDate"].ToString());
                    objDocObjects.ExpirationDate = (rdr["ExpirationDate"].ToString());
                    objDocObjects.vDocumentID = Convert.ToInt32(rdr["vDocumentID"].ToString());
                    objDocObjects.TrainingCount = Convert.ToBoolean(rdr["TrainingCount"].ToString());
                    objDocObjects.DocumentType = (rdr["DocumentType"].ToString());
                    objDocObjects.VersionNumber = (rdr["VersionNumber"].ToString());
                    objDocObjects.IsTraining = Convert.ToBoolean(rdr["IsTraining"].ToString());
                    objDocObjects.PendingDocRequestCount = Convert.ToInt32(rdr["PendingDocRequestCount"].ToString());
                    objDocObjects.RequestTypeID = Convert.ToInt32(rdr["RequestTypeID"].ToString());
                    objDocObjects.ReviewYear= rdr["ReviewYear"].ToString();
                    objDocObjects.ReviewYearID = Convert.ToInt32(rdr["ReviewYearID"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofDates.Add(objDocObjects);
                }
            }
            //JQDataTableBO _objJQDataTableBO = new JQDataTableBO();
            //_objJQDataTableBO.iMode = 2;
            //_objJQDataTableBO.pkid = 0;
            //_objJQDataTableBO.iDisplayLength = iDisplayLength;
            //_objJQDataTableBO.iDisplayStart = iDisplayStart;
            //_objJQDataTableBO.iSortCol = iSortCol_0;
            //_objJQDataTableBO.sSortDir = sSortDir_0;
            //_objJQDataTableBO.sSearch = sSearch;
            //_objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            //_objJQDataTableBO.RType = Convert.ToInt32(ReqType);
            //DataTable dtcount = DMS_BalDC.DMS_Listofeffectivedocuments(_objJQDataTableBO);
            var result = new
            {
                iTotalRecords = filteredCount,
                iTotalDisplayRecords = filteredCount,
                aaData = listofDates
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetDates

        #region GetReviewDocList
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetReviewDocList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_DocumentNumber = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentName = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Department = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentType = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_InitiatedBy = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_CreatedBy = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestType = HttpContext.Current.Request.Params["sSearch_9"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            var ReqType = HttpContext.Current.Request.Params["ReqTypeID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentNumber;
            objJQDataTableBO.sSearchDocumentName = sSearch_DocumentName;
            objJQDataTableBO.sSearchDepartment = sSearch_Department;
            objJQDataTableBO.sSearchDocumentType = sSearch_DocumentType;
            objJQDataTableBO.sSearchInitiatedBy = sSearch_InitiatedBy;
            objJQDataTableBO.sSearchCreatedBy = sSearch_CreatedBy;
            objJQDataTableBO.sSearchRequestType = sSearch_RequestType;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType = Convert.ToInt32(ReqType);
            objJQDataTableBO.DocStatus = "R";
            List<ReviewDocBO> listofReviewDocuments = new List<ReviewDocBO>();
            int filteredCount = 0;
            DataTable dt = DMS_BalDM.DMS_GetReviewDocuments(objJQDataTableBO);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    ReviewDocBO objDocObjects = new ReviewDocBO();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    objDocObjects.VersionID = Convert.ToInt32(rdr["VersionID"].ToString());
                    objDocObjects.Id = Convert.ToInt32(rdr["ProcessTypeID"].ToString());
                    objDocObjects.Department = (rdr["Department"].ToString());
                    objDocObjects.DocumentName = (rdr["DocumentName"].ToString());
                    objDocObjects.DocumentNumber = (rdr["DocumentNumber"].ToString());
                    objDocObjects.DocumentType = (rdr["DocumentType"].ToString());
                    objDocObjects.Initiator = (rdr["Initiator"].ToString());
                    objDocObjects.Creator = (rdr["Creator"].ToString());
                    objDocObjects.RequestType = (rdr["RequestType"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofReviewDocuments.Add(objDocObjects);
                }
            }
            //JQDataTableBO _objJQDataTableBO = new JQDataTableBO();
            //_objJQDataTableBO.iMode = 2;
            //_objJQDataTableBO.pkid = 0;
            //_objJQDataTableBO.iDisplayLength = iDisplayLength;
            //_objJQDataTableBO.iDisplayStart = iDisplayStart;
            //_objJQDataTableBO.iSortCol = iSortCol_0;
            //_objJQDataTableBO.sSortDir = sSortDir_0;
            //_objJQDataTableBO.sSearch = sSearch;
            //_objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            //_objJQDataTableBO.RType = Convert.ToInt32(ReqType);
            //_objJQDataTableBO.DocStatus = "R";
            //DataTable dtcount = DMS_BalDM.DMS_GetReviewDocuments(_objJQDataTableBO);
            var result = new
            {
                iTotalRecords = filteredCount,
                iTotalDisplayRecords = filteredCount,
                aaData = listofReviewDocuments
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetReviewDocList

        #region GetRejectedDocList
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetRejectedDocList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_DocumentNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Department = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentType = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
            var sSearch_InitiatedBy = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Status = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestType = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            var ReqType = HttpContext.Current.Request.Params["ReqTypeID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentNumber;
            objJQDataTableBO.sSearchDocumentName = sSearch_DocumentName;
            objJQDataTableBO.sSearchDepartment = sSearch_Department;
            objJQDataTableBO.sSearchDocumentType = sSearch_DocumentType;
            objJQDataTableBO.sSearchInitiatedBy = sSearch_InitiatedBy;
            objJQDataTableBO.sSearchStatus = sSearch_Status;
            objJQDataTableBO.sSearchRequestType = sSearch_RequestType;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType= Convert.ToInt32(ReqType);
            List<RejectedDocBO> listofRejectedDocuments = new List<RejectedDocBO>();
            int filteredCount = 0;
            DataTable dt = DMS_BalDC.DMS_GetNewDocIntiatorList(objJQDataTableBO);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    RejectedDocBO objDocObjects = new RejectedDocBO();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    objDocObjects.VersionID = Convert.ToInt32(rdr["VersionID"].ToString());
                    objDocObjects.Department = (rdr["Department"].ToString());
                    objDocObjects.DocumentName = (rdr["DocumentName"].ToString());
                    objDocObjects.DocumentNumber = (rdr["DocumentNumber"].ToString());
                    objDocObjects.DocumentType = (rdr["DocumentType"].ToString());
                    objDocObjects.InitiatedBy = (rdr["IntiatedBy"].ToString());
                    objDocObjects.RecordStatus = (rdr["RecordStatus"].ToString());
                    objDocObjects.RequestType = (rdr["RequestType"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofRejectedDocuments.Add(objDocObjects);
                }
            }
            //JQDataTableBO _objJQDataTableBO = new JQDataTableBO();
            //_objJQDataTableBO.iMode = 2;
            //_objJQDataTableBO.pkid = 0;
            //_objJQDataTableBO.iDisplayLength = iDisplayLength;
            //_objJQDataTableBO.iDisplayStart = iDisplayStart;
            //_objJQDataTableBO.iSortCol = iSortCol_0;
            //_objJQDataTableBO.sSortDir = sSortDir_0;
            //_objJQDataTableBO.sSearch = sSearch;
            //_objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            //_objJQDataTableBO.RType = Convert.ToInt32(ReqType);
            //DataTable dtcount = DMS_BalDC.DMS_GetNewDocIntiatorList(_objJQDataTableBO);
            var result = new
            {
                iTotalRecords = filteredCount,
                iTotalDisplayRecords = filteredCount,
                aaData = listofRejectedDocuments
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetRejectedDocList

        #region GetManageDocList
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetManageDocList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_DocumentNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Department = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentType = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
            var sSearch_InitiatedBy = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Status = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestType = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);

            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentNumber;
            objJQDataTableBO.sSearchDocumentName = sSearch_DocumentName;
            objJQDataTableBO.sSearchDepartment = sSearch_Department;
            objJQDataTableBO.sSearchDocumentType = sSearch_DocumentType;
            objJQDataTableBO.sSearchInitiatedBy = sSearch_InitiatedBy;
            objJQDataTableBO.sSearchStatus = sSearch_Status;
            objJQDataTableBO.sSearchRequestType = sSearch_RequestType;
            List<ManageDocBO> listofManageDocuments = new List<ManageDocBO>();
            int filteredCount = 0;
            DataTable dt = DMS_BalDC.DMS_GetmanagedocumentList(objJQDataTableBO);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    ManageDocBO objDocObjects = new ManageDocBO();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    objDocObjects.VersionID = Convert.ToInt32(rdr["VersionID"].ToString());
                    objDocObjects.Department = (rdr["Department"].ToString());
                    objDocObjects.DocumentName = (rdr["DocumentName"].ToString());
                    objDocObjects.DocumentNumber = (rdr["DocumentNumber"].ToString());
                    objDocObjects.DocumentType = (rdr["DocumentType"].ToString());
                    objDocObjects.InitiatedBy = (rdr["IntiatedBy"].ToString());
                    objDocObjects.RecordStatus = (rdr["RecordStatus"].ToString());
                    objDocObjects.RequestType = (rdr["RequestType"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofManageDocuments.Add(objDocObjects);
                }
            }
            //JQDataTableBO _objJQDataTableBO = new JQDataTableBO();
            //_objJQDataTableBO.iMode = 2;
            //_objJQDataTableBO.pkid = 0;
            //_objJQDataTableBO.iDisplayLength = iDisplayLength;
            //_objJQDataTableBO.iDisplayStart = iDisplayStart;
            //_objJQDataTableBO.iSortCol = iSortCol_0;
            //_objJQDataTableBO.sSortDir = sSortDir_0;
            //_objJQDataTableBO.sSearch = sSearch;
            //DataTable dtcount = DMS_BalDC.DMS_GetmanagedocumentList(_objJQDataTableBO);
            var result = new
            {
                iTotalRecords = filteredCount,
                iTotalDisplayRecords = filteredCount,
                aaData = listofManageDocuments
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetManageDocList

        #region GetQADocList
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetQADocList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_DocumentNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);           
            var sSearch_Department = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentType = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
            var sSearch_InitiatedBy = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Status = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestType = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            var ReqType = HttpContext.Current.Request.Params["ReqTypeID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentNumber;
            objJQDataTableBO.sSearchDocumentName = sSearch_DocumentName;
            objJQDataTableBO.sSearchDepartment = sSearch_Department;
            objJQDataTableBO.sSearchDocumentType = sSearch_DocumentType;
            objJQDataTableBO.sSearchInitiatedBy = sSearch_InitiatedBy;
            objJQDataTableBO.sSearchStatus = sSearch_Status;
            objJQDataTableBO.sSearchRequestType = sSearch_RequestType;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType = Convert.ToInt32(ReqType);
            List<GetQADocBO> listofQADocuments = new List<GetQADocBO>();
            int filteredCount = 0;
            DataTable dt = DMS_BalDC.DMS_GetNewDocQAList(objJQDataTableBO);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    GetQADocBO objDocObjects = new GetQADocBO();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    objDocObjects.VersionID = Convert.ToInt32(rdr["VersionID"].ToString());
                    objDocObjects.Department = (rdr["Department"].ToString());
                    objDocObjects.DocumentName = (rdr["DocumentName"].ToString());
                    objDocObjects.DocumentNumber = (rdr["DocumentNumber"].ToString());
                    objDocObjects.DocumentType = (rdr["DocumentType"].ToString());
                    objDocObjects.InitiatedBy = (rdr["IntiatedBy"].ToString());
                    objDocObjects.RecordStatus = (rdr["RecordStatus"].ToString());
                    objDocObjects.RequestType = (rdr["RequestType"].ToString());
                    objDocObjects.EffectiveDate = (rdr["EffectiveDate"].ToString());
                    objDocObjects.ExpirationDate = (rdr["ExpirationDate"].ToString());
                    objDocObjects.Status = (rdr["Status"].ToString());
                    objDocObjects.vDocumentID = Convert.ToInt32(rdr["vDocumentID"].ToString());
                    objDocObjects.TrainingCount = Convert.ToBoolean(rdr["TrainingCount"].ToString());
                    objDocObjects.IsTraining = Convert.ToBoolean(rdr["IsTraining"].ToString());
                    objDocObjects.PendingDocRequestCount= Convert.ToInt32(rdr["PendingDocRequestCount"].ToString());
                    objDocObjects.RequestTypeID= Convert.ToInt32(rdr["RequestTypeID"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofQADocuments.Add(objDocObjects);
                }
            }
            //JQDataTableBO _objJQDataTableBO = new JQDataTableBO();
            //_objJQDataTableBO.iMode = 2;
            //_objJQDataTableBO.pkid = 0;
            //_objJQDataTableBO.iDisplayLength = iDisplayLength;
            //_objJQDataTableBO.iDisplayStart = iDisplayStart;
            //_objJQDataTableBO.iSortCol = iSortCol_0;
            //_objJQDataTableBO.sSortDir = sSortDir_0;
            //_objJQDataTableBO.sSearch = sSearch;
            //_objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            //_objJQDataTableBO.RType = Convert.ToInt32(ReqType);
            //DataTable dtcount = DMS_BalDC.DMS_GetNewDocQAList(_objJQDataTableBO);
            var result = new
            {
                iTotalRecords = filteredCount,
                iTotalDisplayRecords = filteredCount,
                aaData = listofQADocuments
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetQADocList

        #region GetApproveDocList
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetApproveDocList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_DocumentNumber = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentName = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Department = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentType = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_InitiatedBy = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_CreatedBy = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestType = HttpContext.Current.Request.Params["sSearch_9"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            var ReqType = HttpContext.Current.Request.Params["ReqTypeID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentNumber;
            objJQDataTableBO.sSearchDocumentName = sSearch_DocumentName;
            objJQDataTableBO.sSearchDepartment = sSearch_Department;
            objJQDataTableBO.sSearchDocumentType = sSearch_DocumentType;
            objJQDataTableBO.sSearchInitiatedBy = sSearch_InitiatedBy;
            objJQDataTableBO.sSearchCreatedBy = sSearch_CreatedBy;           
            objJQDataTableBO.sSearchRequestType = sSearch_RequestType;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType = Convert.ToInt32(ReqType);
            objJQDataTableBO.DocStatus = "A";
            List<ApproveDocBO> listofApproveDocuments = new List<ApproveDocBO>();
            int filteredCount = 0;
            DataTable dt = DMS_BalDM.DMS_GetReviewDocuments(objJQDataTableBO);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    ApproveDocBO objDocObjects = new ApproveDocBO();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    objDocObjects.VersionID = Convert.ToInt32(rdr["VersionID"].ToString());
                    objDocObjects.Id = Convert.ToInt32(rdr["ProcessTypeID"].ToString());
                    objDocObjects.Department = (rdr["Department"].ToString());
                    objDocObjects.DocumentName = (rdr["DocumentName"].ToString());
                    objDocObjects.DocumentNumber = (rdr["DocumentNumber"].ToString());
                    objDocObjects.DocumentType = (rdr["DocumentType"].ToString());
                    objDocObjects.Initiator = (rdr["Initiator"].ToString());
                    objDocObjects.Creator = (rdr["Creator"].ToString());
                    objDocObjects.RequestType = (rdr["RequestType"].ToString());
                    objDocObjects.authorCount = Convert.ToInt32(rdr["authorCount"].ToString());
                    objDocObjects.TrainingCount = Convert.ToBoolean(rdr["TrainingCount"].ToString());
                    objDocObjects.vDocumentID = Convert.ToInt32(rdr["vDocumentID"].ToString());
                    objDocObjects.IsTraining = Convert.ToBoolean(rdr["IsTraining"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofApproveDocuments.Add(objDocObjects);
                }
            }
            //JQDataTableBO _objJQDataTableBO = new JQDataTableBO();
            //_objJQDataTableBO.iMode = 2;
            //_objJQDataTableBO.pkid = 0;
            //_objJQDataTableBO.iDisplayLength = iDisplayLength;
            //_objJQDataTableBO.iDisplayStart = iDisplayStart;
            //_objJQDataTableBO.iSortCol = iSortCol_0;
            //_objJQDataTableBO.sSortDir = sSortDir_0;
            //_objJQDataTableBO.sSearch = sSearch;
            //_objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            //_objJQDataTableBO.RType = Convert.ToInt32(ReqType);
            //_objJQDataTableBO.DocStatus = "A";
            //DataTable dtcount = DMS_BalDM.DMS_GetReviewDocuments(_objJQDataTableBO);
            var result = new
            {
                iTotalRecords = filteredCount,
                iTotalDisplayRecords = filteredCount,
                aaData = listofApproveDocuments
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetApproveDocList

        #region GetAuthorizeDocList
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetAuthorizeDocList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_DocumentNumber = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentName = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Department = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentType = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_InitiatedBy = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_CreatedBy = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestType = HttpContext.Current.Request.Params["sSearch_9"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            var ReqType = HttpContext.Current.Request.Params["ReqTypeID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentNumber;
            objJQDataTableBO.sSearchDocumentName = sSearch_DocumentName;
            objJQDataTableBO.sSearchDepartment = sSearch_Department;
            objJQDataTableBO.sSearchDocumentType = sSearch_DocumentType;
            objJQDataTableBO.sSearchInitiatedBy = sSearch_InitiatedBy;
            objJQDataTableBO.sSearchCreatedBy = sSearch_CreatedBy;
            objJQDataTableBO.sSearchRequestType = sSearch_RequestType;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType = Convert.ToInt32(ReqType);
            objJQDataTableBO.DocStatus = "Au";
            List<AuthorizeDocBO> listofAuthorizeDocuments = new List<AuthorizeDocBO>();
            int filteredCount = 0;
            DataTable dt = DMS_BalDM.DMS_GetReviewDocuments(objJQDataTableBO);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    AuthorizeDocBO objDocObjects = new AuthorizeDocBO();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    objDocObjects.VersionID = Convert.ToInt32(rdr["VersionID"].ToString());
                    objDocObjects.Id = Convert.ToInt32(rdr["ProcessTypeID"].ToString());
                    objDocObjects.Department = (rdr["Department"].ToString());
                    objDocObjects.DocumentName = (rdr["DocumentName"].ToString());
                    objDocObjects.DocumentNumber = (rdr["DocumentNumber"].ToString());
                    objDocObjects.DocumentType = (rdr["DocumentType"].ToString());
                    objDocObjects.Initiator = (rdr["Initiator"].ToString());
                    objDocObjects.Creator = (rdr["Creator"].ToString());
                    objDocObjects.RequestType = (rdr["RequestType"].ToString());
                    objDocObjects.TrainingCount = Convert.ToBoolean(rdr["TrainingCount"].ToString());
                    objDocObjects.vDocumentID = Convert.ToInt32(rdr["vDocumentID"].ToString());
                    objDocObjects.IsTraining = Convert.ToBoolean(rdr["IsTraining"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofAuthorizeDocuments.Add(objDocObjects);
                }
            }
            //JQDataTableBO _objJQDataTableBO = new JQDataTableBO();
            //_objJQDataTableBO.iMode = 2;
            //_objJQDataTableBO.pkid = 0;
            //_objJQDataTableBO.iDisplayLength = iDisplayLength;
            //_objJQDataTableBO.iDisplayStart = iDisplayStart;
            //_objJQDataTableBO.iSortCol = iSortCol_0;
            //_objJQDataTableBO.sSortDir = sSortDir_0;
            //_objJQDataTableBO.sSearch = sSearch;
            //_objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            //_objJQDataTableBO.RType = Convert.ToInt32(ReqType);
            //_objJQDataTableBO.DocStatus = "Au";
            //DataTable dtcount = DMS_BalDM.DMS_GetReviewDocuments(_objJQDataTableBO);
            var result = new
            {
                iTotalRecords = filteredCount,
                iTotalDisplayRecords = filteredCount,
                aaData = listofAuthorizeDocuments
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetAuthorizeDocList

        #region GetRenewDocList
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetRenewDocList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_DocumentNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Department = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ExpirationDate = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestType = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            var ReqType = HttpContext.Current.Request.Params["ReqTypeID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentNumber;
            objJQDataTableBO.sSearchDocumentName = sSearch_DocumentName;
            objJQDataTableBO.sSearchDepartment = sSearch_Department;
            objJQDataTableBO.sSearchExpirationDate = sSearch_ExpirationDate;
            objJQDataTableBO.sSearchRequestType = sSearch_RequestType;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType = Convert.ToInt32(ReqType);
            List<RenewDocBO> listofRenewDocuments = new List<RenewDocBO>();
            int totalCount = 0;
            DataTable dt = DMS_BalDC.DMS_ExtendDocument(objJQDataTableBO,out totalCount);
            if (dt.Rows.Count > 0)
            {
                //int RowNumber = 1;
                foreach (DataRow rdr in dt.Rows)
                {
                    RenewDocBO objDocObjects = new RenewDocBO();                 
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"]) ;
                    objDocObjects.VersionID = Convert.ToInt32(rdr["VersionID"].ToString());
                    objDocObjects.Department = (rdr["Department"].ToString());
                    objDocObjects.DocumentName = (rdr["DocumentName"].ToString());
                    objDocObjects.DocumentNumber = (rdr["DocumentNumber"].ToString());
                    objDocObjects.ExpirationDate = (rdr["ExpirationDate"].ToString());
                    objDocObjects.RequestType= (rdr["RequestTypeID"].ToString());
                    objDocObjects.PendingDocRequestCount = Convert.ToInt32(rdr["PendingDocRequestCount"].ToString());
                    //filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    //RowNumber = RowNumber + 1;
                    listofRenewDocuments.Add(objDocObjects);
                }
            }
            //JQDataTableBO _objJQDataTableBO = new JQDataTableBO();
            //_objJQDataTableBO.iMode = 2;
            //_objJQDataTableBO.pkid = 0;
            //_objJQDataTableBO.iDisplayLength = iDisplayLength;
            //_objJQDataTableBO.iDisplayStart = iDisplayStart;
            //_objJQDataTableBO.iSortCol = iSortCol_0;
            //_objJQDataTableBO.sSortDir = sSortDir_0;
            //_objJQDataTableBO.sSearch = sSearch;
            //_objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            //_objJQDataTableBO.RType = Convert.ToInt32(ReqType);
            //DataTable dtcount = DMS_BalDC.DMS_ExtendDocument(_objJQDataTableBO);

            var result = new
            {
                iTotalRecords = dt.Rows.Count,
                iTotalDisplayRecords = totalCount,
                aaData = listofRenewDocuments
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetRenewDocList

        #region GetNewDocCreatorList
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetNewDocCreatorList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_DocumentNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Department = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentType = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
            var sSearch_InitiatedBy = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestType = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            var ReqType = HttpContext.Current.Request.Params["ReqTypeID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentNumber;
            objJQDataTableBO.sSearchDocumentName = sSearch_DocumentName;
            objJQDataTableBO.sSearchDepartment = sSearch_Department;
            objJQDataTableBO.sSearchDocumentType = sSearch_DocumentType;
            objJQDataTableBO.sSearchInitiatedBy = sSearch_InitiatedBy;
            objJQDataTableBO.sSearchRequestType = sSearch_RequestType;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType = Convert.ToInt32(ReqType);
            List<NewDocCreatorBO> listofNewCreatorDocuments = new List<NewDocCreatorBO>();
            int filteredCount = 0;
            DataTable dt = DMS_BalDC.DMS_GetNewDocCreatorList(objJQDataTableBO);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    NewDocCreatorBO objDocObjects = new NewDocCreatorBO();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    objDocObjects.VersionID = Convert.ToInt32(rdr["VersionID"].ToString());
                    objDocObjects.Department = (rdr["Department"].ToString());
                    objDocObjects.DocumentName = (rdr["DocumentName"].ToString());
                    objDocObjects.DocumentNumber = (rdr["DocumentNumber"].ToString());
                    objDocObjects.DocumentType = (rdr["DocumentType"].ToString());
                    objDocObjects.Initiator = (rdr["IntiatedBy"].ToString());
                    objDocObjects.RequestType = (rdr["RequestType"].ToString());
                    objDocObjects.Create = (rdr["Create"].ToString());
                    objDocObjects.ProcessTypeID = Convert.ToInt32(rdr["ProcessTypeID"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofNewCreatorDocuments.Add(objDocObjects);
                }
            }
            //JQDataTableBO _objJQDataTableBO = new JQDataTableBO();
            //_objJQDataTableBO.iMode = 2;
            //_objJQDataTableBO.pkid = 0;
            //_objJQDataTableBO.iDisplayLength = iDisplayLength;
            //_objJQDataTableBO.iDisplayStart = iDisplayStart;
            //_objJQDataTableBO.iSortCol = iSortCol_0;
            //_objJQDataTableBO.sSortDir = sSortDir_0;
            //_objJQDataTableBO.sSearch = sSearch;
            //_objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            //_objJQDataTableBO.RType = Convert.ToInt32(ReqType);
            //DataTable dtcount = DMS_BalDC.DMS_GetNewDocCreatorList(_objJQDataTableBO);
            var result = new
            {
                iTotalRecords = filteredCount,
                iTotalDisplayRecords = filteredCount,
                aaData = listofNewCreatorDocuments
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetNewDocCreatorList

        #region GetPendingDocCreatorList
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetPendingDocCreatorList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_DocumentNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Department = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentType = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
            var sSearch_InitiatedBy = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestType = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            var ReqType = HttpContext.Current.Request.Params["ReqTypeID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentNumber;
            objJQDataTableBO.sSearchDocumentName = sSearch_DocumentName;
            objJQDataTableBO.sSearchDepartment = sSearch_Department;
            objJQDataTableBO.sSearchDocumentType = sSearch_DocumentType;
            objJQDataTableBO.sSearchInitiatedBy = sSearch_InitiatedBy;
            objJQDataTableBO.sSearchRequestType = sSearch_RequestType;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType = Convert.ToInt32(ReqType);
            List<PendingDocCreatorBO> listofPendingCreatorDocuments = new List<PendingDocCreatorBO>();
            int filteredCount = 0;
            DataTable dt = DMS_BalDC.DMS_GetPendingDocCreatorList(objJQDataTableBO);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    PendingDocCreatorBO objDocObjects = new PendingDocCreatorBO();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    objDocObjects.VersionID = Convert.ToInt32(rdr["VersionID"].ToString());
                    objDocObjects.Department = (rdr["Department"].ToString());
                    objDocObjects.DocumentName = (rdr["DocumentName"].ToString());
                    objDocObjects.DocumentNumber = (rdr["DocumentNumber"].ToString());
                    objDocObjects.DocumentType = (rdr["DocumentType"].ToString());
                    objDocObjects.Initiator = (rdr["IntiatedBy"].ToString());
                    objDocObjects.RequestType = (rdr["RequestType"].ToString());
                    objDocObjects.ProcessTypeID = Convert.ToInt32(rdr["ProcessTypeID"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofPendingCreatorDocuments.Add(objDocObjects);
                }
            }
            //JQDataTableBO _objJQDataTableBO = new JQDataTableBO();
            //_objJQDataTableBO.iMode = 2;
            //_objJQDataTableBO.pkid = 0;
            //_objJQDataTableBO.iDisplayLength = iDisplayLength;
            //_objJQDataTableBO.iDisplayStart = iDisplayStart;
            //_objJQDataTableBO.iSortCol = iSortCol_0;
            //_objJQDataTableBO.sSortDir = sSortDir_0;
            //_objJQDataTableBO.sSearch = sSearch;
            //_objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            //objJQDataTableBO.RType = Convert.ToInt32(ReqType);
            //DataTable dtcount = DMS_BalDC.DMS_GetPendingDocCreatorList(_objJQDataTableBO);
            var result = new
            {
                iTotalRecords = filteredCount,
                iTotalDisplayRecords = filteredCount,
                aaData = listofPendingCreatorDocuments
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetPendingDocCreatorList

        #region GetAdminDocList
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetAdminDocList()
        {
            Session["MainDocumentList"] = null;
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_DocumentNumber = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentName = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_VersionNumber = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentType = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Department = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestType = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);
            var sSearch_EffectiveDate = HttpContext.Current.Request.Params["sSearch_9"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ExpirationDate = HttpContext.Current.Request.Params["sSearch_10"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Status = HttpContext.Current.Request.Params["sSearch_11"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentNumber;
            objJQDataTableBO.sSearchDocumentName = sSearch_DocumentName;
            objJQDataTableBO.sSearchVersionNumber = sSearch_VersionNumber;
            objJQDataTableBO.sSearchDocumentType = sSearch_DocumentType;
            objJQDataTableBO.sSearchDepartment = sSearch_Department;
            objJQDataTableBO.sSearchRequestType = sSearch_RequestType;
            objJQDataTableBO.sSearchEffectiveDate = sSearch_EffectiveDate;
            objJQDataTableBO.sSearchExpirationDate = sSearch_ExpirationDate;
            objJQDataTableBO.sSearchStatus = sSearch_Status;
            objJQDataTableBO.ChartStatus = 0;
            objJQDataTableBO.ChartDocTypeID = 0;
            objJQDataTableBO.ChartDeptID = 0;
            objJQDataTableBO.ChartFromDate = DateTime.Now;
            objJQDataTableBO.ChartToDate = DateTime.Now;
            objJQDataTableBO.DocStatus = string.Empty;
            objJQDataTableBO.ChartDeptCode = string.Empty;
            objJQDataTableBO.ChartDocTypeName = string.Empty;
            objJQDataTableBO.EmpID = 0;
            objJQDataTableBO.IsJQueyList = "Y";



            DataTable dtx = (HttpContext.Current.Session["UserDetails"] as DataSet).Tables[1];
            DataTable dtTemp = new DataTable();
            DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
            if (drUMS.Length > 0)
            {
                dtTemp = drUMS.CopyToDataTable();
                if (dtTemp.Select("RoleID=11").Length > 0)//11-author role
                {
                    objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
                }
                if (dtTemp.Select("RoleID=2").Length > 0)//2-admin role
                {
                    objJQDataTableBO.EmpID = 0;
                }
                if (dtTemp.Select("RoleID=9").Length > 0)//9-approver role
                {
                    objJQDataTableBO.EmpID = 0;
                }
                if (dtTemp.Select("RoleID=10").Length > 0)//10-reviewer role
                {
                    objJQDataTableBO.EmpID = 0;
                }
                if (dtTemp.Select("RoleID=12").Length > 0)//12-initiator role
                {
                    objJQDataTableBO.EmpID = 0;
                }
                if (dtTemp.Select("RoleID=13").Length > 0)//13-authorizer role
                {
                    objJQDataTableBO.EmpID = 0;
                }
            }
            List<AdminDocListBO> listofAdminDocuments = new List<AdminDocListBO>();
            int filteredCount = 0;
            DataTable dt = new DataTable();
            if (Session["ChartStatus"].ToString() != "0")
            {
                if (Session["ChartStatus"].ToString() == "1")
                {

                    objJQDataTableBO.ChartStatus = 1;
                    objJQDataTableBO.ChartDocTypeID = Convert.ToInt32(Session["ChartDocumentType"].ToString());
                    objJQDataTableBO.ChartDeptID = Convert.ToInt32(Session["ChartDepartment"].ToString());
                    objJQDataTableBO.ChartDeptCode = Session["ChartDeptCode"].ToString();
                    objJQDataTableBO.ChartDocTypeName = Session["ChartDocTypeName"].ToString();
                }
                if (Session["ChartStatus"].ToString() == "3")
                {

                    objJQDataTableBO.ChartStatus = 3;
                    objJQDataTableBO.ChartFromDate = Convert.ToDateTime(Session["ChartFromDate"].ToString());
                    objJQDataTableBO.ChartToDate = Convert.ToDateTime(Session["ChartToDate"].ToString());
                    objJQDataTableBO.ChartDeptCode = Session["ChartDeptCode"].ToString();
                }
                if (Session["ChartStatus"].ToString() == "2")
                {

                    objJQDataTableBO.ChartStatus = 2;
                    objJQDataTableBO.ChartDocTypeID = Convert.ToInt32(Session["ChartDocumentType"].ToString());
                    objJQDataTableBO.ChartDeptID = Convert.ToInt32(Session["ChartDepartment"].ToString());
                    objJQDataTableBO.ChartFromDate = Convert.ToDateTime(Session["ChartFromDate"].ToString());
                    objJQDataTableBO.ChartToDate = Convert.ToDateTime(Session["ChartToDate"].ToString());
                    objJQDataTableBO.ChartDeptCode = Session["ChartDeptCode"].ToString();
                    objJQDataTableBO.ChartDocTypeName = Session["ChartDocTypeName"].ToString();
                }
                if (Session["ChartStatus"].ToString() == "4")
                {

                    objJQDataTableBO.ChartStatus = 4;
                    objJQDataTableBO.ChartDocTypeID = Convert.ToInt32(Session["ChartDocumentType"].ToString());
                    objJQDataTableBO.ChartDeptID = Convert.ToInt32(Session["ChartDepartment"].ToString());
                    objJQDataTableBO.ChartDeptCode = Session["ChartDeptCode"].ToString();
                    objJQDataTableBO.ChartDocTypeName = Session["ChartDocTypeName"].ToString();
                }
                if (Session["ChartStatus"].ToString() == "5")
                {

                    objJQDataTableBO.ChartStatus = 5;
                    objJQDataTableBO.ChartDocTypeID = Convert.ToInt32(Session["ChartDocumentType"].ToString());
                    objJQDataTableBO.ChartDeptID = Convert.ToInt32(Session["ChartDepartment"].ToString());
                    objJQDataTableBO.ChartDeptCode = Session["ChartDeptCode"].ToString();
                    objJQDataTableBO.ChartDocTypeName = Session["ChartDocTypeName"].ToString();
                }
                if (Session["ChartStatus"].ToString() == "6")
                {

                    objJQDataTableBO.ChartStatus = 6;
                    objJQDataTableBO.ChartDocTypeID = Convert.ToInt32(Session["ChartDocumentType"].ToString());
                    objJQDataTableBO.ChartDeptID = Convert.ToInt32(Session["ChartDepartment"].ToString());
                    objJQDataTableBO.ChartDeptCode = Session["ChartDeptCode"].ToString();
                    objJQDataTableBO.ChartDocTypeName = Session["ChartDocTypeName"].ToString();
                }
                if (Session["ChartStatus"].ToString() == "7")
                {

                    objJQDataTableBO.ChartStatus = 7;
                    objJQDataTableBO.ChartDocTypeID = Convert.ToInt32(Session["ChartDocumentType"].ToString());
                    objJQDataTableBO.ChartDeptID = Convert.ToInt32(Session["ChartDepartment"].ToString());
                    objJQDataTableBO.ChartDeptCode = Session["ChartDeptCode"].ToString();
                    objJQDataTableBO.ChartDocTypeName = Session["ChartDocTypeName"].ToString();
                }
                if (Session["ChartStatus"].ToString() == "8")
                {

                    objJQDataTableBO.ChartStatus = 8;
                    objJQDataTableBO.ChartDocTypeID = Convert.ToInt32(Session["ChartDocumentType"].ToString());
                    objJQDataTableBO.ChartDeptID = Convert.ToInt32(Session["ChartDepartment"].ToString());
                    objJQDataTableBO.ChartDeptCode = Session["ChartDeptCode"].ToString();
                    objJQDataTableBO.ChartDocTypeName = Session["ChartDocTypeName"].ToString();
                }
            }
            //Assign all JQueryTableparameters
            Session["MainDocumentList"] = objJQDataTableBO;
            dt = DMS_BalDC.DMS_AdminListOFDocuments(objJQDataTableBO);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    AdminDocListBO objDocObjects = new AdminDocListBO();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    objDocObjects.VersionID = Convert.ToInt32(rdr["VersionID"].ToString());
                    objDocObjects.vDocumentID = Convert.ToInt32(rdr["vDocumentID"].ToString());
                    objDocObjects.Department = (rdr["Department"].ToString());
                    objDocObjects.DocumentName = (rdr["DocumentName"].ToString());
                    objDocObjects.VersionNumber = (rdr["VersionNumber"].ToString());
                    objDocObjects.DocumentNumber = (rdr["DocumentNumber"].ToString());
                    objDocObjects.DocumentType = (rdr["DocumentType"].ToString());
                    objDocObjects.RequestType = (rdr["RequestType"].ToString());
                    objDocObjects.statusMsg = (rdr["statusMsg"].ToString());
                    objDocObjects.EffectiveDate = (rdr["EffectiveDate"].ToString());
                    objDocObjects.ExpirationDate = (rdr["ExpirationDate"].ToString());
                    objDocObjects.expStatus = Convert.ToInt32(rdr["expStatus"].ToString());
                    objDocObjects.DocLocationCount = (rdr["DocLocationCount"].ToString());
                    objDocObjects.DocTitleLablName= (rdr["ReportTitle"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofAdminDocuments.Add(objDocObjects);
                }
            }
            //JQDataTableBO _objJQDataTableBO = new JQDataTableBO();
            //_objJQDataTableBO.iMode = 2;
            //_objJQDataTableBO.pkid = 0;
            //_objJQDataTableBO.iDisplayLength = iDisplayLength;
            //_objJQDataTableBO.iDisplayStart = iDisplayStart;
            //_objJQDataTableBO.iSortCol = iSortCol_0;
            //_objJQDataTableBO.sSortDir = sSortDir_0;
            //_objJQDataTableBO.sSearch = sSearch;
            //if (drUMS.Length > 0)
            //{
            //    dtTemp = drUMS.CopyToDataTable();
            //    if (dtTemp.Select("RoleID=11").Length > 0)//11-author role
            //    {
            //        _objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            //    }
            //    if (dtTemp.Select("RoleID=2").Length > 0)//2-admin role
            //    {
            //        objJQDataTableBO.EmpID = 0;
            //    }
            //    if (dtTemp.Select("RoleID=9").Length > 0)//9-approver role
            //    {
            //        objJQDataTableBO.EmpID = 0;
            //    }
            //    if (dtTemp.Select("RoleID=10").Length > 0)//10-reviewer role
            //    {
            //        objJQDataTableBO.EmpID = 0;
            //    }
            //    if (dtTemp.Select("RoleID=12").Length > 0)//12-initiator role
            //    {
            //        objJQDataTableBO.EmpID = 0;
            //    }
            //    if (dtTemp.Select("RoleID=13").Length > 0)//13-authorizer role
            //    {
            //        objJQDataTableBO.EmpID = 0;
            //    }
            //}
            //DataTable dtcount = DMS_BalDC.DMS_AdminListOFDocuments(_objJQDataTableBO);
            var result = new
            {
                iTotalRecords = filteredCount,
                iTotalDisplayRecords = filteredCount,
                aaData = listofAdminDocuments
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetAdminDocList

        #region GetObsoleteDocList
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetObsoleteDocList()
        {
            //Session["MainDocumentList"] = null;
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_DocumentNumber = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentName = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_VersionNumber = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentType = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Department = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestType = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);
            var sSearch_EffectiveDate = HttpContext.Current.Request.Params["sSearch_9"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ExpirationDate = HttpContext.Current.Request.Params["sSearch_10"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Status = HttpContext.Current.Request.Params["sSearch_11"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 2;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentNumber;
            objJQDataTableBO.sSearchDocumentName = sSearch_DocumentName;
            objJQDataTableBO.sSearchVersionNumber = sSearch_VersionNumber;
            objJQDataTableBO.sSearchDocumentType = sSearch_DocumentType;
            objJQDataTableBO.sSearchDepartment = sSearch_Department;
            objJQDataTableBO.sSearchRequestType = sSearch_RequestType;
            objJQDataTableBO.sSearchEffectiveDate = sSearch_EffectiveDate;
            objJQDataTableBO.sSearchExpirationDate = sSearch_ExpirationDate;
            objJQDataTableBO.sSearchStatus = sSearch_Status;
            objJQDataTableBO.EmpID = 0;
            objJQDataTableBO.IsJQueyList = "Y";
            objJQDataTableBO.ChartStatus = 0;
            List<AdminDocListBO> listofAdminDocuments = new List<AdminDocListBO>();
            int filteredCount = 0;
            DataTable dt = new DataTable();
            
            //Assign all JQueryTableparameters
            Session["MainDocumentList"] = objJQDataTableBO;
            dt = DMS_BalDC.DMS_AdminListOFDocuments(objJQDataTableBO);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    AdminDocListBO objDocObjects = new AdminDocListBO();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    objDocObjects.VersionID = Convert.ToInt32(rdr["VersionID"].ToString());
                    objDocObjects.vDocumentID = Convert.ToInt32(rdr["vDocumentID"].ToString());
                    objDocObjects.Department = (rdr["Department"].ToString());
                    objDocObjects.DocumentName = (rdr["DocumentName"].ToString());
                    objDocObjects.VersionNumber = (rdr["VersionNumber"].ToString());
                    objDocObjects.DocumentNumber = (rdr["DocumentNumber"].ToString());
                    objDocObjects.DocumentType = (rdr["DocumentType"].ToString());
                    objDocObjects.RequestType = (rdr["RequestType"].ToString());
                    objDocObjects.statusMsg = (rdr["statusMsg"].ToString());
                    objDocObjects.EffectiveDate = (rdr["EffectiveDate"].ToString());
                    objDocObjects.ExpirationDate = (rdr["ExpirationDate"].ToString());
                    objDocObjects.expStatus = Convert.ToInt32(rdr["expStatus"].ToString());
                    objDocObjects.DocLocationCount = (rdr["DocLocationCount"].ToString());
                    objDocObjects.VersionCount =Convert.ToInt32 (rdr["VersionCount"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofAdminDocuments.Add(objDocObjects);
                }
            }
            
            var result = new
            {
                iTotalRecords = filteredCount,
                iTotalDisplayRecords = filteredCount,
                aaData = listofAdminDocuments
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetAdminDocList

        #region GetDocPrintReqList
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetDocPrintReqList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_RequestNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentNumber = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentVersionNumber = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
            var sSearch_PrintPurpose = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_PageNumbers = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ApprovedBy = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestBy = HttpContext.Current.Request.Params["sSearch_9"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestedDate = HttpContext.Current.Request.Params["sSearch_10"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchRequestNumber = sSearch_RequestNumber;
            objJQDataTableBO.sSearchDocumentName = sSearch_DocumentName;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentNumber;
            objJQDataTableBO.sSearchVersionNumber = sSearch_DocumentVersionNumber;
            objJQDataTableBO.sSearchPurpose = sSearch_PrintPurpose;
            objJQDataTableBO.sSearchPageNumbers = sSearch_PageNumbers;
            objJQDataTableBO.sSearchApprovedBy = sSearch_ApprovedBy;
            objJQDataTableBO.sSearchRequestBy = sSearch_RequestBy;
            objJQDataTableBO.sSearchRequestedDate = sSearch_RequestedDate;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType = 1;
            List<DocPrintRequest> listofDocPrintRequestDocuments = new List<DocPrintRequest>();
            int filteredCount = 0;
            DataSet ds = DMS_BalDC.DMS_DocPrintReqList(objJQDataTableBO);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in ds.Tables[0].Rows)
                {
                    DocPrintRequest objDocObjects = new DocPrintRequest();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"].ToString());
                    objDocObjects.DocReqID = Convert.ToInt32(rdr["DocRequestID"].ToString());
                    objDocObjects.RequestNumber = (rdr["RequestNumber"].ToString());
                    objDocObjects.DocumentName = (rdr["DocumentName"].ToString());
                    objDocObjects.DocumentNumber = (rdr["DocumentNumber"].ToString());
                    objDocObjects.DocumentVersionNumber = (rdr["DocumentVersionNumber"].ToString());
                    objDocObjects.PrintPurpose = (rdr["PrintPurpose"].ToString());
                    objDocObjects.PageNumbers = (rdr["PageNumbers"].ToString());
                    objDocObjects.ActionStatusID = Convert.ToInt32(rdr["ActionStatusID"].ToString());
                    objDocObjects.DocVersionID= Convert.ToInt32(rdr["DocVersionID"].ToString());
                    objDocObjects.IsSoftCopy = Convert.ToBoolean(rdr["isSoftCopy"].ToString());
                    objDocObjects.RequestBy = rdr["RequestBy"].ToString();
                    objDocObjects.ApprovedBy = rdr["ApprovedBy"].ToString();
                    objDocObjects.RequestedDate = rdr["RequestedDate"].ToString();
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofDocPrintRequestDocuments.Add(objDocObjects);
                }
            }         
            var result = new
            {
                iTotalRecords = ds.Tables[1].Rows.Count > 0 ? Convert.ToInt32(ds.Tables[1].Rows[0][0]) : 0,
                iTotalDisplayRecords = filteredCount,
                aaData = listofDocPrintRequestDocuments
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetDocPrintReqList

        #region GetDocPrintReqReviewerList
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetDocPrintReqReviewerList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_RequestNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentNumber = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentVersionNumber = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);

            var sSearch_PrintPurpose = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_PageNumbers = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ApprovedBy = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestBy = HttpContext.Current.Request.Params["sSearch_9"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestedDate = HttpContext.Current.Request.Params["sSearch_10"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchRequestNumber = sSearch_RequestNumber;
            objJQDataTableBO.sSearchDocumentName = sSearch_DocumentName;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentNumber;
            objJQDataTableBO.sSearchVersionNumber = sSearch_DocumentVersionNumber;
            objJQDataTableBO.sSearchPurpose = sSearch_PrintPurpose;
            objJQDataTableBO.sSearchPageNumbers = sSearch_PageNumbers;
            objJQDataTableBO.sSearchApprovedBy = sSearch_ApprovedBy;
            objJQDataTableBO.sSearchRequestBy = sSearch_RequestBy;
            objJQDataTableBO.sSearchRequestedDate = sSearch_RequestedDate;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType = 2;
            List<DocPrintRequest> listofDocPrintRequestDocuments = new List<DocPrintRequest>();
            int filteredCount = 0;
            DataSet ds = DMS_BalDC.DMS_DocPrintReqList(objJQDataTableBO);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in ds.Tables[0].Rows)
                {
                    DocPrintRequest objDocObjects = new DocPrintRequest();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"].ToString());
                    objDocObjects.DocReqID = Convert.ToInt32(rdr["DocRequestID"].ToString());
                    objDocObjects.RequestNumber = (rdr["RequestNumber"].ToString());
                    objDocObjects.DocumentName = (rdr["DocumentName"].ToString());
                    objDocObjects.DocumentNumber= (rdr["DocumentNumber"].ToString());
                    objDocObjects.DocumentVersionNumber= (rdr["DocumentVersionNumber"].ToString());
                    objDocObjects.PrintPurpose = (rdr["PrintPurpose"].ToString());
                    objDocObjects.PageNumbers = (rdr["PageNumbers"].ToString());
                    objDocObjects.DocVersionID = Convert.ToInt32(rdr["DocVersionID"].ToString());
                    objDocObjects.RequestBy = rdr["RequestBy"].ToString();
                    objDocObjects.ApprovedBy = rdr["ApprovedBy"].ToString();
                    objDocObjects.RequestedDate = rdr["RequestedDate"].ToString();
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofDocPrintRequestDocuments.Add(objDocObjects);
                }
            }          
            var result = new
            {
                iTotalRecords = ds.Tables[1].Rows.Count > 0 ? Convert.ToInt32(ds.Tables[1].Rows[0][0]) : 0,
                iTotalDisplayRecords = filteredCount,
                aaData = listofDocPrintRequestDocuments
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetDocPrintReqReviewerList

        #region GetDocPrintReqControllerList
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetDocPrintReqControllerList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_RequestNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentNumber = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentVersionNumber = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
            var sSearch_PrintPurpose = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_PageNumbers = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ApprovedBy = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestBy = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestedDate = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchRequestNumber = sSearch_RequestNumber;
            objJQDataTableBO.sSearchDocumentName = sSearch_DocumentName;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentNumber;
            objJQDataTableBO.sSearchVersionNumber = sSearch_DocumentVersionNumber;
            objJQDataTableBO.sSearchPurpose = sSearch_PrintPurpose;
            objJQDataTableBO.sSearchPageNumbers = sSearch_PageNumbers;
            objJQDataTableBO.sSearchApprovedBy = sSearch_ApprovedBy;
            objJQDataTableBO.sSearchRequestBy = sSearch_RequestBy;
            objJQDataTableBO.sSearchRequestedDate = sSearch_RequestedDate;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType = 3;
            List<DocPrintRequest> listofDocPrintRequestDocuments = new List<DocPrintRequest>();
            int filteredCount = 0;
            DataSet ds = DMS_BalDC.DMS_DocPrintReqList(objJQDataTableBO);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in ds.Tables[0].Rows)
                {
                    DocPrintRequest objDocObjects = new DocPrintRequest();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"].ToString());
                    objDocObjects.DocReqID = Convert.ToInt32(rdr["DocRequestID"].ToString());
                    objDocObjects.RequestNumber = (rdr["RequestNumber"].ToString());
                    objDocObjects.DocumentName = (rdr["DocumentName"].ToString());
                    objDocObjects.DocumentNumber = (rdr["DocumentNumber"].ToString());
                    objDocObjects.DocumentVersionNumber = (rdr["DocumentVersionNumber"].ToString());
                    objDocObjects.PrintPurpose = (rdr["PrintPurpose"].ToString());
                    objDocObjects.PageNumbers = (rdr["PageNumbers"].ToString());
                    objDocObjects.DocVersionID = Convert.ToInt32(rdr["DocVersionID"].ToString());
                    objDocObjects.RequestBy = rdr["RequestBy"].ToString();
                    objDocObjects.ApprovedBy = rdr["ApprovedBy"].ToString();
                    objDocObjects.RequestedDate = rdr["RequestedDate"].ToString();
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofDocPrintRequestDocuments.Add(objDocObjects);
                }
            }
            var result = new
            {
                iTotalRecords = ds.Tables[1].Rows.Count > 0 ? Convert.ToInt32(ds.Tables[1].Rows[0][0]) : 0,
                iTotalDisplayRecords = filteredCount,
                aaData = listofDocPrintRequestDocuments
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetDocPrintReqControllerList

        #region GetDocPrintReqManageList
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetDocPrintReqManageList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_RequestNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentNumber= HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentVersionNumber = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);

            var sSearch_PrintPurpose = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_PageNumbers = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ApprovedBy = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestBy = HttpContext.Current.Request.Params["sSearch_9"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestedDate = HttpContext.Current.Request.Params["sSearch_10"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchRequestNumber = sSearch_RequestNumber;
            objJQDataTableBO.sSearchDocumentName = sSearch_DocumentName;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentVersionNumber;
            objJQDataTableBO.sSearchVersionNumber = sSearch_DocumentVersionNumber;
            objJQDataTableBO.sSearchPurpose = sSearch_PrintPurpose;
            objJQDataTableBO.sSearchPageNumbers = sSearch_PageNumbers;
            objJQDataTableBO.sSearchApprovedBy = sSearch_ApprovedBy;
            objJQDataTableBO.sSearchRequestBy = sSearch_RequestBy;
            objJQDataTableBO.sSearchRequestedDate = sSearch_RequestedDate;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType = 4;
            List<DocPrintRequest> listofDocPrintRequestDocuments = new List<DocPrintRequest>();
            int filteredCount = 0;
            DataSet ds = DMS_BalDC.DMS_DocPrintReqList(objJQDataTableBO);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in ds.Tables[0].Rows)
                {
                    DocPrintRequest objDocObjects = new DocPrintRequest();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"].ToString());
                    objDocObjects.DocReqID = Convert.ToInt32(rdr["DocRequestID"].ToString());
                    objDocObjects.RequestNumber = (rdr["RequestNumber"].ToString());
                    objDocObjects.DocumentName = (rdr["DocumentName"].ToString());
                    objDocObjects.DocumentNumber = (rdr["DocumentNumber"].ToString());
                    objDocObjects.DocumentVersionNumber = (rdr["DocumentVersionNumber"].ToString());
                    objDocObjects.PrintPurpose = (rdr["PrintPurpose"].ToString());
                    objDocObjects.PageNumbers = (rdr["PageNumbers"].ToString());
                    objDocObjects.DocVersionID = Convert.ToInt32(rdr["DocVersionID"].ToString());
                    objDocObjects.RequestBy = rdr["RequestBy"].ToString();
                    objDocObjects.ApprovedBy = rdr["ApprovedBy"].ToString();
                    objDocObjects.RequestedDate = rdr["RequestedDate"].ToString();
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofDocPrintRequestDocuments.Add(objDocObjects);
                }
            }
            var result = new
            {
                iTotalRecords = ds.Tables[1].Rows.Count > 0 ? Convert.ToInt32(ds.Tables[1].Rows[0][0]) : 0,
                iTotalDisplayRecords = filteredCount,
                aaData = listofDocPrintRequestDocuments
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetDocPrintReqManageList

        #region GetFormCreationList
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetFormCreationList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            var ReqType = HttpContext.Current.Request.Params["ReqTypeID"].ToString(CultureInfo.CurrentCulture);

            var sSearch_FormNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_FormName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Purpose = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_VersionNumber = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentNumber = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Department = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);            
            var sSearch_CreatedBy = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ApprovedBy = HttpContext.Current.Request.Params["sSearch_9"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ActionName = HttpContext.Current.Request.Params["sSearch_10"].ToString(CultureInfo.CurrentCulture);
            var sSearch_FormRequestType = HttpContext.Current.Request.Params["sSearch_11"].ToString(CultureInfo.CurrentCulture);

            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchFormNumber = sSearch_FormNumber;
            objJQDataTableBO.sSearchFormName = sSearch_FormName;
            objJQDataTableBO.sSearchPurpose = sSearch_Purpose;
            objJQDataTableBO.sSearchVersionNumber = sSearch_VersionNumber;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentNumber;
            objJQDataTableBO.sSearchDepartment = sSearch_Department;            
            objJQDataTableBO.sSearchCreatedBy = sSearch_CreatedBy;
            objJQDataTableBO.sSearchApprovedBy = sSearch_ApprovedBy;
            objJQDataTableBO.sSearchActionName = sSearch_ActionName;
            objJQDataTableBO.sSearchRequestType = sSearch_FormRequestType;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType = 1;
            objJQDataTableBO.ReqTypeID = Convert.ToInt32(ReqType);
            List<DocObjects> listofForms = new List<DocObjects>();
            int filteredCount = 0;
            DataSet ds= DMS_BalDC.DMS_GetFormList(objJQDataTableBO);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in ds.Tables[0].Rows)
                {
                    DocObjects objDocObjects = new DocObjects();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"].ToString());
                    objDocObjects.FormVersionID = Convert.ToInt32(rdr["FormVersionID"].ToString());
                    objDocObjects.FormNumber = (rdr["FormNumber"].ToString());
                    objDocObjects.FormName = (rdr["FormName"].ToString());
                    objDocObjects.Purpose = (rdr["FormDescription"].ToString());
                    objDocObjects.VersionNumber = (rdr["FormVersionNumber"].ToString());
                    objDocObjects.dept = (rdr["DepartmentName"].ToString());
                    objDocObjects.documentNUmber = (rdr["RefDocNumber"].ToString());
                    objDocObjects.CreatedBy = (rdr["CreatorName"].ToString());
                    objDocObjects.oldApprovedBY = (rdr["ApproverName"].ToString());
                    objDocObjects.action = Convert.ToInt32(rdr["ActionStatusID"].ToString());
                    objDocObjects.ActionName = rdr["ActionName"].ToString();
                    objDocObjects.RequestTypeID = Convert.ToInt32(rdr["FormRequestTypeID"].ToString());
                    objDocObjects.FormRequestType = (rdr["FormRequestType"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofForms.Add(objDocObjects);
                }
            }
            var result = new
            {
                iTotalRecords = ds.Tables[1].Rows.Count > 0 ? Convert.ToInt32(ds.Tables[1].Rows[0][0]) : 0,
                iTotalDisplayRecords = filteredCount,
                aaData = listofForms
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetFormCreationList

        #region GetFormApproverList
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetFormApproverList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_FormNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_FormName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Purpose = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_VersionNumber = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentNumber = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Department = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_CreatedBy = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ApprovedBy = HttpContext.Current.Request.Params["sSearch_9"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ActionName = HttpContext.Current.Request.Params["sSearch_10"].ToString(CultureInfo.CurrentCulture);
            var sSearch_FormRequestType = HttpContext.Current.Request.Params["sSearch_11"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            var ReqType = HttpContext.Current.Request.Params["ReqTypeID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchFormNumber = sSearch_FormNumber;
            objJQDataTableBO.sSearchFormName = sSearch_FormName;
            objJQDataTableBO.sSearchPurpose = sSearch_Purpose;
            objJQDataTableBO.sSearchVersionNumber = sSearch_VersionNumber;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentNumber;
            objJQDataTableBO.sSearchDepartment = sSearch_Department;
            objJQDataTableBO.sSearchCreatedBy = sSearch_CreatedBy;
            objJQDataTableBO.sSearchApprovedBy = sSearch_ApprovedBy;
            objJQDataTableBO.sSearchActionName = sSearch_ActionName;
            objJQDataTableBO.sSearchRequestType = sSearch_FormRequestType;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType = 2;
            objJQDataTableBO.ReqTypeID = Convert.ToInt32(ReqType);
            List<DocObjects> listofForms = new List<DocObjects>();
            int filteredCount = 0;
            DataSet ds= DMS_BalDC.DMS_GetFormList(objJQDataTableBO);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in ds.Tables[0].Rows)
                {
                    DocObjects objDocObjects = new DocObjects();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"].ToString());
                    objDocObjects.FormVersionID = Convert.ToInt32(rdr["FormVersionID"].ToString());
                    objDocObjects.FormNumber = (rdr["FormNumber"].ToString());
                    objDocObjects.FormName = (rdr["FormName"].ToString());
                    objDocObjects.Purpose = (rdr["FormDescription"].ToString());
                    objDocObjects.VersionNumber = (rdr["FormVersionNumber"].ToString());
                    objDocObjects.dept = (rdr["DepartmentName"].ToString());
                    objDocObjects.documentNUmber = (rdr["RefDocNumber"].ToString());
                    objDocObjects.CreatedBy = (rdr["CreatorName"].ToString());
                    objDocObjects.oldApprovedBY = (rdr["ApproverName"].ToString());
                    objDocObjects.action = Convert.ToInt32(rdr["ActionStatusID"].ToString());
                    objDocObjects.ActionName = rdr["ActionName"].ToString();
                    objDocObjects.RequestTypeID = Convert.ToInt32(rdr["FormRequestTypeID"].ToString());
                    objDocObjects.FormRequestType = (rdr["FormRequestType"].ToString());
                    objDocObjects.PendingFormRequestCount = Convert.ToInt32(rdr["PendingFormRequestCount"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofForms.Add(objDocObjects);
                }
            }
            var result = new
            {
                iTotalRecords = ds.Tables[1].Rows.Count > 0 ? Convert.ToInt32(ds.Tables[1].Rows[0][0]) : 0,
                iTotalDisplayRecords = filteredCount,
                aaData = listofForms
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetFormApproverList

        #region GetFormActiveList
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetFormActiveList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_FormNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_FormName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Purpose = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_VersionNumber = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentNumber = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Department = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_CreatedBy = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ApprovedBy = HttpContext.Current.Request.Params["sSearch_9"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ActionName = HttpContext.Current.Request.Params["sSearch_10"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchFormNumber = sSearch_FormNumber;
            objJQDataTableBO.sSearchFormName = sSearch_FormName;
            objJQDataTableBO.sSearchPurpose = sSearch_Purpose;
            objJQDataTableBO.sSearchVersionNumber = sSearch_VersionNumber;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentNumber;
            objJQDataTableBO.sSearchDepartment = sSearch_Department;
            objJQDataTableBO.sSearchCreatedBy = sSearch_CreatedBy;
            objJQDataTableBO.sSearchApprovedBy = sSearch_ApprovedBy;
            objJQDataTableBO.sSearchActionName = sSearch_ActionName;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType = 3;
            List<DocObjects> listofForms = new List<DocObjects>();
            int filteredCount = 0;
            DataSet ds= DMS_BalDC.DMS_GetFormList(objJQDataTableBO);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in ds.Tables[0].Rows)
                {
                    DocObjects objDocObjects = new DocObjects();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"].ToString());
                    objDocObjects.FormVersionID = Convert.ToInt32(rdr["FormVersionID"].ToString());
                    objDocObjects.FormNumber = (rdr["FormNumber"].ToString());
                    objDocObjects.FormName = (rdr["FormName"].ToString());
                    objDocObjects.Purpose = (rdr["FormDescription"].ToString());
                    objDocObjects.VersionNumber = (rdr["FormVersionNumber"].ToString());
                    objDocObjects.dept = (rdr["DepartmentName"].ToString());
                    objDocObjects.documentNUmber = (rdr["RefDocNumber"].ToString());
                    objDocObjects.CreatedBy = (rdr["CreatorName"].ToString());
                    objDocObjects.oldApprovedBY = (rdr["ApproverName"].ToString());
                    objDocObjects.action = Convert.ToInt32(rdr["ActionStatusID"].ToString());
                    objDocObjects.ActionName = rdr["ActionName"].ToString();
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofForms.Add(objDocObjects);
                }
            }
            var result = new
            {
                iTotalRecords = ds.Tables[1].Rows.Count > 0 ? Convert.ToInt32(ds.Tables[1].Rows[0][0]) : 0,
                iTotalDisplayRecords = filteredCount,
                aaData = listofForms
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetFormActiveList

        #region GetFormPrintReqList
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetFormPrintReqList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_RequestNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_FormName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_FormNumber = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_FormVersionNumber = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);

            var sSearch_RequestPurpose = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_NoofCopies = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ApprovedBy = HttpContext.Current.Request.Params["sSearch_9"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestBy = HttpContext.Current.Request.Params["sSearch_10"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestedDate = HttpContext.Current.Request.Params["sSearch_11"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchRequestNumber = sSearch_RequestNumber;
            objJQDataTableBO.sSearchFormName = sSearch_FormName;
            objJQDataTableBO.sSearchFormNumber = sSearch_FormNumber;
            objJQDataTableBO.sSearchVersionNumber = sSearch_FormVersionNumber;
            objJQDataTableBO.sSearchPurpose = sSearch_RequestPurpose;
            objJQDataTableBO.sSearchNoofCopies = sSearch_NoofCopies;
            objJQDataTableBO.sSearchApprovedBy = sSearch_ApprovedBy;
            objJQDataTableBO.sSearchRequestBy = sSearch_RequestBy;
            objJQDataTableBO.sSearchRequestedDate = sSearch_RequestedDate;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType = 1;
            if (Session["FormPrint"].ToString() == "n")
            {
                objJQDataTableBO.Verify = 1;
            }
            else
            {
                objJQDataTableBO.Verify = 0;
            }
            List<FormPrintRequest> listofFormPrintRequestDocuments = new List<FormPrintRequest>();
            int filteredCount = 0;
            DataSet ds = DMS_BalDC.DMS_GetFormPrintRequestList(objJQDataTableBO);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in ds.Tables[0].Rows)
                {
                    FormPrintRequest objFormObjects = new FormPrintRequest();
                    objFormObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"].ToString());
                    objFormObjects.FormReqID = Convert.ToInt32(rdr["FormRequestID"].ToString());
                    objFormObjects.RequestNumber = (rdr["RequestNo"].ToString());
                    objFormObjects.FormName = (rdr["FormName"].ToString());
                    objFormObjects.FormNumber = (rdr["FormNumber"].ToString());
                    objFormObjects.FormVersionNumber = (rdr["FormVersionNumber"].ToString());
                    objFormObjects.RequestPurpose = (rdr["RequestPurpose"].ToString());
                    objFormObjects.NoofCopies = Convert.ToInt32(rdr["NooFCopies"].ToString());
                    objFormObjects.ActionStatusID = Convert.ToInt32(rdr["ActionStatusID"].ToString());
                    objFormObjects.FormVersionID = Convert.ToInt32(rdr["FormVersionID"].ToString());
                    objFormObjects.CopyNumbers= rdr["CopyNumbers"].ToString();
                    objFormObjects.RequestBy= rdr["RequestBy"].ToString();
                    objFormObjects.ApprovedBy = rdr["ApprovedBy"].ToString();
                    objFormObjects.RequestedDate = rdr["RequestedDate"].ToString();
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofFormPrintRequestDocuments.Add(objFormObjects);
                }
            }
            var result = new
            {
                iTotalRecords = ds.Tables[1].Rows.Count > 0 ? Convert.ToInt32(ds.Tables[1].Rows[0][0]) : 0,
                iTotalDisplayRecords = filteredCount,
                aaData = listofFormPrintRequestDocuments
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetFormPrintReqList

        #region GetFormPrintReqReviewerList
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetFormPrintReqReviewerList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_RequestNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_FormName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_FormNumber = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_FormVersionNumber= HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestPurpose = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_NoofCopies = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ApprovedBy = HttpContext.Current.Request.Params["sSearch_9"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestBy = HttpContext.Current.Request.Params["sSearch_10"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestedDate = HttpContext.Current.Request.Params["sSearch_10"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchRequestNumber = sSearch_RequestNumber;
            objJQDataTableBO.sSearchFormName = sSearch_FormName;
            objJQDataTableBO.sSearchFormNumber = sSearch_FormNumber;
            objJQDataTableBO.sSearchVersionNumber = sSearch_FormVersionNumber;
            objJQDataTableBO.sSearchPurpose = sSearch_RequestPurpose;
            objJQDataTableBO.sSearchNoofCopies = sSearch_NoofCopies;
            objJQDataTableBO.sSearchApprovedBy = sSearch_ApprovedBy;
            objJQDataTableBO.sSearchRequestBy = sSearch_RequestBy;
            objJQDataTableBO.sSearchRequestedDate = sSearch_RequestedDate;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType = 2;
            List<FormPrintRequest> listofFormPrintRequestDocuments = new List<FormPrintRequest>();
            int filteredCount = 0;
            DataSet ds= DMS_BalDC.DMS_GetFormPrintRequestList(objJQDataTableBO);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in ds.Tables[0].Rows)
                {
                    FormPrintRequest objFormObjects = new FormPrintRequest();
                    objFormObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"].ToString());
                    objFormObjects.FormReqID = Convert.ToInt32(rdr["FormRequestID"].ToString());
                    objFormObjects.RequestNumber = (rdr["RequestNo"].ToString());
                    objFormObjects.FormName = (rdr["FormName"].ToString());
                    objFormObjects.FormNumber = (rdr["FormNumber"].ToString());
                    objFormObjects.FormVersionNumber = (rdr["FormVersionNumber"].ToString());
                    objFormObjects.RequestPurpose = (rdr["RequestPurpose"].ToString());
                    objFormObjects.NoofCopies = Convert.ToInt32(rdr["NooFCopies"].ToString());
                    objFormObjects.FormVersionID = Convert.ToInt32(rdr["FormVersionID"].ToString());
                    objFormObjects.CopyNumbers = rdr["CopyNumbers"].ToString();
                    objFormObjects.RequestBy = rdr["RequestBy"].ToString();
                    objFormObjects.ApprovedBy = rdr["ApprovedBy"].ToString();
                    objFormObjects.RequestedDate = rdr["RequestedDate"].ToString();
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofFormPrintRequestDocuments.Add(objFormObjects);
                }
            }
            var result = new
            {
                iTotalRecords = ds.Tables[1].Rows.Count > 0 ? Convert.ToInt32(ds.Tables[1].Rows[0][0]) : 0,
                iTotalDisplayRecords = filteredCount,
                aaData = listofFormPrintRequestDocuments
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetFormPrintReqReviewerList

        #region GetFormPrintReqControllerList
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetFormPrintReqControllerList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_RequestNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_FormName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_FormNumber = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_FormVersionNumber = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);

            var sSearch_RequestPurpose = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_NoofCopies = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ApprovedBy = HttpContext.Current.Request.Params["sSearch_9"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestBy = HttpContext.Current.Request.Params["sSearch_10"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestedDate = HttpContext.Current.Request.Params["sSearch_11"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchRequestNumber = sSearch_RequestNumber;
            objJQDataTableBO.sSearchFormName = sSearch_FormName;
            objJQDataTableBO.sSearchFormNumber = sSearch_FormNumber;
            objJQDataTableBO.sSearchVersionNumber = sSearch_FormVersionNumber;
            objJQDataTableBO.sSearchPurpose = sSearch_RequestPurpose;
            objJQDataTableBO.sSearchNoofCopies = sSearch_NoofCopies;
            objJQDataTableBO.sSearchApprovedBy = sSearch_ApprovedBy;
            objJQDataTableBO.sSearchRequestBy = sSearch_RequestBy;
            objJQDataTableBO.sSearchRequestedDate = sSearch_RequestedDate;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType = 3;          
            List<FormPrintRequest> listofFormPrintRequestDocuments = new List<FormPrintRequest>();
            int filteredCount = 0;
            DataSet ds= DMS_BalDC.DMS_GetFormPrintRequestList(objJQDataTableBO);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in ds.Tables[0].Rows)
                {
                    FormPrintRequest objFormObjects = new FormPrintRequest();
                    objFormObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"].ToString());
                    objFormObjects.FormReqID = Convert.ToInt32(rdr["FormRequestID"].ToString());
                    objFormObjects.RequestNumber = (rdr["RequestNo"].ToString());
                    objFormObjects.FormName = (rdr["FormName"].ToString());
                    objFormObjects.FormNumber=(rdr["FormNumber"].ToString());
                    objFormObjects.FormVersionNumber= (rdr["FormVersionNumber"].ToString());
                    objFormObjects.RequestPurpose = (rdr["RequestPurpose"].ToString());
                    objFormObjects.NoofCopies = Convert.ToInt32(rdr["NooFCopies"].ToString());
                    objFormObjects.ActionStatusID = Convert.ToInt32(rdr["ActionStatusID"].ToString());
                    objFormObjects.FormVersionID = Convert.ToInt32(rdr["FormVersionID"].ToString());
                    objFormObjects.CopyNumbers = rdr["CopyNumbers"].ToString();
                    objFormObjects.RequestBy = rdr["RequestBy"].ToString();
                    objFormObjects.ApprovedBy = rdr["ApprovedBy"].ToString();
                    objFormObjects.RequestedDate = rdr["RequestedDate"].ToString();
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofFormPrintRequestDocuments.Add(objFormObjects);
                }
            }
            var result = new
            {
                iTotalRecords = ds.Tables[1].Rows.Count > 0 ? Convert.ToInt32(ds.Tables[1].Rows[0][0]) : 0,
                iTotalDisplayRecords = filteredCount,
                aaData = listofFormPrintRequestDocuments
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetFormPrintReqControllerList

        #region GetFormPrintReqManageList
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetFormPrintReqManageList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_RequestNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_FormName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_FormNumber = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_FormVersionNumber = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);

            var sSearch_RequestPurpose = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_NoofCopies = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ApprovedBy = HttpContext.Current.Request.Params["sSearch_9"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestBy = HttpContext.Current.Request.Params["sSearch_10"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestedDate = HttpContext.Current.Request.Params["sSearch_11"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchRequestNumber = sSearch_RequestNumber;
            objJQDataTableBO.sSearchFormName = sSearch_FormName;
            objJQDataTableBO.sSearchFormNumber = sSearch_FormNumber;
            objJQDataTableBO.sSearchVersionNumber = sSearch_FormVersionNumber;
            objJQDataTableBO.sSearchPurpose = sSearch_RequestPurpose;
            objJQDataTableBO.sSearchNoofCopies = sSearch_NoofCopies;
            objJQDataTableBO.sSearchApprovedBy = sSearch_ApprovedBy;
            objJQDataTableBO.sSearchRequestBy = sSearch_RequestBy;
            objJQDataTableBO.sSearchRequestedDate = sSearch_RequestedDate;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType = 4;
            List<FormPrintRequest> listofFormPrintRequestDocuments = new List<FormPrintRequest>();
            int filteredCount = 0;
            DataSet ds = DMS_BalDC.DMS_GetFormPrintRequestList(objJQDataTableBO);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in ds.Tables[0].Rows)
                {
                    FormPrintRequest objFormObjects = new FormPrintRequest();
                    objFormObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"].ToString());
                    objFormObjects.FormReqID = Convert.ToInt32(rdr["FormRequestID"].ToString());
                    objFormObjects.RequestNumber = (rdr["RequestNo"].ToString());
                    objFormObjects.FormName = (rdr["FormName"].ToString());
                    objFormObjects.FormNumber = (rdr["FormNumber"].ToString());
                    objFormObjects.FormVersionNumber = (rdr["FormVersionNumber"].ToString());
                    objFormObjects.RequestPurpose = (rdr["RequestPurpose"].ToString());
                    objFormObjects.NoofCopies = Convert.ToInt32(rdr["NooFCopies"].ToString());
                    objFormObjects.ActionStatusID = Convert.ToInt32(rdr["ActionStatusID"].ToString());
                    objFormObjects.FormVersionID = Convert.ToInt32(rdr["FormVersionID"].ToString());
                    objFormObjects.CopyNumbers = rdr["CopyNumbers"].ToString();
                    objFormObjects.RequestBy = rdr["RequestBy"].ToString();
                    objFormObjects.ApprovedBy = rdr["ApprovedBy"].ToString();
                    objFormObjects.RequestedDate = rdr["RequestedDate"].ToString();
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofFormPrintRequestDocuments.Add(objFormObjects);
                }
            }
            var result = new
            {
                iTotalRecords = ds.Tables[1].Rows.Count > 0 ? Convert.ToInt32(ds.Tables[1].Rows[0][0]) : 0,
                iTotalDisplayRecords = filteredCount,
                aaData = listofFormPrintRequestDocuments
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetFormPrintReqManageList

        #region GetDocPrintReqMainList
        [WebMethod(EnableSession =true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetDocPrintReqMainList()
        {
            Session["DocPrintMainList"] = null;
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_RequestNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentNumber = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentVersionNumber = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);

            var sSearch_Department = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_PrintPurpose = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_PageNumbers = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ApprovedBy = HttpContext.Current.Request.Params["sSearch_9"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestBy = HttpContext.Current.Request.Params["sSearch_10"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestedDate = HttpContext.Current.Request.Params["sSearch_11"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ActionName = HttpContext.Current.Request.Params["sSearch_12"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 2;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchRequestNumber = sSearch_RequestNumber;
            objJQDataTableBO.sSearchDocumentName = sSearch_DocumentName;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentNumber;
            objJQDataTableBO.sSearchVersionNumber = sSearch_DocumentVersionNumber;
            objJQDataTableBO.sSearchDepartment = sSearch_Department;
            objJQDataTableBO.sSearchPageNumbers = sSearch_PageNumbers;
            objJQDataTableBO.sSearchPurpose = sSearch_PrintPurpose;
            objJQDataTableBO.sSearchApprovedBy = sSearch_ApprovedBy;
            objJQDataTableBO.sSearchRequestBy = sSearch_RequestBy;
            objJQDataTableBO.sSearchRequestedDate = sSearch_RequestedDate;
            objJQDataTableBO.sSearchActionName = sSearch_ActionName;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType = 0;
            List<DocPrintRequest> listofDocPrintRequestDocuments = new List<DocPrintRequest>();
            int filteredCount = 0;
            objJQDataTableBO.IsJQueyList = "Y";
            Session["DocPrintMainList"]= objJQDataTableBO;
            DataSet ds = DMS_BalDC.DMS_DocPrintReqList(objJQDataTableBO);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in ds.Tables[0].Rows)
                {
                    DocPrintRequest objDocObjects = new DocPrintRequest();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"].ToString());
                    objDocObjects.DocReqID = Convert.ToInt32(rdr["DocRequestID"].ToString());
                    objDocObjects.RequestNumber = (rdr["RequestNumber"].ToString());
                    objDocObjects.DocumentName = (rdr["DocumentName"].ToString());
                    objDocObjects.DocumentNumber = (rdr["DocumentNumber"].ToString());
                    objDocObjects.DocumentVersionNumber= (rdr["DocumentVersionNumber"].ToString());
                    objDocObjects.PrintPurpose = (rdr["PrintPurpose"].ToString());
                    objDocObjects.PageNumbers = (rdr["PageNumbers"].ToString());
                    objDocObjects.ActionStatusID = Convert.ToInt32(rdr["ActionStatusID"].ToString());
                    objDocObjects.ActionName = rdr["ActionName"].ToString();
                    objDocObjects.DeptName = (rdr["DepartmentName"].ToString());
                    objDocObjects.RequestBy = rdr["RequestBy"].ToString();
                    objDocObjects.ApprovedBy = rdr["ApprovedBy"].ToString();
                    objDocObjects.RequestedDate = rdr["RequestedDate"].ToString();
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofDocPrintRequestDocuments.Add(objDocObjects);
                }
            }
            var result = new
            {
                iTotalRecords = ds.Tables[1].Rows.Count > 0 ? Convert.ToInt32(ds.Tables[1].Rows[0][0]) : 0,
                iTotalDisplayRecords = filteredCount,
                aaData = listofDocPrintRequestDocuments
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetDocPrintReqMainList

        #region GetFormPrintReqMainList
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetFormPrintReqMainList()
        {
            Session["FormPrintMainList"] = null;
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_RequestNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_FormName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_FormNumber = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_FormVersionNumber = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);

            var sSearch_Department = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestPurpose = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_NoofCopies = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ApprovedBy = HttpContext.Current.Request.Params["sSearch_10"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestBy = HttpContext.Current.Request.Params["sSearch_11"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestedDate = HttpContext.Current.Request.Params["sSearch_12"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ActionName = HttpContext.Current.Request.Params["sSearch_13"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 2;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchRequestNumber = sSearch_RequestNumber;
            objJQDataTableBO.sSearchFormName= sSearch_FormName;
            objJQDataTableBO.sSearchFormNumber = sSearch_FormNumber;
            objJQDataTableBO.sSearchVersionNumber = sSearch_FormVersionNumber;
            objJQDataTableBO.sSearchDepartment = sSearch_Department;
            objJQDataTableBO.sSearchPurpose = sSearch_RequestPurpose;
            objJQDataTableBO.sSearchNoofCopies = sSearch_NoofCopies;
            objJQDataTableBO.sSearchApprovedBy = sSearch_ApprovedBy;
            objJQDataTableBO.sSearchRequestBy = sSearch_RequestBy;
            objJQDataTableBO.sSearchRequestedDate = sSearch_RequestedDate;
            objJQDataTableBO.sSearchActionName = sSearch_ActionName;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType = 0;
            objJQDataTableBO.IsJQueyList = "Y";
            
            if (Session["FormReturnVerify"].ToString() == "t")
            {
                objJQDataTableBO.Verify = 1;
            }
            else
            {
                objJQDataTableBO.Verify = 0;
            }
            List<FormPrintRequest> listofFormPrintRequestDocuments = new List<FormPrintRequest>();
            int filteredCount = 0;
            Session["FormPrintMainList"] = objJQDataTableBO;
            DataSet ds = DMS_BalDC.DMS_GetFormPrintRequestList(objJQDataTableBO);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in ds.Tables[0].Rows)
                {
                    FormPrintRequest objFormObjects = new FormPrintRequest();
                    objFormObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"].ToString());
                    objFormObjects.FormReqID = Convert.ToInt32(rdr["FormRequestID"].ToString());
                    objFormObjects.RequestNumber = (rdr["RequestNo"].ToString());
                    objFormObjects.FormName = (rdr["FormName"].ToString());
                    objFormObjects.FormNumber = (rdr["FormNumber"].ToString());
                    objFormObjects.FormVersionNumber=(rdr["FormVersionNumber"].ToString());
                    objFormObjects.RequestPurpose = (rdr["RequestPurpose"].ToString());
                    objFormObjects.NoofCopies = Convert.ToInt32(rdr["NooFCopies"].ToString());
                    objFormObjects.ActionStatusID = Convert.ToInt32(rdr["ActionStatusID"].ToString());
                    objFormObjects.ActionName = rdr["ActionName"].ToString();
                    objFormObjects.DeptName = rdr["DepartmentName"].ToString();
                    objFormObjects.CopyNumbers = rdr["CopyNumbers"].ToString();
                    objFormObjects.DocControllerID = rdr["DocControlerID"].ToString();
                    objFormObjects.RequestBy = rdr["RequestBy"].ToString();
                    objFormObjects.ApprovedBy = rdr["ApprovedBy"].ToString();
                    objFormObjects.RequestedDate = rdr["RequestedDate"].ToString();
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofFormPrintRequestDocuments.Add(objFormObjects);
                }
            }
            var result = new
            {
                iTotalRecords = ds.Tables[1].Rows.Count > 0 ? Convert.ToInt32(ds.Tables[1].Rows[0][0]) : 0,
                iTotalDisplayRecords = filteredCount,
                aaData = listofFormPrintRequestDocuments
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetFormPrintReqMainList

        #region GetFormMainList
        [WebMethod(EnableSession =true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetFormMainList()
        {
            Session["MainFormList"] = null;
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_FormNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_FormName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Purpose = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_VersionNumber = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Department = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentNumber = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_CreatedBy = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ApprovedBy = HttpContext.Current.Request.Params["sSearch_9"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ActionName = HttpContext.Current.Request.Params["sSearch_10"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 2;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchFormNumber = sSearch_FormNumber;
            objJQDataTableBO.sSearchFormName = sSearch_FormName;
            objJQDataTableBO.sSearchPurpose = sSearch_Purpose;
            objJQDataTableBO.sSearchVersionNumber = sSearch_VersionNumber;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentNumber;
            objJQDataTableBO.sSearchDepartment = sSearch_Department;
            objJQDataTableBO.sSearchCreatedBy = sSearch_CreatedBy;
            objJQDataTableBO.sSearchApprovedBy = sSearch_ApprovedBy;
            objJQDataTableBO.sSearchActionName = sSearch_ActionName;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType = 0;
            objJQDataTableBO.IsJQueyList = "Y";
            Session["MainFormList"]= objJQDataTableBO;
            List<DocObjects> listofForms = new List<DocObjects>();
            int filteredCount = 0;
            DataSet ds= DMS_BalDC.DMS_GetFormList(objJQDataTableBO);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in ds.Tables[0].Rows)
                {
                    DocObjects objDocObjects = new DocObjects();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"].ToString());
                    objDocObjects.FormVersionID = Convert.ToInt32(rdr["FormVersionID"].ToString());
                    objDocObjects.FormNumber = (rdr["FormNumber"].ToString());
                    objDocObjects.FormName = (rdr["FormName"].ToString());
                    objDocObjects.Purpose = (rdr["FormDescription"].ToString());
                    objDocObjects.VersionNumber = (rdr["FormVersionNumber"].ToString());
                    objDocObjects.dept = (rdr["DepartmentName"].ToString());
                    objDocObjects.documentNUmber = (rdr["RefDocNumber"].ToString());
                    objDocObjects.CreatedBy = (rdr["CreatorName"].ToString());
                    objDocObjects.action = Convert.ToInt32(rdr["ActionStatusID"].ToString());
                    objDocObjects.oldApprovedBY = (rdr["ApproverName"].ToString());
                    objDocObjects.ActionName = rdr["ActionName"].ToString();
                    objDocObjects.PendingFormRevisionCount = Convert.ToInt32(rdr["PendingFormRevisionCount"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofForms.Add(objDocObjects);
                }
            }
            var result = new
            {
                iTotalRecords = ds.Tables[1].Rows.Count > 0 ? Convert.ToInt32(ds.Tables[1].Rows[0][0]) : 0,
                iTotalDisplayRecords = filteredCount,
                aaData = listofForms
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetFormMainList

        #region GetFormPendingList
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetFormPendingList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            var ReqType = HttpContext.Current.Request.Params["ReqTypeID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType = 4;
            objJQDataTableBO.ReqTypeID = Convert.ToInt32(ReqType);
            List<DocObjects> listofForms = new List<DocObjects>();
            int filteredCount = 0;
            DataSet ds = DMS_BalDC.DMS_GetFormList(objJQDataTableBO);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in ds.Tables[0].Rows)
                {
                    DocObjects objDocObjects = new DocObjects();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"].ToString());
                    objDocObjects.FormVersionID = Convert.ToInt32(rdr["FormVersionID"].ToString());
                    objDocObjects.FormNumber = (rdr["FormNumber"].ToString());
                    objDocObjects.FormName = (rdr["FormName"].ToString());
                    objDocObjects.Purpose = (rdr["FormDescription"].ToString());
                    objDocObjects.VersionNumber = (rdr["FormVersionNumber"].ToString());
                    objDocObjects.dept = (rdr["DepartmentName"].ToString());
                    objDocObjects.documentNUmber = (rdr["RefDocNumber"].ToString());
                    objDocObjects.CreatedBy = (rdr["CreatorName"].ToString());
                    objDocObjects.oldApprovedBY = (rdr["ApproverName"].ToString());
                    objDocObjects.action = Convert.ToInt32(rdr["ActionStatusID"].ToString());
                    objDocObjects.ActionName = rdr["ActionName"].ToString();
                    objDocObjects.RequestTypeID = Convert.ToInt32(rdr["FormRequestTypeID"].ToString());
                    objDocObjects.FormRequestType = (rdr["FormRequestType"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofForms.Add(objDocObjects);
                }
            }
            var result = new
            {
                iTotalRecords = ds.Tables[1].Rows.Count > 0 ? Convert.ToInt32(ds.Tables[1].Rows[0][0]) : 0,
                iTotalDisplayRecords = filteredCount,
                aaData = listofForms
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetFormPendingList

        #region GetFormManageList
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetFormManageList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_FormNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_FormName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Purpose = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_VersionNumber = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Department = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentNumber = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_CreatedBy = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ApprovedBy = HttpContext.Current.Request.Params["sSearch_9"].ToString(CultureInfo.CurrentCulture);
            var sSearch_ActionName = HttpContext.Current.Request.Params["sSearch_10"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            var ReqType = HttpContext.Current.Request.Params["ReqTypeID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchFormNumber = sSearch_FormNumber;
            objJQDataTableBO.sSearchFormName = sSearch_FormName;
            objJQDataTableBO.sSearchPurpose = sSearch_Purpose;
            objJQDataTableBO.sSearchVersionNumber = sSearch_VersionNumber;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentNumber;
            objJQDataTableBO.sSearchDepartment = sSearch_Department;
            objJQDataTableBO.sSearchCreatedBy = sSearch_CreatedBy;
            objJQDataTableBO.sSearchApprovedBy = sSearch_ApprovedBy;
            objJQDataTableBO.sSearchActionName = sSearch_ActionName;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            objJQDataTableBO.RType = 5;
            objJQDataTableBO.ReqTypeID = Convert.ToInt32(ReqType);
            List<DocObjects> listofForms = new List<DocObjects>();
            int filteredCount = 0;
            DataSet ds = DMS_BalDC.DMS_GetFormList(objJQDataTableBO);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in ds.Tables[0].Rows)
                {
                    DocObjects objDocObjects = new DocObjects();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"].ToString());
                    objDocObjects.FormVersionID = Convert.ToInt32(rdr["FormVersionID"].ToString());
                    objDocObjects.FormNumber = (rdr["FormNumber"].ToString());
                    objDocObjects.FormName = (rdr["FormName"].ToString());
                    objDocObjects.Purpose = (rdr["FormDescription"].ToString());
                    objDocObjects.VersionNumber = (rdr["FormVersionNumber"].ToString());
                    objDocObjects.dept = (rdr["DepartmentName"].ToString());
                    objDocObjects.documentNUmber = (rdr["RefDocNumber"].ToString());
                    objDocObjects.CreatedBy = (rdr["CreatorName"].ToString());
                    objDocObjects.oldApprovedBY = (rdr["ApproverName"].ToString());
                    objDocObjects.action = Convert.ToInt32(rdr["ActionStatusID"].ToString());
                    objDocObjects.ActionName = rdr["ActionName"].ToString();
                    objDocObjects.RequestTypeID = Convert.ToInt32(rdr["FormRequestTypeID"].ToString());
                    objDocObjects.FormRequestType = (rdr["FormRequestType"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofForms.Add(objDocObjects);
                }
            }
            var result = new
            {
                iTotalRecords = ds.Tables[1].Rows.Count > 0 ? Convert.ToInt32(ds.Tables[1].Rows[0][0]) : 0,
                iTotalDisplayRecords = filteredCount,
                aaData = listofForms
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetFormManageList

        #region GetEveryoneComments
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public List<EveryoneComments> GetEveryoneComments(int DocVID)
        {
            DataTable dt = new DataTable();
            DocObjects doc = new DocObjects();       
            doc.Version = Convert.ToInt32(DocVID);
            doc.Mode = 0;
            dt = DMS_BalDC.DMS_GetTempRevertedReviewersDetails(doc);
            List<EveryoneComments> objEveryoneComments = new List<EveryoneComments>();
            foreach (DataRow drresult in dt.Rows)
            {
                objEveryoneComments.Add(new EveryoneComments {
                    EmpName = drresult["RevertedBy"].ToString(),
                    Role = drresult["Role"].ToString(),
                    Comments=drresult["Comments"].ToString(),
                });
            }
            return objEveryoneComments;
        }
        #endregion GetEveryoneComments

        #region GetRevertedComments
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public List<EveryoneComments> GetRevertedComments(string DocVID,string ProcessID)
        {
            DataTable dt = new DataTable();
            dt = DMS_BalDC.DMS_GetRejectCommentsByID(DocVID, ProcessID);
            List<EveryoneComments> objEveryoneComments = new List<EveryoneComments>();
            foreach (DataRow drresult in dt.Rows)
            {
                objEveryoneComments.Add(new EveryoneComments
                {
                    EmpName = drresult["RevertedBy"].ToString(),
                    Role = drresult["Role"].ToString(),
                    Comments = drresult["Comments"].ToString(),
                });
            }
            return objEveryoneComments;
        }
        #endregion GetRevertedComments

        #region GetFormComments
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public List<EveryoneComments> GetFormComments(string FormVersionID)
        {
            DataTable dt = new DataTable();
            dt = DMS_BalDM.DMS_GetLatestFormComment(FormVersionID);
            List<EveryoneComments> objEveryoneComments = new List<EveryoneComments>();
            foreach (DataRow drresult in dt.Rows)
            {
                objEveryoneComments.Add(new EveryoneComments
                {
                    EmpName = drresult["RevertedBy"].ToString(),
                    Role = drresult["Role"].ToString(),
                    Comments = drresult["Comments"].ToString(),
                });
            }
            return objEveryoneComments;
        }
        #endregion GetFormComments

        #region PrintMainDocumentList
        DataTable fillCompany()
        {
            try
            {
                CompanyBO objCompanyBO;
                UMS_BAL objUMS_BAL;
                DataTable dtcompany = new DataTable();
                objCompanyBO = new CompanyBO();
                objCompanyBO.Mode = 2;
                objCompanyBO.CompanyID = 0;
                objCompanyBO.CompanyCode = "";
                objCompanyBO.CompanyName = "";
                objCompanyBO.CompanyDescription = "";
                objCompanyBO.CompanyPhoneNo1 = "";
                objCompanyBO.CompanyPhoneNo2 = "";
                objCompanyBO.CompanyFaxNo1 = "";
                objCompanyBO.CompanyFaxNo2 = ""; //txtFaxNo2.Value;
                objCompanyBO.CompanyEmailID = "";
                objCompanyBO.CompanyWebUrl = "";
                objCompanyBO.CompanyAddress = "";
                objCompanyBO.CompanyLocation = "";// txtLocat.Value;
                objCompanyBO.CompanyCity = "";
                objCompanyBO.CompanyState = "";
                objCompanyBO.CompanyCountry = "";
                objCompanyBO.CompanyPinCode = "";
                objCompanyBO.CompanyStartDate = "";
                objCompanyBO.CompanyLogo = new byte[0];
                objUMS_BAL = new UMS_BAL();
                dtcompany = objUMS_BAL.CompanyQuery(objCompanyBO);
                return dtcompany;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private System.Drawing.Image BindApplicantImage(byte[] applicantImage)
        {
            using (var ms = new MemoryStream(applicantImage))
            {
                return System.Drawing.Image.FromStream(ms);
            }
        }

        [WebMethod(EnableSession = true)]
       // [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public ReportTitlePath PrintMainDocList(string IsJQueryList,string ReportName)//,string EmpName
        {
            string EmpName = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString();
            MainDocumentListReport objmainDocumentListReport = new MainDocumentListReport();
            int filteredCount = 0; string PrintFilePath=""; string ReprotTitleModel = ""; string fileName;
            DataTable dt = new DataTable();

            DataTable _dtCompany = fillCompany();
            if (!string.IsNullOrEmpty(_dtCompany.Rows[0]["CompanyLogo"].ToString()))
            {
                ((XRPictureBox)(objmainDocumentListReport.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
            }
            JQDataTableBO objJQDataTableBO = (JQDataTableBO)Session["MainDocumentList"];
            objJQDataTableBO.IsJQueyList = IsJQueryList;

            ReportTitlePath objResult = new ReportTitlePath();
            dt = DMS_BalDC.DMS_AdminListOFDocuments(objJQDataTableBO);
            if (dt.Rows.Count > 0)
            {
                //foreach (DataRow rdr in dt.Rows)
                //{
                //    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                //}
                objmainDocumentListReport.DataSource = dt;
                objmainDocumentListReport.DataMember = dt.TableName;
                objResult.ReportTitle = Convert.ToString(dt.Rows[0]["ReportTitle"]);
                ((XRLabel)(objmainDocumentListReport.FindControl("lblPrintedBy", false))).Text = EmpName;
                string strFilePath = DynamicFolder.CreateDynamicFolder(12);
                
                fileName = ReportName+"_"+ DateTime.Now.ToString("ddMMMMyyyyHHmm") + ".pdf";
                //fileName = "AdminMainList.pdf";
                strFilePath= strFilePath + "\\" + fileName;
                //for Print Path
                objResult.ReportPath = System.Web.VirtualPathUtility.ToAbsolute("/DMSPDFFolder/web/" + DateTime.Today.ToString("yyyyMMdd") + "/" + fileName); 
                objmainDocumentListReport.ExportToPdf(strFilePath);
            }
            else
            {
                PrintFilePath = "NoData";
            }
            return objResult;
            //var result = objResult;
            //JavaScriptSerializer js = new JavaScriptSerializer();
            //return js.Serialize(result);
            //else
            //{
            //    foreach (DataRow rdr in dt.Rows)
            //    {
            //        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
            //    }
            //    objmainDocumentListReport.DataSource = dt;
            //    objmainDocumentListReport.DataMember = dt.TableName;
            //    ((XRLabel)(objmainDocumentListReport.FindControl("lblPrintedBy", false))).Text = EmpName;
            //    string strFilePath = DynamicFolder.CreateDynamicFolder(12);
            //    fileName = "AdminMainList.pdf";
            //    strFilePath = strFilePath + "\\" + fileName;
            //    //for Print Path
            //    PrintFilePath = System.Web.VirtualPathUtility.ToAbsolute("/DMSPDFFolder/web/" + DateTime.Today.ToString("yyyyMMdd") + "/" + fileName);
            //    objmainDocumentListReport.ExportToPdf(strFilePath);
            //}
            //string path = Server.MapPath(PrintFilePath);
            //byte[] bytes = System.IO.File.ReadAllBytes(path);
            //return Convert.ToBase64String(bytes);

            //var result = new
            //{
            //    iTotalRecords = filteredCount,
            //    iTotalDisplayRecords = filteredCount,
            //    aaData = PrintFilePath
            //};
            //JavaScriptSerializer js = new JavaScriptSerializer();
            //return js.Serialize(result);

        }
        #endregion

        #region MainFormList Report
        [WebMethod(EnableSession = true)]
        public string ReportMainFormList(string IsJqueryDataList)
        {
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", "ShowPleaseWaitGifDisableImage();", true);
            string EmpName = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString();
            int filteredCount = 0; string PrintFilePath; string fileName;
            MainFormListReport objmainFormListReport = new MainFormListReport();
            DataTable _dtCompany = fillCompany();
            if (!string.IsNullOrEmpty(_dtCompany.Rows[0]["CompanyLogo"].ToString()))
            {
                ((XRPictureBox)(objmainFormListReport.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
            }
            JQDataTableBO objJQDataTableBO = (JQDataTableBO)Session["MainFormList"];
            objJQDataTableBO.IsJQueyList = IsJqueryDataList;
            DataSet ds = DMS_BalDC.DMS_GetFormList(objJQDataTableBO);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in ds.Tables[0].Rows)
                {
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                }
                objmainFormListReport.DataSource = ds.Tables[0];
                objmainFormListReport.DataMember = ds.Tables[0].TableName;
                ((XRLabel)(objmainFormListReport.FindControl("lblPrintedBy", false))).Text = EmpName;
                string strFilePath = DynamicFolder.CreateDynamicFolder(12);
                fileName = "MainFormList_"+DateTime.Now.ToString("ddMMMMyyyyHHmm") + ".pdf";
                 strFilePath = strFilePath + "\\" + fileName;
                //for Print Path
                PrintFilePath = System.Web.VirtualPathUtility.ToAbsolute("/DMSPDFFolder/web/" + DateTime.Today.ToString("yyyyMMdd") + "/" + fileName);
                objmainFormListReport.ExportToPdf(strFilePath);
            }
            else
            {
                PrintFilePath = "NoData";
            }
            var result = PrintFilePath;
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
            //else
            //{
            //    foreach (DataRow rdr in ds.Tables[0].Rows)
            //    {
            //        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
            //    }
            //    objmainFormListReport.DataSource = ds.Tables[0];
            //    objmainFormListReport.DataMember = ds.Tables[0].TableName;
            //    ((XRLabel)(objmainFormListReport.FindControl("lblPrintedBy", false))).Text = EmpName;
            //    string strFilePath = DynamicFolder.CreateDynamicFolder(12);
            //    fileName = "MainFormList.pdf";
            //    strFilePath = strFilePath + "\\" + fileName;
            //    //for Print Path
            //    PrintFilePath = System.Web.VirtualPathUtility.ToAbsolute("/DMSPDFFolder/web/" + DateTime.Today.ToString("yyyyMMdd") + "/" + fileName);
            //    objmainFormListReport.ExportToPdf(strFilePath);
            //}
            //string path = Server.MapPath(PrintFilePath);
            //byte[] bytes = System.IO.File.ReadAllBytes(path);
            //return Convert.ToBase64String(bytes);
        }
        #endregion

        #region FromPrintMain List Report
        [WebMethod(EnableSession =true)]
        public string FromPrintReqMainListReport(string IsJqueryDataList)
        {
            string EmpName = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString();

            int filteredCount = 0; string PrintFilePath; string fileName;
            FormPrintReqMainListReport objformPrintReqMainListReport = new FormPrintReqMainListReport();
            DataTable _dtCompany = fillCompany();
            if (!string.IsNullOrEmpty(_dtCompany.Rows[0]["CompanyLogo"].ToString()))
            {
                ((XRPictureBox)(objformPrintReqMainListReport.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
            }
            JQDataTableBO objJQDataTableBO = (JQDataTableBO)Session["FormPrintMainList"];
            objJQDataTableBO.IsJQueyList = IsJqueryDataList;
            DataSet ds = DMS_BalDC.DMS_GetFormPrintRequestList(objJQDataTableBO);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in ds.Tables[0].Rows)
                {
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                }
                objformPrintReqMainListReport.DataSource = ds.Tables[0];
                objformPrintReqMainListReport.DataMember = ds.Tables[0].TableName;
                ((XRLabel)(objformPrintReqMainListReport.FindControl("lblPrintedBy", false))).Text = EmpName;
                string strFilePath = DynamicFolder.CreateDynamicFolder(12);
                fileName = "FormPrintReqMainList_" + DateTime.Now.ToString("ddMMMMyyyyHHmm") + ".pdf";
                strFilePath = strFilePath + "\\" + fileName;
                //for Print Path
                PrintFilePath = System.Web.VirtualPathUtility.ToAbsolute("/DMSPDFFolder/web/" + DateTime.Today.ToString("yyyyMMdd") + "/" + fileName);
                objformPrintReqMainListReport.ExportToPdf(strFilePath);
            }
            else
            {
                PrintFilePath = "NoData";
            }
            var result = PrintFilePath;
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
            //else
            //{
            //    foreach (DataRow rdr in ds.Tables[0].Rows)
            //    {
            //        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
            //    }
            //    objformPrintReqMainListReport.DataSource = ds.Tables[0];
            //    objformPrintReqMainListReport.DataMember = ds.Tables[0].TableName;
            //    ((XRLabel)(objformPrintReqMainListReport.FindControl("lblPrintedBy", false))).Text = EmpName;
            //    string strFilePath = DynamicFolder.CreateDynamicFolder(12);
            //    //fileName = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
            //    fileName ="FormPrintReqMainList.pdf";
            //    strFilePath = strFilePath + "\\" + fileName;
            //    //for Print Path
            //    PrintFilePath = System.Web.VirtualPathUtility.ToAbsolute("/DMSPDFFolder/web/" + DateTime.Today.ToString("yyyyMMdd") + "/" + fileName);
            //    objformPrintReqMainListReport.ExportToPdf(strFilePath);
            //}

            //string path = Server.MapPath(PrintFilePath);
            //byte[] bytes = System.IO.File.ReadAllBytes(path);
            //return Convert.ToBase64String(bytes);
        }
        #endregion

        #region DocPrintMainList Report
        [WebMethod(EnableSession = true)]
        public string DocPrintReqMainListReport(string IsJqueryDataList)
        {
            string EmpName = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString();
          
            int filteredCount = 0; string PrintFilePath=""; string fileName;
            DocPrintReqMainListReport objdocPrintReqMainListReport = new DocPrintReqMainListReport();
            DataTable _dtCompany = fillCompany();
            if (!string.IsNullOrEmpty(_dtCompany.Rows[0]["CompanyLogo"].ToString()))
            {
                ((XRPictureBox)(objdocPrintReqMainListReport.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
            }
            JQDataTableBO objJQDataTableBO = (JQDataTableBO)Session["DocPrintMainList"];
            objJQDataTableBO.IsJQueyList = "N";
            DataSet ds = DMS_BalDC.DMS_DocPrintReqList(objJQDataTableBO);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in ds.Tables[0].Rows)
                {
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                }
                objdocPrintReqMainListReport.DataSource = ds.Tables[0];
                objdocPrintReqMainListReport.DataMember = ds.Tables[0].TableName;
                ((XRLabel)(objdocPrintReqMainListReport.FindControl("lblPrintedBy", false))).Text = EmpName;
                string strFilePath = DynamicFolder.CreateDynamicFolder(12);
                 fileName = "DocReqPrintMainList"+DateTime.Now.ToString("ddMMMMyyyyHHmm") + ".pdf";
                //fileName = "DocReqPrintMainList.pdf";
                strFilePath = strFilePath + "\\" + fileName;
                //for Print Path
                PrintFilePath = System.Web.VirtualPathUtility.ToAbsolute("/DMSPDFFolder/web/" + DateTime.Today.ToString("yyyyMMdd") + "/" + fileName);
                objdocPrintReqMainListReport.ExportToPdf(strFilePath);
            }
            else
            {
                PrintFilePath = "NoData";
            }
            var result = PrintFilePath;
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
            //else
            //{
            //    foreach (DataRow rdr in ds.Tables[0].Rows)
            //    {
            //        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
            //    }
            //    objdocPrintReqMainListReport.DataSource = ds.Tables[0];
            //    objdocPrintReqMainListReport.DataMember = ds.Tables[0].TableName;

            //    ((XRLabel)(objdocPrintReqMainListReport.FindControl("lblPrintedBy", false))).Text = EmpName;


            //    string strFilePath = DynamicFolder.CreateDynamicFolder(12);
            //    // fileName = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
            //    fileName = "DocReqPrintMainList.pdf";
            //    strFilePath = strFilePath + "\\" + fileName;
            //    //for Print Path
            //    PrintFilePath = System.Web.VirtualPathUtility.ToAbsolute("/DMSPDFFolder/web/" + DateTime.Today.ToString("yyyyMMdd") + "/" + fileName);
            //    objdocPrintReqMainListReport.ExportToPdf(strFilePath);

            //}
            //string path = Server.MapPath(PrintFilePath);
            //byte[] bytes = System.IO.File.ReadAllBytes(path);
            //return Convert.ToBase64String(bytes);
        }
        #endregion

        #region mainDocmentListDetails

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetMainDocumentDetailsList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var DocumetID = int.Parse(HttpContext.Current.Request.Params["DocumetID"]);


            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;

            int filteredCount = 0;
            DataTable dt = new DataTable();

            List<AdminDocListBO> listofAdminDocuments = new List<AdminDocListBO>();
            dt = DMS_BalDC.DMS_GetMainDocumentDetailsList(objJQDataTableBO, DocumetID);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    AdminDocListBO objDocObjects = new AdminDocListBO();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    objDocObjects.VersionID = Convert.ToInt32(rdr["VersionID"].ToString());
                    objDocObjects.vDocumentID = Convert.ToInt32(rdr["vDocumentID"].ToString());
                    objDocObjects.Department = (rdr["Department"].ToString());
                    objDocObjects.DocumentName = (rdr["DocumentName"].ToString());
                    objDocObjects.VersionNumber = (rdr["VersionNumber"].ToString());
                    objDocObjects.DocumentNumber = (rdr["DocumentNumber"].ToString());
                    objDocObjects.DocumentType = (rdr["DocumentType"].ToString());
                    objDocObjects.RequestType = (rdr["RequestType"].ToString());
                    objDocObjects.statusMsg = (rdr["statusMsg"].ToString());
                    objDocObjects.EffectiveDate = (rdr["EffectiveDate"].ToString());
                    objDocObjects.ExpirationDate = (rdr["ExpirationDate"].ToString());
                    objDocObjects.expStatus = Convert.ToInt32(rdr["expStatus"].ToString());
                    objDocObjects.DocLocationCount = (rdr["DocLocationCount"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofAdminDocuments.Add(objDocObjects);
                }
            }
            var result = new
            {
                iTotalRecords = filteredCount,
                iTotalDisplayRecords = filteredCount,
                aaData = listofAdminDocuments
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);

        }
        #endregion

        #region GetWithdrawDocList
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetWithdrawDocList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var sSearch_DocumentNumber = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentName = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Department = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
            var sSearch_DocumentType = HttpContext.Current.Request.Params["sSearch_5"].ToString(CultureInfo.CurrentCulture);
            var sSearch_InitiatedBy = HttpContext.Current.Request.Params["sSearch_6"].ToString(CultureInfo.CurrentCulture);
            var sSearch_Status = HttpContext.Current.Request.Params["sSearch_7"].ToString(CultureInfo.CurrentCulture);
            var sSearch_RequestType = HttpContext.Current.Request.Params["sSearch_8"].ToString(CultureInfo.CurrentCulture);

            var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
            JQDataTableBO objJQDataTableBO = new JQDataTableBO();
            objJQDataTableBO.iMode = 1;
            objJQDataTableBO.pkid = 0;
            objJQDataTableBO.iDisplayLength = iDisplayLength;
            objJQDataTableBO.iDisplayStart = iDisplayStart;
            objJQDataTableBO.iSortCol = iSortCol_0;
            objJQDataTableBO.sSortDir = sSortDir_0;
            objJQDataTableBO.sSearch = sSearch;
            objJQDataTableBO.sSearchDocumentNumber = sSearch_DocumentNumber;
            objJQDataTableBO.sSearchDocumentName = sSearch_DocumentName;
            objJQDataTableBO.sSearchDepartment = sSearch_Department;
            objJQDataTableBO.sSearchDocumentType = sSearch_DocumentType;
            objJQDataTableBO.sSearchInitiatedBy = sSearch_InitiatedBy;
            objJQDataTableBO.sSearchStatus = sSearch_Status;
            objJQDataTableBO.sSearchRequestType = sSearch_RequestType;
            objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            List<GetQADocBO> listofQADocuments = new List<GetQADocBO>();
            int filteredCount = 0;
            DataTable dt = DMS_BalDC.DMS_GetWithdrawDocList(objJQDataTableBO);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    GetQADocBO objDocObjects = new GetQADocBO();
                    objDocObjects.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    objDocObjects.VersionID = Convert.ToInt32(rdr["VersionID"].ToString());
                    objDocObjects.Department = (rdr["Department"].ToString());
                    objDocObjects.DocumentName = (rdr["DocumentName"].ToString());
                    objDocObjects.DocumentNumber = (rdr["DocumentNumber"].ToString());
                    objDocObjects.DocumentType = (rdr["DocumentType"].ToString());
                    objDocObjects.InitiatedBy = (rdr["IntiatedBy"].ToString());
                    objDocObjects.RecordStatus = (rdr["RecordStatus"].ToString());
                    objDocObjects.RequestType = (rdr["RequestType"].ToString());
                    objDocObjects.EffectiveDate = (rdr["EffectiveDate"].ToString());
                    objDocObjects.ExpirationDate = (rdr["ExpirationDate"].ToString());
                    objDocObjects.Status = (rdr["Status"].ToString());
                    objDocObjects.vDocumentID = Convert.ToInt32(rdr["vDocumentID"].ToString());
                    objDocObjects.TrainingCount = Convert.ToBoolean(rdr["TrainingCount"].ToString());
                    objDocObjects.IsTraining = Convert.ToBoolean(rdr["IsTraining"].ToString());
                    objDocObjects.PendingDocRequestCount = Convert.ToInt32(rdr["PendingDocRequestCount"].ToString());
                    objDocObjects.RequestTypeID = Convert.ToInt32(rdr["RequestTypeID"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listofQADocuments.Add(objDocObjects);
                }
            }
            //JQDataTableBO _objJQDataTableBO = new JQDataTableBO();
            //_objJQDataTableBO.iMode = 2;
            //_objJQDataTableBO.pkid = 0;
            //_objJQDataTableBO.iDisplayLength = iDisplayLength;
            //_objJQDataTableBO.iDisplayStart = iDisplayStart;
            //_objJQDataTableBO.iSortCol = iSortCol_0;
            //_objJQDataTableBO.sSortDir = sSortDir_0;
            //_objJQDataTableBO.sSearch = sSearch;
            //_objJQDataTableBO.EmpID = Convert.ToInt32(EmpID);
            //_objJQDataTableBO.RType = Convert.ToInt32(ReqType);
            //DataTable dtcount = DMS_BalDC.DMS_GetNewDocQAList(_objJQDataTableBO);
            var result = new
            {
                iTotalRecords = filteredCount,
                iTotalDisplayRecords = filteredCount,
                aaData = listofQADocuments
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion GetWithdrawDocList

        #region GetPrinterNames
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public List<Printer_Names> GetPrinterNamesList()
        {

            List<Printer_Names> ListOFPrinterNames = new List<Printer_Names>();
            DataSet ds = DMS_BalDC.BAL_GetPrinterNaems();
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in ds.Tables[0].Rows)
                {
                    Printer_Names objPrinterBo = new Printer_Names();
                    objPrinterBo.printerID = Convert.ToInt32(rdr["PrinterID"].ToString());
                    objPrinterBo.PrinterAreanName = (rdr["Printer_AreaName"].ToString());
                    objPrinterBo.PrinterName = (rdr["Printer_Name"].ToString());
                    ListOFPrinterNames.Add(objPrinterBo);
                }
            }
            return ListOFPrinterNames;
        }
        #endregion

        #region validatePageNumbers
        [WebMethod]
       // [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string validatePageNumbers(string strPageNos,int pagecount)
        {
            try
            {
                var t1 = strPageNos.Substring(strPageNos.Length - 1);
                var t2 = strPageNos.Substring(0,1);
                if (t1.Contains(',') || t1.Contains('-') || t2.Contains(',') || t2.Contains('-'))
                {
                    return "1";//restrict first special character and last special character 
                }
                else
                {
                    strPageNos = Regex.Replace(strPageNos.Trim(), @",+", ",");
                    strPageNos = Regex.Replace(strPageNos.Trim(), @"-+", "-");
                    strPageNos = "," + strPageNos;
                    string[] arrPnos1 = strPageNos.Split(',');
                    ArrayList PgNo = new ArrayList();
                   // if (arrPnos1[1] != "" && arrPnos1[1] != "-")
                   // {
                        foreach (string strPg in arrPnos1)
                        {
                            if (strPg.Trim().Length > 0)
                            {
                                if (strPg.Trim().Contains("-"))
                                {
                                    string[] arrPg2 = strPg.Split('-');
                                    if (arrPg2[0] != "" && arrPg2[1] != "")
                                    {
                                        for (int i = Convert.ToInt32(arrPg2[0]); i <= Convert.ToInt32(arrPg2[1]); i++)
                                        {
                                            PgNo.Add(i);
                                        }
                                    }
                                    else
                                    {

                                        return "3";
                                    }
                                }
                                else
                                {
                                    PgNo.Add(Convert.ToInt32(strPg));
                                }
                            }
                        }
                    //}
                    //else
                    //{
                    //    return false;
                    //}
                    string isValidPgNO = "0";//No restriction
                    foreach (int page in PgNo)
                    {
                        if (page > pagecount || page == 0)
                        {
                            isValidPgNO = "2";//restict 0 and page numbers grater than page count.
                            break;
                        }
                    }
                    return isValidPgNO;
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        #endregion validatePageNumbers
    }
}
