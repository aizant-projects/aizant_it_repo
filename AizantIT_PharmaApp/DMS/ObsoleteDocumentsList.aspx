﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="ObsoleteDocumentsList.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.MainDocumentDetailsList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        table tbody tr th {
            background-color: #07889a;
            color: #fff;
            text-align: center;
        }

        table tbody tr {
            text-align: center;
            height: 30px;
        }

        .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
            background-color: #07889a !important;
        }

        iframe {
            overflow-y: hidden;
        }
        /*inner table Icon */
        td.details-control {
            background: url(../Images/table_plusicon.png) no-repeat center center;
            cursor: pointer;
            width: 15px;
        }

        tr.shown td.details-control {
            background: url(../Images/table_minusicon.png) no-repeat center center;
        }
        #innerDocList_length, #innerDocList_info,#innerDocList_paginate {
            text-align:left;
            margin:3px 0px 10px 0px;
            display:none;
        }

        #innerDocList_filter {
            margin:3px 0px 10px 0px;
            display:none
        }
        .inner_doc_list thead th {
    padding: 10px;
    border-right: 1px solid #c3c3c3;
    border-bottom: 1px solid #c3c3c3;
    background: #bcdee4 !important;
    color: #44707d !important;
    font-family: seguisb;
    font-size: 10pt;
    
}
        .inner_doc_list tbody tr:nth-of-type(odd) {
    background-color: #ffffff !important;
}
         .inner_doc_list tbody tr:nth-of-type(even) {
    background-color: #ffffff !important;
}
    </style>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="padding-none">
        <asp:Button ID="btnAdd" runat="server" class="float-right  btn-signup_popup" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
    </div>
    <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 padding-none float-left">
        <div class=" col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 dms_outer_border float-left padding-none">
            <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server" visible="false">
                <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
            </div>
            <div id="divMainContainer" runat="server" visible="true">
                <div class=" col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 padding-none float-right ">
                    <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12  padding-none float-left">
                        <div class="grid_header col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 float-left">
                            Obsolete Documents 
                        </div>
                        <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 grid_panel_full border_top_none bottom float-left">
                            <div class="col-12 top float-left  padding-none">
                                <table id="dtDocMainList" class="display datatable_cust" cellspacing="0" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>VersionID</th>
                                            <th></th>
                                            <th>vDocumentID</th>
                                            <th>Document Number</th>
                                            <th>Document Name</th>
                                            <th>Version</th>
                                            <th>Document Type</th>
                                            <th>Department</th>
                                            <th>Request Type</th>
                                            <th>Effective Date</th>
                                            <th>Review Date</th>
                                            <th>Status</th>
                                            <th>View Doc</th>
                                            <th>History</th>
                                            <th>Privacy</th>
                                            <th>Document Status</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class=" col-lg-12  float-left bottom">
                                <img src="<%=ResolveUrl("~/Images/GridActionImages/revert_tms.png")%>" /><b class="grid_icon_legend">Review Date Exceeded</b>
                                <img src="<%=ResolveUrl("~/Images/GridActionImages/summary_latest.png")%>" /><b class="grid_icon_legend">History</b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hdnEmpID" runat="server" />
    <asp:HiddenField ID="hdfRole" runat="server" />
    <asp:HiddenField ID="hdfVID" runat="server" />
    <asp:HiddenField ID="hdfDocNum" runat="server" />
    <asp:HiddenField ID="hdfStatus" runat="server" />
    <asp:HiddenField ID="hdfViewType" runat="server" />
    <asp:HiddenField ID="hdfViewEmpID" runat="server" />
    <asp:HiddenField ID="hdfViewRoleID" runat="server" />
    <asp:HiddenField ID="hdfViewDivID" runat="server" />



    <asp:UpdatePanel ID="UpGridButtons" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnFileView" runat="server" Text="Submit" OnClick="btnFileView_Click" Style="display: none" OnClientClick="showImg();" />
            <asp:Button ID="btnFileView1" runat="server" Text="Submit" OnClick="btnFileView1_Click" Style="display: none" OnClientClick="showImg();" />
            <asp:Button ID="btnCommentsHistoryView" runat="server" Text="Submit" Style="display: none" OnClick="btnCommentsHistoryView_Click" OnClientClick="showImg();" />
        </ContentTemplate>
    </asp:UpdatePanel>


    <!--------Comment History View-------------->
    <div id="myCommentHistory" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 85%">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel ID="upcomment" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-header">

                            <span id="span4" runat="server"></span>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body" style="max-height: 600px; overflow-y: auto">
                            <div class=" history_grid" style="overflow-y: auto;">
                                <asp:GridView ID="gv_CommentHistory" runat="server" CssClass="table table-hover" ClientIDMode="Static" EmptyDataText="No Records Found" AutoGenerateColumns="False" PagerSettings-Mode="NumericFirstLast" BackColor="White" BorderColor="#d2d2d2" BorderStyle="None" BorderWidth="1px" OnRowDataBound="gv_CommentHistory_RowDataBound" OnRowCommand="gv_CommentHistory_RowCommand" AllowPaging="true" PageSize="10" OnPageIndexChanging="gv_CommentHistory_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="User" ItemStyle-Width="12%" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_GivenBy" runat="server" Text='<%# Eval("GivenBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Role" runat="server" Text='<%# Eval("CommentedRole") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Date" runat="server" Text='<%# Eval("CommentedDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Time Spent">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Time" runat="server" Text='<%# Eval("Time") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="22%" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Action" runat="server" Text='<%# Eval("Action") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="26%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_shortComments" runat="server" Text='<%# Eval("Comments") %>'></asp:Label>
                                                <asp:TextBox ID="txt_Comments" runat="server" ReadOnly="true" Rows="4" TextMode="MultiLine" Text='<%# Eval("Comments") %>' Width="100%"></asp:TextBox>
                                                <asp:LinkButton ID="lbcommentsmore" runat="server" CommandArgument='<%# Eval("Comments")%>' CommandName="SM">Show More</asp:LinkButton>
                                                <asp:LinkButton ID="lbcommentsless" runat="server" CommandArgument='<%# Eval("Comments") %>' CommandName="SL">Show Less</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Assigned To">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_AssignedTo" runat="server" Text='<%# Eval("AssignedTo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="gridpager" HorizontalAlign="Right" />
                                </asp:GridView>
                            </div>
                        </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!--------End Comment History View-------------->

    <!-------- File VIEW-------------->
    <div id="myModal_lock" class="modal department fade" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 99%;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">

                    <asp:UpdatePanel ID="UpHeading" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <span id="span_FileTitle" runat="server">Document</span>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" style="max-height: 680px;">
                    <div id="divPDF_Viewer">
                        <noscript>
                            Please Enable JavaScript.
                        </noscript>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-------- End File VIEW-------------->


    <script>
        <%--function getadmincolumn() {
            var RoleID = document.getElementById("<%=hdfRole.ClientID%>").value;
            if (RoleID == "2") {
                otblDocMainList.column(14).visible(true);
            }
            else {
                otblDocMainList.column(14).visible(false);
            }
        }--%>
    </script>
    <!--main List-->
    <script>
        $('#dtDocMainList thead tr').clone(true).appendTo('#dtDocMainList thead');
        $('#dtDocMainList thead tr:eq(1) th').each(function (i) {
            if (i < 13) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder=" Search " />');

                $('input', this).on('keyup change', function () {
                    if (otblDocMainList.column(i).search() !== this.value) {
                        otblDocMainList
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
                if (i == 1 || i == 2) {
                    $(this).html('');
                }
            }
            else {
                $(this).html('');
            }
        });
        $('#dtDocMainList').wrap('<div class="dataTables_scroll" />');
        var otblDocMainList = $('#dtDocMainList').DataTable({
            columns: [
                { 'data': 'RowNumber' },//0
                { 'data': 'VersionID' },//1
                {
                    "className": 'details-control',
                    "defaultContent": ''
                },//2
                { 'data': 'vDocumentID' },//2
                { 'data': 'DocumentNumber' },//3
                { 'data': 'DocumentName' },//4
                { 'data': 'VersionNumber' },//5
                { 'data': 'DocumentType' },//6
                { 'data': 'Department' },//7
                { 'data': 'RequestType' },//8
                { 'data': 'EffectiveDate' },//9
                { 'data': 'ExpirationDate' },//10
                { 'data': 'statusMsg' },//11
                { 'data': 'expStatus' },//12
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="summary_latest" title="History"  data-toggle="modal" data-target="#myCommentHistory"  onclick=ViewCommentHistory(' + o.VersionID + ')></a>'; }
                },//13
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="privacy" title="privacy"  data-toggle="modal" data-target="#myDocumentPrivacyEdit"  onclick=EditDocumentPrivacy(' + o.VersionID + ',' + o.vDocumentID + ')></a>'; }
                },//14
                { 'data': 'DocLocationCount' }//15
            ],
            "createdRow": function (row, data, dataIndex) {
                if (data.VersionCount <= 1) {
                    $(row).find('td').removeClass('details-control');
                }
            },
            "scrollX": true,
            "responsive": true,
            "paging": true,
            "search": {
                "regex": true
            },
            "aoColumnDefs": [{ "targets": [0, 1, 3, 9, 15, 16], "visible": false, "searchable": false }, { "bSortable": false, "aTargets": [0, 2, 13, 14] }, { "className": "dt-body-left", "targets": [4, 5, 7, 8, 9, 12] },
            {
                targets: [13], render: function (a, b, data, d) {
                    if (data.expStatus > 0) {
                        return '<a class="view_Training" title="View Doc"  data-toggle="modal" data-target="#myModal_lock" onclick=ViewDocument1(' + data.VersionID + ')></a>';
                    }
                    else if (data.DocLocationCount != "0" && data.expStatus == 0) {
                        return '<a class="view"  data-toggle="modal" data-target="#myModal_lock"   title="View Doc" onclick=ViewDocument(' + data.VersionID + ')></a>';
                    }
                    else {
                        return "N/A";
                    }

                }
            },
            ],
            "orderCellsTop": true,
            "order": [[1, "desc"]],
            'bAutoWidth': true,

            sServerMethod: 'Post',
            "sAjaxSource": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetObsoleteDocList" )%>',
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#dtDocMainList").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        UserSessionCheck();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
    </script>
    <!--innser Table -->
    <script>
        var _RowHide = null;
        var _trRow = null;
        // Add event listener for opening and closing details
        $('#dtDocMainList tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = otblDocMainList.row(tr);
            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');

            }
            else {
                //hide Open Column
                if (_RowHide != null) {
                    _RowHide.child.hide();
                _trRow.removeClass('shown');
                }
                _RowHide = row;
                _trRow = tr;
                var result = row.data();
                // Open this row
                row.child(format(result.vDocumentID)).show();
                tr.addClass('shown');
                innerDocDataTable(result.vDocumentID);
            }
        });
       
        function format() {
            return '<table  id="innerDocList"  class="inner_doc_list display datatable_cust" cellspacing="0" style="width: 100%;">' +
                '<thead>' +
                '<tr id="tblTRHide">' +
                '<th>S.No</th>' +
                '<th>VersionID</th>' +
                '<th>vDocumentID</th>' +
                '<th>Document Number</th>' +
                '<th>Document Name</th>' +
                '<th>Version</th>' +
                '<th>Document Type</th>' +
                '<th>Department</th>' +
                '<th>Request Type</th>' +
                '<th>Effective Date</th>' +
                '<th>Review Date</th>' +
                '<th>Status</th>' +
                '<th>View Doc</th>' +
                '<th>History</th>' +
                '<th>Privacy</th>' +
                '<th>Document Status</th>' +
                '</tr>' +
                '</thead>' +
                '</table>';
        }

 //$('#innerDocList thead tr').clone(true).appendTo('#innerDocList thead');
 //       $('#innerDocList thead tr:eq(1) th').each(function (i) {
 //           if (i < 11) {
 //               var title = $(this).text();
 //               $(this).html('<input type="text" placeholder=" Search " />');

 //               $('input', this).on('keyup change', function () {
 //                   if (oinnerTable.column(i).search() !== this.value) {
 //                       oinnerTable
 //                           .column(i)
 //                           .search(this.value)
 //                           .draw();
 //                   }
 //               });
 //           }
 //           else {
 //               $(this).html('');
 //           }
 //       });

        function innerDocDataTable(vDocumentID) {
            var oinnerTable = $('#innerDocList').DataTable({
                columns: [
                    { 'data': 'RowNumber' },//0
                    { 'data': 'VersionID' },//1
                    { 'data': 'vDocumentID' },//2
                    { 'data': 'DocumentNumber' },//3
                    { 'data': 'DocumentName' },//4
                    { 'data': 'VersionNumber' },//5
                    { 'data': 'DocumentType' },//6
                    { 'data': 'Department' },//7
                    { 'data': 'RequestType' },//8
                    { 'data': 'EffectiveDate' },//9
                    { 'data': 'ExpirationDate' },//10
                    { 'data': 'statusMsg' },//11
                    { 'data': 'expStatus' },//12
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) { return '<a  class="summary_latest" title="History"  data-toggle="modal" data-target="#myCommentHistory"  onclick=ViewCommentHistory(' + o.VersionID + ')></a>'; }
                    },//13
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) { return '<a  class="privacy" title="privacy"  data-toggle="modal" data-target="#myDocumentPrivacyEdit"  onclick=EditDocumentPrivacy(' + o.VersionID + ',' + o.vDocumentID + ')></a>'; }
                    },//14
                    { 'data': 'DocLocationCount' }//15
                ],
                "responsive": true,
                "paging": true,
                "search": {
                    "regex": true
                },
                "aoColumnDefs": [{ "targets": [0, 1, 2, 8, 14, 15], "visible": false, "searchable": false }, { "bSortable": false, "aTargets": [0, 12, 13] }, { "className": "dt-body-left", "targets": [3, 4, 6, 7, 8, 11] },
                {
                    targets: [12], render: function (a, b, data, d) {
                        if (data.expStatus > 0) {
                            return '<a class="view_Training" title="View Doc"  data-toggle="modal" data-target="#myModal_lock" onclick=ViewDocument1(' + data.VersionID + ')></a>';
                        }
                        else if (data.DocLocationCount != "0" && data.expStatus == 0) {
                            return '<a class="view"  data-toggle="modal" data-target="#myModal_lock"   title="View Doc" onclick=ViewDocument(' + data.VersionID + ')></a>';
                        }
                        else {
                            return "N/A";
                        }

                    }
                },
                ],
                "orderCellsTop": true,
                "destroy": true,
                "order": [[1, "desc"]],
                'bAutoWidth': true,
                sServerMethod: 'Post',
                "sAjaxSource": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetMainDocumentDetailsList" )%>',
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "DocumetID", "value": vDocumentID });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#dtDocList").show();
                            $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                             //$(" .dataTables_filter").css({ "display": "none" });
                            UserSessionCheck();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                }
            });
        }
    </script>

    <script>
        
        var RoleID = document.getElementById("<%=hdfRole.ClientID%>").value;
        if (RoleID == 2) {
            $("#divMainDocListReport").show();
        }
        function ViewDocument(V_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = V_ID;
            var btnFV = document.getElementById("<%=btnFileView.ClientID %>");
            btnFV.click();
        }
        function ViewDocument1(V_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = V_ID;
            var btnFV1 = document.getElementById("<%=btnFileView1.ClientID %>");
            btnFV1.click();
        }
        function ViewCommentHistory(PK_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = PK_ID;
            var btnCV = document.getElementById("<%=btnCommentsHistoryView.ClientID %>");
            btnCV.click();
        }
    </script>

    <script src='<%= Page.ResolveUrl("~/AppScripts/PDF_Viewer.min.js")%>'></script>
    <script src='<%= Page.ResolveUrl("~/AppScripts/MainDocPDF_Viewer.js")%>'></script>
    <script>
        function showImg() {
            ShowPleaseWait('show');
        }
        function hideImg() {
            ShowPleaseWait('hide');
        }
        function LoadPDFDocument() {
            PdfViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>', "0", $('#<%=hdfVID.ClientID%>').val(), "0", "11", "#divPDF_Viewer", '');
            }
            function LoadfinalPDFDocument() {
                PdfViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>', "0", $('#<%=hdfVID.ClientID%>').val(), "0", "0", "#divPDF_Viewer", '');
        }
    </script>
    <script>
            $(document).ready(function () {
                $('#paragraph2').addClass('col-12');
                $('#paragraph2').removeClass('col-10');
                $('#paragraph1').hide();
                $('.dataTable').DataTable().columns.adjust();
                $('.closebtn1').show();
                $('.closebtn').hide();
            });
    </script>
</asp:Content>
