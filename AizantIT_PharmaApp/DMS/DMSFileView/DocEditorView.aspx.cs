﻿using AizantIT_DMSBAL;
using AizantIT_PharmaApp.Common;
using DevExpress.Web.Office;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AizantIT_PharmaApp.DMS.DMSFileView
{
    public partial class DocEditorView : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString.Count > 0)
                    {
                        string vid = Request.QueryString[0];
                        string rid = Request.QueryString[1];
                        EditDocument(vid, rid);
                    }
                }
            }
            catch(Exception ex)
            {

            }
        }
        private void EditDocument(string vid,string rid)
        {
            hdfPKID.Value = vid.ToString();
            DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
            if (DMS_DT.Rows[0]["FileType"].ToString() == "pdf")
            {
                if (Session["word"] != null && Session["CreateFileUploadExtension"] != null)
                {
                    byte[] docBytes = (byte[])Session["CreateFileUpload"];
                    string strFileUpload = DynamicFolder.CreateDynamicFolder(1);
                    string File1Path = DateTime.Now.ToString("yyyyMMddHHmmss") + Session["CreateFileUploadExtension"].ToString();
                    string strTargPath = (strFileUpload + "\\" + File1Path);
                    File.WriteAllBytes(strTargPath, docBytes);
                    if (Session["CreateFileUploadExtension"].ToString() == ".doc")
                    {
                        RichEditDocumentServer server = new RichEditDocumentServer();
                        server.LoadDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Doc);
                        Document document = server.Document;
                        Section firstSection = document.Sections[0];
                        //SubDocument myHeader = firstSection.BeginUpdateHeader(HeaderFooterType.Primary);
                        //DocumentRange[] deptrange = myHeader.FindAll("docdept1", SearchOptions.CaseSensitive, myHeader.Range);
                        //myHeader.InsertText(deptrange[0].Start, DMS_DT.Rows[0]["Department"].ToString());
                        //myHeader.Delete(deptrange[0]);
                        ////foreach (DocumentRange documentRange in deptrange)
                        ////{
                        ////    myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["Department"].ToString());
                        ////    myHeader.Delete(documentRange);
                        ////}
                        //DocumentRange[] typerange = myHeader.FindAll("doctype1", SearchOptions.CaseSensitive, myHeader.Range);
                        //myHeader.InsertText(typerange[0].Start, DMS_DT.Rows[0]["DocumentType"].ToString());
                        //myHeader.Delete(typerange[0]);
                        ////foreach (DocumentRange documentRange in typerange)
                        ////{
                        ////    myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentType"].ToString());
                        ////    myHeader.Delete(documentRange);
                        ////}
                        //DocumentRange[] docnamerange = myHeader.FindAll("docname1", SearchOptions.CaseSensitive, myHeader.Range);
                        //myHeader.InsertText(docnamerange[0].Start, DMS_DT.Rows[0]["DocumentName"].ToString());
                        //myHeader.Delete(docnamerange[0]);
                        ////foreach (DocumentRange documentRange in docnamerange)
                        ////{
                        ////    myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentName"].ToString());
                        ////    myHeader.Delete(documentRange);
                        ////}
                        //DocumentRange[] docnumrange = myHeader.FindAll("docnum1", SearchOptions.CaseSensitive, myHeader.Range);
                        //myHeader.InsertText(docnumrange[0].Start, DMS_DT.Rows[0]["DocumentNumber"].ToString() + "-" + DMS_DT.Rows[0]["VersionNumber"].ToString());
                        //myHeader.Delete(docnumrange[0]);
                        ////foreach (DocumentRange documentRange in docnumrange)
                        ////{
                        ////    myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentNumber"].ToString() + "-0" + DMS_DT.Rows[0]["VersionNumber"].ToString());
                        ////    myHeader.Delete(documentRange);
                        ////}
                        //firstSection.EndUpdateHeader(myHeader);
                        document.SaveDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Doc);
                        server.Dispose();
                        //Marshal.ReleaseComObject(server);

                        DocumentManager.CloseDocument(Guid.NewGuid().ToString());
                        ASPxRichEdit2.Open(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Doc);
                        ASPxRichEdit2.DocumentId = Guid.NewGuid().ToString();
                    }
                    if (Session["CreateFileUploadExtension"].ToString() == ".docx")
                    {
                        RichEditDocumentServer server = new RichEditDocumentServer();
                        server.LoadDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                        Document document = server.Document;
                        Section firstSection = document.Sections[0];
                        //SubDocument myHeader = firstSection.BeginUpdateHeader(HeaderFooterType.Primary);
                        //DocumentRange[] deptrange = myHeader.FindAll("docdept1", SearchOptions.CaseSensitive, myHeader.Range);
                        //myHeader.InsertText(deptrange[0].Start, DMS_DT.Rows[0]["Department"].ToString());
                        //myHeader.Delete(deptrange[0]);
                        ////foreach (DocumentRange documentRange in deptrange)
                        ////{
                        ////    myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["Department"].ToString());
                        ////    myHeader.Delete(documentRange);
                        ////}
                        //DocumentRange[] typerange = myHeader.FindAll("doctype1", SearchOptions.CaseSensitive, myHeader.Range);
                        //myHeader.InsertText(typerange[0].Start, DMS_DT.Rows[0]["DocumentType"].ToString());
                        //myHeader.Delete(typerange[0]);
                        ////foreach (DocumentRange documentRange in typerange)
                        ////{
                        ////    myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentType"].ToString());
                        ////    myHeader.Delete(documentRange);
                        ////}
                        //DocumentRange[] docnamerange = myHeader.FindAll("docname1", SearchOptions.CaseSensitive, myHeader.Range);
                        //myHeader.InsertText(docnamerange[0].Start, DMS_DT.Rows[0]["DocumentName"].ToString());
                        //myHeader.Delete(docnamerange[0]);
                        ////foreach (DocumentRange documentRange in docnamerange)
                        ////{
                        ////    myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentName"].ToString());
                        ////    myHeader.Delete(documentRange);
                        ////}
                        //DocumentRange[] docnumrange = myHeader.FindAll("docnum1", SearchOptions.CaseSensitive, myHeader.Range);
                        //myHeader.InsertText(docnumrange[0].Start, DMS_DT.Rows[0]["DocumentNumber"].ToString() + "-" + DMS_DT.Rows[0]["VersionNumber"].ToString());
                        //myHeader.Delete(docnumrange[0]);
                        ////foreach (DocumentRange documentRange in docnumrange)
                        ////{
                        ////    myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentNumber"].ToString() + "-0" + DMS_DT.Rows[0]["VersionNumber"].ToString());
                        ////    myHeader.Delete(documentRange);
                        ////}
                        //firstSection.EndUpdateHeader(myHeader);
                        document.SaveDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                        server.Dispose();
                        //Marshal.ReleaseComObject(server);

                        DocumentManager.CloseDocument(Guid.NewGuid().ToString());
                        ASPxRichEdit2.Open(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                        ASPxRichEdit2.DocumentId = Guid.NewGuid().ToString();
                    }
                    //if (File.Exists(strTargPath))
                    //{
                    //    // Note that no lock is put on the
                    //    // file and the possibility exists
                    //    // that another process could do
                    //    // something with it between
                    //    // the calls to Exists and Delete.
                    //    File.Delete(strTargPath);
                    //}
                }
                else
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Please Upload Original Word Document To Proceed Further!", "info");
                }

            }
            else
            {
                if (DMS_DT.Rows[0]["ActiveRequestTypeID"].ToString() == "4")
                {
                    if (DMS_DT.Rows[0]["Status"].ToString() != "0")
                    {
                        //byte[] docBytes = (byte[])Session["CreateFileUpload"];
                        //string strFileUpload = DynamicFolder.CreateDynamicFolder(1);
                        //string File1Path = DateTime.Now.ToString("yyyyMMddHHmmss") + Session["CreateFileUploadExtension"].ToString();
                        //string strTargPath = (strFileUpload + "\\" + File1Path);
                        //File.WriteAllBytes(strTargPath, docBytes);
                        //if (Session["CreateFileUploadExtension"].ToString() == ".doc")
                        //{
                        //    RichEditDocumentServer server = new RichEditDocumentServer();
                        //    server.LoadDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Doc);
                        //    Document document = server.Document;
                        //    Section firstSection = document.Sections[0];
                        //    SubDocument myHeader = firstSection.BeginUpdateHeader(HeaderFooterType.Primary);
                        //    DocumentRange[] deptrange = myHeader.FindAll("docdept1", SearchOptions.CaseSensitive, myHeader.Range);
                        //    myHeader.InsertText(deptrange[0].Start, DMS_DT.Rows[0]["Department"].ToString());
                        //    myHeader.Delete(deptrange[0]);
                        //    //foreach (DocumentRange documentRange in deptrange)
                        //    //{
                        //    //    myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["Department"].ToString());
                        //    //    myHeader.Delete(documentRange);
                        //    //}
                        //    DocumentRange[] typerange = myHeader.FindAll("doctype1", SearchOptions.CaseSensitive, myHeader.Range);
                        //    myHeader.InsertText(typerange[0].Start, DMS_DT.Rows[0]["DocumentType"].ToString());
                        //    myHeader.Delete(typerange[0]);
                        //    //foreach (DocumentRange documentRange in typerange)
                        //    //{
                        //    //    myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentType"].ToString());
                        //    //    myHeader.Delete(documentRange);
                        //    //}
                        //    DocumentRange[] docnamerange = myHeader.FindAll("docname1", SearchOptions.CaseSensitive, myHeader.Range);
                        //    myHeader.InsertText(docnamerange[0].Start, DMS_DT.Rows[0]["DocumentName"].ToString());
                        //    myHeader.Delete(docnamerange[0]);
                        //    //foreach (DocumentRange documentRange in docnamerange)
                        //    //{
                        //    //    myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentName"].ToString());
                        //    //    myHeader.Delete(documentRange);
                        //    //}
                        //    DocumentRange[] docnumrange = myHeader.FindAll("docnum1", SearchOptions.CaseSensitive, myHeader.Range);
                        //    myHeader.InsertText(docnumrange[0].Start, DMS_DT.Rows[0]["DocumentNumber"].ToString() + "-" + DMS_DT.Rows[0]["VersionNumber"].ToString());
                        //    myHeader.Delete(docnumrange[0]);
                        //    //foreach (DocumentRange documentRange in docnumrange)
                        //    //{
                        //    //    myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentNumber"].ToString() + "-0" + DMS_DT.Rows[0]["VersionNumber"].ToString());
                        //    //    myHeader.Delete(documentRange);
                        //    //}
                        //    firstSection.EndUpdateHeader(myHeader);
                        //    document.SaveDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Doc);
                        //    server.Dispose();
                        //    //Marshal.ReleaseComObject(server);

                        //    DocumentManager.CloseDocument(Guid.NewGuid().ToString());
                        //    ASPxRichEdit2.Open(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Doc);
                        //    ASPxRichEdit2.DocumentId = Guid.NewGuid().ToString();
                        //}
                        //if (Session["CreateFileUploadExtension"].ToString() == ".docx")
                        //{
                        //    RichEditDocumentServer server = new RichEditDocumentServer();
                        //    server.LoadDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                        //    Document document = server.Document;
                        //    Section firstSection = document.Sections[0];
                        //    SubDocument myHeader = firstSection.BeginUpdateHeader(HeaderFooterType.Primary);
                        //    DocumentRange[] deptrange = myHeader.FindAll("docdept1", SearchOptions.CaseSensitive, myHeader.Range);
                        //    myHeader.InsertText(deptrange[0].Start, DMS_DT.Rows[0]["Department"].ToString());
                        //    myHeader.Delete(deptrange[0]);
                        //    //foreach (DocumentRange documentRange in deptrange)
                        //    //{
                        //    //    myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["Department"].ToString());
                        //    //    myHeader.Delete(documentRange);
                        //    //}
                        //    DocumentRange[] typerange = myHeader.FindAll("doctype1", SearchOptions.CaseSensitive, myHeader.Range);
                        //    myHeader.InsertText(typerange[0].Start, DMS_DT.Rows[0]["DocumentType"].ToString());
                        //    myHeader.Delete(typerange[0]);
                        //    //foreach (DocumentRange documentRange in typerange)
                        //    //{
                        //    //    myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentType"].ToString());
                        //    //    myHeader.Delete(documentRange);
                        //    //}
                        //    DocumentRange[] docnamerange = myHeader.FindAll("docname1", SearchOptions.CaseSensitive, myHeader.Range);
                        //    myHeader.InsertText(docnamerange[0].Start, DMS_DT.Rows[0]["DocumentName"].ToString());
                        //    myHeader.Delete(docnamerange[0]);
                        //    //foreach (DocumentRange documentRange in docnamerange)
                        //    //{
                        //    //    myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentName"].ToString());
                        //    //    myHeader.Delete(documentRange);
                        //    //}
                        //    DocumentRange[] docnumrange = myHeader.FindAll("docnum1", SearchOptions.CaseSensitive, myHeader.Range);
                        //    myHeader.InsertText(docnumrange[0].Start, DMS_DT.Rows[0]["DocumentNumber"].ToString() + "-" + DMS_DT.Rows[0]["VersionNumber"].ToString());
                        //    myHeader.Delete(docnumrange[0]);
                        //    //foreach (DocumentRange documentRange in docnumrange)
                        //    //{
                        //    //    myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentNumber"].ToString() + "-0" + DMS_DT.Rows[0]["VersionNumber"].ToString());
                        //    //    myHeader.Delete(documentRange);
                        //    //}
                        //    firstSection.EndUpdateHeader(myHeader);
                        //    document.SaveDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                        //    server.Dispose();
                        //    //Marshal.ReleaseComObject(server);

                        //    DocumentManager.CloseDocument(Guid.NewGuid().ToString());
                        //    ASPxRichEdit2.Open(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                        //    ASPxRichEdit2.DocumentId = Guid.NewGuid().ToString();
                        //}
                        ASPxRichEdit2.Open(
                            Guid.NewGuid().ToString(),
                            DevExpress.XtraRichEdit.DocumentFormat.Rtf,
                            () =>
                            {
                                byte[] docBytes = (byte[])DMS_DT.Rows[0]["Content"];
                                return new MemoryStream(docBytes);
                            }
                            );

                    }
                    else
                    {
                        if (DMS_DT.Rows[0]["Content"] != DBNull.Value)
                        {
                            ASPxRichEdit2.Open(
                           Guid.NewGuid().ToString(),
                           DevExpress.XtraRichEdit.DocumentFormat.Rtf,
                           () =>
                           {
                               byte[] docBytes = (byte[])DMS_DT.Rows[0]["Content"];
                               return new MemoryStream(docBytes);
                           }
                           );
                        }
                        else
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Please Upload Original Word Document To Proceed Further!", "info");
                        }
                    }
                }
                else
                {
                    if (rid == "New Document")
                    {
                        if (DMS_DT.Rows[0]["Content"] != DBNull.Value)
                        {
                            ASPxRichEdit2.Open(
                            Guid.NewGuid().ToString(),
                            DevExpress.XtraRichEdit.DocumentFormat.Rtf,
                            () =>
                            {
                                byte[] docBytes = (byte[])DMS_DT.Rows[0]["Content"];
                                return new MemoryStream(docBytes);
                            }
                            );
                        }
                    }
                    else if (rid == "Revision Of Existing Document")
                    {
                        if (DMS_DT.Rows[0]["Content"] != DBNull.Value)
                        {
                            if (Request.QueryString["tab"] == "1")
                            {
                                byte[] docBytes = (byte[])DMS_DT.Rows[0]["Content"];
                                string strFileUpload = DynamicFolder.CreateDynamicFolder(1);
                                string File1Path = DateTime.Now.ToString("yyyyMMddHHmmss") + ".rtf";
                                string strTargPath = (strFileUpload + "\\" + File1Path);
                                File.WriteAllBytes(strTargPath, docBytes);
                                RichEditDocumentServer server = new RichEditDocumentServer();
                                server.LoadDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
                                Document document = server.Document;
                                Section firstSection = document.Sections[0];
                                SubDocument myHeader = firstSection.BeginUpdateHeader(HeaderFooterType.Primary);
                                if (Convert.ToInt32(DMS_DT.Rows[0]["VersionNumber"]) == 1)
                                {
                                    DocumentRange[] docnumrange = myHeader.FindAll(DMS_DT.Rows[0]["DocumentNumber"].ToString() + "-00", SearchOptions.CaseSensitive, myHeader.Range);
                                    if (docnumrange.Length > 0)
                                    {
                                        myHeader.InsertText(docnumrange[0].Start, DMS_DT.Rows[0]["DocumentNumber"].ToString() + "-" );
                                        myHeader.Delete(docnumrange[0]);
                                    }
                                }
                                else if (Convert.ToInt32(DMS_DT.Rows[0]["VersionNumber"]) <= 10)
                                {
                                    DocumentRange[] docnumrange = myHeader.FindAll(DMS_DT.Rows[0]["DocumentNumber"].ToString() + "-0" + (Convert.ToInt32(DMS_DT.Rows[0]["VersionNumber"]) - 1).ToString(), SearchOptions.CaseSensitive, myHeader.Range);
                                    if (docnumrange.Length > 0)
                                    {
                                        myHeader.InsertText(docnumrange[0].Start, DMS_DT.Rows[0]["DocumentNumber"].ToString() + "-" );
                                        myHeader.Delete(docnumrange[0]);
                                    }
                                }
                                else
                                {
                                    DocumentRange[] docnumrange = myHeader.FindAll(DMS_DT.Rows[0]["DocumentNumber"].ToString() + "-" + (Convert.ToInt32(DMS_DT.Rows[0]["VersionNumber"]) - 1).ToString(), SearchOptions.CaseSensitive, myHeader.Range);
                                    if (docnumrange.Length > 0)
                                    {
                                        myHeader.InsertText(docnumrange[0].Start, DMS_DT.Rows[0]["DocumentNumber"].ToString() + "-" );
                                        myHeader.Delete(docnumrange[0]);
                                    }
                                }
                                firstSection.EndUpdateHeader(myHeader);
                                document.SaveDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
                                server.Dispose();
                                //Marshal.ReleaseComObject(server);

                                DocumentManager.CloseDocument(Guid.NewGuid().ToString());
                                ASPxRichEdit2.Open(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
                                ASPxRichEdit2.DocumentId = Guid.NewGuid().ToString();
                            }
                            else
                            {
                                ASPxRichEdit2.Open(
                                                    Guid.NewGuid().ToString(),
                                                    DevExpress.XtraRichEdit.DocumentFormat.Rtf,
                                                    () =>
                                                    {
                                                        byte[] docBytes = (byte[])DMS_DT.Rows[0]["Content"];
                                                        return new MemoryStream(docBytes);
                                                    }
                                                  );
                            }
                        }
                    }
                }
            }
        }

        protected void Btn_Upload_Click(object sender, EventArgs e)
        {
            Btn_Upload.Enabled = false;
            try
            {
                OpenFromTempFolder();
                www.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                //Aizant_log.Error("DD_M14:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M14:" + strline + "  " + "Document Upload failed", "error");
            }
            finally
            {
                Btn_Upload.Enabled = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop3", "reload();", true);
            }
        }
        public void OpenFromTempFolder()
        {
            if (file_word.HasFile)
            {
                Session["CreateFileUpload"] = null;
                Session["word"] = null;
                byte[] imgbyte = null;
                if (file_word.PostedFile.FileName.Length > 5)
                {
                    string ext = System.IO.Path.GetExtension(file_word.FileName);
                    Stream fs = file_word.PostedFile.InputStream;
                    BinaryReader br = new BinaryReader(fs);
                    imgbyte = br.ReadBytes((Int32)fs.Length);
                    fs.Close();
                    //string strFileName = file_word.FileName;
                    //string strTargPath = Server.MapPath(@"~/Files") + "\\" + strFileName;
                    string strFileUpload = DynamicFolder.CreateDynamicFolder(1);
                    string File1Path = DateTime.Now.ToString("yyyyMMddHHmmss") + ext;
                    string strTargPath = (strFileUpload + "\\" + File1Path);
                    File.WriteAllBytes(strTargPath, imgbyte);
                    string mime = MimeType.GetMimeType(imgbyte, file_word.FileName);
                    string[] filemimes1 = new string[] { "application/pdf", "application/x-msdownload", "application/unknown", "application/octet-stream" };
                    string[] filemimes = new string[] { "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/x-zip-compressed" };
                    if (filemimes1.Contains(mime))
                    {
                        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop1", "ShowDocDetails1();", true);
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' Please upload .doc or .docx files only ! <br/> The uploaded file is not valid or corrupted','warning');", true);
                        return;
                    }
                    if (!filemimes.Contains(mime))
                    {
                        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop1", "ShowDocDetails1();", true);
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' Please upload .doc or .docx files only ! <br/> The uploaded file is not valid or corrupted','warning');", true);
                        return;
                    }
                    if (ext == ".doc" || ext == ".docx")
                    {
                        Session["CreateFileUploadExtension"] = ext;
                        DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                        if (ext == ".doc")
                        {                           
                            ASPxRichEdit2.Open(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Doc);
                        }
                        else if (ext == ".docx")
                        {                          
                            ASPxRichEdit2.Open(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                        }                   
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/> Please upload  .doc or .docx files only','warning');", true);
                        return;
                    }
                }
                Session["CreateFileUpload"] = imgbyte;
                Session["word"] = file_word.FileName;
            }
        }
    }
}