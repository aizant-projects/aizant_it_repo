﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HtmlDiff;
using DocumentFormat.OpenXml.Packaging;
using OpenXmlPowerTools;
using System.Xml.Linq;
using System.Drawing.Imaging;
using AizantIT_PharmaApp.Common;
using System.Runtime.InteropServices;

namespace AizantIT_PharmaApp.DMS.DMSFileView
{
    public partial class DocCompareView : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocObjects docObjects = new DocObjects();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString.Count > 0)
                    {
                        string vid = Request.QueryString[0];
                        hdfDocVID.Value = vid;
                        string pid = Request.QueryString[1];
                        if (pid == "1")
                        {
                            string ToEmpID = Request.QueryString[2];
                            string ToRoleID = Request.QueryString[3];
                            string FromEmpID = Request.QueryString[4];
                            hdfFromEmpID.Value = FromEmpID;
                            string FromRoleID = Request.QueryString[5];
                            hdfFromRoleID.Value = FromRoleID;
                            ToTrackChanges(vid, ToEmpID, ToRoleID, FromEmpID, FromRoleID);
                            if (ToRoleID == "11")
                            {
                                Button1.Visible = true;
                            }
                        }
                        else if (pid == "2" || pid == "3")
                        {
                            ToTrackChanges(vid);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DCV_M1:" + strline + "  " + strMsg);
                string strError = "DCV_M1:" + strline + "  " + "Page loading failed";
                lblError.Visible = true;
                lblError.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        private void ToTrackChanges(string vid)
        {
            try
            {
                docObjects.Version = Convert.ToInt32(vid);
                DataTable dt = DMS_Bal.DMS_GetTempDocumentPath(docObjects);
                string html = GetHTMLContent(dt.Rows[0]["FilePath"].ToString());
                ltFile1.InnerHtml = GetHTMLContent(dt.Rows[1]["FilePath"].ToString());
                ltFile2.InnerHtml = GetDiffHtml(ltFile1.InnerHtml, html);
                div2.InnerText = dt.Rows[1]["RoleName"].ToString() + "-" + dt.Rows[1]["EmpName"].ToString();
                div3.InnerText = dt.Rows[0]["RoleName"].ToString() + "-" + dt.Rows[0]["EmpName"].ToString();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DCV_M2:" + strline + "  " + strMsg);
                string strError = "DCV_M2:" + strline + "  " + "Track changes failed";
                lblError.Visible = true;
                lblError.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        private void ToTrackChanges(string vid, string ToEmpID, string ToRoleID, string FromEmpID, string FromRoleID)
        {
            try
            {
                docObjects.Version = Convert.ToInt32(vid);
                docObjects.RoleID = Convert.ToInt32(ToRoleID);
                docObjects.EmpID = Convert.ToInt32(ToEmpID);
                DataTable dt = DMS_Bal.DMS_GetEveryoneTempDocumentPath(docObjects);

                DocObjects docObjects1 = new DocObjects();
                docObjects1.Version = Convert.ToInt32(vid);
                docObjects1.RoleID = Convert.ToInt32(FromRoleID);
                docObjects1.EmpID = Convert.ToInt32(FromEmpID);
                DataTable dt1 = DMS_Bal.DMS_GetEveryoneTempDocumentPath(docObjects1);
                string html = GetHTMLContent(dt1.Rows[0]["FilePath"].ToString());
                ltFile1.InnerHtml = GetHTMLContent(dt.Rows[0]["FilePath"].ToString());
                ltFile2.InnerHtml = GetDiffHtml(ltFile1.InnerHtml, html);
                div2.InnerText = dt.Rows[0]["RoleName"].ToString() + "-" + dt.Rows[0]["EmpName"].ToString();
                div3.InnerText = dt1.Rows[0]["RoleName"].ToString() + "-" + dt1.Rows[0]["EmpName"].ToString();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DCV_M3:" + strline + "  " + strMsg);
                string strError = "DCV_M3:" + strline + "  " + "Everyone Track changes failed";
                lblError.Visible = true;
                lblError.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        private string GetHTMLContent(string path)
        {
            try
            {
                string docPath = Server.MapPath(@path);
                byte[] byteArray = File.ReadAllBytes(docPath);
                string docimageFolderPath = Server.MapPath(@"~/TempDMSCompare/img");
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    memoryStream.Write(byteArray, 0, byteArray.Length);
                    using (WordprocessingDocument doc = WordprocessingDocument.Open(memoryStream, true))
                    {
                        int imageCounter = 0;
                        HtmlConverterSettings settings = new HtmlConverterSettings()
                        {
                            PageTitle = "My Page Title",
                            ImageHandler = imageInfo =>
                            {
                                DirectoryInfo localDirInfo = new DirectoryInfo(docimageFolderPath);
                                if (!localDirInfo.Exists)
                                    localDirInfo.Create();
                                ++imageCounter;
                                string extension = imageInfo.ContentType.Split('/')[1].ToLower();
                                ImageFormat imageFormat = null;
                                if (extension == "png")
                                {
                                    extension = "gif";
                                    imageFormat = ImageFormat.Gif;
                                }
                                else if (extension == "gif")
                                    imageFormat = ImageFormat.Gif;
                                else if (extension == "bmp")
                                    imageFormat = ImageFormat.Bmp;
                                else if (extension == "jpeg")
                                    imageFormat = ImageFormat.Jpeg;
                                else if (extension == "tiff")
                                {
                                    extension = "gif";
                                    imageFormat = ImageFormat.Gif;
                                }
                                else if (extension == "x-wmf")
                                {
                                    extension = "wmf";
                                    imageFormat = ImageFormat.Wmf;
                                }
                                if (imageFormat == null)
                                    return null;

                                string imageFileName = "img/image" +
                                    imageCounter.ToString() + "." + extension;
                                try
                                {
                                    imageInfo.Bitmap.Save(imageFileName, imageFormat);
                                }
                                catch (System.Runtime.InteropServices.ExternalException)
                                {
                                    return null;
                                }
                                XElement img = new XElement(Xhtml.img,
                                    new XAttribute(NoNamespace.src, imageFileName),
                                    imageInfo.ImgStyleAttribute,
                                    imageInfo.AltText != null ?
                                        new XAttribute(NoNamespace.alt, imageInfo.AltText) : null);
                                return img;
                            }
                        };
                        XElement html = HtmlConverter.ConvertToHtml(doc, settings);
                        return html.ToStringNewLineOnAttributes();
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //string strline = HelpClass.LineNo(ex);
                //string strMsg = HelpClass.SQLEscapeString(ex.Message);
                //Aizant_log.Error("DCV_M4:" + strline + "  " + strMsg);
                //string strError = "DCV_M4:" + strline + "  " + "HTML Conversion failed";
                //lblError.Visible = true;
                //lblError.Text = @" <b style=""color:red;"">" + strError + "</b> ";
                //return null;
            }
        }
        private string GetDiffHtml(string text1, string text2)
        {
            try
            {
                HtmlDiff.HtmlDiff diffHelper = new HtmlDiff.HtmlDiff(text1, text2);
                return diffHelper.Build();
            }
            catch (Exception ex)
            {
                throw ex;
                //string strline = HelpClass.LineNo(ex);
                //string strMsg = HelpClass.SQLEscapeString(ex.Message);
                //Aizant_log.Error("DCV_M5:" + strline + "  " + strMsg);
                //string strError = "DCV_M5:" + strline + "  " + "HTML Difference Calculation failed";
                //lblError.Visible = true;
                //lblError.Text = @" <b style=""color:red;"">" + strError + "</b> ";
                //return null;
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            Button1.Enabled = false;
            try
            {
                docObjects.Version = Convert.ToInt32(hdfDocVID.Value);
                docObjects.EmpID = Convert.ToInt32(hdfFromEmpID.Value);
                docObjects.RoleID = Convert.ToInt32(hdfFromRoleID.Value);
                DataTable dt = DMS_Bal.DMS_GetEveryoneChangedDocumentPath(docObjects);
                string strFilePath = Server.MapPath(@dt.Rows[0][0].ToString());
                ASPxRichEdit1.Open(strFilePath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "hideTrackChanges();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DCV_M4:" + strline + "  " + strMsg);
                string strError = "DCV_M4:" + strline + "  " + "Original Document Viewing failed";
                lblError.Visible = true;
                lblError.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
            finally
            {
                Button1.Enabled = true;
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }

    }
}