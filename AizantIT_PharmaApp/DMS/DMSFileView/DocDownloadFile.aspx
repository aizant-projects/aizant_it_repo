﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocDownloadFile.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DMSFileView.DocDownloadFile" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
             <asp:Literal ID="literaldownload" runat="server" ></asp:Literal>
             <asp:Label runat="server" ID="lblDownload">Wait until Download finishes, then click on ok.</asp:Label>
        </div>
    </form>
</body>
</html>
