﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using DevExpress.Web.Office;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Layout;
using DevExpress.XtraRichEdit.API.Native;

namespace AizantIT_PharmaApp.DMS.DMSFileView
{
    public partial class FormPrintView : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        DocObjects docObjects = new DocObjects();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString.Count > 0)
                    {
                        string rid = Request.QueryString[0];
                        hdfRID.Value = rid;
                        if (Request.QueryString["reader"] != null)
                        {
                            Btn_Print.Visible = true;
                            //hdfEmpName.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString();
                            //msgSuccess.InnerHtml = "New Form Request has been Initiated Successfully";
                        }
                        ToViewForm(rid);
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FPV_M1:" + strline + "  " + strMsg);
                string strError = "FPV_M1:" + strline + "  " + "Page loading failed";
                divliteral.Visible = true;
                richedit2.Visible = false;
                literalSop1.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }

        protected void Btn_Print_Click(object sender, EventArgs e)
        {
            try
            {
                System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
                docObjects.FormReqID = Convert.ToInt32(hdfRID.Value);
                DataTable dt1 = DMS_Bal.DMS_GetFormPrintReqDetails(docObjects);
                docObjects.FormVersionID = Convert.ToInt32(dt1.Rows[0]["FormVersionID"]);
                DataTable data1 = DMS_Bal.DMS_GetFormDetails(docObjects);
                DataTable DMS_DT = DMS_Bal.DMS_GetFormContent(docObjects);
                byte[] docBytes1 = (byte[])DMS_DT.Rows[0]["FormContent"];
                //string strTargPath1 = Server.MapPath(@"~/Files/WordFile.rtf");           
                for (int i = Convert.ToInt32(dt1.Rows[0]["printStartSerialNo"]); i <= Convert.ToInt32(dt1.Rows[0]["printEndSerialNo"]); i++)
                {
                    printDocument(dt1, data1, i, docBytes1, printerSettings);
                }
                DocPrintRequest DocReqObjects = new DocPrintRequest();
                DocReqObjects.DocReqID = Convert.ToInt32(hdfRID.Value);
                DocReqObjects.RequestedByID = Convert.ToInt32(dt1.Rows[0]["RequestByID"]);
                int DMS_AP = DMS_BalDM.DMS_FormPrintReqByRequestor(DocReqObjects);
                //HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Form Request Number " + dt1.Rows[0]["RequestNo"].ToString() + " Printed by Requester Successfully", "success", "reloadtable();");
                msgSuccess.InnerHtml = "New Document Request with no " + dt1.Rows[0]["RequestNo"].ToString() + " Printed by Requester Successfully";
                Upsucsess.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "$('#ModalSuccess').modal({ backdrop: 'static', keyboard: false });", true);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop3", "closeiframe();", true); 
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FPV_M2:" + strline + "  " + strMsg);
                string strError = "FPV_M2:" + strline + "  " + "Form Printing failed";
                divliteral.Visible = true;
                richedit2.Visible = false;
                literalSop1.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        public void printDocument(DataTable dt, DataTable data, int copyNo, byte[] docBytes, System.Drawing.Printing.PrinterSettings printerSettings)
        {
            try
            {
                string strFileUpload = DynamicFolder.CreateDynamicFolder(6);
                string File1Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".rtf";
                string strTargPath = (strFileUpload + "\\" + File1Path);
                File.WriteAllBytes(strTargPath, docBytes);
                RichEditDocumentServer serverSource = new RichEditDocumentServer();
                serverSource.LoadDocument(strTargPath);
                int SourcePageCount = serverSource.DocumentLayout.GetPageCount();
                RichEditDocumentServer server = new RichEditDocumentServer();
                server.LoadDocument(Server.MapPath(@"~/Files/FormPrintTemplate.rtf"), DevExpress.XtraRichEdit.DocumentFormat.Rtf);
                Document document = server.Document;
                Section firstSection = document.Sections[0];
                //DataTable table = DMS_Bal.DMS_GetEndSNoofFormRequest(docObjects);
                SubDocument myHeader1 = firstSection.BeginUpdateHeader(HeaderFooterType.Primary);
                DocumentRange[] printbyformNumber = myHeader1.FindAll("formnum1", SearchOptions.CaseSensitive, myHeader1.Range);
                myHeader1.InsertText(printbyformNumber[0].Start, data.Rows[0]["FormNumber"].ToString() + "-" + data.Rows[0]["FormVersionNumber"].ToString());
                myHeader1.Delete(printbyformNumber[0]);
                DocumentRange[] printbyformName = myHeader1.FindAll("formname1", SearchOptions.CaseSensitive, myHeader1.Range);
                myHeader1.InsertText(printbyformName[0].Start, data.Rows[0]["FormName"].ToString());
                myHeader1.Delete(printbyformName[0]);
                //foreach (DocumentRange documentRange in printbyformNumber)
                //{
                //    myHeader1.InsertText(documentRange.Start, data.Rows[0]["FormNumber"].ToString());
                //    myHeader1.Delete(documentRange);
                //}
                DocumentRange[] printbyrequestNumber = myHeader1.FindAll("reqnum1", SearchOptions.CaseSensitive, myHeader1.Range);
                myHeader1.InsertText(printbyrequestNumber[0].Start, dt.Rows[0]["RequestNo"].ToString());
                myHeader1.Delete(printbyrequestNumber[0]);
                //foreach (DocumentRange documentRange in printbyrequestNumber)
                //{
                //    myHeader1.InsertText(documentRange.Start, dt.Rows[0]["RequestNo"].ToString());
                //    myHeader1.Delete(documentRange);
                //}
                DocumentRange[] printbydepartment = myHeader1.FindAll("fromdept1", SearchOptions.CaseSensitive, myHeader1.Range);
                myHeader1.InsertText(printbydepartment[0].Start, data.Rows[0]["DeptName"].ToString());
                myHeader1.Delete(printbydepartment[0]);
                //foreach (DocumentRange documentRange in printbydepartment)
                //{
                //    myHeader1.InsertText(documentRange.Start, data.Rows[0]["DeptName"].ToString());
                //    myHeader1.Delete(documentRange);
                //}
                //if (table.Rows[0][0].ToString() != "0")
                //{
                //for(int i= Convert.ToInt32(dt.Rows[0]["printStartSerialNo"]);i<= Convert.ToInt32(dt.Rows[0]["printEndSerialNo"]); i++)
                //{
                DocumentRange[] printbycopyNumber = myHeader1.FindAll("copnum1", SearchOptions.CaseSensitive, myHeader1.Range);
                myHeader1.InsertText(printbycopyNumber[0].Start, "0000" + copyNo.ToString());
                myHeader1.Delete(printbycopyNumber[0]);
                //}
                //DocumentRange[] printbycopyNumber = myHeader1.FindAll("copnum1", SearchOptions.CaseSensitive, myHeader1.Range);
                //myHeader1.InsertText(printbycopyNumber[0].Start, data.Rows[0]["RequestBy"].ToString());
                //myHeader1.Delete(printbycopyNumber[0]);
                //}
                //else
                //{
                //    for (int i = 1; i <= Convert.ToInt32(dt.Rows[0]["NooFCopies"]); i++)
                //    {
                //        DocumentRange[] printbycopyNumber = myHeader1.FindAll("copnum1", SearchOptions.CaseSensitive, myHeader1.Range);
                //        myHeader1.InsertText(printbycopyNumber[0].Start, "0000" + i.ToString());
                //        myHeader1.Delete(printbycopyNumber[0]);
                //    }
                //}
                //DocumentRange[] printbycopyNumber = myHeader1.FindAll("copnum1", SearchOptions.CaseSensitive, myHeader1.Range);
                //myHeader1.InsertText(printbycopyNumber[0].Start, data.Rows[0]["RequestBy"].ToString());
                //myHeader1.Delete(printbycopyNumber[0]);
                //foreach (DocumentRange documentRange in printbycopyNumber)
                //{
                //    myHeader1.InsertText(documentRange.Start, dt.Rows[0]["RequestBy"].ToString());
                //    myHeader1.Delete(documentRange);
                //}
                DocumentRange[] printbyrefSOPNumber = myHeader1.FindAll("refsopnum1", SearchOptions.CaseSensitive, myHeader1.Range);
                myHeader1.InsertText(printbyrefSOPNumber[0].Start, data.Rows[0]["Ref_DocNumber"].ToString());
                myHeader1.Delete(printbyrefSOPNumber[0]);
                //foreach (DocumentRange documentRange in printbyrefSOPNumber)
                //{
                //    myHeader1.InsertText(documentRange.Start, data.Rows[0]["Ref_DocNumber"].ToString());
                //    myHeader1.Delete(documentRange);
                //}
                firstSection.EndUpdateHeader(myHeader1);
                //SubDocument myFooter1 = firstSection.BeginUpdateFooter(HeaderFooterType.Primary);
                //DocumentRange[] requestByrange1 = myFooter1.FindAll("reqby1", SearchOptions.CaseSensitive, myFooter1.Range);
                //myFooter1.InsertText(requestByrange1[0].Start, dt.Rows[0]["RequestBy"].ToString());
                //myFooter1.Delete(requestByrange1[0]);
                ////foreach (DocumentRange documentRange in requestByrange1)
                ////{
                ////    myFooter1.InsertText(documentRange.Start, dt.Rows[0]["RequestBy"].ToString());
                ////    myFooter1.Delete(documentRange);
                ////}
                //DocumentRange[] requestDaterange1 = myFooter1.FindAll("reqd1", SearchOptions.CaseSensitive, myFooter1.Range);
                //myFooter1.InsertText(requestDaterange1[0].Start, dt.Rows[0]["RequestDate"].ToString());
                //myFooter1.Delete(requestDaterange1[0]);
                ////foreach (DocumentRange documentRange in requestDaterange1)
                ////{
                ////    myFooter1.InsertText(documentRange.Start, dt.Rows[0]["RequestDate"].ToString());
                ////    myFooter1.Delete(documentRange);
                ////}
                //DocumentRange[] reviewedByrange1 = myFooter1.FindAll("revby1", SearchOptions.CaseSensitive, myFooter1.Range);
                //myFooter1.InsertText(reviewedByrange1[0].Start, dt.Rows[0]["ReviewedBy"].ToString());
                //myFooter1.Delete(reviewedByrange1[0]);
                ////foreach (DocumentRange documentRange in reviewedByrange1)
                ////{
                ////    myFooter1.InsertText(documentRange.Start, dt.Rows[0]["ReviewedBy"].ToString());
                ////    myFooter1.Delete(documentRange);
                ////}
                //DocumentRange[] reviewedDaterange1 = myFooter1.FindAll("revd1", SearchOptions.CaseSensitive, myFooter1.Range);
                //myFooter1.InsertText(reviewedDaterange1[0].Start, dt.Rows[0]["ReviewedDate"].ToString());
                //myFooter1.Delete(reviewedDaterange1[0]);
                ////foreach (DocumentRange documentRange in reviewedDaterange1)
                ////{
                ////    myFooter1.InsertText(documentRange.Start, dt.Rows[0]["ReviewedDate"].ToString());
                ////    myFooter1.Delete(documentRange);
                ////}
                //DocumentRange[] approvedByrange1 = myFooter1.FindAll("appby1", SearchOptions.CaseSensitive, myFooter1.Range);
                //myFooter1.InsertText(approvedByrange1[0].Start, dt.Rows[0]["DocController"].ToString());
                //myFooter1.Delete(approvedByrange1[0]);
                ////foreach (DocumentRange documentRange in approvedByrange1)
                ////{
                ////    myFooter1.InsertText(documentRange.Start, dt.Rows[0]["DocController"].ToString());
                ////    myFooter1.Delete(documentRange);
                ////}
                //DocumentRange[] approvedDaterange1 = myFooter1.FindAll("appd1", SearchOptions.CaseSensitive, myFooter1.Range);
                //myFooter1.InsertText(approvedDaterange1[0].Start, dt.Rows[0]["ControlledDate"].ToString());
                //myFooter1.Delete(approvedDaterange1[0]);
                ////foreach (DocumentRange documentRange in approvedDaterange1)
                ////{
                ////    myFooter1.InsertText(documentRange.Start, dt.Rows[0]["ControlledDate"].ToString());
                ////    myFooter1.Delete(documentRange);
                ////}
                //firstSection.EndUpdateFooter(myFooter1);
                //document.CaretPosition=firstSection.Range.Start;
                //string path = Server.MapPath(@"~/Files/FirstPage.docx");
                string strFile1Upload = DynamicFolder.CreateDynamicFolder(6);
                string File2Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".rtf";
                string path = (strFile1Upload + "\\" + File2Path);
                DocumentPosition pos1 = document.CreatePosition(0);
                document.InsertDocumentContent(pos1, strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Rtf, InsertOptions.KeepSourceFormatting);
                int PageCount = server.DocumentLayout.GetPageCount();
                for (int i = PageCount; i > SourcePageCount; i--)
                {
                    LayoutPage page = server.DocumentLayout.GetPage(i - 1);
                    DocumentRange rangeToDelete = document.CreateRange(page.MainContentRange.Start, page.MainContentRange.Length);
                    document.Delete(rangeToDelete);
                }
                //Section lastSection = server.Document.Sections[server.Document.Sections.Count - 1];
                //int startPosition = lastSection.Paragraphs[0].Range.Start.ToInt();
                //int endPosition = lastSection.Paragraphs[lastSection.Paragraphs.Count - 1].Range.End.ToInt();
                //DocumentRange lastSectionDoccumentRange = server.Document.CreateRange((startPosition), (endPosition - startPosition));
                //string restDocumentText = server.Document.GetText(lastSectionDoccumentRange);
                //if ((restDocumentText.Replace("\r\n", "").Trim() == ""))
                //{
                //    server.Document.Delete(lastSectionDoccumentRange);
                //}
                document.SaveDocument(path, DevExpress.XtraRichEdit.DocumentFormat.Rtf);

                //Byte[] docBytes1=File.ReadAllBytes(path);
                //string Message = Encoding.ASCII.GetString(docBytes1);
                //string output = Regex.Replace(Message, @"<p>\s*</p>", "").Trim();
                //RichEditDocumentServer server1 = new RichEditDocumentServer();
                //Document document1 = server1.Document;
                ////document1.AppendDocumentContent(path, DocumentFormat.OpenXml, path ,InsertOptions.MergeFormatting);
                //server1.LoadDocument(path, DocumentFormat.Rtf);

                ////document1.AppendDocumentContent(strTargPath, DocumentFormat.Rtf, strTargPath ,InsertOptions.MergeFormatting);
                //document1.SaveDocument(path,DocumentFormat.Rtf);


                try
                {
                    //Specify the number of copies:
                    //printerSettings.Copies = Convert.ToSByte(dt.Rows[0]["NooFCopies"].ToString());
                    printerSettings.Duplex = System.Drawing.Printing.Duplex.Simplex;
                    //printerSettings.FromPage = 1;
                    //printerSettings.ToPage = PageCount - 1;
                    //printerSettings.PrintRange = System.Drawing.Printing.PrintRange.SomePages;

                    //Print the document: 
                    server.Print(printerSettings);
                }
                catch { }

                DocumentManager.CloseDocument(Guid.NewGuid().ToString());
                // Delete the file if it exists.
                //if (File.Exists(strTargPath))
                //{
                //    // Note that no lock is put on the
                //    // file and the possibility exists
                //    // that another process could do
                //    // something with it between
                //    // the calls to Exists and Delete.
                //    File.Delete(strTargPath);
                //}
                //if (File.Exists(path))
                //{
                //    // Note that no lock is put on the
                //    // file and the possibility exists
                //    // that another process could do
                //    // something with it between
                //    // the calls to Exists and Delete.
                //    File.Delete(path);
                //}
                server.Dispose();
                //Marshal.ReleaseComObject(server);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRL_M9:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRL_M9:" + strline + "  " + "Form Request Print Copy failed", "error");
            }
        }
        private void ToViewForm(string rid)
        {
            try
            {
                docObjects.FormReqID = Convert.ToInt32(rid);
                DataTable DMS_DT1 = DMS_Bal.DMS_GetFormPrintReqDetails(docObjects);
                DocObjects docObjects1 = new DocObjects();
                docObjects1.FormVersionID = Convert.ToInt32(DMS_DT1.Rows[0]["FormVersionID"]);
                DataTable dt = DMS_Bal.DMS_GetFormContent(docObjects1);
                divliteral.Visible = false;
                richedit2.Visible = true;
                if (dt.Rows[0]["FormContent"].ToString().Length > 0)
                {
                    ASPxRichEdit1.Open(
                    Guid.NewGuid().ToString(),
                    DevExpress.XtraRichEdit.DocumentFormat.Rtf,
                    () =>
                    {
                        byte[] docBytes = (byte[])dt.Rows[0]["FormContent"];
                        return new MemoryStream(docBytes);
                    }
                    );
                }
                else
                {
                    divliteral.Visible = true;
                    richedit2.Visible = false;
                    literalSop1.Text = @" <b style=""color:red;"">" + "Form Not Available To View " + "</b> ";
                }
            }
            catch (Exception)
            {
                divliteral.Visible = true;
                richedit2.Visible = false;
                literalSop1.Text = "Form Not Available To View, Bad data ";
            }
        }
    }
}