﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AizantIT_PharmaApp.DMS.DMSFileView
{
    public partial class DocPrintView : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocObjects docObjects = new DocObjects();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString.Count > 0)
                    {
                        string rid = Request.QueryString[0];
                        hdfRID.Value = rid;
                        string status = Request.QueryString[1];
                        if (Request.QueryString["reader"] != null)
                        {
                            msgSuccess.InnerHtml = "New Document Request has been Initiated Successfully";
                            //msgError.InnerText = "All * fields are mandatory";
                        }
                        ToViewSopDocument(rid);
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DWRV_M1:" + strline + "  " + strMsg);
                string strError = "DWRV_M1:" + strline + "  " + "Page loading failed";
                divliteral.Visible = true;
                richedit2.Visible = false;
                divliteral.InnerText = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        private void ToViewSopDocument(string rid)
        {
            try
            {
            }
            catch (Exception)
            {
                divliteral.Visible = true;
                richedit2.Visible = false;
                literalSop1.Text = "File Not Available To View, Bad data ";
            }
        }
    }
}