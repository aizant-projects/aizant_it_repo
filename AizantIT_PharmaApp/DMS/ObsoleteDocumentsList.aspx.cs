﻿using AizantIT_DMSBAL;
using AizantIT_PharmaApp.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AizantIT_PharmaApp.DMS
{
    public partial class MainDocumentDetailsList : System.Web.UI.Page
    {
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();

        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        protected void Page_Load(object sender, EventArgs e)
        {
            if (HelpClass.IsUserAuthenticated())
            {
                if (!IsPostBack)
                {
                    hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                  //  InitializeThePage();
                    DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                    DataTable dtTemp = new DataTable();
                    DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                    if (drUMS.Length > 0)
                    {
                        dtTemp = drUMS.CopyToDataTable();
                        if (dtTemp.Select("RoleID=2").Length > 0)//admin role
                        {
                            hdfRole.Value = "2";
                        }
                        else
                        {
                            loadAuthorizeErrorMsg();
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx", false);
                    }
                }
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", "getadmincolumn();", true);
            }
            else
            {
                Response.Redirect("~/UserLogin.aspx", false);
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        protected void btnCommentsHistoryView_Click(object sender, EventArgs e)
        {
            btnCommentsHistoryView.Enabled = false;
            try
            {
                DataTable DMS_DT = DMS_BalDM.DMS_GetComments(hdfVID.Value);
                gv_CommentHistory.DataSource = DMS_DT;
                ViewState["HistoryDataTable"] = DMS_DT;
                gv_CommentHistory.DataBind();
                UpGridButtons.Update();
                if (DMS_DT.Rows.Count > 0)
                {
                    span4.Attributes.Add("title", DMS_DT.Rows[0]["DocumentVersionTitle"].ToString());
                    if (DMS_DT.Rows[0]["DocumentVersionTitle"].ToString().Length > 60)
                    {
                        span4.InnerHtml = DMS_DT.Rows[0]["DocumentVersionTitle"].ToString().Substring(0, 60) + "...";
                    }
                    else
                    {
                        span4.InnerHtml = DMS_DT.Rows[0]["DocumentVersionTitle"].ToString();
                    }
                    upcomment.Update();
                }
                else
                {
                    span4.InnerText = "Comment History";
                    upcomment.Update();
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myCommentHistory').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MDL_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MDL_M3:" + strline + "  " + "History viewing failed.", "error");
            }
            finally
            {
                btnCommentsHistoryView.Enabled = true;
            }
        }

        protected void gv_CommentHistory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string _fullcomments = (e.Row.Cells[5].FindControl("txt_Comments") as TextBox).Text;
                    LinkButton _lnkmorecomments = (e.Row.Cells[5].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (e.Row.Cells[5].FindControl("lbcommentsless") as LinkButton);
                    if (_fullcomments.Length > 70)
                    {
                        (e.Row.Cells[5].FindControl("lbl_shortComments") as Label).Visible = true;
                        TextBox FullTbx = (e.Row.Cells[5].FindControl("txt_Comments") as TextBox);
                        (e.Row.Cells[5].FindControl("lbl_shortComments") as Label).Text = _fullcomments.Substring(0, 70) + "..";
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = true;

                        if (_fullcomments.Length > 250 && _fullcomments.Length <= 500)
                        {
                            FullTbx.Rows = 6;
                        }
                        else if (_fullcomments.Length > 500 && _fullcomments.Length <= 1000)
                        {
                            FullTbx.Rows = 9;
                        }
                        else if (_fullcomments.Length > 1000 && _fullcomments.Length <= 1500)
                        {
                            FullTbx.Rows = 12;
                        }
                        else if (_fullcomments.Length > 1500 && _fullcomments.Length <= 2000)
                        {
                            FullTbx.Rows = 15;
                        }
                        else if (_fullcomments.Length > 2000)
                        {
                            FullTbx.Rows = 20;
                        }
                        FullTbx.Visible = false;
                    }
                    else
                    {
                        (e.Row.Cells[5].FindControl("lbl_shortComments") as Label).Visible = true;
                        (e.Row.Cells[5].FindControl("txt_Comments") as TextBox).Visible = false;
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MDL_M7:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MDL_M7:" + strline + "  " + "Comment History row data-bound failed.", "error");
            }
        }
        protected void gv_CommentHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "SM")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    LinkButton _lnkmorecomments = (row.Cells[5].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (row.Cells[5].FindControl("lbcommentsless") as LinkButton);
                    (row.Cells[5].FindControl("lbl_shortComments") as Label).Visible = false;
                    (row.Cells[5].FindControl("txt_Comments") as TextBox).Visible = true;
                    _lnklesscomments.Visible = true;
                    _lnkmorecomments.Visible = false;
                }
                else if (e.CommandName == "SL")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    LinkButton _lnkmorecomments = (row.Cells[5].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (row.Cells[5].FindControl("lbcommentsless") as LinkButton);
                    (row.Cells[5].FindControl("lbl_shortComments") as Label).Visible = true;
                    (row.Cells[5].FindControl("txt_Comments") as TextBox).Visible = false;
                    _lnklesscomments.Visible = false;
                    _lnkmorecomments.Visible = true;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MDL_M8:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MDL_M8:" + strline + "  " + "Comment History row command failed.", "error");
            }
        }
        protected void gv_CommentHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_CommentHistory.PageIndex = e.NewPageIndex;
                gv_CommentHistory.DataSource = (DataTable)(ViewState["HistoryDataTable"]);
                gv_CommentHistory.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MDL_M9:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MDL_M9:" + strline + "  " + "grid view page index changing failed.", "error");
            }
        }
     
        protected void btnFileView1_Click(object sender, EventArgs e)
        {
            btnFileView1.Enabled = false;
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfVID.Value);
                hdfStatus.Value = DMS_DT.Rows[0]["DocStatus"].ToString();
                span_FileTitle.InnerHtml = "<b>" + DMS_DT.Rows[0]["DocumentName"].ToString() + "</b>";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_lock').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                if (hdfStatus.Value == "A" || hdfStatus.Value == "I" || hdfStatus.Value == "R")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view2", "LoadfinalPDFDocument();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view2", "LoadPDFDocument();", true);
                }
                hdfViewType.Value = "0";
                hdfViewEmpID.Value = "0";
                hdfViewRoleID.Value = "11";//author

                UpHeading.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MDL_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MDL_M2:" + strline + "  " + "Document Viewing1 failed.", "error");
            }
            finally
            {
                btnFileView1.Enabled = true;
            }
        }

        protected void btnFileView_Click(object sender, EventArgs e)
        {
            btnFileView.Enabled = false;
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfVID.Value);
                hdfStatus.Value = DMS_DT.Rows[0]["DocStatus"].ToString();
                span_FileTitle.InnerHtml = "<b>" + DMS_DT.Rows[0]["DocumentName"].ToString() + "</b>";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_lock').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                if (hdfStatus.Value == "A" || hdfStatus.Value == "I" || hdfStatus.Value == "R")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view1", "LoadfinalPDFDocument();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view1", "LoadPDFDocument();", true);
                }
                hdfViewType.Value = "0";
                hdfViewEmpID.Value = "0";
                hdfViewRoleID.Value = "11";//author               
                UpHeading.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MDL_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MDL_M1:" + strline + "  " + "Document Viewing failed.", "error");
            }
            finally
            {
                btnFileView.Enabled = true;
            }
        }
    }
}