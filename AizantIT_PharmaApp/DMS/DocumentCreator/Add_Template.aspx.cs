﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace AizantIT_PharmaApp.DMS.DocumentCreator
{
    public partial class Add_Template : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        ReviewObjects reviewObjects = new ReviewObjects();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            ConfirmCustomAlert.buttonClick += new EventHandler(ConfirmCustomAlert_buttonClick);
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=2").Length > 0)//2-DMSAdmin
                            {
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Uclose", "CloseBrowser();", true);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Uclose", "CloseBrowser();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("AT_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "AT_M1:" + strline + "  " + "Document Load failed", "error");
            }
        }
        protected void ConfirmCustomAlert_buttonClick(object sender, EventArgs e)
        {
            try
            {
                DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
                ReviewObjects reviewObjects = new ReviewObjects();
                JQDataTableBO objJQDataTableBO = new JQDataTableBO();
                objJQDataTableBO.iMode = 0;
                objJQDataTableBO.pkid = Convert.ToInt32(hdfPKID.Value);
                objJQDataTableBO.iDisplayLength = 0;
                objJQDataTableBO.iDisplayStart = 0;
                objJQDataTableBO.iSortCol = 0;
                objJQDataTableBO.sSortDir = "";
                objJQDataTableBO.sSearch = "";
                DataTable dt = DMS_Bal.DMS_GetTemplateNameList(objJQDataTableBO);
                hdfEmpID.Value = dt.Rows[0]["CreatedBy"].ToString();
                if (hdfEmpID.Value == (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString())
                {
                    reviewObjects.Id = Convert.ToInt32(hdfPKID.Value);
                    int DMS_Del = DMS_Bal.DMS_DeleteTemplate(reviewObjects);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "reloadtable();", true);
                    ScriptManager.RegisterStartupScript(btnTemplateDel, btnTemplateDel.GetType(), "Pop", "custAlertMsg('Template Removed Successfully.','success');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(btnTemplateDel, btnTemplateDel.GetType(), "Pop", "custAlertMsg('You done have permission to Remove this template!','warning');", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("AT_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "AT_M2:" + strline + "  " + "Template Deletion failed", "error");
            }
        }
        protected void btn_tSubmit_Click(object sender, EventArgs e)
        {
            btn_tSubmit.Enabled = false;
            try
            {
                StringBuilder sbErrorMsg = new StringBuilder();
                if (txt_TemplateName.Text.Trim() == "")
                {
                    sbErrorMsg.Append("Please Enter a Template Name" + "<br/>");
                }
                if (!File_Template.HasFile)
                {
                    sbErrorMsg.Append("Please Upload a File" + "<br/>");
                }
                if (sbErrorMsg.Length > 8)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('" + sbErrorMsg.ToString() + "','warning');", true);
                    return;
                }
                reviewObjects.TemplateName = txt_TemplateName.Text.Trim();
                DataTable dt = DMS_Bal.DMS_TemplateNameSearch(reviewObjects);
                if (dt.Rows.Count > 0)
                {
                    txt_TemplateName.Text = "";
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('Template Name Already Available in Database.Please Enter Another Name','warning');", true);
                    return;
                }
                else
                {
                    byte[] imgbyte = null;
                    string ext = System.IO.Path.GetExtension(File_Template.FileName);
                    if (File_Template.HasFile)
                    {
                        Stream fs = File_Template.PostedFile.InputStream;
                        BinaryReader br = new BinaryReader(fs);
                        imgbyte = br.ReadBytes((Int32)fs.Length);
                        string mime = MimeType.GetMimeType(imgbyte, File_Template.FileName);
                        string[] filemimes1 = new string[] { "application/pdf", "application/x-msdownload", "application/unknown", "application/octet-stream" };
                        string[] filemimes = new string[] { "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/x-zip-compressed" };
                        if (filemimes1.Contains(mime))
                        {
                            txt_TemplateName.Text = "";
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('Upload only .doc or .docx files to upload! <br/> The uploaded file is not valid or corrupted','warning');", true);
                            return;
                        }
                        if (!filemimes.Contains(mime))
                        {
                            txt_TemplateName.Text = "";
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('Upload only .doc or .docx files to upload! <br/> The uploaded file is not valid or corrupted','warning');", true);
                            return;
                        }
                    }
                    if (ext == ".doc" || ext == ".docx")
                    {
                        string strFileName = File_Template.FileName;
                        string strFileUpload = DynamicFolder.CreateDynamicFolder(8);
                        string File1Path = DateTime.Now.ToString("yyyyMMddHHmmss") + ext;
                        // File.Create(strFileUpload + File1Path);
                        // string strTargPath = Server.MapPath(@"~/Files") + "\\" + strFileName;
                        string strTargPath = (strFileUpload + "\\" + File1Path);
                        File_Template.PostedFile.SaveAs(strTargPath);
                        if (ext == ".doc")
                        {
                            //RichEditDocumentServer server = new RichEditDocumentServer();
                            //Document document = server.Document;
                            //server.LoadDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Doc);
                            //Section firstSection = document.Sections[0];
                            //RichEditDocumentServer server1 = new RichEditDocumentServer();
                            //Document document1 = server1.Document;
                            //StringBuilder sbHFErrorMsg = new StringBuilder();
                            ////if (firstSection.HasHeader(HeaderFooterType.Primary))
                            ////{
                            ////    sbHFErrorMsg.Append("Upload the .docx file without header!" + "<br/>");
                            ////}
                            ////if (firstSection.HasFooter(HeaderFooterType.Primary))
                            ////{
                            ////    sbHFErrorMsg.Append("Upload the .docx file without footer!" + "<br/>");
                            ////}
                            //if (sbHFErrorMsg.Length > 8)
                            //{
                            //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('" + sbHFErrorMsg.ToString() + "','warning');", true);
                            //    return;
                            //}
                            //else
                            //{
                            //    //document1.AppendDocumentContent(Server.MapPath(@"~/Files/DocDetailsTemplate.docx"), DevExpress.XtraRichEdit.DocumentFormat.OpenXml, Server.MapPath(@"~/Files/DocDetailsTemplate.docx"), InsertOptions.KeepSourceFormatting);
                            //    //document1.AppendDocumentContent(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Doc, strTargPath, InsertOptions.KeepSourceFormatting);
                            //}
                            //document1.SaveDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Doc);
                            //server.Dispose();
                            //server1.Dispose();
                            //Marshal.ReleaseComObject(server);
                            //Marshal.ReleaseComObject(server1);
                            Array.Clear(imgbyte, 0, imgbyte.Length);
                            Stream fs = File.OpenRead(strTargPath);
                            BinaryReader br = new BinaryReader(fs);
                            imgbyte = br.ReadBytes((Int32)fs.Length);
                            fs.Close();
                            //Marshal.ReleaseComObject(fs);

                        }
                        else if (ext == ".docx")
                        {
                            //RichEditDocumentServer server = new RichEditDocumentServer();
                            //Document document = server.Document;
                            //server.LoadDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                            //Section firstSection = document.Sections[0];
                            //RichEditDocumentServer server1 = new RichEditDocumentServer();
                            //Document document1 = server1.Document;
                            //StringBuilder sbHFErrorMsg = new StringBuilder();
                            ////if (firstSection.HasHeader(HeaderFooterType.Primary))
                            ////{
                            ////    sbHFErrorMsg.Append("Upload the .docx file without header!" + "<br/>");
                            ////}
                            ////if (firstSection.HasFooter(HeaderFooterType.Primary))
                            ////{
                            ////    sbHFErrorMsg.Append("Upload the .docx file without footer!" + "<br/>");
                            ////}
                            //if (sbHFErrorMsg.Length > 8)
                            //{
                            //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('" + sbHFErrorMsg.ToString() + "','warning');", true);
                            //    return;
                            //}
                            //else
                            //{
                            //    //document1.AppendDocumentContent(Server.MapPath(@"~/Files/DocDetailsTemplate.docx"), DevExpress.XtraRichEdit.DocumentFormat.OpenXml, Server.MapPath(@"~/Files/DocDetailsTemplate.docx"), InsertOptions.KeepSourceFormatting);
                            //    //document1.AppendDocumentContent(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml, strTargPath, InsertOptions.KeepSourceFormatting);
                            //}
                            //document1.SaveDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                            //server.Dispose();
                            //server1.Dispose();
                            //Marshal.ReleaseComObject(server);
                            //Marshal.ReleaseComObject(server1);
                            Array.Clear(imgbyte, 0, imgbyte.Length);
                            Stream fs = File.OpenRead(strTargPath);
                            BinaryReader br = new BinaryReader(fs);
                            imgbyte = br.ReadBytes((Int32)fs.Length);
                            fs.Close();
                            //Marshal.ReleaseComObject(fs);
                        }
                        reviewObjects.Ext = ext.Trim();
                        reviewObjects.TemplateName = txt_TemplateName.Text.Trim();
                        reviewObjects.Content = imgbyte;
                        reviewObjects.Creator = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        int DMS_ins = DMS_Bal.DMS_InsertTemplate(reviewObjects);
                        //if (File.Exists(strTargPath))
                        //{
                        //    // Note that no lock is put on the
                        //    // file and the possibility exists
                        //    // that another process could do
                        //    // something with it between
                        //    // the calls to Exists and Delete.
                        //    File.Delete(strTargPath);
                        //}
                        txt_TemplateName.Text = "";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "reloadtable();", true);
                        HelpClass.custAlertMsg(this, this.GetType(), "Template Uploaded Successfully", "success");
                    }
                    else
                    {
                        txt_TemplateName.Text = "";
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('Upload only .doc or .docx files to upload!','warning');", true);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("AT_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "AT_M3:" + strline + "  " + "Template Upload failed", "error");
            }
            finally
            {
                btn_tSubmit.Enabled = true;
            }
        }
        protected void btn_Reset_Click(object sender, EventArgs e)
        {
            btn_Reset.Enabled = false;
            try
            {
                txt_TemplateName.Text = "";
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("AT_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "AT_M4:" + strline + "  " + "Reset failed", "error");
            }
            finally { btn_Reset.Enabled = true; }
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        protected void btnTemplateView_Click(object sender, EventArgs e)
        {
            btnTemplateView.Enabled = false;
            try
            {
                DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
                JQDataTableBO objJQDataTableBO = new JQDataTableBO();
                objJQDataTableBO.iMode = 0;
                objJQDataTableBO.pkid = Convert.ToInt32(hdfPKID.Value);
                objJQDataTableBO.iDisplayLength = 0;
                objJQDataTableBO.iDisplayStart = 0;
                objJQDataTableBO.iSortCol = 0;
                objJQDataTableBO.sSortDir = "";
                objJQDataTableBO.sSearch = "";
                DataTable dt = DMS_Bal.DMS_GetTemplateNameList(objJQDataTableBO);
                span_FileTitle.InnerHtml = "<b>" + dt.Rows[0]["TemplateName"].ToString() + "</b>";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_lock').modal({ show: 'true', backdrop: 'static', keyboard: false });", true);
                dviframe.InnerHtml = "&nbsp";
                dviframe.InnerHtml = "<iframe src=\"" + ResolveUrl("~/DMS/DocumentCreator/TemplateView.aspx") + "?DocID=" + hdfPKID.Value + "#toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" height=\"640px\" ></iframe>";
                UpdatePanel4.Update();
                UpHeading.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("AT_M5:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "AT_M5:" + strline + "  " + "Template viewing failed", "error");
            }
            finally
            {
                btnTemplateView.Enabled = true;
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}