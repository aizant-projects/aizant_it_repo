﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace AizantIT_PharmaApp.DMS.DocumentCreator
{
    public partial class Existing_DocumentList : System.Web.UI.Page
    {
        DocObjects docObjects = new DocObjects();
        DocObjects docObjects1 = new DocObjects();
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        ViewState["pdfFileName"] = "";
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=11").Length > 0)//11-Author
                            {
                                ViewState["emp_id_DMS"] = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EDL_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EDL_M1:" + strline + "  " + "Document Load failed.", "error");
            }
        }
        protected void btnFileView_Click(object sender, EventArgs e)
        {
            btnFileView.Enabled = false;
            try
            {
                span_FileTitle.InnerHtml = "&nbsp";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "clearH", "  $('literalSop').html('');", true);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "clearH", "  $('span_FileTitle').html('');", true);
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(Convert.ToString(hdfVID.Value));
                if (DMS_DT != null && DMS_DT.Rows.Count > 0)
                {
                    docObjects.Version = Convert.ToInt32(DMS_DT.Rows[0]["DocumentID"]);
                    docObjects.RoleID = 0;
                    docObjects.EmpID = 0;
                    docObjects.FileType1 = ".pdf";
                    docObjects.Mode = 0;
                    System.Data.DataTable dt = DMS_Bal.DMS_GetDocumentLocationList(docObjects);
                    if (dt.Rows[0]["DyFileName"].ToString() == "0")
                    {
                        string storefolder = HttpContext.Current.Server.MapPath(@"~/Scripts/pdf.js/web/");
                        byte[] Filebyte = (byte[])DMS_DT.Rows[0]["Content"];
                        string File2Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                        string strTargPath = (storefolder + File2Path);
                        File.WriteAllBytes(strTargPath, Filebyte);

                        docObjects1.Version = Convert.ToInt32(DMS_DT.Rows[0]["DocumentID"]);
                        docObjects1.RoleID = 11;//Author
                        docObjects1.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        docObjects1.FilePath1 = null;
                        docObjects1.FilePath2 = strTargPath;
                        docObjects1.isTemp = 0;
                        docObjects1.FileType1 = null;
                        docObjects1.FileType2 = ".pdf";
                        docObjects1.FileName1 = null;
                        docObjects1.FileName2 = File2Path;
                        int DOC_Ins = DMS_Bal.DMS_InsertDocumentLocation(docObjects);

                        ViewState["pdfFileName"] = File2Path;
                    }
                    else
                    {
                        ViewState["pdfFileName"] = dt.Rows[0]["DyFileName"].ToString();
                    }
                    span_FileTitle.InnerHtml = "<b>" + DMS_DT.Rows[0]["FileName"].ToString() + "</b>";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view1", "ViewExistingDoc();", true);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_lock').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>File not available.','info');", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EDL_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EDL_M2:" + strline + "  " + "File Viewing failed.", "error");
            }
            finally
            {
                hdfVID.Value = "";
                btnFileView.Enabled = true;
            }
        }
        protected void btnFileView1_Click(object sender, EventArgs e)
        {
            btnFileView1.Enabled = true;
            try
            {
                span_FileTitle.InnerHtml = "&nbsp";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "clearH", "  $('literalSop').html('');", true);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "clearH", "  $('span_FileTitle').html('');", true);
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(Convert.ToString(hdfVID.Value));
                if (DMS_DT != null && DMS_DT.Rows.Count > 0)
                {
                    docObjects.Version = Convert.ToInt32(DMS_DT.Rows[0]["DocumentID"]);
                    docObjects.RoleID = 0;
                    docObjects.EmpID = 0;
                    docObjects.FileType1 = ".pdf";
                    docObjects.Mode = 0;
                    System.Data.DataTable dt = DMS_Bal.DMS_GetDocumentLocationList(docObjects);
                    if (dt.Rows[0]["DyFileName"].ToString() == "0")
                    {
                        string storefolder = HttpContext.Current.Server.MapPath(@"~/Scripts/pdf.js/web/");
                        byte[] Filebyte = (byte[])DMS_DT.Rows[0]["Content"];
                        string File2Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                        string strTargPath = (storefolder + File2Path);
                        File.WriteAllBytes(strTargPath, Filebyte);

                        docObjects1.Version = Convert.ToInt32(DMS_DT.Rows[0]["DocumentID"]);
                        docObjects1.RoleID = 11;//Author
                        docObjects1.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        docObjects1.FilePath1 = null;
                        docObjects1.FilePath2 = strTargPath;
                        docObjects1.isTemp = 0;
                        docObjects1.FileType1 = null;
                        docObjects1.FileType2 = ".pdf";
                        docObjects1.FileName1 = null;
                        docObjects1.FileName2 = File2Path;
                        int DOC_Ins = DMS_Bal.DMS_InsertDocumentLocation(docObjects);

                        ViewState["pdfFileName"] = File2Path;
                    }
                    else
                    {
                        ViewState["pdfFileName"] = dt.Rows[0]["DyFileName"].ToString();
                    }
                    span_FileTitle.InnerHtml = "<b>" + DMS_DT.Rows[0]["FileName"].ToString() + "</b>";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view2", "ViewExistingDoc();", true);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_lock').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>File not available.','info');", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EDL_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EDL_M3:" + strline + "  " + "File Viewing1 failed.", "error");
            }
            finally
            {
                hdfVID.Value = "";
                btnFileView1.Enabled = true;
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        protected void btnCommentsHistoryView_Click(object sender, EventArgs e)
        {
            btnCommentsHistoryView.Enabled = false;
            try
            {
                DataTable DMS_DT = DMS_BalDM.DMS_GetComments(hdfVID.Value);
                gv_CommentHistory.DataSource = DMS_DT;
                ViewState["HistoryDataTable"] = DMS_DT;
                gv_CommentHistory.DataBind();
                upBtns.Update();
                if (DMS_DT.Rows.Count > 0)
                {
                    span4.Attributes.Add("title", DMS_DT.Rows[0]["DocumentTitle"].ToString());
                    if (DMS_DT.Rows[0]["DocumentTitle"].ToString().Length > 60)
                    {
                        span4.InnerHtml = DMS_DT.Rows[0]["DocumentTitle"].ToString().Substring(0, 60) + "...";
                    }
                    else
                    {
                        span4.InnerHtml = DMS_DT.Rows[0]["DocumentTitle"].ToString();
                    }
                    upcomment.Update();
                }
                else
                {
                    span4.InnerText = "Comment History";
                    upcomment.Update();
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myCommentHistory').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MDL_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MDL_M3:" + strline + "  " + "History viewing failed.", "error");
            }
            finally
            {
                btnCommentsHistoryView.Enabled = true;
            }
        }
        protected void gv_CommentHistory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string _fullcomments = (e.Row.Cells[5].FindControl("txt_Comments") as TextBox).Text;
                    LinkButton _lnkmorecomments = (e.Row.Cells[5].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (e.Row.Cells[5].FindControl("lbcommentsless") as LinkButton);
                    if (_fullcomments.Length > 70)
                    {
                        (e.Row.Cells[5].FindControl("lbl_shortComments") as Label).Visible = true;
                        TextBox FullTbx = (e.Row.Cells[5].FindControl("txt_Comments") as TextBox);
                        (e.Row.Cells[5].FindControl("lbl_shortComments") as Label).Text = _fullcomments.Substring(0, 70) + "..";
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = true;

                        if (_fullcomments.Length > 250 && _fullcomments.Length <= 500)
                        {
                            FullTbx.Rows = 6;
                        }
                        else if (_fullcomments.Length > 500 && _fullcomments.Length <= 1000)
                        {
                            FullTbx.Rows = 9;
                        }
                        else if (_fullcomments.Length > 1000 && _fullcomments.Length <= 1500)
                        {
                            FullTbx.Rows = 12;
                        }
                        else if (_fullcomments.Length > 1500 && _fullcomments.Length <= 2000)
                        {
                            FullTbx.Rows = 15;
                        }
                        else if (_fullcomments.Length > 2000)
                        {
                            FullTbx.Rows = 20;
                        }
                        FullTbx.Visible = false;
                    }
                    else
                    {
                        (e.Row.Cells[5].FindControl("lbl_shortComments") as Label).Visible = true;
                        (e.Row.Cells[5].FindControl("txt_Comments") as TextBox).Visible = false;
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MDL_M7:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MDL_M7:" + strline + "  " + "Comment History row data-bound failed.", "error");
            }
        }
        protected void gv_CommentHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                
                if (e.CommandName == "SM")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    LinkButton _lnkmorecomments = (row.Cells[5].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (row.Cells[5].FindControl("lbcommentsless") as LinkButton);
                    (row.Cells[5].FindControl("lbl_shortComments") as Label).Visible = false;
                    (row.Cells[5].FindControl("txt_Comments") as TextBox).Visible = true;
                    _lnklesscomments.Visible = true;
                    _lnkmorecomments.Visible = false;
                }
                else if (e.CommandName == "SL")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    LinkButton _lnkmorecomments = (row.Cells[5].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (row.Cells[5].FindControl("lbcommentsless") as LinkButton);
                    (row.Cells[5].FindControl("lbl_shortComments") as Label).Visible = true;
                    (row.Cells[5].FindControl("txt_Comments") as TextBox).Visible = false;
                    _lnklesscomments.Visible = false;
                    _lnkmorecomments.Visible = true;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MDL_M8:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MDL_M8:" + strline + "  " + "Comment History row command failed.", "error");
            }
        }
        protected void gv_CommentHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_CommentHistory.PageIndex = e.NewPageIndex;
                gv_CommentHistory.DataSource = (DataTable)(ViewState["HistoryDataTable"]);
                gv_CommentHistory.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MDL_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MDL_M4:" + strline + "  " + "grid view page index changing failed.", "error");
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }        
    }
}