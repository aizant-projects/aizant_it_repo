﻿using AizantIT_DMSBAL;
using AizantIT_PharmaApp.Common;
using DevExpress.XtraRichEdit;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AizantIT_PharmaApp.DMS.DocumentCreator
{
    public partial class ExistingDocumentView : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString.Count > 0)
                    {
                        if (Request.QueryString.Count == 1)
                        {
                            string vid = Request.QueryString["DocID"];
                            ViewExistingDocument(vid);
                        }
                        else if (Request.QueryString.Count == 2)
                        {
                            string vid = Request.QueryString["DocID"];
                            string sessop = Request.QueryString["sessop"];
                            ViewExistingDocumentSession(vid, sessop);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EDV_M1:" + strline + "  " + strMsg);
                string strError = "EDV_M1:" + strline + "  " + "Page loading failed";
                lblError.Visible = true;
                lblError.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        private void ViewExistingDocument(string vid)
        {
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(vid);
                if (DMS_DT.Rows[0]["Content"] != DBNull.Value)
                {
                    ASPxRichEdit1.Open(
                    Guid.NewGuid().ToString(),
                    DevExpress.XtraRichEdit.DocumentFormat.Rtf,
                    () =>
                    {
                        byte[] docBytes = (byte[])DMS_DT.Rows[0]["Content"];
                        return new MemoryStream(docBytes);
                    }
                );
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EDV_M2:" + strline + "  " + strMsg);
                string strError = "EDV_M2:" + strline + "  " + "Existing Document loading failed";
                lblError.Visible = true;
                lblError.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        private void ViewExistingDocumentSession(string vid,string sessop)
        {
            try
            {
                if (sessop.Trim() == "true")
                {
                    DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(vid);
                    if (Session["ExistingFileContent"] != null)
                    {
                        ASPxRichEdit1.Open(
                        Guid.NewGuid().ToString(),
                        DevExpress.XtraRichEdit.DocumentFormat.Rtf,
                        () =>
                        {
                            byte[] docBytes = Session["ExistingFileContent"] as byte[];
                            return new MemoryStream(docBytes);
                        }
                    );
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EDV_M3:" + strline + "  " + strMsg);
                string strError = "EDV_M3:" + strline + "  " + "Existing Document Session loading failed";
                lblError.Visible = true;
                lblError.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}