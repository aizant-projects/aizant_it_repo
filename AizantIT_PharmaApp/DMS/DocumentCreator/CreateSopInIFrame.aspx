﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateSopInIFrame.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentCreator.CreateSopInIFrame" %>

<%@ Register Assembly="DevExpress.Web.ASPxRichEdit.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRichEdit" TagPrefix="dx" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <dx:ASPxRichEdit ID="ASPxRichEdit1" runat="server" Width="1400px" WorkDirectory="~\Files">
                <Settings>
                    <SpellChecker Enabled="true" Culture="en-US">
                        <OptionsSpelling IgnoreEmails="true" IgnoreUri="true" />
                    </SpellChecker>
                </Settings>
                <SettingsDocumentSelector UploadSettings-Enabled="true" UploadSettings-AdvancedModeSettings-TemporaryFolder="~\Files"></SettingsDocumentSelector>
            </dx:ASPxRichEdit>
            <asp:Label ID="lblError" runat="server" Visible="false"></asp:Label>
        </div>
    </form>
</body>
</html>
