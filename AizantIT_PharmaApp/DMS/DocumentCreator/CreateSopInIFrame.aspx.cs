﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using DevExpress.XtraRichEdit;
namespace AizantIT_PharmaApp.DMS.DocumentCreator
{
    public partial class CreateSopInIFrame : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Session["CreateFileUpload"] != null)
                    {
                        if (Request.QueryString.Count > 0)
                        {
                            ToCreateSopDocumentSession(Request.QueryString[0], Request.QueryString[1]);
                        }
                    }
                    else
                    {
                        if (Request.QueryString.Count > 0)
                        {
                            ToCreateSopDocument(Request.QueryString[0], Request.QueryString[1]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("CSIF_M1:" + strline + "  " + strMsg);
                string strError = "CSIF_M1:" + strline + "  " + "Page loading failed";
                lblError.Visible = true;
                lblError.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        private void ToCreateSopDocument(string vid, string tvisible)
        {
            try
            {
                if (tvisible == "1")
                {
                    if (vid != "0")
                    {
                        DataTable DMS_template = DMS_Bal.DMS_GetTempalteContent(vid);
                        if (DMS_template.Rows[0]["FileType"].ToString() == ".doc")
                        {
                            ASPxRichEdit1.Open(
                            Guid.NewGuid().ToString(),
                            DevExpress.XtraRichEdit.DocumentFormat.Doc,
                            () =>
                            {
                                byte[] docBytes = (byte[])DMS_template.Rows[0]["Content"];
                                return new MemoryStream(docBytes);
                            }
                            );
                        }
                        if (DMS_template.Rows[0]["FileType"].ToString() == ".docx")
                        {
                            ASPxRichEdit1.Open(
                            Guid.NewGuid().ToString(),
                            DevExpress.XtraRichEdit.DocumentFormat.OpenXml,
                            () =>
                            {
                                byte[] docBytes = (byte[])DMS_template.Rows[0]["Content"];
                                return new MemoryStream(docBytes);
                            }
                            );
                        }
                    }
                }
                else
                {
                    DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(vid);
                    if (DMS_DT.Rows[0]["FileType"].ToString() != "pdf")
                    {
                        if (DMS_DT.Rows[0]["Content"] != DBNull.Value)
                        {
                            ASPxRichEdit1.Open(
                            Guid.NewGuid().ToString(),
                            DevExpress.XtraRichEdit.DocumentFormat.Rtf,
                            () =>
                            {
                                byte[] docBytes = (byte[])DMS_DT.Rows[0]["Content"];
                                return new MemoryStream(docBytes);
                            }
                            );
                        }
                    }
                }
                Session["DocData"] = ASPxRichEdit1.SaveCopy(DevExpress.XtraRichEdit.DocumentFormat.Rtf);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("CSIF_M2:" + strline + "  " + strMsg);
                string strError = "CSIF_M2:" + strline + "  " + "Create SOP loading failed";
                lblError.Visible = true;
                lblError.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        private void ToCreateSopDocumentSession(string vid, string tvisible)
        {
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(vid);
                if (Session["CreateFileUploadExtension"].ToString() != "pdf")
                {
                    if (Session["CreateFileUploadExtension"].ToString() == ".doc")
                    {
                        ASPxRichEdit1.Open(
                        Guid.NewGuid().ToString(),
                        DevExpress.XtraRichEdit.DocumentFormat.Doc,
                        () =>
                        {
                            byte[] docBytes = (byte[])Session["CreateFileUpload"];
                            return new MemoryStream(docBytes);
                        }
                        );
                    }
                    if (Session["CreateFileUploadExtension"].ToString() == ".docx")
                    {
                        ASPxRichEdit1.Open(
                        Guid.NewGuid().ToString(),
                        DevExpress.XtraRichEdit.DocumentFormat.OpenXml,
                        () =>
                        {
                            byte[] docBytes = (byte[])Session["CreateFileUpload"];
                            return new MemoryStream(docBytes);
                        }
                        );
                    }
                }
                Session["DocData"] = ASPxRichEdit1.SaveCopy(DevExpress.XtraRichEdit.DocumentFormat.Rtf);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("CSIF_M3:" + strline + "  " + strMsg);
                string strError = "CSIF_M3:" + strline + "  " + "Create SOP Session loading failed";
                lblError.Visible = true;
                lblError.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}