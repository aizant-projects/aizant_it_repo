﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using AizantIT_PharmaApp.Common;
using UMS_BusinessLayer;
namespace AizantIT_PharmaApp.DMS.DocumentCreator
{
    public partial class Create_Document : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        ReviewObjects reviewObjects = new ReviewObjects();
        UMS_BAL objUMS_BAL;
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=11").Length > 0)//11-Author
                            {
                                if (Request.QueryString["Notification_ID"] != null)
                                {
                                    objUMS_BAL = new UMS_BAL();
                                    int[] empIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, empIDs); //2 represents NotifyEmp had visited the page through notification link.
                                    DataTable dt = DMS_Bal.DMS_NotificationDocVersionID(Convert.ToInt32(Request.QueryString["Notification_ID"]));
                                    if (dt.Rows[0]["DocumentStatus"].ToString() == "0")
                                    {
                                        hdnNewDocTab.Value = "NewTab";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tab1", " TabClick();", true);
                                        Response.Redirect("~/DMS/DocumentCreator/Document_Details.aspx?DocumentID=" + dt.Rows[0][0].ToString() + "&tab=1" + "&PID=" + dt.Rows[0]["ProcessType"], false);
                                    }
                                    if (dt.Rows[0]["DocumentStatus"].ToString() == "1R" || dt.Rows[0]["DocumentStatus"].ToString() == "1A" || dt.Rows[0]["DocumentStatus"].ToString() == "1Au")
                                    {
                                        hdnNewDocTab.Value = "RevertedTab";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tab1", " TabClick();", true);
                                        Response.Redirect("~/DMS/DocumentCreator/Document_Details.aspx?DocumentID=" + dt.Rows[0][0].ToString() + "&tab=2" + "&PID=" + dt.Rows[0]["ProcessType"], false);
                                    }
                                }
                                if (Request.QueryString["tab"] != null)
                                {
                                    if (Request.QueryString["tab"] == "1")
                                    {
                                        hdnNewDocTab.Value = "NewTab";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tab1", " TabClick();", true);
                                        if(Request.QueryString["rtp"] != null)
                                        {
                                            if (Request.QueryString["rtp"] == "1")
                                            {
                                                newDocTab.InnerText = "New ";
                                            }
                                            else if (Request.QueryString["rtp"] == "2")
                                            {
                                                newDocTab.InnerText = "Revision ";
                                            }
                                            else if (Request.QueryString["rtp"] == "5")
                                            {
                                                newDocTab.InnerText = "Renew ";
                                            }
                                            else
                                            {
                                                revertDocTab.InnerText = "New ";
                                            }
                                        }
                                    }
                                    else if (Request.QueryString["tab"] == "2")
                                    {
                                        hdnNewDocTab.Value = "RevertedTab";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tab1", " TabClick();", true);
                                        if (Request.QueryString["rtp"] != null)
                                        {
                                            if (Request.QueryString["rtp"] == "1")
                                            {
                                                revertDocTab.InnerText = "New ";
                                            }
                                            else if (Request.QueryString["rtp"] == "2")
                                            {
                                                revertDocTab.InnerText = "Revision ";
                                            }
                                            else if (Request.QueryString["rtp"] == "5")
                                            {
                                                revertDocTab.InnerText = "Renew ";
                                            }
                                            else
                                            {
                                                revertDocTab.InnerText = "Reverted ";
                                            }
                                        }
                                    }
                                    InitializeThePage();
                                }
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("CD_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "CD_M1:" + strline + "  " + "Document Load failed.", "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        protected void btnCommentsHistoryView_Click(object sender, EventArgs e)
        {
            btnCommentsHistoryView.Enabled = false;
            try
            {
                DataTable DMS_DT = DMS_BalDM.DMS_GetComments(hdfPKID.Value);
                gv_CommentHistory.DataSource = DMS_DT;
                ViewState["HistoryDataTable"] = DMS_DT;
                gv_CommentHistory.DataBind();
                if (DMS_DT.Rows.Count > 0)
                {
                    span4.Attributes.Add("title", DMS_DT.Rows[0]["DocumentTitle"].ToString());
                    if (DMS_DT.Rows[0]["DocumentTitle"].ToString().Length > 60)
                    {
                        span4.InnerHtml = DMS_DT.Rows[0]["DocumentTitle"].ToString().Substring(0, 60) + "...";
                    }
                    else
                    {
                        span4.InnerHtml = DMS_DT.Rows[0]["DocumentTitle"].ToString();
                    }
                }
                else
                {
                    span4.InnerText = "Comment History";
                }
                upcomment.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myCommentHistory').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("CD_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "CD_M2:" + strline + "  " + "History viewing failed.", "error");
            }
            finally
            {
                btnCommentsHistoryView.Enabled = true;
            }
        }
        private void InitializeThePage()
        {
            try
            {
                if (Request.QueryString["rtp"] != null)
                {
                    if (Request.QueryString["rtp"] == "1")
                    {
                        hdnRequestType.Value = "1";
                    }
                    else if (Request.QueryString["rtp"] == "2")
                    {
                        hdnRequestType.Value = "2";
                    }
                    else if (Request.QueryString["rtp"] == "5")
                    {
                        hdnRequestType.Value = "5";
                    }
                    else
                    {
                        hdnRequestType.Value = "0";
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("CD_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "CD_M3:" + strline + "  " + "card view details loading failed.", "error");
            }
        }
        protected void gv_CommentHistory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string _fullcomments = (e.Row.Cells[5].FindControl("txt_Comments") as TextBox).Text;
                    LinkButton _lnkmorecomments = (e.Row.Cells[5].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (e.Row.Cells[5].FindControl("lbcommentsless") as LinkButton);
                    if (_fullcomments.Length > 70)
                    {
                        (e.Row.Cells[5].FindControl("lbl_shortComments") as Label).Visible = true;
                        TextBox FullTbx = (e.Row.Cells[5].FindControl("txt_Comments") as TextBox);
                        (e.Row.Cells[5].FindControl("lbl_shortComments") as Label).Text = _fullcomments.Substring(0, 70) + "..";
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = true;

                        if (_fullcomments.Length > 250 && _fullcomments.Length <= 500)
                        {
                            FullTbx.Rows = 6;
                        }
                        else if (_fullcomments.Length > 500 && _fullcomments.Length <= 1000)
                        {
                            FullTbx.Rows = 9;
                        }
                        else if (_fullcomments.Length > 1000 && _fullcomments.Length <= 1500)
                        {
                            FullTbx.Rows = 12;
                        }
                        else if (_fullcomments.Length > 1500 && _fullcomments.Length <= 2000)
                        {
                            FullTbx.Rows = 15;
                        }
                        else if (_fullcomments.Length > 2000)
                        {
                            FullTbx.Rows = 20;
                        }
                        FullTbx.Visible = false;
                    }
                    else
                    {
                        (e.Row.Cells[5].FindControl("lbl_shortComments") as Label).Visible = true;
                        (e.Row.Cells[5].FindControl("txt_Comments") as TextBox).Visible = false;
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("CD_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "CD_M4:" + strline + "  " + "Comment History row databound failed.", "error");
            }
        }
        protected void gv_CommentHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                
                if (e.CommandName == "SM")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    LinkButton _lnkmorecomments = (row.Cells[5].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (row.Cells[5].FindControl("lbcommentsless") as LinkButton);
                    (row.Cells[5].FindControl("lbl_shortComments") as Label).Visible = false;
                    (row.Cells[5].FindControl("txt_Comments") as TextBox).Visible = true;
                    _lnklesscomments.Visible = true;
                    _lnkmorecomments.Visible = false;
                }
                else if (e.CommandName == "SL")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    LinkButton _lnkmorecomments = (row.Cells[5].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (row.Cells[5].FindControl("lbcommentsless") as LinkButton);
                    (row.Cells[5].FindControl("lbl_shortComments") as Label).Visible = true;
                    (row.Cells[5].FindControl("txt_Comments") as TextBox).Visible = false;
                    _lnklesscomments.Visible = false;
                    _lnkmorecomments.Visible = true;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("CD_M5:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "CD_M5:" + strline + "  " + "Comment History row command failed.", "error");
            }
        }
        protected void gv_CommentHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_CommentHistory.PageIndex = e.NewPageIndex;
                gv_CommentHistory.DataSource = (DataTable)(ViewState["HistoryDataTable"]);
                gv_CommentHistory.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("CD_M6:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "CD_M6:" + strline + "  " + "grid view page index changing failed.", "error");
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }       
    }
}