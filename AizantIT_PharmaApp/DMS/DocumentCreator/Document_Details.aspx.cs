﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using System.Data;
using System.IO;
using UMS_BusinessLayer;
using System.Text.RegularExpressions;

namespace AizantIT_PharmaApp.DMS.DocumentCreator
{
    public partial class Document_Details : System.Web.UI.Page
    {
        private Word2PDFConverter.RemoteConverter converter;
        private string storefolder = HttpContext.Current.Server.MapPath(@"~/Scripts/pdf.js/web/");
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        ReviewObjects reviewObjects = new ReviewObjects();
        DocObjects docObjects = new DocObjects();
        DocObjects docObjects1 = new DocObjects();
        DocUploadBO objectDocUploadBO;
        UMS_BAL objUMS_BAL;
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            ElectronicSign.buttonClick += new EventHandler(ElectronicSign_buttonClick);
            ConfirmCustomAlert.buttonClick += new EventHandler(ConfirmCustomAlert_buttonClick);

            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    converter = (Word2PDFConverter.RemoteConverter)Activator.GetObject(typeof(Word2PDFConverter.RemoteConverter), "http://localhost:" + System.Configuration.ConfigurationManager.AppSettings["Word2PdfPort"].ToString() + "/RemoteConverter");
                    if (!IsPostBack)
                    {
                        Session["vwdocObjects"] = null;
                        Session["vwreviewObjects"] = null;
                        Session["vwdocObjects1"] = null;
                        ViewState["PDFPhysicalName"] = "";
                        System.Data.DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        System.Data.DataTable dtTemp = new System.Data.DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        ViewState["TargPath"] = "";
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=11").Length > 0)//11-Author
                            {
                                if (Request.QueryString["Notification_ID"] != null)
                                {
                                    objUMS_BAL = new UMS_BAL();
                                    int[] empIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, empIDs); //2 represents NotifyEmp had visited the page through notification link.
                                }
                                InitializeThePage();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M1:" + strline + "  " + "Document Load failed.", "error");
            }
        }
        protected void ElectronicSign_buttonClick(object sender, EventArgs e)
        {
            try
            {
                bool b = ElectronicSign.IsPasswordValid;
                if (b == true)
                {
                    if (hdnAction.Value == "Submit" || ViewState["Action"].ToString() == "Submit")
                    {
                        Submit();
                    }
                }
                if (b == false)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Password is Incorrect!", "error");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M2:" + strline + "  " + "Electronic Signature validation failed.", "error");
            }
        }
        protected void ConfirmCustomAlert_buttonClick(object sender, EventArgs e)
        {
            try
            {
                if (hdfConfirm.Value == "Submit")
                {
                    ViewState["Action"] = "Submit";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop4", "openUC_ElectronicSign();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M3:" + strline + "  " + "Document Save/Document Submit Electronic Signature loading failed.", "error");
            }
        }
        public void DMS_GetDepartmentList()
        {
            try
            {
                System.Data.DataTable DMS_Dt = DMS_BalDM.DMS_GetDepartmentList();
                ddl_Dept.DataValueField = DMS_Dt.Columns["DeptID"].ToString();
                ddl_Dept.DataTextField = DMS_Dt.Columns["DepartmentName"].ToString();
                ddl_Dept.DataSource = DMS_Dt;
                ddl_Dept.DataBind();
                ddl_Dept.Items.Insert(0, new ListItem("Select Department", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M4:" + strline + "  " + "Department Load Failed.", "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        private void InitializeThePage()
        {
            try
            {
                DMS_GetDepartmentList();
                if (Request.QueryString["DocumentID"] != null && Request.QueryString["PID"] != null)
                {
                    int DocumentID = Convert.ToInt32(Request.QueryString["DocumentID"]);
                    int ProcessID = Convert.ToInt32(Request.QueryString["PID"]);
                    RetriveDocumentDetails(DocumentID, ProcessID);
                    RetrieveDocumentContent(DocumentID, ProcessID);
                    RetrieveRefferalContent(DocumentID);
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M5:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M5:" + strline + "  " + "Page Initialization failed.", "error");
            }
        }
        private void RetriveDocumentDetails(int DocumentID, int ProcessID)
        {
            try
            {
                Session["CreateFileUpload"] = null;
                Session["CreateFileUploadExtension"] = null;
                hdfPKID.Value = DocumentID.ToString();
                hdfPID.Value = ProcessID.ToString();
                System.Data.DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                if (DMS_DT.Rows.Count > 0)
                {
                    txt_requestType.Text = DMS_DT.Rows[0]["RequestType"].ToString();
                    txt_documentType.Text = DMS_DT.Rows[0]["DocumentType"].ToString();
                    txt_Department.Text = DMS_DT.Rows[0]["Department"].ToString();
                    txt_DocumentNumber.Text = DMS_DT.Rows[0]["DocumentNumber"].ToString();
                    txt_DocumentName.Text = DMS_DT.Rows[0]["DocumentName"].ToString();
                    if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "1")
                    {
                        lblComments.InnerHtml = "Purpose of Initiation";
                        txt_Comments.Text = DMS_DT.Rows[0]["CreatePurpose"].ToString();
                        txt_VersionID.Text = "00";
                    }
                    else if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "2")
                    {
                        lblComments.InnerHtml = "Reason for Revision";
                        txt_Comments.Text = DMS_DT.Rows[0]["CreatePurpose"].ToString();
                        txt_VersionID.Text = DMS_DT.Rows[0]["versionNumber"].ToString();
                    }
                    else if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "5")
                    {
                        lblComments.InnerHtml = "Reason for Renew";
                        txt_Comments.Text = DMS_DT.Rows[0]["WithdrawPurpose"].ToString();
                        txt_VersionID.Text = DMS_DT.Rows[0]["versionNumber"].ToString();
                    }
                    if (Convert.ToBoolean(DMS_DT.Rows[0]["isPublic"].ToString()) == true)
                    {
                        RadioButtonList1.SelectedValue = "yes";
                    }
                    else
                    {
                        RadioButtonList1.SelectedValue = "no";
                    }
                    RadioButtonList1.Enabled = false;
                    txt_Author.Text = DMS_DT.Rows[0]["CreatorID"].ToString();
                    txt_Authorizer.Text = DMS_DT.Rows[0]["AuthorizedBy"].ToString();
                    if (txt_Authorizer.Text == "")
                    {
                        div1.Visible = false;
                    }
                    else
                    {
                        div1.Visible = true;
                    }
                    txt_Controller.Text = DMS_DT.Rows[0]["ControlledBy"].ToString();
                    if (txt_Controller.Text == "")
                    {
                        divcontroller.Visible = false;
                    }
                    else
                    {
                        divcontroller.Visible = true;
                    }
                    System.Data.DataTable aprrove_list = DMS_Bal.DMS_GetApproverReviewerSelectedList(hdfPKID.Value, 5);
                    ViewState["ApproverData"] = aprrove_list;
                    gvApprover.DataSource = aprrove_list;
                    gvApprover.DataBind();
                    UpMyDocFiles.Update();
                    if (Request.QueryString["tab"] == "1")
                    {
                        if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "1")
                        {
                            liCreate.Visible = true;
                            liModify.Visible = false;
                            liReferral.Visible = true;
                            btn_SubmittoReviewer.Text = "Submit";
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "hide1", "$('.Refferaltab').hide();", true);
                            lbl_Upload.Visible = true;
                        }
                        else if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "2")
                        {
                            liCreate.Visible = false;
                            liModify.Visible = true;
                            liReferral.Visible = true;
                            btn_SubmittoReviewer.Text = "Update";
                            if (DMS_DT.Rows[0]["ActiveRequestTypeID"].ToString() != "4" && DMS_DT.Rows[0]["ActiveRequestTypeID"].ToString() != "5")
                            {
                                btn_Download.Visible = true;
                            }
                            lbl_Upload.Visible = true;
                        }
                        else if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "5")
                        {
                            liCreate.Visible = false;
                            liModify.Visible = true;
                            liReferral.Visible = false;
                            btn_SubmittoReviewer.Text = "Update";
                            lbl_Upload.Visible = false;
                            //if (DMS_DT.Rows[0]["ActiveRequestTypeID"].ToString() != "4")
                            //{
                            //    btn_Download.Visible = true;
                            //}
                        }
                        //if (DMS_DT.Rows[0]["Content"] == DBNull.Value)
                        //{
                        //lbl_Upload.Visible = true;
                        //}
                        //else if (DMS_DT.Rows[0]["FileType"].ToString() == "pdf" || DMS_DT.Rows[0]["ActiveRequestTypeID"].ToString() == "4")
                        //{
                        //    lbl_Upload.Visible = true;
                        //}
                        //else
                        //{
                        //    lbl_Upload.Visible = true;
                        //}
                        hdnFieldFrom.Value = "1";
                        resetReferrels();
                    }
                    else if (Request.QueryString["tab"] == "2")
                    {
                        liCreate.Visible = false;
                        liModify.Visible = true;
                        if(DMS_DT.Rows[0]["RequestTypeID"].ToString() == "5")
                        {
                            liReferral.Visible = false;
                        }
                        else
                        {
                            liReferral.Visible = true;
                        }
                        hdnFieldFrom.Value = "2";
                        btn_SubmittoReviewer.Text = "Update";
                        hdfCrChange.Value = "1";
                        hdfDocStatus.Value = DMS_DT.Rows[0]["Status"].ToString();
                        if (DMS_DT.Rows[0]["Status"].ToString() == "1R")
                        {
                            hdfFromRoleID.Value = "10";
                        }
                        else if (DMS_DT.Rows[0]["Status"].ToString() == "1A")
                        {
                            hdfFromRoleID.Value = "9";
                        }
                        else if (DMS_DT.Rows[0]["Status"].ToString() == "1Au")
                        {
                            hdfFromRoleID.Value = "13";
                        }
                        divcomments.Visible = true;
                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show1", "getEveryoneComments();", true);
                        btn_Download.Visible = true;
                    }
                    Btn_ShowOriginal.Visible = false;
                    //if (ProcessID == 1)
                    //{
                    Btn_ShowModified.Visible = false;
                    if (Request.QueryString["tab"] == "2")
                    {
                        DataTable dt = DMS_GetDocRevertedByList();
                        if (dt.Rows.Count == 0)
                        {
                            ddl_RevertedBy.Visible = false;
                        }
                        else
                        {
                            ddl_RevertedBy.Visible = true;
                            HelpClass.custAlertMsg(this, this.GetType(), "Please Download Reverted Document to view Track Changes!", "info");
                        }
                    }
                    else
                    {
                        ddl_RevertedBy.Visible = false;
                    }
                    //}
                    //else
                    //{
                    //    Btn_ShowModified.Visible = false;
                    //    ddl_RevertedBy.Visible = false;
                    //}
                    hdfRUpload.Value = DMS_DT.Rows[0]["RUploadCount"].ToString();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M6:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M6:" + strline + "  " + "Retrieve Document Details failed.", "error");
            }
        }
        private void RetrieveDocumentContent(int DocumentID, int ProcessID)
        {
            try
            {
                hdfPKID.Value = DocumentID.ToString();
                System.Data.DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "1")
                {
                    if (DMS_DT.Rows[0]["FileName"] != DBNull.Value)
                    {
                        if (Request.QueryString["tab"] == "1")
                        {
                            hdfViewType.Value = "0";
                            hdfViewEmpID.Value = "0";
                            hdfViewRoleID.Value = "0";
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view1", "ViewDocument('#divPDF_Viewer');", true);
                        }
                        else if (Request.QueryString["tab"] == "2")
                        {
                            //if (ProcessID == 1)
                            //{          
                            hdfViewType.Value = "0";
                            hdfViewEmpID.Value = "0";
                            hdfViewRoleID.Value = "11";//author
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view2", "ViewDocument('#divPDF_Viewer');", true);          
                            //}
                            //else
                            //{
                            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view3", "ViewDocument('#divPDF_Viewer');", true);
                            //}
                        }
                    }
                    else
                    {
                        btn_SubmittoReviewer.Visible = false;
                        HelpClass.custAlertMsg(this, this.GetType(), "Please Upload Original Word Document To Proceed Further!", "info");
                    }
                }
                else if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "2")
                {
                    if (DMS_DT.Rows[0]["FileName"] != DBNull.Value)
                    {
                        if (Request.QueryString["tab"] == "1")
                        {
                            docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                            docObjects.RoleID = 0;
                            docObjects.EmpID = 0;
                            docObjects.FileType1 = ".pdf";
                            docObjects.Mode = 0;
                            System.Data.DataTable dt = DMS_Bal.DMS_GetDocumentLocationList(docObjects);
                            if (dt.Rows[0][0].ToString() != "0")
                            {
                                hdfViewType.Value = "0";
                                hdfViewEmpID.Value = "0";
                                hdfViewRoleID.Value = "0";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view4", "ViewDocument('#divPDF_Viewer');", true);
                            }
                            else
                            {
                                hdfViewType.Value = "1";
                                hdfViewEmpID.Value = "0";
                                hdfViewRoleID.Value = "0";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view5", "ViewDocument('#divPDF_Viewer');", true);
                                btn_SubmittoReviewer.Visible = false;
                                HelpClass.custAlertMsg(this, this.GetType(), "Please Upload Original Word Document To Proceed Further!", "info");
                            }
                        }
                        else if (Request.QueryString["tab"] == "2")
                        {
                            //if (ProcessID == 1)
                            //{
                            hdfViewType.Value = "0";
                            hdfViewEmpID.Value = "0";
                            hdfViewRoleID.Value = "11";//author
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view6", "ViewDocument('#divPDF_Viewer');", true);
                            HelpClass.custAlertMsg(this, this.GetType(), "Please Download Document to Track Changes!", "info");
                            //}
                            //else
                            //{
                            //    hdfViewType.Value = "0";
                            //    hdfViewEmpID.Value = "0";
                            //    hdfViewRoleID.Value = "0";
                            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view7", "ViewDocument('#divPDF_Viewer');", true);
                            //}
                        }
                    }
                    else
                    {
                        hdfViewType.Value = "1";
                        hdfViewEmpID.Value = "0";
                        hdfViewRoleID.Value = "0";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view8", "ViewDocument('#divPDF_Viewer');", true);
                        btn_SubmittoReviewer.Visible = false;
                        HelpClass.custAlertMsg(this, this.GetType(), "Please Upload Original Word Document To Proceed Further!", "info");
                    }
                }
                else if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "5")
                {
                    if (DMS_DT.Rows[0]["FileName"] != DBNull.Value)
                    {
                        if (Request.QueryString["tab"] == "1")
                        {
                            docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                            docObjects.RoleID = 0;
                            docObjects.EmpID = 0;
                            docObjects.FileType1 = ".pdf";
                            docObjects.Mode = 0;
                            System.Data.DataTable dt = DMS_Bal.DMS_GetDocumentLocationList(docObjects);
                            if (dt.Rows[0][0].ToString() != "0")
                            {
                                hdfViewType.Value = "0";
                                hdfViewEmpID.Value = "0";
                                hdfViewRoleID.Value = "0";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view4", "ViewDocument('#divPDF_Viewer');", true);
                            }
                            else
                            {
                                hdfViewType.Value = "1";
                                hdfViewEmpID.Value = "0";
                                hdfViewRoleID.Value = "0";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view5", "ViewDocument('#divPDF_Viewer');", true);
                                btn_SubmittoReviewer.Visible = false;                        
                                //HelpClass.custAlertMsg(this, this.GetType(), "Please Upload Original Word Document To Proceed Further!", "info");
                            }
                        }
                        else if (Request.QueryString["tab"] == "2")
                        {
                            //if (ProcessID == 1)
                            //{
                            hdfViewType.Value = "5";
                            hdfViewEmpID.Value = "0";
                            hdfViewRoleID.Value = "11";//author
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view6", "ViewRenewDocument('#divPDF_Viewer');", true);
                            btn_Download.Visible = false;
                            //HelpClass.custAlertMsg(this, this.GetType(), "Please Download Document to Track Changes!", "info");
                            //}
                            //else
                            //{
                            //    hdfViewType.Value = "0";
                            //    hdfViewEmpID.Value = "0";
                            //    hdfViewRoleID.Value = "0";
                            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view7", "ViewDocument('#divPDF_Viewer');", true);
                            //}
                        }
                    }
                    else
                    {
                        hdfViewType.Value = "1";
                        hdfViewEmpID.Value = "0";
                        hdfViewRoleID.Value = "0";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view8", "ViewDocument('#divPDF_Viewer');", true);
                        btn_SubmittoReviewer.Visible = false;
                        HelpClass.custAlertMsg(this, this.GetType(), "Please Upload Original Word Document To Proceed Further!", "info");
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M7:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M7:" + strline + "  " + "Retrieve Document Content failed.", "error");
            }
        }
        private void RetrieveRefferalContent(int DocumentID)
        {
            try
            {
                hdfPKID.Value = DocumentID.ToString();
                string DMS_DocumentID = hdfPKID.Value;
                txtLinkReferrences.InnerText = DMS_Bal.DMS_GetLinkReferences(Convert.ToInt32(DMS_DocumentID));
                System.Data.DataTable dtInternalDocs = DMS_Bal.DMS_GetReferalInternalDocs(Convert.ToInt32(DMS_DocumentID));
                Session["dtAssignedDocuments"] = dtInternalDocs;
                gvAssignedDocuments.DataSource = dtInternalDocs;
                gvAssignedDocuments.DataBind();
                System.Data.DataTable dtExternalDocs = DMS_Bal.DMS_GetReferalExternalFiles(Convert.ToInt32(DMS_DocumentID));
                gvExternalFiles.DataSource = dtExternalDocs;
                gvExternalFiles.DataBind();
                ViewState["dtExternalDocuments"] = dtExternalDocs;
                TempExternalFileBO objTemp = new TempExternalFileBO();
                objTemp.CrudType = 4;
                objTemp.Ref_FileID = 0;
                objTemp.FileName = "";
                objTemp.FileExtension = "";
                objTemp.FileContent = new byte[0];
                objTemp.DocExternal_VersionID = Convert.ToInt32(DMS_DocumentID);
                System.Data.DataTable dt = DMS_Bal.DMS_TempCrudExternalFiles(objTemp);
                GetgvExternalFiles();
                getDocumentList(true);
                hdnGrdiReferesh.Value = "";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowRefferal();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M8:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M8:" + strline + "  " + "Add Referrals failed.", "error");
            }
        }
        public void Submit()
        {
            try
            {
                Session.Remove("word");
                System.Data.DataTable dt = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                objectDocUploadBO = new DocUploadBO();
                if (dt.Rows[0]["Status"].ToString() != "RJ")
                {
                    if (dt.Rows[0]["Status"].ToString() == "0")
                    {
                        if (Session["vwdocObjects"] != null)
                        {
                            objectDocUploadBO.docLocation = (DocObjects)(Session["vwdocObjects"]);
                            // int DOC_Ins = DMS_Bal.DMS_InsertDocumentLocation((DocObjects)(Session["vwdocObjects"]));
                        }
                        if (Session["vwreviewObjects"] != null)
                        {
                            objectDocUploadBO.docContent = (ReviewObjects)(Session["vwreviewObjects"]);
                            // int DMS_Status = DMS_Bal.DMS_UpdateContentByID((ReviewObjects)(Session["vwreviewObjects"]));
                        }
                        if (Session["vwdocObjects1"] != null)
                        {
                            objectDocUploadBO.docComments = (DocObjects)(Session["vwdocObjects1"]);
                            //DMS_Bal.DMS_DocComments((DocObjects)(Session["vwdocObjects1"]));
                        }
                        if (Session["vwdocObjects"] != null && Session["vwdocObjects1"] != null)
                        {
                            string DMS_DocumentID = hdfPKID.Value;
                            reviewObjects.Id = Convert.ToInt32(DMS_DocumentID);
                            reviewObjects.Creator = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            reviewObjects.Comments = Regex.Replace(txt_RevertComments.Value.Trim(), @"\s+", " ");
                            objectDocUploadBO.docReviwer = reviewObjects;
                            int DMS_Ins = DMS_Bal.DMS_DocumentToReviewer(objectDocUploadBO);
                            //int DMS_Ins = DMS_Bal.DMS_DocumentToReviewer(reviewObjects);
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "HidePopup();", true);
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document with Document Number <b>" + txt_DocumentNumber.Text + "</b> Submitted successfully.", "success", "ReloadCreatorPage()");
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "custAlertMsg(' <br/> Document Submission failed, due to incorrect upload.','error');", true);
                            return;
                        }
                    }
                    else if (dt.Rows[0]["Status"].ToString() == "1R" || dt.Rows[0]["Status"].ToString() == "1A" || dt.Rows[0]["Status"].ToString() == "1Au")
                    {
                        if (Session["vwdocObjects"] != null)
                        {
                            objectDocUploadBO.docLocation = (DocObjects)(Session["vwdocObjects"]);
                            //int DOC_Ins = DMS_Bal.DMS_InsertDocumentLocation((DocObjects)(Session["vwdocObjects"]));
                        }
                        if (Session["vwreviewObjects"] != null)
                        {
                            objectDocUploadBO.docContent = (ReviewObjects)(Session["vwreviewObjects"]);
                            //int DMS_Status = DMS_Bal.DMS_UpdateContentByID((ReviewObjects)(Session["vwreviewObjects"]));
                        }
                        if (Session["vwdocObjects1"] != null)
                        {
                            objectDocUploadBO.docComments = (DocObjects)(Session["vwdocObjects1"]);
                            //DMS_Bal.DMS_DocComments((DocObjects)(Session["vwdocObjects1"]));
                        }
                        string DMS_DocumentID = hdfPKID.Value;
                        reviewObjects.Id = Convert.ToInt32(DMS_DocumentID);
                        reviewObjects.Creator = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        reviewObjects.Comments = Regex.Replace(txt_RevertComments.Value.Trim(), @"\s+", " ");
                        objectDocUploadBO.docReviwer = reviewObjects;
                        int DMS_Ins = DMS_Bal.DMS_DocumentToReviewer(objectDocUploadBO);
                        //int DMS_Ins = DMS_Bal.DMS_DocumentToReviewer(reviewObjects);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "HidePopup();", true);
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document with Document Number <b>" + txt_DocumentNumber.Text + "</b> Updated successfully.", "success", "ReloadCreatorPage()");
                    }
                }
                else
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document with Document Number <b>" + txt_DocumentNumber.Text + "</b> has been already Rejected.", "info", "ReloadCreatorPage()");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M9:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M9:" + strline + "  " + "Document Submit failed.", "error");
            }
        }
        protected void Btn_Upload_Click(object sender, EventArgs e)
        {
            Btn_Upload.Enabled = false;
            try
            {
                Session["vwdocObjects"] = null;
                Session["vwreviewObjects"] = null;
                Session["vwdocObjects1"] = null;
                byte[] Filebyte = null;

                if (file_word.HasFile)
                {
                    System.Data.DataTable dt = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                    Stream fs = file_word.PostedFile.InputStream;
                    BinaryReader br = new BinaryReader(fs);
                    Filebyte = br.ReadBytes((Int32)fs.Length);
                    
                    string ext = System.IO.Path.GetExtension(file_word.FileName).ToLower();
                    string mime = MimeType.GetMimeType(Filebyte, file_word.FileName);

                    string[] filemimes1;
                    string[] filemimes;
                    if (dt.Rows[0]["RequestTypeID"].ToString() != "5")
                    {
                         filemimes1 = new string[] { "application/x-msdownload", "application/unknown", "application/octet-stream", "image/png", "image/gif", "application/octet-stream" };
                         filemimes = new string[] { "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/x-zip-compressed" };
                        if (filemimes1.Contains(mime))
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' Please upload .doc or .docx files only ! <br/> The uploaded file is not valid or corrupted.','warning');", true);
                            if (dt.Rows[0]["RequestTypeID"].ToString() == "2")
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "viewpop", "ViewDocument('#divPDF_Viewer');", true);
                            }
                            return;
                        }
                        if (!filemimes.Contains(mime))
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' Please upload .doc or .docx files only ! <br/> The uploaded file is not valid or corrupted.','warning');", true);
                            if (dt.Rows[0]["RequestTypeID"].ToString() == "2")
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "viewpop", "ViewDocument('#divPDF_Viewer');", true);
                            }
                            return;
                        }
                    }
                    else
                    {
                        filemimes1 = new string[] {" application / msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/x-zip-compressed","application/x-msdownload", "application/unknown", "application/octet-stream", "image/png", "image/gif" };
                        filemimes = new string[] { "application/pdf" };
                        if (filemimes1.Contains(mime))
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' Please upload .pdf files only ! <br/> The uploaded file is not valid or corrupted.','warning');", true);
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "viewpop", "ViewDocument('#divPDF_Viewer');", true);
                            return;
                        }
                        if (!filemimes.Contains(mime))
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' Please upload .pdf files only ! <br/> The uploaded file is not valid or corrupted.','warning');", true);
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "viewpop", "ViewDocument('#divPDF_Viewer');", true);
                            return;
                        }
                    }
                    
                    
                    if (Filebyte.Length < 1000)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' Not able to read content,document upload failed','warning');", true);
                        return;
                    }
                    if (dt.Rows[0]["RequestTypeID"].ToString() != "5")
                    {
                        if (ext == ".doc" || ext == ".docx")
                        {
                            string strWordMainFolderPath = DynamicFolder.CreateTempDocumentFolder(dt.Rows[0]["DocumentNumber"].ToString(), dt.Rows[0]["VersionNumber"].ToString());
                            string File1Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ext;
                            string strSourcePath = (strWordMainFolderPath + "\\" + File1Path);
                            string File2Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                            string strTargPath = (storefolder + File2Path);

                            //Save original file
                            file_word.SaveAs(strSourcePath);

                            //call the converter method
                            bool isValid = converter.convert(strSourcePath, strTargPath);
                            if (isValid)
                            {
                                docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                                docObjects.RoleID = 11;//author
                                docObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                docObjects.FilePath1 = string.Format(@"~/DMSTempDocuments/{0}/{1}/", dt.Rows[0]["DocumentNumber"].ToString(), dt.Rows[0]["VersionNumber"].ToString()) + File1Path;
                                docObjects.FilePath2 = string.Format(@"~/Scripts/pdf.js/web/") + File2Path;
                                docObjects.isTemp = 0;
                                docObjects.FileType1 = ext;
                                docObjects.FileType2 = ".pdf";
                                docObjects.FileName1 = File1Path;
                                docObjects.FileName2 = File2Path;
                                ViewState["PDFPhysicalName"] = File2Path;
                                hdfFileName.Value = ViewState["PDFPhysicalName"].ToString();
                                Session["vwdocObjects"] = docObjects;
                                reviewObjects.Id = Convert.ToInt32(hdfPKID.Value);
                                //reviewObjects.Content = Filebyte;
                                reviewObjects.Ext = ext;
                                reviewObjects.TemplateName = file_word.FileName;
                                Session["vwreviewObjects"] = reviewObjects;

                                btn_SubmittoReviewer.Visible = true;
                                btn_Download.Visible = false;

                                docObjects1.Version = Convert.ToInt32(hdfPKID.Value);
                                docObjects1.RoleID = 11;//11-Author
                                docObjects1.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                docObjects1.Remarks = "N/A";
                                docObjects1.Time = "N/A";
                                docObjects1.action = 43;
                                docObjects1.CommentType = 1;
                                Session["vwdocObjects1"] = docObjects1;

                                hdfViewType.Value = "0";
                                hdfViewEmpID.Value = "0";
                                hdfViewRoleID.Value = "0";
                                hdfIsFirst.Value = "1";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view9", "ViewTempDocument('#divPDF_Viewer','" + File2Path + "');", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "custAlertMsg(' <br/> Server error , contact admin.','error');", true);
                                return;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/> Please upload  .doc or .docx files only.','warning');", true);
                            return;
                        }
                    }
                    else
                    {
                        if (ext == ".pdf")
                        {
                            string strPDFMainFolderPath = HttpContext.Current.Server.MapPath(@"~/Scripts/pdf.js/web");
                            string File2Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                            string strTargPath = (strPDFMainFolderPath + "\\" + File2Path);
                            File.WriteAllBytes(strTargPath, Filebyte);

                            docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                            docObjects.RoleID = 11;//author
                            docObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            docObjects.FilePath1 = "";
                            docObjects.FilePath2 = string.Format(@"~/Scripts/pdf.js/web/") + File2Path;
                            docObjects.isTemp = 1;
                            docObjects.FileType1 = "";
                            docObjects.FileType2 = ".pdf";
                            docObjects.FileName1 = "";
                            docObjects.FileName2 = File2Path;
                            ViewState["PDFPhysicalName"] = File2Path;
                            hdfFileName.Value = ViewState["PDFPhysicalName"].ToString();
                            Session["vwdocObjects"] = docObjects;
                            //reviewObjects.Content = Filebyte;

                            btn_SubmittoReviewer.Visible = true;
                            btn_Download.Visible = false;

                            docObjects1.Version = Convert.ToInt32(hdfPKID.Value);
                            docObjects1.RoleID = 11;//11-Author
                            docObjects1.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                            docObjects1.Remarks = "N/A";
                            docObjects1.Time = "N/A";
                            docObjects1.action = 43;
                            docObjects1.CommentType = 1;
                            Session["vwdocObjects1"] = docObjects1;

                            hdfViewType.Value = "0";
                            hdfViewEmpID.Value = "0";
                            hdfViewRoleID.Value = "0";
                            hdfIsFirst.Value = "1";
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view9", "ViewTempDocument('#divPDF_Viewer','" + File2Path + "');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/> Please upload  .pdf files only.','warning');", true);
                            return;
                        }
                    }
                }
                else
                {
                    // if has no file
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('<br/> Uploaded document is empty.','warning');", true);
                    return;
                }
                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "show2", "$('.Refferaltab').show();", true);
                upUpload.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M10:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M10:" + strline + "  " + "Document Upload failed.", "error");
            }
            finally
            {
                Btn_Upload.Enabled = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop3", "reload();", true);
            }
        }
        #region Referrals
        void resetReferrels()
        {
            try
            {
                txtLinkReferrences.InnerText = string.Empty;
                ddl_Dept.SelectedIndex = -1;
                Session["dtAvailableDocuments"] = null;
                Session["dtAssignedDocuments"] = null;
                gvAssignedDocuments.DataSource = null;
                gvAssignedDocuments.DataBind();
                gvExternalFiles.DataSource = null;
                gvExternalFiles.DataBind();
                gvAvailableDocuments.DataSource = null;
                gvAvailableDocuments.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M11:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M11:" + strline + "  " + "Referrals resetting failed.", "error");
            }
        }
        protected void ddl_Dept_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hdnGrdiReferesh.Value = "Internal";
                hdnNewDocTab.Value = "Refferal";
                getDocumentList(true);
                upInternalDoc.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowRefferal();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M12:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M12:" + strline + "  " + "Internal Documents Loading failed.", "error");
            }
        }
        public void getDocumentList(bool isSelectedIncex)
        {
            try
            {
                if (Session["dtAssignedDocuments"] == null)
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt.Columns.AddRange(new DataColumn[5] { new DataColumn("VersionID"), new DataColumn("DocumentName"), new DataColumn("DocumentNumber"), new DataColumn("DeptID"), new DataColumn("DocumentID") });
                    Session["dtAssignedDocuments"] = dt;
                }
                System.Data.DataTable DMS_Dt = DMS_Bal.DMS_GetInternalDocumentsbyDepartment(Convert.ToInt32(ddl_Dept.SelectedValue));
                System.Data.DataTable DMS_DT1 = GetDistinctAvailableDocuments((System.Data.DataTable)Session["dtAssignedDocuments"], DMS_Dt);
                gvAvailableDocuments.DataSource = DMS_DT1;
                gvAvailableDocuments.DataBind();
                if (gvAvailableDocuments.Rows.Count > 0)
                {
                    gvAvailableDocuments.UseAccessibleHeader = true;
                    gvAvailableDocuments.HeaderRow.TableSection = TableRowSection.TableHeader;
                    gvAvailableDocuments.HeaderStyle.CssClass = "headerstyle";
                    if (ddl_Dept.SelectedIndex > 0)
                    {
                        //dvrow_DocSearch.Visible = true;//true
                    }
                }
                upInternalDoc.Update();
                Session["dtAvailableDocuments"] = DMS_DT1;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M13:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M13:" + strline + "  " + "Internal Document List failed.", "error");
            }
        }
        static System.Data.DataTable GetDistinctAvailableDocuments(System.Data.DataTable dtAssDoc, System.Data.DataTable dtAvlDoc)
        {
            foreach (DataRow row in dtAssDoc.Rows)
            {
                string DocumentID = row["DocumentID"].ToString();
                int Doccount = dtAvlDoc.AsEnumerable().Count(drow => drow.Field<int>("DocumentID").ToString() == DocumentID);
                if (Doccount > 0)
                {
                    dtAvlDoc.Rows.Remove(dtAvlDoc.AsEnumerable().Where(r => r.Field<Int32>("DocumentID").ToString() == DocumentID).FirstOrDefault());
                    dtAvlDoc.AcceptChanges();
                }
            }
            return dtAvlDoc;
        }
        protected void btn_AddDocuments_Click(object sender, EventArgs e)
        {
            btn_AddDocuments.Enabled = false;
            try
            {
                hdnGrdiReferesh.Value = "Internal";
                hdnNewDocTab.Value = "Refferal";
                System.Data.DataTable dt;
                if (Session["dtAssignedDocuments"] == null)
                {
                    dt = new System.Data.DataTable();
                    dt.Columns.AddRange(new DataColumn[5] { new DataColumn("VersionID"), new DataColumn("DocumentName"), new DataColumn("DocumentNumber"), new DataColumn("DeptID"), new DataColumn("DocumentID") });
                }
                else
                    dt = (System.Data.DataTable)Session["dtAssignedDocuments"];
                System.Data.DataTable dtAssDoc = (System.Data.DataTable)Session["dtAvailableDocuments"];
                foreach (GridViewRow row in gvAvailableDocuments.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        System.Web.UI.WebControls.CheckBox chkRow = (row.Cells[0].FindControl("cbxSelect") as System.Web.UI.WebControls.CheckBox);
                        if (chkRow.Checked)
                        {
                            string DocumentNumber = (row.Cells[1].FindControl("lbl_DocumentNumber") as Label).Text;
                            string DocumentID = (row.Cells[1].FindControl("lbl_DocumentID") as Label).Text;
                            string DocumentName = (row.Cells[2].FindControl("lbl_DocumentName") as Label).Text;
                            string DeptID = (row.Cells[2].FindControl("lbl_DeptID") as Label).Text;
                            string DocID = (row.Cells[2].FindControl("lbl_DocID") as Label).Text;

                            dt.Rows.Add(DocumentID, DocumentName, DocumentNumber, DeptID, DocID);
                            dtAssDoc.Rows.Remove(dtAssDoc.AsEnumerable().Where(r => r.Field<Int32>("DocumentID").ToString() == DocID).FirstOrDefault());
                            dtAssDoc.AcceptChanges();
                        }
                    }
                }
                gvAssignedDocuments.DataSource = dt;
                gvAssignedDocuments.DataBind();
                gvAvailableDocuments.DataSource = dtAssDoc;
                gvAvailableDocuments.DataBind();
                Session["dtAvailableDocuments"] = dtAssDoc;
                Session["dtAssignedDocuments"] = dt;
                if (gvAvailableDocuments.Rows.Count > 0)
                {
                    gvAvailableDocuments.UseAccessibleHeader = true;
                    gvAvailableDocuments.HeaderRow.TableSection = TableRowSection.TableHeader;
                    gvAvailableDocuments.HeaderStyle.CssClass = "headerstyle";

                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowRefferal();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M14:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M14:" + strline + "  " + "Add Internal Document failed.", "error");
            }
            finally
            {
                btn_AddDocuments.Enabled = true;
            }
        }
        protected void btn_RemoveDocuments_Click(object sender, EventArgs e)
        {
            btn_RemoveDocuments.Enabled = false;
            try
            {
                hdnGrdiReferesh.Value = "Internal";
                hdnNewDocTab.Value = "Refferal";
                System.Data.DataTable dt;
                if (Session["dtAssignedDocuments"] == null)
                {
                    dt = new System.Data.DataTable();
                    dt.Columns.AddRange(new DataColumn[5] { new DataColumn("VersionID"), new DataColumn("DocumentName"), new DataColumn("DocumentNumber"), new DataColumn("DeptID"), new DataColumn("DocumentID") });
                }
                else
                    dt = (System.Data.DataTable)Session["dtAssignedDocuments"];
                System.Data.DataTable dtAssDoc = (System.Data.DataTable)Session["dtAvailableDocuments"];
                foreach (GridViewRow row in gvAssignedDocuments.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        System.Web.UI.WebControls.CheckBox chkRow = (row.Cells[0].FindControl("cbxSelect") as System.Web.UI.WebControls.CheckBox);
                        if (chkRow.Checked)
                        {
                            string DeptID = (row.Cells[0].FindControl("lbl_DeptID") as Label).Text;
                            string DocumentID = (row.Cells[1].FindControl("lbl_DocumentID") as Label).Text;
                            string DocumentNumber = (row.Cells[1].FindControl("lbl_DocumentNumber") as Label).Text;
                            string DocumentName = (row.Cells[2].FindControl("lbl_DocumentName") as Label).Text;
                            string DocID = (row.Cells[2].FindControl("lbl_DocID") as Label).Text;
                            dt.Rows.Remove(dt.AsEnumerable().Where(r => r.Field<Int32>("DocumentID").ToString() == DocID).FirstOrDefault());
                            dt.AcceptChanges();
                            foreach (GridViewRow row1 in gvAvailableDocuments.Rows)
                            {
                                if (row1.RowType == DataControlRowType.DataRow)
                                {
                                    string DeptID1 = (row1.Cells[0].FindControl("lbl_DeptID") as Label).Text;
                                    if (DeptID1 == DeptID)
                                    {
                                        dtAssDoc.Rows.Add(DocumentID, DocumentName, DocumentNumber, DeptID, DocID);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                gvAssignedDocuments.DataSource = dt;
                gvAssignedDocuments.DataBind();
                gvAvailableDocuments.DataSource = dtAssDoc;
                gvAvailableDocuments.DataBind();
                Session["dtAvailableDocuments"] = dtAssDoc;
                Session["dtAssignedDocuments"] = dt;
                if (gvAvailableDocuments.Rows.Count > 0)
                {
                    gvAvailableDocuments.UseAccessibleHeader = true;
                    gvAvailableDocuments.HeaderRow.TableSection = TableRowSection.TableHeader;
                    gvAvailableDocuments.HeaderStyle.CssClass = "headerstyle";
                }
                ddl_Dept_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowRefferal();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M15:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M15:" + strline + "  " + "Remove Internal Document failed.", "error");
            }
            finally
            {
                btn_RemoveDocuments.Enabled = true;
            }
        }
        protected void btnFileUpload_Click(object sender, EventArgs e)
        {
            btnFileUpload.Enabled = false;
            try
            {
                if (FileUpload1.PostedFiles.Count > 0)
                {
                    hdnGrdiReferesh.Value = "External";
                    hdnNewDocTab.Value = "Refferal";
                    string DMS_DocumentID = hdfPKID.Value;
                    string ErrorMsg = "";
                    List<TempExternalFileBO> objbo = new List<TempExternalFileBO>();
                    foreach (HttpPostedFile postedFile in FileUpload1.PostedFiles)
                    {
                        string fileName = Path.GetFileName(postedFile.FileName);
                        if (!string.IsNullOrEmpty(fileName))
                        {
                            string ext = System.IO.Path.GetExtension(FileUpload1.FileName);
                            Stream fs = postedFile.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            byte[] binData = br.ReadBytes((Int32)fs.Length);
                            if (binData.Length < 1000)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' Not able to read content,document upload failed','warning');", true);
                                return;
                            }
                            string mime = MimeType.GetMimeType(binData, fileName);
                            string[] filemimes = new string[] { "application/x-msdownload", "application/unknown", "application/octet-stream", "application/x-shockwave-flash", "application/x-rar-compressed",
                             "video/x-msvideo" ,"audio/x-wav", "audio/x-ms-wma" ,"video/x-ms-wmv"};
                            if (filemimes.Contains(mime))
                            {
                                Session["CreateFileUpload"] = null;
                                Session["word"] = null;
                                ErrorMsg += "<br/>uploaded file " + fileName + " is not valid or corrupted.";
                            }
                            if (DMS_Bal.DMS_GetExistingFileCount(postedFile.FileName, Convert.ToInt32(DMS_DocumentID)) > 0)
                            {
                                // validation for permentant table
                                ErrorMsg += "<br/>File " + fileName + "  name already exists.";
                            }
                            else
                            {

                                //bool isDuplicate = false;
                                TempExternalFileBO objTemp = new TempExternalFileBO();

                                objTemp.CrudType = 5;
                                objTemp.Ref_FileID = 0;
                                objTemp.FileName = postedFile.FileName;
                                objTemp.FileExtension = "";
                                objTemp.FileContent = new byte[0];
                                objTemp.DocExternal_VersionID = 0;
                                System.Data.DataTable dt = DMS_Bal.DMS_TempCrudExternalFiles(objTemp);
                                if (Convert.ToInt32(dt.Rows[0][0].ToString()) > 0)
                                {
                                    ErrorMsg += "<br/>File " + fileName + "  name already exists.";
                                }
                                // insert to temp table
                                objbo.Add(new TempExternalFileBO
                                {
                                    CrudType = 1,
                                    Ref_FileID = 0,
                                    FileName = postedFile.FileName,
                                    FileExtension = ext,
                                    FileContent = binData,
                                    DocExternal_VersionID = Convert.ToInt32(DMS_DocumentID)
                                });
                            }
                        }
                    }
                    if (ErrorMsg == "")
                    {
                        foreach (var item in objbo)
                        {
                            TempExternalFileBO objTemp1 = new TempExternalFileBO();
                            objTemp1.CrudType = item.CrudType;
                            objTemp1.Ref_FileID = item.Ref_FileID;
                            objTemp1.FileName = item.FileName;
                            objTemp1.FileExtension = item.FileExtension;
                            objTemp1.FileContent = item.FileContent;
                            objTemp1.DocExternal_VersionID = Convert.ToInt32(DMS_DocumentID);
                            System.Data.DataTable dt1 = DMS_Bal.DMS_TempCrudExternalFiles(objTemp1);
                        }
                        GetgvExternalFiles();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "custAlertMsg('" + ErrorMsg + "','error');", true);
                    }

                }
                UpdatePanel8.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop3", "reload();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M16:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M16:" + strline + "  " + "External File Upload failed.", "error");
            }
            finally
            {
                btnFileUpload.Enabled = true;
            }
        }
        void GetgvExternalFiles()
        {
            try
            {
                string DMS_DocumentID = hdfPKID.Value;
                //get from permanent table
                System.Data.DataTable dtExternalDocs = DMS_Bal.DMS_GetReferalExternalFiles(Convert.ToInt32(DMS_DocumentID));
                ViewState["dtExternalDocuments"] = dtExternalDocs;
                //get temp table files and add to datatable
                TempExternalFileBO objTemp = new TempExternalFileBO();
                objTemp.CrudType = 3;
                objTemp.Ref_FileID = 0;
                objTemp.FileName = "";
                objTemp.FileExtension = "";
                objTemp.FileContent = new byte[0];
                objTemp.DocExternal_VersionID = Convert.ToInt32(DMS_DocumentID);
                System.Data.DataTable dtTempExternalDocs = DMS_Bal.DMS_TempCrudExternalFiles(objTemp);
                foreach (DataRow dr in dtTempExternalDocs.Rows)
                {
                    dtExternalDocs.Rows.Add(0, dr["FileName"].ToString(), "", "", "", dr["DocExternal_VersionID"].ToString());
                }
                if (dtExternalDocs.Rows.Count > 0)
                {
                    gvExternalFiles.DataSource = dtExternalDocs;
                    gvExternalFiles.DataBind();
                }
                else
                {
                    gvExternalFiles.DataSource = new List<object>();
                    gvExternalFiles.DataBind();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M17:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M17:" + strline + "  " + "External files Loading failed.", "error");
            }
        }
        protected void DownloadFile(object sender, EventArgs e)
        {
            try
            {
                string filePath = (sender as LinkButton).CommandArgument;
                //view temp file or permanent file                
                ExternalRefDiv.InnerHtml = "&nbsp";
                ExternalRefDiv.InnerHtml = "<iframe src=\"" + ResolveUrl("~/DMS/DocumentCreator/CreateRefferalInIFrame.aspx") + "?FilePath=" + filePath + "#toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" height=\"690px\" ></iframe>";
                UpdatePanel4.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowExternalFile();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M18:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M18:" + strline + "  " + "External File viewing failed.", "error");
            }
        }
        protected void DeleteFile(object sender, EventArgs e)
        {
            try
            {
                string filePath = (sender as LinkButton).CommandArgument;
                string[] stringSeparators = new string[] { "@$" };
                string[] result = filePath.Split(stringSeparators, StringSplitOptions.None);

                if (result[1] == "0")
                {
                    // temp delete
                    TempExternalFileBO objTemp = new TempExternalFileBO();
                    objTemp.CrudType = 4;
                    objTemp.Ref_FileID = 0;
                    objTemp.FileName = result[0];
                    objTemp.FileExtension = "";
                    objTemp.FileContent = new byte[0];
                    objTemp.DocExternal_VersionID = Convert.ToInt32(result[2]);
                    System.Data.DataTable dtTempExternalDocs = DMS_Bal.DMS_TempCrudExternalFiles(objTemp);
                }
                else
                {
                    // permanent delete
                    int del = DMS_Bal.DMS_DeleteExternalFiles(Convert.ToInt32(result[1]));
                }
                GetgvExternalFiles();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowRefferal();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M19:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M19:" + strline + "  " + "External file Removal failed.", "error");
            }
        }
        protected void gvExternalFiles_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string _pkid = (e.Row.Cells[1].FindControl("lbl_pkid") as Label).Text;
                    if (Convert.ToInt32(_pkid) > 0)
                    {
                        LinkButton _lnkDelete = (e.Row.Cells[2].FindControl("lnkDelete") as LinkButton);
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M20:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M20:" + strline + "  " + "External file row data-bound failed.", "error");
            }
        }
        protected void btn_Refferals_Cancel_Click(object sender, EventArgs e)
        {
            btn_Refferals_Cancel.Enabled = false;
            try
            {
                //resetReferrels();
                if (btn_SubmittoReviewer.Text == "Submit")
                {
                    hdnNewDocTab.Value = "Create";
                }
                else if (btn_SubmittoReviewer.Text == "Update")
                {
                    hdnNewDocTab.Value = "Modify";
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "poptab", "ReferalsTabState();", true);
                if (ViewState["PDFPhysicalName"].ToString() != "")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view15", "ViewTempDocument('#divPDF_Viewer','" + ViewState["PDFPhysicalName"].ToString() + "');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view15", "ViewDocument('#divPDF_Viewer');", true);
                }
                UpdatePanel5.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M21:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M21:" + strline + "  " + "Referrals cancel click failed.", "error");
            }
            finally { btn_Refferals_Cancel.Enabled = true; }
        }
        protected void btn_Add_Referrences_Click(object sender, EventArgs e)
        {
            btn_Add_Referrences.Enabled = false;
            try
            {
                string DMS_DocumentID = hdfPKID.Value;
                TempExternalFileBO objTemp = new TempExternalFileBO();
                objTemp.CrudType = 3;
                objTemp.Ref_FileID = 0;
                objTemp.FileName = "";
                objTemp.FileExtension = "";
                objTemp.FileContent = new byte[0];
                objTemp.DocExternal_VersionID = Convert.ToInt32(DMS_DocumentID);
                System.Data.DataTable dtTempExternalDocs = DMS_Bal.DMS_TempCrudExternalFiles(objTemp);
                if ((txtLinkReferrences.InnerText == "<div><br></div>" || txtLinkReferrences.InnerText == "<br>") && gvAssignedDocuments.Rows.Count == 0 && dtTempExternalDocs.Rows.Count == 0)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Please Add Referral.", "warning");
                }
                else
                {
                    ReferralBO obj_ReferralBO = new ReferralBO();
                    obj_ReferralBO.Doc_VersionID = Convert.ToInt32(DMS_DocumentID);
                    obj_ReferralBO.Doc_RefContent = (txtLinkReferrences.InnerText);
                    int resultLnk = DMS_Bal.DMS_insertLinkReferences(obj_ReferralBO);
                    List<ReferralBO> lst_ReferralBO = new List<ReferralBO>();
                    foreach (GridViewRow rowInt in gvAssignedDocuments.Rows)
                    {
                        if (rowInt.RowType == DataControlRowType.DataRow)
                        {
                            string RefDocumentID = (rowInt.Cells[1].FindControl("lbl_DocumentID") as Label).Text;
                            ReferralBO obj_ReferralBOInternal = new ReferralBO();
                            obj_ReferralBOInternal.Doc_VersionID = Convert.ToInt32(DMS_DocumentID);
                            obj_ReferralBOInternal.Doc_Ref_VersionID = Convert.ToInt32(RefDocumentID);
                            lst_ReferralBO.Add(obj_ReferralBOInternal);
                        }
                    }
                    int resultInternal = DMS_Bal.DMS_insertReferalInternalDocs(lst_ReferralBO);

                    foreach (DataRow dr in dtTempExternalDocs.Rows)
                    {
                        ReferralBO obj_ReferralBOExternal = new ReferralBO();
                        obj_ReferralBOExternal.Doc_VersionID = Convert.ToInt32(DMS_DocumentID);
                        obj_ReferralBOExternal.Doc_FileName = dr["FileName"].ToString();
                        obj_ReferralBOExternal.Doc_FileExtension = dr["FileExtension"].ToString();
                        obj_ReferralBOExternal.Doc_FileContent = new byte[0];
                        int resultExternal = DMS_Bal.DMS_insertReferalExternalFiles(obj_ReferralBOExternal);
                    }
                    RetrieveRefferalContent(Convert.ToInt32(DMS_DocumentID));
                    HelpClass.custAlertMsg(this, this.GetType(), "Referrals saved successfully.", "success");
                }
                if (hdnGrdiReferesh.Value == "Link")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tab1", "TabRefClick();", true);
                }
                if (hdnGrdiReferesh.Value == "Internal")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tab2", "TabRefClick();", true);
                }
                if (hdnGrdiReferesh.Value == "External")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tab3", "TabRefClick();", true);
                }
                UpdatePanel5.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M22:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M22:" + strline + "  " + "Save Reference Files failed.", "error");
            }
            finally
            {
                btn_Add_Referrences.Enabled = true;
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "hidewait", "HideWait();", true);
            }
        }
        protected void gvAvailableDocuments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvAvailableDocuments.PageIndex = e.NewPageIndex;
            ddl_Dept_SelectedIndexChanged(null, null);
        }
        protected void gvAssignedDocuments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvAssignedDocuments.PageIndex = e.NewPageIndex;
                gvAssignedDocuments.DataSource = (System.Data.DataTable)Session["dtAssignedDocuments"];
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowRefferal();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M23:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M23:" + strline + "  " + "Internal Documents Page Index Changing failed.", "error");
            }
        }
        #endregion Referrals
        protected void btn_Download_Click(object sender, EventArgs e)
        {
            btn_Download.Enabled = false;
            try
            {
                System.Data.DataTable dt = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                if (ddl_RevertedBy.Visible == false)
                {
                    if (Request.QueryString["tab"] == "1")
                    {
                        if (dt.Rows[0]["RequestTypeID"].ToString() == "1")
                        {
                            docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                            docObjects.RoleID = 0;
                            docObjects.EmpID = 0;
                            docObjects.FileType1 = dt.Rows[0]["FileType"].ToString();
                            docObjects.Mode = 0;
                        }
                        else if (dt.Rows[0]["RequestTypeID"].ToString() == "2")
                        {
                            docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                            docObjects.RoleID = 0;
                            docObjects.EmpID = 0;
                            docObjects.FileType1 = dt.Rows[0]["FileType"].ToString();
                            docObjects.Mode = 1;
                        }
                    }
                    else if (Request.QueryString["tab"] == "2")
                    {
                        docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                        docObjects.RoleID = 0;
                        docObjects.EmpID = 0;
                        docObjects.FileType1 = dt.Rows[0]["FileType"].ToString();
                        docObjects.Mode = 0;
                    }
                }
                else
                {
                    if (ddl_RevertedBy.SelectedValue == "0")
                    {
                        docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                        docObjects.RoleID = 11;//Author
                        docObjects.EmpID = 0;
                        docObjects.FileType1 = dt.Rows[0]["FileType"].ToString();
                        docObjects.Mode = 0;
                    }
                    else
                    {
                        docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                        docObjects.RoleID = 0;
                        docObjects.EmpID = Convert.ToInt32(ddl_RevertedBy.SelectedValue);
                        docObjects.FileType1 = dt.Rows[0]["FileType"].ToString();
                        docObjects.Mode = 0;
                    }
                }
                System.Data.DataTable DMS_DT = DMS_Bal.DMS_GetDocumentLocationList(docObjects);
                string DocPath = null;
                string DocName = null;
                if (DMS_DT.Rows.Count > 0)
                {
                    DocPath = Server.MapPath(@DMS_DT.Rows[0]["FilePath"].ToString());
                }
                else
                {
                    if (dt.Rows[0]["FileName"] != DBNull.Value)
                    {
                        docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                        docObjects.RoleID = 11;//Author
                        docObjects.EmpID = 0;
                        docObjects.FileType1 = dt.Rows[0]["FileType"].ToString();
                        docObjects.Mode = 0;
                        System.Data.DataTable table = DMS_Bal.DMS_GetDocumentLocationList(docObjects);
                        DocPath = Server.MapPath(@table.Rows[0]["FilePath"].ToString());
                    }
                    else
                    {
                        docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                        docObjects.RoleID = 0;
                        docObjects.EmpID = 0;
                        docObjects.FileType1 = dt.Rows[0]["FileType"].ToString();
                        docObjects.Mode = 1;
                        System.Data.DataTable table = DMS_Bal.DMS_GetDocumentLocationList(docObjects);
                        DocPath = Server.MapPath(@table.Rows[0]["FilePath"].ToString());
                    }
                }

                if (DMS_DT.Rows[0]["FileType"].ToString() == ".doc")
                {
                    Response.ContentType = "application/msword";
                }
                else if (DMS_DT.Rows[0]["FileType"].ToString() == ".docx")
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                }
                if (dt.Rows[0]["FileType"].ToString() == DMS_DT.Rows[0]["FileType"].ToString())
                {
                    DocName = DMS_DT.Rows[0]["FileName"].ToString();
                }
                else
                {
                    string[] FileName = DMS_DT.Rows[0]["FileName"].ToString().Split('.');
                    DocName = FileName[0] + DMS_DT.Rows[0]["FileType"].ToString();
                }
                Response.AddHeader("Content-Disposition", "attachment; filename=" + DocName);
                Response.TransmitFile(DocPath);
                DownloadRecord();
                Response.End();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M24:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M24:" + strline + "  " + "download click failed.", "error");
            }
            finally
            {
                btn_Download.Enabled = true;
            }
        }
        public void DownloadRecord()
        {
            docObjects1.Version = Convert.ToInt32(hdfPKID.Value);
            docObjects1.RoleID = 11;//11-Author
            docObjects1.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            docObjects1.Remarks = "N/A";
            docObjects1.Time = "N/A";
            docObjects1.action = 42;
            docObjects1.CommentType = 1;
            DMS_Bal.DMS_DocComments(docObjects1);
        }
        public System.Data.DataTable DMS_GetDocRevertedByList()
        {
            try
            {
                docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                docObjects.Mode = 1;
                System.Data.DataTable DMS_Dt = DMS_Bal.DMS_GetTempRevertedReviewersDetails(docObjects);
                ddl_RevertedBy.DataValueField = DMS_Dt.Columns["RevertedByID"].ToString();
                ddl_RevertedBy.DataTextField = DMS_Dt.Columns["RevertedBy"].ToString();
                ddl_RevertedBy.DataSource = DMS_Dt;
                ddl_RevertedBy.DataBind();
                ddl_RevertedBy.Items.Insert(0, new ListItem("-- Select Reverted By --", "0"));
                return DMS_Dt;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M30:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M30:" + strline + "  " + "Reverted By Load Failed.", "error");
                return null;
            }
        }
        protected void Btn_ShowOriginal_Click(object sender, EventArgs e)
        {
            Btn_ShowOriginal.Enabled = false;
            try
            {
                hdfViewType.Value = "0";
                hdfViewEmpID.Value = "0";
                hdfViewRoleID.Value = "11";//Author
                if (ViewState["PDFPhysicalName"].ToString() != "")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view10", "ViewTempDocument('#divPDF_Viewer','" + ViewState["PDFPhysicalName"].ToString() + "');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view10", "ViewDocument('#divPDF_Viewer');", true);
                }
                Btn_ShowOriginal.Visible = false;
                //if (hdfPID.Value == "1")
                //{
                ddl_RevertedBy.SelectedValue = "0";
                //}
                //else
                //{
                //    Btn_ShowModified.Visible = true;
                //}
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M31:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M31:" + strline + "  " + "Show Original Click Failed.", "error");
            }
            finally
            {
                Btn_ShowOriginal.Enabled = true;
            }
        }
        protected void Btn_ShowModified_Click(object sender, EventArgs e)
        {
            Btn_ShowModified.Enabled = false;
            try
            {
                hdfViewType.Value = "0";
                hdfViewEmpID.Value = ddl_RevertedBy.SelectedValue;
                hdfViewRoleID.Value = "0";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view11", "ViewDocument('#divPDF_Viewer');", true);
                Btn_ShowOriginal.Visible = true;
                //if (hdfPID.Value == "1")
                //{

                //}
                //else
                //{
                //    Btn_ShowModified.Visible = false;
                //}
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M32:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M32:" + strline + "  " + "Show Modified Click Failed.", "error");
            }
            finally
            {
                Btn_ShowModified.Enabled = true;
            }
        }
        protected void ddl_RevertedBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddl_RevertedBy.SelectedValue == "0")
                {
                    hdfViewType.Value = "0";
                    hdfViewEmpID.Value = "0";
                    hdfViewRoleID.Value = "11";//Author
                    if (ViewState["PDFPhysicalName"].ToString() != "")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view12", "ViewTempDocument('#divPDF_Viewer','" + ViewState["PDFPhysicalName"].ToString() + "');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view12", "ViewDocument('#divPDF_Viewer');", true);
                    }
                    Btn_ShowOriginal.Visible = false;
                }
                else
                {
                    docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                    docObjects.RoleID = 0;
                    docObjects.EmpID = Convert.ToInt32(ddl_RevertedBy.SelectedValue);
                    docObjects.FileType1 = ".pdf";
                    docObjects.Mode = 0;
                    System.Data.DataTable dt = DMS_Bal.DMS_GetDocumentLocationList(docObjects);
                    if (dt.Rows.Count == 0)
                    {
                        hdfViewType.Value = "0";
                        hdfViewEmpID.Value = "0";
                        hdfViewRoleID.Value = "0";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view13", "ViewDocument('#divPDF_Viewer');", true);
                    }
                    else
                    {
                        hdfViewType.Value = "0";
                        hdfViewEmpID.Value = ddl_RevertedBy.SelectedValue;
                        hdfViewRoleID.Value = "0";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view14", "ViewDocument('#divPDF_Viewer');", true);
                    }
                    Btn_ShowOriginal.Visible = true;
                }
                hdfIsFirst.Value = "1";
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M32:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M33:" + strline + "  " + "Reverted By Selected Index Changed Failed.", "error");
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}