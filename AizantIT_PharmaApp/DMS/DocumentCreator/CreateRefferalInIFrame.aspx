﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateRefferalInIFrame.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentCreator.CreateRefferalInIFrame" %>

<%@ Register Assembly="DevExpress.Web.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxRichEdit.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRichEdit" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpreadsheet.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpreadsheet" TagPrefix="dx" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        .dxreView {
            height: 640px !important;
        }

        .dxrePage {
            height: 1123px !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <dx:PanelContent runat="server">
                <dx:ASPxSpreadsheet ID="Spreadsheet" Height="650px" Width="100%" runat="server" ReadOnly="true" WorkDirectory="~/App_Data/WorkDirectory" RibbonMode="None" Visible="false">
                    <SettingsDocumentSelector>
                        <EditingSettings AllowCopy="false" />
                    </SettingsDocumentSelector>
                </dx:ASPxSpreadsheet>
                <dx:ASPxRichEdit ID="RichEdit" runat="server" ShowConfirmOnLosingChanges="false" Settings-HorizontalRuler-Visibility="Hidden" Settings-DocumentCapabilities-Bookmarks="Disabled"
                    Settings-Bookmarks-Visibility="Hidden" ReadOnly="true" WorkDirectory="~\App_Data\WorkDirectory" RibbonMode="None" Width="1150px" Visible="false">
                    <Settings>
                        <Behavior Copy="Disabled" Download="Disabled" Cut="Disabled" Paste="Disabled" Printing="Disabled" AcceptsTab="false" Save="Disabled" />
                    </Settings>
                </dx:ASPxRichEdit>
                <dx:ASPxBinaryImage runat="server" ID="binImage" Height="300" Visible="false">
                </dx:ASPxBinaryImage>
                <div id="pdfdiv">
                    <asp:Literal ID="literalSop" runat="server" Visible="false"></asp:Literal>
                </div>
            </dx:PanelContent>
        </div>
        <script>
            function CloseBrowser() {
                window.close();
            }
        </script>
    </form>
</body>
</html>
