﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using DevExpress.Web.Office;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_BusinessLayer;
namespace AizantIT_PharmaApp.DMS.DocumentCreator
{
    public partial class DocumentDetails : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        ReviewObjects reviewObjects = new ReviewObjects();
        UMS_BAL objUMS_BAL;
        protected void Page_Load(object sender, EventArgs e)
        {
            ConfirmCustomAlert.buttonClick += new EventHandler(ConfirmCustomAlert1_buttonClick);
            ConfirmCustomAlert.buttonClick += new EventHandler(ConfirmCustomAlert_buttonClick);
            if (HelpClass.IsUserAuthenticated())
            {
                if (!IsPostBack)
                {
                    DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                    DataTable dtTemp = new DataTable();
                    DataRow[] drUMS = dtx.Select("ModuleID=2");
                    if (drUMS.Length > 0)
                    {
                        dtTemp = drUMS.CopyToDataTable();
                        if (dtTemp.Select("RoleID=11").Length > 0)
                        {
                            if (Request.QueryString["Notification_ID"] != null)
                            {
                                objUMS_BAL = new UMS_BAL();
                                objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, (Session["UserSignIn"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString()); //2 represents NotifyEmp had visited the page through notification link.
                            }
                            InitializeThePage();
                        }
                        else
                        {
                            loadAuthorizeErrorMsg();
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx", false);
                    }
                }
                else
                {
                    ElectronicSign.buttonClick += new EventHandler(ElectronicSign_buttonClick);
                    SessionTimeOut.UpdateSessionTimeOut();
                }
            }
            else
            {
                Response.Redirect("~/UserLogin.aspx", false);
            }
        }
        protected void ElectronicSign_buttonClick(object sender, EventArgs e)
        {
            try
            {
                bool b = ElectronicSign.IsPasswordValid;
                if (b == true)
                {
                    if (ViewState["hdnAction"].ToString() == "Submit")
                    {
                        Submit();
                    }
                }
                if (b == false)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Password is Incorrect!", "error");
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    Electronic Signature validation failed','error');", true);
            }
        }
        protected void ConfirmCustomAlert_buttonClick(object sender, EventArgs e)
        {
            try
            {
                if (hdfConfirm.Value == "Save")
                {
                    Session.Remove("word");
                    lbl_Template.Visible = false;
                    ddl_Template.Visible = false;
                    WordUpload.Visible = false;
                    byte[] Doc_data;
                    Doc_data = ASPxRichEdit2.SaveCopy(DevExpress.XtraRichEdit.DocumentFormat.Rtf);
                    string DMS_DocumentID = hdfPKID.Value;
                    reviewObjects.Id = Convert.ToInt32(DMS_DocumentID);
                    reviewObjects.Content = Doc_data;
                    int DMS_Status = DMS_Bal.DMS_UpdateContentByID(reviewObjects);
                    if (DMS_Status > 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "SaveYes();", true);
                        HelpClass.custAlertMsg(this, this.GetType(), "Document Saved Sucessfully", "success");
                    }
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>   Document Save failed','error');", true);
            }
        }
        protected void ConfirmCustomAlert1_buttonClick(object sender, EventArgs e)
        {
            try
            {
                if (hdfConfirm.Value == "Submit")
                {
                    ViewState["hdnAction"] = "Submit";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "openElectronicSignModal();", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>   Document Submit failed','error');", true);
            }
        }
        public void DMS_GetTemplateNameList()
        {
            try
            {
                JQDataTableBO objJQDataTableBO = new JQDataTableBO();
                objJQDataTableBO.iMode = 0;
                objJQDataTableBO.pkid = 0;
                objJQDataTableBO.iDisplayLength = 0;
                objJQDataTableBO.iDisplayStart = 0;
                objJQDataTableBO.iSortCol = 0;
                objJQDataTableBO.sSortDir = "";
                objJQDataTableBO.sSearch = "";
                DataTable DMS_Dt = DMS_Bal.DMS_GetTemplateNameList(objJQDataTableBO);
                ddl_Template.DataValueField = DMS_Dt.Columns["TemplateID"].ToString();
                ddl_Template.DataTextField = DMS_Dt.Columns["TemplateName"].ToString();
                ddl_Template.DataSource = DMS_Dt;
                ddl_Template.DataBind();
                ddl_Template.Items.Insert(0, new ListItem("--Select Template--", "0"));
                UpMyDocFiles.Update();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>   Select Template failed','error');", true);
            }
        }
        public void DMS_GetDepartmentList()
        {
            try
            {
                DataTable DMS_Dt = DMS_BalDM.DMS_GetDepartmentList();
                ddl_Dept.DataValueField = DMS_Dt.Columns["DeptID"].ToString();
                ddl_Dept.DataTextField = DMS_Dt.Columns["DepartmentName"].ToString();
                ddl_Dept.DataSource = DMS_Dt;
                ddl_Dept.DataBind();
                ddl_Dept.Items.Insert(0, new ListItem("Select Department", "0"));
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "pop", "custAlertMsg('<br/> Department Failed ','error');", true);
            }
        }
        public void Rejected_comments()
        {
            try
            {
                DataTable DMS_Commets = DMS_Bal.DMS_GetRejectCommentsByID(hdfPKID.Value);
                if (DMS_Commets.Rows.Count > 0)
                {
                    gv_rejectComments.GridLines = GridLines.None;
                    gv_rejectComments.DataSource = DMS_Commets;
                    gv_rejectComments.DataBind();
                }
                else
                {
                    divgvcomments.Visible = false;
                    gv_rejectComments.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            btn_Submit.Enabled = false;
            try
            {
                DataTable DMS_template;
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                if (DMS_DT.Rows[0]["FileType"].ToString() == "pdf")
                {
                    if (Session["word"] != null)
                    {
                        byte[] docBytes = (byte[])Session["CreateFileUpload"];
                        string strTargPath = Server.MapPath(@"~/Files/PDFtoWordUpload") + Session["CreateFileUploadExtension"].ToString();
                        if (File.Exists(strTargPath))
                        {
                            // Note that no lock is put on the
                            // file and the possibility exists
                            // that another process could do
                            // something with it between
                            // the calls to Exists and Delete.
                            File.Delete(strTargPath);
                        }
                        File.WriteAllBytes(strTargPath, docBytes);
                        if (Session["CreateFileUploadExtension"].ToString() == ".doc")
                        {
                            RichEditDocumentServer server = new RichEditDocumentServer();
                            server.LoadDocument(strTargPath, DocumentFormat.Doc);
                            Document document = server.Document;
                            Section firstSection = document.Sections[0];
                            SubDocument myHeader = firstSection.BeginUpdateHeader(HeaderFooterType.Primary);
                            DocumentRange[] deptrange = myHeader.FindAll("docdept1", SearchOptions.CaseSensitive, myHeader.Range);
                            foreach (DocumentRange documentRange in deptrange)
                            {
                                myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["Department"].ToString());
                                myHeader.Delete(documentRange);
                            }
                            DocumentRange[] typerange = myHeader.FindAll("doctype1", SearchOptions.CaseSensitive, myHeader.Range);
                            foreach (DocumentRange documentRange in typerange)
                            {
                                myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentType"].ToString());
                                myHeader.Delete(documentRange);
                            }
                            DocumentRange[] docnamerange = myHeader.FindAll("docname1", SearchOptions.CaseSensitive, myHeader.Range);
                            foreach (DocumentRange documentRange in docnamerange)
                            {
                                myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentName"].ToString());
                                myHeader.Delete(documentRange);
                            }
                            DocumentRange[] docnumrange = myHeader.FindAll("docnum1", SearchOptions.CaseSensitive, myHeader.Range);
                            foreach (DocumentRange documentRange in docnumrange)
                            {
                                myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentNumber"].ToString() + "-0" + DMS_DT.Rows[0]["VersionNumber"].ToString());
                                myHeader.Delete(documentRange);
                            }
                            firstSection.EndUpdateHeader(myHeader);
                            document.SaveDocument(strTargPath, DocumentFormat.Doc);
                            ASPxRichEdit2.Open(strTargPath, DocumentFormat.Doc);
                        }
                        if (Session["CreateFileUploadExtension"].ToString() == ".docx")
                        {
                            RichEditDocumentServer server = new RichEditDocumentServer();
                            server.LoadDocument(strTargPath, DocumentFormat.OpenXml);
                            Document document = server.Document;
                            Section firstSection = document.Sections[0];
                            SubDocument myHeader = firstSection.BeginUpdateHeader(HeaderFooterType.Primary);
                            DocumentRange[] deptrange = myHeader.FindAll("docdept1", SearchOptions.CaseSensitive, myHeader.Range);
                            foreach (DocumentRange documentRange in deptrange)
                            {
                                myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["Department"].ToString());
                                myHeader.Delete(documentRange);
                            }
                            DocumentRange[] typerange = myHeader.FindAll("doctype1", SearchOptions.CaseSensitive, myHeader.Range);
                            foreach (DocumentRange documentRange in typerange)
                            {
                                myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentType"].ToString());
                                myHeader.Delete(documentRange);
                            }
                            DocumentRange[] docnamerange = myHeader.FindAll("docname1", SearchOptions.CaseSensitive, myHeader.Range);
                            foreach (DocumentRange documentRange in docnamerange)
                            {
                                myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentName"].ToString());
                                myHeader.Delete(documentRange);
                            }
                            DocumentRange[] docnumrange = myHeader.FindAll("docnum1", SearchOptions.CaseSensitive, myHeader.Range);
                            foreach (DocumentRange documentRange in docnumrange)
                            {
                                myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentNumber"].ToString() + "-0" + DMS_DT.Rows[0]["VersionNumber"].ToString());
                                myHeader.Delete(documentRange);
                            }
                            firstSection.EndUpdateHeader(myHeader);
                            document.SaveDocument(strTargPath, DocumentFormat.OpenXml);
                            ASPxRichEdit2.Open(strTargPath, DocumentFormat.OpenXml);
                        }
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowDocCreate();", true);
                    }
                    else
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Please Upload Original Word Document To Proceed Further!", "error");
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowDocDetails();", true);
                    }
                }
                else
                {
                    if (DMS_DT.Rows[0]["Content"] != DBNull.Value)
                    {
                        ASPxRichEdit2.Open(
                        Guid.NewGuid().ToString(),
                        DocumentFormat.Rtf,
                        () =>
                        {
                            byte[] docBytes = (byte[])DMS_DT.Rows[0]["Content"];
                            return new MemoryStream(docBytes);
                        }
                        );
                    }
                }
                if (ddl_Template.Visible == true)
                {
                    if (ddl_Template.SelectedValue != "0")
                    {
                        DMS_template = DMS_Bal.DMS_GetTempalteContent(ddl_Template.SelectedValue);
                        byte[] docBytes = (byte[])DMS_template.Rows[0]["Content"];
                        string strTargPath = Server.MapPath(@"~/Files") + "\\" + DMS_template.Rows[0]["TemplateName"].ToString() + DMS_template.Rows[0]["FileType"].ToString();
                        if (File.Exists(strTargPath))
                        {
                            // Note that no lock is put on the
                            // file and the possibility exists
                            // that another process could do
                            // something with it between
                            // the calls to Exists and Delete.
                            File.Delete(strTargPath);
                        }
                        File.WriteAllBytes(strTargPath, docBytes);
                        if (DMS_template.Rows[0]["FileType"].ToString() == ".doc")
                        {
                            RichEditDocumentServer server = new RichEditDocumentServer();
                            MemoryStream stream = new MemoryStream(docBytes);
                            server.LoadDocument(strTargPath, DocumentFormat.Doc);
                            Document document = server.Document;
                            Section firstSection = document.Sections[0];
                            SubDocument myHeader = firstSection.BeginUpdateHeader(HeaderFooterType.Primary);
                            DocumentRange[] deptrange = myHeader.FindAll("docdept1", SearchOptions.CaseSensitive, myHeader.Range);
                            foreach (DocumentRange documentRange in deptrange)
                            {
                                myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["Department"].ToString());
                                myHeader.Delete(documentRange);
                            }
                            DocumentRange[] typerange = myHeader.FindAll("doctype1", SearchOptions.CaseSensitive, myHeader.Range);
                            foreach (DocumentRange documentRange in typerange)
                            {
                                myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentType"].ToString());
                                myHeader.Delete(documentRange);
                            }
                            DocumentRange[] docnamerange = myHeader.FindAll("docname1", SearchOptions.CaseSensitive, myHeader.Range);
                            foreach (DocumentRange documentRange in docnamerange)
                            {
                                myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentName"].ToString());
                                myHeader.Delete(documentRange);
                            }
                            DocumentRange[] docnumrange = myHeader.FindAll("docnum1", SearchOptions.CaseSensitive, myHeader.Range);
                            foreach (DocumentRange documentRange in docnumrange)
                            {
                                myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentNumber"].ToString() + "-0" + DMS_DT.Rows[0]["VersionNumber"].ToString());
                                myHeader.Delete(documentRange);
                            }
                            firstSection.EndUpdateHeader(myHeader);
                            document.SaveDocument(strTargPath, DocumentFormat.Doc);
                            ASPxRichEdit2.Open(strTargPath, DocumentFormat.Doc);
                        }
                        if (DMS_template.Rows[0]["FileType"].ToString() == ".docx")
                        {
                            RichEditDocumentServer server = new RichEditDocumentServer();
                            server.LoadDocument(strTargPath, DocumentFormat.OpenXml);
                            Document document = server.Document;
                            Section firstSection = document.Sections[0];
                            SubDocument myHeader = firstSection.BeginUpdateHeader(HeaderFooterType.Primary);
                            DocumentRange[] deptrange = myHeader.FindAll("docdept1", SearchOptions.CaseSensitive, myHeader.Range);
                            foreach (DocumentRange documentRange in deptrange)
                            {
                                myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["Department"].ToString());
                                myHeader.Delete(documentRange);
                            }
                            DocumentRange[] typerange = myHeader.FindAll("doctype1", SearchOptions.CaseSensitive, myHeader.Range);
                            foreach (DocumentRange documentRange in typerange)
                            {
                                myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentType"].ToString());
                                myHeader.Delete(documentRange);
                            }
                            DocumentRange[] docnamerange = myHeader.FindAll("docname1", SearchOptions.CaseSensitive, myHeader.Range);
                            foreach (DocumentRange documentRange in docnamerange)
                            {
                                myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentName"].ToString());
                                myHeader.Delete(documentRange);
                            }
                            DocumentRange[] docnumrange = myHeader.FindAll("docnum1", SearchOptions.CaseSensitive, myHeader.Range);
                            foreach (DocumentRange documentRange in docnumrange)
                            {
                                myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentNumber"].ToString() + "-0" + DMS_DT.Rows[0]["VersionNumber"].ToString());
                                myHeader.Delete(documentRange);
                            }
                            firstSection.EndUpdateHeader(myHeader);
                            document.SaveDocument(strTargPath, DocumentFormat.OpenXml);
                            ASPxRichEdit2.Open(strTargPath, DocumentFormat.OpenXml);
                        }
                    }
                    else
                    {
                        RichEditDocumentServer server = new RichEditDocumentServer();
                        server.LoadDocument(Server.MapPath(@"~/Files/WordSample2.docx"), DocumentFormat.OpenXml);
                        Document document = server.Document;
                        Section firstSection = document.Sections[0];
                        SubDocument myHeader = firstSection.BeginUpdateHeader(HeaderFooterType.Primary);
                        DocumentRange[] deptrange = myHeader.FindAll("docdept1", SearchOptions.CaseSensitive, myHeader.Range);
                        foreach (DocumentRange documentRange in deptrange)
                        {
                            myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["Department"].ToString());
                            myHeader.Delete(documentRange);
                        }
                        DocumentRange[] typerange = myHeader.FindAll("doctype1", SearchOptions.CaseSensitive, myHeader.Range);
                        foreach (DocumentRange documentRange in typerange)
                        {
                            myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentType"].ToString());
                            myHeader.Delete(documentRange);
                        }
                        DocumentRange[] docnamerange = myHeader.FindAll("docname1", SearchOptions.CaseSensitive, myHeader.Range);
                        foreach (DocumentRange documentRange in docnamerange)
                        {
                            myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentName"].ToString());
                            myHeader.Delete(documentRange);
                        }
                        DocumentRange[] docnumrange = myHeader.FindAll("docnum1", SearchOptions.CaseSensitive, myHeader.Range);
                        foreach (DocumentRange documentRange in docnumrange)
                        {
                            myHeader.InsertText(documentRange.Start, DMS_DT.Rows[0]["DocumentNumber"].ToString() + "-0" + DMS_DT.Rows[0]["VersionNumber"].ToString());
                            myHeader.Delete(documentRange);
                        }
                        firstSection.EndUpdateHeader(myHeader);
                        string path = Server.MapPath(@"~/Files/FirstPage.docx");
                        // Delete the file if it exists.
                        if (File.Exists(path))
                        {
                            // Note that no lock is put on the
                            // file and the possibility exists
                            // that another process could do
                            // something with it between
                            // the calls to Exists and Delete.
                            File.Delete(path);
                        }
                        document.SaveDocument(Server.MapPath(@"~/Files/FirstPage.docx"), DocumentFormat.OpenXml);
                        ASPxRichEdit2.Open(Server.MapPath(@"~/Files/FirstPage.docx"), DocumentFormat.OpenXml);
                    }
                }
                UpdatePanel1.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowDocCreate();", true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                btn_Submit.Enabled = true;
            }
        }
        protected void btn_SubmittoReviewer_Click(object sender, EventArgs e)
        {
            btn_SubmittoReviewer.Enabled = false;
            try
            {
                ViewState["hdnAction"] = "Submit";

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "openElectronicSignModal();", true);
                upbns.Update();
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>   Document Submit failed','error');", true);
            }
            finally { btn_SubmittoReviewer.Enabled = true; }
        }
        public void Submit()
        {
            try
            {
                Session.Remove("word");
                lbl_Template.Visible = false;
                ddl_Template.Visible = false;
                WordUpload.Visible = false;
                byte[] Doc_data;
                Doc_data = ASPxRichEdit2.SaveCopy(DevExpress.XtraRichEdit.DocumentFormat.Rtf);
                string DMS_DocumentID = hdfPKID.Value;
                reviewObjects.Id = Convert.ToInt32(DMS_DocumentID);
                reviewObjects.Content = Doc_data;
                reviewObjects.Creator = Convert.ToInt32((Session["UserSignIn"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                int DMS_Ins = DMS_Bal.DMS_DocumentToReviewer(reviewObjects);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "HidePopup();", true);
                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document with DocumentNumber " + txt_DocumentNumber.Text + " Submited successfully", "success", "ReloadCreatorPage()");
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>   Document Submit failed','error');", true);
            }
        }
        protected void gv_rejectComments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_rejectComments.PageIndex = e.NewPageIndex;
                gv_rejectComments.DataSource = DMS_Bal.DMS_GetRejectCommentsByID(hdfPKID.Value);
                gv_rejectComments.DataBind();
                UpdatePanel1.Update();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>  reject Comments PageIndex changing  failed','error');", true);
            }
        }
        protected void lnk_Word_Click(object sender, EventArgs e)
        {
            lnk_Word.Enabled = false;
            try
            {
                UpMyDocFiles.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowFileUpload();", true);
            }
            catch { }
            finally
            {
                lnk_Word.Enabled = true;
            }
        }
        protected void Btn_Upload_Click(object sender, EventArgs e)
        {
            Btn_Upload.Enabled = false;
            try
            {
                if (file_word.HasFile)
                {
                    Session["CreateFileUpload"] = null;
                    Session["word"] = null;
                    byte[] imgbyte = null;
                    if (file_word.PostedFile.FileName.Length > 5)
                    {
                        string ext = System.IO.Path.GetExtension(file_word.FileName);
                        Stream fs = file_word.PostedFile.InputStream;
                        BinaryReader br = new BinaryReader(fs);
                        imgbyte = br.ReadBytes((Int32)fs.Length);
                        fs.Close();
                        string mime = MimeType.GetMimeType(imgbyte, file_word.FileName);
                        string[] filemimes1 = new string[] { "application/pdf", "application/x-msdownload", "application/unknown", "application/octet-stream" };
                        string[] filemimes = new string[] { "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/x-zip-compressed" };
                        if (filemimes1.Contains(mime))
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop1", "ShowDocDetails1();", true);
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' Please upload .doc or .docx files only ! <br/> The uploaded file is not valid or corrupted','warning');", true);
                            return;
                        }
                        if (!filemimes.Contains(mime))
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop1", "ShowDocDetails1();", true);
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' Please upload .doc or .docx files only ! <br/> The uploaded file is not valid or corrupted','warning');", true);
                            return;
                        }
                        if (ext == ".doc" || ext == ".docx")
                        {
                            Session["CreateFileUploadExtension"] = ext;
                            string strFileName = file_word.FileName;
                            string strTargPath = Server.MapPath(@"~/Files") + "\\" + strFileName;
                            file_word.PostedFile.SaveAs(strTargPath);
                            if (ext == ".doc")
                            {
                                RichEditDocumentServer server = new RichEditDocumentServer();
                                Document document = server.Document;
                                server.LoadDocument(strTargPath, DocumentFormat.Doc);
                                Section firstSection = document.Sections[0];
                                RichEditDocumentServer server1 = new RichEditDocumentServer();
                                Document document1 = server1.Document;
                                StringBuilder sbHFErrorMsg = new StringBuilder();
                                if (firstSection.HasHeader(HeaderFooterType.Primary) || firstSection.HasHeader(HeaderFooterType.First) || firstSection.HasHeader(HeaderFooterType.Even) || firstSection.HasHeader(HeaderFooterType.Odd))
                                {
                                    sbHFErrorMsg.Append("Upload the .docx file without header!" + "<br/>");
                                }
                                if (firstSection.HasFooter(HeaderFooterType.Primary) || firstSection.HasFooter(HeaderFooterType.First) || firstSection.HasFooter(HeaderFooterType.Even) || firstSection.HasFooter(HeaderFooterType.Odd))
                                {
                                    sbHFErrorMsg.Append("Upload the .docx file without footer!" + "<br/>");
                                }
                                if (sbHFErrorMsg.Length > 8)
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('" + sbHFErrorMsg.ToString() + "','warning');", true);
                                    return;
                                }
                                else
                                {
                                    document1.AppendDocumentContent(Server.MapPath(@"~/Files/WordSample2.docx"), DocumentFormat.OpenXml, Server.MapPath(@"~/Files/WordSample2.docx"), InsertOptions.KeepSourceFormatting);
                                    document1.AppendDocumentContent(strTargPath, DocumentFormat.Doc, strTargPath, InsertOptions.KeepSourceFormatting);
                                }
                                document1.SaveDocument(strTargPath, DocumentFormat.Doc);
                                Array.Clear(imgbyte, 0, imgbyte.Length);
                                fs = File.OpenRead(strTargPath);
                                BinaryReader br1 = new BinaryReader(fs);
                                imgbyte = br.ReadBytes((Int32)fs.Length);
                                fs.Close();
                            }
                            else if (ext == ".docx")
                            {
                                RichEditDocumentServer server = new RichEditDocumentServer();
                                Document document = server.Document;
                                server.LoadDocument(strTargPath, DocumentFormat.OpenXml);
                                Section firstSection = document.Sections[0];
                                RichEditDocumentServer server1 = new RichEditDocumentServer();
                                Document document1 = server1.Document;
                                StringBuilder sbHFErrorMsg = new StringBuilder();
                                if (firstSection.HasHeader(HeaderFooterType.Primary))
                                {
                                    sbHFErrorMsg.Append("Upload the .docx file without header!" + "<br/>");
                                }
                                if (firstSection.HasFooter(HeaderFooterType.Primary))
                                {
                                    sbHFErrorMsg.Append("Upload the .docx file without footer!" + "<br/>");
                                }
                                if (sbHFErrorMsg.Length > 8)
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('" + sbHFErrorMsg.ToString() + "','warning');", true);
                                    return;
                                }
                                else
                                {
                                    document1.AppendDocumentContent(Server.MapPath(@"~/Files/WordSample2.docx"), DocumentFormat.OpenXml, Server.MapPath(@"~/Files/WordSample2.docx"), InsertOptions.KeepSourceFormatting);
                                    document1.AppendDocumentContent(strTargPath, DocumentFormat.OpenXml, strTargPath, InsertOptions.KeepSourceFormatting);
                                }

                                document1.SaveDocument(strTargPath, DocumentFormat.OpenXml);
                                Array.Clear(imgbyte, 0, imgbyte.Length);
                                fs = File.OpenRead(strTargPath);
                                BinaryReader br1 = new BinaryReader(fs);
                                imgbyte = br1.ReadBytes((Int32)fs.Length);
                                fs.Close();
                            }
                            if (File.Exists(strTargPath))
                            {
                                // Note that no lock is put on the
                                // file and the possibility exists
                                // that another process could do
                                // something with it between
                                // the calls to Exists and Delete.
                                File.Delete(strTargPath);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop1", "ShowDocDetails1();", true);
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/> Please upload  .doc or .docx files only','warning');", true);
                            return;
                        }
                    }
                    Session["CreateFileUpload"] = imgbyte;
                    Session["word"] = file_word.FileName;
                }
                www.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowDocDetails();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>   Document Upload failed','error');", true);
            }
            finally
            {
                Btn_Upload.Enabled = true;
            }
        }
        #region Referrals
        protected void btn_Addreferrals_Click(object sender, EventArgs e)
        {
            btn_Addreferrals.Enabled = false;
            try
            {
                string DMS_DocumentID = hdfPKID.Value;
                txtLinkReferrences.InnerText = DMS_Bal.DMS_GetLinkReferences(Convert.ToInt32(DMS_DocumentID));
                DataTable dtInternalDocs = DMS_Bal.DMS_GetReferalInternalDocs(Convert.ToInt32(DMS_DocumentID));
                Session["dtAssignedDocuments"] = dtInternalDocs;
                gvAssignedDocuments.DataSource = dtInternalDocs;
                gvAssignedDocuments.DataBind();
                DataTable dtExternalDocs = DMS_Bal.DMS_GetReferalExternalFiles(Convert.ToInt32(DMS_DocumentID));
                gvExternalFiles.DataSource = dtExternalDocs;
                gvExternalFiles.DataBind();
                ViewState["dtExternalDocuments"] = dtExternalDocs;
                TempExternalFileBO objTemp = new TempExternalFileBO();
                objTemp.CrudType = 4;
                objTemp.Ref_FileID = 0;
                objTemp.FileName = "";
                objTemp.FileExtension = "";
                objTemp.FileContent = new byte[0];
                objTemp.DocExternal_VersionID = Convert.ToInt32(DMS_DocumentID);
                DataTable dt = DMS_Bal.DMS_TempCrudExternalFiles(objTemp);
                GetgvExternalFiles();
                getDocumentList(true);
                hdnGrdiReferesh.Value = "";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowRefferal();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>   Add Referrals failed','error');", true);
            }
            finally { btn_Addreferrals.Enabled = true; }
        }
        void resetReferrels()
        {
            try
            {
                txtLinkReferrences.InnerText = string.Empty;
                ddl_Dept.SelectedIndex = -1;
                Session["dtAvailableDocuments"] = null;
                Session["dtAssignedDocuments"] = null;
                gvAssignedDocuments.DataSource = null;
                gvAssignedDocuments.DataBind();
                gvExternalFiles.DataSource = null;
                gvExternalFiles.DataBind();
                gvAvailableDocuments.DataSource = null;
                gvAvailableDocuments.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/> Referrals resetting failed','error');", true);
            }
        }
        protected void ddl_Dept_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hdnGrdiReferesh.Value = "Internal";
                getDocumentList(true);
                upInternalDoc.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowRefferal();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>  Department failed','error');", true);
            }
        }
        public void getDocumentList(bool isSelectedIncex)
        {
            try
            {
                if (Session["dtAssignedDocuments"] == null)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.AddRange(new DataColumn[4] { new DataColumn("VersionID"), new DataColumn("DocumentName"), new DataColumn("DocumentNumber"), new DataColumn("DeptID") });
                    Session["dtAssignedDocuments"] = dt;
                }
                DataTable DMS_Dt = DMS_Bal.DMS_GetInternalDocumentsbyDepartment(Convert.ToInt32(ddl_Dept.SelectedValue));
                DataTable DMS_DT1 = GetDistinctAvailableDocuments((DataTable)Session["dtAssignedDocuments"], DMS_Dt);
                gvAvailableDocuments.DataSource = DMS_DT1;
                gvAvailableDocuments.DataBind();
                if (gvAvailableDocuments.Rows.Count > 0)
                {
                    gvAvailableDocuments.UseAccessibleHeader = true;
                    gvAvailableDocuments.HeaderRow.TableSection = TableRowSection.TableHeader;
                    gvAvailableDocuments.HeaderStyle.CssClass = "headerstyle";
                    if (ddl_Dept.SelectedIndex > 0)
                    {
                        //dvrow_DocSearch.Visible = true;//true
                    }
                }
                upInternalDoc.Update();
                Session["dtAvailableDocuments"] = DMS_DT1;
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>  DocumentList failed','error');", true);
            }
        }
        static DataTable GetDistinctAvailableDocuments(DataTable dtAssDoc, DataTable dtAvlDoc)
        {
            foreach (DataRow row in dtAssDoc.Rows)
            {
                string DocumentID = row["VersionID"].ToString();
                int Doccount = dtAvlDoc.AsEnumerable().Count(drow => drow.Field<int>("VersionID").ToString() == DocumentID);
                if (Doccount > 0)
                {
                    dtAvlDoc.Rows.Remove(dtAvlDoc.AsEnumerable().Where(r => r.Field<Int32>("VersionID").ToString() == DocumentID).FirstOrDefault());
                    dtAvlDoc.AcceptChanges();
                }
            }
            return dtAvlDoc;
        }
        protected void btn_RemoveDocuments_Click(object sender, EventArgs e)
        {
            btn_RemoveDocuments.Enabled = false;
            try
            {
                hdnGrdiReferesh.Value = "Internal";
                DataTable dt;
                if (Session["dtAssignedDocuments"] == null)
                {
                    dt = new DataTable();
                    dt.Columns.AddRange(new DataColumn[4] { new DataColumn("VersionID"), new DataColumn("DocumentName"), new DataColumn("DocumentNumber"), new DataColumn("DeptID") });
                }
                else
                    dt = (DataTable)Session["dtAssignedDocuments"];
                DataTable dtAssDoc = (DataTable)Session["dtAvailableDocuments"];
                foreach (GridViewRow row in gvAssignedDocuments.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox chkRow = (row.Cells[0].FindControl("cbxSelect") as CheckBox);
                        if (chkRow.Checked)
                        {
                            string DeptID = (row.Cells[0].FindControl("lbl_DeptID") as Label).Text;
                            string DocumentID = (row.Cells[1].FindControl("lbl_DocumentID") as Label).Text;
                            string DocumentNumber = (row.Cells[1].FindControl("lbl_DocumentNumber") as Label).Text;
                            string DocumentName = (row.Cells[2].FindControl("lbl_DocumentName") as Label).Text;
                            dt.Rows.Remove(dt.AsEnumerable().Where(r => r.Field<Int32>("VersionID").ToString() == DocumentID).FirstOrDefault());
                            dt.AcceptChanges();
                            foreach (GridViewRow row1 in gvAvailableDocuments.Rows)
                            {
                                if (row1.RowType == DataControlRowType.DataRow)
                                {
                                    string DeptID1 = (row1.Cells[0].FindControl("lbl_DeptID") as Label).Text;
                                    if (DeptID1 == DeptID)
                                    {
                                        dtAssDoc.Rows.Add(DocumentID, DocumentName, DocumentNumber, DeptID);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                gvAssignedDocuments.DataSource = dt;
                gvAssignedDocuments.DataBind();
                gvAvailableDocuments.DataSource = dtAssDoc;
                gvAvailableDocuments.DataBind();
                Session["dtAssignedDocuments"] = dt;
                Session["dtAvailableDocuments"] = dtAssDoc;
                if (gvAvailableDocuments.Rows.Count > 0)
                {
                    gvAvailableDocuments.UseAccessibleHeader = true;
                    gvAvailableDocuments.HeaderRow.TableSection = TableRowSection.TableHeader;
                    gvAvailableDocuments.HeaderStyle.CssClass = "headerstyle";
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowRefferal();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>  Document Delete failed','error');", true);
            }
            finally
            {
                btn_RemoveDocuments.Enabled = true;
            }
        }
        protected void btn_AddDocuments_Click(object sender, EventArgs e)
        {
            btn_AddDocuments.Enabled = false;
            try
            {
                hdnGrdiReferesh.Value = "Internal";
                DataTable dt;
                if (Session["dtAssignedDocuments"] == null)
                {
                    dt = new DataTable();
                    dt.Columns.AddRange(new DataColumn[4] { new DataColumn("VersionID"), new DataColumn("DocumentName"), new DataColumn("DocumentNumber"), new DataColumn("DeptID") });
                }
                else
                    dt = (DataTable)Session["dtAssignedDocuments"];
                DataTable dtAssDoc = (DataTable)Session["dtAvailableDocuments"];
                foreach (GridViewRow row in gvAvailableDocuments.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox chkRow = (row.Cells[0].FindControl("cbxSelect") as CheckBox);
                        if (chkRow.Checked)
                        {
                            string DocumentNumber = (row.Cells[1].FindControl("lbl_DocumentNumber") as Label).Text;
                            string DocumentID = (row.Cells[1].FindControl("lbl_DocumentID") as Label).Text;
                            string DocumentName = (row.Cells[2].FindControl("lbl_DocumentName") as Label).Text;
                            string DeptID = (row.Cells[2].FindControl("lbl_DeptID") as Label).Text;
                            dt.Rows.Add(DocumentID, DocumentName, DocumentNumber, DeptID);
                            dtAssDoc.Rows.Remove(dtAssDoc.AsEnumerable().Where(r => r.Field<Int32>("VersionID").ToString() == DocumentID).FirstOrDefault());
                            dtAssDoc.AcceptChanges();
                        }
                    }
                }
                gvAssignedDocuments.DataSource = dt;
                gvAssignedDocuments.DataBind();
                gvAvailableDocuments.DataSource = dtAssDoc;
                gvAvailableDocuments.DataBind();
                Session["dtAvailableDocuments"] = dtAssDoc;
                Session["dtAssignedDocuments"] = dt;
                if (gvAvailableDocuments.Rows.Count > 0)
                {
                    gvAvailableDocuments.UseAccessibleHeader = true;
                    gvAvailableDocuments.HeaderRow.TableSection = TableRowSection.TableHeader;
                    gvAvailableDocuments.HeaderStyle.CssClass = "headerstyle";

                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowRefferal();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/> Add Document failed','error');", true);
            }
            finally
            {
                btn_AddDocuments.Enabled = true;
            }
        }
        protected void btnFileUpload_Click(object sender, EventArgs e)
        {
            btnFileUpload.Enabled = false;
            try
            {
                if (FileUpload1.PostedFiles.Count > 0)
                {
                    hdnGrdiReferesh.Value = "External";
                    string DMS_DocumentID = hdfPKID.Value;
                    foreach (HttpPostedFile postedFile in FileUpload1.PostedFiles)
                    {
                        string fileName = Path.GetFileName(postedFile.FileName);
                        if (!string.IsNullOrEmpty(fileName))
                        {
                            string ext = System.IO.Path.GetExtension(FileUpload1.FileName);
                            Stream fs = postedFile.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            byte[] binData = br.ReadBytes((Int32)fs.Length);
                            string mime = MimeType.GetMimeType(binData, fileName);
                            string[] filemimes = new string[] { "application/x-msdownload", "application/unknown", "application/octet-stream", "application/x-shockwave-flash", "application/x-rar-compressed",
                             "video/x-msvideo" ,"audio/x-wav", "audio/x-ms-wma" ,"video/x-ms-wmv"};
                            if (filemimes.Contains(mime))
                            {
                                Session["CreateFileUpload"] = null;
                                Session["word"] = null;
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowRefferal1();", true);
                                HelpClass.custAlertMsg(this, this.GetType(), "uploaded file is not valid or corrupted.", "warning");
                                return;
                            }
                            if (DMS_Bal.DMS_GetExistingFileCount(postedFile.FileName) > 0)
                            {
                                // validation for permentant table
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowRefferal1();", true);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "custAlertMsg(' <br/> " + fileName + "  name already exists.','error');", true);
                                return;
                            }
                            else
                            {

                                bool isDuplicate = false;
                                TempExternalFileBO objTemp = new TempExternalFileBO();
                                objTemp.CrudType = 5;
                                objTemp.Ref_FileID = 0;
                                objTemp.FileName = postedFile.FileName;
                                objTemp.FileExtension = "";
                                objTemp.FileContent = new byte[0];
                                objTemp.DocExternal_VersionID = 0;
                                DataTable dt = DMS_Bal.DMS_TempCrudExternalFiles(objTemp);
                                if (Convert.ToInt32(dt.Rows[0][0].ToString()) > 0)
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "custAlertMsg(' <br/> File " + fileName + "  name already exists.','error');", true);
                                    isDuplicate = true;
                                }
                                if (isDuplicate == false)
                                {
                                    // insert to temp table
                                    TempExternalFileBO objTemp1 = new TempExternalFileBO();
                                    objTemp1.CrudType = 1;
                                    objTemp1.Ref_FileID = 0;
                                    objTemp1.FileName = postedFile.FileName;
                                    objTemp1.FileExtension = ext;
                                    objTemp1.FileContent = binData;
                                    objTemp1.DocExternal_VersionID = Convert.ToInt32(DMS_DocumentID);
                                    DataTable dt1 = DMS_Bal.DMS_TempCrudExternalFiles(objTemp1);
                                }
                            }
                        }
                    }
                    GetgvExternalFiles();
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowRefferal();", true);
                UpdatePanel8.Update();
                if (hdnGrdiReferesh.Value == "External")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "poptab", "TabRefClick ();", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/> File Upload failed','error');", true);
            }
            finally
            {
                btnFileUpload.Enabled = true;
            }
        }
        void GetgvExternalFiles()
        {
            try
            {
                string DMS_DocumentID = hdfPKID.Value;
                //get from permenant table
                DataTable dtExternalDocs = DMS_Bal.DMS_GetReferalExternalFiles(Convert.ToInt32(DMS_DocumentID));
                ViewState["dtExternalDocuments"] = dtExternalDocs;
                //get temp table files and add to datatable
                TempExternalFileBO objTemp = new TempExternalFileBO();
                objTemp.CrudType = 3;
                objTemp.Ref_FileID = 0;
                objTemp.FileName = "";
                objTemp.FileExtension = "";
                objTemp.FileContent = new byte[0];
                objTemp.DocExternal_VersionID = Convert.ToInt32(DMS_DocumentID);
                DataTable dtTempExternalDocs = DMS_Bal.DMS_TempCrudExternalFiles(objTemp);
                foreach (DataRow dr in dtTempExternalDocs.Rows)
                {
                    dtExternalDocs.Rows.Add(0, dr["FileName"].ToString(), "", "", "", dr["DocExternal_VersionID"].ToString());
                }
                if (dtExternalDocs.Rows.Count > 0)
                {
                    gvExternalFiles.DataSource = dtExternalDocs;
                    gvExternalFiles.DataBind();
                }
                else
                {
                    gvExternalFiles.DataSource = new List<object>();
                    gvExternalFiles.DataBind();
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/> External files failed','error');", true);
            }
        }
        protected void DownloadFile(object sender, EventArgs e)
        {
            try
            {
                string filePath = (sender as LinkButton).CommandArgument;
                //view temp file or permenant file
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowExternalFile();", true);
                dviframe.InnerHtml = "&nbsp";
                dviframe.InnerHtml = "<iframe src=\"" + ResolveUrl("~/DMS/DocumentCreator/CreateRefferalInIFrame.aspx") + "?FilePath=" + filePath + "#toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" height=\"690px\" ></iframe>";
                UpdatePanel4.Update();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/> Download File failed','error');", true);
            }
        }
        protected void DeleteFile(object sender, EventArgs e)
        {
            try
            {
                string filePath = (sender as LinkButton).CommandArgument;
                string[] stringSeparators = new string[] { "@$" };
                string[] result = filePath.Split(stringSeparators, StringSplitOptions.None);

                if (result[1] == "0")
                {
                    // temp delete
                    TempExternalFileBO objTemp = new TempExternalFileBO();
                    objTemp.CrudType = 4;
                    objTemp.Ref_FileID = 0;
                    objTemp.FileName = result[0];
                    objTemp.FileExtension = "";
                    objTemp.FileContent = new byte[0];
                    objTemp.DocExternal_VersionID = Convert.ToInt32(result[2]);
                    DataTable dtTempExternalDocs = DMS_Bal.DMS_TempCrudExternalFiles(objTemp);
                }
                else
                {
                    // permenant delete
                    int del = DMS_Bal.DMS_DeleteExternalFiles(Convert.ToInt32(result[1]));
                }
                GetgvExternalFiles();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowRefferal();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/> Delete file failed','error');", true);
            }
        }
        protected void gvExternalFiles_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string _pkid = (e.Row.Cells[1].FindControl("lbl_pkid") as Label).Text;
                    if (Convert.ToInt32(_pkid) > 0)
                    {
                        LinkButton _lnkDelete = (e.Row.Cells[2].FindControl("lnkDelete") as LinkButton);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void DeleteReferenceFiles()
        {
            try
            {
                string[] filePaths = Directory.GetFiles(Server.MapPath("~/ReferenceFiles/"));
                foreach (string filePath in filePaths)
                {
                    File.Delete(filePath);
                }
                string[] filePaths1 = Directory.GetFiles(Server.MapPath("~/DownloadReferrences/"));
                foreach (string filePath1 in filePaths1)
                {
                    File.Delete(filePath1);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/> Referrals deletion failed','error');", true);
            }
        }
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetDocListByNameorNumber(string prefixText)
        {
            List<string> _lstdistinctDocNames = new List<string>();
            if (HttpContext.Current.Session["dtAvailableDocuments"] != null)
            {
                DataTable DMS_DT1 = (DataTable)HttpContext.Current.Session["dtAvailableDocuments"];
                var distinctDocName = (from row in DMS_DT1.AsEnumerable()
                                       where row.Field<string>("DocumentNumber").ToLower().Contains(prefixText.ToLower()) || row.Field<string>("DocumentName").ToLower().Contains(prefixText.ToLower())
                                       select (row.Field<string>("DocumentNumber") + " " + row.Field<string>("DocumentName"))).Distinct();
                if (distinctDocName != null)
                    _lstdistinctDocNames = distinctDocName.ToList();
            }
            return _lstdistinctDocNames;
        }
        protected void btn_Add_Referrences_Click(object sender, EventArgs e)
        {
            btn_Add_Referrences.Enabled = false;
            try
            {
                string DMS_DocumentID = hdfPKID.Value;
                ReferralBO obj_ReferralBO = new ReferralBO();
                obj_ReferralBO.Doc_VersionID = Convert.ToInt32(DMS_DocumentID);
                obj_ReferralBO.Doc_RefContent = (txtLinkReferrences.InnerText);
                int resultLnk = DMS_Bal.DMS_insertLinkReferences(obj_ReferralBO);
                List<ReferralBO> lst_ReferralBO = new List<ReferralBO>();
                foreach (GridViewRow rowInt in gvAssignedDocuments.Rows)
                {
                    if (rowInt.RowType == DataControlRowType.DataRow)
                    {
                        string RefDocumentID = (rowInt.Cells[1].FindControl("lbl_DocumentID") as Label).Text;
                        ReferralBO obj_ReferralBOInternal = new ReferralBO();
                        obj_ReferralBOInternal.Doc_VersionID = Convert.ToInt32(DMS_DocumentID);
                        obj_ReferralBOInternal.Doc_Ref_VersionID = Convert.ToInt32(RefDocumentID);
                        lst_ReferralBO.Add(obj_ReferralBOInternal);
                    }
                }
                int resultInternal = DMS_Bal.DMS_insertReferalInternalDocs(lst_ReferralBO);
                TempExternalFileBO objTemp = new TempExternalFileBO();
                objTemp.CrudType = 3;
                objTemp.Ref_FileID = 0;
                objTemp.FileName = "";
                objTemp.FileExtension = "";
                objTemp.FileContent = new byte[0];
                objTemp.DocExternal_VersionID = Convert.ToInt32(DMS_DocumentID);
                DataTable dtTempExternalDocs = DMS_Bal.DMS_TempCrudExternalFiles(objTemp);
                foreach (DataRow dr in dtTempExternalDocs.Rows)
                {
                    ReferralBO obj_ReferralBOExternal = new ReferralBO();
                    obj_ReferralBOExternal.Doc_VersionID = Convert.ToInt32(DMS_DocumentID);
                    obj_ReferralBOExternal.Doc_FileName = dr["FileName"].ToString();
                    obj_ReferralBOExternal.Doc_FileExtension = dr["FileExtension"].ToString();
                    obj_ReferralBOExternal.Doc_FileContent = new byte[0];
                    int resultExternal = DMS_Bal.DMS_insertReferalExternalFiles(obj_ReferralBOExternal);
                }
                HelpClass.custAlertMsg(this, this.GetType(), "Referrals saved sucssesfully.", "success");
                if (hdnGrdiReferesh.Value == "Link")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tab1", "TabRefClick();", true);
                }
                if (hdnGrdiReferesh.Value == "Internal")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tab2", "TabRefClick();", true);
                }
                if (hdnGrdiReferesh.Value == "External")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tab3", "TabRefClick();", true);
                }
                asdqwe.Update();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/> Add ReferenceFiles failed','error');", true);
            }
            finally { btn_Add_Referrences.Enabled = true; }
        }
        protected void btn_Refferals_Cancel_Click(object sender, EventArgs e)
        {
            btn_Refferals_Cancel.Enabled = false;
            try
            {
                resetReferrels();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "closeReffers();", true);
                UpdatePanel5.Update();
                upInternalDoc.Update();
            }
            catch { }
            finally { btn_Refferals_Cancel.Enabled = true; }
        }
        protected void gvAvailableDocuments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvAvailableDocuments.PageIndex = e.NewPageIndex;
            ddl_Dept_SelectedIndexChanged(null, null);
        }
        protected void gvAssignedDocuments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvAssignedDocuments.PageIndex = e.NewPageIndex;
                gvAssignedDocuments.DataSource = (DataTable)Session["dtAssignedDocuments"];
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowRefferal();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    Internal Documents Page Index Changing failed','error');", true);
            }
        }
        #endregion Refferals
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        protected void btnRefClose_Click(object sender, EventArgs e)
        {
            resetReferrels();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "pop1", "ShowDocDetails();", true);
        }
        private void InitializeThePage()
        {
            try
            {
                DMS_GetTemplateNameList();
                DMS_GetDepartmentList();
                if (Request.QueryString["DocumentID"] != null)
                {
                    int DocumentID = Convert.ToInt32(Request.QueryString["DocumentID"]);
                    RetriveDocumentDetails(DocumentID);
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    Page Initialization  failed','error');", true);
            }
        }
        private void RetriveDocumentDetails(int DocumentID)
        {
            try
            {
                Session["CreateFileUpload"] = null;
                Session["CreateFileUploadExtension"] = null;
                hdfPKID.Value = DocumentID.ToString();
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                if (DMS_DT.Rows.Count > 0)
                {
                    txt_requestType.Text = DMS_DT.Rows[0]["RequestType"].ToString();
                    txt_documentType.Text = DMS_DT.Rows[0]["DocumentType"].ToString();
                    txt_Department.Text = DMS_DT.Rows[0]["Department"].ToString();
                    txt_DocumentNumber.Text = DMS_DT.Rows[0]["DocumentNumber"].ToString();
                    txt_DocumentName.Text = DMS_DT.Rows[0]["DocumentName"].ToString();
                    if (txt_requestType.Text == "New Document")
                    {
                        lblComments.Text = "Purpose of Initiation";
                        txt_Comments.Text = DMS_DT.Rows[0]["CreatePurpose"].ToString();
                        txt_VersionID.Text = "00";
                    }
                    else if (txt_requestType.Text == "Revision Of Existing Document")
                    {
                        lblComments.Text = "Reason for Change";
                        txt_Comments.Text = DMS_DT.Rows[0]["CreatePurpose"].ToString();
                        txt_VersionID.Text = DMS_DT.Rows[0]["versionNumber"].ToString();
                    }
                    txt_Author.Text = DMS_DT.Rows[0]["CreatorID"].ToString();
                    txt_Authorizer.Text = DMS_DT.Rows[0]["AuthorizedBy"].ToString();
                    if (txt_Authorizer.Text == "")
                    {
                        div1.Visible = false;
                    }
                    else
                    {
                        div1.Visible = true;
                    }
                    DataTable aprrove_list = DMS_Bal.DMS_GetApproverReviewerSelectedList(hdfPKID.Value, 5);
                    ViewState["ApproverData"] = aprrove_list;
                    gvApprover.DataSource = aprrove_list;
                    gvApprover.DataBind();
                    Rejected_comments();
                    asdqwe.Update();
                    UpMyDocFiles.Update();
                    if (Request.QueryString["tab"] == "1")
                    {
                        ddl_Template.SelectedIndex = -1;
                        btn_Submit.Text = "Create";
                        if (DMS_DT.Rows[0]["Content"] == DBNull.Value)
                        {
                            lbl_Template.Visible = true;
                            ddl_Template.Visible = true;
                            WordUpload.Visible = false;
                        }
                        else
                        {
                            lbl_Template.Visible = false;
                            ddl_Template.Visible = false;
                        }
                        if (DMS_DT.Rows[0]["FileType"].ToString() == "pdf")
                        {
                            lbl_Template.Visible = false;
                            ddl_Template.Visible = false;
                            WordUpload.Visible = true;
                        }
                        else
                        {
                            WordUpload.Visible = false;
                        }
                        resetReferrels();
                    }
                    else if (Request.QueryString["tab"] == "2")
                    {
                        lbl_Template.Visible = false;
                        ddl_Template.Visible = false;
                        WordUpload.Visible = false;
                        btn_Submit.Text = "Modify";
                    }
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    Retrive Document Details  failed','error');", true);
            }
        }
        protected void ASPxRichEdit2_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            string path = MapPath(@"~\\App_Data\\WorkDirectory") + "\\" + e.Parameter;
            using (MemoryStream stream = new MemoryStream())
            {
                using (RichEditDocumentServer server = new RichEditDocumentServer())
                {
                    server.LoadDocument(path);
                    server.Document.Sections[0].Page.PaperKind = System.Drawing.Printing.PaperKind.Custom;
                    server.Document.Sections[0].Page.Width = 105f;
                    server.Document.Sections[0].Page.Height = 297f;
                    server.SaveDocument(stream, DocumentFormat.OpenXml);
                    stream.Position = 0;
                    DocumentManager.CloseDocument("uploadedDocId");
                    ASPxRichEdit2.Open("uploadedDocId", DocumentFormat.OpenXml, () => stream);
                }
            }
        }
    }
}