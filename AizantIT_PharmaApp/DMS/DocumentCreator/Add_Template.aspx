﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="Add_Template.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentCreator.Add_Template" %>

<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="padding-none">
        <asp:Button ID="btnAdd" runat="server" class="pull-right btn btn-approve_popup" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
    </div>
    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 TMS_AuthorizationError" id="divAutorizedMsg" runat="server" visible="false">
        <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
    </div>
    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none" id="divMainContainer" runat="server" visible="true">
        <div class="panel panel-default">
            <div class="panel-heading dms_header">
                Template Creation
            </div>
            <div class="panel-body">
                <asp:UpdatePanel ID="asdsd" runat="server">
                    <ContentTemplate>
                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12  dms_outer_border_temp bottom">
                            <div class="col-md-5 col-lg-5 col-xs-6 col-sm-6  pull-left bottom top" runat="server">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none  pull-left">
                                    <b class="template_label ">Template Name : </b><span id="spanComments" class="smallred">*</span>
                                </div>
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none  pull-left">
                                    <asp:TextBox ID="txt_TemplateName" runat="server" CssClass="form-control" placeholder="Enter Template Name" MaxLength="200" autocomplete="off" TabIndex="1"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4  top pull-left " id="ProcessType" runat="server">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 pull-left"><b class="template_label">Upload File : </b><span class="smallred">*</span></div>
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 pull-left">
                                    <asp:FileUpload ID="File_Template" CssClass="" runat="server" TabIndex="2" />
                                </div>
                            </div>
                            <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12 top" style="margin-top: 30px;">
                                <asp:Button ID="btn_tSubmit" runat="server" Text="Submit" CssClass="btn btn-approve_popup" OnClientClick="return TemplateValidation();" OnClick="btn_tSubmit_Click" TabIndex="3" />
                                <asp:Button ID="btn_Reset" runat="server" Text="Reset" CssClass="btn  btn-canceldms_popup" OnClientClick="ResetTemplate();" OnClick="btn_Reset_Click" TabIndex="4" />
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btn_tSubmit" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <div class=" col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none dms_outer_border_temp">
                <div class=" col-md-12 col-lg-12 col-xs-12 col-sm-12 ">
                    <div class=" col-md-12 col-lg-12 col-xs-12 col-sm-12 padding-none pull-right ">
                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 grid_panel_full padding-none">
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 top bottom">
                                <table id="dtDocType" class="display" cellspacing="0" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Template Id</th>
                                            <th>Template Name</th>
                                            <th>Created By</th>
                                            <th>View</th>
                                            <th>Remove</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-------- Template VIEW-------------->
    <div id="myModal_lock" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 80%; height: 500px">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpHeading">
                        <ContentTemplate>
                            <span id="span_FileTitle" runat="server"></span>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="dviframe" runat="server">
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <!--------End Template VIEW-------------->
    <uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />

    <asp:HiddenField ID="hdfPKID" runat="server" />
    <asp:HiddenField ID="hdfEmpID" runat="server" />
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTemplateDel" runat="server" Text="Remove" Style="display: none" OnClientClick="return confirm()" />
            <asp:Button ID="btnTemplateView" runat="server" Text="View" Style="display: none" OnClick="btnTemplateView_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>
        $('#dtDocType').wrap('<div class="dataTables_scroll" />');
        var tblReload = $('#dtDocType').DataTable({
            columns: [
                { 'data': 'RowNumber' },
                { 'data': 'TemplateID' },
                { 'data': 'TemplateName' },
                { 'data': 'CreatedBy' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a class="view" title="View" href="#" onclick=ViewDocument(' + o.TemplateID + ')></a>'; }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a href="#" class="delete_icon" onclick=DeleteDocument(' + o.TemplateID + ')><i class="glyphicon glyphicon-trash" title="Remove" style="color:red;"></i></a>'; }
                }
            ],
            "aoColumnDefs": [{ "targets": [1, 3], "visible": false, "searchable": false }, { "targets": [1], "visible": false, "searchable": false }, { "bSortable": false, "aTargets": [0, 1, 4] }, { "className": "dt-body-left", "targets": [2] },],
            "order": [[1, "desc"]],
            'bAutoWidth': true,
            sServerMethod: 'Post',
            "sAjaxSource": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetAddTemplates" )%>',
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push();
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#dtDocType").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
        function reloadtable() {
            tblReload.ajax.reload();
            $("#myModal_lock").modal('hide');
        }
    </script>
    <script>
        function DeleteDocument(PK_ID) {
            document.getElementById("<%=hdfPKID.ClientID%>").value = PK_ID;
            var btnFV = document.getElementById("<%=btnTemplateDel.ClientID %>");
            btnFV.click();
        }
        function confirm() {
            ConformAlert();
        }
        function ViewDocument(PK_ID) {
            document.getElementById("<%=hdfPKID.ClientID%>").value = PK_ID;
            var btnTV = document.getElementById("<%=btnTemplateView.ClientID %>");
            btnTV.click();
        }
        function TemplateValidation() {
            var TemplateName = document.getElementById("<%=txt_TemplateName.ClientID%>").value;
            var File_Templae = document.getElementById("<%=File_Template.ClientID%>").value;
            errors = [];
            if (TemplateName == "") {
                errors.push(" Enter  Template Name");
            }
            if (File_Templae == "") {
                errors.push("Please Upload a File");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }
        function ResetTemplate() {
            document.getElementById("<%=txt_TemplateName.ClientID%>").value = "";
            document.getElementById("<%=File_Template.ClientID%>").value = "";
        }
    </script>
    <script>
        function ConformAlert() {
            custAlertMsg('Are you sure you want to Remove this Template ?', 'confirm', true);
        }
        function CloseBrowser() {
            window.close();
        }
    </script>
</asp:Content>
