﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="Existing_Documents.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentCreator.Existing_Documents" %>

<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ScriptManager>
   
    <div class="padding-none">
        <asp:Button ID="btnAdd" runat="server" class="float-right  btn-signup_popup" PostBackUrl="~/DMS/DocumentCreator/Existing_DocumentList.aspx" Text="Document List" />
    </div>
    <div class=" col-md-12 col-lg-12 col-12 col-sm-12 dms_outer_border float-left padding-none">
        <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server" visible="false">
            <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
        </div>
        <div id="divMainContainer" runat="server" visible="true">
            <div class=" col-12  padding-none float-right ">
                <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left">
                    <div class="grid_header float-left col-12" id="div_PageTitle" runat="server">
                    </div>
                    <div class="col-12 float-left grid_panel_full bottom border_top_none">
                    <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none top float-left">
                        <div class="form-group col-md-6 col-lg-6 col-6 col-sm-6 float-left ">
                            <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left">
                                <label class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none">Department<span class="smallred_label">*</span></label>
                                <asp:DropDownList ID="ddl_DeptName" runat="server" data-size="9" data-live-search="true" CssClass="form-control col-md-12 col-lg-12 col-12 col-sm-12 selectpicker  regulatory_dropdown_style drop_down" Style="width: 100%;" TabIndex="1" AutoPostBack="true" OnSelectedIndexChanged="ddl_DeptName_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group col-md-6 col-lg-6 col-6 col-sm-6 float-left">
                            <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left">
                                <label class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none ">Document Type<span class="smallred_label">*</span></label>
                                <asp:DropDownList ID="ddl_DocumentType" runat="server" data-size="9" data-live-search="true" Style="width: 100%;" CssClass="form-control col-md-12 col-lg-12 col-12 col-sm-12 selectpicker  regulatory_dropdown_style drop_down" TabIndex="2" AutoPostBack="true" OnSelectedIndexChanged="ddl_DocumentType_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group col-sm-6 float-left">
                            <label for="lbl_DocNumber" class=" padding-none col-sm-12 control-label custom_label_answer">Document Number<span class="smallred_label">*</span></label>
                            <div class="col-sm-12 padding-none">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txt_DocumentNumber" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Document Number" AutoPostBack="true" OnTextChanged="txt_DocumentNumber_TextChanged" TabIndex="3" autocomplete="off" MaxLength="30" onkeypress="return RestrictDirectoryCharacters(event);" onpaste="return RestrictDirectoryCharactersOnPaste(event);"></asp:TextBox>
                                        <asp:Label ID="lbl_number" runat="server" CssClass="label_status" Visible="false"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="form-group col-sm-6 float-left">
                            <label class="padding-none col-sm-12 control-label custom_label_answer">Version<span class="smallred_label">*</span></label>
                            <div class="col-sm-12 padding-none">
                                <asp:TextBox ID="txt_Version" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Version Number" TabIndex="4" onkeypress="return restrictAlphabets(event);" min="0" max="999" TextMode="Number" MaxLength="3" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-sm-12 float-left">
                            <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">Document Name<span class="smallred_label">*</span></label>
                            <div class="col-sm-12 padding-none">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                                    <ContentTemplate>
                                        <textarea ID="txt_DocumentName" runat="server" class="form-control" placeholder="Enter Document Name" TabIndex="5" MaxLength="300" onkeypress="return RestrictStarFrontSlashSquareBraces(event);" onpaste="return ValidateStarFrontSlashSquareBracesOnPaste(event);"></textarea>
                                        <asp:Label ID="lblStatus" runat="server" CssClass="label_status" Visible="false"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="form-group col-sm-6 float-left">
                            <label class=" padding-none col-sm-12 control-label custom_label_answer">Prepared By<span class="smallred_label">*</span></label>
                            <div class="col-sm-12 padding-none">
                                <asp:TextBox ID="txt_PreparedBy" runat="server" CssClass="form-control login_input_sign_up " placeholder="Enter Prepared BY" TabIndex="6" MaxLength="100" onkeypress="return RestrictPercentageSquareBraces(event);"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-sm-6 float-left">
                            <label class=" padding-none col-sm-12 control-label custom_label_answer">Prepared Date<span class="smallred_label">*</span></label>
                            <div class="col-sm-12 padding-none">
                                <asp:TextBox ID="txt_PreparedDate" runat="server" CssClass="form-control login_input_sign_up" placeholder="dd MMM yyyy" TabIndex="7" autocomplete="off"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-sm-6 float-left">
                            <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">Reviewed By<span class="smallred_label">*</span></label>
                            <div class="col-sm-12 padding-none">
                                <asp:TextBox ID="txt_ReviewedBY" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Reviewed BY" TabIndex="8" MaxLength="100" onkeypress="return RestrictPercentageSquareBraces(event);" onchanged="renewYears();"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-sm-6 float-left">
                            <label for="inputEmail3" class=" padding-none col-sm-12 control-label custom_label_answer">Reviewed Date<span class="smallred_label">*</span></label>
                            <div class="col-sm-12 padding-none">
                                <asp:TextBox ID="txt_ReviewDate" runat="server" CssClass="form-control login_input_sign_up" placeholder="dd MMM yyyy" TabIndex="9" autocomplete="off"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-sm-6 float-left">
                            <label class="padding-none col-sm-12 control-label custom_label_answer">Approved By<span class="smallred_label">*</span></label>
                            <div class="col-sm-12 padding-none">
                                <asp:TextBox ID="txt_ApprovedBY" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Approved BY" TabIndex="10" MaxLength="100" onkeypress="return RestrictPercentageSquareBraces(event);"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-sm-6 float-left">
                            <label class=" padding-none col-sm-12 control-label custom_label_answer">Approved Date<span class="smallred_label">*</span></label>
                            <div class="col-sm-12 padding-none">
                                <asp:TextBox ID="txt_ApproveDate" runat="server" CssClass="form-control login_input_sign_up" placeholder="dd MMM yyyy" TabIndex="11" autocomplete="off"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-sm-6 float-left">
                            <label class=" padding-none col-sm-12 control-label custom_label_answer">Effective Date<span class="smallred_label">*</span></label>
                            <div class="col-sm-12 padding-none">
                                <asp:TextBox ID="txt_EffectiveDate" runat="server" placeholder="dd MMM yyyy" CssClass="form-control login_input_sign_up" TabIndex="12" autocomplete="off"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-sm-6  float-left">
                            <label class=" padding-none col-sm-12 control-label custom_label_answer">Review Date<span id="reviewdate" runat="server" visible="false" class="smallred_label">*</span></label>
                            <div class="col-sm-12 padding-none">
                                <asp:TextBox ID="txt_ExpirationDate" runat="server" CssClass="form-control login_input_sign_up" placeholder="dd MMM yyyy" TabIndex="13" autocomplete="off"></asp:TextBox>
                                <label class=" padding-none col-sm-12" style="color:black !important" id="lblExpiationDate" runat="server" visible="false">N/A</label>
                            </div>
                        </div>
                        <div class="form-group col-lg-6 float-left">
                        <div class="top col-lg-3 float-left padding-none" id="uploadDoc">
                            <asp:UpdatePanel ID="www" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="file upload-button  btn btn-primary col-sm-12 col-12 col-lg-12 col-md-12" id="dvFup" runat="server" style="margin-top: 0px !important;">Upload<asp:FileUpload class="upload-button_input" ID="file_word" runat="server" onchange="UploadClicK()" accept=".pdf" ToolTip="Click to Upload Form" /></div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="top col-lg-9 float-left" id="divViewDoc">
                            <label>View Doc : </label>
                            <asp:LinkButton ID="lb_Upload" runat="server" OnClick="lb_Upload_Click" ForeColor="OrangeRed" Font-Bold="true"></asp:LinkButton>
                        </div>
                        </div>
                        <div class="form-group col-sm-12 col-12 col-lg-6 col-md-12 bottom top float-left" data-toggle="buttons" id="divOpinion" runat="server">
                            <label for="inputPassword3" class="float-left padding-right_div control-label">Privacy :<span class="smallred_label">*</span></label>
                            <label class=" btn btn-primary_radio col-6 col-lg-4 col-md-6 col-sm-6 active float-left  radio_check" id="lblRadioPublic">
                                <asp:RadioButton ID="rbtnYes" value="Yes" runat="server" GroupName="A" Text="Public" Checked="true" CssClass=""/>
                            </label>
                            <label class="btn btn-primary_radio col-6 col-lg-4 col-md-6 col-sm-6 float-left radio_check" id="lblRadioPrivate">
                                <asp:RadioButton ID="rbtnNo" value="No" runat="server" GroupName="A" Text="Private" />
                            </label>
                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-12 col-sm-12  float-right top bottom">
                            <asp:Button ID="btn_Close" Text="Cancel" Class="float-right  btn-cancel_popup bottom" runat="server" PostBackUrl="~/DMS/DocumentCreator/Existing_DocumentList.aspx" TabIndex="18" style="margin-left:2px;"/>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Button ID="btn_Reset" Text="Reset" Class="float-right  btn-revert_popup bottom" runat="server" OnClientClick="redirectself();" TabIndex="17" style="margin-left:2px;" />
                                    <asp:Button ID="btn_Submit" Text="Submit" Class="float-right  btn-signup_popup bottom" runat="server" OnClientClick="ConfirmAlert();" ValidationGroup="save" TabIndex="16" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <!--------SOP VIEW-------------->
    <div id="myModal_lock" class="modal department fade" data-backdrop="static" role="dialog">
        <div class=" modal-dialog modal-lg" style="min-width:90%;">
                    <div class="dms_sop_list ">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    
                    <span id="span_FileTitle" runat="server"></span>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                
              </div>
                <div class="modal-body">
                    <div id="divPDF_Viewer">
                        <noscript>
                            Please Enable JavaScript.
                        </noscript>
                    </div>
                </div>
            </div>
        </div>
            </div>
    </div>
    <!-------- End SOP VIEW-------------->
    <asp:HiddenField ID="hdfPkID" runat="server" />
    <asp:HiddenField ID="hdfViewType" runat="server" />
    <asp:HiddenField ID="hdfViewEmpID" runat="server" />
    <asp:HiddenField ID="hdfViewRoleID" runat="server" />
    <asp:HiddenField ID="hdfViewDivID" runat="server" />
    <asp:HiddenField ID="hdfFileName" runat="server" />
    <asp:HiddenField ID="hdfConfirm" runat="server" />
    <asp:HiddenField ID="hdfReviewYear" runat="server" />
    <asp:Button ID="btnUpload" runat="server" Text="Button" OnClick="btnUpload_Click" Style="display: none" />
    <asp:Button ID="btnNameChanged" runat="server" Text="Button" OnClick="txt_DocumentName_TextChanged" Style="display: none" />
    <uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />
    <script>
        //for start date and end date validations startdate should be < enddate
        $(function () {           
            $('#<%=txt_PreparedDate.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                useCurrent: false,
            });
            //var Prepdate = document.getElementById('<%=txt_PreparedDate.ClientID%>').value;
            $('#<%=txt_ReviewDate.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                useCurrent: false,
            });
            $('#<%=txt_ApproveDate.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                useCurrent: false,
            });
            $('#<%=txt_EffectiveDate.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                useCurrent: false,
            });
            $('#<%=txt_ExpirationDate.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                useCurrent: false,
            });
            $('#<%=txt_PreparedDate.ClientID%>').on("dp.change", function (e) {
                $('#<%=txt_ReviewDate.ClientID%>').data("DateTimePicker").minDate(moment(e.date).format('DD MMM YYYY'));
                $('#<%=txt_ReviewDate.ClientID%>').val("");
                //$('#<%=txt_ApproveDate.ClientID%>').data("DateTimePicker").minDate(e.date);
                $('#<%=txt_ApproveDate.ClientID%>').val("");
                //$('#<%=txt_EffectiveDate.ClientID%>').data("DateTimePicker").minDate(e.date);
                $('#<%=txt_EffectiveDate.ClientID%>').val("");
                //$('#<%=txt_ExpirationDate.ClientID%>').data("DateTimePicker").minDate(e.date);
                $('#<%=txt_ExpirationDate.ClientID%>').val("");
            });
            $('#<%=txt_ReviewDate.ClientID%>').on("dp.change", function (e) {
                var _mindate = new Date(moment(e.date).format('DD MMM YYYY'));
                _mindate.setDate(_mindate.getDate());
                $('#<%=txt_ApproveDate.ClientID%>').data("DateTimePicker").minDate(_mindate);
                $('#<%=txt_ApproveDate.ClientID%>').val("");
                $('#<%=txt_EffectiveDate.ClientID%>').val("");
                $('#<%=txt_ExpirationDate.ClientID%>').val("");
            });
            $('#<%=txt_ApproveDate.ClientID%>').on("dp.change", function (e) {
                var _mindate = new Date(moment(e.date).format('DD MMM YYYY'));
                //_mindate.setDate(_mindate.getDate() - 1);
                $('#<%=txt_EffectiveDate.ClientID%>').data("DateTimePicker").minDate(_mindate);
                $('#<%=txt_EffectiveDate.ClientID%>').val("");
                $('#<%=txt_ExpirationDate.ClientID%>').val("");
            });
            $('#<%=txt_EffectiveDate.ClientID%>').on("dp.change", function (e) { 
                var _reviewYear=parseInt($('#<%=hdfReviewYear.ClientID%>').val());
                $('#<%=txt_ExpirationDate.ClientID%>').data("DateTimePicker").minDate(moment(e.date).format('DD MMM YYYY'));
                if (_reviewYear != 0) {
                    var _mindate = new Date(e.date);
                    _mindate.setDate(_mindate.getDate() - 1)// subtract one day
                    _mindate.setFullYear(_mindate.getFullYear() + _reviewYear) //to add years
                    $('#<%=txt_ExpirationDate.ClientID%>').val(_mindate.format('dd MMM yyyy'))
                }
                else {
                    $('#<%=txt_ExpirationDate.ClientID%>').val("");
                }
                <%--var _mindate = new Date(moment(e.date).format('DD MMM YYYY'));
                _mindate.setDate(_mindate.getDate() - 1)
                _mindate.setFullYear(_mindate.getFullYear() + 2);
                $('#<%=txt_ExpirationDate.ClientID%>').val(_mindate.format('dd MMM yyyy'))--%>
            });
        });
    </script>
    <script type="text/javascript">
        //for datepickers
        var startdate2 = document.getElementById('<%=txt_EffectiveDate.ClientID%>').value
        if (startdate2 == "") {
            $('#<%=txt_EffectiveDate.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                useCurrent: false,
            });
            $('#<%=txt_ExpirationDate.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                useCurrent: false,
            });
        }
        function redirectlist() {
            window.open("<%=ResolveUrl("~/DMS/DocumentCreator/Existing_DocumentList.aspx")%>", "_self");
        }
        function redirectself() {
            window.open("<%=ResolveUrl("~/DMS/DocumentCreator/Existing_Documents.aspx")%>", "_self");
        }
    </script>
    <script>
        $(document).ready(function () {
            $("#<%=ddl_DeptName.ClientID%>").keypress(function (e) {
                if (e.keyCode == 9) {  // detect the tab key
                    document.getElementByID('<%=ddl_DocumentType.ClientID%>').focus();
                }
            });
            $("#<%=ddl_DocumentType.ClientID%>").keypress(function (e) {
                if (e.keyCode == 9) {  // detect the tab key
                    document.getElementByID('<%=txt_DocumentName.ClientID%>').focus();
                }
            });
            $("#<%=txt_Version.ClientID%>").keypress(function (e) {
                if (e.keyCode == 13) {  // detect the enter key
                    return false;
                }
                if (e.keyCode == 9) {  // detect the tab key
                    document.getElementByID('<%=txt_EffectiveDate.ClientID%>').focus();
                 }
            });
            $("#<%=txt_PreparedBy.ClientID%>").keypress(function (e) {
                if (e.keyCode == 13) {  // detect the enter key
                    return false;
                }
                if (e.keyCode == 9) {  // detect the tab key
                    document.getElementByID('<%=txt_PreparedDate.ClientID%>').focus();
                 }
            });
            $("#<%=txt_ReviewedBY.ClientID%>").keypress(function (e) {
                if (e.keyCode == 13) {  // detect the enter key
                    return false;
                }
                if (e.keyCode == 9) {  // detect the tab key
                    document.getElementByID('<%=txt_ReviewDate.ClientID%>').focus();
                }
            });
            $("#<%=txt_ApprovedBY.ClientID%>").keypress(function (e) {
                if (e.keyCode == 13) {  // detect the enter key
                    return false;
                }
                if (e.keyCode == 9) {  // detect the tab key
                    document.getElementByID('<%=txt_ApproveDate.ClientID%>').focus();
                 }
            });
            $("#<%=rbtnNo.ClientID%>").keypress(function (e) {
                if (e.keyCode == 13) {  // detect the enter key
                    return false;
                }
                if (e.keyCode == 9) {  // detect the tab key
                    document.getElementByID('<%=btn_Submit.ClientID%>').focus();
                }
            });
            $("#<%=rbtnYes.ClientID%>").keypress(function (e) {
                if (e.keyCode == 13) {  // detect the enter key
                    return false;
                }
                if (e.keyCode == 9) {  // detect the tab key
                    document.getElementByID('<%=btn_Submit.ClientID%>').focus();
                }
            });
            $("#<%=txt_PreparedDate.ClientID%>").keypress(function (e) {
                var k = e.keyCode;
                return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57));
            });
            $("#<%=txt_ReviewDate.ClientID%>").keypress(function (e) {
                var k = e.keyCode;
                return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57));
            });
            $("#<%=txt_ApproveDate.ClientID%>").keypress(function (e) {
                 var k = e.keyCode;
                 return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57));
             });
            $("#<%=txt_EffectiveDate.ClientID%>").keypress(function (e) {
                 var k = e.keyCode;
                 return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57));
             });
             $("#<%=txt_ExpirationDate.ClientID%>").keypress(function (e) {
                 var k = e.keyCode;
                 return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57));
             });           
        });
    </script>
    <script>
        function RestrictMinus(e) {
            if (e.keyCode === 45 || e.keyCode === 109 || e.keyCode === 189) {
                return false;
            }
            else {
                return true;
            }
        };
    </script>
    <script src='<%= Page.ResolveUrl("~/AppScripts/PDF_Viewer.min.js")%>'></script>
    <script>
        function disableRadioPublic() {
            $('#lblRadioPublic').removeClass('active');
            $('#lblRadioPrivate').addClass('active');
        }
        function enableRadioPublic() {
            $('#lblRadioPublic').addClass('active');
            $('#lblRadioPrivate').removeClass('active');
        }
        function UploadClicK() {
            var btnUpload1 = document.getElementById("<%=btnUpload.ClientID%>");
            btnUpload1.click();
        }
        function ViewExistingDoc() {
            PdfViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>', $('#<%=hdfViewType.ClientID%>').val(), $('#<%=hdfPkID.ClientID%>').val(), $('#<%=hdfViewEmpID.ClientID%>').val(), $('#<%=hdfViewRoleID.ClientID%>').val(), "#divPDF_Viewer");
        }
        function ViewTempExistingDoc() {
            PdfFormViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_Of_File")%>', $('#<%=hdfFileName.ClientID%>').val(), "#divPDF_Viewer");
        }
    </script>
    <script>
        function showUpload() {
            $('#uploadDoc').show();
        }
        function hideUpload() {
            $('#uploadDoc').hide();
        }
        function showViewDoc() {
            $('#divViewDoc').show();
        }
        function hideViewDoc() {
            $('#divViewDoc').hide();
        }
        function restrictAlphabets(e) {

            var x = e.which || e.keycode;
            if ((x >= 48 && x <= 57) || x == 8 ||
                (x >= 35 && x <= 40))
                return true;
            else
                return false;
        }
    </script>
    <script>
        function ConfirmAlert() {
            errors = [];
            var upload = "<%=this.ViewState["filename"].ToString()%>";
            var IsTraining = "<%=this.ViewState["IsTraining"].ToString()%>";
            if ($('#<%=ddl_DocumentType.ClientID %>').val() == "0") {
                errors.push("Select Document Type.");
            }
            if ($('#<%=ddl_DeptName.ClientID %>').val() == "0") {
                errors.push("Select Department.");
            }
            if ($('#<%=txt_DocumentNumber.ClientID %>').val().trim() == "") {
                errors.push("Enter Document Number.");
            }
            if ($('#<%=txt_Version.ClientID %>').val().trim() == "") {
                errors.push("Enter Version Number.");
            }
            if ($('#<%=txt_DocumentName.ClientID %>').val().trim() == "") {
                errors.push("Enter Document Name.");
            }
            if ($('#<%=txt_PreparedBy.ClientID %>').val().trim() == "") {
                errors.push("Enter Prepared By.");
            }
            if ($('#<%=txt_PreparedDate.ClientID %>').val().trim() == "") {
                errors.push("Enter Prepared Date.");
            }
            if ($('#<%=txt_ReviewedBY.ClientID %>').val().trim() == "") {
                errors.push("Enter Reviewed By.");
            }
            if ($('#<%=txt_ReviewDate.ClientID %>').val().trim() == "") {
                errors.push("Enter Reviewed Date.");
            }
            if ($('#<%=txt_ApprovedBY.ClientID %>').val().trim() == "") {
                errors.push("Enter Approved By.");
            }
            if ($('#<%=txt_ApproveDate.ClientID %>').val().trim() == "") {
                errors.push("Enter Approved Date.");
            }
            if ($('#<%=txt_EffectiveDate.ClientID %>').val().trim() == "") {
                errors.push("Enter Effective Date.");
            }
            if (IsTraining == "true") {
                if ($('#<%=txt_ExpirationDate.ClientID %>').val().trim() == "") {
                errors.push("Enter Review Date.");
                }
            }
            
            if (upload == "") {
                errors.push("upload Document.");
            }
            if (upload.length > 95) {
                errors.push("Uploaded file length should not be greater than 95 characters");
            }
            if (upload.includes(",") || upload.includes("/") || upload.includes(":") || upload.includes("*") || upload.includes("?") ||
                upload.includes("<") || upload.includes(">") || upload.includes("|") || upload.includes("\"") || upload.includes("\\") ||
                upload.includes(";")) {
                errors.push("Uploaded file name can contain only the following special characters !@#()-_'+=$^%&~` ");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                document.getElementById("<%=hdfConfirm.ClientID%>").value = 'Submit';
                if ($('#<%=txt_ExpirationDate.ClientID %>').val().trim() != "") {
                    custAlertMsg('Please verify the details before submitting,Once Submitted data cannot be modified again.', 'confirm', true);
                }
                else {
                    custAlertMsg('Do you want to submit data without giving Review Date?', 'confirm', true);
                }
            }
        }
    </script>
    <script>
        $(function () {
            InsertDocNumTitle();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            InsertDocNumTitle();
        });
        function InsertDocNumTitle() {
            var text_val;
            $("#<%=txt_DocumentNumber.ClientID%>").on('keyup', function () {
                                text_val = $(this).val();
                                $("#<%=txt_DocumentNumber.ClientID%>").attr('title', text_val);
                            });
        }
  </script>
    <script>
        function DocNameChange() {
            var btnNameChange = document.getElementById("<%=btnNameChanged.ClientID%>");
            btnNameChange.click();
        }
        function restrictReviewDate()
        {
           var errors = [];
           var DocumentTypeID = $('#<%=ddl_DocumentType.ClientID %>').val();
            $.ajax({
         type: "GET",
        url: '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetIsTraining" )%>',
        data: { DocumentTypeID: DocumentTypeID },
        contentType: "application/json; charset=utf-8",
        processData: false,
        dataType: "json",
        success: function (response) {
            alert(response); // [object Object]
        }
 });
        }
    </script>
</asp:Content>
