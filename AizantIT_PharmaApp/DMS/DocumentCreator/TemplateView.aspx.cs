﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using DevExpress.XtraRichEdit;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace AizantIT_PharmaApp.DMS.DocumentCreator
{
    public partial class TemplateView : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        JQDataTableBO objJQDataTableBO = new JQDataTableBO();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString.Count > 0)
                    {
                        string vid = Request.QueryString[0];
                        ToViewTemplate(vid);
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("TV_M1:" + strline + "  " + strMsg);
                string strError = "TV_M1:" + strline + "  " + "Page loading failed";
                lblError.Visible = true;
                lblError.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        private void ToViewTemplate(string vid)
        {
            try
            {
                JQDataTableBO objJQDataTableBO = new JQDataTableBO();
                objJQDataTableBO.iMode = 0;
                objJQDataTableBO.pkid = Convert.ToInt32(vid);
                objJQDataTableBO.iDisplayLength = 0;
                objJQDataTableBO.iDisplayStart = 0;
                objJQDataTableBO.iSortCol = 0;
                objJQDataTableBO.sSortDir = "";
                objJQDataTableBO.sSearch = "";
                DataTable dt = DMS_Bal.DMS_GetTemplateNameList(objJQDataTableBO);
                divliteral.Visible = false;
                richedit2.Visible = true;
                if (dt.Rows[0]["Content"].ToString().Length > 0)
                {
                    if (dt.Rows[0]["FileType"].ToString() == ".doc")
                    {
                        ASPxRichEdit1.Open(
                        Guid.NewGuid().ToString(),
                        DevExpress.XtraRichEdit.DocumentFormat.Doc,
                        () =>
                        {
                            byte[] docBytes = (byte[])dt.Rows[0]["Content"];
                            return new MemoryStream(docBytes);
                        }
                        );
                    }
                    if (dt.Rows[0]["FileType"].ToString() == ".docx")
                    {
                        ASPxRichEdit1.Open(
                        Guid.NewGuid().ToString(),
                        DevExpress.XtraRichEdit.DocumentFormat.OpenXml,
                        () =>
                        {
                            byte[] docBytes = (byte[])dt.Rows[0]["Content"];
                            return new MemoryStream(docBytes);
                        }
                        );
                    }
                }
                else
                {
                    divliteral.Visible = true;
                    richedit2.Visible = false;
                    literalSop1.Text = @" <b style=""color:red;"">" + "File Not Available To View " + "</b> ";
                }
            }
            catch (Exception)
            {
                divliteral.Visible = true;
                richedit2.Visible = false;
                literalSop1.Text = "File Not Available To View, Bad data ";
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}