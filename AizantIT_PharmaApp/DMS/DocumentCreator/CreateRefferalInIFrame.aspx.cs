﻿using System;
using System.Data;
using System.IO;
using System.Web.UI;
using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;

namespace AizantIT_PharmaApp.DMS.DocumentCreator
{
    public partial class CreateRefferalInIFrame : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        if (Request.QueryString.Count > 0)
                        {
                            string filePath = Request.QueryString[0];
                            ToViewReferralDocument(filePath);
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Uclose", "CloseBrowser();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("CRIF_M1:" + strline + "  " + strMsg);
                string strError = "CRIF_M1:" + strline + "  " + "Page loading failed.";
                literalSop.Visible = true;
                Spreadsheet.Visible = false;
                RichEdit.Visible = false;
                binImage.Visible = false;
                literalSop.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        public void ToViewReferralDocument(string filePath)
        {
            try
            {
                string[] stringSeparators = new string[] { "@$" };
                string[] result = filePath.Split(stringSeparators, StringSplitOptions.None);
                string fname = string.Empty, FilePath = string.Empty; byte[] filecontent = null;
                if (result[1] == "0")
                {
                    TempExternalFileBO objTemp = new TempExternalFileBO();
                    objTemp.CrudType = 2;
                    objTemp.Ref_FileID = 0;
                    objTemp.FileName = result[0];
                    objTemp.FileExtension = "";
                    objTemp.FileContent = new byte[0];
                    objTemp.DocExternal_VersionID = Convert.ToInt32(result[2]);
                    DataTable dtTempExternalDocs = DMS_Bal.DMS_TempCrudExternalFiles(objTemp);
                    if (dtTempExternalDocs.Rows.Count > 0)
                    {
                        filecontent = (byte[])dtTempExternalDocs.Rows[0][0];
                        fname = result[0];
                    }
                    else
                    {
                        fname = result[0];
                    }
                }
                else
                {
                    //DownloadReferrences
                    filecontent = DMS_Bal.DMS_GetExternalFileContent(Convert.ToInt32(result[1]));
                    fname = result[0];
                }
                string[] arrfile = fname.Split('.');
                switch (arrfile[arrfile.Length - 1].ToLower())
                {
                    case "jpeg":
                        binImage.Visible = true;
                        Spreadsheet.Visible = false;
                        RichEdit.Visible = false;
                        literalSop.Visible = false;
                        binImage.ContentBytes = filecontent;
                        break;
                    case "jpg":
                        binImage.Visible = true;
                        Spreadsheet.Visible = false;
                        RichEdit.Visible = false;
                        literalSop.Visible = false;
                        binImage.ContentBytes = filecontent;
                        break;
                    case "png":
                        binImage.Visible = true;
                        Spreadsheet.Visible = false;
                        RichEdit.Visible = false;
                        literalSop.Visible = false;
                        binImage.ContentBytes = filecontent;
                        break;
                    case "gif":
                        binImage.Visible = true;
                        Spreadsheet.Visible = false;
                        RichEdit.Visible = false;
                        literalSop.Visible = false;
                        binImage.ContentBytes = filecontent;
                        break;
                    case "bmp":
                        binImage.Visible = true;
                        Spreadsheet.Visible = false;
                        RichEdit.Visible = false;
                        literalSop.Visible = false;
                        binImage.ContentBytes = filecontent;
                        break;
                    case "pdf":
                        literalSop.Visible = true;
                        Spreadsheet.Visible = false;
                        RichEdit.Visible = false;
                        binImage.Visible = false;
                        Session["TempRefFileContent"] = filecontent;
                        literalSop.Text = " <embed src=\"" + ResolveUrl("~/ASPXHandlers/docdetaik.ashx") + "?SopId=0&sessop=true&ref=1 #toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" height=\"661px\" />";
                        break;
                    case "xls":
                        Spreadsheet.Visible = true;
                        RichEdit.Visible = false;
                        binImage.Visible = false;
                        literalSop.Visible = false;
                        Spreadsheet.Open(
                           Guid.NewGuid().ToString(),
                           DevExpress.Spreadsheet.DocumentFormat.Xls,
                           () =>
                           {
                               byte[] docBytes = filecontent;
                               return new MemoryStream(docBytes);
                           }
                           );
                        break;
                    case "xlsx":
                        Spreadsheet.Visible = true;
                        RichEdit.Visible = false;
                        binImage.Visible = false;
                        literalSop.Visible = false;
                        Spreadsheet.Open(
                            Guid.NewGuid().ToString(),
                            DevExpress.Spreadsheet.DocumentFormat.Xlsx,
                            () =>
                            {
                                byte[] docBytes = filecontent;
                                return new MemoryStream(docBytes);
                            }
                            );
                        break;
                    case "rtf":
                        RichEdit.Visible = true;
                        Spreadsheet.Visible = false;
                        binImage.Visible = false;
                        literalSop.Visible = false;
                        RichEdit.Open(
                           Guid.NewGuid().ToString(),
                           DevExpress.XtraRichEdit.DocumentFormat.Rtf,
                           () =>
                           {
                               byte[] docBytes = filecontent;
                               return new MemoryStream(docBytes);
                           }
                           );
                        break;
                    case "doc":
                        RichEdit.Visible = true;
                        Spreadsheet.Visible = false;
                        binImage.Visible = false;
                        literalSop.Visible = false;
                        RichEdit.Open(
                          Guid.NewGuid().ToString(),
                          DevExpress.XtraRichEdit.DocumentFormat.Doc,
                          () =>
                          {
                              byte[] docBytes = filecontent;
                              return new MemoryStream(docBytes);
                          }
                          );
                        break;
                    case "docx":
                        RichEdit.Visible = true;
                        Spreadsheet.Visible = false;
                        binImage.Visible = false;
                        literalSop.Visible = false;
                        RichEdit.Open(
                          Guid.NewGuid().ToString(),
                          DevExpress.XtraRichEdit.DocumentFormat.OpenXml,
                          () =>
                          {
                              byte[] docBytes = filecontent;
                              return new MemoryStream(docBytes);
                          }
                          );
                        break;
                    case "txt":
                        RichEdit.Visible = true;
                        Spreadsheet.Visible = false;
                        binImage.Visible = false;
                        literalSop.Visible = false;
                        RichEdit.Open(
                          Guid.NewGuid().ToString(),
                          DevExpress.XtraRichEdit.DocumentFormat.PlainText,
                          () =>
                          {
                              byte[] docBytes = filecontent;
                              return new MemoryStream(docBytes);
                          }
                          );
                        break;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("CRIF_M2:" + strline + "  " + strMsg);
                string strError = "CRIF_M2:" + strline + "  " + "External Referral loading failed.";
                literalSop.Visible = true;
                Spreadsheet.Visible = false;
                RichEdit.Visible = false;
                binImage.Visible = false;
                literalSop.Text = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}