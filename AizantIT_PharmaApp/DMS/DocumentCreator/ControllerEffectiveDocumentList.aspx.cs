﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AizantIT_PharmaApp.DMS.DocumentCreator
{
    public partial class ControllerEffectiveDocumentList : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        DocPrintRequest DocReqObjects = new DocPrintRequest();
        DocObjects docObjects = new DocObjects();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=30").Length > 0)//30-Reader
                            {
                                hdfRole.Value = "0";
                                InitializeThePage();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DL_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DL_M1:" + strline + "  " + "Document Load failed.", "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        protected void btnFileView_Click(object sender, EventArgs e)
        {
            btnFileView.Enabled = false;
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfVID.Value);
                hdfStatus.Value = DMS_DT.Rows[0]["DocStatus"].ToString();
                span_FileTitle.InnerHtml = "<b>" + DMS_DT.Rows[0]["DocumentName"].ToString() + "</b>";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_lock').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                ExternalRefDiv.InnerHtml = "&nbsp";
                divPDF_Viewer.InnerHtml = "&nbsp";
                //btn_DirectPrint.Visible = true;
                divPDF_Viewer.InnerHtml = "<iframe id=readerframe name=readerframe src=\"" + ResolveUrl("~/DMS/DocumentsList/DMSWordReaderView.aspx") + "?DocID=" + hdfVID.Value + "&Status=" + hdfStatus.Value + "#toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" height=\"620px\"></iframe>";
                UpViewDL.Update();
                UpHeading.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DL_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DL_M2:" + strline + "  " + "Document Viewing failed.", "error");
            }
            finally
            {
                btnFileView.Enabled = true;
            }
        }
        protected void btnFileView1_Click(object sender, EventArgs e)
        {
            btnFileView1.Enabled = false;
            try
            {
                Session["VID"] = hdfVID.Value;
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfVID.Value);
                hdfStatus.Value = DMS_DT.Rows[0]["DocStatus"].ToString();
                span_FileTitle.InnerHtml = "<b>" + DMS_DT.Rows[0]["DocumentName"].ToString() + "</b>";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_lock').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                ExternalRefDiv.InnerHtml = "&nbsp";
                divPDF_Viewer.InnerHtml = "&nbsp";
                divPDF_Viewer.InnerHtml = "<iframe id=readerframe name=readerframe src=\"" + ResolveUrl("~/DMS/DocumentsList/DMSWordReaderView.aspx") + "?DocID=" + hdfVID.Value + "&Status=" + hdfStatus.Value + "&reader=y" + "#toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" height=\"635px\"></iframe>";
                UpViewDL.Update();
                UpHeading.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DL_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DL_M3:" + strline + "  " + "Document Viewing1 failed.", "error");
            }
            finally
            {
                btnFileView1.Enabled = true;
            }
        }
        protected void btnRefferalView_Click(object sender, EventArgs e)
        {
            btnRefferalView.Enabled = false;
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfVID.Value);
                span1.InnerHtml = "<b>" + DMS_DT.Rows[0]["DocumentName"].ToString() + " Referrals" + "</b>";
                txtReferrences.Text = DMS_Bal.DMS_GetLinkReferences(Convert.ToInt32(hdfVID.Value)).Replace("<a", "<a  target='_blank' ");
                DataTable dtInternalDocs = DMS_Bal.DMS_GetReferalInternalDocs(Convert.ToInt32(hdfVID.Value));
                Session["dtAssignedDocuments"] = dtInternalDocs;
                gvAssignedDocuments.DataSource = dtInternalDocs;
                gvAssignedDocuments.DataBind();
                DataTable dtExternalDocs = DMS_Bal.DMS_GetReferalExternalFiles(Convert.ToInt32(hdfVID.Value));
                gvExternalFiles.DataSource = dtExternalDocs;
                gvExternalFiles.DataBind();
                ViewState["dtExternalDocuments"] = dtExternalDocs;
                if (txtReferrences.Text != "" || dtExternalDocs.Rows.Count > 0 || dtInternalDocs.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_Refferal').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                }
                else
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "There are no Referrals for this document.", "info");
                }
                UpReferralHeading.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DL_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DL_M4:" + strline + "  " + "Referral VIew failed.", "error");
            }
            finally
            {
                btnRefferalView.Enabled = true;
            }
        }
        protected void lnk_ViewInternal_Click(object sender, EventArgs e)
        {
            try
            {
                hdnGrdiReferesh.Value = "Internal";
                LinkButton lnk = (LinkButton)sender;
                GridViewRow i = (GridViewRow)lnk.NamingContainer;
                Label lbl_text = (Label)i.FindControl("lbl_DocumentID");
                ViewState["isDetails"] = false;
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(lbl_text.Text);
                span2.InnerHtml = "<b>" + DMS_DT.Rows[0]["DocumentName"].ToString() + "</b>";
                hdfViewType.Value = "0";
                hdfViewEmpID.Value = "0";
                hdfViewRoleID.Value = "0";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view1", "ViewPDFDocument('#InternalRefDiv','" + lbl_text.Text + "');", true);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "ShowInternalFile();", true);
                if (hdnGrdiReferesh.Value == "Internal")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "TabRefClick();", true);
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DL_M6:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DL_M6:" + strline + "  " + "Internal Referral View failed.", "error");
            }
        }
        protected void lnkDownload_Click(object sender, EventArgs e)
        {
            try
            {
                hdnGrdiReferesh.Value = "External";
                string filePath = (sender as LinkButton).CommandArgument;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#mpop_FileViewer').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                ExternalRefDiv.InnerHtml = "&nbsp";
                divPDF_Viewer.InnerHtml = "&nbsp";
                ExternalRefDiv.InnerHtml = "<iframe src=\"" + ResolveUrl("~/DMS/DocumentCreator/CreateRefferalInIFrame.aspx") + "?FilePath=" + filePath + "#toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" height=\"680px\" ></iframe>";
                UpdatePanel6.Update();
                if (hdnGrdiReferesh.Value == "External")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "TabRefClick();", true);
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DL_M7:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DL_M7:" + strline + "  " + "External Referral View failed.", "error");
            }
        }
        protected void gvAssignedDocuments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvAssignedDocuments.PageIndex = e.NewPageIndex;
                gvAssignedDocuments.DataSource = (DataTable)Session["dtAssignedDocuments"];
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_Refferal').modal({ show: true, backdrop: 'static', keyboard: false });", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DL_M8:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DL_M8:" + strline + "  " + "grid view page index changing failed.", "error");
            }
        }
        private void InitializeThePage()
        {
            try
            {
                Session["ChartStatus"] = "0";
                Session["ChartDeptCode"] = "";
                Session["ChartDocTypeName"] = "";
                Session["ChartDocumentType"] = 0;
                Session["ChartDepartment"] = 0;
                Session["ChartFromDate"] = "";
                Session["ChartToDate"] = "";
                if (Request.QueryString.Count > 0)
                {
                    if (Request.QueryString["cstatus"].ToString() == "1")
                    {
                        Session["ChartStatus"] = Request.QueryString["cstatus"];
                        if (Request.QueryString["dcode"] != null)
                        {
                            string DepartmentCode = (Request.QueryString["dcode"]);
                            Session["ChartDeptCode"] = DepartmentCode;
                        }
                        else
                        {
                            Session["ChartDeptCode"] = string.Empty;
                        }
                        if (Request.QueryString["dtype"] != null)
                        {
                            string DocumentTypeName = (Request.QueryString["dtype"]);
                            Session["ChartDocTypeName"] = DocumentTypeName;
                        }
                        else
                        {
                            Session["ChartDocTypeName"] = string.Empty;
                        }
                        string DocumentTypeID = (Request.QueryString["dtypeID"]);
                        Session["ChartDocumentType"] = DocumentTypeID;
                        string DepartmentID = (Request.QueryString["dID"]);
                        Session["ChartDepartment"] = DepartmentID;
                    }
                    else if (Request.QueryString["cstatus"].ToString() == "3")
                    {
                        Session["ChartStatus"] = Request.QueryString["cstatus"];
                        string DepartmentCode = (Request.QueryString["dcode"]);
                        Session["ChartDeptCode"] = DepartmentCode;
                        string fdate = Request.QueryString["fdate"];
                        fdate = fdate.Replace("%20", " ");
                        Session["ChartFromDate"] = fdate;
                        string tdate = Request.QueryString["tdate"];
                        tdate = tdate.Replace("%20", " ");
                        Session["ChartToDate"] = tdate;
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DL_M8:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DL_M8:" + strline + "  " + "Page Initialization failed.", "error");
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}