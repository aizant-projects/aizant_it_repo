﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace AizantIT_PharmaApp.DMS.DocumentCreator
{
    public partial class Existing_Documents : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        DocObjects docObjects = new DocObjects();
        DocObjects docObjects1 = new DocObjects();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            ConfirmCustomAlert.buttonClick += new EventHandler(ConfirmCustomAlert_buttonClick);
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        ViewState["pdfFileName"] = "";
                        ViewState["filename"] = "";
                        ViewState["IsTraining"] = "";
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=11").Length > 0)//11-Author
                            {
                                ViewState["emp_id_DMS"] = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();

                                //ViewState["ExistingFileContent"] = null;
                                
                                DMS_GetDocumentTypeList();
                                DMS_GetDepartmentList();
                                div_PageTitle.InnerHtml = "Add Existing Document";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "hide1", "hideViewDoc();", true);
                                if (Request.QueryString.Count > 0)
                                {
                                    ViewState["vid"] = Request.QueryString[0];
                                    FillDocumentDetails(Convert.ToInt32(ViewState["vid"]));
                                }
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ED_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ED_M1:" + strline + "  " + "Document Load failed.", "error");
            }
        }
        protected void ConfirmCustomAlert_buttonClick(object sender, EventArgs e)
        {
            try
            {
                if (hdfConfirm.Value == "Submit")
                {
                    Submit();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DD_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DD_M3:" + strline + "  " + "Document Save/Document Submit Electronic Signature loading failed.", "error");
            }
        }
        public void DMS_GetDocumentTypeList()
        {
            try
            {
                JQDataTableBO objJQDataTableBO = new JQDataTableBO();
                objJQDataTableBO.iMode = 0;
                objJQDataTableBO.pkid = 0;
                objJQDataTableBO.iDisplayLength = 0;
                objJQDataTableBO.iDisplayStart = 0;
                objJQDataTableBO.iSortCol = 0;
                objJQDataTableBO.sSortDir = "";
                objJQDataTableBO.sSearch = "";
                DataTable DMS_Dt = DMS_Bal.DMS_GetDocumentTypeList(objJQDataTableBO);
                ddl_DocumentType.DataValueField = DMS_Dt.Columns["DocumentTypeID"].ToString();
                ddl_DocumentType.DataTextField = DMS_Dt.Columns["DocumentType"].ToString();
                ddl_DocumentType.DataSource = DMS_Dt;
                ddl_DocumentType.DataBind();
                ddl_DocumentType.Items.Insert(0, new ListItem("--Select Document Type--", "0"));
                //ddl_DocumentType.SelectedIndex = 1;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ED_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ED_M2:" + strline + "  " + "Document Type load failed.", "error");
            }
        }
        public void DMS_GetDepartmentList()
        {
            try
            {
                DataTable DMS_Dt = DMS_BalDM.DMS_GetDepartmentList();
                ddl_DeptName.DataValueField = DMS_Dt.Columns["DeptID"].ToString();
                ddl_DeptName.DataTextField = DMS_Dt.Columns["DepartmentName"].ToString();
                ddl_DeptName.DataSource = DMS_Dt;
                ddl_DeptName.DataBind();
                ddl_DeptName.Items.Insert(0, new ListItem("--Select Department--", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ED_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ED_M3:" + strline + "  " + "Department load failed.", "error");
            }
        }
        public void FillDocumentDetails(int VID)
        {
            try
            {
                div_PageTitle.InnerHtml = "<h6 class='panel-title'>View Existing Document</h6>";
                btn_Submit.Text = "Update";
                //ViewState["ExistingFileContent"] = null;
                ViewState["filename"] = "";
                JQDataTableBO objJQDataTableBO = new JQDataTableBO();
                objJQDataTableBO.iMode = 0;
                objJQDataTableBO.pkid = VID;
                objJQDataTableBO.iDisplayLength = 0;
                objJQDataTableBO.iDisplayStart = 0;
                objJQDataTableBO.iSortCol = 0;
                objJQDataTableBO.sSortDir = "";
                objJQDataTableBO.sSearch = "";
                DataTable dt = DMS_Bal.DMS_GetExistingDocuments(objJQDataTableBO);
                if (dt.Rows.Count > 0)
                {
                    ddl_DeptName.SelectedValue = dt.Rows[0]["DeptID"].ToString();
                    ddl_DocumentType.SelectedValue = dt.Rows[0]["DocumentTypeID"].ToString();
                    DataTable DMS_DT1 = DMS_BalDM.DMS_GetIsTraining(Convert.ToInt32(ddl_DocumentType.SelectedValue));
                    if (DMS_DT1.Rows[0]["IsTraining"].ToString()=="True")
                    {
                        reviewdate.Visible = true;
                    }
                    else
                    {
                        reviewdate.Visible = false;
                    }
                    txt_DocumentName.Value = dt.Rows[0]["DocumentName"].ToString();
                    txt_DocumentNumber.Text = dt.Rows[0]["DocumentNumber"].ToString();
                    txt_Version.Text = dt.Rows[0]["VersionNumber"].ToString();
                    txt_EffectiveDate.Text = dt.Rows[0]["EffectiveDate"].ToString();
                    if (dt.Rows[0]["ExpirationDate"].ToString() == "N/A")
                    {
                        lblExpiationDate.Visible = true;
                        txt_ExpirationDate.Visible = false;
                    }
                    else
                    {
                        lblExpiationDate.Visible = false;
                        txt_ExpirationDate.Text = dt.Rows[0]["ExpirationDate"].ToString();
                    }
                    txt_PreparedBy.Text = dt.Rows[0]["PreparedBY"].ToString();
                    txt_PreparedDate.Text = dt.Rows[0]["PeparedDate"].ToString();
                    txt_ReviewedBY.Text = dt.Rows[0]["ReviewedBy"].ToString();
                    txt_ReviewDate.Text = dt.Rows[0]["ReviewedDate"].ToString();
                    txt_ApprovedBY.Text = dt.Rows[0]["ApprovedBY"].ToString();
                    txt_ApproveDate.Text = dt.Rows[0]["ApprovedDate"].ToString();
                    if (Convert.ToBoolean(dt.Rows[0]["isPublic"].ToString()) == true)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "enableRadioPublic();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "disableRadioPublic();", true);
                    }
                    ddl_DeptName.Enabled = false;
                    ddl_DocumentType.Enabled = false;
                    txt_DocumentName.Disabled = true;
                    txt_DocumentNumber.ReadOnly = true;
                    txt_Version.ReadOnly = true;
                    txt_EffectiveDate.ReadOnly = true;
                    txt_ExpirationDate.ReadOnly = true;
                    txt_PreparedBy.ReadOnly = true;
                    txt_PreparedDate.ReadOnly = true;
                    txt_ReviewedBY.ReadOnly = true;
                    txt_ReviewDate.ReadOnly = true;
                    txt_ApprovedBY.ReadOnly = true;
                    txt_ApproveDate.ReadOnly = true;
                    divOpinion.Style.Add("pointer-events", "none");
                    //dvFup.Visible = false;
                    btn_Submit.Visible = false;
                    btn_Reset.Visible = false;
                    www.Update();
                    //if (dt.Rows[0]["Content"] != null)
                    //{
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show1", "showViewDoc();", true);
                    if (dt.Rows[0]["FileName"].ToString().Length > 15)
                    {
                        lb_Upload.Attributes.Add("title", dt.Rows[0]["FileName"].ToString());
                        lb_Upload.Text = dt.Rows[0]["FileName"].ToString().Substring(0, 15) + "...";
                    }
                    else
                    {
                        lb_Upload.Attributes.Add("title", dt.Rows[0]["FileName"].ToString());
                        lb_Upload.Text = dt.Rows[0]["FileName"].ToString();
                    }
                        ViewState["filename"] = dt.Rows[0]["FileName"].ToString();
                        //byte[] byteData = (byte[])dt.Rows[0]["Content"];
                        //ViewState["ExistingFileContent"] = byteData;

                        docObjects.Version = Convert.ToInt32(VID);
                        docObjects.RoleID = 0;
                        docObjects.EmpID = 0;
                        docObjects.FileType1 = ".pdf";
                        docObjects.Mode = 0;
                        System.Data.DataTable DMS_DT = DMS_Bal.DMS_GetDocumentLocationList(docObjects);
                        //if (dt.Rows[0]["DyFileName"].ToString() == "0")
                        //{
                        //    string storefolder = HttpContext.Current.Server.MapPath(@"~/Scripts/pdf.js/web/");
                        //    string File2Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                        //    string strTargPath = (storefolder + "\\" + File2Path);
                        //    File.WriteAllBytes(strTargPath, byteData);

                        //    docObjects1.Version = Convert.ToInt32(VID);
                        //    docObjects1.RoleID = 11;//Author
                        //    docObjects1.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        //    docObjects1.FilePath1 = null;
                        //    docObjects1.FilePath2 = strTargPath;
                        //    docObjects1.isTemp = 0;
                        //    docObjects1.FileType1 = null;
                        //    docObjects1.FileType2 = ".pdf";
                        //    docObjects1.FileName1 = null;
                        //    docObjects1.FileName2 = File2Path;
                        //    int DOC_Ins = DMS_Bal.DMS_InsertDocumentLocation(docObjects);

                        //    ViewState["pdfFileName"] = File2Path;
                        //}
                        //else
                        //{
                            ViewState["pdfFileName"] = dt.Rows[0]["DyFileName"].ToString();
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "hide2", "hideUpload();", true);
                    //}
                }
                //}
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ED_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ED_M4:" + strline + "  " + "Document details load failed.", "error");
            }
        }
        protected void txt_DocumentName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddl_DeptName.SelectedIndex == 0)
                {
                    txt_DocumentName.Value = "";
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>  Please select Department.','error');", true);
                }
                else
                {
                    if (!string.IsNullOrEmpty(txt_DocumentName.Value))
                    {
                        docObjects.DMS_DocumentName = txt_DocumentName.Value.Trim();
                        docObjects.dept = ddl_DeptName.SelectedValue;
                        DataTable DMS_dt = DMS_Bal.DMS_DocumentNameSearch(docObjects);
                        if (DMS_dt.Rows.Count > 0)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>  Document Name  Already Exists.','error');", true);
                            txt_DocumentName.Value = "";
                        }
                        else
                        {
                        }
                    }
                    else
                    {
                        lblStatus.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ED_M5:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ED_M5:" + strline + "  " + "Document name search failed.", "error");
            }
        }
        protected void txt_DocumentNumber_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txt_DocumentNumber.Text))
                {
                    docObjects.documentNUmber = txt_DocumentNumber.Text.Trim();
                    DataTable DMS_dt = DMS_Bal.DMS_DocumentNumberSearch(docObjects);
                    if (DMS_dt.Rows.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>  Document Number  Already Exists.','error');", true);
                        txt_DocumentNumber.Text = "";
                    }
                }
                else
                {
                    lbl_number.Visible = false;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ED_M6:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ED_M6:" + strline + "  " + "Document number search failed.", "error");
            }
        }
        public void reset()
        {
            try
            {
                if (ViewState["vid"] != null)
                {
                    if (Convert.ToInt32(ViewState["vid"]) > 0)
                    {
                        //ViewState.Remove("ExistingFileContent");
                        ViewState.Remove("filename");
                        //ViewState["ExistingFileContent"] = null;
                        ViewState["filename"] = "";
                        FillDocumentDetails(Convert.ToInt32(ViewState["vid"]));
                    }
                }
                else
                {
                    btn_Submit.Text = "Submit";
                    txt_DocumentNumber.Text = "";
                    ddl_DocumentType.SelectedIndex = 1;
                    ddl_DeptName.SelectedIndex = 0;
                    txt_DocumentName.Value = "";
                    txt_EffectiveDate.Text = "";
                    txt_ExpirationDate.Text = "";
                    txt_Version.Text = "";
                    txt_PreparedBy.Text = "";
                    txt_PreparedDate.Text = "";
                    txt_ReviewedBY.Text = "";
                    txt_ReviewDate.Text = "";
                    txt_ApprovedBY.Text = "";
                    txt_ApproveDate.Text = "";
                    //ViewState.Remove("ExistingFileContent");
                    ViewState.Remove("filename");
                    //ViewState["ExistingFileContent"] = null;
                    ViewState["filename"] = "";
                    lb_Upload.Text = "";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "enableRadioPublic();", true);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "hide2", "hideViewDoc();", true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            btnUpload.Enabled = false;
            try
            {
                if (file_word.HasFile)
                {
                    if (file_word.FileName.Trim().Length > 4)
                    {
                        string ext = System.IO.Path.GetExtension(file_word.FileName);

                        if (ext.ToLower() == ".pdf")
                        {
                            ViewState["filename"] = file_word.FileName.Trim()/*.Substring(0, file_Document.FileName.Length - 4) + ".rtf"*/;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show3", "showViewDoc();", true);
                            if (file_word.FileName.Trim().Length > 15)
                            {
                                lb_Upload.Attributes.Add("title", file_word.FileName.Trim());
                                lb_Upload.Text = file_word.FileName.Trim().Substring(0, 15) + "...";
                            }
                            else
                            {
                                lb_Upload.Attributes.Add("title", file_word.FileName.Trim());
                                lb_Upload.Text = file_word.FileName.Trim();
                            }
                            //lb_Upload.Text = file_word.FileName.Trim()/*.Substring(0, file_Document.FileName.Length - 4) + ".rtf"*/;
                            byte[] imgbyte = null;
                            int length = file_word.PostedFile.ContentLength;
                            imgbyte = new byte[length];
                            HttpPostedFile img = file_word.PostedFile;
                            img.InputStream.Read(imgbyte, 0, length);
                            if (imgbyte.Length < 1000)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' Not able to read content,document upload failed','warning');", true);
                                return;
                            }
                            string mime = MimeType.GetMimeType(imgbyte, file_word.FileName);
                            if (mime.ToLower() != "application/pdf")
                            {
                                ViewState["filename"] = "";
                                lb_Upload.Text = "";
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    Please upload  pdf file only ! <br/> The uploaded file is not valid or corrupted.','warning');", true);
                                return;
                            }
                            ViewState["ExistingFileContent"] = imgbyte.Length;

                            string strPDFMainFolderPath = HttpContext.Current.Server.MapPath(@"~/Scripts/pdf.js/web");
                            string File2Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                            string strTargPath = (strPDFMainFolderPath + "\\" + File2Path);
                            File.WriteAllBytes(strTargPath, imgbyte);
                            ViewState["pdfFileName"] = File2Path;
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    Please upload  pdf file only.','warning');", true);
                        }
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ED_M8:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ED_M8:" + strline + "  " + "upload button click failed.", "error");
            }
            finally
            {
                btnUpload.Enabled = true;
            }
        }
        protected void lb_Upload_Click(object sender, EventArgs e)
        {
            lb_Upload.Enabled = false;
            try
            {
                if (Convert.ToString(ViewState["filename"]).Length > 3)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show4", "showViewDoc();", true);
                    if (Convert.ToString(ViewState["filename"]).Length > 15)
                    {
                        lb_Upload.Attributes.Add("title", Convert.ToString(ViewState["filename"]));
                        lb_Upload.Text = Convert.ToString(ViewState["filename"]).Substring(0, 15) + "...";
                    }
                    else
                    {
                        lb_Upload.Attributes.Add("title", Convert.ToString(ViewState["filename"]));
                        lb_Upload.Text = Convert.ToString(ViewState["filename"]);
                    }
                    span_FileTitle.InnerHtml = "<b>" + Convert.ToString(ViewState["filename"]) + "</b>";
                    hdfFileName.Value = ViewState["pdfFileName"].ToString();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view1", "ViewTempExistingDoc();", true);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_lock').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                    if (ViewState["vid"] != null)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "hide1", "hideUpload();", true);
                    }
                        return;
                }
                if (ViewState["vid"] != null)
                {
                    if (Convert.ToInt32(ViewState["vid"]) > 0)
                    {
                        if (file_word.FileName.Trim().Length < 4)
                        {
                            DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(ViewState["vid"].ToString());
                            if (DMS_DT.Rows[0]["FileType"].ToString() == ".pdf")
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show5", "showViewDoc();", true);
                                if (DMS_DT.Rows[0]["FileName"].ToString().Length > 15)
                                {
                                    lb_Upload.Attributes.Add("title", DMS_DT.Rows[0]["FileName"].ToString());
                                    lb_Upload.Text = DMS_DT.Rows[0]["FileName"].ToString().Substring(0, 15) + "...";
                                }
                                else
                                {
                                    lb_Upload.Attributes.Add("title", DMS_DT.Rows[0]["FileName"].ToString());
                                    lb_Upload.Text = DMS_DT.Rows[0]["FileName"].ToString();
                                }
                                span_FileTitle.InnerHtml = "<b>" + DMS_DT.Rows[0]["FileName"].ToString() + "</b>";
                                hdfFileName.Value = ViewState["pdfFileName"].ToString();
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view2", "ViewTempExistingDoc();", true);
                            }
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "$('#ModalError').modal('hide');", true);
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "$('#ModalSuccess').modal('hide');", true);
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "$('#ModalWarning').modal('hide');", true);
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "$('#ModalConfirm').modal('hide');", true);
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "$('#ModalWarning').modal('hide');", true);
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_lock').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                            
                        }
                    }
                    return;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ED_M9:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ED_M9:" + strline + "  " + "upload link click failed.", "error");
            }
            finally
            {
                lb_Upload.Enabled = true;
            }
        }
        public void Submit()
        {
            btn_Submit.Enabled = false;
            try
            {
                StringBuilder sbErrorMsg = new StringBuilder();
                if (ddl_DocumentType.SelectedValue.ToString().Trim() == "0")
                {
                    sbErrorMsg.Append("Please Select Document Type." + "<br/>");
                }
                if (ddl_DeptName.SelectedValue.ToString().Trim() == "0")
                {
                    sbErrorMsg.Append("Please Select Department." + "<br/>");
                }
                if (txt_DocumentNumber.Text.Trim() == "")
                {
                    sbErrorMsg.Append("Please Enter Document Number." + "<br/>");
                }
                //if (!string.IsNullOrEmpty(txt_DocumentNumber.Text.Trim()))
                //{
                //    docObjects.documentNUmber = txt_DocumentNumber.Text.Trim();
                //    DataTable DMS_dt = DMS_Bal.DMS_DocumentNumberSearch(docObjects);
                //    if (DMS_dt.Rows.Count > 0)
                //    {
                //        sbErrorMsg.Append("Document Number Already exists, Please Enter another Number." + "<br/>");
                //    }
                //}
                if (txt_Version.Text.Trim() == "")
                {
                    sbErrorMsg.Append("Please Enter  Version Number." + "<br/>");
                }
                else
                {
                    try
                    {
                        if (Convert.ToInt32(txt_Version.Text.Trim()) < 0)
                        {
                            sbErrorMsg.Append("Version Number Starts From 0." + "<br/>");
                        }
                        if (txt_Version.Text.Trim().ToLower().Contains("e"))
                        {
                            sbErrorMsg.Append("Version Number Should be Integer." + "<br/>");
                        }
                    }
                    catch
                    {
                    }
                }
                if (txt_DocumentName.Value.Trim() == "")
                {
                    sbErrorMsg.Append("Please Enter Document Name." + "<br/>");
                }
                //if (!string.IsNullOrEmpty(txt_DocumentName.Value.Trim()))
                //{

                //    docObjects.DMS_DocumentName = txt_DocumentName.Value.Trim();
                //    docObjects.dept = ddl_DeptName.SelectedValue;
                //    DataTable DMS_dt = DMS_Bal.DMS_DocumentNameSearch(docObjects);
                //    if (DMS_dt.Rows.Count > 0)
                //    {
                //        sbErrorMsg.Append("Document Name Already exists, Please Enter another Name." + "<br/>");
                //    }
                //}
                if (txt_PreparedBy.Text.Trim() == "")
                {
                    sbErrorMsg.Append("Please Enter Prepared By." + "<br/>");
                }
                if (txt_PreparedDate.Text.Trim() == "")
                {
                    sbErrorMsg.Append("Please Enter Prepared Date." + "<br/>");
                }
                if (txt_ReviewedBY.Text.Trim() == "")
                {
                    sbErrorMsg.Append("Please Enter Reviewed By." + "<br/>");
                }
                if (txt_ReviewDate.Text.Trim() == "")
                {
                    sbErrorMsg.Append("Please Enter Reviewed Date." + "<br/>");
                }
                if (txt_ApprovedBY.Text.Trim() == "")
                {
                    sbErrorMsg.Append("Please Enter Approved By." + "<br/>");
                }
                if (txt_ApproveDate.Text.Trim() == "")
                {
                    sbErrorMsg.Append("Please Enter Approve Date." + "<br/>");
                }
                if (txt_EffectiveDate.Text.Trim() == "")
                {
                    sbErrorMsg.Append("Please Enter Effective Date." + "<br/>");
                }
                if (ViewState["IsTraining"].ToString() == "true")
                {
                    if (txt_ExpirationDate.Text.Trim() == "")
                    {
                        sbErrorMsg.Append("Please Enter Review Date." + "<br/>");
                    }
                }
                if (txt_ExpirationDate.Text.Trim() != "")
                {
                    if (Convert.ToDateTime(txt_EffectiveDate.Text.Trim()) == Convert.ToDateTime(txt_ExpirationDate.Text.Trim()))
                    {
                        sbErrorMsg.Append("Effective Date and Review Date must not be same." + "<br/>");
                    }
                    else if (Convert.ToDateTime(txt_EffectiveDate.Text.Trim()) > Convert.ToDateTime(txt_ExpirationDate.Text.Trim()))
                    {
                        sbErrorMsg.Append("Effective Date should not be greater than Review Date." + "<br/>");
                    }
                }
                if (file_word.HasFile == false && ViewState["filename"].ToString() == "")
                {
                    sbErrorMsg.Append("Please upload  Document." + "<br/>");
                }
                if (Convert.ToInt32(ViewState["ExistingFileContent"].ToString()) < 1000)
                {
                    sbErrorMsg.Append("Not able to read content,document upload failed." + "<br/>");
                }
                if (rbtnYes.Checked == false && rbtnNo.Checked == false)
                {
                    sbErrorMsg.Append("Please Select privacy level.");
                }
                if (sbErrorMsg.Length > 8)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
                    return;
                }
                byte[] imgbyte = null;
                if (file_word.HasFile)
                {
                    int length = file_word.PostedFile.ContentLength;
                    imgbyte = new byte[length];
                    HttpPostedFile img = file_word.PostedFile;
                    img.InputStream.Read(imgbyte, 0, length);
                    string mime = MimeType.GetMimeType(imgbyte, file_word.FileName.Trim());
                    if (mime.ToLower() != "application/pdf")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    Please upload  pdf file only ! <br/> The uploaded file is not valid or corrupted.','warning');", true);
                        return;
                    }
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show2", "showViewDoc();", true);
                    lb_Upload.Text = file_word.FileName.Trim()/*Substring(0, file_Document.FileName.Length - 4) + ".rtf"*/;
                }
                //else
                //{
                //    if (ViewState["ExistingFileContent"] != null)
                //    {
                //        imgbyte = ViewState["ExistingFileContent"] as byte[];
                //    }
                //}
                docObjects.pkid = 0;
                //docObjects.Content = imgbyte;
                docObjects.DMS_CreatedBy = Convert.ToInt32(ViewState["emp_id_DMS"].ToString());
                docObjects.effective = Convert.ToDateTime(txt_EffectiveDate.Text.Trim());
                if (txt_ExpirationDate.Text.ToString().Trim() != "")
                {
                    docObjects.expiration = Convert.ToDateTime(txt_ExpirationDate.Text.Trim());
                }
                else
                {
                    docObjects.expiration = null;
                }
                docObjects.Filename = ViewState["filename"].ToString().Trim();
                docObjects.Version = Convert.ToInt32(txt_Version.Text);
                docObjects.DMS_DocumentName = Regex.Replace(txt_DocumentName.Value.Trim(), @"\s+", " ");
                docObjects.documentNUmber = Regex.Replace(txt_DocumentNumber.Text.Trim(), @"\s+", " ");
                docObjects.DMS_DocumentType = Convert.ToInt32(ddl_DocumentType.SelectedValue);
                docObjects.DMS_Deparment = Convert.ToInt32(ddl_DeptName.SelectedValue);
                docObjects.RequestTypeID = 4;
                docObjects.PreparedBY = Regex.Replace(txt_PreparedBy.Text.Trim(), @"\s+", " ");
                docObjects.oldReviewedBy = Regex.Replace(txt_ReviewedBY.Text.Trim(), @"\s+", " ");
                docObjects.oldApprovedBY = Regex.Replace(txt_ApprovedBY.Text.Trim(), @"\s+", " ");
                docObjects.PreparedDate = Convert.ToDateTime(txt_PreparedDate.Text.Trim());
                docObjects.ReviewedDate = Convert.ToDateTime(txt_ReviewDate.Text.Trim());
                docObjects.ApprovedDate = Convert.ToDateTime(txt_ApproveDate.Text.Trim());
                docObjects.FileName2 = ViewState["pdfFileName"].ToString();
                docObjects.FilePath2 = "~/Scripts/pdf.js/web/" + ViewState["pdfFileName"].ToString();
                docObjects.FileType2 = ".pdf";
                //Session["Update_EDoc"] = null;
                if (ViewState["vid"] != null)
                {
                    if (Convert.ToInt32(ViewState["vid"]) > 0)
                    {
                        //update code
                        docObjects.pkid = Convert.ToInt32(ViewState["vid"]);
                        int DMS_Return = DMS_Bal.DMS_ExistingDocumentInsertion(docObjects, (rbtnYes.Checked == true ? true : false));
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), " Document with number <b>" + txt_DocumentNumber.Text.Trim() + "</b> updated successfully.", "success", "redirectlist()");
                    }
                }
                else
                {
                    // insert code
                    int DMS_Return = DMS_Bal.DMS_ExistingDocumentInsertion(docObjects, (rbtnYes.Checked == true ? true : false));
                    if (DMS_Return == 1)
                    {
                        string msg = "Document with number <b>" + txt_DocumentNumber.Text.Trim() + "</b> already exists!";
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), msg.ToString(), "warning", "redirectself()");
                        reset();
                    }
                    else if (DMS_Return == 2)
                    {
                        string msg = "Document with number <b>" + txt_DocumentNumber.Text.Trim() + "</b> submitted successfully.";
                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), msg.ToString(), "success", "redirectself()");
                        reset();
                    }
                }
                UpdatePanel3.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ED_M10:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ED_M10:" + strline + "  " + "Document submission failed.", "error");
            }
            finally
            {
                btn_Submit.Enabled = true;
            }
        }
        protected void ddl_DeptName_SelectedIndexChanged(object sender, EventArgs e)
        {
            txt_DocumentName.Value = "";
        }
        protected void ddl_DocumentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl_DocumentType.SelectedValue != "0")
            {
                DataTable DMS_DT = DMS_BalDM.DMS_GetIsTraining(Convert.ToInt32(ddl_DocumentType.SelectedValue));
                if (DMS_DT.Rows[0]["IsTraining"].ToString() == "True")
                {
                    reviewdate.Visible = true;
                    ViewState["IsTraining"] = "true";
                    hdfReviewYear.Value = DMS_DT.Rows[0]["ReviewYears"].ToString();
                }
                else
                {
                    reviewdate.Visible = false;
                    ViewState["IsTraining"] = "false";
                    hdfReviewYear.Value = DMS_DT.Rows[0]["ReviewYears"].ToString();
                }
            }
            txt_EffectiveDate.Text = "";
            txt_ExpirationDate.Text = "";
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }

        
    }
}