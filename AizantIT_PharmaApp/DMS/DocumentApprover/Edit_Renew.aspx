﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="Edit_Renew.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentApprover.Edit_Renew" %>
<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <div class=" col-lg-12 col-12 col-md-12 col-sm-12  dms_outer_border float-left padding-none">
        <div class="col-sm-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server" visible="false">
                <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-md-12 col-lg-12 col-12 col-sm-12 dms_outer_border bottom padding-none float-left" id="divMainContainer" runat="server" visible="true">
    <div id="accordion_author" class="accordion qms_accordion">

           <div class="card mb-0">
               <div class="card-header header_col  accordion-toggle" data-toggle="collapse" href="#collapse1">
                      
                                <a class="card-title">Document Details
                                </a>
                          </div>

               <div id="collapse1" class="collapse show grid_panel_full border_top_none" data-parent="#accordion_author1">
                    <div class="card-body acc_boby float-left ">
                        <%--<asp:UpdatePanel ID="UP_RNWpanel" runat="server">
                                <ContentTemplate>--%>
                                      
                    <div class="col-lg-12  float-left">
                        <div class="col-lg-12 float-left padding-none top">
                            <div class="col-lg-4 float-left padding-none">
                                <div class="label-style ">Document Number:</div>
                                <asp:TextBox ID="txt_DocumentNumber" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-4 float-left">
                                <div class="label-style">Document Name:</div>
                                <asp:TextBox ID="txt_DocumentName" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-4 float-left padding-none">
                                <div class=" label-style">Department:</div>
                                <asp:TextBox ID="txt_Department" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                         <div class="col-lg-12 padding-none float-left top bottom">
                             <div class="col-lg-4 float-left padding-none">
                        <div class=" label-style">Review Date :</div>
                        <div class="col-lg-12  float-left padding-none">
                            <asp:TextBox ID="txt_Expiration" runat="server" CssClass="form-control"></asp:TextBox>
                            <label id="noof" runat="server" style="color: red;margin-top: 4px;">
                                You have only
                                <asp:Label ID="lbl_Attempts" runat="server"></asp:Label>
                                attempt('s) left to extend this document</label>
                        </div>
                           </div>
                         <div id="div_Upload" class="col-lg-6 float-left" style="margin-top: 19px;display:none;">
                            
                                    <div class="file upload-button  btn btn-primary  col-lg-4  float-left" id="dvFup" runat="server" style="margin-top: 0px !important;">Upload<asp:FileUpload class="upload-button_input" ID="file_word" runat="server" onchange="UploadClicK()" accept=".pdf" ToolTip="Click to Upload Form" /></div>
                                                     
                        <div class="col-lg-8 float-left" id="divViewDoc">
                            <label>View Doc : </label>
                            <asp:LinkButton ID="lb_Upload" runat="server"  ForeColor="OrangeRed" Font-Bold="true" OnClick="lb_Upload_Click"></asp:LinkButton>
                        </div>
                    </div>
            
                                    <%--</ContentTemplate>
                            </asp:UpdatePanel>--%>
                        
                             <div class=" float-right padding-none" style="margin-top: 19px;">
                                 <asp:UpdatePanel ID="upbns" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                    <button type="button" id="btn_RenewSubmit" class="btn-signup_popup" onclick="return RequiredReviewDate();">Update</button>
                        <button type="button" data-dismiss="modal" runat="server" id="Button1" class="btn-cancel_popup" onclick="redirectToList();">Cancel</button>
                                                    </ContentTemplate>
                                            </asp:UpdatePanel>
                   </div>
                                </div>    
                        </div>
                   </div>
               </div>
        <div class="card mb-0">
               <div class="card-header header_col  accordion-toggle " data-toggle="collapse" href="#collapse2">
                      
                                <a class="card-title">Renew Document
                                </a>
                          </div>

               <div id="collapse2" class="collapse  show grid_panel_full border_top_none" data-parent="#accordion_author">
                    <div class="card-body acc_boby float-left ">
                      
                         <div class="col-12 top" id="divPDF_Viewer">
                        <noscript>
                            Please Enable JavaScript.
                        </noscript>
                    </div>
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                        </div>
                   </div>
               </div>

        </div>
                </div>
         </div>

    <asp:HiddenField ID="hdfPKID" runat="server" />
    <asp:HiddenField ID="hdfStatus" runat="server" value="0"/>
    <asp:HiddenField ID="hdfDocNum" runat="server" />
    <asp:HiddenField ID="hdfMaxDateForRenew" runat="server" />
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnRequestType" runat="server" Value="0" />
    <asp:HiddenField ID="hdnRequestTypeID" runat="server" Value="0" />    
    <asp:HiddenField ID="hdfViewType" runat="server" />
    <asp:HiddenField ID="hdfViewEmpID" runat="server" />
    <asp:HiddenField ID="hdfViewRoleID" runat="server" />
    <asp:HiddenField ID="hdfViewDivID" runat="server" />
    <asp:HiddenField ID="hdfFileName" runat="server" />
    <asp:HiddenField ID="hdnConfirm" runat="server" />
    <asp:HiddenField ID="hdfExDate" runat="server"/>
    <uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />
    <asp:Button ID="btnUpload" runat="server" Text="Button" OnClick="btnUpload_Click" Style="display: none" />
    <script>
        
                // show Upload
                //document.getElementById("<%=hdnRequestTypeID.ClientID%>").value = RTYPID;
        function showUpload() {
            $('#div_Upload').show();
            $('#div_Upload').css("display", "block");
        }
               
                function hideUpload()
            {
                $('#div_Upload').hide();
                $('#div_Upload').css("display", "none");
            }

        function UploadClicK() {
            var btnUpload1 = document.getElementById("<%=btnUpload.ClientID%>");
             btnUpload1.click();
         }
    </script>
    <script>
          <!--for datePicker-- >     
        $('#<%=txt_Expiration.ClientID%>').datetimepicker({
            format: 'DD MMM YYYY',
            minDate: new Date('<%=HiddenField1.Value%>'),
            useCurrent: true,
        });
        $('#<%=txt_Expiration.ClientID%>').on("dp.change", function (e) {
            document.getElementById('<%=hdfExDate.ClientID%>').value = moment(e.date).format('DD MMM YYYY');
        });
        function myfunction1() {
               if (document.getElementById('<%=hdfExDate.ClientID%>').value != "") {
                   document.getElementById('<%=txt_Expiration.ClientID%>').value = document.getElementById('<%=hdfExDate.ClientID%>').value;
             }
             else {
                   document.getElementById('<%=txt_Expiration.ClientID%>').value = document.getElementById('<%=HiddenField1.ClientID%>').value;
             }
        }
       var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('#<%=txt_Expiration.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                minDate: new Date('<%=HiddenField1.Value%>'),
                useCurrent: true,
            });
        $('#<%=txt_Expiration.ClientID%>').on("dp.change", function (e) {
            document.getElementById('<%=hdfExDate.ClientID%>').value = moment(e.date).format('DD MMM YYYY');
        });
        function myfunction1() {
               if (document.getElementById('<%=hdfExDate.ClientID%>').value != "") {
                   document.getElementById('<%=txt_Expiration.ClientID%>').value = document.getElementById('<%=hdfExDate.ClientID%>').value;
             }
             else {
                   document.getElementById('<%=txt_Expiration.ClientID%>').value = document.getElementById('<%=HiddenField1.ClientID%>').value;
             }
        }
        });
        window.onload = myfunction1;
    </script>
    <script src='<%= Page.ResolveUrl("~/AppScripts/PDF_Viewer.min.js")%>'></script>
    <script type="text/javascript">
        function RequiredReviewDate() {
            var ReviewDate = document.getElementById("<%=txt_Expiration.ClientID%>").value;
            var RtypValue = document.getElementById("<%=hdnRequestType.ClientID%>").value;          
            var isUploadValue = "<%=this.ViewState["filename"].ToString()%>";
            
            errors = [];
            if (ReviewDate.trim() == "") {
                errors.push("Select Review Date.");
            }
            if (RtypValue == "4") {
                // check for file upload
                if (isUploadValue == "") {
                    errors.push("Please upload Renewed Document.");
                }
                if (isUploadValue.length > 95) {
                    errors.push("Uploaded file length should not be greater than 95 characters");
                }
                if (isUploadValue.includes(",") || isUploadValue.includes("/") || isUploadValue.includes(":") || isUploadValue.includes("*") || isUploadValue.includes("?") ||
                    isUploadValue.includes("<") || isUploadValue.includes(">") || isUploadValue.includes("|") || isUploadValue.includes("\"") || isUploadValue.includes("\\") ||
                    isUploadValue.includes(";")) {
                    errors.push("Uploaded file name can contain only the following special characters !@#()-_'+=$^%&~` ");
                }
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                ConformAlertRenew();
                return true;
            }
        }
        function CloseBrowser() {
            window.close();
        }
        function ViewDocument() {
            PdfViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>', $('#<%=hdfViewType.ClientID%>').val(), $('#<%=hdfPKID.ClientID%>').val(), "0", "0", "#divPDF_Viewer", '');
        }
        function ViewTempExistingDoc() {
            PdfFormViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_Of_File")%>', $('#<%=hdfFileName.ClientID%>').val(), "#divPDF_Viewer");
        }
        function redirectToList() {
            var RtypValue = document.getElementById("<%=hdnRequestType.ClientID%>").value;
            if (RtypValue == "1") {
                window.open("<%=ResolveUrl("~/DMS/DocumentApprover/Renew_Document.aspx?rtp=1")%>", "_self");
            }
            if (RtypValue == "2") {
                window.open("<%=ResolveUrl("~/DMS/DocumentApprover/Renew_Document.aspx?rtp=2")%>", "_self");
            }
            if (RtypValue == "4") {
                window.open("<%=ResolveUrl("~/DMS/DocumentApprover/Renew_Document.aspx?rtp=4")%>", "_self");
            }
            if (RtypValue == "5") {
                window.open("<%=ResolveUrl("~/DMS/DocumentApprover/Renew_Document.aspx")%>", "_self");
            }
        }
        function ConformAlertRenew() {
            document.getElementById("<%=hdnConfirm.ClientID%>").value = 'Renew';
            custAlertMsg('Are you sure you want to Renew the Document?', 'confirm', true);
        }
 function showViewDoc() {
            $('#divViewDoc').show();
        }
        function hideViewDoc() {
            $('#divViewDoc').hide();
        }
    </script>
</asp:Content>
