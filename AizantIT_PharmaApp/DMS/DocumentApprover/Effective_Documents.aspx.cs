﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using iTextSharp.text.pdf;
using iTextSharp.text;
using UMS_BusinessLayer;
using System.Data.SqlClient;
using UMS_BO;
using System.Text.RegularExpressions;

namespace AizantIT_PharmaApp.DMS.DocumentApprover
{
    public partial class Effective_Documents : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        ReviewObjects reviewObjects = new ReviewObjects();
        DocObjects docObjects = new DocObjects();
        private string storefolder = HttpContext.Current.Server.MapPath(@"~/Scripts/pdf.js/web/");
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            ConfirmCustomAlert.buttonClick += new EventHandler(ConfirmCustomAlert_buttonClick);
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        HiddenField1.Value = 3.ToString();
                        hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=30").Length > 0)//30-DocController
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EFD_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EFD_M1:" + strline + "  " + "Document Load failed.", "error");
            }
        }
        protected void ConfirmCustomAlert_buttonClick(object sender, EventArgs e)
        {
            SqlTransaction transaction = null;
            SqlConnection EFFConn = null;
            try
            {
                if (hdnAction.Value == "Submit")
                {
                    StringBuilder sbErrorMsg = new StringBuilder();
                    int DateCount = 0;
                    if (txt_EffectiveDate.Text.Trim() == "")
                    {
                        sbErrorMsg.Append("Please Select Effective Date." + "<br/>");
                        DateCount++;
                    }
                    if (hdfisTraining.Value == "true")
                    {
                        if (txt_expirationdate.Text.ToString().Trim() == "")
                        {
                            sbErrorMsg.Append("Please Select Review Date." + "<br/>");
                            DateCount++;
                        }
                    }
                    if (txt_expirationdate.Text.ToString().Trim() != "")
                    {
                        if (DateCount == 0)
                        {
                            if (Convert.ToDateTime(txt_EffectiveDate.Text.Trim()) == Convert.ToDateTime(txt_expirationdate.Text.Trim()))
                            {
                                sbErrorMsg.Append("Effective Date and Review Date must not be same." + "<br/>");
                            }
                            else if (Convert.ToDateTime(txt_EffectiveDate.Text.Trim()) > Convert.ToDateTime(txt_expirationdate.Text.Trim()))
                            {
                                sbErrorMsg.Append("Effective Date should not be greater than Review Date." + "<br/>");
                            }
                        }
                    }
                    if (txt_Reject.Value.Trim() == "")
                    {
                        sbErrorMsg.Append("Please Enter the Comments." + "<br/>");
                    }
                    if (sbErrorMsg.Length > 8)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
                        return;
                    }
                    else
                    {
                        DataTable dt1 = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                        int docPCount = DMS_Bal.DMS_PendingDocRequestCount(Convert.ToInt32(hdfPKID.Value));
                        if (dt1.Rows[0]["Training"].ToString() == "False")
                        {
                            if (docPCount == 0)
                            {
                                DataTable _dtCompany = fillCompany();
                                if (!string.IsNullOrEmpty(_dtCompany.Rows[0]["CompanyLogo"].ToString()))
                                {
                                    docObjects.effective = Convert.ToDateTime(txt_EffectiveDate.Text.Trim());
                                    if (txt_expirationdate.Text.ToString().Trim() != "")
                                    {
                                        docObjects.expiration = Convert.ToDateTime(txt_expirationdate.Text.Trim());
                                    }
                                    else
                                    {
                                        docObjects.expiration = null;
                                    }
                                    docObjects.DocumentID = hdfPKID.Value;
                                    docObjects.Purpose = Regex.Replace(txt_Reject.Value.Trim(), @"\s+", " ");
                                    docObjects.DMS_IntiatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                    int DMS_ins = DMS_Bal.DMS_inserteffectivedate(docObjects, ref transaction, ref EFFConn);

                                    docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                                    docObjects.RoleID = 11;//Author
                                    docObjects.EmpID = 0;
                                    docObjects.FileType1 = ".pdf";
                                    docObjects.Mode = 0;
                                    System.Data.DataTable dt = DMS_Bal.DMS_GetDocumentLocationList(docObjects, ref transaction, ref EFFConn);
                                    string SourcePath = Server.MapPath(@dt.Rows[0]["FilePath"].ToString());
                                    string File2Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                                    string TargetPath = (storefolder + File2Path);
                                    PdfReader reader = new PdfReader(SourcePath);
                                    PdfStamper stamper = new PdfStamper(reader, new FileStream(TargetPath, FileMode.Create));
                                    int pages = reader.NumberOfPages;
                                    stamper.InsertPage(1, iTextSharp.text.PageSize.A4);

                                    PdfContentByte overContent = stamper.GetOverContent(1);

                                    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                                    image.Alignment = iTextSharp.text.Image.ALIGN_RIGHT;
                                    image.SetAbsolutePosition(460, 800);
                                    image.ScaleToFit(110, 55);
                                    overContent.AddImage(image);



                                    DataSet ds = DMS_Bal.DMS_ShowFirstPageDetails(hdfPKID.Value, ref transaction, ref EFFConn);
                                    float[] HeadertablecolWidths = { 340f, 160f };
                                    PdfPTable Headertab = new PdfPTable(HeadertablecolWidths);
                                    Headertab.TotalWidth = 500F;
                                    //row 1
                                    string DocumentName = ds.Tables[0].Rows[0]["DocumentName"].ToString();
                                    PdfPCell DocName = new PdfPCell(new Phrase(DocumentName,
                                                            new Font(Font.FontFamily.TIMES_ROMAN, 12F, Font.BOLD)));
                                    DocName.Colspan = 2;
                                    DocName.HorizontalAlignment = Element.ALIGN_CENTER; //0=Left, 1=Centre, 2=Right
                                                                                        //Style
                                                                                        //cell.BorderColor = new BaseColor(System.Drawing.Color.LightGray);
                                                                                        //cell.BorderWidth = 3f;
                                    DocName.Border = Rectangle.NO_BORDER;
                                    Headertab.AddCell(DocName);
                                    //row 2
                                    string DocumentType = "Document Type     : " + ds.Tables[0].Rows[0]["DocumentType"].ToString();
                                    PdfPCell DocType = new PdfPCell(new Phrase(DocumentType,
                                                       new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                    DocType.Colspan = 2;
                                    DocType.Border = Rectangle.NO_BORDER;
                                    Headertab.AddCell(DocType);
                                    //row 3
                                    string DocNum = null;
                                    if (Convert.ToInt32(ds.Tables[0].Rows[0]["VersionNumber"]) <= 9)
                                    {
                                        DocNum = "Document Number: " + ds.Tables[0].Rows[0]["DocumentNumber"].ToString() + "-0" + ds.Tables[0].Rows[0]["VersionNumber"].ToString();
                                    }
                                    else
                                    {
                                        DocNum = "Document Number: " + ds.Tables[0].Rows[0]["DocumentNumber"].ToString() + "-" + ds.Tables[0].Rows[0]["VersionNumber"].ToString();
                                    }
                                    PdfPCell DocNumber = new PdfPCell(new Phrase(DocNum,
                                                        new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                    DocNumber.Border = Rectangle.NO_BORDER;
                                    Headertab.AddCell(DocNumber);
                                    string EffDate = "Effective Date: " + ds.Tables[0].Rows[0]["EffectiveDate"].ToString();
                                    PdfPCell EffectiveDate = new PdfPCell(new Phrase(EffDate,
                                                        new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                    EffectiveDate.Border = Rectangle.NO_BORDER;
                                    Headertab.AddCell(EffectiveDate);
                                    //row 4
                                    string Dept = "Department            : " + ds.Tables[0].Rows[0]["DocDepartment"].ToString();
                                    PdfPCell Department = new PdfPCell(new Phrase(Dept,
                                                        new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                    Department.Border = Rectangle.NO_BORDER;
                                    Headertab.AddCell(Department);
                                    string RevDate = "Review Date   : " + ds.Tables[0].Rows[0]["ExpirationDate"].ToString();
                                    PdfPCell ReviewDate = new PdfPCell(new Phrase(RevDate,
                                                        new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                    ReviewDate.Border = Rectangle.NO_BORDER;
                                    Headertab.AddCell(ReviewDate);
                                    iTextSharp.text.Document Headerdocument = overContent.PdfDocument;
                                    Headertab.WriteSelectedRows(0, -1, 40f, Headerdocument.Top - 20, overContent);

                                    int tableCount = 0;
                                    if (ds.Tables[4].Rows.Count > 0)
                                    {
                                        tableCount = ds.Tables.Count - 1;
                                    }
                                    else
                                    {
                                        tableCount = ds.Tables.Count - 2;
                                    }
                                    float[] tablecolWidths = { 80f, 170f, 80f, 170f };
                                    PdfPTable Contenttab = new PdfPTable(tablecolWidths);
                                    Contenttab.TotalWidth = 520F;
                                    StringBuilder tblHeaderText = new StringBuilder();
                                    for (int i = 1; i <= tableCount; i++)
                                    {
                                        switch (i)
                                        {
                                            case 1:
                                                tblHeaderText.Clear();
                                                tblHeaderText.Append("Prepared By");
                                                break;
                                            case 2:
                                                tblHeaderText.Clear();
                                                tblHeaderText.Append("Reviewed By");
                                                break;
                                            case 3:
                                                tblHeaderText.Clear();
                                                tblHeaderText.Append("Approved By");
                                                break;
                                            case 4:
                                                tblHeaderText.Clear();
                                                tblHeaderText.Append("Authorized By");
                                                break;
                                            default:
                                                break;
                                        }
                                        PdfPCell tblHeader = new PdfPCell(new Phrase(tblHeaderText.ToString(),
                                        new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                        tblHeader.Colspan = 4;
                                        tblHeader.HorizontalAlignment = Element.ALIGN_MIDDLE; //0=Left, 1=Centre, 2=Right
                                        tblHeader.Padding = 5;                                  //Style
                                        tblHeader.BackgroundColor = new BaseColor(System.Drawing.Color.LightGray);
                                        Contenttab.AddCell(tblHeader);
                                        for (int j = 1; j <= ds.Tables[i].Rows.Count; j++)
                                        {
                                            PdfPCell Namecell1 = new PdfPCell(new Phrase("Name         :",
                                            new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                            Namecell1.Border = Rectangle.LEFT_BORDER;
                                            Namecell1.Padding = 5;
                                            Contenttab.AddCell(Namecell1);
                                            PdfPCell Namecell2 = new PdfPCell(new Phrase(ds.Tables[i].Rows[j - 1][0].ToString(),
                                                                new Font(Font.FontFamily.TIMES_ROMAN, 12F, 0, BaseColor.BLUE)));
                                            Namecell2.Border = Rectangle.NO_BORDER;
                                            Namecell2.Padding = 5;
                                            Contenttab.AddCell(Namecell2);
                                            PdfPCell Descell1 = new PdfPCell(new Phrase("Designation   :",
                                                                new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                            Descell1.Border = Rectangle.NO_BORDER;
                                            Descell1.Padding = 5;
                                            Contenttab.AddCell(Descell1);
                                            PdfPCell Descell2 = new PdfPCell(new Phrase(ds.Tables[i].Rows[j - 1][3].ToString(),
                                                                new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                            Descell2.Border = Rectangle.RIGHT_BORDER;
                                            Descell2.Padding = 5;
                                            Contenttab.AddCell(Descell2);
                                            PdfPCell Datecell1 = new PdfPCell(new Phrase("Date           :",
                                               new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                            Datecell1.BorderWidthTop = 0;
                                            Datecell1.BorderWidthRight = 0;
                                            Datecell1.Padding = 5;
                                            Contenttab.AddCell(Datecell1);
                                            PdfPCell Datecell2 = new PdfPCell(new Phrase(ds.Tables[i].Rows[j - 1][1].ToString(),
                                                                new Font(Font.FontFamily.TIMES_ROMAN, 12F, 0, BaseColor.BLUE)));
                                            Datecell2.Border = Rectangle.BOTTOM_BORDER;
                                            Datecell2.Padding = 5;
                                            Contenttab.AddCell(Datecell2);
                                            PdfPCell Deptcell1 = new PdfPCell(new Phrase("Department    :",
                                                               new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                            Deptcell1.Border = Rectangle.BOTTOM_BORDER;
                                            Deptcell1.Padding = 5;
                                            Contenttab.AddCell(Deptcell1);
                                            PdfPCell Deptcell2 = new PdfPCell(new Phrase(ds.Tables[i].Rows[j - 1][2].ToString(),
                                                                new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                            Deptcell2.BorderWidthTop = 0;
                                            Deptcell2.BorderWidthLeft = 0;
                                            Deptcell2.Padding = 5;
                                            Contenttab.AddCell(Deptcell2);
                                        }
                                    }
                                    ColumnText ct = new ColumnText(stamper.GetOverContent(1));
                                    ct.AddElement(Contenttab);
                                    Rectangle rect = new Rectangle(0, 90, 0, 136);
                                    ct.SetSimpleColumn(-25, 56, PageSize.A4.Width + 25, PageSize.A4.Height - 175);
                                    ct.Go();

                                    PdfContentByte middleContent = stamper.GetOverContent(1);
                                    PdfPTable Declerationtab = new PdfPTable(1);
                                    Declerationtab.TotalWidth = 520F;
                                    //row 1
                                    PdfPCell Decleration = new PdfPCell(new Phrase("This document is electronically signed and doesn’t require manual signature",
                                                        new Font(Font.FontFamily.TIMES_ROMAN, 10F)));
                                    Decleration.HorizontalAlignment = Element.ALIGN_CENTER;
                                    Decleration.BorderWidth = 0;
                                    Declerationtab.AddCell(Decleration);
                                    iTextSharp.text.Document Declarationdocument = middleContent.PdfDocument;
                                    Declerationtab.WriteSelectedRows(0, -1, 40f, Declarationdocument.Bottom + 100f, middleContent);

                                    PdfContentByte underContent = stamper.GetOverContent(1);
                                    PdfPTable Footertab = new PdfPTable(1);
                                    Footertab.TotalWidth = 520F;
                                    //row 1
                                    PdfPCell FootHeader = new PdfPCell(new Phrase("CONFIDENTIAL AND PROPRIETARY",
                                                        new Font(Font.FontFamily.TIMES_ROMAN, 12F, Font.BOLD, BaseColor.BLACK)));
                                    FootHeader.HorizontalAlignment = Element.ALIGN_CENTER;
                                    FootHeader.BorderWidthBottom = 0;
                                    Footertab.AddCell(FootHeader);
                                    //row 2
                                    PdfPCell FootBody = new PdfPCell(new Phrase("The contents of this document are confidential and proprietary to Aizant Drug Research Solutions Pvt. Ltd. Any unauthorized use, disclosure or reproduction is strictly prohibited.",
                                                        new Font(Font.FontFamily.TIMES_ROMAN, 10F)));
                                    FootBody.BorderWidthTop = 0;
                                    Footertab.AddCell(FootBody);
                                    iTextSharp.text.Document Footerdocument = underContent.PdfDocument;
                                    Footertab.WriteSelectedRows(0, -1, 40f, Footerdocument.Bottom + 70f, underContent);
                                    stamper.Close();

                                    docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                                    docObjects.RoleID = 9;//approver
                                    docObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                    docObjects.FilePath1 = "N/A";
                                    docObjects.FilePath2 = string.Format(@"~/Scripts/pdf.js/web/") + File2Path;
                                    docObjects.isTemp = 0;
                                    docObjects.FileType1 = "N/A";
                                    docObjects.FileType2 = ".pdf";
                                    docObjects.FileName1 = "N/A";
                                    docObjects.FileName2 = File2Path;
                                    int DOC_Ins = DMS_Bal.DMS_InsertDocumentLocation(docObjects, ref transaction, ref EFFConn);

                                    //DataTable dt1 = DMS_Bal.DMS_GetDocByID(hdfPKID.Value, ref transaction, ref EFFConn);
                                    transaction.Commit();
                                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Effective Date and Review Date are Updated for <b>" + dt1.Rows[0]["DocumentNumber"].ToString() + "</b> Document Number Successfully.", "success", "ReloadEffectiveWithML()");
                                }
                            }
                            else
                            {
                                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "There is/are document distribution request/s pending , effective date cannot be given", "error", "ReloadEffetive()");
                            }
                        }
                        else
                        {
                            bool TrainingCount = DMS_Bal.TMS_AllowDocumentActions(3, Convert.ToInt32(hdfDocID.Value), Convert.ToInt32(hdfPKID.Value));
                            if (TrainingCount == true && docPCount == 0)
                            {
                                DataTable _dtCompany = fillCompany();
                                if (!string.IsNullOrEmpty(_dtCompany.Rows[0]["CompanyLogo"].ToString()))
                                {
                                    docObjects.effective = Convert.ToDateTime(txt_EffectiveDate.Text.Trim());
                                    if (txt_expirationdate.Text.ToString().Trim() != "")
                                    {
                                        docObjects.expiration = Convert.ToDateTime(txt_expirationdate.Text.Trim());
                                    }
                                    else
                                    {
                                        docObjects.expiration = null;
                                    }
                                    docObjects.DocumentID = hdfPKID.Value;
                                    docObjects.Purpose = Regex.Replace(txt_Reject.Value.Trim(), @"\s+", " ");
                                    docObjects.DMS_IntiatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                    int DMS_ins = DMS_Bal.DMS_inserteffectivedate(docObjects, ref transaction, ref EFFConn);

                                    docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                                    docObjects.RoleID = 11;//Author
                                    docObjects.EmpID = 0;
                                    docObjects.FileType1 = ".pdf";
                                    docObjects.Mode = 0;
                                    System.Data.DataTable dt = DMS_Bal.DMS_GetDocumentLocationList(docObjects, ref transaction, ref EFFConn);
                                    string SourcePath = Server.MapPath(@dt.Rows[0]["FilePath"].ToString());
                                    string File2Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                                    string TargetPath = (storefolder + File2Path);
                                    PdfReader reader = new PdfReader(SourcePath);
                                    PdfStamper stamper = new PdfStamper(reader, new FileStream(TargetPath, FileMode.Create));
                                    int pages = reader.NumberOfPages;
                                    stamper.InsertPage(1, iTextSharp.text.PageSize.A4);

                                    PdfContentByte overContent = stamper.GetOverContent(1);

                                    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                                    image.Alignment = iTextSharp.text.Image.ALIGN_RIGHT;
                                    image.SetAbsolutePosition(460, 800);
                                    image.ScaleToFit(110, 55);
                                    overContent.AddImage(image);



                                    DataSet ds = DMS_Bal.DMS_ShowFirstPageDetails(hdfPKID.Value, ref transaction, ref EFFConn);
                                    float[] HeadertablecolWidths = { 340f, 160f };
                                    PdfPTable Headertab = new PdfPTable(HeadertablecolWidths);
                                    Headertab.TotalWidth = 500F;
                                    //row 1
                                    string DocumentName = ds.Tables[0].Rows[0]["DocumentName"].ToString();
                                    PdfPCell DocName = new PdfPCell(new Phrase(DocumentName,
                                                            new Font(Font.FontFamily.TIMES_ROMAN, 12F, Font.BOLD)));
                                    DocName.Colspan = 2;
                                    DocName.HorizontalAlignment = Element.ALIGN_CENTER; //0=Left, 1=Centre, 2=Right
                                                                                        //Style
                                                                                        //cell.BorderColor = new BaseColor(System.Drawing.Color.LightGray);
                                                                                        //cell.BorderWidth = 3f;
                                    DocName.Border = Rectangle.NO_BORDER;
                                    Headertab.AddCell(DocName);
                                    //row 2
                                    string DocumentType = "Document Type     : " + ds.Tables[0].Rows[0]["DocumentType"].ToString();
                                    PdfPCell DocType = new PdfPCell(new Phrase(DocumentType,
                                                       new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                    DocType.Colspan = 2;
                                    DocType.Border = Rectangle.NO_BORDER;
                                    Headertab.AddCell(DocType);
                                    //row 3
                                    string DocNum = null;
                                    if (Convert.ToInt32(ds.Tables[0].Rows[0]["VersionNumber"]) <= 9)
                                    {
                                        DocNum = "Document Number: " + ds.Tables[0].Rows[0]["DocumentNumber"].ToString() + "-0" + ds.Tables[0].Rows[0]["VersionNumber"].ToString();
                                    }
                                    else
                                    {
                                        DocNum = "Document Number: " + ds.Tables[0].Rows[0]["DocumentNumber"].ToString() + "-" + ds.Tables[0].Rows[0]["VersionNumber"].ToString();
                                    }
                                    PdfPCell DocNumber = new PdfPCell(new Phrase(DocNum,
                                                        new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                    DocNumber.Border = Rectangle.NO_BORDER;
                                    Headertab.AddCell(DocNumber);
                                    string EffDate = "Effective Date: " + ds.Tables[0].Rows[0]["EffectiveDate"].ToString();
                                    PdfPCell EffectiveDate = new PdfPCell(new Phrase(EffDate,
                                                        new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                    EffectiveDate.Border = Rectangle.NO_BORDER;
                                    Headertab.AddCell(EffectiveDate);
                                    //row 4
                                    string Dept = "Department            : " + ds.Tables[0].Rows[0]["DocDepartment"].ToString();
                                    PdfPCell Department = new PdfPCell(new Phrase(Dept,
                                                        new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                    Department.Border = Rectangle.NO_BORDER;
                                    Headertab.AddCell(Department);
                                    string RevDate = "Review Date   : " + ds.Tables[0].Rows[0]["ExpirationDate"].ToString();
                                    PdfPCell ReviewDate = new PdfPCell(new Phrase(RevDate,
                                                        new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                    ReviewDate.Border = Rectangle.NO_BORDER;
                                    Headertab.AddCell(ReviewDate);
                                    iTextSharp.text.Document Headerdocument = overContent.PdfDocument;
                                    Headertab.WriteSelectedRows(0, -1, 40f, Headerdocument.Top - 20, overContent);

                                    int tableCount = 0;
                                    if (ds.Tables[4].Rows.Count > 0)
                                    {
                                        tableCount = ds.Tables.Count - 1;
                                    }
                                    else
                                    {
                                        tableCount = ds.Tables.Count - 2;
                                    }
                                    float[] tablecolWidths = { 80f, 170f, 80f, 170f };
                                    PdfPTable Contenttab = new PdfPTable(tablecolWidths);
                                    Contenttab.TotalWidth = 520F;
                                    StringBuilder tblHeaderText = new StringBuilder();
                                    for (int i = 1; i <= tableCount; i++)
                                    {
                                        switch (i)
                                        {
                                            case 1:
                                                tblHeaderText.Clear();
                                                tblHeaderText.Append("Prepared By");
                                                break;
                                            case 2:
                                                tblHeaderText.Clear();
                                                tblHeaderText.Append("Reviewed By");
                                                break;
                                            case 3:
                                                tblHeaderText.Clear();
                                                tblHeaderText.Append("Approved By");
                                                break;
                                            case 4:
                                                tblHeaderText.Clear();
                                                tblHeaderText.Append("Authorized By");
                                                break;
                                            default:
                                                break;
                                        }
                                        PdfPCell tblHeader = new PdfPCell(new Phrase(tblHeaderText.ToString(),
                                        new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                        tblHeader.Colspan = 4;
                                        tblHeader.HorizontalAlignment = Element.ALIGN_MIDDLE; //0=Left, 1=Centre, 2=Right
                                        tblHeader.Padding = 5;                                  //Style
                                        tblHeader.BackgroundColor = new BaseColor(System.Drawing.Color.LightGray);
                                        Contenttab.AddCell(tblHeader);
                                        for (int j = 1; j <= ds.Tables[i].Rows.Count; j++)
                                        {
                                            PdfPCell Namecell1 = new PdfPCell(new Phrase("Name         :",
                                            new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                            Namecell1.Border = Rectangle.LEFT_BORDER;
                                            Namecell1.Padding = 5;
                                            Contenttab.AddCell(Namecell1);
                                            PdfPCell Namecell2 = new PdfPCell(new Phrase(ds.Tables[i].Rows[j - 1][0].ToString(),
                                                                new Font(Font.FontFamily.TIMES_ROMAN, 12F, 0, BaseColor.BLUE)));
                                            Namecell2.Border = Rectangle.NO_BORDER;
                                            Namecell2.Padding = 5;
                                            Contenttab.AddCell(Namecell2);
                                            PdfPCell Descell1 = new PdfPCell(new Phrase("Designation   :",
                                                                new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                            Descell1.Border = Rectangle.NO_BORDER;
                                            Descell1.Padding = 5;
                                            Contenttab.AddCell(Descell1);
                                            PdfPCell Descell2 = new PdfPCell(new Phrase(ds.Tables[i].Rows[j - 1][3].ToString(),
                                                                new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                            Descell2.Border = Rectangle.RIGHT_BORDER;
                                            Descell2.Padding = 5;
                                            Contenttab.AddCell(Descell2);
                                            PdfPCell Datecell1 = new PdfPCell(new Phrase("Date           :",
                                               new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                            Datecell1.BorderWidthTop = 0;
                                            Datecell1.BorderWidthRight = 0;
                                            Datecell1.Padding = 5;
                                            Contenttab.AddCell(Datecell1);
                                            PdfPCell Datecell2 = new PdfPCell(new Phrase(ds.Tables[i].Rows[j - 1][1].ToString(),
                                                                new Font(Font.FontFamily.TIMES_ROMAN, 12F, 0, BaseColor.BLUE)));
                                            Datecell2.Border = Rectangle.BOTTOM_BORDER;
                                            Datecell2.Padding = 5;
                                            Contenttab.AddCell(Datecell2);
                                            PdfPCell Deptcell1 = new PdfPCell(new Phrase("Department    :",
                                                               new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                            Deptcell1.Border = Rectangle.BOTTOM_BORDER;
                                            Deptcell1.Padding = 5;
                                            Contenttab.AddCell(Deptcell1);
                                            PdfPCell Deptcell2 = new PdfPCell(new Phrase(ds.Tables[i].Rows[j - 1][2].ToString(),
                                                                new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                                            Deptcell2.BorderWidthTop = 0;
                                            Deptcell2.BorderWidthLeft = 0;
                                            Deptcell2.Padding = 5;
                                            Contenttab.AddCell(Deptcell2);
                                        }
                                    }
                                    ColumnText ct = new ColumnText(stamper.GetOverContent(1));
                                    ct.AddElement(Contenttab);
                                    //Rectangle rect = new Rectangle(0, 100, 0, 136);
                                    ct.SetSimpleColumn(-25, 56, PageSize.A4.Width + 25, PageSize.A4.Height - 175);
                                    ct.Go();

                                    PdfContentByte middleContent = stamper.GetOverContent(1);
                                    PdfPTable Declerationtab = new PdfPTable(1);
                                    Declerationtab.TotalWidth = 520F;
                                    //row 1
                                    PdfPCell Decleration = new PdfPCell(new Phrase("This document is electronically signed and doesn’t require manual signature",
                                                        new Font(Font.FontFamily.TIMES_ROMAN, 10F)));
                                    Decleration.HorizontalAlignment = Element.ALIGN_CENTER;
                                    Decleration.BorderWidth = 0;
                                    Declerationtab.AddCell(Decleration);
                                    iTextSharp.text.Document Declarationdocument = middleContent.PdfDocument;
                                    Declerationtab.WriteSelectedRows(0, -1, 40f, Declarationdocument.Bottom + 100f, middleContent);

                                    PdfContentByte underContent = stamper.GetOverContent(1);
                                    PdfPTable Footertab = new PdfPTable(1);
                                    Footertab.TotalWidth = 520F;
                                    //row 1
                                    PdfPCell FootHeader = new PdfPCell(new Phrase("CONFIDENTIAL AND PROPRIETARY",
                                                        new Font(Font.FontFamily.TIMES_ROMAN, 12F, Font.BOLD, BaseColor.BLACK)));
                                    FootHeader.HorizontalAlignment = Element.ALIGN_CENTER;
                                    FootHeader.BorderWidthBottom = 0;
                                    Footertab.AddCell(FootHeader);
                                    //row 2
                                    PdfPCell FootBody = new PdfPCell(new Phrase("The contents of this document are confidential and proprietary to Aizant Drug Research Solutions Pvt. Ltd. Any unauthorized use, disclosure or reproduction is strictly prohibited.",
                                                        new Font(Font.FontFamily.TIMES_ROMAN, 10F)));
                                    FootBody.BorderWidthTop = 0;
                                    Footertab.AddCell(FootBody);
                                    iTextSharp.text.Document Footerdocument = underContent.PdfDocument;
                                    Footertab.WriteSelectedRows(0, -1, 40f, Footerdocument.Bottom + 70f, underContent);
                                    stamper.Close();

                                    docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                                    docObjects.RoleID = 9;//approver
                                    docObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                    docObjects.FilePath1 = "N/A";
                                    docObjects.FilePath2 = string.Format(@"~/Scripts/pdf.js/web/") + File2Path;
                                    docObjects.isTemp = 0;
                                    docObjects.FileType1 = "N/A";
                                    docObjects.FileType2 = ".pdf";
                                    docObjects.FileName1 = "N/A";
                                    docObjects.FileName2 = File2Path;
                                    int DOC_Ins = DMS_Bal.DMS_InsertDocumentLocation(docObjects, ref transaction, ref EFFConn);

                                    //DataTable dt1 = DMS_Bal.DMS_GetDocByID(hdfPKID.Value, ref transaction, ref EFFConn);
                                    transaction.Commit();
                                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Effective Date and Review Date are Updated for <b>" + dt1.Rows[0]["DocumentNumber"].ToString() + "</b> Document Number Successfully.", "success", "ReloadEffectiveWithML()");
                                }
                            }
                            else if (docPCount == 1)
                            {
                                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "There is/are document distribution request/s pending, effective date cannot be given.", "error", "ReloadEffetive()");
                            }
                            else if (TrainingCount == false && dt1.Rows[0]["RequestTypeID"].ToString() == "1")
                            {
                                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Not able to effective New document", "error", "ReloadEffetive()");
                            }
                            else if (TrainingCount == false && dt1.Rows[0]["RequestTypeID"].ToString() == "2")
                            {
                                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Not able to effective Revision Doc", "error", "ReloadEffetive()");
                            }
                        }
                    }
                }
            }            
            catch (Exception ex)
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EFD_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EFD_M2:" + strline + "  " + "Effective and Review Dates update failed.", "error");
            }
            finally
            {
                if (EFFConn != null)
                {
                    EFFConn.Close();
                    EFFConn.Dispose();
                }
            }
        }
        //protected void btn_Submit_Click(object sender, EventArgs e)
        //{
        //    SqlTransaction transaction = null;
        //    SqlConnection EFFConn = null;
        //    btn_Submit.Enabled = false;
        //    try
        //    {
        //        StringBuilder sbErrorMsg = new StringBuilder();
        //        int DateCount = 0;
        //        if (txt_EffectiveDate.Text.Trim() == "")
        //        {
        //            sbErrorMsg.Append("Please Select Effective Date." + "<br/>");
        //            DateCount++;
        //        }
        //        if (txt_expirationdate.Text.ToString().Trim() == "")
        //        {
        //            sbErrorMsg.Append("Please Select Review Date." + "<br/>");
        //            DateCount++;
        //        }
        //        if (DateCount == 0)
        //        {
        //            if (Convert.ToDateTime(txt_EffectiveDate.Text.Trim()) == Convert.ToDateTime(txt_expirationdate.Text.Trim()))
        //            {
        //                sbErrorMsg.Append("Effective Date and Review Date must not be same." + "<br/>");
        //            }
        //            else if (Convert.ToDateTime(txt_EffectiveDate.Text.Trim()) > Convert.ToDateTime(txt_expirationdate.Text.Trim()))
        //            {
        //                sbErrorMsg.Append("Effective Date should not be greater than Review Date." + "<br/>");
        //            }
        //        }
        //        if (txt_Reject.Value.Trim() == "")
        //        {
        //            sbErrorMsg.Append("Please Enter the Comments." + "<br/>");
        //        }
        //        if (sbErrorMsg.Length > 8)
        //        {
        //            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
        //            return;
        //        }
        //        else
        //        {
        //            DataTable dt1 = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
        //            int docPCount = DMS_Bal.DMS_PendingDocRequestCount(Convert.ToInt32(hdfPKID.Value));
        //            if (dt1.Rows[0]["Training"].ToString() == "False")
        //            {
        //                if (docPCount == 0)
        //                {
        //                    DataTable _dtCompany = fillCompany();
        //                    if (!string.IsNullOrEmpty(_dtCompany.Rows[0]["CompanyLogo"].ToString()))
        //                    {
        //                        docObjects.effective = Convert.ToDateTime(txt_EffectiveDate.Text.Trim());
        //                        docObjects.expiration = Convert.ToDateTime(txt_expirationdate.Text.Trim());
        //                        docObjects.DocumentID = hdfPKID.Value;
        //                        docObjects.Purpose = Regex.Replace(txt_Reject.Value.Trim(), @"\s+", " ");
        //                        docObjects.DMS_IntiatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
        //                        int DMS_ins = DMS_Bal.DMS_inserteffectivedate(docObjects, ref transaction, ref EFFConn);

        //                        docObjects.Version = Convert.ToInt32(hdfPKID.Value);
        //                        docObjects.RoleID = 11;//Author
        //                        docObjects.EmpID = 0;
        //                        docObjects.FileType1 = ".pdf";
        //                        docObjects.Mode = 0;
        //                        System.Data.DataTable dt = DMS_Bal.DMS_GetDocumentLocationList(docObjects, ref transaction, ref EFFConn);
        //                        string SourcePath = Server.MapPath(@dt.Rows[0]["FilePath"].ToString());
        //                        string File2Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
        //                        string TargetPath = (storefolder + File2Path);
        //                        PdfReader reader = new PdfReader(SourcePath);
        //                        PdfStamper stamper = new PdfStamper(reader, new FileStream(TargetPath, FileMode.Create));
        //                        int pages = reader.NumberOfPages;
        //                        stamper.InsertPage(1, iTextSharp.text.PageSize.A4);

        //                        PdfContentByte overContent = stamper.GetOverContent(1);

        //                        iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
        //                        image.Alignment = iTextSharp.text.Image.ALIGN_RIGHT;
        //                        image.SetAbsolutePosition(460, 800);
        //                        image.ScaleToFit(110, 55);
        //                        overContent.AddImage(image);



        //                        DataSet ds = DMS_Bal.DMS_ShowFirstPageDetails(hdfPKID.Value, ref transaction, ref EFFConn);
        //                        float[] HeadertablecolWidths = { 340f, 160f };
        //                        PdfPTable Headertab = new PdfPTable(HeadertablecolWidths);
        //                        Headertab.TotalWidth = 500F;
        //                        //row 1
        //                        string DocumentName = ds.Tables[0].Rows[0]["DocumentName"].ToString();
        //                        PdfPCell DocName = new PdfPCell(new Phrase(DocumentName,
        //                                                new Font(Font.FontFamily.TIMES_ROMAN, 12F, Font.BOLD)));
        //                        DocName.Colspan = 2;
        //                        DocName.HorizontalAlignment = Element.ALIGN_CENTER; //0=Left, 1=Centre, 2=Right
        //                                                                            //Style
        //                                                                            //cell.BorderColor = new BaseColor(System.Drawing.Color.LightGray);
        //                                                                            //cell.BorderWidth = 3f;
        //                        DocName.Border = Rectangle.NO_BORDER;
        //                        Headertab.AddCell(DocName);
        //                        //row 2
        //                        string DocumentType = "Document Type     : " + ds.Tables[0].Rows[0]["DocumentType"].ToString();
        //                        PdfPCell DocType = new PdfPCell(new Phrase(DocumentType,
        //                                           new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
        //                        DocType.Colspan = 2;
        //                        DocType.Border = Rectangle.NO_BORDER;
        //                        Headertab.AddCell(DocType);
        //                        //row 3
        //                        string DocNum = null;
        //                        if (Convert.ToInt32(ds.Tables[0].Rows[0]["VersionNumber"]) <= 9)
        //                        {
        //                            DocNum = "Document Number: " + ds.Tables[0].Rows[0]["DocumentNumber"].ToString() + "-0" + ds.Tables[0].Rows[0]["VersionNumber"].ToString();
        //                        }
        //                        else
        //                        {
        //                            DocNum = "Document Number: " + ds.Tables[0].Rows[0]["DocumentNumber"].ToString() + "-" + ds.Tables[0].Rows[0]["VersionNumber"].ToString();
        //                        }
        //                        PdfPCell DocNumber = new PdfPCell(new Phrase(DocNum,
        //                                            new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
        //                        DocNumber.Border = Rectangle.NO_BORDER;
        //                        Headertab.AddCell(DocNumber);
        //                        string EffDate = "Effective Date: " + ds.Tables[0].Rows[0]["EffectiveDate"].ToString();
        //                        PdfPCell EffectiveDate = new PdfPCell(new Phrase(EffDate,
        //                                            new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
        //                        EffectiveDate.Border = Rectangle.NO_BORDER;
        //                        Headertab.AddCell(EffectiveDate);
        //                        //row 4
        //                        string Dept = "Department            : " + ds.Tables[0].Rows[0]["DocDepartment"].ToString();
        //                        PdfPCell Department = new PdfPCell(new Phrase(Dept,
        //                                            new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
        //                        Department.Border = Rectangle.NO_BORDER;
        //                        Headertab.AddCell(Department);
        //                        string RevDate = "Review Date   : " + ds.Tables[0].Rows[0]["ExpirationDate"].ToString();
        //                        PdfPCell ReviewDate = new PdfPCell(new Phrase(RevDate,
        //                                            new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
        //                        ReviewDate.Border = Rectangle.NO_BORDER;
        //                        Headertab.AddCell(ReviewDate);
        //                        iTextSharp.text.Document Headerdocument = overContent.PdfDocument;
        //                        Headertab.WriteSelectedRows(0, -1, 40f, Headerdocument.Top - 20, overContent);

        //                        int tableCount = 0;
        //                        if (ds.Tables[4].Rows.Count > 0)
        //                        {
        //                            tableCount = ds.Tables.Count - 1;
        //                        }
        //                        else
        //                        {
        //                            tableCount = ds.Tables.Count - 2;
        //                        }
        //                        float[] tablecolWidths = { 80f, 170f, 80f, 170f };
        //                        PdfPTable Contenttab = new PdfPTable(tablecolWidths);
        //                        Contenttab.TotalWidth = 520F;
        //                        StringBuilder tblHeaderText = new StringBuilder();
        //                        for (int i = 1; i <= tableCount; i++)
        //                        {
        //                            switch (i)
        //                            {
        //                                case 1:
        //                                    tblHeaderText.Clear();
        //                                    tblHeaderText.Append("Prepared By");
        //                                    break;
        //                                case 2:
        //                                    tblHeaderText.Clear();
        //                                    tblHeaderText.Append("Reviewed By");
        //                                    break;
        //                                case 3:
        //                                    tblHeaderText.Clear();
        //                                    tblHeaderText.Append("Approved By");
        //                                    break;
        //                                case 4:
        //                                    tblHeaderText.Clear();
        //                                    tblHeaderText.Append("Authorized By");
        //                                    break;
        //                                default:
        //                                    break;
        //                            }
        //                            PdfPCell tblHeader = new PdfPCell(new Phrase(tblHeaderText.ToString(),
        //                            new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
        //                            tblHeader.Colspan = 4;
        //                            tblHeader.HorizontalAlignment = Element.ALIGN_MIDDLE; //0=Left, 1=Centre, 2=Right
        //                            tblHeader.Padding = 5;                                  //Style
        //                            tblHeader.BackgroundColor = new BaseColor(System.Drawing.Color.LightGray);
        //                            Contenttab.AddCell(tblHeader);
        //                            for (int j = 1; j <= ds.Tables[i].Rows.Count; j++)
        //                            {
        //                                PdfPCell Namecell1 = new PdfPCell(new Phrase("Name         :",
        //                                new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
        //                                Namecell1.Border = Rectangle.LEFT_BORDER;
        //                                Namecell1.Padding = 5;
        //                                Contenttab.AddCell(Namecell1);
        //                                PdfPCell Namecell2 = new PdfPCell(new Phrase(ds.Tables[i].Rows[j - 1][0].ToString(),
        //                                                    new Font(Font.FontFamily.TIMES_ROMAN, 12F, 0, BaseColor.BLUE)));
        //                                Namecell2.Border = Rectangle.NO_BORDER;
        //                                Namecell2.Padding = 5;
        //                                Contenttab.AddCell(Namecell2);
        //                                PdfPCell Descell1 = new PdfPCell(new Phrase("Designation   :",
        //                                                    new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
        //                                Descell1.Border = Rectangle.NO_BORDER;
        //                                Descell1.Padding = 5;
        //                                Contenttab.AddCell(Descell1);
        //                                PdfPCell Descell2 = new PdfPCell(new Phrase(ds.Tables[i].Rows[j - 1][3].ToString(),
        //                                                    new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
        //                                Descell2.Border = Rectangle.RIGHT_BORDER;
        //                                Descell2.Padding = 5;
        //                                Contenttab.AddCell(Descell2);
        //                                PdfPCell Datecell1 = new PdfPCell(new Phrase("Date           :",
        //                                   new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
        //                                Datecell1.BorderWidthTop = 0;
        //                                Datecell1.BorderWidthRight = 0;
        //                                Datecell1.Padding = 5;
        //                                Contenttab.AddCell(Datecell1);
        //                                PdfPCell Datecell2 = new PdfPCell(new Phrase(ds.Tables[i].Rows[j - 1][1].ToString(),
        //                                                    new Font(Font.FontFamily.TIMES_ROMAN, 12F, 0, BaseColor.BLUE)));
        //                                Datecell2.Border = Rectangle.BOTTOM_BORDER;
        //                                Datecell2.Padding = 5;
        //                                Contenttab.AddCell(Datecell2);
        //                                PdfPCell Deptcell1 = new PdfPCell(new Phrase("Department    :",
        //                                                   new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
        //                                Deptcell1.Border = Rectangle.BOTTOM_BORDER;
        //                                Deptcell1.Padding = 5;
        //                                Contenttab.AddCell(Deptcell1);
        //                                PdfPCell Deptcell2 = new PdfPCell(new Phrase(ds.Tables[i].Rows[j - 1][2].ToString(),
        //                                                    new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
        //                                Deptcell2.BorderWidthTop = 0;
        //                                Deptcell2.BorderWidthLeft = 0;
        //                                Deptcell2.Padding = 5;
        //                                Contenttab.AddCell(Deptcell2);
        //                            }
        //                        }
        //                        ColumnText ct = new ColumnText(stamper.GetOverContent(1));
        //                        ct.AddElement(Contenttab);
        //                        Rectangle rect = new Rectangle(0, 90, 0, 136);
        //                        ct.SetSimpleColumn(-25, 56, PageSize.A4.Width + 25, PageSize.A4.Height - 175);
        //                        ct.Go();

        //                        PdfContentByte middleContent = stamper.GetOverContent(1);
        //                        PdfPTable Declerationtab = new PdfPTable(1);
        //                        Declerationtab.TotalWidth = 520F;
        //                        //row 1
        //                        PdfPCell Decleration = new PdfPCell(new Phrase("This document is electronically signed and doesn’t require manual signature",
        //                                            new Font(Font.FontFamily.TIMES_ROMAN, 10F)));
        //                        Decleration.HorizontalAlignment = Element.ALIGN_CENTER;
        //                        Decleration.BorderWidth = 0;
        //                        Declerationtab.AddCell(Decleration);
        //                        iTextSharp.text.Document Declarationdocument = middleContent.PdfDocument;
        //                        Declerationtab.WriteSelectedRows(0, -1, 40f, Declarationdocument.Bottom + 100f, middleContent);

        //                        PdfContentByte underContent = stamper.GetOverContent(1);
        //                        PdfPTable Footertab = new PdfPTable(1);
        //                        Footertab.TotalWidth = 520F;
        //                        //row 1
        //                        PdfPCell FootHeader = new PdfPCell(new Phrase("CONFIDENTIAL AND PROPRIETARY",
        //                                            new Font(Font.FontFamily.TIMES_ROMAN, 12F, Font.BOLD, BaseColor.BLACK)));
        //                        FootHeader.HorizontalAlignment = Element.ALIGN_CENTER;
        //                        FootHeader.BorderWidthBottom = 0;
        //                        Footertab.AddCell(FootHeader);
        //                        //row 2
        //                        PdfPCell FootBody = new PdfPCell(new Phrase("The contents of this document are confidential and proprietary to Aizant Drug Research Solutions Pvt. Ltd. Any unauthorized use, disclosure or reproduction is strictly prohibited.This document is electronically signed and doesn’t require manual signature",
        //                                            new Font(Font.FontFamily.TIMES_ROMAN, 10F)));
        //                        FootBody.BorderWidthTop = 0;
        //                        Footertab.AddCell(FootBody);
        //                        iTextSharp.text.Document Footerdocument = underContent.PdfDocument;
        //                        Footertab.WriteSelectedRows(0, -1, 40f, Footerdocument.Bottom + 70f, underContent);
        //                        stamper.Close();

        //                        docObjects.Version = Convert.ToInt32(hdfPKID.Value);
        //                        docObjects.RoleID = 9;//approver
        //                        docObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
        //                        docObjects.FilePath1 = "N/A";
        //                        docObjects.FilePath2 = string.Format(@"~/Scripts/pdf.js/web/") + File2Path;
        //                        docObjects.isTemp = 0;
        //                        docObjects.FileType1 = "N/A";
        //                        docObjects.FileType2 = ".pdf";
        //                        docObjects.FileName1 = "N/A";
        //                        docObjects.FileName2 = File2Path;
        //                        int DOC_Ins = DMS_Bal.DMS_InsertDocumentLocation(docObjects, ref transaction, ref EFFConn);

        //                        //DataTable dt1 = DMS_Bal.DMS_GetDocByID(hdfPKID.Value, ref transaction, ref EFFConn);
        //                        transaction.Commit();
        //                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Effective Date and Review Date are Updated for <b>" + dt1.Rows[0]["DocumentNumber"].ToString() + "</b> Document Number Successfully.", "success", "ReloadEffectiveWithML()");
        //                    }
        //                }
        //                else
        //                {
        //                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "There is/are document distribution request/s pending , effective date cannot be given", "error", "ReloadEffetive()");
        //                }
        //            }
        //            else
        //            {
        //                bool TrainingCount = DMS_Bal.TMS_AllowDocumentActions(3, Convert.ToInt32(hdfDocID.Value), Convert.ToInt32(hdfPKID.Value));
        //                if (TrainingCount == true && docPCount == 0)
        //                {
        //                    DataTable _dtCompany = fillCompany();
        //                    if (!string.IsNullOrEmpty(_dtCompany.Rows[0]["CompanyLogo"].ToString()))
        //                    {
        //                        docObjects.effective = Convert.ToDateTime(txt_EffectiveDate.Text.Trim());
        //                        docObjects.expiration = Convert.ToDateTime(txt_expirationdate.Text.Trim());
        //                        docObjects.DocumentID = hdfPKID.Value;
        //                        docObjects.Purpose = Regex.Replace(txt_Reject.Value.Trim(), @"\s+", " ");
        //                        docObjects.DMS_IntiatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
        //                        int DMS_ins = DMS_Bal.DMS_inserteffectivedate(docObjects, ref transaction, ref EFFConn);

        //                        docObjects.Version = Convert.ToInt32(hdfPKID.Value);
        //                        docObjects.RoleID = 11;//Author
        //                        docObjects.EmpID = 0;
        //                        docObjects.FileType1 = ".pdf";
        //                        docObjects.Mode = 0;
        //                        System.Data.DataTable dt = DMS_Bal.DMS_GetDocumentLocationList(docObjects, ref transaction, ref EFFConn);
        //                        string SourcePath = Server.MapPath(@dt.Rows[0]["FilePath"].ToString());
        //                        string File2Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
        //                        string TargetPath = (storefolder + File2Path);
        //                        PdfReader reader = new PdfReader(SourcePath);
        //                        PdfStamper stamper = new PdfStamper(reader, new FileStream(TargetPath, FileMode.Create));
        //                        int pages = reader.NumberOfPages;
        //                        stamper.InsertPage(1, iTextSharp.text.PageSize.A4);

        //                        PdfContentByte overContent = stamper.GetOverContent(1);

        //                        iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
        //                        image.Alignment = iTextSharp.text.Image.ALIGN_RIGHT;
        //                        image.SetAbsolutePosition(460, 800);
        //                        image.ScaleToFit(110, 55);
        //                        overContent.AddImage(image);



        //                        DataSet ds = DMS_Bal.DMS_ShowFirstPageDetails(hdfPKID.Value, ref transaction, ref EFFConn);
        //                        float[] HeadertablecolWidths = { 340f, 160f };
        //                        PdfPTable Headertab = new PdfPTable(HeadertablecolWidths);
        //                        Headertab.TotalWidth = 500F;
        //                        //row 1
        //                        string DocumentName = ds.Tables[0].Rows[0]["DocumentName"].ToString();
        //                        PdfPCell DocName = new PdfPCell(new Phrase(DocumentName,
        //                                                new Font(Font.FontFamily.TIMES_ROMAN, 12F, Font.BOLD)));
        //                        DocName.Colspan = 2;
        //                        DocName.HorizontalAlignment = Element.ALIGN_CENTER; //0=Left, 1=Centre, 2=Right
        //                                                                            //Style
        //                                                                            //cell.BorderColor = new BaseColor(System.Drawing.Color.LightGray);
        //                                                                            //cell.BorderWidth = 3f;
        //                        DocName.Border = Rectangle.NO_BORDER;
        //                        Headertab.AddCell(DocName);
        //                        //row 2
        //                        string DocumentType = "Document Type     : " + ds.Tables[0].Rows[0]["DocumentType"].ToString();
        //                        PdfPCell DocType = new PdfPCell(new Phrase(DocumentType,
        //                                           new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
        //                        DocType.Colspan = 2;
        //                        DocType.Border = Rectangle.NO_BORDER;
        //                        Headertab.AddCell(DocType);
        //                        //row 3
        //                        string DocNum = null;
        //                        if (Convert.ToInt32(ds.Tables[0].Rows[0]["VersionNumber"]) <= 9)
        //                        {
        //                            DocNum = "Document Number: " + ds.Tables[0].Rows[0]["DocumentNumber"].ToString() + "-0" + ds.Tables[0].Rows[0]["VersionNumber"].ToString();
        //                        }
        //                        else
        //                        {
        //                            DocNum = "Document Number: " + ds.Tables[0].Rows[0]["DocumentNumber"].ToString() + "-" + ds.Tables[0].Rows[0]["VersionNumber"].ToString();
        //                        }
        //                        PdfPCell DocNumber = new PdfPCell(new Phrase(DocNum,
        //                                            new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
        //                        DocNumber.Border = Rectangle.NO_BORDER;
        //                        Headertab.AddCell(DocNumber);
        //                        string EffDate = "Effective Date: " + ds.Tables[0].Rows[0]["EffectiveDate"].ToString();
        //                        PdfPCell EffectiveDate = new PdfPCell(new Phrase(EffDate,
        //                                            new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
        //                        EffectiveDate.Border = Rectangle.NO_BORDER;
        //                        Headertab.AddCell(EffectiveDate);
        //                        //row 4
        //                        string Dept = "Department            : " + ds.Tables[0].Rows[0]["DocDepartment"].ToString();
        //                        PdfPCell Department = new PdfPCell(new Phrase(Dept,
        //                                            new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
        //                        Department.Border = Rectangle.NO_BORDER;
        //                        Headertab.AddCell(Department);
        //                        string RevDate = "Review Date   : " + ds.Tables[0].Rows[0]["ExpirationDate"].ToString();
        //                        PdfPCell ReviewDate = new PdfPCell(new Phrase(RevDate,
        //                                            new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
        //                        ReviewDate.Border = Rectangle.NO_BORDER;
        //                        Headertab.AddCell(ReviewDate);
        //                        iTextSharp.text.Document Headerdocument = overContent.PdfDocument;
        //                        Headertab.WriteSelectedRows(0, -1, 40f, Headerdocument.Top - 20, overContent);

        //                        int tableCount = 0;
        //                        if (ds.Tables[4].Rows.Count > 0)
        //                        {
        //                            tableCount = ds.Tables.Count - 1;
        //                        }
        //                        else
        //                        {
        //                            tableCount = ds.Tables.Count - 2;
        //                        }
        //                        float[] tablecolWidths = { 80f, 170f, 80f, 170f };
        //                        PdfPTable Contenttab = new PdfPTable(tablecolWidths);
        //                        Contenttab.TotalWidth = 520F;
        //                        StringBuilder tblHeaderText = new StringBuilder();
        //                        for (int i = 1; i <= tableCount; i++)
        //                        {
        //                            switch (i)
        //                            {
        //                                case 1:
        //                                    tblHeaderText.Clear();
        //                                    tblHeaderText.Append("Prepared By");
        //                                    break;
        //                                case 2:
        //                                    tblHeaderText.Clear();
        //                                    tblHeaderText.Append("Reviewed By");
        //                                    break;
        //                                case 3:
        //                                    tblHeaderText.Clear();
        //                                    tblHeaderText.Append("Approved By");
        //                                    break;
        //                                case 4:
        //                                    tblHeaderText.Clear();
        //                                    tblHeaderText.Append("Authorized By");
        //                                    break;
        //                                default:
        //                                    break;
        //                            }
        //                            PdfPCell tblHeader = new PdfPCell(new Phrase(tblHeaderText.ToString(),
        //                            new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
        //                            tblHeader.Colspan = 4;
        //                            tblHeader.HorizontalAlignment = Element.ALIGN_MIDDLE; //0=Left, 1=Centre, 2=Right
        //                            tblHeader.Padding = 5;                                  //Style
        //                            tblHeader.BackgroundColor = new BaseColor(System.Drawing.Color.LightGray);
        //                            Contenttab.AddCell(tblHeader);
        //                            for (int j = 1; j <= ds.Tables[i].Rows.Count; j++)
        //                            {
        //                                PdfPCell Namecell1 = new PdfPCell(new Phrase("Name         :",
        //                                new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
        //                                Namecell1.Border = Rectangle.LEFT_BORDER;
        //                                Namecell1.Padding = 5;
        //                                Contenttab.AddCell(Namecell1);
        //                                PdfPCell Namecell2 = new PdfPCell(new Phrase(ds.Tables[i].Rows[j - 1][0].ToString(),
        //                                                    new Font(Font.FontFamily.TIMES_ROMAN, 12F, 0, BaseColor.BLUE)));
        //                                Namecell2.Border = Rectangle.NO_BORDER;
        //                                Namecell2.Padding = 5;
        //                                Contenttab.AddCell(Namecell2);
        //                                PdfPCell Descell1 = new PdfPCell(new Phrase("Designation   :",
        //                                                    new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
        //                                Descell1.Border = Rectangle.NO_BORDER;
        //                                Descell1.Padding = 5;
        //                                Contenttab.AddCell(Descell1);
        //                                PdfPCell Descell2 = new PdfPCell(new Phrase(ds.Tables[i].Rows[j - 1][3].ToString(),
        //                                                    new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
        //                                Descell2.Border = Rectangle.RIGHT_BORDER;
        //                                Descell2.Padding = 5;
        //                                Contenttab.AddCell(Descell2);
        //                                PdfPCell Datecell1 = new PdfPCell(new Phrase("Date           :",
        //                                   new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
        //                                Datecell1.BorderWidthTop = 0;
        //                                Datecell1.BorderWidthRight = 0;
        //                                Datecell1.Padding = 5;
        //                                Contenttab.AddCell(Datecell1);
        //                                PdfPCell Datecell2 = new PdfPCell(new Phrase(ds.Tables[i].Rows[j - 1][1].ToString(),
        //                                                    new Font(Font.FontFamily.TIMES_ROMAN, 12F, 0, BaseColor.BLUE)));
        //                                Datecell2.Border = Rectangle.BOTTOM_BORDER;
        //                                Datecell2.Padding = 5;
        //                                Contenttab.AddCell(Datecell2);
        //                                PdfPCell Deptcell1 = new PdfPCell(new Phrase("Department    :",
        //                                                   new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
        //                                Deptcell1.Border = Rectangle.BOTTOM_BORDER;
        //                                Deptcell1.Padding = 5;
        //                                Contenttab.AddCell(Deptcell1);
        //                                PdfPCell Deptcell2 = new PdfPCell(new Phrase(ds.Tables[i].Rows[j - 1][2].ToString(),
        //                                                    new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
        //                                Deptcell2.BorderWidthTop = 0;
        //                                Deptcell2.BorderWidthLeft = 0;
        //                                Deptcell2.Padding = 5;
        //                                Contenttab.AddCell(Deptcell2);
        //                            }
        //                        }
        //                        ColumnText ct = new ColumnText(stamper.GetOverContent(1));
        //                        ct.AddElement(Contenttab);
        //                        //Rectangle rect = new Rectangle(0, 100, 0, 136);
        //                        ct.SetSimpleColumn(-25, 56, PageSize.A4.Width + 25, PageSize.A4.Height - 175);
        //                        ct.Go();

        //                        PdfContentByte underContent = stamper.GetOverContent(1);
        //                        PdfPTable Footertab = new PdfPTable(1);
        //                        Footertab.TotalWidth = 520F;
        //                        //row 1
        //                        PdfPCell FootHeader = new PdfPCell(new Phrase("CONFIDENTIAL AND PROPRIETARY",
        //                                            new Font(Font.FontFamily.TIMES_ROMAN, 12F, Font.BOLD, BaseColor.BLACK)));
        //                        FootHeader.HorizontalAlignment = Element.ALIGN_CENTER;
        //                        FootHeader.BorderWidthBottom = 0;
        //                        Footertab.AddCell(FootHeader);
        //                        //row 2
        //                        PdfPCell FootBody = new PdfPCell(new Phrase("The contents of this document are confidential and proprietary to Aizant Drug Research Solutions Pvt. Ltd. Any unauthorized use, disclosure or reproduction is strictly prohibited.This document is electronically signed and doesn’t require manual signature",
        //                                            new Font(Font.FontFamily.TIMES_ROMAN, 10F)));
        //                        FootBody.BorderWidthTop = 0;
        //                        Footertab.AddCell(FootBody);
        //                        iTextSharp.text.Document Footerdocument = underContent.PdfDocument;
        //                        Footertab.WriteSelectedRows(0, -1, 40f, Footerdocument.Bottom + 70f, underContent);
        //                        stamper.Close();

        //                        docObjects.Version = Convert.ToInt32(hdfPKID.Value);
        //                        docObjects.RoleID = 9;//approver
        //                        docObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
        //                        docObjects.FilePath1 = "N/A";
        //                        docObjects.FilePath2 = string.Format(@"~/Scripts/pdf.js/web/") + File2Path;
        //                        docObjects.isTemp = 0;
        //                        docObjects.FileType1 = "N/A";
        //                        docObjects.FileType2 = ".pdf";
        //                        docObjects.FileName1 = "N/A";
        //                        docObjects.FileName2 = File2Path;
        //                        int DOC_Ins = DMS_Bal.DMS_InsertDocumentLocation(docObjects, ref transaction, ref EFFConn);

        //                        //DataTable dt1 = DMS_Bal.DMS_GetDocByID(hdfPKID.Value, ref transaction, ref EFFConn);
        //                        transaction.Commit();
        //                        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Effective Date and Review Date are Updated for <b>" + dt1.Rows[0]["DocumentNumber"].ToString() + "</b> Document Number Successfully.", "success", "ReloadEffectiveWithML()");
        //                    }
        //                }
        //                else if (docPCount == 1)
        //                {
        //                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "There is/are document distribution request/s pending, effective date cannot be given.", "error", "ReloadEffetive()");
        //                }
        //                else if (TrainingCount == false && dt1.Rows[0]["RequestTypeID"].ToString()=="1")
        //                {
        //                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Not able to effective New document", "error", "ReloadEffetive()");
        //                }
        //                else if (TrainingCount == false && dt1.Rows[0]["RequestTypeID"].ToString() == "2")
        //                {
        //                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Not able to effective Revision Doc", "error", "ReloadEffetive()");
        //                }
        //            }                                 
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (transaction != null)
        //        {
        //            transaction.Rollback();
        //        }
        //        string strline = HelpClass.LineNo(ex);
        //        string strMsg = HelpClass.SQLEscapeString(ex.Message);
        //        Aizant_log.Error("EFD_M2:" + strline + "  " + strMsg);
        //        HelpClass.custAlertMsg(this, this.GetType(), "EFD_M2:" + strline + "  " + "Effective and Review Dates update failed.", "error");
        //    }
        //    finally
        //    {
        //        if (EFFConn != null)
        //        {
        //            EFFConn.Close();
        //            EFFConn.Dispose();
        //        }
        //    }

        //}
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page. .";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        protected void btnDatesEdit_Click(object sender, EventArgs e)
        {
            btnDatesEdit.Enabled = false;
            try
            {
                DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
               // int TrainingCount = DMS_Bal.DMS_DocumentTrainingCount(2, Convert.ToInt32(hdfDocID.Value), Convert.ToInt32(hdfPKID.Value));
                DataTable dt1 = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                hdfDocNum.Value = dt1.Rows[0]["DocumentNumber"].ToString();

                JQDataTableBO objJQDataTableBO = new JQDataTableBO();
                objJQDataTableBO.iMode = 0;
                objJQDataTableBO.pkid = Convert.ToInt32(hdfPKID.Value);
                objJQDataTableBO.iDisplayLength = 0;
                objJQDataTableBO.iDisplayStart = 0;
                objJQDataTableBO.iSortCol = 0;
                objJQDataTableBO.sSortDir = "";
                objJQDataTableBO.sSearch = "";
                objJQDataTableBO.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                DataTable dt = DMS_Bal.DMS_Listofeffectivedocuments(objJQDataTableBO);
                txt_EffectiveDate.Text = dt.Rows[0]["EffectiveDate"].ToString();
                txt_expirationdate.Text = dt.Rows[0]["ExpirationDate"].ToString();
                if (txt_EffectiveDate.Text != "" && txt_expirationdate.Text != "")
                {
                    btn_Submit.Value = "Update";
                }
                else
                {
                    btn_Submit.Value = "Submit";
                }
                if(hdfisTraining.Value == "true")
                {
                    reviewdate.Visible = true;
                }
                else
                {
                    reviewdate.Visible = false;
                }
                txt_Reject.Value = "";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_lock').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EFD_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EFD_M3:" + strline + "  " + "Effective and Review Dates Loading failed.", "error");
            }
            finally
            {
                btnDatesEdit.Enabled = true;
            }
        }
        protected void btnDatesEdit1_Click(object sender, EventArgs e)
        {
            btnDatesEdit1.Enabled = false;
            try
            {
                DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
                bool TrainingCount = DMS_Bal.TMS_AllowDocumentActions(3, Convert.ToInt32(hdfDocID.Value), Convert.ToInt32(hdfPKID.Value));
                int PendingDocRequestCount = DMS_Bal.DMS_PendingDocRequestCount(Convert.ToInt32(hdfPKID.Value));
                DataTable dt1 = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                hdfDocNum.Value = dt1.Rows[0]["DocumentNumber"].ToString();
                bool Training = Convert.ToBoolean(dt1.Rows[0]["Training"].ToString());
                if (TrainingCount == false && Training == true && dt1.Rows[0]["RequestTypeID"].ToString()=="1")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop1", "custAlertMsg('<br/>Not able to effective New document.','warning');", true);
                }
                else if (TrainingCount == false && Training == true && dt1.Rows[0]["RequestTypeID"].ToString() == "2")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop1", "custAlertMsg('<br/>Not able to effective Revision document.','warning');", true);
                }
                else if (PendingDocRequestCount != 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop1", "custAlertMsg('<br/>There is/are document distribution request/s pending.','warning');", true);
                }
                else
                {
                    btn_Submit.Visible = true;
                    txt_EffectiveDate.Text = "";
                    txt_expirationdate.Text = "";
                    JQDataTableBO objJQDataTableBO = new JQDataTableBO();
                    objJQDataTableBO.iMode = 0;
                    objJQDataTableBO.pkid = Convert.ToInt32(hdfPKID.Value);
                    objJQDataTableBO.iDisplayLength = 0;
                    objJQDataTableBO.iDisplayStart = 0;
                    objJQDataTableBO.iSortCol = 0;
                    objJQDataTableBO.sSortDir = "";
                    objJQDataTableBO.sSearch = "";
                    objJQDataTableBO.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    DataTable dt = DMS_Bal.DMS_Listofeffectivedocuments(objJQDataTableBO);
                    txt_EffectiveDate.Text = dt.Rows[0]["EffectiveDate"].ToString();
                    txt_expirationdate.Text = dt.Rows[0]["ExpirationDate"].ToString();
                    if (txt_EffectiveDate.Text != "" && txt_expirationdate.Text != "")
                    {
                        btn_Submit.Value = "Update";
                    }
                    else
                    {
                        btn_Submit.Value = "Submit";
                    }
                    if (hdfisTraining.Value == "true")
                    {
                        reviewdate.Visible = true;
                    }
                    else
                    {
                        reviewdate.Visible = false;
                    }
                    txt_Reject.Value = "";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_lock').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EFD_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EFD_M4:" + strline + "  " + "Effective and Review Dates1 Loading failed.", "error");
            }
            finally
            {
                btnDatesEdit1.Enabled = true;
            }
        }
        protected void btnFileView_Click(object sender, EventArgs e)
        {
            btnFileView.Enabled = false;
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                hdfStatus.Value = DMS_DT.Rows[0]["DocStatus"].ToString();
                span1.InnerHtml = "<b>" + DMS_DT.Rows[0]["DocumentName"].ToString() + "</b>";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_ViewDoc').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                hdfViewType.Value = "0";
                hdfViewEmpID.Value = "0";
                hdfViewRoleID.Value = "0";//Author
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view1", "ViewPDFDocument('#divPDF_Viewer');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
                UpViewDL.Update();
                UpHeading.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EFD_M5:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EFD_M5:" + strline + "  " + "Effective document viewing failed.", "error");
            }
            finally
            {
                btnFileView.Enabled = true;
            }
        }
        private void InitializeThePage()
        {
            try
            {
                if (Request.QueryString["rtp"] != null)
                {
                    if (Request.QueryString["rtp"] == "1")
                    {
                        hdnRequestType.Value = "1";
                    }
                    else if (Request.QueryString["rtp"] == "2")
                    {
                        hdnRequestType.Value = "2";
                    }
                    else if (Request.QueryString["rtp"] == "3")
                    {
                        hdnRequestType.Value = "3";
                    }
                    else
                    {
                        hdnRequestType.Value = "0";
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("EFD_M6:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "EFD_M6:" + strline + "  " + "Cards view details loading failed.", "error");
            }
        }
        DataTable fillCompany()
        {
            try
            {
                CompanyBO objCompanyBO;
                UMS_BAL objUMS_BAL;
                DataTable dtcompany = new DataTable();
                objCompanyBO = new CompanyBO();
                objCompanyBO.Mode = 2;
                objCompanyBO.CompanyID = 0;
                objCompanyBO.CompanyCode = "";
                objCompanyBO.CompanyName = "";
                objCompanyBO.CompanyDescription = "";
                objCompanyBO.CompanyPhoneNo1 = "";
                objCompanyBO.CompanyPhoneNo2 = "";
                objCompanyBO.CompanyFaxNo1 = "";
                objCompanyBO.CompanyFaxNo2 = ""; //txtFaxNo2.Value;
                objCompanyBO.CompanyEmailID = "";
                objCompanyBO.CompanyWebUrl = "";
                objCompanyBO.CompanyAddress = "";
                objCompanyBO.CompanyLocation = "";// txtLocat.Value;
                objCompanyBO.CompanyCity = "";
                objCompanyBO.CompanyState = "";
                objCompanyBO.CompanyCountry = "";
                objCompanyBO.CompanyPinCode = "";
                objCompanyBO.CompanyStartDate = "";
                objCompanyBO.CompanyLogo = new byte[0];
                objUMS_BAL = new UMS_BAL();
                dtcompany = objUMS_BAL.CompanyQuery(objCompanyBO);
                return dtcompany;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}
