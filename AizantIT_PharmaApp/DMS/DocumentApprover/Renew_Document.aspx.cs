﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using UMS_BusinessLayer;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Text;
using System.Data.SqlClient;
using UMS_BO;

namespace AizantIT_PharmaApp.DMS.DocumentApprover
{
    public partial class Renew_Document : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        ReviewObjects reviewObjects = new ReviewObjects();
        DocObjects docObjects = new DocObjects();
        private string storefolder = HttpContext.Current.Server.MapPath(@"~/Scripts/pdf.js/web/");
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        hdfFileName.Value = "";
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=30").Length > 0)//9-Approver,30-DocController
                            {
                                InitializeThePage();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ARD_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ARD_M1:" + strline + "  " + "Document Load failed.", "error");
            }
        }
        protected void btn_RenewSubmit_Click(object sender, EventArgs e)
        {
            SqlTransaction transaction = null;
            SqlConnection RnwConn = null;
            btn_RenewSubmit.Enabled = false;
            try
            {
                docObjects.expiration = Convert.ToDateTime(txt_Expiration.Text.Trim());
                docObjects.DocumentID = hdfPKID.Value;
                docObjects.DMS_IntiatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                docObjects.TypeID = "R";                


                if (hdnRequestTypeID.Value=="4")
                {
                    //save new pdf file                  
                    if (ViewState["filename"].ToString().Trim() != "")
                    {
                        docObjects.Filename = ViewState["filename"].ToString();
                        docObjects.FileName2 = hdfFileName.Value.ToString();
                        docObjects.FilePath2 = "~/Scripts/pdf.js/web/" + hdfFileName.Value.ToString();
                        docObjects.FileType2 = "1";
                        int DMS_ins = DMS_Bal.DMS_insertRenewdate(docObjects,ref transaction,ref RnwConn);
                        HelpClass.custAlertMsg(this, this.GetType(), "Review Date is Updated for " + hdfDocNum.Value + " Document Number Successfully.", "success");
                    }
                    else
                    {                       
                        HelpClass.custAlertMsg(this, this.GetType(), "Please upload document.", "error");
                        return;
                    }

                }
                else {
                    //create new doc with  fresh firstpage
                    docObjects.Filename = "";
                    docObjects.FileName2 = "";
                    docObjects.FilePath2 = "";
                    docObjects.FileType2 = "0";
                    CreateNewDoumentWithFirstpage(docObjects,ref transaction,ref RnwConn);
                }

                transaction.Commit();
               
            }
            catch (Exception ex)
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ARD_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ARD_M2:" + strline + "  " + "Review Date Renewal Failed.", "error");
            }
            finally
            {
                RnwConn.Close();
                RnwConn.Dispose();
                btn_RenewSubmit.Enabled = true;
            }
        }
        private void CreateNewDoumentWithFirstpage(DocObjects docObjects,ref SqlTransaction _Transaction, ref SqlConnection DMS_Con)
        {
            DataTable _dtCompany = fillCompany();
            if (!string.IsNullOrEmpty(_dtCompany.Rows[0]["CompanyLogo"].ToString()))
            {
                int DMS_ins = DMS_Bal.DMS_insertRenewdate(docObjects,ref _Transaction,ref DMS_Con);
                docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                docObjects.RoleID = 11;//Author
                docObjects.EmpID = 0;
                docObjects.FileType1 = ".pdf";
                docObjects.Mode = 0;
                System.Data.DataTable dt = DMS_Bal.DMS_GetDocumentLocationList(docObjects, ref _Transaction, ref DMS_Con);
                string SourcePath = Server.MapPath(@dt.Rows[0]["FilePath"].ToString());
                string File2Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                string TargetPath = (storefolder + File2Path);
                PdfReader reader = new PdfReader(SourcePath);
                PdfStamper stamper = new PdfStamper(reader, new FileStream(TargetPath, FileMode.Create));
                int pages = reader.NumberOfPages;
                stamper.InsertPage(1, iTextSharp.text.PageSize.A4);

                PdfContentByte overContent = stamper.GetOverContent(1);

                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                image.Alignment = iTextSharp.text.Image.ALIGN_RIGHT;
                image.SetAbsolutePosition(460, 800);
                image.ScaleToFit(110, 55);
                overContent.AddImage(image);



                DataSet ds = DMS_Bal.DMS_ShowFirstPageDetails(hdfPKID.Value, ref _Transaction, ref DMS_Con);                
                float[] HeadertablecolWidths = { 340f, 160f };
                PdfPTable Headertab = new PdfPTable(HeadertablecolWidths);
                Headertab.TotalWidth = 500F;
                //row 1
                string DocumentName = "Title: " + ds.Tables[0].Rows[0]["DocumentName"].ToString();
                PdfPCell DocName = new PdfPCell(new Phrase(DocumentName,
                                        new Font(Font.FontFamily.TIMES_ROMAN, 12F, Font.BOLD)));
                DocName.Colspan = 2;
                DocName.HorizontalAlignment = Element.ALIGN_CENTER; //0=Left, 1=Centre, 2=Right
                                                                    //Style
                                                                    //cell.BorderColor = new BaseColor(System.Drawing.Color.LightGray);
                                                                    //cell.BorderWidth = 3f;
                DocName.Border = Rectangle.NO_BORDER;
                Headertab.AddCell(DocName);
                //row 2
                string DocumentType = "Document Type     : " + ds.Tables[0].Rows[0]["DocumentType"].ToString();
                PdfPCell DocType = new PdfPCell(new Phrase(DocumentType,
                                   new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                DocType.Colspan = 2;
                DocType.Border = Rectangle.NO_BORDER;
                Headertab.AddCell(DocType);
                //row 3
                string DocNum = null;
                if (Convert.ToInt32(ds.Tables[0].Rows[0]["VersionNumber"]) <= 9)
                {
                    DocNum = "Document Number: " + ds.Tables[0].Rows[0]["DocumentNumber"].ToString() + "-0" + ds.Tables[0].Rows[0]["VersionNumber"].ToString();
                }
                else
                {
                    DocNum = "Document Number: " + ds.Tables[0].Rows[0]["DocumentNumber"].ToString() + "-" + ds.Tables[0].Rows[0]["VersionNumber"].ToString();
                }
                PdfPCell DocNumber = new PdfPCell(new Phrase(DocNum,
                                    new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                DocNumber.Border = Rectangle.NO_BORDER;
                Headertab.AddCell(DocNumber);
                string EffDate = "Effective Date: " + ds.Tables[0].Rows[0]["EffectiveDate"].ToString();
                PdfPCell EffectiveDate = new PdfPCell(new Phrase(EffDate,
                                    new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                EffectiveDate.Border = Rectangle.NO_BORDER;
                Headertab.AddCell(EffectiveDate);
                //row 4
                string Dept = "Department            : " + ds.Tables[0].Rows[0]["DocDepartment"].ToString();
                PdfPCell Department = new PdfPCell(new Phrase(Dept,
                                    new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                Department.Border = Rectangle.NO_BORDER;
                Headertab.AddCell(Department);
                string RevDate = "Review Date   : " + ds.Tables[0].Rows[0]["ExpirationDate"].ToString();
                PdfPCell ReviewDate = new PdfPCell(new Phrase(RevDate,
                                    new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                ReviewDate.Border = Rectangle.NO_BORDER;
                Headertab.AddCell(ReviewDate);
                iTextSharp.text.Document Headerdocument = overContent.PdfDocument;
                Headertab.WriteSelectedRows(0, -1, 40f, Headerdocument.Top - 20, overContent);

                int tableCount = 0;
                if (ds.Tables[4].Rows.Count > 0)
                {
                    tableCount = ds.Tables.Count - 1;
                }
                else
                {
                    tableCount = ds.Tables.Count - 2;
                }
                float[] tablecolWidths = { 80f, 170f, 80f, 170f };
                PdfPTable Contenttab = new PdfPTable(tablecolWidths);
                Contenttab.TotalWidth = 520F;
                StringBuilder tblHeaderText = new StringBuilder();
                for (int i = 1; i <= tableCount; i++)
                {
                    switch (i)
                    {
                        case 1:
                            tblHeaderText.Clear();
                            tblHeaderText.Append("Prepared By");
                            break;
                        case 2:
                            tblHeaderText.Clear();
                            tblHeaderText.Append("Reviewed By");
                            break;
                        case 3:
                            tblHeaderText.Clear();
                            tblHeaderText.Append("Approved By");
                            break;
                        case 4:
                            tblHeaderText.Clear();
                            tblHeaderText.Append("Authorized By");
                            break;
                        default:
                            break;
                    }
                    PdfPCell tblHeader = new PdfPCell(new Phrase(tblHeaderText.ToString(),
                    new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                    tblHeader.Colspan = 4;
                    tblHeader.HorizontalAlignment = Element.ALIGN_MIDDLE; //0=Left, 1=Centre, 2=Right
                    tblHeader.Padding = 5;                                  //Style
                    tblHeader.BackgroundColor = new BaseColor(System.Drawing.Color.LightGray);
                    Contenttab.AddCell(tblHeader);
                    for (int j = 1; j <= ds.Tables[i].Rows.Count; j++)
                    {
                        PdfPCell Namecell1 = new PdfPCell(new Phrase("Name         :",
                        new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                        Namecell1.Border = Rectangle.LEFT_BORDER;
                        Namecell1.Padding = 5;
                        Contenttab.AddCell(Namecell1);
                        PdfPCell Namecell2 = new PdfPCell(new Phrase(ds.Tables[i].Rows[j - 1][0].ToString(),
                                            new Font(Font.FontFamily.TIMES_ROMAN, 12F, 0, BaseColor.BLUE)));
                        Namecell2.Border = Rectangle.NO_BORDER;
                        Namecell2.Padding = 5;
                        Contenttab.AddCell(Namecell2);
                        PdfPCell Descell1 = new PdfPCell(new Phrase("Designation   :",
                                            new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                        Descell1.Border = Rectangle.NO_BORDER;
                        Descell1.Padding = 5;
                        Contenttab.AddCell(Descell1);
                        PdfPCell Descell2 = new PdfPCell(new Phrase(ds.Tables[i].Rows[j - 1][3].ToString(),
                                            new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                        Descell2.Border = Rectangle.RIGHT_BORDER;
                        Descell2.Padding = 5;
                        Contenttab.AddCell(Descell2);
                        PdfPCell Datecell1 = new PdfPCell(new Phrase("Date           :",
                           new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                        Datecell1.BorderWidthTop = 0;
                        Datecell1.BorderWidthRight = 0;
                        Datecell1.Padding = 5;
                        Contenttab.AddCell(Datecell1);
                        PdfPCell Datecell2 = new PdfPCell(new Phrase(ds.Tables[i].Rows[j - 1][1].ToString(),
                                            new Font(Font.FontFamily.TIMES_ROMAN, 12F, 0, BaseColor.BLUE)));
                        Datecell2.Border = Rectangle.BOTTOM_BORDER;
                        Datecell2.Padding = 5;
                        Contenttab.AddCell(Datecell2);
                        PdfPCell Deptcell1 = new PdfPCell(new Phrase("Department    :",
                                           new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                        Deptcell1.Border = Rectangle.BOTTOM_BORDER;
                        Deptcell1.Padding = 5;
                        Contenttab.AddCell(Deptcell1);
                        PdfPCell Deptcell2 = new PdfPCell(new Phrase(ds.Tables[i].Rows[j - 1][2].ToString(),
                                            new Font(Font.FontFamily.TIMES_ROMAN, 12F)));
                        Deptcell2.BorderWidthTop = 0;
                        Deptcell2.BorderWidthLeft = 0;
                        Deptcell2.Padding = 5;
                        Contenttab.AddCell(Deptcell2);
                    }
                }
                ColumnText ct = new ColumnText(stamper.GetOverContent(1));
                ct.AddElement(Contenttab);
                Rectangle rect = new Rectangle(0, 90, 0, 136);
                ct.SetSimpleColumn(-25, 56, PageSize.A4.Width + 25, PageSize.A4.Height - 175);
                ct.Go();

                PdfContentByte underContent = stamper.GetOverContent(1);
                PdfPTable Footertab = new PdfPTable(1);
                Footertab.TotalWidth = 520F;
                //row 1
                PdfPCell FootHeader = new PdfPCell(new Phrase("CONFIDENTIAL AND PROPRIETARY",
                                    new Font(Font.FontFamily.TIMES_ROMAN, 12F, Font.BOLD, BaseColor.BLACK)));
                FootHeader.HorizontalAlignment = Element.ALIGN_CENTER;
                FootHeader.BorderWidthBottom = 0;
                Footertab.AddCell(FootHeader);
                //row 2
                PdfPCell FootBody = new PdfPCell(new Phrase("The contents of this document are confidential and proprietary to Aizant Drug Research Solutions Pvt. Ltd. Any unauthorized use, disclosure or reproduction is strictly prohibited.",
                                    new Font(Font.FontFamily.TIMES_ROMAN, 10F)));
                FootBody.BorderWidthTop = 0;
                Footertab.AddCell(FootBody);
                iTextSharp.text.Document Footerdocument = underContent.PdfDocument;
                Footertab.WriteSelectedRows(0, -1, 40f, Footerdocument.Bottom + 30f, underContent);
                stamper.Close();

                docObjects.Version = Convert.ToInt32(hdfPKID.Value);
                docObjects.RoleID = 9;//approver
                docObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                docObjects.FilePath1 = "N/A";
                docObjects.FilePath2 = string.Format(@"~/Scripts/pdf.js/web/") + File2Path;
                docObjects.isTemp = 0;
                docObjects.FileType1 = "N/A";
                docObjects.FileType2 = ".pdf";
                docObjects.FileName1 = "N/A";
                docObjects.FileName2 = File2Path;
                int DOC_Ins = DMS_Bal.DMS_InsertDocumentLocation(docObjects, ref _Transaction, ref DMS_Con);

                DataTable dt1 = DMS_Bal.DMS_GetDocByID(hdfPKID.Value, ref _Transaction, ref DMS_Con);
                HelpClass.custAlertMsg(this, this.GetType(), "Review Date is Updated for " + hdfDocNum.Value + " Document Number Successfully.", "success");
            }
        }
        DataTable fillCompany()
        {
            try
            {
                CompanyBO objCompanyBO;
                UMS_BAL objUMS_BAL;
                DataTable dtcompany = new DataTable();
                objCompanyBO = new CompanyBO();
                objCompanyBO.Mode = 2;
                objCompanyBO.CompanyID = 0;
                objCompanyBO.CompanyCode = "";
                objCompanyBO.CompanyName = "";
                objCompanyBO.CompanyDescription = "";
                objCompanyBO.CompanyPhoneNo1 = "";
                objCompanyBO.CompanyPhoneNo2 = "";
                objCompanyBO.CompanyFaxNo1 = "";
                objCompanyBO.CompanyFaxNo2 = ""; //txtFaxNo2.Value;
                objCompanyBO.CompanyEmailID = "";
                objCompanyBO.CompanyWebUrl = "";
                objCompanyBO.CompanyAddress = "";
                objCompanyBO.CompanyLocation = "";// txtLocat.Value;
                objCompanyBO.CompanyCity = "";
                objCompanyBO.CompanyState = "";
                objCompanyBO.CompanyCountry = "";
                objCompanyBO.CompanyPinCode = "";
                objCompanyBO.CompanyStartDate = "";
                objCompanyBO.CompanyLogo = new byte[0];
                objUMS_BAL = new UMS_BAL();
                dtcompany = objUMS_BAL.CompanyQuery(objCompanyBO);
                return dtcompany;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        protected void btnRenewDateEdit_Click(object sender, EventArgs e)
        {
            btnRenewDateEdit.Enabled = false;
            try
            {
                DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
                DataTable DMS_DT1 = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                hdfStatus.Value = DMS_DT1.Rows[0]["DocStatus"].ToString();
                hdfDocNum.Value = DMS_DT1.Rows[0]["DocumentNumber"].ToString();
                DataTable DMS_DT2 = DMS_Bal.DMS_GetNoOFAttempts(hdfPKID.Value);
                if (DMS_DT2.Rows.Count > 0)
                {
                    int a = DMS_DT2.Rows.Count;
                    if (a == 1)
                    {
                        lbl_Attempts.Text = "1";
                    }
                }
                else
                {
                    lbl_Attempts.Text = "2";
                }
                HiddenField1.Value = lbl_Attempts.Text;
                string exp = DMS_DT1.Rows[0]["ExpirationDate"].ToString();
                exp = exp.Replace(" 00:00:00", "");
                txt_Expiration.Text = exp;
                DateTime MinDate = Convert.ToDateTime(txt_Expiration.Text);
                HiddenField1.Value = MinDate.ToString("dd MMM yyyy");
                DateTime maxDate = Convert.ToDateTime(txt_Expiration.Text).AddYears(+2).AddDays(-1);
                hdfMaxDateForRenew.Value = maxDate.ToString("dd MMM yyyy");
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_Renew').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view1", "ViewDocument();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ARD_M5:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ARD_M5:" + strline + "  " + "Review Date loading Failed.", "error");
            }
            finally
            {
                btnRenewDateEdit.Enabled = true;
            }
        }
        private void InitializeThePage()
        {
            try
            {
                if (Request.QueryString["rtp"] != null)
                {
                    if (Request.QueryString["rtp"] == "1")
                    {
                        hdnRequestType.Value = "1";
                    }
                    else if (Request.QueryString["rtp"] == "2")
                    {
                        hdnRequestType.Value = "2";
                    }
                    else if (Request.QueryString["rtp"] == "4")
                    {
                        hdnRequestType.Value = "4";
                    }
                    else if (Request.QueryString["rtp"] == "5")
                    {
                        hdnRequestType.Value = "5";
                    }
                    else
                    {
                        hdnRequestType.Value = "0";
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ARD_M6:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ARD_M6:" + strline + "  " + "card view details loading Failed.", "error");
            }
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            btnUpload.Enabled = false;
            try
            {
                if (file_word.HasFile)
                {
                    if (file_word.FileName.Trim().Length > 4)
                    {
                        string ext = System.IO.Path.GetExtension(file_word.FileName);

                        if (ext.ToLower() == ".pdf")
                        {
                            ViewState["filename"] = file_word.FileName.Trim()/*.Substring(0, file_Document.FileName.Length - 4) + ".rtf"*/;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show3", "showViewDoc();", true);
                            //lb_Upload.Text = file_word.FileName.Trim()/*.Substring(0, file_Document.FileName.Length - 4) + ".rtf"*/;
                            byte[] imgbyte = null;
                            int length = file_word.PostedFile.ContentLength;
                            imgbyte = new byte[length];
                            HttpPostedFile img = file_word.PostedFile;
                            img.InputStream.Read(imgbyte, 0, length);
                            string mime = MimeType.GetMimeType(imgbyte, file_word.FileName);
                            if (mime.ToLower() != "application/pdf")
                            {
                                hdfFileName.Value = "";
                                lb_Upload.Text = "";
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    Please upload  pdf file only ! <br/> The uploaded file is not valid or corrupted.','warning');", true);
                                return;
                            }
                            
                            lb_Upload.Visible = true;
                            lb_Upload.Text = ViewState["filename"].ToString();
                            string strPDFMainFolderPath = HttpContext.Current.Server.MapPath(@"~/Scripts/pdf.js/web");
                            string File2Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                            string strTargPath = (strPDFMainFolderPath + "\\" + File2Path);
                            File.WriteAllBytes(strTargPath, imgbyte);                           
                            hdfFileName.Value = File2Path;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_Renew').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    Please upload  pdf file only.','warning');", true);
                        }
                        return;
                    }
                }
                //upUpload.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ARD_M8:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ARD_M8:" + strline + "  " + "File upload failed.", "error");
            }
            finally
            {
                btnUpload.Enabled = true;
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}