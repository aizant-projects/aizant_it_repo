﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using AizantIT_PharmaApp.UserControls;
using System;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_BusinessLayer;
namespace AizantIT_PharmaApp.DMS.DocumentApprover
{
    public partial class Initiated_Documents : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        ReviewObjects reviewObjects = new ReviewObjects();
        DocObjects docObjects = new DocObjects();
        UMS_BAL objUMS_BAL;
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=9").Length > 0)//9-Approver
                            {
                                if (Request.QueryString["Notification_ID"] != null)
                                {
                                    objUMS_BAL = new UMS_BAL();
                                    int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs); //2 represents NotifyEmp had visited the page through notification link.
                                    DataTable dt = DMS_Bal.DMS_NotificationDocVersionID(Convert.ToInt32(Request.QueryString["Notification_ID"]));
                                    //if (dt.Rows[0]["TrainingCount"].ToString() == "True" && dt.Rows[0]["PendingDocRequestCount"].ToString() == "0")
                                    //{
                                        hdfPKID.Value = dt.Rows[0]["VersionID"].ToString();                                
                                        //hdfTraining.Value = "0";
                                        btnViewDocumentDetails_Click(null, null);
                                    //}
                                    //else
                                    //{
                                    //    hdfPKID.Value = dt.Rows[0]["VersionID"].ToString();
                                    //    hdfTraining.Value = dt.Rows[0]["TrainingCount"].ToString();
                                    //    btnViewDocumentDetails1_Click(null, null);
                                    //}
                                }
                                InitializeThePage();
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                    else
                    {
                        ElectronicSign.buttonClick += new EventHandler(ElectronicSign_buttonClick);
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ID_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ID_M1:" + strline + "  " + "Document Load failed.", "error");
            }
        }
        protected void ElectronicSign_buttonClick(object sender, EventArgs e)
        {
            try
            {
                bool b = ElectronicSign.IsPasswordValid;
                if (b == true)
                {
                    if (ViewState["hdnAction"].ToString() == "Approve")
                    {
                        Approve();
                    }
                    else if (ViewState["hdnAction"].ToString() == "Revert")
                    {
                        Revert();
                    }
                }
                if (b == false)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Password is Incorrect!", "error");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ID_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ID_M2:" + strline + "  " + "Electronic Sign button click failed.", "error");
            }
        }
        protected void btn_Approve_Click(object sender, EventArgs e)
        {
            btn_Approve.Enabled = false;
            try
            {
                ViewState["hdnAction"] = "Approve";
                if (txt_requestType.Text == "Withdrawal Document")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "WithdrawHide();", true);
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "openElectronicSignModal();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ID_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ID_M3:" + strline + "  " + "Electronic Sign Approve Open failed.", "error");
            }
            finally
            {
                btn_Approve.Enabled = true;
            }
        }
        public void Approve()
        {
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                if (DMS_DT.Rows[0]["Status"].ToString() != "AR" && DMS_DT.Rows[0]["Status"].ToString() != "RJ")
                {
                    if (Session["dropdown"].ToString() == "withdraw")
                    {
                        //int docPCount = DMS_Bal.DMS_PendingDocRequestCount(Convert.ToInt32(hdfPKID.Value));
                        //if (DMS_DT.Rows[0]["Training"].ToString() == "False")
                        //{
                        //    if(docPCount == 0)
                        //    {
                                reviewObjects.Id = Convert.ToInt32(hdfPKID.Value);
                                reviewObjects.Status = 2;
                                reviewObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                reviewObjects.Comments = Regex.Replace(txt_Reject.Value.Trim(), @"\s+", " ");
                                int DMS_UP = DMS_Bal.DMS_ApproveDocument(reviewObjects);
                                Session.Remove("dropdown");
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "reloadtable();", true);
                                if (DMS_UP == 1)
                                {
                                DMS_Bal.DMS_DeleteDocumentRecommendations(reviewObjects);
                                HelpClass.custAlertMsg(this, this.GetType(), "Withdraw Document Initiation with Document Number <b>" + txt_DocumentNumber.Text + "</b>  Approved Successfully.", "success");
                                }
                                else
                                {
                                    HelpClass.custAlertMsg(this, this.GetType(), "Document Number <b>" + txt_DocumentNumber.Text + "</b> Withdraw Initiation Approved.", "success");
                                }
                        //    }
                        //    else
                        //    {
                        //        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "There is/are document distribution request/s pending, document cannot be withdrawn.", "error", "reloadtable()");
                        //    }
                        //}
                        //else
                        //{
                        //    bool TrainingCount = DMS_Bal.TMS_AllowDocumentActions(2, Convert.ToInt32(DMS_DT.Rows[0]["docnum"].ToString()), Convert.ToInt32(hdfPKID.Value));
                        //    if (TrainingCount == true && docPCount == 0)
                        //    {
                        //        reviewObjects.Id = Convert.ToInt32(hdfPKID.Value);
                        //        reviewObjects.Status = 2;
                        //        reviewObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        //        reviewObjects.Comments = Regex.Replace(txt_Reject.Value.Trim(), @"\s+", " ");
                        //        int DMS_UP = DMS_Bal.DMS_ApproveDocument(reviewObjects);
                        //        Session.Remove("dropdown");
                        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "reloadtable();", true);
                        //        if (DMS_UP == 1)
                        //        {
                        //            DMS_Bal.DMS_DeleteDocumentRecommendations(reviewObjects);
                        //            HelpClass.custAlertMsg(this, this.GetType(), "Document Number <b>" + txt_DocumentNumber.Text + "</b> Withdrawn Successfully.", "success");
                        //        }
                        //        else
                        //        {
                        //            HelpClass.custAlertMsg(this, this.GetType(), "Document Number <b>" + txt_DocumentNumber.Text + "</b> Withdraw Approved Successfully.", "success");
                        //        }
                        //    }
                        //    else if (docPCount == 1)
                        //    {
                        //        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "There is/are document distribution request/s pending, document cannot be withdrawn.", "error", "reloadtable()");
                        //    }
                        //    else if (TrainingCount == false)
                        //    {
                        //        HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document Under Training, Not able to Withdrawal Doc.", "error", "reloadtable()");
                        //    }
                        //}
                        
                    }
                    else if (Session["dropdown"].ToString() == "New")
                    {
                        reviewObjects.Id = Convert.ToInt32(hdfPKID.Value);
                        reviewObjects.Status = 1;
                        reviewObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        reviewObjects.Comments = Regex.Replace(txt_Reject.Value.Trim(), @"\s+", " ");
                        int DMS_UP = DMS_Bal.DMS_ApproveDocument(reviewObjects);
                        Session.Remove("dropdown");
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "reloadtable();", true);
                        if (DMS_UP == 1)
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "New Document Initiation with Document Number <b>" + txt_DocumentNumber.Text + "</b> Approved Successfully.", "success");
                        }
                        else
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Document Number <b>" + txt_DocumentNumber.Text + "</b> New Initiation Approved.", "success");
                        }
                    }
                    else if (Session["dropdown"].ToString() == "Revision")
                    {
                        reviewObjects.Id = Convert.ToInt32(hdfPKID.Value);
                        reviewObjects.Status = 1;
                        reviewObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        reviewObjects.Comments = Regex.Replace(txt_Reject.Value.Trim(), @"\s+", " ");
                        int DMS_UP = DMS_Bal.DMS_ApproveDocument(reviewObjects);
                        Session.Remove("dropdown");
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "reloadtable();", true);
                        if (DMS_UP == 1)
                        {
                        HelpClass.custAlertMsg(this, this.GetType(), " Revision Document Initiation with Document Number <b>" + txt_DocumentNumber.Text + "</b> Approved  Successfully.", "success");
                        }
                        else
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Document Number <b>" + txt_DocumentNumber.Text + "</b> Revision Initiation Approved.", "success");
                        }
                    }
                    else if (Session["dropdown"].ToString() == "renew")
                    {
                        reviewObjects.Id = Convert.ToInt32(hdfPKID.Value);
                        if (DMS_DT.Rows[0]["authorID"].ToString() == "0" || DMS_DT.Rows[0]["authorID"].ToString() == "")
                        {
                            reviewObjects.Status = 4;
                        }
                        else
                        {
                            reviewObjects.Status = 1;
                        }
                        reviewObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        reviewObjects.Comments = Regex.Replace(txt_Reject.Value.Trim(), @"\s+", " ");
                        int DMS_UP = DMS_Bal.DMS_ApproveDocument(reviewObjects);
                        Session.Remove("dropdown");
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "reloadtable();", true);
                        if (DMS_UP == 1)
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), " Renew Document Initiation with Document Number <b>" + txt_DocumentNumber.Text + "</b> Approved  Successfully.", "success");
                        }
                        else
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Document Number <b>" + txt_DocumentNumber.Text + "</b> Renew Initiation Approved.", "success");
                        }
                    }
                }
                else if (DMS_DT.Rows[0]["Status"].ToString() == "AR")
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document with Document Number <b>" + txt_DocumentNumber.Text + "</b> has been already Reverted.", "info", "reloadtable();");
                }
                else if (DMS_DT.Rows[0]["Status"].ToString() == "RJ")
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document with Document Number <b>" + txt_DocumentNumber.Text + "</b> has been already Rejected.", "info", "reloadtable();");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ID_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ID_M4:" + strline + "  " + "Approve status submission failed.", "error");
            }
        }
        protected void btn_Reject_Click(object sender, EventArgs e)
        {
            btn_Reject.Enabled = false;
            try
            {
                ViewState["hdnAction"] = "Revert";
                if (txt_requestType.Text == "Withdrawal Document")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "WithdrawHide();", true);
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "openElectronicSignModal();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ID_M5:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ID_M5:" + strline + "  " + "Electronic Sign Reject Open failed.", "error");
            }
            finally
            {
                btn_Reject.Enabled = true;
            }
        }
        public void Revert()
        {
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                if (DMS_DT.Rows[0]["Status"].ToString() != "AR" && DMS_DT.Rows[0]["Status"].ToString() != "RJ")
                {
                    reviewObjects.Id = Convert.ToInt32(hdfPKID.Value);
                    reviewObjects.Comments = Regex.Replace(txt_Reject.Value.Trim(), @"\s+", " ");
                    reviewObjects.DocStatus = "AR";
                    reviewObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    int DMS_Up = DMS_Bal.DMS_RejectDocument(reviewObjects);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "reloadtable();", true);
                    HelpClass.custAlertMsg(this, this.GetType(), "Document with Document Number <b>" + txt_DocumentNumber.Text + "</b> Reverted Successfully.", "success");
                }
                else if(DMS_DT.Rows[0]["Status"].ToString() == "AR")
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document with Document Number <b>" + txt_DocumentNumber.Text + "</b> has been already Reverted.", "info", "reloadtable();");
                }
                else if(DMS_DT.Rows[0]["Status"].ToString() == "RJ")
                {
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document with Document Number <b>" + txt_DocumentNumber.Text + "</b> has been already Rejected.", "info", "reloadtable();");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ID_M6:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ID_M6:" + strline + "  " + "Revert status submission failed.", "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        protected void btnViewDocumentDetails_Click(object sender, EventArgs e)
        {
            btnViewDocumentDetails.Enabled = false;
            try
            {
                DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
                DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
                ReviewObjects reviewObjects = new ReviewObjects();
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                if (DMS_DT.Rows.Count > 0)
                {
                    txt_requestType.Text = DMS_DT.Rows[0]["RequestType"].ToString();
                    txt_documentType.Text = DMS_DT.Rows[0]["DocumentType"].ToString();
                    txt_Department.Text = DMS_DT.Rows[0]["Department"].ToString();
                    txt_DocumentNumber.Text = DMS_DT.Rows[0]["DocumentNumber"].ToString();
                    txt_DocumentName.Text = DMS_DT.Rows[0]["DocumentName"].ToString();
                    if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "1")
                    {
                        Session["dropdown"] = "New";
                        lblComments.Text = "Purpose of Initiation";
                        txt_Comments.Text = DMS_DT.Rows[0]["CreatePurpose"].ToString();
                        //if (DMS_DT.Rows[0]["ControllerID"].ToString() == "0" || DMS_DT.Rows[0]["ControllerID"].ToString() == "")
                        //{
                        //    txt_Controller.Visible = false;
                        //    Label6.Visible = false;
                        //}
                    }
                    else if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "2")
                    {
                        Session["dropdown"] = "Revision";
                        lblComments.Text = "Reason for Change";
                        txt_Comments.Text = DMS_DT.Rows[0]["CreatePurpose"].ToString();
                        //if (DMS_DT.Rows[0]["ControllerID"].ToString() == "0" || DMS_DT.Rows[0]["ControllerID"].ToString() == "")
                        //{
                        //    txt_Controller.Visible = false;
                        //    Label6.Visible = false;
                        //}
                    }
                    else if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "3")
                    {
                        ///txt_Controller.Text = DMS_DT.Rows[0]["ControlledBy"].ToString();
                        lblComments.Text = "Reason for Withdrawal";
                        txt_Comments.Text = DMS_DT.Rows[0]["WithdrawPurpose"].ToString();
                        //txt_Controller.Visible = true;
                        //Label6.Visible = true;
                        Session["dropdown"] = "withdraw";
                        //btn_Approve.Text = "Withdraw";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "WithdrawHide();", true);
                    }
                    else if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "5")
                    {
                        //txt_Controller.Text = DMS_DT.Rows[0]["ControlledBy"].ToString();
                        lblComments.Text = "Reason for Renew";
                        txt_Comments.Text = DMS_DT.Rows[0]["WithdrawPurpose"].ToString();
                        //txt_Controller.Visible = true;
                        //Label6.Visible = true;
                        //if (DMS_DT.Rows[0]["authorID"].ToString()=="0" || DMS_DT.Rows[0]["authorID"].ToString() == "")
                        //{
                        //    txt_Author.Visible = false;
                        //    Label7.Visible = false;
                        //}
                        Session["dropdown"] = "renew";
                        //btn_Approve.Text = "Withdraw";
                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "WithdrawHide();", true);
                    }
                    txt_VersionID.Text = DMS_DT.Rows[0]["VersionNumber"].ToString();
                    if (DMS_DT.Rows[0]["RequestTypeID"].ToString() != "3")
                    {
                        //btn_Approve.Text = "Approve";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "NonWithdrawShow();", true);
                        //txt_Author.Text = DMS_DT.Rows[0]["CreatorID"].ToString();
                        //txt_Authorizer.Text = DMS_DT.Rows[0]["AuthorizedBy"].ToString();
                        //if (txt_Authorizer.Text == "")
                        //{
                        //    txt_Authorizer.Visible = false;
                        //    Label8.Visible = false;
                        //}
                        //else
                        //{
                        //    txt_Authorizer.Visible = true;
                        //    Label8.Visible = true;
                        //}
                    }
                    if (Convert.ToBoolean(DMS_DT.Rows[0]["isPublic"].ToString()) == true)
                    {
                        RadioButtonList1.SelectedValue = "yes";
                    }
                    else
                    {
                        RadioButtonList1.SelectedValue = "no";
                    }
                    RadioButtonList1.Enabled = false;
                    DataTable aprrove_list = DMS_Bal.DMS_GetApproverReviewerSelectedList(hdfPKID.Value, 6);
                    ViewState["ApproverData"] = aprrove_list;
                    gvApprover.DataSource = aprrove_list;
                    gvApprover.DataBind();
                    UpDocDeails.Update();
                    btn_Approve.Visible = true;
                    upBtnsDocDetails.Update();
                    hdfTraining.Value = "0";
                    reviewObjects.Id = Convert.ToInt32(hdfPKID.Value);
                    reviewObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_docDetails').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
                }
                txt_Reject.Focus();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ID_M7:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ID_M7:" + strline + "  " + "Document Details loading failed.", "error");
            }
            finally
            {
                btnViewDocumentDetails.Enabled = true;
            }
        }
        protected void btnViewDocumentDetails1_Click(object sender, EventArgs e)
        {
            btnViewDocumentDetails1.Enabled = false;
            try
            {
                DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
                DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
                ReviewObjects reviewObjects = new ReviewObjects();
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfPKID.Value);
                if (DMS_DT.Rows.Count > 0)
                {
                    txt_requestType.Text = DMS_DT.Rows[0]["RequestType"].ToString();
                    txt_documentType.Text = DMS_DT.Rows[0]["DocumentType"].ToString();
                    txt_Department.Text = DMS_DT.Rows[0]["Department"].ToString();
                    txt_DocumentNumber.Text = DMS_DT.Rows[0]["DocumentNumber"].ToString();
                    txt_DocumentName.Text = DMS_DT.Rows[0]["DocumentName"].ToString();
                    if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "1")
                    {
                        Session["dropdown"] = "New";
                        lblComments.Text = "Purpose of Initiation";
                        txt_Comments.Text = DMS_DT.Rows[0]["CreatePurpose"].ToString();
                    }
                    else if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "2")
                    {
                        Session["dropdown"] = "Revision";
                        lblComments.Text = "Reason for Change";
                        txt_Comments.Text = DMS_DT.Rows[0]["CreatePurpose"].ToString();
                    }
                    else if (DMS_DT.Rows[0]["RequestTypeID"].ToString() == "3")
                    {
                        //txt_Controller.Text = DMS_DT.Rows[0]["ControlledBy"].ToString();
                        lblComments.Text = "Reason for Withdrawal";
                        txt_Comments.Text = DMS_DT.Rows[0]["WithdrawPurpose"].ToString();
                        Session["dropdown"] = "withdraw";
                        //btn_Approve.Text = "Withdraw";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "WithdrawHide();", true);
                    }
                    txt_VersionID.Text = DMS_DT.Rows[0]["versionNumber"].ToString();
                    if (DMS_DT.Rows[0]["RequestTypeID"].ToString() != "3")
                    {
                        //btn_Approve.Text = "Approve";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "NonWithdrawShow();", true);
                        //txt_Author.Text = DMS_DT.Rows[0]["CreatorID"].ToString();
                        //txt_Authorizer.Text = DMS_DT.Rows[0]["AuthorizedBy"].ToString();
                        //if (txt_Authorizer.Text == "")
                        //{
                        //    txt_Authorizer.Visible = false;
                        //    Label8.Visible = false;
                        //}
                        //else
                        //{
                        //    txt_Authorizer.Visible = true;
                        //    Label8.Visible = true;
                        //}
                    }
                    if (Convert.ToBoolean(DMS_DT.Rows[0]["isPublic"].ToString()) == true)
                    {
                        RadioButtonList1.SelectedValue = "yes";
                    }
                    else
                    {
                        RadioButtonList1.SelectedValue = "no";
                    }
                    RadioButtonList1.Enabled = false;
                    DataTable aprrove_list = DMS_Bal.DMS_GetApproverReviewerSelectedList(hdfPKID.Value, 5);
                    ViewState["ApproverData"] = aprrove_list;
                    gvApprover.DataSource = aprrove_list;
                    gvApprover.DataBind();
                    UpDocDeails.Update();
                    btn_Approve.Visible = false;
                    upBtnsDocDetails.Update();
                    hdfTraining.Value = "1";
                    reviewObjects.Id = Convert.ToInt32(hdfPKID.Value);
                    reviewObjects.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_docDetails').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);

                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ID_M8:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ID_M8:" + strline + "  " + "Document Details1 loading failed.", "error");
            }
            finally
            {
                btnViewDocumentDetails1.Enabled = true;
            }
        }
        protected void btnCommentsHistoryView_Click(object sender, EventArgs e)
        {
            btnCommentsHistoryView.Enabled = false;
            try
            {
                DataTable DMS_DT = DMS_BalDM.DMS_GetComments(hdfPKID.Value);
                gv_CommentHistory.DataSource = DMS_DT;
                ViewState["HistoryDataTable"] = DMS_DT;
                gv_CommentHistory.DataBind();
                if (DMS_DT.Rows.Count > 0)
                {
                    span1.Attributes.Add("title", DMS_DT.Rows[0]["DocumentTitle"].ToString());
                    if (DMS_DT.Rows[0]["DocumentTitle"].ToString().Length > 60)
                    {
                        span1.InnerHtml = DMS_DT.Rows[0]["DocumentTitle"].ToString().Substring(0, 60) + "...";
                    }
                    else
                    {
                        span1.InnerHtml = DMS_DT.Rows[0]["DocumentTitle"].ToString();
                    }
                }
                else
                {
                    span1.InnerText = "Comment History";
                }
                upcomment.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myCommentHistory').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ID_M9:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ID_M9:" + strline + "  " + "History viewing failed.", "error");
            }
            finally
            {
                btnCommentsHistoryView.Enabled = true;
            }
        }
        private void InitializeThePage()
        {
            try
            {
                if (Request.QueryString["rtp"] != null)
                {
                    if (Request.QueryString["rtp"] == "1")
                    {
                        hdnRequestType.Value = "1";
                    }
                    else if (Request.QueryString["rtp"] == "2")
                    {
                        hdnRequestType.Value = "2";
                    }
                    else if (Request.QueryString["rtp"] == "3")
                    {
                        hdnRequestType.Value = "3";
                    }
                    else if (Request.QueryString["rtp"] == "5")
                    {
                        hdnRequestType.Value = "5";
                    }
                    else
                    {
                        hdnRequestType.Value = "0";
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ID_M10:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ID_M10:" + strline + "  " + "cards view details loading failed.", "error");
            }
        }
        protected void gv_CommentHistory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string _fullcomments = (e.Row.Cells[5].FindControl("txt_Comments") as TextBox).Text;
                    LinkButton _lnkmorecomments = (e.Row.Cells[5].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (e.Row.Cells[5].FindControl("lbcommentsless") as LinkButton);
                    if (_fullcomments.Length > 70)
                    {
                        (e.Row.Cells[5].FindControl("lbl_shortComments") as Label).Visible = true;
                        TextBox FullTbx = (e.Row.Cells[5].FindControl("txt_Comments") as TextBox);
                        (e.Row.Cells[5].FindControl("lbl_shortComments") as Label).Text = _fullcomments.Substring(0, 70) + "..";
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = true;

                        if (_fullcomments.Length > 250 && _fullcomments.Length <= 500)
                        {
                            FullTbx.Rows = 6;
                        }
                        else if (_fullcomments.Length > 500 && _fullcomments.Length <= 1000)
                        {
                            FullTbx.Rows = 9;
                        }
                        else if (_fullcomments.Length > 1000 && _fullcomments.Length <= 1500)
                        {
                            FullTbx.Rows = 12;
                        }
                        else if (_fullcomments.Length > 1500 && _fullcomments.Length <= 2000)
                        {
                            FullTbx.Rows = 15;
                        }
                        else if (_fullcomments.Length > 2000)
                        {
                            FullTbx.Rows = 20;
                        }
                        FullTbx.Visible = false;
                    }
                    else
                    {
                        (e.Row.Cells[5].FindControl("lbl_shortComments") as Label).Visible = true;
                        (e.Row.Cells[5].FindControl("txt_Comments") as TextBox).Visible = false;
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ID_M11:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ID_M11:" + strline + "  " + "Comment History row databound failed.", "error");
            }
        }
        protected void gv_CommentHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                
                if (e.CommandName == "SM")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    LinkButton _lnkmorecomments = (row.Cells[5].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (row.Cells[5].FindControl("lbcommentsless") as LinkButton);
                    (row.Cells[5].FindControl("lbl_shortComments") as Label).Visible = false;
                    (row.Cells[5].FindControl("txt_Comments") as TextBox).Visible = true;
                    _lnklesscomments.Visible = true;
                    _lnkmorecomments.Visible = false;
                }
                else if (e.CommandName == "SL")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    LinkButton _lnkmorecomments = (row.Cells[5].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (row.Cells[5].FindControl("lbcommentsless") as LinkButton);
                    (row.Cells[5].FindControl("lbl_shortComments") as Label).Visible = true;
                    (row.Cells[5].FindControl("txt_Comments") as TextBox).Visible = false;
                    _lnklesscomments.Visible = false;
                    _lnkmorecomments.Visible = true;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ID_M12:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ID_M12:" + strline + "  " + "Comment History row command failed.", "error");
            }
        }
        protected void gv_CommentHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_CommentHistory.PageIndex = e.NewPageIndex;
                gv_CommentHistory.DataSource = (DataTable)(ViewState["HistoryDataTable"]);
                gv_CommentHistory.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("ID_M13:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "ID_M13:" + strline + "  " + "grid view page index changing failed.", "error");
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }       
    }
}