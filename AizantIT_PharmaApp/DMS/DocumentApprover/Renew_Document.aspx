﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="Renew_Document.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentApprover.Renew_Document" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        iframe {
            overflow-y: hidden;
        }
         .rowred {
        background-color: #f7d2d8 !important;
        width: 20px;
        height: 20px;
        float:left;
        border: 2px solid #e67e7e;
        border-radius: 4px;
        margin-top: 3px;
    }

    .tablerowred {
        background-color: #f7d2d8 !important;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="padding-none">
        <asp:Button ID="btnAdd" runat="server" class="float-right  btn-signup_popup" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
    </div>
    <div class=" col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12  padding-none float-left">
        <div class=" col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12  dms_outer_border float-left padding-none">
            <div class="col-sm-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server" visible="false">
                <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
            </div>
            <div id="divMainContainer" runat="server" visible="true">
                <div class=" col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12  padding-none float-right ">
                    <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12   padding-none float-left">
                        <div class="grid_header col-lg-12 float-left">Renew Documents</div>
                        <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 grid_panel_full border_top_none bottom float-left">
                            <div class="col-12 float-left top padding-none">
                            <table id="dtDates" class="display datatable_cust" cellspacing="0" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Version ID</th>
                                        <th>Document Number</th>
                                        <th>Document Name</th>
                                        <th>Department</th>
                                        <th>Review Date</th>
                                        <th>Request Type</th>
                                        <th>Renew</th>
                                    </tr>
                                </thead>
                            </table>
                                </div>
                            <div class=" col-lg-12  float-left bottom">
                               <div class="legend_images float-left"> <img src="<%=ResolveUrl("~/Images/GridActionImages/view_tms.png")%>" /><b class="grid_icon_legend">Renew Document</b></div>
                                <div class="legend_images float-left"><img src="<%=ResolveUrl("~/Images/GridActionImages/revert_tms.png")%>" /><b class="grid_icon_legend">Document Distribution Requests Pending</b></div>
                               <div class="legend_images float-left"><span class="reminder rowred"></span><b class="grid_icon_legend">Overdue Documents</b></div>
                               
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--------Review Date EDIT-------------->
    <div id="myModal_Renew" class="modal department fade" role="dialog">
        <div class="modal-dialog " style="min-width: 45%; height: 500px">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <span id="span1" runat="server">Renew Document</span>
                    <button type="button" class="close" data-dismiss="modal" style="width: 50px;">&times;</button>

                </div>
                <div class="modal-body">
                    <div id="divPDF_Viewer">
                        <noscript>
                            Please Enable JavaScript.
                        </noscript>
                    </div>
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                    <asp:UpdatePanel ID="UP_RNWpanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                      
                    <div class="col-lg-12 padding-none float-left">
                        <div class="col-lg-12 padding-none label-style">Review Date :</div>
                        <div class="col-lg-12 padding-none float-left">
                            <asp:TextBox ID="txt_Expiration" runat="server" CssClass="form-control col-6"></asp:TextBox>
                            <label id="noof" runat="server" style="color: red;margin-top: 4px;">
                                You have only
                                <asp:Label ID="lbl_Attempts" runat="server"></asp:Label>
                                attempt('s) left to extend this document</label>
                        </div>
                         <div id="div_Upload" class="col-lg-12 float-left padding-none" style="padding-left: 10px; padding-right: 0px;margin-top: 4px;">
                            
                                    <div class="file upload-button  btn btn-primary  col-lg-4  float-left" id="dvFup" runat="server" style="margin-top: 0px !important;">Upload<asp:FileUpload class="upload-button_input" ID="file_word" runat="server" onchange="UploadClicK()" accept=".pdf" ToolTip="Click to Upload Form" /></div>
                                                     
                        <div class="col-lg-8 float-left" id="divViewDoc">
                            <label>View Doc : </label>
                            <asp:LinkButton ID="lb_Upload" runat="server"  ForeColor="OrangeRed" Font-Bold="true" OnClientClick="javascript:ViewTempExistingDoc();" ></asp:LinkButton>
                        </div>
                    </div>
                </div>
                                    </ContentTemplate>
                            </asp:UpdatePanel>
                <div class="modal-footer col-12 text-right padding-none" style="border: none">
                    <asp:Button ID="btn_RenewSubmit" runat="server" Text="Update" OnClick="btn_RenewSubmit_Click" CssClass="  btn-signup_popup" OnClientClick="javascript:return RequiredReviewDate();" />
                        <button type="button" data-dismiss="modal" runat="server" id="Button1" class="  btn-cancel_popup">Cancel</button>

                    </div>
            </div>
        </div>
    </div>
    <!--------End Review Date EDIT-------------->

    <asp:HiddenField ID="hdfPKID" runat="server" />
    <asp:HiddenField ID="hdfStatus" runat="server" />
    <asp:HiddenField ID="hdfDocNum" runat="server" />
    <asp:HiddenField ID="hdfMaxDateForRenew" runat="server" />
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnRequestType" runat="server" Value="0" />
    <asp:HiddenField ID="hdnRequestTypeID" runat="server" Value="0" />    
    <asp:HiddenField ID="hdfViewType" runat="server" />
    <asp:HiddenField ID="hdfViewEmpID" runat="server" />
    <asp:HiddenField ID="hdfViewRoleID" runat="server" />
    <asp:HiddenField ID="hdfViewDivID" runat="server" />
    <asp:HiddenField ID="hdfFileName" runat="server" />
        <asp:UpdatePanel ID="upUpload" runat="server">
            <ContentTemplate>
                <asp:Button ID="btnUpload" runat="server" Text="Button" OnClick="btnUpload_Click" Style="display: none" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnUpload" />
            </Triggers>
        </asp:UpdatePanel>
     
    <asp:Button ID="btnRenewDateEdit" runat="server" Text="Submit" Style="display: none" OnClick="btnRenewDateEdit_Click" OnClientClick="showImg();"/>
    <script>
        $('#dtDates thead tr').clone(true).appendTo('#dtDates thead');
        $('#dtDates thead tr:eq(1) th').each(function (i) {
            if (i < 7) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder=" Search " />');

                $('input', this).on('keyup change', function () {
                    if (table.column(i).search() !== this.value) {
                        table
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
                if (i == 1) {
                    $(this).html('');
                }
            }
            else {
                $(this).html('');
            }
        });
        $('#dtDates').wrap('<div class="dataTables_scroll" />');
        var table = $('#dtDates').DataTable({
            columns: [
                { 'data': 'RowNumber' },
                { 'data': 'VersionID' },
                { 'data': 'DocumentNumber' },
                { 'data': 'DocumentName' },
                { 'data': 'Department' },
                { 'data': 'ExpirationDate' },
                { 'data': 'RequestType' },
                { 'data': 'PendingDocRequestCount' }               
            ],
            'createdRow': function (row, data, index) {
                if (new Date(data.ExpirationDate) < new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(), 0, 0, 0)) {
                    $(row).closest('tr').addClass('tablerowred');
                }
            },
            "aoColumnDefs": [{ "targets": [0,1, 6], "visible": false, "searchable": false }, { "bSortable": false, "aTargets": [0, 1, 6, 7] }, { "className": "dt-body-left", "targets": [2, 3, 4] },
                {
                    targets: [7], render: function (a, b, data, d) {
                        if (data.PendingDocRequestCount != 0) {
                            return '<a  class="view_Training" title="Renew" href=/DMS/DocumentApprover/Edit_Renew.aspx?DocumentID=' + data.VersionID + '&rtp=' + data.RequestType + '&docPCount=1 ></a>';
                        }
                        else {
                            return '<a  class="view" title="Renew" href=/DMS/DocumentApprover/Edit_Renew.aspx?DocumentID=' + data.VersionID + '&rtp=' + data.RequestType + '&docPCount=0 ></a>';
                        }
                        return "N/A";
                    }
                },
            ],
            "orderCellsTop": true,
            "order": [[1, "desc"]],
            'bAutoWidth': true,
            'sServerMethod': 'Post',
            "sAjaxSource": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetRenewDocList" )%>',
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> },
                    { "name": "ReqTypeID", "value": <%=hdnRequestType.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#dtDates").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        UserSessionCheck();
                    },
                    error: function (xhr, textStatus, error) {
                        errors = [];
                        errors.push("No Records Found");
                        if (errors.length > 0) {
                            custAlertMsg(errors.join("<br/>"), "error");
                        }
                    }
                });
            }
        });
    </script>
    <script>
        function RenewDocuments(PK_ID, RTYPID) {
            document.getElementById("<%=hdfPKID.ClientID%>").value = PK_ID;            
            var btnRD = document.getElementById("<%=btnRenewDateEdit.ClientID %>");
            document.getElementById("<%=hdnRequestTypeID.ClientID%>").value = 0;
            document.getElementById("<%=hdfFileName.ClientID%>").value = "";
            document.getElementById("<%=lb_Upload.ClientID%>").value = "";
           // document.getElementById("<%=lb_Upload.ClientID%>").style.display = "none";
            if (RTYPID == '4') {
                // show Upload
                document.getElementById("<%=hdnRequestTypeID.ClientID%>").value = RTYPID;
                $('#div_Upload').show();
            }
            else {
                $('#div_Upload').hide();
            }
            btnRD.click();
        }
        function UploadClicK() {
            var btnUpload1 = document.getElementById("<%=btnUpload.ClientID%>");
             btnUpload1.click();
         }
    </script>
    <script>
          <!--for datePicker-- >     
        $('#<%=txt_Expiration.ClientID%>').datetimepicker({
            format: 'DD MMM YYYY',
            minDate: new Date('<%=HiddenField1.Value%>'),
            maxDate: new Date('<%=hdfMaxDateForRenew.Value%>'),
            useCurrent: true,
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('#<%=txt_Expiration.ClientID%>').datetimepicker({
                format: 'DD MMM YYYY',
                minDate: new Date('<%=HiddenField1.Value%>'),
                maxDate: new Date('<%=hdfMaxDateForRenew.Value%>'),
                useCurrent: true,
            });
        });
    </script>
    <script src='<%= Page.ResolveUrl("~/AppScripts/PDF_Viewer.min.js")%>'></script>
    <script type="text/javascript">
        function RequiredReviewDate() {
            var ReviewDate = document.getElementById("<%=txt_Expiration.ClientID%>").value;
            var RtypValue = document.getElementById("<%=hdnRequestTypeID.ClientID%>").value;
            var isUploadValue = document.getElementById("<%=hdfFileName.ClientID%>").value;
            
            errors = [];
            if (ReviewDate == "") {
                errors.push("Select Review Date.");
            }
            if (RtypValue == "4") {
                // check for file upload
                if (isUploadValue == "") {
                    errors.push("Please upload Renewed Document.");
                }
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                return true;
            }
        }
        function CloseBrowser() {
            window.close();
        }
        function ViewDocument() {
            PdfViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>', "0", $('#<%=hdfPKID.ClientID%>').val(), "0", "0", "#divPDF_Viewer", '');
        }
        function ViewTempExistingDoc() {
            PdfFormViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_Of_File")%>', $('#<%=hdfFileName.ClientID%>').val(), "#divPDF_Viewer");
        }
    </script>
        <script>
            function showImg() {
                ShowPleaseWait('show');
            }
            function hideImg() {
                ShowPleaseWait('hide');
            }
    </script>
</asp:Content>
