﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Web;
using AizantIT_PharmaApp.Common;

namespace AizantIT_PharmaApp.DMS
{
    public partial class DMS_HomePage : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_bal = new DocumentCreationBAL();
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        ReviewObjects reviewObjects = new ReviewObjects();
        DocObjects docObjects = new DocObjects();
        string AizantIT_DMS_Con = ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            WebChartControl3.BorderOptions.Visibility = DevExpress.Utils.DefaultBoolean.False;
            //WebChartControl2.BorderOptions.Visibility = DevExpress.Utils.DefaultBoolean.False;
            WebChartControl1.BorderOptions.Visibility = DevExpress.Utils.DefaultBoolean.False;
            //WebChartControl6.BorderOptions.Visibility = DevExpress.Utils.DefaultBoolean.False;
            //WebChartControl11.BorderOptions.Visibility = DevExpress.Utils.DefaultBoolean.False;
            //WebChartControl7.BorderOptions.Visibility = DevExpress.Utils.DefaultBoolean.False;
           //WebChartControl12.BorderOptions.Visibility = DevExpress.Utils.DefaultBoolean.False;
            //WebChartControl8.BorderOptions.Visibility = DevExpress.Utils.DefaultBoolean.False;
            //WebChartControl13.BorderOptions.Visibility = DevExpress.Utils.DefaultBoolean.False;
            //WebChartControl5.BorderOptions.Visibility = DevExpress.Utils.DefaultBoolean.False;
            //WebChartControl10.BorderOptions.Visibility = DevExpress.Utils.DefaultBoolean.False;
            if (!IsPostBack)
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    DataSet ds = (DataSet)Session["UserSignIn"];
                    string EMPID = ds.Tables[0].Rows[0]["EmpID"].ToString();
                    ViewState["emp_id_DMS"] = EMPID;
                    Session["EmpID"] = EMPID;
                    DataSet ds_emp = DMS_bal.DMS_GetEmpDeptList(Convert.ToInt32(EMPID));
                    for (int i = 0; i < ds_emp.Tables[0].Rows.Count; i++)
                    {
                        string RoleID = ds_emp.Tables[0].Rows[i]["RoleID"].ToString();
                        if (RoleID == "2")
                        {
                            initiator.Visible = true;
                        }
                        if (RoleID == "9")
                        {
                            initiator.Visible = true;
                            RenewDocument();
                            EffeectiveDoucment();
                            ApproverDocu();
                            QAApprover();
                        }
                        if (RoleID == "10")
                        {
                            initiator.Visible = true;
                            Reviewer();
                        }
                        if (RoleID == "11")
                        {
                            initiator.Visible = true;
                            CreatorApprover();
                            CreatorRevert();
                        }
                        if (RoleID == "12")
                        {
                            initiator.Visible = true;
                            InitiateReverted();
                        }
                        if (RoleID == "13")
                        {
                            initiator.Visible = true;
                            AuthorizeDoc();
                        }
                        if (RoleID == "23")
                        {
                            initiator.Visible = true;
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx");
                }
                DMS_GetDepartmentList();
                DMS_GetDocumentTypeList();
                ChartFill();
            }
        }
        public void RenewDocument()
        {
            try
            {
                JQDataTableBO objJQDataTableBO = new JQDataTableBO();
                objJQDataTableBO.iMode = 0;
                objJQDataTableBO.pkid = 0;
                objJQDataTableBO.iDisplayLength = 0;
                objJQDataTableBO.iDisplayStart = 0;
                objJQDataTableBO.iSortCol = 0;
                objJQDataTableBO.sSortDir = "";
                objJQDataTableBO.sSearch = "";
                objJQDataTableBO.EmpID = Convert.ToInt32(ViewState["emp_id_DMS"]);
                DataTable dt = DMS_bal.DMS_ExtendDocument(objJQDataTableBO);
                if (dt.Rows.Count > 0)
                {
                    lblRenewDocument.Text = (dt.Rows.Count.ToString());
                    divRenewDocument.Visible = true;
                }
                else
                {
                    divRenewDocument.Visible = false;
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/> Renew Notification failed','error');", true);
            }
        }
        public void DMS_GetDepartmentList()
        {
            try
            {
                DataTable DMS_Dt = DMS_BalDM.DMS_GetDepartmentList();
                ddl_DeptName.DataValueField = DMS_Dt.Columns["DeptID"].ToString();
                ddl_DeptName.DataTextField = DMS_Dt.Columns["DepartmentName"].ToString();
                ddl_DeptName.DataSource = DMS_Dt;
                ddl_DeptName.DataBind();
                ddl_DeptName.Items.Insert(0, new ListItem("All Departments", "0"));

                //ddl_Department.DataValueField = DMS_Dt.Columns["DeptID"].ToString();
                //ddl_Department.DataTextField = DMS_Dt.Columns["DepartmentName"].ToString();
                //ddl_Department.DataSource = DMS_Dt;
                //ddl_Department.DataBind();
                //ddl_Department.Items.Insert(0, new ListItem("--Select Departments--", "0"));

                //DropDownList2.DataValueField = DMS_Dt.Columns["DeptID"].ToString();
                //DropDownList2.DataTextField = DMS_Dt.Columns["DepartmentName"].ToString();
                //DropDownList2.DataSource = DMS_Dt;
                //DropDownList2.DataBind();
                //DropDownList2.Items.Insert(0, new ListItem("--Select QA Departments--", "0"));

                //DropDownList3.DataValueField = DMS_Dt.Columns["DeptID"].ToString();
                //DropDownList3.DataTextField = DMS_Dt.Columns["DepartmentName"].ToString();
                //DropDownList3.DataSource = DMS_Dt;
                //DropDownList3.DataBind();
                //DropDownList3.Items.Insert(0, new ListItem("--Select Reviewer Departments--", "0"));

                //DropDownList4.DataValueField = DMS_Dt.Columns["DeptID"].ToString();
                //DropDownList4.DataTextField = DMS_Dt.Columns["DepartmentName"].ToString();
                //DropDownList4.DataSource = DMS_Dt;
                //DropDownList4.DataBind();
                //DropDownList4.Items.Insert(0, new ListItem("--Select Approver Departments--", "0"));

                //DropDownList5.DataValueField = DMS_Dt.Columns["DeptID"].ToString();
                //DropDownList5.DataTextField = DMS_Dt.Columns["DepartmentName"].ToString();
                //DropDownList5.DataSource = DMS_Dt;
                //DropDownList5.DataBind();
                //DropDownList5.Items.Insert(0, new ListItem("--Select Authorizer Departments--", "0"));
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/> Department Dropdown loading failed','error');", true);
            }
        }
        public void DMS_GetDocumentTypeList()
        {
            try
            {
                JQDataTableBO objJQDataTableBO = new JQDataTableBO();
                objJQDataTableBO.iMode = 0;
                objJQDataTableBO.pkid = 0;
                objJQDataTableBO.iDisplayLength = 0;
                objJQDataTableBO.iDisplayStart = 0;
                objJQDataTableBO.iSortCol = 0;
                objJQDataTableBO.sSortDir = "";
                objJQDataTableBO.sSearch = "";
                DataTable DMS_Dt = DMS_bal.DMS_GetDocumentTypeList(objJQDataTableBO);
                ddl_DocType.DataValueField = DMS_Dt.Columns["DocumentTypeID"].ToString();
                ddl_DocType.DataTextField = DMS_Dt.Columns["DocumentType"].ToString();
                ddl_DocType.DataSource = DMS_Dt;
                ddl_DocType.DataBind();
                ddl_DocType.Items.Insert(0, new ListItem("All Document Type", "0"));

                //ddl_DocumentType.DataValueField = DMS_Dt.Columns["DocumentTypeID"].ToString();
                //ddl_DocumentType.DataTextField = DMS_Dt.Columns["DocumentType"].ToString();
                //ddl_DocumentType.DataSource = DMS_Dt;
                //ddl_DocumentType.DataBind();
                //ddl_DocumentType.Items.Insert(0, new ListItem("All Document Type", "0"));
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/> DocumentType Dropdown loading failed','error');", true);
            }
        }
        protected void ddl_DeptName_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChartFill();
        }
        public void InitiateReverted()
        {
            try
            {
                int EmpID = Convert.ToInt32(Session["EmpID"]);
                string Status = "I";
                int count = DMS_bal.RevertedInitator(EmpID, Status);
                if (count >= 1)
                {
                    lblRevertedinitiation.Text = count.ToString();
                    divRevertedInitiation.Visible = true;
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/> Reverted Notification failed','error');", true);
            }
        }
        public void QAApprover()
        {
            try
            {
                int EmpID = Convert.ToInt32(Session["EmpID"]);
                string Status = "QA";
                int count = DMS_bal.RevertedInitator(EmpID, Status);
                if (count >= 1)
                {
                    lblApproverQA.Text = count.ToString();
                    divApproverQA.Visible = true;
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    Approver Notification  failed','error');", true);
            }
        }
        public void CreatorApprover()
        {
            try
            {
                int EmpID = Convert.ToInt32(Session["EmpID"]);
                string Status = "C";
                int count = DMS_bal.RevertedInitator(EmpID, Status);
                if (count >= 1)
                {
                    lblApproverCreator.Text = count.ToString();
                    divApproverCreator.Visible = true;
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>  Approver Notification failed','error');", true);
            }
        }
        public void CreatorRevert()
        {
            try
            {
                int EmpID = Convert.ToInt32(Session["EmpID"]);
                string Status = "C_R";
                int count = DMS_bal.RevertedInitator(EmpID, Status);
                if (count >= 1)
                {
                    lblCreatorRevert.Text = count.ToString();
                    divCreatorRevert.Visible = true;
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>  Reverted Notification failed','error');", true);
            }
        }
        public void Reviewer()
        {
            try
            {
                int EmpID = Convert.ToInt32(Session["EmpID"]);
                string Status = "R_Ap";
                int count = DMS_bal.RevertedInitator(EmpID, Status);
                if (count >= 1)
                {
                    lblReviewer.Text = count.ToString();
                    divReviewer.Visible = true;
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>  Reviwer Notification failed','error');", true);
            }
        }
        public void ApproverDocu()
        {
            try
            {
                int EmpID = Convert.ToInt32(Session["EmpID"]);
                string Status = "Ap_D";
                int count = DMS_bal.RevertedInitator(EmpID, Status);
                if (count >= 1)
                {
                    lblApproverDoc.Text = count.ToString();
                    divApproverDoc.Visible = true;
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/> Approver Notification failed','error');", true);
            }
        }
        public void AuthorizeDoc()
        {
            try
            {
                int EmpID = Convert.ToInt32(Session["EmpID"]);
                string Status = "Au";
                int count = DMS_bal.RevertedInitator(EmpID, Status);
                if (count >= 1)
                {
                    lblAuthorize.Text = count.ToString();
                    divAuthorize.Visible = true;
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>  Approver Notification failed','error');", true);
            }
        }
        public void EffeectiveDoucment()
        {
            try
            {
                DocumentCreationBAL DMS_BalDC = new DocumentCreationBAL();
                JQDataTableBO objJQDataTableBO = new JQDataTableBO();
                objJQDataTableBO.iMode = 2;
                objJQDataTableBO.pkid = 0;
                objJQDataTableBO.iDisplayLength = 0;
                objJQDataTableBO.iDisplayStart = 0;
                objJQDataTableBO.iSortCol = 0;
                objJQDataTableBO.sSortDir = "0";
                objJQDataTableBO.sSearch = "0";
                objJQDataTableBO.EmpID = 0;
                DataTable dt = DMS_BalDC.DMS_Listofeffectivedocuments(objJQDataTableBO);
                int count;
                if (dt.Rows.Count > 0)
                {
                    count = Convert.ToInt32(dt.Rows[0][0].ToString());
                    if (count >= 1)
                    {
                        lblEffective.Text = count.ToString();
                        dvEfective.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>  Effective Document Notification failed','error');", true);
            }
        }
        public void ChartFill()
        {
            try
            {
                DataTable dt = DMS_bal.DMS_HomeDocumentListChart(Convert.ToInt32(ddl_DeptName.SelectedValue), Convert.ToInt32(ddl_DocType.SelectedValue), 0);
                this.WebChartControl3.DataSource = dt;
                this.WebChartControl3.Series[0].ArgumentDataMember = "DocumentType";
                this.WebChartControl3.Series[0].ValueDataMembers.AddRange(new string[] { "NoofDocuments" });
                this.WebChartControl3.Series[0].ChangeView(ViewType.Pie);
                this.WebChartControl3.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>  DocumentType chart filling failed','error');", true);
            }
        }
        public void ChartFill1()
        {
            try
            {
                DataTable dt = DMS_bal.DMS_HomeDocumentListChart(Convert.ToInt32(ddl_DeptName.SelectedValue), Convert.ToInt32(ddl_DocType.SelectedValue), 1);
                this.WebChartControl3.DataSource = dt;
                this.WebChartControl3.Series[0].ArgumentDataMember = "DeptCode";
                this.WebChartControl3.Series[0].ValueDataMembers.AddRange(new string[] { "NoofDocuments" });
                this.WebChartControl3.Series[0].ChangeView(ViewType.Bar);
                this.WebChartControl3.Series[0].LegendTextPattern = "{A}:{V}";
                this.WebChartControl3.CrosshairEnabled = DevExpress.Utils.DefaultBoolean.False;
                this.WebChartControl3.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>  Department chart filling failed','error');", true);
            }
        }
        public void ChartFill2()
        {
            try
            {
                DataTable dt = DMS_bal.DMS_RecentlyRevisedDocumentsChart(docObjects);
                docObjects.FromDate = Convert.ToDateTime(txt_FromDate.Text);
                docObjects.ToDate = Convert.ToDateTime(txt_ToDate.Text);
                this.WebChartControl1.DataSource = dt;
                this.WebChartControl1.Series[0].ArgumentDataMember = "DeptCode";
                this.WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "No.ofRecentlyRevisedDocuments" });
                this.WebChartControl1.Series[0].ChangeView(ViewType.Pie);
                this.WebChartControl1.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>  DocumentType chart filling failed','error');", true);
            }
        }
        //public void ChartFill2()
        //{
        //    DataTable dt = DMS_bal.DMS_HomeDocumentListChart(Convert.ToInt32(ddl_DeptName.SelectedValue), Convert.ToInt32(ddl_DocType.SelectedValue), 2);
        //    this.WebChartControl3.DataSource = dt;
        //    this.WebChartControl3.Series[0].ArgumentDataMember = "DocumentType";
        //    this.WebChartControl3.Series[0].ValueDataMembers.AddRange(new string[] { "NoofDocuments" });
        //    this.WebChartControl3.Series[0].ChangeView(ViewType.Pie);
        //    this.WebChartControl3.FillStyle.FillMode = FillMode.Solid;
        //    this.WebChartControl3.DataBind();
        //}
        protected void ddl_DocType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl_DeptName.SelectedValue == "0" && ddl_DocType.SelectedValue == "0")
            {
                ChartFill();
            }
            else if (ddl_DeptName.SelectedValue != "0" && ddl_DocType.SelectedValue == "0")
            {
                ChartFill();
            }
            else
            {
                ChartFill1();
            }
        }

        protected void btn_RecentlyRevised_Click(object sender, EventArgs e)
        {
            ChartFill2();
        }
        //public void sampleChart()
        //{
        //    WebChartControl WebChartControl = new WebChartControl();
        //    // Add the chart to the form.
        //    // Note that a chart isn't initialized until it's added to the form's collection of controls.
        //    this.Form.Controls.Add(WebChartControl);
        //    // Create a new bar series.
        //    Series series = new Series("2017", ViewType.Pie);
        //    // Add the series to the chart.
        //    WebChartControl.Series.Add(series);
        //    // Specify the series data source.
        //    DataTable seriesData = GetData();
        //    series.DataSource = seriesData;
        //    // Specify an argument data member.
        //    series.ArgumentDataMember = "Region";
        //    // Specify a value data member.
        //    series.ValueDataMembers.AddRange(new string[] { "Sales" });
        //    // Rotate the diagram (if necessary).
        //    ((XYDiagram)WebChartControl.Diagram).Rotated = true;
        //}
        //public DataTable GetData()
        //{
        //    DataTable table = new DataTable();
        //    table.Columns.AddRange(new DataColumn[] {
        //    new DataColumn("Region", typeof(string)),
        //    new DataColumn("Sales", typeof(decimal))
        //});
        //    table.Rows.Add("Asia", 5.289M);
        //    table.Rows.Add("North America", 4.182M);
        //    table.Rows.Add("Europe", 3.725M);
        //    table.Rows.Add("Australia", 2.272M);
        //    table.Rows.Add("South America", 2.117M);
        //    return table;
        //}
    }
}