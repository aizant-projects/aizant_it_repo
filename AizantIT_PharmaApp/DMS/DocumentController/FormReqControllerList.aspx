﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="FormReqControllerList.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentController.FormReqControllerList" %>

<%@ Register Src="~/UserControls/ElectronicSign.ascx" TagPrefix="uc1" TagName="ElectronicSign" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="padding-none">
        <asp:Button ID="btnAdd" runat="server" class="float-right  btn-signup_popup" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
    </div>
    <div class=" col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left">
        <div class=" col-md-12 col-lg-12 col-12 col-sm-12 dms_outer_border float-left padding-none">
            <div class="col-md-12 col-lg-12 col-12 col-sm-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server" visible="false">
                <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left" id="divMainContainer" runat="server" visible="true">
                <div class=" col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-right ">
                    <div class="col-md-12 col-lg-12 col-12 col-sm-12  padding-none float-left">
                        <div class="grid_header col-md-12 col-lg-12 col-12 col-sm-12 float-left">Form Print Request Controller List</div>
                        <div class="col-md-12 col-lg-12 col-12 col-sm-12 grid_panel_full border_top_none bottom float-left">
                            <div class="col-12 float-left top padding-none bottom">
                            <table id="dtDocList" class="display datatable_cust" cellspacing="0" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>FormRequestID</th>
                                        <th>Request Number</th>
                                        <th>Form Name</th>
                                        <th>Form Number</th>
                                        <th>Form Version</th>
                                        <th>Purpose</th>
                                        <th>No.of Copies</th>
                                        <th>Copy Numbers</th>
                                        <th>Approved By</th>
                                        <th>Request By</th>
                                        <th>Requested Date</th>
                                        <th>Approve Request</th>
                                        <th>History</th>
                                        <th>FormVersionID</th>
                                    </tr>
                                </thead>
                            </table>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-------- Request VIEW-------------->
    <div id="myModal_request" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 70%">
            <!-- Modal content-->
            <div class="modal-content col-lg-12 padding-none">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            
                            <span id="span_FileTitle" runat="server"></span>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">

                            <div id="div_RequestedBy" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group padding-right_div float-left">
                                <asp:Label ID="lblRequestedBy" runat="server" Text="Requested By" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_RequestedBy" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Requested By" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_NoofCopies" class="col-md-6 col-lg-2 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="lblNoofCopies" runat="server" Text="No.of Copies" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_NoofCopies" runat="server" CssClass="form-control login_input_sign_up" TextMode="Number" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_Project" class="col-md-6 col-lg-4 col-12 col-sm-12 form-group padding-left_div float-left">
                                <asp:Label ID="Label2" runat="server" Text="Project Number" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_ProjectNumber" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Project Number" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_formNo" class="col-md-6 col-lg-6 col-12 col-sm-12 padding-right_div form-group float-left">
                                    <asp:Label ID="lblFormNum" runat="server" Text="Form Number" CssClass="label-style"></asp:Label>
                                    <asp:TextBox ID="txt_FormNumber" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true" ></asp:TextBox>
                                </div>
                                <div id="div_versionno" class="col-md-6 col-lg-3 col-12 col-sm-12 form-group float-left ">
                                    <asp:Label ID="lblVersionNum" runat="server" Text="Version Number" CssClass="label-style"></asp:Label>
                                    <asp:TextBox ID="txt_VersionNumber" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true" ></asp:TextBox>
                                </div>
                            <div id="div_DocName" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group padding-right_div float-left">
                                <asp:Label ID="Label4" runat="server" Text="Form Name" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_DocumentName" runat="server" TextMode="MultiLine" CssClass="form-control " placeholder="Enter Form Name" AutoPostBack="true" Rows="2" MaxLength="500" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_Purpose" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group padding-left_div float-left">
                                <asp:Label ID="lblComments" runat="server" Text="Purpose" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_Purpose" runat="server" TextMode="MultiLine" CssClass="form-control " placeholder="Enter Purpose" Width="100%" Rows="2" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_ReviewedBy" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group padding-right_div float-left">
                                <asp:Label ID="Label6" runat="server" Text="Reviewed By" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_ReviewedBy" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_DocController" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group padding-left_div float-left">
                                <asp:Label ID="Label7" runat="server" Text="Document Controller" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_DocController" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_Comments" class="col-md-12 col-lg-12 col-12 col-sm-12 form-group padding-none float-left" runat="server">
                                <asp:Label ID="Label1" runat="server" Text="Comments" CssClass="label-style"></asp:Label><span class="smallred_label">*</span>
                                <textarea ID="txt_Comments" runat="server" class="form-control " placeholder="Enter Comments" TabIndex="1" maxlength="300"></textarea>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-12 modal-footer  float-left" id="buttons" runat="server">
                            <div class="col-12 float-left text-right padding-none">
                            <asp:Button ID="btn_View" Text="View Form" CssClass="float-left  btn-signup_popup" runat="server" TabIndex="2" OnClick="btn_View_Click" OnClientClick="showImg();"/>
                            <input type="button" id="btn_Approve" runat="server" class=" btn-signup_popup" onclick="return commentsRequiredApprove();" value="Approve" tabindex="2" />
                            <input type="button" id="btn_Reject" runat="server" class=" btn-revert_popup" onclick="return commentsRequiredReject();" value="Reject" tabindex="3" />
                            <asp:Button ID="btn_Cancel" Text="Cancel" data-dismiss="modal" CssClass=" btn-cancel_popup" runat="server" CausesValidation="false" TabIndex="4" />
                        </div>
                             </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!-------- End Request VIEW-------------->

    <!--------Comment History View-------------->
    <div id="myCommentHistory" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 85%">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel ID="upcomment" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-header">
                            
                            <span id="span4" runat="server"></span>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body" style="max-height: 600px; overflow-y: auto">
                            <div class=" history_grid" style="overflow-y: auto;">
                                <asp:GridView ID="gv_CommentHistory" runat="server" CssClass="table table-hover" ClientIDMode="Static" EmptyDataText="No Records Found" AutoGenerateColumns="False" PagerSettings-Mode="NumericFirstLast" BackColor="White" BorderColor="#d2d2d2" BorderStyle="None" BorderWidth="1px" OnRowDataBound="gv_CommentHistory_RowDataBound" OnRowCommand="gv_CommentHistory_RowCommand" AllowPaging="true" PageSize="10" OnPageIndexChanging="gv_CommentHistory_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="User" ItemStyle-Width="12%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_GivenBy" runat="server" Text='<%# Eval("ActionBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" ItemStyle-Width="11%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Date" runat="server" Text='<%# Eval("ActionDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role" ItemStyle-Width="13%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Role" runat="server" Text='<%# Eval("CommentedRole") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="20%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Action" runat="server" Text='<%# Eval("Action") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments" ItemStyle-Width="29%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_shortComments" runat="server" Text='<%# Eval("Comments") %>'></asp:Label>
                                                <asp:TextBox ID="txt_Comments" runat="server" ReadOnly="true" Rows="2" TextMode="MultiLine" Text='<%# Eval("Comments") %>' Width="100%"></asp:TextBox>
                                                <asp:LinkButton ID="lbcommentsmore" runat="server" CommandArgument='<%# Eval("Comments")%>' CommandName="SM">Show More</asp:LinkButton>
                                                <asp:LinkButton ID="lbcommentsless" runat="server" CommandArgument='<%# Eval("Comments") %>' CommandName="SL">Show Less</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Assigned To">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_AssignedTo" runat="server" Text='<%# Eval("AssignedTo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle cssClass="gridpager"  HorizontalAlign="Right" />
                                </asp:GridView>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!--------End Comment History View-------------->

    <!-------- Form VIEW-------------->
    <div id="myFormView" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 90%; height: 500px">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpHeading">
                        <ContentTemplate>
                            <span id="span1" runat="server"></span>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="divPDF_Viewer">
                        <noscript>
                            Please Enable JavaScript.
                        </noscript>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--------End Form VIEW-------------->
    <uc1:ElectronicSign runat="server" ID="ElectronicSign" />
    <asp:HiddenField ID="hdfVID" runat="server" />
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
    <asp:HiddenField runat="server" ID="hdnAction" />
    <asp:HiddenField ID="hdfPkID" runat="server" />
    <asp:HiddenField ID="hdfViewType" runat="server" />
    <asp:HiddenField ID="hdfViewEmpID" runat="server" />
    <asp:HiddenField ID="hdfViewRoleID" runat="server" />
    <asp:HiddenField ID="hdfViewDivID" runat="server" />
    <asp:UpdatePanel ID="UpGridButtons" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnRequestControl" runat="server" Text="Submit" OnClick="btnRequestControl_Click" Style="display: none" OnClientClick="showImg();"/>
            <asp:Button ID="btnCommentsHistoryView" runat="server" Text="Submit" Style="display: none" OnClick="btnCommentsHistoryView_Click" OnClientClick="showImg();"/>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>
        $('#dtDocList thead tr').clone(true).appendTo('#dtDocList thead');
        $('#dtDocList thead tr:eq(1) th').each(function (i) {
            if (i < 12) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder=" Search " />');

                $('input', this).on('keyup change', function () {
                    if (oTable.column(i).search() !== this.value) {
                        oTable
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
                if (i == 1 || i==8) {
                    $(this).html('');
                }
            }
            else {
                $(this).html('');
            }
        });
        $('#dtDocList').wrap('<div class="dataTables_scroll" />');
        var oTable = $('#dtDocList').DataTable({
            columns: [
                { 'data': 'RowNumber' },//0
                { 'data': 'FormReqID' },//1
                { 'data': 'RequestNumber' },//2
                { 'data': 'FormName' },//3
                { 'data': 'FormNumber' },//4
                {'data':'FormVersionNumber'},//5
                { 'data': 'RequestPurpose' },//6
                { 'data': 'NoofCopies' },//7
                { 'data': 'CopyNumbers' },//8
                { 'data': 'ApprovedBy' },//9
                { 'data': 'RequestBy' },//10
                { 'data': 'RequestedDate' },//11
                {//12
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="view_req" title="View" data-toggle="modal" data-target="#myModal_request" onclick=ReviewRequest(' + o.FormReqID + ',' + o.FormVersionID + ')></a>'; }
                },
                {//13
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="summary_latest" title="History" data-toggle="modal" data-target="#myCommentHistory" onclick=ViewCommentHistory(' + o.FormReqID + ')></a>'; }
                },
                { 'data': 'FormVersionID' }//14
            ],
            "responsive": true,
            "paging": true,
            "search": {
                "regex": true
            },
            "aoColumnDefs": [{ "targets": [0,1,3,6,14], "visible": false, "searchable": false }, { "bSortable": false, "aTargets": [0] }, { "className": "dt-body-left", "targets": [3,4, 6,9,10] },
            ],
            "orderCellsTop": true,
            "order": [[1, "desc"]],
            'bAutoWidth': true,
            sServerMethod: 'Post',
            "sAjaxSource": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetFormPrintReqControllerList" )%>',
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#dtDocList").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        UserSessionCheck();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
        function reloadtable() {
            oTable.ajax.reload();
            $('#myModal_request').modal('hide');
            $('#usES_Modal').modal('hide');
        }
    </script>
    <script>
        function ReviewRequest(V_ID, PkID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = V_ID;
            document.getElementById("<%=hdfPkID.ClientID%>").value = PkID;
            var btnRC = document.getElementById("<%=btnRequestControl.ClientID %>");
            btnRC.click();
        }
        function ViewCommentHistory(PK_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = PK_ID;
            var btnCV = document.getElementById("<%=btnCommentsHistoryView.ClientID %>");
            btnCV.click();
        }
    </script>
    <script type="text/javascript">
        function commentsRequiredApprove() {
            var comments = document.getElementById("<%=txt_Comments.ClientID%>").value;
            errors = [];
            if (comments.trim() == "") {
                errors.push("Enter Comments.");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                ApproveAction();
                return true;
            }
        }
        function commentsRequiredReject() {
            var comments = document.getElementById("<%=txt_Comments.ClientID%>").value;
            errors = [];
            if (comments.trim() == "") {
                errors.push("Enter Comments.");
            }
            if (errors.length > 0) {
                custAlertMsg(errors.join("<br/>"), "error");
                return false;
            }
            else {
                RejectAction();
                return true;
            }
        }
        function ApproveAction() {
            document.getElementById("<%=hdnAction.ClientID%>").value = "Approve";
            openElectronicSignModal();
        }
        function RejectAction() {
            document.getElementById("<%=hdnAction.ClientID%>").value = "Reject";
            openElectronicSignModal();
        }
    </script>
    <script src='<%= Page.ResolveUrl("~/AppScripts/PDF_Viewer.min.js")%>'></script>
    <script>
        function openElectronicSignModal() {
            openUC_ElectronicSign();
        }
        function CloseBrowser() {
            window.close();
        }
        function ViewForm() {
            $('#myFormView').modal({ show: true, backdrop: 'static', keyboard: false });
            PdfViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>', "3", $('#<%=hdfPkID.ClientID%>').val(), "0", "0", "#divPDF_Viewer");
        }
    </script>
     <script>
         function showImg() {
             ShowPleaseWait('show');
         }
         function hideImg() {
             ShowPleaseWait('hide');
         }
         function HidePopup() {
             $('#usES_Modal').modal('hide');
         }
    </script>
</asp:Content>
