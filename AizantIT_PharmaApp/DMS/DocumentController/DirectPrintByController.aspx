﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="DirectPrintByController.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentController.DirectPrintByController" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="grid_header col-md-12 col-lg-12 col-12 col-sm-12 float-left">Request to Print</div>
                        <div class="col-md-12 col-lg-12 col-12 col-sm-12 grid_panel_full border_top_none bottom float-left">
    <div id="div_RequestedBy" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group padding-right_div float-left top">
                                    <asp:Label ID="lblRequestedBy" runat="server" Text="Requested By" CssClass="label-style"></asp:Label>
                                    <asp:TextBox ID="txt_RequestedBy" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Requested By" ReadOnly="true" MaxLength="100"></asp:TextBox>
                                </div>
                                
                                <div id="divSoftCopy" class="form-group col-sm-12 col-lg-3 col-12 col-md-12 col-sm-12 float-left top" style="padding-left: 50px">
                                    <asp:Label ID="lblRadiobutton" runat="server" Text="Type of Request" CssClass="label-style"></asp:Label><span class="smallred_label">*</span>
                                    <asp:RadioButtonList runat="server" ID="RadioButtonList1" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Download" Value="yes"></asp:ListItem>
                                        <asp:ListItem Text="Print" Value="no"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                                <div id="div_NoofCopies" class="col-md-6 col-lg-3 col-12 col-sm-12 form-group float-left padding-none top">
                                    <asp:Label ID="lblNoofCopies" runat="server" Text="No.of Copies" CssClass="label-style"></asp:Label>
                                    <asp:TextBox ID="txt_NoofCopies" runat="server" CssClass="form-control login_input_sign_up" TextMode="Number" Text="1" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div id="div_PageNo" class="col-md-6 col-lg-4 col-12 col-sm-12 form-group padding-right_div float-left">
                                    <asp:Label ID="lblPageNo" runat="server" Text="Page Numbers" CssClass="label-style"></asp:Label>
                                    <asp:TextBox ID="txt_PageNo" runat="server" CssClass="form-control login_input_sign_up" placeholder="For Example, Type 1,3,5-12" autocomplete="off" onkeypress="return Allowcommadashnumbers(event);" onpaste="return AllowcommadashnumbersOnPaste(event);" MaxLength="30"></asp:TextBox>
                                    <asp:Label ID="lblPageNumbers" runat="server" Style="color: red !important"></asp:Label>
                                </div>
                                <div id="div_docNo" class="col-md-6 col-lg-5 col-12 col-sm-12 form-group float-left">
                                    <asp:Label ID="lblDocNum" runat="server" Text="Document Number" CssClass="label-style"></asp:Label>
                                    <asp:TextBox ID="txt_DocumentNumber" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true" ></asp:TextBox>
                                </div>
                                <div id="div_versionnoNo" class="col-md-6 col-lg-3 col-12 col-sm-12 form-group float-left padding-none">
                                    <asp:Label ID="lblVersionNum" runat="server" Text="Version Number" CssClass="label-style"></asp:Label>
                                    <asp:TextBox ID="txt_VersionNumber" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true" ></asp:TextBox>
                                </div>
                                <div class="col-12 float-left padding-none">
                                <div id="div_DocName" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group padding-right_div float-left">
                                    <asp:Label ID="Label4" runat="server" Text="Document Name" CssClass="label-style"></asp:Label>
                                    <asp:TextBox ID="txt_DocumentName" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="Enter Document Name" AutoPostBack="true" Rows="2" MaxLength="500" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div id="div_Purpose" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group padding-left_div float-left">
                                    <asp:Label ID="lblComments" runat="server" Text="Purpose"  CssClass="label-style"></asp:Label><span class="smallred_label">*</span>
                                    <textarea ID="txt_Purpose" runat="server" class="form-control " placeholder="Enter Purpose" TabIndex="1" MaxLength="300"></textarea>
                                </div>
                                    </div>
                                
                                <div id="div_CopyType" class="col-md-6 col-lg-4 col-12 col-sm-12 form-group padding-none float-left">
                                    <asp:Label ID="Label1" runat="server" Text="Type of Copy" CssClass="label-style"></asp:Label><span class="smallred_label">*</span>
                                    <asp:DropDownList ID="ddl_CopyType" OnChange="ddlCopyOnChange(this);" data-size="9" runat="server" CssClass="col-md-12 col-lg-12 col-12 col-sm-12 selectpicker1 regulatory_dropdown_style drop_down padding-none " data-live-search="true" placeholder="Select Type of Copy">
                                    </asp:DropDownList>
                                </div>
                             <div id="div_ValidDays" class="col-md-6 col-lg-2 col-12 col-sm-12 form-group    float-left" style="display:none">
                                <asp:Label ID="lblValidDays" runat="server" Text="Validity(In Days)" CssClass="label-style"></asp:Label><span class="smallred_label">*</span>
                                <asp:TextBox ID="txt_ValidDays" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Valid Days" TextMode="Number" TabIndex="1" onkeypress="return RestrictMinus(event);" min="0"></asp:TextBox>
                            </div>
                            <div id="div_Comments" class="col-md-12 col-lg-12 col-12 col-sm-12 form-group padding-none  float-left">
                                <asp:Label ID="Label2" runat="server" Text="Comments" CssClass="label-style"></asp:Label><span class="smallred_label">*</span>
                                <textarea ID="txt_Comments" runat="server" class="form-control" placeholder="Enter Comments" TabIndex="2" maxlength="300"></textarea>
                            </div>
            <div class="modal-footer col-12 float-left text-right padding-none bottom top">
           <input type="button" title="Submit" value="Submit" class=" btn-signup_popup" onclick="PrintDocument();" /> 
                 <asp:Button ID="btn_Submit" Text="Submit" Style="display: none" CssClass=" btn-signup_popup" runat="server" ValidationGroup="DMS_DC"  onclick="btn_Submit_Click"/>
                                <asp:Button ID="btn_Reset" Text="Reset" CssClass=" btn-revert_popup" runat="server" CausesValidation="false" onclick="btn_Reset_Click"/>
                                <asp:Button ID="btn_Cancel" Text="Cancel" data-dismiss="modal" CssClass=" btn-cancel_popup" runat="server" CausesValidation="false" OnClick="btn_Cancel_Click" />
            </div>
                            </div>
     <div id="MyModelPrinterNames" class="modal department fade" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 90%; overflow: hidden">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                            <span id="span3" runat="server">Printer Names</span>
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" style="max-height: 680px;margin-bottom: 13px;">
                   <div class="col-xl-12 col-md-12 col-sm-12 col-lg-12 float-left col-12">
                        <table id="DocPrintertable" class="table datatable_cust table-striped table-bordered" style="width:100%;" nodata="no data">
                            <thead>
                                <tr>
                                    <th hidden>PrinterID</th>
                                    <th>S.No</th>
                                    <th>Printer Area Name</th>
                                    <th>Printer Name</th>
                                    <th>Select</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdfVID" runat="server" />
        <asp:HiddenField ID="hdfDeptID" runat="server" />
        <asp:HiddenField ID="hdfPageCount" runat="server" />
 <asp:HiddenField ID="hdnPrinterName" runat="server" />
    <asp:HiddenField ID="hdnWebCnfgDocValue" runat="server" />


        <script>
         $(function () {
                $(".selectpicker1").selectpicker();
            });
            function CloseBrowser() {
                window.close();
            }
             </script>

    <script type="text/javascript">
            var rdnDownPrint = "";
            var radio = "";
            var WebConfigDefaultValue = "";
            function PrintDocument() {
                 WebConfigDefaultValue = document.getElementById("<%=hdnWebCnfgDocValue.ClientID%>").value;
                 var txt_Purpose = document.getElementById("<%=txt_Purpose.ClientID%>").value;
            var txtValidDays = document.getElementById("<%=txt_ValidDays.ClientID%>").value;
            var txtComments = document.getElementById("<%=txt_Comments.ClientID%>").value;
            var ddl_CopyType = document.getElementById("<%=ddl_CopyType.ClientID%>").value;
            var txt_PageNo = document.getElementById("<%=txt_PageNo.ClientID%>").value;
                var PageCount = document.getElementById("<%=hdfPageCount.ClientID%>").value;
                
             rdnDownPrint = document.getElementById("<%=RadioButtonList1.ClientID%>");
                 radio = rdnDownPrint.getElementsByTagName("input");
                errors = [];
               
            if (txt_Purpose.trim() == "") {
                errors.push("Enter Purpose.");
                }
                 if (ddl_CopyType == 0) {
                errors.push("Select Type of Copy.");
                 }
                 if (ddl_CopyType == 1) {
                     if (txtValidDays.trim() == "") {
                         errors.push("Enter Valid Days.");
                     }
                     else if (txtValidDays == "0") {
                    errors.push("Validity(In Days) should be greater than zero.");
                }
                 }
                 if (txtComments.trim() == "") {
                errors.push("Enter Comments .");
                 }
                 if (errors.length > 0) {
                     custAlertMsg(errors.join("<br/>"), "error");
                     return false;
                 } else {
                     if (txt_PageNo != "") {
                         var PageNoValid = checkPageNumbers(txt_PageNo, PageCount);
                         //if (PageNoValid == "false") {
                         //    debugger;
                         //    //errors.push("Requested No.of Pages Exceed Available No.of Pages.");
                         //    custAlertMsg("Requested No.of Pages Exceed Available No.of Pages.", "error");
                         // return false;
                         //}
                     }
                     else {
                         GetPrinterNamesandDownload();
                     }
                     
                 }
            }
           
         function GetPrinterDetails() {
             $.ajax({                 
                  "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetPrinterNamesList" )%>',
                    "data": {},
                    success: function (result) {
                        $('#DocPrintertable tbody').empty();
                        $.each(result.d, function (i, item) {
                            var tr = $("<tr></tr>");
                            var id = i + 1;
                            tr.append(
                                '<td class="ClsPRinterID" style=display:none>' + item.printerID + "</td>" +
                               '<td class="ClsID" >' + id + "</td>" +
                                '<td class="ClsPrinterAreaName">' + item.PrinterAreanName + "</td>" +
                                '<td class="ClsPrinterName">' + item.PrinterName + "</td>" +
                                '<td class="text_left_icon"><a class="print" title="Print" onclick="SelectDocPrinter(this)"></a>' +
                           '</td>' 
                            );
                            $('#DocPrintertable tbody').append(tr);
                        });

                    } 
             });
                    $('#MyModelPrinterNames').modal('show');

         }
         function checkPageNumbers(pageno,pagecount) {
             var data = "";
             $.ajax({
                 "dataType": 'json',
                 "contentType": "application/json; charset=utf-8",
                 "type": "POST",
                 "url": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/validatePageNumbers")%>',
                 "data":JSON.stringify({ strPageNos: pageno,pagecount:pagecount }), 
                 success: function (result) {
                      var message = JSON.stringify(result.d);
                 var parsePath = JSON.parse(message);
                      data= parsePath;                    
                      if (data == "1") {
                          custAlertMsg("Starting and ending letters should not be , or - .", "error");
                          return false;
                      }
                      else if (data == "2") {
                          custAlertMsg("Requested No.of Pages Exceed Available No.of Pages.", "error");
                          return false;
                      }
                      else if (data == "3") {
                          custAlertMsg("Special characters , and - should not be combined.", "error");
                          return false;
                      }
                      else if (data == "0") {
                          GetPrinterNamesandDownload();
                      }
                      else {
                          custAlertMsg(data, "error");
                          return false;
                      }
                 }
             });
             
         }
         function GetPrinterNamesandDownload() {
            if (radio[1].checked)//Print
                     {
                         
                         if (WebConfigDefaultValue == "0")//Printer Names POPU Open 
                         {
                             GetPrinterDetails();
                         }
                         else {
                             showPrint();
                             var btnPD = document.getElementById("<%=btn_Submit.ClientID %>");
                             btnPD.click();                            
                         }
                     } else {
                         showDownload();//Show validation message
                         var btnPD = document.getElementById("<%=btn_Submit.ClientID %>");
                             btnPD.click();

                     }
            }
           
    </script>
        <script>
              function showDownload() {
            custAlertMsg("Document is downloading please wait,Click OK After Download Is Completed..","info","reloadtable();");
            }
             function showPrint() {
            custAlertMsg("Document is printing please wait,Click OK After Print Is Completed..","info","reloadtable();");
        }
             function reloadtable() {
            window.open("<%=ResolveUrl("~/DMS/DocumentCreator/ControllerEffectiveDocumentList.aspx")%>", "_self");
        }
         function SelectDocPrinter(cntrl) {
             var PrinterID = $(cntrl).closest("tr").find(".ClsPRinterID").eq(0).text().trim();
        var PrinterName = $(cntrl).closest("tr").find(".ClsPrinterName").eq(0).text().trim();
        var PrinterAreaName = $(cntrl).closest("tr").find(".ClsPrinterAreaName").eq(0).text().trim();
            document.getElementById("<%=hdnPrinterName.ClientID%>").value = PrinterName;             
            var btnPD = document.getElementById("<%=btn_Submit.ClientID %>");
            btnPD.click();
            showPrint();
         }
    </script>
    <script>
         function ddlCopyOnChange() {
             var CopySelectedValue = document.getElementById("<%=ddl_CopyType.ClientID %>").value;
             if (CopySelectedValue =="1") {
                 $("#div_ValidDays").show();
             } else {
                 $("#div_ValidDays").hide();

             }
         }
        function isNumberCommaDash(event) {
                event = (event) ? event : window.event;
                var charCode = (event.which) ? event.which : event.keyCode;
                if (charCode != 45 && charCode != 44 && charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                else {
                    return true;
                }
         }
        function validatePageNumbers() {
            var txt_PageNo = document.getElementById("<%=txt_PageNo.ClientID%>").value;
            var PageCount = document.getElementById("<%=hdfPageCount.ClientID%>").value;
            var Pages = txt_PageNo.split(',');
            var PgList=null;
            for (i = 0; i < Pages.length; i++) {
                if (Pages[i].includes("-")) {
                    var Pages2 = Pages[i].split("-");
                    for (j = parseInt(Pages2[0]); j <= parseInt(Pages2[1]); j++) {
                        PgList=PgList+j;
                    }
                }
                else {
                    PgList=PgList+parseInt(Pages[i]);
                }
            }
            for (k = 0; k < PgList.length; k++) {
                if (parseInt(PgList[k]) > parseInt(PageCount) || parseInt(PgList[k]) == 0) {
                    return false;
                    break;
                }
            }
            return true;
        }
    </script>

</asp:Content>
