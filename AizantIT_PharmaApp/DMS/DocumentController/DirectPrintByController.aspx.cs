﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
//using DevExpress.DataAccess.Native.Data;
using DevExpress.XtraRichEdit;
using iTextSharp.text.pdf;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;
using iTextSharp.text;
using DevExpress.Pdf;
using System.Printing;
using System.Configuration;

namespace AizantIT_PharmaApp.DMS.DocumentController
{
    public partial class DirectPrintByController : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocObjects docObjects = new DocObjects();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        hdnWebCnfgDocValue.Value = ConfigurationManager.AppSettings.Get("isDefaultPrinter");

                        if (Request.QueryString.Count > 0)
                        {
                            string vid = Request.QueryString[0];
                            hdfVID.Value = vid;
                            PrintDetails();
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Uclose", "CloseBrowser();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DWRV_M1:" + strline + "  " + strMsg);
                string strError = "DWRV_M1:" + strline + "  " + "Page loading failed.";
            }
        }
        //public void DMS_GetReviewerList()
        //{
        //    try
        //    {
        //        UMS_BAL objUMS_Bal = new UMS_BAL();
        //        System.Data.DataTable DMS_Dt = objUMS_Bal.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(hdfDeptID.Value), (int)DMS_UserRole.PrintReviewer_DMS, true);
        //        if (DMS_Dt.Rows.Count > 0)
        //        {
        //            DataRow[] filtered = DMS_Dt.Select("EmpID NOT IN ('" + (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString() + "')");
        //            if (filtered.Any())
        //            {
        //                DMS_Dt = filtered.CopyToDataTable();
        //                ddl_ReviewedBy.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
        //                ddl_ReviewedBy.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
        //                ddl_ReviewedBy.DataSource = DMS_Dt;
        //                ddl_ReviewedBy.DataBind();
        //                ddl_ReviewedBy.Items.Insert(0, new System.Web.UI.WebControls.ListItem("-- Select Request Reviewer --", "0"));
        //            }
        //        }
        //        else
        //        {
        //            //literalSop1.Text = "There are no employees with print reviewer role.";
        //            //msgError.InnerHtml = "There are no employees with print reviewer role for the document department.";
        //            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#ModalError').modal({ backdrop: 'static', keyboard: false });", true);
        //            //upError.Update();
        //            //divliteral.Visible = true;
        //            //richedit2.Visible = false;
        //            //divliteral.InnerText = "There are no employees with print reviewer role.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string strline = HelpClass.LineNo(ex);
        //        string strMsg = HelpClass.SQLEscapeString(ex.Message);
        //        Aizant_log.Error("DWRV_M3:" + strline + "  " + strMsg);
        //        string strError = "DWRV_M3:" + strline + "  " + "Reviewer drop-down loading failed.";
        //        //divliteral.Visible = true;
        //        //richedit2.Visible = false;
        //        //divliteral.InnerText = @" <b style=""color:red;"">" + strError + "</b> ";
        //    }
        //}
        //public void DMS_GetControllerList()
        //{
        //    try
        //    {
        //        System.Data.DataTable DMS_Dt = DMS_Bal.DMS_GetControllersList();
        //        if (DMS_Dt.Rows.Count > 0)
        //        {
        //            DataRow[] filtered = DMS_Dt.Select("EmpID NOT IN ('" + (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString() + "','" + ddl_ReviewedBy.SelectedValue + "')");
        //            if (filtered.Any())
        //            {
        //                DMS_Dt = filtered.CopyToDataTable();
        //                ddl_DocController.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
        //                ddl_DocController.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
        //                ddl_DocController.DataSource = DMS_Dt;
        //                ddl_DocController.DataBind();
        //                ddl_DocController.Items.Insert(0, new System.Web.UI.WebControls.ListItem("-- Select Document Controller --", "0"));
        //            }
        //        }
        //        else
        //        {
        //            HelpClass.custAlertMsg(this, this.GetType(), "There are no employees with controller role.", "info");
        //            //msgError.InnerHtml = "There are no employees with controller role.";
        //            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#ModalError').modal({ backdrop: 'static', keyboard: false });", true);
        //            //upError.Update();
        //            //literalSop1.Text = "There are no employees with controller role.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string strline = HelpClass.LineNo(ex);
        //        string strMsg = HelpClass.SQLEscapeString(ex.Message);
        //        Aizant_log.Error("DWRV_M4:" + strline + "  " + strMsg);
        //        string strError = "DWRV_M4:" + strline + "  " + "Controller drop-down loading failed.";
        //        //divliteral.Visible = true;
        //        //richedit2.Visible = false;
        //        ///divliteral.InnerText = @" <b style=""color:red;"">" + strError + "</b> ";
        //    }
        //}
        public void DMS_GetCopyType()
        {
            try
            {
                System.Data.DataTable DMS_Dt = DMS_Bal.DMS_GetCopyType();
                ddl_CopyType.DataValueField = DMS_Dt.Columns["DistributionCopyTypeID"].ToString();
                ddl_CopyType.DataTextField = DMS_Dt.Columns["DistributionCopyTypeName"].ToString();
                ddl_CopyType.DataSource = DMS_Dt;
                ddl_CopyType.DataBind();
                ddl_CopyType.Items.Insert(0, new System.Web.UI.WebControls.ListItem("-- Select Type of Copy --", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DWRV_M4:" + strline + "  " + strMsg);
                string strError = "DWRV_M4:" + strline + "  " + "Controller drop-down loading failed.";
                //divliteral.Visible = true;
                //richedit2.Visible = false;
                //divliteral.InnerText = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        protected void btn_Reset_Click(object sender, EventArgs e)
        {
            btn_Reset.Enabled = false;
            try
            {
                DMS_Reset();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "select1", "$('.selectpicker1').selectpicker();", true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                btn_Reset.Enabled = true;
            }
        }
        public void DMS_Reset()
        {
            try
            {
                txt_PageNo.Text = "";
                //ddl_DocController.Enabled = false;
                ///ddl_DocController.SelectedIndex = -1;
                //ddl_ReviewedBy.SelectedIndex = -1;
                txt_Purpose.Value = "";
                RadioButtonList1.SelectedValue = "no";
                ddl_CopyType.SelectedIndex = -1;
                txt_Comments.Value = "";
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DWRV_M5:" + strline + "  " + strMsg);
                string strError = "DWRV_M5:" + strline + "  " + "Document Request Reset failed.";
                //divliteral.Visible = true;
                //richedit2.Visible = false;
                //divliteral.InnerText = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            btn_Submit.Enabled = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "select1", "$('.selectpicker1').selectpicker();", true);
            try
            {
                StringBuilder sbErrorMsg = new StringBuilder();
                sbErrorMsg.Clear();
                bool isPageNoValid = validatePageNumbers();
                if (txt_Purpose.Value.Trim() == "")
                {
                    sbErrorMsg.Append(" Enter Purpose." + "<br/>");
                }
                if (ddl_CopyType.SelectedValue.ToString().Trim() == "0")
                {
                    sbErrorMsg.Append(" Select Distribution Copy Type." + "<br/>");
                }
                if (txt_PageNo.Text != "")
                {
                    if (!isPageNoValid)
                    {
                        sbErrorMsg.Append(" Requested No.of Pages Exceed Available No.of Pages." + "<br/>");
                    }
                }
                if (ddl_CopyType.SelectedValue.ToString().Trim() == "1")
                {
                    if (txt_ValidDays.Text.Trim() == "")
                    {
                        sbErrorMsg.Append(" Enter Valid Days." + "<br/>");
                    }
                }
                if (txt_Comments.Value.Trim() == "")
                {
                    sbErrorMsg.Append(" Enter Comments." + "<br/>");
                }
                if (sbErrorMsg.Length > 8)
                {
                    //msgError.InnerHtml = sbErrorMsg.ToString();
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
                    //upError.Update();
                    return;
                }
                System.Data.DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfVID.Value);
                DataTable dt = DMS_Bal.DMS_LatestActiveDoc(Convert.ToInt32(hdfVID.Value));
                if (hdfVID.Value == dt.Rows[0]["VersionID"].ToString() && DMS_DT.Rows[0]["expStatus"].ToString() != "1")
                {
                    docObjects.pkid = Convert.ToInt32(hdfVID.Value);
                    docObjects.Purpose = Regex.Replace(txt_Purpose.Value.Trim(), @"\s+", " ");
                    docObjects.Pageno = Regex.Replace(txt_PageNo.Text.Trim(), @"\s+", " ");
                    docObjects.DMSRequestByID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);

                    //docObjects.DMSReviewedByID = Convert.ToInt32(ddl_ReviewedBy.SelectedValue);
                    //if (ddl_DocController.SelectedValue.ToString().Trim() == "0")
                    //{
                    //    docObjects.DMSDocControllerID = 0;
                    //}
                    //else
                    //{
                    //    docObjects.DMSDocControllerID = Convert.ToInt32(ddl_DocController.SelectedValue);
                    //}
                    docObjects.IsSoftCopy = RadioButtonList1.SelectedValue == "yes" ? true : false;
                    docObjects.CopyType = Convert.ToInt32(ddl_CopyType.SelectedValue);
                    if (docObjects.CopyType == 1) { 
                    docObjects.Validity = Convert.ToInt32(Regex.Replace(txt_ValidDays.Text.Trim(), @"\s+", " "));
                }
                    docObjects.PrintReqComments = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                    docObjects.Mode = 1;
                    int DMS_ins = DMS_Bal.DMS_DocPrintRequestSubmission(docObjects, out int DocReqID);
                    //msgSuccess.InnerHtml = "New Document Request with no. <b>" + DMS_ins.ToString() + "</b> has been Initiated Successfully.";
                    //Upsucsess.Update();
                    DocPrintRequest DocReqObjects = new DocPrintRequest();
                    DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();

                    if (RadioButtonList1.SelectedValue == "yes")//Download
                    {
                            Session["DwnloadFilePath"] = "";
                            DataTable dtDown = DMS_Bal.DMS_GetDocPrintReqDetails(Convert.ToString(DocReqID));
                            docObjects.Version = Convert.ToInt32(hdfVID.Value);
                            docObjects.RoleID = 0;
                            docObjects.EmpID = 0;
                            docObjects.FileType1 = ".pdf";
                            docObjects.Mode = 0;
                            System.Data.DataTable DMS_DT1 = DMS_Bal.DMS_GetDocumentLocationList(docObjects);
                            string pathin = Server.MapPath(@DMS_DT1.Rows[0]["FilePath"].ToString());
                            string strFile2Upload = DynamicFolder.CreateDynamicFolder(3);
                            string File3Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                            string TargetPDF = (strFile2Upload + "\\" + File3Path);

                            PdfReader reader = new PdfReader(pathin);
                            PdfStamper stamper = new PdfStamper(reader, new FileStream(TargetPDF, FileMode.Create));
                            int pages = reader.NumberOfPages;
                            for (int i = 1; i <= pages; i++)
                            {
                                PdfContentByte underContent = stamper.GetOverContent(i);
                                PdfPTable Footertab = new PdfPTable(3);
                                Footertab.TotalWidth = 500F;
                                //row 1
                                string ReqNum = "Request Number: " + dtDown.Rows[0]["RequestNumber"].ToString();
                                PdfPCell RequestNumber = new PdfPCell(new Phrase(ReqNum,
                                               new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                                Footertab.AddCell(RequestNumber);
                                string CopyNum = "Copy Number: " + "0000" + dtDown.Rows[0]["PrintSerialNo"].ToString();
                                PdfPCell CopyNumber = new PdfPCell(new Phrase(CopyNum,
                                               new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                                Footertab.AddCell(CopyNumber);
                                PdfPCell TypeofCopy = new PdfPCell(new Phrase(dtDown.Rows[0]["DistributionCopyType"].ToString(),
                                               new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                                Footertab.AddCell(TypeofCopy);
                                //row 2
                                string PrintBy = "Printed By: " + dtDown.Rows[0]["RequestBy"].ToString();
                                PdfPCell PrintedBy = new PdfPCell(new Phrase(PrintBy,
                                               new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                                Footertab.AddCell(PrintedBy);
                                string PrintDate = "Printed Date: " + DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
                                PdfPCell PrintedDate = new PdfPCell(new Phrase(PrintDate,
                                               new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                                Footertab.AddCell(PrintedDate);
                                if (dtDown.Rows[0]["DistributionCopyTypeID"].ToString() == "1")
                                {
                                    string validity = "Valid Upto: " + "(" + (Convert.ToInt32(dtDown.Rows[0]["ValidDays"]) * 24) + " hrs) " + DateTime.Now.AddDays(Convert.ToDouble(dtDown.Rows[0]["ValidDays"])).ToString("dd MMM yyyy HH:mm:ss");
                                    PdfPCell ValidUpto = new PdfPCell(new Phrase(validity,
                                                   new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                                    Footertab.AddCell(ValidUpto);
                                }
                                else
                                {
                                    PdfPCell ValidUpto = new PdfPCell(new Phrase("Validity: (N/A)",
                                                   new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                                    Footertab.AddCell(ValidUpto);
                                }
                                iTextSharp.text.Document document = underContent.PdfDocument;
                                Footertab.WriteSelectedRows(0, -1, 50f, document.Bottom + 5f, underContent);
                            }
                            stamper.Close();
                            reader.Close();
                            string DownloadPDF = ExtractPages(TargetPDF, dtDown);
                            System.IO.FileInfo file = new System.IO.FileInfo(DownloadPDF);
                        //string[] FileName = dtDown.Rows[0]["FileName"].ToString().Split('.');
                        string FileName = dtDown.Rows[0]["DownloadTitle"].ToString() + file.Extension;
                        Response.Clear();
                            Response.ContentType = "application/pdf";
                            Response.AddHeader("Content-Disposition", "attachment; filename=" + FileName);
                            Response.TransmitFile(file.FullName);
                            DocReqObjects.DocReqID = Convert.ToInt32(Convert.ToString(DocReqID));
                            DocReqObjects.RequestedByID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                        DocReqObjects.Mode = 1;
                        DocReqObjects.Comments = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                        int DMS_AP = DMS_BalDM.DMS_DocDownloadReqByRequestor(DocReqObjects);
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document Request Number <b>" + dtDown.Rows[0]["RequestNumber"].ToString() + "</b> Downloaded by Requester Successfully.", "success", "reloadtable();");
                       
                    }
                    else if(RadioButtonList1.SelectedValue == "no")//Print
                    {
                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "showPrint();", true);
                        System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
                        string DefaultPrinter = string.Empty,
                            isDefaultPrinter =hdnWebCnfgDocValue.Value;
                        // ConfigurationManager.AppSettings.Get("isDefaultPrinter");;
                        List<string> ListofInstalledPrinters=new List<string>();
                        String pkInstalledPrinters;
                        for (int i = 0; i < System.Drawing.Printing.PrinterSettings.InstalledPrinters.Count; i++)
                        {
                            pkInstalledPrinters = System.Drawing.Printing.PrinterSettings.InstalledPrinters[i];
                            ListofInstalledPrinters.Add(pkInstalledPrinters);
                        }
                        //ListofInstalledPrinters.Add( System.Drawing.Printing.PrinterSettings.InstalledPrinters)
                        if (isDefaultPrinter == "1")
                        {
                            DefaultPrinter = printerSettings.PrinterName = LocalPrintServer.GetDefaultPrintQueue().FullName;
                        }
                        else if (isDefaultPrinter == "0")
                        {
                            DefaultPrinter = printerSettings.PrinterName =hdnPrinterName.Value;//get Printer Name from client side
                        }

                        if (!ListofInstalledPrinters.Contains(DefaultPrinter)||DefaultPrinter == "Microsoft Print to PDF" || DefaultPrinter == "Microsoft XPS Document Writer" || DefaultPrinter.Contains("Send To OneNote"))
                        {
                            HelpClass.custAlertMsg(this, this.GetType(), "Sorry ! There is no Printer configured to print.", "error");
                            return;
                        }

                        else
                        {
                            DataTable dtPrint = DMS_Bal.DMS_GetDocPrintReqDetails(Convert.ToString(DocReqID));
                            docObjects.Version = Convert.ToInt32(hdfVID.Value);
                            docObjects.RoleID = 0;
                            docObjects.EmpID = 0;
                            docObjects.FileType1 = ".pdf";
                            docObjects.Mode = 0;
                            System.Data.DataTable DMS_DTPrint = DMS_Bal.DMS_GetDocumentLocationList(docObjects);
                            string pathin = Server.MapPath(@DMS_DTPrint.Rows[0]["FilePath"].ToString());
                            string strFile2Upload = DynamicFolder.CreateDynamicFolder(3);
                            string File3Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                            string TargetPDF = (strFile2Upload + "\\" + File3Path);

                            PdfReader reader = new PdfReader(pathin);
                            PdfStamper stamper = new PdfStamper(reader, new FileStream(TargetPDF, FileMode.Create));
                            int pages = reader.NumberOfPages;
                            for (int i = 1; i <= pages; i++)
                            {
                                PdfContentByte underContent = stamper.GetOverContent(i);
                                PdfPTable Footertab = new PdfPTable(3);
                                Footertab.TotalWidth = 500F;
                                //row 1
                                string ReqNum = "Request Number: " + dtPrint.Rows[0]["RequestNumber"].ToString();
                                PdfPCell RequestNumber = new PdfPCell(new Phrase(ReqNum,
                                               new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                                Footertab.AddCell(RequestNumber);
                                string CopyNum = "Copy Number: " + "0000" + dtPrint.Rows[0]["PrintSerialNo"].ToString();
                                PdfPCell CopyNumber = new PdfPCell(new Phrase(CopyNum,
                                               new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                                Footertab.AddCell(CopyNumber);

                                PdfPCell TypeofCopy = new PdfPCell(new Phrase(dtPrint.Rows[0]["DistributionCopyType"].ToString(),
                                               new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                                Footertab.AddCell(TypeofCopy);
                                //row 2
                                string PrintBy = "Printed By: " + dtPrint.Rows[0]["RequestBy"].ToString();
                                PdfPCell PrintedBy = new PdfPCell(new Phrase(PrintBy,
                                               new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                                Footertab.AddCell(PrintedBy);
                                string PrintDate = "Printed Date: " + DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
                                PdfPCell PrintedDate = new PdfPCell(new Phrase(PrintDate,
                                               new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                                Footertab.AddCell(PrintedDate);
                                if (dtPrint.Rows[0]["DistributionCopyTypeID"].ToString() == "1")
                                {
                                    string validity = "Valid Upto: " + "(" + (Convert.ToInt32(dtPrint.Rows[0]["ValidDays"]) * 24) + " hrs) " + DateTime.Now.AddDays(Convert.ToDouble(dtPrint.Rows[0]["ValidDays"])).ToString("dd MMM yyyy HH:mm:ss");
                                    PdfPCell ValidUpto = new PdfPCell(new Phrase(validity,
                                                   new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                                    Footertab.AddCell(ValidUpto);
                                }
                                else
                                {
                                    PdfPCell ValidUpto = new PdfPCell(new Phrase("Validity: (N/A)",
                                                   new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                                    Footertab.AddCell(ValidUpto);
                                }
                                iTextSharp.text.Document document = underContent.PdfDocument;
                                Footertab.WriteSelectedRows(0, -1, 50f, document.Bottom + 5f, underContent);
                            }
                            stamper.Close();
                            reader.Close();
                            // Create a Pdf Document Processor instance and load a PDF into it. 
                            PdfDocumentProcessor documentProcessor = new PdfDocumentProcessor();
                            documentProcessor.LoadDocument(TargetPDF);

                            //Declare the PDF printer settings.
                            PdfPrinterSettings pdfPrinterSettings = new PdfPrinterSettings();

                            //Specify the PDF printer settings.
                            if (string.IsNullOrEmpty(dtPrint.Rows[0]["PageNumbers"].ToString()))
                            {
                            }
                            else
                            {
                                string strPageNos = dtPrint.Rows[0]["PageNumbers"].ToString();
                                strPageNos = "," + strPageNos;
                                string[] arrPnos1 = strPageNos.Split(',');
                                ArrayList PgNo = new ArrayList();
                                PgNo.Add(1);
                                foreach (string strPg in arrPnos1)
                                {
                                    if (strPg.Trim().Length > 0)
                                    {
                                        if (strPg.Trim().Contains("-"))
                                        {
                                            string[] arrPg2 = strPg.Split('-');
                                            for (int i = Convert.ToInt32(arrPg2[0]); i <= Convert.ToInt32(arrPg2[1]); i++)
                                            {
                                                PgNo.Add(i + 1);
                                            }
                                        }
                                        else
                                        {
                                            PgNo.Add(Convert.ToInt32(strPg) + 1);
                                        }
                                    }
                                }

                                pdfPrinterSettings.PageNumbers = PgNo.OfType<int>().ToArray();
                            }
                            //Setting the PdfPrintScaleMode property to CustomScale requires
                            //specifying the Scale property, as well.                  


                            pdfPrinterSettings.ScaleMode = PdfPrintScaleMode.Fit;
                            pdfPrinterSettings.Settings.Copies = 1;
                            pdfPrinterSettings.Settings.Duplex = System.Drawing.Printing.Duplex.Simplex;
                            pdfPrinterSettings.Settings.PrinterName = DefaultPrinter;
                            //Print the document using the specified printer settings. 
                            documentProcessor.Print(pdfPrinterSettings);
                            DocReqObjects.DocReqID = Convert.ToInt32(DocReqID);
                            DocReqObjects.RequestedByID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                            DocReqObjects.Mode = 1;
                            DocReqObjects.Comments = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                            int DMS_AP = DMS_BalDM.DMS_DocPrintReqByRequestor(DocReqObjects);
                            HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document Request Number <b>" + dtPrint.Rows[0]["RequestNumber"].ToString() + "</b> Printed Successfully.", "success", "reloadtable();");
                        }
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "$('#ModalSuccess').modal({ backdrop: 'static', keyboard: false });", true);
                        DMS_Reset();
                    }
                    
                }
                else if (hdfVID.Value != dt.Rows[0]["VersionID"].ToString())
                {
                    //msgError.InnerHtml = "The document with this version is not being used anymore";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#ModalError').modal({ backdrop: 'static', keyboard: false });", true);
                    //upError.Update();
                    return;
                }
                else if (DMS_DT.Rows[0]["expStatus"].ToString() == "1")
                {
                    //msgError.InnerHtml = "The document with this version is overdue";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#ModalError').modal({ backdrop: 'static', keyboard: false });", true);
                    //upError.Update();
                    return;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DWRV_M6:" + strline + "  " + strMsg);
                string strError = "DWRV_M6:" + strline + "  " + "Document Request Submission failed.";
                //divliteral.Visible = true;
                //richedit2.Visible = false;
                //divliteral.InnerText = @" <b style=""color:red;"">" + strError + "</b> ";
            }
            finally
            {
                btn_Submit.Enabled = true;
            }
        }
        public string ExtractPages(string SourcePDF, DataTable dt1)
        {
            try
            {

                if (string.IsNullOrEmpty(dt1.Rows[0]["PageNumbers"].ToString()))
                {
                    return SourcePDF;
                }
                else
                {
                    string strPageNos = dt1.Rows[0]["PageNumbers"].ToString();
                    strPageNos = "," + strPageNos;
                    string[] arrPnos1 = strPageNos.Split(',');
                    ArrayList PgNo = new ArrayList();
                    PgNo.Add(1);
                    string strFile2Upload = DynamicFolder.CreateDynamicFolder(3);
                    string File3Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                    string TargetPDF = (strFile2Upload + "\\" + File3Path);
                    foreach (string strPg in arrPnos1)
                    {
                        if (strPg.Trim().Length > 0)
                        {
                            if (strPg.Trim().Contains("-"))
                            {
                                string[] arrPg2 = strPg.Split('-');
                                for (int i = Convert.ToInt32(arrPg2[0]); i <= Convert.ToInt32(arrPg2[1]); i++)
                                {
                                    PgNo.Add(i + 1);
                                }
                            }
                            else
                            {
                                PgNo.Add(Convert.ToInt32(strPg) + 1);
                            }
                        }
                    }
                    using (PdfDocumentProcessor source = new PdfDocumentProcessor())
                    {
                        source.LoadDocument(SourcePDF);
                        using (PdfDocumentProcessor target = new PdfDocumentProcessor())
                        {

                            target.CreateEmptyDocument(TargetPDF);
                            foreach (int Pg1 in PgNo)
                            {
                                target.Document.Pages.Add(source.Document.Pages[Pg1 - 1]);
                            }
                        }
                    }
                    return TargetPDF;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btn_Cancel_Click(object sender, EventArgs e)
        {
            btn_Cancel.Enabled = false;
            try
            {
                Response.Redirect("~/DMS/DocumentCreator/ControllerEffectiveDocumentList.aspx", false);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DWRV_M7:" + strline + "  " + strMsg);
                string strError = "DWRV_M7:" + strline + "  " + "Document Request Cancel failed.";
                //divliteral.Visible = true;
                //richedit2.Visible = false;
                //divliteral.InnerText = @" <b style=""color:red;"">" + strError + "</b> ";
            }
            finally
            {
                btn_Cancel.Enabled = true;
            }
        }
        private int getPageCountByPath(string strTargPath)
        {
            RichEditDocumentServer serverSource = new RichEditDocumentServer();
            serverSource.LoadDocument(strTargPath);
            int SourcePageCount = serverSource.DocumentLayout.GetPageCount();
            return SourcePageCount;
        }
        private int getPageCountByStream(MemoryStream memoryStream)
        {
            RichEditDocumentServer serverSource = new RichEditDocumentServer();
            serverSource.LoadDocument(memoryStream);
            int SourcePageCount = serverSource.DocumentLayout.GetPageCount();
            return SourcePageCount;
        }
        private bool validatePageNumbers()
        {
            string strPageNos = txt_PageNo.Text;
            strPageNos = "," + strPageNos;
            string[] arrPnos1 = strPageNos.Split(',');
            ArrayList PgNo = new ArrayList();
            foreach (string strPg in arrPnos1)
            {
                if (strPg.Trim().Length > 0)
                {
                    if (strPg.Trim().Contains("-"))
                    {
                        string[] arrPg2 = strPg.Split('-');
                        for (int i = Convert.ToInt32(arrPg2[0]); i <= Convert.ToInt32(arrPg2[1]); i++)
                        {
                            PgNo.Add(i);
                        }
                    }
                    else
                    {
                        PgNo.Add(Convert.ToInt32(strPg));
                    }
                }
            }
            bool isValidPgNO = true;
            foreach (int page in PgNo)
            {
                if (page > Convert.ToInt32(hdfPageCount.Value) || page == 0)
                {
                    isValidPgNO = false;
                    break;
                }
            }
            return isValidPgNO;
        }
        //protected void ddl_ReviewedBy_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //   // ddl_DocController.Enabled = true;
        //    //DMS_GetControllerList();
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "select1", "$('.selectpicker1').selectpicker();", true);
        //}
        protected void PrintDetails()
        {
            DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfVID.Value);
            DataTable dt = DMS_Bal.DMS_LatestActiveDoc(Convert.ToInt32(hdfVID.Value));
            hdfDeptID.Value = DMS_DT.Rows[0]["DeptID"].ToString();
            if (DMS_DT.Rows[0]["FileName"].ToString().Length > 0)
            {
                docObjects.Version = Convert.ToInt32(hdfVID.Value);
                docObjects.RoleID = 0;
                docObjects.EmpID = 0;
                docObjects.FileType1 = ".pdf";
                docObjects.Mode = 0;
                System.Data.DataTable dt1 = DMS_Bal.DMS_GetDocumentLocationList(docObjects);
                string pathin = Server.MapPath(dt1.Rows[0]["FilePath"].ToString());
                PdfReader reader = new PdfReader(pathin);
                hdfPageCount.Value = Convert.ToString(reader.NumberOfPages - 1);
            }
            //DMS_GetReviewerList();
            DMS_GetCopyType();
            if (hdfVID.Value == dt.Rows[0]["VersionID"].ToString() && DMS_DT.Rows[0]["expStatus"].ToString() != "1")
            {
                txt_DocumentName.Text = DMS_DT.Rows[0]["DocumentName"].ToString();
                //span_FileTitle.InnerHtml = hdfDocNum.Value;
                txt_RequestedBy.Text = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString(); ;
                lblPageNumbers.Text = "Total No.of Pages : " + hdfPageCount.Value;
                txt_DocumentNumber.Text = DMS_DT.Rows[0]["DocumentNumber"].ToString();
                txt_VersionNumber.Text = DMS_DT.Rows[0]["VersionNumber"].ToString();
                DMS_Reset();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_request').modal({ show: true, backdrop: 'static', keyboard: false });", true);
            }
            else if (hdfVID.Value != dt.Rows[0]["VersionID"].ToString())
            {
                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "The document with this version is not being used anymore.", "error", "reloadtable();");
                //msgError.InnerHtml = "The document with this version is not being used anymore";
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#ModalError').modal({ backdrop: 'static', keyboard: false });", true);
                //upError.Update();
                return;
            }
            //else if (DMS_DT.Rows[0]["expStatus"].ToString() == "1")
            //{
            //    //msgError.InnerHtml = "The document with this version is overdue";
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#ModalError').modal({ backdrop: 'static', keyboard: false });", true);
            //    //upError.Update();
            //    return;
            //}
        }
    }
}