﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_BusinessLayer;

namespace AizantIT_PharmaApp.DMS.DocumentController
{
    public partial class DocReqControllerList : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        DocPrintRequest DocReqObjects = new DocPrintRequest();
        DocObjects docObjects = new DocObjects();
        UMS_BAL objUMS_BAL;
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=30").Length > 0)//10-controller
                            {
                                if (Request.QueryString["Notification_ID"] != null)
                                {
                                    objUMS_BAL = new UMS_BAL();
                                    int[] empIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, empIDs); //2 represents NotifyEmp had visited the page through notification link.
                                    DataTable dt = DMS_Bal.DMS_NotificationDocRequestID(Convert.ToInt32(Request.QueryString["Notification_ID"]));
                                    hdfVID.Value = dt.Rows[0]["DocRequestID"].ToString();
                                    btnRequestControl_Click(null, null);
                                }
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                    else
                    {
                        ElectronicSign.buttonClick += new EventHandler(ElectronicSign_buttonClick);
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DRCL_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DRCL_M1:" + strline + "  " + "Document Load failed.", "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        protected void btnRequestControl_Click(object sender, EventArgs e)
        {
            btnRequestControl.Enabled = false;
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocPrintReqDetails(hdfVID.Value);
                txt_RequestedBy.Text = DMS_DT.Rows[0]["RequestBy"].ToString();
                if (Convert.ToBoolean(DMS_DT.Rows[0]["isSoftCopy"].ToString()) == true)
                {
                    RadioButtonList1.SelectedValue = "yes";
                }
                else
                {
                    RadioButtonList1.SelectedValue = "no";
                }
                RadioButtonList1.Enabled = false;
                txt_PageNo.Text = string.IsNullOrEmpty(DMS_DT.Rows[0]["PageNumbers"].ToString()) ? "All" : DMS_DT.Rows[0]["PageNumbers"].ToString();
                txt_DocumentName.Text = DMS_DT.Rows[0]["DocumentName"].ToString();
                txt_Purpose.Text = DMS_DT.Rows[0]["PrintPurpose"].ToString();
                txt_ReviewedBy.Text = DMS_DT.Rows[0]["ReviewedBy"].ToString();
                txt_DocController.Text = DMS_DT.Rows[0]["DocController"].ToString();
                txt_CopyType.Text = DMS_DT.Rows[0]["DistributionCopyType"].ToString();
                txt_DocumentNumber.Text = DMS_DT.Rows[0]["DocumentNumber"].ToString();
                txt_VersionNumber.Text = DMS_DT.Rows[0]["VersionNumber"].ToString();
                if (DMS_DT.Rows[0]["DistributionCopyTypeID"].ToString() == "1")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show1", "showValidity();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "hide1", "hideValidity();", true);
                }
                txt_ValidDays.Text = "";
                txt_Comments.Value = "";
                UpGridButtons.Update();
                span_FileTitle.InnerText = "Request Number-" + DMS_DT.Rows[0]["RequestNumber"].ToString();
                hdfPkID.Value = DMS_DT.Rows[0]["DocVersionID"].ToString();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_request').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DRCL_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DRCL_M2:" + strline + "  " + "Request modal loading failed.", "error");
            }
            finally
            {
                btnRequestControl.Enabled = true;
            }
        }
        protected void btnCommentsHistoryView_Click(object sender, EventArgs e)
        {
            btnCommentsHistoryView.Enabled = false;
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocDistributionHistory(hdfVID.Value);
                gv_CommentHistory.DataSource = DMS_DT;
                ViewState["HistoryDataTable"] = DMS_DT;
                gv_CommentHistory.DataBind();
                UpGridButtons.Update();
                span4.InnerHtml = DMS_DT.Rows[0]["DocReqTitle"].ToString();
                upcomment.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myCommentHistory').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DRCL_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DRCL_M3:" + strline + "  " + "Document Request History loading failed.", "error");
            }
            finally
            {
                btnCommentsHistoryView.Enabled = true;
            }
        }
        protected void gv_CommentHistory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string _fullcomments = (e.Row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox).Text;
                    LinkButton _lnkmorecomments = (e.Row.Cells[3].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (e.Row.Cells[3].FindControl("lbcommentsless") as LinkButton);
                    if (_fullcomments.Length > 70)
                    {
                        (e.Row.Cells[3].FindControl("lbl_shortComments") as Label).Visible = true;
                        System.Web.UI.WebControls.TextBox FullTbx = (e.Row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox);
                        (e.Row.Cells[3].FindControl("lbl_shortComments") as Label).Text = _fullcomments.Substring(0, 70) + "..";
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = true;

                        if (_fullcomments.Length > 100 && _fullcomments.Length <= 200)
                        {
                            FullTbx.Rows = 3;
                        }
                        else if (_fullcomments.Length > 200 && _fullcomments.Length <= 300)
                        {
                            FullTbx.Rows = 4;
                        }
                        FullTbx.Visible = false;
                    }
                    else
                    {
                        (e.Row.Cells[3].FindControl("lbl_shortComments") as Label).Visible = true;
                        (e.Row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox).Visible = false;
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DRCL_M6:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DRCL_M6:" + strline + "  " + "Comment History row data-bound failed.", "error");
            }
        }
        protected void gv_CommentHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                
                if (e.CommandName == "SM")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    LinkButton _lnkmorecomments = (row.Cells[3].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (row.Cells[3].FindControl("lbcommentsless") as LinkButton);
                    (row.Cells[3].FindControl("lbl_shortComments") as Label).Visible = false;
                    (row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox).Visible = true;
                    _lnklesscomments.Visible = true;
                    _lnkmorecomments.Visible = false;
                }
                else if (e.CommandName == "SL")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    LinkButton _lnkmorecomments = (row.Cells[3].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (row.Cells[3].FindControl("lbcommentsless") as LinkButton);
                    (row.Cells[3].FindControl("lbl_shortComments") as Label).Visible = true;
                    (row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox).Visible = false;
                    _lnklesscomments.Visible = false;
                    _lnkmorecomments.Visible = true;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DRCL_M7:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DRCL_M7:" + strline + "  " + "Comment History row command failed.", "error");
            }
        }
        protected void ElectronicSign_buttonClick(object sender, EventArgs e)
        {
            try
            {
                bool b = ElectronicSign.IsPasswordValid;
                if (b == true)
                {
                    if (hdnAction.Value == "Approve")
                    {
                        Approve();
                    }
                    else if (hdnAction.Value == "Reject")
                    {
                        Reject();
                    }
                }
                if (b == false)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Password is Incorrect!", "error");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DRCL_M8:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DRCL_M8:" + strline + "  " + "Electronic Signature validation failed.", "error");
            }
        }
        public void Approve()
        {
            try
            {
                StringBuilder sbErrorMsg = new StringBuilder();
                sbErrorMsg.Clear();
                if (txt_Comments.Value.Trim() == "")
                {
                    sbErrorMsg.Append(" Enter Comments." + "<br/>");
                }
                if (sbErrorMsg.Length > 8)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
                    return;
                }
                DocReqObjects.DocReqID = Convert.ToInt32(hdfVID.Value);
                DocReqObjects.ReviewedByID = Convert.ToInt32(hdnEmpID.Value);
                DocReqObjects.Comments = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                if (txt_ValidDays.Text.Trim() == "")
                {
                    DocReqObjects.Validity = 0;
                }
                else
                {
                    DocReqObjects.Validity = Convert.ToInt32(txt_ValidDays.Text.Trim());
                }
                int DMS_AP = DMS_BalDM.DMS_DocReqApprovedByController(DocReqObjects);
                DataTable DMS_DT = DMS_Bal.DMS_GetDocPrintReqDetails(hdfVID.Value);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document Request Number <b>" + DMS_DT.Rows[0]["RequestNumber"].ToString() + "</b> Approved by Controller Successfully.", "success", "reloadtable();");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DRCL_M9:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DRCL_M9:" + strline + "  " + "Document Request Approval failed.", "error");
            }
        }
        public void Reject()
        {
            try
            {
                StringBuilder sbErrorMsg = new StringBuilder();
                sbErrorMsg.Clear();
                if (txt_Comments.Value.Trim() == "")
                {
                    sbErrorMsg.Append(" Enter Comments." + "<br/>");
                }
                if (sbErrorMsg.Length > 8)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
                    return;
                }
                DocReqObjects.DocReqID = Convert.ToInt32(hdfVID.Value);
                DocReqObjects.ReviewedByID = Convert.ToInt32(hdnEmpID.Value);
                DocReqObjects.Comments = Regex.Replace(txt_Comments.Value.Trim(), @"\s+", " ");
                int DMS_AP = DMS_BalDM.DMS_DocReqRejectedByReviewer(DocReqObjects);
                DataTable DMS_DT = DMS_Bal.DMS_GetDocPrintReqDetails(hdfVID.Value);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document Request Number <b>" + DMS_DT.Rows[0]["RequestNumber"].ToString() + "</b> Rejected by Controller Successfully.", "success", "reloadtable();");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DRCL_M10:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DRCL_M10:" + strline + "  " + "Document Request Reject failed.", "error");
            }
        }
        protected void btn_View_Click(object sender, EventArgs e)
        {
            btn_View.Enabled = false;
            try
            {
                DataTable DMS_DT1 = DMS_Bal.DMS_GetDocPrintReqDetails(hdfVID.Value);
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(DMS_DT1.Rows[0]["DocVersionID"].ToString());
                span1.InnerHtml = "<b>" + DMS_DT.Rows[0]["DocumentName"].ToString() + "</b>";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myDocumentView').modal({ show: true, backdrop: 'static', keyboard: false });", true);

                if (DMS_DT1.Rows[0]["DistributionCopyTypeID"].ToString() == "1")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "show1", "showValidity();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "hide1", "hideValidity();", true);
                }
                hdfViewType.Value = "0";
                hdfViewEmpID.Value = "0";
                hdfViewRoleID.Value = "0";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view1", "ViewDocument('#divPDF_Viewer');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
                UpHeading.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DRCL_M11:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DRCL_M11:" + strline + "  " + "Document Viewing failed.", "error");
            }
            finally
            {
                btn_View.Enabled = true;
            }
        }
        protected void gv_CommentHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_CommentHistory.PageIndex = e.NewPageIndex;
                gv_CommentHistory.DataSource = (DataTable)(ViewState["HistoryDataTable"]);
                gv_CommentHistory.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DRCL_M12:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DRCL_M12:" + strline + "  " + "grid view page index changing failed.", "error");
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }        
    }
}