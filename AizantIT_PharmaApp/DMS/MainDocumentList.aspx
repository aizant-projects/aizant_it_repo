﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="MainDocumentList.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.MainDocumentList" %>

<%@ Register Src="~/UserControls/ConfirmCustomAlert.ascx" TagPrefix="uc1" TagName="ConfirmCustomAlert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        table tbody tr th {
            background-color: #07889a;
            color: #fff;
            text-align: center;
        }

        table tbody tr {
            text-align: center;
            height: 30px;
        }

        .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
            background-color: #07889a !important;
        }

        iframe {
            overflow-y: hidden;
        }       
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div id="divMainDocListReport" style="display:none">
        <%--<asp:Button ID="btnAdmintblprint" runat="server" class="float-right  btn-signup_popup" Text="Report" OnClick="btnAdmintblprint_Click" style="margin-left:4px;" OnClientClick="DoubleClickHandleReport()" />--%>
       <input type="button" id="btnAdmintblprint"  class="float-right  btn-signup_popup report_btn" onclick="PrintManiDocumentList();" value="Reports"/>
    </div>
    <div class="">
        <asp:Button ID="btnAdd" runat="server" class="float-right  btn-signup_popup" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
    </div>
    <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 padding-none float-left">
        <div class=" col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 dms_outer_border float-left padding-none">
            <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server" visible="false">
                <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
            </div>
            <div id="divMainContainer" runat="server" visible="true">
                <div class=" col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 padding-none float-right ">
                    <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12  padding-none float-left">
                        <div class="grid_header col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 float-left" id="dvDocumentLblName">Document List
                       <%--   <div class=" float-right">
                     <a href="#" data-target="#documentlistmodal" data-toggle="modal" >
                         <button type="button" class=" dms_btn-search">Submit</button> 
                         </a>
                     </div>--%>
                            <%--<div class="col-4 float-right ">
            <div class="input-group search_bgcolor">
                <input class="form-control search_bgcolor searchableinput" type="search" value=" abc pqr" >
                <span class="input-group-append">
                    <div class="input-group-text searchicon_border bg-transparent"><i class="fa fa-search"></i></div>
                </span>
            </div>
                 
        </div>--%>
                 

                        </div>
                        <div class="col-lg-12 col-lg-12 col-12 col-md-12 col-sm-12 grid_panel_full border_top_none bottom float-left">
                            <div class="col-12 top float-left  padding-none">
                            <table id="dtDocList" class="display datatable_cust" cellspacing="0" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>VersionID</th>
                                        <th>vDocumentID</th>
                                        <th>Document Number</th>
                                        <th>Document Name</th>
                                        <th>Version</th>
                                        <th>Document Type</th>
                                        <th>Department</th>
                                        <th>Request Type</th>
                                        <th>Effective Date</th>
                                        <th>Review Date</th>
                                        <th>Status</th>
                                        <th>View Doc</th>
                                        <th>History</th>
                                        <th>Privacy</th>
                                        <th>Document Status</th>
                                    </tr>
                                </thead>
                            </table>
                                </div>
                            <div class=" col-lg-12  float-left bottom">
                                <img src="<%=ResolveUrl("~/Images/GridActionImages/revert_tms.png")%>" /><b class="grid_icon_legend">Review Date Exceeded</b>
                                <img src="<%=ResolveUrl("~/Images/GridActionImages/summary_latest.png")%>" /><b class="grid_icon_legend">History</b>
                                <img src="<%=ResolveUrl("~/Images/GridActionImages/privacy.png")%>" /><b class="grid_icon_legend">Privacy</b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-------- File VIEW-------------->
    <div id="myModal_lock" class="modal department fade" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 99%;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    
                    <asp:UpdatePanel ID="UpHeading" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <span id="span_FileTitle" runat="server">Document</span>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" style="max-height: 680px;">
                    <div id="divPDF_Viewer">
                        <noscript>
                            Please Enable JavaScript.
                        </noscript>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-------- End File VIEW-------------->

    <!--------Comment History View-------------->
    <div id="myCommentHistory" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 85%">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel ID="upcomment" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-header">
                            
                            <span id="span4" runat="server"></span>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body" style="max-height: 600px; overflow-y: auto">
                            <div class=" history_grid" style="overflow-y: auto;">
                                <asp:GridView ID="gv_CommentHistory" runat="server" CssClass="table table-hover" ClientIDMode="Static" EmptyDataText="No Records Found" AutoGenerateColumns="False" PagerSettings-Mode="NumericFirstLast" BackColor="White" BorderColor="#d2d2d2" BorderStyle="None" BorderWidth="1px" OnRowDataBound="gv_CommentHistory_RowDataBound" OnRowCommand="gv_CommentHistory_RowCommand" AllowPaging="true" PageSize="10" OnPageIndexChanging="gv_CommentHistory_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="User" ItemStyle-Width="12%" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_GivenBy" runat="server" Text='<%# Eval("GivenBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Role" runat="server" Text='<%# Eval("CommentedRole") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" >
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Date" runat="server" Text='<%# Eval("CommentedDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Time Spent" >
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Time" runat="server" Text='<%# Eval("Time") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="22%" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Action" runat="server" Text='<%# Eval("Action") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="26%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_shortComments" runat="server" Text='<%# Eval("Comments") %>'></asp:Label>
                                                <asp:TextBox ID="txt_Comments" runat="server" ReadOnly="true" Rows="4" TextMode="MultiLine" Text='<%# Eval("Comments") %>' Width="100%"></asp:TextBox>
                                                <asp:LinkButton ID="lbcommentsmore" runat="server" CommandArgument='<%# Eval("Comments")%>' CommandName="SM">Show More</asp:LinkButton>
                                                <asp:LinkButton ID="lbcommentsless" runat="server" CommandArgument='<%# Eval("Comments") %>' CommandName="SL">Show Less</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Assigned To">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_AssignedTo" runat="server" Text='<%# Eval("AssignedTo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle cssClass="gridpager"  HorizontalAlign="Right" />
                                </asp:GridView>
                            </div>
                        </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!--------End Comment History View-------------->

    <!--------Edit Document Privacy View-------------->
    <div id="myDocumentPrivacyEdit" class="modal department fade" role="dialog" style=" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg modal-dialog-centered" style="min-width: 19%; overflow: hidden">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <span id="span1" runat="server"></span>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" style="max-height: 680px;">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <label for="inputPassword3" class="col-2 control-label padding-none float-left">Privacy:<span class="smallred_label">*</span></label>
                            <div class="btn-group padding-none col-sm-12 col-12 col-lg-12 col-md-12 bottom float-left" data-toggle="buttons" id="divOpinion" runat="server">                               
                                <label class="btn btn-primary_radio radio_check col-6 control-label" id="lblRadioPublic">
                                    <asp:RadioButton ID="rYes" value="Yes" runat="server" GroupName="A" Text="Public" Checked="true" />
                                </label>
                                <label class="btn btn-primary_radio radio_check col-6 control-label" id="lblRadioPrivate">
                                    <asp:RadioButton ID="rbtnNo" value="No" runat="server" GroupName="A" Text="Private" />
                                </label>
                            </div>
                            <div class="col-lg-12 form-group bottom padding-none float-left" id="divRemarks">
                                <asp:Label ID="lbl_Remarks" runat="server" CssClass="label_name">Comments:<span class="smallred_label">*</span></asp:Label>
                                <textarea ID="txt_Remarks" runat="server" class="form-control" MaxLength="500"></textarea>
                                <asp:HyperLink ID="HyperLink1" runat="server" Visible="false">HyperLink</asp:HyperLink>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer form-group">
                    <button type="button" id="btn_Submit" runat="server" onclick="return RequiredFields();" class=" btn-signup_popup">Submit</button>
                    <button type="button" data-dismiss="modal" runat="server" id="btnclose1" class=" btn-cancel_popup">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!--------End Edit Document Privacy View-------------->
    <!--- document search modal--->
   <%-- <div id="documentlistmodal" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 85%">
            <!-- Modal content-->
            <div class="modal-content">              
                        <div class="modal-header">  Select Result : abc  pqr                    
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div class="col-12 float-left padding-none documentdiv_border">
                              <div class="col-12 float-left ">
                                  <div class="documentnum_div">Document No : <span>123456</span></div>
                                  <div class="documentcontent_div top "><span class="colour_div">Lorem Ipsum </span>Lorem Ipsum Lorem Ipsumv Lorem IpsumLorem Ipsum</div>
                              </div>
                               <div class="col-12 float-left text-right max_text ">Max : 150</div>
                            </div>
                            <div class="col-12 float-left padding-none documentdiv_border top">
                              <div class="col-12 float-left ">
                                  <div class="documentnum_div">Document No : <span>123456</span></div>
                                  <div class="documentcontent_div top "><span class="colour_div">Lorem Ipsum </span>Lorem Ipsum Lorem Ipsumv Lorem IpsumLorem Ipsum</div>
                                  </div>
                                    <div class="col-12 float-left text-right max_text">Max : 150</div>                           
                            </div>
                            <div class="col-12 float-left padding-none documentdiv_border top">
                              <div class="col-12 float-left ">
                                  <div class="documentnum_div">Document No : <span>123456</span></div>
                                  <div class="documentcontent_div top "><span class="colour_div">Lorem Ipsum </span>Lorem Ipsum Lorem Ipsumv Lorem IpsumLorem Ipsum</div>
                                  </div>
                                <div class="col-12 float-left text-right max_text">Max : 150</div>
                            </div>
                            <div class="col-12 float-left padding-none documentdiv_border top">
                              <div class="col-12 float-left ">
                                  <div class="documentnum_div">Document No : <span>123456</span></div>
                                  <div class="documentcontent_div top "><span class="colour_div">Lorem Ipsum </span>Lorem Ipsum Lorem Ipsumv Lorem IpsumLorem Ipsum</div>
                              </div>
                                <div class="col-12 float-left text-right max_text">Max : 150</div>
                            </div>
                            <div class="col-12 float-left text-right padding-none top">
                                     <button type="button" data-dismiss="modal" class=" dms_btn-ok ">Ok</button>
                            </div>
                       </div>
            </div>                   
            </div>
        </div>--%>

     <!--- document search modal--->
    <!--Mani Document Fill View -->
        <div id="MyModelMainDocView" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
            <div id="divModalSize" class="modal-dialog modal-lg" style="min-width:80%;">
                <!-- Modal content-->
                <div class="modal-content padding-none">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <div class="modal-header">
                        <h4 class="modal-title" id="idReportName">Document List Report</h4>
                        <div class=" float-right padding-none">
                         
                            <button type="button" class="close" data-dismiss="modal" >&times;</button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="embed-responsive embed-responsive-16by9" id="dvfileframe">
                        </div>
                    </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>                   
                </div>
            </div>
        </div>
        <!--End-->

    <script>
        $('#dtDocList thead tr').clone(true).appendTo('#dtDocList thead');
        $('#dtDocList thead tr:eq(1) th').each(function (i) {
            if (i < 12) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder=" Search " />');

                $('input', this).on('keyup change', function () {
                    if (oTable.column(i).search() !== this.value) {
                        oTable
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
                if (i == 1) {
                    $(this).html('');
                }
            }
            else {
                $(this).html('');
            }
        });
        $('#dtDocList').wrap('<div class="dataTables_scroll" />');
        var oTable = $('#dtDocList').DataTable({
            columns: [
                { 'data': 'RowNumber' },//0
                { 'data': 'VersionID' },//1
                { 'data': 'vDocumentID' },//2
                { 'data': 'DocumentNumber' },//3
                { 'data': 'DocumentName' },//4
                { 'data': 'VersionNumber' },//5
                { 'data': 'DocumentType' },//6
                { 'data': 'Department' },//7
                { 'data': 'RequestType' },//8
                { 'data': 'EffectiveDate' },//9
                { 'data': 'ExpirationDate' },//10
                { 'data': 'statusMsg' },//11
                { 'data': 'expStatus' },//12
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) {
                        $("#dvDocumentLblName").html(o.DocTitleLablName);
                        return '<a  class="summary_latest" title="History"  data-toggle="modal" data-target="#myCommentHistory"  onclick=ViewCommentHistory(' + o.VersionID + ')></a>';
                    }
                },//13
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="privacy" title="privacy"  data-toggle="modal" data-target="#myDocumentPrivacyEdit"  onclick=EditDocumentPrivacy(' + o.VersionID + ',' + o.vDocumentID + ')></a>'; }
                },//14
                { 'data': 'DocLocationCount' }//15
            ],
            "scrollX": true,
            "responsive": true,
            "paging": true,
            "search": {
                "regex": true
            },
            "aoColumnDefs": [{ "targets": [0,1, 2, 14, 15], "visible": false, "searchable": false }, { "bSortable": false, "aTargets": [0, 12, 13] }, { "className": "dt-body-left", "targets": [3, 4, 6, 7, 8, 11] },
            {
                targets: [12], render: function (a, b, data, d) {
                    if (data.expStatus > 0) {
                        return '<a class="view_Training" title="View Doc"  data-toggle="modal" data-target="#myModal_lock" onclick=ViewDocument1(' + data.VersionID + ')></a>';
                    }
                    else if (data.DocLocationCount != "0" && data.expStatus == 0) {
                        return '<a class="view"  data-toggle="modal" data-target="#myModal_lock"   title="View Doc" onclick=ViewDocument(' + data.VersionID + ')></a>';
                    }
                    else {
                        return "N/A";
                    }

                }
            },
            ],
            "orderCellsTop": true,
            "order": [[1, "desc"]],
            'bAutoWidth': true,
            sServerMethod: 'Post',
            "sAjaxSource": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetAdminDocList" )%>',
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#dtDocList").show();
                        UserSessionCheck();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
    </script>
    <asp:HiddenField ID="hdfVID" runat="server" />
    <asp:HiddenField ID="hdfStatus" runat="server" />
    <asp:HiddenField ID="hdfDocNum" runat="server" />
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
    <asp:HiddenField ID="hdfRole" runat="server" />
    <asp:HiddenField ID="hdnConfirm" runat="server" />
    <asp:HiddenField ID="hdfViewType" runat="server" />
    <asp:HiddenField ID="hdfViewEmpID" runat="server" />
    <asp:HiddenField ID="hdfViewRoleID" runat="server" />
    <asp:HiddenField ID="hdfViewDivID" runat="server" />
    <uc1:ConfirmCustomAlert runat="server" ID="ConfirmCustomAlert" />
    <asp:UpdatePanel ID="UpGridButtons" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnFileView" runat="server" Text="Submit" OnClick="btnFileView_Click" Style="display: none" OnClientClick="showImg();"/>
            <asp:Button ID="btnFileView1" runat="server" Text="Submit" OnClick="btnFileView1_Click" Style="display: none" OnClientClick="showImg();"/>
            <asp:Button ID="btnCommentsHistoryView" runat="server" Text="Submit" Style="display: none" OnClick="btnCommentsHistoryView_Click" OnClientClick="showImg();"/>
            <asp:Button ID="btnEdit" runat="server" Text="Submit" Style="display: none" OnClick="btnEdit_Click" OnClientClick="showImg();"/>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>
        function DoubleClickHandleReport() {
            custAlertMsg("Document Report is downloading please wait,Click OK after download is completed", "info");
        }
        var RoleID = document.getElementById("<%=hdfRole.ClientID%>").value;
        if (RoleID == 2 || RoleID == 30) {
            $("#divMainDocListReport").show();
        } 
        function ViewDocument(V_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = V_ID;
            var btnFV = document.getElementById("<%=btnFileView.ClientID %>");
            btnFV.click();
        }
        function ViewDocument1(V_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = V_ID;
            var btnFV1 = document.getElementById("<%=btnFileView1.ClientID %>");
            btnFV1.click();
        }
        function ViewCommentHistory(PK_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = PK_ID;
            var btnCV = document.getElementById("<%=btnCommentsHistoryView.ClientID %>");
            btnCV.click();
        }
        function EditDocumentPrivacy(PK_ID, DocNum) {

            document.getElementById("<%=hdfVID.ClientID%>").value = PK_ID;
            document.getElementById("<%=hdfDocNum.ClientID%>").value = DocNum;
            var btnDE = document.getElementById("<%=btnEdit.ClientID %>");
            btnDE.click();
        }
    </script>
    <script>
        function ReloadCurrentPage() {
            window.open("<%=ResolveUrl("~/DMS/MainDocumentList.aspx")%>", "_self");
        }
    </script>
    <script type="text/javascript">
             if (document.layers) {
                 //Capture the MouseDown event.
                 document.captureEvents(Event.MOUSEDOWN);
                 //Disable the OnMouseDown event handler.
                 document.onmousedown = function () {
                     return false;
                 };
             }
             else {
                 //Disable the OnMouseUp event handler.
                 document.onmouseup = function (e) {
                     if (e != null && e.type == "mouseup") {
                         //Check the Mouse Button which is clicked.
                         if (e.which == 2 || e.which == 3) {
                             //If the Button is middle or right then disable.
                             return false;
                         }
                     }
                 };
             }
             //Disable the Context Menu event.
             document.oncontextmenu = function () {
                 return false;
             };
    </script>
    <script>
            function ShowRefferal() {
                $('#myModal_Refferal').modal({ show: true, backdrop: 'static', keyboard: false });
            }
    </script>
    <script type="text/javascript">
            function RequiredFields() {
                var rbyes = document.getElementById("<%=rYes.ClientID%>");
                var rbno = document.getElementById("<%=rbtnNo.ClientID%>");
                var Comments = document.getElementById("<%=txt_Remarks.ClientID%>").value;
                //var Comments = $('<%=txt_Remarks.ClientID%>').val().trim();
                errors = [];
                if (rbyes.checked == false && rbno.checked == false) {
                    errors.push("Select a privacy level.");
                }
                if (Comments.trim() == "") {
                    errors.push("Enter Comments.");
                }
                if (errors.length > 0) {
                    custAlertMsg(errors.join("<br/>"), "error");
                    return false;
                }
                else {
                    ConformAlertPrivacy();
                    return true;
                }

            }
    </script>
    <script>
            function getadmincolumn() {
                var RoleID = document.getElementById("<%=hdfRole.ClientID%>").value;
                if (RoleID == "2") {
                    oTable.column(14).visible(true);
                }
                else {
                    oTable.column(14).visible(false);
                }
            }
    </script>
    <script>
            function ConformAlertPrivacy() {
                document.getElementById("<%=hdnConfirm.ClientID%>").value = 'Update';
                custAlertMsg('Are you sure you want to Update the Document Privacy?', 'confirm', true);
            }
    </script>
    <script src='<%= Page.ResolveUrl("~/AppScripts/PDF_Viewer.min.js")%>'></script>
    <script src='<%= Page.ResolveUrl("~/AppScripts/MainDocPDF_Viewer.js")%>'></script>
    <script>
            function disableRadioPublic() {
                $('#lblRadioPublic').removeClass('active');
                $('#lblRadioPrivate').addClass('active');
            }
            function enableRadioPublic() {
                $('#lblRadioPublic').addClass('active');
                $('#lblRadioPrivate').removeClass('active');
            }
            function CloseBrowser() {
                window.close();
            }
            function LoadPDFDocument() {
                PdfViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>', "0", $('#<%=hdfVID.ClientID%>').val(), "0", "11", "#divPDF_Viewer", '');
            }
            function LoadfinalPDFDocument() {
                PdfViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>', "0", $('#<%=hdfVID.ClientID%>').val(), "0", "0", "#divPDF_Viewer", '');
            }
    </script>
    <script>
            function showImg() {
                ShowPleaseWait('show');
            }
            function hideImg() {
                ShowPleaseWait('hide');
            }
        function PrintManiDocumentList()
        {
            var doc = $("#dvDocumentLblName").text();
         $.ajax({  
             type: "POST",  
             url: "WebServices/DMSService.asmx/PrintMainDocList",  
             contentType: "application/json; charset=utf-8",  
             dataType: "json",  
             data: JSON.stringify({ IsJQueryList: "N",ReportName:doc }),
             success: function (result) {
                 var message = JSON.stringify(result.d.ReportPath);
                 var parsePath = JSON.parse(message);
                 //let finalPath = JSON.parse(parsePath);
                 if (parsePath=="NoData")
                 {
                     custAlertMsg("Records Not Found", "error");
                 }
                 else
                 {
                    var dvShow = document.getElementById("dvfileframe");
                    AdminMainDocPrintFilePath = parsePath;//Store File Path
                    PdfMainDocViewerContentLoading(AdminMainDocPrintFilePath, dvShow);
                      $('#idReportName').text(result.d.ReportTitle);
                     $("#MyModelMainDocView").modal("show");
                 }
             },  
             failure: function (response) {  
                 alert(response.d);  
             }  
         }); 
        }
    </script>

</asp:Content>
