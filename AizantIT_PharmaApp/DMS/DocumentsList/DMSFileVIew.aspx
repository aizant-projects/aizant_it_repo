﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DMSFileVIew.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentsList.DMSFileVIew" %>

<%@ Register Assembly="DevExpress.Web.ASPxRichEdit.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRichEdit" TagPrefix="dx" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script lang="JavaScript">
        /**
          * Disable right-click of mouse, F12 key, and save key combinations on page
          * By Arthur Gareginyan (arthurgareginyan@gmail.com)
          * For full source code, visit http://www.mycyberuniverse.com
          */
        window.onload = function () {
            document.addEventListener("contextmenu", function (e) {
                e.preventDefault();
            }, false);
            document.addEventListener("keydown", function (e) {
                //document.onkeydown = function(e) {
                // "I" key
                if (e.ctrlKey && e.shiftKey && e.keyCode == 73) {
                    disabledEvent(e);
                }
                // "J" key
                if (e.ctrlKey && e.shiftKey && e.keyCode == 74) {
                    disabledEvent(e);
                }
                // "S" key + macOS
                if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
                    disabledEvent(e);
                }
                // "U" key
                if (e.ctrlKey && e.keyCode == 85) {
                    disabledEvent(e);
                }
                // "F12" key
                if (event.keyCode == 123) {
                    disabledEvent(e);
                }
            }, false);
            function disabledEvent(e) {
                if (e.stopPropagation) {
                    e.stopPropagation();
                } else if (window.event) {
                    window.event.cancelBubble = true;
                }
                e.preventDefault();
                return false;
            }
        };
    </script>
    <style>
        .dxreView {
            height: 599px !important;
        }

        /*.dxrePage {
            height: 1056px !important;
        }*/
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div id="divliteral" runat="server" visible="false">
                <asp:Literal ID="literalSop1" runat="server"></asp:Literal>
            </div>
            <div id="richedit2" runat="server" visible="false">
                <dx:ASPxRichEdit ID="ASPxRichEdit2"
                    ShowConfirmOnLosingChanges="false" runat="server" Settings-HorizontalRuler-Visibility="Hidden" Settings-DocumentCapabilities-Bookmarks="Disabled"
                    Settings-Bookmarks-Visibility="Hidden" StylesRibbon-GroupPopup-Wrap="False" StylesPopupMenu-Item-Cursor="wait" StylesPopupMenu-SubMenuItem-Wrap="False"
                    ReadOnly="true" WorkDirectory="~\App_Data\WorkDirectory" RibbonMode="None" Width="100%">
                    <Settings>
                        <Behavior Copy="Disabled" Download="Disabled" Cut="Disabled" Paste="Disabled" Printing="Disabled" AcceptsTab="false" Save="Disabled" FullScreen="Enabled" SaveAs="Disabled" CreateNew="Disabled" />
                    </Settings>
                    <SettingsDocumentSelector EditingSettings-AllowCopy="false"></SettingsDocumentSelector>
                </dx:ASPxRichEdit>
            </div>
        </div>
    </form>
</body>
</html>
