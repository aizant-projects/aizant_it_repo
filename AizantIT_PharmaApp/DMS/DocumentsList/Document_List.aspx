﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="Document_List.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentsList.Document_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        table tbody tr th {
            background-color: #07889a;
            color: #fff;
            text-align: center;
        }

        table tbody tr {
            text-align: center;
            height: 30px;
        }

        .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
            background-color: #07889a !important;
        }

        iframe {
            overflow-y: hidden;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="padding-none">
        <asp:Button ID="btnAdd" runat="server" class="float-right  btn-signup_popup" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
    </div>
    <div class=" col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left">
        <div class=" col-md-12 col-lg-12 col-12 col-sm-12 dms_outer_border float-left padding-none">
            <div class="col-sm-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server" visible="false">
                <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
            </div>
            <div id="divMainContainer" runat="server" visible="true">
                <div class=" col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-right ">
                    <div class="col-md-12 col-lg-12 col-12 col-sm-12  padding-none float-left">
                        <div class="grid_header col-md-12 col-lg-12 col-12 col-sm-12 float-left">Effective Document List</div>
                        <div class="col-md-12 col-lg-12 col-12 col-sm-12 grid_panel_full border_top_none bottom float-left">
                           <div class="col-12 float-left padding-none top">
                            <table id="dtDocList" class="display datatable_cust" cellspacing="0" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>VersionID</th>
                                        <th>Document Number</th>
                                        <th>Document Name</th>
                                        <th>Document Version</th>
                                        <th>Document Type</th>
                                        <th>Department</th>
                                        <th>Effective Date</th>
                                        <th>Review Date</th>
                                        <th>Privacy</th>
                                        <th>View Doc</th>
                                        <th>Referrals</th>
                                    </tr>
                                </thead>
                            </table>
                               </div>
                            <div class=" col-lg-12  float-left bottom">
                                <img src="<%=ResolveUrl("~/Images/GridActionImages/view_tms.png")%>" /><b class="grid_icon_legend">Effective Document</b>
                                <%--<img src="<%=ResolveUrl("~/Images/GridActionImages/revert_tms.png")%>" /><b class="grid_icon_legend">Review Date Exceeded</b>--%>
                                <img src="<%=ResolveUrl("~/Images/GridActionImages/refarals1.png")%>" /><b class="grid_icon_legend">referrals</b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-------- File VIEW-------------->
    <div id="myModal_lock" class="modal department fade"  data-backdrop="static" data-keyboard="false" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg" style="min-width: 98%">
            <!-- Modal content-->
            <div class="modal-content col-lg-12 padding-none">
                <div class="modal-header">
                    
                    <asp:UpdatePanel ID="UpHeading" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <span id="span_FileTitle" runat="server"></span>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpViewDL" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="divPDF_Viewer" runat="server">
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class=" btn-cancel_popup">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-------- End File VIEW-------------->

    <!-------- Referral VIEW-------------->
    <div id="myModal_Refferal" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg " style="min-width:70%;">
            <!-- Modal content-->
            <div class="modal-content ">
                <div class="modal-header">
                   
                    <asp:UpdatePanel runat="server" ID="UpReferralHeading" UpdateMode="Conditional">
                        <ContentTemplate>
                            <span id="span1" runat="server"></span>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <ul class="nav nav-tabs tab_grid">
                        <li class="  nav-item"><a class="Referralstab nav-link active" data-toggle="tab" href="#TabPanel1">Link Referrals</a></li>
                        <li class=" nav-item"><a class="internaltab nav-link "  data-toggle="tab" href="#TabPanel2">Internal Document</a></li>
                        <li class=" nav-item"><a class="Externaltab nav-link "  data-toggle="tab" href="#TabPanel3">External Documents</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="TabPanel1" class="Referralstabbody tab-pane fade in active show">
                            <div class="col-sm-12 float-left">
                                <br />
                                <div class="col-sm-12 float-left">
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                        <ContentTemplate>
                                            <asp:Literal ID="txtReferrences" runat="server"></asp:Literal>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                        <div id="TabPanel2" class="internaltabbody tab-pane fade">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <div>
                                        <div class="col-sm-12 padding-none top">
                                            <asp:GridView ID="gvAssignedDocuments" runat="server" class="display bottom" AutoGenerateColumns="false" Width="100%" EmptyDataText="No files uploaded" AllowPaging="true" PageSize="15" OnPageIndexChanging="gvAssignedDocuments_PageIndexChanging" BorderColor="#07889a">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Document Number">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_DocumentNumber" runat="server" Text='<%# Eval("DocumentNumber") %>'></asp:Label>
                                                            <asp:Label ID="lbl_DocumentID" runat="server" Text='<%# Eval("VersionID") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lbl_DeptID" runat="server" Text='<%# Eval("DeptID") %>' Visible="false"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Document Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_DocumentName" runat="server" Text='<%# Eval("DocumentName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="View Doc">
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel ID="jhj" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:LinkButton ID="lnk_ViewInternal" runat="server" class="view" OnClick="lnk_ViewInternal_Click" OnClientClick="showImg();"></asp:LinkButton>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="lnk_ViewInternal" EventName="Click" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div id="TabPanel3" class="Externaltabbody tab-pane fade">
                            <div>
                                <div class="col-sm-12 top bottom">
                                    <div class="col-sm-12 padding-none top">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <asp:GridView ID="gvExternalFiles" runat="server" AutoGenerateColumns="false" EmptyDataText="No files uploaded" Width="100%" BorderColor="#07889a">
                                                    <Columns>
                                                        <asp:BoundField DataField="FileName" HeaderText="File Name" />
                                                        <asp:TemplateField HeaderText="View Doc">
                                                            <ItemTemplate>
                                                                <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton ID="lnkDownload" class="view" CommandArgument='<%# Eval("FileName") +"@$"+Eval("Ref_FileID")+"@$"+Eval("pkid")%>' runat="server" OnClick="lnkDownload_Click" OnClientClick="showImg();"></asp:LinkButton>
                                                                        <asp:Label ID="lbl_isSavedFile" runat="server" Text='<%# Eval("isSavedFile") %>' Visible="false"></asp:Label>
                                                                        <asp:Label ID="lbl_Ref_FileID" runat="server" Text='<%# Eval("Ref_FileID") %>' Visible="false"></asp:Label>
                                                                        <asp:Label ID="lbl_pkid" runat="server" Text='<%# Eval("pkid") %>' Visible="false"></asp:Label>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="lnkDownload" EventName="Click" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-lg-12 top">
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>
                                <button type="button" data-dismiss="modal" runat="server" id="btn_Refferals_Cancel1" class="float-right  btn-cancel_popup">Cancel</button>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-------- End Referral VIEW-------------->

    <!--------Internal Referral VIEW-------------->
    <div id="moppnlInternalView" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg" style="min-width: 75%">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                  
                    <span id="span2" runat="server">Internal Referral</span>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="col-lg-12 modal-body" style="background-color: #fff">
                    <div class="col-lg-12">
                        <div id="InternalRefDiv">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--------End Internal Referral VIEW-------------->

    <!--------External Referral VIEW-------------->
    <div id="mpop_FileViewer" class="modal department fade" role="dialog">
        <div class="modal-dialog modal-lg " style="min-width: 86%">
            <!-- Modal content-->
            <div class="modal-content ">
                <div class="modal-header">
                  
                    <span id="span3" runat="server">External Referral</span>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="col-lg-12 modal-body" style="background-color: #fff">
                    <div class="col-sm-12">
                        <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="ExternalRefDiv" runat="server">
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--------End External Referral VIEW-------------->

    <asp:HiddenField ID="hdnGrdiReferesh" runat="server" />
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
    <asp:HiddenField ID="hdfRole" runat="server" />
    <asp:HiddenField ID="hdfViewType" runat="server" />
    <asp:HiddenField ID="hdfViewEmpID" runat="server" />
    <asp:HiddenField ID="hdfViewRoleID" runat="server" />
    <asp:HiddenField ID="hdfViewDivID" runat="server" />
    <script>
        $('#dtDocList thead tr').clone(true).appendTo('#dtDocList thead');
        $('#dtDocList thead tr:eq(1) th').each(function (i) {
            if (i < 9) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder=" Search " />');

                $('input', this).on('keyup change', function () {
                    if (oTable.column(i).search() !== this.value) {
                        oTable
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
                if (i == 1) {
                    $(this).html('');
                }
            }
            else {
                $(this).html('');
            }
        });
        $('#dtDocList').wrap('<div class="dataTables_scroll" />');
        var oTable = $('#dtDocList').DataTable({
            columns: [
                { 'data': 'RowNumber' },
                { 'data': 'VersionID' },
                { 'data': 'DocumentNumber' },
                { 'data': 'DocumentName' },
                { 'data': 'VersionNumber' },
                { 'data': 'DocumentType' },
                { 'data': 'Department' },
                { 'data': 'EffectiveDate' },
                { 'data': 'ExpirationDate' },
                { 'data': 'IsPublic' },
                { 'data': 'expStatus' },
                { 'data': 'ReferralCount' }
            ],
            "orderCellsTop": true,
            "order": [1, "desc"],
            "aoColumnDefs": [{ "targets": [0,1, 9], "visible": false, "searchable": false }, { "bSortable": false, "aTargets": [0, 1, 10, 11] }, { "className": "dt-body-left", "targets": [2, 3, 5, 6] },
            {
                targets: [10], render: function (a, b, data, d) {

                    if (data.expStatus > 0) {
                        return '<a class="view_Training" data-toggle="modal" data-target="#myModal_lock" title="View Doc" onclick=ViewDocument1(' + data.VersionID + ')></a>';
                    } else {
                        return '<a class="view" data-toggle="modal" data-target="#myModal_lock" title="View Doc" onclick=ViewDocument(' + data.VersionID + ')></a>';
                    }
                    return "N/A";
                }
            },
            {
                targets: [11], render: function (a, b, data, d) {

                    if (data.ReferralCount > 0) {
                        return '<a class="Refferals" data-toggle="modal" data-target="#myModal_Refferal" title="Referrals" onclick=ViewRefferals(' + data.VersionID + ')></a>';
                    } else {
                        return '<a  data-toggle="modal" data-target="#myModal_Refferal" title="Referrals" onclick=ViewRefferals(' + data.VersionID + '))>N/A</a>';
                    }
                    return "N/A";
                }
            },
            ],
            sServerMethod: 'Post',
            "sAjaxSource": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetListOfDocuments" )%>',
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#dtDocList").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        UserSessionCheck();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
    </script>
    <asp:HiddenField ID="hdfVID" runat="server" />
    <asp:HiddenField ID="hdfPKID" runat="server" />
    <asp:HiddenField ID="hdfStatus" runat="server" />
    <asp:HiddenField ID="hdfDocNum" runat="server" />
    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnFileView" runat="server" Text="Submit" OnClick="btnFileView_Click" Style="display: none" OnClientClick="showImg();"/>
            <asp:Button ID="btnFileView1" runat="server" Text="Submit" OnClick="btnFileView1_Click" Style="display: none" OnClientClick="showImg();"/>
            <asp:Button ID="btnRefferalView" runat="server" Text="Submit" OnClick="btnRefferalView_Click" Style="display: none" OnClientClick="showImg();"/>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>
        function ViewDocument(V_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = V_ID;
            var btnFV = document.getElementById("<%=btnFileView.ClientID %>");
            btnFV.click();
        }
        function ViewDocument1(V_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = V_ID;
            var btnFV1 = document.getElementById("<%=btnFileView1.ClientID %>");
            btnFV1.click();
        }
        function ViewRefferals(V_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = V_ID;
            var btnRV = document.getElementById("<%=btnRefferalView.ClientID %>");
            btnRV.click();
        }
    </script>
    <script>
        function ReloadCurrentPage() {
            window.open("<%=ResolveUrl("~/DMS/DocumentsList/Document_List.aspx")%>", "_self");
        }
    </script>
    <script type="text/javascript">
        if (document.layers) {
            //Capture the MouseDown event.
            document.captureEvents(Event.MOUSEDOWN);
            //Disable the OnMouseDown event handler.
            document.onmousedown = function () {
                return false;
            };
        }
        else {
            //Disable the OnMouseUp event handler.
            document.onmouseup = function (e) {
                if (e != null && e.type == "mouseup") {
                    //Check the Mouse Button which is clicked.
                    if (e.which == 2 || e.which == 3) {
                        //If the Button is middle or right then disable.
                        return false;
                    }
                }
            };
        }
        //Disable the Context Menu event.
        document.oncontextmenu = function () {
            return false;
        };
    </script>
    <script>
             function ShowRefferal() {
                 $('#myModal_Refferal').modal({ show: true, backdrop: 'static', keyboard: false });
             }
    </script>
    <script>
            $(document).ready(function () {
                ReferalsTabState();
                TabRefClick();
                // Grab the iframe document              
                $('#readerframe').on('contextmenu', function (e) {
                    e.preventDefault();
                });
            });
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_endRequest(function () {
                ReferalsTabState();
                TabRefClick();
                // Grab the iframe document               
                $('#readerframe').on('contextmenu', function (e) {
                    e.preventDefault();
                });
            });
            function ReferalsTabState() {
                if ($('#<%=hdnGrdiReferesh.ClientID%>').val() == 'Link') {
                    $('.internaltab').removeClass('active');
                    $('.Externaltab').removeClass('active');
                    $('.Referralstab').addClass('active');
                    $('.Externaltabbody').removeClass('active');
                    $('.Externaltabbody').removeClass('show');
                    $('.internaltabbody').removeClass('active');
                    $('.internaltabbody').removeClass('show');
                    $('.Referralstabbody').addClass('active');
                    $('.Referralstabbody').addClass('show');
                }
                if ($('#<%=hdnGrdiReferesh.ClientID%>').val() == 'Internal') {
                    $('.Externaltab').removeClass('active');
                    $('.Referralstab').removeClass('active');
                    $('.internaltab').addClass('active');
                    $('.Externaltabbody').removeClass('active');
                    $('.Externaltabbody').removeClass('show');
                    $('.Referralstabbody').removeClass('active');
                    $('.Referralstabbody').removeClass('show');
                    $('.internaltabbody').addClass('active');
                    $('.internaltabbody').addClass('show');
                }
                if ($('#<%=hdnGrdiReferesh.ClientID%>').val() == 'External') {
                    $('.internaltab').removeClass('active');
                    $('.Referralstab').removeClass('active');
                    $('.Externaltab').addClass('active');
                    $('.internaltabbody').removeClass('active');
                    $('.internaltabbody').removeClass('show');
                    $('.Referralstabbody').removeClass('active');
                    $('.Referralstabbody').removeClass('show');
                    $('.Externaltabbody').addClass('active');
                    $('.Externaltabbody').addClass('show');
                }
            }
    </script>
    <script src='<%= Page.ResolveUrl("~/AppScripts/PDF_Viewer.min.js")%>'></script>
    <script>
            function TabRefClick() {
                $(".Referralstab").click(function () {
                    $('#<%=hdnGrdiReferesh.ClientID%>').val('Link');
                });
                $(".internaltab").click(function () {
                    $('#<%=hdnGrdiReferesh.ClientID%>').val('Internal');
                });
                $(".Externaltab").click(function () {
                    $('#<%=hdnGrdiReferesh.ClientID%>').val('External');
                });
            }
            function CloseBrowser() {
                window.close();
            }
            function ShowInternalFile() {
                $('#moppnlInternalView').modal({ show: true, backdrop: 'static', keyboard: false });
            }
            function ViewPDFDocument(DivID, IRefID) {
                PdfViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>', $('#<%=hdfViewType.ClientID%>').val(), IRefID, $('#<%=hdfViewEmpID.ClientID%>').val(), $('#<%=hdfViewRoleID.ClientID%>').val(), DivID);
            }
    </script>
    <script>
            function showImg() {
                ShowPleaseWait('show');
            }
            function hideImg() {
                ShowPleaseWait('hide');
            }
    </script>
</asp:Content>
