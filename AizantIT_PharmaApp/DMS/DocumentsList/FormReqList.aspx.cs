﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.IO;
using System.Printing;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_BusinessLayer;
using iTextSharp.text.pdf;
using iTextSharp.text;
using DevExpress.Pdf;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Configuration;

namespace AizantIT_PharmaApp.DMS.DocumentsList
{
    public partial class FormReqList : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        DocPrintRequest DocReqObjects = new DocPrintRequest();
        DocObjects docObjects = new DocObjects();
        UMS_BAL objUMS_BAL;
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        hdnWebCnfgFormValue.Value = ConfigurationManager.AppSettings.Get("isDefaultPrinter");
                        hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            Session["FormPrint"] = "0";
                            ViewState["FRCopyNumbers"] = "0";
                            if (dtTemp.Select("RoleID=23").Length > 0 || dtTemp.Select("RoleID=30").Length > 0)//23-Reader,30-DocController
                            {
                                if (Request.QueryString["Notification_ID"] != null)
                                {
                                    objUMS_BAL = new UMS_BAL();
                                    int[] empIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, empIDs); //2 represents NotifyEmp had visited the page through notification link.
                                }
                                hdfRole.Value = "23";
                                if (Request.QueryString["print"] != null)
                                {
                                    Session["FormPrint"] = Request.QueryString["print"].ToString();
                                }
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "change1", "changeHeaderText();", true);
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                    else
                    {
                        ElectronicSign.buttonClick += new EventHandler(ElectronicSign_buttonClick);
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRL_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRL_M1:" + strline + "  " + "Document Load failed.", "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        protected void btnRequestView_Click(object sender, EventArgs e)
        {
            btnRequestView.Enabled = false;
            try
            {
                docObjects.FormReqID = Convert.ToInt32(hdfRID.Value);
                DataSet DMS_DT = DMS_Bal.DMS_GetFormPrintReqDetails(docObjects);
                txt_RequestedBy.Text = DMS_DT.Tables[0].Rows[0]["RequestBy"].ToString();
                txt_ProjectNumber.Text = string.IsNullOrEmpty(DMS_DT.Tables[0].Rows[0]["ProjectNumber"].ToString()) ? "N/A" : DMS_DT.Tables[0].Rows[0]["ProjectNumber"].ToString();
                txt_NoofCopies.Text = DMS_DT.Tables[0].Rows[0]["NooFCopies"].ToString();
                txt_DocumentName.Text = DMS_DT.Tables[0].Rows[0]["FormName"].ToString();
                txt_Purpose.Text = DMS_DT.Tables[0].Rows[0]["RequestPurpose"].ToString();
                txt_ReviewedBy.Text = DMS_DT.Tables[0].Rows[0]["ReviewedBy"].ToString();
                txt_DocController.Text = DMS_DT.Tables[0].Rows[0]["DocController"].ToString();
                txt_FormNumber.Text = DMS_DT.Tables[0].Rows[0]["FormNumber"].ToString();
                txt_VersionNumber.Text = DMS_DT.Tables[0].Rows[0]["FormVersionNumber"].ToString();
                UpGridButtons.Update();
                span_FileTitle.InnerText = "Request Number-" + DMS_DT.Tables[0].Rows[0]["RequestNo"].ToString();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_request').modal({ show: true, backdrop: 'static', keyboard: false });", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRL_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRL_M2:" + strline + "  " + "Request Details viewing failed.", "error");
            }
            finally
            {
                btnRequestView.Enabled = true;
            }
        }
        protected void btnCommentsHistoryView_Click(object sender, EventArgs e)
        {
            btnCommentsHistoryView.Enabled = false;
            try
            {
                docObjects.FormReqID = Convert.ToInt32(hdfRID.Value);
                DataTable DMS_DT = DMS_Bal.DMS_GetFormPrintReqHistory(docObjects);
                gv_CommentHistory.DataSource = DMS_DT;
                ViewState["HistoryDataTable"] = DMS_DT;
                gv_CommentHistory.DataBind();
                UpGridButtons.Update();
                span4.InnerHtml = DMS_DT.Rows[0]["FormReqTitle"].ToString();
                upcomment.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myCommentHistory').modal({ show: true, backdrop: 'static', keyboard: false });", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRL_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRL_M3:" + strline + "  " + "Form Request History loading failed.", "error");
            }
            finally
            {
                btnCommentsHistoryView.Enabled = true;
            }
        }
        protected void btnPrintDocument_Click(object sender, EventArgs e)
        {
            btnPrintDocument.Enabled = false;
            try
            {
                System.Drawing.Printing.PrinterSettings printerSettings1 = new System.Drawing.Printing.PrinterSettings();
                // string DefaultPrinter = printerSettings1.PrinterName = LocalPrintServer.GetDefaultPrintQueue().FullName;
                string DefaultPrinter = string.Empty,
                    isDefaultPrinter = hdnWebCnfgFormValue.Value;
                // ConfigurationManager.AppSettings.Get("isDefaultPrinter");
                List<string> ListofInstalledPrinters = new List<string>();
                String pkInstalledPrinters;
                for (int i = 0; i < System.Drawing.Printing.PrinterSettings.InstalledPrinters.Count; i++)
                {
                    pkInstalledPrinters = System.Drawing.Printing.PrinterSettings.InstalledPrinters[i];
                    ListofInstalledPrinters.Add(pkInstalledPrinters);
                }
                if (isDefaultPrinter == "1")
                {
                    DefaultPrinter = printerSettings1.PrinterName = LocalPrintServer.GetDefaultPrintQueue().FullName;
                }
                else if (isDefaultPrinter == "0")
                {
                    DefaultPrinter = printerSettings1.PrinterName = hdnFormPrinterName.Value;//get Printer Name from client side

                }
                if (!ListofInstalledPrinters.Contains(DefaultPrinter) || DefaultPrinter == "Microsoft Print to PDF" || DefaultPrinter == "Microsoft XPS Document Writer" || DefaultPrinter.Contains("Send To OneNote"))
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Sorry ! There is no Printer configured to print.", "error");
                    return;
                }
                else
                {
                    docObjects.FormReqID = Convert.ToInt32(hdfRID.Value);
                    DataSet dt1 = DMS_Bal.DMS_GetFormPrintReqDetails(docObjects);
                    docObjects.FormVersionID = Convert.ToInt32(hdfVID.Value);
                    DataTable data1 = DMS_Bal.DMS_GetFormDetails(docObjects);
                    DataTable DMS_DT = DMS_Bal.DMS_GetFormContent(docObjects);
                    for (int i = Convert.ToInt32(dt1.Tables[0].Rows[0]["printStartSerialNo"]); i <= Convert.ToInt32(dt1.Tables[0].Rows[0]["printEndSerialNo"]); i++)
                    {
                        printDocument(dt1, data1, i, printerSettings1,DefaultPrinter);
                    }
                    DocReqObjects.DocReqID = Convert.ToInt32(hdfRID.Value);
                    DocReqObjects.RequestedByID = Convert.ToInt32(hdnEmpID.Value);
                    int DMS_AP = DMS_BalDM.DMS_FormPrintReqByRequestor(DocReqObjects);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Form Request Number <b>" + dt1.Tables[0].Rows[0]["RequestNo"].ToString() + "</b> Printed Successfully.", "success", "reloadtable();");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRL_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRL_M4:" + strline + "  " + "Form Request Print failed.", "error");
            }
            finally
            {
                btnPrintDocument.Enabled = true;
            }
        }
        protected void gv_CommentHistory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string _fullcomments = (e.Row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox).Text;
                    LinkButton _lnkmorecomments = (e.Row.Cells[3].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (e.Row.Cells[3].FindControl("lbcommentsless") as LinkButton);
                    if (_fullcomments.Length > 70)
                    {
                        (e.Row.Cells[3].FindControl("lbl_shortComments") as Label).Visible = true;
                        System.Web.UI.WebControls.TextBox FullTbx = (e.Row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox);
                        (e.Row.Cells[3].FindControl("lbl_shortComments") as Label).Text = _fullcomments.Substring(0, 70) + "..";
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = true;

                        if (_fullcomments.Length > 100 && _fullcomments.Length <= 200)
                        {
                            FullTbx.Rows = 3;
                        }
                        else if (_fullcomments.Length > 200 && _fullcomments.Length <= 300)
                        {
                            FullTbx.Rows = 4;
                        }

                        FullTbx.Visible = false;
                    }
                    else
                    {
                        (e.Row.Cells[3].FindControl("lbl_shortComments") as Label).Visible = true;
                        (e.Row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox).Visible = false;
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRL_M5:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRL_M5:" + strline + "  " + "Comment History row databound failed.", "error");
            }
        }
        protected void gv_CommentHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "SM")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    LinkButton _lnkmorecomments = (row.Cells[3].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (row.Cells[3].FindControl("lbcommentsless") as LinkButton);
                    (row.Cells[3].FindControl("lbl_shortComments") as Label).Visible = false;
                    (row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox).Visible = true;
                    _lnklesscomments.Visible = true;
                    _lnkmorecomments.Visible = false;
                }
                else if (e.CommandName == "SL")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    LinkButton _lnkmorecomments = (row.Cells[3].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (row.Cells[3].FindControl("lbcommentsless") as LinkButton);
                    (row.Cells[3].FindControl("lbl_shortComments") as Label).Visible = true;
                    (row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox).Visible = false;
                    _lnklesscomments.Visible = false;
                    _lnkmorecomments.Visible = true;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRL_M6:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRL_M6:" + strline + "  " + "Comment History row command failed.", "error");
            }
        }
        protected void btnEditFormReturn_Click(object sender, EventArgs e)
        {
            btnEditFormReturn.Enabled = false;
            try
            {
                docObjects.FormReqID = Convert.ToInt32(hdfRID.Value);
                DataSet DMS_DT = DMS_Bal.DMS_GetFormPrintReqDetails(docObjects);
                txt_RequestedByReturn.Text = DMS_DT.Tables[0].Rows[0]["RequestBy"].ToString();
                txt_ProjectNumberReturn.Text = string.IsNullOrEmpty(DMS_DT.Tables[0].Rows[0]["ProjectNumber"].ToString()) ? "N/A" : DMS_DT.Tables[0].Rows[0]["ProjectNumber"].ToString();
                txt_NoofCopiesReturn.Text = DMS_DT.Tables[0].Rows[0]["NooFCopies"].ToString();
                txt_DocumentNameReturn.Text = DMS_DT.Tables[0].Rows[0]["FormName"].ToString();
                txt_PurposeReturn.Text = DMS_DT.Tables[0].Rows[0]["RequestPurpose"].ToString();
                txt_ReviewedByReturn.Text = DMS_DT.Tables[0].Rows[0]["ReviewedBy"].ToString();
                txt_DocControllerReturn.Text = DMS_DT.Tables[0].Rows[0]["DocController"].ToString();
                txt_FormNumberReturn.Text= DMS_DT.Tables[0].Rows[0]["FormNumber"].ToString();
                txt_VersionNumberReturn.Text = DMS_DT.Tables[0].Rows[0]["FormVersionNumber"].ToString();
                span2.InnerText = "Request Number-"+DMS_DT.Tables[0].Rows[0]["RequestNo"].ToString();
                ArrayList aryCopyNumbers=new ArrayList();
                int CopyCount = DMS_DT.Tables[1].Rows.Count;
                if (CopyCount > 0)
                {
                    foreach (DataRow dr in DMS_DT.Tables[1].Rows)
                    {
                        aryCopyNumbers.Add(dr[0]);
                    }
                    hdfPrintCopyCount.Value = string.Join(",", aryCopyNumbers.ToArray());
                }
                txt_CopyNumbers.Value = "";
                txt_NoofUsedCopies.Text = "";
                txt_NoofReturnedCopies.Text = "";
                txt_Remarks.Value = "";
                UpdatePanel2.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myFormReturnEdit').modal({ show: true, backdrop: 'static', keyboard: false });", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRL_M7:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRL_M7:" + strline + "  " + "Form Return Details loading failed.", "error");
            }
            finally
            {
                btnEditFormReturn.Enabled = true;
            }
        }
        public void printDocument(DataSet dt, DataTable data, int copyNo, System.Drawing.Printing.PrinterSettings printerSettings,string DefaultPrinter)
        {
            try
            {
                docObjects.Version = Convert.ToInt32(hdfVID.Value);
                docObjects.RoleID = 0;
                docObjects.EmpID = 0;
                docObjects.FileType1 = ".pdf";
                docObjects.Mode = 0;
                System.Data.DataTable DMS_DT = DMS_Bal.DMS_GetFormLocationList(docObjects);
                string pathin = Server.MapPath(@DMS_DT.Rows[0]["FilePath"].ToString());
                string pathout = DynamicFolder.CreateDynamicFolder(6) + "\\" + DateTime.Now.ToString("ddMMyyHHmmssfff") + ".pdf";
                string imgpathround = Server.MapPath("~/Images/GridActionImages/roundstamp.png");
                string imgpathrect = Server.MapPath("~/Images/GridActionImages/stamps_qa_rect.png");
                PdfReader reader = new PdfReader(pathin);
                PdfStamper stamper = new PdfStamper(reader, new FileStream(pathout, FileMode.Create));
                int pages = reader.NumberOfPages;

                PdfContentByte underContent1 = stamper.GetOverContent(1);

                iTextSharp.text.Image imageround = iTextSharp.text.Image.GetInstance(imgpathround);
                imageround.Alignment = iTextSharp.text.Image.ALIGN_RIGHT;
                imageround.SetAbsolutePosition(495, 2);
                imageround.ScaleToFit(90, 90);
                underContent1.AddImage(imageround);

                PdfPTable Footertab1 = new PdfPTable(2);
                Footertab1.TotalWidth = 430F;
                //row 1
                string ReqNum = "Request Number: " + dt.Tables[0].Rows[0]["RequestNo"].ToString();
                PdfPCell RequestNumber = new PdfPCell(new Phrase(ReqNum,
                               new Font(Font.FontFamily.TIMES_ROMAN, 10F)));
                Footertab1.AddCell(RequestNumber);
                string CopyNum = "Copy Number: " + "0000" + copyNo.ToString();
                PdfPCell CopyNumber = new PdfPCell(new Phrase(CopyNum,
                               new Font(Font.FontFamily.TIMES_ROMAN, 10F)));
                Footertab1.AddCell(CopyNumber);
                //row 2
                string PrintBy = "Printed By: " + dt.Tables[0].Rows[0]["DocController"].ToString();
                PdfPCell PrintedBy = new PdfPCell(new Phrase(PrintBy,
                               new Font(Font.FontFamily.TIMES_ROMAN, 10F)));
                Footertab1.AddCell(PrintedBy);
                string PrintDate = "Printed Date: " + DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
                PdfPCell PrintedDate = new PdfPCell(new Phrase(PrintDate,
                               new Font(Font.FontFamily.TIMES_ROMAN, 10F)));
                Footertab1.AddCell(PrintedDate);
                iTextSharp.text.Document document1 = underContent1.PdfDocument;
                Footertab1.WriteSelectedRows(0, -1, 50f, document1.Bottom + 10f, underContent1);
                for (int i = 2; i <= pages; i++)
                {
                    PdfContentByte underContent = stamper.GetOverContent(i);

                    iTextSharp.text.Image imagerect = iTextSharp.text.Image.GetInstance(imgpathrect);
                    imagerect.Alignment = iTextSharp.text.Image.ALIGN_RIGHT;
                    imagerect.SetAbsolutePosition(495, 15);
                    imagerect.ScaleToFit(90, 90);
                    underContent.AddImage(imagerect);

                    PdfPTable Footertab = new PdfPTable(2);
                    Footertab.TotalWidth = 430F;
                    //row 1
                    //string ReqNum = "Request Number: " + dt.Rows[0]["RequestNo"].ToString();
                    //PdfPCell RequestNumber = new PdfPCell(new Phrase(ReqNum,
                    //               new Font(Font.FontFamily.TIMES_ROMAN, 10F)));
                    Footertab.AddCell(RequestNumber);
                    //string CopyNum = "Copy Number: " + "0000" + copyNo.ToString();
                    //PdfPCell CopyNumber = new PdfPCell(new Phrase(CopyNum,
                    //               new Font(Font.FontFamily.TIMES_ROMAN, 10F)));
                    Footertab.AddCell(CopyNumber);
                    //row 2
                    //string PrintBy = "Printed By: " + dt.Rows[0]["RequestBy"].ToString();
                    //PdfPCell PrintedBy = new PdfPCell(new Phrase(PrintBy,
                    //               new Font(Font.FontFamily.TIMES_ROMAN, 10F)));
                    Footertab.AddCell(PrintedBy);
                    //string PrintDate = "Printed Date: " + DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
                    //PdfPCell PrintedDate = new PdfPCell(new Phrase(PrintDate,
                    //               new Font(Font.FontFamily.TIMES_ROMAN, 10F)));
                    Footertab.AddCell(PrintedDate);
                    iTextSharp.text.Document document = underContent.PdfDocument;
                    Footertab.WriteSelectedRows(0, -1, 50f, document.Bottom + 10f, underContent);
                }
                stamper.Close();
                reader.Close();
                // Create a Pdf Document Processor instance and load a PDF into it. 
                PdfDocumentProcessor documentProcessor = new PdfDocumentProcessor();
                documentProcessor.LoadDocument(pathout);

                //Declare the PDF printer settings.
                PdfPrinterSettings pdfPrinterSettings = new PdfPrinterSettings();

                //Specify the PDF printer settings.
                pdfPrinterSettings.Settings.Copies = 1;// Convert.ToSByte(dt.Rows[0]["NooFCopies"].ToString());
                pdfPrinterSettings.Settings.Duplex = System.Drawing.Printing.Duplex.Simplex;
                pdfPrinterSettings.Settings.PrinterName = DefaultPrinter;
                //Setting the PdfPrintScaleMode property to CustomScale requires
                //specifying the Scale property, as well.
                pdfPrinterSettings.ScaleMode = PdfPrintScaleMode.Fit;

                //Print the document using the specified printer settings. 
                documentProcessor.Print(pdfPrinterSettings);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void txt_NoofUsedCopies_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txt_NoofUsedCopies.Text != "")
                {
                    txt_NoofReturnedCopies.Text = (Convert.ToInt32(txt_NoofCopiesReturn.Text) - Convert.ToInt32(txt_NoofUsedCopies.Text)).ToString();
                    if (Convert.ToInt32(txt_NoofReturnedCopies.Text) < 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('Used copies must not be greater than copies issued.','warning');", true);
                        txt_NoofReturnedCopies.Text = "";
                        txt_NoofUsedCopies.Text = "";
                    }
                    UpdatePanel2.Update();
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRL_M10:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRL_M10:" + strline + "  " + "Form Request Returned Copies loading failed.", "error");
            }
        }
        protected void ElectronicSign_buttonClick(object sender, EventArgs e)
        {
            try
            {
                bool b = ElectronicSign.IsPasswordValid;
                if (b == true)
                {
                    if (hdnAction.Value == "Print")
                    {
                        //Print();
                    }
                    else if (hdnAction.Value == "Return")
                    {
                        Return();
                    }
                }
                if (b == false)
                {
                    HelpClass.custAlertMsg(this, this.GetType(), "Password is Incorrect!", "error");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRL_M11:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRL_M11:" + strline + "  " + "Electronic Signature validation failed.", "error");
            }
        }
        public void Return()
        {
            try
            {
                StringBuilder sbErrorMsg = new StringBuilder();
                if (txt_NoofUsedCopies.Text.Trim() == "")
                {
                    sbErrorMsg.Append("Please Enter number of used copies." + "<br/>");
                }
                else if (Convert.ToInt32(txt_NoofUsedCopies.Text.Trim()) > Convert.ToInt32(txt_NoofCopiesReturn.Text.Trim()))
                {
                    sbErrorMsg.Append("number of used copies must not be greater than number of copies issued." + "<br/>");
                }
                else if (Convert.ToInt32(txt_NoofUsedCopies.Text.Trim()) < 0)
                {
                    sbErrorMsg.Append("number of used copies must not be less than zero." + "<br/>");
                }
                //if ((Convert.ToInt32(txt_NoofUsedCopies.Text.Trim()) != Convert.ToInt32(txt_NoofCopiesReturn.Text.Trim())))
                //{
                //    if (txt_CopyNumbers.Value.Trim() == "")
                //    {
                //        sbErrorMsg.Append("Please Enter copy numbers." + "<br/>");                   
                //    }
                //    else
                //    {
                //        if (ViewState["FRCopyNumbers"] != null)
                //        {
                //            DataTable dtPrintedCopyNum = (DataTable)ViewState["FRCopyNumbers"];
                //            List<int> ReturnedCopyNums = txt_CopyNumbers.Value.Trim().Split(',').Select(Int32.Parse).ToList();
                //            bool IsCopyNumExists = true;
                //            if (ReturnedCopyNums.Count == Convert.ToInt32(txt_NoofReturnedCopies.Text))
                //            {
                //                foreach (var item in ReturnedCopyNums)
                //                {
                //                    if (dtPrintedCopyNum.Select("Number=" + item).Length == 0)
                //                    {
                //                        IsCopyNumExists = false;
                //                        break;
                //                    }
                //                }
                //                if (!IsCopyNumExists)
                //                {
                //                    sbErrorMsg.Append("Enter valid Copy Number/s" + "<br/>");
                //                }
                //            }
                //            else
                //            {
                //                sbErrorMsg.Append("UnusedCopyNumbers and NoofCopiesReturned count is not same" + "<br/>");
                //            }
                //        }
                //    }
                //}
                if (txt_Remarks.Value.Trim() == "")
                {
                    sbErrorMsg.Append("Please Enter remarks." + "<br/>");
                }
                
                if (sbErrorMsg.Length > 8)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg('" + sbErrorMsg.ToString() + "','warning');", true);
                    return;
                }
                else
                {
                    docObjects.FormReqID = Convert.ToInt32(hdfRID.Value);
                    DataSet DMS_DT = DMS_Bal.DMS_GetFormPrintReqDetails(docObjects);
                    docObjects.Remarks = Regex.Replace(txt_Remarks.Value.Trim(), @"\s+", " ");
                    docObjects.NoofCopies = Convert.ToInt32(txt_NoofReturnedCopies.Text.Trim());
                    docObjects.CopyNumbers = Regex.Replace(txt_CopyNumbers.Value.Trim(), @"\s+", " ");
                    docObjects.DMSRequestByID = Convert.ToInt32(hdnEmpID.Value);
                    int DMS_ins = DMS_Bal.DMS_SubmitFormReturn(docObjects);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "HidePopup();", true);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Form with Form Request Number <b>" + DMS_DT.Tables[0].Rows[0]["RequestNo"].ToString() + "</b> has been Returned Successfully.", "success", "reloadreturntable()");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRL_M13:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRL_M13:" + strline + "  " + "Form Request Return failed.", "error");
            }
        }
        protected void btnFormView_Click(object sender, EventArgs e)
        {
            btnFormView.Enabled = false;
            try
            {
                DocObjects docObjects1 = new DocObjects();
                docObjects1.FormVersionID = Convert.ToInt32(hdfVID.Value);
                hdfViewType.Value = "3";
                hdfViewEmpID.Value = "0";
                hdfViewRoleID.Value = "0";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view1", "ViewPDFForm();", true);

                DataTable DMS_DT = DMS_Bal.DMS_GetFormHistory(docObjects1);
                span1.Attributes.Add("title", DMS_DT.Rows[0]["FormTitle"].ToString());
                if (DMS_DT.Rows[0]["FormTitle"].ToString().Length > 60)
                {
                    span1.InnerHtml = DMS_DT.Rows[0]["FormTitle"].ToString().Substring(0, 60) + "...";
                }
                else
                {
                    span1.InnerHtml = DMS_DT.Rows[0]["FormTitle"].ToString();
                }
                UpHeading.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRL_M14:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRL_M14:" + strline + "  " + "Form Request Return failed.", "error");
            }
            finally
            {
                btnFormView.Enabled = true;
            }
        }
        protected void gv_CommentHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_CommentHistory.PageIndex = e.NewPageIndex;
                gv_CommentHistory.DataSource = (DataTable)(ViewState["HistoryDataTable"]);
                gv_CommentHistory.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FRL_M15:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FRL_M15:" + strline + "  " + "grid view page index changing failed.", "error");
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }      
    }
}