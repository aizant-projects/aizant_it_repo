﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using DevExpress.XtraRichEdit;
using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;
using iTextSharp.text.pdf;
using System.Text.RegularExpressions;

namespace AizantIT_PharmaApp.DMS.DocumentsList
{
    public partial class DMSWordReaderView : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocObjects docObjects = new DocObjects();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        if (Request.QueryString.Count > 0)
                        {
                            string vid = Request.QueryString[0];
                            hdfVID.Value = vid;
                            string status = Request.QueryString[1];
                            if (Request.QueryString["reader"] != null)
                            {
                                if(Request.QueryString["reader"] == "y")
                                {
                                    Btn_Request.Visible = true;
                                }                             
                                hdfEmpName.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString();
                                msgSuccess.InnerHtml = "New Document Request has been Initiated Successfully.";
                            }
                            ToViewSopDocument(vid, status);
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Uclose", "CloseBrowser();", true);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DWRV_M1:" + strline + "  " + strMsg);
                string strError = "DWRV_M1:" + strline + "  " + "Page loading failed.";
                divliteral.Visible = true;
                richedit2.Visible = false;
                divliteral.InnerText = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        private void ToViewSopDocument(string vid, string status)
        {
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(vid);
                hdfDocNum.Value = DMS_DT.Rows[0]["DocumentNumber"].ToString();
                hdfDeptID.Value = DMS_DT.Rows[0]["DeptID"].ToString();               
                DMS_GetCopyType();
                divliteral.Visible = false;
                richedit2.Visible = true;
                if (DMS_DT.Rows[0]["FileName"].ToString().Length > 0)
                {
                    docObjects.Version = Convert.ToInt32(hdfVID.Value);
                    docObjects.RoleID = 0;
                    docObjects.EmpID = 0;
                    docObjects.FileType1 = ".pdf";
                    docObjects.Mode = 0;
                    System.Data.DataTable dt = DMS_Bal.DMS_GetDocumentLocationList(docObjects);
                    hdfViewType.Value = "0";
                    hdfViewEmpID.Value = "0";
                    hdfViewRoleID.Value = "0";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view1", "ViewDocument('#divPDF_Viewer');", true);
                    string pathin = Server.MapPath(dt.Rows[0]["FilePath"].ToString());
                    PdfReader reader = new PdfReader(pathin);
                    hdfPageCount.Value = Convert.ToString(reader.NumberOfPages - 1);
                }
            }
            catch (Exception)
            {
                divliteral.Visible = true;
                richedit2.Visible = false;
                literalSop1.Text = "File Not Available To View, Bad data.";
            }
        }
        protected void Btn_Request_Click(object sender, EventArgs e)
        {
            Btn_Request.Enabled = false;
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfVID.Value);
                DataTable dt = DMS_Bal.DMS_LatestActiveDoc(Convert.ToInt32(hdfVID.Value));
                DMS_GetReviewerList();
                if (hdfVID.Value == dt.Rows[0]["VersionID"].ToString() && DMS_DT.Rows[0]["expStatus"].ToString() != "1")
                {
                    txt_DocumentName.Text = DMS_DT.Rows[0]["DocumentName"].ToString();
                    //span_FileTitle.InnerHtml = hdfDocNum.Value;
                    txt_RequestedBy.Text = hdfEmpName.Value;
                    lblPageNumbers.Text = "Total No.of Pages : " + hdfPageCount.Value;
                    txt_DocumentNumber.Text= DMS_DT.Rows[0]["DocumentNumber"].ToString();
                    txt_VersionNumber.Text= DMS_DT.Rows[0]["VersionNumber"].ToString();
                    DMS_Reset();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_request').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                    hdfViewType.Value = "0";
                    hdfViewEmpID.Value = "0";
                    hdfViewRoleID.Value = "0";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view2", "ViewDocument('#divPDF_Viewer');", true);
                }
                else if(hdfVID.Value != dt.Rows[0]["VersionID"].ToString())
                {
                    msgError.InnerHtml = "The document with this version is not being used anymore";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#ModalError').modal({ backdrop: 'static', keyboard: false });", true);
                    hdfViewType.Value = "0";
                    hdfViewEmpID.Value = "0";
                    hdfViewRoleID.Value = "0";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view2", "ViewDocument('#divPDF_Viewer');", true);
                    upError.Update();
                    return;
                }
                else if (DMS_DT.Rows[0]["expStatus"].ToString() == "1")
                {
                    msgError.InnerHtml = "The document with this version is overdue";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#ModalError').modal({ backdrop: 'static', keyboard: false });", true);
                    hdfViewType.Value = "0";
                    hdfViewEmpID.Value = "0";
                    hdfViewRoleID.Value = "0";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view2", "ViewDocument('#divPDF_Viewer');", true);
                    upError.Update();
                    return;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DWRV_M2:" + strline + "  " + strMsg);
                string strError = "DWRV_M2:" + strline + "  " + "Request pop-up loading failed.";
                divliteral.Visible = true;
                richedit2.Visible = false;
                divliteral.InnerText = @" <b style=""color:red;"">" + strError + "</b> ";
            }
            finally
            {
                Btn_Request.Enabled = true;
            }
        }
        public void DMS_GetReviewerList()
        {
            try
            {
                UMS_BAL objUMS_Bal = new UMS_BAL();
                DataTable DMS_Dt = objUMS_Bal.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(hdfDeptID.Value), (int)DMS_UserRole.PrintReviewer_DMS, true);
                if (DMS_Dt.Rows.Count > 0)
                {
                    DataRow[] filtered = DMS_Dt.Select("EmpID NOT IN ('" + (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString() + "')");
                    if (filtered.Any())
                    {
                        DMS_Dt = filtered.CopyToDataTable();
                        ddl_ReviewedBy.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                        ddl_ReviewedBy.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                        ddl_ReviewedBy.DataSource = DMS_Dt;
                        ddl_ReviewedBy.DataBind();
                        ddl_ReviewedBy.Items.Insert(0, new System.Web.UI.WebControls.ListItem("-- Select Request Reviewer --", "0"));
                    }
                }
                else
                {
                    //literalSop1.Text = "There are no employees with print reviewer role.";
                    msgError.InnerHtml = "There are no employees with print reviewer role for the document department.";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#ModalError').modal({ backdrop: 'static', keyboard: false });", true);
                    upError.Update();
                    //divliteral.Visible = true;
                    //richedit2.Visible = false;
                    //divliteral.InnerText = "There are no employees with print reviewer role.";
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DWRV_M3:" + strline + "  " + strMsg);
                string strError = "DWRV_M3:" + strline + "  " + "Reviewer drop-down loading failed.";
                divliteral.Visible = true;
                richedit2.Visible = false;
                divliteral.InnerText = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        public void DMS_GetControllerList()
        {
            try
            {
                DataTable DMS_Dt = DMS_Bal.DMS_GetControllersList();
                if (DMS_Dt.Rows.Count > 0)
                {
                    DataRow[] filtered = DMS_Dt.Select("EmpID NOT IN ('" + (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString() + "','" + ddl_ReviewedBy.SelectedValue + "')");
                    if (filtered.Any())
                    {
                        DMS_Dt = filtered.CopyToDataTable();
                        ddl_DocController.DataValueField = DMS_Dt.Columns["EmpID"].ToString();
                        ddl_DocController.DataTextField = DMS_Dt.Columns["EmpName"].ToString();
                        ddl_DocController.DataSource = DMS_Dt;
                        ddl_DocController.DataBind();
                        ddl_DocController.Items.Insert(0, new System.Web.UI.WebControls.ListItem("-- Select Document Controller --", "0"));
                    }
                }
                else
                {
                    msgError.InnerHtml = "There are no employees with controller role.";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#ModalError').modal({ backdrop: 'static', keyboard: false });", true);
                    upError.Update();
                    //literalSop1.Text = "There are no employees with controller role.";
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DWRV_M4:" + strline + "  " + strMsg);
                string strError = "DWRV_M4:" + strline + "  " + "Controller drop-down loading failed.";
                divliteral.Visible = true;
                richedit2.Visible = false;
                divliteral.InnerText = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        public void DMS_GetCopyType()
        {
            try
            {
                DataTable DMS_Dt = DMS_Bal.DMS_GetCopyType();
                ddl_CopyType.DataValueField = DMS_Dt.Columns["DistributionCopyTypeID"].ToString();
                ddl_CopyType.DataTextField = DMS_Dt.Columns["DistributionCopyTypeName"].ToString();
                ddl_CopyType.DataSource = DMS_Dt;
                ddl_CopyType.DataBind();
                ddl_CopyType.Items.Insert(0, new System.Web.UI.WebControls.ListItem("-- Select Type of Copy --", "0"));
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DWRV_M4:" + strline + "  " + strMsg);
                string strError = "DWRV_M4:" + strline + "  " + "Controller drop-down loading failed.";
                divliteral.Visible = true;
                richedit2.Visible = false;
                divliteral.InnerText = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        protected void btn_Reset_Click(object sender, EventArgs e)
        {
            btn_Reset.Enabled = false;
            try
            {
                DMS_Reset();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "select1", "$('.selectpicker1').selectpicker();", true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                btn_Reset.Enabled = true;
            }
        }
        public void DMS_Reset()
        {
            try
            {
                txt_PageNo.Text = "";
                ddl_DocController.Enabled = false;
                ddl_DocController.SelectedIndex = -1;
                ddl_ReviewedBy.SelectedIndex = -1;
                txt_Purpose.Text = "";
                RadioButtonList1.SelectedValue = "no";
                ddl_CopyType.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DWRV_M5:" + strline + "  " + strMsg);
                string strError = "DWRV_M5:" + strline + "  " + "Document Request Reset failed.";
                divliteral.Visible = true;
                richedit2.Visible = false;
                divliteral.InnerText = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            btn_Submit.Enabled = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "select1", "$('.selectpicker1').selectpicker();", true);
            try
            {
                StringBuilder sbErrorMsg = new StringBuilder();
                sbErrorMsg.Clear();    
                if (txt_Purpose.Text.Trim() == "")
                {
                    sbErrorMsg.Append(" Enter Purpose." + "<br/>");
                }
                if (ddl_ReviewedBy.SelectedValue.ToString().Trim() == "0")
                {
                    sbErrorMsg.Append(" Select Request Reviewer." + "<br/>");
                }
                if (ddl_DocController.SelectedValue.ToString().Trim() == "0" || ddl_DocController.SelectedValue.ToString().Trim() == "")
                {
                    sbErrorMsg.Append(" Select Document Controller." + "<br/>");
                }
                if (ddl_CopyType.SelectedValue.ToString().Trim() == "0")
                {
                    sbErrorMsg.Append(" Select Distribution Copy Type." + "<br/>");
                }
                if (txt_PageNo.Text != "")
                {
                    string isPageNoValid = validatePageNumbers();
                    if (isPageNoValid == "1")
                    {
                        sbErrorMsg.Append(" Starting and ending letters should not be , or - ." + "<br/>");
                        //custAlertMsg("Starting and ending letters should not be , or - .", "error");
                        //return false;
                    }
                    else if (isPageNoValid == "2")
                    {
                        sbErrorMsg.Append(" Requested No.of Pages Exceed Available No.of Pages." + "<br/>");
                        //custAlertMsg("Requested No.of Pages Exceed Available No.of Pages.", "error");
                        //return false;
                    }
                    else if (isPageNoValid == "3")
                    {
                        sbErrorMsg.Append(" Special characters , and - should not be combined." + "<br/>");
                        //custAlertMsg("Special characters , and - should not be combined.", "error");
                        //return false;
                    }
                }
                if (sbErrorMsg.Length > 8)
                {
                    msgError.InnerHtml = sbErrorMsg.ToString();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#ModalError').modal({ backdrop: 'static', keyboard: false });", true);
                    upError.Update();
                    return;
                }
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfVID.Value);
                DataTable dt = DMS_Bal.DMS_LatestActiveDoc(Convert.ToInt32(hdfVID.Value));
                if (hdfVID.Value == dt.Rows[0]["VersionID"].ToString() && DMS_DT.Rows[0]["expStatus"].ToString() != "1")
                {
                    docObjects.pkid = Convert.ToInt32(hdfVID.Value);
                    docObjects.Purpose = Regex.Replace(txt_Purpose.Text.Trim(), @"\s+", " ");
                    docObjects.Pageno = Regex.Replace(txt_PageNo.Text.Trim(), @"\s+", " ");
                    docObjects.DMSRequestByID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    docObjects.DMSReviewedByID = Convert.ToInt32(ddl_ReviewedBy.SelectedValue);
                    if (ddl_DocController.SelectedValue.ToString().Trim() == "0")
                    {
                        docObjects.DMSDocControllerID = 0;
                    }
                    else
                    {
                        docObjects.DMSDocControllerID = Convert.ToInt32(ddl_DocController.SelectedValue);
                    }
                    docObjects.IsSoftCopy = RadioButtonList1.SelectedValue == "yes" ? true : false;
                    docObjects.CopyType = Convert.ToInt32(ddl_CopyType.SelectedValue);
                    int DMS_ins = DMS_Bal.DMS_DocPrintRequestSubmission(docObjects, out int DocReqID);
                    msgSuccess.InnerHtml = "New Document Request with no. <b>" + DMS_ins.ToString() + "</b> has been Initiated Successfully.";
                    Upsucsess.Update();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "$('#ModalSuccess').modal({ backdrop: 'static', keyboard: false });", true);
                    DMS_Reset();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop3", "$('#myModal_request').modal('hide');", true);
                }
                else if (hdfVID.Value != dt.Rows[0]["VersionID"].ToString())
                {
                    msgError.InnerHtml = "The document with this version is not being used anymore";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#ModalError').modal({ backdrop: 'static', keyboard: false });", true);
                    upError.Update();
                    return;
                }
                else if (DMS_DT.Rows[0]["expStatus"].ToString() == "1")
                {
                    msgError.InnerHtml = "The document with this version is overdue";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#ModalError').modal({ backdrop: 'static', keyboard: false });", true);
                    upError.Update();
                    return;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DWRV_M6:" + strline + "  " + strMsg);
                string strError = "DWRV_M6:" + strline + "  " + "Document Request Submission failed.";
                divliteral.Visible = true;
                richedit2.Visible = false;
                divliteral.InnerText = @" <b style=""color:red;"">" + strError + "</b> ";
            }
            finally
            {
                btn_Submit.Enabled = true;
            }
        }
        protected void btn_Cancel_Click(object sender, EventArgs e)
        {
            btn_Cancel.Enabled = false;
            try
            {
                Response.Redirect("~/DMS/DocPrintReqMainList.aspx", false);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DWRV_M7:" + strline + "  " + strMsg);
                string strError = "DWRV_M7:" + strline + "  " + "Document Request Cancel failed.";
                divliteral.Visible = true;
                richedit2.Visible = false;
                divliteral.InnerText = @" <b style=""color:red;"">" + strError + "</b> ";
            }
            finally
            {
                btn_Cancel.Enabled = true;
            }
        }
        private int getPageCountByPath(string strTargPath)
        {
            RichEditDocumentServer serverSource = new RichEditDocumentServer();
            serverSource.LoadDocument(strTargPath);
            int SourcePageCount = serverSource.DocumentLayout.GetPageCount();
            return SourcePageCount;
        }
        private int getPageCountByStream(MemoryStream memoryStream)
        {
            RichEditDocumentServer serverSource = new RichEditDocumentServer();
            serverSource.LoadDocument(memoryStream);
            int SourcePageCount = serverSource.DocumentLayout.GetPageCount();
            return SourcePageCount;
        }
        private string validatePageNumbers()
        {
                string strPageNos = txt_PageNo.Text;
                var t1 = strPageNos.Substring(strPageNos.Length - 1);
                var t2 = strPageNos.Substring(0, 1);
                if (t1.Contains(',') || t1.Contains('-') || t2.Contains(',') || t2.Contains('-'))
                {
                    return "1";//restrict first special character and last special character 
                }
                else
                {
                    strPageNos = Regex.Replace(strPageNos.Trim(), @",+", ",");
                    strPageNos = Regex.Replace(strPageNos.Trim(), @"-+", "-");
                    strPageNos = "," + strPageNos;
                    string[] arrPnos1 = strPageNos.Split(',');
                    ArrayList PgNo = new ArrayList();
                    // if (arrPnos1[1] != "" && arrPnos1[1] != "-")
                    // {
                    foreach (string strPg in arrPnos1)
                    {
                        if (strPg.Trim().Length > 0)
                        {
                            if (strPg.Trim().Contains("-"))
                            {
                                string[] arrPg2 = strPg.Split('-');
                                if (arrPg2[0] != "" && arrPg2[1] != "")
                                {
                                    for (int i = Convert.ToInt32(arrPg2[0]); i <= Convert.ToInt32(arrPg2[1]); i++)
                                    {
                                        PgNo.Add(i);
                                    }
                                }
                                else
                                {

                                    return "3";//
                                }
                            }
                            else
                            {
                                PgNo.Add(Convert.ToInt32(strPg));
                            }
                        }
                    }
                    //}
                    //else
                    //{
                    //    return false;
                    //}
                    string isValidPgNO = "0";//No restriction
                    foreach (int page in PgNo)
                    {
                        if (page > Convert.ToInt32(hdfPageCount.Value) || page == 0)
                        {
                            isValidPgNO = "2";//restict 0 and page numbers grater than page count.
                            break;
                        }
                    }
                    return isValidPgNO;
                }
        }
        protected void ddl_ReviewedBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddl_DocController.Enabled = true;
            DMS_GetControllerList();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "select1", "$('.selectpicker1').selectpicker();", true);
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }      
    }
}