﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/DMS_Master/NestedDMS_Master.master" AutoEventWireup="true" CodeBehind="DocRequestList.aspx.cs" Inherits="AizantIT_PharmaApp.DMS.DocumentsList.DocRequestList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="padding-none">
        <asp:Button ID="btnAdd" runat="server" class="float-right  btn-signup_popup" PostBackUrl="~/DMS/DMSHomePage.aspx" Text="Dashboard" />
    </div>
    <div class=" col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left">
        <div class=" col-md-12 col-lg-12 col-12 col-sm-12 dms_outer_border float-left padding-none">
            <div class="col-md-12 col-lg-12 col-12 col-sm-12 TMS_AuthorizationError float-left" id="divAutorizedMsg" runat="server" visible="false">
                <asp:Label ID="lblAutorizedMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-left" id="divMainContainer" runat="server" visible="true">
                <div class=" col-md-12 col-lg-12 col-12 col-sm-12 padding-none float-right ">
                    <div class="col-md-12 col-lg-12 col-12 col-sm-12  padding-none float-left">
                        <div class="grid_header col-md-12 col-lg-12 col-12 col-sm-12 float-left">Document Print Request List</div>
                        <div class="col-md-12 col-lg-12 col-12 col-sm-12 grid_panel_full border_top_none bottom float-left">
                           <div class="col-12 float-left padding-none top ">
                            <table id="dtDocList" class="display datatable_cust" cellspacing="0" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>DocRequestID</th>
                                        <th>Request Number</th>
                                        <th>Document Name</th>
                                        <th>Document Number</th>
                                        <th>Document Version</th>
                                        <th>Print Purpose</th>
                                        <th>Page Numbers</th>
                                        <th>Approved By</th>
                                        <th>Request By</th>
                                        <th>Requested Date</th>
                                        <th>Print/Download</th>
                                        <th>View Request</th>
                                        <th>View Document</th>
                                        <th>History</th>
                                        <th>DocVersionID</th>
                                        <th>isSoftCopy</th>
                                    </tr>
                                </thead>
                            </table>
                               </div>
                            <div class=" col-lg-12  float-left bottom">
                                <img src="<%=ResolveUrl("~/Images/GridActionImages/Print.png")%>" /><b class="grid_icon_legend">Print Document</b>
                                <img src="<%=ResolveUrl("~/Images/GridActionImages/download.png")%>" /><b class="grid_icon_legend">Download Document</b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-------- Request VIEW-------------->
    <div id="myModal_request" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 98%">
            <!-- Modal content-->
            <div class="modal-content col-lg-12 col-md-12  padding-none">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            
                            <span id="span_FileTitle" runat="server"></span>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body ">
                            <div id="div_RequestedBy" class="col-md-5 col-lg-6 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="lblRequestedBy" runat="server" Text="Requested By" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_RequestedBy" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Requested By" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="divSoftCopy" class="form-group col-sm-12 col-lg-3 col-12 col-md-3 col-sm-12 float-left" style="padding-left: 50px">
                                <asp:Label ID="lblRadiobutton" runat="server" Text="Type of Request" CssClass="label-style"></asp:Label>
                                <asp:RadioButtonList runat="server" ID="RadioButtonList1" RepeatDirection="Horizontal" Enabled="false">
                                    <asp:ListItem Text="Download" Value="yes"></asp:ListItem>
                                    <asp:ListItem Text="Print" Value="no"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                            <div id="div_NoofCopies" class="col-md-2 col-lg-3 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="lblNoofCopies" runat="server" Text="No.of Copies" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_NoofCopies" runat="server" CssClass="form-control login_input_sign_up" TextMode="Number" Text="1" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_PageNo" class="col-md-2 col-lg-4 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="lblPageNo" runat="server" Text="Page Numbers" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_PageNo" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Page Numbers" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_docNo" class="col-md-6 col-lg-5 col-12 col-sm-12 form-group float-left">
                                    <asp:Label ID="lblDocNum" runat="server" Text="Document Number" CssClass="label-style"></asp:Label>
                                    <asp:TextBox ID="txt_DocumentNumber" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true" ></asp:TextBox>
                                </div>
                                <div id="div_versionnoNo" class="col-md-6 col-lg-3 col-12 col-sm-12 form-group float-left ">
                                    <asp:Label ID="lblVersionNum" runat="server" Text="Version Number" CssClass="label-style"></asp:Label>
                                    <asp:TextBox ID="txt_VersionNumber" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true" ></asp:TextBox>
                                </div>
                            <div id="div_DocName" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label4" runat="server" Text="Document Name" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_DocumentName" runat="server" TextMode="MultiLine" CssClass="form-control login_input_sign_up" placeholder="Enter Document Name" AutoPostBack="true" Rows="2" MaxLength="500" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_Purpose" class="col-md-6 col-lg-6 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="lblComments" runat="server" Text="Purpose" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_Purpose" runat="server" TextMode="MultiLine" CssClass="form-control login_input_sign_up" placeholder="Enter Purpose" Width="100%" Rows="2" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_ReviewedBy" class="col-md-6 col-lg-3 col-12 col-sm-12 form-group bottom float-left">
                                <asp:Label ID="Label6" runat="server" Text="Reviewed By" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_ReviewedBy" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_DocController" class="col-md-6 col-lg-3 col-12 col-sm-12 form-group bottom float-left">
                                <asp:Label ID="Label7" runat="server" Text="Document Controller" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_DocController" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_CopyType" class="col-md-6 col-lg-4 col-12 col-sm-12 form-group float-left">
                                <asp:Label ID="Label2" runat="server" Text="Type of Copy" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_CopyType" runat="server" CssClass="form-control login_input_sign_up" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="div_ValidDays" class="col-md-6 col-lg-2 col-12 col-sm-12 form-group   float-left">
                                <asp:Label ID="lblValidDays" runat="server" Text="Validity(In Days)" CssClass="label-style"></asp:Label>
                                <asp:TextBox ID="txt_ValidDays" runat="server" CssClass="form-control login_input_sign_up" placeholder="Enter Valid Days" readonly="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="col-12 text-right">
                    <button data-dismiss="modal" class=" btn-cancel_popup">Close</button>
                                </div>
                </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!-------- End Request VIEW-------------->

    <!--------Comment History View-------------->
    <div id="myCommentHistory" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width:85%">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel ID="upcomment" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-header">
                            
                            <span id="span4" runat="server"></span>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body" style="max-height: 600px; overflow-y: auto">
                            <div class="history_grid" style="overflow-y: auto;">
                                <asp:GridView ID="gv_CommentHistory" runat="server" CssClass="table table-hover" ClientIDMode="Static" EmptyDataText="No Records Found" AutoGenerateColumns="False" PagerSettings-Mode="NumericFirstLast" BackColor="White" BorderColor="#d2d2d2" BorderStyle="None" BorderWidth="1px" OnRowDataBound="gv_CommentHistory_RowDataBound" OnRowCommand="gv_CommentHistory_RowCommand" AllowPaging="true" PageSize="10" OnPageIndexChanging="gv_CommentHistory_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="User" ItemStyle-Width="12%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_GivenBy" runat="server" Text='<%# Eval("ActionBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" ItemStyle-Width="11%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Date" runat="server" Text='<%# Eval("ActionDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role" ItemStyle-Width="13%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Role" runat="server" Text='<%# Eval("CommentedRole") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="20%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Action" runat="server" Text='<%# Eval("Action") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments" ItemStyle-Width="29%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_shortComments" runat="server" Text='<%# Eval("Comments") %>'></asp:Label>
                                                <asp:TextBox ID="txt_Comments" runat="server" ReadOnly="true" Rows="2" TextMode="MultiLine" Text='<%# Eval("Comments") %>' Width="100%"></asp:TextBox>
                                                <asp:LinkButton ID="lbcommentsmore" runat="server" CommandArgument='<%# Eval("Comments")%>' CommandName="SM">Show More</asp:LinkButton>
                                                <asp:LinkButton ID="lbcommentsless" runat="server" CommandArgument='<%# Eval("Comments") %>' CommandName="SL">Show Less</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Assigned To">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_AssignedTo" runat="server" Text='<%# Eval("AssignedTo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle cssClass="gridpager"  HorizontalAlign="Right" />
                                </asp:GridView>
                            </div>
                        </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!--------End Comment History View-------------->

    <div id="myModalDownload" class="modal department fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm" style="min-width: 30%;">
            <!-- Modal content-->
            <div class="modal-content col-lg-12 padding-none">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <span id="span1" runat="server">Download Info</span>
                             <button type="button" class="close" data-dismiss="modal">&times;</button>

                        </div>
                        <div class="modal-body ">
                            <div class="col-lg-12 float-left">
                                <asp:Literal ID="literaldownload1" runat="server"></asp:Literal>

                                <asp:Label runat="server" ID="lblDownload">Wait until Download finishes, then click on OK.</asp:Label>
                            </div>
                            <input id="btnOk" type="button" class="float-right  btn-signup_popup top bottom" onclick="reloadtable();" value="OK" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <!-------- Document VIEW-------------->
    <div id="myDocumentView" class="modal department fade" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 90%; overflow: hidden">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <asp:UpdatePanel ID="UpHeading" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <span id="span2" runat="server">Document</span>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                     <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body" style="max-height: 680px;margin-bottom: 13px;">
                    <div id="divPDF_Viewer">
                        <noscript>
                            Please Enable JavaScript.
                        </noscript>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-------- End Document VIEW-------------->
      <!-------- Document VIEW-------------->
    <div id="MyModelPrinterNames" class="modal department fade" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 90%; overflow: hidden">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                            <span id="span3" runat="server">Printer Names</span>
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" style="max-height: 680px;margin-bottom: 13px;">
                   <div class="col-xl-12 col-md-12 col-sm-12 col-lg-12 float-left col-12">
                        <table id="DocPrintertable" class="table datatable_cust table-striped table-bordered" style="width:100%;" nodata="no data">
                            <thead>
                                <tr>
                                    <th hidden>PrinterID</th>
                                    <th>S.No</th>
                                    <th>Printer Area Name</th>
                                    <th>Printer Name</th>
                                    <th>Select</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-------- End Document VIEW-------------->
    <asp:HiddenField ID="hdfVID" runat="server" />
    <asp:HiddenField ID="hdfRID" runat="server" />
    <asp:HiddenField ID="hdnEmpID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnAction" runat="server" />
    <asp:HiddenField ID="hdfViewType" runat="server" />
    <asp:HiddenField ID="hdfViewEmpID" runat="server" />
    <asp:HiddenField ID="hdfViewRoleID" runat="server" />
    <asp:HiddenField ID="hdfViewDivID" runat="server" />
    <asp:HiddenField ID="hdnPrinterName" runat="server" />
    <asp:HiddenField ID="hdnWebCnfgDocValue" runat="server" />

    <asp:UpdatePanel ID="UpGridButtons" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnRequestView" runat="server" Text="Submit" OnClick="btnRequestView_Click" Style="display: none" />
            <asp:Button ID="btnCommentsHistoryView" runat="server" Text="Submit" Style="display: none" OnClick="btnCommentsHistoryView_Click" />
            <asp:Button ID="btnPrintDocument" runat="server" Text="Submit" Style="display: none" OnClick="btnPrintDocument_Click" />
            <asp:Button ID="btnDownloadDocument" runat="server" Text="Submit" Style="display: none" OnClick="btnDownloadDocument_Click" OnClientClick="showDownload();" />
            <asp:Button ID="btnDocumentView" runat="server" Text="Submit" OnClick="btnDocumentView_Click" Style="display: none" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnDownloadDocument" />
        </Triggers>
    </asp:UpdatePanel>

    <script>
        $('#dtDocList thead tr').clone(true).appendTo('#dtDocList thead');
        $('#dtDocList thead tr:eq(1) th').each(function (i) {
            if (i < 11) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder=" Search " />');

                $('input', this).on('keyup change', function () {
                    if (oTable.column(i).search() !== this.value) {
                        oTable
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
                if (i == 1) {
                    $(this).html('');
                }
            }
            else {
                $(this).html('');
            }
        });
        $('#dtDocList').wrap('<div class="dataTables_scroll" />');
        var oTable = $('#dtDocList').DataTable({
            columns: [
                { 'data': 'RowNumber' },
                { 'data': 'DocReqID' },
                { 'data': 'RequestNumber' },
                { 'data': 'DocumentName' },
                 { 'data': 'DocumentNumber' },//4
                  { 'data': 'DocumentVersionNumber' },//5
                { 'data': 'PrintPurpose' },
                { 'data': 'PageNumbers' },
                { 'data': 'ApprovedBy' },
                { 'data': 'RequestBy' },
                { 'data': 'RequestedDate' },
                { 'data': 'ActionStatusID' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="view_req" title="View" data-toggle="modal" data-target="#myModal_request" onclick=ViewRequest(' + o.DocReqID + ')></a>'; }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="view" title="View" data-toggle="modal" data-target="#myDocumentView" onclick=ViewDocument(' + o.DocVersionID + ')></a>'; }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) { return '<a  class="summary_latest" title="History" data-toggle="modal" data-target="#myCommentHistory" onclick=ViewCommentHistory(' + o.DocReqID + ')></a>'; }
                },
                { 'data': 'DocVersionID' },
                { 'data': 'IsSoftCopy' },
            ],
            "responsive": true,
            "paging": true,
            "search": {
                "regex": true
            },
            "aoColumnDefs": [{ "targets": [0,1, 3,6,15, 16], "visible": false, "searchable": false }, { "bSortable": false, "aTargets": [0, 11] }, { "className": "dt-body-left", "targets": [3,4, 6,8,9] },
            {
                targets: [11], render: function (a, b, data, d) {
                    if (data.ActionStatusID == 3 && data.IsSoftCopy == false) {
                        return '<a class="print" title="Print" data-toggle="modal" onclick=PrintDocument(' + data.DocReqID + ',' + data.DocVersionID + ')></a>';
                    }
                    else if (data.ActionStatusID == 3 && data.IsSoftCopy == true) {
                        return '<a class="download" title="download" data-toggle="modal"  onclick=DownloadDocument(' + data.DocReqID + ',' + data.DocVersionID + ')></a>';//data-target="#myModalDownload"
                    }
                    else {
                        return "N/A";
                    }
                }
            },
            ],
            "orderCellsTop": true,
            "order": [[1, "desc"]],
            'bAutoWidth': true,
            sServerMethod: 'Post',
            "sAjaxSource": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetDocPrintReqList" )%>',
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({ "name": "EmpID", "value": <%=hdnEmpID.Value%> });
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                        $("#dtDocList").show();
                        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
                        UserSessionCheck();
                    },
                    error: function (xhr, textStatus, error) {
                        if (typeof console == "object") {
                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        }
                    }
                });
            }
        });
        function reloadtable() {
            window.open("<%=ResolveUrl("~/DMS/DocumentsList/DocRequestList.aspx")%>", "_self");
        }
    </script>
    <script>
        function ViewRequest(V_ID) {
            document.getElementById("<%=hdfRID.ClientID%>").value = V_ID;
            var btnRV = document.getElementById("<%=btnRequestView.ClientID %>");
            btnRV.click();
        }
        function ViewCommentHistory(PK_ID) {
            document.getElementById("<%=hdfRID.ClientID%>").value = PK_ID;
            var btnCV = document.getElementById("<%=btnCommentsHistoryView.ClientID %>");
            btnCV.click();
        }
        function PrintDocument(PK_ID, V_ID) {
            var WebConfigDefaultValue = document.getElementById("<%=hdnWebCnfgDocValue.ClientID%>").value;
          
            document.getElementById("<%=hdfRID.ClientID%>").value = PK_ID;
            document.getElementById("<%=hdfVID.ClientID%>").value = V_ID;
            if (WebConfigDefaultValue == "0")//Printer Names POPU Open 
            {
                GetPrinterDetails();
              } else {
                   var btnPD = document.getElementById("<%=btnPrintDocument.ClientID %>");
                  btnPD.click();
            }
           
        }
        function DownloadDocument(PK_ID, V_ID) {
            document.getElementById("<%=hdfRID.ClientID%>").value = PK_ID;
            document.getElementById("<%=hdfVID.ClientID%>").value = V_ID;
            var btnDD = document.getElementById("<%=btnDownloadDocument.ClientID %>");
            btnDD.click();
        }
        function ViewDocument(V_ID) {
            document.getElementById("<%=hdfVID.ClientID%>").value = V_ID;
            var btnDV = document.getElementById("<%=btnDocumentView.ClientID %>");
            btnDV.click();
        }
    </script>
    <script src='<%= Page.ResolveUrl("~/AppScripts/PDF_Viewer.min.js")%>'></script>
    <script>
        function showDownload() {
            custAlertMsg("Document is downloading please wait,Click OK After Download Is Completed..","info","reloadtable();");
           // $('#myModalDownload').modal({ show: true, backdrop: 'static', keyboard: false });
        }
        function ViewPDFDocument(DivID) {
            PdfViewerContentLoading('<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>', "0", $('#<%=hdfVID.ClientID%>').val(), "0", "0", DivID);
        }
    </script>
     <script type="text/javascript">
         function GetPrinterDetails() {
             $.ajax({
                 
                  "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": '<%= ResolveUrl("~/DMS/WebServices/DMSService.asmx/GetPrinterNamesList" )%>',
                    "data": {},
                    success: function (result) {
                        $('#DocPrintertable tbody').empty();
                        $.each(result.d, function (i, item) {
                            var tr = $("<tr></tr>");
                            var id = i + 1;
                            tr.append(
                                '<td class="ClsPRinterID" style=display:none>' + item.printerID + "</td>" +
                               '<td class="ClsID" >' + id + "</td>" +
                                '<td class="ClsPrinterAreaName">' + item.PrinterAreanName + "</td>" +
                                '<td class="ClsPrinterName">' + item.PrinterName + "</td>" +
                                '<td class="text_left_icon"><a class="print" title="Print" onclick="SelectDocPrinter(this)"></a>' +
                           '</td>' 
                            );
                            $('#DocPrintertable tbody').append(tr);
                        });

                    } 
             });
                    $('#MyModelPrinterNames').modal('show');

         }
    
    </script>
    <script>
         function SelectDocPrinter(cntrl) {
             var PrinterID = $(cntrl).closest("tr").find(".ClsPRinterID").eq(0).text().trim();
        var PrinterName = $(cntrl).closest("tr").find(".ClsPrinterName").eq(0).text().trim();
        var PrinterAreaName = $(cntrl).closest("tr").find(".ClsPrinterAreaName").eq(0).text().trim();
          document.getElementById("<%=hdnPrinterName.ClientID%>").value = PrinterName;
            var btnPD = document.getElementById("<%=btnPrintDocument.ClientID %>");
            btnPD.click();
         }
    </script>
    <div class="col-lg-12" style="position: absolute; top: -13px; left: 0">
        <asp:UpdateProgress ID="upActionProgress" runat="server" AssociatedUpdatePanelID="UpGridButtons">
            <ProgressTemplate>
                <div class="" style="position: absolute; height: 93vh; background-color: #3e3e3e; z-index: 1; width: 100%; opacity: 0.5;"></div>
                <div class="ProgressModal" style="z-index: 99; position: relative; padding: 10pc 21pc; left: 9px">
                    <div class="ProgressCenter">
                        <img alt="" src="<%=ResolveUrl("~/Images/TMS_Images/please_wait.gif")%>" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
</asp:Content>
