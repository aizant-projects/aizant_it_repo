﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using System.Web.UI;

namespace AizantIT_PharmaApp.DMS.DocumentsList
{
    public partial class FormActiveList : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=23").Length > 0)//23-Reader
                            {
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FAL_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FAL_M1:" + strline + "  " + "Document Load failed.", "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        protected void btnFormView_Click(object sender, EventArgs e)
        {
            btnFormView.Enabled = false;
            try
            {
                DocObjects docObjects = new DocObjects();
                docObjects.FormVersionID = Convert.ToInt32(hdfVID.Value);
                DataTable dt = DMS_Bal.DMS_GetFormContent(docObjects);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myFormView').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                divPDF_Viewer.InnerHtml = "&nbsp";
                divPDF_Viewer.InnerHtml = "<iframe src=\"" + ResolveUrl("~/DMS/DMSAdmin/FormView.aspx") + "?FormVID=" + hdfVID.Value + "&reader=y" + "#toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" height=\"650px\" frameborder=\"0\"></iframe>";
                UpdatePanel4.Update();
                DataTable DMS_DT = DMS_Bal.DMS_GetFormHistory(docObjects);
                span1.Attributes.Add("title", DMS_DT.Rows[0]["FormTitle"].ToString());
                if (DMS_DT.Rows[0]["FormTitle"].ToString().Length > 60)
                {
                    span1.InnerHtml = DMS_DT.Rows[0]["FormTitle"].ToString().Substring(0, 60) + "...";
                }
                else
                {
                    span1.InnerHtml = DMS_DT.Rows[0]["FormTitle"].ToString();
                }
                UpHeading.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("FAL_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "FAL_M2:" + strline + "  " + "Form viewing failed.", "error");
            }
            finally
            {
                btnFormView.Enabled = true;
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}