﻿using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using AizantIT_DMSBO;
using AizantIT_DMSBAL;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_BusinessLayer;
using System.IO;
using DevExpress.Pdf;
using System.Collections;
using System.Printing;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Configuration;
using System.Collections.Generic;

namespace AizantIT_PharmaApp.DMS.DocumentsList
{
    public partial class DocRequestList : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        DocPrintRequest DocReqObjects = new DocPrintRequest();
        DocObjects docObjects = new DocObjects();
        UMS_BAL objUMS_BAL;
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    if (!IsPostBack)
                    {
                        hdnWebCnfgDocValue.Value= ConfigurationManager.AppSettings.Get("isDefaultPrinter");
                        hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                        DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                        DataTable dtTemp = new DataTable();
                        DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                        if (drUMS.Length > 0)
                        {
                            dtTemp = drUMS.CopyToDataTable();
                            if (dtTemp.Select("RoleID=23").Length > 0)//23-Reader
                            {
                                if (Request.QueryString["Notification_ID"] != null)
                                {
                                    objUMS_BAL = new UMS_BAL();
                                    int[] empIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, empIDs); //2 represents NotifyEmp had visited the page through notification link.
                                }
                            }
                            else
                            {
                                loadAuthorizeErrorMsg();
                            }
                        }
                        else
                        {
                            Response.Redirect("~/UserLogin.aspx", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/UserLogin.aspx", false);
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DRL_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DRL_M1:" + strline + "  " + "Document Load failed.", "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        protected void btnRequestView_Click(object sender, EventArgs e)
        {
            btnRequestView.Enabled = false;
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocPrintReqDetails(hdfRID.Value);
                txt_RequestedBy.Text = DMS_DT.Rows[0]["RequestBy"].ToString();
                if (Convert.ToBoolean(DMS_DT.Rows[0]["isSoftCopy"].ToString()) == true)
                {
                    RadioButtonList1.SelectedValue = "yes";
                }
                else
                {
                    RadioButtonList1.SelectedValue = "no";
                }
                txt_PageNo.Text = string.IsNullOrEmpty(DMS_DT.Rows[0]["PageNumbers"].ToString()) ? "All" : DMS_DT.Rows[0]["PageNumbers"].ToString();
                txt_DocumentName.Text = DMS_DT.Rows[0]["DocumentName"].ToString();
                txt_Purpose.Text = DMS_DT.Rows[0]["PrintPurpose"].ToString();
                txt_ReviewedBy.Text = DMS_DT.Rows[0]["ReviewedBy"].ToString();
                txt_DocController.Text = DMS_DT.Rows[0]["DocController"].ToString();
                txt_CopyType.Text = DMS_DT.Rows[0]["DistributionCopyType"].ToString();
                txt_DocumentNumber.Text = DMS_DT.Rows[0]["DocumentNumber"].ToString();
                txt_VersionNumber.Text = DMS_DT.Rows[0]["VersionNumber"].ToString();
                if (DMS_DT.Rows[0]["ValidDays"].ToString() != "0" && DMS_DT.Rows[0]["ValidDays"].ToString() != "")
                {
                    txt_ValidDays.Text = DMS_DT.Rows[0]["ValidDays"].ToString();
                }
                else
                {
                    txt_ValidDays.Text = "N/A";
                }
                UpGridButtons.Update();
                span_FileTitle.InnerText = "Request Number-" + DMS_DT.Rows[0]["RequestNumber"].ToString();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_request').modal({ show: true, backdrop: 'static', keyboard: false });", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DRL_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DRL_M2:" + strline + "  " + "Document Load failed.", "error");
            }
            finally
            {
                btnRequestView.Enabled = true;
            }
        }
        protected void btnCommentsHistoryView_Click(object sender, EventArgs e)
        {
            btnCommentsHistoryView.Enabled = false;
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocDistributionHistory(hdfRID.Value);
                gv_CommentHistory.DataSource = DMS_DT;
                ViewState["HistoryDataTable"] = DMS_DT;
                gv_CommentHistory.DataBind();
                UpGridButtons.Update();
                span4.InnerHtml = DMS_DT.Rows[0]["DocReqTitle"].ToString();
                upcomment.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myCommentHistory').modal({ show: true, backdrop: 'static', keyboard: false });", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DRL_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DRL_M3:" + strline + "  " + "Document Request History loading failed.", "error");
            }
            finally
            {
                btnCommentsHistoryView.Enabled = true;
            }
        }
        protected void btnPrintDocument_Click(object sender, EventArgs e)
        {
            btnPrintDocument.Enabled = false;
            try
            {
                System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
                string DefaultPrinter = string.Empty,
                    isDefaultPrinter = hdnWebCnfgDocValue.Value;
                // ConfigurationManager.AppSettings.Get("isDefaultPrinter");
                List<string> ListofInstalledPrinters = new List<string>();
                String pkInstalledPrinters;
                for (int i = 0; i < System.Drawing.Printing.PrinterSettings.InstalledPrinters.Count; i++)
                {
                    pkInstalledPrinters = System.Drawing.Printing.PrinterSettings.InstalledPrinters[i];
                    ListofInstalledPrinters.Add(pkInstalledPrinters);
                }
                if (isDefaultPrinter == "1")
                {
                    DefaultPrinter = printerSettings.PrinterName = LocalPrintServer.GetDefaultPrintQueue().FullName;
                }else if (isDefaultPrinter == "0")
                {
                    DefaultPrinter = printerSettings.PrinterName =hdnPrinterName.Value ;//get Printer Name from client side

                }
                if (!ListofInstalledPrinters.Contains(DefaultPrinter) ||DefaultPrinter == "Microsoft Print to PDF" || DefaultPrinter == "Microsoft XPS Document Writer" || DefaultPrinter.Contains("Send To OneNote"))
                    {
                        HelpClass.custAlertMsg(this, this.GetType(), "Sorry ! There is no Printer configured to print.", "error");
                    return;
                }
                
                else
                {
                    DataTable dt = DMS_Bal.DMS_GetDocPrintReqDetails(hdfRID.Value);
                    docObjects.Version = Convert.ToInt32(hdfVID.Value);
                    docObjects.RoleID = 0;
                    docObjects.EmpID = 0;
                    docObjects.FileType1 = ".pdf";
                    docObjects.Mode = 0;
                    System.Data.DataTable DMS_DT = DMS_Bal.DMS_GetDocumentLocationList(docObjects);
                    string pathin = Server.MapPath(@DMS_DT.Rows[0]["FilePath"].ToString());
                    string strFile2Upload = DynamicFolder.CreateDynamicFolder(3);
                    string File3Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                    string TargetPDF = (strFile2Upload + "\\" + File3Path);

                    PdfReader reader = new PdfReader(pathin);
                    PdfStamper stamper = new PdfStamper(reader, new FileStream(TargetPDF, FileMode.Create));
                    int pages = reader.NumberOfPages;
                    for (int i = 1; i <= pages; i++)
                    {
                        PdfContentByte underContent = stamper.GetOverContent(i);
                        PdfPTable Footertab = new PdfPTable(3);
                        Footertab.TotalWidth = 500F;
                        //row 1
                        string ReqNum = "Request Number: " + dt.Rows[0]["RequestNumber"].ToString();
                        PdfPCell RequestNumber = new PdfPCell(new Phrase(ReqNum,
                                       new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                        Footertab.AddCell(RequestNumber);
                        string CopyNum = "Copy Number: " + "0000" + dt.Rows[0]["PrintSerialNo"].ToString();
                        PdfPCell CopyNumber = new PdfPCell(new Phrase(CopyNum,
                                       new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                        Footertab.AddCell(CopyNumber);

                        PdfPCell TypeofCopy = new PdfPCell(new Phrase(dt.Rows[0]["DistributionCopyType"].ToString(),
                                       new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                        Footertab.AddCell(TypeofCopy);
                        //row 2
                        string PrintBy = "Printed By: " + dt.Rows[0]["RequestBy"].ToString();
                        PdfPCell PrintedBy = new PdfPCell(new Phrase(PrintBy,
                                       new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                        Footertab.AddCell(PrintedBy);
                        string PrintDate = "Printed Date: " + DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
                        PdfPCell PrintedDate = new PdfPCell(new Phrase(PrintDate,
                                       new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                        Footertab.AddCell(PrintedDate);
                        if (dt.Rows[0]["DistributionCopyTypeID"].ToString() == "1")
                        {
                            string validity = "Valid Upto: " + "(" + (Convert.ToInt32(dt.Rows[0]["ValidDays"]) * 24) + " hrs) " + DateTime.Now.AddDays(Convert.ToDouble(dt.Rows[0]["ValidDays"])).ToString("dd MMM yyyy HH:mm:ss");
                            PdfPCell ValidUpto = new PdfPCell(new Phrase(validity,
                                           new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                            Footertab.AddCell(ValidUpto);
                        }
                        else
                        {
                            PdfPCell ValidUpto = new PdfPCell(new Phrase("Validity: (N/A)",
                                           new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                            Footertab.AddCell(ValidUpto);
                        }
                        iTextSharp.text.Document document = underContent.PdfDocument;
                        Footertab.WriteSelectedRows(0, -1, 50f, document.Bottom + 5f, underContent);
                    }
                    stamper.Close();
                    reader.Close();
                    // Create a Pdf Document Processor instance and load a PDF into it. 
                    PdfDocumentProcessor documentProcessor = new PdfDocumentProcessor();
                    documentProcessor.LoadDocument(TargetPDF);

                    //Declare the PDF printer settings.
                    PdfPrinterSettings pdfPrinterSettings = new PdfPrinterSettings();

                    //Specify the PDF printer settings.
                    if (string.IsNullOrEmpty(dt.Rows[0]["PageNumbers"].ToString()))
                    {
                    }
                    else
                    {
                        string strPageNos = dt.Rows[0]["PageNumbers"].ToString();
                        strPageNos = "," + strPageNos;
                        string[] arrPnos1 = strPageNos.Split(',');
                        ArrayList PgNo = new ArrayList();
                        PgNo.Add(1);
                        foreach (string strPg in arrPnos1)
                        {
                            if (strPg.Trim().Length > 0)
                            {
                                if (strPg.Trim().Contains("-"))
                                {
                                    string[] arrPg2 = strPg.Split('-');
                                    for (int i = Convert.ToInt32(arrPg2[0]); i <= Convert.ToInt32(arrPg2[1]); i++)
                                    {
                                        PgNo.Add(i + 1);
                                    }
                                }
                                else
                                {
                                    PgNo.Add(Convert.ToInt32(strPg) + 1);
                                }
                            }
                        }

                        pdfPrinterSettings.PageNumbers = PgNo.OfType<int>().ToArray();
                    }
                    //Setting the PdfPrintScaleMode property to CustomScale requires
                    //specifying the Scale property, as well.                  


                    pdfPrinterSettings.ScaleMode = PdfPrintScaleMode.Fit;
                    pdfPrinterSettings.Settings.Copies = 1;
                    pdfPrinterSettings.Settings.Duplex = System.Drawing.Printing.Duplex.Simplex;
                    pdfPrinterSettings.Settings.PrinterName = DefaultPrinter;
                    //Print the document using the specified printer settings. 
                    documentProcessor.Print(pdfPrinterSettings);
                    DocReqObjects.DocReqID = Convert.ToInt32(hdfRID.Value);
                    DocReqObjects.RequestedByID = Convert.ToInt32(hdnEmpID.Value);
                    int DMS_AP = DMS_BalDM.DMS_DocPrintReqByRequestor(DocReqObjects);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document Request Number <b>" + dt.Rows[0]["RequestNumber"].ToString() + "</b> Printed Successfully.", "success", "reloadtable();");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DRL_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DRL_M4:" + strline + "  " + "Document Request Print failed.", "error");
            }
            finally
            {
                btnPrintDocument.Enabled = false;
            }
        }
        protected void gv_CommentHistory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string _fullcomments = (e.Row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox).Text;
                    LinkButton _lnkmorecomments = (e.Row.Cells[3].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (e.Row.Cells[3].FindControl("lbcommentsless") as LinkButton);
                    if (_fullcomments.Length > 70)
                    {
                        (e.Row.Cells[3].FindControl("lbl_shortComments") as Label).Visible = true;
                        System.Web.UI.WebControls.TextBox FullTbx = (e.Row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox);
                        (e.Row.Cells[3].FindControl("lbl_shortComments") as Label).Text = _fullcomments.Substring(0, 70) + "..";
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = true;

                        if (_fullcomments.Length > 100 && _fullcomments.Length <= 200)
                        {
                            FullTbx.Rows = 3;
                        }
                        else if (_fullcomments.Length > 200 && _fullcomments.Length <= 300)
                        {
                            FullTbx.Rows = 4;
                        }

                        FullTbx.Visible = false;
                    }
                    else
                    {
                        (e.Row.Cells[3].FindControl("lbl_shortComments") as Label).Visible = true;
                        (e.Row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox).Visible = false;
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DRL_M5:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DRL_M5:" + strline + "  " + "Comment History row databound failed.", "error");
            }
        }
        protected void gv_CommentHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
               
                if (e.CommandName == "SM")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    LinkButton _lnkmorecomments = (row.Cells[3].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (row.Cells[3].FindControl("lbcommentsless") as LinkButton);
                    (row.Cells[3].FindControl("lbl_shortComments") as Label).Visible = false;
                    (row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox).Visible = true;
                    _lnklesscomments.Visible = true;
                    _lnkmorecomments.Visible = false;
                }
                else if (e.CommandName == "SL")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    LinkButton _lnkmorecomments = (row.Cells[3].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (row.Cells[3].FindControl("lbcommentsless") as LinkButton);
                    (row.Cells[3].FindControl("lbl_shortComments") as Label).Visible = true;
                    (row.Cells[3].FindControl("txt_Comments") as System.Web.UI.WebControls.TextBox).Visible = false;
                    _lnklesscomments.Visible = false;
                    _lnkmorecomments.Visible = true;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DRL_M6:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DRL_M6:" + strline + "  " + "Comment History row command failed.", "error");
            }
        }
        protected void btnDownloadDocument_Click(object sender, EventArgs e)
        {
            btnDownloadDocument.Enabled = false;
            try
            {
                Session["DwnloadFilePath"] = "";
                DataTable dt = DMS_Bal.DMS_GetDocPrintReqDetails(hdfRID.Value);
                docObjects.Version = Convert.ToInt32(hdfVID.Value);
                docObjects.RoleID = 0;
                docObjects.EmpID = 0;
                docObjects.FileType1 = ".pdf";
                docObjects.Mode = 0;
                System.Data.DataTable DMS_DT = DMS_Bal.DMS_GetDocumentLocationList(docObjects);
                string pathin = Server.MapPath(@DMS_DT.Rows[0]["FilePath"].ToString());
                string strFile2Upload = DynamicFolder.CreateDynamicFolder(3);
                string File3Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                string TargetPDF = (strFile2Upload + "\\" + File3Path);

                PdfReader reader = new PdfReader(pathin);
                PdfStamper stamper = new PdfStamper(reader, new FileStream(TargetPDF, FileMode.Create));
                int pages = reader.NumberOfPages;
                for (int i = 1; i <= pages; i++)
                {
                    PdfContentByte underContent = stamper.GetOverContent(i);
                    PdfPTable Footertab = new PdfPTable(3);
                    Footertab.TotalWidth = 500F;
                    //row 1
                    string ReqNum = "Request Number: " + dt.Rows[0]["RequestNumber"].ToString();
                    PdfPCell RequestNumber = new PdfPCell(new Phrase(ReqNum,
                                   new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                    Footertab.AddCell(RequestNumber);
                    string CopyNum = "Copy Number: " + "0000" + dt.Rows[0]["PrintSerialNo"].ToString();
                    PdfPCell CopyNumber = new PdfPCell(new Phrase(CopyNum,
                                   new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                    Footertab.AddCell(CopyNumber);
                    PdfPCell TypeofCopy = new PdfPCell(new Phrase(dt.Rows[0]["DistributionCopyType"].ToString(),
                                   new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                    Footertab.AddCell(TypeofCopy);
                    //row 2
                    string PrintBy = "Printed By: " + dt.Rows[0]["RequestBy"].ToString();
                    PdfPCell PrintedBy = new PdfPCell(new Phrase(PrintBy,
                                   new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                    Footertab.AddCell(PrintedBy);
                    string PrintDate = "Printed Date: " + DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
                    PdfPCell PrintedDate = new PdfPCell(new Phrase(PrintDate,
                                   new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                    Footertab.AddCell(PrintedDate);
                    if (dt.Rows[0]["DistributionCopyTypeID"].ToString() == "1")
                    {
                        string validity = "Valid Upto: " + "(" + (Convert.ToInt32(dt.Rows[0]["ValidDays"]) * 24) + " hrs) " + DateTime.Now.AddDays(Convert.ToDouble(dt.Rows[0]["ValidDays"])).ToString("dd MMM yyyy HH:mm:ss");
                        PdfPCell ValidUpto = new PdfPCell(new Phrase(validity,
                                       new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                        Footertab.AddCell(ValidUpto);
                    }
                    else
                    {
                        PdfPCell ValidUpto = new PdfPCell(new Phrase("Validity: (N/A)",
                                       new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10F)));
                        Footertab.AddCell(ValidUpto);
                    }
                    iTextSharp.text.Document document = underContent.PdfDocument;
                    Footertab.WriteSelectedRows(0, -1, 50f, document.Bottom + 5f, underContent);
                }
                stamper.Close();
                reader.Close();
                string DownloadPDF = ExtractPages(TargetPDF, dt);
                System.IO.FileInfo file = new System.IO.FileInfo(DownloadPDF);
                //string[] FileName = dt.Rows[0]["FileName"].ToString().Split('.');
                string FileName = dt.Rows[0]["DownloadTitle"].ToString() + file.Extension;
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + FileName);
                Response.TransmitFile(file.FullName);
                DocReqObjects.DocReqID = Convert.ToInt32(hdfRID.Value);
                DocReqObjects.RequestedByID = Convert.ToInt32(hdnEmpID.Value);
                int DMS_AP = DMS_BalDM.DMS_DocDownloadReqByRequestor(DocReqObjects);
                HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document Request Number <b>" + dt.Rows[0]["RequestNumber"].ToString() + "</b> Downloaded by Requester Successfully.", "success", "reloadtable();");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DRL_M7:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DRL_M7:" + strline + "  " + "Document Request download failed.", "error");
            }
            finally
            {
                btnDownloadDocument.Enabled = true;
            }
            lblDownload.Text = "Successfully downloaded";
        }
        public string ExtractPages(string SourcePDF, DataTable dt1)
        {
            try
            {

                if (string.IsNullOrEmpty(dt1.Rows[0]["PageNumbers"].ToString()))
                {
                    return SourcePDF;
                }
                else
                {
                    string strPageNos = dt1.Rows[0]["PageNumbers"].ToString();
                    strPageNos = "," + strPageNos;
                    string[] arrPnos1 = strPageNos.Split(',');
                    ArrayList PgNo = new ArrayList();
                    PgNo.Add(1);
                    string strFile2Upload = DynamicFolder.CreateDynamicFolder(3);
                    string File3Path = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                    string TargetPDF = (strFile2Upload + "\\" + File3Path);
                    foreach (string strPg in arrPnos1)
                    {
                        if (strPg.Trim().Length > 0)
                        {
                            if (strPg.Trim().Contains("-"))
                            {
                                string[] arrPg2 = strPg.Split('-');
                                for (int i = Convert.ToInt32(arrPg2[0]); i <= Convert.ToInt32(arrPg2[1]); i++)
                                {
                                    PgNo.Add(i + 1);
                                }
                            }
                            else
                            {
                                PgNo.Add(Convert.ToInt32(strPg) + 1);
                            }
                        }
                    }
                    using (PdfDocumentProcessor source = new PdfDocumentProcessor())
                    {
                        source.LoadDocument(SourcePDF);
                        using (PdfDocumentProcessor target = new PdfDocumentProcessor())
                        {

                            target.CreateEmptyDocument(TargetPDF);
                            foreach (int Pg1 in PgNo)
                            {
                                target.Document.Pages.Add(source.Document.Pages[Pg1 - 1]);
                            }
                        }
                    }
                    return TargetPDF;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btnDocumentView_Click(object sender, EventArgs e)
        {
            btnDocumentView.Enabled = false;
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfVID.Value);
                span2.InnerHtml = "<b>" + DMS_DT.Rows[0]["DocumentName"].ToString() + "</b>";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myDocumentView').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                hdfViewType.Value = "0";
                hdfViewEmpID.Value = "0";
                hdfViewRoleID.Value = "0";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view1", "ViewPDFDocument('#divPDF_Viewer');", true);
                UpHeading.Update();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DRL_M9:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DRL_M9:" + strline + "  " + "Document Viewing failed.", "error");
            }
            finally
            {
                btnDocumentView.Enabled = true;
            }
        }
        protected void gv_CommentHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_CommentHistory.PageIndex = e.NewPageIndex;
                gv_CommentHistory.DataSource = (DataTable)(ViewState["HistoryDataTable"]);
                gv_CommentHistory.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DRL_M10:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "DRL_M10:" + strline + "  " + "grid view page index changing failed.", "error");
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }       
    }
}