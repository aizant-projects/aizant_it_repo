﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AizantIT_DMSBAL;
using AizantIT_PharmaApp.Common;
using DevExpress.Office;
using DevExpress.Web.Office;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
namespace AizantIT_PharmaApp.DMS.DocumentsList
{
    public partial class DMSFileVIew : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString.Count > 0)
                    {
                        string vid = Request.QueryString[0];
                        ToViewSopDocument(vid);
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("DFV_M1:" + strline + "  " + strMsg);
                string strError = "DFV_M1:" + strline + "  " + "Page loading failed";
                divliteral.Visible = true;
                richedit2.Visible = false;
                divliteral.InnerText = @" <b style=""color:red;"">" + strError + "</b> ";
            }
        }
        private void ToViewSopDocument(string vid)
        {
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(vid);
                if (DMS_DT.Rows[0]["FileType"].ToString() == "pdf")
                {
                    divliteral.Visible = true;
                    richedit2.Visible = false;
                    literalSop1.Text = " <object data=\"" + ResolveUrl("~/ASPXHandlers/docdetaik.ashx") + "?SopId=" + DMS_DT.Rows[0]["DocumentID"].ToString() + "#toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" height=\"612px\" ></object>";
                }
                else
                {
                    divliteral.Visible = false;
                    richedit2.Visible = true;
                    if (DMS_DT.Rows[0]["Content"].ToString().Length > 0)
                    {
                        if (DMS_DT.Rows[0]["RequestTypeID"].ToString() != "4")
                        {
                            if (DMS_DT.Rows[0]["DocStatus"].ToString() == "A")
                            {
                                byte[] docBytes = (byte[])DMS_DT.Rows[0]["Content"];
                                //string strTargPath = Server.MapPath(@"~/Files/WordFile.rtf");
                                string strFileUpload = DynamicFolder.CreateDynamicFolder(4);
                                string File1Path = DateTime.Now.ToString("yyyyMMddHHmmss") + ".rtf";
                                string strTargPath = (strFileUpload + "\\" + File1Path);
                                File.WriteAllBytes(strTargPath, docBytes);
                                RichEditDocumentServer server = new RichEditDocumentServer();
                                server.LoadDocument(Server.MapPath(@"~/Files/DocActiveTemplate.docx"), DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                                Document document = server.Document;
                                Section firstSection = document.Sections[0];
                                DataSet ds = DMS_Bal.DMS_ShowFirstPageDetails(vid);
                                SubDocument myHeader = firstSection.BeginUpdateHeader(HeaderFooterType.First);
                                DocumentRange[] deptrange = myHeader.FindAll("docdept1", SearchOptions.CaseSensitive, myHeader.Range);
                                myHeader.InsertText(deptrange[0].Start, ds.Tables[0].Rows[0][2].ToString());
                                myHeader.Delete(deptrange[0]);
                                /*Not using foreach loop*/
                                //foreach (DocumentRange documentRange in deptrange)
                                //{
                                //    myHeader.InsertText(documentRange.Start, ds.Tables[0].Rows[0][2].ToString());
                                //    myHeader.Delete(documentRange);
                                //}
                                DocumentRange[] typerange = myHeader.FindAll("doctype1", SearchOptions.CaseSensitive, myHeader.Range);
                                myHeader.InsertText(typerange[0].Start, ds.Tables[0].Rows[0][5].ToString());
                                myHeader.Delete(typerange[0]);
                                /*Not using foreach loop*/
                                //foreach (DocumentRange documentRange in typerange)
                                //{
                                //    myHeader.InsertText(documentRange.Start, ds.Tables[0].Rows[0][5].ToString());
                                //    myHeader.Delete(documentRange);
                                //}
                                DocumentRange[] docnamerange = myHeader.FindAll("docname1", SearchOptions.CaseSensitive, myHeader.Range);
                                myHeader.InsertText(docnamerange[0].Start, ds.Tables[0].Rows[0][3].ToString());
                                myHeader.Delete(docnamerange[0]);
                                /*Not using foreach loop*/
                                //foreach (DocumentRange documentRange in docnamerange)
                                //{
                                //    myHeader.InsertText(documentRange.Start, ds.Tables[0].Rows[0][3].ToString());
                                //    myHeader.Delete(documentRange);
                                //}
                                DocumentRange[] docnumrange = myHeader.FindAll("docnum1", SearchOptions.CaseSensitive, myHeader.Range);
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][6]) <= 9)
                                {
                                    myHeader.InsertText(docnumrange[0].Start, ds.Tables[0].Rows[0][4].ToString() + "-0" + ds.Tables[0].Rows[0][6].ToString());
                                    myHeader.Delete(docnumrange[0]);
                                }
                                else
                                {
                                    myHeader.InsertText(docnumrange[0].Start, ds.Tables[0].Rows[0][4].ToString() + "-" + ds.Tables[0].Rows[0][6].ToString());
                                    myHeader.Delete(docnumrange[0]);
                                }
                                /*Not using foreach loop*/
                                //foreach (DocumentRange documentRange in docnumrange)
                                //{
                                //    myHeader.InsertText(documentRange.Start, ds.Tables[0].Rows[0][4].ToString() + "-0" + ds.Tables[0].Rows[0][6].ToString());
                                //    myHeader.Delete(documentRange);
                                //}
                                DocumentRange[] effdaterange = myHeader.FindAll("effdate1", SearchOptions.CaseSensitive, myHeader.Range);
                                myHeader.InsertText(effdaterange[0].Start, ds.Tables[0].Rows[0][0].ToString());
                                myHeader.Delete(effdaterange[0]);
                                /*Not using foreach loop*/
                                //foreach (DocumentRange documentRange in effdaterange)
                                //{
                                //    myHeader.InsertText(documentRange.Start, ds.Tables[0].Rows[0][0].ToString());
                                //    myHeader.Delete(documentRange);
                                //}
                                DocumentRange[] revdaterange = myHeader.FindAll("revdate1", SearchOptions.CaseSensitive, myHeader.Range);
                                myHeader.InsertText(revdaterange[0].Start, ds.Tables[0].Rows[0][1].ToString());
                                myHeader.Delete(revdaterange[0]);
                                /*Not using foreach loop*/
                                //foreach (DocumentRange documentRange in revdaterange)
                                //{
                                //    myHeader.InsertText(documentRange.Start, ds.Tables[0].Rows[0][1].ToString());
                                //    myHeader.Delete(documentRange);
                                //}
                                firstSection.EndUpdateHeader(myHeader);
                                SubDocument myHeader1 = firstSection.BeginUpdateHeader(HeaderFooterType.Primary);
                                DocumentRange[] deptrange1 = myHeader1.FindAll("docdept1", SearchOptions.CaseSensitive, myHeader1.Range);
                                myHeader1.InsertText(deptrange1[0].Start, ds.Tables[0].Rows[0][2].ToString());
                                myHeader1.Delete(deptrange1[0]);
                                /*Not using foreach loop*/
                                //foreach (DocumentRange documentRange in deptrange1)
                                //{
                                //    myHeader1.InsertText(documentRange.Start, ds.Tables[0].Rows[0][2].ToString());
                                //    myHeader1.Delete(documentRange);
                                //}
                                DocumentRange[] typerange1 = myHeader1.FindAll("doctype1", SearchOptions.CaseSensitive, myHeader1.Range);
                                myHeader1.InsertText(typerange1[0].Start, ds.Tables[0].Rows[0][5].ToString());
                                myHeader1.Delete(typerange1[0]);
                                /*Not using foreach loop*/
                                //foreach (DocumentRange documentRange in typerange1)
                                //{
                                //    myHeader1.InsertText(documentRange.Start, ds.Tables[0].Rows[0][5].ToString());
                                //    myHeader1.Delete(documentRange);
                                //}
                                DocumentRange[] docnamerange1 = myHeader1.FindAll("docname1", SearchOptions.CaseSensitive, myHeader1.Range);
                                myHeader1.InsertText(docnamerange1[0].Start, ds.Tables[0].Rows[0][3].ToString());
                                myHeader1.Delete(docnamerange1[0]);
                                /*Not using foreach loop*/
                                //foreach (DocumentRange documentRange in docnamerange1)
                                //{
                                //    myHeader1.InsertText(documentRange.Start, ds.Tables[0].Rows[0][3].ToString());
                                //    myHeader1.Delete(documentRange);
                                //}
                                DocumentRange[] docnumrange1 = myHeader1.FindAll("docnum1", SearchOptions.CaseSensitive, myHeader1.Range);
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][6]) <= 9)
                                {
                                    myHeader1.InsertText(docnumrange1[0].Start, ds.Tables[0].Rows[0][4].ToString() + "-0" + ds.Tables[0].Rows[0][6].ToString());
                                    myHeader1.Delete(docnumrange1[0]);
                                }
                                else
                                {
                                    myHeader1.InsertText(docnumrange1[0].Start, ds.Tables[0].Rows[0][4].ToString() + "-" + ds.Tables[0].Rows[0][6].ToString());
                                    myHeader1.Delete(docnumrange1[0]);
                                }
                                /*Not using foreach loop*/
                                //foreach (DocumentRange documentRange in docnumrange1)
                                //{
                                //    myHeader1.InsertText(documentRange.Start, ds.Tables[0].Rows[0][4].ToString() + "-0" + ds.Tables[0].Rows[0][6].ToString());
                                //    myHeader1.Delete(documentRange);
                                //}
                                //DocumentRange[] effdaterange1 = myHeader1.FindAll("effdate1", SearchOptions.CaseSensitive, myHeader1.Range);
                                //myHeader1.InsertText(effdaterange1[0].Start, ds.Tables[0].Rows[0][0].ToString());
                                //myHeader1.Delete(effdaterange1[0]);
                                //foreach (DocumentRange documentRange in effdaterange1)
                                //{
                                //    myHeader1.InsertText(documentRange.Start, ds.Tables[0].Rows[0][0].ToString());
                                //    myHeader1.Delete(documentRange);
                                //}
                                //DocumentRange[] revdaterange1 = myHeader1.FindAll("revdate1", SearchOptions.CaseSensitive, myHeader1.Range);
                                //myHeader1.InsertText(revdaterange1[0].Start, ds.Tables[0].Rows[0][1].ToString());
                                //myHeader1.Delete(revdaterange1[0]);
                                //foreach (DocumentRange documentRange in revdaterange1)
                                //{
                                //    myHeader1.InsertText(documentRange.Start, ds.Tables[0].Rows[0][1].ToString());
                                //    myHeader1.Delete(documentRange);
                                //}
                                firstSection.EndUpdateHeader(myHeader1);
                                int rowcount = 0, tableCount = 0;
                                if (ds.Tables[4].Rows.Count > 0)
                                {
                                    tableCount = ds.Tables.Count - 1;
                                }
                                else
                                {
                                    tableCount = ds.Tables.Count - 2;
                                }
                                for (int i = 0; i < ds.Tables.Count; i++)
                                {
                                    rowcount = rowcount + ds.Tables[i].Rows.Count;
                                }
                                rowcount = rowcount - 1;
                                rowcount = (rowcount * 2) + tableCount;
                                document.InsertText(document.Range.Start, "\n");
                                DevExpress.XtraRichEdit.API.Native.Table tbl = document.Tables.Create(document.CaretPosition, rowcount, 2, AutoFitBehaviorType.AutoFitToWindow);
                                if (rowcount > 0)
                                {
                                    int tblheaderRow = 0;
                                    int drawRow = 1;
                                    StringBuilder tblHeaderText = new StringBuilder();
                                    for (int i = 1; i <= tableCount; i++)
                                    {
                                        switch (i)
                                        {
                                            case 1:
                                                tblHeaderText.Clear();
                                                tblHeaderText.Append("Prepared By");
                                                break;
                                            case 2:
                                                tblHeaderText.Clear();
                                                tblHeaderText.Append("Reviewed By");
                                                break;
                                            case 3:
                                                tblHeaderText.Clear();
                                                tblHeaderText.Append("Approved By");
                                                break;
                                            case 4:
                                                tblHeaderText.Clear();
                                                tblHeaderText.Append("Authorized By");
                                                break;
                                            default:
                                                break;
                                        }
                                        document.InsertText(tbl[tblheaderRow, 0].Range.Start, tblHeaderText.ToString());
                                        tbl[tblheaderRow, 0].BackgroundColor = System.Drawing.Color.LightGray;
                                        tbl[tblheaderRow, 1].BackgroundColor = System.Drawing.Color.LightGray;
                                        tbl[tblheaderRow, 0].Borders.Right.LineColor = System.Drawing.Color.LightGray;
                                        tbl[tblheaderRow, 1].Borders.Left.LineColor = System.Drawing.Color.LightGray;
                                        for (int j = 1; j <= ds.Tables[i].Rows.Count; j++)
                                        {
                                            document.InsertText(tbl[drawRow, 0].Range.Start, "Name:" + ds.Tables[i].Rows[j - 1][0].ToString());
                                            tbl[drawRow, 0].Borders.Right.LineColor = System.Drawing.Color.White;
                                            tbl[drawRow, 0].Borders.Bottom.LineColor = System.Drawing.Color.White;
                                            document.InsertText(tbl[drawRow, 1].Range.Start, "Designation:" + ds.Tables[i].Rows[j - 1][3].ToString());
                                            tbl[drawRow, 1].Borders.Left.LineColor = System.Drawing.Color.White;
                                            tbl[drawRow, 1].Borders.Bottom.LineColor = System.Drawing.Color.White;
                                            drawRow = drawRow + 1;
                                            document.InsertText(tbl[drawRow, 0].Range.Start, "Date:" + ds.Tables[i].Rows[j - 1][1].ToString());
                                            tbl[drawRow, 0].Borders.Right.LineColor = System.Drawing.Color.White;
                                            tbl[drawRow, 0].Borders.Top.LineColor = System.Drawing.Color.White;
                                            document.InsertText(tbl[drawRow, 1].Range.Start, "Department:" + ds.Tables[i].Rows[j - 1][2].ToString());
                                            tbl[drawRow, 1].Borders.Left.LineColor = System.Drawing.Color.White;
                                            tbl[drawRow, 1].Borders.Top.LineColor = System.Drawing.Color.White;
                                            drawRow = drawRow + 1;
                                        }
                                        tblheaderRow = drawRow;
                                        drawRow = drawRow + 1;
                                    }
                                }
                                document.InsertText(document.CaretPosition, Characters.PageBreak.ToString());
                                // string path = Server.MapPath(@"~/Files/FirstPage.docx");
                                string strFile1Upload = DynamicFolder.CreateDynamicFolder(4);
                                string File2Path = DateTime.Now.ToString("yyyyMMddHHmmss") + ".docx";
                                string path = (strFile1Upload + "\\" + File2Path);
                                // Delete the file if it exists.                           
                                document.SaveDocument(path, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                                RichEditDocumentServer server1 = new RichEditDocumentServer();
                                Document document1 = server1.Document;
                                document1.AppendDocumentContent(path, DevExpress.XtraRichEdit.DocumentFormat.OpenXml, path, InsertOptions.KeepSourceFormatting);
                                document1.AppendDocumentContent(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Rtf, strTargPath, InsertOptions.KeepSourceFormatting);
                                CharacterProperties chprps = document1.BeginUpdateCharacters(document1.Range);
                                chprps.ForeColor = System.Drawing.Color.Black;
                                document1.EndUpdateCharacters(chprps);
                                document1.SaveDocument(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
                                server.Dispose();
                                server1.Dispose();
                                //Marshal.ReleaseComObject(server);
                                //Marshal.ReleaseComObject(server1);
                                DocumentManager.CloseDocument(Guid.NewGuid().ToString());
                                ASPxRichEdit2.Open(strTargPath, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
                                ASPxRichEdit2.DocumentId = Guid.NewGuid().ToString();
                                /*Not using file delete*/
                                //if (File.Exists(strTargPath))
                                //{
                                //    // Note that no lock is put on the
                                //    // file and the possibility exists
                                //    // that another process could do
                                //    // something with it between
                                //    // the calls to Exists and Delete.
                                //    File.Delete(strTargPath);
                                //}
                                //if (File.Exists(path))
                                //{
                                //    // Note that no lock is put on the
                                //    // file and the possibility exists
                                //    // that another process could do
                                //    // something with it between
                                //    // the calls to Exists and Delete.
                                //    File.Delete(path);
                                //}
                            }
                            else
                            {
                                ASPxRichEdit2.Open(
                                Guid.NewGuid().ToString(),
                                    DevExpress.XtraRichEdit.DocumentFormat.Rtf,
                                    () =>
                                    {
                                        byte[] docBytes = (byte[])DMS_DT.Rows[0]["Content"];
                                        return new MemoryStream(docBytes);
                                    }
                                );
                            }
                        }
                        else
                        {
                            ASPxRichEdit2.Open(
                            Guid.NewGuid().ToString(),
                                DevExpress.XtraRichEdit.DocumentFormat.Rtf,
                                () =>
                                {
                                    byte[] docBytes = (byte[])DMS_DT.Rows[0]["Content"];
                                    return new MemoryStream(docBytes);
                                }
                            );
                        }
                    }
                    else
                    {
                        divliteral.Visible = true;
                        richedit2.Visible = false;
                        literalSop1.Text = @" <b style=""color:red;"">" + "File Not Available To View " + "</b> ";
                    }
                }
            }
            catch (Exception)
            {
                divliteral.Visible = true;
                richedit2.Visible = false;
                literalSop1.Text = "File Not Available To View, Bad data ";
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }
    }
}