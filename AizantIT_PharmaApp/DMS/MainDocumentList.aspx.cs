﻿using AizantIT_DMSBAL;
using AizantIT_DMSBO;
using AizantIT_PharmaApp.Common;
using AizantIT_PharmaApp.DMS.WebServices;
using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace AizantIT_PharmaApp.DMS
{
    public partial class MainDocumentList : System.Web.UI.Page
    {
        DocumentCreationBAL DMS_Bal = new DocumentCreationBAL();
        DocumentManagmentBAL DMS_BalDM = new DocumentManagmentBAL();
        DocObjects docObjects = new DocObjects();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            ConfirmCustomAlert.buttonClick += new EventHandler(ConfirmCustomAlert_buttonClick);
            if (HelpClass.IsUserAuthenticated())
            {
                if (!IsPostBack)
                {
                    hdnEmpID.Value = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                    InitializeThePage();
                    DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                    DataTable dtTemp = new DataTable();
                    DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                    if (drUMS.Length > 0)
                    {
                        dtTemp = drUMS.CopyToDataTable();
                        if (dtTemp.Select("RoleID=2").Length > 0)//admin role
                        {
                            hdfRole.Value = "2";
                        }
                        else if (dtTemp.Select("RoleID=30").Length > 0)//Controller role
                        {
                            hdfRole.Value = "30";
                        }
                        else if (dtTemp.Select("RoleID=9").Length > 0 || dtTemp.Select("RoleID=10").Length > 0 || dtTemp.Select("RoleID=11").Length > 0 || dtTemp.Select("RoleID=12").Length > 0 || dtTemp.Select("RoleID=13").Length > 0)
                        {
                            hdfRole.Value = "0";
                        }
                        else
                        {
                            loadAuthorizeErrorMsg();
                        }
                    }
                    else
                    {
                        Response.Redirect("~/UserLogin.aspx", false);
                    }
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyFun1", "getadmincolumn();", true);
            }
            else
            {
                Response.Redirect("~/UserLogin.aspx", false);
            }
        }
        protected void btnFileView_Click(object sender, EventArgs e)
        {
            btnFileView.Enabled = false;
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfVID.Value);
                hdfStatus.Value = DMS_DT.Rows[0]["DocStatus"].ToString();
                span_FileTitle.InnerHtml = "<b>" + DMS_DT.Rows[0]["DocumentName"].ToString() + "</b>";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_lock').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                if (hdfStatus.Value == "A" || hdfStatus.Value == "I" || hdfStatus.Value == "R")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view1", "LoadfinalPDFDocument();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view1", "LoadPDFDocument();", true);
                }
                hdfViewType.Value = "0";
                hdfViewEmpID.Value = "0";
                hdfViewRoleID.Value = "11";//author               
                UpHeading.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MDL_M1:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MDL_M1:" + strline + "  " + "Document Viewing failed.", "error");
            }
            finally
            {
                btnFileView.Enabled = true;
            }
        }
        protected void btnFileView1_Click(object sender, EventArgs e)
        {
            btnFileView1.Enabled = false;
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfVID.Value);
                hdfStatus.Value = DMS_DT.Rows[0]["DocStatus"].ToString();
                span_FileTitle.InnerHtml = "<b>" + DMS_DT.Rows[0]["DocumentName"].ToString() + "</b>";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_lock').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                if (hdfStatus.Value == "A" || hdfStatus.Value == "I" || hdfStatus.Value == "R")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view2", "LoadfinalPDFDocument();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "view2", "LoadPDFDocument();", true);
                }
                hdfViewType.Value = "0";
                hdfViewEmpID.Value = "0";
                hdfViewRoleID.Value = "11";//author

                UpHeading.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MDL_M2:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MDL_M2:" + strline + "  " + "Document Viewing1 failed.", "error");
            }
            finally
            {
                btnFileView1.Enabled = true;
            }
        }
        protected void btnCommentsHistoryView_Click(object sender, EventArgs e)
        {
            btnCommentsHistoryView.Enabled = false;
            try
            {
                DataTable DMS_DT = DMS_BalDM.DMS_GetComments(hdfVID.Value);
                gv_CommentHistory.DataSource = DMS_DT;
                ViewState["HistoryDataTable"] = DMS_DT;
                gv_CommentHistory.DataBind();
                UpGridButtons.Update();
                if (DMS_DT.Rows.Count > 0)
                {
                    span4.Attributes.Add("title", DMS_DT.Rows[0]["DocumentTitle"].ToString());
                    if (DMS_DT.Rows[0]["DocumentTitle"].ToString().Length > 60)
                    {
                        span4.InnerHtml = DMS_DT.Rows[0]["DocumentTitle"].ToString().Substring(0, 60) + "...";
                    }
                    else
                    {
                        span4.InnerHtml = DMS_DT.Rows[0]["DocumentTitle"].ToString();
                    }
                    upcomment.Update();
                }
                else
                {
                    span4.InnerText = "Comment History";
                    upcomment.Update();
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myCommentHistory').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MDL_M3:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MDL_M3:" + strline + "  " + "History viewing failed.", "error");
            }
            finally
            {
                btnCommentsHistoryView.Enabled = true;
            }
        }
        private void InitializeThePage()
        {
            try
            {
                Session["ChartStatus"] = "0";
                Session["ChartDeptCode"] = "";
                Session["ChartDocTypeName"] = "";
                Session["ChartDocumentType"] = 0;
                Session["ChartDepartment"] = 0;
                Session["ChartFromDate"] = "";
                Session["ChartToDate"] = "";
                if (Request.QueryString.Count > 0)
                {
                    if (Request.QueryString["cstatus"].ToString() == "1")
                    {
                        Session["ChartStatus"] = Request.QueryString["cstatus"];
                        if (Request.QueryString["dcode"] != null)
                        {
                            string DepartmentCode = (Request.QueryString["dcode"]);
                            Session["ChartDeptCode"] = DepartmentCode;
                        }
                        else
                        {
                            Session["ChartDeptCode"] = string.Empty;
                        }
                        if (Request.QueryString["dtype"] != null)
                        {
                            string DocumentTypeName = (Request.QueryString["dtype"]);
                            Session["ChartDocTypeName"] = DocumentTypeName;
                        }
                        else
                        {
                            Session["ChartDocTypeName"] = string.Empty;
                        }
                        string DocumentTypeID = (Request.QueryString["dtypeID"]);
                        Session["ChartDocumentType"] = DocumentTypeID;
                        string DepartmentID = (Request.QueryString["dID"]);
                        Session["ChartDepartment"] = DepartmentID;
                    }
                    else if (Request.QueryString["cstatus"].ToString() == "3")
                    {
                        Session["ChartStatus"] = Request.QueryString["cstatus"];
                        string DepartmentCode = (Request.QueryString["dcode"]);
                        Session["ChartDeptCode"] = DepartmentCode;
                        string fdate = Request.QueryString["fdate"];
                        fdate = fdate.Replace("%20", " ");
                        Session["ChartFromDate"] = fdate;
                        string tdate = Request.QueryString["tdate"];
                        tdate = tdate.Replace("%20", " ");
                        Session["ChartToDate"] = tdate;
                    }
                    else if (Request.QueryString["cstatus"].ToString() == "2")
                    {
                        Session["ChartStatus"] = Request.QueryString["cstatus"];
                        if (Request.QueryString["dcode"] != null)
                        {
                            string DepartmentCode = (Request.QueryString["dcode"]);
                            Session["ChartDeptCode"] = DepartmentCode;
                        }
                        else
                        {
                            Session["ChartDeptCode"] = string.Empty;
                        }
                        if (Request.QueryString["dtype"] != null)
                        {
                            string DocumentTypeName = (Request.QueryString["dtype"]);
                            Session["ChartDocTypeName"] = DocumentTypeName;
                        }
                        else
                        {
                            Session["ChartDocTypeName"] = string.Empty;
                        }
                        string DocumentTypeID = (Request.QueryString["dtypeID"]);
                        Session["ChartDocumentType"] = DocumentTypeID;
                        string DepartmentID = (Request.QueryString["dID"]);
                        Session["ChartDepartment"] = DepartmentID;
                        string fdate = Request.QueryString["fdate"];
                        fdate = fdate.Replace("%20", " ");
                        Session["ChartFromDate"] = fdate;
                        string tdate = Request.QueryString["tdate"];
                        tdate = tdate.Replace("%20", " ");
                        Session["ChartToDate"] = tdate;
                    }
                    else if (Request.QueryString["cstatus"].ToString() == "4")
                    {
                        Session["ChartStatus"] = Request.QueryString["cstatus"];
                        if (Request.QueryString["dcode"] != null)
                        {
                            string DepartmentCode = (Request.QueryString["dcode"]);
                            Session["ChartDeptCode"] = DepartmentCode;
                        }
                        else
                        {
                            Session["ChartDeptCode"] = string.Empty;
                        }
                        if (Request.QueryString["dtype"] != null)
                        {
                            string DocumentTypeName = (Request.QueryString["dtype"]);
                            Session["ChartDocTypeName"] = DocumentTypeName;
                        }
                        else
                        {
                            Session["ChartDocTypeName"] = string.Empty;
                        }
                        string DocumentTypeID = (Request.QueryString["dtypeID"]);
                        Session["ChartDocumentType"] = DocumentTypeID;
                        string DepartmentID = (Request.QueryString["dID"]);
                        Session["ChartDepartment"] = DepartmentID;
                    }
                    else if (Request.QueryString["cstatus"].ToString() == "5")
                    {
                        Session["ChartStatus"] = Request.QueryString["cstatus"];
                        if (Request.QueryString["dcode"] != null)
                        {
                            string DepartmentCode = (Request.QueryString["dcode"]);
                            Session["ChartDeptCode"] = DepartmentCode;
                        }
                        else
                        {
                            Session["ChartDeptCode"] = string.Empty;
                        }
                        if (Request.QueryString["dtype"] != null)
                        {
                            string DocumentTypeName = (Request.QueryString["dtype"]);
                            Session["ChartDocTypeName"] = DocumentTypeName;
                        }
                        else
                        {
                            Session["ChartDocTypeName"] = string.Empty;
                        }
                        string DocumentTypeID = (Request.QueryString["dtypeID"]);
                        Session["ChartDocumentType"] = DocumentTypeID;
                        string DepartmentID = (Request.QueryString["dID"]);
                        Session["ChartDepartment"] = DepartmentID;
                    }
                    else if (Request.QueryString["cstatus"].ToString() == "6")
                    {
                        Session["ChartStatus"] = Request.QueryString["cstatus"];
                        if (Request.QueryString["dcode"] != null)
                        {
                            string DepartmentCode = (Request.QueryString["dcode"]);
                            Session["ChartDeptCode"] = DepartmentCode;
                        }
                        else
                        {
                            Session["ChartDeptCode"] = string.Empty;
                        }
                        if (Request.QueryString["dtype"] != null)
                        {
                            string DocumentTypeName = (Request.QueryString["dtype"]);
                            Session["ChartDocTypeName"] = DocumentTypeName;
                        }
                        else
                        {
                            Session["ChartDocTypeName"] = string.Empty;
                        }
                        string DocumentTypeID = (Request.QueryString["dtypeID"]);
                        Session["ChartDocumentType"] = DocumentTypeID;
                        string DepartmentID = (Request.QueryString["dID"]);
                        Session["ChartDepartment"] = DepartmentID;
                    }
                    else if (Request.QueryString["cstatus"].ToString() == "7")
                    {
                        Session["ChartStatus"] = Request.QueryString["cstatus"];
                        if (Request.QueryString["dcode"] != null)
                        {
                            string DepartmentCode = (Request.QueryString["dcode"]);
                            Session["ChartDeptCode"] = DepartmentCode;
                        }
                        else
                        {
                            Session["ChartDeptCode"] = string.Empty;
                        }
                        if (Request.QueryString["dtype"] != null)
                        {
                            string DocumentTypeName = (Request.QueryString["dtype"]);
                            Session["ChartDocTypeName"] = DocumentTypeName;
                        }
                        else
                        {
                            Session["ChartDocTypeName"] = string.Empty;
                        }
                        string DocumentTypeID = (Request.QueryString["dtypeID"]);
                        Session["ChartDocumentType"] = DocumentTypeID;
                        string DepartmentID = (Request.QueryString["dID"]);
                        Session["ChartDepartment"] = DepartmentID;
                    }
                    else if (Request.QueryString["cstatus"].ToString() == "8")
                    {
                        Session["ChartStatus"] = Request.QueryString["cstatus"];
                        if (Request.QueryString["dcode"] != null)
                        {
                            string DepartmentCode = (Request.QueryString["dcode"]);
                            Session["ChartDeptCode"] = DepartmentCode;
                        }
                        else
                        {
                            Session["ChartDeptCode"] = string.Empty;
                        }
                        if (Request.QueryString["dtype"] != null)
                        {
                            string DocumentTypeName = (Request.QueryString["dtype"]);
                            Session["ChartDocTypeName"] = DocumentTypeName;
                        }
                        else
                        {
                            Session["ChartDocTypeName"] = string.Empty;
                        }
                        string DocumentTypeID = (Request.QueryString["dtypeID"]);
                        Session["ChartDocumentType"] = DocumentTypeID;
                        string DepartmentID = (Request.QueryString["dID"]);
                        Session["ChartDepartment"] = DepartmentID;
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MDL_M4:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MDL_M4:" + strline + "  " + "Page Initialization failed.", "error");
            }
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            btnEdit.Enabled = false;
            try
            {
                DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfVID.Value);
                span1.InnerHtml = DMS_DT.Rows[0]["DocumentNumber"].ToString();
                if (Convert.ToBoolean(DMS_DT.Rows[0]["isPublic"].ToString()) == true)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "enableRadioPublic();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop2", "disableRadioPublic();", true);
                }
                txt_Remarks.Value = "";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myDocumentPrivacyEdit').modal({ show: true, backdrop: 'static', keyboard: false });", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), DateTime.Now.ToString("hh.mm.ss.fff"), "hideImg();", true);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MDL_M5:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MDL_M5:" + strline + "  " + "Page Initialization failed.", "error");
            }
            finally
            {
                btnEdit.Enabled = true;
            }
        }
        protected void ConfirmCustomAlert_buttonClick(object sender, EventArgs e)
        {
            try
            {
                if (hdnConfirm.Value == "Update")
                {
                    StringBuilder sbErrorMsg = new StringBuilder();
                    if (txt_Remarks.Value.Trim() == "")
                    {
                        sbErrorMsg.Append("Enter Comments." + "<br/>");
                    }
                    if (sbErrorMsg.Length > 8)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "custAlertMsg(' <br/>    " + sbErrorMsg.ToString() + "','error');", true);
                        return;
                    }
                    int DMS_ins;
                    docObjects.DocumentID = hdfVID.Value;
                    docObjects.DMS_IntiatedBy = Convert.ToInt32(hdnEmpID.Value);
                    docObjects.Remarks = txt_Remarks.Value.Trim();
                    DMS_ins = DMS_BalDM.DMS_UpdateDocPrivacyLevel(docObjects, (rYes.Checked == true ? true : false));
                    DataTable DMS_DT = DMS_Bal.DMS_GetDocByID(hdfVID.Value);
                    HelpClass.custAlertMsgWithFunc(this, this.GetType(), "Document Privacy Level for Document Number <b>" + DMS_DT.Rows[0]["DocumentNumber"].ToString() + "</b> Updated Successfully.", "success", "ReloadCurrentPage()");
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MDL_M6:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MDL_M6:" + strline + "  " + "Document submit/update failed", "error");
            }
        }
        private void loadAuthorizeErrorMsg()
        {
            lblAutorizedMsg.Text = "Your Are Not Authorized to This Page.";
            divAutorizedMsg.Visible = true;
            divMainContainer.Visible = false;
        }
        protected void gv_CommentHistory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string _fullcomments = (e.Row.Cells[5].FindControl("txt_Comments") as TextBox).Text;
                    LinkButton _lnkmorecomments = (e.Row.Cells[5].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (e.Row.Cells[5].FindControl("lbcommentsless") as LinkButton);
                    if (_fullcomments.Length > 70)
                    {
                        (e.Row.Cells[5].FindControl("lbl_shortComments") as Label).Visible = true;
                        TextBox FullTbx = (e.Row.Cells[5].FindControl("txt_Comments") as TextBox);
                        (e.Row.Cells[5].FindControl("lbl_shortComments") as Label).Text = _fullcomments.Substring(0, 70) + "..";
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = true;

                        if (_fullcomments.Length > 250 && _fullcomments.Length <= 500)
                        {
                            FullTbx.Rows = 6;
                        }
                        else if (_fullcomments.Length > 500 && _fullcomments.Length <= 1000)
                        {
                            FullTbx.Rows = 9;
                        }
                        else if (_fullcomments.Length > 1000 && _fullcomments.Length <= 1500)
                        {
                            FullTbx.Rows = 12;
                        }
                        else if (_fullcomments.Length > 1500 && _fullcomments.Length <= 2000)
                        {
                            FullTbx.Rows = 15;
                        }
                        else if (_fullcomments.Length > 2000)
                        {
                            FullTbx.Rows = 20;
                        }
                        FullTbx.Visible = false;
                    }
                    else
                    {
                        (e.Row.Cells[5].FindControl("lbl_shortComments") as Label).Visible = true;
                        (e.Row.Cells[5].FindControl("txt_Comments") as TextBox).Visible = false;
                        _lnklesscomments.Visible = false;
                        _lnkmorecomments.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MDL_M7:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MDL_M7:" + strline + "  " + "Comment History row data-bound failed.", "error");
            }
        }
        protected void gv_CommentHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "SM")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    LinkButton _lnkmorecomments = (row.Cells[5].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (row.Cells[5].FindControl("lbcommentsless") as LinkButton);
                    (row.Cells[5].FindControl("lbl_shortComments") as Label).Visible = false;
                    (row.Cells[5].FindControl("txt_Comments") as TextBox).Visible = true;
                    _lnklesscomments.Visible = true;
                    _lnkmorecomments.Visible = false;
                }
                else if (e.CommandName == "SL")
                {
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    LinkButton _lnkmorecomments = (row.Cells[5].FindControl("lbcommentsmore") as LinkButton);
                    LinkButton _lnklesscomments = (row.Cells[5].FindControl("lbcommentsless") as LinkButton);
                    (row.Cells[5].FindControl("lbl_shortComments") as Label).Visible = true;
                    (row.Cells[5].FindControl("txt_Comments") as TextBox).Visible = false;
                    _lnklesscomments.Visible = false;
                    _lnkmorecomments.Visible = true;
                }
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MDL_M8:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MDL_M8:" + strline + "  " + "Comment History row command failed.", "error");
            }
        }
        protected void gv_CommentHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_CommentHistory.PageIndex = e.NewPageIndex;
                gv_CommentHistory.DataSource = (DataTable)(ViewState["HistoryDataTable"]);
                gv_CommentHistory.DataBind();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Aizant_log.Error("MDL_M9:" + strline + "  " + strMsg);
                HelpClass.custAlertMsg(this, this.GetType(), "MDL_M9:" + strline + "  " + "grid view page index changing failed.", "error");
            }
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Aizant_log.Error(ex.Message);
        }

        protected void btnAdmintblprint_Click(object sender, EventArgs e)
        {
            //DMSService service = new DMSService();
            //string EmpName = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString();
            //string base64String = service.PrintMainDocList("N", EmpName);
            //byte[] bytes = Convert.FromBase64String(base64String);
            //System.IO.MemoryStream ms = new System.IO.MemoryStream(bytes);
            //Response.Clear();
            //Response.Buffer = true;
            //Response.ContentType = "application/pdf";
            //string _dwnFileName = "AdminMainList_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".pdf";
            //Response.AddHeader("content-disposition", "attachment;filename=" + _dwnFileName);
            //ms.WriteTo(Response.OutputStream);
            //Response.End();
        }
    }
}