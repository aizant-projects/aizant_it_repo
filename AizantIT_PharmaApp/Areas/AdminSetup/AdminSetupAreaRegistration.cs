﻿using System.Web.Mvc;

namespace AizantIT_PharmaApp.Areas.AdminSetup
{
    public class AdminSetupAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "AdminSetup";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "AdminSetup_default",
                "AdminSetup/{controller}/{action}/{id}",
                 //new { action = "Index", id = UrlParameter.Optional }
                 new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}