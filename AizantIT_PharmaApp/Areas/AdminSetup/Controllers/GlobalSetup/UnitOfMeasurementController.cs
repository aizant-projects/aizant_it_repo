﻿using AizantIT_PharmaApp.Common.CustomFilters;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
//using Models.GlobalSetupBO.UOM_BO;


using static Aizant_Enums.AizantEnums;
using Aizant_API_Entities;
using AizantIT_PharmaApp.Areas.AdminSetup.Models.GlobalSetupBO;
using System.Data;

namespace AizantIT_PharmaApp.Areas.AdminSetup.Controllers.GlobalSetup
{
    [Route("AdminSetup/GlobalSetup/BusinessDivisionAndSiteController/{action}")]
    [RouteArea]
    [CustAuthentication]
    public class UnitOfMeasurementController : Controller
    {
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
       private AizantIT_DevEntities dbEF = new AizantIT_DevEntities();
        //Models.GlobalSetupBO.UOM_BO UnitOfMeasurementBO = new Models.GlobalSetupBO.UOM_BO();
        

        // GET: AdminSetup/UnitOfMeasurement
        [CustAuthorization((int)Modules.AdminSetup,
        (int)AdminSetup_UserRole.GlobalAdmin_UMS)]
        public ActionResult UnitOfMeasurement()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.SysError = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber()) + " :" + ex.Message.Replace("'", "\'");
                Aizant_log.Error(ViewBag.SysError);
                return View();
            }
        }
       [HttpPost]
        public JsonResult JsonCreateUnitOfMeasurement(UOM_BO aizantIT_UOMMaster)
        {
            try
            {
                try
                {
                    using (AizantIT_DevEntities dbAizantIT_API = new AizantIT_DevEntities())
                    {
                        using (DbContextTransaction transaction = dbAizantIT_API.Database.BeginTransaction())
                        {
                            try
                            {
                                bool UOMName = dbAizantIT_API.AizantIT_UOMMaster.Any(x => x.UOMName.Trim() == aizantIT_UOMMaster.UOM_Name.Trim() && x.UOMID != aizantIT_UOMMaster.UOM_ID);
                                bool Description = dbAizantIT_API.AizantIT_UOMMaster.Any(y => y.UOMDescription.Trim() == aizantIT_UOMMaster.UOM_Description.Trim() && y.UOMID != aizantIT_UOMMaster.UOM_ID);
                                if (UOMName|| Description)
                                {
                                    if (UOMName == true && Description == false)
                                    {
                                        return Json(new { msg = "UOM Name already exists", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                    }
                                    else if (UOMName == false && Description == true)
                                    {
                                        return Json(new { msg = "UOM Description already exists", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                    }
                                    else
                                    {
                                        return Json(new { msg = "UOM Name & Description already exists", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                                else
                                {
                                    AizantIT_UOMMaster objAizantIT_UOM = new AizantIT_UOMMaster();
                                    objAizantIT_UOM.UOMName = aizantIT_UOMMaster.UOM_Name;
                                    objAizantIT_UOM.UOMDescription = aizantIT_UOMMaster.UOM_Description;
                                    dbAizantIT_API.AizantIT_UOMMaster.Add(objAizantIT_UOM);
                                    dbAizantIT_API.SaveChanges();
                                    //1 for Creation of UOM
                                    InsertIntoUOM_MasterAudit(objAizantIT_UOM.UOMID, 1, "UOM Name : " + objAizantIT_UOM.UOMName + ", UOM Description : " + objAizantIT_UOM.UOMDescription, aizantIT_UOMMaster.UOM_Comments == "" ? null : aizantIT_UOMMaster.UOM_Comments);
                                    transaction.Commit();
                                    return Json(new { msg = "UOM Created.", msgType = "success" }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                throw ex;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string ExceptionMsg = "Error " + (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber()) + ":" + ex.Message.Replace("'", "\'");
                    Aizant_log.Error(ExceptionMsg);
                    return Json(new { msg = ExceptionMsg, msgType = "error" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + ex.Message;
                return Json(new { msg = message.Replace("'", "\'"), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult JsonUpdateUnitOfMeasurement(UOM_BO aizantIT_UOMMaster)
        {
            try
            {
                try
                {
                    using (AizantIT_DevEntities dbAizantIT_API = new AizantIT_DevEntities())
                    {
                        using (DbContextTransaction transaction = dbAizantIT_API.Database.BeginTransaction())
                        {
                            try
                            {
                                bool IsRecordExists = (from itemUOM in dbAizantIT_API.AizantIT_UOMMaster
                                                       where itemUOM.UOMID == aizantIT_UOMMaster.UOM_ID
                                                       select new
                                                       {
                                                           itemUOM.UOMID
                                                       }).Any();
                                if (IsRecordExists)
                                {
                                    AizantIT_UOMMaster objAizantIT_UOMMaster =
                                    dbAizantIT_API.AizantIT_UOMMaster.SingleOrDefault(p => p.UOMID == aizantIT_UOMMaster.UOM_ID);
                                    if (objAizantIT_UOMMaster.UOMName != aizantIT_UOMMaster.UOM_Name || objAizantIT_UOMMaster.UOMDescription != aizantIT_UOMMaster.UOM_Description)
                                    {
                                        bool UOMName = dbAizantIT_API.AizantIT_UOMMaster.Any(x => x.UOMName.Trim() == aizantIT_UOMMaster.UOM_Name.Trim() && x.UOMID != aizantIT_UOMMaster.UOM_ID);
                                        bool Description = dbAizantIT_API.AizantIT_UOMMaster.Any(y => y.UOMDescription.Trim() == aizantIT_UOMMaster.UOM_Description.Trim() && y.UOMID != aizantIT_UOMMaster.UOM_ID);
                                        if (UOMName || Description)
                                        {
                                            if (UOMName == true && Description == false)
                                            {
                                                return Json(new { msg = "UOM Name already exists", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                            }
                                            else if (UOMName == false && Description == true)
                                            {
                                                return Json(new { msg = "UOM Description already exists", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                            }
                                            else
                                            {
                                                return Json(new { msg = "UOM Name & Description already exists", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                            }
                                        }
                                        else
                                        {
                                            AizantIT_UOMMaster objAizantIT_UOM = new AizantIT_UOMMaster();
                                            objAizantIT_UOM = dbAizantIT_API.AizantIT_UOMMaster.SingleOrDefault(x => x.UOMID == aizantIT_UOMMaster.UOM_ID);
                                            objAizantIT_UOM.UOMName = aizantIT_UOMMaster.UOM_Name;
                                            objAizantIT_UOM.UOMDescription = aizantIT_UOMMaster.UOM_Description;
                                            dbAizantIT_API.SaveChanges();
                                            //2 for Update of UOM
                                            InsertIntoUOM_MasterAudit(objAizantIT_UOM.UOMID, 2, "UOM Name : " + objAizantIT_UOM.UOMName + ", UOM Description : " + objAizantIT_UOM.UOMDescription, aizantIT_UOMMaster.UOM_Comments == "" ? null : aizantIT_UOMMaster.UOM_Comments);
                                            transaction.Commit();
                                            return Json(new { msg = "UOM Updated.", msgType = "success" }, JsonRequestBehavior.AllowGet);
                                        }
                                    }
                                    else
                                    {
                                        return Json(new { msg = "There Are No Changes To Update.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                                else
                                {
                                    return Json(new { msg = "UOM does not exist.", msgType = "error" }, JsonRequestBehavior.AllowGet);
                                }

                            }
                            catch (Exception)
                            {
                                transaction.Rollback();
                                throw;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string ExceptionMsg = "Error " + (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber()) + ":" + ex.Message.Replace("'", "\'");
                    Aizant_log.Error(ExceptionMsg);
                    return Json(new { msg = ExceptionMsg, msgType = "error" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + ex.Message;
                return Json(new { msg = message.Replace("'", "\'"), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public void InsertIntoUOM_MasterAudit(int UOM_ID, int ActionStatusID,string UOM_Data, string UOM_Comments = null)
        {
            using (AizantIT_DevEntities dbAizantIT_API1 = new AizantIT_DevEntities())
            {
                AizantIT_UOMMasterAudit objAizantIT_UOM_Audit = new AizantIT_UOMMasterAudit();
                objAizantIT_UOM_Audit.UOMID = UOM_ID;
                objAizantIT_UOM_Audit.ActionBY = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                objAizantIT_UOM_Audit.RoleID = (int)AdminSetup_UserRole.GlobalAdmin_UMS;
                objAizantIT_UOM_Audit.ActionDate = DateTime.Now;
                objAizantIT_UOM_Audit.ActionStatusID = ActionStatusID;
                objAizantIT_UOM_Audit.SubmittedData = UOM_Data;
                objAizantIT_UOM_Audit.Comments = UOM_Comments;
                dbAizantIT_API1.AizantIT_UOMMasterAudit.Add(objAizantIT_UOM_Audit);
                dbAizantIT_API1.SaveChanges();
            }
        }
       
        [HttpGet]
        public JsonResult UnitOfMeasurementJqueryList()
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];

            List<UOM_BO> objUOMList = new List<UOM_BO>();
            try
            {
                int TotalRecordsCount = 0;
                using (AizantIT_DevEntities objDEV_Entities = new AizantIT_DevEntities())
                {
                    var UOM_List = objDEV_Entities.AizantIT_SP_GetUnitOfMeasurementListData(
                        displayLength,
                        displayStart,
                        sortCol,
                        sSortDir,
                        sSearch
                        );
                    foreach(var item in UOM_List)
                    {
                        if(TotalRecordsCount ==0)
                        {
                            TotalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objUOMList.Add(new UOM_BO(Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.UOMID), item.UOMName, item.UOMDescription));
                    }
                }
                return Json(new
                {
                    iTotalRecords = TotalRecordsCount,
                    iTotalDisplayRecords = TotalRecordsCount,
                    aaData = objUOMList
                }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = "Yes";
                return Json(new { msg = "Error " + LineNo + " :" + ex.Message.Replace("'", "\'"), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
            //return View();
        }

        public JsonResult UOM_HistoryDetails( )
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            int UOM_ID = 0;
            int.TryParse(Request.Params["UOM_ID"], out UOM_ID);
            List<UOM_HistoryBO> objHistoryUOM_List = new List<UOM_HistoryBO>();
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities objDEV_Entitie = new AizantIT_DevEntities())
                {
                    var UOM_HistoryList = objDEV_Entitie.AizantIT_SP_GetUnitOfMeasurementHistoryListData(
                                                    displayLength,
                                                    displayStart,
                                                    sortCol,
                                                    sSortDir,
                                                    sSearch != null ? sSearch.Trim() : "",
                                                    UOM_ID);

                    foreach (var item in UOM_HistoryList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objHistoryUOM_List.Add(new UOM_HistoryBO(Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.PKID), item.ActionByName, item.RoleName, item.ActionDate, item.ActionStatusName, item.SubmittedData, item.Comments));
                    }

                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objHistoryUOM_List
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + ex.Message.Replace("'", "\'"), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }


            //return View();
        }
    }
}