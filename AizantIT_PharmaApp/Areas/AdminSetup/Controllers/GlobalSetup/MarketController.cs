﻿using Aizant_API_Entities;
using AizantIT_PharmaApp.Areas.AdminSetup.Models;
using AizantIT_PharmaApp.Areas.PRODUCT.Models.DataLayer;
using AizantIT_PharmaApp.Common.CustomFilters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.Areas.AdminSetup.Controllers.GlobalSetup
{
    [Route("AdminSetup/GlobalSetup/MarketController/{action}")]
    [RouteArea]
    [CustAuthentication]
    public class MarketController : Controller
    {
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // GET: AdminSetup/Market
        [CustAuthorization((int)Modules.AdminSetup,
          (int)AdminSetup_UserRole.GlobalAdmin_UMS)]
        public ActionResult Market()
        {
            try
            {
               return View();
            }
            catch (Exception ex)
            {
                ViewBag.SysError = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber()) + " :" + ex.Message.Replace("'", "\'");
                Aizant_log.Error(ViewBag.SysError);
                return View();
            }
        }
        //GetMarketList
        [HttpGet]
       public ActionResult GetMarketList()
       {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            List<MasterProperties> objmarketList = new List<MasterProperties>();
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities objDEVEntities = new AizantIT_DevEntities())
                {
                    var MarketList = objDEVEntities.AizantIT_SP_GetMarketListData(
                                                    displayLength,
                                                    displayStart,
                                                    sortCol,
                                                    sSortDir,
                                                    sSearch);

                    foreach (var item in MarketList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objmarketList.Add(new MasterProperties(Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.MarketID), item.MarketName, item.MarketCode));
                    }
                    return Json(new
                    {
                        iTotalRecords = totalRecordsCount,
                        iTotalDisplayRecords = totalRecordsCount,
                        aaData = objmarketList
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = "Yes";
                return Json(new { msg = "Error " + LineNo + " :" + ex.Message.Replace("'", "\'"), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}
