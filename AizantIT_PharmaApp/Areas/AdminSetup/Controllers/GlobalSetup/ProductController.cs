﻿using Aizant_API_Entities;
using AizantIT_PharmaApp.Areas.AdminSetup.Models.GlobalSetupBO;
using AizantIT_PharmaApp.Areas.AdminSetup.Models.DataLayer;
using AizantIT_PharmaApp.Areas.PRODUCT.Models.ModelProductList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Aizant_Enums.AizantEnums;
using System.Data.Entity;
using AizantIT_PharmaApp.Common.CustomFilters;

namespace AizantIT_PharmaApp.Areas.AdminSetup.Controllers.GlobalSetup
{
    [Route("AdminSetup/GlobalSetup/ProductController/{action}")]
    [RouteArea]
    [CustAuthentication]
    public class ProductController : Controller
    {
        // GET: AdminSetup/Product
        public ActionResult Index()
        {
            return View();
        }

        [CustAuthorization((int)Modules.AdminSetup,
          (int)AdminSetup_UserRole.GlobalAdmin_UMS)]
        public ActionResult Product()
        {
            return View();
        }

        //Add the user ProductListDetails
        public JsonResult AddProductListDetails(ProductBO objProductlist)
        {
            try
            {
                using (AizantIT_DevEntities objDEVEntitie = new AizantIT_DevEntities())
                {
                    AizantIT_ProductMaster objProductmaster = new AizantIT_ProductMaster();
                    objProductmaster.ProductName = objProductlist.ProductName;
                    var Msg = "";
                    bool productCode = objDEVEntitie.AizantIT_ProductMaster.Any(x => x.ProductCode.ToLower().Trim() == objProductlist.ProductCode.ToLower().Trim());
                    if (productCode)
                    {
                        Msg = "Product Code Already Exists" + "<br/>";
                    }
                    if (Msg != "")
                    {
                        return Json(new { hasError = "Warning", data = Msg }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objProductmaster.ProductCode = objProductlist.ProductCode;
                        objDEVEntitie.AizantIT_ProductMaster.Add(objProductmaster);
                        objDEVEntitie.SaveChanges();

                        //ProductMasterAudit Table
                        AizantIT_Product_History objProdMasterAudit = new AizantIT_Product_History();
                        objProdMasterAudit.ProductID = objProductmaster.ProductID;
                        objProdMasterAudit.ActionBy = Convert.ToInt32(((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                        objProdMasterAudit.RoleID = Convert.ToInt32(AdminSetup_UserRole.GlobalAdmin_UMS);//Role for(product[Admin])
                        objProdMasterAudit.ActionDate = DateTime.Now;
                        objProdMasterAudit.ActionStatus = 1;//ActionName(Created By Admin)
                        objProdMasterAudit.Comments = "N/A";
                        objDEVEntitie.AizantIT_Product_History.Add(objProdMasterAudit);
                        objDEVEntitie.SaveChanges();
                        return Json(new { hasError = false }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { hasError = true, msg = "Error " + LineNo + " :" + ex.Message.Replace("'", "\'"), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditProducttDetails(int ProductID, string ProductCode, string ProductName, string Comments)
        {
            try
            {
                using (AizantIT_DevEntities objDEVEntitie = new AizantIT_DevEntities())
                {
                    bool productCode = objDEVEntitie.AizantIT_ProductMaster.Any(x => x.ProductCode.ToLower().Trim() == ProductCode.ToLower().Trim() && x.ProductID != ProductID);
                    if (productCode)// Code Already Exisits if Condition is true
                    {
                        return Json(new { hasError = "Warning" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        using (DbContextTransaction objDBTranscation = objDEVEntitie.Database.BeginTransaction())
                        {
                            try
                            {
                                AizantIT_ProductMaster objProductmaster = objDEVEntitie.AizantIT_ProductMaster.SingleOrDefault(id => id.ProductID == ProductID);
                                objProductmaster.ProductCode = ProductCode;
                                objProductmaster.ProductName = ProductName;
                                objDEVEntitie.SaveChanges();

                                AizantIT_Product_History objProdMasterHistory = new AizantIT_Product_History();
                                objProdMasterHistory.ProductID = objProductmaster.ProductID;
                                objProdMasterHistory.ActionBy = Convert.ToInt32(((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                                objProdMasterHistory.RoleID = Convert.ToInt32(AdminSetup_UserRole.GlobalAdmin_UMS);//Role for(product[Admin])
                                objProdMasterHistory.ActionDate = DateTime.Now;
                                objProdMasterHistory.ActionStatus = 2;//ActionName(Modifed By RAAdmin)
                                objProdMasterHistory.Comments = Comments;
                                objDEVEntitie.AizantIT_Product_History.Add(objProdMasterHistory);
                                objDEVEntitie.SaveChanges();

                                objDBTranscation.Commit();
                                return Json(new { hasError = false }, JsonRequestBehavior.AllowGet);
                            }
                            catch (Exception ex)
                            {
                                objDBTranscation.Rollback();
                                throw ex;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { hasError = true, msg = "Error " + LineNo + " :" + ex.Message.Replace("'", "\'"), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetProductListDetails(string ShowTypeID)
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            string ShowType = ShowTypeID;
            int LoginEmpId = Convert.ToInt32(((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));

            List<ProductList> objProductList = new List<ProductList>();
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities objPMSEntities = new AizantIT_DevEntities())
                {
                    ProductDal objDal = new ProductDal();
                    DataTable dt = objDal.GetProductListDAL(displayLength, displayStart, sortCol, sSortDir, sSearch, ShowType, LoginEmpId);
                    if (dt.Rows.Count > 0)
                    {
                        totalRecordsCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                        foreach (DataRow rdr in dt.Rows)
                        {
                            ProductList objProduct = new ProductList();
                            objProduct.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                            objProduct.ProductID = Convert.ToInt32(rdr["ProductID"]);
                            objProduct.ProductCode = (rdr["ProductCode"]).ToString();
                            objProduct.ProductName = (rdr["ProductName"]).ToString();
                            objProductList.Add(objProduct);
                        }
                    }
                    return Json(new
                    {
                        iTotalRecords = totalRecordsCount,
                        iTotalDisplayRecords = totalRecordsCount,
                        aaData = objProductList
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { hasError = true, msg = "Error " + LineNo + " :" + ex.Message.Replace("'", "\'"), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetProductData(int ProductID)
        {
            AizantIT_DevEntities objDEV_Entities = new AizantIT_DevEntities();
            var objProductMasterEnitiy = objDEV_Entities.AizantIT_ProductMaster.Where(id => id.ProductID == ProductID).Select(pid => new { pid.ProductID, pid.ProductCode, pid.ProductName }).SingleOrDefault();
            return Json(objProductMasterEnitiy, JsonRequestBehavior.AllowGet);
        }


        #region---History
        public ActionResult _HistoryDetails(int id = 0)
        {
            ViewBag.DosageList = id;
            return PartialView();

        }
        public ActionResult EachHistoryDetails(int ID)
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities objPMS_Entitie = new AizantIT_DevEntities())
                {
                    ProductDal objDAL = new ProductDal();
                    DataTable dt = objDAL.GetHistoryListDAL(displayLength, displayStart, sortCol, sSortDir, sSearch, ID);
                    List<ProductHistoryView> objDosageAuditList = new List<ProductHistoryView>();

                    if (dt.Rows.Count > 0)
                    {
                        totalRecordsCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                        foreach (DataRow dr in dt.Rows)
                        {
                            ProductHistoryView objDosagFomAudit = new ProductHistoryView();
                            objDosagFomAudit.PKID = Convert.ToInt32(dr["Product_HistoryID"]);
                            objDosagFomAudit.RowNumber = Convert.ToInt32(dr["RowNumber"]);
                            objDosagFomAudit.FirstName = Convert.ToString(dr["EmployeeName"]);
                            objDosagFomAudit.RoleName = Convert.ToString(dr["RoleName"]);
                            objDosagFomAudit.ActionDate = (dr["ActionDate"]).ToString();
                            objDosagFomAudit.AuditActionName = Convert.ToString(dr["AuditActionName"]);
                            objDosagFomAudit.Comments = Convert.ToString(dr["Comments"]);
                            objDosagFomAudit.CommentLength = Convert.ToInt32(dr["CommLength"]);
                            objDosageAuditList.Add(objDosagFomAudit);
                        }
                    }
                    return Json(
                        new
                        {
                            iTotalRecords = totalRecordsCount,
                            iTotalDisplayRecords = totalRecordsCount,
                            aaData = objDosageAuditList
                        }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + ex.Message.Replace("'", "\'"), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}