﻿using Aizant_API_Entities;
using AizantIT_PharmaApp.Areas.AdminSetup.Models.GlobalSetupBO;
using AizantIT_PharmaApp.Common.CustomFilters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.Areas.AdminSetup.Controllers.GlobalSetup
{
    [Route("AdminSetup/GlobalSetup/DosageFormController/{action}")]
    [RouteArea]
    [CustAuthentication]
    public class DosageFormController : Controller
    {
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // GET: AdminSetup/DossageForm  
        [CustAuthorization((int)Modules.AdminSetup,
          (int)AdminSetup_UserRole.GlobalAdmin_UMS)]
        public ActionResult Dosage()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.SysError = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber()) + " :" + ex.Message.Replace("'", "\'");
                Aizant_log.Error(ViewBag.SysError);
                return View();
            }
        }

        public PartialViewResult DosageFormList()
        {
            return PartialView("_GetDosageFormList");
        }

        public JsonResult GetDosageFormList()
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];

            List<DosageFormList> objDosageList = new List<DosageFormList>();
            try
            {
                int TotalRecordsCount = 0;
                using (AizantIT_DevEntities objDEV_Entities = new AizantIT_DevEntities())
                {
                    var DosageList = objDEV_Entities.AizantIT_SP_GetDosageFormListData(
                                              displayLength,
                                              displayStart,
                                              sortCol,
                                              sSortDir,
                                              sSearch);

                    foreach (var item in DosageList)
                    {
                        if (TotalRecordsCount == 0)
                        {
                            TotalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objDosageList.Add(new DosageFormList(Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.DosageFormID), item.DosageFormName, item.DosageDescription, Convert.ToBoolean(item.isActive), Convert.ToInt32(item.DescriptionLength),item.Status));
                    }
                    return Json(new
                    {
                        iTotalRecords = TotalRecordsCount,
                        iTotalDisplayRecords = TotalRecordsCount,
                        aaData = objDosageList
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = "Yes";
                return Json(new { msg = "Error " + LineNo + " :" + ex.Message.Replace("'", "\'"), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AddDosageForm(string DosageFormName, string Description)
        {
            try
            {
                using (AizantIT_DevEntities objDEV_Entities = new AizantIT_DevEntities())
                {
                    var Comments = "N/A";
                    var ActionBY = Convert.ToInt32(((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                    ObjectParameter objectParameter = new ObjectParameter("AuditActionID", typeof(int));
                    objDEV_Entities.AizantIT_SP_DosageCreationUpdation(DosageFormName, Description, 0, ActionBY, true, objectParameter, Comments);
                    int result = Convert.ToInt32(objectParameter.Value);
                    return Json(new { hasError = true, resultType = result }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = "Yes";
                return Json(new { msg = "Error " + LineNo + " :" + ex.Message.Replace("'", "\'"), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetDosageFormData(int DosageFormID)
        {
            AizantIT_DevEntities objDEV_Entities = new AizantIT_DevEntities();
            var objDosageForm = objDEV_Entities.AizantIT_DosageForm.SingleOrDefault(id => id.DosageFormID == DosageFormID);
            return Json(objDosageForm, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateDosageForm(int DosageID, string DosageFormName, string Description, bool IsActive, string Comments)
        {
            try
            {
                using (AizantIT_DevEntities objDEV_Entities = new AizantIT_DevEntities())
                {
                    var ActionBY = Convert.ToInt32(((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                    ObjectParameter objectParameter = new ObjectParameter("AuditActionID", typeof(int));
                    objDEV_Entities.AizantIT_SP_DosageCreationUpdation(DosageFormName, Description, DosageID, ActionBY, IsActive, objectParameter, Comments);
                    int result = Convert.ToInt32(objectParameter.Value);
                    return Json(new { hasError = true, resultType = result }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = "Yes";
                return Json(new { msg = "Error " + LineNo + " :" + ex.Message.Replace("'", "\'"), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult HistoryDosageList(int DosageID)
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            List<HistoryDosageList> objHistoryDosageList = new List<HistoryDosageList>();
            try
            {
                int TotalRecordsCount = 0;
                using (AizantIT_DevEntities objDEV_Entitie = new AizantIT_DevEntities())
                {
                    var DosageHistoryList=objDEV_Entitie.AizantIT_SP_HistoryDosageList(
                                                                        displayLength,
                                                                        displayStart,
                                                                        sortCol,
                                                                        sSortDir,
                                                                        sSearch,
                                                                        DosageID);
                    foreach (var item in DosageHistoryList)
                    {
                        if(TotalRecordsCount==0)
                        {
                            TotalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objHistoryDosageList.Add(new HistoryDosageList( Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.PKID), item.FirstName, item.RoleName, item.ActionDate, item.AuditActionName, item.SubmittedData, item.Comments, Convert.ToInt32(item.CommentLength)));
                    }
                }
                return Json(new
                {
                    iTotalRecords = TotalRecordsCount,
                    iTotalDisplayRecords = TotalRecordsCount,
                    aaData = objHistoryDosageList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + ex.Message.Replace("'", "\'"), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}