﻿using Aizant_API_Entities;
using AizantIT_PharmaApp.Common.CustomFilters;
using AizantIT_PharmaApp.Areas.AdminSetup.Models;
using QMS_BO.QMS_Audit_BO.AuditMaster;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Aizant_Enums.AizantEnums;
using System.Data;
using System.Data.Entity;

namespace AizantIT_PharmaApp.Areas.AdminSetup.Controllers.GlobalSetup
{
    [Route("AdminSetup/GlobalSetup/ClientController/{action}")]
    [RouteArea]
    [CustAuthentication]
    public class ClientController : Controller
    {
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // GET: AdminSetup/Client
        [CustAuthorization((int)Modules.AdminSetup,
          (int)AdminSetup_UserRole.GlobalAdmin_UMS)]
        public ActionResult Client()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.SysError = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber()) + " :" + ex.Message.Replace("'", "\'");
                Aizant_log.Error(ViewBag.SysError);
                return View();
            }
        }
     
        public PartialViewResult Clientlist(bool IsDataList)
        {
            ViewBag.IsDataList = IsDataList;//Hide the Edit & and history Fields
            return PartialView("_GetClientList");
        }

        [HttpGet]
        public JsonResult GetClientLists()
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];

            List<ClientModel> objClientList = new List<ClientModel>();

            try
            {
                int TotalRecordsCount = 0;
                using (AizantIT_DevEntities objDEV_Entities = new AizantIT_DevEntities())
                {
                    var ClientList = objDEV_Entities.AizantIT_SP_GetClientListData(
                                               displayLength,
                                               displayStart,
                                               sortCol,
                                               sSortDir,
                                               sSearch);

                    foreach (var item in ClientList)
                    {
                        if (TotalRecordsCount == 0)
                        {
                            TotalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objClientList.Add(new ClientModel(Convert.ToInt32(item.RowNumber),Convert.ToInt32(item.ClientID),item.ClientCode, item.ClientName, Convert.ToBoolean(item.isActive),item.Status));
                    }
                  }
                
                return Json(new
                {
                    iTotalRecords = TotalRecordsCount,
                    iTotalDisplayRecords = TotalRecordsCount,
                    aaData = objClientList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = "Yes";
                return Json(new { msg = "Error " + LineNo + " :" + ex.Message.Replace("'", "\'"), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
           // return View();
           }

        public JsonResult AddClientData(string ClientName,string ClientCode)//string ClientName, string ClientCode, int CountryID, int StateID, int CityID, string Pincode, string Phone, string EmailID, string Fax, string Address
        {
            try
            {
                using (AizantIT_DevEntities objDev_Entitie = new AizantIT_DevEntities())
                {
                    string _ClientCode = "Client_Code", _ClientName = "Client_Name", _ClientCodeandName = "Client_CodeName";
                    bool clientName = objDev_Entitie.AizantIT_Client.Any(x => x.ClientName.Trim() == ClientName.ToLower().Trim() && x.ClientID != 0);
                    bool clientCode = objDev_Entitie.AizantIT_Client.Any(x => x.ClientCode.Trim() == ClientCode.ToLower().Trim() && x.ClientID != 0);
                    if (clientName || clientCode)//Name or Code Already Exisiting if Condition is true
                    {
                        if (clientCode == true && clientName == false)
                        {
                            return Json(new { hasError = "Warning", data_Code = _ClientCode }, JsonRequestBehavior.AllowGet);
                        }
                        else if (clientCode == false && clientName == true)
                        {
                            return Json(new { hasError = "Warning", data_Name = _ClientName }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { hasError = "Warning", data_CodeName = _ClientCodeandName }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        using (DbContextTransaction objTranscation = objDev_Entitie.Database.BeginTransaction())
                        {
                            try
                            {
                                AizantIT_Client objAizantClient = new AizantIT_Client();
                                objAizantClient.ClientName = ClientName;
                                objAizantClient.ClientCode = ClientCode;
                                objDev_Entitie.AizantIT_Client.Add(objAizantClient);
                                objDev_Entitie.SaveChanges();

                                AizantIT_ClientAudit objClientAudit = new AizantIT_ClientAudit();
                                objClientAudit.ClientID = objAizantClient.ClientID;
                                objClientAudit.ActionBY = Convert.ToInt32(((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                                objClientAudit.RoleID = 50;
                                objClientAudit.ActionDate = DateTime.Now;
                                objClientAudit.ActionStatusID = 1;
                                objClientAudit.SubmittedData = ClientName;
                                objClientAudit.Comments = "N/A";
                                objDev_Entitie.AizantIT_ClientAudit.Add(objClientAudit);
                                objDev_Entitie.SaveChanges();
                                objTranscation.Commit();
                                return Json(new { hasError = false }, JsonRequestBehavior.AllowGet);
                            }
                            catch (Exception ex)
                            {
                                objTranscation.Rollback();
                                throw ex;
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { hasError = true, msg = "Error " + LineNo + " :" + ex.Message.Replace("'", "\'"), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult EditClientDetails(int ClientID,string ClientName,string ClientCode, bool IsActive, string Comment)
        {
            try
             {
                using (AizantIT_DevEntities objDEVEntitie = new AizantIT_DevEntities())
                {
                    string _ClientCode = "Client_Code", _ClientName = "Client_Name", _ClientCodeandName = "Client_CodeName";
                    bool clientName = objDEVEntitie.AizantIT_Client.Any(x => x.ClientName.Trim() == ClientName.ToLower().Trim() && x.ClientID != ClientID);
                    bool clientCode = objDEVEntitie.AizantIT_Client.Any(x => x.ClientCode.Trim() == ClientCode.ToLower().Trim() && x.ClientID != ClientID);
                    if (clientName || clientCode)//Name or Code Already Exisiting if Condition is true
                    {
                        if (clientCode == true && clientName == false)
                        {
                            return Json(new { hasError = "Warning", data_Code = _ClientCode }, JsonRequestBehavior.AllowGet);
                        }
                        else if (clientCode == false && clientName == true)
                        {
                            return Json(new { hasError = "Warning", data_Name = _ClientName }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { hasError = "Warning", data_CodeName = _ClientCodeandName }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        using (DbContextTransaction objDBTranscation = objDEVEntitie.Database.BeginTransaction())
                        {
                            try
                            {
                                AizantIT_Client objAizantClient = objDEVEntitie.AizantIT_Client.SingleOrDefault(id => id.ClientID == ClientID);
                                objAizantClient.ClientName = ClientName;
                                objAizantClient.ClientCode = ClientCode;
                                objAizantClient.IsActive = IsActive;
                                objDEVEntitie.SaveChanges();

                                AizantIT_ClientAudit objAizantClientAudit = new AizantIT_ClientAudit();
                                objAizantClientAudit.ClientID = ClientID;
                                objAizantClientAudit.ActionBY = Convert.ToInt32(((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                                objAizantClientAudit.RoleID = 50;
                                objAizantClientAudit.ActionDate = DateTime.Now;
                                objAizantClientAudit.ActionStatusID = 2;
                                objAizantClientAudit.SubmittedData = ClientName;
                                objAizantClientAudit.Comments = Comment;
                                objDEVEntitie.AizantIT_ClientAudit.Add(objAizantClientAudit);
                                objDEVEntitie.SaveChanges();
                                objDBTranscation.Commit();
                                return Json(new { hasError = false }, JsonRequestBehavior.AllowGet);
                            }
                            catch (Exception ex)
                            {
                                objDBTranscation.Rollback();
                                throw ex;
                            }
                        }
                           
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { hasError = true, msg = "Error " + LineNo + " :" + ex.Message.Replace("'", "\'"), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }

        }
        
        public ActionResult EachHistoryDetails(int ClientID)
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            List<historyClient> objHistoryClientList = new List<historyClient>();
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities objDEV_Entitie = new AizantIT_DevEntities())
                {
                    var clientList = objDEV_Entitie.AizantIT_SP_HistoryClientList(
                                                    displayLength,
                                                    displayStart,
                                                    sortCol,
                                                    sSortDir,
                                                    sSearch,
                                                    ClientID);

                    foreach (var item in clientList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }                                        
                        objHistoryClientList.Add(new historyClient( Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.PKID), item.FirstName, item.RoleName, item.ActionDate, item.AuditActionName, item.SubmittedData, item.Comments, Convert.ToInt32(item.CommentLength)));
                    }
                }
                return Json( new
                               {
                                   iTotalRecords = totalRecordsCount,
                                   iTotalDisplayRecords = totalRecordsCount,
                                   aaData = objHistoryClientList
                               }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + ex.Message.Replace("'", "\'"), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }         
        }

        public JsonResult BindClientDetails(int ClientID)
        {
            AizantIT_DevEntities objDEV_Entities = new AizantIT_DevEntities();
            var ClientData = objDEV_Entities.AizantIT_Client.SingleOrDefault(id=>id.ClientID==ClientID);
            return Json(ClientData, JsonRequestBehavior.AllowGet);
        }

    }
}