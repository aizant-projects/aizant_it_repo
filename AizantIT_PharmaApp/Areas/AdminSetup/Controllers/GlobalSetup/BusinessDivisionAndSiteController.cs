﻿using Aizant_API_Entities;
using AizantIT_PharmaApp.Areas.AdminSetup.Models.GlobalSetupBO;
using AizantIT_PharmaApp.Common.CustomFilters;
using AizantIT_PharmaApp.Controllers.Aizant_BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.Areas.AdminSetup.Controllers.GlobalSetup
{
    [Route("AdminSetup/GlobalSetup/BusinessDivisionAndSiteController/{action}")]
    [RouteArea]
    [CustAuthentication]
    public class BusinessDivisionAndSiteController : Controller
    {
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // GET: AdminSetup/BusinessDivisionAndSite
        #region BusinessDivision
        [CustAuthorization((int)Modules.AdminSetup,
         (int)AdminSetup_UserRole.GlobalAdmin_UMS)]
        public ActionResult BusinessDivision()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.SysError = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber()) + " :" + ex.Message.Replace("'", "\'");
                Aizant_log.Error(ViewBag.SysError);
                return View();
            }
        }
        public ActionResult GetBusinessDivisionList(bool IsEditable)
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_API = new AizantIT_DevEntities())
                {
                    List<BusinessDivisionBO> BusinessDivisionList = new List<BusinessDivisionBO>();
                    var BusinessDivisionItemsList =
                    (from objBusinessDivision in dbAizantIT_API.AizantIT_Business_Division
                     join objEmpCreatedByDetails in dbAizantIT_API.AizantIT_EmpMaster 
                        on objBusinessDivision.CreatedBy equals objEmpCreatedByDetails.EmpID into LeftJoinCreatedEMP
                        from objEmpCreatedByDetails in LeftJoinCreatedEMP.DefaultIfEmpty()
                     join objEmpModifiedByDetails in dbAizantIT_API.AizantIT_EmpMaster
                        on objBusinessDivision.ModifiedBy equals objEmpModifiedByDetails.EmpID into LeftJoinModifiedEmp
                     from objEmpModifiedByDetails in LeftJoinModifiedEmp.DefaultIfEmpty()
                     select new
                     {
                         objBusinessDivision.BusinessDivisionID,
                         objBusinessDivision.BusinessDivisionCode,
                         objBusinessDivision.BusinessDivisionName,
                         objBusinessDivision.IsActive,
                         CreatedByEmpName = objBusinessDivision.CreatedBy==null?"System": objEmpCreatedByDetails.FirstName + " " + objEmpCreatedByDetails.LastName
                         //(objEmpCreatedByDetails.FirstName + " " + objEmpCreatedByDetails.LastName) == " " ? "N/A" : (objEmpCreatedByDetails.FirstName + " " + objEmpCreatedByDetails.LastName)
                         ,
                         objBusinessDivision.CreatedDate,
                         ModifiedByName = (objEmpModifiedByDetails.FirstName + " " + objEmpModifiedByDetails.LastName) == " " ? "N/A" : (objEmpModifiedByDetails.FirstName + " " + objEmpModifiedByDetails.LastName),
                         objBusinessDivision.ModifiedDate
                     }).ToList();
                    foreach (var BusinessDivisionItem in BusinessDivisionItemsList)
                    {
                        BusinessDivisionList.Add(new BusinessDivisionBO
                        {
                            BusinessDivisionID = BusinessDivisionItem.BusinessDivisionID,
                            BusinessDivisionCode = BusinessDivisionItem.BusinessDivisionCode,
                            BusinessDivisionName = BusinessDivisionItem.BusinessDivisionName,
                            IsActive = BusinessDivisionItem.IsActive,
                            CreatedByName = BusinessDivisionItem.CreatedByEmpName,
                            CreatedDate = BusinessDivisionItem.CreatedDate == null ? "N/A" : Convert.ToDateTime(BusinessDivisionItem.CreatedDate).ToString("dd MMM yyyy HH:mm:ss"),
                            ModifiedByName = BusinessDivisionItem.ModifiedByName,
                            ModifiedDate = BusinessDivisionItem.ModifiedDate == null ? "N/A" : Convert.ToDateTime(BusinessDivisionItem.ModifiedDate).ToString("dd MMM yyyy HH:mm:ss")
                        });
                    }
                    ViewBag.IsEditable = IsEditable;
                    return PartialView("_GetBusinessDivisionList", BusinessDivisionList);
                    //return PartialView(BusinessDivisionList);
                }
            }
            catch (Exception ex)
            {
                ViewBag.SysError = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber()) + " :" + ex.Message.Replace("'", "\'");
                Aizant_log.Error(ViewBag.SysError);
                return PartialView("_SysException");
            }
        }

        [HttpPost]
        public JsonResult BusinessDivisionCreate(string BusinessDivisionCode, string BusinessDivisionName)
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_API = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbAizantIT_API.Database.BeginTransaction())
                    {
                        try
                        {
                            bool IsBussinessDivision_Code =(from bussinessCode in dbAizantIT_API.AizantIT_Business_Division
                                                            where bussinessCode.BusinessDivisionCode == BusinessDivisionCode
                                                            select new { bussinessCode.BusinessDivisionCode }).Any();

                            bool IsBussinessDivision_Name =(from bussinessName in dbAizantIT_API.AizantIT_Business_Division
                                                            where bussinessName.BusinessDivisionName == BusinessDivisionName
                                                            select new { bussinessName.BusinessDivisionName }).Any();

                            if (IsBussinessDivision_Code && IsBussinessDivision_Name)
                            {
                                return Json(new { msg = "Business Division Code & Business Division Name already exists", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                            if (IsBussinessDivision_Code)
                            {
                                return Json(new { msg = "Business Division Code already exists", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                            if(IsBussinessDivision_Name)
                            {
                                return Json(new { msg = "Business Division Name already exists", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                            else 
                            {
                                AizantIT_Business_Division objAizantIT_BusinessDivision = new AizantIT_Business_Division();
                                objAizantIT_BusinessDivision.BusinessDivisionCode = BusinessDivisionCode;
                                objAizantIT_BusinessDivision.BusinessDivisionName = BusinessDivisionName;
                                objAizantIT_BusinessDivision.IsActive = true;
                                objAizantIT_BusinessDivision.CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                objAizantIT_BusinessDivision.CreatedDate = DateTime.Now;
                                dbAizantIT_API.AizantIT_Business_Division.Add(objAizantIT_BusinessDivision);
                                dbAizantIT_API.SaveChanges();
                                transaction.Commit();
                                return Json(new { msg = "Business Division Created.", msgType = "success" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string ExceptionMsg = "Error " + (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber()) + ":" + ex.Message.Replace("'", "\'");
                Aizant_log.Error(ExceptionMsg);
                return Json(new { msg = ExceptionMsg, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult BusinessDivisionModify(int BusinessDivisionID, string BusinessDivisionCode, string BusinessDivisionName, bool IsActive)
        {
            try
            {
                bool IsNameOrCodeChanged = false;
                using (AizantIT_DevEntities dbAizantIT_API = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbAizantIT_API.Database.BeginTransaction())
                    {
                        try
                        {
                            AizantIT_Business_Division dtAizantIT_BusinessDivision =
                            dbAizantIT_API.AizantIT_Business_Division.SingleOrDefault(p => p.BusinessDivisionID == BusinessDivisionID);

                            if (dtAizantIT_BusinessDivision.BusinessDivisionCode != BusinessDivisionCode || dtAizantIT_BusinessDivision.BusinessDivisionName != BusinessDivisionName || dtAizantIT_BusinessDivision.IsActive != IsActive)
                            {
                                IsNameOrCodeChanged = true;
                            }

                            if (IsNameOrCodeChanged)
                            {
                                bool IsBussinessDivision_Code =(from bussinessCode in dbAizantIT_API.AizantIT_Business_Division
                                                                where bussinessCode.BusinessDivisionCode == BusinessDivisionCode && bussinessCode.BusinessDivisionID != BusinessDivisionID
                                                                select new { bussinessCode.BusinessDivisionCode }).Any();
                                bool IsBussinessDivision_Name =(from bussinessName in dbAizantIT_API.AizantIT_Business_Division
                                                                where bussinessName.BusinessDivisionName == BusinessDivisionName && bussinessName.BusinessDivisionID != BusinessDivisionID
                                                                select new { bussinessName.BusinessDivisionName }).Any();

                                if(IsBussinessDivision_Code && IsBussinessDivision_Name)
                                {
                                    return Json(new { msg = "Business Division Code & Business Division Name already exists", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                }
                                if (IsBussinessDivision_Code)
                                {
                                    return Json(new { msg = "Business Division Code already exists", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                }
                                if (IsBussinessDivision_Name)
                                {
                                    return Json(new { msg = "Business Division Name already exists", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                }
                                else 
                                {

                                    AizantIT_Business_Division objAizantIT_BusinessDivision = new AizantIT_Business_Division();
                                    objAizantIT_BusinessDivision = dbAizantIT_API.AizantIT_Business_Division.SingleOrDefault(x => x.BusinessDivisionID == BusinessDivisionID);

                                    objAizantIT_BusinessDivision.BusinessDivisionCode = BusinessDivisionCode;
                                    objAizantIT_BusinessDivision.BusinessDivisionName = BusinessDivisionName;
                                    objAizantIT_BusinessDivision.IsActive = IsActive;
                                    objAizantIT_BusinessDivision.ModifiedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                    objAizantIT_BusinessDivision.ModifiedDate = DateTime.Now;
                                    dbAizantIT_API.SaveChanges();
                                    transaction.Commit();
                                    return Json(new
                                    {
                                        msg = "Business Division Updated.",
                                        msgType = "success",
                                        ModifiedByName = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString(),
                                        ModifiedDate = DateTime.Now.ToString("dd MMM yyyy HH:mm:ss")
                                    }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json(new { msg = "There are no changes to Update", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string ExceptionMsg = "Error " + (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber()) + ":" + ex.Message.Replace("'", "\'");
                Aizant_log.Error(ExceptionMsg);
                return Json(new { msg = ExceptionMsg, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Site
        [CustAuthorization((int)Modules.AdminSetup,
        (int)AdminSetup_UserRole.GlobalAdmin_UMS)]
        public ActionResult Site_BusinessDivision()
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_API = new AizantIT_DevEntities())
                {
                    //code for Loading the Business Division ddl.
                    List<SelectListItem> BusinessDivisionList = new List<SelectListItem>();
                    var BusinessDivisionItemsList = (from objBusinessDivision in dbAizantIT_API.AizantIT_Business_Division
                                                     where objBusinessDivision.IsActive == true
                                                     select new { objBusinessDivision.BusinessDivisionID,
                                                         BusinessDivisionName = (objBusinessDivision.BusinessDivisionCode + " - " + objBusinessDivision.BusinessDivisionName) }).ToList();

                    foreach (var BusinessDivisionItem in BusinessDivisionItemsList)
                    {
                        BusinessDivisionList.Add(new SelectListItem
                        {
                            Text = BusinessDivisionItem.BusinessDivisionName,
                            Value = BusinessDivisionItem.BusinessDivisionID.ToString(),
                        });
                    }
                    ViewBag.BusinessDivisionList = new SelectList(BusinessDivisionList.ToList(), "Value", "Text");

                    //Code for Departments List for ddl.
                    //List<SelectListItem> DepartmetList = new List<SelectListItem>();
                    //var DepartmetItemsList = (from objDepartmet in dbAizantIT_API.AizantIT_DepartmentMaster
                    //                          select new { objDepartmet.DeptID, objDepartmet.DepartmentName }).ToList();

                    //foreach (var DepartmetItem in DepartmetItemsList)
                    //{
                    //    DepartmetList.Add(new SelectListItem
                    //    {
                    //        Text = DepartmetItem.DepartmentName,
                    //        Value = DepartmetItem.DeptID.ToString(),
                    //    });
                    //}
                    //ViewBag.DepartmetList = new SelectList(DepartmetList.ToList(), "Value", "Text");

                    Aizant_Bl aizant_Bl = new Aizant_Bl();
                   
                    ViewBag.DepartmetList = aizant_Bl.GetDepartmentsList();

                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.SysError = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber()) + " :" + ex.Message.Replace("'", "\'");
                Aizant_log.Error(ViewBag.SysError);
                return View();
            }
        }
        public ActionResult GetSite_BusinessDivisionList(bool IsEditable)
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_API = new AizantIT_DevEntities())
                {
                    List<SiteBO> SiteList = new List<SiteBO>();
                    var SiteDepartmentsList = dbAizantIT_API.Database.SqlQuery<SiteDepartmentsBO>("exec [API].[AizantIT_SP_GetSiteDepartmentsList]").ToList();
                    var SiteItemsList =
                    (from objSite in dbAizantIT_API.AizantIT_Site_BusinessDivision
                     join objBusinessDivision in dbAizantIT_API.AizantIT_Business_Division on objSite.BusinessDivisionID equals objBusinessDivision.BusinessDivisionID
                     join objEmpCreatedDetails in dbAizantIT_API.AizantIT_EmpMaster on objSite.CreatedBy equals objEmpCreatedDetails.EmpID
                     join objEmpDetails in dbAizantIT_API.AizantIT_EmpMaster
                     on objSite.ModifiedBy equals objEmpDetails.EmpID into LeftJoinEmp
                     from objEmpDetails in LeftJoinEmp.DefaultIfEmpty()
                     select new
                     {
                         objSite.SiteID,
                         objSite.BusinessDivisionID,
                         BusinessDivisionName = (objBusinessDivision.BusinessDivisionCode + " - " + objBusinessDivision.BusinessDivisionName),
                         objSite.SiteName,
                         objSite.IsActive,
                         CreatedByName = (objEmpCreatedDetails.FirstName + " " + objEmpCreatedDetails.LastName),
                         objSite.CreatedDate,
                         ModifiedByName = (objEmpDetails.FirstName + " " + objEmpDetails.LastName) == " " ? "N/A" : (objEmpDetails.FirstName + " " + objEmpDetails.LastName),
                         objSite.ModifiedDate
                     }).ToList();
                    foreach (var SiteItem in SiteItemsList)
                    {
                        SiteBO newSiteBo = new SiteBO();
                        foreach (var SiteDeptitem in SiteDepartmentsList)
                        {
                            if (SiteItem.SiteID == SiteDeptitem.SiteID)
                            {
                                newSiteBo.SelectedDepartmentsIDs = SiteDeptitem.SiteDepartmentsIDs;
                                newSiteBo.SelectedDepartmentsNames = SiteDeptitem.SiteDepartmentsNames;
                            }
                        }
                        newSiteBo.SiteID = SiteItem.SiteID;
                        newSiteBo.BusinessDivisionID = SiteItem.BusinessDivisionID;
                        newSiteBo.BusinessDivisionName = SiteItem.BusinessDivisionName;
                        newSiteBo.SiteName = SiteItem.SiteName;
                        newSiteBo.IsActive = SiteItem.IsActive;
                        newSiteBo.CreatedByName = SiteItem.CreatedByName;
                        newSiteBo.CreatedDate = SiteItem.CreatedDate == null ? "N/A" : Convert.ToDateTime(SiteItem.CreatedDate).ToString("dd MMM yyyy HH:mm:ss");
                        newSiteBo.ModifiedByName = SiteItem.ModifiedByName;
                        newSiteBo.ModifiedDate = SiteItem.ModifiedDate == null ? "N/A" : Convert.ToDateTime(SiteItem.ModifiedDate).ToString("dd MMM yyyy HH:mm:ss");
                        SiteList.Add(newSiteBo);
                    }
                    ViewBag.IsEditable = IsEditable;
                    return PartialView("_Site_BusinessDivisionList", SiteList);
                }
            }
            catch (Exception ex)
            {
                ViewBag.SysError = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber()) + " :" + ex.Message.Replace("'", "\'");
                Aizant_log.Error(ViewBag.SysError);
                return PartialView("_SysException");
            }
        }
        [HttpGet]
        public JsonResult GetSite_BD_List()
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            List<SiteBOList> objSiteBO = new List<SiteBOList>();
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbAizantIT_API = new AizantIT_DevEntities())
                {
                    var objAuditObservationForHODandQAList = dbAizantIT_API.AizantIT_SP_GetSIT_BD_List(
                               displayLength,
                               displayStart,
                               sortCol,
                               sSortDir,
                               sSearch != null ? sSearch.Trim() : ""
                               );

                    foreach (var item in objAuditObservationForHODandQAList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objSiteBO.Add(new SiteBOList(Convert.ToInt32(item.SiteID), Convert.ToInt32(item.BusinessDivisionID),
                            item.SiteDeptIDs,item.SiteName,item.BusinessDivisionCodeAndName,item.IsActive,item.SiteDeptNames,
                            item.SiteDeptCodes, item.CreatedByName, item.CreatedDate, item.ModifiedByName,item.ModifiedDate));
                    }
                }

                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objSiteBO
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string ExceptionMsg = "Error " + (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber()) + ":" + ex.Message.Replace("'", "\'");
                Aizant_log.Error(ExceptionMsg);
                return Json(new { msg = ExceptionMsg, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult SiteCreate(string SiteName, int BusinessDivisionID, string SelectedDetIDs)
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_API = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbAizantIT_API.Database.BeginTransaction())
                    {
                        try
                        {
                            int[] DeptIDs = SelectedDetIDs.Split(',').Select(DeptID => Convert.ToInt32(DeptID)).ToArray();

                            bool IsSiteNameExist = (from itemSite in dbAizantIT_API.AizantIT_Site_BusinessDivision
                                                    where itemSite.SiteName == SiteName
                                                    select new { itemSite.SiteName }).Any();
                            if (!IsSiteNameExist)
                            {
                                DataTable _dtDeptIDs = AddDeptIDsCreate(DeptIDs);
                                SqlParameter ParamDeptsTB = new SqlParameter("@SiteDepartments", _dtDeptIDs);
                                SqlParameter ParamSiteName = new SqlParameter("@SiteName", SiteName);
                                SqlParameter ParamBusinessDivisionID = new SqlParameter("@BusinessDivisionID", BusinessDivisionID);
                                SqlParameter ParamCreatedBy = new SqlParameter("@CreatedBy", Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                                ParamDeptsTB.TypeName = "[dbo].[AizantIT_DepartmentType]";
                                dbAizantIT_API.Database.ExecuteSqlCommand("exec [API].[AizantIT_SP_Site_BD_Create] @SiteName,@BusinessDivisionID,@CreatedBy,@SiteDepartments", ParamSiteName, ParamBusinessDivisionID, ParamCreatedBy, ParamDeptsTB);
                                transaction.Commit();
                                return Json(new { msg = "Site Created.", msgType = "success" }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { msg = "Site Name Already Exists", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string ExceptionMsg = "Error " + (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber()) + ":" + ex.Message.Replace("'", "\'");
                Aizant_log.Error(ExceptionMsg);
                return Json(new { msg = ExceptionMsg, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult SiteModify(int SiteID, string SiteName, bool IsActive, string SelectedDetIDs)
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_API = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbAizantIT_API.Database.BeginTransaction())
                    {
                        try
                        {
                            int[] DeptIDs = SelectedDetIDs.Split(',').Select(DeptID => Convert.ToInt32(DeptID)).ToArray();
                            bool IsNameChanged = true;


                            AizantIT_Site_BusinessDivision dtAizantIT_Site =
                                    dbAizantIT_API.AizantIT_Site_BusinessDivision.SingleOrDefault(p => p.SiteID == SiteID);

                            if (dtAizantIT_Site.SiteName != SiteName || dtAizantIT_Site.IsActive != IsActive)
                            {
                                IsNameChanged = true;
                            }

                            if (IsNameChanged)
                            {
                                bool IsRecordExists = (from itemSite in dbAizantIT_API.AizantIT_Site_BusinessDivision
                                                       where (itemSite.SiteName == SiteName)
                                                       && itemSite.SiteID != SiteID
                                                       select new { itemSite.SiteName }).Any();

                                if (!IsRecordExists)
                                {
                                    DataTable _dtDeptIDs = AddDeptIDsCreate(DeptIDs);
                                    SqlParameter ParamDeptsTB = new SqlParameter("@SiteDepartments", _dtDeptIDs);
                                    SqlParameter ParamSiteID = new SqlParameter("@SiteID", SiteID);
                                    SqlParameter ParamSiteName = new SqlParameter("@SiteName", SiteName);
                                    SqlParameter ParamSiteIsActive = new SqlParameter("@IsActive", IsActive);
                                    SqlParameter ParamModifiedBy = new SqlParameter("@ModifiedBy", Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                                    ParamDeptsTB.TypeName = "[dbo].[AizantIT_DepartmentType]";
                                    dbAizantIT_API.Database.ExecuteSqlCommand("exec [API].[AizantIT_SP_Site_BD_Update] @SiteID,@SiteName,@IsActive,@ModifiedBy,@SiteDepartments", ParamSiteID, ParamSiteName, ParamSiteIsActive, ParamModifiedBy, ParamDeptsTB);
                                    transaction.Commit();
                                    return Json(new
                                    {
                                        msg = "Site Updated.",
                                        msgType = "success",
                                        ModifiedByName = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString(),
                                        ModifiedDate = DateTime.Now.ToString("dd MMM yyyy HH:mm:ss")
                                    }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    return Json(new { msg = "Site Name already Exists", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json(new { msg = "There are no changes to Update.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + ex.Message;
                message.Replace("'", "\'");
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        private DataTable AddDeptIDsCreate(int[] DeptIDs)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("DeptID");
            foreach (var DeptID in DeptIDs)
            {
                dt.Rows.Add(DeptID);
            }
            return dt;
        }


        public JsonResult GetSiteData(int SiteID)
        {
            AizantIT_DevEntities objDEV_Entities = new AizantIT_DevEntities();
            var Single_SiteData = (from SB in objDEV_Entities.AizantIT_Site_BusinessDivision
                                   join SD in objDEV_Entities.AizantIT_Site_Depts on SB.SiteID equals SD.SiteID
                                   join BD in objDEV_Entities.AizantIT_Business_Division on SB.BusinessDivisionID equals BD.BusinessDivisionID
                                   where SB.SiteID == SiteID
                                   select new {

                                       BusinessDivisionName = BD.BusinessDivisionName,
                                       SiteID=SB.SiteID,
                                       SiteName = SB.SiteName,
                                       IsActive = SB.IsActive,
                                       DeptIDs = SD.DepartmentID
                                   }).ToList();
            return Json(new {hasError = false,data = Single_SiteData },JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}