﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.Areas.AdminSetup.Models.GlobalSetupBO
{
    public class SiteBO
    {
        public int SiteID { get; set; }
        public string SiteName { get; set; }
        public int BusinessDivisionID { get; set; }
        public string BusinessDivisionName { get; set; }
        public bool IsActive { get; set; }
        public string IsActiveName { get; set; }
        public string SelectedDepartmentsIDs { get; set; }
        public string SelectedDepartmentsNames { get; set; }
        public string SelectedDeptCodes { get; set; }
        public string CreatedByName { get; set; }
        public string CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public string ModifiedByName { get; set; }
        public string ModifiedDate { get; set; }
    }
    public class SiteBOList
    {
        public int SiteID { get; set; }
        public string SiteName { get; set; }
        public int BusinessDivisionID { get; set; }
        public string BusinessDivisionName { get; set; }
        public bool IsActive { get; set; }
        public string IsActiveName { get; set; }
        public string SelectedDepartmentsIDs { get; set; }
        public string SelectedDepartmentsNames { get; set; }
        public string SelectedDeptCodes { get; set; }
        public string CreatedByName { get; set; }
        public string CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public string ModifiedByName { get; set; }
        public string ModifiedDate { get; set; }

        public SiteBOList(int _SiteID, int _BusinessDivisionID, string _SelectedDepartmentsIDs, string _SiteName,  string _BusinessDivisionName, string _IsActiveName, 
         string _SelectedDepartmentsNames, string _SelectedDeptCodes, string _CreatedByName, string _CreatedDate, string _ModifiedByName,string _ModifiedDate)
        {
            SiteID = _SiteID;
            BusinessDivisionID = _BusinessDivisionID;
            SelectedDepartmentsIDs = _SelectedDepartmentsIDs;
            SiteName = _SiteName;
            BusinessDivisionName = _BusinessDivisionName;
            IsActiveName = _IsActiveName;
            SelectedDepartmentsNames = _SelectedDepartmentsNames;
            SelectedDeptCodes = _SelectedDeptCodes;
            CreatedByName = _CreatedByName;
            CreatedDate = _CreatedDate;
            ModifiedByName = _ModifiedByName;
            ModifiedDate = _ModifiedDate;
        }
    }
}