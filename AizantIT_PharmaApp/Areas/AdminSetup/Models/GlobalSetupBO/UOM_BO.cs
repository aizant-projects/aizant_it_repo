﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.Areas.AdminSetup.Models.GlobalSetupBO
{
    public class UOM_BO
    {
        //UnitOfMeasurement
        public int RowNumber { get; set; }
        public int UOM_ID { get; set; }
        public string UOM_Name { get; set; }
        public string UOM_Description { get; set; }
        public string UOM_Comments { get; set; }


        public UOM_BO(int _RowNumber, int _UOM_ID, string _UOM_Name, string _UOM_Description)
        {
            RowNumber = _RowNumber;
            UOM_ID = _UOM_ID;
            UOM_Name = _UOM_Name;
            UOM_Description = _UOM_Description;
        }
        public UOM_BO()
        {

        }
    }

    public class UOM_HistoryBO
    {
        public int PKID { get; set; }
        public int RowNumber { get; set; }
        public string ActionByName { get; set; }
        public string RoleName { get; set; }
        public string ActionDate { get; set; }
        public string ActionStatusName { get; set; }
        public string SubmittedData { get; set; }
        public string Comments { get; set; }
    public UOM_HistoryBO(int _RowNumber, int _PKID, string _ActionByName, string _RoleName, string _ActionDate, string _ActionStatusName, string _SubmittedData, string _Comments)
    {

        RowNumber = _RowNumber;
        PKID = _PKID;
        ActionByName = _ActionByName;
        RoleName = _RoleName;
        ActionDate = _ActionDate;
        ActionStatusName = _ActionStatusName;
        SubmittedData = _SubmittedData;
        Comments = _Comments;
    }
  }
}