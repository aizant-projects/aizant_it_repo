﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.Areas.AdminSetup.Models.GlobalSetupBO
{
    public class SiteDepartmentsBO
    {
        public int SiteID { get; set; }
        public string SiteDepartmentsIDs { get; set; }
        public string SiteDepartmentsNames { get; set; }
    }
}