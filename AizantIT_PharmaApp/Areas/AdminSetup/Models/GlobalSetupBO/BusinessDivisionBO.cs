﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.Areas.AdminSetup.Models.GlobalSetupBO
{
    public class BusinessDivisionBO
    {
        public int BusinessDivisionID { get; set; }
        public string BusinessDivisionCode { get; set; }
        public string BusinessDivisionName { get; set; }
        public bool IsActive { get; set; }
        public string CreatedByName { get; set; }
        public string CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public string ModifiedByName { get; set; }
        public string ModifiedDate { get; set; }
    }
}