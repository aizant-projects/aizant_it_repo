﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.Areas.AdminSetup.Models.GlobalSetupBO
{
    public class DosageFormBO
    {
        public int TotalCount { get; set; }
        public int DosageFormID { get; set; }
        public string DosageFormName { get; set; }
        public string Description { get; set; }
        public bool isActive { get; set; }
        public int RowNumber { get; set; }
        public int Count { get; set; }
        public string Comment { get; set; }
        public int DescriptionLength { set; get; }
    }
    public class DosageFormList
    {
       // public int TotalCount { get; set; }
        public int RowNumber { get; set; }
        public int DosageFormID { get; set; }
        public string DosageFormName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public int DescriptionLength { set; get; }

        public string Status { get; set; }
        //public int Count { get; set; }
        //public string Comment { get; set; }

        public DosageFormList(int _RowNumber,int _DosageFormID,string _DosageFormName,string _Description,bool _IsActive,int _DescriptionLength,string _Status)
        {
            RowNumber = _RowNumber;
            DosageFormID = _DosageFormID;
            DosageFormName = _DosageFormName;
            Description = _Description;
            IsActive = _IsActive;
            DescriptionLength = _DescriptionLength;
            Status = _Status;
        }
    }

    public class HistoryDosageList
    {
        public int PKID { get; set; }
        public int RowNumber { get; set; }
        public string FirstName { get; set; }
        public string RoleName { get; set; }
        public string ActionDate { get; set; }
        public string AuditActionName { get; set; }
        public string SubmittedData { get; set; }
        public string Comments { get; set; }
        public int CommentLength { get; set; }

        public HistoryDosageList(int _RowNumber, int _PKID, string _FirstName, string _RoleName, string _ActionDate, string _AuditActionName, string _SubmittedData, string _Comments, int _CommentLength)
        {
            RowNumber = _RowNumber;
            PKID = _PKID;
            FirstName = _FirstName;
            RoleName = _RoleName;
            ActionDate = _ActionDate;
            AuditActionName = _AuditActionName;
            SubmittedData = _SubmittedData;
            Comments = _Comments;
            CommentLength = _CommentLength;
        }
    }
}