﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.Areas.AdminSetup.Models.GlobalSetupBO
{
    public class ProductBO
    {
        public int ProductID { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public decimal? StrengthValue { get; set; }
    }

    public class ProductHistoryView
    {
        public int PKID { get; set; }
        public int RowNumber { get; set; }
        public int DosageFormID { get; set; }
        public string FirstName { get; set; }
        public string RoleName { get; set; }
        public string AuditActionName { get; set; }
        public string SubmittedData { get; set; }
        public string Comments { get; set; }
        public int CommentLength { get; set; }
        public string ActionDate { get; set; }
        public int AssignToId { get; set; }
        public string AssignToName { get; set; }
        public int AssignToRoleId { get; set; }
        public string AssignToRoleName { get; set; }
    }
}