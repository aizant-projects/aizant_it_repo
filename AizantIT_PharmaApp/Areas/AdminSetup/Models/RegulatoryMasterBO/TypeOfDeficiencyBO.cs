﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.Areas.AdminSetup.Models.RegulatoryMasterBO
{
    public class TypeOfDeficiencyBO
    {
        public int TypeOfDeficiencyID { get; set; }
        public string TypeOfDeficiencyName { get; set; }
        public string Remarks { get; set; }
        public bool IsActive { get; set; }
        public string Comments { get; set; }

    }
}