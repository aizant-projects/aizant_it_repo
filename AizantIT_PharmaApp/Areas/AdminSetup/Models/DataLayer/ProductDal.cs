﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.Areas.AdminSetup.Models.DataLayer
{
    public class ProductDal
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());

        public DataTable GetProductListDAL(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, string ShowType, int LoginEmpId)
        {
            try
            {
                string query = "Product.AizantIT_SP_GetProductLists";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;

                SqlParameter paramShowType = new SqlParameter("@ShowType", SqlDbType.VarChar);
                cmd.Parameters.Add(paramShowType).Value = ShowType;
                SqlParameter paramLoginEmpId = new SqlParameter("@EmpLoginID", SqlDbType.VarChar);
                cmd.Parameters.Add(paramLoginEmpId).Value = LoginEmpId;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public DataTable GetHistoryListDAL(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int id)
        {
            try
            {
                string query = "[Product].[AizantIT_SP_ProductHistory]";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramID = new SqlParameter("@Id", SqlDbType.Int);
                cmd.Parameters.Add(paramID).Value = id;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}