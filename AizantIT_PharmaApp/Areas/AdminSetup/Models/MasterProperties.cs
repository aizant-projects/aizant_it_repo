﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AizantIT_PharmaApp.Areas.AdminSetup.Models
{
    public class MasterProperties
    {
        //MarketList
        public int TotalCount { get; set; }
        public int MarketID { get; set; }
        public bool isActive { get; set; }
        public string MarketName { get; set; }
        public string MarketCode { get; set; }
        public int RowNumber { get; set; }
        
        
        public MasterProperties(int _RowNumber,int _MarketID,string _MarketName,string _MarketCode)
        {
            RowNumber = _RowNumber;
            MarketID = _MarketID;
            MarketName = _MarketName;
            MarketCode = _MarketCode;
        }
    }
    public class ClientModel
    {
        //ClientList       
        public int RowNumber { get; set; }
        public int ClientID { get; set; }
        public string ClientName { get; set; }
        public string ClientCode { get; set; }
        public bool IsActive { get; set; }
        public string Status { get; set; }

        public ClientModel(int _RowNumber, int _ClientID,string _ClientCode, string _ClientName, bool _IsActive,string _Status)
        {
            RowNumber = _RowNumber;
            ClientID = _ClientID;
            ClientCode = _ClientCode;
            ClientName = _ClientName;
            IsActive = _IsActive;
            Status = _Status;
        }
    }
    public class Client
    {
        //Client       
        public int RowNumber { get; set; }
        public int ClientID { get; set; }
        public string ClientName { get; set; }
        public string ClientCode { get; set; }
        public bool IsActive { get; set; }
        public string Comment { get; set; }

    }
    public class historyClient
    {
        public int PKID { get; set; }
        public int RowNumber { get; set; }
        public string FirstName { get; set; }
        public string RoleName { get; set; }
        public string ActionDate { get;set;}
        public string AuditActionName { get;set;}
        public string SubmittedData { get;set;}
        public string Comments { get;set;}
        public int CommentLength { get;set;}
   

        public historyClient(int _RowNumber, int _PKID, string _FirstName,string _RoleName, string _ActionDate,string _AuditActionName, string _SubmittedData,string _Comments,int _CommentLength)
        {
            RowNumber = _RowNumber;
            PKID = _PKID;
            FirstName = _FirstName;
            RoleName = _RoleName;
            ActionDate = _ActionDate;
            AuditActionName = _AuditActionName;
            SubmittedData = _SubmittedData;
            Comments = _Comments;
            CommentLength = _CommentLength;    
        }
    }
}