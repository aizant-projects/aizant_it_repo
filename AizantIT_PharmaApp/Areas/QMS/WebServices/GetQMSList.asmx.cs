﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using QMS_BusinessLayer;
using QMS_BO;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Globalization;
using System.Collections;
using QMS_BO.QMS_Masters_BO;
using QMS_BO.QMS_ChangeControl_BO;
using QMS_BO.QMS_CAPABO;
using QMS_BO.QMS_NoteTofile_BO;
using QMS_BO.QMS_Incident_BO;

namespace AizantIT_PharmaApp.Areas.QMS.WebServices
{
    /// <summary>
    /// Summary description for GetQualityEventList
    /// </summary>
    
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class GetQualityEventList : System.Web.Services.WebService
    {
        QMS_BAL QMS_BAL=new QMS_BAL();

        [WebMethod]
        public void GetActionPlanList(int iDisplayLength, int iDisplayStart, int iSortCol_0,
           string sSortDir_0, string sSearch)
        {
            //int displayLength = iDisplayLength;
            //int displayStart = iDisplayStart;
            //int sortCol = iSortCol_0;
            //string sortDir = sSortDir_0;
            //string search = sSearch;        

            List<ActionPlanList> listEmployees = new List<ActionPlanList>();
            int filteredCount = 0;
            string Operation = "Employee";

            DataSet dt = QMS_BAL.GetActionPlanLISTBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch);
            if (dt.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Tables[0].Rows)
                {
                    ActionPlanList actionPlan = new ActionPlanList();
                    actionPlan.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    actionPlan.Capa_ActionID = Convert.ToInt32(rdr["Capa_ActionID"].ToString());
                    actionPlan.Capa_ActionName = (rdr["Capa_ActionName"]).ToString();
                    actionPlan.CurrentStatus = (rdr["currentstatus"].ToString());

                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listEmployees.Add(actionPlan);
                }
            }

            var result = new
            {
                iTotalRecords = dt.Tables[1].Rows.Count,
                iTotalDisplayRecords = filteredCount,
                aaData = listEmployees
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(result));
        }
        [WebMethod]
        public void GetBlockMasterList(int iDisplayLength, int iDisplayStart, int iSortCol_0,
           string sSortDir_0, string sSearch)
        {               

            List<BlockList> listEmployees = new List<BlockList>();
            int filteredCount = 0;
           
            DataSet dt = QMS_BAL.GetBlockMasterLISTBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch);
            if (dt.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Tables[0].Rows)
                {
                    BlockList BlockList = new BlockList();
                    BlockList.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    BlockList.BlockID = Convert.ToInt32(rdr["BlockID"].ToString());
                    BlockList.BlockName = (rdr["BlockName"]).ToString();
                    BlockList.CurrentStatus = (rdr["CurrentStatus"].ToString());

                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listEmployees.Add(BlockList);
                }
            }

            var result = new
            {
                iTotalRecords = dt.Tables[1].Rows.Count,
                iTotalDisplayRecords = filteredCount,
                aaData = listEmployees
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(result));
        }
        [WebMethod]
        public void GetImpactPontsList(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch)
        {

            List<ImpactPointList> listEmployees = new List<ImpactPointList>();
            int filteredCount = 0;

            DataSet dt = QMS_BAL.GetImpactPointLISTBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch);
            if (dt.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Tables[0].Rows)
                {
                    ImpactPointList impact = new ImpactPointList();
                    impact.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    impact.Imp_Desc_ID = Convert.ToInt32(rdr["Imp_Desc_ID"].ToString());
                    impact.Impact_Description = (rdr["Impact_Description"]).ToString();
                    impact.CurrentStatus = (rdr["CurrentStatus"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listEmployees.Add(impact);
                }
            }

            var result = new
            {
                iTotalRecords = dt.Tables[1].Rows.Count,
                iTotalDisplayRecords = filteredCount,
                aaData = listEmployees
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(result));
        }
        [WebMethod]
        public void GetTypeChangeMasterList(int iDisplayLength, int iDisplayStart, int iSortCol_0,
        string sSortDir_0, string sSearch)
        {

            List<TypeChangeMasterList> listEmployees = new List<TypeChangeMasterList>();
            int filteredCount = 0;

            DataSet dt = QMS_BAL.GetTypeChangeMasterLISTBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch);
            if (dt.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Tables[0].Rows)
                {
                    TypeChangeMasterList typeChange = new TypeChangeMasterList();
                    typeChange.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    typeChange.TC_ID = Convert.ToInt32(rdr["TC_ID"].ToString());
                    typeChange.TypeofChange = (rdr["TypeofChange"]).ToString();
                    typeChange.CurrentStatus = (rdr["CurrentStatus"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listEmployees.Add(typeChange);
                }
            }

            var result = new
            {
                iTotalRecords = dt.Tables[1].Rows.Count,
                iTotalDisplayRecords = filteredCount,
                aaData = listEmployees
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(result));
        }
        [WebMethod]
        public void GetImpactPointAndDeptLinkList(int iDisplayLength, int iDisplayStart, int iSortCol_0,
       string sSortDir_0, string sSearch)
        {

            List<ImpactPointAndDeptLinkList> listEmployees = new List<ImpactPointAndDeptLinkList>();
            int filteredCount = 0;

            DataSet dt = QMS_BAL.GetImpactPointAndDeptLinkBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch);
            if (dt.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Tables[0].Rows)
                {
                    ImpactPointAndDeptLinkList typeChange = new ImpactPointAndDeptLinkList();
                    typeChange.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    typeChange.ImpPoint_ID = Convert.ToInt32(rdr["ImpPoint_ID"].ToString());
                    typeChange.Impact_Description =(rdr["Impact_Description"].ToString());
                    typeChange.Department = (rdr["Department"]).ToString();
                    typeChange.BlockName = (rdr["BlockName"]).ToString();
                    typeChange.CurrentStatus = (rdr["CurrentStatus"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listEmployees.Add(typeChange);
                }
            }

            var result = new
            {
                iTotalRecords = dt.Tables[1].Rows.Count,
                iTotalDisplayRecords = filteredCount,
                aaData = listEmployees
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(result));
        }
       
        //[WebMethod(EnableSession = true)]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        //public string GetCAPAList()
        //{
        //    var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
        //    var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
        //    var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
        //    var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
        //    var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
        //    var QA = int.Parse(HttpContext.Current.Request.Params["QA"]);
        //    var HOD = int.Parse(HttpContext.Current.Request.Params["HOD"]);
        //    var EmpID = int.Parse(HttpContext.Current.Request.Params["EmpID"]);
        //    var Role = (HttpContext.Current.Request.Params["Role"]);
        //    var Head_QA = int.Parse(HttpContext.Current.Request.Params["HQA"]);
        //    var User = int.Parse(HttpContext.Current.Request.Params["User"]);
        //    var AdminRole = int.Parse(HttpContext.Current.Request.Params["AdminRole"]);

        //    var RptDeptID = HttpContext.Current.Request.Params["DeptRpt"].ToString();
        //    var RptFrmDate = HttpContext.Current.Request.Params["FromDateRpt"].ToString();
        //    var RptToDate = HttpContext.Current.Request.Params["ToDateReport"].ToString();

        //    List<CapaModel> listEmployees = new List<CapaModel>();
        //    int filteredCount = 0;
        //    var RoleIDCharts = 0;
        //    var deptIdDashboard = 0;
        //    var FromDateDashboard = "";
        //    var ToDateDashboard = "";
        //    var CapaFileterEvent = 0;
        //    if (Session["ChartsCapaFilterData"] != null)
        //    {
        //        Hashtable ht = Session["ChartsCapaFilterData"] as Hashtable;
        //         RoleIDCharts =Convert.ToInt32( ht["RoleIDCapaChart"].ToString());
        //         deptIdDashboard = Convert.ToInt32(ht["DeptIDCapaChart"].ToString());
        //         FromDateDashboard = ht["FromDateCapachart"].ToString();
        //         ToDateDashboard = ht["ToDateCapaChart"].ToString();
        //        CapaFileterEvent = Convert.ToInt32 (ht["CapaFileterEvent"].ToString());

        //    }
        //    var OverDueCapaRoleID = 0;
        //    if (Session["OverDueRoleID"] != null)
        //    {
        //        OverDueCapaRoleID =Convert.ToInt32( Session["OverDueRoleID"]);
        //    }
        //    DataSet dt = QMS_BAL.GetCAPAListBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, EmpID, HOD, QA, Head_QA, User,Role, RoleIDCharts, deptIdDashboard, FromDateDashboard,ToDateDashboard, CapaFileterEvent, OverDueCapaRoleID, AdminRole, RptDeptID, RptFrmDate, RptToDate);
        //    if (dt.Tables[0].Rows.Count > 0)
        //    {
        //        foreach (DataRow rdr in dt.Tables[0].Rows)
        //        {
        //            CapaModel capa = new CapaModel();
        //            capa.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
        //            capa.CAPAID = Convert.ToInt32(rdr["CAPAID"].ToString());
        //            if (rdr["CAPA_Number"].ToString() == "0")
        //            {
        //                capa.CAPA_Number = "NA";
        //            }
        //            else
        //            {
        //                capa.CAPA_Number = (rdr["CAPA_Number"].ToString());
        //            }
        //            capa.CAPA_Date =Convert.ToDateTime (rdr["CAPA_Date"].ToString()).ToString("dd MMM yyyy");
        //            capa.Department = (rdr["Department"]).ToString();
        //            capa.EmployeeName = (rdr["EmployeeName"]).ToString();
        //            capa.CurrentStatus = (rdr["CurrentStatus"].ToString());
        //            capa.QualityEvent_Number = (rdr["QualityEvent_Number"].ToString());
        //            capa.QualityEvent_TypeID = (rdr["QualityEvent_TypeID"].ToString());
        //            capa.RM1 = (rdr["RM1"].ToString());
        //            capa.RM2 = (rdr["RM2"].ToString());
        //            capa.Trasaction = (rdr["Trasaction"].ToString());
        //            capa.Verified_YesOrNO = (rdr["Verified_YesOrNO"].ToString());
        //            capa.TargetDate = Convert.ToDateTime(rdr["TargetDate"]).ToString("dd MMM yyyy");
        //            capa.EmpID= Convert.ToInt32(rdr["EmpID"].ToString());
        //            capa.DeptID = Convert.ToInt32(rdr["DeptID"].ToString());
        //            if (rdr["EmpAcceptedDate"].ToString() == "")
        //            {
        //                capa.EmpAcceptedDate = null;
        //            }
        //            else
        //            {
        //                capa.EmpAcceptedDate =(rdr["EmpAcceptedDate"].ToString());

        //            }
        //            capa.CurrentStatusNumber = (int)rdr["CurrentStatusNumber"];
        //            capa.OverDue = Convert.ToInt32(rdr["OverDue"].ToString());
        //            filteredCount = Convert.ToInt32(rdr["TotalCount"]);
        //            listEmployees.Add(capa);
        //        }
        //    }

        //    var result = new
        //    {
        //        iTotalRecords = dt.Tables[1].Rows.Count,
        //        iTotalDisplayRecords = filteredCount,
        //        aaData = listEmployees
        //    };
        //    JavaScriptSerializer js = new JavaScriptSerializer();
        //    return js.Serialize(result);
        //    //JavaScriptSerializer js = new JavaScriptSerializer();
        //    //Context.Response.Write(js.Serialize(result));
        //}
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetCapaFileUploadList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
            var Capaid = int.Parse(HttpContext.Current.Request.Params["CapaID"]);
            List<CapaModel> CapaList = new List<CapaModel>();
            int filteredCount = 0;
            DataSet dt = QMS_BAL.GetCapaFileUploadListBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, Capaid);
            if (dt.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Tables[0].Rows)
                {
                    CapaModel boCapa = new CapaModel();
                    boCapa.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    boCapa.CapaFileUniqueID = Convert.ToInt32(rdr["UniquePKID"]);
                    boCapa.FileName = (rdr["Document"].ToString());
                    boCapa.FileType = (rdr["FileType"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    CapaList.Add(boCapa);
                }
            }
            var result = new
            {
                iTotalRecords = dt.Tables[0].Rows.Count,
                iTotalDisplayRecords = filteredCount,
                aaData = CapaList
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetChangeControlist()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
            var QA = int.Parse(HttpContext.Current.Request.Params["QA"]);
            var HOD = int.Parse(HttpContext.Current.Request.Params["HOD"]);
            var EmpID = int.Parse(HttpContext.Current.Request.Params["EmpID"]);
            var QAdmin = int.Parse(HttpContext.Current.Request.Params["QAdmin"]);
            var Role = (HttpContext.Current.Request.Params["Role"]);
            var Head_QA = int.Parse(HttpContext.Current.Request.Params["HQA"]);
            var CCN_ID = int.Parse(HttpContext.Current.Request.Params["CCN_ID"]);
            var EventChart= (HttpContext.Current.Request.Params["EventChart"]).ToString();
            var RoleIDCharts = Convert.ToInt32(HttpContext.Current.Request.Params["RoleIDCharts"]);
            var deptIdDashboard = Convert.ToInt32(HttpContext.Current.Request.Params["DeptDashboard"]);
            var FromDateDashboard = (HttpContext.Current.Request.Params["FromDateDashboard"]).ToString();
            var ToDateDashboard = (HttpContext.Current.Request.Params["ToDateDashboard"]).ToString();
            List<CCNList> listEmployees = new List<CCNList>();
            int filteredCount = 0;
            var FromDate = "";
            var ToDate = "";
            if (FromDateDashboard == "0")
            {
                FromDate ="";
            }
            else
            {
                FromDate = FromDateDashboard;
            }
            if (ToDateDashboard == "0")
            {
                ToDate = "";
            }
            else
            {
                ToDate = ToDateDashboard;
            }
            DataSet dt = QMS_BAL.GetCCNListBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, EmpID, HOD, QA,
                Head_QA,Role, CCN_ID, QAdmin, EventChart, RoleIDCharts, deptIdDashboard, FromDate, ToDate);
            if (dt.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Tables[0].Rows)
                {
                    CCNList CCNList = new CCNList();
                    CCNList.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    CCNList.CCNID = Convert.ToInt32(rdr["CCN_ID"].ToString());
                    CCNList.BlockID = Convert.ToInt32(rdr["BlockID"].ToString());
                    CCNList.CCN_Number = (rdr["CCN_No"].ToString());
                    CCNList.Block = (rdr["BlockName"].ToString());
                    CCNList.CCN_Date = (rdr["CCN_Date"].ToString());
                    CCNList.Department = (rdr["Department"]).ToString();
                    CCNList.Initiatedby = rdr["Initiatedby"].ToString();
                    CCNList.CurrentStatus = (rdr["CurrentStatus"]).ToString();
                    CCNList.Transaction = (rdr["Transction"].ToString());
                    CCNList.AssessmentID = Convert.ToInt32(rdr["AssessmentID"]);
                    CCNList.Classification = (rdr["Classification"]).ToString();
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listEmployees.Add(CCNList);
                }
            }

            var result = new
            {
                iTotalRecords = dt.Tables[1].Rows.Count,
                iTotalDisplayRecords = filteredCount,
                aaData = listEmployees
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
            //JavaScriptSerializer js = new JavaScriptSerializer();
            //Context.Response.Write(js.Serialize(result));
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        //This method is not using from webservice, it is using from Controller 28-01-2019
        public string GetCCNReportList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
            //var QA = int.Parse(HttpContext.Current.Request.Params["QA"]);
            var Role = (HttpContext.Current.Request.Params["Role"]);
            var deptIdReport = Convert.ToInt32(HttpContext.Current.Request.Params["DeptRpt"]);
            var FromDateReport = (HttpContext.Current.Request.Params["FromDateRpt"]).ToString();
            var ToDateReport = (HttpContext.Current.Request.Params["ToDateReport"]).ToString();
            List<CCNList> listEmployeesReport = new List<CCNList>();
            int filteredCount = 0;
            if (deptIdReport == 0)
            {

            }
            //DataSet dt = QMS_BAL.GetCCNReportListBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, Role, deptIdReport, FromDateReport, ToDateReport);//
            //if (dt.Tables[0].Rows.Count > 0)
            //{
            //    foreach (DataRow rdr in dt.Tables[0].Rows)
            //    {
            //        CCNList CCNList = new CCNList();
            //        CCNList.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
            //        CCNList.CCNID = Convert.ToInt32(rdr["CCN_ID"].ToString());
            //        CCNList.BlockID = Convert.ToInt32(rdr["BlockID"].ToString());
            //        CCNList.CCN_Number = (rdr["CCN_No"].ToString());
            //        CCNList.Block = (rdr["BlockName"].ToString());
            //        CCNList.CCN_Date = (rdr["CCN_Date"].ToString());
            //        CCNList.Department = (rdr["Department"]).ToString();
            //        CCNList.Initiatedby = rdr["Initiatedby"].ToString();
            //        CCNList.CurrentStatus = (rdr["CurrentStatus"]).ToString();
            //        CCNList.Transaction = (rdr["Transction"].ToString());
            //        CCNList.AssessmentID = Convert.ToInt32(rdr["AssessmentID"]);
            //        CCNList.Classification = (rdr["Classification"]).ToString();
            //        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
            //        listEmployeesReport.Add(CCNList);
            //    }
            //}

            var result = new
            {
               // iTotalRecords = dt.Tables[1].Rows.Count,
                iTotalDisplayRecords = filteredCount,
                aaData = listEmployeesReport
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
            //JavaScriptSerializer js = new JavaScriptSerializer();
            //Context.Response.Write(js.Serialize(result));
        }
       
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        //This method is not using in webservice, this method is using in ChangeController
        public string GetCCN_DeptAssementList()
        {

            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);         
            var CCN_No = int.Parse(HttpContext.Current.Request.Params["CCN_No"]);

            List<ChangeControlCommitte> listEmployees = new List<ChangeControlCommitte>();
            int filteredCount = 0;
            CCNBal qMS_BAL = new CCNBal();
            DataSet dt = qMS_BAL.GetCCN_DeptAssBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch,  CCN_No);
            if (dt.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Tables[0].Rows)
                {
                    ChangeControlCommitte capa = new ChangeControlCommitte();
                    capa.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    capa.UniqueID = Convert.ToInt32(rdr["UniqueID"]);
                    capa.BlockID = Convert.ToInt32(rdr["BlockID"].ToString());
                    capa.BlockName = (rdr["BlockName"].ToString());
                    capa.EmpID = Convert.ToInt32(rdr["EmpID"].ToString());
                    capa.EmployeeName = (rdr["EmmployeeName"]).ToString();
                    capa.DeptName = (rdr["DeptName"]).ToString();
                    capa.DeptID = (rdr["DeptID"].ToString());
                    capa.CuurentStatus = (rdr["CurrentStatus"].ToString());
                    capa.CCN_ID =Convert.ToInt32 (rdr["CCN_ID"].ToString());
                    capa.CurrentStatusNumber = (int)rdr["curentStatusNumber"];//Using this display icons in CNHODApprove Page
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listEmployees.Add(capa);
                }
            }

            var result = new
            {
                iTotalRecords = dt.Tables[1].Rows.Count,
                iTotalDisplayRecords = filteredCount,
                aaData = listEmployees
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetCCN_verificationList()
        {

            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
            var CCN_No = int.Parse(HttpContext.Current.Request.Params["CCN_No"]);

            List<CCN_Verification> listEmployees = new List<CCN_Verification>();
            int filteredCount = 0;
            CCNBal qMS_BAL = new CCNBal();
            DataSet dt = qMS_BAL.GetCCN_VerificationBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, CCN_No);
            if (dt.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Tables[0].Rows)
                {
                    CCN_Verification capa = new CCN_Verification();
                    capa.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    capa.UniqueID = Convert.ToInt32(rdr["UniqueID"]);
                    capa.CCN_Number = (rdr["CCN_No"].ToString());
                    capa.EmpName = (rdr["EmployeeName"].ToString());
                    capa.Implementation_Date = Convert.ToDateTime(rdr["impDate"].ToString()).ToString("dd MMM yyyy");
                    capa.QAName = (rdr["QAName"]).ToString();
                    capa.deptname = (rdr["DeptName"]).ToString();
                    capa.Comments = (rdr["Comments"].ToString());                 

                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listEmployees.Add(capa);
                }
            }

            var result = new
            {
                iTotalRecords = dt.Tables[1].Rows.Count,
                iTotalDisplayRecords = filteredCount,
                aaData = listEmployees
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #region errata
        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        //This Webservice method is not using
        //public string GetErrata_List()
        //{
        //    var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
        //    var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
        //    var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
        //    var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
        //    var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
        //    var Role = (HttpContext.Current.Request.Params["Role"]);
        //    var UserID = (HttpContext.Current.Request.Params["UserID"]);
        //    var RoleIDCharts = Convert.ToInt32(HttpContext.Current.Request.Params["RoleIDCharts"]);
        //    var deptIdDashboard = Convert.ToInt32(HttpContext.Current.Request.Params["DeptDashboard"]);
        //    var FromDateDashboard = (HttpContext.Current.Request.Params["FromDateDashboard"]).ToString();
        //    var ToDateDashboard = (HttpContext.Current.Request.Params["ToDateDashboard"]).ToString();

        //    List<ErrataBO> listEmployees = new List<ErrataBO>();
        //    int filteredCount = 0;
        //    var FromDate = "";
        //    var ToDate = "";
        //    if (FromDateDashboard == "0")
        //    {
        //        FromDate = "";
        //    }
        //    else
        //    {
        //        FromDate = FromDateDashboard;
        //    }
        //    if (ToDateDashboard == "0")
        //    {
        //        ToDate = "";
        //    }
        //    else
        //    {
        //        ToDate = ToDateDashboard;
        //    }
        //    DataSet dt = QMS_BAL.GetErrataListDalBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, Role, UserID, RoleIDCharts, deptIdDashboard, FromDate, ToDate);
        //    if (dt.Tables[0].Rows.Count > 0)
        //    {
        //        foreach (DataRow rdr in dt.Tables[0].Rows)
        //        {
        //            ErrataBO ERRTBO = new ErrataBO();
        //            ERRTBO.Rownumber = Convert.ToInt32(rdr["RowNumber"]);
        //            ERRTBO.ER_ID = Convert.ToInt32(rdr["ER_ID"]);
        //            ERRTBO.DeptName = ((rdr["Department"]).ToString());
        //            ERRTBO.ErrataFileNo = (rdr["ER_file_number"]).ToString();
        //            ERRTBO.DocumentNo = (rdr["DocumentNo"].ToString());
        //            ERRTBO.DocTitle = (rdr["DocTitle"]).ToString();
        //            ERRTBO.EmpName = (rdr["preparedBy"]).ToString();
        //            ERRTBO.Initiator_SubmitedDate = (rdr["CreatedDate"].ToString());
        //            ERRTBO.SectionNo = Convert.ToString((rdr["Section"].ToString()));
        //            ERRTBO.Status = ((rdr["CurrentStatus"])).ToString();
        //            filteredCount = Convert.ToInt32(rdr["TotalCount"]);
        //            listEmployees.Add(ERRTBO);
        //        }
        //    }

        //    var result = new
        //    {
        //        iTotalRecords = dt.Tables[0].Rows.Count,
        //        iTotalDisplayRecords = filteredCount,
        //        aaData = listEmployees
        //    };
        //    JavaScriptSerializer js = new JavaScriptSerializer();
        //    return js.Serialize(result);
        //    //JavaScriptSerializer js = new JavaScriptSerializer();
        //    //Context.Response.Write(js.Serialize(result));
        //}
        #endregion

        #region Incident
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetIncident_List()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var UserID = int.Parse(HttpContext.Current.Request.Params["UserID"]);
            var ShowType = int.Parse(HttpContext.Current.Request.Params["ShowType"]);
            var RoleIDCharts =0;
            var deptIdDashboard =0;
            var FromDate = "";
            var ToDate = "";
            // commented on 05-02-2020
            //var sSearch_EmpCode = HttpContext.Current.Request.Params["sSearch_0"].ToString();

            var sSearch_IRNumber = HttpContext.Current.Request.Params["sSearch_1"].ToString();
            var sSearch_DeptName = HttpContext.Current.Request.Params["sSearch_2"];
            var sSearch_DocumentNo = HttpContext.Current.Request.Params["sSearch_3"];
            var sSearch_DocumentName = HttpContext.Current.Request.Params["sSearch_4"];
            var sSearch_EmpName = HttpContext.Current.Request.Params["sSearch_5"];
            var sSearch_CreatedDate = HttpContext.Current.Request.Params["sSearch_6"];
            var sSearch_OccurrenceDate = HttpContext.Current.Request.Params["sSearch_7"];
            var sSearch_IncidentDueDate = HttpContext.Current.Request.Params["sSearch_8"];
            var sSearch_CurrentStatus = HttpContext.Current.Request.Params["sSearch_9"];

            // commented on 05-02-2020


            List<IncidentBO> listIncident = new List<IncidentBO>();
            int filteredCount = 0;
            var IncidentFileterEvent = "";
            if (Session["IncidentfromandtoDates"] != null)
            {
                Hashtable ht = Session["IncidentfromandtoDates"] as Hashtable;
                RoleIDCharts = Convert.ToInt32(ht["inciRoleID"].ToString());
                deptIdDashboard = Convert.ToInt32(ht["inciDeptID"].ToString());
                FromDate = ht["inciFromDate"].ToString();
                ToDate = ht["inciToDate"].ToString();
                IncidentFileterEvent =ht["TypeofIncidentID"].ToString();
            }
            DataSet dt = QMS_BAL.GetIncidentList(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, UserID, ShowType, RoleIDCharts, deptIdDashboard, FromDate, ToDate, IncidentFileterEvent,
                sSearch_IRNumber, sSearch_DeptName, sSearch_DocumentNo, sSearch_DocumentName, sSearch_EmpName, sSearch_CreatedDate, sSearch_OccurrenceDate, sSearch_IncidentDueDate, sSearch_CurrentStatus);
            if (dt.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Tables[0].Rows)
                {
                    IncidentBO IncdntBO = new IncidentBO();
                    IncdntBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    IncdntBO.IRNumber = (rdr["IRNumber"]).ToString();
                    IncdntBO.DocumentName= (rdr["DocumentName"].ToString());
                    IncdntBO.CreatedDate = Convert.ToDateTime(rdr["CreatedDate"].ToString()).ToString("dd MMM yyyy");
                    IncdntBO.DocumentNo = (rdr["DocumentNo"].ToString());
                    IncdntBO.IncidentDueDate= Convert.ToDateTime(rdr["DueDate"].ToString()).ToString("dd MMM yyyy");
                    IncdntBO.EmpName = (rdr["CreatedBy"]).ToString();
                    IncdntBO.DeptName = ((rdr["Department"]).ToString());
                    IncdntBO.OccurrenceDate = Convert.ToDateTime(rdr["OccurrenceDate"].ToString()).ToString("dd MMM yyyy");
                    IncdntBO.IncidentMasterID = Convert.ToInt32(rdr["IncidentMasterID"]);
                    IncdntBO.CurrentStatus = ((rdr["CurrentStatus"])).ToString();
                    IncdntBO.CurrentStatusID = Convert.ToInt32(rdr["CurrentStatusID"]);
                    //IncdntBO.DocName = (rdr["DocName"]).ToString();
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listIncident.Add(IncdntBO);
                }
            }
            var result = new
            {
                iTotalRecords = dt.Tables[0].Rows.Count,
                iTotalDisplayRecords = filteredCount,
                aaData = listIncident
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }


        //For Getting Data of Incident Report List
        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        ////This method is not using in place of this calling from controller
        //public string GetIncidentReport_List()
        //{
        //    var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
        //    var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
        //    var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
        //    var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
        //    var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
        //    var UserID = int.Parse(HttpContext.Current.Request.Params["UserID"]);
        //    //start commented by pradeep
        //    var deptIdReport = Convert.ToInt32(HttpContext.Current.Request.Params["DeptRpt"]);
        //    var FromDateReport = (HttpContext.Current.Request.Params["FromDateRpt"]).ToString();
        //    var ToDateReport = (HttpContext.Current.Request.Params["ToDateReport"]).ToString();
        //    //end commented by pradeep

        //    List<IncidentBO> listIncident = new List<IncidentBO>();
        //    int filteredCount = 0;
        //    DataSet dt = QMS_BAL.GetIncidentReportList(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, UserID, deptIdReport, FromDateReport, ToDateReport);
        //    if (dt.Tables[0].Rows.Count > 0)
        //    {
        //        foreach (DataRow rdr in dt.Tables[0].Rows)
        //        {
        //            IncidentBO IncdntBO = new IncidentBO();
        //            IncdntBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
        //            IncdntBO.IRNumber = (rdr["IRNumber"]).ToString();
        //            IncdntBO.DocumentName = (rdr["DocumentName"].ToString());
        //            IncdntBO.CreatedDate = Convert.ToDateTime(rdr["CreatedDate"].ToString()).ToString("dd MMM yyyy");
        //            IncdntBO.DocumentNo = (rdr["DocumentNo"].ToString());
        //            IncdntBO.IncidentDueDate = Convert.ToDateTime(rdr["DueDate"].ToString()).ToString("dd MMM yyyy");
        //            IncdntBO.EmpName = (rdr["CreatedBy"]).ToString();
        //            IncdntBO.DeptName = ((rdr["Department"]).ToString());
        //            IncdntBO.OccurrenceDate = Convert.ToDateTime(rdr["OccurrenceDate"].ToString()).ToString("dd MMM yyyy");
        //            IncdntBO.IncidentMasterID = Convert.ToInt32(rdr["IncidentMasterID"]);
        //            IncdntBO.CurrentStatus = ((rdr["CurrentStatus"])).ToString();
        //            IncdntBO.CurrentStatusID = Convert.ToInt32(rdr["CurrentStatusID"]);
        //            //IncdntBO.DocName = (rdr["DocName"]).ToString();
        //            filteredCount = Convert.ToInt32(rdr["TotalCount"]);
        //            listIncident.Add(IncdntBO);
        //        }
        //    }
        //    var result = new
        //    {
        //        iTotalRecords = dt.Tables[0].Rows.Count,
        //        iTotalDisplayRecords = filteredCount,
        //        aaData = listIncident
        //    };
        //    JavaScriptSerializer js = new JavaScriptSerializer();
        //    return js.Serialize(result);
        //}
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        //This method is not using from webservice
        public string GetIncidentPreliminaryAssessmentList1()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
            var IncidentMasterID = int.Parse(HttpContext.Current.Request.Params["IncidentMasterID"]);
            List<IncidentBO> Incidentlst = new List<IncidentBO>();
            int filteredCount = 0;
            DataSet dt = QMS_BAL.GetIncidentPreliminaryAssessmentListBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IncidentMasterID,0);
            if (dt.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Tables[0].Rows)
                {
                    IncidentBO IncdntBO = new IncidentBO();
                    IncdntBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    IncdntBO.UniquePKID = Convert.ToInt32(rdr["UniquePKID"]);
                    IncdntBO.DeptName = (rdr["Department"].ToString());
                    IncdntBO.CapaNumber = (rdr["CAPA_Number"].ToString());
                    IncdntBO.AssCapaID = Convert.ToInt32(rdr["CAPAID"].ToString());
                    IncdntBO.CapaStatusName = (rdr["StatusName"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    Incidentlst.Add(IncdntBO);
                }
            }
            var result = new
            {
                iTotalRecords = dt.Tables[0].Rows.Count,
                iTotalDisplayRecords = filteredCount,
                aaData = Incidentlst
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        //This method is not using from webservice
        public string GetIncidentAnyotherinvestigationList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);

            var IncidentMasterID = int.Parse(HttpContext.Current.Request.Params["IncidentMasterID"]);
            List<IncidentBO> Incidentlst = new List<IncidentBO>();
            int filteredCount = 0;
            DataSet dt = QMS_BAL.GetIncidentAnyotherinvestigationBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IncidentMasterID,0);
            if (dt.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Tables[0].Rows)
                {
                    IncidentBO IncdntBO = new IncidentBO();
                    IncdntBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    IncdntBO.UniquePKID = Convert.ToInt32(rdr["UniquePKID"]);
                    IncdntBO.DeptName = (rdr["Department"].ToString());
                    IncdntBO.IRNumber = (rdr["IRNumber"].ToString());
                    IncdntBO.AssIncidentMasterID = Convert.ToInt32(rdr["IncidentID"]);
                    IncdntBO.AssessmntIncStatus = (rdr["IncidentStatus"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    Incidentlst.Add(IncdntBO);
                }
            }
            var result = new
            {
                iTotalRecords = dt.Tables[0].Rows.Count,
                iTotalDisplayRecords = filteredCount,
                aaData = Incidentlst
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        //This method is not using in Webservice
        public string GetWhoarethepeopleinvolvedList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
            var IncidentMasterID = int.Parse(HttpContext.Current.Request.Params["IncidentMasterID"]);
            List<IncidentBO> Incidentlst = new List<IncidentBO>();
            int filteredCount = 0;
            DataSet dt = QMS_BAL.GetIncidentWhoarethepeopleinvolvedListBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IncidentMasterID);
            if (dt.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Tables[0].Rows)
                {
                    IncidentBO IncdntBO = new IncidentBO();
                    IncdntBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    IncdntBO.UniquePKID = Convert.ToInt32(rdr["UniquePKID"]);
                    IncdntBO.DeptName = (rdr["Department"].ToString());
                    IncdntBO.EmpName = (rdr["EmpName"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    Incidentlst.Add(IncdntBO);
                }
            }
            var result = new
            {
                iTotalRecords = dt.Tables[0].Rows.Count,
                iTotalDisplayRecords = filteredCount,
                aaData = Incidentlst
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        //This method is not using in webservice
        public string GetIncidentHasAffectedDocumentList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
            var IncidentMasterID = int.Parse(HttpContext.Current.Request.Params["IncidentMasterID"]);
            List<IncidentBO> Incidentlst = new List<IncidentBO>();
            int filteredCount = 0;
            DataSet dt = QMS_BAL.GetIncidentHasAffectedDocumentListtBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IncidentMasterID);
            if (dt.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Tables[0].Rows)
                {
                    IncidentBO IncdntBO = new IncidentBO();
                    IncdntBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    IncdntBO.UniquePKID = Convert.ToInt32(rdr["UniquePKID"]);
                    IncdntBO.HasDeptName = (rdr["Department"].ToString());
                    IncdntBO.HasAffectedDocument = (rdr["DocumentName"].ToString());
                    IncdntBO.HasAffectedDocRefNo = (rdr["ReferenceNumber"].ToString());
                    IncdntBO.HasAffectedPageNo = (rdr["PageNo"].ToString());
                    IncdntBO.HasAffectedComments = (rdr["Comments"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    Incidentlst.Add(IncdntBO);
                }
            }
            var result = new
            {
                iTotalRecords = dt.Tables[0].Rows.Count,
                iTotalDisplayRecords = filteredCount,
                aaData = Incidentlst
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        //This method is not using in webservice
        public string GetIncidentFileUploadList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
            var IncidentMasterID = int.Parse(HttpContext.Current.Request.Params["IncidentMasterID"]);
            List<IncidentBO> Incidentlst = new List<IncidentBO>();
            int filteredCount = 0;
            DataSet dt = QMS_BAL.GetIncidentFileUploadListBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IncidentMasterID);
            if (dt.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Tables[0].Rows)
                {
                    IncidentBO IncdntBO = new IncidentBO();
                    IncdntBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    IncdntBO.UniquePKID = Convert.ToInt32(rdr["UniquePKID"]);
                    IncdntBO.TypeofDocumentName = (rdr["TypeofDocument"].ToString());
                    IncdntBO.FileDocumentName = (rdr["Document"].ToString());
                    IncdntBO.FileType = (rdr["FileType"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    Incidentlst.Add(IncdntBO);
                }
            }
            var result = new
            {
                iTotalRecords = dt.Tables[0].Rows.Count,
                iTotalDisplayRecords = filteredCount,
                aaData = Incidentlst
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        //This method is not using in Webservice
        public string GetIncidentDepartmentReviewHodList()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
            var IncidentMasterID = int.Parse(HttpContext.Current.Request.Params["IncidentMasterID"]);
            List<IncidentBO> Incidentlst = new List<IncidentBO>();
            int filteredCount = 0;
            DataSet dt = QMS_BAL.GetIncidentDepartmentReviewHodListBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IncidentMasterID);
            if (dt.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Tables[0].Rows)
                {
                    IncidentBO IncdntBO = new IncidentBO();
                    IncdntBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    IncdntBO.UniquePKID = Convert.ToInt32(rdr["UniquePKID"]);
                    IncdntBO.DeptName = (rdr["Department"].ToString());
                    IncdntBO.EmpName = (rdr["EmpName"].ToString());
                    if(Convert.ToDateTime(rdr["SubmittedDate"].ToString()).ToString("dd MMM yyyy hh:mm:ss")== "01 Jan 1900 12:00:00")
                    {
                        IncdntBO.HodSubmittedDate = "";
                    }
                    else
                    {
                        IncdntBO.HodSubmittedDate = Convert.ToDateTime(rdr["SubmittedDate"].ToString()).ToString("dd MMM yyyy hh:mm:ss");
                    }
                    IncdntBO.HodReviewStatus = (rdr["ActionStatus"].ToString());
                    if (Convert.ToInt32(rdr["HodCommLength"]) > 0)
                    {
                        IncdntBO.DeptHodReviewComments = (rdr["HODComments"].ToString());
                    }
                    else
                    {
                        IncdntBO.DeptHodReviewComments = (rdr["HODComments"].ToString());
                    }
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    Incidentlst.Add(IncdntBO);
                    
                }
                
            }
            var result = new
            {
                iTotalRecords = dt.Tables[0].Rows.Count,
                iTotalDisplayRecords = filteredCount,
                aaData = Incidentlst
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        //This method is not using in webservice
        public string GetIncidentTTS_List()
        {
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            var iSortCol_0 = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString(System.Globalization.CultureInfo.CurrentCulture);
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToString(CultureInfo.CurrentCulture);
            var IncidentMasterID = int.Parse(HttpContext.Current.Request.Params["IncidentMasterID"]);
            List<IncidentBO> Incidentlst = new List<IncidentBO>();
            int filteredCount = 0;
            DataTable dt = QMS_BAL.GetIncidentTTS_ListBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IncidentMasterID);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Rows)
                {
                    IncidentBO IncdntBO = new IncidentBO();
                    IncdntBO.TTSID= Convert.ToInt32(rdr["TTS_ID"]);
                    IncdntBO.TTSDocName = rdr["DocumentName"].ToString();
                    IncdntBO.TTSDept = rdr["DepartmentName"].ToString();
                    IncdntBO.TTSTypeofTraining = rdr["TypeOfTraining"].ToString();
                    IncdntBO.TTSAuthor = rdr["Author"].ToString();
                    IncdntBO.TTSQA = rdr["Reviewer"].ToString();
                    IncdntBO.TTSDate = rdr["ApproveDate"].ToString();
                    IncdntBO.TTSStatus = rdr["StatusName"].ToString();
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    Incidentlst.Add(IncdntBO);
                }
            }
            var result = new
            {
                iTotalRecords = dt.Rows.Count,
                iTotalDisplayRecords = filteredCount,
                aaData = Incidentlst
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(result);
        }
        #endregion
    }
}
