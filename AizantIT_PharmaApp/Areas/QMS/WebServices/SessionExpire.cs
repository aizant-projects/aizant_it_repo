﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AizantIT_PharmaApp.Areas.QMS.WebServices
{
    public class SessionExpireAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;
            // check  sessions here
            if (filterContext.HttpContext.Request.Cookies.Get("CookieEmpID") == null)
            {
                filterContext.Result = new RedirectResult("~//UserLogin.aspx");
                return;
            }
            //if (HttpContext.Current.Session["UserSignIn"] == null)
            //{
            //    //Redirect(Url.Content("~/UserLogin.aspx"));
            //    filterContext.Result = new RedirectResult("~//UserLogin.aspx");
            //    return;
            //}
            base.OnActionExecuting(filterContext);
        }
    }
}
