﻿namespace AizantIT_PharmaApp.Areas.QMS.XtraReports.Incident
{
	partial class IncidentReportPage3
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrInvestCreatedDate1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblInvestDesigName1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableInvestigatorSignaturetxt = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrlblInvestName1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrInvestCreat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableImpactOFIncidentForQualityNonImpacting1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableImpactofincidentonpreviouslblForQNImpact = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrlbQNonImpactInvestandConclusion = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbxCompanyLogo = new DevExpress.XtraReports.UI.XRPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableInvestigatorSignaturetxt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableImpactOFIncidentForQualityNonImpacting1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableImpactofincidentonpreviouslblForQNImpact)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrInvestCreatedDate1,
            this.xrlblInvestDesigName1,
            this.xrTableInvestigatorSignaturetxt,
            this.xrTableImpactOFIncidentForQualityNonImpacting1,
            this.xrTableImpactofincidentonpreviouslblForQNImpact,
            this.xrTable3,
            this.xrTable4});
            this.Detail.HeightF = 871.875F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrInvestCreatedDate1
            // 
            this.xrInvestCreatedDate1.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrInvestCreatedDate1.LocationFloat = new DevExpress.Utils.PointFloat(599.1683F, 329.1669F);
            this.xrInvestCreatedDate1.Multiline = true;
            this.xrInvestCreatedDate1.Name = "xrInvestCreatedDate1";
            this.xrInvestCreatedDate1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrInvestCreatedDate1.SizeF = new System.Drawing.SizeF(184.8315F, 38.3335F);
            this.xrInvestCreatedDate1.StylePriority.UseFont = false;
            this.xrInvestCreatedDate1.StylePriority.UseTextAlignment = false;
            this.xrInvestCreatedDate1.Text = "xrInvestCreatedDate1";
            this.xrInvestCreatedDate1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrlblInvestDesigName1
            // 
            this.xrlblInvestDesigName1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlblInvestDesigName1.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrlblInvestDesigName1.ForeColor = System.Drawing.Color.Blue;
            this.xrlblInvestDesigName1.LocationFloat = new DevExpress.Utils.PointFloat(147.5682F, 329.1669F);
            this.xrlblInvestDesigName1.Multiline = true;
            this.xrlblInvestDesigName1.Name = "xrlblInvestDesigName1";
            this.xrlblInvestDesigName1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblInvestDesigName1.SizeF = new System.Drawing.SizeF(451.6001F, 38.33353F);
            this.xrlblInvestDesigName1.StylePriority.UseBorders = false;
            this.xrlblInvestDesigName1.StylePriority.UseFont = false;
            this.xrlblInvestDesigName1.StylePriority.UseForeColor = false;
            this.xrlblInvestDesigName1.StylePriority.UseTextAlignment = false;
            this.xrlblInvestDesigName1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrTableInvestigatorSignaturetxt
            // 
            this.xrTableInvestigatorSignaturetxt.LocationFloat = new DevExpress.Utils.PointFloat(0F, 284.3751F);
            this.xrTableInvestigatorSignaturetxt.Name = "xrTableInvestigatorSignaturetxt";
            this.xrTableInvestigatorSignaturetxt.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow37});
            this.xrTableInvestigatorSignaturetxt.SizeF = new System.Drawing.SizeF(784F, 44.79181F);
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55,
            this.xrTableCell5,
            this.xrInvestCreat});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.StylePriority.UseBorders = false;
            this.xrTableRow37.Weight = 1D;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Multiline = true;
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell55.StylePriority.UseBorders = false;
            this.xrTableCell55.StylePriority.UseFont = false;
            this.xrTableCell55.StylePriority.UseTextAlignment = false;
            this.xrTableCell55.Text = "Investigated By: \t";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 0.5646742710656355D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrlblInvestName1});
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell5.Multiline = true;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.StylePriority.UsePadding = false;
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell5.Weight = 1.728061865584315D;
            // 
            // xrlblInvestName1
            // 
            this.xrlblInvestName1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlblInvestName1.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrlblInvestName1.ForeColor = System.Drawing.Color.Blue;
            this.xrlblInvestName1.LocationFloat = new DevExpress.Utils.PointFloat(10.00005F, 10.00037F);
            this.xrlblInvestName1.Multiline = true;
            this.xrlblInvestName1.Name = "xrlblInvestName1";
            this.xrlblInvestName1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrlblInvestName1.SizeF = new System.Drawing.SizeF(431.6F, 27.91644F);
            this.xrlblInvestName1.StylePriority.UseBorders = false;
            this.xrlblInvestName1.StylePriority.UseFont = false;
            this.xrlblInvestName1.StylePriority.UseForeColor = false;
            // 
            // xrInvestCreat
            // 
            this.xrInvestCreat.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrInvestCreat.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrInvestCreat.Multiline = true;
            this.xrInvestCreat.Name = "xrInvestCreat";
            this.xrInvestCreat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrInvestCreat.StylePriority.UseBorders = false;
            this.xrInvestCreat.StylePriority.UseFont = false;
            this.xrInvestCreat.StylePriority.UseTextAlignment = false;
            this.xrInvestCreat.Text = "Investigation Date";
            this.xrInvestCreat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrInvestCreat.Weight = 0.70726386335005009D;
            // 
            // xrTableImpactOFIncidentForQualityNonImpacting1
            // 
            this.xrTableImpactOFIncidentForQualityNonImpacting1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableImpactOFIncidentForQualityNonImpacting1.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrTableImpactOFIncidentForQualityNonImpacting1.LocationFloat = new DevExpress.Utils.PointFloat(1.045609F, 227.7083F);
            this.xrTableImpactOFIncidentForQualityNonImpacting1.Name = "xrTableImpactOFIncidentForQualityNonImpacting1";
            this.xrTableImpactOFIncidentForQualityNonImpacting1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22});
            this.xrTableImpactOFIncidentForQualityNonImpacting1.SizeF = new System.Drawing.SizeF(782.9542F, 36.45836F);
            this.xrTableImpactOFIncidentForQualityNonImpacting1.StylePriority.UseBorders = false;
            this.xrTableImpactOFIncidentForQualityNonImpacting1.StylePriority.UseFont = false;
            this.xrTableImpactOFIncidentForQualityNonImpacting1.StylePriority.UseTextAlignment = false;
            this.xrTableImpactOFIncidentForQualityNonImpacting1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.xrTableCell22,
            this.xrTableCell26,
            this.xrTableCell27});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 1D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell21.Multiline = true;
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.Text = "Impact Points\t";
            this.xrTableCell21.Weight = 1.8333331298296014D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell22.Multiline = true;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.Text = "Status\t";
            this.xrTableCell22.Weight = 0.843750305481026D;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell26.Multiline = true;
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.Text = "Related Document Reference No";
            this.xrTableCell26.Weight = 2.8320822139526967D;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell27.Multiline = true;
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell27.StylePriority.UseFont = false;
            this.xrTableCell27.StylePriority.UsePadding = false;
            this.xrTableCell27.Text = "Comments";
            this.xrTableCell27.Weight = 1.6483343481472925D;
            // 
            // xrTableImpactofincidentonpreviouslblForQNImpact
            // 
            this.xrTableImpactofincidentonpreviouslblForQNImpact.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableImpactofincidentonpreviouslblForQNImpact.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableImpactofincidentonpreviouslblForQNImpact.LocationFloat = new DevExpress.Utils.PointFloat(1.045609F, 188.1249F);
            this.xrTableImpactofincidentonpreviouslblForQNImpact.Name = "xrTableImpactofincidentonpreviouslblForQNImpact";
            this.xrTableImpactofincidentonpreviouslblForQNImpact.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow20});
            this.xrTableImpactofincidentonpreviouslblForQNImpact.SizeF = new System.Drawing.SizeF(782.9542F, 39.58337F);
            this.xrTableImpactofincidentonpreviouslblForQNImpact.StylePriority.UseBorders = false;
            this.xrTableImpactofincidentonpreviouslblForQNImpact.StylePriority.UseFont = false;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 1D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell20.Multiline = true;
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell20.StylePriority.UseBorders = false;
            this.xrTableCell20.StylePriority.UseFont = false;
            this.xrTableCell20.StylePriority.UseTextAlignment = false;
            this.xrTableCell20.Text = "Impact of Incident on previous batches / related products / process / study";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell20.Weight = 3D;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0.04561742F, 50.62503F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11});
            this.xrTable3.SizeF = new System.Drawing.SizeF(783.9542F, 137.4999F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseFont = false;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrlbQNonImpactInvestandConclusion});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1D;
            // 
            // xrlbQNonImpactInvestandConclusion
            // 
            this.xrlbQNonImpactInvestandConclusion.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrlbQNonImpactInvestandConclusion.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrlbQNonImpactInvestandConclusion.Multiline = true;
            this.xrlbQNonImpactInvestandConclusion.Name = "xrlbQNonImpactInvestandConclusion";
            this.xrlbQNonImpactInvestandConclusion.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlbQNonImpactInvestandConclusion.StylePriority.UseBorders = false;
            this.xrlbQNonImpactInvestandConclusion.StylePriority.UseFont = false;
            this.xrlbQNonImpactInvestandConclusion.Weight = 3D;
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 10.00001F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow16});
            this.xrTable4.SizeF = new System.Drawing.SizeF(783.9999F, 40.62502F);
            this.xrTable4.StylePriority.UseBorders = false;
            this.xrTable4.StylePriority.UseFont = false;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell30});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1D;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel11,
            this.xrLabel12});
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell30.Multiline = true;
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell30.StylePriority.UseFont = false;
            this.xrTableCell30.StylePriority.UsePadding = false;
            this.xrTableCell30.StylePriority.UseTextAlignment = false;
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell30.Weight = 3D;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(91.67064F, 7.625008F);
            this.xrLabel11.Multiline = true;
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(225F, 23F);
            this.xrLabel11.StylePriority.UseBorders = false;
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.Text = "Investigation and Conclusion:";
            // 
            // xrLabel12
            // 
            this.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(1.045616F, 7.625038F);
            this.xrLabel12.Multiline = true;
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(90.62502F, 23F);
            this.xrLabel12.StylePriority.UseBorders = false;
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.Text = "(Section III)";
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.HeightF = 43.75F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(690.25F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(93.75F, 23F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.TextFormatString = "page {0} of {1}";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7,
            this.pbxCompanyLogo});
            this.PageHeader.HeightF = 104.1667F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrTable7
            // 
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 59.37498F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13});
            this.xrTable7.SizeF = new System.Drawing.SizeF(783.9998F, 34.375F);
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell2.Multiline = true;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "INCIDENT REPORT";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell2.Weight = 1D;
            // 
            // pbxCompanyLogo
            // 
            this.pbxCompanyLogo.LocationFloat = new DevExpress.Utils.PointFloat(606.7088F, 10.00001F);
            this.pbxCompanyLogo.Name = "pbxCompanyLogo";
            this.pbxCompanyLogo.SizeF = new System.Drawing.SizeF(177.2911F, 37.58167F);
            this.pbxCompanyLogo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // IncidentReportPage3
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageFooter,
            this.PageHeader});
            this.Margins = new System.Drawing.Printing.Margins(21, 22, 0, 0);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "18.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTableInvestigatorSignaturetxt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableImpactOFIncidentForQualityNonImpacting1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableImpactofincidentonpreviouslblForQNImpact)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}

		#endregion

		private DevExpress.XtraReports.UI.DetailBand Detail;
		private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
		private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRPictureBox pbxCompanyLogo;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrlbQNonImpactInvestandConclusion;
        private DevExpress.XtraReports.UI.XRTable xrTableImpactofincidentonpreviouslblForQNImpact;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTable xrTableImpactOFIncidentForQualityNonImpacting1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTable xrTableInvestigatorSignaturetxt;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRLabel xrlblInvestName1;
        private DevExpress.XtraReports.UI.XRTableCell xrInvestCreat;
        private DevExpress.XtraReports.UI.XRLabel xrlblInvestDesigName1;
        private DevExpress.XtraReports.UI.XRLabel xrInvestCreatedDate1;
    }
}
