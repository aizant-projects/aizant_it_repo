﻿namespace AizantIT_PharmaApp.Areas.QMS.XtraReports.Incident
{
    partial class IncidentReportPage4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrHeadQACreatedDateActionPlan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrQACreatedDateActionPlan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrHodCreatedDateActionPlan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLblHODDesigActionPlan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLblQADesigActionPlan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLblHeadQADesigActionPlan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrtblPlanningOfCAPAReport4NA = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLblHeadQANameActionPlan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrHeadQACreatedDateActionPlan11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLblHODNameActionPlan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrHodCreatedDateActionPlan11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLblQANameActionPlan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrQACreatedDateActionPlan11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrlblEvaluationandDispostionReport4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrlblAnyTrainingRequired = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrtblPlanningOfCAPAReport4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrlblIsImmediateCorrectionReport4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbxCompanyLogo = new DevExpress.XtraReports.UI.XRPictureBox();
            this.unitOfWork1 = new DevExpress.Xpo.UnitOfWork(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrtblPlanningOfCAPAReport4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unitOfWork1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrHeadQACreatedDateActionPlan,
            this.xrQACreatedDateActionPlan,
            this.xrHodCreatedDateActionPlan,
            this.xrLblHODDesigActionPlan,
            this.xrLblQADesigActionPlan,
            this.xrLblHeadQADesigActionPlan,
            this.xrTable10,
            this.xrTable9,
            this.xrTable8,
            this.xrTable7,
            this.xrTable6,
            this.xrTable5,
            this.xrTable2,
            this.xrtblPlanningOfCAPAReport4,
            this.xrTable4,
            this.xrTable3});
            this.Detail.HeightF = 843.7503F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrHeadQACreatedDateActionPlan
            // 
            this.xrHeadQACreatedDateActionPlan.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrHeadQACreatedDateActionPlan.LocationFloat = new DevExpress.Utils.PointFloat(590.8981F, 795.4169F);
            this.xrHeadQACreatedDateActionPlan.Multiline = true;
            this.xrHeadQACreatedDateActionPlan.Name = "xrHeadQACreatedDateActionPlan";
            this.xrHeadQACreatedDateActionPlan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrHeadQACreatedDateActionPlan.SizeF = new System.Drawing.SizeF(191.1021F, 27.91675F);
            this.xrHeadQACreatedDateActionPlan.StylePriority.UseFont = false;
            this.xrHeadQACreatedDateActionPlan.Text = "xrHeadQACreatedDateActionPlan";
            // 
            // xrQACreatedDateActionPlan
            // 
            this.xrQACreatedDateActionPlan.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrQACreatedDateActionPlan.LocationFloat = new DevExpress.Utils.PointFloat(590.8979F, 683.3332F);
            this.xrQACreatedDateActionPlan.Multiline = true;
            this.xrQACreatedDateActionPlan.Name = "xrQACreatedDateActionPlan";
            this.xrQACreatedDateActionPlan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrQACreatedDateActionPlan.SizeF = new System.Drawing.SizeF(191.1022F, 33.12494F);
            this.xrQACreatedDateActionPlan.StylePriority.UseFont = false;
            this.xrQACreatedDateActionPlan.Text = "xrQACreatedDateActionPlan";
            // 
            // xrHodCreatedDateActionPlan
            // 
            this.xrHodCreatedDateActionPlan.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrHodCreatedDateActionPlan.LocationFloat = new DevExpress.Utils.PointFloat(590.8979F, 385.4167F);
            this.xrHodCreatedDateActionPlan.Multiline = true;
            this.xrHodCreatedDateActionPlan.Name = "xrHodCreatedDateActionPlan";
            this.xrHodCreatedDateActionPlan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrHodCreatedDateActionPlan.SizeF = new System.Drawing.SizeF(191.1022F, 26.87506F);
            this.xrHodCreatedDateActionPlan.StylePriority.UseFont = false;
            this.xrHodCreatedDateActionPlan.StylePriority.UseTextAlignment = false;
            this.xrHodCreatedDateActionPlan.Text = "xrHodCreatedDateActionPlan";
            this.xrHodCreatedDateActionPlan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrLblHODDesigActionPlan
            // 
            this.xrLblHODDesigActionPlan.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLblHODDesigActionPlan.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLblHODDesigActionPlan.ForeColor = System.Drawing.Color.Blue;
            this.xrLblHODDesigActionPlan.LocationFloat = new DevExpress.Utils.PointFloat(161.1606F, 385.4167F);
            this.xrLblHODDesigActionPlan.Multiline = true;
            this.xrLblHODDesigActionPlan.Name = "xrLblHODDesigActionPlan";
            this.xrLblHODDesigActionPlan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblHODDesigActionPlan.SizeF = new System.Drawing.SizeF(429.7372F, 26.87506F);
            this.xrLblHODDesigActionPlan.StylePriority.UseBorders = false;
            this.xrLblHODDesigActionPlan.StylePriority.UseFont = false;
            this.xrLblHODDesigActionPlan.StylePriority.UseForeColor = false;
            // 
            // xrLblQADesigActionPlan
            // 
            this.xrLblQADesigActionPlan.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLblQADesigActionPlan.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLblQADesigActionPlan.ForeColor = System.Drawing.Color.Blue;
            this.xrLblQADesigActionPlan.LocationFloat = new DevExpress.Utils.PointFloat(161.1607F, 683.3332F);
            this.xrLblQADesigActionPlan.Multiline = true;
            this.xrLblQADesigActionPlan.Name = "xrLblQADesigActionPlan";
            this.xrLblQADesigActionPlan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblQADesigActionPlan.SizeF = new System.Drawing.SizeF(429.7372F, 33.12494F);
            this.xrLblQADesigActionPlan.StylePriority.UseBorders = false;
            this.xrLblQADesigActionPlan.StylePriority.UseFont = false;
            this.xrLblQADesigActionPlan.StylePriority.UseForeColor = false;
            // 
            // xrLblHeadQADesigActionPlan
            // 
            this.xrLblHeadQADesigActionPlan.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLblHeadQADesigActionPlan.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLblHeadQADesigActionPlan.ForeColor = System.Drawing.Color.Blue;
            this.xrLblHeadQADesigActionPlan.LocationFloat = new DevExpress.Utils.PointFloat(161.1606F, 795.4168F);
            this.xrLblHeadQADesigActionPlan.Multiline = true;
            this.xrLblHeadQADesigActionPlan.Name = "xrLblHeadQADesigActionPlan";
            this.xrLblHeadQADesigActionPlan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblHeadQADesigActionPlan.SizeF = new System.Drawing.SizeF(429.7373F, 27.91669F);
            this.xrLblHeadQADesigActionPlan.StylePriority.UseBorders = false;
            this.xrLblHeadQADesigActionPlan.StylePriority.UseFont = false;
            this.xrLblHeadQADesigActionPlan.StylePriority.UseForeColor = false;
            this.xrLblHeadQADesigActionPlan.StylePriority.UseTextAlignment = false;
            this.xrLblHeadQADesigActionPlan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable10
            // 
            this.xrTable10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable10.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(0F, 187.5001F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable10.SizeF = new System.Drawing.SizeF(782F, 35.41667F);
            this.xrTable10.StylePriority.UseBorders = false;
            this.xrTable10.StylePriority.UseFont = false;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.xrtblPlanningOfCAPAReport4NA});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell2.Multiline = true;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "CAPA";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell2.Weight = 1.5D;
            // 
            // xrtblPlanningOfCAPAReport4NA
            // 
            this.xrtblPlanningOfCAPAReport4NA.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrtblPlanningOfCAPAReport4NA.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrtblPlanningOfCAPAReport4NA.Multiline = true;
            this.xrtblPlanningOfCAPAReport4NA.Name = "xrtblPlanningOfCAPAReport4NA";
            this.xrtblPlanningOfCAPAReport4NA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrtblPlanningOfCAPAReport4NA.StylePriority.UseBorders = false;
            this.xrtblPlanningOfCAPAReport4NA.StylePriority.UseFont = false;
            this.xrtblPlanningOfCAPAReport4NA.StylePriority.UsePadding = false;
            this.xrtblPlanningOfCAPAReport4NA.StylePriority.UseTextAlignment = false;
            this.xrtblPlanningOfCAPAReport4NA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrtblPlanningOfCAPAReport4NA.Weight = 1.5D;
            // 
            // xrTable9
            // 
            this.xrTable9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable9.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 749.5835F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable9.SizeF = new System.Drawing.SizeF(782F, 45.83337F);
            this.xrTable9.StylePriority.UseBorders = false;
            this.xrTable9.StylePriority.UseFont = false;
            this.xrTable9.StylePriority.UseTextAlignment = false;
            this.xrTable9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.xrTableCell10,
            this.xrHeadQACreatedDateActionPlan11});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell8.Multiline = true;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.Text = "Approved by (E-Sign)";
            this.xrTableCell8.Weight = 0.61826353772162423D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLblHeadQANameActionPlan});
            this.xrTableCell10.Multiline = true;
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.Weight = 1.6486080982556091D;
            // 
            // xrLblHeadQANameActionPlan
            // 
            this.xrLblHeadQANameActionPlan.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLblHeadQANameActionPlan.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLblHeadQANameActionPlan.ForeColor = System.Drawing.Color.Blue;
            this.xrLblHeadQANameActionPlan.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 10F);
            this.xrLblHeadQANameActionPlan.Multiline = true;
            this.xrLblHeadQANameActionPlan.Name = "xrLblHeadQANameActionPlan";
            this.xrLblHeadQANameActionPlan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLblHeadQANameActionPlan.SizeF = new System.Drawing.SizeF(409.7372F, 27.91675F);
            this.xrLblHeadQANameActionPlan.StylePriority.UseBorders = false;
            this.xrLblHeadQANameActionPlan.StylePriority.UseFont = false;
            this.xrLblHeadQANameActionPlan.StylePriority.UseForeColor = false;
            // 
            // xrHeadQACreatedDateActionPlan11
            // 
            this.xrHeadQACreatedDateActionPlan11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrHeadQACreatedDateActionPlan11.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrHeadQACreatedDateActionPlan11.Multiline = true;
            this.xrHeadQACreatedDateActionPlan11.Name = "xrHeadQACreatedDateActionPlan11";
            this.xrHeadQACreatedDateActionPlan11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrHeadQACreatedDateActionPlan11.StylePriority.UseBorders = false;
            this.xrHeadQACreatedDateActionPlan11.StylePriority.UseFont = false;
            this.xrHeadQACreatedDateActionPlan11.Text = "Approved Date";
            this.xrHeadQACreatedDateActionPlan11.Weight = 0.73312836402276671D;
            // 
            // xrTable8
            // 
            this.xrTable8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable8.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 342.7083F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable8.SizeF = new System.Drawing.SizeF(782.0001F, 42.70834F);
            this.xrTable8.StylePriority.UseBorders = false;
            this.xrTable8.StylePriority.UseFont = false;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell9,
            this.xrHodCreatedDateActionPlan11});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell4.Multiline = true;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "Approved by (E-Sign)";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell4.Weight = 0.61826348279557819D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLblHODNameActionPlan});
            this.xrTableCell9.Multiline = true;
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell9.Weight = 1.648608076758505D;
            // 
            // xrLblHODNameActionPlan
            // 
            this.xrLblHODNameActionPlan.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLblHODNameActionPlan.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLblHODNameActionPlan.ForeColor = System.Drawing.Color.Blue;
            this.xrLblHODNameActionPlan.LocationFloat = new DevExpress.Utils.PointFloat(10.00005F, 10F);
            this.xrLblHODNameActionPlan.Multiline = true;
            this.xrLblHODNameActionPlan.Name = "xrLblHODNameActionPlan";
            this.xrLblHODNameActionPlan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLblHODNameActionPlan.SizeF = new System.Drawing.SizeF(409.7372F, 25.83337F);
            this.xrLblHODNameActionPlan.StylePriority.UseBorders = false;
            this.xrLblHODNameActionPlan.StylePriority.UseFont = false;
            this.xrLblHODNameActionPlan.StylePriority.UseForeColor = false;
            // 
            // xrHodCreatedDateActionPlan11
            // 
            this.xrHodCreatedDateActionPlan11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrHodCreatedDateActionPlan11.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrHodCreatedDateActionPlan11.Multiline = true;
            this.xrHodCreatedDateActionPlan11.Name = "xrHodCreatedDateActionPlan11";
            this.xrHodCreatedDateActionPlan11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrHodCreatedDateActionPlan11.StylePriority.UseBorders = false;
            this.xrHodCreatedDateActionPlan11.StylePriority.UseFont = false;
            this.xrHodCreatedDateActionPlan11.StylePriority.UseTextAlignment = false;
            this.xrHodCreatedDateActionPlan11.Text = "Approved Date";
            this.xrHodCreatedDateActionPlan11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrHodCreatedDateActionPlan11.Weight = 0.73312844044591685D;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable7.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 637.5F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable7.SizeF = new System.Drawing.SizeF(782.0001F, 45.83325F);
            this.xrTable7.StylePriority.UseBorders = false;
            this.xrTable7.StylePriority.UseFont = false;
            this.xrTable7.StylePriority.UseTextAlignment = false;
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.xrTableCell7,
            this.xrQACreatedDateActionPlan11});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell5.Multiline = true;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.Text = "Approved by  (E-Sign)";
            this.xrTableCell5.Weight = 0.61826347774475732D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLblQANameActionPlan});
            this.xrTableCell7.Multiline = true;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.Weight = 1.6486081641112635D;
            // 
            // xrLblQANameActionPlan
            // 
            this.xrLblQANameActionPlan.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLblQANameActionPlan.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLblQANameActionPlan.ForeColor = System.Drawing.Color.Blue;
            this.xrLblQANameActionPlan.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 10F);
            this.xrLblQANameActionPlan.Multiline = true;
            this.xrLblQANameActionPlan.Name = "xrLblQANameActionPlan";
            this.xrLblQANameActionPlan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblQANameActionPlan.SizeF = new System.Drawing.SizeF(409.7372F, 26.87494F);
            this.xrLblQANameActionPlan.StylePriority.UseBorders = false;
            this.xrLblQANameActionPlan.StylePriority.UseFont = false;
            this.xrLblQANameActionPlan.StylePriority.UseForeColor = false;
            // 
            // xrQACreatedDateActionPlan11
            // 
            this.xrQACreatedDateActionPlan11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrQACreatedDateActionPlan11.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrQACreatedDateActionPlan11.Multiline = true;
            this.xrQACreatedDateActionPlan11.Name = "xrQACreatedDateActionPlan11";
            this.xrQACreatedDateActionPlan11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrQACreatedDateActionPlan11.StylePriority.UseBorders = false;
            this.xrQACreatedDateActionPlan11.StylePriority.UseFont = false;
            this.xrQACreatedDateActionPlan11.Text = "Approved Date";
            this.xrQACreatedDateActionPlan11.Weight = 0.73312835814397959D;
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 491.6667F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable6.SizeF = new System.Drawing.SizeF(782.0001F, 145.8333F);
            this.xrTable6.StylePriority.UseBorders = false;
            this.xrTable6.StylePriority.UseFont = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrlblEvaluationandDispostionReport4});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrlblEvaluationandDispostionReport4
            // 
            this.xrlblEvaluationandDispostionReport4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblEvaluationandDispostionReport4.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrlblEvaluationandDispostionReport4.Multiline = true;
            this.xrlblEvaluationandDispostionReport4.Name = "xrlblEvaluationandDispostionReport4";
            this.xrlblEvaluationandDispostionReport4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblEvaluationandDispostionReport4.StylePriority.UseBorders = false;
            this.xrlblEvaluationandDispostionReport4.StylePriority.UseFont = false;
            this.xrlblEvaluationandDispostionReport4.Weight = 1D;
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 447.9167F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable5.SizeF = new System.Drawing.SizeF(782F, 43.75003F);
            this.xrTable5.StylePriority.UseBorders = false;
            this.xrTable5.StylePriority.UseFont = false;
            this.xrTable5.StylePriority.UseTextAlignment = false;
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2,
            this.xrLabel1});
            this.xrTableCell3.Multiline = true;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell3.Weight = 3D;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(102F, 10.75003F);
            this.xrLabel2.Multiline = true;
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(238.4166F, 23F);
            this.xrLabel2.StylePriority.UseBorders = false;
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "Evaluation and disposition:";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(2F, 9.999969F);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "(Section V)";
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 293.75F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable2.SizeF = new System.Drawing.SizeF(782.0001F, 48.95831F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrlblAnyTrainingRequired});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrTableCell1.Multiline = true;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "Is Any Training Required:";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell1.Weight = 2.9999998560114181D;
            // 
            // xrlblAnyTrainingRequired
            // 
            this.xrlblAnyTrainingRequired.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblAnyTrainingRequired.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrlblAnyTrainingRequired.Multiline = true;
            this.xrlblAnyTrainingRequired.Name = "xrlblAnyTrainingRequired";
            this.xrlblAnyTrainingRequired.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblAnyTrainingRequired.StylePriority.UseBorders = false;
            this.xrlblAnyTrainingRequired.StylePriority.UseFont = false;
            this.xrlblAnyTrainingRequired.StylePriority.UsePadding = false;
            this.xrlblAnyTrainingRequired.StylePriority.UseTextAlignment = false;
            this.xrlblAnyTrainingRequired.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrlblAnyTrainingRequired.Weight = 3.0000001439885819D;
            // 
            // xrtblPlanningOfCAPAReport4
            // 
            this.xrtblPlanningOfCAPAReport4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrtblPlanningOfCAPAReport4.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrtblPlanningOfCAPAReport4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 222.9167F);
            this.xrtblPlanningOfCAPAReport4.Name = "xrtblPlanningOfCAPAReport4";
            this.xrtblPlanningOfCAPAReport4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12});
            this.xrtblPlanningOfCAPAReport4.SizeF = new System.Drawing.SizeF(782F, 38.54167F);
            this.xrtblPlanningOfCAPAReport4.StylePriority.UseBorders = false;
            this.xrtblPlanningOfCAPAReport4.StylePriority.UseFont = false;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell12,
            this.xrTableCell15,
            this.xrTableCell22,
            this.xrTableCell24,
            this.xrTableCell25,
            this.xrTableCell13,
            this.xrTableCell26});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.StylePriority.UseBorders = false;
            this.xrTableRow12.Weight = 1D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell12.Multiline = true;
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.StylePriority.UsePadding = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "CAPA Number";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell12.Weight = 0.55511612806917965D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell15.Multiline = true;
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "Type of Action";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell15.Weight = 0.68731132154202879D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell22.Multiline = true;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.StylePriority.UseTextAlignment = false;
            this.xrTableCell22.Text = "Department";
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell22.Weight = 0.515591994833065D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell24.Multiline = true;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell24.StylePriority.UseFont = false;
            this.xrTableCell24.StylePriority.UseTextAlignment = false;
            this.xrTableCell24.Text = "Responsible Person";
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell24.Weight = 0.72550154245792253D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell25.Multiline = true;
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell25.StylePriority.UseFont = false;
            this.xrTableCell25.StylePriority.UsePadding = false;
            this.xrTableCell25.StylePriority.UseTextAlignment = false;
            this.xrTableCell25.Text = "Target Date";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell25.Weight = 0.73961752325106755D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell13.Multiline = true;
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell13.StylePriority.UseFont = false;
            this.xrTableCell13.StylePriority.UsePadding = false;
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "Description";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell13.Weight = 0.73961752325106755D;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell26.Multiline = true;
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.StylePriority.UsePadding = false;
            this.xrTableCell26.StylePriority.UseTextAlignment = false;
            this.xrTableCell26.Text = "Employee Status";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell26.Weight = 0.54280935690162413D;
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 51.66667F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11});
            this.xrTable4.SizeF = new System.Drawing.SizeF(782F, 114.5833F);
            this.xrTable4.StylePriority.UseBorders = false;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrlblIsImmediateCorrectionReport4});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1D;
            // 
            // xrlblIsImmediateCorrectionReport4
            // 
            this.xrlblIsImmediateCorrectionReport4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblIsImmediateCorrectionReport4.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrlblIsImmediateCorrectionReport4.Multiline = true;
            this.xrlblIsImmediateCorrectionReport4.Name = "xrlblIsImmediateCorrectionReport4";
            this.xrlblIsImmediateCorrectionReport4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblIsImmediateCorrectionReport4.StylePriority.UseBorders = false;
            this.xrlblIsImmediateCorrectionReport4.StylePriority.UseFont = false;
            this.xrlblIsImmediateCorrectionReport4.Weight = 3D;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 10.00001F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable3.SizeF = new System.Drawing.SizeF(782.0001F, 41.66668F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseFont = false;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell11});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel7,
            this.xrLabel6});
            this.xrTableCell11.Multiline = true;
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell11.Weight = 3D;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(100.8334F, 8.666674F);
            this.xrLabel7.Multiline = true;
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(275F, 23F);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.Text = "Immediate correction done:";
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(2F, 8.666674F);
            this.xrLabel6.Multiline = true;
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(98.83338F, 23F);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.Text = "(Section IV)";
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.HeightF = 42.70833F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(682.0002F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.TextFormatString = "page {0} of {1}";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.pbxCompanyLogo});
            this.PageHeader.HeightF = 109.375F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrTable1
            // 
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 66.24999F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13});
            this.xrTable1.SizeF = new System.Drawing.SizeF(782F, 34.375F);
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell6.Multiline = true;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "INCIDENT REPORT";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell6.Weight = 1D;
            // 
            // pbxCompanyLogo
            // 
            this.pbxCompanyLogo.LocationFloat = new DevExpress.Utils.PointFloat(604.9727F, 10.00001F);
            this.pbxCompanyLogo.Name = "pbxCompanyLogo";
            this.pbxCompanyLogo.SizeF = new System.Drawing.SizeF(177.0273F, 42.79F);
            this.pbxCompanyLogo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // unitOfWork1
            // 
            this.unitOfWork1.IsObjectModifiedOnNonPersistentPropertyChange = null;
            this.unitOfWork1.TrackPropertiesModifications = false;
            // 
            // IncidentReportPage4
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageFooter,
            this.PageHeader});
            this.Margins = new System.Drawing.Printing.Margins(22, 23, 0, 0);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "18.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrtblPlanningOfCAPAReport4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unitOfWork1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRPictureBox pbxCompanyLogo;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrlblIsImmediateCorrectionReport4;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRTable xrtblPlanningOfCAPAReport4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrlblAnyTrainingRequired;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrQACreatedDateActionPlan11;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrlblEvaluationandDispostionReport4;
        private DevExpress.XtraReports.UI.XRLabel xrLblQANameActionPlan;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRLabel xrLblHODNameActionPlan;
        private DevExpress.XtraReports.UI.XRTableCell xrHodCreatedDateActionPlan11;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrHeadQACreatedDateActionPlan11;
        private DevExpress.XtraReports.UI.XRLabel xrLblHeadQANameActionPlan;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrtblPlanningOfCAPAReport4NA;
        private DevExpress.XtraReports.UI.XRLabel xrLblQADesigActionPlan;
        private DevExpress.XtraReports.UI.XRLabel xrLblHeadQADesigActionPlan;
        private DevExpress.XtraReports.UI.XRLabel xrLblHODDesigActionPlan;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRLabel xrHodCreatedDateActionPlan;
        private DevExpress.XtraReports.UI.XRLabel xrQACreatedDateActionPlan;
        private DevExpress.XtraReports.UI.XRLabel xrHeadQACreatedDateActionPlan;
        private DevExpress.Xpo.UnitOfWork unitOfWork1;
    }
}
