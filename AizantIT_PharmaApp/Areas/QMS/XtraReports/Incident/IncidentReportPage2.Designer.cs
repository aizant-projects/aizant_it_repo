﻿namespace AizantIT_PharmaApp.Areas.QMS.XtraReports.Incident
{
    partial class IncidentReportPage2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrInvestCreatedDate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableSectionIVText = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrlblIsImmediateCorrectionReportNew = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableSectionIVLabel = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblInvestDesigName = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableHasIncidentAffectDocs = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableQualityImpactingLabel = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableInvestigatorSignaturetxt = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrlblInvestName = new DevExpress.XtraReports.UI.XRLabel();
            this.xrInvestCreated = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableConclusionText = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrlblConclusion_Desc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableConclusionlbl = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrtblOtherDepHodReview = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableOtherDeptHodReviewlbl = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrtblOtherDepHodReviewNA = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrtblListOfSupportingDocs = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableListofSupprtDocs = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrtblListOfSupportingDocsNA = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrtblReview = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableReviewofWorkorderlbl = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableHasIncidentAffectdocText = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrcbxHasIncidentAffect = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableHasIncidentAffectedDocs = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrtblRunningCondition = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRunningConditionlbl = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrtblInvestigationAffectedOn = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableIncidentAffectedonlbl = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrtblInvestigationImpactOfIncidient = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableHastheincidentaffectlbl = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableImpactofincidentonpreviouslbl = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrtblWhoArePeopleInvolvedReport2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrtblWhoArePeopleInvolved = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableWhoarethepplinvolvlbl = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrtblWhoArePeopleInvolvedReportNA = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableWhereHappenedText = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrlblWhereHappened_Desc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableWhereHappenedlbl = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableWhenhappenedText = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrlblWhenHappened_Desc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableWhenHappenedlbl = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableWhathappenedText = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrlblWhatHappened_Desc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableWhatHappenedlbl = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableInvestigationLabel = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbxCompanyLogo = new DevExpress.XtraReports.UI.XRPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableSectionIVText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableSectionIVLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHasIncidentAffectDocs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableQualityImpactingLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableInvestigatorSignaturetxt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableConclusionText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableConclusionlbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrtblOtherDepHodReview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableOtherDeptHodReviewlbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrtblListOfSupportingDocs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableListofSupprtDocs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrtblReview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableReviewofWorkorderlbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHasIncidentAffectdocText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHasIncidentAffectedDocs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrtblRunningCondition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableRunningConditionlbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrtblInvestigationAffectedOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableIncidentAffectedonlbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrtblInvestigationImpactOfIncidient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHastheincidentaffectlbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableImpactofincidentonpreviouslbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrtblWhoArePeopleInvolvedReport2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableWhoarethepplinvolvlbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableWhereHappenedText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableWhereHappenedlbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableWhenhappenedText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableWhenHappenedlbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableWhathappenedText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableWhatHappenedlbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableInvestigationLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrInvestCreatedDate,
            this.xrTableSectionIVText,
            this.xrTableSectionIVLabel,
            this.xrlblInvestDesigName,
            this.xrTableHasIncidentAffectDocs,
            this.xrTableQualityImpactingLabel,
            this.xrTableInvestigatorSignaturetxt,
            this.xrTableConclusionText,
            this.xrTableConclusionlbl,
            this.xrtblOtherDepHodReview,
            this.xrTableOtherDeptHodReviewlbl,
            this.xrtblListOfSupportingDocs,
            this.xrTableListofSupprtDocs,
            this.xrtblReview,
            this.xrTableReviewofWorkorderlbl,
            this.xrTableHasIncidentAffectdocText,
            this.xrTableHasIncidentAffectedDocs,
            this.xrtblRunningCondition,
            this.xrTableRunningConditionlbl,
            this.xrtblInvestigationAffectedOn,
            this.xrTableIncidentAffectedonlbl,
            this.xrtblInvestigationImpactOfIncidient,
            this.xrTableHastheincidentaffectlbl,
            this.xrTableImpactofincidentonpreviouslbl,
            this.xrtblWhoArePeopleInvolvedReport2,
            this.xrTableWhoarethepplinvolvlbl,
            this.xrTableWhereHappenedText,
            this.xrTableWhereHappenedlbl,
            this.xrTableWhenhappenedText,
            this.xrTableWhenHappenedlbl,
            this.xrTableWhathappenedText,
            this.xrTableWhatHappenedlbl,
            this.xrTableInvestigationLabel});
            this.Detail.HeightF = 1596.875F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrInvestCreatedDate
            // 
            this.xrInvestCreatedDate.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrInvestCreatedDate.LocationFloat = new DevExpress.Utils.PointFloat(592.4927F, 1330.208F);
            this.xrInvestCreatedDate.Multiline = true;
            this.xrInvestCreatedDate.Name = "xrInvestCreatedDate";
            this.xrInvestCreatedDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrInvestCreatedDate.SizeF = new System.Drawing.SizeF(194.507F, 31.04187F);
            this.xrInvestCreatedDate.StylePriority.UseFont = false;
            this.xrInvestCreatedDate.StylePriority.UseTextAlignment = false;
            this.xrInvestCreatedDate.Text = "xrInvestCreatedDate";
            this.xrInvestCreatedDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrTableSectionIVText
            // 
            this.xrTableSectionIVText.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableSectionIVText.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1427.542F);
            this.xrTableSectionIVText.Name = "xrTableSectionIVText";
            this.xrTableSectionIVText.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTableSectionIVText.SizeF = new System.Drawing.SizeF(786.9996F, 114.5833F);
            this.xrTableSectionIVText.StylePriority.UseBorders = false;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrlblIsImmediateCorrectionReportNew});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrlblIsImmediateCorrectionReportNew
            // 
            this.xrlblIsImmediateCorrectionReportNew.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblIsImmediateCorrectionReportNew.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrlblIsImmediateCorrectionReportNew.Multiline = true;
            this.xrlblIsImmediateCorrectionReportNew.Name = "xrlblIsImmediateCorrectionReportNew";
            this.xrlblIsImmediateCorrectionReportNew.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblIsImmediateCorrectionReportNew.StylePriority.UseBorders = false;
            this.xrlblIsImmediateCorrectionReportNew.StylePriority.UseFont = false;
            this.xrlblIsImmediateCorrectionReportNew.Weight = 3D;
            // 
            // xrTableSectionIVLabel
            // 
            this.xrTableSectionIVLabel.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableSectionIVLabel.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableSectionIVLabel.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1385.875F);
            this.xrTableSectionIVLabel.Name = "xrTableSectionIVLabel";
            this.xrTableSectionIVLabel.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTableSectionIVLabel.SizeF = new System.Drawing.SizeF(786.9996F, 41.66663F);
            this.xrTableSectionIVLabel.StylePriority.UseBorders = false;
            this.xrTableSectionIVLabel.StylePriority.UseFont = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel7,
            this.xrLabel6});
            this.xrTableCell7.Multiline = true;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell7.Weight = 3D;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(100.8334F, 8.666674F);
            this.xrLabel7.Multiline = true;
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(275F, 23F);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.Text = "Immediate correction done:";
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(2F, 8.666674F);
            this.xrLabel6.Multiline = true;
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(98.83338F, 23F);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.Text = "(Section IV)";
            // 
            // xrlblInvestDesigName
            // 
            this.xrlblInvestDesigName.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlblInvestDesigName.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrlblInvestDesigName.ForeColor = System.Drawing.Color.Blue;
            this.xrlblInvestDesigName.LocationFloat = new DevExpress.Utils.PointFloat(164.428F, 1330.208F);
            this.xrlblInvestDesigName.Multiline = true;
            this.xrlblInvestDesigName.Name = "xrlblInvestDesigName";
            this.xrlblInvestDesigName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblInvestDesigName.SizeF = new System.Drawing.SizeF(428.0647F, 31.04187F);
            this.xrlblInvestDesigName.StylePriority.UseBorders = false;
            this.xrlblInvestDesigName.StylePriority.UseFont = false;
            this.xrlblInvestDesigName.StylePriority.UseForeColor = false;
            this.xrlblInvestDesigName.StylePriority.UseTextAlignment = false;
            this.xrlblInvestDesigName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrTableHasIncidentAffectDocs
            // 
            this.xrTableHasIncidentAffectDocs.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableHasIncidentAffectDocs.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrTableHasIncidentAffectDocs.LocationFloat = new DevExpress.Utils.PointFloat(0F, 828.1249F);
            this.xrTableHasIncidentAffectDocs.Name = "xrTableHasIncidentAffectDocs";
            this.xrTableHasIncidentAffectDocs.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTableHasIncidentAffectDocs.SizeF = new System.Drawing.SizeF(787F, 35.41669F);
            this.xrTableHasIncidentAffectDocs.StylePriority.UseBorders = false;
            this.xrTableHasIncidentAffectDocs.StylePriority.UseFont = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.xrTableCell3,
            this.xrTableCell4,
            this.xrTableCell6,
            this.xrTableCell8});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell2.Multiline = true;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "Department";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell2.Weight = 0.71079779962841683D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell3.Multiline = true;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "Document Name";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell3.Weight = 0.77028364191971055D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell4.Multiline = true;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "Doc Ref No";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell4.Weight = 0.66010818869363885D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell6.Multiline = true;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UsePadding = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "Page Number";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell6.Weight = 0.48325433718850841D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell8.Multiline = true;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UsePadding = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = "Comments";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell8.Weight = 1.3718940066575265D;
            // 
            // xrTableQualityImpactingLabel
            // 
            this.xrTableQualityImpactingLabel.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableQualityImpactingLabel.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrTableQualityImpactingLabel.LocationFloat = new DevExpress.Utils.PointFloat(0F, 10.00001F);
            this.xrTableQualityImpactingLabel.Name = "xrTableQualityImpactingLabel";
            this.xrTableQualityImpactingLabel.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTableQualityImpactingLabel.SizeF = new System.Drawing.SizeF(787F, 32.29166F);
            this.xrTableQualityImpactingLabel.StylePriority.UseBorders = false;
            this.xrTableQualityImpactingLabel.StylePriority.UseFont = false;
            this.xrTableQualityImpactingLabel.StylePriority.UseTextAlignment = false;
            this.xrTableQualityImpactingLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.StylePriority.UseFont = false;
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell1.Multiline = true;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.Text = "Quality Impacting Incident Investigation and Conclusion Report";
            this.xrTableCell1.Weight = 3D;
            // 
            // xrTableInvestigatorSignaturetxt
            // 
            this.xrTableInvestigatorSignaturetxt.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1288.541F);
            this.xrTableInvestigatorSignaturetxt.Name = "xrTableInvestigatorSignaturetxt";
            this.xrTableInvestigatorSignaturetxt.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow37});
            this.xrTableInvestigatorSignaturetxt.SizeF = new System.Drawing.SizeF(787F, 41.66675F);
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55,
            this.xrTableCell10,
            this.xrInvestCreated});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.StylePriority.UseBorders = false;
            this.xrTableRow37.Weight = 1D;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Multiline = true;
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell55.StylePriority.UseBorders = false;
            this.xrTableCell55.StylePriority.UseFont = false;
            this.xrTableCell55.StylePriority.UseTextAlignment = false;
            this.xrTableCell55.Text = "Investigator  (E-Sign): \t";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell55.Weight = 0.60787003717214672D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrlblInvestName});
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell10.Multiline = true;
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.StylePriority.UsePadding = false;
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell10.Weight = 1.6506790914566771D;
            // 
            // xrlblInvestName
            // 
            this.xrlblInvestName.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlblInvestName.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrlblInvestName.ForeColor = System.Drawing.Color.Blue;
            this.xrlblInvestName.LocationFloat = new DevExpress.Utils.PointFloat(10F, 6.875F);
            this.xrlblInvestName.Multiline = true;
            this.xrlblInvestName.Name = "xrlblInvestName";
            this.xrlblInvestName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrlblInvestName.SizeF = new System.Drawing.SizeF(411.9061F, 26.87512F);
            this.xrlblInvestName.StylePriority.UseBorders = false;
            this.xrlblInvestName.StylePriority.UseFont = false;
            this.xrlblInvestName.StylePriority.UseForeColor = false;
            // 
            // xrInvestCreated
            // 
            this.xrInvestCreated.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrInvestCreated.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrInvestCreated.Multiline = true;
            this.xrInvestCreated.Name = "xrInvestCreated";
            this.xrInvestCreated.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrInvestCreated.StylePriority.UseBorders = false;
            this.xrInvestCreated.StylePriority.UseFont = false;
            this.xrInvestCreated.StylePriority.UseTextAlignment = false;
            this.xrInvestCreated.Text = "Investigation Date";
            this.xrInvestCreated.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrInvestCreated.Weight = 0.74145087137117716D;
            // 
            // xrTableConclusionText
            // 
            this.xrTableConclusionText.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableConclusionText.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1145.833F);
            this.xrTableConclusionText.Name = "xrTableConclusionText";
            this.xrTableConclusionText.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36});
            this.xrTableConclusionText.SizeF = new System.Drawing.SizeF(786.9996F, 119.7917F);
            this.xrTableConclusionText.StylePriority.UseBorders = false;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrlblConclusion_Desc});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 1D;
            // 
            // xrlblConclusion_Desc
            // 
            this.xrlblConclusion_Desc.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblConclusion_Desc.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrlblConclusion_Desc.Multiline = true;
            this.xrlblConclusion_Desc.Name = "xrlblConclusion_Desc";
            this.xrlblConclusion_Desc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblConclusion_Desc.StylePriority.UseBorders = false;
            this.xrlblConclusion_Desc.StylePriority.UseFont = false;
            this.xrlblConclusion_Desc.Weight = 3D;
            // 
            // xrTableConclusionlbl
            // 
            this.xrTableConclusionlbl.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableConclusionlbl.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableConclusionlbl.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1120.833F);
            this.xrTableConclusionlbl.Name = "xrTableConclusionlbl";
            this.xrTableConclusionlbl.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow35});
            this.xrTableConclusionlbl.SizeF = new System.Drawing.SizeF(786.9996F, 25F);
            this.xrTableConclusionlbl.StylePriority.UseBorders = false;
            this.xrTableConclusionlbl.StylePriority.UseFont = false;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell53});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 1D;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell53.Multiline = true;
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell53.StylePriority.UseFont = false;
            this.xrTableCell53.Text = "Conclusion";
            this.xrTableCell53.Weight = 3D;
            // 
            // xrtblOtherDepHodReview
            // 
            this.xrtblOtherDepHodReview.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrtblOtherDepHodReview.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrtblOtherDepHodReview.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1078.125F);
            this.xrtblOtherDepHodReview.Name = "xrtblOtherDepHodReview";
            this.xrtblOtherDepHodReview.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow34});
            this.xrtblOtherDepHodReview.SizeF = new System.Drawing.SizeF(786.9998F, 25F);
            this.xrtblOtherDepHodReview.StylePriority.UseBorders = false;
            this.xrtblOtherDepHodReview.StylePriority.UseFont = false;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell46,
            this.xrTableCell47,
            this.xrTableCell50,
            this.xrTableCell51,
            this.xrTableCell52});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 1D;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell46.Multiline = true;
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell46.StylePriority.UseFont = false;
            this.xrTableCell46.Text = "Department";
            this.xrTableCell46.Weight = 1.4583324806963987D;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell47.Multiline = true;
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell47.StylePriority.UseFont = false;
            this.xrTableCell47.Text = "Hod Name";
            this.xrTableCell47.Weight = 1.3108333680471191D;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell50.Multiline = true;
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell50.StylePriority.UseFont = false;
            this.xrTableCell50.StylePriority.UsePadding = false;
            this.xrTableCell50.Text = "Submitted Date";
            this.xrTableCell50.Weight = 1.2620835228112128D;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell51.Multiline = true;
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell51.StylePriority.UseFont = false;
            this.xrTableCell51.StylePriority.UsePadding = false;
            this.xrTableCell51.Text = "Status";
            this.xrTableCell51.Weight = 1.1250003106409769D;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell52.Multiline = true;
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell52.StylePriority.UseFont = false;
            this.xrTableCell52.StylePriority.UsePadding = false;
            this.xrTableCell52.Text = "Comments";
            this.xrTableCell52.Weight = 1.8237499168745601D;
            // 
            // xrTableOtherDeptHodReviewlbl
            // 
            this.xrTableOtherDeptHodReviewlbl.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1045.833F);
            this.xrTableOtherDeptHodReviewlbl.Name = "xrTableOtherDeptHodReviewlbl";
            this.xrTableOtherDeptHodReviewlbl.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow32});
            this.xrTableOtherDeptHodReviewlbl.SizeF = new System.Drawing.SizeF(787F, 32.29163F);
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.xrtblOtherDepHodReviewNA});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 1D;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell45.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell45.Multiline = true;
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell45.StylePriority.UseBorders = false;
            this.xrTableCell45.StylePriority.UseFont = false;
            this.xrTableCell45.Text = "Others Department Hod\'s Review\r\n";
            this.xrTableCell45.Weight = 1.5D;
            // 
            // xrtblOtherDepHodReviewNA
            // 
            this.xrtblOtherDepHodReviewNA.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrtblOtherDepHodReviewNA.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrtblOtherDepHodReviewNA.Multiline = true;
            this.xrtblOtherDepHodReviewNA.Name = "xrtblOtherDepHodReviewNA";
            this.xrtblOtherDepHodReviewNA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrtblOtherDepHodReviewNA.StylePriority.UseBorders = false;
            this.xrtblOtherDepHodReviewNA.StylePriority.UseFont = false;
            this.xrtblOtherDepHodReviewNA.StylePriority.UsePadding = false;
            this.xrtblOtherDepHodReviewNA.StylePriority.UseTextAlignment = false;
            this.xrtblOtherDepHodReviewNA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrtblOtherDepHodReviewNA.Weight = 1.5D;
            // 
            // xrtblListOfSupportingDocs
            // 
            this.xrtblListOfSupportingDocs.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrtblListOfSupportingDocs.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrtblListOfSupportingDocs.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1001.042F);
            this.xrtblListOfSupportingDocs.Name = "xrtblListOfSupportingDocs";
            this.xrtblListOfSupportingDocs.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow33});
            this.xrtblListOfSupportingDocs.SizeF = new System.Drawing.SizeF(786.9998F, 24.99994F);
            this.xrtblListOfSupportingDocs.StylePriority.UseBorders = false;
            this.xrtblListOfSupportingDocs.StylePriority.UseFont = false;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell48,
            this.xrTableCell49});
            this.xrTableRow33.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.StylePriority.UseBorders = false;
            this.xrTableRow33.StylePriority.UseFont = false;
            this.xrTableRow33.Weight = 1D;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell48.Multiline = true;
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell48.StylePriority.UseFont = false;
            this.xrTableCell48.Text = "Type of Document";
            this.xrTableCell48.Weight = 3.1562495899151948D;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell49.Multiline = true;
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell49.StylePriority.UseFont = false;
            this.xrTableCell49.Text = "Document Name";
            this.xrTableCell49.Weight = 3.8237507450318411D;
            // 
            // xrTableListofSupprtDocs
            // 
            this.xrTableListofSupprtDocs.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableListofSupprtDocs.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableListofSupprtDocs.LocationFloat = new DevExpress.Utils.PointFloat(0F, 965.6251F);
            this.xrTableListofSupprtDocs.Name = "xrTableListofSupprtDocs";
            this.xrTableListofSupprtDocs.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow31});
            this.xrTableListofSupprtDocs.SizeF = new System.Drawing.SizeF(787F, 35.41675F);
            this.xrTableListofSupprtDocs.StylePriority.UseBorders = false;
            this.xrTableListofSupprtDocs.StylePriority.UseFont = false;
            this.xrTableListofSupprtDocs.StylePriority.UseTextAlignment = false;
            this.xrTableListofSupprtDocs.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell44,
            this.xrtblListOfSupportingDocsNA});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 1D;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell44.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell44.Multiline = true;
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell44.StylePriority.UseBorders = false;
            this.xrTableCell44.StylePriority.UseFont = false;
            this.xrTableCell44.Text = "List of Supporting Documents";
            this.xrTableCell44.Weight = 1.5D;
            // 
            // xrtblListOfSupportingDocsNA
            // 
            this.xrtblListOfSupportingDocsNA.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrtblListOfSupportingDocsNA.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrtblListOfSupportingDocsNA.Multiline = true;
            this.xrtblListOfSupportingDocsNA.Name = "xrtblListOfSupportingDocsNA";
            this.xrtblListOfSupportingDocsNA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrtblListOfSupportingDocsNA.StylePriority.UseBorders = false;
            this.xrtblListOfSupportingDocsNA.StylePriority.UseFont = false;
            this.xrtblListOfSupportingDocsNA.StylePriority.UsePadding = false;
            this.xrtblListOfSupportingDocsNA.Weight = 1.5D;
            // 
            // xrtblReview
            // 
            this.xrtblReview.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrtblReview.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrtblReview.LocationFloat = new DevExpress.Utils.PointFloat(0F, 920.8333F);
            this.xrtblReview.Name = "xrtblReview";
            this.xrtblReview.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow30});
            this.xrtblReview.SizeF = new System.Drawing.SizeF(786.9999F, 25F);
            this.xrtblReview.StylePriority.UseBorders = false;
            this.xrtblReview.StylePriority.UseFont = false;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell40,
            this.xrTableCell41,
            this.xrTableCell42,
            this.xrTableCell43});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 1D;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell40.Multiline = true;
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell40.StylePriority.UseFont = false;
            this.xrTableCell40.Text = "Review On";
            this.xrTableCell40.Weight = 1.1458332888467471D;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell41.Multiline = true;
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell41.StylePriority.UseFont = false;
            this.xrTableCell41.Text = "Status";
            this.xrTableCell41.Weight = 1.1041667220836176D;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell42.Multiline = true;
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell42.StylePriority.UseFont = false;
            this.xrTableCell42.Text = "Related Document Reference No";
            this.xrTableCell42.Weight = 2.5729169388874076D;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell43.Multiline = true;
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell43.StylePriority.UseFont = false;
            this.xrTableCell43.StylePriority.UsePadding = false;
            this.xrTableCell43.Text = "Comments";
            this.xrTableCell43.Weight = 2.1570833553580089D;
            // 
            // xrTableReviewofWorkorderlbl
            // 
            this.xrTableReviewofWorkorderlbl.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableReviewofWorkorderlbl.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableReviewofWorkorderlbl.LocationFloat = new DevExpress.Utils.PointFloat(0F, 882.2917F);
            this.xrTableReviewofWorkorderlbl.Name = "xrTableReviewofWorkorderlbl";
            this.xrTableReviewofWorkorderlbl.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow28});
            this.xrTableReviewofWorkorderlbl.SizeF = new System.Drawing.SizeF(786.9999F, 38.54156F);
            this.xrTableReviewofWorkorderlbl.StylePriority.UseBorders = false;
            this.xrTableReviewofWorkorderlbl.StylePriority.UseFont = false;
            this.xrTableReviewofWorkorderlbl.StylePriority.UseTextAlignment = false;
            this.xrTableReviewofWorkorderlbl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 1D;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell37.Multiline = true;
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell37.StylePriority.UseBorders = false;
            this.xrTableCell37.StylePriority.UseFont = false;
            this.xrTableCell37.Text = "Review of Work order / Change Control / Preventive Maintenance / Calibration";
            this.xrTableCell37.Weight = 3D;
            // 
            // xrTableHasIncidentAffectdocText
            // 
            this.xrTableHasIncidentAffectdocText.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableHasIncidentAffectdocText.LocationFloat = new DevExpress.Utils.PointFloat(0F, 784.3749F);
            this.xrTableHasIncidentAffectdocText.Name = "xrTableHasIncidentAffectdocText";
            this.xrTableHasIncidentAffectdocText.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow29});
            this.xrTableHasIncidentAffectdocText.SizeF = new System.Drawing.SizeF(786.9999F, 43.75006F);
            this.xrTableHasIncidentAffectdocText.StylePriority.UseBorders = false;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell38,
            this.xrcbxHasIncidentAffect});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 1D;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrTableCell38.Multiline = true;
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell38.StylePriority.UseFont = false;
            this.xrTableCell38.StylePriority.UseTextAlignment = false;
            this.xrTableCell38.Text = "Has Incident Affected Documents :";
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell38.Weight = 1.8038854170240481D;
            // 
            // xrcbxHasIncidentAffect
            // 
            this.xrcbxHasIncidentAffect.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrcbxHasIncidentAffect.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrcbxHasIncidentAffect.Multiline = true;
            this.xrcbxHasIncidentAffect.Name = "xrcbxHasIncidentAffect";
            this.xrcbxHasIncidentAffect.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrcbxHasIncidentAffect.StylePriority.UseBorders = false;
            this.xrcbxHasIncidentAffect.StylePriority.UseFont = false;
            this.xrcbxHasIncidentAffect.StylePriority.UseTextAlignment = false;
            this.xrcbxHasIncidentAffect.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrcbxHasIncidentAffect.Weight = 3.01903101348484D;
            // 
            // xrTableHasIncidentAffectedDocs
            // 
            this.xrTableHasIncidentAffectedDocs.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableHasIncidentAffectedDocs.LocationFloat = new DevExpress.Utils.PointFloat(0F, 748.9583F);
            this.xrTableHasIncidentAffectedDocs.Name = "xrTableHasIncidentAffectedDocs";
            this.xrTableHasIncidentAffectedDocs.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow27});
            this.xrTableHasIncidentAffectedDocs.SizeF = new System.Drawing.SizeF(787F, 35.41663F);
            this.xrTableHasIncidentAffectedDocs.StylePriority.UseBorders = false;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell36});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.StylePriority.UseTextAlignment = false;
            this.xrTableRow27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableRow27.Weight = 1D;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell36.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell36.Multiline = true;
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell36.StylePriority.UseBorders = false;
            this.xrTableCell36.StylePriority.UseFont = false;
            this.xrTableCell36.Text = "Has the Incident Affected Documents";
            this.xrTableCell36.Weight = 3D;
            // 
            // xrtblRunningCondition
            // 
            this.xrtblRunningCondition.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrtblRunningCondition.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrtblRunningCondition.LocationFloat = new DevExpress.Utils.PointFloat(0F, 708.3332F);
            this.xrtblRunningCondition.Name = "xrtblRunningCondition";
            this.xrtblRunningCondition.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow26});
            this.xrtblRunningCondition.SizeF = new System.Drawing.SizeF(787F, 25F);
            this.xrtblRunningCondition.StylePriority.UseBorders = false;
            this.xrtblRunningCondition.StylePriority.UseFont = false;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32,
            this.xrTableCell33,
            this.xrTableCell34,
            this.xrTableCell35});
            this.xrTableRow26.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.StylePriority.UseFont = false;
            this.xrTableRow26.Weight = 1D;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell32.Multiline = true;
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell32.StylePriority.UseFont = false;
            this.xrTableCell32.Text = "Masters";
            this.xrTableCell32.Weight = 1.0013207185147208D;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell33.Multiline = true;
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell33.StylePriority.UseFont = false;
            this.xrTableCell33.Text = "Status\t";
            this.xrTableCell33.Weight = 0.47364145908763777D;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell34.Multiline = true;
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell34.StylePriority.UseFont = false;
            this.xrTableCell34.Text = "Related Document Reference No";
            this.xrTableCell34.Weight = 1.5603730242894294D;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell35.Multiline = true;
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell35.StylePriority.UseFont = false;
            this.xrTableCell35.StylePriority.UsePadding = false;
            this.xrTableCell35.Text = "Comments";
            this.xrTableCell35.Weight = 0.90816762296696907D;
            // 
            // xrTableRunningConditionlbl
            // 
            this.xrTableRunningConditionlbl.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRunningConditionlbl.LocationFloat = new DevExpress.Utils.PointFloat(0F, 668.7499F);
            this.xrTableRunningConditionlbl.Name = "xrTableRunningConditionlbl";
            this.xrTableRunningConditionlbl.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow25});
            this.xrTableRunningConditionlbl.SizeF = new System.Drawing.SizeF(787F, 39.58331F);
            this.xrTableRunningConditionlbl.StylePriority.UseBorders = false;
            this.xrTableRunningConditionlbl.StylePriority.UseTextAlignment = false;
            this.xrTableRunningConditionlbl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell31});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 1D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell31.Multiline = true;
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell31.StylePriority.UseBorders = false;
            this.xrTableCell31.StylePriority.UseFont = false;
            this.xrTableCell31.Text = "Running Condition";
            this.xrTableCell31.Weight = 1D;
            // 
            // xrtblInvestigationAffectedOn
            // 
            this.xrtblInvestigationAffectedOn.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrtblInvestigationAffectedOn.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrtblInvestigationAffectedOn.LocationFloat = new DevExpress.Utils.PointFloat(0F, 643.7499F);
            this.xrtblInvestigationAffectedOn.Name = "xrtblInvestigationAffectedOn";
            this.xrtblInvestigationAffectedOn.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24});
            this.xrtblInvestigationAffectedOn.SizeF = new System.Drawing.SizeF(787F, 25F);
            this.xrtblInvestigationAffectedOn.StylePriority.UseBorders = false;
            this.xrtblInvestigationAffectedOn.StylePriority.UseFont = false;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell28,
            this.xrTableCell29,
            this.xrTableCell30});
            this.xrTableRow24.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.StylePriority.UseFont = false;
            this.xrTableRow24.Weight = 1D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell25.Multiline = true;
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell25.StylePriority.UseFont = false;
            this.xrTableCell25.Text = "Masters\t";
            this.xrTableCell25.Weight = 1.8174036713816375D;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell28.Multiline = true;
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.Text = "Status";
            this.xrTableCell28.Weight = 0.85966486489120131D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell29.Multiline = true;
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.Text = "Related Document Reference No";
            this.xrTableCell29.Weight = 2.8320960450259891D;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell30.Multiline = true;
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell30.StylePriority.UseFont = false;
            this.xrTableCell30.StylePriority.UsePadding = false;
            this.xrTableCell30.Text = "Comments";
            this.xrTableCell30.Weight = 1.6483354187011721D;
            // 
            // xrTableIncidentAffectedonlbl
            // 
            this.xrTableIncidentAffectedonlbl.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableIncidentAffectedonlbl.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableIncidentAffectedonlbl.LocationFloat = new DevExpress.Utils.PointFloat(0F, 604.1665F);
            this.xrTableIncidentAffectedonlbl.Name = "xrTableIncidentAffectedonlbl";
            this.xrTableIncidentAffectedonlbl.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow23});
            this.xrTableIncidentAffectedonlbl.SizeF = new System.Drawing.SizeF(787F, 39.58337F);
            this.xrTableIncidentAffectedonlbl.StylePriority.UseBorders = false;
            this.xrTableIncidentAffectedonlbl.StylePriority.UseFont = false;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell24.Multiline = true;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell24.StylePriority.UseBorders = false;
            this.xrTableCell24.StylePriority.UseFont = false;
            this.xrTableCell24.StylePriority.UseTextAlignment = false;
            this.xrTableCell24.Text = "Incident Affected On";
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell24.Weight = 1D;
            // 
            // xrtblInvestigationImpactOfIncidient
            // 
            this.xrtblInvestigationImpactOfIncidient.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrtblInvestigationImpactOfIncidient.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrtblInvestigationImpactOfIncidient.LocationFloat = new DevExpress.Utils.PointFloat(0F, 505.2082F);
            this.xrtblInvestigationImpactOfIncidient.Name = "xrtblInvestigationImpactOfIncidient";
            this.xrtblInvestigationImpactOfIncidient.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22});
            this.xrtblInvestigationImpactOfIncidient.SizeF = new System.Drawing.SizeF(786.9999F, 24.99991F);
            this.xrtblInvestigationImpactOfIncidient.StylePriority.UseBorders = false;
            this.xrtblInvestigationImpactOfIncidient.StylePriority.UseFont = false;
            this.xrtblInvestigationImpactOfIncidient.StylePriority.UseTextAlignment = false;
            this.xrtblInvestigationImpactOfIncidient.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.xrTableCell22,
            this.xrTableCell26,
            this.xrTableCell27});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 1D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell21.Multiline = true;
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.Text = "Impact Points\t";
            this.xrTableCell21.Weight = 1.8174000977739218D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell22.Multiline = true;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.Text = "Status\t";
            this.xrTableCell22.Weight = 0.85968333753670556D;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell26.Multiline = true;
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.Text = "Related Document Reference No";
            this.xrTableCell26.Weight = 2.8320822139526967D;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell27.Multiline = true;
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell27.StylePriority.UseFont = false;
            this.xrTableCell27.StylePriority.UsePadding = false;
            this.xrTableCell27.Text = "Comments";
            this.xrTableCell27.Weight = 1.6483343481472925D;
            // 
            // xrTableHastheincidentaffectlbl
            // 
            this.xrTableHastheincidentaffectlbl.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableHastheincidentaffectlbl.LocationFloat = new DevExpress.Utils.PointFloat(0F, 563.5417F);
            this.xrTableHastheincidentaffectlbl.Name = "xrTableHastheincidentaffectlbl";
            this.xrTableHastheincidentaffectlbl.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow21});
            this.xrTableHastheincidentaffectlbl.SizeF = new System.Drawing.SizeF(787F, 40.62469F);
            this.xrTableHastheincidentaffectlbl.StylePriority.UseBorders = false;
            this.xrTableHastheincidentaffectlbl.StylePriority.UseTextAlignment = false;
            this.xrTableHastheincidentaffectlbl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 1D;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell23.Multiline = true;
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell23.StylePriority.UseBorders = false;
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.Text = "Has the incident affected Equipment, Instrument and Area";
            this.xrTableCell23.Weight = 3D;
            // 
            // xrTableImpactofincidentonpreviouslbl
            // 
            this.xrTableImpactofincidentonpreviouslbl.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableImpactofincidentonpreviouslbl.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableImpactofincidentonpreviouslbl.LocationFloat = new DevExpress.Utils.PointFloat(0F, 465.6248F);
            this.xrTableImpactofincidentonpreviouslbl.Name = "xrTableImpactofincidentonpreviouslbl";
            this.xrTableImpactofincidentonpreviouslbl.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow20});
            this.xrTableImpactofincidentonpreviouslbl.SizeF = new System.Drawing.SizeF(787F, 39.58337F);
            this.xrTableImpactofincidentonpreviouslbl.StylePriority.UseBorders = false;
            this.xrTableImpactofincidentonpreviouslbl.StylePriority.UseFont = false;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 1D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell20.Multiline = true;
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell20.StylePriority.UseBorders = false;
            this.xrTableCell20.StylePriority.UseFont = false;
            this.xrTableCell20.StylePriority.UseTextAlignment = false;
            this.xrTableCell20.Text = "Impact of Incident on previous batches / related products / process / study";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell20.Weight = 3D;
            // 
            // xrtblWhoArePeopleInvolvedReport2
            // 
            this.xrtblWhoArePeopleInvolvedReport2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrtblWhoArePeopleInvolvedReport2.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrtblWhoArePeopleInvolvedReport2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 417.7084F);
            this.xrtblWhoArePeopleInvolvedReport2.Name = "xrtblWhoArePeopleInvolvedReport2";
            this.xrtblWhoArePeopleInvolvedReport2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrtblWhoArePeopleInvolved});
            this.xrtblWhoArePeopleInvolvedReport2.SizeF = new System.Drawing.SizeF(786.9999F, 30.20834F);
            this.xrtblWhoArePeopleInvolvedReport2.StylePriority.UseBorders = false;
            this.xrtblWhoArePeopleInvolvedReport2.StylePriority.UseFont = false;
            // 
            // xrtblWhoArePeopleInvolved
            // 
            this.xrtblWhoArePeopleInvolved.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell16,
            this.xrTableCell19});
            this.xrtblWhoArePeopleInvolved.Name = "xrtblWhoArePeopleInvolved";
            this.xrtblWhoArePeopleInvolved.StylePriority.UseTextAlignment = false;
            this.xrtblWhoArePeopleInvolved.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrtblWhoArePeopleInvolved.Weight = 1D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell16.Multiline = true;
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell16.StylePriority.UseFont = false;
            this.xrTableCell16.Text = "Department\t";
            this.xrTableCell16.Weight = 1.3902068305446744D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell19.Multiline = true;
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.Text = "Employee Name";
            this.xrTableCell19.Weight = 1.6097931694553256D;
            // 
            // xrTableWhoarethepplinvolvlbl
            // 
            this.xrTableWhoarethepplinvolvlbl.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableWhoarethepplinvolvlbl.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableWhoarethepplinvolvlbl.LocationFloat = new DevExpress.Utils.PointFloat(0F, 367.7083F);
            this.xrTableWhoarethepplinvolvlbl.Name = "xrTableWhoarethepplinvolvlbl";
            this.xrTableWhoarethepplinvolvlbl.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow18});
            this.xrTableWhoarethepplinvolvlbl.SizeF = new System.Drawing.SizeF(786.9999F, 36.45831F);
            this.xrTableWhoarethepplinvolvlbl.StylePriority.UseBorders = false;
            this.xrTableWhoarethepplinvolvlbl.StylePriority.UseFont = false;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15,
            this.xrtblWhoArePeopleInvolvedReportNA});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.StylePriority.UseBorders = false;
            this.xrTableRow18.Weight = 1D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell15.Multiline = true;
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "Who are the people Involved ?";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell15.Weight = 0.5D;
            // 
            // xrtblWhoArePeopleInvolvedReportNA
            // 
            this.xrtblWhoArePeopleInvolvedReportNA.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrtblWhoArePeopleInvolvedReportNA.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrtblWhoArePeopleInvolvedReportNA.Multiline = true;
            this.xrtblWhoArePeopleInvolvedReportNA.Name = "xrtblWhoArePeopleInvolvedReportNA";
            this.xrtblWhoArePeopleInvolvedReportNA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrtblWhoArePeopleInvolvedReportNA.StylePriority.UseBorders = false;
            this.xrtblWhoArePeopleInvolvedReportNA.StylePriority.UseFont = false;
            this.xrtblWhoArePeopleInvolvedReportNA.StylePriority.UsePadding = false;
            this.xrtblWhoArePeopleInvolvedReportNA.StylePriority.UseTextAlignment = false;
            this.xrtblWhoArePeopleInvolvedReportNA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrtblWhoArePeopleInvolvedReportNA.Weight = 0.5D;
            // 
            // xrTableWhereHappenedText
            // 
            this.xrTableWhereHappenedText.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableWhereHappenedText.LocationFloat = new DevExpress.Utils.PointFloat(0F, 300.0001F);
            this.xrTableWhereHappenedText.Name = "xrTableWhereHappenedText";
            this.xrTableWhereHappenedText.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow17});
            this.xrTableWhereHappenedText.SizeF = new System.Drawing.SizeF(787F, 67.70831F);
            this.xrTableWhereHappenedText.StylePriority.UseFont = false;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrlblWhereHappened_Desc});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 1D;
            // 
            // xrlblWhereHappened_Desc
            // 
            this.xrlblWhereHappened_Desc.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblWhereHappened_Desc.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrlblWhereHappened_Desc.Multiline = true;
            this.xrlblWhereHappened_Desc.Name = "xrlblWhereHappened_Desc";
            this.xrlblWhereHappened_Desc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblWhereHappened_Desc.StylePriority.UseBorders = false;
            this.xrlblWhereHappened_Desc.StylePriority.UseFont = false;
            this.xrlblWhereHappened_Desc.Weight = 2D;
            // 
            // xrTableWhereHappenedlbl
            // 
            this.xrTableWhereHappenedlbl.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableWhereHappenedlbl.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableWhereHappenedlbl.LocationFloat = new DevExpress.Utils.PointFloat(0F, 275.0001F);
            this.xrTableWhereHappenedlbl.Name = "xrTableWhereHappenedlbl";
            this.xrTableWhereHappenedlbl.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow16});
            this.xrTableWhereHappenedlbl.SizeF = new System.Drawing.SizeF(786.9999F, 25F);
            this.xrTableWhereHappenedlbl.StylePriority.UseBorders = false;
            this.xrTableWhereHappenedlbl.StylePriority.UseFont = false;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell12});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell12.Multiline = true;
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell12.StylePriority.UseBorders = false;
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.Text = "Where happened ?";
            this.xrTableCell12.Weight = 3D;
            // 
            // xrTableWhenhappenedText
            // 
            this.xrTableWhenhappenedText.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableWhenhappenedText.LocationFloat = new DevExpress.Utils.PointFloat(0F, 211.4584F);
            this.xrTableWhenhappenedText.Name = "xrTableWhenhappenedText";
            this.xrTableWhenhappenedText.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15});
            this.xrTableWhenhappenedText.SizeF = new System.Drawing.SizeF(786.9999F, 63.54166F);
            this.xrTableWhenhappenedText.StylePriority.UseBorders = false;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrlblWhenHappened_Desc});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 1D;
            // 
            // xrlblWhenHappened_Desc
            // 
            this.xrlblWhenHappened_Desc.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrlblWhenHappened_Desc.Multiline = true;
            this.xrlblWhenHappened_Desc.Name = "xrlblWhenHappened_Desc";
            this.xrlblWhenHappened_Desc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblWhenHappened_Desc.StylePriority.UseFont = false;
            this.xrlblWhenHappened_Desc.Weight = 3D;
            // 
            // xrTableWhenHappenedlbl
            // 
            this.xrTableWhenHappenedlbl.LocationFloat = new DevExpress.Utils.PointFloat(0F, 186.4584F);
            this.xrTableWhenHappenedlbl.Name = "xrTableWhenHappenedlbl";
            this.xrTableWhenHappenedlbl.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14});
            this.xrTableWhenHappenedlbl.SizeF = new System.Drawing.SizeF(787F, 25F);
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.StylePriority.UseBorders = false;
            this.xrTableRow14.Weight = 1D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell14.Multiline = true;
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.Text = "When happened ?";
            this.xrTableCell14.Weight = 1D;
            // 
            // xrTableWhathappenedText
            // 
            this.xrTableWhathappenedText.LocationFloat = new DevExpress.Utils.PointFloat(0F, 123.9584F);
            this.xrTableWhathappenedText.Name = "xrTableWhathappenedText";
            this.xrTableWhathappenedText.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12});
            this.xrTableWhathappenedText.SizeF = new System.Drawing.SizeF(787F, 62.50001F);
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrlblWhatHappened_Desc});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1D;
            // 
            // xrlblWhatHappened_Desc
            // 
            this.xrlblWhatHappened_Desc.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblWhatHappened_Desc.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrlblWhatHappened_Desc.Multiline = true;
            this.xrlblWhatHappened_Desc.Name = "xrlblWhatHappened_Desc";
            this.xrlblWhatHappened_Desc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblWhatHappened_Desc.StylePriority.UseBorders = false;
            this.xrlblWhatHappened_Desc.StylePriority.UseFont = false;
            this.xrlblWhatHappened_Desc.Weight = 2D;
            // 
            // xrTableWhatHappenedlbl
            // 
            this.xrTableWhatHappenedlbl.LocationFloat = new DevExpress.Utils.PointFloat(0F, 98.9584F);
            this.xrTableWhatHappenedlbl.Name = "xrTableWhatHappenedlbl";
            this.xrTableWhatHappenedlbl.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11});
            this.xrTableWhatHappenedlbl.SizeF = new System.Drawing.SizeF(786.9999F, 25F);
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell5.Multiline = true;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.Text = "What happened ?";
            this.xrTableCell5.Weight = 3D;
            // 
            // xrTableInvestigationLabel
            // 
            this.xrTableInvestigationLabel.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableInvestigationLabel.LocationFloat = new DevExpress.Utils.PointFloat(0F, 60.41673F);
            this.xrTableInvestigationLabel.Name = "xrTableInvestigationLabel";
            this.xrTableInvestigationLabel.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTableInvestigationLabel.SizeF = new System.Drawing.SizeF(787F, 38.54166F);
            this.xrTableInvestigationLabel.StylePriority.UseBorders = false;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell11});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell11.Multiline = true;
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.Text = "Investigation";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell11.Weight = 2D;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 62F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.HeightF = 45.83333F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(686.9996F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.TextFormatString = "page {0} of {1}";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7,
            this.pbxCompanyLogo});
            this.PageHeader.HeightF = 98.9583F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrTable7
            // 
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 59.79163F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13});
            this.xrTable7.SizeF = new System.Drawing.SizeF(787F, 34.375F);
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell18});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell18.Multiline = true;
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell18.StylePriority.UseBorders = false;
            this.xrTableCell18.StylePriority.UseFont = false;
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.Text = "INCIDENT REPORT";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell18.Weight = 1D;
            // 
            // pbxCompanyLogo
            // 
            this.pbxCompanyLogo.LocationFloat = new DevExpress.Utils.PointFloat(592.4927F, 10.00001F);
            this.pbxCompanyLogo.Name = "pbxCompanyLogo";
            this.pbxCompanyLogo.SizeF = new System.Drawing.SizeF(194.5073F, 38.62333F);
            this.pbxCompanyLogo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // IncidentReportPage2
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageFooter,
            this.PageHeader});
            this.Margins = new System.Drawing.Printing.Margins(21, 19, 0, 62);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "18.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTableSectionIVText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableSectionIVLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHasIncidentAffectDocs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableQualityImpactingLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableInvestigatorSignaturetxt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableConclusionText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableConclusionlbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrtblOtherDepHodReview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableOtherDeptHodReviewlbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrtblListOfSupportingDocs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableListofSupprtDocs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrtblReview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableReviewofWorkorderlbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHasIncidentAffectdocText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHasIncidentAffectedDocs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrtblRunningCondition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableRunningConditionlbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrtblInvestigationAffectedOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableIncidentAffectedonlbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrtblInvestigationImpactOfIncidient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHastheincidentaffectlbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableImpactofincidentonpreviouslbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrtblWhoArePeopleInvolvedReport2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableWhoarethepplinvolvlbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableWhereHappenedText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableWhereHappenedlbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableWhenhappenedText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableWhenHappenedlbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableWhathappenedText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableWhatHappenedlbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableInvestigationLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRPictureBox pbxCompanyLogo;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTable xrTableInvestigationLabel;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTable xrTableWhatHappenedlbl;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTable xrTableWhenhappenedText;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrlblWhenHappened_Desc;
        private DevExpress.XtraReports.UI.XRTable xrTableWhenHappenedlbl;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTable xrTableWhathappenedText;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrlblWhatHappened_Desc;
        private DevExpress.XtraReports.UI.XRTable xrTableWhereHappenedText;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrlblWhereHappened_Desc;
        private DevExpress.XtraReports.UI.XRTable xrTableWhereHappenedlbl;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTable xrTableWhoarethepplinvolvlbl;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTable xrtblWhoArePeopleInvolvedReport2;
        private DevExpress.XtraReports.UI.XRTableRow xrtblWhoArePeopleInvolved;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTable xrTableHastheincidentaffectlbl;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTable xrTableImpactofincidentonpreviouslbl;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTable xrtblInvestigationImpactOfIncidient;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTable xrTableIncidentAffectedonlbl;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTable xrtblInvestigationAffectedOn;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTable xrTableRunningConditionlbl;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTable xrtblRunningCondition;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTable xrTableHasIncidentAffectedDocs;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTable xrTableHasIncidentAffectdocText;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrcbxHasIncidentAffect;
        private DevExpress.XtraReports.UI.XRTable xrTableReviewofWorkorderlbl;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTable xrtblReview;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTable xrTableListofSupprtDocs;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTable xrtblListOfSupportingDocs;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTable xrTableOtherDeptHodReviewlbl;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTable xrtblOtherDepHodReview;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTable xrTableInvestigatorSignaturetxt;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell xrInvestCreated;
        private DevExpress.XtraReports.UI.XRTable xrTableConclusionText;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrlblConclusion_Desc;
        private DevExpress.XtraReports.UI.XRTable xrTableConclusionlbl;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRLabel xrlblInvestName;
        private DevExpress.XtraReports.UI.XRTable xrTableQualityImpactingLabel;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTable xrTableHasIncidentAffectDocs;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrtblWhoArePeopleInvolvedReportNA;
        private DevExpress.XtraReports.UI.XRTableCell xrtblListOfSupportingDocsNA;
        private DevExpress.XtraReports.UI.XRTableCell xrtblOtherDepHodReviewNA;
        private DevExpress.XtraReports.UI.XRLabel xrlblInvestDesigName;
        private DevExpress.XtraReports.UI.XRTable xrTableSectionIVLabel;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRTable xrTableSectionIVText;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrlblIsImmediateCorrectionReportNew;
        private DevExpress.XtraReports.UI.XRLabel xrInvestCreatedDate;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
    }
}
