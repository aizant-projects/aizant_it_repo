﻿namespace AizantIT_PharmaApp.Areas.QMS.XtraReports.Incident
{
    partial class IR_SectionFive_Report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrHeadQACreatedDateActionPlan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrQACreatedDateActionPlan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrHodCreatedDateActionPlan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLblHeadQADesigActionPlan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLblHeadQANameActionPlan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrHeadQACreatedDateActionPlan11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLblQADesigActionPlan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLblQANameActionPlan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrQACreatedDateActionPlan11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrlblEvaluationandDispostionReport4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLblHODDesigActionPlan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLblHODNameActionPlan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrHodCreatedDateActionPlan11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrlblAnyTrainingRequired = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pbxCompanyLogo = new DevExpress.XtraReports.UI.XRPictureBox();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrHeadQACreatedDateActionPlan,
            this.xrQACreatedDateActionPlan,
            this.xrHodCreatedDateActionPlan,
            this.xrLblHeadQADesigActionPlan,
            this.xrTable9,
            this.xrLblQADesigActionPlan,
            this.xrTable7,
            this.xrTable6,
            this.xrTable5,
            this.xrLblHODDesigActionPlan,
            this.xrTable8,
            this.xrTable2});
            this.Detail.HeightF = 549.7917F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrHeadQACreatedDateActionPlan
            // 
            this.xrHeadQACreatedDateActionPlan.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrHeadQACreatedDateActionPlan.LocationFloat = new DevExpress.Utils.PointFloat(585.1824F, 496.8749F);
            this.xrHeadQACreatedDateActionPlan.Multiline = true;
            this.xrHeadQACreatedDateActionPlan.Name = "xrHeadQACreatedDateActionPlan";
            this.xrHeadQACreatedDateActionPlan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrHeadQACreatedDateActionPlan.SizeF = new System.Drawing.SizeF(190.8175F, 31.04175F);
            this.xrHeadQACreatedDateActionPlan.StylePriority.UseFont = false;
            this.xrHeadQACreatedDateActionPlan.StylePriority.UseTextAlignment = false;
            this.xrHeadQACreatedDateActionPlan.Text = "xrHeadQACreatedDateActionPlan";
            this.xrHeadQACreatedDateActionPlan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrQACreatedDateActionPlan
            // 
            this.xrQACreatedDateActionPlan.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrQACreatedDateActionPlan.LocationFloat = new DevExpress.Utils.PointFloat(585.1824F, 391.6667F);
            this.xrQACreatedDateActionPlan.Multiline = true;
            this.xrQACreatedDateActionPlan.Name = "xrQACreatedDateActionPlan";
            this.xrQACreatedDateActionPlan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrQACreatedDateActionPlan.SizeF = new System.Drawing.SizeF(190.8176F, 26.87491F);
            this.xrQACreatedDateActionPlan.StylePriority.UseFont = false;
            this.xrQACreatedDateActionPlan.StylePriority.UseTextAlignment = false;
            this.xrQACreatedDateActionPlan.Text = "xrQACreatedDateActionPlan";
            this.xrQACreatedDateActionPlan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrHodCreatedDateActionPlan
            // 
            this.xrHodCreatedDateActionPlan.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrHodCreatedDateActionPlan.LocationFloat = new DevExpress.Utils.PointFloat(585.1824F, 104.7917F);
            this.xrHodCreatedDateActionPlan.Multiline = true;
            this.xrHodCreatedDateActionPlan.Name = "xrHodCreatedDateActionPlan";
            this.xrHodCreatedDateActionPlan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrHodCreatedDateActionPlan.SizeF = new System.Drawing.SizeF(190.8176F, 36.25008F);
            this.xrHodCreatedDateActionPlan.StylePriority.UseFont = false;
            this.xrHodCreatedDateActionPlan.Text = "xrHodCreatedDateActionPlan";
            // 
            // xrLblHeadQADesigActionPlan
            // 
            this.xrLblHeadQADesigActionPlan.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLblHeadQADesigActionPlan.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLblHeadQADesigActionPlan.ForeColor = System.Drawing.Color.Blue;
            this.xrLblHeadQADesigActionPlan.LocationFloat = new DevExpress.Utils.PointFloat(171.3315F, 496.8749F);
            this.xrLblHeadQADesigActionPlan.Multiline = true;
            this.xrLblHeadQADesigActionPlan.Name = "xrLblHeadQADesigActionPlan";
            this.xrLblHeadQADesigActionPlan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblHeadQADesigActionPlan.SizeF = new System.Drawing.SizeF(413.8508F, 31.04175F);
            this.xrLblHeadQADesigActionPlan.StylePriority.UseBorders = false;
            this.xrLblHeadQADesigActionPlan.StylePriority.UseFont = false;
            this.xrLblHeadQADesigActionPlan.StylePriority.UseForeColor = false;
            this.xrLblHeadQADesigActionPlan.StylePriority.UseTextAlignment = false;
            this.xrLblHeadQADesigActionPlan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable9
            // 
            this.xrTable9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable9.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 451.0416F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable9.SizeF = new System.Drawing.SizeF(776F, 45.83334F);
            this.xrTable9.StylePriority.UseBorders = false;
            this.xrTable9.StylePriority.UseFont = false;
            this.xrTable9.StylePriority.UseTextAlignment = false;
            this.xrTable9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.xrTableCell10,
            this.xrHeadQACreatedDateActionPlan11});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell8.Multiline = true;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.Text = "Approved by (E-Sign)";
            this.xrTableCell8.Weight = 0.6640770744999458D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLblHeadQANameActionPlan});
            this.xrTableCell10.Multiline = true;
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.Weight = 1.6040725061239003D;
            // 
            // xrLblHeadQANameActionPlan
            // 
            this.xrLblHeadQANameActionPlan.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLblHeadQANameActionPlan.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLblHeadQANameActionPlan.ForeColor = System.Drawing.Color.Blue;
            this.xrLblHeadQANameActionPlan.LocationFloat = new DevExpress.Utils.PointFloat(9.999878F, 10F);
            this.xrLblHeadQANameActionPlan.Multiline = true;
            this.xrLblHeadQANameActionPlan.Name = "xrLblHeadQANameActionPlan";
            this.xrLblHeadQANameActionPlan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblHeadQANameActionPlan.SizeF = new System.Drawing.SizeF(385.983F, 25.83337F);
            this.xrLblHeadQANameActionPlan.StylePriority.UseBorders = false;
            this.xrLblHeadQANameActionPlan.StylePriority.UseFont = false;
            this.xrLblHeadQANameActionPlan.StylePriority.UseForeColor = false;
            // 
            // xrHeadQACreatedDateActionPlan11
            // 
            this.xrHeadQACreatedDateActionPlan11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrHeadQACreatedDateActionPlan11.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrHeadQACreatedDateActionPlan11.Multiline = true;
            this.xrHeadQACreatedDateActionPlan11.Name = "xrHeadQACreatedDateActionPlan11";
            this.xrHeadQACreatedDateActionPlan11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrHeadQACreatedDateActionPlan11.StylePriority.UseBorders = false;
            this.xrHeadQACreatedDateActionPlan11.StylePriority.UseFont = false;
            this.xrHeadQACreatedDateActionPlan11.Text = "Approved Date";
            this.xrHeadQACreatedDateActionPlan11.Weight = 0.73960283172405694D;
            // 
            // xrLblQADesigActionPlan
            // 
            this.xrLblQADesigActionPlan.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLblQADesigActionPlan.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLblQADesigActionPlan.ForeColor = System.Drawing.Color.Blue;
            this.xrLblQADesigActionPlan.LocationFloat = new DevExpress.Utils.PointFloat(171.3317F, 391.6667F);
            this.xrLblQADesigActionPlan.Multiline = true;
            this.xrLblQADesigActionPlan.Name = "xrLblQADesigActionPlan";
            this.xrLblQADesigActionPlan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblQADesigActionPlan.SizeF = new System.Drawing.SizeF(413.8507F, 26.87491F);
            this.xrLblQADesigActionPlan.StylePriority.UseBorders = false;
            this.xrLblQADesigActionPlan.StylePriority.UseFont = false;
            this.xrLblQADesigActionPlan.StylePriority.UseForeColor = false;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable7.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 348.9584F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable7.SizeF = new System.Drawing.SizeF(776F, 42.70828F);
            this.xrTable7.StylePriority.UseBorders = false;
            this.xrTable7.StylePriority.UseFont = false;
            this.xrTable7.StylePriority.UseTextAlignment = false;
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.xrTableCell7,
            this.xrQACreatedDateActionPlan11});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell5.Multiline = true;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.Text = "Approved by (E-Sign)";
            this.xrTableCell5.Weight = 0.662365090687493D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLblQANameActionPlan});
            this.xrTableCell7.Multiline = true;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.Weight = 1.5999380722116821D;
            // 
            // xrLblQANameActionPlan
            // 
            this.xrLblQANameActionPlan.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLblQANameActionPlan.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLblQANameActionPlan.ForeColor = System.Drawing.Color.Blue;
            this.xrLblQANameActionPlan.LocationFloat = new DevExpress.Utils.PointFloat(10F, 10F);
            this.xrLblQANameActionPlan.Multiline = true;
            this.xrLblQANameActionPlan.Name = "xrLblQANameActionPlan";
            this.xrLblQANameActionPlan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblQANameActionPlan.SizeF = new System.Drawing.SizeF(385.983F, 25.83328F);
            this.xrLblQANameActionPlan.StylePriority.UseBorders = false;
            this.xrLblQANameActionPlan.StylePriority.UseFont = false;
            this.xrLblQANameActionPlan.StylePriority.UseForeColor = false;
            // 
            // xrQACreatedDateActionPlan11
            // 
            this.xrQACreatedDateActionPlan11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrQACreatedDateActionPlan11.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrQACreatedDateActionPlan11.Multiline = true;
            this.xrQACreatedDateActionPlan11.Name = "xrQACreatedDateActionPlan11";
            this.xrQACreatedDateActionPlan11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrQACreatedDateActionPlan11.StylePriority.UseBorders = false;
            this.xrQACreatedDateActionPlan11.StylePriority.UseFont = false;
            this.xrQACreatedDateActionPlan11.Text = "Approved Date";
            this.xrQACreatedDateActionPlan11.Weight = 0.73769683710082523D;
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 203.1251F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable6.SizeF = new System.Drawing.SizeF(775.9999F, 145.8333F);
            this.xrTable6.StylePriority.UseBorders = false;
            this.xrTable6.StylePriority.UseFont = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrlblEvaluationandDispostionReport4});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrlblEvaluationandDispostionReport4
            // 
            this.xrlblEvaluationandDispostionReport4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblEvaluationandDispostionReport4.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrlblEvaluationandDispostionReport4.Multiline = true;
            this.xrlblEvaluationandDispostionReport4.Name = "xrlblEvaluationandDispostionReport4";
            this.xrlblEvaluationandDispostionReport4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblEvaluationandDispostionReport4.StylePriority.UseBorders = false;
            this.xrlblEvaluationandDispostionReport4.StylePriority.UseFont = false;
            this.xrlblEvaluationandDispostionReport4.Weight = 1D;
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 159.3751F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable5.SizeF = new System.Drawing.SizeF(775.9999F, 43.75003F);
            this.xrTable5.StylePriority.UseBorders = false;
            this.xrTable5.StylePriority.UseFont = false;
            this.xrTable5.StylePriority.UseTextAlignment = false;
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2,
            this.xrLabel1});
            this.xrTableCell3.Multiline = true;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell3.Weight = 3D;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(102F, 10.75003F);
            this.xrLabel2.Multiline = true;
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(238.4166F, 23F);
            this.xrLabel2.StylePriority.UseBorders = false;
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "Evaluation and disposition:";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(2F, 9.999969F);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "(Section V)";
            // 
            // xrLblHODDesigActionPlan
            // 
            this.xrLblHODDesigActionPlan.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLblHODDesigActionPlan.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLblHODDesigActionPlan.ForeColor = System.Drawing.Color.Blue;
            this.xrLblHODDesigActionPlan.LocationFloat = new DevExpress.Utils.PointFloat(171.3317F, 104.7917F);
            this.xrLblHODDesigActionPlan.Multiline = true;
            this.xrLblHODDesigActionPlan.Name = "xrLblHODDesigActionPlan";
            this.xrLblHODDesigActionPlan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLblHODDesigActionPlan.SizeF = new System.Drawing.SizeF(413.8507F, 36.25008F);
            this.xrLblHODDesigActionPlan.StylePriority.UseBorders = false;
            this.xrLblHODDesigActionPlan.StylePriority.UseFont = false;
            this.xrLblHODDesigActionPlan.StylePriority.UseForeColor = false;
            // 
            // xrTable8
            // 
            this.xrTable8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable8.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 58.95831F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable8.SizeF = new System.Drawing.SizeF(775.9999F, 45.83336F);
            this.xrTable8.StylePriority.UseBorders = false;
            this.xrTable8.StylePriority.UseFont = false;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell9,
            this.xrHodCreatedDateActionPlan11});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell4.Multiline = true;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "Approved by (E-Sign)";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell4.Weight = 0.66236509109066943D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLblHODNameActionPlan});
            this.xrTableCell9.Multiline = true;
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell9.Weight = 1.5999384773655163D;
            // 
            // xrLblHODNameActionPlan
            // 
            this.xrLblHODNameActionPlan.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLblHODNameActionPlan.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLblHODNameActionPlan.ForeColor = System.Drawing.Color.Blue;
            this.xrLblHODNameActionPlan.LocationFloat = new DevExpress.Utils.PointFloat(10F, 7.000004F);
            this.xrLblHODNameActionPlan.Multiline = true;
            this.xrLblHODNameActionPlan.Name = "xrLblHODNameActionPlan";
            this.xrLblHODNameActionPlan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLblHODNameActionPlan.SizeF = new System.Drawing.SizeF(385.9831F, 35.83339F);
            this.xrLblHODNameActionPlan.StylePriority.UseBorders = false;
            this.xrLblHODNameActionPlan.StylePriority.UseFont = false;
            this.xrLblHODNameActionPlan.StylePriority.UseForeColor = false;
            // 
            // xrHodCreatedDateActionPlan11
            // 
            this.xrHodCreatedDateActionPlan11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrHodCreatedDateActionPlan11.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrHodCreatedDateActionPlan11.Multiline = true;
            this.xrHodCreatedDateActionPlan11.Name = "xrHodCreatedDateActionPlan11";
            this.xrHodCreatedDateActionPlan11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrHodCreatedDateActionPlan11.StylePriority.UseBorders = false;
            this.xrHodCreatedDateActionPlan11.StylePriority.UseFont = false;
            this.xrHodCreatedDateActionPlan11.StylePriority.UseTextAlignment = false;
            this.xrHodCreatedDateActionPlan11.Text = "Approved Date";
            this.xrHodCreatedDateActionPlan11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrHodCreatedDateActionPlan11.Weight = 0.73769643154381437D;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 10.00001F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable2.SizeF = new System.Drawing.SizeF(775.9999F, 48.95831F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrlblAnyTrainingRequired});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrTableCell1.Multiline = true;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "Is Any Training Required:";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell1.Weight = 1.78380720036594D;
            // 
            // xrlblAnyTrainingRequired
            // 
            this.xrlblAnyTrainingRequired.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblAnyTrainingRequired.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrlblAnyTrainingRequired.Multiline = true;
            this.xrlblAnyTrainingRequired.Name = "xrlblAnyTrainingRequired";
            this.xrlblAnyTrainingRequired.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblAnyTrainingRequired.StylePriority.UseBorders = false;
            this.xrlblAnyTrainingRequired.StylePriority.UseFont = false;
            this.xrlblAnyTrainingRequired.StylePriority.UsePadding = false;
            this.xrlblAnyTrainingRequired.StylePriority.UseTextAlignment = false;
            this.xrlblAnyTrainingRequired.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrlblAnyTrainingRequired.Weight = 4.21619279963406D;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 1F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.pbxCompanyLogo});
            this.PageHeader.HeightF = 102.0833F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrTable1
            // 
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 64.58335F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13});
            this.xrTable1.SizeF = new System.Drawing.SizeF(776F, 34.375F);
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell6.Multiline = true;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "INCIDENT REPORT";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell6.Weight = 1D;
            // 
            // pbxCompanyLogo
            // 
            this.pbxCompanyLogo.LocationFloat = new DevExpress.Utils.PointFloat(566.8063F, 10.00001F);
            this.pbxCompanyLogo.Name = "pbxCompanyLogo";
            this.pbxCompanyLogo.SizeF = new System.Drawing.SizeF(209.1937F, 41.74833F);
            this.pbxCompanyLogo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(676F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.TextFormatString = "page {0} of {1}";
            // 
            // IR_SectionFive_Report
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.PageFooter});
            this.Margins = new System.Drawing.Printing.Margins(27, 24, 0, 1);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "18.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPictureBox pbxCompanyLogo;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrlblAnyTrainingRequired;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRLabel xrLblHODNameActionPlan;
        private DevExpress.XtraReports.UI.XRTableCell xrHodCreatedDateActionPlan11;
        private DevExpress.XtraReports.UI.XRLabel xrLblHODDesigActionPlan;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrlblEvaluationandDispostionReport4;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRLabel xrLblQANameActionPlan;
        private DevExpress.XtraReports.UI.XRTableCell xrQACreatedDateActionPlan11;
        private DevExpress.XtraReports.UI.XRLabel xrLblQADesigActionPlan;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRLabel xrLblHeadQANameActionPlan;
        private DevExpress.XtraReports.UI.XRTableCell xrHeadQACreatedDateActionPlan11;
        private DevExpress.XtraReports.UI.XRLabel xrLblHeadQADesigActionPlan;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLabel xrHodCreatedDateActionPlan;
        private DevExpress.XtraReports.UI.XRLabel xrQACreatedDateActionPlan;
        private DevExpress.XtraReports.UI.XRLabel xrHeadQACreatedDateActionPlan;
    }
}
