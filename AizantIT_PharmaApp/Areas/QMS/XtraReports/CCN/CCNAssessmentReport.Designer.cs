﻿namespace AizantIT_PharmaApp.Areas.QMS.XtraReports.CCN
{
    partial class CCNAssessmentReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrlblasshoddate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrltblCapa = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrlblassComments = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblDesignation = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblasshod = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblAssessmentHodname = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblCapaActionHeader = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblDeptName = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblAssessmentComments = new DevExpress.XtraReports.UI.XRLabel();
            this.xrtblccnAssessment = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.dsCCNAssessment1 = new AizantIT_PharmaApp.Areas.QMS.DataSets.CCN.dsCCNAssessment();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrlblDept = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblccnNumber = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.pbxCompanyLogo = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            ((System.ComponentModel.ISupportInitialize)(this.xrltblCapa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrtblccnAssessment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsCCNAssessment1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrlblasshoddate,
            this.xrltblCapa,
            this.xrlblassComments,
            this.xrlblDesignation,
            this.xrLabel5,
            this.xrlblasshod,
            this.xrlblAssessmentHodname,
            this.xrLabel3,
            this.xrlblCapaActionHeader,
            this.xrlblDeptName,
            this.xrlblAssessmentComments,
            this.xrtblccnAssessment});
            this.Detail.HeightF = 504.6252F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseBorders = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrlblasshoddate
            // 
            this.xrlblasshoddate.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlblasshoddate.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrlblasshoddate.LocationFloat = new DevExpress.Utils.PointFloat(594.1188F, 426.3336F);
            this.xrlblasshoddate.Multiline = true;
            this.xrlblasshoddate.Name = "xrlblasshoddate";
            this.xrlblasshoddate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblasshoddate.SizeF = new System.Drawing.SizeF(188.8812F, 27.16656F);
            this.xrlblasshoddate.StylePriority.UseBorders = false;
            this.xrlblasshoddate.StylePriority.UseFont = false;
            this.xrlblasshoddate.Text = "xrlblasshoddate";
            // 
            // xrltblCapa
            // 
            this.xrltblCapa.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrltblCapa.LocationFloat = new DevExpress.Utils.PointFloat(0F, 331.2501F);
            this.xrltblCapa.Name = "xrltblCapa";
            this.xrltblCapa.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrltblCapa.SizeF = new System.Drawing.SizeF(783F, 38.54169F);
            this.xrltblCapa.StylePriority.UseBorders = false;
            this.xrltblCapa.StylePriority.UseTextAlignment = false;
            this.xrltblCapa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell24.Multiline = true;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell24.StylePriority.UseBorders = false;
            this.xrTableCell24.StylePriority.UseFont = false;
            this.xrTableCell24.Weight = 6.5641339492792632D;
            // 
            // xrlblassComments
            // 
            this.xrlblassComments.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrlblassComments.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrlblassComments.LocationFloat = new DevExpress.Utils.PointFloat(0F, 164.5417F);
            this.xrlblassComments.Multiline = true;
            this.xrlblassComments.Name = "xrlblassComments";
            this.xrlblassComments.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblassComments.SizeF = new System.Drawing.SizeF(783F, 25.08336F);
            this.xrlblassComments.StylePriority.UseBorders = false;
            this.xrlblassComments.StylePriority.UseFont = false;
            this.xrlblassComments.Text = "Comments";
            // 
            // xrlblDesignation
            // 
            this.xrlblDesignation.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlblDesignation.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrlblDesignation.ForeColor = System.Drawing.Color.Black;
            this.xrlblDesignation.LocationFloat = new DevExpress.Utils.PointFloat(156.8811F, 426.3336F);
            this.xrlblDesignation.Multiline = true;
            this.xrlblDesignation.Name = "xrlblDesignation";
            this.xrlblDesignation.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblDesignation.SizeF = new System.Drawing.SizeF(437.2377F, 27.16656F);
            this.xrlblDesignation.StylePriority.UseBorders = false;
            this.xrlblDesignation.StylePriority.UseFont = false;
            this.xrlblDesignation.StylePriority.UseForeColor = false;
            this.xrlblDesignation.StylePriority.UseTextAlignment = false;
            this.xrlblDesignation.Text = "xrlblDesignation";
            this.xrlblDesignation.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(195.8519F, 0F);
            this.xrLabel5.Multiline = true;
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(335.3288F, 30.29167F);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Impact assessment of the Identified Departments";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrlblasshod
            // 
            this.xrlblasshod.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlblasshod.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrlblasshod.LocationFloat = new DevExpress.Utils.PointFloat(594.1188F, 403.3334F);
            this.xrlblasshod.Multiline = true;
            this.xrlblasshod.Name = "xrlblasshod";
            this.xrlblasshod.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblasshod.SizeF = new System.Drawing.SizeF(188.8812F, 23.00015F);
            this.xrlblasshod.StylePriority.UseBorders = false;
            this.xrlblasshod.StylePriority.UseFont = false;
            this.xrlblasshod.StylePriority.UseTextAlignment = false;
            this.xrlblasshod.Text = "Submitted Date";
            this.xrlblasshod.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrlblAssessmentHodname
            // 
            this.xrlblAssessmentHodname.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlblAssessmentHodname.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrlblAssessmentHodname.ForeColor = System.Drawing.Color.Blue;
            this.xrlblAssessmentHodname.LocationFloat = new DevExpress.Utils.PointFloat(156.8811F, 403.3334F);
            this.xrlblAssessmentHodname.Multiline = true;
            this.xrlblAssessmentHodname.Name = "xrlblAssessmentHodname";
            this.xrlblAssessmentHodname.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblAssessmentHodname.SizeF = new System.Drawing.SizeF(437.2377F, 23.00015F);
            this.xrlblAssessmentHodname.StylePriority.UseBorders = false;
            this.xrlblAssessmentHodname.StylePriority.UseFont = false;
            this.xrlblAssessmentHodname.StylePriority.UseForeColor = false;
            this.xrlblAssessmentHodname.StylePriority.UseTextAlignment = false;
            this.xrlblAssessmentHodname.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 403.3335F);
            this.xrLabel3.Multiline = true;
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(156.8811F, 23.00003F);
            this.xrLabel3.StylePriority.UseBorders = false;
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Submitted By (E-sign)";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrlblCapaActionHeader
            // 
            this.xrlblCapaActionHeader.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlblCapaActionHeader.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrlblCapaActionHeader.LocationFloat = new DevExpress.Utils.PointFloat(0F, 297.125F);
            this.xrlblCapaActionHeader.Multiline = true;
            this.xrlblCapaActionHeader.Name = "xrlblCapaActionHeader";
            this.xrlblCapaActionHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblCapaActionHeader.SizeF = new System.Drawing.SizeF(281.0513F, 23F);
            this.xrlblCapaActionHeader.StylePriority.UseBorders = false;
            this.xrlblCapaActionHeader.StylePriority.UseFont = false;
            this.xrlblCapaActionHeader.StylePriority.UseTextAlignment = false;
            this.xrlblCapaActionHeader.Text = "CAPA Details";
            this.xrlblCapaActionHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrlblDeptName
            // 
            this.xrlblDeptName.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlblDeptName.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrlblDeptName.LocationFloat = new DevExpress.Utils.PointFloat(0F, 60.66666F);
            this.xrlblDeptName.Multiline = true;
            this.xrlblDeptName.Name = "xrlblDeptName";
            this.xrlblDeptName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblDeptName.SizeF = new System.Drawing.SizeF(454.1667F, 28.20835F);
            this.xrlblDeptName.StylePriority.UseBorders = false;
            this.xrlblDeptName.StylePriority.UseFont = false;
            this.xrlblDeptName.StylePriority.UseTextAlignment = false;
            this.xrlblDeptName.Text = "xrlblDeptName";
            this.xrlblDeptName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrlblAssessmentComments
            // 
            this.xrlblAssessmentComments.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblAssessmentComments.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrlblAssessmentComments.LocationFloat = new DevExpress.Utils.PointFloat(0F, 189.625F);
            this.xrlblAssessmentComments.Multiline = true;
            this.xrlblAssessmentComments.Name = "xrlblAssessmentComments";
            this.xrlblAssessmentComments.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblAssessmentComments.SizeF = new System.Drawing.SizeF(783F, 82.3333F);
            this.xrlblAssessmentComments.StylePriority.UseBorders = false;
            this.xrlblAssessmentComments.StylePriority.UseFont = false;
            this.xrlblAssessmentComments.StylePriority.UseTextAlignment = false;
            this.xrlblAssessmentComments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrtblccnAssessment
            // 
            this.xrtblccnAssessment.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrtblccnAssessment.LocationFloat = new DevExpress.Utils.PointFloat(0F, 105.5F);
            this.xrtblccnAssessment.Name = "xrtblccnAssessment";
            this.xrtblccnAssessment.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrtblccnAssessment.SizeF = new System.Drawing.SizeF(783F, 32.29167F);
            this.xrtblccnAssessment.StylePriority.UseFont = false;
            this.xrtblccnAssessment.StylePriority.UseTextAlignment = false;
            this.xrtblccnAssessment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell27,
            this.xrTableCell28});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell25.Multiline = true;
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell25.StylePriority.UseFont = false;
            this.xrTableCell25.Text = "Impact Point";
            this.xrTableCell25.Weight = 1.0956323124999741D;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell26.Multiline = true;
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.Text = "Yes / No";
            this.xrTableCell26.Weight = 0.39748354441186268D;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell27.Multiline = true;
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell27.StylePriority.UseFont = false;
            this.xrTableCell27.Text = "Document Title or Doc Ref No";
            this.xrTableCell27.Weight = 1.1611010018302777D;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell28.Multiline = true;
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.StylePriority.UsePadding = false;
            this.xrTableCell28.Text = "Remarks";
            this.xrTableCell28.Weight = 1.2582968646247921D;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 1F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // dsCCNAssessment1
            // 
            this.dsCCNAssessment1.DataSetName = "dsCCNAssessment";
            this.dsCCNAssessment1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrlblDept,
            this.xrlblccnNumber,
            this.xrLabel19,
            this.xrLabel4,
            this.pbxCompanyLogo,
            this.xrLabel6});
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.PageHeader.HeightF = 113.223F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.StylePriority.UseFont = false;
            // 
            // xrlblDept
            // 
            this.xrlblDept.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrlblDept.LocationFloat = new DevExpress.Utils.PointFloat(547.2911F, 85.89462F);
            this.xrlblDept.Multiline = true;
            this.xrlblDept.Name = "xrlblDept";
            this.xrlblDept.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblDept.SizeF = new System.Drawing.SizeF(235.709F, 23F);
            this.xrlblDept.StylePriority.UseFont = false;
            this.xrlblDept.Text = "xrLabel9";
            // 
            // xrlblccnNumber
            // 
            this.xrlblccnNumber.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrlblccnNumber.LocationFloat = new DevExpress.Utils.PointFloat(102.8211F, 85.89462F);
            this.xrlblccnNumber.Multiline = true;
            this.xrlblccnNumber.Name = "xrlblccnNumber";
            this.xrlblccnNumber.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblccnNumber.SizeF = new System.Drawing.SizeF(194.0956F, 23F);
            this.xrlblccnNumber.StylePriority.UseFont = false;
            this.xrlblccnNumber.Text = "xrlblCCNnumber";
            // 
            // xrLabel19
            // 
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(445.5117F, 85.89462F);
            this.xrLabel19.Multiline = true;
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(101.7794F, 23F);
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.Text = "Department   :";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 85.89462F);
            this.xrLabel4.Multiline = true;
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(102.8211F, 23F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.Text = "CCN No      :";
            // 
            // pbxCompanyLogo
            // 
            this.pbxCompanyLogo.LocationFloat = new DevExpress.Utils.PointFloat(626.4174F, 10.00001F);
            this.pbxCompanyLogo.Name = "pbxCompanyLogo";
            this.pbxCompanyLogo.SizeF = new System.Drawing.SizeF(156.5826F, 42.79F);
            this.pbxCompanyLogo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(244.7918F, 41.29168F);
            this.xrLabel6.Multiline = true;
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(233.3332F, 23F);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.Text = "Change Control Note Report";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.HeightF = 52.12504F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(687.5417F, 9.999974F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(95.45831F, 23F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.TextFormatString = "page {0} of  {1}";
            // 
            // CCNAssessmentReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.PageFooter});
            this.ComponentStorage.AddRange(new System.ComponentModel.IComponent[] {
            this.dsCCNAssessment1});
            this.Margins = new System.Drawing.Printing.Margins(22, 22, 0, 1);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "18.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrltblCapa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrtblccnAssessment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsCCNAssessment1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DataSets.CCN.dsCCNAssessment dsCCNAssessment1;
        private DevExpress.XtraReports.UI.XRLabel xrlblAssessmentComments;
        private DevExpress.XtraReports.UI.XRTable xrtblccnAssessment;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRLabel xrlblDeptName;
        private DevExpress.XtraReports.UI.XRLabel xrlblCapaActionHeader;
        private DevExpress.XtraReports.UI.XRLabel xrlblasshod;
        private DevExpress.XtraReports.UI.XRLabel xrlblAssessmentHodname;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrlblDesignation;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRLabel xrlblDept;
        private DevExpress.XtraReports.UI.XRLabel xrlblccnNumber;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRPictureBox pbxCompanyLogo;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrlblassComments;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRTable xrltblCapa;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRLabel xrlblasshoddate;
    }
}
