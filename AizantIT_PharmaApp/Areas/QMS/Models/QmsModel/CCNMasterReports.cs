﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.Areas.QMS.Models.QmsModel
{
    public class CCNMasterReports
    {
        public string CCN_No { get; set; }

        public string CCN_No_Label { get; set; }
        public string Label_DepartmentName { get; set; }
        public string CCN_Date { get; set; }
        public string CategoryofChange { get; set; }
        public string DepartmentName { get; set; }
        public string DescriptionofProposedChanges { get; set; }
        public string ReasonforChange { get; set; }
        public string JustificationforChange { get; set; }
        public string ExistingProcedure { get; set; }

        public string DueDate { get; set; }
        public string ProjectNumber { get; set; }
        public string ItemCode { get; set; }
        public string MarketName { get; set; }
        public int DocumentType { get; set; }
        public string DocumentTypeName { get; set; }
        public string DocumentName { get; set; }
        public string DocumentNumber { get; set; }
        public string TypeOfChange { get; set; }
        public int currentStatus{get;set;}



        public string InitiatorName { get; set; }
        public string HODName { get; set; }
        public string InitiatorSubmitDate { get; set; }
        public string HODSubmitDate { get; set; }
        public string HodComments { get; set; }
        public string InitiatorDesignation { get; set; }
        public string HodDesignation { get; set; }
        public string ApplicableBatch { get; set; }
        public string ApplicableEquipment { get; set; }
        public string ApplicableTimeLine { get; set; }
    }
   
}