﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.Areas.QMS.Models.QmsModel
{
    public class DocumentViewerBO
    {
        private byte[] _fContent;
        public byte[] fContent
        {
            get
            {
                return _fContent;
            }
            set
            {
                _fContent = value;
            }
        }
        private DevExpress.XtraRichEdit.DocumentFormat _DFormat;
        public DevExpress.XtraRichEdit.DocumentFormat DFormat
        {
            get
            {
                return _DFormat;
            }
            set
            {
                _DFormat = value;
            }
        }
        private DevExpress.Spreadsheet.DocumentFormat _XLFormat;
        public DevExpress.Spreadsheet.DocumentFormat XLFormat
        {
            get
            {
                return _XLFormat;
            }
            set
            {
                _XLFormat = value;
            }
        }
        private string _UQID;
        public string UQID
        {
            get
            {
                return _UQID;
            }
            set
            {
                _UQID = value;
            }
        }
    }
}