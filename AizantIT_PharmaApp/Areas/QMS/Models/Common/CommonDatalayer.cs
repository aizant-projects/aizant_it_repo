﻿using QMS_BO;
using QMS_BO.QMS_CAPABO;
using QMS_BO.QMS_NoteTofile_BO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.Areas.QMS.Models.Common
{
    public class CommonDatalayer
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
        public DataSet GetQMSAndAuditOverDueData(int EmpId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.AizantIT_SP_QMS_DashBordDataForOverDueCard", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter ParamEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(ParamEmpID).Value = EmpId;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);

                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int QualityEventsCapaInsertDal(CapaModel capaModel, string action)
        {
            string query = "QMS.[AizantIT_SP_QualityEventsCapaCreation]";
            string targetdate12 = Convert.ToDateTime(capaModel.TargetDate).ToString("yyyy/MM/dd");
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.CommandType = CommandType.StoredProcedure;
            if (capaModel.CAPAID.ToString() == null)
            {
                SqlParameter deptid1 = new SqlParameter("@capaID", SqlDbType.Int);
                cmd.Parameters.Add(deptid1).Value = 0;
            }
            else
            {
                SqlParameter deptid1 = new SqlParameter("@capaID", SqlDbType.Int);
                cmd.Parameters.Add(deptid1).Value = capaModel.CAPAID;
            }

            SqlParameter deptid = new SqlParameter("@Department", SqlDbType.Int);
            cmd.Parameters.Add(deptid).Value = capaModel.Department;
            SqlParameter emplyeeID = new SqlParameter("@Responisble_Person", SqlDbType.Int);
            cmd.Parameters.Add(emplyeeID).Value = capaModel.EmployeeName;
            SqlParameter QualityEventID = new SqlParameter("@QualityEvent_TypeID", SqlDbType.Int);
            cmd.Parameters.Add(QualityEventID).Value = capaModel.QualityEvent_TypeID;
            SqlParameter QualityEventNumber = new SqlParameter("@QualityEvent_Number", SqlDbType.VarChar);
            cmd.Parameters.Add(QualityEventNumber).Value = capaModel.QualityEvent_Number.Trim();
            SqlParameter ActionPlanID = new SqlParameter("@PlanofAction", SqlDbType.Int);
            cmd.Parameters.Add(ActionPlanID).Value = capaModel.PlanofAction;
            SqlParameter targetdate = new SqlParameter("@TargetDate", SqlDbType.VarChar);
            cmd.Parameters.Add(targetdate).Value = targetdate12;
            SqlParameter Action_desc = new SqlParameter("@Capa_ActionDesc", SqlDbType.VarChar);
            cmd.Parameters.Add(Action_desc).Value = capaModel.Capa_ActionDesc.Trim();
            SqlParameter Createdby = new SqlParameter("@CreatedBy", SqlDbType.Int);
            cmd.Parameters.Add(Createdby).Value = Convert.ToInt32(capaModel.EmpID);
            SqlParameter Action1 = new SqlParameter("@action", SqlDbType.VarChar);
            cmd.Parameters.Add(Action1).Value = action;
            SqlParameter CapaQAEmployee = new SqlParameter("@CapaQAEmployee", SqlDbType.Int);
            cmd.Parameters.Add(CapaQAEmployee).Value = capaModel.CapaQAEmpId;
            SqlParameter AssessmentUniqueID = new SqlParameter("@AssessmentUniqueID", SqlDbType.Int);
            cmd.Parameters.Add(AssessmentUniqueID).Value = capaModel.AssessementUniqueID;
            SqlParameter EventsBAseID = new SqlParameter("@BaseID", SqlDbType.Int);
            cmd.Parameters.Add(EventsBAseID).Value = capaModel.EventsBaseID;
            SqlParameter HeadQAEmployeeID = new SqlParameter("@HeadQA_EmpID", SqlDbType.Int);
            cmd.Parameters.Add(HeadQAEmployeeID).Value = capaModel.HeadQa_EmpID;
            con.Open();
            int count = cmd.ExecuteNonQuery();
            con.Close();

            return count;
        }


    }
}