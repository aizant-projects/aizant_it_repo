﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.Areas.QMS.Models
{
    public class OverdueSticky
    {       
        public int Count { get; set; }
        public string CountFor { get; set; }
        public OverdueSticky(string _CountFor, int _Count)
        {
            CountFor = _CountFor;
            Count = _Count;
        }
    }
}