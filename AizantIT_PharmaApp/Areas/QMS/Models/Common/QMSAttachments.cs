﻿using System.IO;
using System.Web;
using iTextSharp.text.pdf;
using iTextSharp.text;
using DevExpress.Pdf;
using System;
using System.Collections.Generic;
using Aizant_API_Entities;

namespace AizantIT_PharmaApp.Areas.QMS.Models.Common
{
    public class QMSAttachments
    {
        public string SaveUploadedFileinPhysicalPath(HttpPostedFileBase file, string AttachmentType, int QualityEventID)
        {
            string TempAttachmentDir = "";
            string FileName = DateTime.Now.ToString("yyyMMddHHmmssfff") + ".pdf";
            string InsertText = "";
            if (QualityEventID == 1)//Incident
            {
                TempAttachmentDir = System.Web.HttpContext.Current.Server.MapPath("~\\Areas\\QMS\\Models\\Incident\\IncidentAttachments\\");
                InsertText =  AttachmentType + "/" + GetActualFileName(file.FileName);
            }
            else if (QualityEventID == 2)//CCN
            {
                TempAttachmentDir = System.Web.HttpContext.Current.Server.MapPath("~\\Areas\\QMS\\Models\\ChangeControl\\CCNAttachments\\");
                InsertText = AttachmentType + "/" + GetActualFileName(file.FileName);
            }
            else if (QualityEventID == 5)//CCN
            {
                TempAttachmentDir = System.Web.HttpContext.Current.Server.MapPath("~\\Areas\\QMS\\Models\\CAPA\\CAPAAttachments\\");
                InsertText = AttachmentType + "/" + GetActualFileName(file.FileName);
            }
            if (!Directory.Exists(TempAttachmentDir))
            {
                Directory.CreateDirectory(TempAttachmentDir);
            }
            string strFilePath = TempAttachmentDir + FileName;
            var br = new BinaryReader(file.InputStream);
            var reader = new PdfReader(br.ReadBytes(file.ContentLength));
            using (var fileStream = new FileStream(strFilePath, FileMode.Create, FileAccess.Write))
            {
                Rectangle size = reader.GetPageSizeWithRotation(1);
                var document = new Document(reader.GetPageSizeWithRotation(1));
                var writer = PdfWriter.GetInstance(document, fileStream);

                document.Open();

                for (var i = 1; i <= reader.NumberOfPages; i++)
                {
                    document.SetPageSize(reader.GetPageSize(i));
                    document.NewPage();
                    var rotation = reader.GetPageRotation(i);
                    var baseFont = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    var importedPage = writer.GetImportedPage(reader, i);
                    float currentPageHeight = importedPage.Height;
                    float currentPageWidth = importedPage.Width;
                    var contentByte = writer.DirectContent;
                    contentByte.AddTemplate(importedPage, 0, 0);
                    string tempInsertText = InsertText;
                    if (InsertText.Length > 170 && currentPageWidth < 596)
                    {
                        tempInsertText = InsertText.Remove(170) + "...";
                    }
                    else if (InsertText.Length > 210 && currentPageWidth > 596)
                    {
                        tempInsertText = InsertText.Remove(200) + "...";
                    }
                    contentByte.BeginText();
                    contentByte.SetFontAndSize(baseFont, 8);
                    contentByte.ShowTextAligned(Element.JPEG, tempInsertText, 10, currentPageHeight - 15, 0);
                    //contentByte.ShowTextAligned(1, InsertMessage, 200, 200, 0);
                    contentByte.EndText();
                }
                document.Close();
                writer.Close();
                // System.IO.File.Delete(FilePath);
                // System.IO.File.Move(TempFilePath, FilePath);
            }

            return strFilePath.Replace(System.Web.HttpContext.Current.Server.MapPath("~\\"), "");
        }
        private string GetActualFileName(string fileName)
        {
            return Path.GetFileName(fileName).Replace(Path.GetExtension(fileName), "");
        }
        public void RemovePhysicalPathFile(string FilePath)
        {
            if (System.IO.File.Exists(FilePath))
            {
                System.IO.File.Delete(FilePath);
            }
        }
        public string MergeAllTheFilesAsSingleFile(List<string> IndividualFilePaths, string MergedFileDirectory, string MergedFileName)
        {
            try
            {
                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~\\" + MergedFileDirectory)))
                {
                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~\\" + MergedFileDirectory));
                }
                string MergedFilePath = System.Web.HttpContext.Current.Server.MapPath("~\\" + MergedFileDirectory) + "\\" + MergedFileName;
                using (PdfDocumentProcessor pdfDocumentProcessor = new PdfDocumentProcessor())
                {
                    pdfDocumentProcessor.CreateEmptyDocument(MergedFilePath);
                    foreach (string IndividualFilePath in IndividualFilePaths)
                    {
                        if (System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath("~\\" + IndividualFilePath)))
                        {
                            pdfDocumentProcessor.AppendDocument(System.Web.HttpContext.Current.Server.MapPath("~\\" + IndividualFilePath));
                        }
                    }
                }
                return MergedFilePath.Replace(System.Web.HttpContext.Current.Server.MapPath("~\\"), "");
            }
            catch (Exception)
            {
                throw;
            }
        }


        
    }
}