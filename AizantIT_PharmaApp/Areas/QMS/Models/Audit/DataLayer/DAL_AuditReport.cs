﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace AizantIT_PharmaApp.Areas.QMS.Models.Audit.DataLayer
{
    public class DAL_AuditReport
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
        public DataSet GetAuditReportDetails(string AuditID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("QMS_Audit.AizantIT_SP_GetAuditReportDetailsForEdit", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter ParamAuditID = new SqlParameter("@AuditID", SqlDbType.Int);
                cmd.Parameters.Add(ParamAuditID).Value = AuditID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);

                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetAuditObservationDetails(string ObservationID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("QMS_Audit.AizantIT_SP_GetObservationDetailsForEdit", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter ParamObservationID = new SqlParameter("@ObservationID", SqlDbType.Int);
                cmd.Parameters.Add(ParamObservationID).Value = ObservationID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetAuditObservationDetailsForView(string ObservationID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("QMS_Audit.AizantIT_SP_GetObservationDetailsForEdit", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter ParamObservationID = new SqlParameter("@ObservationID", SqlDbType.Int);
                cmd.Parameters.Add(ParamObservationID).Value = ObservationID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //For Loading Audit Dashboard card details 
        public DataSet GetAuditDashBoardDetails(string EmpID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[QMS_Audit].[AizantIT_SP_GetAuditDataToQMS_Dashboard]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter ParamObservationID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(ParamObservationID).Value = Convert.ToInt32(EmpID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetAttachmentContent(int BaseType, int BaseID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[QMS_Audit].[AizantIT_SP_GetFileContent]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter ParamBaseID = new SqlParameter("@BaseID", SqlDbType.Int);
                cmd.Parameters.Add(ParamBaseID).Value = BaseID;
                SqlParameter ParamBaseType = new SqlParameter("@BaseType", SqlDbType.Int);
                cmd.Parameters.Add(ParamBaseType).Value = BaseType;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //For loading the AuditReportList
        public DataTable GetAuditReportList(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, string TypeOfList, int EmpID,
          int CategoryID, int CategoryTypeID, int AuditStatusID, int TypeOfAuditID, int RegulatoryAgencyID, int ClientID, int BusinessDivisionID, DataTable dtAuditSiteIDs, string FromAuditDate, string ToAuditDate)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[QMS_Audit].[AizantIT_SP_AuditReportList]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramTypeOfList = new SqlParameter("@TypeOfList", SqlDbType.VarChar);
                cmd.Parameters.Add(paramTypeOfList).Value = TypeOfList;
                SqlParameter ParamEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(ParamEmpID).Value = EmpID;
                SqlParameter ParamCategoryID = new SqlParameter("@Category", SqlDbType.Int);
                cmd.Parameters.Add(ParamCategoryID).Value = CategoryID;
                SqlParameter ParamCategoryTypeID = new SqlParameter("@CategoryType", SqlDbType.Int);
                cmd.Parameters.Add(ParamCategoryTypeID).Value = CategoryTypeID;
                SqlParameter paramAuditStatusID = new SqlParameter("@AuditStatusID", SqlDbType.Int);
                cmd.Parameters.Add(paramAuditStatusID).Value = AuditStatusID;
                SqlParameter ParamTypeOfAuditID = new SqlParameter("@TypeOfAudit", SqlDbType.Int);
                cmd.Parameters.Add(ParamTypeOfAuditID).Value = TypeOfAuditID;
                SqlParameter ParamRegulatoryAgencyID = new SqlParameter("@RegulatoryAgency", SqlDbType.Int);
                cmd.Parameters.Add(ParamRegulatoryAgencyID).Value = RegulatoryAgencyID;
                SqlParameter ParamClientID = new SqlParameter("@Client", SqlDbType.Int);
                cmd.Parameters.Add(ParamClientID).Value = ClientID;
                SqlParameter ParamBusinessDivisionID = new SqlParameter("@BusinessDivision", SqlDbType.Int);
                cmd.Parameters.Add(ParamBusinessDivisionID).Value = BusinessDivisionID;
                cmd.Parameters.AddWithValue("@Sites", dtAuditSiteIDs);

                SqlParameter ParamFromAuditDate = new SqlParameter("@FromDate", SqlDbType.DateTime);
                if (FromAuditDate != "")
                {
                    cmd.Parameters.Add(ParamFromAuditDate).Value = Convert.ToDateTime(FromAuditDate);
                }
                else
                {
                    cmd.Parameters.Add(ParamFromAuditDate).Value = DBNull.Value;
                }
                SqlParameter ParamToAuditDate = new SqlParameter("@ToDate", SqlDbType.DateTime);
                if (ToAuditDate != "")
                {
                    cmd.Parameters.Add(ParamToAuditDate).Value = Convert.ToDateTime(ToAuditDate);
                }
                else
                {
                    cmd.Parameters.Add(ParamToAuditDate).Value = DBNull.Value;
                }
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetObservationReportList(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch,
            string TypeOfObservationList, int EmpID,bool HasOnlyViewerRole,int AuditID,
         int DepartmentID, int SiteID,int CategoryID, int ObservationCurrentStatusID,// DataTable dtAuditSiteIDs, 
         string FromObservationDueDate, string ToObservationDueDate)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[QMS_Audit].[AizantIT_SP_ObservationReportList]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
                SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
                cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
                SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
                cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
                SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
                SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
                cmd.Parameters.Add(paramSearchString).Value = sSearch;
                SqlParameter paramTypeOfObservationList = new SqlParameter("@TypeOfObservationList", SqlDbType.VarChar);
                cmd.Parameters.Add(paramTypeOfObservationList).Value = TypeOfObservationList;
                SqlParameter ParamEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(ParamEmpID).Value = EmpID;
                SqlParameter ParamHasOnlyViewerRole = new SqlParameter("@HasOnlyViewerRole", SqlDbType.Bit);
                cmd.Parameters.Add(ParamHasOnlyViewerRole).Value = HasOnlyViewerRole;
                SqlParameter ParamAuditID = new SqlParameter("@AuditID", SqlDbType.Int);
                cmd.Parameters.Add(ParamAuditID).Value = AuditID;
                SqlParameter ParamDepartmentID = new SqlParameter("@DepartmentID", SqlDbType.Int);
                cmd.Parameters.Add(ParamDepartmentID).Value = DepartmentID;
                SqlParameter ParamSiteID = new SqlParameter("@SiteID", SqlDbType.Int);
                cmd.Parameters.Add(ParamSiteID).Value = SiteID;
                SqlParameter ParamObservationCategoryID = new SqlParameter("@ObservationCategoryID", SqlDbType.Int);
                cmd.Parameters.Add(ParamObservationCategoryID).Value = CategoryID;
                SqlParameter paramObservationCurrentStatusID = new SqlParameter("@ObservationCurrentStatusID", SqlDbType.Int);
                cmd.Parameters.Add(paramObservationCurrentStatusID).Value = ObservationCurrentStatusID;
                SqlParameter ParamFromObservationDueDate = new SqlParameter("@FromObservationDueDate", SqlDbType.DateTime);
                if (FromObservationDueDate != "")
                {
                    cmd.Parameters.Add(ParamFromObservationDueDate).Value = Convert.ToDateTime(FromObservationDueDate);
                }
                else
                {
                    cmd.Parameters.Add(ParamFromObservationDueDate).Value = DBNull.Value;
                }
                SqlParameter ParamToObservationDueDate = new SqlParameter("@ToObservationDueDate", SqlDbType.DateTime);
                if (ToObservationDueDate != "")
                {
                    cmd.Parameters.Add(ParamToObservationDueDate).Value = Convert.ToDateTime(ToObservationDueDate);
                }
                else
                {
                    cmd.Parameters.Add(ParamToObservationDueDate).Value = DBNull.Value;
                }
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetAuditDashBoardCharts(int BaseTypeID, string RoleType, int EmpID = 0)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[QMS_Audit].[AizantIT_SP_GetAuditDataToQMS_Charts]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter ParamBaseTypeID = new SqlParameter("@BaseTypeID", SqlDbType.Int);
                cmd.Parameters.Add(ParamBaseTypeID).Value = BaseTypeID;
                SqlParameter paramRoleType = new SqlParameter("@RoleType", SqlDbType.VarChar);
                cmd.Parameters.Add(paramRoleType).Value = RoleType;
                SqlParameter ParamEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(ParamEmpID).Value = EmpID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetDataForAR_Report(int AuditID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[QMS_Audit].[AizantIT_SP_AuditReport_ReportData]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter spmAuditID = new SqlParameter("@AuditID", SqlDbType.Int);
                cmd.Parameters.Add(spmAuditID).Value = AuditID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetAuditList(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch)
        {
            SqlCommand cmd = new SqlCommand("[QMS_Audit].[AizantIT_SP_ReAssignAuditList]", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
            cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
            SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
            cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
            SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
            cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
            SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
            cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
            SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
            cmd.Parameters.Add(paramSearchString).Value = sSearch;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            return dt;
        }

        public DataTable GetAuditWiseOpenObservationList(int AuditID, int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch)
        {
            SqlCommand cmd = new SqlCommand("[QMS_Audit].[AizantIT_SP_GetAuditWiseOpenObservationList]", con);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter paramAuditID = new SqlParameter("@AuditID", SqlDbType.Int);
            cmd.Parameters.Add(paramAuditID).Value = AuditID;
            SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
            cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
            SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
            cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
            SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
            cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
            SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
            cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
            SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
            cmd.Parameters.Add(paramSearchString).Value = sSearch;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public void ModifiedReAssignObservationResponsePerson(int ResponsiblePersonID, string Comments, int AssignedObservationID, int ObservationID, int ActionBy, out int ObservationStatus)
        {
            SqlCommand cmd = new SqlCommand("[QMS_Audit].[AizantIT_SP_ModifiedReAssignObservationResponsePerson]", con);
            con.Open();
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter SPResponsiblePersonID = new SqlParameter("@ResponsiblePersonID", SqlDbType.Int);
            SqlParameter SPComments = new SqlParameter("@Comments", SqlDbType.VarChar);
            SqlParameter SPAssignedObservationID = new SqlParameter("@AssignedObservationID", SqlDbType.Int);
            SqlParameter SPObservationID = new SqlParameter("@ObservationID", SqlDbType.Int);
            SqlParameter SPActionBy = new SqlParameter("@ActionBy", SqlDbType.Int);
            SqlParameter SPObservationStatus = cmd.Parameters.Add("@ObservationStatus", SqlDbType.Int);

            cmd.Parameters.Add(SPResponsiblePersonID).Value = ResponsiblePersonID;
            cmd.Parameters.Add(SPComments).Value = Comments;
            cmd.Parameters.Add(SPAssignedObservationID).Value = AssignedObservationID;
            cmd.Parameters.Add(SPObservationID).Value = ObservationID;
            cmd.Parameters.Add(SPActionBy).Value = ActionBy;
            SPObservationStatus.Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();

            string result = cmd.Parameters["@ObservationStatus"].Value.ToString();
            ObservationStatus = Convert.ToInt32(result);
            cmd.Dispose();
        }

        public void ModifiedReAssignAuditUsers(int AuthorID,
            int ApproverID,
            int AuditID,
            string Comments,
            int ActionBy,
           out int @ActionResult
         )

        {
            SqlCommand cmd = new SqlCommand("[QMS_Audit].[AizantIT_SP_ModifiedReAssignAuditUsers]", con);
            con.Open();
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter SPAuthorID = new SqlParameter("@AuthorID", SqlDbType.Int);
            SqlParameter SPApproverID = new SqlParameter("@ApproverID", SqlDbType.Int);
            SqlParameter SPAuditID = new SqlParameter("@AuditID", SqlDbType.Int);
            SqlParameter SPComments = new SqlParameter("@Comments", SqlDbType.VarChar);
            SqlParameter SPActionBy = new SqlParameter("@ActionBy", SqlDbType.Int);
            SqlParameter SPActionResult = cmd.Parameters.Add("@ActionResult", SqlDbType.Int);
            

            cmd.Parameters.Add(SPAuthorID).Value = AuthorID;
            cmd.Parameters.Add(SPApproverID).Value = ApproverID;
            cmd.Parameters.Add(SPAuditID).Value = AuditID;
            cmd.Parameters.Add(SPComments).Value = Comments;
            cmd.Parameters.Add(SPActionBy).Value = ActionBy;

            SPActionResult.Direction = ParameterDirection.Output;
           
            cmd.ExecuteNonQuery();
            ActionResult = Convert.ToInt32(cmd.Parameters["@ActionResult"].Value.ToString());
            cmd.Dispose();
        }

        public DataTable GetAuditResponseReportList(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch,int AuditID)
        {
            SqlCommand cmd = new SqlCommand("[QMS_Audit].[AizantIT_SP_AuditResponseReportList]", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter paramDisplayLength = new SqlParameter("@DisplayLength", SqlDbType.Int);
            cmd.Parameters.Add(paramDisplayLength).Value = iDisplayLength;
            SqlParameter paramDisplayStart = new SqlParameter("@DisplayStart", SqlDbType.Int);
            cmd.Parameters.Add(paramDisplayStart).Value = iDisplayStart;
            SqlParameter paramSortCol = new SqlParameter("@SortCol", SqlDbType.Int);
            cmd.Parameters.Add(paramSortCol).Value = iSortCol_0;
            SqlParameter paramSortDir = new SqlParameter("@SortDir", SqlDbType.VarChar);
            cmd.Parameters.Add(paramSortDir).Value = sSortDir_0;
            SqlParameter paramSearchString = new SqlParameter("@Search", SqlDbType.VarChar);
            cmd.Parameters.Add(paramSearchString).Value = sSearch;
            SqlParameter paramAuditID = new SqlParameter("@AuditID", SqlDbType.Int);
            cmd.Parameters.Add(paramAuditID).Value = AuditID;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            return dt;
        }

    }
}