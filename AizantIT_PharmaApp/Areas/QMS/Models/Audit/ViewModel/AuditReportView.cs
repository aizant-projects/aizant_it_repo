﻿namespace AizantIT_PharmaApp.Areas.QMS.Models.Audit.ViewModel
{
    public class AuditReportView
    {
        public int AuditID { get; set; }
        public int ActionStatusID { get; set; }
        public string AuditNum { get; set; }
        public string Category { get; set; }
        public string CategoryType { get; set; }
        public string TypeOfName { get; set; }
        public string BusinessDivision { get; set; }
        public string Sites { get; set; }
        public string ClientName { get; set; }
        //public string CountryName { get; set; }
        public string RegionName { get; set; }
        public string RegulatoryAgency { get; set; }
        public string NatureOfInspection { get; set; }
        public string AuditFrom { get; set; }
        public string AuditTo { get; set; }
        public string AuditedDates { get; set; }
        public string AuditReportedDate { get; set; }
        public string AuditDueDate { get; set; }
        public string AuditStatus { get; set; }
        public int AuditCurrentStatusID { get; set; } 
        public string Auditors { get; set; }
        public string Observers { get; set; }
        public string CertifiedDate { get; set; }
        public string CertificateValidUntil { get; set; }
        public string AuthorName { get; set; }
        public string ApproverName { get; set; }
        public string ObservationNameInSearchBox { get; set; }
        public string IsApproved { get; set; }
        public int CountOFAuditResponseReport { get; set; }
        public string ShowARR_Button { get; set; }

        public AuditReportView(AuditReportView obj)
        {
            AuditID = obj.AuditID;
            ActionStatusID = obj.AuditID;
            AuditNum = obj.AuditNum;
            Category = obj.Category;
            CategoryType = obj.CategoryType;
            TypeOfName = obj.TypeOfName;
            BusinessDivision = obj.BusinessDivision;
            Sites = obj.Sites;
            ClientName = obj.ClientName;
            // CountryName = obj.CountryName;
            RegionName = obj.RegionName;
            RegulatoryAgency = obj.RegulatoryAgency;
            NatureOfInspection = obj.NatureOfInspection;
            AuditFrom = obj.AuditFrom;
            AuditTo = obj.AuditTo;
            AuditReportedDate = obj.AuditReportedDate;
            AuditDueDate = obj.AuditDueDate;
            AuditStatus = obj.AuditStatus;
            Auditors = obj.Auditors;
            CertifiedDate = obj.CertifiedDate;
            CertificateValidUntil = obj.CertificateValidUntil;
            AuthorName = obj.AuthorName;
            ApproverName = obj.ApproverName;
        }
        public AuditReportView()
        { }
    }

    //public class AuditorSelectList
    //{
    //    public int Value { get; set; }
    //    public string Text { get; set; }

    //    public AuditorSelectList(string _Text, int _Value)
    //    {
    //        Text = _Text;
    //        Value = _Value;
    //    }
    //}
    //public class SitesSelectedList
    //{
    //    public int Value { get; set; }
    //    public string Text { get; set; }
    //    public SitesSelectedList(string _Text, int _Value)
    //    {
    //        Text = _Text;
    //        Value = _Value;
    //    }
    //}
}