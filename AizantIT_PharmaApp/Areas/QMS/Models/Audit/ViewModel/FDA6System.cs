﻿namespace AizantIT_PharmaApp.Areas.QMS.Models.Audit.ViewModel
{
    public class FDA6System
    {
        public int FDA6SystemID { get; set; }
        public int[] ToAddFDA6SystemAreas { get; set; }
        public int[] ToRemoveFDA6SystemAreas { get; set; }
    }
    public class FDA6SystemHistoryList
    {
        public int FDA_SystemHistoryID { get; set; }
        public int RowNumber { get; set; }
        public string ActionByName { get; set; }
        public string RoleName { get; set; }
        public string ActionDate { get; set; }
        public string ActionName { get; set; }
        public string Remarks { get; set; }
        public FDA6SystemHistoryList(int _FDA_SystemHistoryID,int _RowNumber, string _ActionBy, string _ActionRole, string _ActionDate, string _ActionStatus, string _Comments)
        {
            FDA_SystemHistoryID = _FDA_SystemHistoryID;
            RowNumber = _RowNumber;
            ActionByName = _ActionBy;
            RoleName = _ActionRole;
            ActionDate = _ActionDate;
            ActionName = _ActionStatus;
            Remarks = _Comments;
        }
        public FDA6SystemHistoryList() { }
    }
}