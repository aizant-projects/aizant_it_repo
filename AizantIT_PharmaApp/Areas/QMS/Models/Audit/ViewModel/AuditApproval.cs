﻿namespace AizantIT_PharmaApp.Areas.QMS.Models.Audit.ViewModel
{
    public class AuditApproval
    {
        public int AuditID { get; set; }
        public string ApprovalType { get; set; }
        public string Comments { get; set; }
    }
}