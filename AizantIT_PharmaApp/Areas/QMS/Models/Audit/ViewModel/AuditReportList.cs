﻿using System;

namespace AizantIT_PharmaApp.Areas.QMS.Models.Audit.ViewModel
{
    public class AuditReportList
    {
        public int AuditID { get; set; }
        public string AuditNum { get; set; }
        public string Category { get; set; }
        public string CategoryType { get; set; }
        public string BusinessDivision { get; set; }
        //public string AuditFrom { get; set; }
        //public string AuditTo { get; set; }
        public int NoOfAuditDays { get; set; }
        public string AuditDates { get; set; }
        public string ReportedDate { get; set; }
        public string AuditStatus { get; set; }
        public string IsPublic { get; set; }
        public int ActionStatusID { get; set; }
        public string  StatusName { get; set; }
        public bool IsAtleastOneResponseApproveforEveryObservation { get; set; }
        public AuditReportList(int _AuditID,string _AuditNum,string _Category,
            string _CategoryType, string _BusinessDivision,//string _AuditFrom,string _AuditTo,
          int _NoOfAuditDays, string  _AuditDates,string _ReportedDate,string _AuditStatus,string _IsPublic, int _ActionStatusID,bool _IsAtleastOneResponseApproveforEveryObservation)
        {
            AuditID = _AuditID;
            AuditNum = _AuditNum;
            Category = _Category;
            CategoryType = _CategoryType;
            BusinessDivision = _BusinessDivision;
            //AuditFrom = _AuditFrom;
            //AuditTo = _AuditTo;
            NoOfAuditDays = _NoOfAuditDays;
            AuditDates = _AuditDates;
            ReportedDate = _ReportedDate;
            AuditStatus = _AuditStatus;
            IsPublic = _IsPublic;
            ActionStatusID = _ActionStatusID;
            IsAtleastOneResponseApproveforEveryObservation = _IsAtleastOneResponseApproveforEveryObservation;
        }

        public AuditReportList(int _AuditID, string _AuditNum, string _Category,
           string _CategoryType,string _Status)
        {
            AuditID = _AuditID;
            AuditNum = _AuditNum;
            Category = _Category;
            CategoryType = _CategoryType;
            StatusName = _Status;
        }

      
    }

    public class AuditAttachmentList
    {
        public int AttachmentID { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public string Type { get; set; }
        public string FileExtention { get; set; }
        public string AttachmentDate { get; set; }
        public bool CanDelete { get; set; }

        public AuditAttachmentList(int _AttachmentID,string _FilePath, string _FileName, string _Type,
            string _FileExtention, string _AttachmentDate, bool _CanDelete)
        {
            AttachmentID = _AttachmentID;
            FilePath = _FilePath;
            FileName = _FileName;
            Type = _Type;
            FileExtention = _FileExtention;
            AttachmentDate = _AttachmentDate;
            CanDelete = _CanDelete;
        }

        public AuditAttachmentList(int _AttachmentID, string _FilePath, string _FileName,
            string _FileExtention, string _AttachmentDate, bool _CanDelete)
        {
            AttachmentID = _AttachmentID;
            FilePath = _FilePath;
            FileName = _FileName;          
            FileExtention = _FileExtention;
            AttachmentDate = _AttachmentDate;
            CanDelete= _CanDelete;
        }
    }
    public class AuditObservationReportList
    {
        public int AuditID { get; set; }
        public int ObservationID { get; set; }
        public string ObservationNum { get; set; }
        public int DeptID { get; set; }
        public String DepartmentName { get; set; }
        public int EmpID { get; set; }
        public string ResponsiblePersonName { get; set; }
        public int AssignedObservationID { get; set; }

        public AuditObservationReportList(int _AuditID, int _ObservationID, string _ObservationNum,
        int _DeptID,string _DepartmentName, int _EmpID,string _ResponsiblePersonName, int _AssignedObservationID)
        {
            AuditID = _AuditID;
            ObservationID = _ObservationID;
            ObservationNum = _ObservationNum;
            DeptID = _DeptID;
            DepartmentName = _DepartmentName;
            EmpID = _EmpID;
            ResponsiblePersonName = _ResponsiblePersonName;
            AssignedObservationID = _AssignedObservationID;
        }
    }

    public class AuditReAssignReportList
    {
        public int AuditID { get; set; }
        public string AuditNum { get; set; }
        public string Category { get; set; }
        public string CategoryType { get; set; }
        public string AuditStatusType { get; set; }
        public int StatusID { get; set; }
        public string StatusName { get; set; }
        public int AuthorID { get; set; }
        public string AuthorName { get; set; }
        public int ApproverID { get; set; }
        public string ApproverName { get; set; }

      public  AuditReAssignReportList(int _AuditID,string _AuditNum,string _Category,
          string _CategoryType, string _AuditStatusType,string _StatusName,string _AuthorName, string _ApproverName,int _AuthorID,int _ApproverID)
        {
            AuditID = _AuditID;
            AuditNum = _AuditNum;
            Category = _Category;
            CategoryType = _CategoryType;
            AuditStatusType = _AuditStatusType;
            StatusName = _StatusName;
            AuthorName = _AuthorName;
            ApproverName = _ApproverName;
            AuthorID = _AuthorID;
            ApproverID = _ApproverID;


        }

    }


    public class AuditResponseReportList
    {
        public int AuditResponseReportID { get; set; }
        public int AuditID { get; set; }
        public int ResponseVersion { get; set; }
        public string GeneratedBy { get; set; }
        public string GeneratedDate { get; set; }

        public string FilePath { get; set; }

        public AuditResponseReportList(int _AuditID,int _ResponseVersion,string _GeneratedBy,string _GeneratedDate,string _FilePath)
        {
            AuditID = _AuditID;
            ResponseVersion = _ResponseVersion;
            GeneratedBy = _GeneratedBy;
            GeneratedDate = _GeneratedDate;
            FilePath = _FilePath;
        }
    }
}