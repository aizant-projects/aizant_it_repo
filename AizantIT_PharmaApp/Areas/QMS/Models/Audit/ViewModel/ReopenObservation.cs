﻿namespace AizantIT_PharmaApp.Areas.QMS.Models.Audit.ViewModel
{
    public class ReopenObservation
    {
        public int ObservationID { get; set; }
        public int ResponsibleEmpID { get; set; }
        public string ResponseDueDate { get; set; }
        public string QueryDate { get; set; }      
        public string AuditorQuery { get; set; }
    }
}