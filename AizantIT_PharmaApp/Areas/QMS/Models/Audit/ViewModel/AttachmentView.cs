﻿namespace AizantIT_PharmaApp.Areas.QMS.Models.Audit.ViewModel
{
    public class AttachmentView
    {
        public string DocID { get; set; }
        public DevExpress.XtraRichEdit.DocumentFormat DocumentFormat { get; set; }
        public byte[] FileContent { get; set; }
        
    }
}