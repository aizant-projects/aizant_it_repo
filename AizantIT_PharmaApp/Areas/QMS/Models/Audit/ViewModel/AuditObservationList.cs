﻿namespace AizantIT_PharmaApp.Areas.QMS.Models.Audit.ViewModel
{
    public class AuditObservationList
    {
        public int ObservationID { get; set; }
        public string ObservationNum { get; set; }
        public string Site { get; set; }
        public string DeptName { get; set; } 
        public string ObservationDesc { get; set; }
        public string ObservationStatus { get; set; }
        public string ObservationCategory { get; set; }
        public string ActionStatus { get; set; }
        public int ActionStatusID { get; set; }
        public string DueDate { get; set; }

        public AuditObservationList(int _ObservationID, string _ObservationNum, string _Site, string _DeptName, string _ObservationDesc,
           string _ObservationStatus,string _ObservationCategory,string _ActionStatus,int _ActionStatusID,string _DueDate)
        {
            ObservationID=_ObservationID;
            ObservationNum = _ObservationNum;
            Site = _Site;
            DeptName = _DeptName;
            ObservationDesc = _ObservationDesc;
            ObservationStatus = _ObservationStatus;
            ObservationCategory = _ObservationCategory;
            ActionStatus = _ActionStatus;
            ActionStatusID = _ActionStatusID;
            DueDate = _DueDate;
         }
        
    }
    public class ObservationListForHODandQA
    {
        public int AuditID { get; set; }
        public int ObservationID { get; set; }
        public string AuditNumber { get; set; }
        public string ObservationNum { get; set; }
        public string Site { get; set; }
        public string DeptName { get; set; }
        public string ObservationCategory { get; set; }
        public string DueDate { get; set; }
        public string ActionStatus { get; set; }
        public int ActionStatusID { get; set; }
        public int ObservationStatusID { get; set; }


        public ObservationListForHODandQA(int _AuditID, int _ObservationID, string _AuditNumber, string _ObservationNum, string _Site, string _DeptName,
            string _ObservationCategory,string _DueDate, string _ActionStatus, int _ActionStatusID, int _ObservationStatusID)
        {
            AuditID = _AuditID;
            ObservationID = _ObservationID;
            AuditNumber = _AuditNumber;
            ObservationNum = _ObservationNum;
            Site = _Site;
            DeptName = _DeptName;
            ObservationCategory = _ObservationCategory;
            DueDate = _DueDate;
            ActionStatus = _ActionStatus;
            ActionStatusID = _ActionStatusID;
            ObservationStatusID = _ObservationStatusID;
        }
    }
    public class DueDateObservationListForQA
    {
        public int AuditID { get; set; }
        public int ObservationID { get; set; }
        public string AuditNumber { get; set; }
        public string AuditAuthor { get; set; }
        public string ObservationNum { get; set; }
        public string Site { get; set; }
        public string DeptName { get; set; }
        public string ObservationCategory { get; set; }
        public string DueDate { get; set; }
        public string ActionStatus { get; set; }
        public int ActionStatusID { get; set; }

        public DueDateObservationListForQA(int _AuditID, int _ObservationID, string _AuditNumber,string _AuditAuthor, string _ObservationNum, string _Site, string _DeptName,
            string _ObservationCategory, string _DueDate, string _ActionStatus, int _ActionStatusID)
        {
            AuditID = _AuditID;
            ObservationID = _ObservationID;
            AuditNumber = _AuditNumber;
            AuditAuthor = _AuditAuthor;
            ObservationNum = _ObservationNum;
            Site = _Site;
            DeptName = _DeptName;
            ObservationCategory = _ObservationCategory;
            DueDate = _DueDate;
            ActionStatus = _ActionStatus;
            ActionStatusID = _ActionStatusID;
        }
    }
    public class ObservationQueryList
    {
        public int AssignedObservationID { get; set; }
        public int AuditorQueryID { get; set; }
        public string AuditorQuery { get; set; }
        public string QueryDate { get; set; }       

        public ObservationQueryList(int _AssignedObservationID, int _AuditorQueryID, string _AuditorQuery, string _QueryDate)
        {
            AssignedObservationID = _AssignedObservationID;
            AuditorQueryID = _AuditorQueryID;
            AuditorQuery = _AuditorQuery;
            QueryDate = _QueryDate;           
        }
    }
}
    