﻿namespace AizantIT_PharmaApp.Areas.QMS.Models.Audit.ViewModel
{
    public class ObserveMaster
    {
        public int ObserverID { get; set; }
        public int ObserverCategoryID { get; set; }
        public int ObserverBaseID { get; set; }
        public string ObserverName { get; set; }
        public string ObserverEmailID { get; set; }
        public string ObserverContactNo { get; set; }
        public string ObserverAdditionalDetails { get; set; }
    }
}