﻿using QMS_BO.QMS_CAPABO;
using System.Collections.Generic;
using System.Web.Mvc;

namespace AizantIT_PharmaApp.Areas.QMS.Models.Audit.ViewModel
{
    public class AuditCreate
    {
        public string AuditActionCreateOrUpdate { get; set; }
        public int AuditCurrentStatusID { get; set; }
        public int AuditID { get; set; }
        public int CategoryID { get; set; }
        public int CategoryTypeID { get; set; }
        public int TypeOfID { get; set; }
        public string AuditReportNumber { get; set; }
        public int BusinessDivisionID { get; set; }
        public string[] AuditSiteIDs { get; set; }
        public string AuditDates { get; set; }
        public string AuditFromDate { get; set; }
        public string AuditToDate { get; set; }
        public string AuditReportDate { get; set; }
        public string AuditDueDate { get; set; }
        public string AuditCertificateDate { get; set; }
        public string AuditCertificateValidUntilDate { get; set; }
        public int StatusID { get; set; }
        public string[] AuditorIDs { get; set; }
        public string[] ObserverIDs { get; set; }
        //public int RegulatoryAgencyCountryID { get; set; }
        public int RegulatoryAgencyID { get; set; }
        public int NatureOfInspectionID { get; set; }
        public int ClientID { get; set; }
        public int ApproverID { get; set; }
        public string AuditComments { get; set; }
        public int AuditCreationStatus { get; set; }

        public List<SelectListItem> CategoryList { get; set; }
        public List<SelectListItem> CategoryTypeList { get; set; }
        public List<SelectListItem> TypeOfList { get; set; }
        public List<SelectListItem> ClientList { get; set; }
        public List<SelectListItem> RegulatoryAgencyCountryList { get; set; }
        public List<SelectListItem> RegulatoryAgencyList { get; set; }
        public List<SelectListItem> BusinessDivisionList { get; set; }
        public List<SelectListItem> SiteList { get; set; }
        public List<SelectListItem> AuditorList { get; set; }
        public List<SelectListItem> ObserverList { get; set; }
        public List<SelectListItem> ApproversList { get; set; }

        public List<SelectListItem> ObservationSiteList { get; set; }
        public List<SelectListItem> ObservationSiteDepartments { get; set; }
        public List<SelectListItem> ObservationResponsiblePerson { get; set; }


        public List<SelectListItem> AuditQaList { get; set; }
        public bool IsRecordExist { get; set; }
        public bool IsValidAuthor { get; set; }

        public string AuditAuthorName { get; set; }
        public string AuditApproverName { get; set; }
        public int DepartmentID { get; set; }
        public int AssignedObservationID { get; set; }
        public int ResponsiblePersonID { get; set; }
        public int AuthorId { get; set; }

    }
    public class AuditSelectList
    {
        public int Value { get; set; }
        public string Text { get; set; }
        public bool Selected { get; set; }
        public AuditSelectList(string _Text, int _Value, bool _Selected = false)
        {
            Text = _Text;
            Value = _Value;
            Selected = Selected;
        }
    }
    public class AuditObservation
    {
        public int AuditID { get; set; }
        public int AuditStatus { get; set; }
        public int AuditCurrentStatus { get; set; } //HistoryStatus
        public int ObservationID { get; set; }
        public int ObservationCurrentStatus { get; set; }
        public string ObservationNumber { get; set; }
        public string AuditNuber { get; set; }
        public int ObservationSiteID { get; set; }
        public string ObservationSiteName { get; set; }
        public int ObservationDeptID { get; set; }
        public string ObservationDepartmentName { get; set; }
        public int ObservationResponsiblePersonID { get; set; }
        public string ObservationDueDate { get; set; }
        public bool IsResponsiblePersonEntry { get; set; }
        public string ResponsiblePersonName { get; set; }
        public string ObservationDescription { get; set; }
        public int ObservationPriorityID { get; set; }
        public string ObservationPriorityName { get; set; }
        public List<ObservationSystem> ObservationSystems { get; set; }
        public int ObservationStatusID { get; set; }
        public string IsResponseStatusClosed { get; set; }
        public int ObservationResponseStatusID { get; set; }
        public int ObservationResponseActionStatusID { get; set; }
        public string ObservationComents { get; set; }
        public string LastResponseDate { get; set; }
        public List<SelectListItem> SiteList { get; set; }
        public List<SelectListItem> SiteDeptList { get; set; }
        public List<SelectListItem> ResponsiblePersonList { get; set; }
        public List<SelectListItem> AreaList { get; set; }
        public List<SelectListItem> SubAreaList { get; set; }
        public int AssignedObservationID { get; set; }
        public string IsObservationReOpen { get; set; }
        public string AuthorName { get; set; }
        public int MLDataInfo { get; set; }
        
    }
    public class ObservationSystem
    {
        public int ObservationOnSystemID { get; set; }
        public int FDA_SystemID { get; set; }
        public string FDA_SystemName { get; set; }
        public int AreaID { get; set; }
        public string AreaName { get; set; }
        public int SubAreaID { get; set; }
        public string SubAreaName { get; set; }
        public string Remarks { get; set; }
        public List<SelectListItem> FDA6SystemList { get; set; }
        public List<SelectListItem> AreaList { get; set; }
        public List<SelectListItem> SubAreaList { get; set; }
        public List<ObservationSystemList> ObservationSystemListData { get; set; }
       
    }
    public class ObservationSystemList
    {
        public int ObservationOnSystemID { get; set; }
        public int FDA_SystemID { get; set; }
        public string FDA_SystemName { get; set; }
        public int AreaID { get; set; }
        public string AreaName { get; set; }
        public int SubAreaID { get; set; }
        public string SubAreaName { get; set; }
        public string Remarks { get; set; }

    }
    public class ResponseList
    {
        public int ResponseID { get; set; }
        public int ObservationID { get; set; }
        public int AuditStatusID { get; set; }
        public string ResponseDueDate { get; set; }
        public bool IsResponsibleEmpEntry { get; set; }
        public int ResponsibleEmpID { get; set; }
        public string ResponsibleEmpName { get; set; }
        public string AcceptedDate { get; set; }
        public string ExpectedOrCloserDate { get; set; }
        public int IsCapaRequired { get; set; }
        public bool IsddlClosedEnable { get; set; }
        public string SubmittedDate { get; set; }
        public int StatusID { get; set; }
        public int ActionStatusID { get; set; }
        public string ResponseDescription { get; set; }
        public string Comments { get; set; }
        public string ResponsibleHOD_Comments { get; set; }
        public string QAComments { get; set; }
        public List<CapaModel> ResponseCAPAS { get; set; }
    }
    public class HistoryDetailsInAudit
    {
        public int ActionHistoryID { get; set; }
        public string ActionStatus { get; set; }
        public string ActionRole { get; set; }
        public string ActionBy { get; set; }
        public string ActionDate { get; set; }
        public string Remarks { get; set; }
        public HistoryDetailsInAudit(int _ActionHistoryID, string _ActionStatus, string _ActionRole,
            string _ActionBy, string _ActionDate, string _Remarks)
        {
            ActionHistoryID = _ActionHistoryID;
            ActionStatus = _ActionStatus;
            ActionRole = _ActionRole;
            ActionBy = _ActionBy;
            ActionDate = _ActionDate;
            Remarks = _Remarks;
        }
        public HistoryDetailsInAudit() { }
    }
    public class AuditObservationsList
    {
        public int ObservationID { get; set; }
        public int ObservationActionStatusID { get; set; }
        public string ObservationStatus { get; set; }
        public string ObservationNumber { get; set; }
        public string ObservationDescription { get; set; }
        public string ObservationSite { get; set; }
        public string ObservationDepartment { get; set; }
        public string ObservationPriority { get; set; }
        public string ObservationActionStatusName { get; set; }

        public AuditObservationsList(int _ObservationID, int _ObservationActionStatusID, string _ObservationStatus, string _ObservationNumber,
            string _ObservationDescription, string _ObservationSite, string _ObservationDepartment,
            string _ObservationPriority, string _ObservationActionStatusName)
        {
            ObservationID = _ObservationID;
            ObservationActionStatusID = _ObservationActionStatusID;
            ObservationStatus = _ObservationStatus;
            ObservationNumber = _ObservationNumber;
            ObservationDescription = _ObservationDescription;
            ObservationSite = _ObservationSite;
            ObservationDepartment = _ObservationDepartment;
            ObservationPriority = _ObservationPriority;
            ObservationActionStatusName = _ObservationActionStatusName;
        }
        public AuditObservationsList() { }
    }
    public class DepartmentList
    {
        public int DeptID { get; set; }
        public string DeptName { get; set; }
    }
    public class ML_ObservationData
    {
        public string CFR_NUM { get; set; }
        public int CFR_Count { get; set; }
        public string CFR_Name { get; set; }
        public string CFR_Desp { get; set; }
        public ML_ObservationData(string _CFR_NUM, int _CFR_Count, string _CFR_Name,
            string _CFR_Desp)
        {
            CFR_NUM = _CFR_NUM;
            CFR_Count = _CFR_Count;
            CFR_Name = _CFR_Name;
            CFR_Desp = _CFR_Desp;
        }
        public ML_ObservationData() { }
    }
    public class ML_ObservationCFR_Num
    {
        public string Short_Description { get; set; }
        public string Long_Description { get; set; }
        public ML_ObservationCFR_Num(string _Short_Description, string _Long_Description)
        {
            Short_Description = _Short_Description;
            Long_Description = _Long_Description;
        }
        public ML_ObservationCFR_Num() { }
    }
}