﻿namespace AizantIT_PharmaApp.Areas.QMS.Models.Audit.ViewModel
{
    public class AuditCharts
    {
        public string TextField { get; set; }
        public int ValueCount { get; set; }
        public int ValueField { get; set; }
        public AuditCharts(string _TextField,int _ValueCount,int _ValueField)
        {
            TextField = _TextField;
            ValueCount = _ValueCount;
            ValueField = _ValueField;
        }

    }
}