﻿namespace AizantIT_PharmaApp.Areas.QMS.Models.Audit.ViewModel
{
    public class ObservationReportList
    {
        public int AuditID { get; set; }
        public int AuditCurrentStatusID { get; set; }
        public int AuditCreationStatusID { get; set; }
        public int AuditQAEmpID { get; set; }
        public int ObservationID { get; set; }
        public int ObservationCurrentStatusID { get; set; }
        public int ObservationHODEmpID { get; set; }
        public bool IsResponseApprovedForThisVersion { get; set; }
        public string AuditNumber { get; set; }
        public string ObservationNumber { get; set; }
        public string Author { get; set; }
        public string ResponsiblePerson { get; set; }
        public string SiteName { get; set; }
        public string Department { get; set; }
        public string ObservationCategory { get; set; }
        public string DueDate { get; set; }
        public string ObservationStatusName { get; set; }
        public string ActionCurrentStatusName { get; set; }

        public ObservationReportList(int _AuditID, int _AuditCurrentStatusID,
            int _AuditCreationStatusID, int _AuditQAEmpID, int _ObservationID, int _ObservationCurrentStatusID, int _ObservationHODEmpID, bool _IsResponseApprovedForThisVersion, string _AuditNumber,
            string _ObservationNumber,string _Author, string _ResponsiblePerson, string _SiteName, string _Department,
           string _ObservationCategory, string _DueDate, string _ObservationStatusName, string _ActionCurrentStatusName)
        {
            AuditID = _AuditID;
            AuditCurrentStatusID = _AuditCurrentStatusID;
            AuditCreationStatusID = _AuditCreationStatusID;
            AuditQAEmpID = _AuditQAEmpID;
            ObservationID = _ObservationID;
            ObservationCurrentStatusID = _ObservationCurrentStatusID;
            ObservationHODEmpID = _ObservationHODEmpID;
            IsResponseApprovedForThisVersion = _IsResponseApprovedForThisVersion;
            AuditNumber = _AuditNumber;
            ObservationNumber = _ObservationNumber;
            Author = _Author;
            ResponsiblePerson = _ResponsiblePerson;
            SiteName = _SiteName;
            Department = _Department;
            ObservationCategory = _ObservationCategory;
            DueDate = _DueDate;
            ObservationStatusName = _ObservationStatusName;
            ActionCurrentStatusName = _ActionCurrentStatusName;
        }
    }
}