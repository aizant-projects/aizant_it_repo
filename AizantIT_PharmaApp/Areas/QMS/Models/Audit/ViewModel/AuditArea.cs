﻿namespace AizantIT_PharmaApp.Areas.QMS.Models.ViewModel
{
    public class AuditArea
    {
        public int AuditAreaID { get; set; }
        public string AuditAreaName { get; set; }
        public string AuditSubArea { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }

        public AuditArea(int _AuditAreaID, string _AuditAreaName, string _AuditSubArea, 
             string _CreatedBy, string _CreatedDate, string _ModifiedBy, string _ModifiedDate)
        {
            AuditAreaID = _AuditAreaID;
            AuditAreaName = _AuditAreaName;
            AuditSubArea = _AuditSubArea;
            CreatedBy = _CreatedBy;
            CreatedDate = _CreatedDate;
            ModifiedBy = _ModifiedBy;
            ModifiedDate = _ModifiedDate;
        }
        public AuditArea() { }
    }

    public class AuditAreaPost
    {
        public int AreaID { get; set; }
        public string AuditAreaName { get; set; }
        public string[] AuditSubAreaNames { get; set; }
        public string CombinedAuditSubAreaswitComa { get; set; }
        public AuditAreaPost(int _AreaID, string _AuditAreaName, string _CombinedAuditSubAreaswitComa)
        {
            AreaID = _AreaID;
            AuditAreaName = _AuditAreaName;
            CombinedAuditSubAreaswitComa = _CombinedAuditSubAreaswitComa;
        }
        public AuditAreaPost()
        {

        }
    }

    public class AuditSubArea
    {
        public int AuditSubAreaID { get; set; }
        public string AuditSubAreaName { get; set; }
        public string IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }

        public AuditSubArea(int _AuditSubAreaID, string _AuditSubAreaName, string _IsActive, string _CreatedBy, string _CreatedDate,
             string _ModifiedBy, string _ModifiedDate)
        {
            AuditSubAreaID = _AuditSubAreaID;
            AuditSubAreaName = _AuditSubAreaName;
            IsActive = _IsActive;
            CreatedBy = _CreatedBy;
            CreatedDate = _CreatedDate;
            ModifiedBy = _ModifiedBy;
            ModifiedDate = _ModifiedDate;
        }
        public AuditSubArea() { }
    }
}