﻿namespace AizantIT_PharmaApp.Areas.QMS.Models.Audit.ViewModel
{
    public class ResponsesOnObservation_List
    {
        public int ResponseID { get; set; }
        public int AssignedObservationID { get; set; }
        public int StatusID { get; set; }
        public int ActionStatus { get; set; }
        public string ResponseDesc { get; set; }
        public string ResponseStatus { get; set; }
        public string TurnAroundTime { get; set; }
        public string HasAttachments { get; set; }

        public ResponsesOnObservation_List(int _ResponseID, int _AssignedObservationID, int _StatusID,
            int _ActionStatus, string _ResponseDesc, string _ResponseStatus,string _TurnAroundTime, string _HasAttachments)
        {
            ResponseID = _ResponseID;
            AssignedObservationID = _AssignedObservationID;
            StatusID = _StatusID;
            ActionStatus = _ActionStatus;
            ResponseDesc = _ResponseDesc;
            ResponseStatus = _ResponseStatus;
            TurnAroundTime = _TurnAroundTime;
            HasAttachments = _HasAttachments;
        }
    }
}