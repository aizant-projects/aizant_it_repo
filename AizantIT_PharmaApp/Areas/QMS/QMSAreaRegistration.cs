﻿using System.Web.Mvc;

namespace AizantIT_PharmaApp.Areas.QMS
{
    public class QMSAreaRegistration : AreaRegistration 
    {
        public override string AreaName
        {
            get
            {
                return "QMS";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "QMS_default",
                "QMS/{controller}/{action}/{id}",
                // new { action = "Index", id = UrlParameter.Optional }
                 new { controller = "Home", action = "Index", id = UrlParameter.Optional }
                 //new { controller = "NoteToFile", action = "Index", id = UrlParameter.Optional }
                // namespaces: new[] { "AizantIT_PharmaApp.Areas.QMS" }
            );
//            #region AREA QMS MODULE
//            //DashBoard
//            context.MapRoute("SubCommonDB", "QMS/Common/DashBoardController/{action}",
//  new { controller = "DashBoardController", action = "Index" }, new[] { "AizantIT_PharmaApp.Areas.QMS.Common" });
//            #region CHANGE CONTROL
//            //ChangeControler
//            context.MapRoute("SubCCNCC", "QMS/ChangeController/ChangeControlController/{action}",
//new { controller = "ChangeControlController", action = "Index" }, new[] { "AizantIT_PharmaApp.Areas.QMS.ChangeController" });

//            //BlockMasterController
//            context.MapRoute("SubCCNBMC", "QMS/ChangeController/BlockMasterController/{action}",
//new { controller = "BlockMasterController", action = "Index" }, new[] { "AizantIT_PharmaApp.Areas.QMS.ChangeController" });
//            //ImpactDeptLinkController
//            context.MapRoute("SubCCNIDLC", "QMS/ChangeController/ImpactDeptLinkController/{action}",
//new { controller = "ImpactDeptLinkController", action = "Index" }, new[] { "AizantIT_PharmaApp.Areas.QMS.ChangeController" });
//            //ImpactPointsController
//            context.MapRoute("SubCCNIPC", "QMS/ChangeController/ImpactPointsController/{action}",
//new { controller = "ImpactPointsController", action = "Index" }, new[] { "AizantIT_PharmaApp.Areas.QMS.ChangeController" });
//            //TypeChangeMasterController
//            context.MapRoute("SubCCNTCMC", "QMS/ChangeController/TypeChangeMasterController/{action}",
//new { controller = "TypeChangeMasterController", action = "Index" }, new[] { "AizantIT_PharmaApp.Areas.QMS.ChangeController" });

//            #endregion CHANGE CONTROL
//            #region CAPA
//            //ActionPlan
//            context.MapRoute("SubCAPAAPC", "QMS/CAPA/ActionPlanController/{action}",
//    new { controller = "ActionPlanController", action = "Index" }, new[] { "AizantIT_PharmaApp.Areas.QMS.CAPA" });
//            //CapaController
//            context.MapRoute("SubCAPACON", "QMS/CAPA/CapaController/{action}",
//   new { controller = "CapaController", action = "Index" }, new[] { "AizantIT_PharmaApp.Areas.QMS.CAPA" });
//            //QualityEventController
//            context.MapRoute("SubCAPAQEC", "QMS/CAPA/QualityEventController/{action}",
//   new { controller = "QualityEventController", action = "Index" }, new[] { "AizantIT_PharmaApp.Areas.QMS.CAPA" });
//            #endregion CAPA
//            #region EARRATA
//            //ErrataController
//            context.MapRoute("SubEARRATA", "QMS/Errata/ErrataController/{action}",
//  new { controller = "ErrataController", action = "Index" }, new[] { "AizantIT_PharmaApp.Areas.QMS.Errata" });
//            #endregion EARRATA
//            #region NOTETOFILE
//            //NoteToFileController
//            context.MapRoute("SubNTFC", "QMS/NoteToFileFolder/NoteToFileController/{action}",
//  new { controller = "NoteToFileController", action = "Index" }, new[] { "AizantIT_PharmaApp.Areas.QMS.NoteToFileFolder" });
//            #endregion NOTETOFILE
//            #region INCIDENT
//            //IncidentController
//            context.MapRoute("SubIDC", "QMS/Incident/IncidentController/{action}",
// new { controller = "IncidentController", action = "Index" }, new[] { "AizantIT_PharmaApp.Areas.QMS.Incident" });
//            #endregion INCIDENT
//            //
//            #endregion region AREA QMS MODULE
        }
    }
}