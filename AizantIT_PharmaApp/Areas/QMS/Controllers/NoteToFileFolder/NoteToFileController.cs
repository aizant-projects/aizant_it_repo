﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using QMS_BO.QMS_NoteTofile_BO;
using QMS_BusinessLayer;
using AizantIT_PharmaApp.Common.CustomFilters;
using Aizant_Enums;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;
using System.Diagnostics;
using QMS_BO.QMSComonBO;
using Aizant_API_Entities;
using AizantIT_PharmaApp.Common;
using AizantIT_PharmaApp.Areas.QMS.Controllers.Common;

namespace AizantIT_PharmaApp.Areas.QMS.Controllers.NoteToFile
{
    //[SessionExpire]
    [RouteArea]
    [CustAuthentication]
    [CustAuthorization(
        (int)AizantEnums.Modules.QMS,
        (int)QMS_Roles.HOD,
        (int)QMS_Roles.QA,
        (int)QMS_Roles.HeadQA,
        (int)QMS_Roles.Investigator,
        (int)QMS_Roles.AdminRole,
        (int)QMS_Roles.Initiator,
        (int)QMS_Roles.User,
         (int)QMS_Audit_Roles.AuditHOD,
         (int)QMS_Audit_Roles.AuditQA,
         (int)QMS_Audit_Roles.AuditHeadQA)]
    public class NoteToFileController : Controller
    {
        CCNBal Qms_bal1 = new CCNBal();
        QMS_BAL Qms_bal = new QMS_BAL();
        UMS_BAL objUMS_BAL = new UMS_BAL();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private AizantIT_DevEntities dbEF = new AizantIT_DevEntities();
        // GET: QMSNoteFile
        public ActionResult NoteToFileList(int ShowType, int Notification_ID = 0)
        {
            ViewBag.Exception = null;
            try
            {
                switch (ShowType)
                {
                    case 1:
                        ShowType = (int)QMS_NoteToFileListTypes.CreatedList;
                        break;
                    case 2:
                        ShowType = (int)QMS_NoteToFileListTypes.InitiatorRevertedList;
                        break;
                    case 3:
                        ShowType = (int)QMS_NoteToFileListTypes.HOD_ReviewList;
                        break;
                    case 4:
                        ShowType = (int)QMS_NoteToFileListTypes.HOD_RevertedList;
                        break;
                    case 5:
                        ShowType = (int)QMS_NoteToFileListTypes.QaReviewList;
                        break;
                    case 6:
                        ShowType = (int)QMS_NoteToFileListTypes.QaRevertedList;
                        break;
                    case 7:
                        ShowType = (int)QMS_NoteToFileListTypes.HeadQaReviewList;
                        break;
                    case 8:
                        ShowType = (int)QMS_NoteToFileListTypes.MainList;
                        break;
                    case 9:
                        ShowType = (int)QMS_NoteToFileListTypes.PednigCharttList;
                        break;
                    case 10:
                        ShowType = (int)QMS_NoteToFileListTypes.RejectedChartList;
                        break;
                    case 11:
                        ShowType = (int)QMS_NoteToFileListTypes.ApproveChartList;
                        break;
                    case 12:
                        ShowType = (int)QMS_NoteToFileListTypes.NTFManage;
                        break;
                }
                TempData["ShowTypeNtf"] = ShowType;
                TempData.Keep("ShowTypeNtf");
                NoteFileBO objNTF = new NoteFileBO();
                //Notification Update
                GetUserEmpID(objNTF);
                int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                NotificationUpdate(Notification_ID, EmpIDs);
                TempData["UserEmpID"] = objNTF.EmpID;
                if (ShowType == 11 || ShowType == 10 || ShowType == 9)
                {
                    TempData.Keep("RoleIDFromCharts");
                    TempData.Keep("DeptIDFromDashboard");
                    TempData.Keep("FromDateDashboard");
                    TempData.Keep("ToDateDashboard");
                }
                else
                {
                    TempData["ToDateDashboard"] = 0;
                    TempData["FromDateDashboard"] = 0;
                    TempData["DeptIDFromDashboard"] = 0;
                    TempData["RoleIDFromCharts"] = 0;
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "NTF_M1:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
            }
            return View();
        }
        public void NotificationUpdate(int Notification_ID, int[] EmpIDs)
        {
            ViewBag.NTFID = 0;
            if (Notification_ID != 0)
            {
                objUMS_BAL = new UMS_BAL();
                objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs); //2 represents NotifyEmp had visited the page through notification link.

                var GetNTFID = (from N in dbEF.AizantIT_NTFNotificationLink
                                where N.NotificationID == Notification_ID
                                select new { N.NTFID }).ToList();
                foreach (var item in GetNTFID)
                {
                    ViewBag.NTFID = item.NTFID;
                }
            }
        }
        public void GetUserEmpID(NoteFileBO NTF)
        {
            if (Session["UserDetails"] != null)
            {
                NTF.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            }
        }
        #region Initiator

        //Bind Department
        //public void BindDDlDepartment(NoteFileBO NTFBO)
        //{
        //    CCNBal cCNBal = new CCNBal();
        //    /*Commented for Department and ActionPlan Dropdown binding 13-02-2019*/
        //    DataSet ds = cCNBal.GetddlBindingCCNCreate(NTFBO.EmpID);
        //    List<SelectListItem> Dep = new List<SelectListItem>();
        //    Dep.Add(new SelectListItem
        //    {
        //        Text = "Select",
        //        Value = "0",
        //    });
        //    foreach (DataRow rdr in ds.Tables[0].Rows)
        //    {
        //        Dep.Add(new SelectListItem
        //        {
        //            Text = (rdr["DepartmentName"]).ToString(),
        //            Value = (rdr["DeptID"]).ToString(),
        //        });
        //    }
        //    ViewBag.Department = new SelectList(Dep.ToList(), "Value", "Text", NTFBO.DeptID);
        //}
        public void BindDeptAndDocumentType(NoteFileBO NTFBO)
        {
            //Bind Department
           // BindDDlDepartment(NTFBO);
            CCNBal cCNBal = new CCNBal();
            DataSet ds = cCNBal.GetddlBindingCCNCreate(NTFBO.EmpID);
            List<SelectListItem> Dep = new List<SelectListItem>();
            Dep.Add(new SelectListItem
            {
                Text = "Select",
                Value = "0",
            });
            foreach (DataRow rdr in ds.Tables[0].Rows)
            {
                Dep.Add(new SelectListItem
                {
                    Text = (rdr["DepartmentName"]).ToString(),
                    Value = (rdr["DeptID"]).ToString(),
                });
            }
            ViewBag.Department = new SelectList(Dep.ToList(), "Value", "Text", NTFBO.DeptID);
            //Bind Document Type 
            List<SelectListItem> DocumentTypeList = new List<SelectListItem>();
            //var D1 = (from tp in dbEF.AizantIT_DocumentType
            //          select new { tp.DocumentTypeID, tp.DocumentType }).ToList();
            DocumentTypeList.Add(new SelectListItem
            {
                Text = "Select",
                Value = "-1",
            });
            foreach (DataRow rdr in ds.Tables[1].Rows)
            {
                DocumentTypeList.Add(new SelectListItem
                {
                    Text = (rdr["DocumentType"]).ToString(),
                    Value = (rdr["DocumentTypeID"]).ToString(),
                });
            }
            DocumentTypeList.Add(new SelectListItem
            {
                Text = "Others",
                Value = "0",
            });
            ViewBag.DocumentType = new SelectList(DocumentTypeList.ToList(), "Value", "Text",-1);
        }

        public PartialViewResult Create()
        {
            try
            {
                NoteFileBO NTFBO = new NoteFileBO();
                DataTable dt = new DataTable();
                GetUserEmpID(NTFBO);
                BindDeptAndDocumentType(NTFBO);
                return PartialView();
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "NTF_M2:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }
        //Auto genarated notefile number
        [HttpGet]
        public JsonResult NoteFileNo(int DeptID)
        {
            try
            {
                DataTable dt1 = new DataTable();
                //4 means NoteToFile
                dt1 = Qms_bal.BalGetAutoGenaratedNumber(DeptID, 4);
                var NoteToFileNumber = dt1.Rows[0][0].ToString();
                return Json(new { hasError = false, data = NoteToFileNumber }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "NTF_M3:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
       
        [HttpPost]
        public JsonResult Create(NoteFileBO NTFBO)
        {
            try
            {
                DataTable dt = new DataTable();
                GetUserEmpID(NTFBO);
                NTFBO.PreparedBy = NTFBO.EmpID.ToString();
                dt = Qms_bal.NoteToFileInsertBAL(NTFBO);
                string NTFNumber= dt.Rows[0][0].ToString();
                return Json(new { hasError = false, data = NTFNumber }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "NTF_M7:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        //NTF Edit in Initiator Revert page
        public PartialViewResult Details(int id)
        {
            try
            {
                NoteFileBO NTF = new NoteFileBO();
                NTF = BindData(id);
                return PartialView(NTF);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "NTF_M8:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }
        //NTF Edit and bind data to initiator Revert Page
        public NoteFileBO BindData(int id)
        {
            NoteFileBO NTF = new NoteFileBO();
            //int A_status = 0;
            //var value = (from NF in dbEF.AizantIT_NoteToFileMaster
            //             where NF.NTF_ID == id
            //             select new { NF.CurrentStatus }).Take(1);
            //foreach (var item in value)
            //{
            //    A_status = (int)item.CurrentStatus;
            //}
            var NTF2 = (from NF in dbEF.AizantIT_NoteToFileMaster
                        join H in dbEF.AizantIT_NoteToFileHistory on NF.NTF_ID equals H.NTF_ID
                        where NF.NTF_ID == id && H.NTF_ID == id && H.ActionStatus == NF.CurrentStatus
                        select new { NF.NTF_ID, NF.NoteFile_Number, H.Comments, NF.DeptID, NF.CreatedDate, NF.CurrentStatus, NF.DocumentTypeID, NF.HOD_EmpID, NF.NTF_Description }).ToList();
            foreach (var dr in NTF2)
            {
                NTF.NF_ID = Convert.ToInt32(dr.NTF_ID);
                NTF.NoteToFileNo = dr.NoteFile_Number.ToString();
                NTF.DocCreateDate = Convert.ToDateTime(dr.CreatedDate).ToString("dd MMM yyyy");
                NTF.DeptID = Convert.ToInt32(dr.DeptID);
                ViewBag.DeptIDforCAPA = NTF.DeptID;
                NTF.DocumentTypeID = Convert.ToInt32(dr.DocumentTypeID);
                NTF.HOD_EmpID = Convert.ToInt32(dr.HOD_EmpID);
                if (dr.Comments != null)
                {
                    NTF.HODComments = dr.Comments.ToString();
                }
                NTF.CurrentStatus = dr.CurrentStatus.ToString();
                NTF.NTF_Description = dr.NTF_Description.ToString();
                DocumentDetialsClass objDocument = new DocumentDetialsClass();
                QMSCommonActions CommonDocBo = new QMSCommonActions();
                if (NTF.DocumentTypeID != 0)
                {
                    objDocument = CommonDocBo.GetDocumentDetails(NTF.NF_ID, 4);
                        NTF.ReferenceNumber = objDocument.DocumentNumber;
                        NTF.DocTitle = objDocument.DocumentName;
                        NTF.versionID = objDocument.VersionID.ToString(); ;
                }
                if (NTF.DocumentTypeID == 0)
                {
                objDocument = CommonDocBo.GetOtherDocumentDetails(NTF.NF_ID, 4);
                        NTF.ReferenceNumberEnter = objDocument.DocumentNumber;
                        NTF.DocTitle1 = objDocument.DocumentNumber;
                }
            }
            List<SelectListItem> Department2 = new List<SelectListItem>();
            Department2.Add(new SelectListItem
            {
                Text = "Select",
                Value = "-1",
            });
            var D1 = (from tp in dbEF.AizantIT_DocumentType
                      select new { tp.DocumentTypeID, tp.DocumentType }).ToList();
            foreach (var item in D1)
            {
                Department2.Add(new SelectListItem
                {
                    Text = item.DocumentType,
                    Value = Convert.ToString(item.DocumentTypeID),
                });
            }
            Department2.Add(new SelectListItem
            {
                Text = "Others",
                Value = "0",
            });
            ViewBag.DocumentType = new SelectList(Department2.ToList(), "Value", "Text", NTF.DocumentTypeID);
            var RefNo = (from dv in dbEF.AizantIT_DocVersion
                         join DM in dbEF.AizantIT_DocumentMaster on dv.DocumentID equals DM.DocumentID
                         where DM.DocumentTypeID == NTF.DocumentTypeID && DM.DeptID == NTF.DeptID && (dv.Status == "A" || dv.Status=="R") && dv.EffectiveDate != null
                         select new { dv.VersionID, DM.DocumentNumber }).ToList();
            ViewBag.ReferenceNumber = new SelectList(RefNo.ToList(), "VersionID", "DocumentNumber", NTF.versionID);
            int VersionID = Convert.ToInt32(NTF.versionID);
            if (RefNo.Count > 0)
            {
                bool has = RefNo.Any(t => t.VersionID == VersionID);
                if (has == false)
                {
                    var DocumentID = (from t1 in dbEF.AizantIT_DocVersion
                                      where t1.VersionID == VersionID
                                      select t1.DocumentID).SingleOrDefault();

                    var GetNewVersionID = (from t1 in dbEF.AizantIT_DocVersion
                                           orderby t1.VersionID descending
                                           where t1.DocumentID == DocumentID && (t1.Status == "A" || t1.Status == "R") && t1.EffectiveDate != null
                                           select t1.VersionID);
                    int NewVersionID = GetNewVersionID.First();
                    NTF.DocTitle       = (from t1 in dbEF.AizantIT_DocVersionTitle
                                      join t2 in dbEF.AizantIT_DocVersion on t1.DocTitleID equals t2.DocTitleID
                                      where t2.VersionID == NewVersionID
                                      select t1.Title).SingleOrDefault();

                    ViewBag.ReferenceNumber = new SelectList(RefNo.ToList(), "VersionID", "DocumentNumber", NewVersionID);

                }
            }
            /* For Binding HOD EmpName on Assignto Dropdown in place of using linq query 12-02-2019*/
            objUMS_BAL = new UMS_BAL();
            List<SelectListItem> objAssignHod = new List<SelectListItem>();
            DataTable dtemp = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(NTF.DeptID), (int)QMS_Roles.HOD, true); //25=hod
            foreach (DataRow RPItem in dtemp.Rows)
            {
                objAssignHod.Add(new SelectListItem()
                {
                    Text = (RPItem["EmpName"]).ToString(),
                    Value = (RPItem["EmpID"]).ToString()
                });
            }
            ViewBag.HOD = new SelectList(objAssignHod.ToList(), "Value", "Text", NTF.HOD_EmpID);

            NTF.DeptName = (from dm in dbEF.AizantIT_DepartmentMaster
                            where dm.DeptID == NTF.DeptID
                            select  dm.DepartmentName).SingleOrDefault();
            //foreach (var item in DeptName)
            //{
            //    NTF.DeptName = item.DepartmentName;
            //}
            /*End*/
            return NTF;
        }
        [HttpPost]
        public JsonResult UpdateNTF(NoteFileBO NTFBO)
        {
            try
            {
                DataTable dt = new DataTable();
                GetUserEmpID(NTFBO);
                NTFBO.PreparedBy = NTFBO.EmpID.ToString();
                var version = (from v in dbEF.AizantIT_DocVersion
                               where v.DocumentID == NTFBO.DocumentID
                               orderby v.VersionID descending
                               select new { v.VersionID }).Take(1);
                foreach (var item in version)
                {
                    NTFBO.versionID = item.VersionID.ToString();
                }
                Boolean Result = ToCheckEmpValidityNTF(NTFBO.NF_ID, 24);
                if (Result == true)
                {
                int i = Qms_bal.UpdateNoteToFileBAL(NTFBO);
                }
                else
                {
                    return Json(new { hasError = true, data = "You are not authorized User to do action." }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { hasError = false, data = NTFBO }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "NTF_M9:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region HOD_Review
        //QA and Head QA pages
        //[CustAuthorization(4,"26","27")]
        public ActionResult HODApprover(int id, string Number)
        {
            try
            {
                CCNBal cCNBal = new CCNBal();
                NoteFileBO NTF = new NoteFileBO();
                var DeptIDforCapa = TempData["DeptIDforCAPA"];
                ViewBag.Department = DeptIDforCapa;
                if (Number == "1")
                {
                    NTF = BindData1(id);
                    NoteFileBO NTFBO = new NoteFileBO();
                    GetUserEmpID(NTFBO);
                    TempData["EmpID"] = NTFBO.EmpID;
                    /*Commented for Department  Dropdown binding 13-02-2019*/
                    DataSet ds = cCNBal.GetddlBindingCCNCreate(NTFBO.EmpID);
                    List<SelectListItem> Dep = new List<SelectListItem>();
                    Dep.Add(new SelectListItem
                    {
                        Text = "Select",
                        Value = "0",
                    });
                    foreach (DataRow rdr in ds.Tables[0].Rows)
                    {
                        Dep.Add(new SelectListItem
                        {
                            Text = (rdr["DepartmentName"]).ToString(),
                            Value = (rdr["DeptID"]).ToString(),
                            //Selected = (Convert.ToInt32(rdr["DeptID"]) == errataBO.DeptID ? true : false)
                        });
                    }
                    ViewBag.Department = new SelectList(Dep.ToList(), "Value", "Text");

                    if (NTF.CurrentStatus == "6" || NTF.CurrentStatus == "19" || NTF.CurrentStatus == "9" || NTF.CurrentStatus == "20")
                    {
                        var QaEmpName = (from objNtf in dbEF.AizantIT_NoteToFileMaster
                                         join objemp in dbEF.AizantIT_EmpMaster on objNtf.HOD_EmpID equals objemp.EmpID
                                         where objNtf.NTF_ID == NTF.NF_ID
                                         select new { fullName = (objemp.FirstName + "" + objemp.LastName) }).Take(1);
                        foreach (var EmpItem in QaEmpName)
                        {
                            ViewBag.HODEmployeeName = "HOD ( " + EmpItem.fullName + " )";
                        }
                    }
                }
                // prepared,hod,Qa employee id's are not binded to HeadQA dropdown
                int initiatorempId = Convert.ToInt32(NTF.PreparedBy);
                List<int> HqaEmpid = new List<int>() { initiatorempId, NTF.HOD_EmpID, NTF.QA_EmpId };
                if (NTF.CurrentStatus == "11" || NTF.CurrentStatus == "6" || NTF.CurrentStatus == "19")
                {
                    /*For Binding Head QA Employee name*/
                    List<SelectListItem> objAssignHQA = new List<SelectListItem>();
                    DataSet dtemphqa = Qms_bal.GetEmpNamesbasedonRoleIDBal((int)QMS_Roles.HeadQA, "A"); //26=QA
                    foreach (DataRow Item in dtemphqa.Tables[0].Rows)
                    {
                        if (!HqaEmpid.Contains(Convert.ToInt32(Item["EmpID"])))
                        {
                            objAssignHQA.Add(new SelectListItem()
                            {
                                Text = (Item["EmpName"]).ToString(),
                                Value = (Item["EmpID"]).ToString()
                            });
                        }
                    }
                    ViewBag.AssignHeadQA = new SelectList(objAssignHQA.Distinct().ToList(), "Value", "Text", NTF.Head_QA);
                }
                LatestComments(NTF);
                TempData["NTF_ID"] = NTF.NF_ID;
                return PartialView(NTF);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "NTF_M10:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }
       
        public NoteFileBO BindData1(int id)
        {
            NoteFileBO NTF = new NoteFileBO();
            var NTF2 = (from NF in dbEF.AizantIT_NoteToFileMaster
                        where NF.NTF_ID == id
                        select new { NF.NTF_ID, NF.NoteFile_Number, NF.HeadQA, NF.QA_EmpID, NF.CurrentStatus, NF.CapaRequired, NF.DeptID, NF.CreatedDate, NF.DocumentTypeID, NF.HOD_EmpID, NF.NTF_Description, NF.PreparedBy }).ToList();

            foreach (var dr in NTF2)
            {
                NTF.NF_ID = Convert.ToInt32(dr.NTF_ID);
                NTF.NoteToFileNo = dr.NoteFile_Number.ToString();
                NTF.DocCreateDate = Convert.ToDateTime(dr.CreatedDate).ToString("dd MMM yyyy");
                NTF.DeptID = Convert.ToInt32(dr.DeptID);
                NTF.DocumentTypeID = Convert.ToInt32(dr.DocumentTypeID);
                NTF.HOD_EmpID = Convert.ToInt32(dr.HOD_EmpID);
                NTF.CurrentStatus = dr.CurrentStatus.ToString();
                NTF.PreparedBy = Convert.ToInt32(dr.PreparedBy).ToString();
                NTF.QA_EmpId = Convert.ToInt32(dr.QA_EmpID);
                List<String> CurrentStatusNTF = new List<String> { "4", "6", "7","8", "9", "11", "17", "19", "20", "22", "21", "15", "14" };
                if (CurrentStatusNTF.Contains(NTF.CurrentStatus))
                {
                    if (dr.CapaRequired != null)
                    {
                        NTF.IsCAPARequired = (dr.CapaRequired).ToString();
                    }
                }
                DocumentDetialsClass objDocument = new DocumentDetialsClass();
                QMSCommonActions CommonDocBo = new QMSCommonActions();
                if (NTF.DocumentTypeID != 0)
                {
                    objDocument = CommonDocBo.GetDocumentDetails(NTF.NF_ID, 4);
                        NTF.DocumentTypeName = objDocument.DocumentType;
                        NTF.ReferenceNumber = objDocument.DocumentNumber;
                        NTF.DocTitle = objDocument.DocumentName;
                }
                if (NTF.DocumentTypeID == 0)
                {
                    objDocument = CommonDocBo.GetOtherDocumentDetails(NTF.NF_ID, 4);
                        NTF.DocumentTypeName = "Others";
                        NTF.ReferenceNumber = objDocument.DocumentNumber;
                        NTF.DocTitle = objDocument.DocumentName;
                }
                NTF.NTF_Description = dr.NTF_Description.ToString();
                //if (NTF.CurrentStatus == "7")
                //{
                //    NTF.QA_EmpId = (int)dr.QA_EmpID;
                //}
                if (NTF.CurrentStatus == "11" || NTF.CurrentStatus=="9")
                {
                    NTF.Head_QA = Convert.ToInt32(dr.HeadQA);
                }
                TempData["NTF"] = NTF.NoteToFileNo;
            }
            if (NTF.IsCAPARequired == "2")
            {
                NTF.capaRequiredorNot = "NO";
            }
            if (NTF.IsCAPARequired == "1")
            {
                NTF.capaRequiredorNot = "YES";
            }
            //Initiator comments
            Initiator_comments(NTF);
            //department name 
            NTF.DeptName = (from dm in dbEF.AizantIT_DepartmentMaster
                            where dm.DeptID == NTF.DeptID
                            select  dm.DepartmentName ).SingleOrDefault();
            
            return NTF;
        }
        public void LatestComments(NoteFileBO NTF)
        {
            int ActionStatus = 0;
            List<string> NtfStatus = new List<string>() { "1", "2", "6", "7", "9", "19", "17" };
            if (NtfStatus.Contains(NTF.CurrentStatus))
            {
                ActionStatus = Convert.ToInt32(NTF.CurrentStatus);
            }
            var Comments = (from QR in dbEF.AizantIT_NoteToFileHistory
                            where QR.NTF_ID == NTF.NF_ID && QR.ActionStatus == ActionStatus
                            select new { QR.Comments }).ToList();

            foreach (var C1 in Comments)
            {
                NTF.LatestComments = C1.Comments;
            }
        }
        public void Initiator_comments(NoteFileBO NTF)
        {
            #region Initiator
            int ActionStatus = 0;
            if (NTF.CurrentStatus == "2")
            {
                ActionStatus = 2;
            }
            if (NTF.CurrentStatus == "1")
            {
                ActionStatus = 1;
            }
            var Comments = (from QR in dbEF.AizantIT_NoteToFileHistory
                            where QR.NTF_ID == NTF.NF_ID && QR.ActionStatus == ActionStatus
                            select new { QR.Comments }).ToList();

            foreach (var C1 in Comments)
            {
                NTF.InitiatorComments = C1.Comments;
            }
            if (NTF.CurrentStatus == "7") //latest initiator comments,display in HOD page 
            {
                List<int> ids2 = new List<int>() { 1, 2 };
                var RevertQA = (from QR in dbEF.AizantIT_NoteToFileHistory
                                where QR.NTF_ID == NTF.NF_ID && ids2.Contains((int)QR.ActionStatus)
                                select new { QR.Comments }).ToList();
                foreach (var c3 in RevertQA)
                {
                    NTF.InitiatorComments = c3.Comments;
                    NTF.RevertComments = c3.Comments;
                    //use this id in NTF main list page
                    NTF.QAComments = c3.Comments;
                }
            }
            if (NTF.CurrentStatus == "6")//Hod comments ,display in Qa page
            {
                var HodComments = (from QR in dbEF.AizantIT_NoteToFileHistory
                                   where QR.NTF_ID == NTF.NF_ID && QR.ActionStatus == 6
                                   select new { QR.Comments }).ToList();

                foreach (var C1 in HodComments)
                {
                    NTF.HODComments = C1.Comments;
                }
            }
            #endregion
            #region HodComments
            if (NTF.CurrentStatus == "7")
            {
                var CommentHod = (from QR in dbEF.AizantIT_NoteToFileHistory
                                  where QR.NTF_ID == NTF.NF_ID && QR.ActionStatus == 7
                                  select new { QR.Comments }).ToList();
                foreach (var C2 in CommentHod)
                {
                    NTF.RevertComments = C2.Comments;
                }
            }
            if (NTF.CurrentStatus == "4")//ds
            {
                var CommentHod = (from QR in dbEF.AizantIT_NoteToFileHistory
                                  where QR.NTF_ID == NTF.NF_ID && QR.ActionStatus == 4
                                  select new { QR.Comments }).ToList();
                foreach (var C2 in CommentHod)
                {
                    NTF.HODComments = C2.Comments;
                }
            }
            if (NTF.CurrentStatus == "16")//ds
            {
                var CommentHod = (from QR in dbEF.AizantIT_NoteToFileHistory
                                  where QR.NTF_ID == NTF.NF_ID && QR.ActionStatus == 16
                                  select new { QR.Comments }).ToList();
                foreach (var C2 in CommentHod)
                {
                    NTF.HQAComments = C2.Comments;
                }
            }
            if (NTF.CurrentStatus == "14")//ds
            {
                var CommentHod = (from QR in dbEF.AizantIT_NoteToFileHistory
                                  where QR.NTF_ID == NTF.NF_ID && QR.ActionStatus == 14
                                  select new { QR.Comments }).ToList();
                foreach (var C2 in CommentHod)
                {
                    NTF.HQAComments = C2.Comments;
                }
            }
            if (NTF.CurrentStatus == "21")//ds
            {
                var CommentHod = (from QR in dbEF.AizantIT_NoteToFileHistory
                                  where QR.NTF_ID == NTF.NF_ID && QR.ActionStatus == 21
                                  select new { QR.Comments }).ToList();
                foreach (var C2 in CommentHod)
                {
                    NTF.HQAComments = C2.Comments;
                }
            }
            #endregion
            //QAComments
            if (NTF.CurrentStatus == "9" || NTF.CurrentStatus == "20") //QA Revert, display in QA page
            {
                var CommentHod = (from NH in dbEF.AizantIT_NoteToFileHistory
                                  where NH.NTF_ID == NTF.NF_ID && NH.ActionStatus == 9
                                  select new { NH.Comments }).ToList();
                foreach (var C2 in CommentHod)
                {
                    NTF.QAComments = C2.Comments;
                }
            }
            //Head_QA Revert Comment
            if (NTF.CurrentStatus == "11") //HeadQA_Revert
            {
                var CommentHod1 = (from NH in dbEF.AizantIT_NoteToFileHistory
                                   where NH.NTF_ID == NTF.NF_ID && NH.ActionStatus == 11
                                   select new { NH.Comments }).ToList();
                foreach (var C2 in CommentHod1)
                {
                    NTF.RevertComments = C2.Comments;
                }
                var RevertQA = (from QR in dbEF.AizantIT_NoteToFileHistory
                                where QR.NTF_ID == NTF.NF_ID && QR.ActionStatus == 6
                                select new { QR.Comments }).ToList();
                foreach (var c3 in RevertQA)
                {
                    NTF.HODComments = c3.Comments;
                }
            }
        }
        #endregion

        public NoteFileBO BindQAEmployee(NoteFileBO objNTF)
        {
            //PreparedBy and hod employee id's not binded to QA dropdown
            int InitiatorEmpId = Convert.ToInt32(objNTF.PreparedBy);
            List<int> QaEmployeeid = new List<int>() { InitiatorEmpId, objNTF.HOD_EmpID };
            /*QA Emplouee id binded hod page 13-02-2019*/
            List<SelectListItem> objAssignQA = new List<SelectListItem>();
            DataSet dtempqa = Qms_bal.GetEmpNamesbasedonRoleIDBal((int)QMS_Roles.QA, "A"); //26=QA
            foreach (DataRow Item in dtempqa.Tables[0].Rows)
            {
                if (!QaEmployeeid.Contains(Convert.ToInt32(Item["EmpID"])))
                {
                    objAssignQA.Add(new SelectListItem()
                    {
                        Text = (Item["EmpName"]).ToString(),
                        Value = (Item["EmpID"]).ToString()
                    });
                }
            }
            ViewBag.QAEmployee = new SelectList(objAssignQA.Distinct().ToList(), "Value", "Text", objNTF.QA_EmpId);
            return objNTF;
        }
        public void DeleteCapaTemp(int id)
        {
            CCNBal Qms_bal = new CCNBal();
            Qms_bal.DeleteCapaTemp(0, "NTF", id);
        }

        public ActionResult NTF_HODAction(int id, string Number=null, int ShowType = 0, int Notification_ID = 0)
        {
            try
            {
                NoteFileBO NTF = new NoteFileBO();
                DeleteCapaTemp(id);
                GetUserEmpID(NTF);
                TempData["EmpID"] = NTF.EmpID;
                //When Click on  Notification link to Call this method 
                if(Notification_ID != 0)
                {
                    int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                    NotificationUpdate(Notification_ID, EmpIDs);

                    if (ShowType == 3 || ShowType == 4)
                    {
                        Number = "1";
                    }
                }

                if (Number == "1") // Hod Review
                {
                    NTF = BindData1(id);

                    GetUserEmpID(NTF);
                    TempData["EmpID"] = NTF.EmpID;
                    //Bind QA Dropdown 
                    BindQAEmployee(NTF);
                    TempData["NTF_ID"] = NTF.NF_ID;
                    TempData.Keep("NTF_ID");
                }
                if (Number == "2")//Hod Revert Page
                {
                    NTF = BindData1(id);
                    //For Binding HOD EmpName on Assignto Dropdown in place of using linq query 13-02-2019
                    objUMS_BAL = new UMS_BAL();
                    List<SelectListItem> objAssignHod = new List<SelectListItem>();
                    DataTable dtemp = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(NTF.DeptID), (int)QMS_Roles.HOD, true); //25=hod

                    foreach (DataRow RPItem in dtemp.Rows)
                    {
                        objAssignHod.Add(new SelectListItem()
                        {
                            Text = (RPItem["EmpName"]).ToString(),
                            Value = (RPItem["EmpID"]).ToString()
                        });
                    }
                    ViewBag.HOD = new SelectList(objAssignHod.ToList(), "Value", "Text", NTF.HOD_EmpID);
                    //end 
                    GetUserEmpID(NTF);
                    //Bind QA with selected Value
                    BindQAEmployee(NTF);
                }
                LatestComments(NTF);
                ModelState.Clear();
                return View(NTF);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "NTF_M14:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return View();
            }
        }
        
        //To Check How many Capa's is There 
        [HttpPost]
        public JsonResult CapaCheckingExistsOrNot(int NotefileID)
        {
            try
            {
                var CapaRequeriedSubmitValidation = (from CapaM in dbEF.AizantIT_CAPAMaster
                                                    join T2 in dbEF.AizantIT_CAPA_ActionPlan on CapaM.CAPAID equals T2.CAPAID
                                                       where T2.BaseID == NotefileID && CapaM.QualityEvent_TypeID == 4 && T2.CurrentStatus!=10
                                                       select CapaM).Count();
                return Json(new { hasError = false, data = CapaRequeriedSubmitValidation }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "NTF_M18:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
      
        //NoteToFile Revert and Rejected Action
        [HttpPost]
        public JsonResult RevertAndRejectNoteToFileAction(NoteFileBO objNTF)
        {
            try
            {
                int RoleID = 0;
                if (objNTF.Status == "HOD")
                {
                    RoleID = 25;
                }
                else if (objNTF.Status == "QA")
                {
                    RoleID = 26;
                }
                else if (objNTF.Status == "HQA")
                {
                    RoleID = 27;
                }else if (objNTF.Status == "R")
                {
                    RoleID = objNTF.RoleID;
                }
                Boolean Result = ToCheckEmpValidityNTF(objNTF.NF_ID, RoleID);
                if (Result == true)
                {
                    int i = (Qms_bal.RevertHODBAL(objNTF));
                    return Json(new { hasError = false, data = i }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { hasError = true, data = "You are not authorized User to do action." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "NTF_M19:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        //Approve or Update NoteToFile at Hod Role
        //NoteToFile Approve Action to Qa Role
        [HttpPost]
        public JsonResult ApproveAndUpdateNoteToFileAtHod(NoteFileBO BoNtf)
        {
            try
            {
                int RoleID = 0;
                if (BoNtf.Status == "QA" || BoNtf.Status == "HODUpdate")
                {
                    RoleID = 25;
                }
                if (BoNtf.Status == "HQA" || BoNtf.Status == "QAUpdate")
                {
                    RoleID = 26;
                }
                if(BoNtf.Status == "Capa")
                {
                    RoleID = 27;
                }

                Boolean Result = ToCheckEmpValidityNTF(BoNtf.NF_ID, RoleID);
                if (Result == true)
                {
                    int i = Qms_bal.NTFSubmittedToQABal(BoNtf);
                    return Json(new { hasError = false, data = i }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { hasError = true, data = "You are not authorized User to do action." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "NTF_M20:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
       

        public ActionResult NoteToFileListBasedonStatusShowView(int id)
        {
            try
            {
                NoteFileBO NTF = new NoteFileBO();
                TempData.Keep("RoleIDFromCharts");
                TempData.Keep("DeptIDFromDashboard");
                TempData.Keep("FromDateDashboard");
                TempData.Keep("ToDateDashboard");
                TempData.Keep("ShowTypeNtf");
                //BindDDlDepartment(NTF);
                //BindDDlActionPlan();
                //NTF = BindData(id);
                NTF = BindData1(id);
                 //BindQAEmployee(NTF);
                ViewBag.QAName = GetFullName(NTF.QA_EmpId);
                ViewBag.AssignHeadQA = GetFullName(NTF.Head_QA);

                //int initiatorempId = Convert.ToInt32(NTF.PreparedBy);
                //List<int> HqaEmpid = new List<int>() { initiatorempId, NTF.HOD_EmpID, NTF.QA_EmpId };
                ///*For Binding Head QA Employee name*/
                //List<SelectListItem> objAssignHQA = new List<SelectListItem>();
                //DataSet dtemphqa = Qms_bal.GetEmpNamesbasedonRoleIDBal((int)QMS_Roles.HeadQA, "A"); //26=QA
                //foreach (DataRow Item in dtemphqa.Tables[0].Rows)
                //{
                //    if (!HqaEmpid.Contains(Convert.ToInt32(Item["EmpID"])))
                //    {
                //        objAssignHQA.Add(new SelectListItem()
                //        {
                //            Text = (Item["EmpName"]).ToString(),
                //            Value = (Item["EmpID"]).ToString()
                //        });
                //    }
                //}
                //ViewBag.AssignHeadQA = new SelectList(objAssignHQA.Distinct().ToList(), "Value", "Text", NTF.Head_QA);
                /*End*/
                return View(NTF);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "NTF_M21:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return View();
            }
        }
        public JsonResult AssignedToTempDatea(int DeptID, int RoleID, string FromDate, string ToDate)
        {
            try
            {
                TempData["RoleIDFromCharts"] = RoleID;
                TempData.Keep("RoleIDFromCharts");
                TempData["DeptIDFromDashboard"] = DeptID;
                TempData.Keep("DeptIDFromDashboard");
                TempData["FromDateDashboard"] = FromDate == "" ? DateTime.Now.AddDays(-365).ToString("dd MMM yyyy") : FromDate;
                TempData.Keep("FromDateDashboard");
                TempData["ToDateDashboard"] = ToDate == "" ? DateTime.Now.ToString("dd MMM yyyy") : ToDate; ;
                TempData.Keep("ToDateDashboard");
                return Json(new { hasError = false, data = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "NTF_M22:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult GetNoteToFileList()
        {
            try
            {
                int iDisplayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int iDisplayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int iSortCol_0 = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir_0 = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                int ShowType = Convert.ToInt32(Request.Params["ShowType"]);
                int UserEmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                int RoleIDCharts = Convert.ToInt32(Request.Params["RoleIDCharts"]);
                int deptIdDashboard = Convert.ToInt32(Request.Params["DeptDashboard"]);
                string FromDateDashboard = Request.Params["FromDateDashboard"];
                string ToDateDashboard = Request.Params["ToDateDashboard"];

                string sSearch_NoteToFileNo = Request.Params["sSearch_2"];
                string sSearch_DeptName = Request.Params["sSearch_3"];
                string sSearch_ReferenceNumber = Request.Params["sSearch_4"];
                string sSearch_DocTitle = Request.Params["sSearch_5"];
                string sSearch_PreparedBy = Request.Params["sSearch_6"];
                string sSearch_DocCreateDate = Request.Params["sSearch_7"];
                string sSearch_CurrentStatus = Request.Params["sSearch_8"];
                List<NoteFileBO> listEmployees = new List<NoteFileBO>();
                int filteredCount = 0;
                var FromDate = "";
                var ToDate = "";
                if (FromDateDashboard == "0")
                {
                    FromDate = "";
                }
                else
                {
                    FromDate = FromDateDashboard;
                }
                if (ToDateDashboard == "0")
                {
                    ToDate = "";
                }
                else
                {
                    ToDate = ToDateDashboard;
                }

                DataSet dt = Qms_bal.GetNoteToFileListBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, ShowType, 
                    UserEmpID, RoleIDCharts, deptIdDashboard, FromDate, ToDate,
                    sSearch_NoteToFileNo, sSearch_DeptName, sSearch_ReferenceNumber, sSearch_DocTitle, sSearch_PreparedBy
            , sSearch_DocCreateDate, sSearch_CurrentStatus);
                if (dt.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow rdr in dt.Tables[0].Rows)
                    {
                        NoteFileBO objNtf = new NoteFileBO();

                        objNtf.Rownumber = Convert.ToInt32(rdr["RowNumber"]);
                        objNtf.NF_ID = Convert.ToInt32(rdr["NTF_ID"]);
                        objNtf.DeptName = ((rdr["Department"]).ToString());
                        objNtf.NoteToFileNo = (rdr["NoteFile_Number"]).ToString();
                        objNtf.ReferenceNumber = (rdr["ReferenceNumber"].ToString());
                        objNtf.DocTitle = (rdr["DocumentTitle"]).ToString();
                        objNtf.DocCreateDate = (rdr["Doc_CreateDate"].ToString());
                        objNtf.PreparedBy = Convert.ToString((rdr["PreparedBy"].ToString()));
                        objNtf.CurrentStatus = ((rdr["CurrentStatus"]).ToString());
                        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                        listEmployees.Add(objNtf);
                    }
                }
                return Json(new
                {
                    hasError = false,
                    iTotalRecords = dt.Tables[0].Rows.Count,
                    iTotalDisplayRecords = filteredCount,
                    aaData = listEmployees
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = "Yes";
                return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);
            }
        }

        #region Not Using Code
        //For Binding Init and HOD based on Dept and RoleID
        //public List<SelectListItem> BindHODDropdownAccessibleDepartmentsandRole(int DeptID, int Roleid, int SelectedEmpID)
        //{
        //    //Assigned HOD's
        //    List<SelectListItem> objAssignHod = new List<SelectListItem>();
        //    var AssignedHOD = dbEF.Database.SqlQuery<AccessibleEmpOnDeptandRole>("exec UMS.AizantIT_SP_ToGetEmployeesByDeptIDandRoleID " +
        //       +DeptID + "," + Roleid + ",'" + 1 + "'").ToList();
        //    objAssignHod.Add(new SelectListItem
        //    {
        //        Text = "Select",
        //        Value = "0",
        //    });
        //    foreach (var RPItem in AssignedHOD)
        //    {
        //        objAssignHod.Add(new SelectListItem()
        //        {
        //            Text = RPItem.EmpName,
        //            Value = RPItem.EmpID.ToString(),
        //            Selected = (Convert.ToInt32(RPItem.EmpID) == SelectedEmpID ? true : false)
        //        });
        //    }
        //    return objAssignHod.ToList();
        //}

        //For Binding QA and HeadQA based on RoleID
        //public List<SelectListItem> BindQAandHeadQAEmployeeID(int RoleID, int SelectedEmpId, List<int> NotBindingEmpID)
        //{
        //    List<SelectListItem> AssignedQAList = new List<SelectListItem>();
        //    var AssignedQA = dbEF.Database.SqlQuery<AccessibleEmpOnDeptandRole>("exec [UMS].[AizantIT_SP_GetEmployeesByRoleIDAndStatus] " +
        //                     RoleID + ",'A'").ToList();
        //    AssignedQAList.Add(new SelectListItem
        //    {
        //        Text = "Select",
        //        Value = "0",
        //    });
        //    foreach (var item in AssignedQA)
        //    {
        //        if (!NotBindingEmpID.Contains(item.EmpID))
        //        {
        //            AssignedQAList.Add(new SelectListItem()
        //            {
        //                Text = item.EmpName,
        //                Value = item.EmpID.ToString(),
        //                Selected = (item.EmpID == SelectedEmpId ? true : false)
        //            });
        //        }
        //    }
        //    return AssignedQAList;
        //}
        #endregion
        public string GetFullName(int EmpId)
        {
            var EmployeeName = (from E in dbEF.AizantIT_EmpMaster
                                where E.EmpID == EmpId
                                select (E.FirstName + "" + E.LastName)).SingleOrDefault();
            return EmployeeName;
        }
        public PartialViewResult _NTFAdminManage(int id)
        {
            NoteFileBO NTF = new NoteFileBO();
            GetUserEmpID(NTF);
            NTF = BindData1(id);
            DataSet dsEmployees = Qms_bal.GetEmployeesDeptandRoleWiseInAdminManageBAL(NTF.DeptID, 4);
            List<NoteFileBO> ListEmployee = new List<NoteFileBO>();
            for (int i = 0; i < 4; i++)
            {
                if (i == 0)
                {
                    //For getting Initiator Employee Names
                    List<SelectListItem> InitatorList = new List<SelectListItem>();
                    int PreparedBy = Convert.ToInt32(NTF.PreparedBy);
                    InitatorList.Add(new SelectListItem
                    {
                        Text = "Select",
                        Value = "0",
                    });
                    foreach (DataRow rdr in dsEmployees.Tables[0].Rows)
                    {
                        InitatorList.Add(new SelectListItem
                        {
                            Text = (rdr["EmpName"]).ToString(),
                            Value = (rdr["EmpID"]).ToString(),
                            Selected = (Convert.ToInt32(rdr["EmpID"]) == PreparedBy ? true : false)
                        });
                    }
                    ListEmployee.Add(new NoteFileBO
                    {
                        Sno = 1,
                        DDLEmpList = InitatorList,
                        RoleName = "Initiator",
                        SelectDDLEmp = Convert.ToInt16(NTF.PreparedBy),
                        SelectedEmployeeReAssign= GetFullName(Convert.ToInt32(NTF.PreparedBy))
                    });
                }
                if (i == 1)
                {
                    //For getting HOD Employee Names
                    List<SelectListItem> HODList = new List<SelectListItem>();
                    HODList.Add(new SelectListItem
                    {
                        Text = "Select",
                        Value = "0",
                    });
                    foreach (DataRow rdr in dsEmployees.Tables[1].Rows)
                    {
                        HODList.Add(new SelectListItem
                        {
                            Text = (rdr["EmpName"]).ToString(),
                            Value = (rdr["EmpID"]).ToString(),
                            Selected = (Convert.ToUInt32(rdr["EmpID"]) == NTF.HOD_EmpID ? true : false)
                        });
                    }
                    ListEmployee.Add(new NoteFileBO
                    {
                        Sno = 2,
                        DDLEmpList = HODList,
                        RoleName = "HOD",
                        SelectDDLEmp = NTF.HOD_EmpID,
                        SelectedEmployeeReAssign = GetFullName(NTF.HOD_EmpID)
                    });
                }
                if (i == 2)
                {
                    //For getting QA Employee Names
                    List<SelectListItem> QAList = new List<SelectListItem>();
                    List<int> QaEmployeeid = new List<int>() { Convert.ToInt16(NTF.PreparedBy), NTF.HOD_EmpID };
                    QAList.Add(new SelectListItem
                    {
                        Text = "Select",
                        Value = "0",
                    });
                    foreach (DataRow rdr in dsEmployees.Tables[2].Rows)
                    {
                        if (!QaEmployeeid.Contains(Convert.ToInt32(rdr["EmpID"])))
                        {
                            QAList.Add(new SelectListItem
                            {
                                Text = (rdr["EmpName"]).ToString(),
                                Value = (rdr["EmpID"]).ToString(),
                                Selected = (Convert.ToUInt32(rdr["EmpID"]) == NTF.QA_EmpId ? true : false)
                            });
                        }
                    }
                    ListEmployee.Add(new NoteFileBO
                    {
                        Sno = 3,
                        DDLEmpList = QAList,
                        RoleName = "QA",
                        SelectDDLEmp = NTF.QA_EmpId,
                        SelectedEmployeeReAssign = GetFullName(NTF.QA_EmpId)
                    });
                }
                if (i == 3)
                {
                    //For getting HeadQA Employee ID
                    List<SelectListItem> HeadQAList = new List<SelectListItem>();
                    List<int> HeadQAEmployeeid = new List<int>() { Convert.ToInt16(NTF.PreparedBy), NTF.HOD_EmpID, NTF.QA_EmpId };

                    var HEadQA = (from NM in dbEF.AizantIT_NoteToFileMaster
                                  where NM.NTF_ID == id
                                  select NM.HeadQA).SingleOrDefault();
                    int SelectedHEadQA = Convert.ToInt32(HEadQA);

                    HeadQAList.Add(new SelectListItem
                    {
                        Text = "Select",
                        Value = "0",
                    });
                    //For getting HQA Employee Names
                    foreach (DataRow rdr in dsEmployees.Tables[3].Rows)
                    {
                        if (!HeadQAEmployeeid.Contains(Convert.ToInt32(rdr["EmpID"])))
                        {
                            HeadQAList.Add(new SelectListItem
                            {
                                Text = (rdr["EmpName"]).ToString(),
                                Value = (rdr["EmpID"]).ToString(),
                                Selected = (Convert.ToUInt32(rdr["EmpID"]) == SelectedHEadQA ? true : false)

                            });
                        }
                    }
                    ListEmployee.Add(new NoteFileBO
                    {
                        Sno = 4,
                        DDLEmpList = HeadQAList,
                        RoleName = "HeadQa",
                        SelectDDLEmp = SelectedHEadQA,
                        SelectedEmployeeReAssign = GetFullName(SelectedHEadQA)
                    });
                }
            }
            //ViewBag.GetListDropDown = AddDDl;
            ViewBag.ListEmployees = ListEmployee;
            return PartialView(NTF);
        }

        [HttpPost]
        public JsonResult UpdateManageEmp(NoteFileBO objNTF)
        {
            QMS_BAL Qms_bal = new QMS_BAL();
            try
            {
                objNTF.ActionBy = (Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                int i = (Qms_bal.NTFAdminManageBal(objNTF, out int Result));
                if(Result == 0)
                {
                    return Json(new { hasError = false, data = i }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { hasError = true, data = "Modification of Employee is Restricted, as the Transaction is Completed." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "NTF_M19:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        public bool ToCheckEmpValidityNTF(int NTFID, int RoleID)
        {
            int Initiator = 0, HOD = 0, QA = 0, HeadQA = 0;
            Boolean i = true;

            int LoginEmployeeID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);

            var GetDetails = (from NTF in dbEF.AizantIT_NoteToFileMaster
                              where NTF.NTF_ID == NTFID
                              select new
                              {
                                  NTF.PreparedBy,
                                  NTF.HOD_EmpID,
                                  NTF.QA_EmpID,
                                  NTF.HeadQA
                              }).ToList();

            foreach (var item in GetDetails)
            {
                Initiator = (int)item.PreparedBy;
                HOD = (int)item.HOD_EmpID;
                QA = item.QA_EmpID == null ? 0 : (int)item.QA_EmpID;
                HeadQA = item.HeadQA == null ? 0 : (int)item.HeadQA;
            }
            if (RoleID == 24)
            {
                if (LoginEmployeeID == Initiator)
                {
                    i = true;//Success
                }
                else
                {
                    i = false;//fail
                }
            }
            if (RoleID == 25)
            {
                if (LoginEmployeeID == HOD)
                {
                    i = true; //Success
                }
                else
                {
                    i = false;//fail
                }
            }
            if (RoleID == 26)
            {
                if (LoginEmployeeID == QA)
                {
                    i = true;//Success
                }
                else
                {
                    i = false;//fail
                }
            }
            if (RoleID == 27)
            {
                if (LoginEmployeeID == HeadQA)
                {
                    i = true;//Success
                }
                else
                {
                    i = false;//false
                }
            }
            return i;
        }
    }
}

