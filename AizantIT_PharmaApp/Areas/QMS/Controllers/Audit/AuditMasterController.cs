﻿using Aizant_API_Entities;
using AizantIT_PharmaApp.Areas.QMS.Models.Audit.ViewModel;
using AizantIT_PharmaApp.Areas.QMS.Models.ViewModel;
using AizantIT_PharmaApp.Common;
using AizantIT_PharmaApp.Common.CustomFilters;
using QMS_BO.QMS_Audit_BO.AuditMaster;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.Areas.QMS.Controllers.Audit
{
    [Route("QMS/Audit/AuditMasterController/{action}")]
    [RouteArea]
    [CustAuthentication]
    public class AuditMasterController : Controller
    {
        [CustAuthorization((int)Modules.QMS,
         (int)QMS_Audit_Roles.AuditAdmin)]
        public ActionResult AuditMasterList()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = LineNo + " :" + HelpClass.SQLEscapeString(ex.Message);
                return View();
            }
        }

        #region AuditCategoryMaster
        //[AllowAnonymous] to skip the Authentication
        public ActionResult GetAuditCategoryList()
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    List<AuditCategoryBO> CategoryList = new List<AuditCategoryBO>();
                    var CategoryItemsList =
                    (from objAuditCategory in dbAizantIT_QMS_Audit.AizantIT_AuditCategory
                     join objEmpDetails in dbAizantIT_QMS_Audit.AizantIT_EmpMaster
                     on objAuditCategory.ModifiedBy equals objEmpDetails.EmpID into LeftJoinEmp
                     from objEmpDetails in LeftJoinEmp.DefaultIfEmpty()
                     select new
                     {
                         objAuditCategory.CategoryID,
                         objAuditCategory.CategoryName,
                         objAuditCategory.CreatedDate,
                         ModifiedByName = (objEmpDetails.FirstName + " " + objEmpDetails.LastName) == " " ? "N/A" : (objEmpDetails.FirstName + " " + objEmpDetails.LastName),
                         objAuditCategory.ModifiedDate
                     }).ToList();
                    foreach (var CategoryItem in CategoryItemsList)
                    {
                        CategoryList.Add(new AuditCategoryBO
                        {
                            CategoryID = CategoryItem.CategoryID,
                            CategoryName = CategoryItem.CategoryName,
                            CreatedDate = CategoryItem.CreatedDate == null ? "N/A" : Convert.ToDateTime(CategoryItem.CreatedDate).ToString("dd MMM yyyy HH:mm:ss"),
                            ModifiedByName = CategoryItem.ModifiedByName,
                            ModifiedDate = CategoryItem.ModifiedDate == null ? "N/A" : Convert.ToDateTime(CategoryItem.ModifiedDate).ToString("dd MMM yyyy HH:mm:ss")

                        });
                    }
                    return PartialView(CategoryList);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = HelpClass.SQLEscapeString(ex.Message);
                return PartialView("_SysException");
            }
        }

        [HttpPost]
        public JsonResult AuditCategoryNameModify(int CategoryID, string CategoryName)
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbAizantIT_QMS_Audit.Database.BeginTransaction())
                    {
                        try
                        {
                            bool IsRecordExists = (from itemAuditCategory in dbAizantIT_QMS_Audit.AizantIT_AuditCategory
                                                   where itemAuditCategory.CategoryName == CategoryName
                                                    && itemAuditCategory.CategoryID != CategoryID
                                                   select new { itemAuditCategory.CategoryName }).Any();
                            if (!IsRecordExists)
                            {
                                AizantIT_AuditCategory objAizantIT_AuditCategory = new AizantIT_AuditCategory();

                                objAizantIT_AuditCategory = dbAizantIT_QMS_Audit.AizantIT_AuditCategory.SingleOrDefault(x => x.CategoryID == CategoryID);
                                objAizantIT_AuditCategory.CategoryName = CategoryName;
                                objAizantIT_AuditCategory.ModifiedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                objAizantIT_AuditCategory.ModifiedDate = DateTime.Now;
                                dbAizantIT_QMS_Audit.SaveChanges();
                                transaction.Commit();
                                return Json(new
                                {
                                    msg = "Audit Category Details Updated.",
                                    msgType = "success",
                                    ModifiedByName = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString(),
                                    ModifiedDate = DateTime.Now.ToString("dd MMM yyyy HH:mm:ss")
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { msg = "Category Name already exists", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message);
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region AuditCategoryType
        public ActionResult GetAuditCategoryTypeList()
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    List<AuditCategoryTypeBO> CategoryTypeList = new List<AuditCategoryTypeBO>();
                    var CategoryTypeItemsList =
                    (from objAuditCategoryType in dbAizantIT_QMS_Audit.AizantIT_AuditCategoryType
                     join objAuditCategory in dbAizantIT_QMS_Audit.AizantIT_AuditCategory on objAuditCategoryType.CategoryID equals objAuditCategory.CategoryID
                     join objEmpDetails in dbAizantIT_QMS_Audit.AizantIT_EmpMaster
                     on objAuditCategoryType.ModifiedBy equals objEmpDetails.EmpID into LeftJoinEmp
                     from objEmpDetails in LeftJoinEmp.DefaultIfEmpty()
                     select new
                     {
                         objAuditCategoryType.TypeID,
                         objAuditCategoryType.TypeName,
                         objAuditCategoryType.IsActive,
                         objAuditCategoryType.CategoryID,
                         objAuditCategory.CategoryName,
                         objAuditCategoryType.CreatedDate,
                         ModifiedByName = (objEmpDetails.FirstName + " " + objEmpDetails.LastName) == " " ? "N/A" : (objEmpDetails.FirstName + " " + objEmpDetails.LastName),
                         objAuditCategoryType.ModifiedDate
                     }).ToList();
                    foreach (var CategoryTypeItem in CategoryTypeItemsList)
                    {
                        CategoryTypeList.Add(new AuditCategoryTypeBO
                        {
                            TypeID = CategoryTypeItem.TypeID,
                            TypeName = CategoryTypeItem.TypeName,
                            IsActive = CategoryTypeItem.IsActive,
                            CategoryID = CategoryTypeItem.CategoryID,
                            CategoryName = CategoryTypeItem.CategoryName,
                            CreatedDate = Convert.ToDateTime(CategoryTypeItem.CreatedDate).ToString("dd MMM yyyy HH:mm:ss"),
                            ModifiedByName = CategoryTypeItem.ModifiedByName,
                            ModifiedDate = CategoryTypeItem.ModifiedDate != null ? Convert.ToDateTime(CategoryTypeItem.ModifiedDate).ToString("dd MMM yyyy HH:mm:ss") : "N/A"
                        });
                    }
                    return PartialView(CategoryTypeList);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = HelpClass.SQLEscapeString(ex.Message);
                return PartialView("_SysException");
            }
        }
        [HttpPost]
        public JsonResult AuditCategoryTypeModify(int CategoryTypeID, string CategoryTypeName, bool CategoryIsActive, int CategoryTypeCategoryID)
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbAizantIT_QMS_Audit.Database.BeginTransaction())
                    {
                        try
                        {
                            bool IsRecordExists = (from itemAuditCategoryType in dbAizantIT_QMS_Audit.AizantIT_AuditCategoryType
                                                   where (itemAuditCategoryType.TypeName == CategoryTypeName && itemAuditCategoryType.CategoryID == CategoryTypeCategoryID)
                                                    && itemAuditCategoryType.TypeID != CategoryTypeID
                                                   select new { itemAuditCategoryType.TypeName }).Any();
                            if (!IsRecordExists)
                            {
                                AizantIT_AuditCategoryType objAizantIT_AuditCategoryType = new AizantIT_AuditCategoryType();
                                objAizantIT_AuditCategoryType = dbAizantIT_QMS_Audit.AizantIT_AuditCategoryType.SingleOrDefault(x => x.TypeID == CategoryTypeID);

                                objAizantIT_AuditCategoryType.TypeName = CategoryTypeName;
                                objAizantIT_AuditCategoryType.IsActive = CategoryIsActive;
                                objAizantIT_AuditCategoryType.CategoryID = CategoryTypeCategoryID;
                                objAizantIT_AuditCategoryType.ModifiedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                objAizantIT_AuditCategoryType.ModifiedDate = DateTime.Now;
                                dbAizantIT_QMS_Audit.SaveChanges();
                                transaction.Commit();
                                return Json(new
                                {
                                    msg = "Audit Category Type Updated.",
                                    msgType = "success",
                                    ModifiedByName = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString(),
                                    ModifiedDate = DateTime.Now.ToString("dd MMM yyyy HH:mm:ss")
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { msg = "Category Type Name Already Exists for this Category.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message);
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region RegulatoryAgency
        [HttpPost]
        public JsonResult RegulatoryAgencyCreate(RegulatoryAgencyBO objRegulatoryDetails)
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbAizantIT_QMS_Audit.Database.BeginTransaction())
                    {
                        try
                        {
                            bool IsRecordExists = (from itemRegulatoryAgency in dbAizantIT_QMS_Audit.AizantIT_RegulatoryAgency
                                                   where itemRegulatoryAgency.RegulatoryAgencyCode == objRegulatoryDetails.RegulatoryAgencyCode
                                                   || itemRegulatoryAgency.RegulatoryAgencyName == objRegulatoryDetails.RegulatoryAgencyName
                                                   || itemRegulatoryAgency.Region == objRegulatoryDetails.RegionName
                                                   select new { itemRegulatoryAgency.RegulatoryAgencyName, itemRegulatoryAgency.RegulatoryAgencyCode }).Any();
                            if (!IsRecordExists)
                            {
                                AizantIT_RegulatoryAgency objAizantIT_RegulatoryAgency = new AizantIT_RegulatoryAgency();
                                objAizantIT_RegulatoryAgency.RegulatoryAgencyCode = objRegulatoryDetails.RegulatoryAgencyCode;
                                objAizantIT_RegulatoryAgency.RegulatoryAgencyName = objRegulatoryDetails.RegulatoryAgencyName;
                                objAizantIT_RegulatoryAgency.Region = objRegulatoryDetails.RegionName;
                                objAizantIT_RegulatoryAgency.IsActive = true;
                                objAizantIT_RegulatoryAgency.CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                objAizantIT_RegulatoryAgency.CreatedDate = DateTime.Now;
                                dbAizantIT_QMS_Audit.AizantIT_RegulatoryAgency.Add(objAizantIT_RegulatoryAgency);
                                dbAizantIT_QMS_Audit.SaveChanges();
                                transaction.Commit();
                                return Json(new { msg = "Regulatory Agency Created.", msgType = "success" }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { msg = "Regulatory Agency Already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message);
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetRegulatoryAgencyList()
        {
            try
            {
                return PartialView();
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = HelpClass.SQLEscapeString(ex.Message);
                return PartialView("_SysException");
            }
        }

        [HttpGet]
        public JsonResult GetRegulatoryListData(jQueryDataTableParamModel param)
        {
            List<RegulatoryAgencyBO> objRegulatoryAgencyList = new List<RegulatoryAgencyBO>();
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    var objRegulatoryAgencyData = dbAizantIT_QMS_Audit.AizantIT_SP_GetRegulatoryList(param.iDisplayLength,
                    param.iDisplayStart,
                    param.iSortCol_0,
                    param.sSortDir_0,
                    param.sSearch != null ? param.sSearch.Trim() : "");
                    foreach (var item in objRegulatoryAgencyData)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objRegulatoryAgencyList.Add(new RegulatoryAgencyBO((int)item.RegulatoryAgencyID, item.RegulatoryAgencyCode,
                            item.RegulatoryAgencyName,item.RegionName,(bool)item.IsActive,
                            item.CreatedByName, item.CreatedDate, item.ModifiedByName, item.ModifiedDate));
                    }
                    return Json(new
                    {
                        iTotalRecords = totalRecordsCount,
                        iTotalDisplayRecords = totalRecordsCount,
                        aaData = objRegulatoryAgencyList
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult RegulatoryAgencyModify(RegulatoryAgencyBO objRegulatoryDetails)
        {
            try
            {
                bool IsNameOrCodeChanged = false;
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbAizantIT_QMS_Audit.Database.BeginTransaction())
                    {
                        try
                        {
                            AizantIT_RegulatoryAgency dtAizantIT_RegulatoryAgency =
                            dbAizantIT_QMS_Audit.AizantIT_RegulatoryAgency.SingleOrDefault(p => p.RegulatoryAgencyID == objRegulatoryDetails.RegulatoryAgencyID);
                            if (dtAizantIT_RegulatoryAgency.RegulatoryAgencyCode != objRegulatoryDetails.RegulatoryAgencyCode 
                                || dtAizantIT_RegulatoryAgency.RegulatoryAgencyName != objRegulatoryDetails.RegulatoryAgencyName 
                                || dtAizantIT_RegulatoryAgency.IsActive != objRegulatoryDetails.IsActive 
                                || dtAizantIT_RegulatoryAgency.Region != objRegulatoryDetails.RegionName)
                            {
                                IsNameOrCodeChanged = true;
                            }
                            if (IsNameOrCodeChanged)
                            {
                                bool IsRecordExists = (from itemRegulatoryAgency in dbAizantIT_QMS_Audit.AizantIT_RegulatoryAgency
                                                       where (itemRegulatoryAgency.RegulatoryAgencyCode == objRegulatoryDetails.RegulatoryAgencyCode
                                                       || itemRegulatoryAgency.RegulatoryAgencyName == objRegulatoryDetails.RegulatoryAgencyName
                                                       || itemRegulatoryAgency.Region == objRegulatoryDetails.RegionName)
                                                       && itemRegulatoryAgency.RegulatoryAgencyID != objRegulatoryDetails.RegulatoryAgencyID
                                                       select new { itemRegulatoryAgency.RegulatoryAgencyName,
                                                           itemRegulatoryAgency.RegulatoryAgencyCode }).Any();
                                if (!IsRecordExists)
                                {
                                    dtAizantIT_RegulatoryAgency.RegulatoryAgencyCode = objRegulatoryDetails.RegulatoryAgencyCode;
                                    dtAizantIT_RegulatoryAgency.RegulatoryAgencyName = objRegulatoryDetails.RegulatoryAgencyName;
                                    dtAizantIT_RegulatoryAgency.Region = objRegulatoryDetails.RegionName;
                                    dtAizantIT_RegulatoryAgency.IsActive = objRegulatoryDetails.IsActive;
                                    dtAizantIT_RegulatoryAgency.ModifiedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                    dtAizantIT_RegulatoryAgency.ModifiedDate = DateTime.Now;
                                    dbAizantIT_QMS_Audit.SaveChanges();
                                    transaction.Commit();
                                    return Json(new
                                    {
                                        msg = "Regulatory Agency Updated.",
                                        msgType = "success"                                       
                                    }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    return Json(new { msg = "Regulatory Agency Already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json(new { msg = "There are no changes to update", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message);
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Type Of
        [HttpPost]
        public JsonResult TypeOfCreate(int TypeOfRepresents, string TypeOfName)
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbAizantIT_QMS_Audit.Database.BeginTransaction())
                    {
                        try
                        {
                            bool IsRecordExists = (from itemTypeOF in dbAizantIT_QMS_Audit.AizantIT_TypeOf
                                                   where itemTypeOF.TypeOfName == TypeOfName
                                                   select new { itemTypeOF.TypeOfID }).Any();
                            if (!IsRecordExists)
                            {
                                AizantIT_TypeOf objAizantIT_TypeOf = new AizantIT_TypeOf();
                                objAizantIT_TypeOf.TypeOfName = TypeOfName;
                                objAizantIT_TypeOf.TypeOfRepresents = TypeOfRepresents;
                                objAizantIT_TypeOf.IsActive = true;
                                objAizantIT_TypeOf.CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                objAizantIT_TypeOf.CreatedDate = DateTime.Now;
                                dbAizantIT_QMS_Audit.AizantIT_TypeOf.Add(objAizantIT_TypeOf);
                                dbAizantIT_QMS_Audit.SaveChanges();
                                transaction.Commit();
                                return Json(new { msg = "Type of Audit Created.", msgType = "success" }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { msg = "Category Name should be Unique as per Type Of Audit.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message);
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetTypeOfList()
        {
            try
            {
                return PartialView();
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = HelpClass.SQLEscapeString(ex.Message);
                return PartialView("_SysException");
            }
        }
        [HttpPost]
        public JsonResult TypeOfModify(TypeOfBO objTypeOfData)
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbAizantIT_QMS_Audit.Database.BeginTransaction())
                    {
                        try
                        {
                            bool IsNameOrActiveStatusChanged = false;
                            AizantIT_TypeOf dtAizantIT_TypeOf =
                                    dbAizantIT_QMS_Audit.AizantIT_TypeOf.SingleOrDefault(p => p.TypeOfID == objTypeOfData.TypeOfID);
                            if ((dtAizantIT_TypeOf.TypeOfName != objTypeOfData.TypeOfName 
                                && dtAizantIT_TypeOf.TypeOfRepresents == objTypeOfData.TypeOfRepresents) 
                                || dtAizantIT_TypeOf.IsActive != objTypeOfData.IsActive)
                            {
                                IsNameOrActiveStatusChanged = true;
                            }
                            if (IsNameOrActiveStatusChanged)
                            {
                                bool IsRecordExists = (from itemTypeOf in dbAizantIT_QMS_Audit.AizantIT_TypeOf
                                                       where (itemTypeOf.TypeOfName == objTypeOfData.TypeOfName) 
                                                       && itemTypeOf.TypeOfID != objTypeOfData.TypeOfID
                                                       select new { itemTypeOf.TypeOfID }).Any();
                                if (!IsRecordExists)
                                {
                                    dtAizantIT_TypeOf.TypeOfName = objTypeOfData.TypeOfName;
                                    dtAizantIT_TypeOf.IsActive = objTypeOfData.IsActive;
                                    dtAizantIT_TypeOf.IsActive = objTypeOfData.IsActive;
                                    dtAizantIT_TypeOf.ModifiedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                    dtAizantIT_TypeOf.ModifiedDate = DateTime.Now;
                                    dbAizantIT_QMS_Audit.SaveChanges();
                                    transaction.Commit();
                                    return Json(new
                                    {                                       
                                        msgType = "success"                                      
                                    }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    return Json(new { msg = "Category Name Already Exists in this Type of Audit.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json(new { msg = "There are no Changes to Update.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message);
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetTypeOfListData(jQueryDataTableParamModel param)
        {
            List<TypeOfBO> objTypeOfList = new List<TypeOfBO>();
            try
            {
               
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {                   
                    var objTypeOfListData = dbAizantIT_QMS_Audit.AizantIT_SP_GetTypeOfList(param.iDisplayLength,
                    param.iDisplayStart,
                    param.iSortCol_0,
                    param.sSortDir_0,
                    param.sSearch != null ? param.sSearch.Trim() : "");
                    foreach (var item in objTypeOfListData)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objTypeOfList.Add(new TypeOfBO((int)item.TypeOfID,item.TypeOfName,
                            (int)item.TypeOfRepresents,(bool)item.IsActive,
                            item.CreatedByName,item.CreatedDate,item.ModifiedByName,item.ModifiedDate));
                    }
                    return Json(new
                    {
                        iTotalRecords = totalRecordsCount,
                        iTotalDisplayRecords = totalRecordsCount,
                        aaData = objTypeOfList
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Auditor Details
        public ActionResult GetAuditorDetailsList()
        {
            try
            {
                return PartialView();
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = HelpClass.SQLEscapeString(ex.Message);
                return PartialView("_SysException");
            }
        }
        [HttpPost]
        public JsonResult AuditorCreate(int AuditorCategoryID, int AuditorBaseID, string AuditorName, string AuditorEmailID, string AuditorContactNo)
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbAizantIT_QMS_Audit.Database.BeginTransaction())
                    {
                        try
                        {
                            long ContactNumber = Convert.ToInt64(AuditorContactNo);
                            bool IsContactExists = (from itemAuditor in dbAizantIT_QMS_Audit.AizantIT_AuditorDetails
                                                    where itemAuditor.ContactNum == ContactNumber
                                                    select new { itemAuditor.AuditorID }).Any();

                            bool IsAuthorNameExists = (from itemAuditor in dbAizantIT_QMS_Audit.AizantIT_AuditorDetails
                                                       where itemAuditor.AuditorName == AuditorName
                                                       select new { itemAuditor.AuditorID }).Any();

                            bool IsEmailExists = (from itemAuditor in dbAizantIT_QMS_Audit.AizantIT_AuditorDetails
                                                  where itemAuditor.Email_ID == AuditorEmailID
                                                  select new { itemAuditor.AuditorID }).Any();

                            if (IsEmailExists && IsAuthorNameExists && IsContactExists)
                            {
                                return Json(new { msg = "Auditor Name ,Email ID and Contact number already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                            else if (IsContactExists && IsEmailExists)
                            {
                                return Json(new { msg = "Email ID and Contact number already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                            else if (IsContactExists && IsAuthorNameExists)
                            {
                                return Json(new { msg = "Auditor Name and Contact number already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                            else if (IsEmailExists && IsAuthorNameExists)
                            {
                                return Json(new { msg = "Auditor Name and Email ID already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                            else if (IsContactExists)
                            {
                                return Json(new { msg = "Contact number already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                            else if (IsAuthorNameExists)
                            {
                                return Json(new { msg = "Auditor Name already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                            else if (IsEmailExists)
                            {
                                return Json(new { msg = "Email ID already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                AizantIT_AuditorDetails objAizantIT_Auditor = new AizantIT_AuditorDetails();
                                objAizantIT_Auditor.BaseType = AuditorCategoryID;
                                objAizantIT_Auditor.BaseID = AuditorBaseID;
                                objAizantIT_Auditor.AuditorName = AuditorName;
                                objAizantIT_Auditor.Email_ID = AuditorEmailID;
                                objAizantIT_Auditor.ContactNum = ContactNumber;
                                objAizantIT_Auditor.CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                objAizantIT_Auditor.CreatedDate = DateTime.Now;
                                dbAizantIT_QMS_Audit.AizantIT_AuditorDetails.Add(objAizantIT_Auditor);
                                dbAizantIT_QMS_Audit.SaveChanges();
                                transaction.Commit();
                                return Json(new { msg = "Auditor Created.", msgType = "success" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message);
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult OpenCreateAuditor(string CreateOrEdit="")
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    // For Loading the RegulatoryAgency ddl
                    List<SelectListItem> RegulatoryAgencyList = new List<SelectListItem>();
                    List<SelectListItem> AuditorClientList = new List<SelectListItem>();
                    if (CreateOrEdit=="Edit")
                    {
                        var  RegulatoryAgencyAllItemsList = (from objAuditRegulatoryAgency in dbAizantIT_QMS_Audit.AizantIT_RegulatoryAgency
                                                     select new { objAuditRegulatoryAgency.RegulatoryAgencyID, RegulatoryAgencyName = (objAuditRegulatoryAgency.RegulatoryAgencyCode + " - " + objAuditRegulatoryAgency.RegulatoryAgencyName) }).ToList();
                        foreach (var RegulatoryAgencyItem in RegulatoryAgencyAllItemsList)
                        {
                            RegulatoryAgencyList.Add(new SelectListItem
                            {
                                Text = RegulatoryAgencyItem.RegulatoryAgencyName,
                                Value = RegulatoryAgencyItem.RegulatoryAgencyID.ToString(),
                            });
                        }
                        // For Loading the Client ddl
                        var ClientAllItemsList = (from objAuditClient in dbAizantIT_QMS_Audit.AizantIT_Client
                                               select new { objAuditClient.ClientID, objAuditClient.ClientName }).ToList();
                        foreach (var ClientItem in ClientAllItemsList)
                        {
                            AuditorClientList.Add(new SelectListItem
                            {
                                Text = ClientItem.ClientName,
                                Value = ClientItem.ClientID.ToString(),
                            });
                        }
                    }
                    else
                    {
                      var  RegulatoryAgencyOnlyActiveItemsList = (from objAuditRegulatoryAgency in dbAizantIT_QMS_Audit.AizantIT_RegulatoryAgency
                                                     where objAuditRegulatoryAgency.IsActive == true
                                                     select new { objAuditRegulatoryAgency.RegulatoryAgencyID, RegulatoryAgencyName = (objAuditRegulatoryAgency.RegulatoryAgencyCode + " - " + objAuditRegulatoryAgency.RegulatoryAgencyName) }).ToList();
                        foreach (var RegulatoryAgencyItem in RegulatoryAgencyOnlyActiveItemsList)
                        {
                            RegulatoryAgencyList.Add(new SelectListItem
                            {
                                Text = RegulatoryAgencyItem.RegulatoryAgencyName,
                                Value = RegulatoryAgencyItem.RegulatoryAgencyID.ToString(),
                            });
                        }
                        var ClientOnlyActiveItemsList = (from objAuditClient in dbAizantIT_QMS_Audit.AizantIT_Client
                                                      where objAuditClient.IsActive == true
                                                  select new { objAuditClient.ClientID, objAuditClient.ClientName }).ToList();
                        foreach (var ClientItem in ClientOnlyActiveItemsList)
                        {
                            AuditorClientList.Add(new SelectListItem
                            {
                                Text = ClientItem.ClientName,
                                Value = ClientItem.ClientID.ToString(),
                            });
                        }
                    }
                    ViewBag.RegulatoryAgencyList = new SelectList(RegulatoryAgencyList.ToList(), "Value", "Text");
                    ViewBag.AuditorClientList = new SelectList(AuditorClientList.ToList(), "Value", "Text");
                    ViewBag.CreateOrEdit = CreateOrEdit;
                    return PartialView();
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = HelpClass.SQLEscapeString(ex.Message);
                return PartialView("_SysException");
            }
        }
        [HttpGet]
        public JsonResult GetAuditorsList()
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            List<AuditorDetailsBO> objAuditorsList = new List<AuditorDetailsBO>();
            try
            {
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    int totalRecordsCount = 0;
                    var ObjAuditorsList = dbAizantIT_QMS_Audit.AizantIT_SP_GetAuditorDetailsList(displayLength,
                            displayStart,
                            sortCol,
                            sSortDir,
                            sSearch != null ? sSearch.Trim() : "");
                    foreach (var item in ObjAuditorsList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objAuditorsList.Add(new AuditorDetailsBO(Convert.ToInt32(item.AuditorID),
                            Convert.ToInt32(item.RegulatoryAgencyOrClientID),
                           item.AuditorName,
                           item.AuditorEmailID,
                           item.AuditorContactNumber,
                           item.CreatedByName,
                           item.CreatedDate,
                           item.ModifiedByName,
                           item.ModifiedDate,
                           item.AuditorCategory,
                           item.AgencyOrClientName
                            ));
                    }
                    return Json(new
                    {
                        iTotalRecords = totalRecordsCount,
                        iTotalDisplayRecords = totalRecordsCount,
                        aaData = objAuditorsList
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult AuditorDetailsModify(int AuditorID, string AuditorName, string AuditorEmailID, string AuditorContactNo)
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbAizantIT_QMS_Audit.Database.BeginTransaction())
                    {
                        try
                        {
                            bool IsNameOrEmailorContactChanged = false;
                            AizantIT_AuditorDetails dtAizantIT_AuditorDetails =
                                    dbAizantIT_QMS_Audit.AizantIT_AuditorDetails.SingleOrDefault(p => p.AuditorID == AuditorID);
                            if (dtAizantIT_AuditorDetails.AuditorName != AuditorName || dtAizantIT_AuditorDetails.Email_ID != AuditorEmailID || dtAizantIT_AuditorDetails.ContactNum != Convert.ToInt64(AuditorContactNo))
                            {
                                IsNameOrEmailorContactChanged = true;
                            }
                            if (IsNameOrEmailorContactChanged)
                            {
                                long AuditorContactNumber = Convert.ToInt64(AuditorContactNo);
                                bool IsContactExists = (from itemAuditor in dbAizantIT_QMS_Audit.AizantIT_AuditorDetails
                                                        where (itemAuditor.ContactNum == AuditorContactNumber)
                                                         && itemAuditor.AuditorID != AuditorID
                                                        select new { itemAuditor.ContactNum }).Any();

                                bool IsAuthorNameExists = (from itemAuditor in dbAizantIT_QMS_Audit.AizantIT_AuditorDetails
                                                           where (itemAuditor.AuditorName == AuditorName)
                                                           && itemAuditor.AuditorID != AuditorID
                                                           select new { itemAuditor.AuditorName }).Any();

                                bool IsEmailExists = (from itemAuditor in dbAizantIT_QMS_Audit.AizantIT_AuditorDetails
                                                      where (itemAuditor.Email_ID == AuditorEmailID)
                                                      && itemAuditor.AuditorID != AuditorID
                                                      select new { itemAuditor.Email_ID }).Any();
                                if (IsEmailExists && IsAuthorNameExists && IsContactExists)
                                {
                                    return Json(new { msg = "Auditor Name , Email ID and Contact number already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                }
                                else if (IsContactExists && IsEmailExists)
                                {
                                    return Json(new { msg = "Email ID and Contact number already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                }
                                else if (IsContactExists && IsAuthorNameExists)
                                {
                                    return Json(new { msg = "Auditor Name and Contact number already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                }
                                else if (IsEmailExists && IsAuthorNameExists)
                                {
                                    return Json(new { msg = "Auditor Name and Email ID already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                }
                                else if (IsContactExists)
                                {
                                    return Json(new { msg = "Contact number already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                }
                                else if (IsAuthorNameExists)
                                {
                                    return Json(new { msg = "Auditor Name already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                }
                                else if (IsEmailExists)
                                {
                                    return Json(new { msg = "Email ID already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    dtAizantIT_AuditorDetails.AuditorName = AuditorName;
                                    dtAizantIT_AuditorDetails.Email_ID = AuditorEmailID;
                                    dtAizantIT_AuditorDetails.ContactNum = AuditorContactNumber;
                                    dtAizantIT_AuditorDetails.ModifiedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                    dtAizantIT_AuditorDetails.ModifiedDate = DateTime.Now;
                                    dbAizantIT_QMS_Audit.SaveChanges();
                                    transaction.Commit();
                                    return Json(new
                                    {
                                        msg = "Auditor Details Updated.",
                                        msgType = "success",
                                        ModifiedByName = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString()
                                        ,
                                        ModifiedDate = DateTime.Now.ToString("dd MMM yyyy HH:mm:ss")
                                    }, JsonRequestBehavior.AllowGet);
                                }

                            }
                            else
                            {
                                return Json(new { msg = "There are no changes to Update.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message);
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Observer
        public ActionResult GetObserverDetailsList()
        {
            return PartialView();
        }

        public ActionResult GetObserverList()
        {
            try
            {
                List<ObserverDetailsBO> ObserverList = new List<ObserverDetailsBO>();
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    int totalRecordsCount = 0;
                    int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                    int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                    int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
                    string sSortDir = Request.Params["sSortDir_0"];
                    string sSearch = Request.Params["sSearch"];
                    
                    var ObserverDetailsList = dbAizantIT_QMS_Audit.AizantIT_SP_GetObserverDetailsList(displayLength, displayStart, sortCol, sSortDir, sSearch);
                    foreach (var ObserverItem in ObserverDetailsList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(ObserverItem.TotalCount);
                        }
                        ObserverList.Add(new ObserverDetailsBO
                       (
                                Convert.ToInt32(ObserverItem.ObserverID),
                                ObserverItem.ObserverName,
                                ObserverItem.ObserverEmailID,
                                ObserverItem.ObserverContactNumber,
                                ObserverItem.ObserverAdditionalDetails,
                                ObserverItem.ObserverCategory,
                                ObserverItem.ClientOrRegulatoryID,
                                ObserverItem.CreatedByName,
                                ObserverItem.CreatedDate,
                                ObserverItem.ModifiedByName,
                                ObserverItem.ModifiedDate,
                                 Convert.ToInt32(ObserverItem.BaseType),
                                Convert.ToInt32(ObserverItem.BaseID)
                        ));
                    }
                    return Json(new
                    {
                        iTotalRecords = totalRecordsCount,
                        iTotalDisplayRecords = totalRecordsCount,
                        aaData = ObserverList
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = HelpClass.SQLEscapeString(ex.Message);
                return PartialView("_SysException");
            }
        }
        public ActionResult OpenCreateObserver(int ObserverID=0)
        {
            try
            {
               return PartialView();
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = HelpClass.SQLEscapeString(ex.Message);
                return PartialView("_SysException");
            }
        }
        [HttpPost]
        //int ObserverCategoryID, int ObserverBaseID, string ObserverName, string ObserverEmailID, string ObserverContactNo, string ObserverAdditionalDetails = null
        public JsonResult ObserverCreate(ObserveMaster auditObservData)
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbAizantIT_QMS_Audit.Database.BeginTransaction())
                    {
                        try
                        {
                            long ObserverContactNumber = Convert.ToInt64(auditObservData.ObserverContactNo);
                            bool IsContactExists = (from itemObserver in dbAizantIT_QMS_Audit.AizantIT_ObserverDetails
                                                    where itemObserver.ContactNum == ObserverContactNumber
                                                    select new { itemObserver.ObserverID }).Any();

                            bool IsAuthorNameExists = (from itemObserver in dbAizantIT_QMS_Audit.AizantIT_ObserverDetails
                                                       where itemObserver.ObserverName == auditObservData.ObserverName
                                                       select new { itemObserver.ObserverID }).Any();

                            bool IsEmailExists = (from itemObserver in dbAizantIT_QMS_Audit.AizantIT_ObserverDetails
                                                  where itemObserver.Email_ID == auditObservData.ObserverEmailID
                                                  select new { itemObserver.ObserverID }).Any();

                            if (IsEmailExists && IsAuthorNameExists && IsContactExists)
                            {
                                return Json(new { msg = "Observer Name , Email ID and Contact number already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                            else if (IsContactExists && IsEmailExists)
                            {
                                return Json(new { msg = "Email ID and Contact number already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                            else if (IsContactExists && IsAuthorNameExists)
                            {
                                return Json(new { msg = "Observer Name and Contact number already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                            else if (IsEmailExists && IsAuthorNameExists)
                            {
                                return Json(new { msg = "Observer Name and Email ID already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                            else if (IsContactExists)
                            {
                                return Json(new { msg = "Contact number already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                            else if (IsAuthorNameExists)
                            {
                                return Json(new { msg = "Observer Name already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                            else if (IsEmailExists)
                            {
                                return Json(new { msg = "Email ID already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                AizantIT_ObserverDetails objAizantIT_Observer = new AizantIT_ObserverDetails();
                                objAizantIT_Observer.BaseType = auditObservData.ObserverCategoryID;
                                objAizantIT_Observer.BaseID = auditObservData.ObserverBaseID;
                                objAizantIT_Observer.ObserverName = auditObservData.ObserverName;
                                objAizantIT_Observer.Email_ID = auditObservData.ObserverEmailID;
                                objAizantIT_Observer.ContactNum = ObserverContactNumber;
                                objAizantIT_Observer.AdditionalDetails = auditObservData.ObserverAdditionalDetails == "" ? null : auditObservData.ObserverAdditionalDetails;
                                objAizantIT_Observer.CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                objAizantIT_Observer.CreatedDate = DateTime.Now;
                                dbAizantIT_QMS_Audit.AizantIT_ObserverDetails.Add(objAizantIT_Observer);
                                dbAizantIT_QMS_Audit.SaveChanges();
                                transaction.Commit();
                                return Json(new { msg = "Observer Created.", msgType = "success" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message);
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult ObserverDetailsModify(ObserveMaster auditObservData)
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbAizantIT_QMS_Audit.Database.BeginTransaction())
                    {
                        try
                        {
                            //bool IsNameOrEmailorContactOrAdditionalDetailsChanged = false;
                            AizantIT_ObserverDetails dtAizantIT_ObserverDetails =
                                   dbAizantIT_QMS_Audit.AizantIT_ObserverDetails.SingleOrDefault(p => p.ObserverID == auditObservData.ObserverID);
                                long ObserverContactNumber = Convert.ToInt64(auditObservData.ObserverContactNo);
                                bool IsContactExists = (from itemObserver in dbAizantIT_QMS_Audit.AizantIT_ObserverDetails
                                                        where (itemObserver.ContactNum == ObserverContactNumber)
                                                        && itemObserver.ObserverID != auditObservData.ObserverID
                                                        select new { itemObserver.ContactNum }).Any();

                                bool IsAuthorNameExists = (from itemObserver in dbAizantIT_QMS_Audit.AizantIT_ObserverDetails
                                                           where (itemObserver.ObserverName == auditObservData.ObserverName)
                                                           && itemObserver.ObserverID != auditObservData.ObserverID
                                                           select new { itemObserver.ObserverName }).Any();

                                bool IsEmailExists = (from itemObserver in dbAizantIT_QMS_Audit.AizantIT_ObserverDetails
                                                      where (itemObserver.Email_ID == auditObservData.ObserverEmailID)
                                                      && itemObserver.ObserverID != auditObservData.ObserverID
                                                      select new { itemObserver.Email_ID }).Any();

                                if (IsEmailExists && IsAuthorNameExists && IsContactExists)
                                {
                                    return Json(new { msg = "Observer Name ,Email ID and Contact number already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                }
                                else if (IsContactExists && IsEmailExists)
                                {
                                    return Json(new { msg = "Email ID and Contact number already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                }
                                else if (IsContactExists && IsAuthorNameExists)
                                {
                                    return Json(new { msg = "Observer Name and Contact number already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                }
                                else if (IsEmailExists && IsAuthorNameExists)
                                {
                                    return Json(new { msg = "Observer Name and Email ID already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                }
                                else if (IsContactExists)
                                {
                                    return Json(new { msg = "Contact number already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                }
                                else if (IsAuthorNameExists)
                                {
                                    return Json(new { msg = "Observer Name already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                }
                                else if (IsEmailExists)
                                {
                                    return Json(new { msg = "Email ID already Exists.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    dtAizantIT_ObserverDetails.ObserverName = auditObservData.ObserverName;
                                    dtAizantIT_ObserverDetails.Email_ID = auditObservData.ObserverEmailID;
                                    dtAizantIT_ObserverDetails.ContactNum = ObserverContactNumber;
                                    dtAizantIT_ObserverDetails.AdditionalDetails = auditObservData.ObserverAdditionalDetails == "" ? null : auditObservData.ObserverAdditionalDetails;
                                    dtAizantIT_ObserverDetails.ModifiedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                    dtAizantIT_ObserverDetails.ModifiedDate = DateTime.Now;
                                    dbAizantIT_QMS_Audit.SaveChanges();
                                    transaction.Commit();
                                    return Json(new
                                    {
                                        msg = "Observer Details Updated.",
                                        msgType = "success",                                       
                                    }, JsonRequestBehavior.AllowGet);
                                }                          
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message);
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        public JsonResult LoadRegulatoryList()
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    // For Loading the RegulatoryAgency ddl
                    List<SelectListItem> RegulatoryAgencyList = new List<SelectListItem>();
                    var RegulatoryAgencyItemsList = (from objAuditRegulatoryAgency in dbAizantIT_QMS_Audit.AizantIT_RegulatoryAgency
                                                     where objAuditRegulatoryAgency.IsActive == true
                                                     select new { objAuditRegulatoryAgency.RegulatoryAgencyID, RegulatoryAgencyName = (objAuditRegulatoryAgency.RegulatoryAgencyCode + " - " + objAuditRegulatoryAgency.RegulatoryAgencyName) }).ToList();
                    foreach (var RegulatoryAgencyItem in RegulatoryAgencyItemsList)
                    {
                        RegulatoryAgencyList.Add(new SelectListItem
                        {
                            Text = RegulatoryAgencyItem.RegulatoryAgencyName,
                            Value = RegulatoryAgencyItem.RegulatoryAgencyID.ToString(),
                        });
                    }
                    return Json(RegulatoryAgencyList, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public JsonResult LoadClientList()
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    // For Loading the Client ddl 
                    List<SelectListItem> AuditorClientList = new List<SelectListItem>();
                    var ClientItemsList = (from objAuditClient in dbAizantIT_QMS_Audit.AizantIT_Client
                                           where objAuditClient.IsActive == true
                                           select new { objAuditClient.ClientID, objAuditClient.ClientName }).ToList();
                    foreach (var ClientItem in ClientItemsList)
                    {
                        AuditorClientList.Add(new SelectListItem
                        {
                            Text = ClientItem.ClientName,
                            Value = ClientItem.ClientID.ToString(),
                        });
                    }
                    return Json(AuditorClientList, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #region Audit Area
        [HttpGet]
        public ActionResult GetAuditAreaList()
        {
            try
            {
                return PartialView();
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = HelpClass.SQLEscapeString(ex.Message);
                return PartialView("_SysException");
            }
        }
        [HttpGet]
        public JsonResult GetAuditAreaDetails(jQueryDataTableParamModel param)
        {
            List<AuditArea> objAuditArea = new List<AuditArea>();
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    var objAuditAreaList = dbAizantIT_QMS_Audit.AizantIT_SP_GetAuditAreaList(param.iDisplayLength,
                    param.iDisplayStart,
                    param.iSortCol_0,
                    param.sSortDir_0,
                    param.sSearch != null ? param.sSearch.Trim() : "");
                    foreach (var item in objAuditAreaList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objAuditArea.Add(new AuditArea(Convert.ToInt32(item.AuditAreaID), item.AuditAreaName, item.AuditSubArea,
                             item.CreatedBy, item.CreatedDate, item.ModifiedBy, item.ModifiedDate));
                    }
                    return Json(new
                    {
                        iTotalRecords = totalRecordsCount,
                        iTotalDisplayRecords = totalRecordsCount,
                        aaData = objAuditArea
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetListOfSubAreas(string AuditAreaID)
        {
            try
            {
                List<AuditSubArea> objAuditSubAreas = new List<AuditSubArea>();
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    var objAuditSubAreasListResult = dbAizantIT_QMS_Audit.AizantIT_SP_GetAuditSubAreaList(Convert.ToInt32(AuditAreaID));
                    foreach (var item in objAuditSubAreasListResult)
                    {
                        objAuditSubAreas.Add(new AuditSubArea(item.SubAreaID, item.SubAreaName, item.IsActive == true ? "Yes" : "No",
                            item.CreatedBy, item.CreatedDate,
                            item.ModifiedBy, item.ModifiedDate));
                    }
                    return Json(new { data = objAuditSubAreas, msgType = "success" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message);
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult CreateAuditArea(AuditAreaPost auditAreaPost)
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbAizantIT_QMS_Audit.Database.BeginTransaction())
                    {
                        try
                        {
                            bool IsAuditAreaExists = (from itemAuditArea in dbAizantIT_QMS_Audit.AizantIT_Area
                                                      where itemAuditArea.AreaName == auditAreaPost.AuditAreaName
                                                      select itemAuditArea.AreaID).Any();
                            if (!IsAuditAreaExists)
                            {
                                DataTable dtAuditSubAreas = new DataTable();
                                dtAuditSubAreas.Columns.Add("AuditAreaName");
                                foreach (var item in auditAreaPost.AuditSubAreaNames)
                                {
                                    dtAuditSubAreas.Rows.Add(item);
                                }
                                int CreatedByEmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                SqlParameter ParamAuditAreaName = new SqlParameter("@AuditAreaName", auditAreaPost.AuditAreaName);
                                SqlParameter ParamCreatedBy = new SqlParameter("@CreatedBy", CreatedByEmpID);
                                SqlParameter ParamAuditSubArea = new SqlParameter("@AuditSubArea", dtAuditSubAreas);
                                ParamAuditSubArea.TypeName = "QMS_Audit.AizantIT_TVP_AuditSubArea";
                                dbAizantIT_QMS_Audit.Database.ExecuteSqlCommand("exec [QMS_Audit].AizantIT_SP_CreateAuditArea " +
                                    "@AuditAreaName,@CreatedBy,@AuditSubArea", ParamAuditAreaName, ParamCreatedBy, ParamAuditSubArea);
                                transaction.Commit();
                                return Json(new { msg = "Audit Area Created", msgType = "success" }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { msg = "Audit Area Name Already Exists", msgType = "error" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message);
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult UpdateAuditAreaName(AuditArea ModifyAuditAreaName)
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbAizantIT_QMS_Audit.Database.BeginTransaction())
                    {
                        try
                        {
                            bool IsAuditAreaNameExistsInAnotherAreas = (from item in dbAizantIT_QMS_Audit.AizantIT_Area
                                                                        where item.AreaName == ModifyAuditAreaName.AuditAreaName && item.AreaID != ModifyAuditAreaName.AuditAreaID
                                                                        select item.AreaName).Any();
                            if (!IsAuditAreaNameExistsInAnotherAreas)
                            {
                                var IsAuditAreaUpdated = (from item in dbAizantIT_QMS_Audit.AizantIT_Area
                                                          where item.AreaID == ModifyAuditAreaName.AuditAreaID
                                                          select new { item.AreaName }).SingleOrDefault();
                                // bool checkActive = ModifyAuditAreaName.IsActive == 1 ? true : false;
                                if (IsAuditAreaUpdated.AreaName != ModifyAuditAreaName.AuditAreaName)
                                {
                                    bool IsAuditSubAreaExists = (from item in dbAizantIT_QMS_Audit.AizantIT_SubArea
                                                                 where item.AreaID == ModifyAuditAreaName.AuditAreaID
                                                                 && item.SubAreaName == ModifyAuditAreaName.AuditAreaName
                                                                 select item.SubAreaName).Any();
                                    if (!IsAuditSubAreaExists)
                                    {
                                        var objAuditArea = dbAizantIT_QMS_Audit.AizantIT_Area.First(p => p.AreaID == ModifyAuditAreaName.AuditAreaID);
                                        objAuditArea.AreaName = ModifyAuditAreaName.AuditAreaName;
                                        //objAuditAreatem.IsActive = checkActive;
                                        objAuditArea.ModifiedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                        objAuditArea.ModifiedDate = DateTime.Now;
                                        dbAizantIT_QMS_Audit.SaveChanges();
                                        transaction.Commit();
                                        return Json(new { msg = "Audit Area Updated.", msgType = "success" }, JsonRequestBehavior.AllowGet);
                                    }
                                    else
                                    {
                                        return Json(new { msg = "Audit Area Name and Sub-Area Name Should not be Same.", msgType = "error" }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                                else
                                {
                                    return Json(new { msg = "There are no changes to Update.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json(new { msg = "Audit Area Name already exist ", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message);
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult UpdateAuditSubAreas(string AuditAreaID, string AuditSubAreaID, string AuditSubAreaName, bool IsActive)
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbAizantIT_QMS_Audit.Database.BeginTransaction())
                    {
                        try
                        {
                            int iAuditSubAreaID = Convert.ToInt32(AuditSubAreaID);
                            int iAuditAreaID = Convert.ToInt32(AuditAreaID);
                            bool IsAuditSubAreaExists = (from item in dbAizantIT_QMS_Audit.AizantIT_SubArea
                                                         where item.SubAreaName == AuditSubAreaName
                                                         && item.AreaID == iAuditAreaID
                                                         && item.SubAreaID != iAuditSubAreaID
                                                         select item.SubAreaName).Any();
                            if (!IsAuditSubAreaExists)
                            {
                                bool IsAuditAreaSubAreaName = (from item in dbAizantIT_QMS_Audit.AizantIT_Area
                                                               where item.AreaID == iAuditAreaID
                                                               && item.AreaName == AuditSubAreaName
                                                               select item.AreaName).Any();
                                if (!IsAuditAreaSubAreaName)
                                {
                                    var updateObj = dbAizantIT_QMS_Audit.AizantIT_SubArea.First(p => p.SubAreaID == iAuditSubAreaID);
                                    updateObj.SubAreaName = AuditSubAreaName;
                                    updateObj.IsActive = IsActive;
                                    updateObj.ModifiedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                    updateObj.ModifiedDate = DateTime.Now;

                                    string _ModifiedByEmpName = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString();
                                    string _ModifiedDate = DateTime.Now.ToString("dd MMM yyyy hh:mm:ss");
                                    var updateAuditObj = dbAizantIT_QMS_Audit.AizantIT_Area.First(p => p.AreaID == iAuditAreaID);
                                    updateAuditObj.ModifiedBy = updateObj.ModifiedBy;
                                    updateAuditObj.ModifiedDate = updateObj.ModifiedDate;
                                    dbAizantIT_QMS_Audit.SaveChanges();
                                    transaction.Commit();
                                    var dataResult = new { ModifiedBy = _ModifiedByEmpName, ModifiedDate = _ModifiedDate };
                                    return Json(new { msg = "Audit Sub Area Updated.", msgType = "success", data = dataResult }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    return Json(new { msg = "Audit Area Name and Sub-Area Name Should not be Same.", msgType = "error" }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json(new { msg = "Audit Sub-Area Name Should be Unique", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message);
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult AddSubAreasForAuditArea(AizantIT_SubArea objAuditSubArea)
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbAizantIT_QMS_Audit.Database.BeginTransaction())
                    {
                        try
                        {
                            bool IsSubAreaExists = (from item in dbAizantIT_QMS_Audit.AizantIT_SubArea
                                                    where item.AreaID == objAuditSubArea.AreaID
                                                    && item.SubAreaName == objAuditSubArea.SubAreaName
                                                    select item.SubAreaID).Any();
                            if (!IsSubAreaExists)
                            {
                                bool IsSubAreaMatchAuditArea = (from item in dbAizantIT_QMS_Audit.AizantIT_Area
                                                                where item.AreaID == objAuditSubArea.AreaID
                                                                && item.AreaName == objAuditSubArea.SubAreaName
                                                                select item.AreaName).Any();
                                if (!IsSubAreaMatchAuditArea)
                                {
                                    objAuditSubArea.CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                    objAuditSubArea.CreatedDate = DateTime.Now;
                                    dbAizantIT_QMS_Audit.AizantIT_SubArea.Add(objAuditSubArea);
                                    dbAizantIT_QMS_Audit.SaveChanges();
                                    transaction.Commit();
                                    return Json(new { msg = "Sub-Area Added", msgType = "success" }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    return Json(new { msg = "Audit Area Name and Sub-Area Name Should not be Same.", msgType = "error" }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json(new { msg = "Sub-Area Name Should be Unique", msgType = "error" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message);
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region FDA6 System
        [HttpGet]
        public ActionResult _GetFDA6SystemList()
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    // For Loading the Area ddl
                    List<SelectListItem> AreaList = new List<SelectListItem>();
                    var AreaItemsList = (from objAuditArea in dbAizantIT_QMS_Audit.AizantIT_Area
                                         select new { objAuditArea.AreaID, objAuditArea.AreaName }).ToList();
                    foreach (var AreaItem in AreaItemsList)
                    {
                        AreaList.Add(new SelectListItem
                        {
                            Text = AreaItem.AreaName,
                            Value = AreaItem.AreaID.ToString(),
                        });
                    }
                    ViewBag.AreaList = new SelectList(AreaList.ToList(), "Value", "Text");
                    List<AizantIT_FDA_System> objFDA_SystemList = new List<AizantIT_FDA_System>();
                    var ObjFDA_SystemList = (from objFDA_systemList in dbAizantIT_QMS_Audit.AizantIT_FDA_System
                                             select new { objFDA_systemList.FDA_SystemID, objFDA_systemList.FDA_SystemName }).ToList();
                    foreach (var FDA_SystemItem in ObjFDA_SystemList)
                    {
                        objFDA_SystemList.Add(new AizantIT_FDA_System
                        {
                            FDA_SystemID = FDA_SystemItem.FDA_SystemID,
                            FDA_SystemName = FDA_SystemItem.FDA_SystemName
                        });
                    }
                    return PartialView(objFDA_SystemList);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = HelpClass.SQLEscapeString(ex.Message);
                return PartialView("_SysException");
            }
        }
        [HttpPost]
        public JsonResult SubmitFDA6SystemAreas(FDA6System objFDA6System)
        {
            try
            {
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    int[] ToRemoveFDA6SystemAreas = { };
                    int[] ToAddFDA6SystemAreas = { };
                    if (objFDA6System.ToRemoveFDA6SystemAreas != null)
                    {
                        ToRemoveFDA6SystemAreas = objFDA6System.ToRemoveFDA6SystemAreas;
                    }
                    if (objFDA6System.ToAddFDA6SystemAreas != null)
                    {
                        ToAddFDA6SystemAreas = objFDA6System.ToAddFDA6SystemAreas;
                    }
                    if ((ToRemoveFDA6SystemAreas.Length > 0) || (ToAddFDA6SystemAreas.Length > 0))
                    {
                        if (ToRemoveFDA6SystemAreas.Length > 0)
                        {
                            dbAizantIT_QMS_Audit.AizantIT_SystemArea.RemoveRange(
                            dbAizantIT_QMS_Audit.AizantIT_SystemArea.Where(p => ToRemoveFDA6SystemAreas.Contains(p.AreaID) && p.FDA_SystemID==objFDA6System.FDA6SystemID));
                            InsertRecordinFDA6SystemHistoryTable(objFDA6System.FDA6SystemID, String.Join(",", ToRemoveFDA6SystemAreas.Select(p => p.ToString()).ToArray()), 2, dbAizantIT_QMS_Audit); // status=2 Removed
                        }
                        if (ToAddFDA6SystemAreas.Length > 0)
                        {
                            List<AizantIT_SystemArea> objAizantIT_SystemArea = new List<AizantIT_SystemArea>();
                            foreach (var NewlyAddedAreaIDItem in ToAddFDA6SystemAreas)
                            {
                                if (!(from itemAuditCategory in dbAizantIT_QMS_Audit.AizantIT_SystemArea
                                      where itemAuditCategory.AreaID == NewlyAddedAreaIDItem
                                       && itemAuditCategory.FDA_SystemID == objFDA6System.FDA6SystemID
                                      select itemAuditCategory.SystemAreaID).Any())
                                {
                                    AizantIT_SystemArea objSystemArea = new AizantIT_SystemArea();
                                    objSystemArea.FDA_SystemID = objFDA6System.FDA6SystemID;
                                    objSystemArea.AreaID = NewlyAddedAreaIDItem;
                                    objAizantIT_SystemArea.Add(objSystemArea);
                                }
                            }
                            dbAizantIT_QMS_Audit.AizantIT_SystemArea.AddRange(objAizantIT_SystemArea);
                            InsertRecordinFDA6SystemHistoryTable(objFDA6System.FDA6SystemID, String.Join(",", ToAddFDA6SystemAreas.Select(p => p.ToString()).ToArray()), 1, dbAizantIT_QMS_Audit); // status=1 Added
                        }
                        dbAizantIT_QMS_Audit.SaveChanges();
                        return Json(new { msg = "Areas are submitted.", msgType = "success" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { msg = "No Changes to submit.", msgType = "Warning" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message);
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public void InsertRecordinFDA6SystemHistoryTable(int FDA_SystemID, string Remarks, int Status, AizantIT_DevEntities dbAizantIT_QMS_Audit)
        {
            AizantIT_FDA_SystemHistory objAizantIT_FDA_SystemHistory = new AizantIT_FDA_SystemHistory();
            objAizantIT_FDA_SystemHistory.FDA_SystemID = FDA_SystemID;
            objAizantIT_FDA_SystemHistory.RoleID = (int)QMS_Audit_Roles.AuditAdmin;
            objAizantIT_FDA_SystemHistory.ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            objAizantIT_FDA_SystemHistory.ActionDate = DateTime.Now;
            objAizantIT_FDA_SystemHistory.Remarks = Remarks;
            objAizantIT_FDA_SystemHistory.Status = Status;
            dbAizantIT_QMS_Audit.AizantIT_FDA_SystemHistory.Add(objAizantIT_FDA_SystemHistory);
        }
        [HttpGet]
        public JsonResult GetListOfFDA6SystemAreasAndSubAreas(int FDA6SystemID)
        {
            try
            {
                List<AuditAreaPost> objFDA6StstemAreasList = new List<AuditAreaPost>();
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    var objFDA6SystemAreasResult = (from objAizantIT_SystemArea in dbAizantIT_QMS_Audit.AizantIT_SystemArea
                                                    join objAizantIT_AizantIT_Area in dbAizantIT_QMS_Audit.AizantIT_Area on objAizantIT_SystemArea.AreaID equals objAizantIT_AizantIT_Area.AreaID
                                                    orderby objAizantIT_AizantIT_Area.AreaID
                                                    where objAizantIT_SystemArea.FDA_SystemID == FDA6SystemID
                                                    select new
                                                    {
                                                        objAizantIT_SystemArea.SystemAreaID,
                                                        objAizantIT_AizantIT_Area.AreaID,
                                                        objAizantIT_AizantIT_Area.AreaName
                                                    }).ToList();
                    foreach (var item in objFDA6SystemAreasResult)
                    {
                        string CombinedSubAreas = string.Join(", ", dbAizantIT_QMS_Audit.AizantIT_SubArea.Where(p => p.AreaID == item.AreaID && p.IsActive == true)
                                  .Select(p => p.SubAreaName.ToString())).ToString();
                        objFDA6StstemAreasList.Add(new AuditAreaPost(item.AreaID, item.AreaName, CombinedSubAreas == "" ? "N/A" : CombinedSubAreas));
                    }
                    return Json(new { data = objFDA6StstemAreasList, msgType = "success" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message);
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetFDAHistoryList(int FDA_SystemID)
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            List<FDA6SystemHistoryList> objFDA6systemHistoryList = new List<FDA6SystemHistoryList>();

            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    var FDA6systemHistoryList1 = dbAizantIT_QMS_Audit.AizantIT_SP_GetFDA_HistoryList(
                               displayLength,
                               displayStart,
                               sortCol,
                               sSortDir,
                               sSearch != null ? sSearch.Trim() : "",
                               FDA_SystemID
                               );
                    foreach (var item in FDA6systemHistoryList1)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objFDA6systemHistoryList.Add(new FDA6SystemHistoryList(Convert.ToInt32(item.FDA_SystemHistoryID), Convert.ToInt32(item.RowNumber), item.ActionByName, item.RoleName, item.ActionDate, item.ActionName, item.Remarks));
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objFDA6systemHistoryList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                //ViewBag.SysError = LineNo + " :" + HelpClass.SQLEscapeString(ex.Message);
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}
