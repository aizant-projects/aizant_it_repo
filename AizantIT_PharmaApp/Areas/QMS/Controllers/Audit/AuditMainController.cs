﻿using AizantIT_PharmaApp.Areas.QMS.Models.Audit.DataLayer;
using AizantIT_PharmaApp.Areas.QMS.Models.Audit.Reports.Datasource;
using AizantIT_PharmaApp.Areas.QMS.Models.Audit.ViewModel;
using AizantIT_PharmaApp.Models.Common;
using AizantIT_PharmaApp.Common.CustomFilters;
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;
using System.Threading.Tasks;
using AizantIT_PharmaApp.Areas.QMS.Models.Common;
using QMS_BusinessLayer;
using QMS_BO.QMS_CAPABO;
using Aizant_API_Entities;
using AizantIT_PharmaApp.Controllers.Aizant_BL;
using System.Data.Entity.Core.Objects;
using AizantIT_PharmaApp.Common;
using AizantIT_PharmaApp.Areas.QMS.Controllers.Common;
using System.Data.Entity;
using iTextSharp.text.pdf;
using iTextSharp.text;
using DevExpress.Pdf;
using System.Text.RegularExpressions;

namespace AizantIT_PharmaApp.Areas.QMS.Controllers.Audit
{
    [Route("QMS/Audit/AuditMainController/{action}")]
    [RouteArea]
    [CustAuthentication]
    public class AuditMainController : Controller
    {
        DateTime dtResponseReportedDate;
        AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities();
        private AizantIT_DevEntities dbQms = new AizantIT_DevEntities();
        #region Audit Report Creation
        [CustAuthorization((int)Modules.QMS,
        (int)QMS_Audit_Roles.AuditQA)]
        [HttpGet]
        //For Loading/Showing the Create Auditor View
        public ActionResult CreateAuditReport()
        {
            try
            {
                AuditCreate objAuditCreate = new AuditCreate();
                ViewBag.loadSeletPicker = "Yes";
                objAuditCreate.AuditActionCreateOrUpdate = "Create";
                InitializeEmptyLists(objAuditCreate);
                //For loading the Category DDl
                List<SelectListItem> objCategoryList = new List<SelectListItem>();
                var CategoryItemsList = (from objAuditCategory in dbAizantIT_QMS_Audit.AizantIT_AuditCategory
                                         select new { objAuditCategory.CategoryID, objAuditCategory.CategoryName }).ToList();
                foreach (var CategoryItem in CategoryItemsList)
                {
                    objCategoryList.Add(new SelectListItem() { Text = CategoryItem.CategoryName, Value = CategoryItem.CategoryID.ToString() });
                }
                objAuditCreate.CategoryList = objCategoryList;
                //For Loading the Type DDL for "ForAizant"
                List<SelectListItem> ObjCategoryTypeList = new List<SelectListItem>();
                var CategoryTypeItemsList = (from objAuditCategoryType in dbAizantIT_QMS_Audit.AizantIT_AuditCategoryType
                                             where objAuditCategoryType.CategoryID == 2 && objAuditCategoryType.IsActive == true // 2 represents For Aizant
                                             select new { objAuditCategoryType.TypeID, objAuditCategoryType.TypeName }).ToList();
                foreach (var CategoryTypeItem in CategoryTypeItemsList)
                {
                    ObjCategoryTypeList.Add(new SelectListItem()
                    {
                        Text = CategoryTypeItem.TypeName,
                        Value = CategoryTypeItem.TypeID.ToString(),
                    });
                }
                objAuditCreate.CategoryTypeList = ObjCategoryTypeList;
                // For Loading the Audit Client Country ddl
                List<SelectListItem> ObjAuditClientList = new List<SelectListItem>();
                var AuditClientItemsList = (from objAuditClient in dbAizantIT_QMS_Audit.AizantIT_Client
                                            where objAuditClient.IsActive == true
                                            select new { objAuditClient.ClientID, objAuditClient.ClientName }).ToList();
                ObjAuditClientList.Add(new SelectListItem()
                {
                    Text = "-- Select Client --",
                    Value = "0"
                });
                foreach (var AuditClientItem in AuditClientItemsList)
                {
                    ObjAuditClientList.Add(new SelectListItem()
                    {
                        Text = AuditClientItem.ClientName,
                        Value = AuditClientItem.ClientID.ToString()
                    });
                }
                objAuditCreate.ClientList = ObjAuditClientList;
                // For Loading the RegulatoryAgency ddl
                List<SelectListItem> ObjRegulatoryAgencyList = new List<SelectListItem>();
                var RegulatoryAgencyItemsList = (from objRegulatoryAgency in dbAizantIT_QMS_Audit.AizantIT_RegulatoryAgency
                                                 where objRegulatoryAgency.IsActive == true
                                                 select new { objRegulatoryAgency.RegulatoryAgencyID, AgencyName = (objRegulatoryAgency.RegulatoryAgencyCode + " (" + objRegulatoryAgency.RegulatoryAgencyName) + ")" }).ToList();
                foreach (var RegulatoryAgencyItem in RegulatoryAgencyItemsList)
                {
                    ObjRegulatoryAgencyList.Add(new SelectListItem()
                    {
                        Text = RegulatoryAgencyItem.AgencyName,
                        Value = RegulatoryAgencyItem.RegulatoryAgencyID.ToString()
                    });
                }
                objAuditCreate.RegulatoryAgencyList = ObjRegulatoryAgencyList;
                // For Loading the Observers ddl
                List<SelectListItem> ObjObserversList = new List<SelectListItem>();
                var AuditObserversItemsList = (from objObserver in dbAizantIT_QMS_Audit.AizantIT_ObserverDetails
                                               select new { objObserver.ObserverID, objObserver.ObserverName }).ToList();
                foreach (var ObserverItem in AuditObserversItemsList)
                {
                    ObjObserversList.Add(new SelectListItem()
                    {
                        Text = ObserverItem.ObserverName,
                        Value = ObserverItem.ObserverID.ToString()
                    });
                }
                objAuditCreate.ObserverList = ObjObserversList;
                //For Loading the BusinesDivision DDL
                List<SelectListItem> ObjBusinessDivisionList = new List<SelectListItem>();
                var BusinessDivisionItemsList = (from objBusinessDivision in dbAizantIT_QMS_Audit.AizantIT_Business_Division//dbAizantIT_QMS_Audit.AizantIT_BusinessDivision
                                                 where objBusinessDivision.IsActive == true
                                                 select new { objBusinessDivision.BusinessDivisionID, BusinessDivisionName = (objBusinessDivision.BusinessDivisionCode + " - " + objBusinessDivision.BusinessDivisionName) }).ToList();
                foreach (var BusinessDivisionItem in BusinessDivisionItemsList)
                {
                    ObjBusinessDivisionList.Add(new SelectListItem()
                    {
                        Text = BusinessDivisionItem.BusinessDivisionName,
                        Value = BusinessDivisionItem.BusinessDivisionID.ToString()
                    });
                }
                objAuditCreate.BusinessDivisionList = ObjBusinessDivisionList;
                //For Loading the Approvers DDL using SP
                List<SelectListItem> ObjApproversList = new List<SelectListItem>();
                Aizant_Bl objAizantBal = new Aizant_Bl();
                int[] empId = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                objAuditCreate.ApproversList = objAizantBal.GetListofEmpByRole(empId, (int)QMS_Audit_Roles.AuditHeadQA, "A");
                return View(objAuditCreate);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = LineNo + " :" + HelpClass.SQLEscapeString(ex.Message);
                return View();
            }
        }
        // For initializing the AuditReport Business Objects
        public void InitializeEmptyLists(AuditCreate objAuditCreate)
        {
            try
            {
                objAuditCreate.TypeOfList = new List<SelectListItem>();
                objAuditCreate.ClientList = new List<SelectListItem>();
                objAuditCreate.RegulatoryAgencyList = new List<SelectListItem>();
                objAuditCreate.SiteList = new List<SelectListItem>();
                objAuditCreate.AuditorList = new List<SelectListItem>();
                objAuditCreate.ObservationSiteList = new List<SelectListItem>();
                objAuditCreate.ObservationSiteDepartments = new List<SelectListItem>();
                objAuditCreate.ObservationResponsiblePerson = new List<SelectListItem>();
            }
            catch (Exception)
            {
                throw;
            }
        }
        [HttpPost]
        // For Submitting the Audit Report master Data(Creating the New Audit)
        public JsonResult CreateAuditReport(AuditCreate objAuditCreate)
        {
            try
            {
                int categoryID = objAuditCreate.CategoryID;
                int CategoryTypeID = objAuditCreate.CategoryTypeID;
                string AuditReportNumber = objAuditCreate.AuditReportNumber;
                int BusinessDivisionID = objAuditCreate.BusinessDivisionID;
                string[] AuditSiteIDs = objAuditCreate.AuditSiteIDs;
                string AuditDates = objAuditCreate.AuditDates;
                string AuditReportDate = objAuditCreate.AuditReportDate;
                int StatusID = objAuditCreate.StatusID;
                string[] AuditorIDs = objAuditCreate.AuditorIDs;
                int RegulatoryAgencyID = objAuditCreate.RegulatoryAgencyID;
                int ClientID = objAuditCreate.ClientID;
                int ApproverID = objAuditCreate.ApproverID;
                string AuditComments = objAuditCreate.AuditComments;
                // Date Validation
                if (Convert.ToDateTime(objAuditCreate.AuditFromDate).Date > DateTime.Now.Date || Convert.ToDateTime(objAuditCreate.AuditToDate).Date > DateTime.Now.Date || Convert.ToDateTime(objAuditCreate.AuditReportDate).Date > DateTime.Now.Date)
                {
                    return Json(new { msg = "From date or To date or Report date should not be greater than Present date.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                }
                else if (objAuditCreate.StatusID == 2 && (Convert.ToDateTime(objAuditCreate.AuditDueDate).Date > DateTime.Now.Date))
                {
                    //Since it is a completed dates should not be more than today
                    return Json(new { msg = "Due date should not be greater than present date for Closed Audit.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                }
                DataTable dtAuditSiteIds = new DataTable();
                dtAuditSiteIds.Columns.Add("AuditSiteID");
                foreach (var SiteID in objAuditCreate.AuditSiteIDs)
                {
                    dtAuditSiteIds.Rows.Add(SiteID);
                }
                DataTable dtAuditorIds = new DataTable();
                dtAuditorIds.Columns.Add("AuditorID");
                foreach (var AuditorID in objAuditCreate.AuditorIDs)
                {
                    dtAuditorIds.Rows.Add(AuditorID);
                }
                DataTable dtObserverIds = new DataTable();
                dtObserverIds.Columns.Add("ObserverID");
                if (objAuditCreate.ObserverIDs != null)
                {
                    foreach (var ObserverID in objAuditCreate.ObserverIDs)
                    {
                        dtObserverIds.Rows.Add(ObserverID);
                    }
                }
                DataTable dtAuditDates = new DataTable();
                dtAuditDates.Columns.Add("AuditDates", typeof(DateTime));
                foreach (var AuditDate in objAuditCreate.AuditDates.Split(','))
                {
                    dtAuditDates.Rows.Add(Convert.ToDateTime(AuditDate));
                }
                DateTime FromDate =
                Convert.ToDateTime(
                                    ((from DataRow dr in dtAuditDates.Rows
                                      orderby Convert.ToDateTime(dr["AuditDates"]) ascending
                                      select dr).FirstOrDefault()["AuditDates"]
                                      )
                                    );
                DateTime ToDate =
                    Convert.ToDateTime(
                        ((from DataRow dr in dtAuditDates.Rows
                          orderby Convert.ToDateTime(dr["AuditDates"]) descending
                          select dr).FirstOrDefault()["AuditDates"]
                          )
                        );
                SqlParameter ParamAuditCategoryID = new SqlParameter("@CategoryID", objAuditCreate.CategoryID);
                SqlParameter ParamAuditCategoryTypeID = new SqlParameter("@CategoryTypeID", objAuditCreate.CategoryTypeID);
                SqlParameter ParamAuditTypeOfID = new SqlParameter("@TypeOfID", objAuditCreate.TypeOfID);
                SqlParameter ParamAuditBusinessDivisionID = new SqlParameter("@BusinessDivisionID", objAuditCreate.BusinessDivisionID);
                SqlParameter ParamAuditReportNumber = new SqlParameter("@AuditReportNumber", objAuditCreate.AuditReportNumber);
                SqlParameter ParamAuditFromDate = new SqlParameter("@AuditFromDate", FromDate);
                SqlParameter ParamAuditToDate = new SqlParameter("@AuditToDate", ToDate);
                SqlParameter ParamAuditReportDate = new SqlParameter("@AuditReportDate", objAuditCreate.AuditReportDate);
                SqlParameter ParamAuditDueDate = new SqlParameter("@AuditDueDate", objAuditCreate.AuditDueDate);
                SqlParameter ParamAuditCertificateValidUntilDate = new SqlParameter("@AuditCertificateValidUntilDate", SqlDbType.DateTime);
                SqlParameter ParamAuditCertificateDate = new SqlParameter("@AuditCertificateDate", SqlDbType.DateTime);
                if (objAuditCreate.AuditCertificateDate == null)
                {
                    ParamAuditCertificateDate.Value = (object)DBNull.Value;
                }
                else
                {
                    ParamAuditCertificateDate.Value = (object)objAuditCreate.AuditCertificateDate;
                }
                if (objAuditCreate.AuditCertificateValidUntilDate == null)
                {
                    ParamAuditCertificateValidUntilDate.Value = (object)DBNull.Value;
                }
                else
                {
                    ParamAuditCertificateValidUntilDate.Value = (object)objAuditCreate.AuditCertificateValidUntilDate;
                }
                SqlParameter ParamAuthorEmpID = new SqlParameter("@AuthorEmpID", Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                SqlParameter ParamActionRole = new SqlParameter("@ActionRole", (int)QMS_Audit_Roles.AuditQA);
                SqlParameter ParamAuditApproverID = new SqlParameter("@ApproverID", objAuditCreate.ApproverID);
                SqlParameter ParamAuditStatusID = new SqlParameter("@StatusID", objAuditCreate.StatusID);
                SqlParameter ParamAuditCreationStatus = new SqlParameter("@AuditCreationStatus", objAuditCreate.AuditCreationStatus);
                SqlParameter ParamAuditClientID = new SqlParameter("@ClientID", objAuditCreate.ClientID);
                SqlParameter ParamAuditRegulatoryAgencyID = new SqlParameter("@RegulatoryAgencyID", objAuditCreate.RegulatoryAgencyID);
                SqlParameter ParamAuditNatureOfInspectionID = new SqlParameter("@NatureOfInspectionID", objAuditCreate.NatureOfInspectionID);
                SqlParameter ParamAuditComments = new SqlParameter("@AuditComments", objAuditCreate.AuditComments);
                SqlParameter ParamAuditDates = new SqlParameter("@AuditDates", dtAuditDates);
                SqlParameter ParamAuditSiteIDs = new SqlParameter("@AuditSiteIDs", dtAuditSiteIds);
                SqlParameter ParamAuditAuditorIDs = new SqlParameter("@AuditAuditors", dtAuditorIds);
                SqlParameter ParamAuditObserverIDs = new SqlParameter("@AuditObservers", dtObserverIds);
                SqlParameter ParamAuditID = new SqlParameter("@AuditID", SqlDbType.Int);
                ParamAuditID.Direction = ParameterDirection.Output;
                SqlParameter ParamAuditCreateResultStatus = new SqlParameter("@AuditCreateResultStatus", SqlDbType.Int);
                ParamAuditCreateResultStatus.Direction = ParameterDirection.Output;
                ParamAuditSiteIDs.TypeName = "QMS_Audit.AizantIT_TVP_AuditSites";
                ParamAuditAuditorIDs.TypeName = "QMS_Audit.AizantIT_TVP_Auditors";
                ParamAuditObserverIDs.TypeName = "QMS_Audit.AizantIT_TVP_Observers";
                ParamAuditDates.TypeName = "QMS_Audit.AizantIT_TVP_AuditOnDate";
                dbAizantIT_QMS_Audit.Database.ExecuteSqlCommand("exec [QMS_Audit].[AizantIT_SP_CreateAuditReport] " +
                    "@AuditSiteIDs,@AuditAuditors,@AuditObservers,@AuditDates,@CategoryID,@CategoryTypeID,@TypeOfID,@BusinessDivisionID,@AuditReportNumber," +
                    "@AuditFromDate,@AuditToDate,@AuditReportDate,@AuditDueDate,@AuditCertificateDate,@AuditCertificateValidUntilDate,@StatusID,@RegulatoryAgencyID,@NatureOfInspectionID," +
                    "@ClientID,@ApproverID," +
                    "@AuditComments," +
                    "@AuthorEmpID,@ActionRole,@AuditCreationStatus,@AuditID out,@AuditCreateResultStatus Out",
                      ParamAuditSiteIDs, ParamAuditAuditorIDs, ParamAuditObserverIDs, ParamAuditDates, ParamAuditCategoryID, ParamAuditCategoryTypeID,
                        ParamAuditTypeOfID, ParamAuditBusinessDivisionID, ParamAuditReportNumber,
                        ParamAuditFromDate, ParamAuditToDate, ParamAuditReportDate, ParamAuditDueDate, ParamAuditCertificateDate, ParamAuditCertificateValidUntilDate,
                        ParamAuditStatusID, ParamAuditRegulatoryAgencyID, ParamAuditNatureOfInspectionID,
                        ParamAuditClientID, ParamAuditApproverID,
                        ParamAuditComments,
                        ParamAuthorEmpID, ParamActionRole, ParamAuditCreationStatus, ParamAuditID, ParamAuditCreateResultStatus);
                int AuditId = Convert.ToInt32(ParamAuditID.Value);
                if (Convert.ToInt32(ParamAuditCreateResultStatus.Value) == 1)
                {
                    return Json(new { msg = "Audit Created Successfully", msgType = "success", AuditID = AuditId }, JsonRequestBehavior.AllowGet);
                }
                else if (Convert.ToInt32(ParamAuditCreateResultStatus.Value) == 2)
                {
                    return Json(new { msg = "Audit report Number Already exists", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { msg = "Audit report  failed to create contact Admin.", msgType = "error" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region AuditReport Modification
        [CustAuthorization((int)Modules.QMS,
       (int)QMS_Audit_Roles.AuditQA)]
        [HttpGet]
        //For Loading the Audit Modification View
        public ActionResult ModifyAuditReport(int AuditCurrentStatusID, int AuditID = 0, string FromListType = "", int Notification_ID = 0)
        {
            try
            {
                if (Notification_ID != 0)
                {
                    MakeAuditNotificationstoVisited(Notification_ID);
                    AuditID = GetAuditIDByNotificationID(Notification_ID);
                }
                if (AuditReportStatus(AuditID) == 1 || AuditReportStatus(AuditID) == 3)
                {
                    ViewBag.FromListType = FromListType;
                    int LoginEMPID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    AuditCreate objAuditCreate = new AuditCreate();
                    objAuditCreate.IsRecordExist = (from itemobjAuditReport in dbAizantIT_QMS_Audit.AizantIT_AuditReport
                                                    where itemobjAuditReport.AuditID == AuditID
                                                    select new { itemobjAuditReport.AuditID }).Any();
                    InitializeEmptyLists(objAuditCreate);
                    var objAizantIT_AuditReportRecord = (from objAuditReport in dbAizantIT_QMS_Audit.AizantIT_AuditReport
                                                         join objAuditCategory in dbAizantIT_QMS_Audit.AizantIT_AuditCategory on objAuditReport.CategoryID equals objAuditCategory.CategoryID
                                                         join objAuditCategoryType in dbAizantIT_QMS_Audit.AizantIT_AuditCategoryType on objAuditReport.TypeID equals objAuditCategoryType.TypeID
                                                         join objAuditTypeOf in dbAizantIT_QMS_Audit.AizantIT_TypeOf on objAuditReport.TypeOfID equals objAuditTypeOf.TypeOfID
                                                         join objAuditBusinessDivision in dbAizantIT_QMS_Audit.AizantIT_Business_Division on objAuditReport.BusinessDivisionID equals objAuditBusinessDivision.BusinessDivisionID
                                                         join objAuditApproverEmp in dbAizantIT_QMS_Audit.AizantIT_EmpMaster on objAuditReport.ApproverEmpID equals objAuditApproverEmp.EmpID
                                                         where objAuditReport.AuditID == AuditID
                                                         select new
                                                         {
                                                             objAuditReport.AuditID,
                                                             objAuditReport.CategoryID,
                                                             objAuditCategory.CategoryName,
                                                             objAuditReport.TypeID,
                                                             objAuditCategoryType.TypeName,
                                                             objAuditReport.TypeOfID,
                                                             objAuditTypeOf.TypeOfName,
                                                             objAuditReport.BusinessDivisionID,
                                                             objAuditBusinessDivision.BusinessDivisionName,
                                                             objAuditReport.InspectionOrAuditReportNum,
                                                             objAuditReport.AuditFromDate,
                                                             objAuditReport.AuditToDate,
                                                             objAuditReport.AuditReportedDate,
                                                             objAuditReport.AuditDueDate,
                                                             objAuditReport.CertifiedDate,
                                                             objAuditReport.CertificateValidUntil,
                                                             objAuditReport.AuthorEmpID,
                                                             objAuditReport.ApproverEmpID,
                                                             ApproverName = objAuditApproverEmp.FirstName + " " + objAuditApproverEmp.LastName,
                                                             objAuditReport.Status,
                                                             objAuditReport.NatureOfInspectionID
                                                         }).SingleOrDefault();
                    if (objAizantIT_AuditReportRecord.AuthorEmpID == LoginEMPID)
                    {
                        objAuditCreate.IsValidAuthor = true;
                    }
                    else
                    {
                        objAuditCreate.IsValidAuthor = false;
                    }
                    objAuditCreate.AuditID = objAizantIT_AuditReportRecord.AuditID;
                    objAuditCreate.CategoryID = objAizantIT_AuditReportRecord.CategoryID;
                    objAuditCreate.CategoryTypeID = objAizantIT_AuditReportRecord.TypeID;
                    objAuditCreate.TypeOfID = objAizantIT_AuditReportRecord.TypeOfID;
                    objAuditCreate.BusinessDivisionID = objAizantIT_AuditReportRecord.BusinessDivisionID;
                    objAuditCreate.AuditReportNumber = objAizantIT_AuditReportRecord.InspectionOrAuditReportNum;
                    objAuditCreate.AuditFromDate = objAizantIT_AuditReportRecord.AuditFromDate.ToString("dd MMM yyyy");
                    objAuditCreate.AuditToDate = objAizantIT_AuditReportRecord.AuditToDate.ToString("dd MMM yyyy");
                    objAuditCreate.AuditReportDate = objAizantIT_AuditReportRecord.AuditReportedDate.ToString("dd MMM yyyy");
                    objAuditCreate.AuditDueDate = objAizantIT_AuditReportRecord.AuditDueDate.ToString("dd MMM yyyy");
                    objAuditCreate.AuditCertificateDate = objAizantIT_AuditReportRecord.CertifiedDate.HasValue ? objAizantIT_AuditReportRecord.CertifiedDate.Value.ToString("dd MMM yyyy") : string.Empty;
                    objAuditCreate.AuditCertificateValidUntilDate = objAizantIT_AuditReportRecord.CertificateValidUntil.HasValue ? objAizantIT_AuditReportRecord.CertificateValidUntil.Value.ToString("dd MMM yyyy") : string.Empty;
                    objAuditCreate.ApproverID = objAizantIT_AuditReportRecord.ApproverEmpID;
                    objAuditCreate.StatusID = objAizantIT_AuditReportRecord.Status;
                    objAuditCreate.RegulatoryAgencyID = 0;
                    objAuditCreate.NatureOfInspectionID = objAizantIT_AuditReportRecord.NatureOfInspectionID;
                    #region BindingDropdowns
                    List<SelectListItem> ObjClientList = new List<SelectListItem>();
                    List<SelectListItem> ObjSiteList = new List<SelectListItem>();
                    List<SelectListItem> ObjAuditorList = new List<SelectListItem>();
                    List<SelectListItem> ObjObserverList = new List<SelectListItem>();
                    List<SelectListItem> ObjRegulatoryAgency = new List<SelectListItem>();
                    DAL_AuditReport objAuditDal = new DAL_AuditReport();
                    DataSet dsAuditDetails = objAuditDal.GetAuditReportDetails(AuditID.ToString());
                    if (dsAuditDetails.Tables.Count >= 6)
                    {
                        if (dsAuditDetails.Tables[0].Rows.Count > 0)
                        {
                            objAuditCreate.ClientID = Convert.ToInt32(dsAuditDetails.Tables[0].Rows[0]["ClientID"]);
                            foreach (DataRow dr in dsAuditDetails.Tables[0].Rows)
                            {
                                ObjClientList.Add(new SelectListItem()
                                {
                                    Text = dr["ClientName"].ToString(),
                                    Value = dr["ClientID"].ToString(),
                                    Selected = true
                                });
                            }
                        }
                        if (dsAuditDetails.Tables[1].Rows.Count > 0)
                        {
                            //objAuditCreate.NatureOfInspectionID = Convert.ToInt32(dsAuditDetails.Tables[1].Rows[0]["NatureOfInspectionID"]);
                            objAuditCreate.RegulatoryAgencyID = Convert.ToInt32(dsAuditDetails.Tables[1].Rows[0]["RegulatoryAgencyID"]);

                            ObjRegulatoryAgency.Add(new SelectListItem()
                            {
                                Text = dsAuditDetails.Tables[1].Rows[0]["RegulatoryAgencyName"].ToString(),
                                Value = dsAuditDetails.Tables[1].Rows[0]["RegulatoryAgencyID"].ToString(),
                                Selected = true
                            });
                        }
                        if (dsAuditDetails.Tables[2].Rows.Count > 0)
                        {
                            foreach (DataRow drSites in dsAuditDetails.Tables[2].Rows)
                            {
                                ObjSiteList.Add(new SelectListItem()
                                {
                                    Text = drSites["SiteName"].ToString(),
                                    Value = drSites["SiteID"].ToString(),
                                    Selected = true
                                });
                            }
                        }
                        if (dsAuditDetails.Tables[3].Rows.Count > 0)
                        {
                            foreach (DataRow drAuditor in dsAuditDetails.Tables[3].Rows)
                            {
                                ObjAuditorList.Add(new SelectListItem()
                                {
                                    Text = drAuditor["AuditorName"].ToString(),
                                    Value = drAuditor["AuditorID"].ToString(),
                                    Selected = true
                                });
                            }
                        }
                        if (dsAuditDetails.Tables[4].Rows.Count > 0)
                        {
                            foreach (DataRow drObservers in dsAuditDetails.Tables[4].Rows)
                            {
                                ObjObserverList.Add(new SelectListItem()
                                {
                                    Text = drObservers["ObserverName"].ToString(),
                                    Value = drObservers["ObserverID"].ToString(),
                                    Selected = true
                                });
                            }
                        }
                        if (dsAuditDetails.Tables[5].Rows.Count > 0)// For Audit Dates
                        {
                            int i = 0;
                            string[] myList = new string[dsAuditDetails.Tables[5].Rows.Count];
                            foreach (DataRow dr in dsAuditDetails.Tables[5].Rows)
                            {
                                myList[i] = dr["AuditDate"].ToString();
                                i++;
                            }
                            ViewBag.AuditDates = myList;
                        }
                    }
                    objAuditCreate.ClientList = ObjClientList;
                    objAuditCreate.SiteList = ObjSiteList;
                    objAuditCreate.AuditorList = ObjAuditorList;
                    objAuditCreate.ObserverList = ObjObserverList;
                    objAuditCreate.RegulatoryAgencyList = ObjRegulatoryAgency;
                    List<SelectListItem> ObjCategoryList = new List<SelectListItem>();
                    ObjCategoryList.Add(new SelectListItem()
                    {
                        Text = objAizantIT_AuditReportRecord.CategoryName,
                        Value = objAizantIT_AuditReportRecord.CategoryID.ToString(),
                        Selected = true
                    });
                    objAuditCreate.CategoryList = ObjCategoryList;
                    List<SelectListItem> ObjCategoryTypeList = new List<SelectListItem>();
                    ObjCategoryTypeList.Add(new SelectListItem()
                    {
                        Text = objAizantIT_AuditReportRecord.TypeName,
                        Value = objAizantIT_AuditReportRecord.TypeID.ToString(),
                        Selected = true
                    });
                    objAuditCreate.CategoryTypeList = ObjCategoryTypeList;
                    List<SelectListItem> ObjTypeOf = new List<SelectListItem>();
                    ObjTypeOf.Add(new SelectListItem()
                    {
                        Text = objAizantIT_AuditReportRecord.TypeOfName,
                        Value = objAizantIT_AuditReportRecord.TypeOfID.ToString(),
                        Selected = true
                    });
                    objAuditCreate.TypeOfList = ObjTypeOf;
                    List<SelectListItem> ObjClient = new List<SelectListItem>();
                    List<SelectListItem> ObjApprover = new List<SelectListItem>();
                    ObjApprover.Add(new SelectListItem()
                    {
                        Text = objAizantIT_AuditReportRecord.ApproverName,
                        Value = objAizantIT_AuditReportRecord.ApproverEmpID.ToString(),
                        Selected = true
                    });
                    objAuditCreate.ApproversList = ObjApprover;
                    List<SelectListItem> ObjBusinessDivision = new List<SelectListItem>();
                    ObjBusinessDivision.Add(new SelectListItem()
                    {
                        Text = objAizantIT_AuditReportRecord.BusinessDivisionName,
                        Value = objAizantIT_AuditReportRecord.BusinessDivisionID.ToString(),
                        Selected = true
                    });
                    objAuditCreate.BusinessDivisionList = ObjBusinessDivision;
                    #endregion

                    objAuditCreate.AuditActionCreateOrUpdate = "Update";
                    objAuditCreate.AuditCreationStatus = objAuditCreate.StatusID;
                    objAuditCreate.AuditCurrentStatusID = AuditReportStatus(AuditID);
                    ViewBag.loadSeletPicker = "Yes";
                    if (objAuditCreate.IsValidAuthor)
                    {
                        return View("CreateAuditReport", objAuditCreate);
                    }
                    else
                    {
                        ViewBag.SysError = "You are not a valid Author for this Audit Report.";
                        return View("CreateAuditReport");
                    }
                }
                else
                {
                    ViewBag.SysError = "Record is not in a status of Modification.";
                    return View("CreateAuditReport");
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = LineNo + " :" + HelpClass.SQLEscapeString(ex.Message);
                return View("CreateAuditReport");
            }
        }
        [HttpPost]
        // For Updating the Audit Master Details
        public JsonResult ModifiedAuditReport(AuditCreate objAuditUpdate)
        {
            try
            {
                string MessageDescription = "";
                string MessageType = "";
                //Checking the Date validations based on Status of Audit(Completed or In-progress)
                if (Convert.ToDateTime(objAuditUpdate.AuditFromDate).Date > DateTime.Now.Date || Convert.ToDateTime(objAuditUpdate.AuditToDate).Date > DateTime.Now.Date || Convert.ToDateTime(objAuditUpdate.AuditReportDate).Date > DateTime.Now.Date)
                {
                    return Json(new { msg = "From date or To date or Report date should not be greater than Present date.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                }
                else if (objAuditUpdate.AuditCreationStatus == 2 && (Convert.ToDateTime(objAuditUpdate.AuditDueDate).Date > DateTime.Now.Date))
                {
                    //Since it is a completed dates should not be more than today
                    return Json(new { msg = "Due date should not be greater than present date for Closed Audit.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                }
                ArrayList AttachmentsValidation = new ArrayList();
                //If It is Submit for Approval or Updating the reverted Audit. validating the Minimum required fields
                if (objAuditUpdate.AuditCurrentStatusID == 2 || objAuditUpdate.AuditCurrentStatusID == 4)
                {
                    //Checking the Validations of scribeNotes and AuditReport Attachments
                    bool IsAudtReportAttachmentExists = (from
//itemAttachmentLink in dbAizantIT_QMS_Audit.AizantIT_AuditAttachmentLink join
itemAuditAttachment in dbAizantIT_QMS_Audit.AizantIT_AuditAttachment //on itemAttachmentLink.AttachmentID equals itemAuditAttachment.AttachmentID
                                                         where itemAuditAttachment.AuditID == objAuditUpdate.AuditID && itemAuditAttachment.AttachmentType == 1 //For AuditReport
                                                         select new { itemAuditAttachment.AttachmentID }).Any();
                    bool IsScribeNotesExists = (from //itemAttachmentLink in dbAizantIT_QMS_Audit.AizantIT_AuditAttachmentLink join
itemAuditAttachment in dbAizantIT_QMS_Audit.AizantIT_AuditAttachment //on itemAttachmentLink.AttachmentID equals itemAuditAttachment.AttachmentID
                                                where itemAuditAttachment.AuditID == objAuditUpdate.AuditID && itemAuditAttachment.AttachmentType == 3 //For ScribeNotes
                                                select new { itemAuditAttachment.AttachmentID }).Any();
                    bool IsOnlyClientAudit = (from itemClientAuditBy in dbAizantIT_QMS_Audit.AizantIT_ClientAuditBy
                                              where itemClientAuditBy.AuditID == objAuditUpdate.AuditID
                                              select new { itemClientAuditBy.ClientID }).Any();
                    if (!IsAudtReportAttachmentExists)
                    {
                        AttachmentsValidation.Add("At least one Audit Report Attachment is Mandatory");
                    }
                    //Checking Observations
                    bool IsObservationsExist = (from itemAuditObserevation in dbAizantIT_QMS_Audit.AizantIT_AuditObservation
                                                where itemAuditObserevation.AuditID == objAuditUpdate.AuditID
                                                select new { itemAuditObserevation.ObservationID }).Any();
                    if (!IsObservationsExist)
                    {
                        AttachmentsValidation.Add("At least one Observation is Mandatory");
                    }
                    //Checking the Responses for Observations
                    var AuditObservationList =
                                        (from itemAuditObserevation in dbAizantIT_QMS_Audit.AizantIT_AuditObservation
                                         where itemAuditObserevation.AuditID == objAuditUpdate.AuditID
                                         select new
                                         {
                                             itemAuditObserevation.ObservationID
                                         }).ToList();
                    var AuditAssignedObservationList =
                                       (from itemAuditObserevation in dbAizantIT_QMS_Audit.AizantIT_AuditObservation
                                        join
itemAssignedObservation in dbAizantIT_QMS_Audit.AizantIT_AssignedObservation on itemAuditObserevation.ObservationID equals itemAssignedObservation.ObservationID
                                        where itemAuditObserevation.AuditID == objAuditUpdate.AuditID
                                        select new
                                        {
                                            itemAuditObserevation.ObservationID,
                                            itemAssignedObservation.DueDate
                                        }).Distinct().ToList();
                    if (objAuditUpdate.StatusID == 1)//Only For In-progress Audit
                    {
                        bool IsObsDuedateExceedsAuditDueDate = false;
                        foreach (var item in AuditAssignedObservationList)
                        {
                            if (Convert.ToDateTime(objAuditUpdate.AuditDueDate).Date < Convert.ToDateTime(item.DueDate).Date)
                            {
                                IsObsDuedateExceedsAuditDueDate = true;
                            }
                        }
                        if (IsObsDuedateExceedsAuditDueDate)
                        {
                            AttachmentsValidation.Add("The Observation Due date is exceeding the Response Due Date.");
                        }
                    }
                    //There are Observations without response for completed Audit Report
                    if ((AuditObservationList.Where(p => !AuditAssignedObservationList.Any(p2 => p2.ObservationID == p.ObservationID)).ToList().Count > 0) && objAuditUpdate.StatusID == 2)//For Completed there's an Observations without response
                    {
                        AttachmentsValidation.Add("There's an Observations without Response");
                    }
                }
                StringBuilder Validation = new StringBuilder();
                foreach (string errormsg in AttachmentsValidation)
                {
                    Validation.Append(errormsg);
                    if (AttachmentsValidation.IndexOf(errormsg) == AttachmentsValidation.Count - 1)//This is last item of this list
                    {
                        Validation.Append(".");
                    }
                    else
                    {
                        Validation.Append(",<br/>");
                    }
                }
                MessageDescription = Validation.ToString();
                //Validating and returning back if not satisfied the validation.
                if (AttachmentsValidation.Count > 0)
                {
                    return Json(new { msg = MessageDescription, msgType = "error" }, JsonRequestBehavior.AllowGet);
                }
                //For Submitting the data to Db for Update of Audit Report.
                DataTable dtAuditDates = new DataTable();
                dtAuditDates.Columns.Add("AuditDates", typeof(DateTime));
                foreach (var AuditDate in objAuditUpdate.AuditDates.Split(','))
                {
                    dtAuditDates.Rows.Add(Convert.ToDateTime(AuditDate));
                }
                DateTime FromDate =
                Convert.ToDateTime(
                                    ((from DataRow dr in dtAuditDates.Rows
                                      orderby Convert.ToDateTime(dr["AuditDates"]) ascending
                                      select dr).FirstOrDefault()["AuditDates"]
                                      )
                                    );
                DateTime ToDate =
                    Convert.ToDateTime(
                        ((from DataRow dr in dtAuditDates.Rows
                          orderby Convert.ToDateTime(dr["AuditDates"]) descending
                          select dr).FirstOrDefault()["AuditDates"]
                          )
                        );
                SqlParameter ParamAuditID = new SqlParameter("@AuditID", objAuditUpdate.AuditID);
                SqlParameter ParamAuditDates = new SqlParameter("@AuditDates", dtAuditDates);
                ParamAuditDates.TypeName = "QMS_Audit.AizantIT_TVP_AuditOnDate";
                SqlParameter ParamAuditFromDate = new SqlParameter("@AuditFromDate", FromDate);
                SqlParameter ParamAuditToDate = new SqlParameter("@AuditToDate", ToDate);
                SqlParameter ParamAuditReportDate = new SqlParameter("@AuditReportDate", objAuditUpdate.AuditReportDate);
                SqlParameter ParamAuditDueDate = new SqlParameter("@AuditDueDate", objAuditUpdate.AuditDueDate);
                SqlParameter ParamAuditCertificateValidUntilDate = new SqlParameter("@AuditCertificateValidUntilDate", SqlDbType.DateTime);
                SqlParameter ParamAuditCertificateDate = new SqlParameter("@AuditCertificateDate", SqlDbType.DateTime);
                if (objAuditUpdate.AuditCertificateDate == null)
                {
                    ParamAuditCertificateDate.Value = (object)DBNull.Value;
                }
                else
                {
                    ParamAuditCertificateDate.Value = (object)objAuditUpdate.AuditCertificateDate;
                }
                if (objAuditUpdate.AuditCertificateValidUntilDate == null)
                {
                    ParamAuditCertificateValidUntilDate.Value = (object)DBNull.Value;
                }
                else
                {
                    ParamAuditCertificateValidUntilDate.Value = (object)objAuditUpdate.AuditCertificateValidUntilDate;
                }
                SqlParameter ParamActionRole = new SqlParameter("@ActionRole", (int)QMS_Audit_Roles.AuditQA);
                SqlParameter ParamModifiedBy = new SqlParameter("@AuditModifiedBy", Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                SqlParameter ParamAuditCurrentStatusID = new SqlParameter("@ActionStatus", objAuditUpdate.AuditCurrentStatusID);
                SqlParameter ParamAuditComments = new SqlParameter("@ModifyRemarks", objAuditUpdate.AuditComments);
                SqlParameter ParamModifiedResult = new SqlParameter("@ModifiedResultStatus", SqlDbType.Int);
                ParamModifiedResult.Direction = ParameterDirection.Output;
                dbAizantIT_QMS_Audit.Database.ExecuteSqlCommand("exec [QMS_Audit].[AizantIT_SP_UpdateAuditReport] @AuditID,@AuditDates,@AuditFromDate,@AuditToDate,@AuditReportDate,@AuditDueDate,@AuditCertificateDate,@AuditCertificateValidUntilDate,@AuditModifiedBy,@ActionRole,@ActionStatus,@ModifyRemarks,@ModifiedResultStatus OUT",
                     ParamAuditID, ParamAuditDates, ParamAuditFromDate, ParamAuditToDate, ParamAuditReportDate, ParamAuditDueDate, ParamAuditCertificateDate, ParamAuditCertificateValidUntilDate, ParamModifiedBy, ParamActionRole, ParamAuditCurrentStatusID, ParamAuditComments, ParamModifiedResult);
                int ModifiedResultStatus = Convert.ToInt32(ParamModifiedResult.Value);
                switch (ModifiedResultStatus)
                {
                    case 0:
                        MessageDescription = "Can not update the Audit Report, Contact Admin.";
                        MessageType = "error";
                        break;
                    case 1:
                        MessageDescription = "You are not a valid User to Perform the Action.";
                        MessageType = "error";
                        break;
                    case 2:
                        MessageDescription = "Audit Report is not in a Status of Update.";
                        MessageType = "warning";
                        break;
                    case 3:
                        MessageDescription = "Audit Report Saved Successfully ";//Updated
                        MessageType = "success";
                        break;
                    case 4:
                        MessageDescription = "Submitted for Approval.";
                        MessageType = "success";
                        break;
                    case 5:
                        MessageDescription = "The Observation Due date is exceeding the Response Due Date.";
                        MessageType = "error";
                        break;
                    default:
                        MessageDescription = "Failed to Update, Please contact Admin.";
                        MessageType = "error";
                        break;
                }
                return Json(new { msg = HelpClass.SQLEscapeString(MessageDescription), msgType = MessageType }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult SubmitAuditCompletion(AuditCreate objAuditReport)
        {
            try
            {
                int EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                if (GetAuthorIDByAuditID(objAuditReport.AuditID) == EmpID)
                {
                    if (AuditReportStatus(objAuditReport.AuditID) == 6) //6 Represents Completed Audit
                    {
                        AizantIT_AuditReport objAR = dbAizantIT_QMS_Audit.AizantIT_AuditReport.Where(p => p.AuditID == objAuditReport.AuditID).SingleOrDefault();
                        if (objAuditReport.AuditCertificateDate != null && objAuditReport.AuditCertificateValidUntilDate != null)
                        {
                            objAR.CertifiedDate = Convert.ToDateTime(objAuditReport.AuditCertificateDate);
                            objAR.CertificateValidUntil = Convert.ToDateTime(objAuditReport.AuditCertificateValidUntilDate);
                        }
                        objAR.Status = 2; //Completed
                                          //If we want to merge only the Files that are added after the 1st merge we should writr the code here.
                                          // MergeOnlytheAuditAttachmentsWhichAreNotMergedBefore(objAuditReport.AuditID); commented since audit attachments are not Required.
                                          // dbAizantIT_QMS_Audit.AizantIT_SP_AuditAttachmentHistoryOnCompletion(objAuditReport.AuditID, EmpID, DateTime.Now);
                        objAR.CurrentResponseVersion = null;//once Complete the AuditReport Reset CurrentResponse version Number 
                        AuditAttachmentHistoryOnCompletion(objAuditReport.AuditID, dbAizantIT_QMS_Audit);
                        InsertInToAuditHistory(objAuditReport.AuditID, (int)QMS_Audit_Roles.AuditQA, 8, objAuditReport.AuditComments, dbAizantIT_QMS_Audit);
                        //Delete all the Observation Notifications ,Since audit was closed
                        int[] ObservationIDs = dbAizantIT_QMS_Audit.AizantIT_AuditObservation.Where(p => p.AuditID == objAuditReport.AuditID).Select(p => p.ObservationID).ToArray();
                        int[] NotificationIDs = dbAizantIT_QMS_Audit.AizantIT_Observation_NotifyLink.Where(p => ObservationIDs.Contains(p.ObservationID)).Select(p => p.NotificationID).ToArray();
                        dbAizantIT_QMS_Audit.AizantIT_Observation_NotifyLink.RemoveRange(
                        dbAizantIT_QMS_Audit.AizantIT_Observation_NotifyLink.Where(p => NotificationIDs.Contains(p.NotificationID)));
                        dbAizantIT_QMS_Audit.SaveChanges();
                        DelNotificationsAsync(NotificationIDs);
                        return Json(new { msg = "Audit Completion Submitted", msgType = "success" });
                    }
                    else
                    {
                        return Json(new { msg = "All Observations are not yet closed", msgType = "info" });
                    }
                }
                else
                {
                    return Json(new { msg = "You are not Authorized User", msgType = "warning" });
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" });
            }
        }

        public async Task<string> DelNotificationsAsync(int[] NotificationIDs)
        {
            NotificationActionByLinq objNotifications = new NotificationActionByLinq();
            objNotifications.DeleteNotification(NotificationIDs);
            return "";
        }
        #endregion

        #region bind fields by Selection
        // For Loading the Active TypeOf List based on CategoryType
        [HttpPost]
        public JsonResult GetTypeOfList(int TypeOfRepresents)
        {
            try
            {
                // For Loading the Client ddl
                List<SelectListItem> TypeOfList = new List<SelectListItem>();
                var TypeOfItemsList = (from objAuditTypeOf in dbAizantIT_QMS_Audit.AizantIT_TypeOf
                                       where objAuditTypeOf.IsActive == true && objAuditTypeOf.TypeOfRepresents == TypeOfRepresents
                                       select new { objAuditTypeOf.TypeOfID, objAuditTypeOf.TypeOfName }).ToList();
                foreach (var TypeOfItem in TypeOfItemsList)
                {
                    TypeOfList.Add(new SelectListItem
                    {
                        Text = TypeOfItem.TypeOfName,
                        Value = TypeOfItem.TypeOfID.ToString(),
                    });
                }
                var jsonData = TypeOfList;
                return Json(new { msg = jsonData, msgType = "success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        // For Loading the Auditors based on Category and BusinessDivision
        [HttpPost]
        public JsonResult GetAuditorsListByClientIDOrBusinessDivisionID(int ClientID = 0, int RegulatoryAgencyID = 0)
        {
            try
            {
                //For Loading the Auditors to DDL
                List<SelectListItem> AuditorsList = new List<SelectListItem>();
                List<SelectListItem> ObserverList = new List<SelectListItem>();
                if (ClientID != 0 && RegulatoryAgencyID != 0)
                {
                    //Auditors
                    var AuditorsItemsList = (from objAuditors in dbAizantIT_QMS_Audit.AizantIT_AuditorDetails
                                             where (objAuditors.BaseID == ClientID && objAuditors.BaseType == 1) || (objAuditors.BaseID == RegulatoryAgencyID && objAuditors.BaseType == 2)
                                             select new
                                             {
                                                 objAuditors.AuditorID,
                                                 AuditorName = (objAuditors.AuditorName
                                             )
                                             }).ToList();
                    foreach (var AuditorItem in AuditorsItemsList)
                    {
                        AuditorsList.Add(new SelectListItem
                        {
                            Text = AuditorItem.AuditorName,
                            Value = AuditorItem.AuditorID.ToString(),
                        });
                    }
                    //Observers
                    var ObserverItemList = (from objObserver in dbAizantIT_QMS_Audit.AizantIT_ObserverDetails
                                            where (objObserver.BaseID == ClientID && objObserver.BaseType == 1) || (objObserver.BaseID == RegulatoryAgencyID && objObserver.BaseType == 2)
                                            select new
                                            {
                                                objObserver.ObserverID,
                                                ObserverName = (objObserver.ObserverName
                                            )
                                            }).ToList();
                    foreach (var ObserverItem in ObserverItemList)
                    {
                        ObserverList.Add(new SelectListItem
                        {
                            Text = ObserverItem.ObserverName,
                            Value = ObserverItem.ObserverID.ToString(),
                        });
                    }
                }
                else if (ClientID != 0)
                {
                    //Auditors
                    var AuditorsItemsList = (from objAuditors in dbAizantIT_QMS_Audit.AizantIT_AuditorDetails
                                             where objAuditors.BaseID == ClientID && objAuditors.BaseType == 1
                                             select new
                                             {
                                                 objAuditors.AuditorID,
                                                 AuditorName = (objAuditors.AuditorName
                                             )
                                             }).ToList();
                    foreach (var AuditorItem in AuditorsItemsList)
                    {
                        AuditorsList.Add(new SelectListItem
                        {
                            Text = AuditorItem.AuditorName,
                            Value = AuditorItem.AuditorID.ToString(),
                        });
                    }
                    //Observers
                    var ObserverItemList = (from objObserver in dbAizantIT_QMS_Audit.AizantIT_ObserverDetails
                                            where objObserver.BaseID == ClientID && objObserver.BaseType == 1
                                            select new
                                            {
                                                objObserver.ObserverID,
                                                ObserverName = (objObserver.ObserverName
                                            )
                                            }).ToList();
                    foreach (var ObserverItem in ObserverItemList)
                    {
                        ObserverList.Add(new SelectListItem
                        {
                            Text = ObserverItem.ObserverName,
                            Value = ObserverItem.ObserverID.ToString(),
                        });
                    }

                }
                else if (RegulatoryAgencyID != 0)
                {
                    //Auditors
                    var AuditorsItemsList = (from objAuditors in dbAizantIT_QMS_Audit.AizantIT_AuditorDetails
                                             where objAuditors.BaseID == RegulatoryAgencyID && objAuditors.BaseType == 2
                                             select new
                                             {
                                                 objAuditors.AuditorID,
                                                 AuditorName = (objAuditors.AuditorName
                                             )
                                             }).ToList();
                    foreach (var AuditorItem in AuditorsItemsList)
                    {
                        AuditorsList.Add(new SelectListItem
                        {
                            Text = AuditorItem.AuditorName,
                            Value = AuditorItem.AuditorID.ToString(),
                        });
                    }
                    //Observers
                    var ObserverItemList = (from objObserver in dbAizantIT_QMS_Audit.AizantIT_ObserverDetails
                                            where objObserver.BaseID == RegulatoryAgencyID && objObserver.BaseType == 2
                                            select new
                                            {
                                                objObserver.ObserverID,
                                                ObserverName = (objObserver.ObserverName
                                            )
                                            }).ToList();
                    foreach (var ObserverItem in ObserverItemList)
                    {
                        ObserverList.Add(new SelectListItem
                        {
                            Text = ObserverItem.ObserverName,
                            Value = ObserverItem.ObserverID.ToString(),
                        });
                    }
                }
                var jsonAuditorsData = AuditorsList;
                var jsonObserverData = ObserverList;
                return Json(new { AuditorsMsg = jsonAuditorsData, ObserverMsg = jsonObserverData, msgType = "success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        // For Loading the Observers after creating the Observer in Audit Report
        [HttpPost]
        public JsonResult GetObserver()
        {
            try
            {
                //For Loading the Observers to DDL
                List<SelectListItem> ObserverList = new List<SelectListItem>();
                var AuditObserversItemsList = (from objObserver in dbAizantIT_QMS_Audit.AizantIT_ObserverDetails
                                               select new { objObserver.ObserverID, objObserver.ObserverName }).ToList();
                foreach (var ObserverItem in AuditObserversItemsList)
                {
                    ObserverList.Add(new SelectListItem()
                    {
                        Text = ObserverItem.ObserverName,
                        Value = ObserverItem.ObserverID.ToString()
                    });
                }
                var jsonData = ObserverList;
                return Json(new { msg = jsonData, msgType = "success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        // For Loading the Active Site List based on BusinessDivisionID
        [HttpPost]
        public JsonResult GetSiteListByBusinessDivisionID(int BusinessDivisionID)
        {
            try
            {
                // For Loading the Client ddl
                List<SelectListItem> SiteList = new List<SelectListItem>();
                var SiteItemsList = (from objAuditSite in dbAizantIT_QMS_Audit.AizantIT_Site_BusinessDivision
                                     where objAuditSite.IsActive == true && objAuditSite.BusinessDivisionID == BusinessDivisionID
                                     select new { objAuditSite.SiteID, objAuditSite.SiteName }).ToList();
                foreach (var SiteItem in SiteItemsList)
                {
                    SiteList.Add(new SelectListItem
                    {
                        Text = SiteItem.SiteName,
                        Value = SiteItem.SiteID.ToString(),
                    });
                }
                var jsonData = SiteList;
                return Json(new { msg = jsonData, msgType = "success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        // For Loading the Dept List based on SiteID
        [HttpPost]
        public JsonResult GetDeptListBySiteID(int SiteID = 0)
        {
            try
            {
                // For Loading the Department ddl
                List<SelectListItem> DeptList = new List<SelectListItem>();
                var DeptItemsList = dbAizantIT_QMS_Audit.Database.SqlQuery<Models.Audit.ViewModel.DepartmentList>("exec [QMS_Audit].AizantIT_SP_GetSiteDepartmentsbySiteID " + SiteID).ToList();
                foreach (var DeptItem in DeptItemsList)
                {
                    DeptList.Add(new SelectListItem
                    {
                        Text = DeptItem.DeptName,
                        Value = DeptItem.DeptID.ToString(),
                    });
                }
                var jsonData = DeptList;
                return Json(new { msg = jsonData, msgType = "success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        // For Loading the Employees Based on Accessible DeptID , RoleID and ActiveStatus
        [HttpPost]
        public JsonResult GetEmpByDeptIDRoleID(int DeptID = 0, int RoleID = 0, string EmpStatus = "")
        {
            try
            {
                var jsonData = GetListofEmpByDeptIDRoleIDAndStatus(true, DeptID, RoleID, EmpStatus);
                return Json(new { msg = jsonData, msgType = "success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Audit Observation
        [HttpPost]
        public JsonResult CreateAuditObservation(AuditObservation auditObservationAddPost)
        {
            try
            {
                string MessageDescription = "", MessageType = "";
                DateTime DueDate = DateTime.Now;
                DataTable dtAreas = new DataTable();
                dtAreas.Columns.Add("FDA_SystemID");
                dtAreas.Columns.Add("AreaID");
                dtAreas.Columns.Add("SubAreaID");
                dtAreas.Columns.Add("Remarks");
                foreach (var item in auditObservationAddPost.ObservationSystems)
                {
                    DataRow drAreas = dtAreas.NewRow();
                    drAreas["FDA_SystemID"] = item.FDA_SystemID;
                    drAreas["AreaID"] = item.AreaID;
                    drAreas["SubAreaID"] = item.SubAreaID;
                    if (item.Remarks == "N/A")
                    {
                        drAreas["Remarks"] = null;
                    }
                    else
                    {
                        drAreas["Remarks"] = item.Remarks;
                    }
                    dtAreas.Rows.Add(drAreas);
                }
                SqlParameter ParamAuditID = new SqlParameter("@AuditID", auditObservationAddPost.AuditID);
                SqlParameter ParamObservationNumber = new SqlParameter("@ObservationNumber", auditObservationAddPost.ObservationNumber);
                SqlParameter ParamObservationSiteID = new SqlParameter("@ObservationSiteID", auditObservationAddPost.ObservationSiteID);
                SqlParameter ParamObservationDeptID = new SqlParameter("@ObservationDeptID", auditObservationAddPost.ObservationDeptID);
                SqlParameter ParamObservationPriorityID = new SqlParameter("@ObservationPriorityID", auditObservationAddPost.ObservationPriorityID);
                if (auditObservationAddPost.ObservationDueDate != "" && auditObservationAddPost.ObservationDueDate != null)
                {
                    DueDate = Convert.ToDateTime(auditObservationAddPost.ObservationDueDate);
                }
                SqlParameter ParamObservationDueDate = new SqlParameter("@ObservationDueDate", DueDate);
                SqlParameter ParamObservationIsResponsiblePersonEntry = new SqlParameter("@ObservationIsResponsiblePersonEntry", auditObservationAddPost.IsResponsiblePersonEntry);
                SqlParameter ParamObservationResponsiblePerson = new SqlParameter("@ObservationResponsiblePersonID", auditObservationAddPost.ObservationResponsiblePersonID);
                SqlParameter ParamObservationStatusID = new SqlParameter("@ObservationStatusID", auditObservationAddPost.ObservationStatusID);
                SqlParameter ParamObservationDescription = new SqlParameter("@ObservationDescription", auditObservationAddPost.ObservationDescription);
                SqlParameter ParamObservationCreatedBy = new SqlParameter("@ObservationCreatedBy", Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                SqlParameter ParamActionRoleID = new SqlParameter("@ActionRoleID", (int)QMS_Audit_Roles.AuditQA);
                SqlParameter ParamObservationAreas = new SqlParameter("@ObservationSystems", dtAreas);
                SqlParameter ParamObservationComments = new SqlParameter("@ObservationComments", auditObservationAddPost.ObservationComents);
                SqlParameter ParamCreationStatus = new SqlParameter("@ObservationCreationStatus", SqlDbType.Int);
                ParamCreationStatus.Direction = ParameterDirection.Output;
                SqlParameter ParamCreatedObservationID = new SqlParameter("@CreatedObservationID", SqlDbType.Int);
                ParamCreatedObservationID.Direction = ParameterDirection.Output;
                ParamObservationAreas.TypeName = "QMS_Audit.AizantIT_TVP_ObservationSystems";
                dbAizantIT_QMS_Audit.Database.ExecuteSqlCommand("exec [QMS_Audit].[AizantIT_SP_CreateAuditObservation] " +
                "@ObservationSystems,@AuditID,@ObservationNumber,@ObservationSiteID,@ObservationDeptID,@ObservationPriorityID," +
                "@ObservationDueDate," + "@ObservationIsResponsiblePersonEntry," +
                "@ObservationResponsiblePersonID,@ObservationStatusID," +
                "@ObservationDescription,@ObservationCreatedBy,@ActionRoleID,@ObservationComments,@ObservationCreationStatus out,@CreatedObservationID out",
                 ParamObservationAreas, ParamAuditID, ParamObservationNumber, ParamObservationSiteID, ParamObservationDeptID, ParamObservationPriorityID, ParamObservationDueDate,
                 ParamObservationIsResponsiblePersonEntry,
                 ParamObservationResponsiblePerson, ParamObservationStatusID,
                 ParamObservationDescription, ParamObservationCreatedBy, ParamActionRoleID, ParamObservationComments, ParamCreationStatus, ParamCreatedObservationID);
                int CreationStatus = Convert.ToInt32(ParamCreationStatus.Value);
                int ObservtionID = Convert.ToInt32(ParamCreatedObservationID.Value);
                switch (CreationStatus)
                {
                    case 1:
                        MessageDescription = "Observation Created Successfully.";
                        MessageType = "success";
                        break;
                    case 2:
                        MessageDescription = "Observation Number Already Exists for this Audit Report.";
                        MessageType = "info";
                        break;
                    case 3:
                        MessageDescription = "Creation Failed, Since you are not Authorized person to create this observation.";
                        MessageType = "warning";
                        break;
                    default:
                        MessageDescription = "Creation Failed, contact Admin.";
                        MessageType = "error";
                        break;
                }
                return Json(new { msg = HelpClass.SQLEscapeString(MessageDescription), msgType = MessageType, ObservationID = ObservtionID }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult UpdateObservation(int ObservationID)
        {
            try
            {
                int SiteID = 0, DeptID = 0, ObservationPriorityID = 0, ResponsiblePersonID = 0;
                AuditObservation objAuditObservation = new AuditObservation();
                objAuditObservation.ObservationID = ObservationID;
                DAL_AuditReport objAuditDal = new DAL_AuditReport();
                DataSet dsObservationDetails = objAuditDal.GetAuditObservationDetails(ObservationID.ToString());
                if (dsObservationDetails.Tables.Count >= 1)
                {
                    if (dsObservationDetails.Tables[0].Rows.Count > 0)
                    {
                        objAuditObservation.ObservationID = ObservationID;
                        objAuditObservation.ObservationCurrentStatus = Convert.ToInt32(dsObservationDetails.Tables[0].Rows[0]["ObservationCurrentStatus"]);
                        objAuditObservation.AuditCurrentStatus = Convert.ToInt32(dsObservationDetails.Tables[0].Rows[0]["AuditCurrentStatus"]);
                        objAuditObservation.AuditStatus = Convert.ToInt32(dsObservationDetails.Tables[0].Rows[0]["AuditStatus"]);
                        objAuditObservation.ObservationNumber = dsObservationDetails.Tables[0].Rows[0]["ObservationNumber"].ToString();
                        SiteID = Convert.ToInt32(dsObservationDetails.Tables[0].Rows[0]["SiteID"]);
                        objAuditObservation.ObservationSiteID = Convert.ToInt32(dsObservationDetails.Tables[0].Rows[0]["SiteID"]);
                        objAuditObservation.ObservationSiteName = dsObservationDetails.Tables[0].Rows[0]["SiteName"].ToString();
                        objAuditObservation.ObservationDeptID = Convert.ToInt32(dsObservationDetails.Tables[0].Rows[0]["DeptID"]);
                        DeptID = Convert.ToInt32(dsObservationDetails.Tables[0].Rows[0]["DeptID"]);
                        objAuditObservation.ObservationDepartmentName = dsObservationDetails.Tables[0].Rows[0]["DepartmentName"].ToString();
                        ObservationPriorityID = Convert.ToInt32(dsObservationDetails.Tables[0].Rows[0]["PriorityID"]);
                        objAuditObservation.ObservationPriorityID = Convert.ToInt32(dsObservationDetails.Tables[0].Rows[0]["PriorityID"]);
                        objAuditObservation.ObservationPriorityName = dsObservationDetails.Tables[0].Rows[0]["ObservationPriority"].ToString();
                        objAuditObservation.ObservationDescription = dsObservationDetails.Tables[0].Rows[0]["ObservationDescription"].ToString();
                        if (dsObservationDetails.Tables[0].Rows[0]["IsResponsibleEmpEntry"].ToString() != "")
                        {
                            objAuditObservation.IsResponsiblePersonEntry = Convert.ToBoolean(dsObservationDetails.Tables[0].Rows[0]["IsResponsibleEmpEntry"]);
                        }
                        if (dsObservationDetails.Tables[0].Rows[0]["ResponsiblePersonID"].ToString() == "")
                        {
                            ResponsiblePersonID = 0;
                            objAuditObservation.ObservationResponsiblePersonID = 0;
                        }
                        else
                        {
                            ResponsiblePersonID = Convert.ToInt32(dsObservationDetails.Tables[0].Rows[0]["ResponsiblePersonID"]);
                            objAuditObservation.ObservationResponsiblePersonID = Convert.ToInt32(dsObservationDetails.Tables[0].Rows[0]["ResponsiblePersonID"]);
                        }
                        objAuditObservation.ResponsiblePersonName = dsObservationDetails.Tables[0].Rows[0]["ResponsibleEmpName"].ToString();
                        objAuditObservation.ObservationDueDate = dsObservationDetails.Tables[0].Rows[0]["ResponseDueDate"].ToString();
                    }
                }
                //For Loading the Sites of Audit Report in Observation Edit"
                List<SelectListItem> ObjAuditSiteList = new List<SelectListItem>();
                var AuditSiteItemsList = (from objAuditObservationTable in dbAizantIT_QMS_Audit.AizantIT_AuditObservation
                                          join objAuditOnSite in dbAizantIT_QMS_Audit.AizantIT_AuditOnSite on objAuditObservationTable.AuditID equals objAuditOnSite.AuditID
                                          join objAuditSite in dbAizantIT_QMS_Audit.AizantIT_Site_BusinessDivision on objAuditOnSite.SiteID equals objAuditSite.SiteID

                                          where objAuditObservationTable.ObservationID == ObservationID
                                          select new { objAuditOnSite.SiteID, objAuditSite.SiteName }).ToList();
                foreach (var SiteItem in AuditSiteItemsList)
                {
                    ObjAuditSiteList.Add(new SelectListItem()
                    {
                        Text = SiteItem.SiteName,
                        Value = SiteItem.SiteID.ToString(),
                        Selected = (SiteItem.SiteID == SiteID ? true : false)
                    });
                }
                objAuditObservation.SiteList = ObjAuditSiteList;
                //For Loading the Depts list of Site 
                List<SelectListItem> ObjSiteDeptList = new List<SelectListItem>();
                var SiteDeptItemsList = (from objSiteDepartments in dbAizantIT_QMS_Audit.AizantIT_Site_Depts
                                         join objDepartments in dbAizantIT_QMS_Audit.AizantIT_DepartmentMaster on objSiteDepartments.DepartmentID equals objDepartments.DeptID
                                         where objSiteDepartments.SiteID == SiteID
                                         select new { objSiteDepartments.DepartmentID, objDepartments.DepartmentName }).ToList();
                foreach (var DeptItem in SiteDeptItemsList)
                {
                    ObjSiteDeptList.Add(new SelectListItem()
                    {
                        Text = DeptItem.DepartmentName,
                        Value = DeptItem.DepartmentID.ToString(),
                        Selected = (DeptItem.DepartmentID == DeptID ? true : false)
                    });
                }
                objAuditObservation.SiteDeptList = ObjSiteDeptList;
                //For Loading the Approvers DDL using SP
                objAuditObservation.ResponsiblePersonList = GetListofEmpByDeptIDRoleIDAndStatus(true, objAuditObservation.ObservationDeptID, (int)QMS_Audit_Roles.AuditHOD, "A");
                return Json(new { objAuditObservation }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        // For Loading the View of Observation Edit
        public ActionResult _ViewObservationDetails(int ObservationID)
        {
            try
            {
                bool IsValidAuthorQA = false;
                bool IsObservationDeptIsStillAsAnAccessibleDeptForThisHOD = false;
                int CountOfAuditorQuaries = 0;
                int CountOfAuditObservationCapas = 0;
                AuditObservation objAuditObservation = new AuditObservation();
                objAuditObservation.ObservationID = ObservationID;
                DAL_AuditReport objAuditDal = new DAL_AuditReport();
                DataSet dsObservationDetails = objAuditDal.GetAuditObservationDetailsForView(ObservationID.ToString());
                if (dsObservationDetails.Tables.Count >= 1)
                {
                    if (dsObservationDetails.Tables[0].Rows.Count > 0)
                    {
                        objAuditObservation.ObservationCurrentStatus = Convert.ToInt32(dsObservationDetails.Tables[0].Rows[0]["ObservationCurrentStatus"]);
                        objAuditObservation.ObservationStatusID = Convert.ToInt32(dsObservationDetails.Tables[0].Rows[0]["ObservationStatus"]);
                        objAuditObservation.AuditCurrentStatus = Convert.ToInt32(dsObservationDetails.Tables[0].Rows[0]["AuditCurrentStatus"]);
                        objAuditObservation.AuditStatus = Convert.ToInt32(dsObservationDetails.Tables[0].Rows[0]["AuditStatus"]);
                        objAuditObservation.ObservationNumber = dsObservationDetails.Tables[0].Rows[0]["ObservationNumber"].ToString();
                        objAuditObservation.AuditNuber = dsObservationDetails.Tables[0].Rows[0]["AuditNumber"].ToString();
                        objAuditObservation.ObservationSiteName = dsObservationDetails.Tables[0].Rows[0]["SiteName"].ToString();
                        objAuditObservation.ObservationDeptID = Convert.ToInt32(dsObservationDetails.Tables[0].Rows[0]["DeptID"]);
                        objAuditObservation.ObservationDepartmentName = dsObservationDetails.Tables[0].Rows[0]["DepartmentName"].ToString();
                        objAuditObservation.ObservationPriorityName = dsObservationDetails.Tables[0].Rows[0]["ObservationPriority"].ToString();
                        objAuditObservation.ObservationDescription = dsObservationDetails.Tables[0].Rows[0]["ObservationDescription"].ToString();
                        objAuditObservation.ResponsiblePersonName = dsObservationDetails.Tables[0].Rows[0]["ResponsibleEmpName"].ToString();
                        objAuditObservation.ObservationDueDate = dsObservationDetails.Tables[0].Rows[0]["ResponseDueDate"].ToString();
                        objAuditObservation.IsResponseStatusClosed = dsObservationDetails.Tables[0].Rows[0]["IsResponseStatusClosed"].ToString();
                        objAuditObservation.LastResponseDate = dsObservationDetails.Tables[0].Rows[0]["LastResponseSubmittedDate"].ToString();
                        objAuditObservation.AuthorName = ((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"]).ToString();
                        if (dsObservationDetails.Tables[0].Rows[0]["AuthorEmpID"].ToString() == (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString())
                        {
                            IsValidAuthorQA = true;
                        }
                        CountOfAuditorQuaries = Convert.ToInt32(dsObservationDetails.Tables[0].Rows[0]["CountOfAuditorQuaries"]);
                        CountOfAuditObservationCapas = Convert.ToInt32(dsObservationDetails.Tables[0].Rows[0]["CountOfAuditObservationCapas"]);
                        IsObservationDeptIsStillAsAnAccessibleDeptForThisHOD = Convert.ToBoolean(dsObservationDetails.Tables[0].Rows[0]["IsObservationDeptIsStillAsAnAccessibleDeptForThisHOD"]);
                    }
                }
                int AssignedObservationID = Convert.ToInt32(dsObservationDetails.Tables[0].Rows[0]["AssignedObservationID"]);

                #region ML_Observation Data
                API_BusinessLayer.API_BAL aPI_BAL = new API_BusinessLayer.API_BAL();
                //ML Feature is IsEnable 
                    if (Array.Exists(aPI_BAL.GetMLFeatureChecking(4), s => s.Equals(3)) == true)//4: Audit module ID,3:ML_Feature ID 
                    {
                    var ML_DataInfo = (from ML in dbAizantIT_QMS_Audit.AizantIT_AuditReport
                                       join Obser in dbAizantIT_QMS_Audit.AizantIT_AuditObservation on ML.AuditID equals Obser.AuditID
                                       where Obser.ObservationID == ObservationID
                                       select new
                                       {
                                           ML.ML_DataInfo
                                       }).SingleOrDefault();
                    objAuditObservation.MLDataInfo = ML_DataInfo.ML_DataInfo;

                    ViewBag.ML_FDACount = (from ML in dbAizantIT_QMS_Audit.AizantIT_Similar_FDA_Obv
                                           where ML.ObservationID == ObservationID
                                           select ML).Count();
                }
                #endregion

                bool AllowAddResponse = false;
                int EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                var IsResponsiblePerson = (from T1 in dbAizantIT_QMS_Audit.AizantIT_AssignedObservation
                                           join T2 in dbAizantIT_QMS_Audit.AizantIT_ResponseEmpIsNotEntry on T1.AssignedObservationID equals T2.AssignedObservationID
                                           where (T2.AssignedObservationID == AssignedObservationID)
                                           && (T1.Status == 1) // Open
                                           && (T2.ResponsibleEmpID == EmpID)
                                           select T2.ResponsibleEmpID).Any();
                if (IsResponsiblePerson && IsObservationDeptIsStillAsAnAccessibleDeptForThisHOD)
                {
                    AllowAddResponse = true;
                }
                ViewBag.AllowAddResponse = AllowAddResponse;
                ViewBag.IsValidAuthorQA = IsValidAuthorQA;
                ViewBag.CountOfAuditorQuaries = CountOfAuditorQuaries;
                ViewBag.CountOfAuditObservationCAPAs = CountOfAuditObservationCapas;
                ViewBag.HasOnlyViewerRole = HasOnlyThisRole((int)QMS_Audit_Roles.AuditViewer);
                ViewBag.IsResponseddlStatusCloseOptionEnable = IsAllCAPAS_ClosedOnThisObservation(ObservationID);
                //if Audit CurrentStatus not in state of Completed then Checking Audit CurrentResponseVersion if CurrentResponseVersion 
                int auditID = Convert.ToInt32(dsObservationDetails.Tables[0].Rows[0]["AuditID"]);
                //For New Response Button
                bool IsAlreadyResponseCreatedForThisVersion = (from objObservationResponse in dbAizantIT_QMS_Audit.AizantIT_ObservationResponse
                                                               join objAssignedObservation in dbAizantIT_QMS_Audit.AizantIT_AssignedObservation on objObservationResponse.AssignedObservationID equals objAssignedObservation.AssignedObservationID
                                                               join objAuditObservation1 in dbAizantIT_QMS_Audit.AizantIT_AuditObservation on objAssignedObservation.ObservationID equals objAuditObservation1.ObservationID
                                                               join objAuditReport in dbAizantIT_QMS_Audit.AizantIT_AuditReport on objAuditObservation1.AuditID equals objAuditReport.AuditID
                                                               where objAuditReport.AuditID == auditID && objAssignedObservation.ObservationID == ObservationID && objObservationResponse.ResponseVersion == objAuditReport.CurrentResponseVersion
                                                               orderby objAuditObservation1.ObservationID, objObservationResponse.ResponseID
                                                               select new { objObservationResponse.ResponseID }).Any();
                ViewBag.IsAlreadyResponseCreatedForThisVersion = IsAlreadyResponseCreatedForThisVersion;
                //For Reopen Button
                var AuditResponseReport = false;
                if (!IsAlreadyResponseCreatedForThisVersion)
                {
                    int AuditCurrentVersionID = Convert.ToInt32(dsObservationDetails.Tables[0].Rows[0]["CurrentResponseVersion"]);
                    if (objAuditObservation.AuditCurrentStatus != 8&& AuditCurrentVersionID!=0)
                    {
                        bool IsAuditQueryExistTothisObservationForthisResponseVersion = (from ObjAizantIT_AuditorQuery in dbAizantIT_QMS_Audit.AizantIT_AuditorQuery
                                                                                         where ObjAizantIT_AuditorQuery.AssignedObservationID == AssignedObservationID
                                                                                         && ObjAizantIT_AuditorQuery.ResponseVersion == AuditCurrentVersionID
                                                                                         select new { ObjAizantIT_AuditorQuery.AuditorQueryID }).Any();
                        if (!IsAuditQueryExistTothisObservationForthisResponseVersion)
                        {
                            AuditResponseReport = true;
                        }
                    }
                }
                objAuditObservation.IsObservationReOpen = AuditResponseReport == true ? "Y" : "N";

                return PartialView(objAuditObservation);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = LineNo + " :" + HelpClass.SQLEscapeString(ex.Message);
                return PartialView("_SysException");
            }
        }

        //public int LatestObservationHistoryStatus(int ObservationID)
        //{
        //    int ObservationHistoryID = GetLatestObservationHistoryID(ObservationID);
        //    int ObservationCurrentStatus = dbAizantIT_QMS_Audit.AizantIT_AuditObservationHistory.Where(u => u.ObservationHistoryID == ObservationHistoryID).Select(u => u.Status).SingleOrDefault();
        //    return ObservationCurrentStatus;
        //}
        [HttpPost]
        // For Submitting the Modified Observation 
        public JsonResult UpdateObservation(AuditObservation auditObservationUpdatePost)
        {
            try
            {
                string MessageDescription = "", MessageType = "";
                DateTime DueDate = DateTime.Now;
                DataTable dtSystems = new DataTable();
                dtSystems.Columns.Add("FDA_SystemID");
                dtSystems.Columns.Add("AreaID");
                dtSystems.Columns.Add("SubAreaID");
                dtSystems.Columns.Add("Remarks");
                if (auditObservationUpdatePost.ObservationSystems != null)
                {
                    if (auditObservationUpdatePost.ObservationSystems.Count != 0)
                    {
                        foreach (var item in auditObservationUpdatePost.ObservationSystems)
                        {
                            DataRow drSystems = dtSystems.NewRow();
                            drSystems["FDA_SystemID"] = item.FDA_SystemID;
                            drSystems["AreaID"] = item.AreaID;
                            drSystems["SubAreaID"] = item.SubAreaID;
                            drSystems["Remarks"] = item.Remarks;
                            dtSystems.Rows.Add(drSystems);
                        }
                    }
                }
                SqlParameter ParamObservationID = new SqlParameter("@ObservationID", auditObservationUpdatePost.ObservationID);
                SqlParameter ParamObservationNumber = new SqlParameter("@ObservationNumber", auditObservationUpdatePost.ObservationNumber);
                SqlParameter ParamObservationSiteID = new SqlParameter("@ObservationSiteID", auditObservationUpdatePost.ObservationSiteID);
                SqlParameter ParamObservationDeptID = new SqlParameter("@ObservationDeptID", auditObservationUpdatePost.ObservationDeptID);
                SqlParameter ParamObservationPriorityID = new SqlParameter("@ObservationPriorityID", auditObservationUpdatePost.ObservationPriorityID);
                if (auditObservationUpdatePost.ObservationDueDate != "" && auditObservationUpdatePost.ObservationDueDate != null)
                {
                    DueDate = Convert.ToDateTime(auditObservationUpdatePost.ObservationDueDate);
                }
                SqlParameter ParamObservationDueDate = new SqlParameter("@ObservationDueDate", DueDate);
                SqlParameter ParamObservationIsResponsiblePersonEntry = new SqlParameter("@ObservationIsResponsiblePersonEntry", auditObservationUpdatePost.IsResponsiblePersonEntry);
                SqlParameter ParamObservationResponsiblePersonID = new SqlParameter("@ObservationResponsiblePersonID", auditObservationUpdatePost.ObservationResponsiblePersonID);
                SqlParameter ParamObservationStatusID = new SqlParameter("@ObservationStatusID", auditObservationUpdatePost.ObservationStatusID);
                SqlParameter ParamObservationDescription = new SqlParameter("@ObservationDescription", auditObservationUpdatePost.ObservationDescription);
                SqlParameter ParamObservationModifiedBy = new SqlParameter("@ObservationModifiedBy", Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                SqlParameter ParamActionRoleID = new SqlParameter("@ActionRoleID", (int)QMS_Audit_Roles.AuditQA);
                SqlParameter ParamObservationHistoryStatusID = new SqlParameter("@ObservationHistoryStatusID", 1);
                SqlParameter ParamObservationSystems = new SqlParameter("@ObservationSystems", dtSystems);
                SqlParameter ParamObservationComments = new SqlParameter("@ObservationComments", "N/A");
                SqlParameter ParamModifiedResultStatus = new SqlParameter("@ObservationModifedResultStatus", SqlDbType.Int);
                ParamModifiedResultStatus.Direction = ParameterDirection.Output;
                ParamObservationSystems.TypeName = "QMS_Audit.AizantIT_TVP_ObservationSystems";
                dbAizantIT_QMS_Audit.Database.ExecuteSqlCommand("exec [QMS_Audit].[AizantIT_SP_UpdateAuditObservation] " +
                "@ObservationSystems,@ObservationID,@ObservationNumber,@ObservationSiteID,@ObservationDeptID,@ObservationPriorityID," +
                "@ObservationDueDate,@ObservationIsResponsiblePersonEntry," +
                "@ObservationResponsiblePersonID," +
                "@ObservationStatusID,@ObservationDescription,@ObservationModifiedBy,@ActionRoleID," +
                "@ObservationHistoryStatusID,@ObservationComments,@ObservationModifedResultStatus out",
                 ParamObservationSystems, ParamObservationID, ParamObservationNumber, ParamObservationSiteID, ParamObservationDeptID,
                 ParamObservationPriorityID, ParamObservationDueDate, ParamObservationIsResponsiblePersonEntry,
                 ParamObservationStatusID, ParamObservationDescription, ParamObservationResponsiblePersonID, ParamObservationModifiedBy, ParamActionRoleID,
                 ParamObservationHistoryStatusID, ParamObservationComments, ParamModifiedResultStatus);
                int CreationStatus = Convert.ToInt32(ParamModifiedResultStatus.Value);
                switch (CreationStatus)
                {
                    case 1:
                        MessageDescription = "Observation Updated Successfully.";
                        MessageType = "success";
                        break;
                    case 2:
                        MessageDescription = "Observation Number Already Exists for this Audit.";
                        MessageType = "warning";
                        break;
                    case 3:
                        MessageDescription = "Observation is not in a status of Update.";
                        MessageType = "warning";
                        break;
                    case 4:
                        MessageDescription = "you are not an Authorized person to update this Observation.";
                        MessageType = "info";
                        break;
                    default:
                        MessageDescription = "Failed to Update Observation, Please contact Admin.";
                        MessageType = "error";
                        break;
                }
                return Json(new { msg = HelpClass.SQLEscapeString(MessageDescription), msgType = MessageType }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        //For Accept Observation by Responsible HOD
        [HttpPost]
        public JsonResult ApproveObservationByResponsibleHOD(int ObservationID)
        {
            try
            {
                string MessageDescription = "", MessageType = "";

                bool IsRecordExists = (from itemObservation in dbAizantIT_QMS_Audit.AizantIT_AuditObservation
                                       where itemObservation.ObservationID == ObservationID
                                       select new { itemObservation.ObservationID }).Any();
                if (IsRecordExists)
                {
                    int AssignedObservationID = dbAizantIT_QMS_Audit.AizantIT_AssignedObservation.Where(u => u.ObservationID == ObservationID).Max(u => u.AssignedObservationID);
                    var objResponsiblePerson = (from ObjResponse in dbAizantIT_QMS_Audit.AizantIT_ResponseEmpIsNotEntry
                                                where ObjResponse.AssignedObservationID == AssignedObservationID
                                                select new { ObjResponse.ResponsibleEmpID }).Single();
                    int ResponsiblePersonID = objResponsiblePerson.ResponsibleEmpID;
                    if (ResponsiblePersonID == Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]))
                    {
                        if ((dbAizantIT_QMS_Audit.AizantIT_AssignedObservation.Where(u => u.AssignedObservationID == AssignedObservationID && u.Status == 1).Select(u => u.AcceptedDate).SingleOrDefault()) == null)
                        {
                            AizantIT_AssignedObservation dtAizantIT_AssignedObservation =
                            dbAizantIT_QMS_Audit.AizantIT_AssignedObservation.SingleOrDefault(p => p.AssignedObservationID == AssignedObservationID && p.Status == 1);
                            dtAizantIT_AssignedObservation.AcceptedDate = DateTime.Now;//Assigned Observation Status is closed
                            InsertInToObservationHistory(ObservationID, (int)QMS_Audit_Roles.AuditHOD, 3, null);// 3 Observation Approved
                            MessageDescription = "Observation Accepted"; MessageType = "success";
                            dbAizantIT_QMS_Audit.SaveChanges();
                            //Notification for Hod to complete the Response
                            NotificationActionByLinq objNotifications = new NotificationActionByLinq();
                            NotificationBO objNotificationBo = new NotificationBO();
                            objNotificationBo.NotificationID = GetObservationNotificationIDByObservationID(ObservationID);
                            objNotificationBo.Description = "Response is pending for the Observation '" + GetObservationNumberByObservationID(ObservationID) + "' of Audit Report '" + GetAuditNumberByAuditID(GetAuditIDByObservationID(ObservationID)) + "'.";
                            objNotificationBo.NotificationLink = "QMS/AuditMain/ViewAuditReport?viewType=ObservationAssignedCompleteList";
                            objNotificationBo.ModuleID = (int)Modules.QMS; List<int> objNotificationEmpIDs = new List<int>();
                            objNotificationEmpIDs.Add(ResponsiblePersonID);
                            objNotificationBo.NotificationEmpIDS = objNotificationEmpIDs;
                            List<NotificationRoleIDandDeptID> objNotificationRoleIDandDeptID = new List<NotificationRoleIDandDeptID>();
                            objNotificationRoleIDandDeptID.Add(new NotificationRoleIDandDeptID() { RoleID = (int)QMS_Audit_Roles.AuditHOD, DeptID = dbAizantIT_QMS_Audit.AizantIT_AuditObservation.Where(u => u.ObservationID == ObservationID).Select(u => u.DeptID).SingleOrDefault() });
                            objNotificationBo.NotificationRoleIDSandDeptIDs = objNotificationRoleIDandDeptID;
                            objNotifications.NotificationToNextEmp(objNotificationBo);
                        }
                        else
                        {
                            MessageDescription = "Record is not in a Status of Acceptance"; MessageType = "error";
                        }
                    }
                    else
                    {
                        MessageDescription = "You Are not Authorized Person to Accept"; MessageType = "error";
                    }
                }
                else
                {
                    MessageDescription = "There's no Observation with this details"; MessageType = "error";
                }
                return Json(new { msg = HelpClass.SQLEscapeString(MessageDescription), msgType = MessageType }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public void UpdateObservationHistory(int ObservationHistoryID, string HOD_Remarks)
        {
            try
            {
                AizantIT_AuditObservationHistory objAizantIT_AuditObservationHistory = new AizantIT_AuditObservationHistory();
                objAizantIT_AuditObservationHistory = dbAizantIT_QMS_Audit.AizantIT_AuditObservationHistory.SingleOrDefault(x => x.ObservationHistoryID == ObservationHistoryID);
                objAizantIT_AuditObservationHistory.Remarks = HOD_Remarks == "" ? null : HOD_Remarks;
                objAizantIT_AuditObservationHistory.ActionDate = DateTime.Now;
                dbAizantIT_QMS_Audit.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }
        [HttpPost]
        public JsonResult ReopenObservation(ReopenObservation reopenObservation)
        {
            try
            {
                var objAuidtObservation = dbAizantIT_QMS_Audit.AizantIT_AuditObservation.Where(p => p.ObservationID == reopenObservation.ObservationID && p.Status == 2).Select(p => p).SingleOrDefault();
                if (objAuidtObservation != null)
                {
                    //  int AuditID = dbAizantIT_QMS_Audit.AizantIT_AuditObservation.Where(u => u.ObservationID == reopenObservation.ObservationID).Select(u => u.AuditID).SingleOrDefault();--comment by Eswar.k 02/08/2019

                    var AuditData = (from auditReport in dbAizantIT_QMS_Audit.AizantIT_AuditReport
                                     join
                                      auditObservation in dbAizantIT_QMS_Audit.AizantIT_AuditObservation on auditReport.AuditID equals auditObservation.AuditID
                                     where auditObservation.ObservationID == reopenObservation.ObservationID
                                     select new { auditReport.AuditID, auditReport.CurrentResponseVersion }).SingleOrDefault();

                    if (GetAuthorIDByAuditID(AuditData.AuditID) == Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]))
                    {
                        if (AuditReportStatus(AuditData.AuditID) == 5 || AuditReportStatus(AuditData.AuditID) == 6 || AuditReportStatus(AuditData.AuditID) == 7)//5 approved Audit, 6 Response closed,7 reopen observations
                        {
                            int PreviousAssignedObservationID = dbAizantIT_QMS_Audit.AizantIT_AssignedObservation.Where(p => p.ObservationID == reopenObservation.ObservationID).Max(p => p.AssignedObservationID);
                            objAuidtObservation.Status = 3; //3 Represents Reopen
                                                            //Assigned Observation to Responsible Employee
                            AizantIT_AssignedObservation objAssignedObservation = new AizantIT_AssignedObservation();
                            objAssignedObservation.ObservationID = reopenObservation.ObservationID;
                            objAssignedObservation.Status = 1;
                            objAssignedObservation.DueDate = Convert.ToDateTime(reopenObservation.ResponseDueDate);
                            objAssignedObservation.AssignedDate = DateTime.Now;
                            objAssignedObservation.IsResponsibleEmpEntry = false;
                            dbAizantIT_QMS_Audit.AizantIT_AssignedObservation.Add(objAssignedObservation);
                            dbAizantIT_QMS_Audit.SaveChanges();

                            AizantIT_ResponseEmpIsNotEntry objResponsibleEmp = new AizantIT_ResponseEmpIsNotEntry();
                            objResponsibleEmp.ResponsibleEmpID = reopenObservation.ResponsibleEmpID;
                            objResponsibleEmp.AssignedObservationID = objAssignedObservation.AssignedObservationID;
                            dbAizantIT_QMS_Audit.AizantIT_ResponseEmpIsNotEntry.Add(objResponsibleEmp);

                            int ResponseID = dbAizantIT_QMS_Audit.AizantIT_ObservationResponse.Where(p => p.AssignedObservationID == PreviousAssignedObservationID).Max(p => p.ResponseID);

                            AizantIT_AuditorQuery objAuditorQuery = new AizantIT_AuditorQuery();
                            objAuditorQuery.AssignedObservationID = objAssignedObservation.AssignedObservationID;
                            objAuditorQuery.OnResponseID = ResponseID;
                            objAuditorQuery.AuditorQuery = reopenObservation.AuditorQuery;
                            objAuditorQuery.QueryDate = Convert.ToDateTime(reopenObservation.QueryDate);
                            objAuditorQuery.CreatedDate = DateTime.Now;
                            objAuditorQuery.ResponseVersion = (int)AuditData.CurrentResponseVersion;
                            dbAizantIT_QMS_Audit.AizantIT_AuditorQuery.Add(objAuditorQuery);

                            //Record transaction to History
                            InsertInToObservationHistory(reopenObservation.ObservationID, (int)QMS_Audit_Roles.AuditQA, 9, "");
                            InsertInToAuditHistory(AuditData.AuditID, (int)QMS_Audit_Roles.AuditQA, 7, "", dbAizantIT_QMS_Audit);// 7 for Re-opened Observations
                                                                                                           //Notification to HOD for Approval of Re-opened Observation
                            NotificationActionByLinq objNotifications = new NotificationActionByLinq();
                            NotificationBO objNotificationBo = new NotificationBO();
                            objNotificationBo.NotificationID = GetObservationNotificationIDByObservationID(reopenObservation.ObservationID);
                            objNotificationBo.Description = "Re-opened Observation '" + GetObservationNumberByObservationID(reopenObservation.ObservationID) + "' of Audit Report '" + GetAuditNumberByAuditID(GetAuditIDByObservationID(reopenObservation.ObservationID)) + "' is assigned to you.";
                            objNotificationBo.NotificationLink = "QMS/AuditMain/ViewAuditReport?viewType=ObservationAssignedCompleteList";
                            objNotificationBo.ModuleID = (int)Modules.QMS;
                            List<int> objNotificationEmpIDs = new List<int>();
                            objNotificationEmpIDs.Add(reopenObservation.ResponsibleEmpID);
                            objNotificationBo.NotificationEmpIDS = objNotificationEmpIDs;
                            List<NotificationRoleIDandDeptID> objNotificationRoleIDandDeptID = new List<NotificationRoleIDandDeptID>();
                            objNotificationRoleIDandDeptID.Add(new NotificationRoleIDandDeptID() { RoleID = (int)QMS_Audit_Roles.AuditHOD, DeptID = objAuidtObservation.DeptID });
                            objNotificationBo.NotificationRoleIDSandDeptIDs = objNotificationRoleIDandDeptID;
                            objNotifications.NotificationToNextEmp(objNotificationBo);
                            return Json(new { msg = "Observation Re-Opened.", msgType = "success" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { msg = "Audit Report is not in a status of this action", msgType = "error" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { msg = "You are not Authorized User.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { msg = "Observation not yet closed to Reopen", msgType = "error" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetObservationQueryList()
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            int ObservationID = 0;
            int.TryParse(Request.Params["ObservationID"], out ObservationID);
            List<ObservationQueryList> objAuditorQueries = new List<ObservationQueryList>();
            try
            {
                int totalRecordsCount = 0;
                var objAuditHistoryList = dbAizantIT_QMS_Audit.AizantIT_SP_AuditorObservationQuery(displayLength,
                        displayStart,
                        sortCol,
                        sSortDir,
                        sSearch != null ? sSearch.Trim() : "",
                        ObservationID);
                foreach (var item in objAuditHistoryList)
                {
                    if (totalRecordsCount == 0)
                    {
                        totalRecordsCount = Convert.ToInt32(item.TotalCount);
                    }
                    objAuditorQueries.Add(new ObservationQueryList(Convert.ToInt32(item.AssignedObservationID),
                        Convert.ToInt32(item.AuditorQueryID),
                        item.AuditorQuery, item.QueryDate));
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objAuditorQueries
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Observation System
        public ActionResult _ObservationSystem(string ShowType, int ObservationID = 0)
        {
            try
            {
                ObservationSystem ObjObservatonSystem = new ObservationSystem();
                List<SelectListItem> ObjSystemList = new List<SelectListItem>();
                var SystemItemsList = (from objArea in dbAizantIT_QMS_Audit.AizantIT_FDA_System
                                           //where objArea.IsActive == true
                                       select new { objArea.FDA_SystemID, objArea.FDA_SystemName }).ToList();
                foreach (var AuditorItem in SystemItemsList)
                {
                    ObjSystemList.Add(new SelectListItem()
                    {
                        Text = AuditorItem.FDA_SystemName,
                        Value = AuditorItem.FDA_SystemID.ToString()
                    });
                }
                ObjObservatonSystem.FDA6SystemList = ObjSystemList;
                ObjObservatonSystem.AreaList = new List<SelectListItem>();
                ObjObservatonSystem.SubAreaList = new List<SelectListItem>();
                var ObjObservationSystemData = (from ObjAizantIT_ObservationOnSystem in dbAizantIT_QMS_Audit.AizantIT_ObservationOnSystem
                                                join ObjAizantIT_FDA_System in dbAizantIT_QMS_Audit.AizantIT_FDA_System on ObjAizantIT_ObservationOnSystem.FDA_SystemID equals ObjAizantIT_FDA_System.FDA_SystemID
                                                join ObjAizantIT_Area in dbAizantIT_QMS_Audit.AizantIT_Area on ObjAizantIT_ObservationOnSystem.AreaID equals ObjAizantIT_Area.AreaID
                                                join ObjAizantIT_SubArea in dbAizantIT_QMS_Audit.AizantIT_SubArea on ObjAizantIT_ObservationOnSystem.SubAreaID equals ObjAizantIT_SubArea.SubAreaID
                                                where ObjAizantIT_ObservationOnSystem.ObservationID == ObservationID
                                                select new
                                                {
                                                    ObjAizantIT_ObservationOnSystem.ObservationOnSystemID,
                                                    ObjAizantIT_ObservationOnSystem.FDA_SystemID,
                                                    ObjAizantIT_FDA_System.FDA_SystemName,
                                                    ObjAizantIT_ObservationOnSystem.AreaID,
                                                    ObjAizantIT_Area.AreaName,
                                                    ObjAizantIT_ObservationOnSystem.SubAreaID,
                                                    ObjAizantIT_SubArea.SubAreaName,
                                                    ObjAizantIT_ObservationOnSystem.Remarks
                                                }).ToList();
                List<ObservationSystemList> objObseravationList = new List<ObservationSystemList>();
                foreach (var itemObjObservationSystemData in ObjObservationSystemData)
                {
                    objObseravationList.Add(new ObservationSystemList
                    {
                        ObservationOnSystemID = itemObjObservationSystemData.ObservationOnSystemID,
                        FDA_SystemID = itemObjObservationSystemData.FDA_SystemID,
                        FDA_SystemName = itemObjObservationSystemData.FDA_SystemName,
                        AreaID = itemObjObservationSystemData.AreaID,
                        AreaName = itemObjObservationSystemData.AreaName,
                        SubAreaID = itemObjObservationSystemData.SubAreaID,
                        SubAreaName = itemObjObservationSystemData.SubAreaName,
                        Remarks = itemObjObservationSystemData.Remarks == null ? "N/A" : itemObjObservationSystemData.Remarks
                    });
                }
                ObjObservatonSystem.ObservationSystemListData = objObseravationList;
                ViewBag.ShowType = ShowType;
                return PartialView(ObjObservatonSystem);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = LineNo + " :" + HelpClass.SQLEscapeString(ex.Message);
                return View();
            }
        }
        [HttpPost]
        //For Deleting the Observation System
        public JsonResult DeleteObservationSystem(string ObservationOnSystemiD)
        {
            try
            {
                int ObservationOnSystemid = Convert.ToInt32(ObservationOnSystemiD);
                int ObservationID = dbAizantIT_QMS_Audit.AizantIT_ObservationOnSystem
            .Where(p1 => p1.ObservationOnSystemID == ObservationOnSystemid).Select(p1 => p1.ObservationID).SingleOrDefault();
                int AuditID = dbAizantIT_QMS_Audit.AizantIT_AuditObservation
            .Where(p1 => p1.ObservationID == ObservationID).Select(p1 => p1.AuditID).SingleOrDefault();
                if (GetAuthorIDByAuditID(AuditID) == Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]))
                {
                    AizantIT_ObservationOnSystem objAizantIT_ObservationOnSystem = new AizantIT_ObservationOnSystem();
                    objAizantIT_ObservationOnSystem = dbAizantIT_QMS_Audit.AizantIT_ObservationOnSystem.SingleOrDefault(x => x.ObservationOnSystemID == ObservationOnSystemid);
                    dbAizantIT_QMS_Audit.AizantIT_ObservationOnSystem.Remove(objAizantIT_ObservationOnSystem);
                    dbAizantIT_QMS_Audit.SaveChanges();
                    return Json(new { msg = "Audit System Removed.", msgType = "success" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { msg = "You are not Authorized User", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult AddObservationSystem(int ObservationID, int FDA_SystemID, int AreaID, int SubAreaID, string Remarks)
        {
            try
            {
                int AuditID = dbAizantIT_QMS_Audit.AizantIT_AuditObservation
         .Where(p1 => p1.ObservationID == ObservationID).Select(p1 => p1.AuditID).SingleOrDefault();
                if (GetAuthorIDByAuditID(AuditID) == Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]))
                {
                    var IsRecordAlreadyExist = (from ObjAizantIT_ObservationOnSystem in dbAizantIT_QMS_Audit.AizantIT_ObservationOnSystem
                                                where ObjAizantIT_ObservationOnSystem.ObservationID == ObservationID
                                                && ObjAizantIT_ObservationOnSystem.FDA_SystemID == FDA_SystemID
                                                && ObjAizantIT_ObservationOnSystem.AreaID == AreaID
                                                && ObjAizantIT_ObservationOnSystem.SubAreaID == SubAreaID
                                                select ObjAizantIT_ObservationOnSystem.ObservationOnSystemID).Any();
                    if (!IsRecordAlreadyExist)
                    {
                        AizantIT_ObservationOnSystem objAizantIT_ObservationOnSystem = new AizantIT_ObservationOnSystem();
                        objAizantIT_ObservationOnSystem.FDA_SystemID = FDA_SystemID;
                        objAizantIT_ObservationOnSystem.SubAreaID = SubAreaID;
                        objAizantIT_ObservationOnSystem.AreaID = AreaID;
                        objAizantIT_ObservationOnSystem.ObservationID = ObservationID;
                        objAizantIT_ObservationOnSystem.Remarks = Remarks;
                        dbAizantIT_QMS_Audit.AizantIT_ObservationOnSystem.Add(objAizantIT_ObservationOnSystem);
                        dbAizantIT_QMS_Audit.SaveChanges();
                        int OnAuditSystemId = objAizantIT_ObservationOnSystem.ObservationOnSystemID;
                        return Json(new { msg = "System Added to this observation", msgType = "success", OnAuditSystemID = OnAuditSystemId }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { msg = "6 System, Area, Sub Area already exist.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { msg = "You are not Authorized User", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult UpdateObservationSystem(int OnObservationSystemID, int FDA_SystemID, int AreaID, int SubAreaID, string Remarks)
        {
            try
            {

                int ObservationID = dbAizantIT_QMS_Audit.AizantIT_ObservationOnSystem
         .Where(p1 => p1.ObservationOnSystemID == OnObservationSystemID).Select(p1 => p1.ObservationID).SingleOrDefault();
                if (GetAuthorIDByAuditID(GetAuditIDByObservationID(ObservationID)) == Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]))
                {
                    var IsRecordAlreadyExist = (from ObjAizantIT_ObservationOnSystem in dbAizantIT_QMS_Audit.AizantIT_ObservationOnSystem
                                                where ObjAizantIT_ObservationOnSystem.ObservationID == ObservationID
                                                && ObjAizantIT_ObservationOnSystem.FDA_SystemID == FDA_SystemID
                                                && ObjAizantIT_ObservationOnSystem.AreaID == AreaID
                                                && ObjAizantIT_ObservationOnSystem.SubAreaID == SubAreaID
                                                && ObjAizantIT_ObservationOnSystem.ObservationOnSystemID != OnObservationSystemID
                                                select ObjAizantIT_ObservationOnSystem.ObservationOnSystemID).Any();
                    if (!IsRecordAlreadyExist)
                    {
                        AizantIT_ObservationOnSystem objAizantIT_ObservationOnSystem = new AizantIT_ObservationOnSystem();
                        objAizantIT_ObservationOnSystem = dbAizantIT_QMS_Audit.AizantIT_ObservationOnSystem.SingleOrDefault(x => x.ObservationOnSystemID == OnObservationSystemID);
                        objAizantIT_ObservationOnSystem.FDA_SystemID = FDA_SystemID;
                        objAizantIT_ObservationOnSystem.SubAreaID = SubAreaID;
                        objAizantIT_ObservationOnSystem.AreaID = AreaID;
                        objAizantIT_ObservationOnSystem.ObservationID = ObservationID;
                        objAizantIT_ObservationOnSystem.Remarks = Remarks;
                        dbAizantIT_QMS_Audit.SaveChanges();
                        return Json(new { msg = "System Updated Successfully", msgType = "success" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { msg = "6 System, Area, Sub Area already exist.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { msg = "You are not Authorized User", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetAreaListByFDASystemID(int FDA_SystemID)
        {
            try
            {
                // For Loading the Client ddl
                List<SelectListItem> AreaList = new List<SelectListItem>();
                var AreaItemsList = (from objSystemArea in dbAizantIT_QMS_Audit.AizantIT_SystemArea
                                     join objAuditArea in dbAizantIT_QMS_Audit.AizantIT_Area on objSystemArea.AreaID equals objAuditArea.AreaID
                                     where objSystemArea.FDA_SystemID == FDA_SystemID
                                     select new { objAuditArea.AreaID, objAuditArea.AreaName }).ToList();
                foreach (var SubAreaItem in AreaItemsList)
                {
                    AreaList.Add(new SelectListItem
                    {
                        Text = SubAreaItem.AreaName,
                        Value = SubAreaItem.AreaID.ToString(),
                    });
                }
                var jsonData = AreaList;
                return Json(new { msg = jsonData, msgType = "success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        // For Loading the Active SubAreas List based on AreaID
        [HttpPost]
        public JsonResult GetSubAreaListByAreaID(int AreaID)
        {
            try
            {
                // For Loading the Client ddl
                List<SelectListItem> SubAreaList = new List<SelectListItem>();
                var SubAreaItemsList = (from objAuditSubArea in dbAizantIT_QMS_Audit.AizantIT_SubArea
                                        where objAuditSubArea.IsActive == true && objAuditSubArea.AreaID == AreaID
                                        select new { objAuditSubArea.SubAreaID, objAuditSubArea.SubAreaName }).ToList();
                foreach (var SubAreaItem in SubAreaItemsList)
                {
                    SubAreaList.Add(new SelectListItem
                    {
                        Text = SubAreaItem.SubAreaName,
                        Value = SubAreaItem.SubAreaID.ToString(),
                    });
                }
                var jsonData = SubAreaList;
                return Json(new { msg = jsonData, msgType = "success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Audit CAPA
        private AizantIT_DevEntities db = new AizantIT_DevEntities();
        //For Getting the Audit CAPANumber and Responsible person list (on department change), called form _ViewObservationDetails.cshtml
        [HttpGet]
        public JsonResult GetResponsiblePersonAndCAPANumberbyDepartmentID(int CAPA_DepartmentID)
        {
            try
            {
                AuditCAPA_BO ObjAuditCAPA_BO = new AuditCAPA_BO();
                //Code for Auto generated CAPA Number
                QMS_BAL objQMS_Bal = new QMS_BAL();
                ObjAuditCAPA_BO.AutogeneratedCAPA_Number = objQMS_Bal.BalGetAutoGenaratedNumber(CAPA_DepartmentID, 1).Rows[0][0].ToString();
                //Code for Responsible Employees ddl
                ObjAuditCAPA_BO.CAPA_ResponsiblePersonList = GetListofEmpByDeptIDRoleIDAndStatus(true, CAPA_DepartmentID, (int)QMS_Roles.User, "A", "-- Select Responsible Person --");

                return Json(new { CapaDetailsByDept = ObjAuditCAPA_BO, msgType = "success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        //For Getting the Audit CAPA default data(page load), called form _ViewObservationDetails.cshtml
        [HttpGet]
        public JsonResult CreateAuditCapa(int ObservationID)
        {
            try
            {
                AuditCAPA_BO ObjAuditCAPA_BO = new AuditCAPA_BO();
                // Get the Audit and Observation Details Number Based on ObservationID
                var AuditAndObservtionDetails = (from itemAuditObservation in dbAizantIT_QMS_Audit.AizantIT_AuditObservation
                                                 join itemAuditReport in dbAizantIT_QMS_Audit.AizantIT_AuditReport
                                                    on itemAuditObservation.AuditID equals itemAuditReport.AuditID
                                                 where (itemAuditObservation.ObservationID == ObservationID)
                                                 select new
                                                 {
                                                     itemAuditObservation.ObservationNum,
                                                     itemAuditObservation.DeptID,
                                                     itemAuditReport.ApproverEmpID,
                                                     itemAuditReport.AuthorEmpID,
                                                     itemAuditReport.InspectionOrAuditReportNum
                                                 }).SingleOrDefault();

                int ObservationDeptID = AuditAndObservtionDetails.DeptID;
                ObjAuditCAPA_BO.ObservationDeptID = ObservationDeptID;
                ObjAuditCAPA_BO.AuditQA_ID = AuditAndObservtionDetails.AuthorEmpID;
                ObjAuditCAPA_BO.AuditHeadQA_ID = AuditAndObservtionDetails.ApproverEmpID;
                ObjAuditCAPA_BO.CAPA_QualityEventNumber = AuditAndObservtionDetails.InspectionOrAuditReportNum + "/" + AuditAndObservtionDetails.ObservationNum;
                //Code for Responsible Employees ddl
                ObjAuditCAPA_BO.CAPA_ResponsiblePersonList = GetListofEmpByDeptIDRoleIDAndStatus(true, ObservationDeptID, (int)QMS_Roles.User, "A", "-- Select Responsible Person --");

                Aizant_Bl objAizantBal = new Aizant_Bl();
                int[] empId = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                ObjAuditCAPA_BO.Audit_QA_List = objAizantBal.GetListofEmpByRole(empId, (int)QMS_Audit_Roles.AuditQA, "A", "", "-- Select CAPA QA --");
                //GetListofEmpByRoleIDAndStatus(true, (int)QMS_Audit_Roles.AuditQA, "A", "-- Select CAPA QA --");
                //Code for Departments List for ddl.
                List<SelectListItem> AllDepartmentList = new List<SelectListItem>();
                var DepartmetItemsList = (from objDepartmet in dbAizantIT_QMS_Audit.AizantIT_DepartmentMaster
                                          select new { objDepartmet.DeptID, objDepartmet.DepartmentName }).ToList();
                foreach (var DepartmetItem in DepartmetItemsList)
                {
                    AllDepartmentList.Add(new SelectListItem
                    {
                        Text = DepartmetItem.DepartmentName,
                        Value = DepartmetItem.DeptID.ToString(),
                    });
                }
                ObjAuditCAPA_BO.AllDepartmentList = AllDepartmentList;
                //Code for Auto generated CAPA Number
                QMS_BAL objQMS_Bal = new QMS_BAL();
                ObjAuditCAPA_BO.AutogeneratedCAPA_Number = objQMS_Bal.BalGetAutoGenaratedNumber(ObservationDeptID, 1).Rows[0][0].ToString();
                // Code for ActionPlanList
                List<SelectListItem> ActionPlanList = new List<SelectListItem>();
                var ActionPlan = (from AD in db.AizantIT_ActionPlanDescription
                                  where AD.CurrentStatus == true
                                  select new { AD.Capa_ActionID, AD.Capa_ActionName }).ToList();
                ActionPlanList.Add(new SelectListItem
                {
                    Text = "-- Select Plan of Action --",
                    Value = "0",
                });
                foreach (var item in ActionPlan)
                {
                    ActionPlanList.Add(new SelectListItem
                    {
                        Text = item.Capa_ActionName,
                        Value = item.Capa_ActionID.ToString(),
                    });
                }
                ObjAuditCAPA_BO.PlanofActionList = ActionPlanList;
                return Json(new { AuditCapaDataBind = ObjAuditCAPA_BO, msgType = "success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        //For Inserting (Creating) the Audit CAPA
        [HttpPost]
        // For Creating the Audit Capa, called form _ViewObservationDetails.cshtml
        public JsonResult CreateAuditCapa(AuditCAPA_BO objCAPA_Creation)
        {
            try
            {
                string MessageDescription = "", MessageType = "";
                if (GetResponsibleHODIDByObservationID(objCAPA_Creation.ObservationID) == Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString()))
                {
                    CommonDatalayer objAuditDal = new CommonDatalayer();
                    CapaModel capaModel = new CapaModel();

                    capaModel.Department = (objCAPA_Creation.CAPA_DeptID).ToString();
                    capaModel.QualityEvent_Number = objCAPA_Creation.CAPA_QualityEventNumber;
                    capaModel.QualityEvent_TypeID = "9";//since it's is Audit by Naveen(QMS).
                    capaModel.PlanofAction = objCAPA_Creation.CAPA_PlanOfActionID;
                    capaModel.EmployeeName = objCAPA_Creation.CAPA_ResponsiblePersonID.ToString();
                    capaModel.TargetDate = objCAPA_Creation.CAPA_TargetDate;
                    capaModel.CapaQAEmpId = objCAPA_Creation.CAPA_QA_ID;
                    capaModel.HeadQa_EmpID = objCAPA_Creation.CAPA_HeadQA_ID;
                    capaModel.Capa_ActionDesc = objCAPA_Creation.CAPA_Description;
                    capaModel.EventsBaseID = objCAPA_Creation.ObservationID;
                    capaModel.AssessementUniqueID = 0;
                    capaModel.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                    int CreationStatus = objAuditDal.QualityEventsCapaInsertDal(capaModel, "audit");
                    if (CreationStatus > 0)
                    {
                        MessageDescription = "Capa Created Successfully.";
                        MessageType = "success";
                    }
                    else
                    {
                        MessageDescription = "Failed to create Capa, Please contact Admin.";
                        MessageType = "error";
                    }
                }
                else
                {
                    MessageDescription = "You are not Authorized User.";
                    MessageType = "warning";
                }
                return Json(new { msg = HelpClass.SQLEscapeString(MessageDescription), msgType = MessageType }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        // For Creating the Audit Capa List, called form _ViewObservationDetails.cshtml
        [HttpGet]
        public JsonResult GetObservationCAPA_List()
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            int ObservationID = 0;
            int.TryParse(Request.Params["ObservationID"], out ObservationID);

            List<ObservationCapaList> objObservationCapaList = new List<ObservationCapaList>();
            try
            {
                int totalRecordsCount = 0;
                var objAuditCAPA_List = dbAizantIT_QMS_Audit.AizantIT_SP_GetAuditObservationCAPAsList(displayLength,
                        displayStart,
                        sortCol,
                        sSortDir,
                        sSearch != null ? sSearch.Trim() : "",
                        ObservationID);
                foreach (var item in objAuditCAPA_List)
                {
                    if (totalRecordsCount == 0)
                    {
                        totalRecordsCount = Convert.ToInt32(item.TotalCount);
                    }
                    objObservationCapaList.Add(new ObservationCapaList(Convert.ToInt32(item.CapaID),
                        item.CapaDeptCode, item.CapaDepartment, item.capaNumber, item.capaDescription, item.CapaTargetDate, item.CapaCreatedDate,
                        item.capaResponsiblePerson, item.CapaPlanOfAction, item.capaStatus));
                }

                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objObservationCapaList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Audit Observation Response
        //For Creating the new Response of Completed Audit
        [HttpPost]
        public JsonResult CreateObservationResponseForCompletedAudit(ResponseList objResponse)
        {
            try
            {
                SqlParameter ParamObservationID = new SqlParameter("@ObservationID", objResponse.ObservationID);
                SqlParameter ParamActionRoleID = new SqlParameter("@ActionRoleID", (int)QMS_Audit_Roles.AuditQA);
                SqlParameter ParamActionByEmpID = new SqlParameter("@ActionByEmpID", Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                SqlParameter ParamResponseDescription = new SqlParameter("@ResponseDescription", objResponse.ResponseDescription);
                SqlParameter ParamIsResponsiblePersonEmpEntry = new SqlParameter("@IsResponsiblePersonEmpEntry", objResponse.IsResponsibleEmpEntry);
                SqlParameter ParamResponseSubmittedByEmpID = new SqlParameter("@ResponseSubmittedByEmpID", objResponse.ResponsibleEmpID);
                SqlParameter ParamResponseSubmittedByEmpName = new SqlParameter("@ResponseSubmittedByEmpName", objResponse.ResponsibleEmpName);
                SqlParameter ParamRemarks = new SqlParameter("@Remarks", objResponse.Comments);
                SqlParameter ParamResponseSubmittedDate = new SqlParameter("@ResponseSubmittedDate", objResponse.SubmittedDate);
                SqlParameter ParamResponseCreatedResult = new SqlParameter("@ResponseCreatedResult", SqlDbType.Int);
                ParamResponseCreatedResult.Direction = ParameterDirection.Output;

                dbAizantIT_QMS_Audit.Database.ExecuteSqlCommand("exec [QMS_Audit].[AizantIT_SP_CreateResponseForCompletedAudit] " +
                "@ObservationID,@ActionRoleID,@ActionByEmpID,@ResponseDescription,@IsResponsiblePersonEmpEntry," +
                "@ResponseSubmittedByEmpID,@ResponseSubmittedByEmpName,@Remarks,@ResponseSubmittedDate,@ResponseCreatedResult out",
                ParamObservationID, ParamActionRoleID, ParamActionByEmpID, ParamResponseDescription, ParamIsResponsiblePersonEmpEntry,
                ParamResponseSubmittedByEmpID, ParamResponseSubmittedByEmpName, ParamRemarks, ParamResponseSubmittedDate, ParamResponseCreatedResult);
                int CreatedResultStatus = Convert.ToInt32(ParamResponseCreatedResult.Value);
                string MessageDescription = "", MessageType = "";

                switch (CreatedResultStatus)
                {
                    case 1:
                        MessageDescription = "Response Submitted Successfully";
                        MessageType = "success";
                        break;
                    case 2:
                        MessageDescription = "You are not Authorized User";
                        MessageType = "warning";
                        break;
                    default:
                        MessageDescription = "Can not Update, Please contact Admin";
                        MessageType = "error";
                        break;
                }
                return Json(new { msg = HelpClass.SQLEscapeString(MessageDescription), msgType = MessageType }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult UpdateObservationResponseForCompletedAudit(int ResponseID)
        {
            try
            {
                ResponseList objObservationResponse = new ResponseList();

                var ObservationResponseMainDetails = (from objObservationResponseItem in dbAizantIT_QMS_Audit.AizantIT_ObservationResponse
                                                      join objAssignedObservationItem in dbAizantIT_QMS_Audit.AizantIT_AssignedObservation
                                                      on objObservationResponseItem.AssignedObservationID equals objAssignedObservationItem.AssignedObservationID
                                                      where objObservationResponseItem.ResponseID == ResponseID
                                                      select new
                                                      {
                                                          objObservationResponseItem.Description,
                                                          objObservationResponseItem.ResponseDate,
                                                          objAssignedObservationItem.AssignedObservationID,
                                                          objAssignedObservationItem.IsResponsibleEmpEntry,
                                                      }).SingleOrDefault();
                objObservationResponse.ResponseDescription = ObservationResponseMainDetails.Description;
                objObservationResponse.SubmittedDate = ObservationResponseMainDetails.ResponseDate == null ? "" : Convert.ToDateTime(ObservationResponseMainDetails.ResponseDate).ToString("dd MMM yyyy");
                objObservationResponse.IsResponsibleEmpEntry = ObservationResponseMainDetails.IsResponsibleEmpEntry;

                if (ObservationResponseMainDetails.IsResponsibleEmpEntry == true)
                {
                    var ObservationResponseEmpEntry = (from ObjRPEntry in dbAizantIT_QMS_Audit.AizantIT_ResponseEmpIsEntry
                                                       where ObjRPEntry.AssignedObservationID == ObservationResponseMainDetails.AssignedObservationID
                                                       select new { ObjRPEntry.ResponsibleEmpName }).SingleOrDefault();
                    objObservationResponse.ResponsibleEmpName = ObservationResponseEmpEntry.ResponsibleEmpName;
                    objObservationResponse.ResponsibleEmpID = 0;
                }
                else
                {
                    var ObservationResponseEmpNotEntry = (from ObjRPNotEntry in dbAizantIT_QMS_Audit.AizantIT_ResponseEmpIsNotEntry
                                                          join ObjEmpMaster in dbAizantIT_QMS_Audit.AizantIT_EmpMaster on ObjRPNotEntry.ResponsibleEmpID equals ObjEmpMaster.EmpID
                                                          where ObjRPNotEntry.AssignedObservationID == ObservationResponseMainDetails.AssignedObservationID
                                                          select new { ObjRPNotEntry.ResponsibleEmpID, ResponsibleEmpName = ObjEmpMaster.FirstName + " " + ObjEmpMaster.LastName }).SingleOrDefault();
                    objObservationResponse.ResponsibleEmpName = ObservationResponseEmpNotEntry.ResponsibleEmpName;
                    objObservationResponse.ResponsibleEmpID = ObservationResponseEmpNotEntry.ResponsibleEmpID;
                }
                return Json(new { objObservationResponse }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult UpdateObservationResponseForCompletedAudit(ResponseList objResponseUpdate)
        {
            try
            {
                string MessageDescription = "", MessageType = "";
                SqlParameter ParamUpdatedResult = new SqlParameter("@ResponseUpdatedResult", SqlDbType.Int);
                ParamUpdatedResult.Direction = ParameterDirection.Output;
                dbAizantIT_QMS_Audit.Database.ExecuteSqlCommand("exec [QMS_Audit].[AizantIT_SP_UpdateResponseForCompletedAudit] " +
                    objResponseUpdate.ResponseID + "," + (int)QMS_Audit_Roles.AuditQA + "," + Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) + ",'"
                    + objResponseUpdate.ResponseDescription + "','" + objResponseUpdate.IsResponsibleEmpEntry + "'," + objResponseUpdate.ResponsibleEmpID + ",'" + objResponseUpdate.ResponsibleEmpName + "','"
                    + objResponseUpdate.Comments + "','" + objResponseUpdate.SubmittedDate + "',@ResponseUpdatedResult out", ParamUpdatedResult);
                int UpdatedResultStatus = Convert.ToInt32(ParamUpdatedResult.Value);
                switch (UpdatedResultStatus)
                {
                    case 1:
                        MessageDescription = "There's no record available with this ID to Update";
                        MessageType = "error";
                        break;
                    case 2:
                        MessageDescription = "You are not a valid Author to update this Record";
                        MessageType = "warning";
                        break;
                    case 3:
                        MessageDescription = "Response is not in a status of Update";
                        MessageType = "warning";
                        break;
                    case 4:
                        MessageDescription = "Response Updated";
                        MessageType = "success";
                        break;
                    default:
                        MessageDescription = "can not Update, Please contact Admin";
                        MessageType = "error";
                        break;
                }
                return Json(new { msg = HelpClass.SQLEscapeString(MessageDescription), msgType = MessageType }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult ViewResponseDetails(int ResponseID)
        {
            try
            {
                ResponseList objObservationResponse = new ResponseList();
                var ObservationResponseMainDetails = (from objObservationResponseItem in dbAizantIT_QMS_Audit.AizantIT_ObservationResponse
                                                      join objAssignedObservationItem in dbAizantIT_QMS_Audit.AizantIT_AssignedObservation on objObservationResponseItem.AssignedObservationID equals objAssignedObservationItem.AssignedObservationID
                                                      where objObservationResponseItem.ResponseID == ResponseID
                                                      select new
                                                      {
                                                          objObservationResponseItem.Description,
                                                          objObservationResponseItem.ResponseDate,
                                                          objObservationResponseItem.ExpectedOrCloserDate,
                                                          objObservationResponseItem.IsCapaRequired,
                                                          objAssignedObservationItem.AssignedObservationID,
                                                          objAssignedObservationItem.IsResponsibleEmpEntry,
                                                      }).SingleOrDefault();
                objObservationResponse.ResponseDescription = ObservationResponseMainDetails.Description;
                objObservationResponse.SubmittedDate = ObservationResponseMainDetails.ResponseDate == null ? "" : Convert.ToDateTime(ObservationResponseMainDetails.ResponseDate).ToString("dd MMM yyyy");
                objObservationResponse.ExpectedOrCloserDate = ObservationResponseMainDetails.ExpectedOrCloserDate == null ? "" : Convert.ToDateTime(ObservationResponseMainDetails.ExpectedOrCloserDate).ToString("dd MMM yyyy");
                objObservationResponse.IsResponsibleEmpEntry = ObservationResponseMainDetails.IsResponsibleEmpEntry;
                objObservationResponse.IsCapaRequired = ObservationResponseMainDetails.IsCapaRequired == "N" ? 0 : 1;
                if (ObservationResponseMainDetails.IsResponsibleEmpEntry == true)
                {
                    var ObservationResponseEmpEntry = (from ObjRPEntry in dbAizantIT_QMS_Audit.AizantIT_ResponseEmpIsEntry
                                                       where ObjRPEntry.AssignedObservationID == ObservationResponseMainDetails.AssignedObservationID
                                                       select new { ObjRPEntry.ResponsibleEmpName }).SingleOrDefault();
                    objObservationResponse.ResponsibleEmpName = ObservationResponseEmpEntry.ResponsibleEmpName;
                    objObservationResponse.ResponsibleEmpID = 0;
                }
                else
                {
                    var ObservationResponseEmpNotEntry = (from ObjRPNotEntry in dbAizantIT_QMS_Audit.AizantIT_ResponseEmpIsNotEntry
                                                          join ObjEmpMaster in dbAizantIT_QMS_Audit.AizantIT_EmpMaster on ObjRPNotEntry.ResponsibleEmpID equals ObjEmpMaster.EmpID
                                                          where ObjRPNotEntry.AssignedObservationID == ObservationResponseMainDetails.AssignedObservationID
                                                          select new { ObjRPNotEntry.ResponsibleEmpID, ResponsibleEmpName = ObjEmpMaster.FirstName + " " + ObjEmpMaster.LastName }).SingleOrDefault();
                    objObservationResponse.ResponsibleEmpName = ObservationResponseEmpNotEntry.ResponsibleEmpName;
                    objObservationResponse.ResponsibleEmpID = ObservationResponseEmpNotEntry.ResponsibleEmpID;
                }
                return Json(new { objObservationResponse }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #region InProgress Audit Response
        [HttpPost]
        public JsonResult CreateObservationResponseForInProgressAudit(ResponseList objResponse)//,int ObservationID, string ResponseDescription, int ResponseStatusID, string ResponseClosureDate, string HODRemarks)
        {
            try
            {
                using (DbContextTransaction transaction = dbAizantIT_QMS_Audit.Database.BeginTransaction())
                {
                    try
                    {
                        string MessageDescription = "", MessageType = "";
                        int InprogressAuditObservationResponseID = 0;
                        // Check the Status of this Observation and Valid Response Person or not thenInsert into Observation Table and History Table
                        int AssignedObservationID = dbAizantIT_QMS_Audit.AizantIT_AssignedObservation.Where(u => u.ObservationID == objResponse.ObservationID).Max(u => u.AssignedObservationID);
                        if (AssignedObservationID != 0)
                        {
                            var objResponsiblePerson = (from ObjResponse in dbAizantIT_QMS_Audit.AizantIT_ResponseEmpIsNotEntry
                                                        where ObjResponse.AssignedObservationID == AssignedObservationID
                                                        select new { ObjResponse.ResponsibleEmpID }).Single();
                            if (objResponsiblePerson.ResponsibleEmpID == Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]))
                            {
                                int LatestObservationHistoryID = GetLatestObservationHistoryID(objResponse.ObservationID);
                                // Check weather already the response is created or not
                                int CurrentObservationStatus = dbAizantIT_QMS_Audit.AizantIT_AuditObservationHistory.Where(u => u.ObservationHistoryID == LatestObservationHistoryID).Select(u => u.Status).SingleOrDefault();
                                if (CurrentObservationStatus == 3 || CurrentObservationStatus == 8)//3 for Accepted Observation and 8 for Approved Response
                                {
                                    if ((objResponse.IsCapaRequired == 1 &&
                                       objResponse.ResponseCAPAS.Count > 0) || objResponse.IsCapaRequired == 0)
                                    {
                                        //Inserting Response Table
                                        AizantIT_ObservationResponse objAizantIT_AuditObservation = new AizantIT_ObservationResponse();
                                        objAizantIT_AuditObservation.AssignedObservationID = AssignedObservationID;
                                        //  objAizantIT_AuditObservation.ResponseNumber = (NumberOfResponseInObservation.Count() + 1);//When Response is Added Increment 1
                                        objAizantIT_AuditObservation.Description = objResponse.ResponseDescription;
                                        objAizantIT_AuditObservation.ResponseDate = DateTime.Now;
                                        objAizantIT_AuditObservation.IsCapaRequired = objResponse.IsCapaRequired == 1 ? "Y" : "N";
                                        if (objResponse.ExpectedOrCloserDate != null)
                                        {
                                            objAizantIT_AuditObservation.ExpectedOrCloserDate = Convert.ToDateTime(objResponse.ExpectedOrCloserDate).Date;
                                        }
                                        else
                                        {
                                            objAizantIT_AuditObservation.ExpectedOrCloserDate = null;
                                        }
                                        objAizantIT_AuditObservation.Status = objResponse.StatusID;
                                        objAizantIT_AuditObservation.ActionStatus = 1;//Response Created
                                        dbAizantIT_QMS_Audit.AizantIT_ObservationResponse.Add(objAizantIT_AuditObservation);
                                        InsertInToObservationHistory(objResponse.ObservationID, (int)QMS_Audit_Roles.AuditHOD, 4, objResponse.ResponsibleHOD_Comments);//4 Response Added
                                        dbAizantIT_QMS_Audit.SaveChanges();
                                        InprogressAuditObservationResponseID = objAizantIT_AuditObservation.ResponseID;

                                        if (objResponse.IsCapaRequired == 1)
                                        {
                                            foreach (var itemCAPA in objResponse.ResponseCAPAS)
                                            {
                                                QMSCommonActions CAPAObj = new QMSCommonActions();
                                                CapaModel objcapa = new CapaModel();
                                                objcapa.DeptID = itemCAPA.DeptID;
                                                objcapa.ResponsibleEmpID = itemCAPA.ResponsibleEmpID;
                                                objcapa.QualityEvent_TypeID = 9.ToString();
                                                objcapa.QualityEvent_Number = itemCAPA.QualityEvent_Number;
                                                objcapa.PlanofAction = itemCAPA.PlanofAction;
                                                objcapa.TargetDate = itemCAPA.TargetDate;
                                                objcapa.Capa_ActionDesc = itemCAPA.Capa_ActionDesc;
                                                objcapa.CreatedBy = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                                                objcapa.CapaQAEmpId = itemCAPA.QA_EmpId;
                                                objcapa.EventsBaseID = InprogressAuditObservationResponseID;
                                                int result = CAPAObj.InsertCAPADetails(objcapa, dbAizantIT_QMS_Audit);
                                            }
                                        }
                                        MessageDescription = "Response Created"; MessageType = "success";
                                    }
                                    else
                                    {
                                        MessageDescription = "There should at-least one CAPA for this response"; MessageType = "error";
                                    }
                                }
                                else
                                {
                                    MessageDescription = "Record is not in a status of this Action"; MessageType = "error";
                                }
                            }
                            else
                            {
                                MessageDescription = "You Are not Authorized Person to perform Action"; MessageType = "error";
                            }
                        }
                        else
                        {
                            MessageDescription = "Record Doesn't Exist"; MessageType = "error";
                        }
                        transaction.Commit();
                        return Json(new { msg = HelpClass.SQLEscapeString(MessageDescription), msgType = MessageType, CreatedResponseID = InprogressAuditObservationResponseID }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public bool IsAllCAPAS_ClosedOnThisObservation(int ObservationID)
        {
            int[] ResponseIDs = (from AO in dbAizantIT_QMS_Audit.AizantIT_AssignedObservation
                                 join OR in dbAizantIT_QMS_Audit.AizantIT_ObservationResponse
                                 on AO.AssignedObservationID equals OR.AssignedObservationID
                                 where AO.ObservationID == ObservationID
                                 select OR.ResponseID).ToArray();
            //Naveen Vang Please call ur Method by passing the ResponseIDS as Input parameters and return true or false
            //return AllCapasCompletedorNot(ResponseIDs);

            return !(from CA in dbAizantIT_QMS_Audit.AizantIT_CAPA_ActionPlan
                     join CM in dbAizantIT_QMS_Audit.AizantIT_CAPAMaster on CA.CAPAID equals CM.CAPAID
                     where !(new List<int?> { 9, 21, 22, 10 }).Contains(CA.CurrentStatus)
                     && ResponseIDs.Contains((int)CA.BaseID) && CM.QualityEvent_TypeID == 9
                     select CA).Any(); //9=Audit
        }
        [HttpGet]
        public JsonResult UpdateObservationResponseForInProgressAudit(int ResponseID)
        {
            try
            {
                ResponseList objObservationResponse = new ResponseList();
                var ObservationResponseMainDetails = (from objObservationResponseItem in dbAizantIT_QMS_Audit.AizantIT_ObservationResponse
                                                      join itemAssignedObservation in dbAizantIT_QMS_Audit.AizantIT_AssignedObservation 
                                                      on objObservationResponseItem.AssignedObservationID equals itemAssignedObservation.AssignedObservationID
                                                      where objObservationResponseItem.ResponseID == ResponseID
                                                      select new
                                                      {
                                                          itemAssignedObservation.ObservationID,
                                                          objObservationResponseItem.Description,
                                                          objObservationResponseItem.ResponseDate,
                                                          objObservationResponseItem.ExpectedOrCloserDate,
                                                          objObservationResponseItem.Status,
                                                          objObservationResponseItem.IsCapaRequired,
                                                          objObservationResponseItem.ActionStatus,
                                                      }).SingleOrDefault();
                objObservationResponse.ResponseDescription = ObservationResponseMainDetails.Description;
                objObservationResponse.SubmittedDate = ObservationResponseMainDetails.ResponseDate == null ? "" : Convert.ToDateTime(ObservationResponseMainDetails.ResponseDate).ToString("dd MMM yyyy");
                objObservationResponse.ExpectedOrCloserDate = ObservationResponseMainDetails.ExpectedOrCloserDate == null ? "" : Convert.ToDateTime(ObservationResponseMainDetails.ExpectedOrCloserDate).ToString("dd MMM yyyy");
                objObservationResponse.IsCapaRequired = ObservationResponseMainDetails.IsCapaRequired == "N" ? 0 : 1;
                objObservationResponse.StatusID = ObservationResponseMainDetails.Status;
                objObservationResponse.ActionStatusID = ObservationResponseMainDetails.ActionStatus;
                objObservationResponse.IsddlClosedEnable = IsAllCAPAS_ClosedOnThisObservation(ObservationResponseMainDetails.ObservationID);
                //For Getting the latest comments of HOD Or QA               

                objObservationResponse.ResponsibleHOD_Comments = GetLatestResponseComments((int)QMS_Audit_Roles.AuditQA, ObservationResponseMainDetails.ObservationID);               
                objObservationResponse.QAComments = GetLatestResponseComments((int)QMS_Audit_Roles.AuditHOD, ObservationResponseMainDetails.ObservationID);

                return Json(new { objObservationResponse }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult UpdateObservationResponseForInProgressAudit(ResponseList objResponse)//(int ResponseID, string ResponseDescription, int ResponseStatusID, string ResponseClosureDate, string HODRemarks)
        {
            try
            {
                string MessageDescription = "", MessageType = "";
                var ResponseObservationAuditDetails = (from itemObservationDetails in dbAizantIT_QMS_Audit.AizantIT_ObservationResponse
                                                      join itemAssignedObservation in dbAizantIT_QMS_Audit.AizantIT_AssignedObservation
                                                        on itemObservationDetails.AssignedObservationID equals itemAssignedObservation.AssignedObservationID
                                                      join itemAuditObservation in dbAizantIT_QMS_Audit.AizantIT_AuditObservation
                                                         on itemAssignedObservation.ObservationID equals itemAuditObservation.ObservationID
                                                      join itemAuditReport in dbAizantIT_QMS_Audit.AizantIT_AuditReport
                                                         on itemAuditObservation.AuditID equals itemAuditReport.AuditID
                                                      where (itemObservationDetails.ResponseID == objResponse.ResponseID)
                                                      select new { itemObservationDetails.ResponseID, itemObservationDetails.ActionStatus, itemAssignedObservation.AssignedObservationID, itemAuditObservation.ObservationID, itemAuditReport.AuditID, itemAuditReport.AuthorEmpID }).SingleOrDefault();
                int ObservationHistoryID = GetLatestObservationHistoryID(ResponseObservationAuditDetails.ObservationID);
                int ObservationCurrentStatusID = dbAizantIT_QMS_Audit.AizantIT_AuditObservationHistory.Where(u => u.ObservationHistoryID == ObservationHistoryID).Select(u => u.Status).SingleOrDefault();
                //Checking the Record Exist or not
                if (ResponseObservationAuditDetails != null)
                {
                    var objResponsiblePerson = (from ObjResponse in dbAizantIT_QMS_Audit.AizantIT_ResponseEmpIsNotEntry
                                                where ObjResponse.AssignedObservationID == ResponseObservationAuditDetails.AssignedObservationID
                                                select new { ObjResponse.ResponsibleEmpID }).Single();
                    //Checking whether valid user or not
                    if (objResponsiblePerson.ResponsibleEmpID == Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]))
                    {
                        //Checking the Status of Record
                        if (ResponseObservationAuditDetails.ActionStatus == 4 || ResponseObservationAuditDetails.ActionStatus == 1 || ResponseObservationAuditDetails.ActionStatus == 3)//4 Response Reverted or 1 for Response Added, 3 for Updated Response
                        {
                            //Check and Create whether There is at-least one CAPA for IsCaparequired1
                            if (objResponse.IsCapaRequired == 1
                                && GetAuditCAPACount(objResponse.ResponseID) > 0
                                || objResponse.IsCapaRequired == 0)
                            {
                                AizantIT_ObservationResponse dtAizantIT_ObservationDetails =
                                dbAizantIT_QMS_Audit.AizantIT_ObservationResponse.SingleOrDefault(p => p.ResponseID == objResponse.ResponseID);
                                dtAizantIT_ObservationDetails.Description = objResponse.ResponseDescription;
                                dtAizantIT_ObservationDetails.Status = objResponse.StatusID;
                                dtAizantIT_ObservationDetails.IsCapaRequired = objResponse.IsCapaRequired == 1 ? "Y" : "N";
                                if (objResponse.ExpectedOrCloserDate != null)
                                {
                                    dtAizantIT_ObservationDetails.ExpectedOrCloserDate = Convert.ToDateTime(objResponse.ExpectedOrCloserDate).Date;
                                }
                                else
                                {
                                    dtAizantIT_ObservationDetails.ExpectedOrCloserDate = null;
                                }
                                dtAizantIT_ObservationDetails.ActionStatus = ResponseObservationAuditDetails.ActionStatus == 4 ? 3 : 1;//3 for Modified Response ,1 for Response pending
                                dbAizantIT_QMS_Audit.SaveChanges();
                                MessageDescription = "Response Updated"; MessageType = "success";
                                if (ObservationCurrentStatusID == 7)// 7 for Reverted Response
                                {
                                    InsertInToObservationHistory(ResponseObservationAuditDetails.ObservationID, (int)QMS_Audit_Roles.AuditHOD, 6, objResponse.ResponsibleHOD_Comments);//6 for Response Modified
                                }
                                else if (ObservationCurrentStatusID == 6 || ObservationCurrentStatusID == 4)// If it is already Updated response updating the date and comments of it.
                                {
                                    UpdateObservationHistory(ObservationHistoryID, objResponse.ResponsibleHOD_Comments);
                                }
                            }
                            else
                            {
                                MessageDescription = "There Should be at-least one CAPA"; MessageType = "error";
                            }
                        }
                        else
                        {
                            MessageDescription = "Record is not in a Status of Update"; MessageType = "error";
                        }
                    }
                    else
                    {
                        MessageDescription = "You Are not Authorized Person to perform Action"; MessageType = "error";
                    }
                }
                else
                {
                    MessageDescription = "No record Exist with this details to perform action"; MessageType = "error";
                }
                return Json(new { msg = HelpClass.SQLEscapeString(MessageDescription), msgType = MessageType }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        //Given by Naveen
        public int GetAuditCAPACount(int BaseID)
        {
            int CAPACount = (from AP in dbAizantIT_QMS_Audit.AizantIT_CAPA_ActionPlan
                             join CM in dbAizantIT_QMS_Audit.AizantIT_CAPAMaster
                              on AP.CAPAID equals CM.CAPAID
                             where AP.BaseID == BaseID && CM.QualityEvent_TypeID == 9 && AP.CurrentStatus != 10
                             select AP).Count();
            return CAPACount;
        }

        [HttpPost]
        public JsonResult ObservationResponseSubmitForApprrovalByResponsibleHOD(int ObservationID)
        {
            try
            {
                string MessageDescription = "", MessageType = "";

                bool IsRecordExists = (from itemObservation in dbAizantIT_QMS_Audit.AizantIT_AuditObservation
                                       where itemObservation.ObservationID == ObservationID
                                       select new { itemObservation.ObservationID }).Any();
                if (IsRecordExists)
                {
                    int AssignedObservationID = dbAizantIT_QMS_Audit.AizantIT_AssignedObservation.Where(u => u.ObservationID == ObservationID).Max(u => u.AssignedObservationID);
                    var objResponsiblePerson = (from ObjResponse in dbAizantIT_QMS_Audit.AizantIT_ResponseEmpIsNotEntry
                                                where ObjResponse.AssignedObservationID == AssignedObservationID
                                                select new { ObjResponse.ResponsibleEmpID }).Single();
                    int ResponsiblePersonID = objResponsiblePerson.ResponsibleEmpID;
                    if (ResponsiblePersonID == Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]))
                    {
                        var ObservationResponse = (from itemObservationResponse in dbAizantIT_QMS_Audit.AizantIT_ObservationResponse
                                                   where itemObservationResponse.AssignedObservationID == AssignedObservationID && itemObservationResponse.ActionStatus != 5
                                                   select new { itemObservationResponse.ResponseID, itemObservationResponse.Status, itemObservationResponse.ActionStatus, itemObservationResponse.IsCapaRequired }).SingleOrDefault();
                        if (ObservationResponse != null)
                        {
                            if (ObservationResponse.ActionStatus != 2 && ObservationResponse.ActionStatus != 5)//For Created 
                            {

                                using (DbContextTransaction transaction = dbAizantIT_QMS_Audit.Database.BeginTransaction())
                                {
                                    try
                                    {
                                        AizantIT_ObservationResponse dtAizantIT_ObservationDetails =
                                                                                           dbAizantIT_QMS_Audit.AizantIT_ObservationResponse.SingleOrDefault(p => p.ResponseID == ObservationResponse.ResponseID);
                                        dtAizantIT_ObservationDetails.ResponseDate = DateTime.Now;
                                        dtAizantIT_ObservationDetails.ActionStatus = 2; //2 Submitted For Approval.
                                        dbAizantIT_QMS_Audit.SaveChanges();
                                        MessageDescription = "Response Submitted For Approval"; MessageType = "success";

                                        MakeResponseAttachmentsIsSubmitted(ObservationResponse.ResponseID, dbAizantIT_QMS_Audit);
                                        // dbAizantIT_QMS_Audit.AizantIT_SP_MakeResponseAttachmentsIsSubmitted(ObservationResponse.ResponseID, Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]), DateTime.Now);
                                        InsertInToObservationHistory(ObservationID, (int)QMS_Audit_Roles.AuditHOD, 5, null);// 5 Response Submitted For Approved 
                                        transaction.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        transaction.Rollback();
                                        throw ex;
                                    }
                                }
                                //Notification to QA for Approval of Response
                                NotificationActionByLinq objNotifications = new NotificationActionByLinq();
                                NotificationBO objNotificationBo = new NotificationBO();
                                objNotificationBo.NotificationID = GetObservationNotificationIDByObservationID(ObservationID);
                                objNotificationBo.Description = "Response submitted for the Observation '" + GetObservationNumberByObservationID(ObservationID) + "' of Audit Report '" + GetAuditNumberByAuditID(GetAuditIDByObservationID(ObservationID)) + "'.";
                                objNotificationBo.NotificationLink = "QMS/AuditMain/ViewAuditReport?viewType=ObservationReviewCompleteList";
                                objNotificationBo.ModuleID = (int)Modules.QMS;
                                List<int> objNotificationEmpIDs = new List<int>();
                                objNotificationEmpIDs.Add(GetAuthorIDByAuditID(GetAuditIDByObservationID(ObservationID)));
                                objNotificationBo.NotificationEmpIDS = objNotificationEmpIDs;
                                List<NotificationRoleIDandDeptID> objNotificationRoleIDandDeptID = new List<NotificationRoleIDandDeptID>();
                                objNotificationRoleIDandDeptID.Add(new NotificationRoleIDandDeptID() { RoleID = (int)QMS_Audit_Roles.AuditQA, DeptID = 0 });
                                objNotificationBo.NotificationRoleIDSandDeptIDs = objNotificationRoleIDandDeptID;
                                objNotifications.NotificationToNextEmp(objNotificationBo);
                                //According to TaskID:4836, 4832
                                if (ObservationResponse.IsCapaRequired == "Y")// Close the CPA Observation Notification if Response has CPAS.
                                {
                                    objNotifications.UpdateNotificationStatus(dbAizantIT_QMS_Audit.AizantIT_ObservationCAPALinkNotification
.Where(p1 => p1.ObservationID == ObservationID).Select(p1 => p1.NotificationID).SingleOrDefault(), 3);
                                }
                            }
                            else
                            {
                                MessageDescription = "Response is not in a status of Submit For Approval"; MessageType = "error";
                            }
                        }
                        else
                        {
                            MessageDescription = "There's no Response to submit for Approval"; MessageType = "error";
                        }
                    }
                    else
                    {
                        MessageDescription = "You Are not Authorized Person to perform this action"; MessageType = "error";
                    }
                }
                else
                {
                    MessageDescription = "There's no Observation with this details"; MessageType = "error";
                }
                return Json(new { msg = HelpClass.SQLEscapeString(MessageDescription), msgType = MessageType }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult InProgressAuditResponseApprovalByQA(int ResponseID, int ApprovalStatus, string RemarksByQA)
        {
            try
            {
                using (DbContextTransaction transaction = dbAizantIT_QMS_Audit.Database.BeginTransaction())
                {
                    try
                    {
                        string MessageDescription = ""; string MessageType = "";


                        var ResponseObservationAuditDetails = (from itemObservationDetails in dbAizantIT_QMS_Audit.AizantIT_ObservationResponse
                                                              join itemAssignedObservation in dbAizantIT_QMS_Audit.AizantIT_AssignedObservation on itemObservationDetails.AssignedObservationID equals itemAssignedObservation.AssignedObservationID
                                                              join itemAuditObservation in dbAizantIT_QMS_Audit.AizantIT_AuditObservation on itemAssignedObservation.ObservationID equals itemAuditObservation.ObservationID
                                                              join itemAuditReport in dbAizantIT_QMS_Audit.AizantIT_AuditReport on itemAuditObservation.AuditID equals itemAuditReport.AuditID
                                                              where (itemObservationDetails.ResponseID == ResponseID)
                                                              select new { itemObservationDetails.ResponseID, itemObservationDetails.Status, itemObservationDetails.ActionStatus, itemAssignedObservation.AssignedObservationID, itemAuditObservation.ObservationID, itemAuditObservation.DeptID, itemAuditReport.AuditID, itemAuditReport.AuthorEmpID, itemAuditReport.CurrentResponseVersion }).SingleOrDefault();
                        //Checking whether Record Exist with this ID or Not
                        if (ResponseObservationAuditDetails != null)
                        {
                            //Checking whether valid User or Not to perform this Action
                            if (ResponseObservationAuditDetails.AuthorEmpID == Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]))
                            {
                                //Checking the Status of Record whether to perform Action
                                if (ResponseObservationAuditDetails.ActionStatus == 2)// 2 For Response Submitted For Approval(Either after created or Modified)
                                {
                                    AizantIT_ObservationResponse dtAizantIT_ObservationDetails =
                                    dbAizantIT_QMS_Audit.AizantIT_ObservationResponse.SingleOrDefault(p => p.ResponseID == ResponseID);
                                    dtAizantIT_ObservationDetails.ActionStatus = ApprovalStatus;
                                    if (ApprovalStatus == 5) { dtAizantIT_ObservationDetails.ResponseVersion = (int)ResponseObservationAuditDetails.CurrentResponseVersion; }//ObservationResponse is Approved then update Audit CurrentResponsVersion into ObservationResponce Table 
                                    dbAizantIT_QMS_Audit.SaveChanges();
                                    MessageDescription = ApprovalStatus == 4 ? "Response Reverted" : "Response Approved"; MessageType = "success";
                                    int ObservationHistoryStatus = ApprovalStatus == 4 ? 7 : 8;//7 for Response reverted and 8 for response approved.
                                    if (ApprovalStatus == 5)
                                    {
                                        //Make can delete false for all the attachments of this Responses
                                        var ResponseAttachments = (from itemResponseAttachments in dbAizantIT_QMS_Audit.AizantIT_ResponseAttachment
                                                                       //join itemResponseAttachmentsList in dbAizantIT_QMS_Audit.AizantIT_ResponseAttachmentLink on itemResponseAttachments.AttachmentID equals itemResponseAttachmentsList.AttachmentID
                                                                   where itemResponseAttachments.ResponseID == ResponseID
                                                                   select new
                                                                   {
                                                                       itemResponseAttachments.AttachmentID//,itemResponseAttachments.CanDelete
                                                                   }
                                                                   ).ToList();
                                        dbAizantIT_QMS_Audit.SaveChanges();
                                    }
                                    InsertInToObservationHistory(ResponseObservationAuditDetails.ObservationID, (int)QMS_Audit_Roles.AuditQA, ObservationHistoryStatus, RemarksByQA == "" ? null : RemarksByQA);

                                    //If the Record is approved and ResponseStatus is Closed
                                    if (ResponseObservationAuditDetails.Status == 2 && ApprovalStatus == 5)//For Closed Response and Approved By QA
                                    {
                                        AizantIT_AssignedObservation dtAizantIT_AssignedObservation =
                                        dbAizantIT_QMS_Audit.AizantIT_AssignedObservation.SingleOrDefault(p => p.AssignedObservationID == ResponseObservationAuditDetails.AssignedObservationID && p.Status == 1);
                                        dtAizantIT_AssignedObservation.Status = 2;//Assigned Observation Status is closed

                                        AizantIT_AuditObservation dtAizantIT_AuditObservation =
                                        dbAizantIT_QMS_Audit.AizantIT_AuditObservation.SingleOrDefault(p => p.ObservationID == ResponseObservationAuditDetails.ObservationID && p.Status != 2);
                                        dtAizantIT_AuditObservation.Status = 2;//Observation Status is closed
                                        dbAizantIT_QMS_Audit.SaveChanges();

                                        //Observation Status is Closed , So CAPAObservationNotification should be Delete at here.
                                        int[] DeleteNotificationIDs = dbAizantIT_QMS_Audit.AizantIT_ObservationCAPALinkNotification.Where(p => p.ObservationID == ResponseObservationAuditDetails.ObservationID).Select(p => p.NotificationID).ToArray();
                                        dbAizantIT_QMS_Audit.AizantIT_ObservationCAPALinkNotification.RemoveRange(
                                        dbAizantIT_QMS_Audit.AizantIT_ObservationCAPALinkNotification.Where(p => p.ObservationID == ResponseObservationAuditDetails.ObservationID));
                                        dbAizantIT_QMS_Audit.SaveChanges();
                                        DelNotificationsAsync(DeleteNotificationIDs);
                                        bool AnyObservationsWithoutClosedStatus = (from itemAuditObservation in dbAizantIT_QMS_Audit.AizantIT_AuditObservation
                                                                                   where (itemAuditObservation.AuditID == ResponseObservationAuditDetails.AuditID && (itemAuditObservation.Status != 2))// 1 for Open Status of Observation, 3 for reopened
                                                                                   select new { itemAuditObservation.ObservationID }).Any();
                                        if (!AnyObservationsWithoutClosedStatus)
                                        {
                                            //Insert into the AuditHistory Table
                                            InsertInToAuditHistory(ResponseObservationAuditDetails.AuditID, (int)QMS_Audit_Roles.AuditQA, 6, "All Responses for all the Observations has Approved.", dbAizantIT_QMS_Audit);// 6 Observation Response Completed.
                                        }
                                        MessageDescription = "Observation Response has Approved."; MessageType = "success";
                                    }

                                    //For Notifications 
                                    NotificationActionByLinq objNotifications = new NotificationActionByLinq();
                                    int NotificationID = GetObservationNotificationIDByObservationID(ResponseObservationAuditDetails.ObservationID);
                                    if (ObservationHistoryStatus == 7) // Response reverted
                                    {
                                        NotificationBO objNotificationBo = new NotificationBO();
                                        objNotificationBo.NotificationID = NotificationID;
                                        objNotificationBo.Description = "Response is Reverted for the  Observation '" + GetObservationNumberByObservationID(ResponseObservationAuditDetails.ObservationID) + "' of Audit Report '" + GetAuditNumberByAuditID(GetAuditIDByObservationID(ResponseObservationAuditDetails.ObservationID)) + "'.";
                                        objNotificationBo.NotificationLink = "QMS/AuditMain/ViewAuditReport?viewType=ObservationAssignedCompleteList";
                                        objNotificationBo.ModuleID = (int)Modules.QMS;
                                        List<int> objNotificationEmpIDs = new List<int>();
                                        objNotificationEmpIDs.Add(GetResponsibleHODIDByObservationID(ResponseObservationAuditDetails.ObservationID));
                                        objNotificationBo.NotificationEmpIDS = objNotificationEmpIDs;
                                        List<NotificationRoleIDandDeptID> objNotificationRoleIDandDeptID = new List<NotificationRoleIDandDeptID>();
                                        objNotificationRoleIDandDeptID.Add(new NotificationRoleIDandDeptID() { RoleID = (int)QMS_Audit_Roles.AuditHOD, DeptID = ResponseObservationAuditDetails.DeptID });
                                        objNotificationBo.NotificationRoleIDSandDeptIDs = objNotificationRoleIDandDeptID;
                                        objNotifications.NotificationToNextEmp(objNotificationBo);
                                    }
                                    else if (ObservationHistoryStatus == 8)// Approved
                                    {
                                        //Notification should be in Inactive state.
                                        int[] EmpIDs = { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString()) };
                                        objNotifications.UpdateNotificationStatus(NotificationID, EmpIDs, 3);// 3 closed the Notification for this QA
                                        //For Approved Response by QA naveen Vanga ResponseID
                                        QMSCommonActions CAPAObj = new QMSCommonActions();

                                        CAPAObj.EventsApprove_SendNtificationToResponsiblePerson(ResponseID, 9);
                                        CAPAObj.EventCAPANumber_AutoGenerated(0, ResponseID, 9, Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString()));
                                        //Merge Response Attachments to Single File
                                        MergeResponseAttachmentstoSingleFile(ResponseID);
                                    }
                                }
                                else
                                {
                                    MessageDescription = "Record is not in a Status of Approval"; MessageType = "error";
                                }
                            }
                            else
                            {
                                MessageDescription = "Not a valid user to perform Action"; MessageType = "error";
                            }
                        }
                        else
                        {
                            MessageDescription = "No record Exist with this details to perform action"; MessageType = "error";
                        }
                        transaction.Commit();
                        return Json(new { msg = HelpClass.SQLEscapeString(MessageDescription), msgType = MessageType }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #endregion
        [HttpGet]
        public JsonResult GetAuditHistoryList()
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            int auditID = 0;
            int.TryParse(Request.Params["AuditID"], out auditID);
            List<HistoryDetailsInAudit> objAuditHistory = new List<HistoryDetailsInAudit>();
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    var objAuditHistoryList = dbAizantIT_QMS_Audit.AizantIT_SP_AuditReportHistory(displayLength,
                        displayStart,
                        sortCol,
                        sSortDir,
                        sSearch != null ? sSearch.Trim() : "",
                        auditID);
                    foreach (var item in objAuditHistoryList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objAuditHistory.Add(new HistoryDetailsInAudit(Convert.ToInt32(item.ActionHistoryID), item.ActionStatus,
                            item.ActionRole, item.ActionBy, item.ActionDate, item.Remarks));
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objAuditHistory
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetObservationHistory()
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            int observationID = 0;
            int.TryParse(Request.Params["ObservationID"], out observationID);
            List<HistoryDetailsInAudit> objAuditHistory = new List<HistoryDetailsInAudit>();
            try
            {
                //throw new Exception("sdfsdf");
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    var objAuditHistoryList = dbAizantIT_QMS_Audit.AizantIT_SP_ObservationHistory(displayLength,
                        displayStart,
                        sortCol,
                        sSortDir,
                        sSearch != null ? sSearch.Trim() : "",
                        observationID);
                    foreach (var item in objAuditHistoryList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objAuditHistory.Add(new HistoryDetailsInAudit(Convert.ToInt32(item.ActionHistoryID), item.ActionStatus,
                            item.ActionRole, item.ActionBy, item.ActionDate, item.Remarks));
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objAuditHistory
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        #region Audit Report Lists
        [CustAuthorization((int)Modules.QMS,
            (int)QMS_Audit_Roles.AuditQA,
            (int)QMS_Audit_Roles.AuditHOD,
            (int)QMS_Audit_Roles.AuditHeadQA,
            (int)QMS_Audit_Roles.AuditViewer,
            (int)QMS_Audit_Roles.AuditAdmin)]
        public ActionResult InitAuditReportList(string listType, int CategoryTypeID = 0, string RegulatoryCodeOrClientName = "", int AuditStatusID = 0)
        {
            try
            {
                if (listType == "AuditReportMain" || listType == "ObservationReportMain"
                ||
                (HasThisRole((int)QMS_Audit_Roles.AuditHOD) && (listType == "ObservationAssignedCompleteList" || listType == "ObservationRevertedResponse" || listType == "ObservationAcceptPending" || listType == "ObservationResponsePending" || listType == "ObservationDueDateReminderToHOD" || listType == "ObservationOverDueToHOD"))
                ||
                (HasThisRole((int)QMS_Audit_Roles.AuditHeadQA) && (listType == "OnlyForApproval" || listType == "Review" || listType == "AllDueAndOverDueObservationsList" || listType == "AllObservationOverDueDate" || listType == "AllObservationDueDateReminder"))
                ||
                (HasThisRole((int)QMS_Audit_Roles.AuditQA) && (listType == "Pending" || listType == "OnlyHeadQAReverted" || listType == "ObservationResponseCompleted" || listType == "ObservationReviewCompleteList" || listType == "AllDueAndOverDueObservationsList" || listType == "AllObservationDueDateReminder" || listType == "ObservationApprovalPending" || listType == "AllObservationOverDueDate")))
                {
                    ViewBag.TypeOfList = listType;
                    ViewBag.CategoryTypeID = CategoryTypeID;
                    if (CategoryTypeID == 3)
                    {
                        ViewBag.RegulatoryAgencyID = GetRegulatoryAgencyIDorClientID(CategoryTypeID, RegulatoryCodeOrClientName);
                        ViewBag.ClientID = 0;
                    }
                    if (CategoryTypeID == 4)
                    {
                        ViewBag.ClientID = GetRegulatoryAgencyIDorClientID(CategoryTypeID, RegulatoryCodeOrClientName);
                        ViewBag.RegulatoryAgencyID = 0;
                    }
                    ViewBag.AuditStatusID = AuditStatusID;
                    BindDropDownsInSearchFilter();
                    ViewBag.HasAuditHeadQARole = HasThisRole((int)QMS_Audit_Roles.AuditHeadQA);
                    ViewBag.HasAuditQARole = HasThisRole((int)QMS_Audit_Roles.AuditQA);
                }
                else
                {
                    ViewBag.TypeOfList = "";
                    ViewBag.SysError = "You are not Authorized User to view this page.";
                }
                return View();
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = LineNo + " :" + HelpClass.SQLEscapeString(ex.Message);
                return View();
            }
        }
        public int GetRegulatoryAgencyIDorClientID(int CategoryID, string RegulatoryCodeOrClientName)
        {
            int RegulatoryAgencyIDorClientID = 0;
            if (CategoryID == 3)//It is RegulatoryAgency
            {
                RegulatoryAgencyIDorClientID = dbAizantIT_QMS_Audit.AizantIT_RegulatoryAgency.Where(p => p.RegulatoryAgencyCode == RegulatoryCodeOrClientName).Select(p => p.RegulatoryAgencyID).SingleOrDefault();
            }
            else//Client
            {
                RegulatoryAgencyIDorClientID = dbAizantIT_QMS_Audit.AizantIT_Client.Where(p => p.ClientName == RegulatoryCodeOrClientName).Select(p => p.ClientID).SingleOrDefault();
            }
            return RegulatoryAgencyIDorClientID;
        }
        [HttpGet]
        public JsonResult GetAuditReportList()
        {
            try
            {
                int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                string TypeOfList = Request.Params["TypeOfList"];
                int CategoryID = 0;
                int.TryParse(Request.Params["CategoryID"], out CategoryID);
                int CategoryTypeID = 0;
                int.TryParse(Request.Params["CategoryTypeID"], out CategoryTypeID);
                int AuditStatusID = 0;
                int.TryParse(Request.Params["AuditStatusID"], out AuditStatusID);
                int TypeOfAuditID = 0;
                int.TryParse(Request.Params["TypeOfAuditID"], out TypeOfAuditID);
                int RegulatoryAgencyID = 0;
                int.TryParse(Request.Params["RegulatoryAgencyID"], out RegulatoryAgencyID);
                int ClientID = 0;
                int.TryParse(Request.Params["ClientID"], out ClientID);
                int BusinessDivisionID = 0;
                int.TryParse(Request.Params["BusinessDivisionID"], out BusinessDivisionID);
                string[] AuditSiteIDs = Request.Params["AuditSiteIDs"].Split(',');
                string FromAuditDate = Request.Params["FromAuditDate"];
                string ToAuditDate = Request.Params["ToAuditDate"];
                DataTable dtAuditSiteIds = new DataTable();
                dtAuditSiteIds.Columns.Add("SiteID");
                foreach (var SiteID in AuditSiteIDs)
                {
                    if (SiteID != "")
                    {
                        dtAuditSiteIds.Rows.Add(SiteID);
                    }
                }
                List<AuditReportList> objAuditList = new List<AuditReportList>();
                int totalRecordsCount = 0;
                int EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);

                DAL_AuditReport objAuditDal = new DAL_AuditReport();
                DataTable dtAuditReportList = objAuditDal.GetAuditReportList(displayLength, displayStart, sortCol, sSortDir, sSearch, TypeOfList, EmpID, CategoryID,
                    CategoryTypeID, AuditStatusID, TypeOfAuditID, RegulatoryAgencyID, ClientID, BusinessDivisionID, dtAuditSiteIds, FromAuditDate, ToAuditDate);
                if (dtAuditReportList.Rows.Count > 0)
                {
                    totalRecordsCount = Convert.ToInt32(dtAuditReportList.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dtAuditReportList.Rows.Count; i++)
                {
                    objAuditList.Add(
                       new AuditReportList(Convert.ToInt32(dtAuditReportList.Rows[i]["AuditID"]), dtAuditReportList.Rows[i]["AuditNum"].ToString(),
                            dtAuditReportList.Rows[i]["Category"].ToString(), dtAuditReportList.Rows[i]["CategoryType"].ToString(), dtAuditReportList.Rows[i]["BusinessDivision"].ToString(),
                            Convert.ToInt32(dtAuditReportList.Rows[i]["NoOfAuditDays"]),
                            dtAuditReportList.Rows[i]["AuditDates"].ToString(), dtAuditReportList.Rows[i]["ReportedDate"].ToString(),
                            dtAuditReportList.Rows[i]["AuditStatus"].ToString(), dtAuditReportList.Rows[i]["IsPublic"].ToString(), Convert.ToInt32(dtAuditReportList.Rows[i]["AuditStatusID"]), Convert.ToBoolean(dtAuditReportList.Rows[i]["IsAtleastOneResponseApproveforEveryObservation"])));
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objAuditList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        //For Binding the DDls in Filter POPUP
        public void BindDropDownsInSearchFilter()
        {
            AuditCreate objAuditSearchFilters = new AuditCreate();
            //For loading the Category DDl
            List<SelectListItem> objCategoryList = new List<SelectListItem>();
            var CategoryItemsList = (from objAuditCategory in dbAizantIT_QMS_Audit.AizantIT_AuditCategory
                                     select new { objAuditCategory.CategoryID, objAuditCategory.CategoryName }).ToList();
            objCategoryList.Add(new SelectListItem()
            {
                Text = " -- Select Category --",
                Value = "0"
            });
            foreach (var CategoryItem in CategoryItemsList)
            {
                objCategoryList.Add(new SelectListItem() { Text = CategoryItem.CategoryName, Value = CategoryItem.CategoryID.ToString() });
            }
            ViewBag.CategoryList = objCategoryList;
            //For Loading the CategoryType DDL for "ForAizant"
            List<SelectListItem> ObjCategoryTypeList = new List<SelectListItem>();
            var CategoryTypeItemsList = (from objAuditCategoryType in dbAizantIT_QMS_Audit.AizantIT_AuditCategoryType
                                         where objAuditCategoryType.CategoryID == 2 //&& objAuditCategoryType.IsActive == true // 2 represents For Aizant
                                         select new { objAuditCategoryType.TypeID, objAuditCategoryType.TypeName }).ToList();
            ObjCategoryTypeList.Add(new SelectListItem()
            {
                Text = " -- Select Category Type --",
                Value = "0"
            });
            foreach (var CategoryTypeItem in CategoryTypeItemsList)
            {
                ObjCategoryTypeList.Add(new SelectListItem()
                {
                    Text = CategoryTypeItem.TypeName,
                    Value = CategoryTypeItem.TypeID.ToString(),
                });
            }
            ViewBag.CategoryTypeList = ObjCategoryTypeList;

            // For Loading the Audit Client ddl
            List<SelectListItem> ObjAuditClientList = new List<SelectListItem>();
            var AuditClientItemsList = (from objAuditClient in dbAizantIT_QMS_Audit.AizantIT_Client
                                        //where objAuditClient.IsActive == true
                                        select new { objAuditClient.ClientID, objAuditClient.ClientName }).ToList();
            ObjAuditClientList.Add(new SelectListItem()
            {
                Text = " -- Select Client --",
                Value = "0"
            });
            foreach (var AuditClientItem in AuditClientItemsList)
            {
                ObjAuditClientList.Add(new SelectListItem()
                {
                    Text = Regex.Replace(AuditClientItem.ClientName, @"\s+", " "),
                    Value = AuditClientItem.ClientID.ToString()
                });
            }
            ViewBag.ClientList = ObjAuditClientList;
            // For Loading the RegulatoryAgency ddl
            List<SelectListItem> ObjRegulatoryAgencyList = new List<SelectListItem>();
            var RegulatoryAgencyItemsList = (from objRegulatoryAgency in dbAizantIT_QMS_Audit.AizantIT_RegulatoryAgency
                                             //where objRegulatoryAgency.IsActive == true
                                             select new { objRegulatoryAgency.RegulatoryAgencyID, AgencyName = (objRegulatoryAgency.RegulatoryAgencyCode + " (" + objRegulatoryAgency.RegulatoryAgencyName) + ")" }).ToList();
            ObjRegulatoryAgencyList.Add(new SelectListItem()
            {
                Text = " -- Select Regulatory Agency --",
                Value = "0"
            });
            foreach (var RegulatoryAgencyItem in RegulatoryAgencyItemsList)
            {
                ObjRegulatoryAgencyList.Add(new SelectListItem()
                {
                    Text = RegulatoryAgencyItem.AgencyName,
                    Value = RegulatoryAgencyItem.RegulatoryAgencyID.ToString()
                });
            }
            ViewBag.RegulatoryAgencyList = ObjRegulatoryAgencyList;

            //For Loading the BusinesDivision DDL
            List<SelectListItem> ObjBusinessDivisionList = new List<SelectListItem>();
            var BusinessDivisionItemsList = (from objBusinessDivision in dbAizantIT_QMS_Audit.AizantIT_Business_Division
                                             //where objBusinessDivision.IsActive == true
                                             select new { objBusinessDivision.BusinessDivisionID, BusinessDivisionName = (objBusinessDivision.BusinessDivisionCode + " - " + objBusinessDivision.BusinessDivisionName) }).ToList();
            ObjBusinessDivisionList.Add(new SelectListItem()
            {
                Text = " -- Select Business Division --",
                Value = "0"
            });
            foreach (var BusinessDivisionItem in BusinessDivisionItemsList)
            {
                ObjBusinessDivisionList.Add(new SelectListItem()
                {
                    Text = BusinessDivisionItem.BusinessDivisionName,
                    Value = BusinessDivisionItem.BusinessDivisionID.ToString()
                });
            }
            ViewBag.BusinessDivisionList = ObjBusinessDivisionList;

            //For Observation Filter
            //For Loading the Site DDL
            List<SelectListItem> ObjDepartmentList = new List<SelectListItem>();
            var DepartmentList = (from objDepartmentMaster in dbAizantIT_QMS_Audit.AizantIT_DepartmentMaster
                                  select new { objDepartmentMaster.DeptID, objDepartmentMaster.DepartmentName }).ToList();
            ObjDepartmentList.Add(new SelectListItem()
            {
                Text = " -- Select Department --",
                Value = "0"
            });
            foreach (var DeptListItem in DepartmentList)
            {
                ObjDepartmentList.Add(new SelectListItem()
                {
                    Text = DeptListItem.DepartmentName,
                    Value = DeptListItem.DeptID.ToString()
                });
            }
            ViewBag.DepartmentList = ObjDepartmentList;
            //For Loading the Site DDL
            List<SelectListItem> ObjSiteList = new List<SelectListItem>();
            var SiteItemsList = (from objSite_BusinessDivision in dbAizantIT_QMS_Audit.AizantIT_Site_BusinessDivision
                                 select new { objSite_BusinessDivision.SiteID, objSite_BusinessDivision.SiteName }).ToList();
            ObjSiteList.Add(new SelectListItem()
            {
                Text = " -- Select Site --",
                Value = "0"
            });
            foreach (var SiteItemsListItem in SiteItemsList)
            {
                ObjSiteList.Add(new SelectListItem()
                {
                    Text = SiteItemsListItem.SiteName,
                    Value = SiteItemsListItem.SiteID.ToString()
                });
            }
            ViewBag.SiteList = ObjSiteList;

        }
        // For Making the Audit to Public and Removing from Public
        [HttpPost]
        public JsonResult MakeAuditReportIsPublic(int AuditID, Boolean IsPublic)
        {
            try
            {
                string MessageDescription = "", MessageType = "";
                AizantIT_AuditReport objAizantIT_AuditReport = new AizantIT_AuditReport();
                objAizantIT_AuditReport = dbAizantIT_QMS_Audit.AizantIT_AuditReport.SingleOrDefault(x => x.AuditID == AuditID);
                if (objAizantIT_AuditReport != null)
                {
                    objAizantIT_AuditReport.IsPublic = IsPublic;
                    InsertInToAuditHistory(AuditID, (int)QMS_Audit_Roles.AuditHeadQA, IsPublic == true ? 10 : 9, null, dbAizantIT_QMS_Audit);
                    MessageDescription = IsPublic == true ? "Audit Report Made Public" : "Audit Report Removed from Public";
                    MessageType = "success";
                }
                else
                {
                    MessageDescription = "Record Doesn't Exist ";
                    MessageType = "error";
                }
                return Json(new { msg = HelpClass.SQLEscapeString(MessageDescription), msgType = MessageType }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Observation Report List
        [HttpGet]
        public JsonResult GetObservationReportList()
        {
            try
            {
                int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                string TypeOfObservationList = Request.Params["TypeOfList"];
                int AuditID = 0;
                int.TryParse(Request.Params["AuditID"], out AuditID);
                int DepartmentID = 0;
                int.TryParse(Request.Params["DepartmentID"], out DepartmentID);
                int ObservationSiteID = 0;
                int.TryParse(Request.Params["SiteID"], out ObservationSiteID);
                int ObservationCategoryID = 0;
                int.TryParse(Request.Params["CategoryID"], out ObservationCategoryID);
                int ObservationCurrentStatusID = 0;
                int.TryParse(Request.Params["StatusID"], out ObservationCurrentStatusID);
                //string[] AuditSiteIDs = Request.Params["AuditSiteIDs"].Split(',');
                string FromObservationDueDate = Request.Params["FromDate"];
                string ToObservationDueDate = Request.Params["ToDate"];             
                List<ObservationReportList> objAuditList = new List<ObservationReportList>();
                bool HasOnlyViewerRole = HasOnlyThisRole((int)QMS_Audit_Roles.AuditViewer);

                int totalRecordsCount = 0;
                int EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);

                DAL_AuditReport objAuditDal = new DAL_AuditReport();
                DataTable dtObservationReportList = objAuditDal.GetObservationReportList(displayLength, displayStart, sortCol, sSortDir, sSearch, TypeOfObservationList, EmpID, HasOnlyViewerRole, AuditID, DepartmentID,
                    ObservationSiteID, ObservationCategoryID, ObservationCurrentStatusID, FromObservationDueDate, ToObservationDueDate);
                if (dtObservationReportList.Rows.Count > 0)
                {
                    totalRecordsCount = Convert.ToInt32(dtObservationReportList.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dtObservationReportList.Rows.Count; i++)
                {
                    objAuditList.Add(
                       new ObservationReportList(Convert.ToInt32(dtObservationReportList.Rows[i]["AuditID"]), Convert.ToInt32(dtObservationReportList.Rows[i]["AuditCurrentStatusID"]), Convert.ToInt32(dtObservationReportList.Rows[i]["AuditCreationStatusID"]), Convert.ToInt32(dtObservationReportList.Rows[i]["AuditQAEmpID"]), Convert.ToInt32(dtObservationReportList.Rows[i]["ObservationID"]), Convert.ToInt32(dtObservationReportList.Rows[i]["ObservationStatusID"]), Convert.ToInt32(dtObservationReportList.Rows[i]["ObservationHODEmpID"]), Convert.ToBoolean(dtObservationReportList.Rows[i]["IsResponseApprovedForThisVersion"]), dtObservationReportList.Rows[i]["AuditNumber"].ToString(),
                           dtObservationReportList.Rows[i]["ObservationNumber"].ToString(), dtObservationReportList.Rows[i]["Author"].ToString(), dtObservationReportList.Rows[i]["ResponsiblePerson"].ToString(), dtObservationReportList.Rows[i]["SiteName"].ToString(),
                            dtObservationReportList.Rows[i]["Department"].ToString(), dtObservationReportList.Rows[i]["ObservationPriority"].ToString(), dtObservationReportList.Rows[i]["DueDate"].ToString(), dtObservationReportList.Rows[i]["ObservationStatusName"].ToString(), dtObservationReportList.Rows[i]["CurrentActionStatusName"].ToString()
                            ));
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objAuditList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        //To get Employees only by RoleID and DeptId
        public List<SelectListItem> GetListofEmpByDeptIDRoleIDAndStatus(bool IsCurrentLoginEmpRemove, int DeptID = 0, int RoleID = 0, string EmpStatus = "", string DefaultSelectText = "")
        {
            try
            {
                List<SelectListItem> EmployeeList = new List<SelectListItem>();
                bool IsOnlyActiveEmployees = false;
                if (EmpStatus == "A")
                {
                    IsOnlyActiveEmployees = true;
                }
                if (DefaultSelectText != "")
                {
                    EmployeeList.Add(new SelectListItem
                    {
                        Text = DefaultSelectText,
                        Value = "0",
                    });
                }
                UMS_BAL objUMS_Bal = new UMS_BAL();
                DataTable EmployeesItemsList = objUMS_Bal.GetEmployeesByDeptIDandRoleID_BAL(DeptID, RoleID, IsOnlyActiveEmployees);
                foreach (DataRow drEmpItem in EmployeesItemsList.Rows)
                {
                    if (Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) != Convert.ToInt32(drEmpItem["EmpID"]) && IsCurrentLoginEmpRemove)
                    {
                        EmployeeList.Add(new SelectListItem
                        {
                            Text = drEmpItem["EmpName"].ToString(),
                            Value = drEmpItem["EmpID"].ToString(),
                        });
                    }
                    else if (!IsCurrentLoginEmpRemove)
                    {
                        EmployeeList.Add(new SelectListItem
                        {
                            Text = drEmpItem["EmpName"].ToString(),
                            Value = drEmpItem["EmpID"].ToString(),
                        });
                    }
                }
                return EmployeeList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void InsertInToAuditHistory(int AuditID, int ActionRole, int Status, string Remarks, AizantIT_DevEntities dbAizantIT_QMS_AuditAuditHistory)
        {
            
            try
            {
                AizantIT_AuditReportHistory objAizantIT_AuditReportHistory = new AizantIT_AuditReportHistory();
                objAizantIT_AuditReportHistory.AuditID = AuditID;
                objAizantIT_AuditReportHistory.ActionRole = ActionRole;
                objAizantIT_AuditReportHistory.ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                objAizantIT_AuditReportHistory.ActionDate = DateTime.Now;
                objAizantIT_AuditReportHistory.Remarks = Remarks;
                objAizantIT_AuditReportHistory.Status = Status;
                dbAizantIT_QMS_AuditAuditHistory.AizantIT_AuditReportHistory.Add(objAizantIT_AuditReportHistory);
                dbAizantIT_QMS_AuditAuditHistory.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void InsertInToObservationHistory(int ObservationID, int ActionRole, int Status, string Remarks = "", DateTime? insertDatetime = null)
        {
            try
            {
                
                //Inserting into Observation HistoryTable
                AizantIT_AuditObservationHistory objAizantIT_AuditObservationHistory = new AizantIT_AuditObservationHistory();
                objAizantIT_AuditObservationHistory.ObservationID = ObservationID;
                objAizantIT_AuditObservationHistory.ActionRole = ActionRole;
                objAizantIT_AuditObservationHistory.ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                objAizantIT_AuditObservationHistory.ActionDate = insertDatetime ?? DateTime.Now;
                objAizantIT_AuditObservationHistory.Remarks = Remarks;
                objAizantIT_AuditObservationHistory.Status = Status;
                dbAizantIT_QMS_Audit.AizantIT_AuditObservationHistory.Add(objAizantIT_AuditObservationHistory);
                dbAizantIT_QMS_Audit.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetLatestResponseComments(int RoleID, int ObservationID)
        {
            int ObservationHistoryID=0;
            string Result="";
            var ObservationHistoryRecords = dbAizantIT_QMS_Audit.AizantIT_AuditObservationHistory
                    .Where(u => u.ObservationID == ObservationID
                    && u.Status != 10
                    && u.Status != 11
                    && u.Status != 12
                    && u.Status != 13
                    && u.Status != 14);           

            var ForHod = ObservationHistoryRecords.Where(p=>p.ActionRole==36 && p.Status==7).Any();
            var ForQa = ObservationHistoryRecords.Where(p => p.ActionRole == 35 && p.Status==5).Any();

            if (ForHod || ForQa)
            {
                var maxObservationHistoryID = ObservationHistoryRecords
                .Where(u => u.ObservationID == ObservationID
                && u.Status != 10
                && u.Status != 11
                && u.Status != 12
                && u.Status != 13
                && u.Status != 14).Max(u => u.ObservationHistoryID);

                var ObservationCurrentStatus = ObservationHistoryRecords
                    .Where(u => u.ObservationHistoryID == maxObservationHistoryID)
                    .Select(u => u.Status).SingleOrDefault();

                if (ForHod && RoleID == 35 && ObservationCurrentStatus==7)
                {
                    ObservationHistoryID = ObservationHistoryRecords
                       .Where(u => u.ActionRole == 36
                       && u.ObservationID == ObservationID
                       && u.Status != 10
                       && u.Status != 11
                       && u.Status != 12
                       && u.Status == 7
                       && u.Status != 13
                       && u.Status != 14).Max(u => u.ObservationHistoryID);
                }

                if (ForQa && RoleID == 36 && ObservationCurrentStatus == 5)
                {
                    var HodResponseModifiedOrNot = ObservationHistoryRecords.Where(p => p.ActionRole == 35 && p.Status == 6).Any();
                    if (HodResponseModifiedOrNot)
                    {
                        ObservationHistoryID = ObservationHistoryRecords
                       .Where(u => u.ActionRole == 35
                       && u.ObservationID == ObservationID
                       && u.Status != 10
                       && u.Status != 11
                       && u.Status != 12
                       && u.Status == 6
                       && u.Status != 13
                       && u.Status != 14).Max(u => u.ObservationHistoryID);
                    }
                }

                var Remarks = (from ObjResponseHistory in dbAizantIT_QMS_Audit.AizantIT_AuditObservationHistory
                               where ObjResponseHistory.ObservationHistoryID == ObservationHistoryID
                               select ObjResponseHistory.Remarks).SingleOrDefault();
                Result = Remarks != null ? Remarks : "";
            }
            return Result;
        }
        public int GetLatestObservationHistoryID(int ObservationID)
        {
            return dbAizantIT_QMS_Audit
                .AizantIT_AuditObservationHistory
                .Where(u=>u.ObservationID == ObservationID
                && u.Status != 10 
                && u.Status != 11 
                && u.Status != 12 
                && u.Status != 13 
                && u.Status != 14).Max(u => u.ObservationHistoryID);
        }

        public void MakeAuditNotificationstoVisited(int NotficationID)
        {

            if (NotficationID != 0)
            {
                NotificationActionByLinq objNotifications = new NotificationActionByLinq();
                int[] EmpIDs = { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString()) };
                objNotifications.UpdateNotificationStatus(NotficationID, EmpIDs, 2);//2 represents NotifyEmp had visited the page through notification link.
            }
        }
        public int GetAuditIDByNotificationID(int NotificationID)
        {
            int AuditID = dbAizantIT_QMS_Audit.AizantIT_Audit_NotifyLink
             .Where(p1 => p1.NotificationID == NotificationID).Select(p1 => p1.AuditID).SingleOrDefault();
            return AuditID;
        }
        public int GetObservationIDByObservationNotificationID(int NotificationID)
        {
            int ObservationID = dbAizantIT_QMS_Audit.AizantIT_Observation_NotifyLink
             .Where(p1 => p1.NotificationID == NotificationID).Select(p1 => p1.ObservationID).SingleOrDefault();
            return ObservationID;
        }
        public int GetObservationNotificationIDByObservationID(int ObservationID)
        {
            int ObservationNotificationID = dbAizantIT_QMS_Audit.AizantIT_Observation_NotifyLink
             .Where(p1 => p1.ObservationID == ObservationID).Select(p1 => p1.NotificationID).SingleOrDefault();
            return ObservationNotificationID;
        }
        public int GetObservationIDByResponseID(int ResponseID)
        {
            int ObservationID = (from ObjResponse in dbAizantIT_QMS_Audit.AizantIT_ObservationResponse
                                 join objAssignedObservation in dbAizantIT_QMS_Audit.AizantIT_AssignedObservation on ObjResponse.AssignedObservationID equals objAssignedObservation.AssignedObservationID
                                 where ObjResponse.ResponseID == ResponseID
                                 select objAssignedObservation.ObservationID).Single();
            return ObservationID;
        }
        public int GetAuditIDByObservationID(int ObservationID)
        {
            int AuditID = dbAizantIT_QMS_Audit.AizantIT_AuditObservation
            .Where(p1 => p1.ObservationID == ObservationID).Select(p1 => p1.AuditID).SingleOrDefault();
            return AuditID;
        }
        public int GetAuthorIDByAuditID(int AuditID)
        {
            int AuthorID = dbAizantIT_QMS_Audit.AizantIT_AuditReport
            .Where(p1 => p1.AuditID == AuditID).Select(p1 => p1.AuthorEmpID).SingleOrDefault();
            return AuthorID;
        }
       
        public int GetResponsibleHODIDByObservationID(int ObservationID)
        {
            int AssignedObservationID = dbAizantIT_QMS_Audit.AizantIT_AssignedObservation.Where(u => u.ObservationID == ObservationID).Max(u => u.AssignedObservationID);
            var objResponsiblePerson = (from ObjResponse in dbAizantIT_QMS_Audit.AizantIT_ResponseEmpIsNotEntry
                                        where ObjResponse.AssignedObservationID == AssignedObservationID
                                        select new { ObjResponse.ResponsibleEmpID }).Single();
            int ResponsibleHOD = objResponsiblePerson.ResponsibleEmpID;
            return objResponsiblePerson.ResponsibleEmpID;
        }
        public string GetObservationNumberByObservationID(int ObservationID)
        {
            string ObservationNumber = dbAizantIT_QMS_Audit.AizantIT_AuditObservation
            .Where(p1 => p1.ObservationID == ObservationID).Select(p1 => p1.ObservationNum).SingleOrDefault();
            return ObservationNumber;
        }
        public string GetAuditNumberByAuditID(int AuditID)
        {
            string AuditNumber = dbAizantIT_QMS_Audit.AizantIT_AuditReport
            .Where(p1 => p1.AuditID == AuditID).Select(p1 => p1.InspectionOrAuditReportNum).SingleOrDefault();
            return AuditNumber;
        }
        public int GetAuditCurrentVersionByAuditId(int AuditID)
        {
            int CurrentAuditVersion = Convert.ToInt32(dbAizantIT_QMS_Audit.AizantIT_AuditReport
            .Where(p1 => p1.AuditID == AuditID).Select(p1 => p1.CurrentResponseVersion).SingleOrDefault());
            return CurrentAuditVersion;
        }

        //Checking whether Has only Role
        public bool HasOnlyThisRole(int RoleID)
        {
            if ((Session["UserDetails"] as DataSet).Tables[1].Select("ModuleID=" + (int)Modules.QMS).Length == 1 &&
                (Session["UserDetails"] as DataSet).Tables[1].Select("ModuleID=" + (int)Modules.QMS).CopyToDataTable().Select("RoleID=" + RoleID).Length > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //Checking whether Has only Role
        public bool HasThisRole(int RoleID)
        {
            if ((Session["UserDetails"] as DataSet).Tables[1].Select("ModuleID=" + (int)Modules.QMS).Length > 0 &&
                (Session["UserDetails"] as DataSet).Tables[1].Select("ModuleID=" + (int)Modules.QMS).CopyToDataTable().Select("RoleID=" + RoleID).Length > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #region View Audit Report
        [HttpGet]
        [CustAuthorization((int)Modules.QMS,
            (int)QMS_Audit_Roles.AuditQA,
            (int)QMS_Audit_Roles.AuditHOD,
            (int)QMS_Audit_Roles.AuditHeadQA,
            (int)QMS_Audit_Roles.AuditViewer,
            (int)QMS_Audit_Roles.AuditAdmin)]
        public ActionResult ViewAuditReport(string AuditID, string viewType, int Notification_ID = 0)
        {
            try
            {
                if (viewType == "AuditReportMain" || viewType == "ObservationReportMain"
                ||
             ((HasThisRole((int)QMS_Audit_Roles.AuditHeadQA) || HasThisRole((int)QMS_Audit_Roles.AuditQA)) && (viewType == "AllDueAndOverDueObservationsList" || viewType == "AllObservationDueDateReminder" || viewType == "AllObservationOverDueDate"))
             ||
             (HasThisRole((int)QMS_Audit_Roles.AuditHeadQA) && (viewType == "OnlyForApproval" || viewType == "Review"))
             ||
                (HasThisRole((int)QMS_Audit_Roles.AuditQA) && (viewType == "Pending" || viewType == "OnlyHeadQAReverted" || viewType == "ObservationResponseCompleted" || viewType == "ObservationDueDateReminderToQA" || viewType == "ObservationApprovalPending" || viewType == "ObservationReviewCompleteList"))
             ||
             (HasThisRole((int)QMS_Audit_Roles.AuditHOD) && (viewType == "ObservationAssignedCompleteList" || viewType == "ObservationAcceptPending" || viewType == "ObservationResponsePending" || viewType == "ObservationRevertedResponse" || viewType == "ObservationDueDateReminderToHOD" || viewType == "ObservationOverDueToHOD"))
             )
                {
                    ViewBag.listType = viewType;
                    AuditReportView objAuditReportView = new AuditReportView();
                    //These are Lists for Responsible HOD and Author QA Observation Records
                    if (Notification_ID != 0 && (viewType == "ObservationAssignedCompleteList" || viewType == "ObservationAcceptPending" || viewType == "ObservationResponsePending" || viewType == "ObservationRevertedResponse" || viewType == "ObservationDueDateReminderToHOD" || viewType == "ObservationOverDueToHOD" || viewType == "ObservationReviewCompleteList"))
                    {
                        int ObservationID = 0;
                        ObservationID = GetObservationIDByObservationNotificationID(Notification_ID);
                        AuditID = GetAuditIDByObservationID(ObservationID).ToString();
                        MakeAuditNotificationstoVisited(Notification_ID);
                        objAuditReportView.ObservationNameInSearchBox = GetObservationNumberByObservationID(ObservationID);
                    }
                    else if (Notification_ID != 0)
                    {
                        AuditID = GetAuditIDByNotificationID(Notification_ID).ToString();
                        MakeAuditNotificationstoVisited(Notification_ID);
                    }
                    int EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);

                    var objAuditDetails = dbAizantIT_QMS_Audit.AizantIT_SP_AuditReportDetails(Convert.ToInt32(AuditID));

                    foreach (var item in objAuditDetails)
                    {
                        objAuditReportView.AuditID = item.AuditID;
                        objAuditReportView.ActionStatusID = Convert.ToInt32(item.ActionStatusID);
                        objAuditReportView.AuditNum = item.AuditNum;
                        objAuditReportView.Category = item.CategoryName;
                        objAuditReportView.CategoryType = item.TypeName;
                        objAuditReportView.TypeOfName = item.TypeOfName;
                        objAuditReportView.BusinessDivision = item.BusinessDivisionName;
                        objAuditReportView.Sites = item.Sites;
                        objAuditReportView.ClientName = item.ClientName;
                        objAuditReportView.RegionName = item.Region;
                        objAuditReportView.RegulatoryAgency = item.RegulatoryAgencyName;
                        objAuditReportView.NatureOfInspection = item.NatureOfInspection;
                        objAuditReportView.AuditedDates = item.AuditDates;
                        objAuditReportView.Observers = item.Observers;
                        objAuditReportView.AuditReportedDate = item.AuditReportedDate;
                        objAuditReportView.AuditDueDate = item.DueDate;
                        objAuditReportView.AuditStatus = item.Status;
                        objAuditReportView.AuditCurrentStatusID = Convert.ToInt32(item.AuditCurrentStatusID);
                        objAuditReportView.Auditors = item.Auditors;
                        objAuditReportView.CertifiedDate = item.CertifiedDate;
                        objAuditReportView.CertificateValidUntil = item.CVD;
                        objAuditReportView.AuthorName = item.AuthorName;
                        objAuditReportView.CountOFAuditResponseReport = Convert.ToInt32(item.CountOFAuditResponseReport);
                        objAuditReportView.ApproverName = item.ApproverName;
                        if (item.ApproverEmpID == EmpID)
                        {
                            ViewBag.EmpRole = "Approver";
                        }
                        else if (item.AuthorEmpID == EmpID)
                        {
                            ViewBag.EmpRole = "Author";
                        }
                        objAuditReportView.IsApproved = item.IsApproved;
                        objAuditReportView.ShowARR_Button = item.ShowARR_Button;
                    }

                    ViewBag.HasOnlyViewerRole = HasOnlyThisRole((int)QMS_Audit_Roles.AuditViewer);
                    //Has only viewer role and Audit is In-progress or Not in public:( Viewer can't see this audit)
                    if (HasOnlyThisRole((int)QMS_Audit_Roles.AuditViewer) && (objAuditReportView.AuditStatus == "1" || objAuditReportView.AuditCurrentStatusID != 10))
                    {
                        return PartialView("QMS_UnAuthorisedView"); //UnAuthorised='not having official permission or approval' , so this viewer is Unauthorized to see this record.
                    }
                    else
                    {
                        ViewBag.loadSeletPicker = "Yes";
                        return View(objAuditReportView);
                    }
                }
                else
                {
                    return PartialView("QMS_UnAuthorisedView");
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = LineNo + " :" + HelpClass.SQLEscapeString(ex.Message);
                return View();
            }
        }
        #endregion

        #region Audit Approval
        [HttpPost]
        public JsonResult AuditApproval(AuditApproval AuditApproval)
        {
            try
            {
                int EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                int resultStatus = 0;
                ObjectParameter objResultStatus = new ObjectParameter("resultStatus", typeof(Int32));
                dbAizantIT_QMS_Audit.AizantIT_SP_AuditApproval(
                Convert.ToInt32(AuditApproval.AuditID),
                AuditApproval.ApprovalType,
                AuditApproval.Comments == null ? "" : AuditApproval.Comments,
                EmpID, objResultStatus);
                resultStatus = (int)objResultStatus.Value;
                
                switch (resultStatus)
                {
                    case 1:
                        return Json(new { msg = (AuditApproval.ApprovalType == "Approved" ? " Approved" : " Reverted"), msgType = "success" }, JsonRequestBehavior.AllowGet);
                    case 2:
                        return Json(new { msg = "Not a valid Approver for this Audit.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                    case 3:
                        return Json(new { msg = "Audit is not in a status of Approval.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                    default:
                        return Json(new { msg = "Internal error Occurred on Audit Approval.", msgType = "error" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Audit File Upload
        [HttpPost]
        //Called from _AuditAttachmentFileUpload.cshtml
        public ActionResult AuditAttachments()
        {
            try
            {
                string Msg = "";
                string MsgType = "";
                string UploadedFilePath = null;
                using (DbContextTransaction transaction = dbAizantIT_QMS_Audit.Database.BeginTransaction())
                {
                    try
                    {
                        int filecount = Request.Files.Count;
                        string AuditID = Request.Params["AuditReportID"];
                        if (GetAuthorIDByAuditID(Convert.ToInt32(AuditID)) == Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]))
                        {
                            HttpFileCollectionBase files = Request.Files;
                            int AttachmentType = Convert.ToInt32(Request.Params["AttachmentType"]);
                            for (int i = 0; i < files.Count; i++)
                            {
                                AizantIT_AuditAttachment objAuditAttachment = new AizantIT_AuditAttachment();
                                HttpPostedFileBase file = files[i];
                                string fileName = GetActualFileName(file.FileName);
                                objAuditAttachment.FileName = fileName;
                                objAuditAttachment.FileExtention = GetOnlyFileExtension(file.FileName);                               
                                UploadedFilePath = SaveUploadedFileinPhysicalPathByAddingText(file, Convert.ToInt32(AuditID), "Audit",objAuditAttachment.FileExtention, AttachmentType);
                                objAuditAttachment.AttachmentDate = DateTime.Now;
                                objAuditAttachment.AttachmentType = AttachmentType;
                                objAuditAttachment.CanDelete = true;
                                objAuditAttachment.FilePath = UploadedFilePath;
                                objAuditAttachment.AuditID = Convert.ToInt32(AuditID);
                                dbAizantIT_QMS_Audit.AizantIT_AuditAttachment.Add(objAuditAttachment);
                                //throw new Exception("Exception occurred");
                                dbAizantIT_QMS_Audit.SaveChanges();
                                UploadedFilePath = null;
                            }
                            Msg = AuditAttachmentType(AttachmentType) + " Uploaded.";
                            MsgType = "success";
                        }
                        else
                        {
                            Msg = "You are not Authorized User.";
                            MsgType = "warning";
                        }
                        transaction.Commit();
                        return Json(new { msg = Msg, msgType = MsgType }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        if (UploadedFilePath != null)
                        {
                            RemovePhysicalPathFile(Server.MapPath("~\\" + UploadedFilePath));
                        }
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        private static string AuditAttachmentType(int AttachmentType)
        {
            string strAttachmentType;
            switch (AttachmentType)
            {
                case 1:
                    strAttachmentType = "Audit Report";
                    break;
                case 2:
                    strAttachmentType = "Audit Response";
                    break;
                case 3:
                    strAttachmentType = "Scribe Notes";
                    break;
                case 4:
                    strAttachmentType = "Certificate";
                    break;
                case 5:
                    strAttachmentType = "Audit Plan";
                    break;
                case 7:
                    strAttachmentType = "Audit Image";
                    break;
                default:
                    strAttachmentType = "File";
                    break;
            }

            return strAttachmentType;
        }
        [HttpPost]
        public ActionResult AuditResponseAttachments()
        {
            try
            {
                int ResponseID = Convert.ToInt32(Request.Params["ResponseID"]);
                var ObservationResponseMainDetails = (from objObservationResponseItem in dbAizantIT_QMS_Audit.AizantIT_ObservationResponse
                                                      join objAssignedObservationItem in dbAizantIT_QMS_Audit.AizantIT_AssignedObservation on objObservationResponseItem.AssignedObservationID equals objAssignedObservationItem.AssignedObservationID
                                                      join objAuditObservation in dbAizantIT_QMS_Audit.AizantIT_AuditObservation on objAssignedObservationItem.ObservationID equals objAuditObservation.ObservationID
                                                      join objAuditReport in dbAizantIT_QMS_Audit.AizantIT_AuditReport on objAuditObservation.AuditID equals objAuditReport.AuditID
                                                      where objObservationResponseItem.ResponseID == ResponseID
                                                      select new
                                                      {
                                                          objAssignedObservationItem.ObservationID,
                                                          objAuditObservation.AuditID,
                                                          objAuditReport.Status
                                                      }).SingleOrDefault();

                if ((ObservationResponseMainDetails.Status == 2 && GetAuthorIDByAuditID(ObservationResponseMainDetails.AuditID) == Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]))
                    ||
                    (ObservationResponseMainDetails.Status == 1 && GetResponsibleHODIDByObservationID(ObservationResponseMainDetails.ObservationID) == Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]))
                    )
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        AizantIT_ResponseAttachment objResponseAttachment = new AizantIT_ResponseAttachment();
                        HttpPostedFileBase file = files[i];
                        objResponseAttachment.FileName = GetActualFileName(file.FileName);
                        objResponseAttachment.FileExtention = GetOnlyFileExtension(file.FileName);
                        byte[] bytes;                       
                        objResponseAttachment.ResponseID = ResponseID;
                        objResponseAttachment.AttachmentDate = DateTime.Now;
                        objResponseAttachment.CanDelete = true;
                        objResponseAttachment.FilePath = SaveUploadedFileinPhysicalPathByAddingText(file, ResponseID, "Response",objResponseAttachment.FileExtention);
                        dbAizantIT_QMS_Audit.AizantIT_ResponseAttachment.Add(objResponseAttachment);
                        dbAizantIT_QMS_Audit.SaveChanges();
                        int AddedAttachmentID = objResponseAttachment.AttachmentID;
                        dbAizantIT_QMS_Audit.SaveChanges();
                    }

                    return Json(new { msg = "File Uploaded", msgType = "success" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { msg = "You are not Authorized User.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        private string GetOnlyFileExtension(string fileName)
        {
            return Path.GetExtension(fileName).Remove(0, 1);
        }
        private string GetActualFileName(string fileName)
        {
            return Path.GetFileName(fileName).Replace(Path.GetExtension(fileName), "");
        }
        #endregion

        #region Audit Attachment List
        [HttpGet]
        public JsonResult GetAuditAttachmentList()
        {
            try
            {
                int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                string AuditID = Request.Params["AuditID"];

                List<AuditAttachmentList> objAuditAttachmentList = new List<AuditAttachmentList>();

                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbQmsAudit = new AizantIT_DevEntities())
                {
                    var objAuditResultList = dbAizantIT_QMS_Audit.AizantIT_SP_AuditAttachmentList(
                               displayLength,
                               displayStart,
                               sortCol,
                               sSortDir,
                               sSearch != null ? sSearch.Trim() : "",
                               Convert.ToInt32(AuditID));

                    foreach (var item in objAuditResultList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objAuditAttachmentList.Add(new AuditAttachmentList(Convert.ToInt32(item.AttachmentID), item.FilePath, item.FileName,
                            item.Type, item.FileExtention, item.AttachmentDate, Convert.ToBoolean(item.CanDelete)));
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objAuditAttachmentList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult GetResponseAttachmentList()
        {
            try
            {
                int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                string ResponseID = Request.Params["ResponseID"];

                List<AuditAttachmentList> objAuditAttachmentList = new List<AuditAttachmentList>();
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbQmsAudit = new AizantIT_DevEntities())
                {
                    var objAuditResultList = dbAizantIT_QMS_Audit.AizantIT_SP_ResponseAttachmentList(
                               displayLength,
                               displayStart,
                               sortCol,
                               sSortDir,
                               sSearch != null ? sSearch.Trim() : "",
                               Convert.ToInt32(ResponseID));

                    foreach (var item in objAuditResultList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objAuditAttachmentList.Add(new AuditAttachmentList(Convert.ToInt32(item.AttachmentID), item.FilePath, item.FileName,
                            item.FileExtention, item.AttachmentDate, Convert.ToBoolean(item.CanDelete)));
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objAuditAttachmentList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region PhysicalPaths
        public string SaveUploadedFileinPhysicalPathByAddingText(HttpPostedFileBase file, int AuditIDorResponseID, string AuditOrObservationAttachment,string FileExtention,int AttachmentType=0)
        {
            string TempAttachmentDir = "";
            string FileName = AuditIDorResponseID + DateTime.Now.ToString("yyyMMddHHmmssfff")+"."+FileExtention;
            string InsertText = "";
            if (AuditOrObservationAttachment == "Audit")
            {
                TempAttachmentDir = Server.MapPath("~\\Areas\\QMS\\Models\\Audit\\AuditAttachments\\");
            }
            else
            {
                TempAttachmentDir = Server.MapPath("~\\Areas\\QMS\\Models\\Audit\\ResponseAttachments\\");
                InsertText = GetAuditNumberByAuditID(GetAuditIDByObservationID(GetObservationIDByResponseID(AuditIDorResponseID))) + "/" + GetObservationNumberByObservationID(GetObservationIDByResponseID(AuditIDorResponseID)) + "/" + GetActualFileName(file.FileName);
            }
            if (!Directory.Exists(TempAttachmentDir))
            {
                Directory.CreateDirectory(TempAttachmentDir);
            }
            string strFilePath = TempAttachmentDir + FileName;
            
            if (AttachmentType == 7)
            {
                file.SaveAs(strFilePath);
                return strFilePath.Replace(Server.MapPath("~\\"), "");
            }
            else
            {
                var br = new BinaryReader(file.InputStream);
                var reader = new PdfReader(br.ReadBytes(file.ContentLength));
                using (var fileStream = new FileStream(strFilePath, FileMode.Create, FileAccess.Write))
                {
                    Rectangle size = reader.GetPageSizeWithRotation(1);
                    var document = new Document(size);
                    var writer = PdfWriter.GetInstance(document, fileStream);

                    document.Open();
                    for (var i = 1; i <= reader.NumberOfPages; i++)
                    {
                        document.SetPageSize(reader.GetPageSize(i));
                        document.NewPage();
                        var rotation = reader.GetPageRotation(i);
                        var baseFont = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        var importedPage = writer.GetImportedPage(reader, i);
                        float currentPageHeight = importedPage.Height;
                        float currentPageWidth = importedPage.Width;
                        string tempInsertText = InsertText;
                        if (InsertText.Length > 170 && currentPageWidth < 596)
                        {
                            tempInsertText = InsertText.Remove(170) + "...";
                        }
                        else if (InsertText.Length > 210 && currentPageWidth > 596)
                        {
                            tempInsertText = InsertText.Remove(200) + "...";
                        }
                        var contentByte = writer.DirectContent;
                        contentByte.AddTemplate(importedPage, 1f, 0f, 0f, 1f, 0f, 0f);
                        contentByte.BeginText();
                        contentByte.SetFontAndSize(baseFont, 8);
                        contentByte.ShowTextAligned(Element.JPEG, tempInsertText, 10, currentPageHeight - 15, 0);
                        contentByte.EndText();
                    }
                    document.Close();
                    writer.Close();
                }
                return strFilePath.Replace(Server.MapPath("~\\"), "");
            }
        }
        public void RemovePhysicalPathFile(string FilePath)
        {
            if (System.IO.File.Exists(FilePath))
            {
                System.IO.File.Delete(FilePath);
            }
        }
        //Not in Use
        //public void MakeAuditAttachmentsToSingleFile(int AuditID)
        //{
        //    List<string> objAuditAttachmentFilePaths = (from item in dbAizantIT_QMS_Audit.AizantIT_AuditAttachment
        //                                                where item.AuditID == AuditID
        //                                                select item.FilePath).ToList();
        //    var objAlreadyMergedAuditAttachments = (from item in dbAizantIT_QMS_Audit.AizantIT_AuditAttachmentsAsSingle
        //                                            where item.AuditID == AuditID
        //                                            select item).ToList();

        //    if (objAlreadyMergedAuditAttachments.Count > 0) //There's already a Merged Attachments File
        //    {
        //        foreach (var item in objAlreadyMergedAuditAttachments)
        //        {
        //            //Delete old Merged File.
        //            RemovePhysicalPathFile(Server.MapPath("~\\" + item.Filepath));
        //        }
        //        //Update Newly Merged File
        //        AizantIT_AuditAttachmentsAsSingle objAuditAttachmentAsSingle =
        //                   dbAizantIT_QMS_Audit.AizantIT_AuditAttachmentsAsSingle.SingleOrDefault(p => p.AuditID == AuditID);
        //        objAuditAttachmentAsSingle.Filepath = MergeAllTheFilesAsSingleFile(objAuditAttachmentFilePaths, "Areas\\QMS\\Models\\Audit\\AuditAttachments\\MergedAttachments", AuditID + DateTime.Now.ToString("yyyMMddHHmmssfff") + ".pdf");
        //    }
        //    else //Create new Merge File
        //    {
        //        AizantIT_AuditAttachmentsAsSingle objAuditAttachmentAsSingle = new AizantIT_AuditAttachmentsAsSingle();
        //        objAuditAttachmentAsSingle.AuditID = AuditID;
        //        objAuditAttachmentAsSingle.Filepath = MergeAllTheFilesAsSingleFile(objAuditAttachmentFilePaths, "Areas\\QMS\\Models\\Audit\\AuditAttachments\\MergedAttachments", AuditID + DateTime.Now.ToString("yyyMMddHHmmssfff") + ".pdf");
        //        dbAizantIT_QMS_Audit.AizantIT_AuditAttachmentsAsSingle.Add(objAuditAttachmentAsSingle);
        //    }
        //    dbAizantIT_QMS_Audit.SaveChanges();
        //}
        //public void MergeOnlytheAuditAttachmentsWhichAreNotMergedBefore(int AuditID)
        //{
        //    // Get The List of Attachments Which are Not merged before i.e IsSubmitted=0 and CanDelete=1 in AuditAttachment Table
        //    List<string> ObjListOfAuditAttachmentsNotMerged = (from objAuditAttachment in dbAizantIT_QMS_Audit.AizantIT_AuditAttachment
        //                                                       where objAuditAttachment.IsSubmitted == false && objAuditAttachment.CanDelete == true
        //                                                       && objAuditAttachment.AuditID == AuditID
        //                                                       select objAuditAttachment.FilePath).ToList();
        //    if (ObjListOfAuditAttachmentsNotMerged.Count > 0)
        //    {
        //        //Get the already Merged file
        //        List<string> AlreadyMergedAuditAttachmentPath = (from objAuditAttachmentAsSingleTable in dbAizantIT_QMS_Audit.AizantIT_AuditAttachmentsAsSingle
        //                                                         where objAuditAttachmentAsSingleTable.AuditID == AuditID
        //                                                         select objAuditAttachmentAsSingleTable.Filepath).ToList();
        //        // call MergeAllTheFilesAsSingleFile() here and Update the New path in Db table.
        //        AizantIT_AuditAttachmentsAsSingle objAuditAttachmentAsSingle =
        //                  dbAizantIT_QMS_Audit.AizantIT_AuditAttachmentsAsSingle.SingleOrDefault(p => p.AuditID == AuditID);
        //        objAuditAttachmentAsSingle.Filepath = MergeAllTheFilesAsSingleFile((AlreadyMergedAuditAttachmentPath.Concat(ObjListOfAuditAttachmentsNotMerged)).ToList(), "Areas\\QMS\\Models\\Audit\\AuditAttachments\\MergedAttachments", AuditID + DateTime.Now.ToString("yyyMMddHHmmssfff") + ".pdf");
        //        dbAizantIT_QMS_Audit.SaveChanges();
        //        //Delete the Previously merged file in Physical path
        //        foreach (var AlreadyMergedAuditAttachmentPathFilePath in AlreadyMergedAuditAttachmentPath)
        //        {
        //            RemovePhysicalPathFile(Server.MapPath("~\\" + AlreadyMergedAuditAttachmentPathFilePath));
        //        }
        //    }
        //}
        public void MergeResponseAttachmentstoSingleFile(int ResponseID)
        {
            List<string> ObjListResponseAttachments = (from objResponseAttachmentTable in dbAizantIT_QMS_Audit.AizantIT_ResponseAttachment
                                                       where objResponseAttachmentTable.ResponseID == ResponseID
                                                       select objResponseAttachmentTable.FilePath).ToList();
            if (ObjListResponseAttachments.Count > 0)
            {
                AizantIT_ResponseAttachmentsAsSingle objAizantIT_ResponseAttachmentsAsSingle = new AizantIT_ResponseAttachmentsAsSingle();
                objAizantIT_ResponseAttachmentsAsSingle.ResponseID = ResponseID;
                objAizantIT_ResponseAttachmentsAsSingle.Filepath = MergeAllTheFilesAsSingleFile(ObjListResponseAttachments, "Areas\\QMS\\Models\\Audit\\ResponseAttachments\\MergedAttachments", ResponseID + DateTime.Now.ToString("yyyMMddHHmmssfff") + ".pdf");
                objAizantIT_ResponseAttachmentsAsSingle.ResponseVersion = Convert.ToInt32((from objObservationResponse in dbAizantIT_QMS_Audit.AizantIT_ObservationResponse
                                                                                           where objObservationResponse.ResponseID == ResponseID
                                                                                           select objObservationResponse.ResponseVersion).SingleOrDefault());
                dbAizantIT_QMS_Audit.AizantIT_ResponseAttachmentsAsSingle.Add(objAizantIT_ResponseAttachmentsAsSingle);
                dbAizantIT_QMS_Audit.SaveChanges();
            }
        }
        public string MergeAllTheFilesAsSingleFile(List<string> IndividualFilePaths, string MergedFileDirectory, string MergedFileName)
        {
            try
            {
                if (!Directory.Exists(Server.MapPath("~\\" + MergedFileDirectory)))
                {
                    Directory.CreateDirectory(Server.MapPath("~\\" + MergedFileDirectory));
                }
                string MergedFilePath = Server.MapPath("~\\" + MergedFileDirectory) + "\\" + MergedFileName;
                using (PdfDocumentProcessor pdfDocumentProcessor = new PdfDocumentProcessor())
                {
                    pdfDocumentProcessor.CreateEmptyDocument(MergedFilePath);
                    foreach (string IndividualFilePath in IndividualFilePaths)
                    {
                        if (System.IO.File.Exists(Server.MapPath("~\\" + IndividualFilePath)))
                        {
                            pdfDocumentProcessor.AppendDocument(Server.MapPath("~\\" + IndividualFilePath));
                        }
                    }
                }
                return MergedFilePath.Replace(Server.MapPath("~\\"), "");
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        [HttpGet]
        public JsonResult GetResponseList()
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            int ObservationID = 0;
            int.TryParse(Request.Params["ObservationID"], out ObservationID);

            List<ResponsesOnObservation_List> objResponseList = new List<ResponsesOnObservation_List>();
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    var objResponseRecords = dbAizantIT_QMS_Audit.AizantIT_SP_ResponseList(displayLength,
                        displayStart,
                        sortCol,
                        sSortDir,
                        sSearch != null ? sSearch.Trim() : "",
                        ObservationID);
                    foreach (var item in objResponseRecords)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objResponseList.Add(new ResponsesOnObservation_List(Convert.ToInt32(item.ResponseID),
                            Convert.ToInt32(item.AssignedObservationID), Convert.ToInt32(item.StatusID),
                            Convert.ToInt32(item.ActionStatus), item.ResponseDesc, item.ResponseStatus, item.TurnAroundTime, item.HasAttachments));
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objResponseList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteAuditAttachment(int AttachmentID)
        {
            try
            {
                AizantIT_AuditAttachment objAuditAttachment = (from item in dbAizantIT_QMS_Audit.AizantIT_AuditAttachment
                                                               where item.AttachmentID == AttachmentID
                                                               select item).Single();
                int AuditID = objAuditAttachment.AuditID;
                if (GetAuthorIDByAuditID(AuditID) == Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]))
                {
                    string fileName = objAuditAttachment.FileName;
                    bool IsSubmitted = objAuditAttachment.IsSubmitted;
                    string AttachmentType = AuditAttachmentType(objAuditAttachment.AttachmentType);
                    RemovePhysicalPathFile(Server.MapPath("~\\" + objAuditAttachment.FilePath));
                    dbAizantIT_QMS_Audit.AizantIT_AuditAttachment.Remove(objAuditAttachment);
                    dbAizantIT_QMS_Audit.SaveChanges();
                    string message = fileName;
                    //Check if the Attachment Status IsSubmitted=1
                    if (IsSubmitted)
                    {
                        InsertInToAuditHistory(AuditID, (int)QMS_Audit_Roles.AuditQA, 12, message, dbAizantIT_QMS_Audit); // 12 Represent Deleted Attachments.
                    }
                    return Json(new { msg = message + " Removed", msgType = "success" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { msg = "You are not Authorized User", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteResponseAttachment(int AttachmentID)
        {
            try
            {
                AizantIT_ResponseAttachment objResponseAttachment = (from item in dbAizantIT_QMS_Audit.AizantIT_ResponseAttachment
                                                                     where item.AttachmentID == AttachmentID
                                                                     select item).Single();
                int ResponseID = objResponseAttachment.ResponseID;
                //For Inserting Response Attachments  into History table
                var ObservationResponseMainDetails = (from objObservationResponseItem in dbAizantIT_QMS_Audit.AizantIT_ObservationResponse
                                                      join objAssignedObservationItem in dbAizantIT_QMS_Audit.AizantIT_AssignedObservation on objObservationResponseItem.AssignedObservationID equals objAssignedObservationItem.AssignedObservationID
                                                      join objAuditObservation in dbAizantIT_QMS_Audit.AizantIT_AuditObservation on objAssignedObservationItem.ObservationID equals objAuditObservation.ObservationID
                                                      join objAuditReport in dbAizantIT_QMS_Audit.AizantIT_AuditReport on objAuditObservation.AuditID equals objAuditReport.AuditID
                                                      where objObservationResponseItem.ResponseID == ResponseID
                                                      select new
                                                      {
                                                          objAssignedObservationItem.ObservationID,
                                                          objAuditObservation.AuditID,
                                                          objAuditReport.Status
                                                      }).SingleOrDefault();

                if ((ObservationResponseMainDetails.Status == 2 && GetAuthorIDByAuditID(ObservationResponseMainDetails.AuditID) == Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]))
                    ||
                    (ObservationResponseMainDetails.Status == 1 && GetResponsibleHODIDByObservationID(ObservationResponseMainDetails.ObservationID) == Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]))
                    )
                {
                    string fileName = objResponseAttachment.FileName;
                    bool IsSubmitted = objResponseAttachment.IsSubmitted;
                    RemovePhysicalPathFile(Server.MapPath("~\\" + objResponseAttachment.FilePath));
                    dbAizantIT_QMS_Audit.AizantIT_ResponseAttachment.Remove(objResponseAttachment);
                    dbAizantIT_QMS_Audit.SaveChanges();
                    if (IsSubmitted)
                    {
                        InsertInToObservationHistory(ObservationResponseMainDetails.ObservationID, ObservationResponseMainDetails.Status == 1 ? (int)QMS_Audit_Roles.AuditHOD : (int)QMS_Audit_Roles.AuditQA, 11, objResponseAttachment.FileName + " Removed.");//1 For Inprogress Audit HOD , for Completed Audit QA and 11 Represent Deleted Attachments for Response.
                    }
                    return Json(new { msg = objResponseAttachment.FileName + " Removed.", msgType = "success" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { msg = "You are not Authorized User.", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetAuditCategoryName(int Type)
        {
            try
            {
                var result = dbAizantIT_QMS_Audit.AizantIT_AuditCategory.Where(p => p.CategoryID == Type).Select(p => p.CategoryName).SingleOrDefault();
                return Json(new { msg = result, msgType = "success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public int AuditReportStatus(int AuditID) // Not Considering Attachment Status
        {
            return dbAizantIT_QMS_Audit.AizantIT_AuditReportHistory
              .Where(p1 => p1.AuditHistoryID == (int)dbAizantIT_QMS_Audit.AizantIT_AuditReportHistory
              .Where(p => p.AuditID == AuditID && (p.Status != 11 && p.Status != 12 && p.Status != 13)).Max(p => p.AuditHistoryID)).Select(p1 => p1.Status).SingleOrDefault();// 11: Attachments Uploaded,12: Attachments Deleted, 13: Employees Reassigned.
        }

        #region Generate Async Report
        public async Task<AuditReportMainDetails> loadReportData(int AuditID, AuditReportMainDetails objAuditReport)
        {
            var tsk1 = loadAuditReportData(objAuditReport, AuditID);
            var tsk2 = loadObservationsData(objAuditReport, AuditID);
            var tsk4 = loadResponsesData(objAuditReport, AuditID);
            var tsk5 = loadAuditorQuriesData(objAuditReport, AuditID);
            var tsk6 = loadAuditCAPAsData(objAuditReport, AuditID);

            await Task.WhenAll(tsk1, tsk2, tsk4, tsk5, tsk6);

            return objAuditReport;
        }
        private async Task loadAuditCAPAsData(AuditReportMainDetails objAuditReport, int auditID)
        {
            var AuditorCAPAsData = (from item in dbAizantIT_QMS_Audit.AizantIT_VW_AuditCAPAsData
                                    where item.AuditID == auditID
                                    select item);
            foreach (var item in AuditorCAPAsData)
            {
                objAuditReport.AuditCapa.AddAuditCapaRow(item.ObservationID, item.CAPA_Number, item.Attachments, item.Capa_ActionDesc,item.CapaStatus);
            }
        }
        private async Task loadAuditorQuriesData(AuditReportMainDetails objAuditReport, int auditID)
        {
            var AuditorQuriesData = dbAizantIT_QMS_Audit.AizantIT_VW_AuditorQueriesOnObservationData.Where(p => p.AuditID == auditID).Select(p => p);
            foreach (var item in AuditorQuriesData)
            {
                objAuditReport.ObservationQuery.AddObservationQueryRow(item.ObservationID, item.OnResponseID, item.QueryDesc,
                     item.QueryDate);
            }
        }
        private async Task loadResponsesData(AuditReportMainDetails objAuditReport, int auditID)
        {
            var ResponsesData = dbAizantIT_QMS_Audit.AizantIT_VW_AuditObservationResponsesData.Where(p => p.AuditID == auditID && p.ActionStatus == 5).Select(p => p);
            foreach (var item in ResponsesData)
            {
                objAuditReport.ObservationResponsesDetails.AddObservationResponsesDetailsRow(item.ObservationID,
                    item.ResponseID, item.ResponseSno.ToString(),
                     item.ResponseBy, item.ResponseDate, item.ResponseDesc, item.ReferenceFiles,
                     item.AuditorQuery, item.QueryDate, item.ResponseStatus);
            }
        }
        private async Task loadObservationsData(AuditReportMainDetails objAuditReport, int auditID)
        {
            var ObservationsData = dbAizantIT_QMS_Audit.AizantIT_VW_AuditObservationData.Where(p => p.AuditID == auditID).Select(p => p);
            foreach (var item in ObservationsData)
            {
                string ecd = item.ECD == null ? "" : item.ECD;
                string ObserStatus = ecd != "" ? item.ObservationStatus + " - " + ecd : item.ObservationStatus;
                objAuditReport.ObservationDetails.AddObservationDetailsRow(item.ObservationID,
                    item.ObservationNum, item.SiteName, item.ObservationDept,
                     ": " + item.DueDate.ToString(), item.Priority, ObserStatus,
                     item.ECD, item.ObservationDesc);
            }
        }
        private async Task loadAuditReportData(AuditReportMainDetails objAuditReport, int AuditID)
        {
            var AuditMainData = dbAizantIT_QMS_Audit.AizantIT_VW_AuditReportData.Where(p => p.AuditID == AuditID).Select(p => p);
            dtResponseReportedDate = DateTime.Now;
            foreach (var item in AuditMainData)
            {
                objAuditReport.AuditReport.AddAuditReportRow(item.AuditID, item.AuditedByTitle+":",
                      item.AuditNum, ": " + item.AuditCategory, item.TypeID.ToString(),
                     ": " + item.AuditCategoryType, ": " + item.TypeOfAudit, item.RegulatoryOrClientName,
                      item.AuditOnDates, item.ReportedDate,
                     ": " + item.AuditDueDate,item.BusinessDivision, item.AuditOnSites,
                      item.Auditors, item.Observers,
                     ": " + item.AuditCreatedBy, ": " + item.AuditCreatedDate, item.AuditRefFiles,
                      item.AuditResponseNum>9? item.AuditResponseNum.ToString():"0"+item.AuditResponseNum.ToString(),
                      dtResponseReportedDate.ToString("dd MMM yyyy HH:mm:ss"));
            }
        }
        public async Task<string> GenerateAuditReport(int AuditID)
        {
            try
            {
                ReportDocument objCrDoc = new ReportDocument();
                string CrPath = Server.MapPath("~/Areas/QMS/Models/Audit/Reports/Cr_AuditReport.rpt");
                objCrDoc.Load(CrPath);
                AuditReportMainDetails objAuditReport = new AuditReportMainDetails();
                objAuditReport = await loadReportData(AuditID, objAuditReport);
                objCrDoc.SetDataSource(objAuditReport);
                objCrDoc.Subreports[0].SetDataSource(objAuditReport);
                objCrDoc.Subreports[1].SetDataSource(objAuditReport.Tables[4]);
                string AuditReportFileName = "Audit_Report_" + GetAuditNumberByAuditID(AuditID) + DateTime.Now.ToString("yyyMMddHHmmssfff") + ".pdf";
                string AuditreportFileDirectory = Server.MapPath("~\\Areas\\QMS\\Models\\Audit\\AuditResponseFile\\Temp");
                if (!Directory.Exists(AuditreportFileDirectory))
                {
                    Directory.CreateDirectory(AuditreportFileDirectory);
                }
                string AuditReportFilePath = AuditreportFileDirectory + "\\" + AuditReportFileName;
                objCrDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, AuditReportFilePath);
                return AuditReportFilePath.Replace(Server.MapPath("~\\"), "");
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult DownloadAuditResponseReport(int AuditID, int AuditReportVersionID)
        {
            try
            {
                string Downloadfilename = "Audit_Report_" + GetAuditNumberByAuditID(AuditID) + ".pdf";
                string filePath = (from objAuditReport in dbAizantIT_QMS_Audit.AizantIT_AuditResponseReport
                                   where objAuditReport.AuditID == AuditID && objAuditReport.ResponseVersion == AuditReportVersionID
                                   select objAuditReport.AuditReportFilePath).SingleOrDefault();
                byte[] fileBytes;
                if (filePath != "" && filePath != null)
                {
                    fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~\\" + filePath));
                }
                else
                {
                    TempData["SysErrorMsg"] = HelpClass.SQLEscapeString("There's no Generated report");
                    return RedirectToAction("InitAuditReportList", new { listType = "AuditReportMain" });
                }
                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, Downloadfilename);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~/Areas/Regulatory/Models/Error.txt"));
                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, "InternalError.txt");
            }
        }
        public async Task<string> GenerateAuditResponseReportAndAddAttachments(int AuditID, string CoverLetterFilePath = null)
        {
            try
            {
                string ReturnMsg = "Success";
                int AuditCurrentResponseVersionID = GetAuditCurrentVersionByAuditId(AuditID);
                if (AuditCurrentResponseVersionID > 0)
                {
                    if (!dbAizantIT_QMS_Audit.AizantIT_AuditResponseReport.Where(u => u.AuditID == AuditID && u.ResponseVersion == AuditCurrentResponseVersionID).Any())
                    {
                        int CurrentLoginEmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        if (dbAizantIT_QMS_Audit.AizantIT_AuditReport.Where(u => u.AuditID == AuditID && u.ApproverEmpID == CurrentLoginEmpID).Any())
                        {
                            if (dbAizantIT_QMS_Audit.AizantIT_AuditReport.Where(u => u.AuditID == AuditID && u.Status==1).Any() && AuditReportStatus(AuditID)<=7)// Should be a inprogres audit
                            {
                                List<string> ObjAuditFilesToMerge = new List<string>();
                                // Generate AuditReport into a temp Folder and get that FilePath
                                string AuditReportFilePath = await GenerateAuditReport(AuditID);
                                if (CoverLetterFilePath != null)
                                {
                                    ObjAuditFilesToMerge.Insert(0, CoverLetterFilePath);
                                    ObjAuditFilesToMerge.Insert(1, AuditReportFilePath);
                                }
                                else
                                {
                                    ObjAuditFilesToMerge.Insert(0, AuditReportFilePath);
                                }
                                ObjAuditFilesToMerge = ObjAuditFilesToMerge.Concat(GetResponseAndCAPA_AttachmentsForThisVersion(AuditID)).ToList();

                                using (DbContextTransaction transaction = dbAizantIT_QMS_Audit.Database.BeginTransaction())
                                {
                                    string GeneratedResponseFilePath = "";
                                    try
                                    {
                                        //MergeAllTheFilesAsSingleFile,Update the DB with File path.
                                        AizantIT_AuditResponseReport objAizantIT_AuditResponseReport = new AizantIT_AuditResponseReport();
                                        objAizantIT_AuditResponseReport.AuditID = AuditID;
                                        objAizantIT_AuditResponseReport.ResponseVersion = GetAuditCurrentVersionByAuditId(AuditID);
                                        objAizantIT_AuditResponseReport.GeneratedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                        objAizantIT_AuditResponseReport.GeneratedDate = dtResponseReportedDate;
                                        objAizantIT_AuditResponseReport.AuditReportFilePath = MergeAllTheFilesAsSingleFile(ObjAuditFilesToMerge, "Areas\\QMS\\Models\\Audit\\AuditResponseFile", AuditID + DateTime.Now.ToString("yyyMMddHHmmssfff") + ".pdf");
                                        GeneratedResponseFilePath = objAizantIT_AuditResponseReport.AuditReportFilePath;
                                        dbAizantIT_QMS_Audit.AizantIT_AuditResponseReport.Add(objAizantIT_AuditResponseReport);
                                        int CurrentInsertedAuditResponseReportID = objAizantIT_AuditResponseReport.AuditResponseReportID;
            
                                        // For Updating the Current Version in AizantIT_AuditReport table
                                        AizantIT_AuditReport ObjAizantIT_AuditReport =
                                        dbAizantIT_QMS_Audit.AizantIT_AuditReport.SingleOrDefault(p => p.AuditID == AuditID);
                                        ObjAizantIT_AuditReport.CurrentResponseVersion = dbAizantIT_QMS_Audit.AizantIT_AuditReport.Where(u => u.AuditID == AuditID).Select(u => u.CurrentResponseVersion).SingleOrDefault() + 1;//Assigned Observation Status is closed
                                        dbAizantIT_QMS_Audit.SaveChanges();
                                        transaction.Commit();
                                    }
                                    catch (Exception)
                                    {
                                        transaction.Rollback();
                                        RemovePhysicalPathFile(Server.MapPath("~\\" + AuditReportFilePath));
                                        RemovePhysicalPathFile(Server.MapPath("~\\" + GeneratedResponseFilePath));
                                        RemovePhysicalPathFile(Server.MapPath("~\\" + CoverLetterFilePath));
                                        throw;
                                    }
                                    // Delete the AuditReport temp file.
                                    RemovePhysicalPathFile(Server.MapPath("~\\" + AuditReportFilePath));
                                    if (CoverLetterFilePath != null)
                                    {
                                        RemovePhysicalPathFile(Server.MapPath("~\\" + CoverLetterFilePath));
                                    }
                                }
                            }
                            else
                            {
                                ReturnMsg = "Not in a status of Report generation.";
                            }
                        }
                        else
                        {
                            ReturnMsg = "You are not a valid Head QA for this action.";
                        }
                    }
                    else
                    {
                        ReturnMsg = "Report is Already Generated for this Version.";
                    }
                }
                else
                {
                    ReturnMsg = "Not in a Status of Report Generation.";
                }
                return ReturnMsg;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<ActionResult> GenerateAuditResponseReport(int AuditID)
        {
            try
            {
                string MessageDescription = "";
                string MessageType = "";
                if (AuditID != 0)
                {
                    MessageDescription = await GenerateAuditResponseReportAndAddAttachments(AuditID);
                    if (MessageDescription == "Success")
                    {
                        MessageType = "success";
                        MessageDescription = "Audit Response Report Generated.";
                    }
                    else
                    {
                        MessageType = "error";
                    }
                }
                else
                {
                    MessageType = "error";
                    MessageDescription = "There is some internal issue please refresh the browser.";
                }
                return Json(new { msg = MessageDescription, msgType = MessageType }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public async Task<ActionResult> GenerateAuditResponseReportWithCoverLetterUploaded()
        {
            try
            {
                string Msg = "";
                string MsgType = "";
                string UploadedFilePath = null;
                try
                {
                    Msg = "";
                    MsgType = "success";
                    int filecount = Request.Files.Count;
                    int AuditID = Convert.ToInt32(Request.Params["AuditReportID"]);
                    HttpFileCollectionBase files = Request.Files;
                    HttpPostedFileBase file = Request.Files[0];
                    string TempAttachmentDir = Server.MapPath("~\\Areas\\QMS\\Models\\Audit\\AuditResponseFile\\Temp\\");
                    if (!Directory.Exists(TempAttachmentDir))
                    {
                        Directory.CreateDirectory(TempAttachmentDir);
                    }
                    string FileName = AuditID + "CL" + DateTime.Now.ToString("yyyMMddHHmmssfff") + ".pdf";
                    string strFilePath = TempAttachmentDir + FileName;
                    file.SaveAs(strFilePath);
                    UploadedFilePath = strFilePath;
                    Msg = await GenerateAuditResponseReportAndAddAttachments(AuditID, strFilePath.Replace(Server.MapPath("~\\"), ""));
                    if (Msg == "Success")
                    {
                        MsgType = "success";
                        Msg = "Audit Response Report Generated.";
                    }
                    else
                    {
                        MsgType = "error";
                    }
                    RemovePhysicalPathFile(strFilePath);
                    return Json(new { msg = Msg, msgType = MsgType }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    if (UploadedFilePath != null)
                    {
                        RemovePhysicalPathFile(UploadedFilePath);
                    }
                    throw;
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public List<string> GetResponseAndCAPA_AttachmentsForThisVersion(int AuditID)
        {
            List<string> ObjResponseAttachmentsWithCAPA_Attachments = new List<string>();
            var ObjResponsesForThisVersion = (from objObservationResponse in dbAizantIT_QMS_Audit.AizantIT_ObservationResponse
                                              join objAssignedObservation in dbAizantIT_QMS_Audit.AizantIT_AssignedObservation on objObservationResponse.AssignedObservationID equals objAssignedObservation.AssignedObservationID
                                              join objAuditObservation in dbAizantIT_QMS_Audit.AizantIT_AuditObservation on objAssignedObservation.ObservationID equals objAuditObservation.ObservationID
                                              join objAuditReport in dbAizantIT_QMS_Audit.AizantIT_AuditReport on objAuditObservation.AuditID equals objAuditReport.AuditID
                                              where objAuditReport.AuditID == AuditID && objObservationResponse.ResponseVersion == objAuditReport.CurrentResponseVersion
                                              orderby objAuditObservation.ObservationID, objObservationResponse.ResponseID
                                              select new { objObservationResponse.ResponseID }).ToList();
            if (ObjResponsesForThisVersion.Count > 0)
            {
                foreach (var itemObjResponsesForThisVersion in ObjResponsesForThisVersion)
                {
                    //For Response Attachments
                    ObjResponseAttachmentsWithCAPA_Attachments.Add(dbAizantIT_QMS_Audit.AizantIT_ResponseAttachmentsAsSingle.Where(u => u.ResponseID == itemObjResponsesForThisVersion.ResponseID).Select(u => u.Filepath).SingleOrDefault());
                    //For CAPA Attachmnets1
                    var ObjCAPA_AttachmentsToThisResponseID = (from t1 in dbAizantIT_QMS_Audit.AizantIT_CAPA_ActionPlan
                                                               join t2 in dbAizantIT_QMS_Audit.AizantIT_CAPAMaster on t1.CAPAID equals t2.CAPAID
                                                               join t3 in dbAizantIT_QMS_Audit.AizantIT_CapaCompltFileAttachments on t1.CAPAID equals t3.CapaID
                                                               where t1.BaseID == itemObjResponsesForThisVersion.ResponseID && t2.QualityEvent_TypeID == 9
                                                               orderby t1.CAPAID
                                                               select t3).ToList();
                    if (ObjCAPA_AttachmentsToThisResponseID.Count > 0)
                    {
                        foreach (var CAPAItem in ObjCAPA_AttachmentsToThisResponseID)
                        {
                            ObjResponseAttachmentsWithCAPA_Attachments.Add(CAPAItem.FilePath);
                        }
                    }
                }
            }

            return ObjResponseAttachmentsWithCAPA_Attachments;
        }
        #endregion

        public void MakeResponseAttachmentsIsSubmitted(int ResponseID, AizantIT_DevEntities dbAizantIT_QMS_Audit)
        {
            var obNewObservationFileNames = "";
            var ObservationExistingFileCount = 0;
            var ObservationID = (from objAssignedObservation in dbAizantIT_QMS_Audit.AizantIT_AssignedObservation
                                 join objObservationResponse in dbAizantIT_QMS_Audit.AizantIT_ObservationResponse
                                 on objAssignedObservation.AssignedObservationID equals objObservationResponse.AssignedObservationID
                                 where objObservationResponse.ResponseID == ResponseID
                                 select new { objAssignedObservation.ObservationID }).SingleOrDefault();
            if ((dbAizantIT_QMS_Audit.AizantIT_ResponseAttachment.Where(a => a.IsSubmitted == false && a.ResponseID == ResponseID).Count()) > 0)
            {
                var FileNames = dbAizantIT_QMS_Audit.AizantIT_ResponseAttachment.Where(a => a.ResponseID == ResponseID && a.IsSubmitted == false).Select(f => f.FileName).ToList();
                foreach (var item in FileNames)
                {
                    if (ObservationExistingFileCount == 0)
                    {
                        if ((obNewObservationFileNames.Length + item.Length) < 1994)
                        {
                            obNewObservationFileNames += item + ",";
                        }
                        else
                        {
                            obNewObservationFileNames += "...etc";
                            ObservationExistingFileCount = 1;
                        }
                    }
                }
                obNewObservationFileNames = obNewObservationFileNames.TrimEnd(',');
                var ResponseAttachmentDetails = dbAizantIT_QMS_Audit.AizantIT_ResponseAttachment.Where(P => P.ResponseID == ResponseID && P.IsSubmitted == false).ToList();
                foreach (var item in ResponseAttachmentDetails)
                {
                    var ResponseAttachmentData = dbAizantIT_QMS_Audit.AizantIT_ResponseAttachment.Where(P => P.AttachmentID == item.AttachmentID).SingleOrDefault();
                    ResponseAttachmentData.IsSubmitted = true;
                    dbAizantIT_QMS_Audit.SaveChanges();

                }
                AizantIT_AuditObservationHistory observationHistory = new AizantIT_AuditObservationHistory();
                observationHistory.ObservationID = Convert.ToInt32(ObservationID.ObservationID);
                observationHistory.ActionRole = Convert.ToInt32(QMS_Audit_Roles.AuditHOD);
                observationHistory.ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                observationHistory.ActionDate = DateTime.Now;
                observationHistory.Status = 10;//10 Attachment's Uploaded.
                observationHistory.Remarks = obNewObservationFileNames;
                dbAizantIT_QMS_Audit.AizantIT_AuditObservationHistory.Add(observationHistory);
                dbAizantIT_QMS_Audit.SaveChanges();
            }
        }
        public void AuditAttachmentHistoryOnCompletion(int AuditID, AizantIT_DevEntities dbAizantIT_QMS_Audit)
        {
            var obNewAuditFileNames = "";
            var AuditExistingFileCount = 0;
            if ((dbAizantIT_QMS_Audit.AizantIT_AuditAttachment.Where(a => a.IsSubmitted == false && a.AuditID == AuditID).Count()) > 0)
            {
                var FileNames = dbAizantIT_QMS_Audit.AizantIT_AuditAttachment.Where(a => a.IsSubmitted == false && a.AuditID == AuditID).Select(f => f.FileName).ToList();
                foreach (var item in FileNames)
                {
                    if (AuditExistingFileCount == 0)
                    {
                        if ((obNewAuditFileNames.Length + item.Length) < 1994)
                        {
                            obNewAuditFileNames += item + ",";
                        }
                        else
                        {
                            obNewAuditFileNames += "...etc";
                            AuditExistingFileCount = 1;
                        }
                    }
                }
                obNewAuditFileNames = obNewAuditFileNames.TrimEnd(',');
                var ResponseAttachmentDetails = dbAizantIT_QMS_Audit.AizantIT_AuditAttachment.Where(P => P.AuditID == AuditID && P.IsSubmitted == false).ToList();
                foreach (var item in ResponseAttachmentDetails)
                {
                    var AuditAttachmentData = dbAizantIT_QMS_Audit.AizantIT_AuditAttachment.Where(P => P.AttachmentID == item.AttachmentID).SingleOrDefault();
                    AuditAttachmentData.IsSubmitted = true;
                    dbAizantIT_QMS_Audit.SaveChanges();
                }
                AizantIT_AuditReportHistory objAuditReportHistory = new AizantIT_AuditReportHistory();
                objAuditReportHistory.AuditID = Convert.ToInt32(AuditID);
                objAuditReportHistory.ActionRole = Convert.ToInt32(QMS_Audit_Roles.AuditHOD);
                objAuditReportHistory.ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                objAuditReportHistory.ActionDate = DateTime.Now;
                objAuditReportHistory.Status = 10;//10 Attachment's Uploaded.
                objAuditReportHistory.Remarks = obNewAuditFileNames;
                dbAizantIT_QMS_Audit.AizantIT_AuditReportHistory.Add(objAuditReportHistory);
                dbAizantIT_QMS_Audit.SaveChanges();
            }
        }
        public ActionResult GetAuditResponseReportList(int AuditID)
        {
            try
            {
                int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];

                List<AuditResponseReportList> objAuditResponseReportList = new List<AuditResponseReportList>();
                int totalRecordsCount = 0;
                DAL_AuditReport objAuditDal = new DAL_AuditReport();
                var AuditResponseReportList = dbAizantIT_QMS_Audit.AizantIT_SP_AuditResponseReportList(displayLength, displayStart, sortCol, sSortDir, sSearch, AuditID);
                foreach (var item in AuditResponseReportList)
                {
                    if (totalRecordsCount == 0)
                    {
                        totalRecordsCount = Convert.ToInt32(item.TotalCount);
                    }
                    objAuditResponseReportList.Add(
                       new AuditResponseReportList(Convert.ToInt32(item.AuditID), Convert.ToInt32(item.ResponseVersionNumber), item.GeneratedBy, item.GeneratedDate, item.FilePath));
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objAuditResponseReportList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetListOfObservationsForThisReponse(int AuditID)
        {
            try
            {
                var objObservationListForThisResponse = (from T1 in dbAizantIT_QMS_Audit.AizantIT_AuditReport
                                                      join T2 in dbAizantIT_QMS_Audit.AizantIT_AuditObservation on T1.AuditID equals T2.AuditID
                                                      join T3 in dbAizantIT_QMS_Audit.AizantIT_AssignedObservation on T2.ObservationID equals T3.ObservationID
                                                      join T4 in dbAizantIT_QMS_Audit.AizantIT_ObservationResponse on T3.AssignedObservationID equals T4.AssignedObservationID
                                                      where T1.AuditID == AuditID && T4.ResponseVersion == T1.CurrentResponseVersion
                                                      select new
                                                      {
                                                          T2.ObservationID,
                                                          T2.ObservationNum
                                                      }).ToList();
                return Json(new { _objObservationListForThisAuditResponseResponse = objObservationListForThisResponse, msgType = "success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetObservationResponseDescription(int ObservationID)
        {
            try
            {
                bool IsallCapasClosedForThisObservation = IsAllCAPAS_ClosedOnThisObservation(ObservationID);
                var ObservationDetails = (from T1 in dbAizantIT_QMS_Audit.AizantIT_AuditReport
                                                      join T2 in dbAizantIT_QMS_Audit.AizantIT_AuditObservation on T1.AuditID equals T2.AuditID
                                                      join T3 in dbAizantIT_QMS_Audit.AizantIT_AssignedObservation on T2.ObservationID equals T3.ObservationID
                                                      join T4 in dbAizantIT_QMS_Audit.AizantIT_ObservationResponse on T3.AssignedObservationID equals T4.AssignedObservationID
                                                      join T5 in dbAizantIT_QMS_Audit.AizantIT_AuditorQuery on T4.AssignedObservationID equals T5.AssignedObservationID into rAuditorQuery
                                                      from pAuditorQuery in rAuditorQuery.DefaultIfEmpty()
                                                      where T2.ObservationID == ObservationID && T1.CurrentResponseVersion == T4.ResponseVersion
                                                      select new
                                                      {
                                                          T2.ObservationDescription,
                                                          ObservationStatus = T2.Status == 2 ? "Close" : "Open",
                                                          IsStatusDropDownEnable=T2.Status==2? false: IsallCapasClosedForThisObservation,                                                      T2.ObservationID,
                                                          T4.Description,
                                                          T4.ResponseID,
                                                          T4.ResponseVersion,
                                                          AuditorQuery = pAuditorQuery == null ? "" : pAuditorQuery.AuditorQuery
                                                      }).SingleOrDefault();

                return Json(new { _ObservationDetails = ObservationDetails, msgType = "success" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult UpdateObservationResponseDescription(int ObservationID, int ResponseID, string ResponseDescription, bool IsObservationStatusChangedtoClose=false, bool IsResponseDescChanged = false)
        {
           
                try
                {
                string MessageOut = "error";
                using (var dbContext = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                    {
                        try
                        {
                            var ObservationResponseData = dbContext.AizantIT_ObservationResponse.Where(a => a.ResponseID == ResponseID).SingleOrDefault();
                            if (IsResponseDescChanged == true)
                            {
                                ObservationResponseData.Description = ResponseDescription;
                            }
                            if (IsObservationStatusChangedtoClose == true)
                            {
                                //making Observation Response status to Close
                                ObservationResponseData.Status = 2;
                                //making Observation status to Close
                                var ObservationData = dbContext.AizantIT_AuditObservation.Where(b => b.ObservationID == ObservationID).SingleOrDefault();
                                ObservationData.Status = 2;
                            }

                            DateTime CurrentDateTime = DateTime.Now;
                            if (IsObservationStatusChangedtoClose == true)
                            {
                                int AssignedObsID = dbAizantIT_QMS_Audit.AizantIT_AssignedObservation.Where(p1 => p1.ObservationID == ObservationID).Select(p1 => p1.AssignedObservationID).Max();
                                AizantIT_AssignedObservation dtAizantIT_AssignedObservation =
                                        dbAizantIT_QMS_Audit.AizantIT_AssignedObservation.SingleOrDefault(p => p.AssignedObservationID == AssignedObsID && p.Status == 1);
                                dtAizantIT_AssignedObservation.Status = 2;//Assigned Observation Status is closed

                                InsertInToObservationHistory(ObservationID, Convert.ToInt32(QMS_Audit_Roles.AuditHeadQA), 14, null, CurrentDateTime);//Response Status Updated
                            }
                            if (IsResponseDescChanged == true)
                            {
                                InsertInToObservationHistory(ObservationID, Convert.ToInt32(QMS_Audit_Roles.AuditHeadQA), 13, null, CurrentDateTime);//Response Description Updated
                            }

                            dbContext.SaveChanges();
                            MessageOut = "success";
                            transaction.Commit();
                        }

                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw ex;
                        }
                    }
                    if (IsObservationStatusChangedtoClose == true)
                    {
                        using (AizantIT_DevEntities dbAizantIT_QMS_Audit1 = new AizantIT_DevEntities())
                        {
                            int AuditID = GetAuditIDByObservationID(ObservationID);
                            bool AnyObservationsWithoutClosedStatus = (from itemAuditObservation in dbAizantIT_QMS_Audit1.AizantIT_AuditObservation
                                                                       where (itemAuditObservation.AuditID == AuditID && (itemAuditObservation.Status != 2))// 1 for Open Status of Observation, 3 for reopened
                                                                       select new { itemAuditObservation.ObservationID }).Any();
                            if (!AnyObservationsWithoutClosedStatus)
                            {
                                //Insert into the AuditHistory Table
                                InsertInToAuditHistory(AuditID, (int)QMS_Audit_Roles.AuditQA, 6, "All Responses for all the Observations has Approved.", dbAizantIT_QMS_Audit1);// 6 Observation Response Completed.
                            }
                        }
                    }
                    return Json(new { msgType = MessageOut }, JsonRequestBehavior.AllowGet);
                }
            }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
                }
            
        }
        #region Audit ML Code
        public ActionResult GetObservationDetailsList(int ObservationId)
        {
            try
            {
                List<ML_ObservationData> objAuditObserDetailsList = new List<ML_ObservationData>();
                var AuditObservationMLData = dbAizantIT_QMS_Audit.AizantIT_SP_Display_CFR_Similar_FDA_Obv(ObservationId);
                foreach (var item in AuditObservationMLData)
                {
                    objAuditObserDetailsList.Add(
                       new ML_ObservationData(item.CFR_NUM, Convert.ToInt32(item.CFR_Count), item.CFR_Name, item.CFR_Desp));
                }
                return Json(new
                {
                    aaData = objAuditObserDetailsList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult GetObservationCFR_Num(string cfrNum, int ObservationId)
        {
            try
            {
                List<ML_ObservationCFR_Num> objAuditObserCFR_Num = new List<ML_ObservationCFR_Num>();
                var AuditObservationCFR_Num = dbAizantIT_QMS_Audit.AizantIT_SP_Display_Descp_Similar_FDA_Obv(ObservationId, cfrNum);
                foreach (var item in AuditObservationCFR_Num)
                {
                    objAuditObserCFR_Num.Add(
                       new ML_ObservationCFR_Num(item.Short_Description, item.Long_Description));
                }
                return Json(new
                {
                    aaData = objAuditObserCFR_Num
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}