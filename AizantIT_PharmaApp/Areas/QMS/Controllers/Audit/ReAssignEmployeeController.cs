﻿using Aizant_API_Entities;
using AizantIT_PharmaApp.Areas.QMS.Models.Audit.DataLayer;
using AizantIT_PharmaApp.Areas.QMS.Models.Audit.ViewModel;
using AizantIT_PharmaApp.Controllers.Aizant_BL;
using AizantIT_PharmaApp.Models.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.Areas.QMS.Controllers.Audit
{
    public class ReAssignEmployeeController : Controller
    {
        // GET: QMS/ReAssignEmployees
        public  Aizant_Bl objAizantBl = new Aizant_Bl();
       
        #region Re Assign Employee
        public ActionResult ReAssignEmployeeMain(int AuditID = 0, int AuthorID=0, int ApproverID=0,string AuditNumber="")
        {
            try
            {
                using (var dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    if (AuditID != 0) // If Request not for Audit List
                    {
                        AuditCreate obAuditCreate = new AuditCreate();
                        
                        obAuditCreate.AuditID = AuditID;
                        obAuditCreate.AuditReportNumber = AuditNumber;
                        // Check status 5  Approved it's true return True  are else return false                        
                        bool AuditStatus = dbAizantIT_QMS_Audit.AizantIT_AuditReportHistory.Any(a => a.AuditID == AuditID && a.Status == 5);
                        obAuditCreate.AuditCurrentStatusID = AuditStatus?5:0;

                        int approverId = ApproverID;
                        int[] aryEmpID = null;
                        if (AuditStatus)
                        {
                            aryEmpID = new int[] { approverId };
                        }
                        obAuditCreate.AuditQaList = objAizantBl.GetListofEmpByRole(aryEmpID, (int)QMS_Audit_Roles.AuditQA, "A", AuthorID.ToString(), "--Select Author--");
                        obAuditCreate.ApproversList = objAizantBl.GetListofEmpByRole(null, (int)QMS_Audit_Roles.AuditHeadQA, "A", ApproverID.ToString(), "--Select Approver--");
                        return PartialView("~/Areas/QMS/Views/ReAssignEmployee/_EditAuditReAssignEmployee.cshtml", obAuditCreate);
                    }
                    else // If Request for Audit List
                    {
                        return PartialView("~/Areas/QMS/Views/ReAssignEmployee/_ReAssignEmployeeList.cshtml");
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + ex.Message;
                message.Replace("'", "\'");
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        //Get Reassign Audit List Bind on Datatable
        public JsonResult GetReAssignAuditList()
        {
            try
            {
                int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                DAL_AuditReport objAuditDal = new DAL_AuditReport();
                int totalRecordsCount = 0;
                DataTable dtAuditList = objAuditDal.GetAuditList(displayLength, displayStart, sortCol, sSortDir, sSearch);
                List<AuditReAssignReportList> objAuditList = new List<AuditReAssignReportList>();
                if (dtAuditList.Rows.Count > 0)
                {
                    totalRecordsCount = Convert.ToInt32(dtAuditList.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dtAuditList.Rows.Count; i++)
                {
                    objAuditList.Add(
                       new AuditReAssignReportList(Convert.ToInt32(dtAuditList.Rows[i]["AuditID"]),
                             dtAuditList.Rows[i]["AuditNum"].ToString(),
                            dtAuditList.Rows[i]["Category"].ToString(),
                            dtAuditList.Rows[i]["CategoryType"].ToString(),
                            dtAuditList.Rows[i]["AuditStatusType"].ToString(),
                            dtAuditList.Rows[i]["StatusName"].ToString(),
                            dtAuditList.Rows[i]["AuthorName"].ToString(),
                            dtAuditList.Rows[i]["ApproverName"].ToString(),
                            Convert.ToInt32(dtAuditList.Rows[i]["AuthorID"]),
                            Convert.ToInt32(dtAuditList.Rows[i]["ApproverID"])
                            ));
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objAuditList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + ex.Message;
                message.Replace("'", "\'");
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);

            }
        }
        //Modification Audit Author or Approver 
        public ActionResult ModifiedReAssignAuditEmployees(int AuthorID, int ApproverID, int AuditID, string Comments)
        {
            try
            {
                using (var dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    int ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    DAL_AuditReport objAuditDal = new DAL_AuditReport();
                    objAuditDal.ModifiedReAssignAuditUsers(AuthorID, 
                        ApproverID,
                        AuditID, 
                        Comments,
                        ActionBy,
                       out int ActionResult
                       );
                    //To Modify the Audit QA when Observation Response Approval Pending
                    if (ActionResult==1 || ActionResult == 2|| ActionResult == 5)
                    {
                        var listOfObservationAtAuthor = (from objAuditObservation in dbAizantIT_QMS_Audit.AizantIT_AuditObservation
                                                         join
                                        objObservationNotifyLink in dbAizantIT_QMS_Audit.AizantIT_Observation_NotifyLink on objAuditObservation.ObservationID equals objObservationNotifyLink.ObservationID
                                        where objAuditObservation.AuditID== AuditID
                                                         select new { objAuditObservation.ObservationID, objObservationNotifyLink.NotificationID }).ToList();

                        foreach (var item in listOfObservationAtAuthor)
                        {
                            if ((IsObservationAtQA(item.ObservationID,dbAizantIT_QMS_Audit)))
                            {
                                NotificationActionByLinq notificationActionByLinq = new NotificationActionByLinq();
                                int[] Author = { AuthorID };
                                notificationActionByLinq.UpdateNotificationEmps(Convert.ToInt32(item.NotificationID), Author, 1);
                            }
                        }
                    }
                    return Json(ActionResult,JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + ex.Message;
                message.Replace("'", "\'");
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public bool IsObservationAtQA(int ObservationID, AizantIT_DevEntities dbAizantIT_QMS_Audit)
        {
            int ObservationHistoryID = dbAizantIT_QMS_Audit.AizantIT_AuditObservationHistory.Where(u => u.ObservationID == ObservationID && u.Status != 10 && u.Status != 11 && u.Status != 12).Max(u => u.ObservationHistoryID); //10: for Attachments Uploaded , 11: for Attachment Deleted,12:Reassign Employees.
            int ObservationCurrentStatus = dbAizantIT_QMS_Audit.AizantIT_AuditObservationHistory.Where(u => u.ObservationHistoryID == ObservationHistoryID).Select(u => u.Status).SingleOrDefault();
            if(ObservationCurrentStatus==5 || ObservationCurrentStatus==6)
            {
                return true;
            }
            else
            {
                return false;
            }
           
        }
        //Get  Audit Wise Onservation List Bind on Datatable
        public JsonResult GetAuditWiseOpenObservationList(int AuditID, int ExceptEmpCount = 0)
        {
            try
            {
                int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];

                DAL_AuditReport objAuditDal = new DAL_AuditReport();
                int totalRecordsCount = 0;
                DataTable dtAuditList = objAuditDal.GetAuditWiseOpenObservationList(AuditID, displayLength, displayStart, sortCol, sSortDir, sSearch);

                List<AuditObservationReportList> objAuditList = new List<AuditObservationReportList>();
                if (dtAuditList.Rows.Count > 0)
                {
                    totalRecordsCount = Convert.ToInt32(dtAuditList.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dtAuditList.Rows.Count; i++)
                {
                    objAuditList.Add(
                       new AuditObservationReportList(
                           Convert.ToInt32(dtAuditList.Rows[i]["AuditID"]),
                            Convert.ToInt32(dtAuditList.Rows[i]["ObservationID"]),
                            dtAuditList.Rows[i]["ObservationNum"].ToString(),
                            Convert.ToInt32(dtAuditList.Rows[i]["DeptID"]),
                            dtAuditList.Rows[i]["DepartmentName"].ToString(),
                            Convert.ToInt32(dtAuditList.Rows[i]["EmpID"]),
                            dtAuditList.Rows[i]["ResponsibleName"].ToString(),
                            Convert.ToInt32(dtAuditList.Rows[i]["AssignedObservationID"])
                            ));
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objAuditList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + ex.Message;
                message.Replace("'", "\'");
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        //modification on Observation Response Person 
        public ActionResult ModifiedReAssignObservationResponsePerson(int ResponsiblePersonID, string Comments, int AssignedObservationID, int ObservationID)
        {
            try
            {
                using (var dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    int ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    DAL_AuditReport objAuditDal = new DAL_AuditReport();
                    objAuditDal.ModifiedReAssignObservationResponsePerson(ResponsiblePersonID, Comments, AssignedObservationID, ObservationID, ActionBy, out int ObservationStatus);
                    if (ObservationStatus==2)
                    {
                        return Json(new { msg = " Not in  the status to  Modify Responsible person  ", msgType = "info" }, JsonRequestBehavior.AllowGet);
                    }
                    else if(ObservationStatus==1)
                    {
                        return Json(new { msg = "Modified  Response Person Successfully", msgType = "success" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { msg = "Modified  Response Person failed ", msgType = "error" });
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + ex.Message;
                message.Replace("'", "\'");
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        //if Author alredy Existing on  Observation(s) it Raise the warning 
        public JsonResult AuthorNameChecking(int EmployeeID, int AuditID,string AuditStatusType)
        {
            try
            {
                using (var dbAizantIT_QMS_Audit = new AizantIT_DevEntities())
                {
                    if (AuditStatusType=="I")//Condition For AuditStatus C:Complete (or) I: In=Prograss
                    {
                      var  employeeIds = (from T1 in dbAizantIT_QMS_Audit.AizantIT_AuditObservation
                                               join T2 in dbAizantIT_QMS_Audit.AizantIT_AssignedObservation.Where(p=>p.Status==1) 
                                                  on T1.ObservationID equals T2.ObservationID 
                                               join T3 in dbAizantIT_QMS_Audit.AizantIT_ResponseEmpIsNotEntry on T2.AssignedObservationID equals T3.AssignedObservationID
                                               where T1.AuditID == AuditID && T1.Status != 2
                                               select new { T3.ResponsibleEmpID }).ToList();

                        var status = employeeIds.Any(a => a.ResponsibleEmpID == EmployeeID);
                        if(status)
                        {
                            return Json(new { msg = "You're not Allowed to Select this user as Author Due to This User  is Hod Responsible Person to Some of The Observation(s)", msgType = "warning" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(true, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + ex.Message;
                message.Replace("'", "\'");
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        //Responsible Person   bind On Observation List in-Line DropdownList 
        public JsonResult GetResponsePersonList(int ResponsiblePersonID, int DeptID, int AuthorId)
        {
            int[] AuditAuthorId = { AuthorId };
            var ResponsiblePersonList = objAizantBl.GetListofEmpByDeptIDRoleIDAndStatus(AuditAuthorId, DeptID, (int)QMS_Audit_Roles.AuditHOD, "A", ResponsiblePersonID.ToString(), "--Select HOD--");
            return Json(ResponsiblePersonList, JsonRequestBehavior.AllowGet);
        } 
        #endregion
    }
}