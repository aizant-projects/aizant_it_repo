﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AizantIT_PharmaApp.Common.CustomFilters;
using Aizant_Enums;
using QMS_BusinessLayer;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;
using static QMS_BO.QMS_Incident_BO.IncidentBO;
using System.IO;
using System.Collections;
using AizantIT_PharmaApp.Common;
using AizantIT_PharmaApp.Areas.QMS.XtraReports.Incident;
using DevExpress.XtraReports.UI;
using UMS_BO;
using System.Diagnostics;
using QMS_BO.QMSComonBO;
using QMS_BO.QMS_Incident_BO;
using System.Drawing;
using Aizant_API_Entities;
using AizantIT_PharmaApp.Areas.QMS.Models.Common;
using System.Threading.Tasks;
using QMS_BO.QMS_CAPABO;
using QMS_BO.QMS_CAPA_BO;
using AizantIT_PharmaApp.Areas.QMS.Controllers.Common;
using TMS_BusinessLayer;
using API_BusinessLayer;

namespace AizantIT_PharmaApp.Areas.QMS.Controllers.QMS
{
    [RouteArea]
    [CustAuthentication]
    [CustAuthorization(
        (int)AizantEnums.Modules.QMS,
        (int)QMS_Roles.Initiator,
        (int)QMS_Roles.HOD,
        (int)QMS_Roles.QA,
        (int)QMS_Roles.HeadQA,
        (int)QMS_Roles.AdminRole,
        (int)QMS_Roles.User,
        (int)QMS_Roles.Investigator,
        (int)QMS_Audit_Roles.AuditHOD,
        (int)QMS_Audit_Roles.AuditQA,
        (int)QMS_Audit_Roles.AuditHeadQA)]
    public class IncidentController : Controller
    {
        QMS_BAL Qms_bal = new QMS_BAL();
        UMS_BAL objUMS_BAL = new UMS_BAL();
        CCNBal CCNQms_bal = new CCNBal();
        // GET: QMS_Incident

        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ActionResult IncidentList(int ShowType, int Notification_ID = 0)
        {
            ViewBag.Exception = null;
            TempData["ShowType"] = 0;
            try
            {
                switch (ShowType)
                {
                    case 1:
                        ShowType = (int)QMS_IncidentListTypes.CreatedList;
                        break;
                    case 2:
                        ShowType = (int)QMS_IncidentListTypes.InitiatorRevertedList;
                        break;
                    case 3:
                        ShowType = (int)QMS_IncidentListTypes.HOD_ReviewList;
                        break;
                    case 4:
                        ShowType = (int)QMS_IncidentListTypes.HOD_RevertedList;
                        break;
                    case 5:
                        ShowType = (int)QMS_IncidentListTypes.QaReviewList;
                        break;
                    case 6:
                        ShowType = (int)QMS_IncidentListTypes.QaRevertedList;
                        break;
                    case 7:
                        ShowType = (int)QMS_IncidentListTypes.HeadQaReviewList;
                        break;
                    case 8:
                        ShowType = (int)QMS_IncidentListTypes.InvestigatorReviewList;
                        break;
                    case 9:
                        ShowType = (int)QMS_IncidentListTypes.InvestigationReviewByHodList;
                        break;
                    case 10:
                        ShowType = (int)QMS_IncidentListTypes.HodActionPlanList;
                        break;
                    case 11:
                        ShowType = (int)QMS_IncidentListTypes.InvestigatorRevertedList;
                        break;
                    case 12:
                        ShowType = (int)QMS_IncidentListTypes.MainList;
                        break;
                    case 13:
                        ShowType = (int)QMS_IncidentListTypes.PendingList;
                        break;
                    case 14:
                        ShowType = (int)QMS_IncidentListTypes.RejectedList;
                        break;
                    case 15:
                        ShowType = (int)QMS_IncidentListTypes.ApprovedList;
                        break;
                    case 16:
                        ShowType = (int)QMS_IncidentListTypes.VerifiedList;
                        break;
                    case 17:
                        ShowType = (int)QMS_IncidentListTypes.ReportIncidentList;
                        break;
                    case 20:
                        ShowType = (int)QMS_IncidentListTypes.IncidentManage;
                        break;
                }
                TempData["ShowType"] = ShowType;
                TempData.Keep("ShowType");
                IncidentBO IncidentBO = new IncidentBO();
               // BindDropdownForIncidentReports();
                EmployeID(IncidentBO);
                ViewBag.IncidentMasterID = 0;
                if (ShowType == 9 || ShowType == 2) { 
                int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                NotificationUpdate(Notification_ID, EmpIDs);
                }

                //TempData["EmpID"] = IncidentBO.EmpID;
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M1:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return View();
            }
            return View();
        }
        public void EmployeID(IncidentBO incidentBO)
        {
            if (Session["UserDetails"] != null)
            {
                incidentBO.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            }
        }
        public void NotificationUpdate(int Notification_ID, int[] EmpIDs)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                if (Notification_ID != 0)
                {
                    objUMS_BAL = new UMS_BAL();
                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs); //2 represents NotifyEmp had visited the page through notification link.

                    var GetIncidentMasterID = (from C in dbEF.AizantIT_IncidentNotificationlink
                                               where C.NotificationID == Notification_ID
                                               select new { C.IncidentMasterID }).ToList();
                    foreach (var item in GetIncidentMasterID)
                    {
                        ViewBag.IncidentMasterID = item.IncidentMasterID;
                        }
                    }
            }
        }

        public void GetNotificationUpdate(int Notification_ID, int[] EmpIDs)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                //ViewBag.IncidentMasterID = 0;
                if (Notification_ID != 0)
                {
                    objUMS_BAL = new UMS_BAL();
                    objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs); //2 represents NotifyEmp had visited the page through notification link.
                }
            }
        }

        #region Bind Methods
        //Bind Department and Document Type
        public void BindDeptAndDocumentType(IncidentBO incidentBO)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {

                EmployeID(incidentBO);
                DataSet ds = CCNQms_bal.GetddlBindingCCNCreate(incidentBO.EmpID);
                //Department binding
                List<SelectListItem> Dep = new List<SelectListItem>();
                Dep.Add(new SelectListItem
                {
                    Text = "Select",
                    Value = "0",
                });
                foreach (DataRow rdr in ds.Tables[0].Rows)
                {
                    Dep.Add(new SelectListItem
                    {
                        Text = (rdr["DepartmentName"]).ToString(),
                        Value = (rdr["DeptID"]).ToString(),
                        Selected = (Convert.ToInt32(rdr["DeptID"]) == incidentBO.DeptID ? true : false)
                    });
                }
                incidentBO.ddlDeptList = Dep;

                var GetDeptNameforView = (from d in dbEF.AizantIT_DepartmentMaster
                                          where d.DeptID == incidentBO.DeptID
                                          select new { d.DepartmentName }).ToList();
                foreach (var item1 in GetDeptNameforView)
                {
                    incidentBO.IncidentViewDeptName = item1.DepartmentName;
                }
                //Document Type Binding to Drop down
                List<SelectListItem> DocumentTypeList = new List<SelectListItem>();
                DocumentTypeList.Add(new SelectListItem
                {
                    Text = "Select",
                    Value = "-1",
                });
                foreach (DataRow rdr in ds.Tables[1].Rows)
                {
                    DocumentTypeList.Add(new SelectListItem
                    {
                        Text = (rdr["DocumentType"]).ToString(),
                        Value = (rdr["DocumentTypeID"]).ToString(),
                        Selected = (Convert.ToInt32(rdr["DocumentTypeID"]) == incidentBO.DocumentType ? true : false)
                    });
                }
                DocumentTypeList.Add(new SelectListItem
                {
                    Text = "Others",
                    Value = "0",
                });
                incidentBO.ddlDocumentTypeList = DocumentTypeList;
                //ViewBag.DocumentType = new SelectList(DocumentTypeList.ToList(), "Value", "Text");
                ViewBag.HasAffDocumentType = new SelectList(DocumentTypeList.ToList(), "Value", "Text");
            }
        }

        public void BindAllDepts()
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {

                List<SelectListItem> Dep = new List<SelectListItem>();
                var Department1 = (from DM in dbEF.AizantIT_DepartmentMaster
                                   orderby DM.DepartmentName
                                   select new { DM.DeptID, DM.DepartmentName }).ToList();
                Dep.Add(new SelectListItem
                {
                    Text = "Select",
                    Value = "0",
                });
                foreach (var item in Department1)
                {
                    Dep.Add(new SelectListItem
                    {
                        Text = item.DepartmentName,
                        Value = item.DeptID.ToString(),
                    });
                }
                ViewBag.DeptID1 = new SelectList(Dep.ToList(), "Value", "Text");
                ViewBag.DeptID2 = new SelectList(Dep.ToList(), "Value", "Text");
                ViewBag.HasAffeDeptID = new SelectList(Dep.ToList(), "Value", "Text");
            }
        }

        //start commented on 27-12-2019
        //For binding Quality Event Names in Dropdown
        public void BindQualityEventNames()
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                List<int> objQultyEventID = new List<int> { 1, 5};

                List<SelectListItem> EventName = new List<SelectListItem>();

                var QualityEventNames = (from Ev in dbEF.AizantIT_QualityEvent
                                     where objQultyEventID.Contains((int)Ev.EventGroupID)
                                     select new { Ev.QualityEvent_TypeID, Ev.QualityEvent_TypeName }).ToList();
            
                EventName.Add(new SelectListItem
                {
                    Text = "Select",
                    Value = "0",
                });

                foreach (var item in QualityEventNames)
                {
                    EventName.Add(new SelectListItem
                    {
                        Text = item.QualityEvent_TypeName,
                        Value = item.QualityEvent_TypeID.ToString(),
                    });
                }

                ViewBag.QualityEvents = new SelectList(EventName.ToList(), "Value", "Text");
            }
        }
        //end commented on 27-12-2019

        //Bind Incident file number 
        [HttpGet]
        public JsonResult BindIncidentNo(int DeptID)
        {
            try
            {
                DataTable dt1 = new DataTable();
                dt1 = Qms_bal.BalGetAutoGenaratedNumber(DeptID, 6);//6-QualityEventNo Incident
                var IncidentNumber = dt1.Rows[0][0].ToString();
                return Json(new { hasError = false, data = IncidentNumber }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M4:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Comman Method for Sending Incident Notification
        //Comman Method for Sending Notification etc..
        public void SendNotifications(int IncidentMasterID, int EmpID, int Action)
        {
            DataTable dt = Qms_bal.SendIncidentNotificationBal(IncidentMasterID, EmpID, Action);
        }
        #endregion

        // To Delete Investigation Form Details when it is not final Submit (delete when isTemp is 1)
        public void DeleteInvestigationFormisTemp(int IncidentMasterID)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                var GetFilePath = (from T1 in dbEF.AizantIT_Incident_IncidentAttachments
                                   where T1.IncidentMasterID == IncidentMasterID && T1.isTemp == false
                                   select T1).ToList();
                if (GetFilePath.Count > 0)
                {
                    foreach (var item in GetFilePath)
                    {
                        //Removed file path in physical folder
                        QMSAttachments objAttachmentsCommon = new QMSAttachments();
                        objAttachmentsCommon.RemovePhysicalPathFile(Server.MapPath("~\\" + item.FilePath));
                    }
                }
                Qms_bal.DeleteInvestigationFormisTempBal(IncidentMasterID);
            }
        }

        // All Incident Models Binding
        public IncidentBO BindAllIncidentDetails(int IncidentMasterID)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {

                //start capa view
                IncidentBO incdntBO = new IncidentBO();
                EmployeID(incdntBO);
                //Bind Department Drop down
                DataSet ds = CCNQms_bal.GetddlBindingCCNCreate(incdntBO.EmpID);
                List<SelectListItem> CAPAIRDep = new List<SelectListItem>();
                CAPAIRDep.Add(new SelectListItem
                {
                    Text = "Select",
                    Value = "0",
                });
                foreach (DataRow rdr in ds.Tables[0].Rows)
                {
                    CAPAIRDep.Add(new SelectListItem
                    {
                        Text = (rdr["DepartmentName"]).ToString(),
                        Value = (rdr["DeptID"]).ToString(),
                        Selected = (Convert.ToInt32(rdr["DeptID"]) == incdntBO.DeptID ? true : false)
                    });
                }
                ViewBag.IRDepartment = new SelectList(CAPAIRDep.ToList(), "Value", "Text");

                //Bind Quality Event excepts CCN ,NTF,Errata ,CAPA,Incident
                List<int> objQulity = new List<int> { 1, 2, 3, 4, 5 };
                ViewBag.IRQualityevent = new SelectList((from QE in dbEF.AizantIT_QualityEvent
                                                         where !objQulity.Contains(QE.QualityEvent_TypeID)
                                                         select new { Name = (QE.QualityEvent_TypeName), ID = QE.QualityEvent_TypeID }), "ID", "Name").ToList();
                //Bind Action Plan
                List<SelectListItem> IRActionplanlst = new List<SelectListItem>();
                var IRActionPlan = (from AD in dbEF.AizantIT_ActionPlanDescription
                                    where AD.CurrentStatus == true
                                    select new { AD.Capa_ActionID, AD.Capa_ActionName }).ToList();
                IRActionplanlst.Add(new SelectListItem
                {
                    Text = "Select",
                    Value = "0",
                });
                foreach (var item in IRActionPlan)
                {
                    IRActionplanlst.Add(new SelectListItem
                    {
                        Text = item.Capa_ActionName,
                        Value = item.Capa_ActionID.ToString(),
                    });
                }
                ViewBag.IRPlanofAction = new SelectList(IRActionplanlst.ToList(), "Value", "Text");
                //end capa view

                TempData["CurrentStatus"] = 0;
                //IncidentBO incdntBO = new IncidentBO();
                var Incdnt_recordslist = (from i in dbEF.AizantIT_IncidentMaster
                                          where i.IncidentMasterID == IncidentMasterID
                                          select new
                                          {
                                              i.IncidentMasterID,
                                              i.HeadQAEmpID,
                                              i.HODEmpID,
                                              i.IncidentDesc,
                                              i.IncidentReportDate,
                                              i.DueDate,
                                              i.InitiatorEmpID,
                                              i.InvestigatorEmpID,
                                              i.IRNumber,
                                              i.IsAnyTrainingRequired,
                                              i.IsCAPARequired,
                                              i.OccurrenceDate,
                                              i.QAClassComments,
                                              i.QAEmpID,
                                              i.TypeofIncidentID,
                                              i.CAPAClosureVerification_Desc,
                                              i.CreatedDate,
                                              i.CurrentStatus,
                                              i.DeptID,
                                              i.DocTypeID,
                                              i.EvalutionAndDisposition_Desc,
                                              i.IsFirstOccurence,
                                              i.AnyOtherInvestigations,
                                              i.FO_Comments,
                                              i.AOI_Comments,
                                              i.ImmediateCorrection,
                                              i.Conclusion_Desc,
                                              i.TypeofCategoryID,
                                              i.HOD_InvestigationNotes
                                              //i.FinalClassification
                                          }).ToList();
                foreach (var item in Incdnt_recordslist)
                {
                    incdntBO.IncidentMasterID = (int)item.IncidentMasterID;
                    incdntBO.DeptID = (int)item.DeptID;
                    incdntBO.CAPAClosureVerification_Desc = item.CAPAClosureVerification_Desc;
                    incdntBO.CreatedDate = Convert.ToDateTime(item.CreatedDate).ToString("dd MMM yyyy");
                    incdntBO.CurrentStatus = item.CurrentStatus.ToString();
                    TempData["CurrentStatus"] = item.CurrentStatus.ToString();
                    TempData.Keep("CurrentStatus");
                    incdntBO.EvaluationandDispostion = item.EvalutionAndDisposition_Desc;
                    incdntBO.IncidentDesc = item.IncidentDesc;
                    incdntBO.IncidentReportDate = Convert.ToDateTime(item.IncidentReportDate).ToString("dd MMM yyyy");
                    incdntBO.IncidentDueDate = Convert.ToDateTime(item.DueDate).ToString("dd MMM yyyy");
                    incdntBO.IncidentReportDateEdit = Convert.ToDateTime(item.IncidentReportDate).ToString("dd MMM yyyy");
                    incdntBO.OccurrenceDate = Convert.ToDateTime(item.OccurrenceDate).ToString("dd MMM yyyy");
                    incdntBO.IRNumber = item.IRNumber;
                    TempData["IRNumber"] = item.IRNumber;
                    incdntBO.InitiatorEmpID = (int)item.InitiatorEmpID;
                    incdntBO.HODEmpID = (int)item.HODEmpID;
                    incdntBO.DocumentType = (int)item.DocTypeID;
                    incdntBO.IsFirstOccurence = item.IsFirstOccurence.ToString();
                    incdntBO.AnyOtherInvestigations = item.AnyOtherInvestigations.ToString();
                    incdntBO.ImmediateCorrection = item.ImmediateCorrection != null ? item.ImmediateCorrection.ToString() : "";
                    incdntBO.IsAnyTrainingRequired = Convert.ToInt32(item.IsAnyTrainingRequired);
                    incdntBO.HeadQAEmpID = Convert.ToInt32(item.HeadQAEmpID);
                    incdntBO.QNonImpConclusion = item.Conclusion_Desc;
                    incdntBO.QAEmpID = Convert.ToInt32(item.QAEmpID);
                    if(item.InvestigatorEmpID!=null)
                    incdntBO.InvestigatorEmpID = Convert.ToInt32(item.InvestigatorEmpID);
                    incdntBO.InvestigatorEmpName = GetEmployeeName(Convert.ToInt32(item.InvestigatorEmpID));
                    //Adding HOD_InvestigationNotes
                    incdntBO.HOD_InvestigationNotes = item.HOD_InvestigationNotes;
                    incdntBO.TypeofCategoryID = Convert.ToInt32(item.TypeofCategoryID);
                    if (item.CurrentStatus == 1 || item.CurrentStatus == 2 || item.CurrentStatus==7)
                    {
                        incdntBO.FO_Comments = "";
                        incdntBO.AOI_Comments = "";
                    }
                    else
                    {
                        incdntBO.FO_Comments = (item.FO_Comments == null || item.FO_Comments == "") ? "N/A" : item.FO_Comments.Trim();
                        incdntBO.AOI_Comments = (item.AOI_Comments == null || item.AOI_Comments == "") ? "N/A" : item.AOI_Comments.Trim();
                    }
                    if (incdntBO.TypeofCategoryID != 0 && incdntBO.TypeofCategoryID!=-1)
                    {
                        incdntBO.TypeofCategory = GetCategoryName(Convert.ToInt32(incdntBO.TypeofCategoryID));
                    }
                    else
                    {
                        incdntBO.TypeofCategory = "N/A";
                    }

                    //To Get Latest Comments
                    var HODRevertedComments = (from IH in dbEF.AizantIT_IncidentHistory
                                               where IH.IncidentMasterID == IncidentMasterID && IH.AssignedTo != 0
                                               && IH.ActionStatus!=33
                                               orderby IH.IncidentHistoryID descending
                                               select new
                                               {
                                                   IH.Comments
                                               }).Take(1);

                    foreach (var hodcmts in HODRevertedComments)
                    {
                        incdntBO.HodRevertedComments = hodcmts.Comments.ToString();
                        incdntBO.QARevertedComments = hodcmts.Comments.ToString();
                        incdntBO.ApproveComments = hodcmts.Comments.ToString();
                    }

                    DocumentDetialsClass objDocument = new DocumentDetialsClass();
                    QMSCommonActions CommonDocBo = new QMSCommonActions();
                    if (incdntBO.DocumentType != 0 && incdntBO.DocumentType != -1)
                    {
                        //start commented on 19-02-2020
                        objDocument = CommonDocBo.GetDocumentDetails(incdntBO.IncidentMasterID, 1);

                        incdntBO.DocumentNo = objDocument.DocumentNumber;
                        incdntBO.DocName = objDocument.DocumentName;
                        incdntBO.ReferenceNumber = objDocument.VersionID.ToString();
                        incdntBO.TypeofDocumentName = objDocument.DocumentType;
                        //end commented on 19-02-2020
                    }
                    else if (incdntBO.DocumentType == 0)//Others
                    {
                        //start commented on 19-02-2020
                        objDocument = CommonDocBo.GetOtherDocumentDetails(incdntBO.IncidentMasterID, 1);

                        incdntBO.DocumentNo = objDocument.DocumentNumber;
                        incdntBO.DocName = objDocument.DocumentName;
                        incdntBO.DocTitle = objDocument.DocumentName;
                        incdntBO.OtherDOCRefNo = objDocument.DocumentNumber;
                        incdntBO.TypeofDocumentName = "Others";
                        //end commented on 19-02-2020
                    }
                    else
                    {
                        incdntBO.DocumentNo = "N/A";
                        incdntBO.DocName = "N/A";
                        incdntBO.DocTitle = "N/A";
                        incdntBO.OtherDOCRefNo = "N/A";
                        incdntBO.TypeofDocumentName = "N/A";
                    }
                    //Modified Current Status using String array with Enums(cs= "6","9","21","12","13","11","10","14","25","15","19","20","16","27")19 - 06 - 2019
                    int[] array_currentStatus = { Convert.ToInt32(Incident_Status.Approved_by_QA), Convert.ToInt32(Incident_Status.Completed_Invest_by_Investigator),
                    Convert.ToInt32(Incident_Status.At_ActionPlan),Convert.ToInt32(Incident_Status.HOD_Approved_and_added_ActionPlan_Training),
                    Convert.ToInt32(Incident_Status.Incd_with_Invest_Approved_by_QA),Convert.ToInt32(Incident_Status.Incd_with_Invest_Reverted_to_Investigator_by_HOD),
                    Convert.ToInt32(Incident_Status.Updated_Invest_by_Investigator),Convert.ToInt32(Incident_Status.Incd_with_Invest_Revert_to_Invest_by_QA),
                    Convert.ToInt32(Incident_Status.Incd_with_Invest_Revert_to_HOD_by_QA),Convert.ToInt32(Incident_Status.Incd_with_Invest_Modified_ActionPlan_by_HOD),
                    Convert.ToInt32(Incident_Status.Incd_with_Invest_Reverted_to_Invest_by_HeadQA),Convert.ToInt32(Incident_Status.Incd_with_Invest_Reverted_to_HOD_by_HeadQA),
                    Convert.ToInt32(Incident_Status.Incd_with_Invest_Approved_by_HeadQA),Convert.ToInt32(Incident_Status.Verified_by_QA), Convert.ToInt32(Incident_Status.Rejected_by_QA_with_Invest),
                    Convert.ToInt32(Incident_Status.Incd_with_Invest_Rejected_by_HeadQA)
                };
                    //Current Status = "6","9","21","12","13","11","10","14","25","15","19","20","16","27"
                    if (array_currentStatus.Contains(Convert.ToInt32(incdntBO.CurrentStatus)))
                    //if (new[] { "6", "9", "21", "12", "13", "11", "10", "14", "25", "15", "19", "20", "16", "27", "26", "18" }.Contains(incdntBO.CurrentStatus))
                    {
                        incdntBO.TypeofIncidentID = Convert.ToInt32(item.TypeofIncidentID);
                        if (incdntBO.TypeofIncidentID == 1)
                        {
                            incdntBO.TypeofIncidentName = "Quality Impacting";
                        }
                        else if (incdntBO.TypeofIncidentID == 2)
                        {
                            incdntBO.TypeofIncidentName = "Quality Non-Impacting";
                        }
                    }

                    

                    var GetStatusName = (from i in dbEF.AizantIT_IncidentMaster
                                         join cs in dbEF.AizantIT_ObjectStatus on i.CurrentStatus equals cs.StatusID
                                         where cs.StatusID.ToString() == incdntBO.CurrentStatus && cs.ObjID == 12
                                         select new { cs.StatusName }).ToList();
                    foreach (var item6 in GetStatusName)
                    {
                        incdntBO.CurrentStatusName = item6.StatusName;
                    }

                    var DeptName = (from dm in dbEF.AizantIT_DepartmentMaster
                                    where dm.DeptID == incdntBO.DeptID
                                    select new { dm.DepartmentName }).ToList();
                    foreach (var item1 in DeptName)
                    {
                        incdntBO.DeptName = item1.DepartmentName;
                    }

                    var EmpName = (from dm in dbEF.AizantIT_EmpMaster
                                   where dm.EmpID == incdntBO.InitiatorEmpID
                                   select new { FullName = dm.FirstName + "  " + dm.LastName }).ToList();
                    foreach (var item2 in EmpName)
                    {
                        incdntBO.EmpName = item2.FullName;
                    }

                    //Initiator ,hod employee id not binded to Assigned to QA Dropdown
                    List<int> QAEmpid = new List<int>() { incdntBO.InitiatorEmpID, incdntBO.HODEmpID };
                    //Initiator ,HOD and QA employee id not binded to Assigned to HeadQA Dropdown

                    List<int> HQAEmpid = new List<int>() { incdntBO.InitiatorEmpID, incdntBO.HODEmpID, incdntBO.QAEmpID };
                    List<SelectListItem> objAssignQA = new List<SelectListItem>();
                    DataSet dtemp = Qms_bal.GetEmpNamesbasedonRoleIDBal((int)QMS_Roles.QA, "A"); //26=QA
                    objAssignQA.Add(new SelectListItem
                    {
                        Text = "Select",
                        Value = "0",
                    });
                    foreach (DataRow Item in dtemp.Tables[0].Rows)
                    {
                        if (!QAEmpid.Contains(Convert.ToInt32(Item["EmpID"])))
                        {
                            objAssignQA.Add(new SelectListItem()
                            {
                                Text = (Item["EmpName"]).ToString(),
                                Value = (Item["EmpID"]).ToString(),
                                Selected = (Convert.ToInt32(Item["EmpID"]) == incdntBO.QAEmpID ? true : false)
                            });
                        }
                    }
                    //ViewBag.QAEmployee = new SelectList(objAssignQA.Distinct().ToList(), "Value", "Text");
                    incdntBO.ddlQaList = objAssignQA;
                    // List<int> InvestigatorEmpID = new List<int>() { incdntBO.InitiatorEmpID, incdntBO.HODEmpID, incdntBO.QAEmpID, incdntBO.HeadQAEmpID ,incdntBO.InvestigatorEmpID};
                    EmployeID(incdntBO);
                    List<int> InvestEmpID = new List<int>() { incdntBO.InitiatorEmpID, incdntBO.HODEmpID };
                    List<SelectListItem> objInvestigatorlst = new List<SelectListItem>();
                    DataSet dtempIN = Qms_bal.GetEmpNamesbasedonRoleIDBal((int)QMS_Roles.Investigator, "A"); //33=Investigator
                    objInvestigatorlst.Add(new SelectListItem
                    {
                        Text = "Select",
                        Value = "0",
                    });
                    foreach (DataRow Item2 in dtempIN.Tables[0].Rows)
                    {
                        if (!InvestEmpID.Contains(Convert.ToInt32(Item2["EmpID"])))
                        {
                            objInvestigatorlst.Add(new SelectListItem()
                            {
                                Text = (Item2["EmpName"]).ToString(),
                                Value = (Item2["EmpID"]).ToString(),
                                Selected = (Convert.ToInt32(Item2["EmpID"]) == incdntBO.InvestigatorEmpID ? true : false)
                            });
                        }
                    }
                    incdntBO.ddlInvestigatorList = objInvestigatorlst;

                    //ViewBag.ddlInvestigatorID = new SelectList(objInvestigatorlst.Distinct().ToList(), "Value", "Text");
                    var InvestID = (from IN in dbEF.AizantIT_IncidentMaster
                                    where IN.IncidentMasterID == IncidentMasterID
                                    select IN.InvestigatorEmpID).SingleOrDefault();
                    int InvesigatorEmpID = Convert.ToInt32(InvestID);
                    List<int> HqaEmpid = new List<int>() { incdntBO.InitiatorEmpID, incdntBO.HODEmpID, incdntBO.QAEmpID, InvesigatorEmpID };
                    List<SelectListItem> objHQAlst = new List<SelectListItem>();
                    DataSet dtempHQA = Qms_bal.GetEmpNamesbasedonRoleIDBal((int)QMS_Roles.HeadQA, "A"); //27=Investigator
                    
                    foreach (DataRow Item3 in dtempHQA.Tables[0].Rows)
                    {
                        if (!HqaEmpid.Contains(Convert.ToInt32(Item3["EmpID"])))
                        {
                            objHQAlst.Add(new SelectListItem()
                            {
                                Text = (Item3["EmpName"]).ToString(),
                                Value = (Item3["EmpID"]).ToString(),
                                

                            });
                        }
                    }
                    
                    ViewBag.ddlAssignHeadQA = new SelectList(objHQAlst.Distinct().ToList(), "Value", "Text");
                    //to load all departments and Employees for Investigation Who are the people involved part-1
                    List<SelectListItem> Depts = new List<SelectListItem>();
                    var GetAllDepts = (from d in dbEF.AizantIT_DepartmentMaster
                                       select new { d.DeptID, d.DepartmentName }).Distinct().ToList();
                    Depts.Add(new SelectListItem
                    {
                        Text = "Select",
                        Value = "0",
                    });
                    foreach (var items in GetAllDepts)
                    {
                        Depts.Add(new SelectListItem
                        {
                            Text = items.DepartmentName,
                            Value = items.DeptID.ToString(),
                        });
                    }
                    ViewBag.ddlAllDepartments = new SelectList(Depts.ToList(), "Value", "Text");
                    ViewBag.ddlAllDepartmentsforhods = new SelectList(Depts.ToList(), "Value", "Text");
                    //to load Masters into dropdowns Yes,No,N/A
                    var GetImpactStatusMasters = (from s in dbEF.AizantIT_Incident_EffectStatusMaster
                                                  select new { s.EffectStatusID, s.EffectStatusName }).Distinct().ToList();
                    ViewBag.DDlYesNo = new SelectList(GetImpactStatusMasters.Distinct().ToList(), "EffectStatusID", "EffectStatusName");
                    List<SelectListItem> DDlYesNolst = new List<SelectListItem>();
                    DDlYesNolst.Add(new SelectListItem
                    {
                        Text = "Select",
                        Value = "0",
                    });
                    foreach (var items in GetImpactStatusMasters)
                    {
                        DDlYesNolst.Add(new SelectListItem
                        {
                            Text = items.EffectStatusName,
                            Value = items.EffectStatusID.ToString(),
                        });
                    }
                    if(incdntBO.IsAnyTrainingRequired==0) //2 means Yes 
                   // if (new[] { "21" }.Contains(incdntBO.CurrentStatus))
                    {
                        ViewBag.IsTrainingDDlYesNo = new SelectList(DDlYesNolst.ToList(), "Value", "Text");
                    }
                    else
                    {
                        ViewBag.IsTrainingDDlYesNo = new SelectList(DDlYesNolst.ToList(), "Value", "Text", incdntBO.IsAnyTrainingRequired);
                    }
                    //For Investigaton Save this viewbag called in pv's.
                    ViewBag.GetIsIncidentExitsCount = (from Q in dbEF.AizantIT_Incident_QualityImpacting
                                                       where Q.IncidentMasterID == IncidentMasterID
                                                       select Q).Count();
                    //to bind Has Effected masters for part-3
                    List<IncidentEffectedPoints> HasAffectedlst = new List<IncidentEffectedPoints>();
                    //Modified Current Status with Enums (cs="6")20-06-2019
                    if (TempData["CurrentStatus"].ToString() == Convert.ToInt32(Incident_Status.Approved_by_QA).ToString() && Convert.ToInt32(ViewBag.GetIsIncidentExitsCount) == 0) //Current Status="6
                    {
                        var HasAffected = (from ef in dbEF.AizantIT_Incident_EffectedMaster
                                           select new
                                           {
                                               ef.EffectedName,
                                               ef.EffectedID
                                           }).ToList();
                        foreach (var dr in HasAffected)
                        {
                            HasAffectedlst.Add(new IncidentEffectedPoints
                            {
                                EffectedName = dr.EffectedName.ToString(),
                                EffectedID = Convert.ToInt32(dr.EffectedID),
                            });
                        }
                        ViewBag.Affectedlst = HasAffectedlst.AsEnumerable();
                    }
                    else
                    {
                        //Binding Data From DB for Update or Show.
                        var GetHasAffectedPointsdata = (from i in dbEF.AizantIT_Incident_Effected
                                                        join e in dbEF.AizantIT_Incident_EffectedMaster on i.EffectedID equals e.EffectedID
                                                        where i.IncidentMasterID == IncidentMasterID && i.EffectTypeID == 1
                                                        select new { i.UniquePKID, i.IncidentMasterID, i.IsEffect, i.EffectedID, i.ReferenceNo, i.EffectedComments, e.EffectedName }).ToList();
                        foreach (var dr in GetHasAffectedPointsdata)
                        {
                            HasAffectedlst.Add(new IncidentEffectedPoints
                            {
                                EffectedName = dr.EffectedName.ToString(),
                                EffectedID = Convert.ToInt32(dr.EffectedID.ToString()),
                                ReferenceNo = dr.ReferenceNo != null ? dr.ReferenceNo.ToString() : "N/A",
                                Comments = dr.EffectedComments != null ? dr.EffectedComments.ToString() : "N/A",
                                isEffect = Convert.ToInt32(dr.IsEffect).ToString(),
                                _lstHasAffectedStatus = new SelectList(dbEF.AizantIT_Incident_EffectStatusMaster.ToList(), "EffectStatusID", "EffectStatusName", Convert.ToInt32(dr.IsEffect).ToString())
                            }
                            );
                        }
                        ViewBag.Affectedlst = HasAffectedlst.AsEnumerable();
                    }

                    //to bind Was Effected masters for part-3
                    List<IncidentWasEffectedPoints> WasAffectedlst = new List<IncidentWasEffectedPoints>();
                    //Modified Current Status with Enums (cs="6")19-06-2019
                    if (TempData["CurrentStatus"].ToString() == Convert.ToInt32(Incident_Status.Approved_by_QA).ToString() && Convert.ToInt32(ViewBag.GetIsIncidentExitsCount) == 0)  //Current Status = "6"
                    {
                        var WasAffected = (from ef in dbEF.AizantIT_Incident_EffectedMaster
                                           select new
                                           {
                                               ef.EffectedName,
                                               ef.EffectedID
                                           }).ToList();
                        foreach (var dr in WasAffected)
                        {
                            WasAffectedlst.Add(new IncidentWasEffectedPoints
                            {
                                EffectedName = dr.EffectedName.ToString(),
                                EffectedID = Convert.ToInt32(dr.EffectedID),
                            });
                        }
                        ViewBag.WasAffectedlst = WasAffectedlst.AsEnumerable();
                    }
                    else
                    {
                        //Binding Data From DB for Update or Show.
                        var GetWasAffectedPointsdata = (from i in dbEF.AizantIT_Incident_Effected
                                                        join e in dbEF.AizantIT_Incident_EffectedMaster on i.EffectedID equals e.EffectedID
                                                        where i.IncidentMasterID == IncidentMasterID && i.EffectTypeID == 2
                                                        select new { i.UniquePKID, i.IncidentMasterID, i.IsEffect, i.EffectedID, i.ReferenceNo, i.EffectedComments, e.EffectedName }).ToList();
                        foreach (var dr in GetWasAffectedPointsdata)
                        {
                            WasAffectedlst.Add(new IncidentWasEffectedPoints
                            {
                                EffectedName = dr.EffectedName.ToString(),
                                EffectedID = Convert.ToInt32(dr.EffectedID.ToString()),
                                ReferenceNo = dr.ReferenceNo != null ? dr.ReferenceNo.ToString() : "N/A",
                                Comments = dr.EffectedComments != null ? dr.EffectedComments.ToString() : "N/A",
                                isEffect = Convert.ToInt32(dr.IsEffect).ToString(),
                                _lstWasAffectedStatus = new SelectList(dbEF.AizantIT_Incident_EffectStatusMaster.ToList(), "EffectStatusID", "EffectStatusName", Convert.ToInt32(dr.IsEffect).ToString())
                            }
                                );
                        }
                        ViewBag.WasAffectedlst = WasAffectedlst.AsEnumerable();
                    }

                    //to bind Review masters for part-5
                    List<IncidentReviewPoints> Reviewlst = new List<IncidentReviewPoints>();
                    //Modified Current Status with Enums (cs="6")19-06-2019
                    if (TempData["CurrentStatus"].ToString() == Convert.ToInt32(Incident_Status.Approved_by_QA).ToString() && Convert.ToInt32(ViewBag.GetIsIncidentExitsCount) == 0) //Current Status = "6"
                    {
                        var GetReview = (from rv in dbEF.AizantIT_Incident_ReviewMaster
                                         select new
                                         {
                                             rv.ReviewName,
                                             rv.ReviewID
                                         }).ToList();
                        foreach (var dr1 in GetReview)
                        {
                            Reviewlst.Add(new IncidentReviewPoints
                            {
                                ReviewName = dr1.ReviewName.ToString(),
                                ReviewID = Convert.ToInt32(dr1.ReviewID),
                            });
                        }
                        ViewBag.ReviewLst = Reviewlst.AsEnumerable();
                    }
                    else
                    {
                        //Binding Data From DB for Update or Show.
                        var GetReviewPointsdata = (from i in dbEF.AizantIT_Incident_Review
                                                   join r in dbEF.AizantIT_Incident_ReviewMaster on i.ReviewID equals r.ReviewID
                                                   where i.IncidentMasterID == IncidentMasterID
                                                   select new { i.UniquePKID, i.IncidentMasterID, i.isReview, i.ReviewID, i.ReferenceNo, i.ReviewComments, r.ReviewName }).ToList();
                        foreach (var dr in GetReviewPointsdata)
                        {
                            Reviewlst.Add(new IncidentReviewPoints
                            {
                                ReviewName = dr.ReviewName.ToString(),
                                ReviewID = Convert.ToInt32(dr.ReviewID.ToString()),
                                ReferenceNo = dr.ReferenceNo != null ? dr.ReferenceNo.ToString() : "N/A",
                                Comments = dr.ReviewComments != null ? dr.ReviewComments.ToString() : "N/A",
                                isReview = Convert.ToInt32(dr.isReview).ToString(),
                                _lstisReviewStatus = new SelectList(dbEF.AizantIT_Incident_EffectStatusMaster.ToList(), "EffectStatusID", "EffectStatusName", Convert.ToInt32(dr.isReview).ToString())
                            }
                                );
                        }
                        ViewBag.ReviewLst = Reviewlst.AsEnumerable();
                    }

                    objUMS_BAL = new UMS_BAL();
                    List<SelectListItem> objAssignHod = new List<SelectListItem>();
                    DataTable dtempr = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(incdntBO.DeptID), (int)QMS_Roles.HOD, true); //25=hod
                    foreach (DataRow RPItem in dtempr.Rows)
                    {
                        objAssignHod.Add(new SelectListItem()
                        {
                            Text = (RPItem["EmpName"]).ToString(),
                            Value = (RPItem["EmpID"]).ToString()
                        });
                    }
                    ViewBag.ddlHods = new SelectList(objAssignHod.ToList(), "Value", "Text", item.HODEmpID);

                    List<SelectListItem> _ddlEmpQAs = new List<SelectListItem>();
                    DataSet dtempQA = Qms_bal.GetEmpNamesbasedonRoleIDBal((int)QMS_Roles.QA, "A"); //26=QA
                    foreach (DataRow Item in dtempQA.Tables[0].Rows)
                    {
                        if (!QAEmpid.Contains(Convert.ToInt32(Item["EmpID"])))
                        {
                            _ddlEmpQAs.Add(new SelectListItem()
                            {
                                Text = (Item["EmpName"]).ToString(),
                                Value = (Item["EmpID"]).ToString()
                            });
                        }
                    }
                    ViewBag.ddlEmpQAs = new SelectList(_ddlEmpQAs.Distinct().ToList(), "Value", "Text", item.QAEmpID);
                    // to load all Masters Type of Document for Investigation 
                    List<SelectListItem> typedocslst = new List<SelectListItem>();
                    var GetTypeofDocsMasters = (from d in dbEF.AizantIT_Incident_InvestTypeMaster
                                                select new { d.InvestTypeID, d.InvestTypeName }).Distinct().ToList();
                    typedocslst.Add(new SelectListItem
                    {
                        Text = "Select",
                        Value = "0",
                    });
                    foreach (var items in GetTypeofDocsMasters)
                    {
                        typedocslst.Add(new SelectListItem
                        {
                            Text = items.InvestTypeName,
                            Value = items.InvestTypeID.ToString(),
                        });
                    }
                    ViewBag.ddlTypeofDocumentID = new SelectList(typedocslst.ToList(), "Value", "Text");
                    //To Update Hod Comments Review Get Unique ID
                    EmployeID(incdntBO);
                    var GetUniqueIDforHodReview = (from dm in dbEF.AizantIT_Incident_InvestigationReviewDeptHods
                                                   where dm.EmpID == incdntBO.EmpID && dm.IncidentMasterID == incdntBO.IncidentMasterID
                                                   select new { dm.UniquePKID }).ToList();
                    foreach (var item2 in GetUniqueIDforHodReview)
                    {
                        incdntBO.UniquePKID = item2.UniquePKID;
                    }

                    List<SelectListItem> IRDep = new List<SelectListItem>();
                    var IRDepartment1 = (from DM in dbEF.AizantIT_DepartmentMaster
                                         orderby DM.DepartmentName
                                         select new { DM.DeptID, DM.DepartmentName }).ToList();
                    IRDep.Add(new SelectListItem
                    {
                        Text = "Select",
                        Value = "0",
                    });
                    foreach (var item5 in IRDepartment1)
                    {
                        IRDep.Add(new SelectListItem
                        {
                            Text = item5.DepartmentName,
                            Value = item5.DeptID.ToString(),
                        });
                    }
                    ViewBag.ActionDeptID = new SelectList(IRDep.ToList(), "Value", "Text");
                    List<SelectListItem> Actionplanlst = new List<SelectListItem>();
                    var ActionPlan = (from AD in dbEF.AizantIT_ActionPlanDescription
                                      where AD.CurrentStatus == true
                                      select new { AD.Capa_ActionID, AD.Capa_ActionName }).ToList();
                    Actionplanlst.Add(new SelectListItem
                    {
                        Text = "Select",
                        Value = "0",
                    });
                    foreach (var item6 in ActionPlan)
                    {
                        Actionplanlst.Add(new SelectListItem
                        {
                            Text = item6.Capa_ActionName,
                            Value = item6.Capa_ActionID.ToString(),
                        });
                    }
                    ViewBag.PlanofAction = new SelectList(Actionplanlst.ToList(), "Value", "Text");
                    ViewBag.Qualityevent = new SelectList(dbEF.AizantIT_QualityEvent, "QualityEvent_TypeID", "QualityEvent_TypeName", 1);

                    //bind investigation details
                    var GetQualityImpactingDetails = (from q in dbEF.AizantIT_Incident_QualityImpacting
                                                      where q.IncidentMasterID == incdntBO.IncidentMasterID
                                                      select new { q.WhatHappened_Desc, q.WhenHappened_Desc, q.WhereHappened_Desc, q.PossibleRootCause_Desc, q.Conclusion_Desc, q.IsHasIncidentAfftectedDoc }).ToList();
                    foreach (var itmQI in GetQualityImpactingDetails)
                    {
                        incdntBO.WhatHappen = itmQI.WhatHappened_Desc;
                        incdntBO.WhenHappen = itmQI.WhenHappened_Desc;
                        incdntBO.WhereHappen = itmQI.WhereHappened_Desc;
                        incdntBO.IsHasIncidentAfftectedDoc = (itmQI.IsHasIncidentAfftectedDoc).ToString();
                        incdntBO.QImpConclusion = itmQI.Conclusion_Desc;
                        incdntBO.RootCauseDesc = itmQI.PossibleRootCause_Desc;
                    }
                    if (new[] { "6" }.Contains(incdntBO.CurrentStatus) && incdntBO.IsHasIncidentAfftectedDoc == "1")
                    {
                        ViewBag.ddlHasAffectedDoc = new SelectList(GetImpactStatusMasters.Distinct().ToList(), "EffectStatusID", "EffectStatusName");
                    }
                    else
                    {
                        ViewBag.ddlHasAffectedDoc = new SelectList(GetImpactStatusMasters.Distinct().ToList(), "EffectStatusID", "EffectStatusName", incdntBO.IsHasIncidentAfftectedDoc);
                    }
                    if (incdntBO.CurrentStatus == ((int)Incident_Status.Approved_by_HOD).ToString() || incdntBO.CurrentStatus == ((int)Incident_Status.Incd_Modified_by_HOD).ToString())//Modified Current Status with Enums (cs="3" || cs="17")19-06-2019
                    {

                        List<int> Roleids = new List<int>() { 24, 25 };//25-hod,24-Initiator
                        var role = (from R in dbEF.AizantIT_Roles
                                    where Roleids.Contains(R.RoleID)
                                    select new { R.RoleID, R.RoleName }).ToList();
                        ViewBag.ddlQMSRole = new SelectList(role.ToList(), "RoleID", "RoleName");
                    }
                    else
                    {
                        List<int> Roleids = new List<int>() { 25, 33 };//25-hod,33-Investigator
                        var role = (from R in dbEF.AizantIT_Roles
                                    where Roleids.Contains(R.RoleID)
                                    select new { R.RoleID, R.RoleName }).ToList();
                        ViewBag.ddlQMSRole = new SelectList(role.ToList(), "RoleID", "RoleName");
                    }
                    incdntBO.CountFiles = (from CInitator in dbEF.AizantIT_Incident_InitiatorAttachments
                                           where CInitator.IncidentMasterID == IncidentMasterID
                                           select CInitator).Count();

                }
                return incdntBO;
            }
        }
        //revert and approve comments
        
        #region IncidentReviewList
        //Incident View with Tabs content
        public string GetEmployeeName(int EmpID)
        {
            AizantIT_DevEntities dbEF = new AizantIT_DevEntities();
            var Empname = (from e in dbEF.AizantIT_EmpMaster where e.EmpID == EmpID
            select  ( e.FirstName + " " + e.LastName )).SingleOrDefault();
                
            return Empname;
        }
        public string GetCategoryName(int CategoryID)
        {
            AizantIT_DevEntities dbEF = new AizantIT_DevEntities();
            var TypeofCategory = (from e in dbEF.AizantIT_TypeOfIncident
                                  where e.TypeofCategoryID == CategoryID
                                  select e.TypeofIncident).SingleOrDefault();
            return TypeofCategory;
        }

        public ActionResult IncidentReviewList(int IncidentMasterID, string Number=null, int UniquePKID=0, int ShowType=0, int Notification_ID = 0)
        {
            try
            {
                using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
                {
                    
                    Session["OnlyforView"] = -1;//(-1) - defalut value for exception handling.
                    TempData["IncidentMasterID"] = 0;
                    TempData["IncidentMasterID"] = IncidentMasterID;
                    TempData.Keep("IncidentMasterID");
                    DeleteInvestigationFormisTemp(IncidentMasterID);
                    DeleteCapaTemp(IncidentMasterID);
                    if (Notification_ID != 0)
                    {
                        int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                        GetNotificationUpdate(Notification_ID, EmpIDs);
                        TempData["ShowType"] = ShowType;
                        TempData.Keep("ShowType");
                        if (ShowType == 3 || ShowType == 4)
                        {
                            Number = "1";
                            UniquePKID = 0;
                        } else if(ShowType == 5)
                        {
                            Number = "2"; UniquePKID = 0;
                         } else if (ShowType == 8 || ShowType == 10 || ShowType == 11 || ShowType == 7) { Number = "0"; UniquePKID = 0; }
                    }
                    else
                    {
                        // ShowType =(int)TempData["ShowType"];
                        //TempData["ShowType"] = ShowType;
                        //TempData.Keep("ShowType");

                        TempData.Keep("ShowType");
                    }
                    IncidentBO incdntBO = new IncidentBO();
                  
                    BindDeptAndDocumentType(incdntBO);
                    BindAllDepts();
                    //For Binding Quality Events Name on Map Quality Events
                    BindQualityEventNames();
                    //end

                    incdntBO = BindAllIncidentDetails(IncidentMasterID);

                    //For Changing Flag value for Mapa CAPA and Map Incident on isTempDel.
                    if (incdntBO.CurrentStatus == "7")
                    {
                        MapCAPAChangingFlagValue(IncidentMasterID);
                        MapIncidentChangingFlagValue(IncidentMasterID);
                    }
                    //end

                    InvestigatorImpactPV(IncidentMasterID);
                    if (Number == "9")
                    {
                        incdntBO.UniquePKID = UniquePKID;
                    }
                    //History Details in Incident
                    //ViewBag.historyIncident = HistoryListIncident(IncidentMasterID);

                    ViewBag.InvestExitsorNot = (from Q in dbEF.AizantIT_Incident_QualityImpacting
                                                where Q.IncidentMasterID == IncidentMasterID
                                                select Q).Count();

                    ViewBag.TTSExitsCount = (from T in dbEF.AizantIT_QITTS_Master
                                             where T.Incident_ID == IncidentMasterID
                                             select T).Count();
                     GetMLStatusExistOrNot(incdntBO);



                    return View(incdntBO);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M5:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return View();
            }
        }
        #endregion
        //ML Checking ML Exist or Not
        public void GetMLStatusExistOrNot(IncidentBO incdntBO)
        {
            API_BAL objApIBAL = new API_BAL();
            if (Array.Exists(objApIBAL.GetMLFeatureChecking(4),a=>a.Equals(2)==true))
            {
                incdntBO.MLIncidentStatus = true;
            }
        }
        public void MapCAPAChangingFlagValue(int IncidentMasterID)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                //AizantIT_IncidentPreAssmnt aipreAssmnt = dbEF.AizantIT_IncidentPreAssmnt.Where(a => a.IncidentMasterID == IncidentMasterID);
                //aipreAssmnt.isTempDel = true;

                var aipreAssmnt = dbEF.AizantIT_IncidentPreAssmnt.Where(a => a.IncidentMasterID == IncidentMasterID && a.isTempDel == false).ToList();
                aipreAssmnt.ForEach(a => a.isTempDel = true);
                dbEF.SaveChanges();
            }
        }

        public void MapIncidentChangingFlagValue(int IncidentMasterID)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                var ai_Inci_preAssmnt = dbEF.AizantIT_IncidentPreAssmntOther.Where(a => a.IncidentMasterID == IncidentMasterID && a.isTempDel == false).ToList();
                ai_Inci_preAssmnt.ForEach(a => a.isTempDel = true);
                dbEF.SaveChanges();
            }
        }

        #region Incident Creation
        //Incident Create Binding Methods
        public ActionResult CreateIncident(int? IncidentMasterID)
        {
            try
            {
                using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
                {
                    TempData.Remove("TempRefID");//file uploads refid
                    IncidentBO incidentBO = new IncidentBO();
                    incidentBO.CurrentDateStatus = "0";
                    Session["OnlyforView"] = -1;//(-1) - defalut value for exception handling.

                    var ShowType = TempData["ShowType"];
                    TempData["ShowType"] = ShowType;
                    TempData.Keep("ShowType");
                    if (TempData["ShowType"].ToString() == "2" || IncidentMasterID == 0)
                    {
                        Session["OnlyforView"] = 2;//2 - Initiator Revert list then show Action buttons.
                    }

                    if (IncidentMasterID == 0 || IncidentMasterID == null)//for Create
                    {
                        BindDeptAndDocumentType(incidentBO);
                       
                        //when creating file uploads if not hardcode then null value issue in AllFileUploadsPV
                        incidentBO.CurrentStatus = "0";
                    }

                   

                    if (IncidentMasterID != 0)//for Edit
                    {
                        DeleteInitiatorTempAttachFile((int)IncidentMasterID);
                        incidentBO = BindAllIncidentDetails(Convert.ToInt32(IncidentMasterID));
                        BindDeptAndDocumentType(incidentBO);
                        var RefNo = (from dv in dbEF.AizantIT_DocVersion
                                     join DM in dbEF.AizantIT_DocumentMaster on dv.DocumentID equals DM.DocumentID
                                     where DM.DocumentTypeID == incidentBO.DocumentType && DM.DeptID == incidentBO.DeptID && (dv.Status == "A" || dv.Status == "R") && dv.EffectiveDate != null
                                     select new { dv.VersionID, DM.DocumentNumber }).ToList();
                        ViewBag.ReferenceNumber = new SelectList(RefNo.ToList(), "VersionID", "DocumentNumber", incidentBO.ReferenceNumber);
                        ViewBag.HasAffReferenceNumber = new SelectList(RefNo.ToList(), "VersionID", "DocumentNumber", incidentBO.HasAffectedDocRefNo);
                        int VersionID = Convert.ToInt32(incidentBO.ReferenceNumber);
                        if (RefNo.Count > 0)
                        {
                            bool has = RefNo.Any(t => t.VersionID == VersionID);
                            if (has == false)
                            {
                                var DocumentID = (from t1 in dbEF.AizantIT_DocVersion
                                                  where t1.VersionID == VersionID
                                                  select t1.DocumentID).SingleOrDefault();

                                var GetNewVersionID = (from t1 in dbEF.AizantIT_DocVersion
                                                       orderby t1.VersionID descending
                                                       where t1.DocumentID == DocumentID && (t1.Status == "A" || t1.Status == "R") && t1.EffectiveDate != null
                                                       select t1.VersionID);
                                int NewVersionID = GetNewVersionID.First();
                                incidentBO.DocName = (from t1 in dbEF.AizantIT_DocVersionTitle
                                                  join t2 in dbEF.AizantIT_DocVersion on t1.DocTitleID equals t2.DocTitleID
                                                  where t2.VersionID == NewVersionID
                                                  select t1.Title).SingleOrDefault();
                                ViewBag.ReferenceNumber = new SelectList(RefNo.ToList(), "VersionID", "DocumentNumber", NewVersionID);

                            }
                        }

                        //bind hod
                        objUMS_BAL = new UMS_BAL();
                        List<SelectListItem> objAssignHod = new List<SelectListItem>();
                        DataTable dtemp = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(incidentBO.DeptID), (int)QMS_Roles.HOD, true); //25=hod
                        foreach (DataRow RPItem in dtemp.Rows)
                        {
                            objAssignHod.Add(new SelectListItem()
                            {
                                Text = (RPItem["EmpName"]).ToString(),
                                Value = (RPItem["EmpID"]).ToString()
                            });
                        }
                        ViewBag.EmployeeName = new SelectList(objAssignHod.ToList(), "Value", "Text", incidentBO.HODEmpID);
                    }
                    var TypeIncident1 = (from q in dbEF.AizantIT_TypeOfIncident
                                         select new { q.TypeofCategoryID, q.TypeofIncident }).ToList();
                    List<SelectListItem> TypeIncidentList = new List<SelectListItem>();
                    TypeIncidentList.Add(new SelectListItem { Text = "select ", Value = "0" });
                    foreach (var q1 in TypeIncident1)
                    {
                        TypeIncidentList.Add(new SelectListItem
                        {
                            Text = q1.TypeofIncident,
                            Value = q1.TypeofCategoryID.ToString(),
                            Selected = (Convert.ToInt32(q1.TypeofCategoryID) == incidentBO.TypeofCategoryID ? true : false)
                        });
                    }
                    incidentBO.ddlCategoryTypeList = TypeIncidentList;
                    return PartialView(incidentBO);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M2:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }
        //For deleting temporary files on Initiator file attachments
        public void DeleteInitiatorTempAttachFile(int IncidentMasterID)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                var initiatorFile = (from IFile in dbEF.AizantIT_Incident_InitiatorAttachments
                                     where IFile.IncidentMasterID == IncidentMasterID && IFile.isTemp == false
                                     select IFile).ToList();
                if (initiatorFile.Count > 0)
                {
                    QMSAttachments objAttachmentsCommon = new QMSAttachments();
                    foreach (var item in initiatorFile)
                    {
                        objAttachmentsCommon.RemovePhysicalPathFile(item.FilePath);
                    }
                    dbEF.AizantIT_Incident_InitiatorAttachments.RemoveRange(initiatorFile);
                    dbEF.SaveChanges();
                }
            }
        }
        // Incident Header Information details View HyperLink
        public ActionResult IncidentPreviousLoadPV(int? IncidentMasterID, int OnlyforView = 0)//1-Query sting value just for view
        {
            try
            {
                using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
                {
                    Session["OnlyforView"] = OnlyforView;//for only view,if not delete button is comming
                    TempData.Remove("TempRefID");
                    IncidentBO incidentBO = new IncidentBO();
                    incidentBO.CurrentDateStatus = "0";

                    var ShowType = TempData["ShowType"];
                    TempData["ShowType"] = ShowType;
                    TempData.Keep("ShowType");

                    if (IncidentMasterID == 0 || IncidentMasterID == null)//for Create
                    {
                        BindDeptAndDocumentType(incidentBO);
                    }
                    if (IncidentMasterID != 0)//for Edit
                    {
                        incidentBO = BindAllIncidentDetails(Convert.ToInt32(IncidentMasterID));
                        BindDeptAndDocumentType(incidentBO);
                        var RefNo = (from dv in dbEF.AizantIT_DocVersion
                                     join DM in dbEF.AizantIT_DocumentMaster on dv.DocumentID equals DM.DocumentID
                                     where DM.DocumentTypeID == incidentBO.DocumentType && DM.DeptID == incidentBO.DeptID && dv.Status == "A" && dv.EffectiveDate != null
                                     select new { dv.VersionID, DM.DocumentNumber }).ToList();
                        ViewBag.ReferenceNumber = new SelectList(RefNo.ToList(), "VersionID", "DocumentNumber", incidentBO.ReferenceNumber);
                        ViewBag.HasAffReferenceNumber = new SelectList(RefNo.ToList(), "VersionID", "DocumentNumber", incidentBO.HasAffectedDocRefNo);
                        //bind hod

                        objUMS_BAL = new UMS_BAL();
                        List<SelectListItem> objAssignHod = new List<SelectListItem>();
                        DataTable dtemp = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(incidentBO.DeptID), (int)QMS_Roles.HOD, true); //25=hod
                        foreach (DataRow RPItem in dtemp.Rows)
                        {
                            objAssignHod.Add(new SelectListItem()
                            {
                                Text = (RPItem["EmpName"]).ToString(),
                                Value = (RPItem["EmpID"]).ToString()
                            });
                        }
                        ViewBag.EmployeeName = new SelectList(objAssignHod.ToList(), "Value", "Text", incidentBO.HODEmpID);
                    }
                    return PartialView(incidentBO);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M2:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }
        //Creating Incident 
        [HttpPost]
        public JsonResult Incident_Create(IncidentBO incidentBO, string Operation)//Create incident
        {
            try
            {
                EmployeID(incidentBO);
                var IncidentNumber = "";
                DataTable dt = new DataTable();
                incidentBO.InitiatorEmpID = incidentBO.EmpID;
                incidentBO.TempRefID = Convert.ToInt32(TempData["TempRefID"]);
                if (Operation == "Submitted")
                {
                    dt = Qms_bal.IncidentInsertBal(incidentBO, Operation);
                    IncidentNumber = dt.Rows[0][0].ToString();
                    TempData.Remove("TempRefID");
                }
                else
                {
                    Boolean Result = ToCheckEmpValidityIncident(incidentBO.IncidentMasterID, 24);
                    if (Result == true)
                    {
                        dt = Qms_bal.IncidentInsertBal(incidentBO, Operation);
                        IncidentNumber = incidentBO.IRNumber;
                        TempData.Remove("TempRefID");
                    }
                    else
                    {
                        return Json(new { hasError = true, data = "You are not authorized User to do action." }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new { hasError = false, data = IncidentNumber }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M3:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Initiator File Uploads
        [HttpPost]
        public JsonResult InitiatorUploadFiles()
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                if (TempData["FileMsgCreate"] == null)
                {
                    TempData["FileMsgCreate"] = "";
                }
                if (TempData["FileAddedCountCreate"] == null)
                {
                    TempData["FileAddedCountCreate"] = 0;
                }
                if (TempData["FileexistsCountCreate"] == null)
                {
                    TempData["FileexistsCountCreate"] = 0;
                }
                if (TempData["TempRefID"] == null)
                {
                    TempData["TempRefID"] = 0;
                }
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    AizantIT_Incident_InitiatorAttachments objFiles = new AizantIT_Incident_InitiatorAttachments();
                    objFiles.IncidentMasterID = Convert.ToInt32(Request.Params["IncidentMasterID"]);
                    objFiles.RoleID = 24;//Initiator Role ID 24
                    objFiles.CreatedbyID = Convert.ToInt32(Request.Params["CreatedBy"]);
                    objFiles.CreatedDate = DateTime.Now;
                    int FileexistsCount = 0;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        if (file != null)
                        {
                            objFiles.FileName = Path.GetFileName(file.FileName);
                            objFiles.FileType = Path.GetExtension(file.FileName);
                            if (objFiles.IncidentMasterID == 0) //Create files
                            {
                                if (Convert.ToInt32(TempData["TempRefID"]) == 0)
                                {
                                    AizantIT_CCNIsTempFileUpload objID = new AizantIT_CCNIsTempFileUpload();
                                    dbEF.AizantIT_CCNIsTempFileUpload.Add(objID);
                                    dbEF.SaveChanges();

                                    objFiles.TempRefID = objID.TemprefID;
                                    TempData["TempRefID"] = objID.TemprefID;
                                    TempData.Keep("TempRefID");
                                }
                                else
                                {
                                    objFiles.TempRefID = Convert.ToInt32(TempData["TempRefID"]);
                                    TempData.Keep("TempRefID");
                                }
                                var Count = dbEF.AizantIT_Incident_InitiatorAttachments.FirstOrDefault(h => h.FileName == objFiles.FileName && h.FileType == objFiles.FileType
                                && h.IncidentMasterID == objFiles.IncidentMasterID && h.TempRefID == objFiles.TempRefID);
                                if (Count != null)
                                {
                                    FileexistsCount++;
                                }
                            }
                            else
                            {
                                var Count1 = dbEF.AizantIT_Incident_InitiatorAttachments.FirstOrDefault(h => h.FileName == objFiles.FileName && h.FileType == objFiles.FileType
                               && h.IncidentMasterID == objFiles.IncidentMasterID && h.IncidentMasterID == objFiles.IncidentMasterID);
                                if (Count1 != null)
                                {
                                    FileexistsCount++;
                                }
                            }
                        }
                        if (FileexistsCount == 0)
                        {
                            if (ModelState.IsValid)
                            {
                                if (file != null)
                                {
                                    //byte[] bytes;
                                    //using (BinaryReader br = new BinaryReader(file.InputStream))
                                    //{
                                    //    bytes = br.ReadBytes(file.ContentLength);
                                    objFiles.FileName = Path.GetFileName(file.FileName);
                                    objFiles.FileType = Path.GetExtension(file.FileName);
                                    QMSAttachments objAttachmentsCommon = new QMSAttachments();
                                    objFiles.FilePath = objAttachmentsCommon.SaveUploadedFileinPhysicalPath(file, "Initiator", 1);
                                    dbEF.AizantIT_Incident_InitiatorAttachments.Add(objFiles);
                                    int result = dbEF.SaveChanges();
                                    TempData["FileMsgCreate"] += Path.GetFileName(file.FileName) + " File uploaded successfully." + "<br/>";
                                    TempData["FileAddedCountCreate"] = Convert.ToInt32(TempData["FileAddedCountCreate"]) + 1;
                                    // }
                                }
                            }
                        }
                        else
                        {
                            TempData["FileMsgCreate"] += Path.GetFileName(file.FileName) + " upload failed as file already exists." + "<br/>";
                            TempData["FileexistsCountCreate"] = Convert.ToInt32(TempData["FileexistsCountCreate"]) + 1;
                        }
                    }
                    TempData.Keep("FileMsgCreate");
                    TempData.Keep("FileexistsCountCreate");
                    TempData.Keep("FileAddedCountCreate");
                    TempData.Keep("TempRefID");
                    var MsgType = "";
                    if (Convert.ToInt32(TempData["FileexistsCountCreate"]) > 0 && Convert.ToInt32(TempData["FileAddedCountCreate"]) == 0)
                    {
                        MsgType = "error";
                    }
                    if (Convert.ToInt32(TempData["FileAddedCountCreate"]) > 0 && Convert.ToInt32(TempData["FileexistsCountCreate"]) == 0)
                    {
                        MsgType = "success";
                    }
                    if (Convert.ToInt32(TempData["FileAddedCountCreate"]) > 0 && Convert.ToInt32(TempData["FileexistsCountCreate"]) > 0)
                    {
                        MsgType = "info";
                    }

                    return Json(new { hasError = false, msg = TempData["FileMsgCreate"], msgType = MsgType }, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M15:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, msg = ErMsg, msgType = "Exceptionerror" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        //Remove tempdata values
        [HttpGet]
        public JsonResult RemoveTempData1(int? RoleTypeTemp)
        {
            try
            {
                if (RoleTypeTemp == 1)// 1- Status to remove tempdata after file upload submit
                {
                    TempData.Remove("FileMsgCreate");
                    TempData.Remove("FileexistsCountCreate");
                    TempData.Remove("FileAddedCountCreate");
                }
                return Json(new { hasError = false }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { hasError = true, data = HelpClass.SQLEscapeString(ex.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        //Delete Incident Initiator File Upload Documents Details
        [HttpPost]
        public JsonResult DeleteIncidentInitiatorFileUploadDocuments(int? UniquePKID, int? IncidentMasterID, int EmpID)
        {
            try
            {
                using (AizantIT_DevEntities dbEF_DeleteIncident = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction _transaction = dbEF_DeleteIncident.Database.BeginTransaction())
                    {
                        try
                        {
                            //maintaining history when file deleted and here in place of empid sending UniquePKID for filename
                            //SendNotifications((int)IncidentMasterID, (int)UniquePKID, 9); // 9-inserting history for file deleting by initiator
                            dbEF_DeleteIncident.AizantIT_SP_IncidentNotifications(
                                IncidentMasterID,
                                UniquePKID,
                                9
                                );

                            var GetFiles = (from t1 in dbEF_DeleteIncident.AizantIT_Incident_InitiatorAttachments
                                            where t1.UniquePKID == UniquePKID
                                            select t1).SingleOrDefault();
                            var _ObjDetete = dbEF_DeleteIncident.AizantIT_Incident_InitiatorAttachments.Where(a => a.UniquePKID == UniquePKID).ToList();
                            //Removed file path in physical folder
                            QMSAttachments objAttachmentsCommon = new QMSAttachments();
                            objAttachmentsCommon.RemovePhysicalPathFile(Server.MapPath("~\\" + GetFiles.FilePath));
                            dbEF_DeleteIncident.AizantIT_Incident_InitiatorAttachments.Remove(_ObjDetete[0]);
                            dbEF_DeleteIncident.SaveChanges();
                            _transaction.Commit();
                            return Json(new { hasError = false, data = UniquePKID }, JsonRequestBehavior.AllowGet);
                        }
                        catch (Exception ex)
                        {
                            _transaction.Rollback();
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M27:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region HOD Actions
        //To Load CAPA's in preliminary Assessments 
        [HttpGet]
        public JsonResult BindCapa(int deptid)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    List<int?> capaHodApproveStatus = new List<int?> { 0, 1, 16, 11, 17, 18, 19, 20, 10 };
                    var Capalist = (from c in dbEF.AizantIT_CAPAMaster
                                    join Dv in dbEF.AizantIT_CAPA_ActionPlan on c.CAPAID equals Dv.CAPAID
                                    where !capaHodApproveStatus.Contains(Dv.CurrentStatus)
                                    && Dv.Capa_Action_Imp_DeptID == deptid && c.QualityEvent_TypeID == 1
                                    orderby c.CAPAID
                                    select new { c.CAPAID, c.CAPA_Number }).ToList();
                    List<SelectListItem> capalist = new List<SelectListItem>();
                    foreach (var item in Capalist)
                    {
                        if (item.CAPA_Number != "0")
                        {
                            capalist.Add(new SelectListItem
                            {
                                Text = item.CAPA_Number,
                                Value = item.CAPAID.ToString(),
                            });
                        }
                    }
                    return Json(capalist, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M6:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        //start commented on 09-01-2020
        //For getting Employee Role Exist or Not when click on Add Training
        public JsonResult ISTMSHOD()
        {

            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    Boolean IsRoleExists = QMSCommonActions.GetIsTMSHODRoleExist();

                return Json(IsRoleExists,JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M6:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        //end

        //Commented on 22-01-2020
        public JsonResult ISTMSHODDeptExist(int Inci_ID)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    string DepartmentName = "";
                    Boolean ISRoleExist = QMSCommonActions.GetIsTMSHODRoleExist();
                     Boolean IsDeptExists = QMSCommonActions.TMSHODAccesibleDeptExist(Inci_ID, ref DepartmentName);

                    return Json(new {hasError=false,data= DepartmentName,IsDeptExists, ISRoleExist }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M6:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        //End commented on 22-01-2020

        public JsonResult DeleteTTSTrainingWhenClickOnNo(int IncidentID)
        {

            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    int UserID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);

                    Qms_bal.DeleteTTSwhenNoBal(IncidentID, UserID);

                    return Json(new { hasError = false, data = "Success" }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M6:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        public JsonResult BindQualityEventNos(int EventTypeID, int DeptID, int IncidentMasterID)
        {
             //Event_Numbers;
            //var Event_Numbers;
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                List<SelectListItem> bindEventList = new List<SelectListItem>();
                    DataTable dt = Qms_bal.GetMapQualityNotifBal(EventTypeID, DeptID, IncidentMasterID);
                    
                        foreach (DataRow rdr in dt.Rows)
                        {
                            bindEventList.Add(new SelectListItem()
                            {
                                Text = rdr["EventNumber"].ToString(),
                                Value = rdr["EventID"].ToString()
                            });
                        }
               
                return Json(bindEventList, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M6:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }


        //To Load All Incidents numbers in dropdownlist
        [HttpGet]
        public JsonResult LoadIncidents(int deptid, int IncidentMaster_ID)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                   DataTable dt =Qms_bal.GetMapIncidentNumberBal(IncidentMaster_ID, deptid);
                    //Modified Current Status with Enums (cs=16)19-06-2019
                    //int[] array_Inci_CurrentStatus = { Convert.ToInt32(Incident_Status.Incd_with_Invest_Approved_by_HeadQA), Convert.ToInt32(Incident_Status.Verified_by_QA)};
                    //Not to show Rejected records and to show only the records which are approved by HOD
                    int[] array_Inci_CurrentStatus = { Convert.ToInt32(Incident_Status.Initiated_by_Initiator), Convert.ToInt32(Incident_Status.Modified_by_Initiator), Convert.ToInt32(Incident_Status.Reverted_by_HOD), Convert.ToInt32(Incident_Status.Incd_Reverted_to_Initiator_by_QA), Convert.ToInt32(Incident_Status.Rejected_by_HOD), Convert.ToInt32(Incident_Status.Rejected_by_QA), Convert.ToInt32(Incident_Status.Incd_with_Invest_Rejected_by_HeadQA), Convert.ToInt32(Incident_Status.Rejected_by_QA_with_Invest) }; //1,2,4,24,5,8,18,26
                    int[] array_Inci_RejectCurrentStatus = { Convert.ToInt32(Incident_Status.Rejected_by_HOD), Convert.ToInt32(Incident_Status.Rejected_by_QA), Convert.ToInt32(Incident_Status.Incd_with_Invest_Rejected_by_HeadQA), Convert.ToInt32(Incident_Status.Rejected_by_QA_with_Invest) }; //5,8,18,26}
                    //var GetIncidents = (from i in dbEF.AizantIT_IncidentMaster
                                        
                    //                    //where i.DeptID == deptid && i.CurrentStatus == (int)Incident_Status.Incd_with_Invest_Approved_by_HeadQA //Current Status = 16
                    //                    where i.DeptID == deptid//Current Status = 16
                    //                    && i.IncidentMasterID!= IncidentMaster_ID
                    //                    && !array_Inci_RejectCurrentStatus.Contains((int)i.CurrentStatus)
                    //                    //QEventNoIDs.Contains((int)ao.EventID)
                    //                    orderby i.IncidentMasterID
                    //                    select new { i.IncidentMasterID, i.IRNumber }).ToList();

                    List<SelectListItem> Incidntslist = new List<SelectListItem>();
                    foreach (DataRow item in dt.Rows)
                    {
                        Incidntslist.Add(new SelectListItem
                        {
                            Text = item["IRNumber"].ToString(),
                            Value = item["IncidentMasterId"].ToString(),//otherincidentID
                        });
                    }

                    //foreach (var item in GetIncidents)
                    //{
                    //    Incidntslist.Add(new SelectListItem
                    //    {
                    //        Text = item.IRNumber,
                    //        Value = item.IncidentMasterID.ToString(),//otherincidentID
                    //    });
                    //}
                    return Json(Incidntslist, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M8:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        //Commented on 31-12-2019
        [HttpPost]
        public JsonResult SavePreliminaryAssessment(AizantIT_IncidentPreAssmnt AizantIT_IncidentPreAssmntBO, List<int> QEventNoIDs)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    DataTable dtMapQultyNtf = new DataTable();
                    dtMapQultyNtf.Columns.Add("IncidentMasterID");
                    dtMapQultyNtf.Columns.Add("DeptID");
                    dtMapQultyNtf.Columns.Add("EventTypeID");
                    dtMapQultyNtf.Columns.Add("EventID");
                    dtMapQultyNtf.Columns.Add("OtherDescription");
                    //dtMapQultyNtf.Columns.Add("CapaID");
                    //dtMapCAPAlst.Columns.Add("CapaStatus");
                    dtMapQultyNtf.Columns.Add("isTemp");

                    DataRow dr;
                    if (AizantIT_IncidentPreAssmntBO.EventTypeID == 10)
                    {
                        dr = dtMapQultyNtf.NewRow();
                        dr["IncidentMasterID"] = AizantIT_IncidentPreAssmntBO.IncidentMasterID;
                        dr["DeptID"] = AizantIT_IncidentPreAssmntBO.DeptID;
                        dr["EventTypeID"] = AizantIT_IncidentPreAssmntBO.EventTypeID;
                        //dr["EventID"] = itemQNotfid;
                        dr["OtherDescription"] = AizantIT_IncidentPreAssmntBO.OtherDescription;
                        dr["isTemp"] = 0;

                        dtMapQultyNtf.Rows.Add(dr);
                    }
                    else
                    {
                        foreach (int itemQNotfid in QEventNoIDs)
                        {
                            //var GetCapaStatus = (from c in dbEF.AizantIT_CAPA_ActionPlan
                            //                     where c.CAPAID == itemCAPAid
                            //                     select new { c.CurrentStatus }).ToList();
                            //foreach (var item2 in GetCapaStatus)
                            //{
                            //    AizantIT_IncidentPreAssmntBO.CapaStatus = (int)item2.CurrentStatus;
                            //}
                            dr = dtMapQultyNtf.NewRow();
                            dr["IncidentMasterID"] = AizantIT_IncidentPreAssmntBO.IncidentMasterID;
                            dr["DeptID"] = AizantIT_IncidentPreAssmntBO.DeptID;
                            dr["EventTypeID"] = AizantIT_IncidentPreAssmntBO.EventTypeID;
                            dr["EventID"] = itemQNotfid;
                            dr["OtherDescription"] = AizantIT_IncidentPreAssmntBO.OtherDescription;
                            dr["isTemp"] = 0;

                            dtMapQultyNtf.Rows.Add(dr);
                        }
                    }
                   
                    //var data_CAPANumbers = "";
                    var data_QualtyNtfNumbers = "";
                    //List<String> data_QualtyNtfNumbers = new List<String>();
                    List<SelectListItem> data_QualtyNtfNumbersList = new List<SelectListItem>();
                   // var data_QualtyNtfNumbers;

                    if (AizantIT_IncidentPreAssmntBO.EventTypeID == 1)
                    {
                        data_QualtyNtfNumbersList = (from ao in dbEF.AizantIT_IncidentPreAssmnt
                                                     join im in dbEF.AizantIT_IncidentMaster on ao.EventID equals im.IncidentMasterID
                                                     where QEventNoIDs.Contains((int)ao.EventID) && ao.isTempDel != false
                                                     && ao.IncidentMasterID == AizantIT_IncidentPreAssmntBO.IncidentMasterID
                                                     && ao.EventTypeID == 1
                                                     select new SelectListItem { Value = ao.EventID.ToString(), Text = im.IRNumber }
                                         ).ToList();
                    }
                   else if (AizantIT_IncidentPreAssmntBO.EventTypeID == 2)
                    {
                        //For Change Control Event
                        data_QualtyNtfNumbersList = (from ao in dbEF.AizantIT_IncidentPreAssmnt
                                                     join cc in dbEF.AizantIT_ChangeControlMaster on ao.EventID equals cc.CCN_ID
                                                     where QEventNoIDs.Contains((int)ao.EventID) && ao.isTempDel != false
                                                     && ao.IncidentMasterID == AizantIT_IncidentPreAssmntBO.IncidentMasterID
                                                     && ao.EventTypeID == 2
                                                     select new SelectListItem { Value = ao.EventID.ToString(), Text = cc.CCN_No }
                                           ).ToList();
                    }

                    else if (AizantIT_IncidentPreAssmntBO.EventTypeID == 3)
                    {
                        //For Errata Event
                        data_QualtyNtfNumbersList = (from ao in dbEF.AizantIT_IncidentPreAssmnt
                                                     join er in dbEF.AizantIT_ErrataMaster on ao.EventID equals er.ER_ID
                                                     where QEventNoIDs.Contains((int)ao.EventID) && ao.isTempDel != false
                                                     && ao.IncidentMasterID == AizantIT_IncidentPreAssmntBO.IncidentMasterID
                                                     && ao.EventTypeID == 3
                                                     select new SelectListItem { Value = ao.EventID.ToString(), Text = er.ER_FileNumber }
                                                ).ToList();
                    }

                    else if (AizantIT_IncidentPreAssmntBO.EventTypeID == 4)
                    {
                        //For Note To File Event
                        data_QualtyNtfNumbersList = (from ao in dbEF.AizantIT_IncidentPreAssmnt
                                                     join nm in dbEF.AizantIT_NoteToFileMaster on ao.EventID equals nm.NTF_ID
                                                     where QEventNoIDs.Contains((int)ao.EventID) && ao.isTempDel != false
                                                     && ao.IncidentMasterID == AizantIT_IncidentPreAssmntBO.IncidentMasterID
                                                     && ao.EventTypeID == 4
                                                     select new SelectListItem { Value = ao.EventID.ToString(), Text = nm.NoteFile_Number }
                                               ).ToList();
                    }

                    
                    //commented on 02-01-2019
                    //When Selecting others Text must not be exist before.
                   if (AizantIT_IncidentPreAssmntBO.EventTypeID == 10)
                    {
                        data_QualtyNtfNumbersList = (from ao in dbEF.AizantIT_IncidentPreAssmnt
                                                     where ao.isTemp != true
                                                     && ao.IncidentMasterID == AizantIT_IncidentPreAssmntBO.IncidentMasterID
                                                     && ao.EventTypeID == 10 && ao.DeptID == AizantIT_IncidentPreAssmntBO.DeptID
                                                     && ao.OtherDescription == AizantIT_IncidentPreAssmntBO.OtherDescription
                                                     select new SelectListItem { Value = ao.EventID.ToString(), Text = ao.OtherDescription }
                                                     ).ToList();
                        if(data_QualtyNtfNumbersList.Count > 0)
                        {
                            return Json(new { hasError = true, data = "Description Already Exists." }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            int count = Qms_bal.AddMapQualityNotificationBal(dtMapQultyNtf);
                        }
                    }
                    else
                    {
                        foreach (var item in data_QualtyNtfNumbersList)
                        {
                            data_QualtyNtfNumbers += "," + item.Text;
                        }
                        if (data_QualtyNtfNumbersList.Count > 0)
                        {
                            return Json(new { hasError = true, data = data_QualtyNtfNumbers.TrimStart(',') + " Already Exists" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            int count = Qms_bal.AddMapQualityNotificationBal(dtMapQultyNtf);
                        }
                    }
                    return Json(new { hasError = false, data = AizantIT_IncidentPreAssmntBO }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M9:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new
                    {
                        hasError = true,
                        data = ErMsg
                    }, JsonRequestBehavior.AllowGet);

                }
            }
        }

        //To Save Any other investigation opened
        [HttpPost]
        public JsonResult SaveAnyotherinvestigationopened(AizantIT_IncidentPreAssmntOther AizantIT_IncidentPreAssmntOtherBO, List<int> OtherIncidentIDs)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    //int? IncdntID = AizantIT_IncidentPreAssmntOtherBO.IncidentMasterID;
                    //int Deptid = (int)AizantIT_IncidentPreAssmntOtherBO.DeptID;
                    //foreach (int item in OtherIncidentIDs)
                    //{
                    //    AizantIT_IncidentPreAssmntOtherBO.OtherIncidentID = item;
                    //    var Count = dbEF.AizantIT_IncidentPreAssmntOther.FirstOrDefault(p => p.DeptID == Deptid && p.OtherIncidentID == AizantIT_IncidentPreAssmntOtherBO.OtherIncidentID && p.IncidentMasterID == IncdntID);
                    //    if (Count != null)
                    //    {
                    //        return Json(new { hasError = true, data = "Already Exists" }, JsonRequestBehavior.AllowGet);
                    //    }
                    //    else
                    //    {
                    //        if (ModelState.IsValid)
                    //        {
                    //            dbEF.AizantIT_IncidentPreAssmntOther.Add(AizantIT_IncidentPreAssmntOtherBO);
                    //            dbEF.SaveChanges();
                    //        }
                    //    }
                    //}

                    DataTable dtMapIncident_lst = new DataTable();
                    dtMapIncident_lst.Columns.Add("IncidentMasterID");
                    dtMapIncident_lst.Columns.Add("DeptID");
                    dtMapIncident_lst.Columns.Add("OtherIncidentID");
                    dtMapIncident_lst.Columns.Add("isTemp");

                    DataRow dr;

                    foreach (var itemIncidentID in OtherIncidentIDs)
                    {
                        dr = dtMapIncident_lst.NewRow();

                        dr["IncidentMasterID"] = AizantIT_IncidentPreAssmntOtherBO.IncidentMasterID;
                        dr["DeptID"] = (int)AizantIT_IncidentPreAssmntOtherBO.DeptID;
                        dr["OtherIncidentID"] = itemIncidentID;
                        dr["isTemp"] = 0;

                        dtMapIncident_lst.Rows.Add(dr);
                    }

                    var data_IncidentNumbers = "";
                    var INCI_NumbersList = (from ao in dbEF.AizantIT_IncidentPreAssmntOther
                                            join im in dbEF.AizantIT_IncidentMaster on ao.OtherIncidentID equals im.IncidentMasterID
                                            where ao.DeptID == (int)AizantIT_IncidentPreAssmntOtherBO.DeptID
                                            && OtherIncidentIDs.Contains((int)ao.OtherIncidentID) && ao.isTempDel!=false
                                            && ao.IncidentMasterID == AizantIT_IncidentPreAssmntOtherBO.IncidentMasterID
                                            select new { im.IRNumber }
                                          ).ToList();
                    foreach (var item1 in INCI_NumbersList)
                    {
                        data_IncidentNumbers += "," + item1.IRNumber;
                    }
                    if (INCI_NumbersList.Count > 0)
                    {
                        return Json(new { hasError = true, data = data_IncidentNumbers.TrimStart(',') + " Already Exists" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        int count = Qms_bal.AddMapINCI_InsertBal(dtMapIncident_lst);
                    }
                    return Json(new { hasError = false, data = AizantIT_IncidentPreAssmntOtherBO }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M10:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        //Delete IncidentPreAssmnt Details
        [HttpPost]
        public JsonResult DeleteIncidentPreAssmnt(int? UniquePKID)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    AizantIT_IncidentPreAssmnt ai = dbEF.AizantIT_IncidentPreAssmnt.First(a => a.UniquePKID == UniquePKID);
                    ai.isTempDel = false;
                    dbEF.SaveChanges();
                    return Json(new { hasError = false, data = UniquePKID }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M11:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        //Delete Any Other Inventigation Detailss
        [HttpPost]
        public JsonResult DeleteAnyOtherInventigation(int? UniquePKIDOther)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    //var _OtherLst = dbEF.AizantIT_IncidentPreAssmntOther.Where(a => a.UniquePKID == UniquePKIDOther).ToList();
                    //dbEF.AizantIT_IncidentPreAssmntOther.Remove(_OtherLst[0]);
                    //dbEF.SaveChanges();
                    //return Json(new { hasError = false, data = UniquePKIDOther }, JsonRequestBehavior.AllowGet);
                    

                    AizantIT_IncidentPreAssmntOther aipre_assm_oth = dbEF.AizantIT_IncidentPreAssmntOther.First(a => a.UniquePKID == UniquePKIDOther);
                    aipre_assm_oth.isTempDel = false;
                    dbEF.SaveChanges();
                    return Json(new { hasError = false, data = UniquePKIDOther }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M12:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = HelpClass.SQLEscapeString(ex.Message) }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        //To Check Count of Preassessments records
        [HttpPost]
        public JsonResult PreassessmentsCountCheckingDB(IncidentBO ObjincidentBO)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    ObjincidentBO.CountofPreAssmnt = (from pa in dbEF.AizantIT_IncidentPreAssmnt
                                                      where pa.IncidentMasterID == ObjincidentBO.IncidentMasterID && pa.isTempDel!=false
                                                      select pa).Count();
                    ObjincidentBO.CountAnyotherInvtign = (from a in dbEF.AizantIT_IncidentPreAssmntOther
                                                          where a.IncidentMasterID == ObjincidentBO.IncidentMasterID && a.isTempDel!=false
                                                          select a).Count();
                    return Json(new { hasError = false, data = ObjincidentBO }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M16:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        //Hod Approve Action
        [HttpPost]
        public JsonResult IncidentHodApprove(IncidentBO objincidentBO)
        {
            try
            {
                Boolean Result = ToCheckEmpValidityIncident(objincidentBO.IncidentMasterID, 25);
                using (AizantIT_DevEntities dbEF_HodApprove = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction _transaction = dbEF_HodApprove.Database.BeginTransaction())
                    {
                        try
                        {
                            // Boolean Result = ToCheckEmpValidityIncident(objincidentBO.IncidentMasterID, 25);
                            if (Result == true)
                            {
                                int DBIncidentID = 0, DBOtherIncidentID = 0;
                                int? IncdntID = objincidentBO.IncidentMasterID;
                                var objIncdtUpdate = dbEF_HodApprove.AizantIT_IncidentMaster.Where(x => x.IncidentMasterID == IncdntID).FirstOrDefault();
                                if (objIncdtUpdate != null)
                                {
                                    objIncdtUpdate.IsFirstOccurence = Convert.ToInt32(objincidentBO.IsFirstOccurence);
                                    objIncdtUpdate.FO_Comments = objincidentBO.FO_Comments == null ? "" : objincidentBO.FO_Comments.Trim();
                                    objIncdtUpdate.AnyOtherInvestigations = Convert.ToInt32(objincidentBO.AnyOtherInvestigations);
                                    objIncdtUpdate.AOI_Comments = objincidentBO.AOI_Comments == null ? "" : objincidentBO.AOI_Comments.Trim();
                                    objIncdtUpdate.QAEmpID = objincidentBO.QAEmpID;
                                    objIncdtUpdate.InvestigatorEmpID = objincidentBO.InvestigatorEmpID;
                                    //Adding HOD_InvestigationNotes
                                    objIncdtUpdate.HOD_InvestigationNotes = objincidentBO.HOD_InvestigationNotes;
                                    EmployeID(objincidentBO);
                                    objIncdtUpdate.HODEmpID = objincidentBO.EmpID;

                                    if (objincidentBO.CurrentStatus == Convert.ToInt32(Incident_Status.Reverted_to_HOD_by_QA).ToString()) // QA Revert  //Modified Current Status with Enums (cs="7")19-06-2019
                                    {
                                        objIncdtUpdate.CurrentStatus = (int)Incident_Status.Incd_Modified_by_HOD;//7 - Hod Modified  //Modified Current Status with Enums (cs=17)19-06-2019
                                    }
                                    else
                                    {
                                        objIncdtUpdate.CurrentStatus = (int)Incident_Status.Approved_by_HOD;//3 - Hod Approved   //Modified Current Status with Enums (cs=3)19-06-2019
                                    }

                                    dbEF_HodApprove.Entry(objIncdtUpdate).State = EntityState.Modified;

                                    if (objIncdtUpdate.IsFirstOccurence == 1 || objIncdtUpdate.IsFirstOccurence == 3)//If Map CAPA is YES
                                    {
                                        var _OtherLst = dbEF_HodApprove.AizantIT_IncidentPreAssmntOther.Where(a => a.IncidentMasterID == IncdntID).ToList();
                                        dbEF_HodApprove.AizantIT_IncidentPreAssmntOther.RemoveRange(_OtherLst);

                                        //var _preAssLst = dbEF_HodApprove.AizantIT_IncidentPreAssmnt.Where(a => a.IncidentMasterID == IncdntID).ToList();
                                        //dbEF_HodApprove.AizantIT_IncidentPreAssmnt.RemoveRange(_preAssLst);
                                    }
                                    else
                                    {
                                        //For Deleting Map Incident Records where isTempDel is false
                                        var aip_preassmt_remove = dbEF_HodApprove.AizantIT_IncidentPreAssmntOther.Where(a => a.isTempDel == false && a.IncidentMasterID == IncdntID);
                                        dbEF_HodApprove.AizantIT_IncidentPreAssmntOther.RemoveRange(aip_preassmt_remove);

                                        //For Deleting Map CAPA Records where isTempDel is false
                                        //var aip = dbEF_HodApprove.AizantIT_IncidentPreAssmnt.Where(a => a.isTempDel == false && a.IncidentMasterID == IncdntID);
                                        //dbEF_HodApprove.AizantIT_IncidentPreAssmnt.RemoveRange(aip);
                                    }
                                    if (objIncdtUpdate.AnyOtherInvestigations == 2 || objIncdtUpdate.AnyOtherInvestigations == 3)//If Map CAPA is NO
                                    {
                                        var _preAssLst = dbEF_HodApprove.AizantIT_IncidentPreAssmnt.Where(a => a.IncidentMasterID == IncdntID).ToList();
                                        dbEF_HodApprove.AizantIT_IncidentPreAssmnt.RemoveRange(_preAssLst);

                                        //var _OtherLst = dbEF_HodApprove.AizantIT_IncidentPreAssmntOther.Where(a => a.IncidentMasterID == IncdntID).ToList();
                                        //dbEF_HodApprove.AizantIT_IncidentPreAssmntOther.RemoveRange(_OtherLst);
                                    }
                                   else
                                    {
                                        var aip = dbEF_HodApprove.AizantIT_IncidentPreAssmnt.Where(a => a.isTempDel == false && a.IncidentMasterID == IncdntID);
                                        dbEF_HodApprove.AizantIT_IncidentPreAssmnt.RemoveRange(aip);

                                        ////For Deleting Map Incident Records where isTempDel is false
                                        //var aip_preassmt_remove = dbEF_HodApprove.AizantIT_IncidentPreAssmntOther.Where(a => a.isTempDel == false && a.IncidentMasterID == IncdntID);
                                        //dbEF_HodApprove.AizantIT_IncidentPreAssmntOther.RemoveRange(aip_preassmt_remove);
                                    }
                                   
                                    int Action = 0;
                                    if (objincidentBO.CurrentStatus == Convert.ToInt32(Incident_Status.Reverted_to_HOD_by_QA).ToString())  //Modified Current Status with Enums (cs="7")19-06-2019
                                    {
                                        Action = 3;  //3 - HOD Approve with modify
                                    }
                                    else
                                    {
                                        Action = 1; // 1-Hod Approve
                                    }

                                    //here to send notification and also updating isTemp as false in AizantIT_IncidentPreAssmnt,,AizantIT_IncidentPreAssmntOther

                                    //SendNotifications(Convert.ToInt32(IncdntID), Convert.ToInt32(objIncdtUpdate.QAEmpID), Action);
                                    //end

                                    dbEF_HodApprove.AizantIT_SP_IncidentNotifications(
                                    IncdntID,
                                    objIncdtUpdate.QAEmpID,
                                    Action
                                    );

                                    //Insert into History table 
                                    AizantIT_IncidentHistory objAizantIT_IncidentHistoryBO = new AizantIT_IncidentHistory();
                                    
                                    if (objincidentBO.CurrentStatus == Convert.ToInt32(Incident_Status.Reverted_to_HOD_by_QA).ToString()) // QA Revert  //Modified Current Status with Enums (cs="7")19-06-2019
                                    {
                                        objAizantIT_IncidentHistoryBO.ActionStatus = 17; //17 - Hod Modified
                                    }
                                    else
                                    {
                                        objAizantIT_IncidentHistoryBO.ActionStatus = 3; //3 - Hod Approved
                                    }
                                    if (ModelState.IsValid)

                                    {
                                        dbEF_HodApprove.AizantIT_SP_InsertIncidentHistory(
                                            objincidentBO.IncidentMasterID,
                                            objincidentBO.EmpID,25,
                                            objAizantIT_IncidentHistoryBO.ActionStatus, 
                                            objincidentBO.HODComments.Trim(), 
                                            objIncdtUpdate.QAEmpID,
                                            26
                                            );
                                        dbEF_HodApprove.SaveChanges();
                                        _transaction.Commit();
                                        return Json(new { hasError = false, data = objAizantIT_IncidentHistoryBO }, JsonRequestBehavior.AllowGet);
                                    }
                                    else
                                    {
                                        _transaction.Rollback();
                                    }
                                }
                                return Json(new { hasError = false, data = objincidentBO }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { hasError = true, data = "You are not authorized User to do action." }, JsonRequestBehavior.AllowGet);
                            }

                        }
                        catch (Exception ex)
                        {
                            _transaction.Rollback();
                            //throw ex;
                            int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                            string ErMsg = "INCI_M13:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                            Aizant_log.Error(ErMsg);
                            return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M13:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult SubmitHodReviewComments(AizantIT_Incident_InvestigationReviewDeptHods objBO, int EmpID)
        {
            try
            {
                Boolean HODReviewResult = AssessmentEmpValid((int)objBO.IncidentMasterID, objBO.UniquePKID, 25);
                using (AizantIT_DevEntities dbEF_SubmitHodReview = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction _transaction = dbEF_SubmitHodReview.Database.BeginTransaction())
                    {
                        try
                        {
                            if (HODReviewResult == true)
                            {
                                var actionPlanHODID = dbEF_SubmitHodReview.AizantIT_IncidentMaster.Where(t1 => t1.IncidentMasterID == objBO.IncidentMasterID).Select(x=>x.HODEmpID).SingleOrDefault();
                                var objUpdate = dbEF_SubmitHodReview.AizantIT_Incident_InvestigationReviewDeptHods.Where(x => x.UniquePKID == objBO.UniquePKID).FirstOrDefault();
                                if (objUpdate != null)
                                {
                                    objUpdate.HODComments = objBO.HODComments.Trim();
                                    objUpdate.SubmittedDate = DateTime.Now;
                                    objUpdate.ApproveStatus = objBO.ApproveStatus;
                                    if (ModelState.IsValid)
                                    {
                                        dbEF_SubmitHodReview.Entry(objUpdate).State = EntityState.Modified;
                                        dbEF_SubmitHodReview.SaveChanges();
                                        //Reveiew HOD Notification Close
                                        //SendNotifications((int)objBO.IncidentMasterID, EmpID, 6);

                                        dbEF_SubmitHodReview.AizantIT_SP_IncidentNotifications(
                                        objBO.IncidentMasterID,
                                        EmpID,
                                        6
                                        );

                                        //update Master Status when All Review Hod's ApproveStatus=2
                                        var GetAllApproveStatus = (from h in dbEF_SubmitHodReview.AizantIT_Incident_InvestigationReviewDeptHods
                                                                   where h.IncidentMasterID == objBO.IncidentMasterID
                                                                   select new { h.ApproveStatus }).ToList();
                                        int Result = 0;
                                        foreach (var item in GetAllApproveStatus)
                                        {
                                            if (item.ApproveStatus == 1)
                                            {
                                                Result++;
                                            }
                                        }
                                        if (Result == 0)
                                        {
                                            //sending Notification for Transaction hod for Action plan Creation
                                            //SendNotifications((int)objBO.IncidentMasterID, 0, 8);

                                            dbEF_SubmitHodReview.AizantIT_SP_IncidentNotifications(
                                            objBO.IncidentMasterID,
                                            0,
                                            8);

                                            var objUpdateStatus = dbEF_SubmitHodReview.AizantIT_IncidentMaster.Where(x => x.IncidentMasterID == objBO.IncidentMasterID).FirstOrDefault();
                                            if (objUpdateStatus != null)
                                            {
                                                objUpdateStatus.CurrentStatus = (int)Incident_Status.At_ActionPlan;  //Modified Current Status with Enums (cs=21)19-06-2019
                                                if (ModelState.IsValid)
                                                {
                                                    dbEF_SubmitHodReview.Entry(objUpdateStatus).State = EntityState.Modified;
                                                    dbEF_SubmitHodReview.SaveChanges();
                                                }
                                            }
                                        }

                                        //Insert into History table 
                                        AizantIT_IncidentHistory objAizantIT_IncidentHistoryBO = new AizantIT_IncidentHistory();
                                        if(Result == 0)
                                        {
                                            objAizantIT_IncidentHistoryBO.AssignedTo = Convert.ToInt32(actionPlanHODID); ;
                                        }
                                        else
                                        {
                                            objAizantIT_IncidentHistoryBO.AssignedTo = 0;
                                        }
                                        //end
                                        if (ModelState.IsValid)
                                        {
                                            dbEF_SubmitHodReview.AizantIT_SP_InsertIncidentHistory(
                                                objBO.IncidentMasterID,
                                                EmpID, 25, 23, objBO.HODComments.Trim(), objAizantIT_IncidentHistoryBO.AssignedTo, 25
                                                );
                                            dbEF_SubmitHodReview.SaveChanges();
                                            _transaction.Commit();
                                            return Json(new { hasError = false, data = objAizantIT_IncidentHistoryBO }, JsonRequestBehavior.AllowGet);
                                        }
                                    }
                                }
                                return Json(new { hasError = false, data = objBO }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { hasError = true, data = "You are not authorized User to do action." }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {

                            _transaction.Rollback();
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M30:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Incident Revert and Reject
        //Incident Revert And Reject
        [HttpPost]
        public JsonResult IncidentRevertAndReject(IncidentBO objincidentBO, int Action_No, int WhomToRevertID)
        {
            try
            {
                int RoleID = 0;
                if (Action_No == 1 || Action_No == 9 || Action_No == 3)
                {
                    RoleID = 25;
                }
                if (Action_No == 2 || Action_No == 5 || Action_No == 4 || Action_No == 6)
                {
                    RoleID = 26;
                }
                if (Action_No == 7 || Action_No == 8)
                {
                    RoleID = 27;
                }
                
                Boolean Result = ToCheckEmpValidityIncident(objincidentBO.IncidentMasterID, RoleID);
                if (Result == true)
                {
                    int i = Qms_bal.IncidentRevertAndRejectBal(objincidentBO, Action_No, WhomToRevertID);
                    return Json(new { hasError = false, data = objincidentBO, Action_No }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { hasError = true, data = "You are not authorized User to do action." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M14:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Investigation
        //Insert Incident Investigation Details
        [HttpPost]
        public JsonResult IncidentInvestigationSubmit(IncidentBO objincidentBO, List<IncidentImpactPoints> Impacts, List<IncidentEffectedPoints> HasAffectedlst, List<IncidentWasEffectedPoints> WasAffectedlst, List<IncidentReviewPoints> Reviewpointslst)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    Boolean ResultBool = ToCheckEmpValidityIncident(objincidentBO.IncidentMasterID, 33);
                    if (ResultBool == true)
                    {

                        //part-2 Impact Point Insert
                        DataTable dtImpactlst = new DataTable();
                        dtImpactlst.Columns.Add("IncidentMasterID");
                        dtImpactlst.Columns.Add("isyesorNo");
                        dtImpactlst.Columns.Add("ImpactPointID");
                        dtImpactlst.Columns.Add("DocNo");
                        dtImpactlst.Columns.Add("Comments");
                        DataRow dr;
                        if (Impacts != null)
                        {
                            foreach (IncidentImpactPoints item in Impacts)
                            {
                                dr = dtImpactlst.NewRow();
                                dr["IncidentMasterID"] = item.IncidentMasterID;
                                dr["isyesorNo"] = item.isyesorNo;
                                dr["ImpactPointID"] = item.ImpactPointID;
                                dr["DocNo"] = item.DocNo;
                                dr["Comments"] = item.Comments;
                                dtImpactlst.Rows.Add(dr);
                            }
                        }
                        //part-3 Has Effected
                        DataTable dtHasAffectedlst = new DataTable();
                        dtHasAffectedlst.Columns.Add("IncidentMasterID");
                        dtHasAffectedlst.Columns.Add("isEffect");
                        dtHasAffectedlst.Columns.Add("EffectedID");
                        dtHasAffectedlst.Columns.Add("EffectedTypeID");
                        dtHasAffectedlst.Columns.Add("ReferenceNo");
                        dtHasAffectedlst.Columns.Add("Comments");
                        DataRow drHasAff;
                        if (HasAffectedlst != null)
                        {
                            foreach (IncidentEffectedPoints item in HasAffectedlst)
                            {
                                drHasAff = dtHasAffectedlst.NewRow();
                                drHasAff["IncidentMasterID"] = item.IncidentMasterID;
                                drHasAff["isEffect"] = item.isEffect;
                                drHasAff["EffectedID"] = item.EffectedID;
                                drHasAff["EffectedTypeID"] = 1; // HasAffected
                                drHasAff["ReferenceNo"] = item.ReferenceNo;
                                drHasAff["Comments"] = item.Comments;
                                dtHasAffectedlst.Rows.Add(drHasAff);
                            }
                        }
                        // Was Effected
                        DataTable dtWasAffectedlst = new DataTable();
                        dtWasAffectedlst.Columns.Add("IncidentMasterID");
                        dtWasAffectedlst.Columns.Add("isEffect");
                        dtWasAffectedlst.Columns.Add("EffectedID");
                        dtWasAffectedlst.Columns.Add("EffectedTypeID");
                        dtWasAffectedlst.Columns.Add("ReferenceNo");
                        dtWasAffectedlst.Columns.Add("Comments");
                        DataRow drWasAff;
                        if (WasAffectedlst != null)
                        {
                            foreach (IncidentWasEffectedPoints item in WasAffectedlst)
                            {
                                drWasAff = dtWasAffectedlst.NewRow();
                                drWasAff["IncidentMasterID"] = item.IncidentMasterID;
                                drWasAff["isEffect"] = item.isEffect;
                                drWasAff["EffectedID"] = item.EffectedID;
                                drWasAff["EffectedTypeID"] = 2; // WasAffected
                                drWasAff["ReferenceNo"] = item.ReferenceNo;
                                drWasAff["Comments"] = item.Comments;
                                dtWasAffectedlst.Rows.Add(drWasAff);
                            }
                        }

                        //part-5 Review Points
                        DataTable dtReviewlst = new DataTable();
                        dtReviewlst.Columns.Add("IncidentMasterID");
                        dtReviewlst.Columns.Add("isReview");
                        dtReviewlst.Columns.Add("ReviewID");
                        dtReviewlst.Columns.Add("ReferenceNo");
                        dtReviewlst.Columns.Add("Comments");
                        DataRow drReview;
                        if (Reviewpointslst != null)
                        {
                            foreach (IncidentReviewPoints item in Reviewpointslst)
                            {
                                drReview = dtReviewlst.NewRow();
                                drReview["IncidentMasterID"] = item.IncidentMasterID;
                                drReview["isReview"] = item.isReview;
                                drReview["ReviewID"] = item.ReviewID;
                                drReview["ReferenceNo"] = item.ReferenceNo;
                                drReview["Comments"] = item.Comments;
                                dtReviewlst.Rows.Add(drReview);
                            }
                        }
                        //If there is no Hod's for Review the investigation update status to (21)
                        var GetDBExitsCount = (from h in dbEF.AizantIT_Incident_InvestigationReviewDeptHods
                                               where h.IncidentMasterID == objincidentBO.IncidentMasterID
                                               select h).Count();
                        if (GetDBExitsCount == 0)
                        {
                            //Modified Current Status with Enums (cs="21")19-06-2019
                            objincidentBO.CurrentStatus = ((int)Incident_Status.At_ActionPlan).ToString();//At Action Plan 21
                        }
                        else
                        {
                            //Modified Current Status with Enums (cs="9")19-06-2019
                            objincidentBO.CurrentStatus = ((int)Incident_Status.Completed_Invest_by_Investigator).ToString();//at Other Dept Hod Review 9
                        }
                        if (objincidentBO.InvestigationOperation == "Submitted" || objincidentBO.InvestigationOperation == "Save" || objincidentBO.InvestigationOperation == "SavedDetailsUpdate")
                        {
                            int i = Qms_bal.InsertInvestigationDetailsBal(objincidentBO, dtImpactlst, dtHasAffectedlst, dtWasAffectedlst, dtReviewlst);
                        }
                        else if (objincidentBO.InvestigationOperation == "Updated")
                        {
                            //update Master Status when All Review Hod's ApproveStatus=2
                            var GetAllApproveStatus = (from h in dbEF.AizantIT_Incident_InvestigationReviewDeptHods
                                                       where h.IncidentMasterID == objincidentBO.IncidentMasterID
                                                       select new { h.ApproveStatus }).ToList();
                            int Result = 0;
                            foreach (var item in GetAllApproveStatus)
                            {
                                if (item.ApproveStatus == 1)
                                {
                                    Result++;
                                }
                            }
                            if (Result == 0)
                            {
                                //Modified Current Status with Enums (cs="10")19-06-2019
                                objincidentBO.CurrentStatus = ((int)Incident_Status.Updated_Invest_by_Investigator).ToString(); //Updated by Investigator 10
                            }
                            else
                            {
                                //Modified Current Status with Enums (cs="9")19-06-2019
                                objincidentBO.CurrentStatus = ((int)Incident_Status.Completed_Invest_by_Investigator).ToString();//at Other Dept Hod Review 9
                            }
                            int i = Qms_bal.InsertInvestigationDetailsBal(objincidentBO, dtImpactlst, dtHasAffectedlst, dtWasAffectedlst, dtReviewlst);
                        }
                        return Json(new { hasError = false, data = objincidentBO }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { hasError = true, data = "You are not authorized User to do action." }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M17:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        //To Get All Employee Used in InvestigationPV
        [HttpGet]
        public JsonResult GetAllEmployees(int IncidentMasterID, int deptid)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    DataTable dataTable = Qms_bal.GetAllEmployeesBal(IncidentMasterID, deptid);
                    //var GetAllEmps = (from e in dbEF.AizantIT_EmpMaster
                    //                  join d in dbEF.AizantIT_DepartmentMaster on e.DefaultDeptID equals d.DeptID
                    //                  where d.DeptID == deptid
                    //                  select new { e.EmpID, EmpName = e.FirstName + " " + e.LastName }).Distinct().ToList();

                    List<SelectListItem> AllEmpsID = new List<SelectListItem>();
                    ////AllEmpsID.Add(new SelectListItem { Text = "select ", Value = "0" });
                    foreach (DataRow q1 in dataTable.Rows)
                    {
                        AllEmpsID.Add(new SelectListItem { Text = q1["EmpName"].ToString(), Value = q1["EmpID"].ToString() });
                    }
                    //foreach (var q1 in GetAllEmps)
                    //{
                    //    AllEmpsID.Add(new SelectListItem { Text = q1.EmpName, Value = q1.EmpID.ToString() });
                    //}
                    return Json(AllEmpsID, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M18:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        //To Get All Hods For Send for Investigatin Review for other hod's
        public ActionResult GetAllHodEmployees(int deptid, int IncidentMasterID)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    //Accessable dept based emp
                    IncidentBO incdntBO = new IncidentBO();
                    EmployeID(incdntBO);
                    int? HODID = 0; int? IRInitiator = 0; int? IRQA = 0; int? IRInvestigatorEmpID = 0;
                    var GetTHOD = (from h in dbEF.AizantIT_IncidentMaster
                                   where h.IncidentMasterID == IncidentMasterID
                                   select new { h.HODEmpID }).Distinct().ToList();
                    foreach (var item in GetTHOD)
                    {
                        HODID = item.HODEmpID;
                    }
                    var GetIRInitiator = (from h in dbEF.AizantIT_IncidentMaster
                                          where h.IncidentMasterID == IncidentMasterID
                                          select new { h.InitiatorEmpID }).Distinct().ToList();
                    foreach (var item2 in GetIRInitiator)
                    {
                        IRInitiator = item2.InitiatorEmpID;
                    }
                    var GetIRQA = (from h in dbEF.AizantIT_IncidentMaster
                                   where h.IncidentMasterID == IncidentMasterID
                                   select new { h.QAEmpID }).Distinct().ToList();
                    foreach (var item3 in GetIRQA)
                    {
                        IRQA = item3.QAEmpID;
                    }
                    var GetIRInvestigatorEmpID = (from h in dbEF.AizantIT_IncidentMaster
                                                  where h.IncidentMasterID == IncidentMasterID
                                                  select new { h.InvestigatorEmpID }).Distinct().ToList();
                    foreach (var item4 in GetIRInvestigatorEmpID)
                    {
                        IRInvestigatorEmpID = item4.InvestigatorEmpID;
                    }
                    objUMS_BAL = new UMS_BAL();
                    List<SelectListItem> AllHodEmpsID = new List<SelectListItem>();
                    DataTable dtemp = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(deptid, (int)QMS_Roles.HOD, true); //25=hod
                    foreach (DataRow RPItem in dtemp.Rows)
                    {
                        //if (HODID != Convert.ToInt32(RPItem["EmpID"]) && IRInitiator != Convert.ToInt32(RPItem["EmpID"]) && IRQA != Convert.ToInt32(RPItem["EmpID"]) && IRInvestigatorEmpID != Convert.ToInt32(RPItem["EmpID"]))
                        if (IRInvestigatorEmpID != Convert.ToInt32(RPItem["EmpID"]))
                        {
                            AllHodEmpsID.Add(new SelectListItem()
                            {
                                Text = (RPItem["EmpName"]).ToString(),
                                Value = (RPItem["EmpID"]).ToString()
                            });
                        }
                    }
                    return Json(AllHodEmpsID, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M19:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        // To Save Who are the people involved
        [HttpPost]
        public JsonResult AddWhoarethepeopleinvolved(AizantIT_Incident_WhoAreThePeolpleInvolved objBO, List<int> EmpIDs)//dropdown multile select.
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    int? IncdntID = objBO.IncidentMasterID;
                    int Deptid = (int)objBO.DeptID;
                    foreach (int item in EmpIDs)
                    {
                        objBO.EmpID = item;
                        var Count = dbEF.AizantIT_Incident_WhoAreThePeolpleInvolved.FirstOrDefault(p => p.DeptID == Deptid && p.EmpID == objBO.EmpID && p.IncidentMasterID == IncdntID);
                        if (Count != null)
                        {
                            return Json(new { hasError = true, data = "Already Exists." }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            if (ModelState.IsValid)
                            {
                                dbEF.AizantIT_Incident_WhoAreThePeolpleInvolved.Add(objBO);
                                dbEF.SaveChanges();
                            }
                        }
                    }
                    return Json(new { hasError = false, data = objBO }, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M20:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        // To Delete Who are the people involved
        [HttpPost]
        public JsonResult DeleteWhoarethepeopleinvolved(int? UniquePKID)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    var _ObjDeleteByID = dbEF.AizantIT_Incident_WhoAreThePeolpleInvolved.Where(a => a.UniquePKID == UniquePKID).ToList();
                    dbEF.AizantIT_Incident_WhoAreThePeolpleInvolved.Remove(_ObjDeleteByID[0]);
                    dbEF.SaveChanges();
                    return Json(new { hasError = false, data = UniquePKID }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M21:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public PartialViewResult InvestigatorImpactPV(int ID)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    ViewBag.GetImpactExitsCount = (from Q in dbEF.AizantIT_Incident_QualityImpacting
                                                   where Q.IncidentMasterID == ID
                                                   select Q).Count();
                    //to bind impact masters for part-2
                    List<IncidentImpactPoints> impctlst = new List<IncidentImpactPoints>();
                    //Modified Current Status with Enums (cs="6")19-06-2019 
                    if (TempData["CurrentStatus"].ToString() == Convert.ToInt32(Incident_Status.Approved_by_QA).ToString() && Convert.ToInt32(ViewBag.GetImpactExitsCount) == 0) //Current Status= 6
                    {
                        var impact = (from imp in dbEF.AizantIT_Incident_ImpactMaster
                                      select new
                                      {
                                          imp.ImpactName,
                                          imp.ImpactID
                                      }).ToList();
                        foreach (var dr in impact)
                        {
                            impctlst.Add(new IncidentImpactPoints
                            {
                                ImpactName = dr.ImpactName.ToString(),
                                ImpactPointID = Convert.ToInt32(dr.ImpactID),
                            });
                        }
                        ViewBag.Impact = impctlst.AsEnumerable();
                    }
                    else
                    {
                        //start
                        //var CStatus = "N/A";
                        //if(TempData["ShowType"].ToString() != "12" && TempData["ShowType"].ToString() != "13")
                        //{
                        //    if(TempData["CurrentStatus"].ToString() == "11")
                        //    {
                        //        CStatus = "";
                        //    }
                        //}
                        //end

                        //Binding Data From DB for Update or Show.
                        var GetImpactPointsdata = (from i in dbEF.AizantIT_Incident_ImpactofIncident
                                                   join mst in dbEF.AizantIT_Incident_ImpactMaster on i.ImpactID equals mst.ImpactID
                                                   where i.IncidentMasterID == ID
                                                   select new { i.UniquePKID, i.IncidentMasterID, i.IsImpact, i.ImpactID, i.ReferenceNo, i.ImpactComments, mst.ImpactName }).ToList();
                        foreach (var dr in GetImpactPointsdata)
                        {
                            impctlst.Add(new IncidentImpactPoints
                            {
                                ImpactName = dr.ImpactName.ToString(),
                                ImpactPointID = Convert.ToInt32(dr.ImpactID.ToString()),
                                DocNo = dr.ReferenceNo != null ? dr.ReferenceNo.ToString() : "N/A",
                                Comments = dr.ImpactComments != null ? dr.ImpactComments.ToString() : "N/A",
                                isyesorNo = Convert.ToInt32(dr.IsImpact).ToString(),
                                _lstImpactStatus = new SelectList(dbEF.AizantIT_Incident_EffectStatusMaster.ToList(), "EffectStatusID", "EffectStatusName", Convert.ToInt32(dr.IsImpact).ToString())
                            }
                            );
                        }
                        ViewBag.Impact = impctlst.AsEnumerable();
                    }
                    return PartialView();
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M31:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    ViewBag.Exception = ErMsg;
                    return PartialView("ExecptionError", ViewBag.Exception);
                }
            }
        }

        //To Save and Update Incident Has Affected Documents
        [HttpPost]
        public JsonResult SaveIncidentHasAffectedDoc(string Operation, IncidentBO incidentBO)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    int DocRefno = 0;
                    int? IncdntID = incidentBO.IncidentMasterID;
                    int _DocumentID = incidentBO.HasAffectedDocumentID;
                    string _AffectedComments = incidentBO.AffectedComments;
                    var alreadyExists=0;
                    if (incidentBO.HasAffectedDocumentID != 0)
                    {
                        DocRefno = Convert.ToInt32(incidentBO.HasAffOtherDOCRefNo);
                     var Count = dbEF.AizantIT_Incident_HasAffectedDocument.FirstOrDefault(h => h.AffectedDocumentID == _DocumentID && h.HasDocRefNo == DocRefno && h.PageNo == incidentBO.HasAffectedPageNo && h.IncidentMasterID == IncdntID );
                        if (Count != null)
                        {
                            alreadyExists = 1;
                        }

                    }
                    else
                    {
                        var Count = dbEF.AizantIT_Incident_HasAffectedDocument.FirstOrDefault(h => h.AffectedDocumentID == _DocumentID && h.HasOtherDocName ==incidentBO.HasAffDocTitle && h.HasOtherRefNo==incidentBO.HasAffOtherDOCRefNo && h.PageNo == incidentBO.HasAffectedPageNo && h.IncidentMasterID == IncdntID);
                        if (Count != null)
                        {
                            alreadyExists = 1;
                        }
                }
                    if (alreadyExists ==1)//Restricting in adding duplicate records
                    {
                        return Json(0, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        int i = Qms_bal.IncidentHasAffectedDocInsertandUpdateBal(incidentBO, Operation);
                        return Json(new { hasError = false, data = incidentBO }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M22:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        //Delete Incident Has Affected Documents Details
        [HttpPost]
        public JsonResult DeleteIncidentHasAffectedDocuments(int? UniquePKID)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    var _hasaffecteddoc = dbEF.AizantIT_Incident_HasAffectedDocument.Where(a => a.UniquePKID == UniquePKID).ToList();
                    dbEF.AizantIT_Incident_HasAffectedDocument.Remove(_hasaffecteddoc[0]);
                    dbEF.SaveChanges();
                    return Json(new { hasError = false, data = UniquePKID }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M23:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        //To get Has Affected Document Details and bind to models
        [HttpGet]
        public JsonResult GetHasAffectedDocDetails(int? UniquePKID)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    var GetHasAffected = (from h in dbEF.AizantIT_Incident_HasAffectedDocument
                                          where h.UniquePKID == UniquePKID
                                          select new { h.IncidentMasterID, h.AffectedDocumentID, h.PageNo, h.Comments, h.UniquePKID, h.DeptID, h.HasDocRefNo, h.HasOtherDocName, h.HasOtherRefNo }).ToList();
                    IncidentBO objincidentBO = new IncidentBO();
                    foreach (var item in GetHasAffected)
                    {
                        objincidentBO.HasAffeDeptID = Convert.ToInt32(item.DeptID);
                        objincidentBO.HasAffectedDocumentID = Convert.ToInt32(item.AffectedDocumentID);
                        objincidentBO.HasAffectedDocRefNo = item.HasDocRefNo.ToString();
                        objincidentBO.HasAffDocTitle = item.HasOtherDocName;
                        objincidentBO.HasAffOtherDOCRefNo = item.HasOtherRefNo;
                        objincidentBO.HasAffectedPageNo = item.PageNo;
                        objincidentBO.AffectedComments = item.Comments;
                        objincidentBO.UniquePKID = item.UniquePKID;
                        objincidentBO.IncidentMasterID = Convert.ToInt32(item.IncidentMasterID);

                        EmployeID(objincidentBO);
                        //to load all departments Investigation
                        List<SelectListItem> Depts = new List<SelectListItem>();
                        var GetAllDepts = (from d in dbEF.AizantIT_DepartmentMaster
                                           select new { d.DeptID, d.DepartmentName }).Distinct().ToList();
                        Depts.Add(new SelectListItem
                        {
                            Text = "Select",
                            Value = "0",
                        });
                        foreach (var items in GetAllDepts)
                        {
                            Depts.Add(new SelectListItem
                            {
                                Text = items.DepartmentName,
                                Value = items.DeptID.ToString(),
                            });
                        }

                        ViewBag.HasAffeDeptID = new SelectList(Depts.ToList(), "Value", "Text");
                        objincidentBO.HasDeptIDList = Depts;


                        //Doc Type binding for dropdown
                        List<SelectListItem> objdoctypelist = new List<SelectListItem>();
                        objdoctypelist.Add(new SelectListItem
                        {
                            Text = "Select",
                            Value = "-1",
                        });
                        var D1 = (from tp in dbEF.AizantIT_DocumentType
                                  select new { tp.DocumentTypeID, tp.DocumentType }).ToList();
                        foreach (var item2 in D1)
                        {
                            objdoctypelist.Add(new SelectListItem
                            {
                                Text = item2.DocumentType,
                                Value = Convert.ToString(item2.DocumentTypeID),
                            });
                        }
                        objdoctypelist.Add(new SelectListItem
                        {
                            Text = "Others",
                            Value = "0",
                        });
                        objincidentBO.HasDoctypeList = objdoctypelist;
                        //Reference number binding for dropdown
                        List<SelectListItem> ObjDocRefList = new List<SelectListItem>();
                        var RefNo = (from dv in dbEF.AizantIT_DocVersion
                                     join DM in dbEF.AizantIT_DocumentMaster on dv.DocumentID equals DM.DocumentID
                                     where DM.DocumentTypeID == objincidentBO.HasAffectedDocumentID && DM.DeptID == objincidentBO.HasAffeDeptID && (dv.Status == "A" || dv.Status=="R")&& dv.EffectiveDate != null
                                     select new { dv.VersionID, DM.DocumentNumber }).ToList();
                        ObjDocRefList.Add(new SelectListItem
                        {
                            Text = "Select",
                            Value = "0",
                        });
                        foreach (var item7 in RefNo)
                        {
                            ObjDocRefList.Add(new SelectListItem
                            {
                                Text = item7.DocumentNumber,
                                Value = item7.VersionID.ToString(),
                            });
                        }
                        objincidentBO.HasDocRefList = ObjDocRefList;
                        if (objincidentBO.HasAffectedDocumentID != 0)
                        {
                           //Bind Latest Document Reference number 
                            int VersionID =Convert.ToInt32( objincidentBO.HasAffectedDocRefNo);
                            
                                bool has = RefNo.Any(t => t.VersionID == VersionID);
                                if (has == false)
                                {
                                    var DocumentID = (from t1 in dbEF.AizantIT_DocVersion
                                                      where t1.VersionID == VersionID
                                                      select t1.DocumentID).SingleOrDefault();
                                    //Getting New version id
                                    var GetNewVersionID = (from t1 in dbEF.AizantIT_DocVersion
                                                           orderby t1.VersionID descending
                                                           where t1.DocumentID == DocumentID && (t1.Status == "A" || t1.Status == "R") && t1.EffectiveDate != null
                                                           select t1.VersionID);
                                    int NewVersionID = GetNewVersionID.First();
                                    objincidentBO.HasDocName = (from t1 in dbEF.AizantIT_DocVersionTitle
                                                                join t2 in dbEF.AizantIT_DocVersion on t1.DocTitleID equals t2.DocTitleID
                                                                where t2.VersionID == NewVersionID
                                                                select t1.Title).SingleOrDefault();

                                    objincidentBO.DocumentNo = (from t1 in dbEF.AizantIT_DocumentMaster
                                                                where t1.DocumentID == DocumentID
                                                                select t1.DocumentNumber).SingleOrDefault();
                                    objincidentBO.ReferenceNumber = NewVersionID.ToString();
                                //New version ID stored
                                objincidentBO.HasAffectedDocRefNo = NewVersionID.ToString();
                                }
                                else
                                {
                                    var DocValues = (from DocR in dbEF.AizantIT_Incident_HasAffectedDocument
                                                     join Dv in dbEF.AizantIT_DocVersion on DocR.HasDocRefNo equals Dv.VersionID
                                                     join DM in dbEF.AizantIT_DocumentMaster on Dv.DocumentID equals DM.DocumentID
                                                     join VT in dbEF.AizantIT_DocVersionTitle on Dv.DocTitleID equals VT.DocTitleID
                                                     where DocR.UniquePKID == UniquePKID
                                                     select new { DM.DocumentNumber, VT.Title, Dv.VersionID }).ToList();
                                    foreach (var D in DocValues)
                                    {
                                        objincidentBO.DocumentNo = D.DocumentNumber;
                                        objincidentBO.HasDocName = D.Title;
                                        objincidentBO.ReferenceNumber = D.VersionID.ToString();
                                    }
                               // }
                            }
                        }
                    }
                    return Json(new { hasError = false, data = objincidentBO }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M24:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }


        //Insert Incident Investigation Quality Non-Impacting Details
        [HttpPost]
        public JsonResult QNonImpngInvestigationSubmit(IncidentBO objincidentBO, List<IncidentImpactPoints> Impacts2)
        {
            try
            {
                Boolean ResultBool = ToCheckEmpValidityIncident(objincidentBO.IncidentMasterID, 33);
                if (ResultBool == true)
                {
                    DataTable dtQNImpactlst = new DataTable();
                    dtQNImpactlst.Columns.Add("IncidentMasterID");
                    dtQNImpactlst.Columns.Add("isyesorNo");
                    dtQNImpactlst.Columns.Add("ImpactPointID");
                    dtQNImpactlst.Columns.Add("DocNo");
                    dtQNImpactlst.Columns.Add("Comments");
                    DataRow dr;
                    foreach (IncidentImpactPoints item in Impacts2)
                    {
                        dr = dtQNImpactlst.NewRow();
                        dr["IncidentMasterID"] = item.IncidentMasterID;
                        dr["isyesorNo"] = item.isyesorNo;
                        dr["ImpactPointID"] = item.ImpactPointID;
                        dr["DocNo"] = item.DocNo;
                        dr["Comments"] = item.Comments;
                        dtQNImpactlst.Rows.Add(dr);
                    }
                    if (objincidentBO.InvestigationOperation == "Submitted")
                    {
                        int i = Qms_bal.InsertInvestigationQNImpactingDetailsBal(objincidentBO, dtQNImpactlst);
                    }
                    else if (objincidentBO.InvestigationOperation == "Updated")
                    {
                        int i = Qms_bal.InsertInvestigationQNImpactingDetailsBal(objincidentBO, dtQNImpactlst);
                    }
                    return Json(new { hasError = false, data = objincidentBO }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { hasError = true, data = "You are not authorized User to do action." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M25:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        //File Upload Insertion
        [HttpPost]
        public JsonResult UploadFiles(IncidentBO objBo)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    AizantIT_Incident_IncidentAttachments objIncidentAttachmentsBO = new AizantIT_Incident_IncidentAttachments();
                    objIncidentAttachmentsBO.IncidentMasterID = objBo.IncidentMasterID;
                    objIncidentAttachmentsBO.Comments = objBo.UploadComments.Trim();
                    objIncidentAttachmentsBO.InvestTypeID = objBo.TypeofDocument;
                    objIncidentAttachmentsBO.isTemp = true;

                    foreach (HttpPostedFileBase file in objBo.files)
                    {
                        if (file != null)
                        {
                            objIncidentAttachmentsBO.FileName = Path.GetFileName(file.FileName);
                            objIncidentAttachmentsBO.FileType = Path.GetExtension(file.FileName);
                        }
                        var Count = dbEF.AizantIT_Incident_IncidentAttachments.FirstOrDefault(h => h.InvestTypeID == objIncidentAttachmentsBO.InvestTypeID
                   && h.FileName == objIncidentAttachmentsBO.FileName && h.FileType == objIncidentAttachmentsBO.FileType
                   && h.IncidentMasterID == objIncidentAttachmentsBO.IncidentMasterID);
                        if (Count != null)
                        {
                            return Json(new { hasError = true, data = "Already Exists" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            if (ModelState.IsValid)
                            {
                                if (objBo.files[0] != null)
                                {
                                    byte[] bytes;
                                    using (BinaryReader br = new BinaryReader(file.InputStream))
                                    {
                                        bytes = br.ReadBytes(file.ContentLength);
                                        //objIncidentAttachmentsBO.IncidentAttachament = bytes;
                                        objIncidentAttachmentsBO.FileName = Path.GetFileName(file.FileName);
                                        objIncidentAttachmentsBO.FileType = Path.GetExtension(file.FileName);
                                        dbEF.AizantIT_Incident_IncidentAttachments.Add(objIncidentAttachmentsBO);
                                        int result = dbEF.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                    var jsonResult = Json(new { hasError = false, data = new AizantIT_Incident_IncidentAttachments() }, JsonRequestBehavior.AllowGet);
                    jsonResult.MaxJsonLength = int.MaxValue;
                    return jsonResult;
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M26:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public JsonResult UploadFilesV1()
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                if (TempData["FileMsg"] == null)
                {
                    TempData["FileMsg"] = "";
                }
                if (TempData["FileAddedCount"] == null)
                {
                    TempData["FileAddedCount"] = 0;
                }
                if (TempData["FileexistsCount"] == null)
                {
                    TempData["FileexistsCount"] = 0;
                }
                try
                {

                    HttpFileCollectionBase files = Request.Files;
                    AizantIT_Incident_IncidentAttachments objIncidentAttachmentsBO = new AizantIT_Incident_IncidentAttachments();
                    objIncidentAttachmentsBO.IncidentMasterID = Convert.ToInt32(Request.Params["IncidentId"]);
                    objIncidentAttachmentsBO.Comments = Request.Params["_UploadComments"];
                    objIncidentAttachmentsBO.InvestTypeID = Convert.ToInt32(Request.Params["_TypeofDocument"]);
                    objIncidentAttachmentsBO.isTemp = false;

                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        if (file != null)
                        {
                            objIncidentAttachmentsBO.FileName = Path.GetFileName(file.FileName);
                            objIncidentAttachmentsBO.FileType = Path.GetExtension(file.FileName);
                        }
                        var Count = dbEF.AizantIT_Incident_IncidentAttachments.FirstOrDefault(h => h.InvestTypeID == objIncidentAttachmentsBO.InvestTypeID
                   && h.FileName == objIncidentAttachmentsBO.FileName && h.FileType == objIncidentAttachmentsBO.FileType
                   && h.IncidentMasterID == objIncidentAttachmentsBO.IncidentMasterID);
                        if (Count != null)
                        {
                            TempData["FileMsg"] += Path.GetFileName(file.FileName) + " upload failed as file already exists." + "<br/>";
                            TempData["FileexistsCount"] = Convert.ToInt32(TempData["FileexistsCount"]) + 1;
                        }
                        else
                        {
                            if (ModelState.IsValid)
                            {
                                if (file != null)
                                {
                                    //byte[] bytes;
                                    //using (BinaryReader br = new BinaryReader(file.InputStream))
                                    //{
                                    //    bytes = br.ReadBytes(file.ContentLength);
                                    //objIncidentAttachmentsBO.IncidentAttachament = bytes;

                                    objIncidentAttachmentsBO.FileName = Path.GetFileName(file.FileName);
                                    objIncidentAttachmentsBO.FileType = Path.GetExtension(file.FileName);
                                    QMSAttachments objAttachmentsCommon = new QMSAttachments();
                                    objIncidentAttachmentsBO.FilePath = objAttachmentsCommon.SaveUploadedFileinPhysicalPath(file, "Investigator", 1);
                                    dbEF.AizantIT_Incident_IncidentAttachments.Add(objIncidentAttachmentsBO);
                                    int result = dbEF.SaveChanges();
                                    TempData["FileMsg"] += Path.GetFileName(file.FileName) + " File uploaded successfully." + "<br/>";
                                    TempData["FileAddedCount"] = Convert.ToInt32(TempData["FileAddedCount"]) + 1;
                                    //}
                                }
                            }
                        }
                    }

                    TempData.Keep("FileMsg");
                    TempData.Keep("FileexistsCount");
                    TempData.Keep("FileAddedCount");
                    var MsgType = "";
                    if (Convert.ToInt32(TempData["FileexistsCount"]) > 0 && Convert.ToInt32(TempData["FileAddedCount"]) == 0)
                    {
                        MsgType = "error";
                    }
                    if (Convert.ToInt32(TempData["FileAddedCount"]) > 0 && Convert.ToInt32(TempData["FileexistsCount"]) == 0)
                    {
                        MsgType = "success";
                    }
                    if (Convert.ToInt32(TempData["FileAddedCount"]) > 0 && Convert.ToInt32(TempData["FileexistsCount"]) > 0)
                    {
                        MsgType = "info";
                    }
                    return Json(new { hasError = false, msg = TempData["FileMsg"], msgType = MsgType }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M15:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, msg = ErMsg, msgType = "Exceptionerror" }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        [HttpGet]
        public JsonResult RemoveTempData()
        {
            try
            {
                TempData.Remove("FileMsg");
                TempData.Remove("FileexistsCount");
                TempData.Remove("FileAddedCount");
                return Json(new { hasError = false }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { hasError = true, data = HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);
            }

        }
        //Delete Incident File Upload Documents Details
        [HttpPost]
        public JsonResult DeleteIncidentFileUploadDocuments(int? UniquePKID, int? IncidentMasterID, int EmpID)
        {
            try
            {
                using (AizantIT_DevEntities dbEF_DeleteInciFileUpload = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction _transaction = dbEF_DeleteInciFileUpload.Database.BeginTransaction())
                    {
                        try
                        {

                            //maintaining history when file deleted and here inplace of empid sending UniquePKID for filename
                            //SendNotifications((int)IncidentMasterID, (int)UniquePKID, 7); // 7-inserting history for file deleting

                            dbEF_DeleteInciFileUpload.AizantIT_SP_IncidentNotifications(
                            IncidentMasterID,
                            UniquePKID,
                            7
                            );

                            var GetFiles = (from T1 in dbEF_DeleteInciFileUpload.AizantIT_Incident_IncidentAttachments
                                            where T1.UniquePKID == UniquePKID
                                            select T1).SingleOrDefault();
                            var _ObjDetete = dbEF_DeleteInciFileUpload.AizantIT_Incident_IncidentAttachments.Where(a => a.UniquePKID == UniquePKID).ToList();
                            //Removed file path in physical folder
                            QMSAttachments objAttachmentsCommon = new QMSAttachments();
                            objAttachmentsCommon.RemovePhysicalPathFile(Server.MapPath("~\\" + GetFiles.FilePath));

                            dbEF_DeleteInciFileUpload.AizantIT_Incident_IncidentAttachments.Remove(_ObjDetete[0]);
                            dbEF_DeleteInciFileUpload.SaveChanges();
                            _transaction.Commit();
                            return Json(new { hasError = false, data = UniquePKID }, JsonRequestBehavior.AllowGet);
                        }
                        catch (Exception ex)
                        {
                            _transaction.Rollback();
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M27:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        // To Add Department Hod's for Incident Review
        [HttpPost]
        public JsonResult AddDeptHodsforReview(AizantIT_Incident_InvestigationReviewDeptHods objBO, List<int> EmpIDs)//dropdown multile select.
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    int? IncdntID = objBO.IncidentMasterID;
                    int? _Dept = objBO.DeptID;
                    foreach (int item in EmpIDs)
                    {
                        objBO.EmpID = item;
                        var Count = dbEF.AizantIT_Incident_InvestigationReviewDeptHods.FirstOrDefault(h => h.DeptID == _Dept && h.EmpID == objBO.EmpID && h.IncidentMasterID == IncdntID);
                        if (Count != null)
                        {
                            return Json(new { hasError = true, data = "Already Exists" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            if (ModelState.IsValid)
                            {
                                if (EmpIDs != null)
                                {
                                    dbEF.AizantIT_Incident_InvestigationReviewDeptHods.Add(objBO);
                                    dbEF.SaveChanges();
                                }
                            }
                        }
                    }
                    return Json(new { hasError = false, data = objBO }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M28:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        // To Delete Department Hod's for Incident Review 
        [HttpPost]
        public JsonResult DeleteDeptHodsforReview(int? UniquePKID)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    var _ObjDeleteByID = dbEF.AizantIT_Incident_InvestigationReviewDeptHods.Where(a => a.UniquePKID == UniquePKID).ToList();
                    dbEF.AizantIT_Incident_InvestigationReviewDeptHods.Remove(_ObjDeleteByID[0]);
                    dbEF.SaveChanges();
                    return Json(new { hasError = false, data = UniquePKID }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M29:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        #endregion

        #region Incident CAPA

        //To Delete Temporary capa's
        public void DeleteCapaTemp(int id)
        {
            CCNBal Qms_bal = new CCNBal();
            Qms_bal.DeleteCapaTemp(0, "Incident", id);
        }

        //To Check How many Capa's is There (for Validation).
        [HttpPost]
        public JsonResult CapaCheckingExistsOrNot(IncidentBO ObjincidentBO)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    ObjincidentBO.CapaExitsVal = (from CapaM in dbEF.AizantIT_CAPAMaster
                                                  where CapaM.QualityEvent_Number == ObjincidentBO.IRNumber && CapaM.QualityEvent_TypeID == 1
                                                  select CapaM).Count();
                    return Json(new { hasError = false, data = ObjincidentBO }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M33:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);
                }
            }
        }


        //Hod Capa Submit along with other fileds
        [HttpPost]
        public JsonResult SubmitHodCapaDetails(IncidentBO incidentBO)
        {
            try
            {
                string DepartmentName = "";
                if (incidentBO.IsAnyTrainingRequired == 2)
                {
                    if (!QMSCommonActions.GetIsTMSHODRoleExist())
                    {
                        return Json(new { hasError = true, data = "You are not authorized TMS HOD to Submit Training Schedule." }, JsonRequestBehavior.AllowGet);
                    }
                    else if (!QMSCommonActions.TMSHODAccesibleDeptExist(incidentBO.IncidentMasterID, ref DepartmentName))
                    {
                        return Json(new { hasError = true, data = "You are not having Accessible <b>" + DepartmentName + "</b> Department for TMS HOD role." }, JsonRequestBehavior.AllowGet);
                    }
                }
                
                    Boolean ResultBool = ToCheckEmpValidityIncident(incidentBO.IncidentMasterID, 25);
                    if (ResultBool == true)
                    {
                        incidentBO.SessionTTSStatus = Convert.ToInt32(Session["Incident_TTS_Status"]);
                        Session.Remove("Incident_TTS_Status");
                        int i = Qms_bal.SubmitHODCapaDetailsBal(incidentBO);
                        return Json(new { hasError = false, data = incidentBO }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { hasError = true, data = "You are not authorized User to do action." }, JsonRequestBehavior.AllowGet);
                    }
                
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M34:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #region capa link view in Assessments tab Not using
        //[HttpGet]
        //public JsonResult BindCAPADataforAssessments(int id)
        //{
        //    IncidentBO capa = new IncidentBO();
        //    if (Session["UserDetails"] != null)
        //    {
        //        capa.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
        //    }
        //    var capa1 = (from pd in dbEF.AizantIT_CAPAMaster
        //                 join od in dbEF.AizantIT_CAPA_ActionPlan on pd.CAPAID equals od.CAPAID
        //                 where pd.CAPAID == id
        //                 select new
        //                 {
        //                     od.CAPAID,
        //                     pd.CAPA_Number,
        //                     pd.CAPA_Date,
        //                     pd.QualityEvent_TypeID,
        //                     od.Capa_Action_Imp_DeptID,
        //                     od.Capa_Action_ImpID,
        //                     od.CurrentStatus,
        //                     pd.Verified_YesOrNO,
        //                     pd.QualityEvent_Number,
        //                     od.ActionPlan_TimeLines,
        //                     od.Capa_ActionID,
        //                     od.Capa_ActionDesc,
        //                     pd.QA_EmpID,
        //                     pd.HeadQA_EmpID,
        //                     pd.CreatedBy,
        //                 }).ToList();

        //    foreach (var dr in capa1)
        //    {
        //        capa.IRCAPAID = Convert.ToInt32(dr.CAPAID);
        //        capa.IRCAPA_Number = dr.CAPA_Number.ToString();
        //        capa.IRCAPA_Date = Convert.ToDateTime(dr.CAPA_Date).ToString("dd MMM yyyy");
        //        capa.DeptName = dr.Capa_Action_Imp_DeptID.ToString();
        //        capa.DeptID = Convert.ToInt32(dr.Capa_Action_Imp_DeptID);
        //        capa.EmpName = dr.Capa_Action_ImpID.ToString();
        //        capa.IRTargetDate = Convert.ToDateTime(dr.ActionPlan_TimeLines).ToString("dd MMM yyyy");
        //        capa.IRQualityEvent_TypeID = dr.QualityEvent_TypeID.ToString();
        //        capa.IRQualityEvent_Number = dr.QualityEvent_Number.ToString();
        //        if (dr.Verified_YesOrNO == true)
        //            capa.IRVerified_YesOrNO = "Yes";
        //        else
        //            capa.IRVerified_YesOrNO = "No";
        //        capa.IRCurrentStatus = dr.CurrentStatus.ToString();
        //        capa.IRPlanofAction = dr.Capa_ActionID.ToString();
        //        capa.IRCapa_ActionDesc = dr.Capa_ActionDesc.ToString();
        //        capa.IRQA_EmpId = Convert.ToInt32(dr.QA_EmpID);
        //        capa.IRCreatedBy = dr.CreatedBy.ToString();
        //    }
        //    int currentStatusID = Convert.ToInt32(capa.IRCurrentStatus);
        //    var StatusName = (from S in dbEF.AizantIT_ObjectStatus
        //                      where S.ObjID == 7 && S.StatusID == currentStatusID
        //                      select new { S.StatusName }).ToList();
        //    foreach (var item in StatusName)
        //    {
        //        capa.CurrentStatusName = item.StatusName;
        //    }

        //    int ResponsibleEmpID = Convert.ToInt32(capa.EmpName);
        //    var ResponsibleprsnName = (from S in dbEF.AizantIT_EmpMaster
        //                               where S.EmpID == ResponsibleEmpID
        //                               select new { FullName = (S.FirstName + "  " + S.LastName) }).ToList();
        //    foreach (var itemname in ResponsibleprsnName)
        //    {
        //        capa.IREmpName = itemname.FullName;
        //    }

        //    DataSet ds = CCNQms_bal.GetddlBindingCCNCreate(capa.EmpID);

        //    capa.IRDepartmentName = (from D in dbEF.AizantIT_DepartmentMaster
        //                             where D.DeptID == capa.DeptID
        //                             select D.DepartmentName).SingleOrDefault();

        //    //bind Quality event
        //    List<SelectListItem> ObjQualityeventList = new List<SelectListItem>();
        //    var Qualityevent = (from QE in dbEF.AizantIT_QualityEvent
        //                        select new { Name = (QE.QualityEvent_TypeName), ID = QE.QualityEvent_TypeID }).ToList();
        //    ObjQualityeventList.Add(new SelectListItem
        //    {
        //        Text = "Select",
        //        Value = "0",
        //    });
        //    foreach (var item7 in Qualityevent)
        //    {
        //        ObjQualityeventList.Add(new SelectListItem
        //        {
        //            Text = item7.Name,
        //            Value = item7.ID.ToString(),
        //        });
        //    }
        //    capa.CAPAQualityeventList = ObjQualityeventList;

        //    //Bind Action Plan
        //    List<SelectListItem> ActionplanlstEdit = new List<SelectListItem>();
        //    var ActionPlan = (from AD in dbEF.AizantIT_ActionPlanDescription
        //                      where AD.CurrentStatus == true
        //                      select new { AD.Capa_ActionID, AD.Capa_ActionName }).ToList();
        //    ActionplanlstEdit.Add(new SelectListItem
        //    {
        //        Text = "Select",
        //        Value = "0",
        //    });
        //    foreach (var item in ActionPlan)
        //    {
        //        ActionplanlstEdit.Add(new SelectListItem
        //        {
        //            Text = item.Capa_ActionName,
        //            Value = item.Capa_ActionID.ToString(),
        //        });
        //    }
        //    capa.CAPAPlanofactionList = ActionplanlstEdit;
        //    return Json(new { hasError = false, data = capa }, JsonRequestBehavior.AllowGet);
        //}
        #endregion
        //To Check Istrianing requried exits or not
        [HttpPost]
        public JsonResult IstrainingRequriedCountCheckingfrmDB(int IncidentMasterID)
        {
            try
            {
                IncidentBO obj = new IncidentBO();
                obj.IstrainingExitsorNot = 0;
                DataTable dt = Qms_bal.GetIstrainingExitsornotBal(IncidentMasterID);
                if (dt.Rows.Count > 0)
                {
                    obj.IstrainingExitsorNot = Convert.ToInt32(dt.Rows[0][0].ToString());
                }
                return Json(new { hasError = false, data = obj }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M42:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        public JsonResult _IncidentAdminManageHodRoleChecking(int IncidentMasterID, int EmpID)
        {
            try
            {
                //For checking TMS HOD Role is Exist or Not when selecting Admin Manage dropdown.
                Boolean IsRoleExists = QMSCommonActions.GetIsTMSHODRoleExist(EmpID);
                string DepartmentName = "";
                Boolean IsDepartmentExists = QMSCommonActions.TMSHODAccesibleDeptExist(IncidentMasterID, ref DepartmentName, EmpID);
                //For checking TTS is Exist or Not in Incident
                IncidentBO obj = new IncidentBO();
                obj._IsRoleExists = IsRoleExists;
                obj.IstrainingExitsorNot = 0;
                DataTable dt = Qms_bal.GetIstrainingExitsornotBal(IncidentMasterID);
                if (dt.Rows.Count > 0)
                {
                    obj.IstrainingExitsorNot = Convert.ToInt32(dt.Rows[0][0].ToString());
                }
                return Json(new { hasError = false, data = obj, IsDepartmentExists, DepartmentName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M42:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        //end 

        #region QA Approve
        //Qa Approve Action
        [HttpPost]
        public JsonResult IncidentQAApprove(IncidentBO objincidentBO)
        {
            try
            {
                Boolean Result = ToCheckEmpValidityIncident(objincidentBO.IncidentMasterID, 26);
                using (AizantIT_DevEntities dbEF_IncidentQAApprove = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction _transaction = dbEF_IncidentQAApprove.Database.BeginTransaction())
                    {
                        try
                        {
                            if (Result == true)
                            {
                                int? IncdntID = objincidentBO.IncidentMasterID;
                                var objIncdtUpdate = dbEF_IncidentQAApprove.AizantIT_IncidentMaster.Where(x => x.IncidentMasterID == IncdntID).FirstOrDefault();
                                if (objIncdtUpdate != null)
                                {
                                    objIncdtUpdate.TypeofIncidentID = Convert.ToInt32(objincidentBO.TypeofIncidentID);
                                    objIncdtUpdate.QAClassComments = objincidentBO.Comments.Trim();
                                    //objIncdtUpdate.InvestigatorEmpID = Convert.ToInt32(objincidentBO.InvestigatorEmpID);
                                    //Modified Current Status with Enums (cs=6)19-06-2019
                                    objIncdtUpdate.CurrentStatus = (int)Incident_Status.Approved_by_QA; //6 - QA Approved
                                    dbEF_IncidentQAApprove.Entry(objIncdtUpdate).State = EntityState.Modified;
                                    //SendNotifications(Convert.ToInt32(IncdntID), Convert.ToInt32(objIncdtUpdate.InvestigatorEmpID), 2); // 2-QA Approve 

                                    dbEF_IncidentQAApprove.AizantIT_SP_IncidentNotifications(
                                    IncdntID,
                                    objIncdtUpdate.InvestigatorEmpID,
                                    2
                                    );
                                    //Insert into History table 
                                    AizantIT_IncidentHistory objAizantIT_IncidentHistoryBO = new AizantIT_IncidentHistory();
                                    if (ModelState.IsValid)
                                    {
                                        dbEF_IncidentQAApprove.AizantIT_SP_InsertIncidentHistory(
                                           objincidentBO.IncidentMasterID, objincidentBO.EmpID,26,6, objincidentBO.Comments.Trim(),
                                           objIncdtUpdate.InvestigatorEmpID,33
                                            );
                                        //dbEF_IncidentQAApprove.AizantIT_IncidentHistory.Add(objAizantIT_IncidentHistoryBO);
                                        dbEF_IncidentQAApprove.SaveChanges();
                                        _transaction.Commit();
                                        return Json(new { hasError = false, data = objAizantIT_IncidentHistoryBO }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                                return Json(new { hasError = false, data = objincidentBO }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { hasError = true, data = "You are not authorized User to do action" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            _transaction.Rollback();
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M15:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        //Qa with Evaluation Approve Action
        [HttpPost]
        public JsonResult QASubmitEvaluation(IncidentBO objincidentBO)
        {
            try
            {
                if (objincidentBO.IsAnyTrainingRequired == 2)
                {
                    string DepartmentName = "";
                    if (!QMSCommonActions.GetIsTMSHODRoleExist(objincidentBO.HODEmpID))
                    {
                        return Json(new { hasError = true, data = "You are not authorized TMS HOD to Submit Training Schedule." }, JsonRequestBehavior.AllowGet);
                    }
                    else if (!QMSCommonActions.TMSHODAccesibleDeptExist(objincidentBO.IncidentMasterID, ref DepartmentName))
                    {
                        return Json(new { hasError = true, data = "QMS HOD is not having Accessible <b>" + DepartmentName + "</b> Department for TMS HOD role, you cannot Approve." }, JsonRequestBehavior.AllowGet);
                    }
                }
                Boolean Result = ToCheckEmpValidityIncident(objincidentBO.IncidentMasterID, 26);
                using (AizantIT_DevEntities dbEF_QASubmitEval = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction _transaction = dbEF_QASubmitEval.Database.BeginTransaction())
                    {
                        try
                        {
                            if (Result == true)
                            {
                                int? IncdntID = objincidentBO.IncidentMasterID;
                                var objIncdtUpdate = dbEF_QASubmitEval.AizantIT_IncidentMaster.Where(x => x.IncidentMasterID == IncdntID).FirstOrDefault();
                                if (objIncdtUpdate != null)
                                {
                                    objIncdtUpdate.EvalutionAndDisposition_Desc = objincidentBO.EvaluationandDispostion.Trim();
                                    objIncdtUpdate.HeadQAEmpID = objincidentBO.HeadQAEmpID;
                                    //objIncdtUpdate.FinalClassification = objincidentBO.FinalClassification;
                                    //Modified Current Status with Enums (cs=13)19-06-2019
                                    objIncdtUpdate.CurrentStatus = (int)Incident_Status.Incd_with_Invest_Approved_by_QA; //13 - Incident with Investigation  Approved by QA
                                    dbEF_QASubmitEval.Entry(objIncdtUpdate).State = EntityState.Modified;
                                    //SendNotifications(Convert.ToInt32(IncdntID), Convert.ToInt32(objincidentBO.HeadQAEmpID), 4); // 4- QA Approve with Evaluation and Dispostion

                                    dbEF_QASubmitEval.AizantIT_SP_IncidentNotifications(
                                    IncdntID,
                                    objincidentBO.HeadQAEmpID,
                                    4);
                                    //Insert into History table 
                                    AizantIT_IncidentHistory objAizantIT_IncidentHistoryBO = new AizantIT_IncidentHistory();
                                    if (ModelState.IsValid)
                                    {
                                        dbEF_QASubmitEval.AizantIT_SP_InsertIncidentHistory(
                                            objincidentBO.IncidentMasterID,
                                            objincidentBO.EmpID,
                                            26,
                                            13,
                                            objincidentBO.QAEvalComments.Trim(),
                                            objIncdtUpdate.HeadQAEmpID,
                                            27);
                                        dbEF_QASubmitEval.SaveChanges();
                                        _transaction.Commit();
                                        return Json(new { hasError = false, data = objAizantIT_IncidentHistoryBO }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                                return Json(new { hasError = false, data = objincidentBO }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { hasError = true, data = "You are not authorized User to do action." }, JsonRequestBehavior.AllowGet);

                            }
                        }
                        catch (Exception ex)
                        {
                            _transaction.Rollback();
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M35:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region HeadQA Approve
        //HeadQa Approve Action
        [HttpPost]
        public JsonResult HeadQASubmit(IncidentBO objincidentBO)
        {
            try
            {
                if (objincidentBO.IsAnyTrainingRequired == 2)
                {
                    string DepartmentName = "";

                    if (!QMSCommonActions.GetIsTMSHODRoleExist(objincidentBO.HODEmpID))
                    {
                        return Json(new { hasError = true, data = "You are not authorized TMS HOD to Submit Training Schedule." }, JsonRequestBehavior.AllowGet);
                    }
                    else if (!QMSCommonActions.TMSHODAccesibleDeptExist(objincidentBO.IncidentMasterID,ref DepartmentName))
                    {
                        return Json(new { hasError = true, data = "QMS HOD is not having Accessible <b>" + DepartmentName + "</b> Department for TMS HOD role, you cannot Approve." }, JsonRequestBehavior.AllowGet);
                    }
                }
                Boolean Result = ToCheckEmpValidityIncident(objincidentBO.IncidentMasterID, 27);
                ////Merge All Attachments

                using (AizantIT_DevEntities IncidentDB = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction _transaction = IncidentDB.Database.BeginTransaction())
                    {
                        try
                        {
                            if (Result == true)
                            {
                                int? IncdntID = objincidentBO.IncidentMasterID;
                                var objIncdtUpdate = IncidentDB.AizantIT_IncidentMaster.Where(x => x.IncidentMasterID == IncdntID).FirstOrDefault();

                                if (objIncdtUpdate != null)
                                {
                                    //Modified Current Status with Enums (cs=16)19-06-2019
                                    objIncdtUpdate.CurrentStatus = (int)Incident_Status.Incd_with_Invest_Approved_by_HeadQA; //16 - Incident with Investigation  Approved by HeadQA
                                    IncidentDB.Entry(objIncdtUpdate).State = EntityState.Modified;

                                    //Here with Capa in capalist
                                    //SendNotifications(Convert.ToInt32(IncdntID), 0, 5); // 5- HeadQA Approve

                                    //SendNotifications(Convert.ToInt32(IncdntID), objincidentBO.EmpID, 5); // 5- HeadQA Approve

                                    IncidentDB.AizantIT_SP_IncidentNotifications(
                                        IncdntID,
                                        objincidentBO.EmpID,
                                        5);
                                    //Insert into History table 
                                    AizantIT_IncidentHistory objAizantIT_IncidentHistoryBO = new AizantIT_IncidentHistory();
                                    if (ModelState.IsValid)
                                    {
                                        IncidentDB.AizantIT_SP_InsertIncidentHistory(
                                            objincidentBO.IncidentMasterID,
                                            objincidentBO.EmpID,
                                            27,
                                            16,
                                            objincidentBO.HeadQAComments.Trim(),0,0
                                            );
                                        //IncidentDB.AizantIT_IncidentHistory.Add(objAizantIT_IncidentHistoryBO);
                                        IncidentDB.SaveChanges();
                                        _transaction.Commit();
                                        return Json(new { hasError = false, data = objAizantIT_IncidentHistoryBO }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                                return Json(new { hasError = false, data = objincidentBO }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { hasError = true, data = "You are not authorized User to do action." }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            _transaction.Rollback();
                            throw ex;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M36:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        //start commented by pradeep 29-06-2020 Incident Report with using Single SP New Code
        #region Incident Reports

        //For Binding Company Logo on Incident Report
        DataTable fillCompany()
        {
            CompanyBO objCompanyBO;
            UMS_BAL objUMS_BAL;
            DataTable dtcompany = new DataTable();
            objCompanyBO = new CompanyBO();
            objCompanyBO.Mode = 2;
            objCompanyBO.CompanyID = 0;
            objCompanyBO.CompanyCode = "";
            objCompanyBO.CompanyName = "";
            objCompanyBO.CompanyDescription = "";
            objCompanyBO.CompanyPhoneNo1 = "";
            objCompanyBO.CompanyPhoneNo2 = "";
            objCompanyBO.CompanyFaxNo1 = "";
            objCompanyBO.CompanyFaxNo2 = ""; //txtFaxNo2.Value;
            objCompanyBO.CompanyEmailID = "";
            objCompanyBO.CompanyWebUrl = "";
            objCompanyBO.CompanyAddress = "";
            objCompanyBO.CompanyLocation = "";// txtLocat.Value;
            objCompanyBO.CompanyCity = "";
            objCompanyBO.CompanyState = "";
            objCompanyBO.CompanyCountry = "";
            objCompanyBO.CompanyPinCode = "";
            objCompanyBO.CompanyStartDate = "";
            objCompanyBO.CompanyLogo = new byte[0];

            objUMS_BAL = new UMS_BAL();
            dtcompany = objUMS_BAL.CompanyQuery(objCompanyBO);
            return dtcompany;
        }
        //For Binding Image Logo on Reports
        private System.Drawing.Image BindApplicantImage(byte[] applicantImage)
        {
            //string base64ImageString = ConvertBytesToBase64(applicantImage);
            //MyImage.ImageUrl = "data:image/jpg;base64," + base64ImageString;
            using (var ms = new MemoryStream(applicantImage))
            {
                return System.Drawing.Image.FromStream(ms);
            }
        }
        //To Generate Incident Report
        public void IncidentReportGenerate(int IncidentMasterID)
        {
            AizantIT_DevEntities dbEF = new AizantIT_DevEntities();
            //try
            //{
            int incidentID = IncidentMasterID;
            IncidentBO incdntBO = new IncidentBO();
            //QMS_BO.QMS_CAPA_BO.CapaEventsBO capaEventsBO = new QMS_BO.QMS_CAPABO.CapaEventsBO();
            //QMS_BO.QMS_CAPABO.CapaModel capaBO = new QMS_BO.QMS_CAPABO.CapaModel();
            IncidentReportPage1 incReport1 = new IncidentReportPage1();
            IncidentReportPage2 incReport2 = new IncidentReportPage2();
            IncidentReportPage3 incReport3 = new IncidentReportPage3();
            IncidentReportPage4 incReport4 = new IncidentReportPage4();
            TTSIncidentReport incTTSReport = new TTSIncidentReport();

            //CAPA in Land scape mode was not using 
            //IncidentReportCAPA incReportCAPA = new IncidentReportCAPA();

            //CAPA in A4 page, with Dynamic design
            IncidentCAPA_Report incCAPA_Report = new IncidentCAPA_Report();

            IR_SectionFive_Report incSectionV = new IR_SectionFive_Report();

            IncidentReportFileAttachmentPage incFileReport = new IncidentReportFileAttachmentPage();

            //Code for Company Logo
            DataTable _dtCompany = fillCompany();
            if (_dtCompany.Rows.Count != 0)
            {
                if (!string.IsNullOrEmpty(_dtCompany.Rows[0]["CompanyLogo"].ToString()))
                {
                    ((XRPictureBox)(incReport1.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                    ((XRPictureBox)(incReport2.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                    ((XRPictureBox)(incReport3.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                    ((XRPictureBox)(incReport4.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                    ((XRPictureBox)(incTTSReport.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                    ((XRPictureBox)(incFileReport.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                    //((XRPictureBox)(incReportCAPA.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                    //For CAPA Report
                    ((XRPictureBox)(incCAPA_Report.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                    ((XRPictureBox)(incSectionV.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                }
            }
            else
            {
                throw new Exception("Please Upload Company Logo");
            }

            DataSet dt_20 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incidentID);

            foreach (DataRow rdr in dt_20.Tables[20].Rows)
            {
                incdntBO.IncidentMasterID = Convert.ToInt32(rdr["IncidentMasterID"]);
                incdntBO.IRNumber = rdr["IRNumber"].ToString();
                incdntBO.IncidentReportDate = Convert.ToDateTime(rdr["IncidentReportDate"]).ToString("dd MMM yyyy");
                incdntBO.OccurrenceDate = Convert.ToDateTime(rdr["OccurrenceDate"]).ToString("dd MMM yyyy");
                incdntBO.IncidentDesc = rdr["IncidentDesc"].ToString();
                incdntBO.DeptID = Convert.ToInt32(rdr["DeptID"]);
                incdntBO.InitiatorEmpID = Convert.ToInt32(rdr["InitiatorEmpID"]);
                incdntBO.CreatedDate = Convert.ToDateTime(rdr["CreatedDate"]).ToString();
                incdntBO.DocumentType = Convert.ToInt32(rdr["DocTypeID"]);
                incdntBO.CurrentStatus = rdr["CurrentStatus"].ToString();
                incdntBO.QAClassComments = rdr["QAClassComments"].ToString();
                incdntBO.AnyOtherInvestigations = rdr["AnyOtherInvestigations"].ToString();
                incdntBO.QNonImpConclusion = rdr["Conclusion_Desc"].ToString();
                incdntBO.TypeofCategoryID = Convert.ToInt32(rdr["TypeofCategoryID"]);
                incdntBO.HOD_InvestigationNotes = rdr["HOD_InvestigationNotes"].ToString();
                if (incdntBO.TypeofCategoryID != 0 && incdntBO.TypeofCategoryID != -1)
                {
                    incdntBO.TypeofCategory = GetCategoryName(incdntBO.TypeofCategoryID);
                }
                else
                {
                    incdntBO.TypeofCategory = "N/A";
                }

                if (dt_20.Tables[20].Rows[0]["IsAnyTrainingRequired"].ToString() != "")
                {
                    incdntBO.IsAnyTrainingRequired = Convert.ToInt32(rdr["IsAnyTrainingRequired"]);
                }

                //if (!string.IsNullOrEmpty(Convert.ToString(item.IsFirstOccurence)))
                if (dt_20.Tables[20].Rows[0]["IsFirstOccurence"].ToString() != "")
                {
                    //incdntBO.IsFirstOccurence = item.IsFirstOccurence.ToString();
                    incdntBO.IsFirstOccurence = rdr["IsFirstOccurence"].ToString();
                }
                if (dt_20.Tables[20].Rows[0]["IsFirstOccurence"].ToString() == "1")
                {
                    incdntBO.IsFirstOccurence = "Yes";
                }
                if (dt_20.Tables[20].Rows[0]["IsFirstOccurence"].ToString() == "2")
                {
                    incdntBO.IsFirstOccurence = "No";
                }
                if (dt_20.Tables[20].Rows[0]["IsFirstOccurence"].ToString() == "3")
                {
                    incdntBO.IsFirstOccurence = "N/A";
                }

                ((XRLabel)(incReport1.FindControl("xrlblIsFirstOccurrence", false))).Text = incdntBO.IsFirstOccurence;
                incdntBO.FO_Comments = rdr["FO_Comments"].ToString();
                //For Binding FO_comments as Remarks
                if (incdntBO.FO_Comments == null || incdntBO.FO_Comments == "")
                {

                    ((XRLabel)(incReport1.FindControl("xrLabelAOI_Comments", false))).Text = "N/A";
                }
                else
                {
                    ((XRLabel)(incReport1.FindControl("xrLabelAOI_Comments", false))).Text = incdntBO.FO_Comments;
                }

                //For Binding Investigation start
                //if (!string.IsNullOrEmpty(Convert.ToString(item.AnyOtherInvestigations)))
                if (dt_20.Tables[20].Rows[0]["AnyOtherInvestigations"].ToString() != "")
                {
                    incdntBO.AnyOtherInvestigations = rdr["AnyOtherInvestigations"].ToString();
                }

                if (dt_20.Tables[20].Rows[0]["AnyOtherInvestigations"].ToString() == "1")
                {
                    incdntBO.AnyOtherInvestigations = "Yes";
                }
                if (dt_20.Tables[20].Rows[0]["AnyOtherInvestigations"].ToString() == "2")
                {
                    incdntBO.AnyOtherInvestigations = "No";
                }
                if (dt_20.Tables[20].Rows[0]["AnyOtherInvestigations"].ToString() == "3")
                {
                    incdntBO.AnyOtherInvestigations = "N/A";
                }

                ((XRLabel)(incReport1.FindControl("xrlblAnyOtherInvestgations", false))).Text = incdntBO.AnyOtherInvestigations;
                //For binding Type of Incident
                if (dt_20.Tables[20].Rows[0]["TypeofIncidentID"].ToString() == "1")
                {
                    incdntBO.TypeofIncidentName = "Quality Impacting";
                }
                if (dt_20.Tables[20].Rows[0]["TypeofIncidentID"].ToString() == "2")
                {
                    incdntBO.TypeofIncidentName = "Quality Non-Impacting";
                }
                ((XRLabel)(incReport1.FindControl("xrlblTypeofIncident", false))).Text = incdntBO.TypeofIncidentName;

                incdntBO.AOI_Comments = rdr["AOI_Comments"].ToString();
                //For Binding AOI_Comments as Remarks
                if (incdntBO.AOI_Comments == null || incdntBO.AOI_Comments == "")
                {
                    ((XRLabel)(incReport1.FindControl("xrLabelFO_Comments", false))).Text = "N/A";
                }
                else
                {
                    ((XRLabel)(incReport1.FindControl("xrLabelFO_Comments", false))).Text = incdntBO.AOI_Comments;
                }

                //commented on 07-01-2020
                if (incdntBO.HOD_InvestigationNotes == null || incdntBO.HOD_InvestigationNotes == "")
                {
                    ((XRLabel)(incReport1.FindControl("xrLabelHOD_InvestigationNotes", false))).Text = "N/A";
                }
                else
                {
                    ((XRLabel)(incReport1.FindControl("xrLabelHOD_InvestigationNotes", false))).Text = incdntBO.HOD_InvestigationNotes;
                }
                //end 07-01-2020

                //if (!string.IsNullOrEmpty(Convert.ToString(item.TypeofIncidentID)))
                if (dt_20.Tables[20].Rows[0]["TypeofIncidentID"].ToString() != "")
                {
                    incdntBO.TypeofIncidentID = Convert.ToInt32(rdr["TypeofIncidentID"]);
                }
                incdntBO.ImmediateCorrection = rdr["ImmediateCorrection"].ToString();
                //For Binding Investigation End
            }
           
            //For Binding Department Name
            DataSet dt_23 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incidentID);
            incdntBO.DeptName = dt_23.Tables[23].Rows[0]["DepartmentName"].ToString();

            //For Binding Designation Name for initiator
            DataSet dt_24 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incidentID);
            ((XRLabel)(incReport1.FindControl("xrLblInitiatorDesig", false))).Text = "( " + dt_24.Tables[24].Rows[0]["DesignationName"] + " )";
           
            //Commented on 18-09-2019
            DocumentDetialsClass objDocument = new DocumentDetialsClass();
            QMSCommonActions CommonDocBo = new QMSCommonActions();
            if (incdntBO.DocumentType != 0 && incdntBO.DocumentType != -1)
            {
                //start commented on 19-02-2020
                objDocument = CommonDocBo.GetDocumentDetails(incdntBO.IncidentMasterID, 1);
                incdntBO.DocumentNo = objDocument.DocumentNumber;
                incdntBO.DocName = objDocument.DocumentName;
                incdntBO.ReferenceNumber = objDocument.VersionID.ToString();
                //    //Commented on 18-09-2019
                incdntBO.TypeofDocumentName = objDocument.DocumentType;
                //end commented on 19-02-2020
            }

            //If select Others
            else if (incdntBO.DocumentType == 0)
            {
                //start commented on 19-02-2020
                objDocument = CommonDocBo.GetOtherDocumentDetails(incdntBO.IncidentMasterID, 1);
                incdntBO.DocumentNo = objDocument.DocumentNumber;
                incdntBO.DocName = objDocument.DocumentName;
                incdntBO.DocTitle = objDocument.DocumentName;
                incdntBO.OtherDOCRefNo = objDocument.DocumentNumber;
                incdntBO.TypeofDocumentName = "Others";
                //end commented on 19-02-2020
            }
            else
            {
                incdntBO.DocumentNo = "N/A";
                incdntBO.DocName = "N/A";
                incdntBO.DocTitle = "N/A";
                incdntBO.OtherDOCRefNo = "N/A";
                incdntBO.TypeofDocumentName = "N/A";
            }

            //For Binding Electronic Signatures
            //start commented by pradeep 24-03-2020
            DataSet dt_25 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incidentID);
            //For Binding Action Date for Initiator
            ((XRLabel)(incReport1.FindControl("xrInitiatorCreatedDate", false))).Text = Convert.ToDateTime(dt_25.Tables[25].Rows[0]["ActionDate"]).ToString("dd MMM yyyy HH: mm:ss");

            //For Binding Action Date for HOD
            ((XRLabel)(incReport1.FindControl("xrHodCreatedDate", false))).Text = Convert.ToDateTime(dt_25.Tables[25].Rows[1]["ActionDate"]).ToString("dd MMM yyyy HH:mm:ss");

            //For Binding Action Date for QA
            ((XRLabel)(incReport1.FindControl("xrQACreatedDateNew1", false))).Text = Convert.ToDateTime(dt_25.Tables[25].Rows[2]["ActionDate"]).ToString("dd MMM yyyy HH:mm:ss");

            //For Binding Action Date for Investigator
            ((XRLabel)(incReport2.FindControl("xrInvestCreatedDate", false))).Text = Convert.ToDateTime(dt_25.Tables[25].Rows[3]["ActionDate"]).ToString("dd MMM yyyy HH:mm:ss");

            //For Binding Initiator Name
            ((XRLabel)(incReport1.FindControl("xrLblInitName", false))).Text = dt_25.Tables[25].Rows[0]["EmpName"].ToString();

            //end commented by pradeep 24-03-2020

            //For Binding Hod Name // Added Current Status 21 on 03-07-2019
            //Modified Current Status with Enums (cs="3" || cs="6" || cs="9" || cs="12" || cs="13" || cs="16" || cs="21" )19-06-2019
            if (incdntBO.CurrentStatus == ((int)Incident_Status.Approved_by_HOD).ToString() || incdntBO.CurrentStatus == ((int)Incident_Status.Approved_by_QA).ToString() || incdntBO.CurrentStatus == ((int)Incident_Status.Completed_Invest_by_Investigator).ToString() || incdntBO.CurrentStatus == ((int)Incident_Status.HOD_Approved_and_added_ActionPlan_Training).ToString() ||
            incdntBO.CurrentStatus == ((int)Incident_Status.Incd_with_Invest_Approved_by_QA).ToString() || incdntBO.CurrentStatus == ((int)Incident_Status.Incd_with_Invest_Approved_by_HeadQA).ToString() ||
            incdntBO.CurrentStatus == ((int)Incident_Status.At_ActionPlan).ToString() || incdntBO.CurrentStatus == ((int)Incident_Status.Verified_by_QA).ToString())// Current Status= "3","6","9","12","13","16"
            {
                //For binding Hod Name
                ((XRLabel)(incReport1.FindControl("xrLblHODName", false))).Text = dt_25.Tables[25].Rows[1]["EmpName"].ToString();
                ((XRLabel)(incReport1.FindControl("xrLblHODDesig", false))).Text = "( " + dt_25.Tables[25].Rows[0]["DesignationName"].ToString() + " )";
            }
            //For Binding QA Name //Added Current Status 21 on 03-07-2019

            //Modified Current Status with Enums (cs="6" || cs="9" || cs="12" || cs="13" || cs="16" || cs="21" )19-06-2019
            if (incdntBO.CurrentStatus == ((int)Incident_Status.Approved_by_QA).ToString() || incdntBO.CurrentStatus == ((int)Incident_Status.Completed_Invest_by_Investigator).ToString() || incdntBO.CurrentStatus == ((int)Incident_Status.HOD_Approved_and_added_ActionPlan_Training).ToString() || incdntBO.CurrentStatus == ((int)Incident_Status.Incd_with_Invest_Approved_by_QA).ToString() ||
                incdntBO.CurrentStatus == ((int)Incident_Status.Incd_with_Invest_Approved_by_HeadQA).ToString() ||
                incdntBO.CurrentStatus == ((int)Incident_Status.At_ActionPlan).ToString() ||
                incdntBO.CurrentStatus == ((int)Incident_Status.Verified_by_QA).ToString())  // Current Status= "6","9","12","13","16"
            {
                //For binding QA Name
                ((XRLabel)(incReport1.FindControl("xrLblQANameNew", false))).Text = dt_25.Tables[25].Rows[2]["EmpName"].ToString();
                //For Binding Designation Name for QA
                ((XRLabel)(incReport1.FindControl("xrLblQADesigNew", false))).Text = "(" + dt_25.Tables[25].Rows[2]["DesignationName"].ToString() + ")";
            }
            //create table for CCN Assessment
            XRTable tblIncPreAssment = ((XRTable)(incReport1.FindControl("xrTablePreliminary", true)));
            tblIncPreAssment.BeginInit();
           
            //Data Set for Preliminary Assessment Map Incident
            //DataSet dt1 = Qms_bal.GetIncidentAnyotherinvestigationBal(0, 0, 1, "desc", "", incidentID, 1);
            DataSet dt_0 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incidentID);

            if (dt_0.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt_0.Tables[0].Rows)
                {
                    XRTableRow row1 = new XRTableRow();//row create
                    XRTableCell cell1 = new XRTableCell();
                    cell1.CanGrow = true;
                    cell1.WidthF = 291.59f;
                    cell1.Text = (rdr["Department"].ToString());
                    row1.Cells.Add(cell1);

                    XRTableCell cell2 = new XRTableCell();
                    cell2.CanGrow = true;
                    cell2.WidthF = 172.49f;
                    cell2.Text = (rdr["IRNumber"].ToString());
                    row1.Cells.Add(cell2);

                    
                    XRTableCell cell3 = new XRTableCell();
                    cell3.CanGrow = true;
                    cell3.WidthF = 318.72f;
                    cell3.Text = (rdr["CAPA_Numbers"].ToString()) != "" ? (rdr["CAPA_Numbers"].ToString()) : "N/A";
                    row1.Cells.Add(cell3);

                    tblIncPreAssment.Rows.Add(row1);
                }
            }
            else
            {
                incReport1.FindControl("xrTablePreliminary", true).Visible = false;
            }
            //end

            tblIncPreAssment.EndInit();

            // For binding Investigation Table
            XRTable tblIncInvestigation = ((XRTable)(incReport1.FindControl("xrTableInvestigation", true)));
            tblIncInvestigation.BeginInit();

            DataSet dt_1 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incidentID);

            if (dt_1.Tables[1].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt_1.Tables[1].Rows)
                {
                    XRTableRow row1 = new XRTableRow();//row create
                    XRTableCell cell1 = new XRTableCell();
                    cell1.CanGrow = true;
                    cell1.WidthF = 179.7f;
                    cell1.Text = (rdr["Department"].ToString());
                    row1.Cells.Add(cell1);

                    XRTableCell cell2 = new XRTableCell();
                    cell2.CanGrow = true;
                    cell2.WidthF = 200.3f;
                    cell2.Text = (rdr["QualityEvent_TypeName"].ToString());
                    row1.Cells.Add(cell2);

                    XRTableCell cell3 = new XRTableCell();
                    cell3.CanGrow = true;
                    cell3.WidthF = 209.63f;
                    cell3.Text = (rdr["EventNumbers"].ToString());
                    row1.Cells.Add(cell3);

                    XRTableCell cell4 = new XRTableCell();
                    cell4.CanGrow = true;
                    cell4.WidthF = 193.18f;
                    //cell4.Text = (rdr["CAPA_Number"].ToString());
                    cell4.Text = (rdr["CAPA_Number"].ToString()) != "" ? (rdr["CAPA_Number"].ToString()) : "N/A";
                    row1.Cells.Add(cell4);
                    tblIncInvestigation.Rows.Add(row1);
                }
            }
            else
            {
                incReport1.FindControl("xrTableInvestigation", true).Visible = false;
            }

            tblIncInvestigation.EndInit();
            incReport1.DataSource = incdntBO;
            incReport1.CreateDocument();

            //For getting Incident CAPA count in 
            DataSet dt_13 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incidentID);
            int NoOfCAPAs = Convert.ToInt32(dt_13.Tables[13].Rows[0]["CAPA_Count"]);

            //For Setting Report3 pages
            //If First occurrence of Incident is true then hide Table
            //For Hiding Report 3 when Quality Imapacting is True , 
            //and Hiding Report 2 when Quality Non Impacting is True,
            if (incdntBO.TypeofIncidentID == 1)
            {
                if (NoOfCAPAs == 0)
                {
                    incReport2.FindControl("xrTableSectionIVLabel", true).Visible = false;
                    incReport2.FindControl("xrTableSectionIVText", true).Visible = false;
                }
                Investigation(incdntBO, incReport2, incReport1, dbEF);
                incReport2.CreateDocument();
                //For Adding Pages
                incReport1.Pages.AddRange(incReport2.Pages);
            }
            else
            {
                InvestigationPage3(incdntBO, incReport3, incReport4, dbEF);
                //For Adding Pages
                incReport3.CreateDocument();
                incReport1.Pages.AddRange(incReport3.Pages);
            }
            //For Adding Incident CAPA page
            if (NoOfCAPAs != 0)
            {
                //This commented code belongs to Landscape capa 
                //IncidentReportCapa(incdntBO, incReportCAPA, incidentID);
                //incReportCAPA.CreateDocument();
                //incReport1.Pages.AddRange(incReportCAPA.Pages);
                //end

                //This code belongs to A4 Dynamic Design capa
                IncidentCAPAs_Report(incCAPA_Report, incidentID, dbEF);
                incCAPA_Report.CreateDocument();
                incReport1.Pages.AddRange(incCAPA_Report.Pages);
                //end

                //For Setting Report Section IV in CAPA
                IncidentSectionVReport(incdntBO, incSectionV, dbEF);
                //For Adding Pages
                incSectionV.CreateDocument();
                incReport1.Pages.AddRange(incSectionV.Pages);
                //end
            }
            else
            {
                //For Setting Report4 pages
                InvestigationPage4(incdntBO, incReport4, dbEF);
                //For Adding Pages
                incReport4.CreateDocument();
                incReport1.Pages.AddRange(incReport4.Pages);
            }
            //end
            //For Setting Report5 pages
            TTSIncidentReportInvestigationPage5(incdntBO, incTTSReport, dbEF);
            incTTSReport.CreateDocument();
            incReport1.Pages.AddRange(incTTSReport.Pages);
            //For Setting Incident File Attachment Report
            IncidentReportFileAttachmentPage(incdntBO, incFileReport, dbEF);
            incFileReport.CreateDocument();
            incReport1.Pages.AddRange(incFileReport.Pages);

            #region File path for Reprots

            string TempAttachmentDir = Server.MapPath("~\\Areas\\QMS\\Models\\Incident\\Reports");

            if (!Directory.Exists(TempAttachmentDir))
            {
                Directory.CreateDirectory(TempAttachmentDir);
            }
            //string strFilePath = DynamicFolder.CreateDynamicFolder(11);
            string fileName = incidentID + DateTime.Now.ToString("yyyMMddHHmmssfff") + ".pdf";
            string strFilePath = TempAttachmentDir + "\\" + fileName;
            incReport1.ExportToPdf(strFilePath);
            string onlyReportPath = "Areas\\QMS\\Models\\Incident\\Reports" + "\\" + fileName;
            var GetAlreadyExistReportPath = (from T1 in dbEF.AizantIT_IncidentFinalReport
                                             where T1.IncidentID == incidentID
                                             select T1).SingleOrDefault();
            if (GetAlreadyExistReportPath != null)
            {
                //Updated Report path 
                AizantIT_IncidentFinalReport objIncidentAttachmentAsSingle =
                           dbEF.AizantIT_IncidentFinalReport.SingleOrDefault(p => p.IncidentID == incidentID);
                objIncidentAttachmentAsSingle.ReportFilePath = onlyReportPath;
                objIncidentAttachmentAsSingle.CurrentStatus = Convert.ToInt32(incdntBO.CurrentStatus);
                dbEF.SaveChanges();
            }
            else
            {
                //Inserted Report path
                AizantIT_IncidentFinalReport objInertReportPath = new AizantIT_IncidentFinalReport();
                objInertReportPath.ReportFilePath = onlyReportPath;
                objInertReportPath.IncidentID = incidentID;
                objInertReportPath.CreatedDate = DateTime.Now;
                objInertReportPath.CurrentStatus = Convert.ToInt32(incdntBO.CurrentStatus);
                dbEF.AizantIT_IncidentFinalReport.Add(objInertReportPath);
                dbEF.SaveChanges();
            }

            //Report generated
            GenerateFinalIncidentReport(onlyReportPath, incidentID, dbEF);

            #endregion
        }

        public void Investigation(IncidentBO incdntBO, IncidentReportPage2 incReport2, IncidentReportPage1 incReport1, AizantIT_DevEntities dbEF)
        {
            //start commented by pradeep 20-03-2020 Duplicate Code
            // For binding Investigation Table
            XRTable tblIncInvestigation = ((XRTable)(incReport1.FindControl("xrTableInvestigation", true)));
            tblIncInvestigation.BeginInit();

            //Data Set for Any other Incvestigation Map Quality Notifications
            //DataSet dt2 = Qms_bal.GetIncidentPreliminaryAssessmentListBal(0, 0, 1, "desc", "", incidentID, 1);
            DataSet dt_1 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incdntBO.IncidentMasterID);

            //end commented by pradeep on 20-03-2020
            if (dt_1.Tables[1].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt_1.Tables[1].Rows)
                {
                    XRTableRow row1 = new XRTableRow();//row create
                    XRTableCell cell1 = new XRTableCell();
                    cell1.CanGrow = true;
                    cell1.WidthF = 179.7f;
                    cell1.Text = (rdr["Department"].ToString());
                    row1.Cells.Add(cell1);

                    XRTableCell cell2 = new XRTableCell();
                    cell2.CanGrow = true;
                    cell2.WidthF = 200.3f;
                    cell2.Text = (rdr["QualityEvent_TypeName"].ToString());
                    row1.Cells.Add(cell2);

                    XRTableCell cell3 = new XRTableCell();
                    cell3.CanGrow = true;
                    cell3.WidthF = 209.63f;
                    cell3.Text = (rdr["EventNumbers"].ToString());
                    row1.Cells.Add(cell3);

                    XRTableCell cell4 = new XRTableCell();
                    cell4.CanGrow = true;
                    cell4.WidthF = 193.18f;
                    //cell4.Text = (rdr["CAPA_Number"].ToString());
                    cell4.Text = (rdr["CAPA_Number"].ToString()) != "" ? (rdr["CAPA_Number"].ToString()) : "N/A";
                    row1.Cells.Add(cell4);
                    tblIncInvestigation.Rows.Add(row1);
                }
            }
            else
            {
                incReport1.FindControl("xrTableInvestigation", true).Visible = false;
            }
            tblIncInvestigation.EndInit();
            //end commentd by pradeep 20-03-2020

            //who are the people Involved
            DataSet dt_2 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incdntBO.IncidentMasterID);
            
            foreach (DataRow rdr in dt_2.Tables[2].Rows)
            {
                ((XRLabel)(incReport2.FindControl("xrlblWhatHappened_Desc", false))).Text = (rdr["WhatHappened_Desc"]).ToString();
                ((XRLabel)(incReport2.FindControl("xrlblWhenHappened_Desc", false))).Text = (rdr["WhenHappened_Desc"]).ToString();
                ((XRLabel)(incReport2.FindControl("xrlblWhereHappened_Desc", false))).Text = (rdr["WhereHappened_Desc"]).ToString();
                if (dt_2.Tables[2].Rows[0]["IsHasIncidentAfftectedDoc"].ToString() == "1")
                {
                    incdntBO.IsHasIncidentAfftectedDoc = "No";
                }
                else if (dt_2.Tables[2].Rows[0]["IsHasIncidentAfftectedDoc"].ToString() == "2")
                {
                    incdntBO.IsHasIncidentAfftectedDoc = "Yes";
                }
                else if (dt_2.Tables[2].Rows[0]["IsHasIncidentAfftectedDoc"].ToString() == "3")
                {
                    ((XRCheckBox)(incReport2.FindControl("xrcbxHasIncidentAffectDocNA", false))).Checked = true;
                    incdntBO.IsHasIncidentAfftectedDoc = "N/A";
                }
                ((XRLabel)(incReport2.FindControl("xrcbxHasIncidentAffect", false))).Text = incdntBO.IsHasIncidentAfftectedDoc;
                ((XRLabel)(incReport2.FindControl("xrlblConclusion_Desc", false))).Text = (rdr["Conclusion_Desc"]).ToString();
            }

            
            //Table 1 Who are the people involved
            //start commented by pradeep 20-03-2020 Not using Linq Code
            XRTable tblIncInvestigationWhoArePeopleInvolved = ((XRTable)(incReport2.FindControl("xrtblWhoArePeopleInvolvedReport2", true)));
            tblIncInvestigationWhoArePeopleInvolved.BeginInit();
            DataSet dt_3 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incdntBO.IncidentMasterID);

            //For Hiding Table Header if no data in Table
            if (dt_3.Tables[3].Rows.Count > 0)
            {

                foreach (DataRow rdr in dt_3.Tables[3].Rows)
                {
                    XRTableRow row1 = new XRTableRow();//row create
                    XRTableCell cell1 = new XRTableCell();
                    cell1.CanGrow = true;
                    cell1.Width = 314;
                    cell1.Text = (rdr["DepartmentName"]).ToString();
                    row1.Cells.Add(cell1);

                    XRTableCell cell2 = new XRTableCell();
                    cell2.CanGrow = true;
                    cell2.Width = 364;
                    cell2.Text = (rdr["EmpName"]).ToString();
                    row1.Cells.Add(cell2);
                    tblIncInvestigationWhoArePeopleInvolved.Rows.Add(row1);
                }
            }
            else
            {
                ((XRLabel)(incReport2.FindControl("xrtblWhoArePeopleInvolvedReportNA", false))).Text = "N/A";
                incReport2.FindControl("xrtblWhoArePeopleInvolvedReport2", true).Visible = false;
            }
            tblIncInvestigationWhoArePeopleInvolved.EndInit();

            //Table 2
            //For Binding Table of Impact Incident
            XRTable tblIncInvestigationImpactOfIncident = ((XRTable)(incReport2.FindControl("xrtblInvestigationImpactOfIncidient", true)));
            tblIncInvestigationImpactOfIncident.BeginInit();

            //Binding Data From DB for Update or Show.
            DataSet dt_4 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incdntBO.IncidentMasterID);

             foreach (DataRow rdr in dt_4.Tables[4].Rows)
            {
                XRTableRow row1 = new XRTableRow();//row create
                XRTableCell cell1 = new XRTableCell();
                cell1.CanGrow = true;
                cell1.Width = 183;
                cell1.Text = (rdr["ImpactName"]).ToString();
                row1.Cells.Add(cell1);

                XRTableCell cell2 = new XRTableCell();
                cell2.CanGrow = true;
                cell2.Width = 84;
                if (dt_4.Tables[4].Rows[0]["IsImpact"].ToString() == "1")
                {
                    cell2.Text = "NO";
                }
                else if (dt_4.Tables[4].Rows[0]["IsImpact"].ToString() == "2")
                {
                    cell2.Text = "Yes";
                }
                else if (dt_4.Tables[4].Rows[0]["IsImpact"].ToString() == "3")
                {
                    cell2.Text = "N/A";
                }
                row1.Cells.Add(cell2);

                XRTableCell cell3 = new XRTableCell();
                cell3.CanGrow = true;
                cell3.Width = 283;
                //cell3.Text = dr.ReferenceNo != null ? dr.ReferenceNo.ToString() : "N/A";
                cell3.Text = (rdr["ReferenceNo"]).ToString() != "" ? (rdr["ReferenceNo"]).ToString() : "N/A";
                row1.Cells.Add(cell3);

                XRTableCell cell4 = new XRTableCell();
                cell4.CanGrow = true;
                cell4.Width = 164;
                //cell4.Text = dr.ImpactComments != null ? dr.ImpactComments.ToString() : "N/A";
                cell4.Text = (rdr["ImpactComments"]).ToString() != "" ? (rdr["ImpactComments"]).ToString() : "N/A";
                row1.Cells.Add(cell4);

                tblIncInvestigationImpactOfIncident.Rows.Add(row1);
            }
            tblIncInvestigationImpactOfIncident.EndInit();

            
            //Table 3
            //Incident Affected On
            XRTable tblIncInvestigationAffectedOn = ((XRTable)(incReport2.FindControl("xrtblInvestigationAffectedOn", true)));
            tblIncInvestigationAffectedOn.BeginInit();
            //Binding Data From DB for Update or Show.
            DataSet dt_5 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incdntBO.IncidentMasterID);

            foreach (DataRow rdr in dt_5.Tables[5].Rows)
            {
                XRTableRow row1 = new XRTableRow();//row create
                XRTableCell cell1 = new XRTableCell();
                cell1.CanGrow = true;
                cell1.WidthF = 191.07F;
                cell1.Text = (rdr["EffectedName"]).ToString();
                row1.Cells.Add(cell1);
                XRTableCell cell2 = new XRTableCell();
                cell2.CanGrow = true;
                cell2.WidthF = 90.38F;
                if (dt_5.Tables[5].Rows[0]["IsEffect"].ToString() == "1")
                {
                    cell2.Text = "No";
                }
                if (dt_5.Tables[5].Rows[0]["IsEffect"].ToString() == "2")
                {
                    cell2.Text = "Yes";
                }
                if (dt_5.Tables[5].Rows[0]["IsEffect"].ToString() == "3")
                {
                    cell2.Text = "N/A";
                }
                row1.Cells.Add(cell2);
                XRTableCell cell3 = new XRTableCell();
                cell3.CanGrow = true;
                cell3.WidthF = 297.75F;
                //cell3.Text = dr.ReferenceNo != null ? dr.ReferenceNo.ToString() : "N/A";
                cell3.Text = (rdr["ReferenceNo"]).ToString() != "" ? (rdr["ReferenceNo"]).ToString() : "N/A";
                row1.Cells.Add(cell3);
                XRTableCell cell4 = new XRTableCell();
                cell4.CanGrow = true;
                cell4.WidthF = 173.03F;
                //cell4.Text = dr.EffectedComments != null ? dr.EffectedComments.ToString() : "N/A";
                cell4.Text = (rdr["EffectedComments"]).ToString() != "" ? (rdr["EffectedComments"]).ToString() : "N/A";
                row1.Cells.Add(cell4);
                tblIncInvestigationAffectedOn.Rows.Add(row1);
            }
            tblIncInvestigationAffectedOn.EndInit();

           
            //start commented on 08-04-2020
            //Table 4
            //For Binding Has the incident Affected Documents Table
            XRTable tblHasIncidentAffectedDocs = ((XRTable)(incReport2.FindControl("xrTableHasIncidentAffectDocs", true)));
            tblHasIncidentAffectedDocs.BeginInit();
            DataSet dt_6 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incdntBO.IncidentMasterID);

            if (dt_6.Tables[6].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt_6.Tables[6].Rows)
                {
                    XRTableRow row1 = new XRTableRow();//row create
                    XRTableCell cell1 = new XRTableCell();
                    cell1.CanGrow = true;
                    cell1.Width = Convert.ToInt32(139.98);
                    cell1.Text = (rdr["DepartmentName"].ToString());
                    row1.Cells.Add(cell1);

                    if (rdr["AffectedDocumentID"].ToString() == "0")//Others
                    {
                        XRTableCell cell2 = new XRTableCell();
                        cell2.CanGrow = true;
                        cell2.Width = Convert.ToInt32(151.69);
                        cell2.Text = (rdr["HasOtherDocName"].ToString());
                        row1.Cells.Add(cell2);

                        XRTableCell cell3 = new XRTableCell();
                        cell3.CanGrow = true;
                        cell3.Width = 130;
                        cell3.Text = (rdr["HasOtherRefNo"].ToString());
                        row1.Cells.Add(cell3);
                    }
                    else
                    {
                        XRTableCell cell2 = new XRTableCell();
                        cell2.CanGrow = true;
                        cell2.Width = Convert.ToInt32(151.69);
                        cell2.Text = (rdr["Title"].ToString());
                        row1.Cells.Add(cell2);

                        XRTableCell cell3 = new XRTableCell();
                        cell3.CanGrow = true;
                        cell3.Width = 130;
                        cell3.Text = (rdr["DocumentNumber"].ToString());
                        row1.Cells.Add(cell3);
                    }
                    if ((rdr["PageNo"].ToString()) != null)
                    {
                        XRTableCell cell4 = new XRTableCell();
                        cell4.CanGrow = true;
                        cell4.Width = Convert.ToInt32(95.17);
                        cell4.Text = (rdr["PageNo"].ToString());
                        row1.Cells.Add(cell4);
                        tblHasIncidentAffectedDocs.Rows.Add(row1);
                    }
                    else
                    {
                        XRTableCell cell4 = new XRTableCell();
                        cell4.CanGrow = true;
                        cell4.Width = Convert.ToInt32(95.17);
                        cell4.Text = "N/A";
                        row1.Cells.Add(cell4);
                        tblHasIncidentAffectedDocs.Rows.Add(row1);
                    }

                    XRTableCell cell5 = new XRTableCell();
                    cell5.CanGrow = true;
                    cell5.Width = Convert.ToInt32(270.17);
                    cell5.Text = (rdr["Comments"].ToString());
                    row1.Cells.Add(cell5);
                    tblHasIncidentAffectedDocs.Rows.Add(row1);
                }
            }
            else
            {
                incReport2.FindControl("xrTableHasIncidentAffectDocs", true).Visible = false;
            }
            tblHasIncidentAffectedDocs.EndInit();

          
            //Table 5
            //Table for Running Condition
            XRTable tblIncInvestigationRunningCondition = ((XRTable)(incReport2.FindControl("xrtblRunningCondition", true)));
            tblIncInvestigationRunningCondition.BeginInit();
            //Table binding For Running Condition
            DataSet dt_7 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incdntBO.IncidentMasterID);

            foreach (DataRow rdr in dt_7.Tables[7].Rows)
            {
                XRTableRow row1 = new XRTableRow();//row create
                XRTableCell cell1 = new XRTableCell();
                cell1.CanGrow = true;
                cell1.WidthF = 191.07F;
                cell1.Text = (rdr["EffectedName"]).ToString();
                row1.Cells.Add(cell1);
                XRTableCell cell2 = new XRTableCell();
                cell2.CanGrow = true;
                cell2.WidthF = 90.38F;
                if (dt_7.Tables[7].Rows[0]["IsEffect"].ToString() == "1")
                {
                    cell2.Text = "No";
                }
                if (dt_7.Tables[7].Rows[0]["IsEffect"].ToString() == "2")
                {
                    cell2.Text = "Yes";
                }
                if (dt_7.Tables[7].Rows[0]["IsEffect"].ToString() == "3")
                {
                    cell2.Text = "N/A";
                }
                row1.Cells.Add(cell2);
                XRTableCell cell3 = new XRTableCell();
                cell3.CanGrow = true;
                cell3.WidthF = 297.75F;
                cell3.Text = (rdr["ReferenceNo"]).ToString() != "" ? (rdr["ReferenceNo"]).ToString() : "NA";
                row1.Cells.Add(cell3);
                XRTableCell cell4 = new XRTableCell();
                cell4.CanGrow = true;
                cell4.WidthF = 173.03F;
                cell4.Text = (rdr["EffectedComments"]).ToString() != "" ? (rdr["EffectedComments"]).ToString() : "NA";
                row1.Cells.Add(cell4);
                tblIncInvestigationRunningCondition.Rows.Add(row1);
            }
            tblIncInvestigationRunningCondition.EndInit();

           
            //Table 6
            //start commented by pradeep 23-03-2020
            //For Table on Review
            XRTable tblIncInvestigationReview = ((XRTable)(incReport2.FindControl("xrtblReview", true)));
            tblIncInvestigationReview.BeginInit();

            DataSet dt_8 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incdntBO.IncidentMasterID);

            foreach (DataRow rdr in dt_8.Tables[8].Rows)
            {
                XRTableRow row1 = new XRTableRow();//row create
                XRTableCell cell1 = new XRTableCell();
                cell1.CanGrow = true;
                cell1.Width = 114;
                cell1.Text = (rdr["ReviewName"]).ToString();
                row1.Cells.Add(cell1);
                XRTableCell cell2 = new XRTableCell();
                cell2.CanGrow = true;
                cell2.Width = 110;
                if (dt_8.Tables[8].Rows[0]["isReview"].ToString() == "1")
                {
                    cell2.Text = "No";
                }
                if (dt_8.Tables[8].Rows[0]["isReview"].ToString() == "2")
                {
                    cell2.Text = "Yes";
                }
                if (dt_8.Tables[8].Rows[0]["isReview"].ToString() == "3")
                {
                    cell2.Text = "N/A";
                }
                row1.Cells.Add(cell2);
                XRTableCell cell3 = new XRTableCell();
                cell3.CanGrow = true;
                cell3.Width = 257;
                //cell3.Text = dr.ReferenceNo != null ? dr.ReferenceNo.ToString() : "N/A";
                cell3.Text = (rdr["ReferenceNo"]).ToString() != "" ? (rdr["ReferenceNo"]).ToString() : "N/A";
                row1.Cells.Add(cell3);
                XRTableCell cell4 = new XRTableCell();
                cell4.CanGrow = true;
                cell4.Width = 215;
                //cell4.Text = dr.ReviewComments != null ? dr.ReviewComments.ToString() : "N/A";
                cell4.Text = (rdr["ReviewComments"]).ToString() != "" ? (rdr["ReviewComments"]).ToString() : "N/A";
                row1.Cells.Add(cell4);
                tblIncInvestigationReview.Rows.Add(row1);
            }
            tblIncInvestigationReview.EndInit();

            //Table 7 
            //start commented by pradeep 23-03-2020
            // Table for List of Supporting Documents
            XRTable tblIncListOfSupportingDocs = ((XRTable)(incReport2.FindControl("xrtblListOfSupportingDocs", true)));
            tblIncListOfSupportingDocs.BeginInit();

            DataSet dt_9 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incdntBO.IncidentMasterID);
            if (dt_9.Tables[9].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt_9.Tables[9].Rows)
                {
                    XRTableRow row1 = new XRTableRow();//row create
                    XRTableCell cell1 = new XRTableCell();
                    cell1.CanGrow = true;
                    cell1.Width = 315;
                    //cell1.Text = item.InvestTypeName;
                    cell1.Text = (rdr["InvestTypeName"]).ToString();
                    row1.Cells.Add(cell1);

                    XRTableCell cell2 = new XRTableCell();
                    cell2.CanGrow = true;
                    cell2.Width = 382;
                    //cell2.Text = item.FileName;
                    cell2.Text = (rdr["FileName"]).ToString();
                    row1.Cells.Add(cell2);
                    tblIncListOfSupportingDocs.Rows.Add(row1);
                }
            }
            else
            {
                ((XRLabel)(incReport2.FindControl("xrtblListOfSupportingDocsNA", false))).Text = "N/A";
                incReport2.FindControl("xrtblListOfSupportingDocs", true).Visible = false;
            }
            tblIncListOfSupportingDocs.EndInit();


           
            //Table 8
            //start commented by pradeep 23-03-2020
            //Table for Others Department Hod Review
            XRTable tblIncOthersdeptHodReview = ((XRTable)(incReport2.FindControl("xrtblOtherDepHodReview", true)));
            tblIncOthersdeptHodReview.BeginInit();

            DataSet dt_10 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incdntBO.IncidentMasterID);

            if (dt_10.Tables[10].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt_10.Tables[10].Rows)
                {
                    XRTableRow row1 = new XRTableRow();//row create
                    XRTableCell cell1 = new XRTableCell();
                    cell1.CanGrow = true;
                    cell1.Width = 145;
                    cell1.Text = (rdr["DepartmentName"]).ToString();
                    row1.Cells.Add(cell1);

                    XRTableCell cell2 = new XRTableCell();
                    cell2.CanGrow = true;
                    cell2.Width = 131;
                    cell2.Text = (rdr["EmpName"]).ToString();
                    row1.Cells.Add(cell2);

                    XRTableCell cell3 = new XRTableCell();
                    cell3.CanGrow = true;
                    cell3.Width = 126;
                    if (dt_10.Tables[10].Rows[0]["ApproveStatus"].ToString() == "1")
                    {
                        cell3.Text = "N/A";
                    }
                    else
                    {
                        cell3.Text = Convert.ToDateTime(rdr["SubmittedDate"]).ToString("dd MMM yyyy HH:mm:ss");

                    }
                    row1.Cells.Add(cell3);

                    XRTableCell cell4 = new XRTableCell();
                    cell4.CanGrow = true;
                    cell4.Width = 112;
                    if (dt_10.Tables[10].Rows[0]["ApproveStatus"].ToString() == "1")
                    {
                        cell4.Text = "Pending";
                    }
                    if (dt_10.Tables[10].Rows[0]["ApproveStatus"].ToString() == "2")
                    {
                        cell4.Text = "Approved";
                    }
                    row1.Cells.Add(cell4);
                    XRTableCell cell5 = new XRTableCell();
                    cell5.CanGrow = true;
                    cell5.Width = 182;
                    cell5.Text = (rdr["HODComments"]).ToString();
                    row1.Cells.Add(cell5);
                    tblIncOthersdeptHodReview.Rows.Add(row1);
                }
            }
            else
            {
                ((XRLabel)(incReport2.FindControl("xrtblOtherDepHodReviewNA", false))).Text = "N/A";
                incReport2.FindControl("xrtblOtherDepHodReview", true).Visible = false;
            }
            tblIncOthersdeptHodReview.EndInit();

            //For Binding Investigator Name
            DataSet dt_25 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incdntBO.IncidentMasterID);
            ((XRLabel)(incReport2.FindControl("xrlblInvestName", false))).Text = dt_25.Tables[25].Rows[3]["EmpName"].ToString();

            //For Binding Designation Name for QA
            ((XRLabel)(incReport2.FindControl("xrlblInvestDesigName", false))).Text = "( " + dt_25.Tables[25].Rows[3]["DesignationName"].ToString() + " )";

            //For Binding Investigator Action Date
            ((XRLabel)(incReport2.FindControl("xrInvestCreatedDate", false))).Text = Convert.ToDateTime(dt_25.Tables[25].Rows[3]["ActionDate"]).ToString("dd MMM yyyy HH:mm:ss");

            /*Start Coding for Incident Section IV Immediated Correction done 02-07-2019*/
            //For Binding Immediate Correction Done
            ((XRLabel)(incReport2.FindControl("xrlblIsImmediateCorrectionReportNew", false))).Text = incdntBO.ImmediateCorrection;
            /*End Coding for Incident Section IV*/
        }

        public void InvestigationPage3(IncidentBO incdntBO, IncidentReportPage3 incReport3, IncidentReportPage4 incReport4, AizantIT_DevEntities dbEF)
        {
            //For Binding Table of Impact Incident
            XRTable tblIncInvestigationImpactOfIncidentNonImpacting = ((XRTable)(incReport3.FindControl("xrTableImpactOFIncidentForQualityNonImpacting1", true)));
            tblIncInvestigationImpactOfIncidentNonImpacting.BeginInit();
            //Binding Data From DB for Update or Show.

            DataSet dt_11 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incdntBO.IncidentMasterID);
            foreach (DataRow rdr in dt_11.Tables[11].Rows)
            {
                XRTableRow row1 = new XRTableRow();//row create
                XRTableCell cell1 = new XRTableCell();
                cell1.CanGrow = true;
                cell1.Width = 183;
                cell1.Text = (rdr["ImpactName"]).ToString();
                row1.Cells.Add(cell1);

                XRTableCell cell2 = new XRTableCell();
                cell2.CanGrow = true;
                cell2.Width = 84;
                if (dt_11.Tables[11].Rows[0]["IsImpact"].ToString() == "1")
                {
                    cell2.Text = "NO";
                }
                if (dt_11.Tables[11].Rows[0]["IsImpact"].ToString() == "2")
                {
                    cell2.Text = "Yes";
                }
                if (dt_11.Tables[11].Rows[0]["IsImpact"].ToString() == "3")
                {
                    cell2.Text = "N/A";
                }
                row1.Cells.Add(cell2);
                XRTableCell cell3 = new XRTableCell();
                cell3.CanGrow = true;
                cell3.Width = 283;
                //cell3.Text = dr.ReferenceNo != null ? dr.ReferenceNo.ToString() : "N/A";
                cell3.Text = (rdr["ReferenceNo"]).ToString() != "" ? (rdr["ReferenceNo"]).ToString() : "N/A";
                row1.Cells.Add(cell3);

                XRTableCell cell4 = new XRTableCell();
                cell4.CanGrow = true;
                cell4.Width = 164;
                cell4.Text = (rdr["ImpactComments"]).ToString() != "" ? (rdr["ImpactComments"]).ToString() : "N/A";
                row1.Cells.Add(cell4);
                tblIncInvestigationImpactOfIncidentNonImpacting.Rows.Add(row1);
            }
            tblIncInvestigationImpactOfIncidentNonImpacting.EndInit();

            DataSet dt_25 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incdntBO.IncidentMasterID);
            //For Binding Investigator Name
            ((XRLabel)(incReport3.FindControl("xrlblInvestName1", false))).Text = dt_25.Tables[25].Rows[3]["EmpName"].ToString();

            //For Binding Designation Name for QA
            ((XRLabel)(incReport3.FindControl("xrlblInvestDesigName1", false))).Text = "( " + dt_25.Tables[25].Rows[3]["DesignationName"].ToString() + " )";

            //For Binding Investigator Action Date
            ((XRLabel)(incReport3.FindControl("xrInvestCreatedDate1", false))).Text = Convert.ToDateTime(dt_25.Tables[25].Rows[3]["ActionDate"]).ToString("dd MMM yyyy HH:mm:ss");

            //For Binding Quality Non Impacting Description
            ((XRTableCell)(incReport3.FindControl("xrlbQNonImpactInvestandConclusion", false))).Text = incdntBO.QNonImpConclusion;
        }

        #region 31-07-2019 For Incident Event CAPA A4 Dynamic view
        public void IncidentCAPAs_Report(IncidentCAPA_Report incCAPA_Report, int incidentID, AizantIT_DevEntities dbEF)
        {
            //start commented by pradeep 23-03-2020
            QMS_BAL QmsBAL = new QMS_BAL();
            XRTable tblPlanningOfCAPAReportNew = ((XRTable)(incCAPA_Report.FindControl("xrTablePlanningOfTestingCAPA_Report", true)));
            //DataSet dt = QmsBAL.GetEventsCAPAListBAL(100, 0, 0, "", "", incidentID, 1, 0);
            DataSet ds_12 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incidentID);

            if (ds_12.Tables[12].Rows.Count > 0)
            {
                tblPlanningOfCAPAReportNew.BeginInit();
                for (int i = 0; i < ds_12.Tables[12].Rows.Count; i++)
                {
                    XRTableRow row1 = new XRTableRow();//row create
                    XRTableCell cell1 = new XRTableCell();

                    cell1.CanGrow = true;
                    cell1.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Bold);
                    cell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell1.Text = "CAPA Number : ";
                    cell1.WidthF = 150.5F;
                    cell1.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    //cell1.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right;
                    row1.Cells.Add(cell1);

                    XRTableCell cell2 = new XRTableCell();
                    cell2.CanGrow = true;
                    cell2.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Regular);
                    cell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell2.WidthF = 254.5F;
                    //cell2.Text = dt.Tables[0].Rows[i]["CAPA_Number"].ToString();
                    cell2.Text = ds_12.Tables[12].Rows[i]["CAPA_Number"].ToString();
                    cell2.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    cell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0);
                    row1.Cells.Add(cell2);


                    XRTableCell cell3 = new XRTableCell();
                    cell3.CanGrow = true;
                    cell3.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Bold);
                    cell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell3.WidthF = 100.5F;
                    cell3.Text = "Department : ";
                    cell3.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    row1.Cells.Add(cell3);

                    XRTableCell cell4 = new XRTableCell();
                    cell4.CanGrow = true;
                    cell4.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Regular);
                    cell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell4.WidthF = 304.5F;
                    //cell4.Text = dt.Tables[0].Rows[i]["Department"].ToString();
                    cell4.Text = ds_12.Tables[12].Rows[i]["Department"].ToString();
                    cell4.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    cell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0);
                    row1.Cells.Add(cell4);

                    XRTableRow row2 = new XRTableRow();//row create

                    XRTableCell cell5 = new XRTableCell();
                    cell5.CanGrow = true;
                    cell5.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Bold);
                    cell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell5.WidthF = 150.5F;
                    cell5.Text = "Type of Action : ";
                    cell5.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    row2.Cells.Add(cell5);

                    XRTableCell cell6 = new XRTableCell();
                    cell6.CanGrow = true;
                    cell6.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Regular);
                    cell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell6.WidthF = 254.5F;
                    //cell6.Text = dt.Tables[0].Rows[i]["PlanofAction"].ToString();
                    cell6.Text = ds_12.Tables[12].Rows[i]["PlanofAction"].ToString();
                    cell6.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    cell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0);
                    row2.Cells.Add(cell6);

                    XRTableCell cell7 = new XRTableCell();
                    cell7.CanGrow = true;
                    cell7.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Bold);
                    cell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell7.WidthF = 100.5F;
                    cell7.Text = "Target Date : ";
                    cell7.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    row2.Cells.Add(cell7);

                    XRTableCell cell8 = new XRTableCell();
                    cell8.CanGrow = true;
                    cell8.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Regular);
                    cell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell8.WidthF = 304.5F;
                    //cell8.Text = Convert.ToDateTime(dt.Tables[0].Rows[i]["TargetDate"]).ToString("dd MMM yyyy");
                    cell8.Text = Convert.ToDateTime(ds_12.Tables[12].Rows[i]["TargetDate"]).ToString("dd MMM yyyy");
                    cell8.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    cell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0);
                    row2.Cells.Add(cell8);

                    XRTableRow row3 = new XRTableRow();//row create

                    XRTableCell cell9 = new XRTableCell();
                    cell9.CanGrow = true;
                    cell9.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Bold);
                    cell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell9.WidthF = 150.5F;
                    cell9.Text = "Status : ";
                    cell9.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    row3.Cells.Add(cell9);

                    XRTableCell cell10 = new XRTableCell();
                    cell10.CanGrow = true;
                    cell10.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Regular);
                    cell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell10.WidthF = 659.5F;
                    //cell10.Text = dt.Tables[0].Rows[i]["CurrentStatus"].ToString().Trim();
                    cell10.Text = ds_12.Tables[12].Rows[i]["CurrentStatus"].ToString().Trim();
                    cell10.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    cell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0);
                    row3.Cells.Add(cell10);

                    XRTableRow row4 = new XRTableRow();
                    XRTableCell cell11 = new XRTableCell();
                    cell11.CanGrow = true;
                    cell11.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Bold);
                    cell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell11.WidthF = 150.5F;
                    cell11.Text = "Responsible Person : ";
                    cell11.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    row4.Cells.Add(cell11);

                    XRTableCell cell12 = new XRTableCell();
                    cell12.CanGrow = true;
                    cell12.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Regular);
                    cell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell12.WidthF = 659.5F;
                    //cell12.Text = dt.Tables[0].Rows[i]["Responsible_Persons"].ToString();
                    cell12.Text = ds_12.Tables[12].Rows[i]["Responsible_Persons"].ToString();
                    cell12.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    cell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0);
                    row4.Cells.Add(cell12);

                    XRTableRow row5 = new XRTableRow();

                    XRTableCell cell13 = new XRTableCell();
                    cell13.CanGrow = true;
                    cell13.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Bold);
                    cell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell13.WidthF = 150.5F;
                    cell13.Text = "Description : ";
                    cell13.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    row5.Cells.Add(cell13);

                    XRTableCell cell14 = new XRTableCell();
                    cell14.CanGrow = true;
                    cell14.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Regular);
                    cell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell14.WidthF = 659.5F;
                    //cell14.Text = dt.Tables[0].Rows[i]["CAPA_Discrption"].ToString();
                    cell14.Text = ds_12.Tables[12].Rows[i]["Capa_ActionDesc"].ToString();
                    cell14.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    cell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0);
                    row5.Cells.Add(cell14);

                    //For giving empty space for row
                    XRTableRow row6 = new XRTableRow();

                    XRTableCell cell15 = new XRTableCell();
                    cell15.CanGrow = true;
                    //cell15.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right;
                    if (ds_12.Tables[12].Rows.Count != (i + 1))
                        cell15.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                    else
                        cell15.Borders = DevExpress.XtraPrinting.BorderSide.None;

                    cell15.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Regular);
                    cell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell15.WidthF = 202.5F;
                    cell15.Text = "  ";

                    XRTableCell cell16 = new XRTableCell();
                    cell16.CanGrow = true;
                    if (ds_12.Tables[12].Rows.Count != (i + 1))
                        cell16.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                    else
                        cell16.Borders = DevExpress.XtraPrinting.BorderSide.None;
                    cell16.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Regular);
                    cell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell16.WidthF = 607.5F;
                    cell16.Text = "  ";
                    row6.Cells.Add(cell16);
                    //end


                    tblPlanningOfCAPAReportNew.Rows.Add(row1);
                    tblPlanningOfCAPAReportNew.Rows.Add(row2);
                    tblPlanningOfCAPAReportNew.Rows.Add(row3);
                    tblPlanningOfCAPAReportNew.Rows.Add(row4);
                    tblPlanningOfCAPAReportNew.Rows.Add(row5);
                    tblPlanningOfCAPAReportNew.Rows.Add(row6);

                }
                tblPlanningOfCAPAReportNew.EndInit();
            }
        }
        #endregion


        #region 01-07-2019 For Incident Event CAPA New Report page
        //This code is not using 
        //public void IncidentReportCapa(IncidentBO incdntBO, IncidentReportCAPA incidentReportCAPA, int incidentID)
        //{
        //    QMS_BAL QmsBAL = new QMS_BAL();
        //    DataSet dt = QmsBAL.GetEventsCAPAListBAL(100, 0, 0, "", "", incidentID, 1, 0);
        //    XRTable tblPlanningOfCAPAReportNew = ((XRTable)(incidentReportCAPA.FindControl("xrTablePlanningOfCAPA_Report", true)));
        //    if (dt.Tables[0].Rows.Count > 0)
        //    {
        //        tblPlanningOfCAPAReportNew.BeginInit();
        //        for (int i = 0; i < dt.Tables[0].Rows.Count; i++)
        //        {
        //            XRTableRow row1 = new XRTableRow();//row create
        //            XRTableCell cell1 = new XRTableCell();
        //            cell1.CanGrow = true;
        //            cell1.Width = 140;
        //            cell1.Text = dt.Tables[0].Rows[i]["CAPA_Number"].ToString();/*==null ? "N/A" : dt.Tables[0].Rows[i]["CAPA_Number"].ToString()*/
        //            row1.Cells.Add(cell1);

        //            XRTableCell cell2 = new XRTableCell();
        //            cell2.CanGrow = true;
        //            cell2.Width = 204;
        //            cell2.Text = dt.Tables[0].Rows[i]["Department"].ToString();
        //            row1.Cells.Add(cell2);

        //            XRTableCell cell3 = new XRTableCell();
        //            cell3.CanGrow = true;
        //            cell3.Width = 172;
        //            cell3.Text = dt.Tables[0].Rows[i]["PlanofAction"].ToString();
        //            row1.Cells.Add(cell3);

        //            XRTableCell cell4 = new XRTableCell();
        //            cell4.CanGrow = true;
        //            cell4.Width = 227;
        //            cell4.Text = dt.Tables[0].Rows[i]["Responsible_Persons"].ToString();

        //            //cell4.Text = Convert.ToDateTime(dt.Tables[0].Rows[i]["TargetDate"]).ToString("dd MMM yyyy");
        //            row1.Cells.Add(cell4);

        //            XRTableCell cell5 = new XRTableCell();
        //            cell5.CanGrow = true;
        //            cell5.Width = 456;
        //            cell5.Text = dt.Tables[0].Rows[i]["CAPA_Discrption"].ToString();
        //            row1.Cells.Add(cell5);

        //            XRTableCell cell6 = new XRTableCell();
        //            cell6.CanGrow = true;
        //            cell6.Width = 120;
        //            cell6.Font = new Font("Times New Roman", 9f);
        //            cell6.Text = Convert.ToDateTime(dt.Tables[0].Rows[i]["TargetDate"]).ToString("dd MMM yyyy");
        //            row1.Cells.Add(cell6);

        //            XRTableCell cell7 = new XRTableCell();
        //            cell7.CanGrow = true;
        //            cell7.Width = 176;
        //            cell7.Text = dt.Tables[0].Rows[i]["CurrentStatus"].ToString().Trim();
        //            row1.Cells.Add(cell7);

        //            tblPlanningOfCAPAReportNew.Rows.Add(row1);
        //        }

        //        tblPlanningOfCAPAReportNew.EndInit();
        //    }
        //}
        #endregion

        #region 02-07-2019 For Incident Section V , hide and show based on CAPA
        public void IncidentSectionVReport(IncidentBO incidentBO, IR_SectionFive_Report incSectionV, AizantIT_DevEntities dbEF)
        {
            DataSet dt_25 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incidentBO.IncidentMasterID);

            //For Binding Is Training Required
            var IsAnyTrainingReq = "";
            if (incidentBO.IsAnyTrainingRequired == 1)
            {
                IsAnyTrainingReq = "No";
            }
            if (incidentBO.IsAnyTrainingRequired == 2)
            {
                IsAnyTrainingReq = "Yes";
            }
            if (incidentBO.IsAnyTrainingRequired == 3)
            {
                IsAnyTrainingReq = "N/A";
            }
        ((XRLabel)(incSectionV.FindControl("xrlblAnyTrainingRequired", false))).Text = IsAnyTrainingReq;

            //For Binding Evaluation and disposition
            DataSet dt_20 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incidentBO.IncidentMasterID);
            incidentBO.EvaluationandDispostion = dt_20.Tables[20].Rows[0]["EvalutionAndDisposition_Desc"].ToString();
            ((XRLabel)(incSectionV.FindControl("xrlblEvaluationandDispostionReport4", false))).Text = dt_20.Tables[20].Rows[0]["EvalutionAndDisposition_Desc"].ToString();

            //For Binding Hod Name after Action Plan
            //Modified Current Status with Enums (cs="12" || cs="13" || cs="16")19-06-2019
            if (incidentBO.CurrentStatus == ((int)Incident_Status.HOD_Approved_and_added_ActionPlan_Training).ToString()
                || incidentBO.CurrentStatus == ((int)Incident_Status.Incd_with_Invest_Approved_by_QA).ToString()
                || incidentBO.CurrentStatus == ((int)Incident_Status.Incd_with_Invest_Approved_by_HeadQA).ToString()
                || incidentBO.CurrentStatus == ((int)Incident_Status.Verified_by_QA).ToString()) //Current Status= "12", "13", "16"
            {
                ((XRLabel)(incSectionV.FindControl("xrLblHODNameActionPlan", false))).Text = dt_25.Tables[25].Rows[1]["EmpName"].ToString();
                //For Binding Designation Name for HOD after Action Plan
                ((XRLabel)(incSectionV.FindControl("xrLblHODDesigActionPlan", false))).Text = "( " + dt_25.Tables[25].Rows[1]["DesignationName"].ToString() + " )";
            }
                
                //For Binding Hod Action Date after Action Plan
                ((XRLabel)(incSectionV.FindControl("xrHodCreatedDateActionPlan", false))).Text = Convert.ToDateTime(dt_25.Tables[25].Rows[4]["ActionDate"]).ToString("dd MMM yyyy HH:mm:ss");

            //For Binding QA Name after Action Plan
            //Modified Current Status with Enums (cs="13" || cs="16")19-06-2019
            if (incidentBO.CurrentStatus == ((int)Incident_Status.Incd_with_Invest_Approved_by_QA).ToString()
                || incidentBO.CurrentStatus == ((int)Incident_Status.Incd_with_Invest_Approved_by_HeadQA).ToString()
                || incidentBO.CurrentStatus == ((int)Incident_Status.Verified_by_QA).ToString()) //Current Status= "13", "16"
            {
                ((XRLabel)(incSectionV.FindControl("xrLblQANameActionPlan", false))).Text = dt_25.Tables[25].Rows[2]["EmpName"].ToString();
                //For Binding Designation Name for QA after Action Plan
                ((XRLabel)(incSectionV.FindControl("xrLblQADesigActionPlan", false))).Text = "( " + dt_25.Tables[25].Rows[2]["DesignationName"].ToString() + " )";

            }

            //For Binding Action Date for QA after Action Plan
            ((XRLabel)(incSectionV.FindControl("xrQACreatedDateActionPlan", false))).Text = Convert.ToDateTime(dt_25.Tables[25].Rows[5]["ActionDate"]).ToString("dd MMM yyyy HH:mm:ss");
            //For Binding HeadQA Name after Action Plan
            ((XRLabel)(incSectionV.FindControl("xrLblHeadQANameActionPlan", false))).Text = dt_25.Tables[25].Rows[6]["EmpName"].ToString();
            //For Binding Designation Name for HeadQA after Action Plan
            ((XRLabel)(incSectionV.FindControl("xrLblHeadQADesigActionPlan", false))).Text = "( " + dt_25.Tables[25].Rows[6]["DesignationName"].ToString() + " )";
            //For Binding HeadQA Action Date after Action Plan
            ((XRLabel)(incSectionV.FindControl("xrHeadQACreatedDateActionPlan", false))).Text = Convert.ToDateTime(dt_25.Tables[25].Rows[6]["ActionDate"]).ToString("dd MMM yyyy HH:mm:ss");

        }
        #endregion

        public void InvestigationPage4(IncidentBO incdntBO, IncidentReportPage4 incReport4, AizantIT_DevEntities dbEF)
        {
            DataSet dt_13 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incdntBO.IncidentMasterID);
            int NoOfCAPAs = Convert.ToInt32(dt_13.Tables[13].Rows[0]["CAPA_Count"]);
            if (NoOfCAPAs == 0)
            {
                ((XRLabel)(incReport4.FindControl("xrtblPlanningOfCAPAReport4NA", false))).Text = "N/A";
                incReport4.FindControl("xrtblPlanningOfCAPAReport4", true).Visible = false;
            }
           
            //For Binding Immediate Correction Done
            ((XRLabel)(incReport4.FindControl("xrlblIsImmediateCorrectionReport4", false))).Text = incdntBO.ImmediateCorrection;

            //For Binding Is Training Required
            var IsAnyTrainingReq = "";
            if (incdntBO.IsAnyTrainingRequired == 1)
            {
                IsAnyTrainingReq = "No";
            }
            if (incdntBO.IsAnyTrainingRequired == 2)
            {
                IsAnyTrainingReq = "Yes";
            }
            if (incdntBO.IsAnyTrainingRequired == 3)
            {
                IsAnyTrainingReq = "N/A";
            }
                ((XRLabel)(incReport4.FindControl("xrlblAnyTrainingRequired", false))).Text = IsAnyTrainingReq;

            DataSet dt_20 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incdntBO.IncidentMasterID);
            //incdntBO.EvaluationandDispostion = dt_20.Tables[20].Rows[0]["EmpName"].ToString();
            incdntBO.EvaluationandDispostion = dt_20.Tables[20].Rows[0]["EvalutionAndDisposition_Desc"].ToString();
            ((XRLabel)(incReport4.FindControl("xrlblEvaluationandDispostionReport4", false))).Text = dt_20.Tables[20].Rows[0]["EvalutionAndDisposition_Desc"].ToString();

            //For Binding Hod Name after Action Plan
            //Modified Current Status with Enums (cs="12" || cs="13" || cs="16")19-06-2019
            DataSet dt_25 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incdntBO.IncidentMasterID);
            if (incdntBO.CurrentStatus == ((int)Incident_Status.HOD_Approved_and_added_ActionPlan_Training).ToString() || incdntBO.CurrentStatus == ((int)Incident_Status.Incd_with_Invest_Approved_by_QA).ToString() || incdntBO.CurrentStatus == ((int)Incident_Status.Incd_with_Invest_Approved_by_HeadQA).ToString()) //Current Status= "12", "13", "16"
            {
                ((XRLabel)(incReport4.FindControl("xrLblHODNameActionPlan", false))).Text = dt_25.Tables[25].Rows[2]["EmpName"].ToString();
                //For Binding Designation Name for HOD after Action Plan
                ((XRLabel)(incReport4.FindControl("xrLblHODDesigActionPlan", false))).Text = "( " + dt_25.Tables[25].Rows[2]["DesignationName"].ToString() + " )";
            }

            //For Binding Hod Action Date after Action Plan
            ((XRLabel)(incReport4.FindControl("xrHodCreatedDateActionPlan", false))).Text = Convert.ToDateTime(dt_25.Tables[25].Rows[4]["ActionDate"]).ToString("dd MMM yyyy HH:mm:ss");

            //For Binding QA Name after Action Plan
            //Modified Current Status with Enums (cs="13" || cs="16")19-06-2019
            if (incdntBO.CurrentStatus == ((int)Incident_Status.Incd_with_Invest_Approved_by_QA).ToString() || incdntBO.CurrentStatus == ((int)Incident_Status.Incd_with_Invest_Approved_by_HeadQA).ToString()) //Current Status= "13", "16"
            {
                ((XRLabel)(incReport4.FindControl("xrLblQANameActionPlan", false))).Text = dt_25.Tables[25].Rows[2]["EmpName"].ToString();
                //For Binding Designation Name for QA after Action Plan
                ((XRLabel)(incReport4.FindControl("xrLblQADesigActionPlan", false))).Text = "( " + dt_25.Tables[25].Rows[2]["DesignationName"].ToString() + " )";
            }

            //For Binding Action Date for QA after Action Plan
            ((XRLabel)(incReport4.FindControl("xrQACreatedDateActionPlan", false))).Text = Convert.ToDateTime(dt_25.Tables[25].Rows[5]["ActionDate"]).ToString("dd MMM yyyy HH:mm:ss");
            //For Binding HeadQA Name after Action Plan
            ((XRLabel)(incReport4.FindControl("xrLblHeadQANameActionPlan", false))).Text = dt_25.Tables[25].Rows[6]["EmpName"].ToString();
            //For Binding Designation Name for HeadQA after Action Plan
            ((XRLabel)(incReport4.FindControl("xrLblHeadQADesigActionPlan", false))).Text = "( " + dt_25.Tables[25].Rows[6]["DesignationName"].ToString() + " )";
            //For Binding HeadQA Action Date after Action Plan
            //For Binding HeadQA Action Date after Action Plan
            ((XRLabel)(incReport4.FindControl("xrHeadQACreatedDateActionPlan", false))).Text = Convert.ToDateTime(dt_25.Tables[25].Rows[6]["ActionDate"]).ToString("dd MMM yyyy HH:mm:ss");
        }

        public void TTSIncidentReportInvestigationPage5(IncidentBO incdntBO, TTSIncidentReport incTTSReport, AizantIT_DevEntities dbEF)
        {
            var TypeOfTraining = "";

            //For Hiding and showing TTS Message in TTS Report
            if(incdntBO.IsAnyTrainingRequired == 1)
            {
                incTTSReport.FindControl("xrLabelTTSMessage", true).Visible = true;
            }
            else
            {
                incTTSReport.FindControl("xrLabelTTSMessage", true).Visible = false;
            }

            EmployeID(incdntBO);
            int CurrentHistoryStatus = 0, TargetCurrentStatus = 0, TTS_ID = 0; ;
            QMS_BAL objQMS_BAL = new QMS_BAL();
            DataSet ds_14 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incdntBO.IncidentMasterID);

            //DataTable dtGetTTsID = objQMS_BAL.GetTTSIDBal(incdntBO.IncidentMasterID);
            if (ds_14.Tables[14].Rows.Count > 0)
            {
                //TTS_ID = Convert.ToInt32(dtGetTTsID.Rows[0]["TTS_ID"].ToString());
                //DataSet dsTTS_Details = objQMS_BAL.GetTargetTrainingDetails(TTS_ID.ToString(), Convert.ToInt32(incdntBO.EmpID), out CurrentHistoryStatus, out TargetCurrentStatus);
                ((XRLabel)(incTTSReport.FindControl("xrlblTTSDepartment", false))).Text = ds_14.Tables[14].Rows[0]["Department"].ToString();
                ((XRLabel)(incTTSReport.FindControl("xrlblTTSDocumentType", false))).Text = ds_14.Tables[14].Rows[0]["DocumentType"].ToString();
                ((XRLabel)(incTTSReport.FindControl("xrlblTTSDocumentName", false))).Text = ds_14.Tables[14].Rows[0]["Document"].ToString();
                ((XRLabel)(incTTSReport.FindControl("xrlblTTSTargetTrainingType", false))).Text = "General";
                TypeOfTraining = ds_14.Tables[14].Rows[0]["TypeOfTraining"].ToString();
                if (TypeOfTraining == "I")
                {
                    ((XRLabel)(incTTSReport.FindControl("xrlblTTSTypeofTraining", false))).Text = "Internal";
                }
                if (TypeOfTraining == "E")
                {
                    ((XRLabel)(incTTSReport.FindControl("xrlblTTSTypeofTraining", false))).Text = "External";
                }
                ((XRLabel)(incTTSReport.FindControl("xrlblTTSDueDate", false))).Text = Convert.ToDateTime(ds_14.Tables[14].Rows[0]["DueDate"]).ToString("dd MMM yyyy");
                //In TTS Current Status column is not showing
                //((XRLabel)(incTTSReport.FindControl("xrlblTTSCurrentStatus", false))).Text = dsTTS_Details.Tables[0].Rows[0]["CurrentStatus"].ToString();
                ((XRLabel)(incTTSReport.FindControl("xrlblTTSReviewerName", false))).Text = ds_14.Tables[14].Rows[0]["ReviewerName"].ToString();
                ((XRLabel)(incTTSReport.FindControl("xrlblTTSAuthorName", false))).Text = ds_14.Tables[14].Rows[0]["AuthorName"].ToString();

                XRTable tblTTSTraierTable = ((XRTable)(incTTSReport.FindControl("xrtblTTSTrainerTable", true)));
                tblTTSTraierTable.BeginInit();

                DataSet dt_15 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incdntBO.IncidentMasterID);

                foreach (DataRow rdr1 in dt_15.Tables[15].Rows)
                {
                    XRTableRow row1 = new XRTableRow();//row create
                    XRTableCell cell1 = new XRTableCell();
                    cell1.CanGrow = true;
                    cell1.Width = 152;
                    cell1.Text = (rdr1["EmpCode"].ToString());

                    row1.Cells.Add(cell1);

                    XRTableCell cell2 = new XRTableCell();
                    cell2.CanGrow = true;
                    cell2.Width = 264;
                    //cell2.Text = dtRow["DepartmentName"].ToString();
                    cell2.Text = (rdr1["DepartmentName"].ToString());
                    row1.Cells.Add(cell2);

                    XRTableCell cell3 = new XRTableCell();
                    cell3.CanGrow = true;
                    cell3.Width = 260;
                    //cell3.Text = dtRow["EmpName"].ToString();
                    cell3.Text = (rdr1["EmpName"].ToString());
                    row1.Cells.Add(cell3);
                    tblTTSTraierTable.Rows.Add(row1);
                }
                tblTTSTraierTable.EndInit();
                //For Binding Trainee List Table
                XRTable tblTTSTrainingTable = ((XRTable)(incTTSReport.FindControl("xrtblTTSTrainingTable", true)));
                tblTTSTrainingTable.BeginInit();

                DataSet dt_16 = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incdntBO.IncidentMasterID);

                foreach (DataRow rdr2 in dt_16.Tables[16].Rows)
                {
                    XRTableRow row1 = new XRTableRow();//row create
                    XRTableCell cell1 = new XRTableCell();
                    cell1.CanGrow = true;
                    cell1.Width = 152;
                    //cell1.Text = dtRow["EmpCode"].ToString();
                    cell1.Text = (rdr2["EmpCode"].ToString());
                    row1.Cells.Add(cell1);

                    XRTableCell cell2 = new XRTableCell();
                    cell2.CanGrow = true;
                    cell2.Width = 264;
                    //cell2.Text = dtRow["DepartmentName"].ToString();
                    cell2.Text = (rdr2["DepartmentName"].ToString());
                    row1.Cells.Add(cell2);

                    XRTableCell cell3 = new XRTableCell();
                    cell3.CanGrow = true;
                    cell3.Width = 260;
                    //cell3.Text = dtRow["EmpName"].ToString();
                    cell3.Text = (rdr2["EmpName"].ToString());
                    row1.Cells.Add(cell3);
                    tblTTSTrainingTable.Rows.Add(row1);
                }
                tblTTSTrainingTable.EndInit();
            }
        }

        public void IncidentReportFileAttachmentPage(IncidentBO incdntBO, IncidentReportFileAttachmentPage incFileReport, AizantIT_DevEntities dbEF)
        {
            QMS_BAL objQMS_BAL = new QMS_BAL();
            //DataSet dsGetIncFileAttch = objQMS_BAL.GetIncidentFileNameBal(incdntBO.IncidentMasterID);
            DataSet dsGetIncFileAttch = Qms_bal.GetIncident_Report_From_SingleSP_BAL(incdntBO.IncidentMasterID);

            if (dsGetIncFileAttch.Tables[17].Rows.Count == 0 && dsGetIncFileAttch.Tables[18].Rows.Count == 0 && dsGetIncFileAttch.Tables[19].Rows.Count == 0)
            {
                incFileReport.Visible = false;
            }
            else
            {
                //For Binding InitatorFile Attachment Table
                XRTable tblInitiatorFileAttachTable = ((XRTable)(incFileReport.FindControl("xrTableInitiatorFileAttachments", true)));
                tblInitiatorFileAttachTable.BeginInit();
                DataTable dtInitiatorTable = dsGetIncFileAttch.Tables[17];
                if (dtInitiatorTable.Rows.Count == 0)
                {
                    incFileReport.FindControl("xrLabelInitiatorFileAttachments", true).Visible = false;
                    incFileReport.FindControl("xrTableInitiatorFileAttachments", true).Visible = false;
                }
                foreach (DataRow dtRow in dtInitiatorTable.Rows)
                {
                    XRTableRow row1 = new XRTableRow();//row create
                    XRTableCell cell1 = new XRTableCell();
                    cell1.CanGrow = true;
                    cell1.Width = 43;
                    cell1.Text = dtRow["Sno"].ToString();
                    row1.Cells.Add(cell1);

                    XRTableCell cell2 = new XRTableCell();
                    cell2.CanGrow = true;
                    cell2.Width = 202;
                    cell2.Text = dtRow["EmpName"].ToString();
                    row1.Cells.Add(cell2);

                    XRTableCell cell3 = new XRTableCell();
                    cell3.CanGrow = true;
                    cell3.Width = 131;
                    cell3.Text = dtRow["RoleName"].ToString();
                    row1.Cells.Add(cell3);

                    XRTableCell cell4 = new XRTableCell();
                    cell4.CanGrow = true;
                    cell4.Width = 324;
                    cell4.Text = dtRow["FileName"].ToString();
                    row1.Cells.Add(cell4);

                    tblInitiatorFileAttachTable.Rows.Add(row1);
                }
                tblInitiatorFileAttachTable.EndInit();

                //For Binding Investigator File Attachment Table
                XRTable lblInvestigatorFileAttachLable = ((XRTable)(incFileReport.FindControl("xrLabelInvestigatorFileAttachments", true)));
                XRTable tblInvestigatorFileAttachTable = ((XRTable)(incFileReport.FindControl("xrTableInvestigatorFileAttachments", true)));
                //if (dtInitiatorTable.Rows.Count == 0)
                //{
                //    lblInvestigatorFileAttachLable.LocationF = new PointF(29.79F, 0F);
                //    tblInvestigatorFileAttachTable.LocationF = new PointF(29.79F, 44.79F);
                //}
                tblInvestigatorFileAttachTable.BeginInit();
                DataTable dtInvestigatorTable = dsGetIncFileAttch.Tables[18];
                if (dtInvestigatorTable.Rows.Count == 0)
                {
                    incFileReport.FindControl("xrLabelInvestigatorFileAttachments", true).Visible = false;
                    incFileReport.FindControl("xrTableInvestigatorFileAttachments", true).Visible = false;
                }
                foreach (DataRow dtRow in dtInvestigatorTable.Rows)
                {
                    XRTableRow row1 = new XRTableRow();//row create
                    XRTableCell cell1 = new XRTableCell();
                    cell1.CanGrow = true;
                    cell1.Width = 43;
                    cell1.Text = dtRow["Sno"].ToString();
                    row1.Cells.Add(cell1);

                    XRTableCell cell2 = new XRTableCell();
                    cell2.CanGrow = true;
                    cell2.Width = 202;
                    cell2.Text = dtRow["EmpName"].ToString();
                    row1.Cells.Add(cell2);

                    XRTableCell cell3 = new XRTableCell();
                    cell3.CanGrow = true;
                    cell3.Width = 131;
                    cell3.Text = dtRow["RoleName"].ToString();
                    row1.Cells.Add(cell3);

                    XRTableCell cell4 = new XRTableCell();
                    cell4.CanGrow = true;
                    cell4.Width = 324;
                    cell4.Text = dtRow["FileName"].ToString();
                    row1.Cells.Add(cell4);
                    tblInvestigatorFileAttachTable.Rows.Add(row1);
                }
                tblInvestigatorFileAttachTable.EndInit();

                //For Binding CAPA File Attachmets Table
                XRTable lblCAPAFileAttachLable = ((XRTable)(incFileReport.FindControl("xrLabelCAPAFileAttachments", true)));
                XRTable tblCAPAFileAttachTable = ((XRTable)(incFileReport.FindControl("xrTableCAPAFileAttachments", true)));
                tblCAPAFileAttachTable.BeginInit();
                DataTable dtCompleteCAPATable = dsGetIncFileAttch.Tables[19];
                if (dtCompleteCAPATable.Rows.Count == 0)
                {
                    incFileReport.FindControl("xrLabelCAPAFileAttachments", true).Visible = false;
                    incFileReport.FindControl("xrTableCAPAFileAttachments", true).Visible = false;
                }
                foreach (DataRow dtRow in dtCompleteCAPATable.Rows)
                {
                    XRTableRow row1 = new XRTableRow();//row create
                    XRTableCell cell1 = new XRTableCell();
                    cell1.CanGrow = true;
                    cell1.WidthF = 45.34f;
                    cell1.Text = dtRow["Sno"].ToString();
                    row1.Cells.Add(cell1);

                    XRTableCell cell2 = new XRTableCell();
                    cell2.CanGrow = true;
                    cell2.WidthF = 211.56f;
                    cell2.Text = dtRow["CAPA_Number"].ToString();
                    row1.Cells.Add(cell2);

                    XRTableCell cell3 = new XRTableCell();
                    cell3.CanGrow = true;
                    cell3.WidthF = 477.81f;
                    cell3.Text = dtRow["FileName"].ToString();
                    row1.Cells.Add(cell3);

                    tblCAPAFileAttachTable.Rows.Add(row1);
                }
                tblCAPAFileAttachTable.EndInit();
                //end

                if (dtInitiatorTable.Rows.Count == 0 && dtInvestigatorTable.Rows.Count != 0)
                {
                    //lblInvestigatorFileAttachLable.LocationF = new PointF(29.79F, 0F);
                    //tblInvestigatorFileAttachTable.LocationF = new PointF(29.79F, 44.79F);

                    //lblCAPAFileAttachLable.LocationF = new PointF(29.79F, 111.46F);
                    //tblCAPAFileAttachTable.LocationF = new PointF(29.79F, 155.21F);

                    lblInvestigatorFileAttachLable.LocationF = new PointF(0F, 0F);
                    tblInvestigatorFileAttachTable.LocationF = new PointF(0F, 44.79F);

                    lblCAPAFileAttachLable.LocationF = new PointF(0F, 111.46F);
                    tblCAPAFileAttachTable.LocationF = new PointF(0F, 155.21F);
                }

                if (dtInvestigatorTable.Rows.Count == 0 && dtInitiatorTable.Rows.Count != 0)
                {
                    //lblCAPAFileAttachLable.LocationF = new PointF(29.79F, 111.46F);
                    //tblCAPAFileAttachTable.LocationF = new PointF(29.79F, 155.21F);

                    lblCAPAFileAttachLable.LocationF = new PointF(0F, 111.46F);
                    tblCAPAFileAttachTable.LocationF = new PointF(0F, 155.21F);
                }

                if (dtInitiatorTable.Rows.Count == 0 && dtInvestigatorTable.Rows.Count == 0)
                {
                    //lblCAPAFileAttachLable.LocationF = new PointF(29.79F, 0F);
                    //tblCAPAFileAttachTable.LocationF = new PointF(29.79F, 44.79F);

                    lblCAPAFileAttachLable.LocationF = new PointF(0F, 0F);
                    tblCAPAFileAttachTable.LocationF = new PointF(0F, 44.79F);
                }
            }

        }

        //Bind Department Dropdown for Incident Report
        public void BindDropdownForIncidentReports()
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                List<SelectListItem> DepRpt = new List<SelectListItem>();
                var DepartmentRept = (from DM in dbEF.AizantIT_DepartmentMaster
                                      orderby DM.DepartmentName
                                      select new { DM.DeptID, DM.DepartmentName }).Distinct().ToList();
                DepRpt.Add(new SelectListItem
                {
                    Text = "Select",
                    Value = "0",
                });
                foreach (var item in DepartmentRept)
                {
                    DepRpt.Add(new SelectListItem
                    {
                        Text = item.DepartmentName,
                        Value = item.DeptID.ToString(),
                    });
                }
                ViewBag.ddlDepartmentBindingInci = new SelectList(DepRpt.ToList(), "Value", "Text");
            }
        }

        public ActionResult IncidentReportList(int ShowType)
        {
            try
            {
                IncidentBO IncidentBO = new IncidentBO();
                EmployeID(IncidentBO);
                TempData["QuerySting"] = ShowType;
                TempData["EmpID"] = IncidentBO.EmpID;
                //For Binding Dropdown on Incident Report 
                BindDropdownForIncidentReports();
                return View();
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M39:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                TempData["Exception"] = ErMsg;
                return View();
            }
        }

        //Incident Report Filter for Print
        //For Getting Print view Report by filtering by date
        [HttpGet]
        public JsonResult IncidentReportFilter(int IRDepartmentID, string IRFromDate, string IRToDate, int RoleIdStatus)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    int UserID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);

                    IncidentFilterReport rpt_Incident = new IncidentFilterReport();
                    QMS_BAL objQMS_BAL = new QMS_BAL();

                    //Code for Company Logo
                    DataTable _dtCompany = fillCompany();
                    if (!string.IsNullOrEmpty(_dtCompany.Rows[0]["CompanyLogo"].ToString()))
                    {
                        ((XRPictureBox)(rpt_Incident.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                    }
                    DataSet dsResult = Qms_bal.GetIncidentReportList(0, 0, 0, "desc", "", UserID, IRDepartmentID, IRFromDate, IRToDate, RoleIdStatus, 1,
                        "", "", "", "", "", "", "", "");
                    if (dsResult.Tables[0].Rows.Count > 0)
                    {
                        rpt_Incident.DataSource = dsResult.Tables[0];
                        rpt_Incident.DataMember = dsResult.Tables[0].TableName;
                        //For Binding Department Name by DepartmentID
                        var DeptName = (from DE in dbEF.AizantIT_DepartmentMaster
                                        where DE.DeptID == IRDepartmentID
                                        select new { DE.DepartmentName }).SingleOrDefault();

                        XRTable tblINC_Dept = ((XRTable)(rpt_Incident.FindControl("xrTableFromToDateINC", true)));
                        XRLabel lblINC_Report = ((XRLabel)(rpt_Incident.FindControl("xrLabel1", true)));
                        if (DeptName == null)
                        {
                            rpt_Incident.FindControl("xrtblINCIDepartment", true).Visible = false;
                            tblINC_Dept.LocationF = new PointF(145.42F, 87.17F);
                            lblINC_Report.LocationF = new PointF(316.25F, 48.62F);
                        }
                        else
                        {
                            ((XRLabel)(rpt_Incident.FindControl("xrlblInciDept", false))).Text = DeptName.DepartmentName;
                        }

                        ((XRLabel)(rpt_Incident.FindControl("xrlblInciFromDate", false))).Text = IRFromDate;
                        ((XRLabel)(rpt_Incident.FindControl("xrlblInciToDate", false))).Text = IRToDate;
                        string strFilePath = DynamicFolder.CreateDynamicFolder(11);
                        string fileName = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                        strFilePath = strFilePath + "\\" + fileName;
                        //for Print Path
                        var PrintFilePath = System.Web.VirtualPathUtility.ToAbsolute("/QMSTempReports/" + DateTime.Today.ToString("yyyyMMdd") + "/" + fileName); ;
                        rpt_Incident.ExportToPdf(strFilePath);
                        return Json(new { hasError = false, data = fileName, PrintFilePath }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { hasError = false, data = "No Records found." }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M40:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        #endregion
        //end commented by pradeep 29-06-2020 Incident Report with using single New Code End

        #region Incident MainListView
        //Incident Main List
        public ActionResult IncidentMainListView(int IncidentMasterID, string Number)
        {
            try
            {
                Session["OnlyforView"] = -1;//(-1) - defalut value for exception handling.
                TempData["IncidentMasterID"] = 0;
                TempData["IncidentMasterID"] = IncidentMasterID;
                TempData.Keep("IncidentMasterID");
                DeleteInvestigationFormisTemp(IncidentMasterID);
                DeleteCapaTemp(IncidentMasterID);
                var ShowType = TempData["ShowType"];
                TempData["ShowType"] = ShowType;
                TempData.Keep("ShowType");
                IncidentBO incdntBO = new IncidentBO();
                BindDeptAndDocumentType(incdntBO);
                BindAllDepts();
                incdntBO = BindAllIncidentDetails(IncidentMasterID);
                InvestigatorImpactPV(IncidentMasterID);
                GetMLStatusExistOrNot(incdntBO);
                return View(incdntBO);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M40:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return View();
            }
        }
        #endregion

        #region Incident Charts for Filtering
        //for Dash Board
        public JsonResult AssignedToTempDataIncident(int DeptID, int RoleID, string FromDate, string ToDate, string TypeofIncidentID)
        {
            try
            {
                string _fromDate = FromDate == "" ? DateTime.Now.AddDays(-365).ToString("dd MMM yyyy") : FromDate;
                string _ToDate = ToDate == "" ? DateTime.Now.ToString("dd MMM yyyy") : ToDate;
                Hashtable htIncidntsendparam = new Hashtable();
                htIncidntsendparam.Add("inciRoleID", RoleID);
                htIncidntsendparam.Add("inciDeptID", DeptID);
                htIncidntsendparam.Add("inciFromDate", _fromDate);
                htIncidntsendparam.Add("inciToDate", _ToDate);
                htIncidntsendparam.Add("TypeofIncidentID", TypeofIncidentID);
                Session["IncidentfromandtoDates"] = htIncidntsendparam;
                return Json(new { hasError = false, data = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M41:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region Incident Lists with Jquery Datatable
        [HttpGet]
        public JsonResult GetIncident_List()
        {
            try
            {
                int iDisplayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int iDisplayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int iSortCol_0 = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir_0 = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                int UserID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                int ShowType = Convert.ToInt32(Request.Params["ShowType"]);
                var RoleIDCharts = 0;
                var deptIdDashboard = 0;
                var FromDate = "";
                var ToDate = "";

                string sSearch_IRNumber = Request.Params["sSearch_1"];
                string sSearch_DeptName = Request.Params["sSearch_2"];
                string sSearch_DocumentNo = Request.Params["sSearch_3"];
                string sSearch_DocumentName = Request.Params["sSearch_4"];
                string sSearch_EmpName = Request.Params["sSearch_5"];
                string sSearch_CreatedDate = Request.Params["sSearch_6"];
                string sSearch_OccurrenceDate = Request.Params["sSearch_7"];
                string sSearch_IncidentDueDate = Request.Params["sSearch_8"];
                string sSearch_CurrentStatus = Request.Params["sSearch_9"];

                List<IncidentBO> listIncident = new List<IncidentBO>();
                int filteredCount = 0;
                var IncidentFileterEvent = "";
                if (Session["IncidentfromandtoDates"] != null)
                {
                    Hashtable ht = Session["IncidentfromandtoDates"] as Hashtable;
                    RoleIDCharts = Convert.ToInt32(ht["inciRoleID"].ToString());
                    deptIdDashboard = Convert.ToInt32(ht["inciDeptID"].ToString());
                    FromDate = ht["inciFromDate"].ToString();
                    ToDate = ht["inciToDate"].ToString();
                    IncidentFileterEvent = ht["TypeofIncidentID"].ToString();
                }
                DataSet dt = Qms_bal.GetIncidentList(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, UserID, ShowType, RoleIDCharts, deptIdDashboard, FromDate, ToDate, IncidentFileterEvent,
                    sSearch_IRNumber, sSearch_DeptName, sSearch_DocumentNo, sSearch_DocumentName, sSearch_EmpName, sSearch_CreatedDate, sSearch_OccurrenceDate, sSearch_IncidentDueDate, sSearch_CurrentStatus);

                int totalRecordsCount = 0;
                if (dt.Tables[0].Rows.Count > 0)
                {
                    totalRecordsCount = Convert.ToInt32(dt.Tables[0].Rows[0]["TotalCount"]);
                    foreach (DataRow rdr in dt.Tables[0].Rows)
                    {
                        IncidentBO IncdntBO = new IncidentBO();
                        IncdntBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                        IncdntBO.IRNumber = (rdr["IRNumber"]).ToString();
                        IncdntBO.DocumentName = (rdr["DocumentName"].ToString());
                        IncdntBO.CreatedDate = Convert.ToDateTime(rdr["CreatedDate"].ToString()).ToString("dd MMM yyyy");
                        IncdntBO.DocumentNo = (rdr["DocumentNo"].ToString());
                        IncdntBO.IncidentDueDate = Convert.ToDateTime(rdr["DueDate"].ToString()).ToString("dd MMM yyyy");
                        IncdntBO.EmpName = (rdr["CreatedBy"]).ToString();
                        IncdntBO.DeptName = ((rdr["Department"]).ToString());
                        IncdntBO.OccurrenceDate = Convert.ToDateTime(rdr["OccurrenceDate"].ToString()).ToString("dd MMM yyyy");
                        IncdntBO.IncidentMasterID = Convert.ToInt32(rdr["IncidentMasterID"]);
                        IncdntBO.CurrentStatus = ((rdr["CurrentStatus"])).ToString();
                        IncdntBO.CurrentStatusID = Convert.ToInt32(rdr["CurrentStatusID"]);
                        IncdntBO.UniquePKID = Convert.ToInt32(rdr["UniquePKID"]);
                        //IncdntBO.DocName = (rdr["DocName"]).ToString();
                        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                        listIncident.Add(IncdntBO);
                    }
                }
                return Json(new
                {
                    hasError = false,
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = filteredCount,
                    aaData = listIncident
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = "Yes";
                return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        //For calling IncidentReport View
        [HttpGet]
        public JsonResult GetIncidentReport_List()
        {
            try
            {
                int iDisplayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int iDisplayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int iSortCol_0 = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir_0 = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                int UserID = Convert.ToInt32(Request.Params["UserID"]);
                int deptIdReport = Convert.ToInt32(Request.Params["DeptRpt"]);
                string FromDateReport = (Request.Params["FromDateRpt"]).ToString();
                string ToDateReport = (Request.Params["ToDateReport"]).ToString();
                int Role = Convert.ToInt32(Request.Params["Role"]);

                string sSearch_IRNumber = Request.Params["sSearch_2"];
                string sSearch_DeptName = Request.Params["sSearch_3"];
                string sSearch_EmpName = Request.Params["sSearch_4"];
                string sSearch_CreatedDate = Request.Params["sSearch_5"];
                string sSearch_OccurrenceDate = Request.Params["sSearch_6"];
                string sSearch_IncidentDueDate = Request.Params["sSearch_7"];
                string sSearch_CurrentStatus = Request.Params["sSearch_8"];
                string sSearch_TypeofIncidentName = Request.Params["sSearch_10"];

                List<IncidentBO> listIncident = new List<IncidentBO>();
                int filteredCount = 0;
                int totalRecordsCount = 0;

                DataSet dt = Qms_bal.GetIncidentReportList(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, UserID, deptIdReport, FromDateReport, ToDateReport, Role, 0,
                    sSearch_IRNumber, sSearch_DeptName, sSearch_EmpName, sSearch_CreatedDate, sSearch_OccurrenceDate, sSearch_IncidentDueDate, sSearch_CurrentStatus, sSearch_TypeofIncidentName);
                if (dt.Tables[0].Rows.Count > 0)
                {
                    totalRecordsCount = Convert.ToInt32(dt.Tables[0].Rows[0]["TotalCount"]);
                    foreach (DataRow rdr in dt.Tables[0].Rows)
                    {
                        IncidentBO IncdntBO = new IncidentBO();
                        IncdntBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                        IncdntBO.IRNumber = (rdr["IRNumber"]).ToString();
                        IncdntBO.CreatedDate = Convert.ToDateTime(rdr["CreatedDate"].ToString()).ToString("dd MMM yyyy");
                        IncdntBO.IncidentDueDate = Convert.ToDateTime(rdr["DueDate"].ToString()).ToString("dd MMM yyyy");
                        IncdntBO.EmpName = (rdr["CreatedBy"]).ToString();
                        IncdntBO.DeptName = ((rdr["Department"]).ToString());
                        IncdntBO.OccurrenceDate = Convert.ToDateTime(rdr["OccurrenceDate"].ToString()).ToString("dd MMM yyyy");
                        IncdntBO.IncidentMasterID = Convert.ToInt32(rdr["IncidentMasterID"]);
                        IncdntBO.CurrentStatus = ((rdr["CurrentStatus"])).ToString();
                        IncdntBO.CurrentStatusID = Convert.ToInt32(rdr["CurrentStatusID"]);
                        IncdntBO.TypeofIncidentName = (rdr["TypeofIncidentName"]).ToString();
                        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                        listIncident.Add(IncdntBO);
                    }
                }
                return Json(new
                {
                    hasError = false,
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = filteredCount,
                    aaData = listIncident
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = "Yes";
                return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        //For Incident HOD prelimenary Assesment table view
        [HttpGet]
        public JsonResult GetIncidentPreliminaryAssessmentList1()
        {
            try
            {
                int iDisplayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int iDisplayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int iSortCol_0 = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir_0 = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                int IncidentMasterID = Convert.ToInt32(Request.Params["IncidentMasterID"]);
                int totalRecordsCount = 0;
                List<IncidentBO> Incidentlst = new List<IncidentBO>();
                int filteredCount = 0;
                DataSet dt = Qms_bal.GetIncidentPreliminaryAssessmentListBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IncidentMasterID, 0);
                if (dt.Tables[0].Rows.Count > 0)
                {
                    totalRecordsCount = Convert.ToInt32(dt.Tables[0].Rows[0]["TotalCount"]);
                    foreach (DataRow rdr in dt.Tables[0].Rows)
                    {
                        IncidentBO IncdntBO = new IncidentBO();
                        IncdntBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                        IncdntBO.UniquePKID = Convert.ToInt32(rdr["UniquePKID"]);
                        IncdntBO.DeptName = (rdr["Department"].ToString());
                        IncdntBO.QualityEvent_TypeName = (rdr["QualityEvent_TypeName"].ToString());
                        IncdntBO.EventNumbers = (rdr["EventNumbers"].ToString());
                        IncdntBO.CapaNumber = rdr["CAPA_Number"].ToString() !="" ? rdr["CAPA_Number"].ToString() : "N/A";
                        IncdntBO.QualityEvent_TypeID =Convert.ToInt32 (rdr["QualityEvent_TypeID"].ToString());
                        IncdntBO.EventBaseID =(rdr["EventID"].ToString()) =="" ? 0 : Convert.ToInt32(rdr["EventID"].ToString());
                        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                        Incidentlst.Add(IncdntBO);
                    }
                }
                return Json(new
                {
                    hasError = false,
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = filteredCount,
                    aaData = Incidentlst
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = "Yes";
                return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) }, JsonRequestBehavior.AllowGet);
            }
        }
        //end 02-01-2020

        //For Incident HOD Any other investigation table view
        [HttpGet]
        public JsonResult GetIncidentAnyotherinvestigationList()
        {
            try
            {
                int iDisplayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int iDisplayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int iSortCol_0 = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir_0 = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];

                int IncidentMasterID = Convert.ToInt32(Request.Params["IncidentMasterID"]);
                int totalRecordsCount = 0;



                List<IncidentBO> Incidentlst = new List<IncidentBO>();
                int filteredCount = 0;
                DataSet dt = Qms_bal.GetIncidentAnyotherinvestigationBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IncidentMasterID, 0);
                if (dt.Tables[0].Rows.Count > 0)
                {
                    totalRecordsCount = Convert.ToInt32(dt.Tables[0].Rows[0]["TotalCount"]);
                    foreach (DataRow rdr in dt.Tables[0].Rows)
                    {
                        IncidentBO IncdntBO = new IncidentBO();
                        IncdntBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                        IncdntBO.UniquePKID = Convert.ToInt32(rdr["UniquePKID"]);
                        IncdntBO.DeptName = (rdr["Department"].ToString());
                        IncdntBO.IRNumber = (rdr["IRNumber"].ToString());
                        IncdntBO.AssIncidentMasterID = Convert.ToInt32(rdr["IncidentID"]);
                        IncdntBO.AssessmntIncStatus = rdr["IncidentStatus"].ToString();
                        IncdntBO.Inci_CAPA_Numbers = rdr["CAPA_Numbers"].ToString() !="" ? rdr["CAPA_Numbers"].ToString() : "N/A";
                        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                        Incidentlst.Add(IncdntBO);
                    }
                }
                return Json(new
                {
                    hasError = false,
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = filteredCount,
                    aaData = Incidentlst
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = "Yes";
                return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        //For Incident Investigator role (who are the people involved),  table view
        [HttpGet]
        public JsonResult GetWhoarethepeopleinvolvedList()
        {
            try
            {
                int iDisplayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int iDisplayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int iSortCol_0 = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir_0 = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                int IncidentMasterID = Convert.ToInt32(Request.Params["IncidentMasterID"]);
                int totalRecordsCount = 0;
                List<IncidentBO> Incidentlst = new List<IncidentBO>();
                int filteredCount = 0;
                DataSet dt = Qms_bal.GetIncidentWhoarethepeopleinvolvedListBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IncidentMasterID);
                if (dt.Tables[0].Rows.Count > 0)
                {
                    totalRecordsCount = Convert.ToInt32(dt.Tables[0].Rows[0]["TotalCount"]);
                    foreach (DataRow rdr in dt.Tables[0].Rows)
                    {
                        IncidentBO IncdntBO = new IncidentBO();
                        IncdntBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                        IncdntBO.UniquePKID = Convert.ToInt32(rdr["UniquePKID"]);
                        IncdntBO.DeptName = (rdr["Department"].ToString());
                        IncdntBO.EmpName = (rdr["EmpName"].ToString());
                        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                        Incidentlst.Add(IncdntBO);
                    }
                }
                return Json(new
                {
                    hasError = false,
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = filteredCount,
                    aaData = Incidentlst
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = "Yes";
                return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        //For Incident Investigator (Has the incident Affected Documents), table view
        [HttpGet]
        public JsonResult GetIncidentHasAffectedDocumentList()
        {
            try
            {
                int iDisplayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int iDisplayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int iSortCol_0 = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir_0 = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                int IncidentMasterID = Convert.ToInt32(Request.Params["IncidentMasterID"]);
                int totalRecordsCount = 0;
                List<IncidentBO> Incidentlst = new List<IncidentBO>();
                int filteredCount = 0;
                DataSet dt = Qms_bal.GetIncidentHasAffectedDocumentListtBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IncidentMasterID);
                if (dt.Tables[0].Rows.Count > 0)
                {
                    totalRecordsCount = Convert.ToInt32(dt.Tables[0].Rows[0]["TotalCount"]);
                    foreach (DataRow rdr in dt.Tables[0].Rows)
                    {
                        IncidentBO IncdntBO = new IncidentBO();
                        IncdntBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                        IncdntBO.UniquePKID = Convert.ToInt32(rdr["UniquePKID"]);
                        IncdntBO.HasDeptName = (rdr["Department"].ToString());
                        IncdntBO.HasAffectedDocument = (rdr["DocumentName"].ToString());
                        IncdntBO.HasAffectedDocRefNo = (rdr["ReferenceNumber"].ToString());
                        IncdntBO.HasAffectedPageNo = (rdr["PageNo"].ToString());
                        if (IncdntBO.HasAffectedPageNo != "")
                        {
                            IncdntBO.HasAffectedPageNo = (rdr["PageNo"].ToString());
                        }
                        else
                        {
                            IncdntBO.HasAffectedPageNo = "N/A";
                        }
                        IncdntBO.HasAffectedComments = (rdr["Comments"].ToString());
                        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                        Incidentlst.Add(IncdntBO);
                    }
                }
                return Json(new
                {
                    hasError = false,
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = filteredCount,
                    aaData = Incidentlst
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = "Yes";
                return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        //For Incident Investigator (List of Supporting Documents), table view
        [HttpGet]
        public JsonResult GetIncidentFileUploadList()
        {
            try
            {
                int iDisplayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int iDisplayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int iSortCol_0 = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir_0 = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                int IncidentMasterID = Convert.ToInt32(Request.Params["IncidentMasterID"]);
                int totalRecordsCount = 0;
                List<IncidentBO> Incidentlst = new List<IncidentBO>();
                int filteredCount = 0;
                DataSet dt = Qms_bal.GetIncidentFileUploadListBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IncidentMasterID);
                if (dt.Tables[0].Rows.Count > 0)
                {
                    totalRecordsCount = Convert.ToInt32(dt.Tables[0].Rows[0]["TotalCount"]);
                    foreach (DataRow rdr in dt.Tables[0].Rows)
                    {
                        IncidentBO IncdntBO = new IncidentBO();
                        IncdntBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                        IncdntBO.UniquePKID = Convert.ToInt32(rdr["UniquePKID"]);
                        IncdntBO.TypeofDocumentName = (rdr["TypeofDocument"].ToString());
                        IncdntBO.FileDocumentName = (rdr["Document"].ToString());
                        IncdntBO.FileType = (rdr["FileType"].ToString());
                        IncdntBO.Comments = (rdr["Comments"].ToString());
                        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                        Incidentlst.Add(IncdntBO);
                    }
                }
                return Json(new
                {
                    hasError = false,
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = filteredCount,
                    aaData = Incidentlst
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = "Yes";
                return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        //For Incident Investigator(Other Departments Hods Review), table view
        [HttpGet]
        public JsonResult GetIncidentDepartmentReviewHodList()
        {
            try
            {
                int iDisplayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int iDisplayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int iSortCol_0 = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir_0 = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                int IncidentMasterID = Convert.ToInt32(Request.Params["IncidentMasterID"]);
                int totalRecordsCount = 0;

                List<IncidentBO> Incidentlst = new List<IncidentBO>();
                int filteredCount = 0;
                DataSet dt = Qms_bal.GetIncidentDepartmentReviewHodListBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IncidentMasterID);
                if (dt.Tables[0].Rows.Count > 0)
                {
                    totalRecordsCount = Convert.ToInt32(dt.Tables[0].Rows[0]["TotalCount"]);
                    foreach (DataRow rdr in dt.Tables[0].Rows)
                    {
                        IncidentBO IncdntBO = new IncidentBO();
                        IncdntBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                        IncdntBO.UniquePKID = Convert.ToInt32(rdr["UniquePKID"]);
                        IncdntBO.DeptName = (rdr["Department"].ToString());
                        IncdntBO.EmpName = (rdr["EmpName"].ToString());
                        if (rdr["SubmittedDate"].ToString() == "N/A")
                        {
                            IncdntBO.HodSubmittedDate = " ";
                        }
                        else
                        {
                            IncdntBO.HodSubmittedDate = Convert.ToDateTime(rdr["SubmittedDate"].ToString()).ToString("dd MMM yyyy HH:mm:ss");
                        }
                        IncdntBO.HodReviewStatus = (rdr["ActionStatus"].ToString());
                        if (Convert.ToInt32(rdr["HodCommLength"]) > 0)
                        {
                            IncdntBO.DeptHodReviewComments = (rdr["HODComments"].ToString());
                        }
                        else
                        {
                            IncdntBO.DeptHodReviewComments = (rdr["HODComments"].ToString());
                        }
                        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                        Incidentlst.Add(IncdntBO);
                    }
                }
                return Json(new
                {
                    hasError = false,
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = filteredCount,
                    aaData = Incidentlst
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = "Yes";
                return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);
            }
        }

        //For Incident Hod Action plan role (Is Training Required), table view
        [HttpGet]
        public JsonResult GetIncidentTTS_List()
        {
            try
            {
                int iDisplayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int iDisplayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int iSortCol_0 = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir_0 = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                int IncidentMasterID = Convert.ToInt32(Request.Params["IncidentMasterID"]);
                int IncidentCurrentStatus = Convert.ToInt32(Request.Params["IncidentCurrentStatus"]);
                
                int totalRecordsCount = 0;
                int TemphdnAddBtn = 0;
                List<IncidentBO> Incidentlst = new List<IncidentBO>();
                int filteredCount = 0;
                if (IncidentCurrentStatus == 16)
                {
                    TMS_BAL objTMS_Bal = new TMS_BAL();
                    DataTable dt = objTMS_Bal.GetQA_ApprovedTTS_ListForHodAndQa(iDisplayLength, iDisplayStart, 
                        iSortCol_0, sSortDir_0, "", "", "", "", "", "", "", "", "", 0, 0, 1, 0,0, "", "", IncidentMasterID, out filteredCount);
                    if (dt.Rows.Count > 0)
                    {
                        TemphdnAddBtn = 1;
                        foreach (DataRow rdr in dt.Rows)
                        {
                            IncidentBO IncdntBO = new IncidentBO();
                            IncdntBO.TTSID = Convert.ToInt32(rdr["TTS_ID"]);
                            IncdntBO.TTSDocName = rdr["DocumentName"].ToString();
                            IncdntBO.TTSDept = rdr["DeptCode"].ToString();
                            IncdntBO.TTSTypeofTraining = rdr["TypeOfTraining"].ToString();
                            IncdntBO.TTSAuthor = rdr["Author"].ToString();
                            IncdntBO.TTSQA = rdr["Reviewer"].ToString();
                            IncdntBO.TTSDate = rdr["ApproveDate"].ToString();
                            IncdntBO.TTSStatus = rdr["StatusName"].ToString();
                            filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                            Incidentlst.Add(IncdntBO);
                        }
                    }
                }
                else
                {
                    DataTable dt = Qms_bal.GetIncidentTTS_ListBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IncidentMasterID);
                    if (dt.Rows.Count > 0)
                    {
                        TemphdnAddBtn = 1;
                        foreach (DataRow rdr in dt.Rows)
                        {
                            IncidentBO IncdntBO = new IncidentBO();
                            IncdntBO.TTSID = Convert.ToInt32(rdr["TTS_ID"]);
                            IncdntBO.TTSDocName = rdr["DocumentName"].ToString();
                            IncdntBO.TTSDept = rdr["DepartmentName"].ToString();
                            IncdntBO.TTSTypeofTraining = rdr["TypeOfTraining"].ToString();
                            IncdntBO.TTSAuthor = rdr["Author"].ToString();
                            IncdntBO.TTSQA = rdr["Reviewer"].ToString() != " " ? rdr["Reviewer"].ToString():"N/A";
                            IncdntBO.TTSDate = rdr["ApproveDate"].ToString();
                            IncdntBO.TTSStatus = rdr["StatusName"].ToString();
                            filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                            Incidentlst.Add(IncdntBO);
                        }
                    }
                }
                return Json(new
                {
                    hasError = false,
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = filteredCount,
                    aaData = Incidentlst,
                    TableExist = TemphdnAddBtn
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = "Yes";
                return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);
            }
        }

        //For Incident Initiator (List of Supporting Documents), table view
        [HttpGet]
        public JsonResult GetIncidentInitiatorFileUploadList()
        {
            try
            {
                TempData.Keep("TempRefID");
                int iDisplayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int iDisplayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int iSortCol_0 = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir_0 = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                int IncidentMasterID = Convert.ToInt32(Request.Params["IncidentMasterID"]);
                if (IncidentMasterID == 0)
                {
                    if (TempData["TempRefID"] != "0")
                    {
                        IncidentMasterID = Convert.ToInt32(TempData["TempRefID"]);
                    }
                }
                int Status = Convert.ToInt32(Request.Params["Status"]);
                int totalRecordsCount = 0;

                List<IncidentBO> Incidentlst = new List<IncidentBO>();
                int filteredCount = 0;
                DataSet dt = Qms_bal.GetIncidentInitiatorFileUploadListBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IncidentMasterID, Status);
                if (dt.Tables[0].Rows.Count > 0)
                {
                    totalRecordsCount = Convert.ToInt32(dt.Tables[0].Rows[0]["TotalCount"]);
                    foreach (DataRow rdr in dt.Tables[0].Rows)
                    {
                        IncidentBO _IncdntBO = new IncidentBO();
                        _IncdntBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                        _IncdntBO.UniquePKID = Convert.ToInt32(rdr["UniquePKID"]);
                        _IncdntBO.FileDocumentName = (rdr["Document"].ToString());
                        _IncdntBO.FileType = (rdr["FileType"].ToString());
                        _IncdntBO.EmpName = (rdr["CreatedBy"].ToString());
                        _IncdntBO.RoleName = (rdr["RoleName"].ToString());
                        _IncdntBO.CreatedDate = (rdr["CreatedDate"].ToString());
                        // _IncdntBO.CreatedDate = Convert.ToDateTime(rdr["CreatedDate"].ToString()).ToString("dd MMM yyyy hh:mm:ss");
                        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                        Incidentlst.Add(_IncdntBO);
                    }
                }
                return Json(new
                {
                    hasError = false,
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = filteredCount,
                    aaData = Incidentlst
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = "Yes";
                return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Incident Tabs Content Loading on Demand
        public ActionResult InvestigationPV()
        {
            try
            {
                TempData.Keep("IncidentMasterID");
                IncidentBO incdntBO = BindAllIncidentDetails(Convert.ToInt32(TempData["IncidentMasterID"]));
                TempData.Keep("ShowType");
                return PartialView(incdntBO);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("_IncidentPVExceptionError", ViewBag.Exception);
            }
        }
        public ActionResult InvestigatorImpactPV()
        {
            try
            {
                TempData.Keep("IncidentMasterID");
                IncidentBO incdntBO = BindAllIncidentDetails(Convert.ToInt32(TempData["IncidentMasterID"]));
                TempData.Keep("ShowType");
                return PartialView(incdntBO);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("_IncidentPVExceptionError", ViewBag.Exception);
            }
        }
        public ActionResult IncidentInvestigationHasAffectedPV()
        {
            try
            {
                TempData.Keep("IncidentMasterID");
                IncidentBO incdntBO = BindAllIncidentDetails(Convert.ToInt32(TempData["IncidentMasterID"]));
                TempData.Keep("ShowType");
                return PartialView(incdntBO);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("_IncidentPVExceptionError", ViewBag.Exception);
            }
        }
        public ActionResult HasAffectedDocumentPV()
        {
            try
            {
                TempData.Keep("IncidentMasterID");
                IncidentBO incdntBO = BindAllIncidentDetails(Convert.ToInt32(TempData["IncidentMasterID"]));
                TempData.Keep("ShowType");
                return PartialView(incdntBO);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("_IncidentPVExceptionError", ViewBag.Exception);
            }
        }
        public ActionResult IncidentInvestigationReviewPV()
        {
            try
            {
                TempData.Keep("IncidentMasterID");
                IncidentBO incdntBO = BindAllIncidentDetails(Convert.ToInt32(TempData["IncidentMasterID"]));
                TempData.Keep("ShowType");
                return PartialView(incdntBO);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("_IncidentPVExceptionError", ViewBag.Exception);
            }
        }
        public ActionResult FileUploadsinIncidentPV()
        {
            try
            {
                TempData.Keep("IncidentMasterID");
                IncidentBO incdntBO = BindAllIncidentDetails(Convert.ToInt32(TempData["IncidentMasterID"]));
                TempData.Keep("ShowType");
                return PartialView(incdntBO);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("_IncidentPVExceptionError", ViewBag.Exception);
            }
        }
        public ActionResult SendIncidentforHodsReviewPV()
        {
            try
            {
                TempData.Keep("IncidentMasterID");
                IncidentBO incdntBO = BindAllIncidentDetails(Convert.ToInt32(TempData["IncidentMasterID"]));
                TempData.Keep("ShowType");
                return PartialView(incdntBO);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("_IncidentPVExceptionError", ViewBag.Exception);
            }
        }
        public ActionResult AllFileUploadsPV()
        {
            try
            {
                TempData.Keep("IncidentMasterID");
                IncidentBO incdntBO = BindAllIncidentDetails(Convert.ToInt32(TempData["IncidentMasterID"]));
                TempData.Keep("ShowType");
                return PartialView(incdntBO);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INCI_M:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                //return PartialView("ExecptionError", ViewBag.Exception);
                return PartialView("_IncidentPVExceptionError", ViewBag.Exception);
            }
        }
        #endregion
        #region Manage Roles
        public IncidentBO BindIncAdminManageDetails(int IncidentMasterID)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                IncidentBO incdntBO = new IncidentBO();
                EmployeID(incdntBO);
                var Incdnt_recordslist = (from i in dbEF.AizantIT_IncidentMaster
                                          where i.IncidentMasterID == IncidentMasterID
                                          select new
                                          {
                                              i.IncidentMasterID,
                                              i.HeadQAEmpID,
                                              i.HODEmpID,
                                              i.IncidentDesc,
                                              i.IncidentReportDate,
                                              i.DueDate,
                                              i.InitiatorEmpID,
                                              i.InvestigatorEmpID,
                                              i.IRNumber,
                                              i.IsAnyTrainingRequired,
                                              i.IsCAPARequired,
                                              i.OccurrenceDate,
                                              i.QAClassComments,
                                              i.QAEmpID,
                                              i.TypeofIncidentID,
                                              i.CAPAClosureVerification_Desc,
                                              i.CreatedDate,
                                              i.CurrentStatus,
                                              i.DeptID,
                                              i.DocTypeID,
                                              i.EvalutionAndDisposition_Desc,
                                              i.IsFirstOccurence,
                                              i.AnyOtherInvestigations,
                                              i.FO_Comments,
                                              i.AOI_Comments,
                                              i.ImmediateCorrection,
                                              i.Conclusion_Desc,

                                          }).ToList();
                foreach (var item in Incdnt_recordslist)
                {
                    incdntBO.IncidentMasterID = (int)item.IncidentMasterID;
                    incdntBO.DeptID = (int)item.DeptID;
                    incdntBO.CAPAClosureVerification_Desc = item.CAPAClosureVerification_Desc;
                    //Created Date
                    incdntBO.CreatedDate = Convert.ToDateTime(item.CreatedDate).ToString("dd MMM yyyy");
                    incdntBO.CurrentStatus = item.CurrentStatus.ToString();
                    TempData["CurrentStatus"] = item.CurrentStatus.ToString();
                    TempData.Keep("CurrentStatus");
                    incdntBO.EvaluationandDispostion = item.EvalutionAndDisposition_Desc;
                    //Incident Description
                    incdntBO.IncidentDesc = item.IncidentDesc;
                    //Incident Report Date
                    incdntBO.IncidentReportDate = Convert.ToDateTime(item.IncidentReportDate).ToString("dd MMM yyyy");
                    //Incident Due Date
                    incdntBO.IncidentDueDate = Convert.ToDateTime(item.DueDate).ToString("dd MMM yyyy");
                    incdntBO.IncidentReportDateEdit = Convert.ToDateTime(item.IncidentReportDate).ToString("dd MMM yyyy");
                    //Occurrence Date
                    incdntBO.OccurrenceDate = Convert.ToDateTime(item.OccurrenceDate).ToString("dd MMM yyyy");
                    //IR Number
                    incdntBO.IRNumber = item.IRNumber;
                    TempData["IRNumber"] = item.IRNumber;
                    incdntBO.InitiatorEmpID = Convert.ToInt16(item.InitiatorEmpID);
                    incdntBO.HODEmpID = Convert.ToInt16(item.HODEmpID);
                    incdntBO.QAEmpID = Convert.ToInt16(item.QAEmpID);
                    incdntBO.InvestigatorEmpID = Convert.ToInt16(item.InvestigatorEmpID);
                    incdntBO.DocumentType = (int)item.DocTypeID;
                    incdntBO.IsFirstOccurence = item.IsFirstOccurence.ToString();
                    incdntBO.AnyOtherInvestigations = item.AnyOtherInvestigations.ToString();
                    incdntBO.ImmediateCorrection = item.ImmediateCorrection != null ? item.ImmediateCorrection.ToString() : "";
                    incdntBO.IsAnyTrainingRequired = Convert.ToInt32(item.IsAnyTrainingRequired);
                    incdntBO.FO_Comments = item.FO_Comments;
                    incdntBO.AOI_Comments = item.AOI_Comments;
                    incdntBO.HeadQAEmpID = Convert.ToInt32(item.HeadQAEmpID);
                    incdntBO.QNonImpConclusion = item.Conclusion_Desc;

                    DocumentDetialsClass objDocument = new DocumentDetialsClass();
                    QMSCommonActions CommonDocBo = new QMSCommonActions();
                    if (incdntBO.DocumentType != 0 && incdntBO.DocumentType != -1)
                    {
                        //start commented on 19-02-2020
                        objDocument = CommonDocBo.GetDocumentDetails(incdntBO.IncidentMasterID, 1);
                        incdntBO.DocumentNo = objDocument.DocumentNumber;
                        incdntBO.DocName = objDocument.DocumentName;
                        incdntBO.ReferenceNumber = objDocument.VersionID.ToString();
                        //end commented on 19-02-2020
                    }
                    else if (incdntBO.DocumentType == 0)
                    {
                        //start commented on 19-02-2020
                        objDocument = CommonDocBo.GetOtherDocumentDetails(incdntBO.IncidentMasterID, 1);
                        incdntBO.DocumentNo = objDocument.DocumentNumber;
                        incdntBO.DocName = objDocument.DocumentName;
                        incdntBO.DocTitle = objDocument.DocumentName;
                        incdntBO.OtherDOCRefNo = objDocument.DocumentNumber;
                        //end commented on 19-02-2020

                    }
                    else
                    {
                        incdntBO.DocumentNo = "N/A";
                        incdntBO.DocName = "N/A";
                        incdntBO.DocTitle = "N/A";
                        incdntBO.OtherDOCRefNo = "N/A";
                    }
                    var GetStatusName = (from i in dbEF.AizantIT_IncidentMaster
                                         join cs in dbEF.AizantIT_ObjectStatus on i.CurrentStatus equals cs.StatusID
                                         where cs.StatusID.ToString() == incdntBO.CurrentStatus && cs.ObjID == 12
                                         select new { cs.StatusName }).ToList();
                    foreach (var item6 in GetStatusName)
                    {
                        incdntBO.CurrentStatusName = item6.StatusName;
                    }
                    var DeptName = (from dm in dbEF.AizantIT_DepartmentMaster
                                    where dm.DeptID == incdntBO.DeptID
                                    select new { dm.DepartmentName }).ToList();
                    foreach (var item1 in DeptName)
                    {
                        incdntBO.DeptName = item1.DepartmentName;
                    }
                    var EmpName = (from dm in dbEF.AizantIT_EmpMaster
                                   where dm.EmpID == incdntBO.InitiatorEmpID
                                   select new { FullName = dm.FirstName + "  " + dm.LastName }).ToList();
                    foreach (var item2 in EmpName)
                    {
                        incdntBO.EmpName = item2.FullName;
                    }
                    //To Update Hod Comments Review Get Unique ID
                    EmployeID(incdntBO);
                }
                return incdntBO;
            }
        }
        public string GetFullName(int EmpId)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                var EmployeeName = (from E in dbEF.AizantIT_EmpMaster
                                    where E.EmpID == EmpId
                                    select (E.FirstName + "" + E.LastName)).SingleOrDefault();


                return EmployeeName;
            }
        }
        public PartialViewResult _IncidentAdminManage(int IncidentMasterID)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                IncidentBO incdntBO = new IncidentBO();
                incdntBO = BindIncAdminManageDetails(IncidentMasterID);
                DataSet dsEmployees = Qms_bal.GetEmployeesDeptandRoleWiseInAdminManageBAL(incdntBO.DeptID, 1);
                List<IncidentBO> ListEmployee = new List<IncidentBO>();
                for (int i = 0; i < 5; i++)
                {
                    if (i == 0)
                    {
                        //For getting Initiator Employee Names
                        List<SelectListItem> InitatorList = new List<SelectListItem>();
                        InitatorList.Add(new SelectListItem
                        {
                            Text = "Select",
                            Value = "0",
                        });
                        foreach (DataRow rdr in dsEmployees.Tables[0].Rows)
                        {
                            InitatorList.Add(new SelectListItem
                            {
                                Text = (rdr["EmpName"]).ToString(),
                                Value = (rdr["EmpID"]).ToString(),
                                Selected = (Convert.ToInt32(rdr["EmpID"]) == incdntBO.InitiatorEmpID ? true : false)
                            });
                        }
                        ListEmployee.Add(new IncidentBO
                        {
                            Sno = 1,
                            DDLEmpList = InitatorList,
                            RoleName = "Initiator",
                            SelectDDLEmp = incdntBO.InitiatorEmpID,
                            SelectedEmpReassign = GetFullName(incdntBO.InitiatorEmpID)
                        });
                    }

                    if (i == 1)
                    {

                        //For getting HOD Employee Names
                        List<SelectListItem> HODList = new List<SelectListItem>();
                        List<int> NotBindingQAInvestEmpID = new List<int>() { incdntBO.QAEmpID, incdntBO.InvestigatorEmpID };

                        HODList.Add(new SelectListItem
                        {
                            Text = "Select",
                            Value = "0",
                        });
                        foreach (DataRow rdr in dsEmployees.Tables[1].Rows)
                        {
                            if (!NotBindingQAInvestEmpID.Contains(Convert.ToInt32(rdr["EmpID"])))
                            {
                                HODList.Add(new SelectListItem
                                {
                                    Text = (rdr["EmpName"]).ToString(),
                                    Value = (rdr["EmpID"]).ToString(),
                                    Selected = (Convert.ToUInt32(rdr["EmpID"]) == incdntBO.HODEmpID ? true : false)
                                });
                            }
                        }
                        ListEmployee.Add(new IncidentBO
                        {
                            Sno = 2,
                            DDLEmpList = HODList,
                            RoleName = "HOD",
                            SelectDDLEmp = incdntBO.HODEmpID,
                            SelectedEmpReassign = GetFullName(incdntBO.HODEmpID)
                        });
                    }

                    if (i == 2)
                    {
                        //For getting QA Employee Names
                        List<SelectListItem> QAList = new List<SelectListItem>();
                        List<int> QaEmployeeid = new List<int>() { incdntBO.InitiatorEmpID, incdntBO.HODEmpID };

                        QAList.Add(new SelectListItem
                        {
                            Text = "Select",
                            Value = "0",
                        });
                        foreach (DataRow rdr in dsEmployees.Tables[2].Rows)
                        {
                            if (!QaEmployeeid.Contains(Convert.ToInt32(rdr["EmpID"])))
                            {
                                QAList.Add(new SelectListItem
                                {
                                    Text = (rdr["EmpName"]).ToString(),
                                    Value = (rdr["EmpID"]).ToString(),
                                    Selected = (Convert.ToUInt32(rdr["EmpID"]) == incdntBO.QAEmpID ? true : false)
                                });
                            }
                        }
                        ListEmployee.Add(new IncidentBO
                        {
                            Sno = 3,
                            DDLEmpList = QAList,
                            RoleName = "QA",
                            SelectDDLEmp = incdntBO.QAEmpID,
                            SelectedEmpReassign = GetFullName(incdntBO.QAEmpID)
                        });
                    }

                    if (i == 3)
                    {
                        //For getting Investigator Employee Names
                        List<SelectListItem> InvestigatorList = new List<SelectListItem>();
                        List<int> InvestEmpIDMng = new List<int>() { incdntBO.InitiatorEmpID, incdntBO.HODEmpID };

                        InvestigatorList.Add(new SelectListItem
                        {
                            Text = "Select",
                            Value = "0",
                        });
                        foreach (DataRow rdr in dsEmployees.Tables[5].Rows)
                        {
                            if (!InvestEmpIDMng.Contains(Convert.ToInt32(rdr["EmpID"])))
                            {
                                InvestigatorList.Add(new SelectListItem
                                {
                                    Text = (rdr["EmpName"]).ToString(),
                                    Value = (rdr["EmpID"]).ToString(),
                                    Selected = (Convert.ToUInt32(rdr["EmpID"]) == incdntBO.InvestigatorEmpID ? true : false)
                                });
                            }
                        }
                        ListEmployee.Add(new IncidentBO
                        {
                            Sno = 4,
                            DDLEmpList = InvestigatorList,
                            RoleName = "Investigator",
                            SelectDDLEmp = incdntBO.InvestigatorEmpID,
                            SelectedEmpReassign = GetFullName(incdntBO.InvestigatorEmpID)
                        });
                    }

                    if (i == 4)
                    {
                        //For getting HeadQA Employee Names
                        List<SelectListItem> HeadQAList = new List<SelectListItem>();
                        List<int> HeadQAEmployeeid = new List<int>() { incdntBO.InitiatorEmpID, incdntBO.HODEmpID, incdntBO.QAEmpID, incdntBO.InvestigatorEmpID };
                        HeadQAList.Add(new SelectListItem
                        {
                            Text = "Select",
                            Value = "0",
                        });
                        foreach (DataRow rdr in dsEmployees.Tables[3].Rows)
                        {
                            if (!HeadQAEmployeeid.Contains(Convert.ToInt32(rdr["EmpID"])))
                            {
                                HeadQAList.Add(new SelectListItem
                                {
                                    Text = (rdr["EmpName"]).ToString(),
                                    Value = (rdr["EmpID"]).ToString(),
                                    Selected = (Convert.ToUInt32(rdr["EmpID"]) == incdntBO.HeadQAEmpID ? true : false)
                                });
                            }
                        }
                        ListEmployee.Add(new IncidentBO
                        {
                            Sno = 5,
                            DDLEmpList = HeadQAList,
                            RoleName = "HeadQa",
                            SelectDDLEmp = incdntBO.HeadQAEmpID,
                            SelectedEmpReassign = GetFullName(incdntBO.HeadQAEmpID)
                        });
                    }
                }
                ViewBag.ListEmployees = ListEmployee;
                //For Getting Assessment Employee Count
                ViewBag.AssessmentExistOrNot = (from INC in dbEF.AizantIT_Incident_InvestigationReviewDeptHods
                                                where INC.IncidentMasterID == IncidentMasterID
                                                select INC).Count();

                return PartialView(incdntBO);
            }
        }
        [HttpPost]
        public JsonResult UpdateManageEmp(IncidentBO objINC)
        {
            QMS_BAL Qms_bal = new QMS_BAL();
            try
            {
                objINC.ActionBy = (Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                int i = (Qms_bal.INCAdminManageBal(objINC, out int Result));
                if (Result == 0)
                {
                    return Json(new { hasError = false, data = i }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { hasError = true, data = "Modification of Employee is Restricted, as the Transaction is Completed." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "NTF_M19:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        //For Binding Init and HOD based on Dept and RoleID
        public List<SelectListItem> BindHODDropdownAccessibleDepartmentsandRole(int DeptID, int Roleid, int SelectedEmpID, int HodEmpID, int InvestEmpID)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                //Assigned HOD's
                List<SelectListItem> objAssignHod = new List<SelectListItem>();
                var AssignedHOD = dbEF.Database.SqlQuery<AccessibleEmpOnDeptandRole>("exec UMS.AizantIT_SP_ToGetEmployeesByDeptIDandRoleID " +
                   +DeptID + "," + Roleid + ",'" + 1 + "'").ToList();
                objAssignHod.Add(new SelectListItem
                {
                    Text = "Select",
                    Value = "0",
                });
                foreach (var RPItem in AssignedHOD)
                {
                    if (InvestEmpID != RPItem.EmpID)
                    {
                        objAssignHod.Add(new SelectListItem()
                        {
                            Text = RPItem.EmpName,
                            Value = RPItem.EmpID.ToString(),
                            Selected = (Convert.ToInt32(RPItem.EmpID) == SelectedEmpID ? true : false)
                        });
                    }
                }
                return objAssignHod.ToList();
            }
        }
        [HttpGet]
        public JsonResult GetAssessmentForINCManage()
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    int iDisplayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                    int iDisplayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                    int iSortCol_0 = Convert.ToInt32(Request.Params["iSortCol_0"]);
                    string sSortDir_0 = Request.Params["sSortDir_0"];
                    string sSearch = Request.Params["sSearch"];
                    int IncidentMasterID = Convert.ToInt32(Request.Params["IncidentMasterID"]);
                    int totalRecordsCount = 0;

                    List<IncidentBO> listEmployees = new List<IncidentBO>();
                    int filteredCount = 0;
                    DataSet dt = Qms_bal.GetIncidentDepartmentReviewHodListBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IncidentMasterID);
                    //DataSet dt = Qms_bal.GetIncidentFileUploadListBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IncidentMasterID);
                    if (dt.Tables[0].Rows.Count > 0)
                    {
                        totalRecordsCount = Convert.ToInt32(dt.Tables[0].Rows[0]["TotalCount"]);
                        foreach (DataRow rdr in dt.Tables[0].Rows)
                        {
                            IncidentBO IncdntBO = new IncidentBO();
                            IncdntBO.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                            IncdntBO.UniquePKID = Convert.ToInt32(rdr["UniquePKID"]);
                            IncdntBO.DeptName = (rdr["Department"].ToString());
                            IncdntBO.EmpName = (rdr["EmpName"].ToString());
                            IncdntBO.DeptID = Convert.ToInt32(rdr["DeptID"]);
                            IncdntBO.EmpID = Convert.ToInt32(rdr["EmpID"]);
                            IncdntBO.ApproveStatus = Convert.ToInt32(rdr["ApproveStatus"]);
                            if (rdr["SubmittedDate"].ToString() == "N/A")
                            {
                                IncdntBO.HodSubmittedDate = "";
                            }
                            else
                            {
                                IncdntBO.HodSubmittedDate = (rdr["SubmittedDate"].ToString());
                            }
                            IncdntBO.HodReviewStatus = (rdr["ActionStatus"].ToString());
                            if (Convert.ToInt32(rdr["HodCommLength"]) > 0)
                            {
                                IncdntBO.DeptHodReviewComments = (rdr["HODComments"].ToString());
                            }
                            else
                            {
                                IncdntBO.DeptHodReviewComments = (rdr["HODComments"].ToString());
                            }
                            filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                            //For Getting DeptID and EmpID
                            ViewBag.AssessmentExistOrNot = (from INC in dbEF.AizantIT_Incident_InvestigationReviewDeptHods
                                                            where INC.IncidentMasterID == IncidentMasterID
                                                            select INC).Count();
                            //HOD Dropdown
                            int DepartmentID = IncdntBO.DeptID;

                            var HodEmpID = (from INM in dbEF.AizantIT_IncidentMaster
                                            where INM.IncidentMasterID == IncidentMasterID
                                            select INM.HODEmpID).SingleOrDefault();

                            IncdntBO.HODEmpID = (int)HodEmpID;
                            //For Restricting Investigator Employee on Acceessment HOD Dropdown
                            var InvestEmpID = (from INM in dbEF.AizantIT_IncidentMaster
                                               where INM.IncidentMasterID == IncidentMasterID
                                               select INM.InvestigatorEmpID).SingleOrDefault();
                            IncdntBO.InvestigatorEmpID = (int)InvestEmpID;

                            List<SelectListItem> objAssignAssessmentHod = BindHODDropdownAccessibleDepartmentsandRole(DepartmentID, (int)QMS_Roles.HOD, IncdntBO.EmpID, IncdntBO.HODEmpID, IncdntBO.InvestigatorEmpID);
                            IncdntBO.ddlAssessmentHODs = objAssignAssessmentHod.ToList();
                            listEmployees.Add(IncdntBO);
                        }
                    }
                    return Json(new
                    {
                        hasError = false,
                        iTotalRecords = totalRecordsCount,
                        iTotalDisplayRecords = filteredCount,
                        aaData = listEmployees
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    ViewBag.SysError = "Yes";
                    return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        //For Updating Review Hod 
        [HttpPost]
        public JsonResult ReviewHODModifyAdmin(int EmployeeID, int UniqueID, int IncidentMasterID, string ReviewHODComments, int Type)
        {
            try
            {
                int ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                QMS_BAL Qms_bal = new QMS_BAL();
                int i = (Qms_bal.UpdateRevieewHodsBal(EmployeeID, UniqueID, IncidentMasterID, ActionBy, ReviewHODComments, out int Result));
                if (Result == 0)
                {
                    return Json(new { hasError = false, data = i }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { hasError = true, data = "Modification of Employee is Restricted, as the Transaction is Completed" }, JsonRequestBehavior.AllowGet);
                }
                //return Json(new { hasError = false, data = i }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "INC_M19:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        public bool ToCheckEmpValidityIncident(int INCID, int RoleID)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                int Initiator = 0, HOD = 0, QA = 0, Investigator = 0, HeadQA = 0;
                Boolean i = true;
                int LoginEmployeeID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                var GetDetails = (from IR in dbEF.AizantIT_IncidentMaster
                                  where IR.IncidentMasterID == INCID
                                  select new
                                  {
                                      IR.InitiatorEmpID,
                                      IR.HODEmpID,
                                      IR.QAEmpID,
                                      IR.InvestigatorEmpID,
                                      IR.HeadQAEmpID
                                  }).ToList();
                foreach (var item in GetDetails)
                {
                    Initiator = (int)item.InitiatorEmpID;
                    HOD = (int)item.HODEmpID;
                    QA = item.QAEmpID == null ? 0 : (int)item.QAEmpID;
                    Investigator = item.InvestigatorEmpID == null ? 0 : (int)item.InvestigatorEmpID;
                    HeadQA = item.HeadQAEmpID == null ? 0 : (int)item.HeadQAEmpID;
                }
                if (RoleID == 24)//Initiator
                {
                    if (LoginEmployeeID == Initiator)
                    {
                        i = true;//Success
                    }
                    else
                    {
                        i = false;//fail
                    }
                }
                else if (RoleID == 25)//HOD
                {
                    if (LoginEmployeeID == HOD)
                    {
                        i = true; //Success
                    }
                    else
                    {
                        i = false;//fail
                    }
                }
                else if (RoleID == 26)//QA
                {
                    if (LoginEmployeeID == QA)
                    {
                        i = true;//Success
                    }
                    else
                    {
                        i = false;//fail
                    }
                }
                else if (RoleID == 33)//Investigator
                {
                    if (LoginEmployeeID == Investigator)
                    {
                        i = true;//Success
                    }
                    else
                    {
                        i = false;//fail
                    }
                }
                else if (RoleID == 27)//HeadQA
                {
                    if (LoginEmployeeID == HeadQA)
                    {
                        i = true;//Success
                    }
                    else
                    {
                        i = false;//false
                    }
                }
                return i;
            }
        }
        //For Checking Valid Employee or Not for HOD Reviewers
        public bool AssessmentEmpValid(int INC_ID, int UniqueID, int RoleId)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                int EmpId = 0;
                Boolean i = true;
                int LoginEmployeeID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);

                var GetDetails = (from RHOD in dbEF.AizantIT_Incident_InvestigationReviewDeptHods
                                  join IM in dbEF.AizantIT_IncidentMaster on RHOD.IncidentMasterID equals IM.IncidentMasterID
                                  where RHOD.IncidentMasterID == INC_ID && RHOD.UniquePKID == UniqueID
                                  select new
                                  {
                                      RHOD.EmpID,
                                  }).ToList();
                foreach (var item in GetDetails)
                {
                    EmpId = (int)item.EmpID;
                }
                if (RoleId == 25)
                {
                    if (LoginEmployeeID == EmpId)
                    {
                        i = true; //Success
                    }
                    else
                    {
                        i = false;//Fail
                    }
                }
                return i;
            }
        }
        #endregion
        //For Getting Event CAPA Count

        public JsonResult GettingEventCAPACount(int BaseID, int QualityTypeID)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    int NoOfCAPAs = (from CA in dbEF.AizantIT_CAPA_ActionPlan
                                     join CM in dbEF.AizantIT_CAPAMaster on CA.CAPAID equals CM.CAPAID
                                     where CA.BaseID == BaseID && CM.QualityEvent_TypeID == QualityTypeID && CA.CurrentStatus != 10 //Current Status 10 means Deleted CAPA 
                                     select CA).Count();
                    return Json(NoOfCAPAs, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M6:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        public void MakeIncidentInitiaotrAttachmentsToSingleFile(int IncidentID)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                // List<string> objCCNAttachmentFilePaths = new List<string>();
                List<string> objIncidentAttachmentFilePaths = (from t1 in dbEF.AizantIT_IncidentMaster
                                                               join t2 in dbEF.AizantIT_Incident_InitiatorAttachments on t1.IncidentMasterID equals t2.IncidentMasterID into LeftJoinT2
                                                               from t2 in LeftJoinT2.DefaultIfEmpty()
                                                               where t1.IncidentMasterID == IncidentID
                                                               select t2.FilePath).ToList();
                if (objIncidentAttachmentFilePaths.Count > 0)
                {
                    AizantIT_Incident_InitiatorAttachments objIncidentttachmentAsSingle = new AizantIT_Incident_InitiatorAttachments();
                    objIncidentttachmentAsSingle.IncidentMasterID = IncidentID;
                    QMSAttachments objAttachmentsCommon = new QMSAttachments();
                    objIncidentttachmentAsSingle.FilePath = objAttachmentsCommon.MergeAllTheFilesAsSingleFile(objIncidentAttachmentFilePaths, "Areas\\QMS\\Models\\Incident\\IncidentAttachments\\MergedAttachments", IncidentID + DateTime.Now.ToString("yyyMMddHHmmssfff") + ".pdf");
                    //dbEF..Add(objIncidentttachmentAsSingle);
                }
                dbEF.SaveChanges();
            }
        }
        #region All attachments are make as Single
        public void MakeIncidentAttachmentsToSingleFile(int IncidentID)
        {
            AizantIT_DevEntities dbEF = new AizantIT_DevEntities();
            List<string> objIncidentAttachmentFilePaths = new List<string> { };
            var IncInitiatorFileAttachment = (from t1 in dbEF.AizantIT_IncidentMaster
                                              join t2 in dbEF.AizantIT_Incident_InitiatorAttachments on t1.IncidentMasterID equals t2.IncidentMasterID
                                              where t1.IncidentMasterID == IncidentID
                                              select t2).ToList();
            foreach (var item in IncInitiatorFileAttachment)
            {
                objIncidentAttachmentFilePaths.Add(item.FilePath);
            }
            var InvestgatorAttachments = (from t1 in dbEF.AizantIT_IncidentMaster
                                          join t2 in dbEF.AizantIT_Incident_IncidentAttachments on t1.IncidentMasterID equals t2.IncidentMasterID
                                          where t1.IncidentMasterID == IncidentID
                                          select t2).ToList();
            foreach (var item in InvestgatorAttachments)
            {
                objIncidentAttachmentFilePaths.Add(item.FilePath);
            }

            var IncCAPACompleteAttachment = (from t1 in dbEF.AizantIT_CAPA_ActionPlan
                                             join t2 in dbEF.AizantIT_CAPAMaster on t1.CAPAID equals t2.CAPAID
                                             join t3 in dbEF.AizantIT_CapaCompltFileAttachments on t1.CAPAID equals t3.CapaID
                                             where t1.BaseID == IncidentID && t2.QualityEvent_TypeID == 1
                                             select t3).ToList();
            foreach (var item in IncCAPACompleteAttachment)
            {
                objIncidentAttachmentFilePaths.Add(item.FilePath);
            }

            var objAlreadyMergedIncidentAttachments = (from item in dbEF.AizantIT_IncidentFinalReport
                                                       where item.IncidentID == IncidentID
                                                       select item).SingleOrDefault();
            if (objAlreadyMergedIncidentAttachments != null) //There's already a Merged Attachments File
            {
                if (objAlreadyMergedIncidentAttachments.AttachmentPath != null)
                {
                    //Delete old Merged File.
                    QMSAttachments objAttachmentsCommon = new QMSAttachments();
                    objAttachmentsCommon.RemovePhysicalPathFile(Server.MapPath("~\\" + objAlreadyMergedIncidentAttachments.AttachmentPath));
                }
                //Update Newly Merged File
                if (objIncidentAttachmentFilePaths.Count > 0)
                {
                    AizantIT_IncidentFinalReport objIncidentAttachmentAsSingle =
                           dbEF.AizantIT_IncidentFinalReport.SingleOrDefault(p => p.IncidentID == IncidentID);
                    QMSAttachments objAttachmentsCommon = new QMSAttachments();
                    objIncidentAttachmentAsSingle.AttachmentPath = objAttachmentsCommon.MergeAllTheFilesAsSingleFile(objIncidentAttachmentFilePaths, "Areas\\QMS\\Models\\Incident\\IncidentAttachments\\MergedAttachments", IncidentID + DateTime.Now.ToString("yyyMMddHHmmssfff") + ".pdf");
                }
            }
            else
            {
                if (objIncidentAttachmentFilePaths.Count > 0)
                {
                    AizantIT_IncidentFinalReport objIncidentAttachmentAsSingle = new AizantIT_IncidentFinalReport();
                    objIncidentAttachmentAsSingle.IncidentID = IncidentID;
                    objIncidentAttachmentAsSingle.CreatedDate = DateTime.Now;
                    QMSAttachments objAttachmentsCommon = new QMSAttachments();
                    objIncidentAttachmentAsSingle.AttachmentPath = objAttachmentsCommon.MergeAllTheFilesAsSingleFile(objIncidentAttachmentFilePaths, "Areas\\QMS\\Models\\Incident\\IncidentAttachments\\MergedAttachments", IncidentID + DateTime.Now.ToString("yyyMMddHHmmssfff") + ".pdf");
                    dbEF.AizantIT_IncidentFinalReport.Add(objIncidentAttachmentAsSingle);
                }
            }
            dbEF.SaveChanges();
        }
        #endregion
        //To Generate Incident Final Report
        #region Final report and Attachments Merged
        public void GenerateFinalIncidentReport(string onlyReportPath, int incidentID, AizantIT_DevEntities dbEF)
        {
            List<string> ObjCCNFilesToMerge = new List<string>();
            ObjCCNFilesToMerge.Insert(0, onlyReportPath);
            var objIncidentListAttachment = (from t1 in dbEF.AizantIT_IncidentFinalReport
                                             where t1.IncidentID == incidentID
                                             select t1).ToList();
            if (objIncidentListAttachment != null)
            {
                foreach (var item in objIncidentListAttachment)
                {
                    ObjCCNFilesToMerge.Insert(1, item.AttachmentPath);
                }
            }
            //throw new Exception("Incident Report Error");
            // MergeAllTheFilesAsSingleFile,Update the DB with File path.
            AizantIT_IncidentFinalReport objAizantITReport = new AizantIT_IncidentFinalReport();
            objAizantITReport = dbEF.AizantIT_IncidentFinalReport.SingleOrDefault(x => x.IncidentID == incidentID);
            QMSAttachments objAttachmentsCommon = new QMSAttachments();
            objAizantITReport.FinalReportPath = objAttachmentsCommon.MergeAllTheFilesAsSingleFile(ObjCCNFilesToMerge, "Areas\\QMS\\Models\\Incident\\Reports\\FinalReport", incidentID + DateTime.Now.ToString("yyyMMddHHmmssfff") + ".pdf");
            dbEF.SaveChanges();
        }
        #endregion

        public JsonResult IncidentGetFinalReportPDFPath(int IncidentID, int CurrentStatus)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                string Msg = "", MsgType = "";
                //int TypeID, int DocOrVersionID, int OfEmpID, int OfRoleID
                try
                {
                    var GetReportGenerate = (from t1 in dbEF.AizantIT_IncidentFinalReport
                                             where t1.IncidentID == IncidentID
                                             select t1).SingleOrDefault();
                    if (GetReportGenerate != null)
                    {
                        if (GetReportGenerate.CurrentStatus != CurrentStatus)
                        {
                            MakeIncidentAttachmentsToSingleFile(IncidentID);
                            //Generate Reports
                            IncidentReportGenerate(IncidentID);
                        }
                    }
                    else
                    {
                        MakeIncidentAttachmentsToSingleFile(IncidentID);
                        //Generate Reports
                        IncidentReportGenerate(IncidentID);
                    }




                    //DocObjects docObjects = new DocObjects();
                    var GetFilePath = (from T1 in dbEF.AizantIT_IncidentFinalReport
                                       where T1.IncidentID == IncidentID
                                       select T1.FinalReportPath).SingleOrDefault();
                     string filepath = "";
                    if (GetFilePath != null)
                    {
                        string FullfilePath = Server.MapPath("~\\" + GetFilePath);
                        filepath = GetFilePath;
                       // string[] filenames = GetFilePath.Split('\\');

                        if (System.IO.File.Exists(FullfilePath))
                        {
                            //string Path = "<iframe src=\"" + System.Web.VirtualPathUtility.ToAbsolute("~/Areas\\QMS\\Models\\Incident\\Reports\\FinalReport\\web\\viewer.html?file=" + filenames[filenames.Length - 1]) + "#toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" frameborder=\"0\" height=\"700px\" ></iframe>";
                            Msg = filepath;
                            MsgType = "success";
                        }
                        else
                        {
                            Msg = "File does not exist.";
                            MsgType = "error";
                        }
                    }
                    else
                    {
                        Msg = "File does not exist.";
                        MsgType = "error";
                    }

                    //JavaScriptSerializer js = new JavaScriptSerializer();
                    return Json(new { msgType = MsgType, data = Msg }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    string strline = HelpClass.LineNo(ex);
                    string strMsg = HelpClass.SQLEscapeString(ex.Message);
                    Msg = "PDF_Viewer:" + strline + "  " + strMsg;
                    MsgType = "error";
                    return Json(new { msgType = MsgType, data = Msg }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        public async Task<ActionResult> DownloadIncidentReport(int IncidentID)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    var GetIncidnetNumber = (from t1 in dbEF.AizantIT_IncidentMaster
                                             where t1.IncidentMasterID == IncidentID
                                             select t1.IRNumber).SingleOrDefault();
                    string Downloadfilename = "Incident_Report" + GetIncidnetNumber + ".pdf";
                    string filePath = (from objCCNeport in dbEF.AizantIT_IncidentFinalReport
                                       where objCCNeport.IncidentID == IncidentID
                                       select objCCNeport.FinalReportPath).SingleOrDefault();
                    byte[] fileBytes;

                    fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~\\" + filePath));
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, Downloadfilename);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    //TempData["SysErrorMsg"] = ex.Message.Replace("'", "\'");
                    TempData["Exception"] = HelpClass.SQLEscapeString(ex.Message);
                    return RedirectToAction("IncidentReportList", new { ShowType = 17 });
                }
            }
        }
        public JsonResult CAPACloseAfterReportGeneration(int CAPAID)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                try
                {
                    var getIncidentID = (from objAP in dbEF.AizantIT_CAPA_ActionPlan
                                         join objCM in dbEF.AizantIT_CAPAMaster on objAP.CAPAID equals objCM.CAPAID
                                         where objAP.CAPAID == CAPAID && objCM.QualityEvent_TypeID == 1
                                         select objAP.BaseID).SingleOrDefault();
                    return Json(new { hasError = false }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "INCI_M6:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public PartialViewResult TypeOfIncidentCreate()
        {
            try
            {
                return PartialView();
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "TypeCM_M1:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }
        public JsonResult TypeOfIncidentSubmit(AizantIT_TypeOfIncident aizantIT_TypeOfIncident)
        {
            AizantIT_DevEntities dbEF = new AizantIT_DevEntities();
            using (DbContextTransaction transaction = dbEF.Database.BeginTransaction())
            {
                try
                {
                    AizantIT_TypeOfIncident objTC = new AizantIT_TypeOfIncident();
                    objTC.TypeofCategoryID = aizantIT_TypeOfIncident.TypeofCategoryID;
                    objTC.TypeofIncident = aizantIT_TypeOfIncident.TypeofIncident.Trim();
                    objTC.Description = aizantIT_TypeOfIncident.Description;
                    objTC.CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                    objTC.CreatedDate = DateTime.Now;
                    var Count = (from Incident in dbEF.AizantIT_TypeOfIncident
                                 where Incident.TypeofIncident == objTC.TypeofIncident
                                 select  Incident.TypeofIncident).SingleOrDefault();
                    //var Count1 = dbEF.AizantIT_TypeOfIncident.FirstOrDefault(p => p.TypeofIncident == objTC.TypeofIncident);
                    if (Count == null)
                    {
                        if (ModelState.IsValid)
                        {
                            dbEF.AizantIT_TypeOfIncident.Add(objTC);
                            dbEF.SaveChanges();
                            transaction.Commit();
                            return Json(objTC, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(0, JsonRequestBehavior.AllowGet);
                    }

                    return Json(0, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "TypeCM_M2:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    transaction.Rollback();
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        //For getting TMS Role Exist or Not
       
        public JsonResult RefreshTypeIncident()//for Refresh Dropdown RefreshItem
        {
            AizantIT_DevEntities dbEF = new AizantIT_DevEntities();
            try
            {
                var TypeChange1 = (from T in dbEF.AizantIT_TypeOfIncident
                                   select new { T.TypeofCategoryID, T.TypeofIncident }).ToList();
                List<SelectListItem> TypeChangeList = new List<SelectListItem>();
                TypeChangeList.Add(new SelectListItem { Text = "select ", Value = "0" });
                foreach (var q1 in TypeChange1)
                {
                    TypeChangeList.Add(new SelectListItem { Text = q1.TypeofIncident, Value = q1.TypeofCategoryID.ToString() });
                }
                return Json(TypeChangeList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M21:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}


