﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using QMS_BusinessLayer;
using AizantIT_PharmaApp.Common.CustomFilters;
using Aizant_Enums;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;
using System.Diagnostics;
using QMS_BO.QMS_Errata_BO;
using Aizant_API_Entities;
using AizantIT_PharmaApp.Common;
using System.Threading.Tasks;
using QMS_BO.QMSComonBO;
using AizantIT_PharmaApp.Areas.QMS.Controllers.Common;

namespace AizantIT_PharmaApp.Areas.QMS.Controllers.Errata
{
    //24 Initiator,25 Hod,26 QA,27 HeadQA
    [RouteArea]
    [CustAuthentication]
    [CustAuthorization(
        (int)AizantEnums.Modules.QMS,
        (int)QMS_Roles.Initiator,
        (int)QMS_Roles.HOD,
        (int)QMS_Roles.QA,
        (int)QMS_Roles.HeadQA,
        (int)QMS_Roles.Investigator,
        (int)QMS_Roles.AdminRole)]
    public class ErrataController : Controller
    {
        QMS_BAL Qms_bal = new QMS_BAL();
        UMS_BAL objUMS_BAL = new UMS_BAL();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private AizantIT_DevEntities dbEF = new AizantIT_DevEntities();
        #region Errata List
        // GET: QMS_Errata
        public ActionResult Errata_List(int ShowType, int Notification_ID = 0)
        {
            try
            {
                ErrataBO ERTABO = new ErrataBO();
                EmployeID(ERTABO);

                switch (ShowType)
                {
                    case 1:
                        ShowType = (int)QMS_ErrataListTypes.E_InitiatorList;
                        break;
                    case 2:
                        ShowType = (int)QMS_ErrataListTypes.E_HODReviewList;
                        break;
                    case 3:
                        ShowType = (int)QMS_ErrataListTypes.E_InitiatorRevertedList;
                        break;
                    case 4:
                        ShowType = (int)QMS_ErrataListTypes.E_QAReviewList;
                        break;
                    case 5:
                        ShowType = (int)QMS_ErrataListTypes.E_HodRevertedList;
                        break;
                    case 6:
                        ShowType = (int)QMS_ErrataListTypes.E_HQAReviewList;
                        break;
                    case 7:
                        ShowType = (int)QMS_ErrataListTypes.E_QAverification;
                        break;
                    case 8:
                        ShowType = (int)QMS_ErrataListTypes.E_MainList;
                        break;
                    case 9:
                        ShowType = (int)QMS_ErrataListTypes.E_PendingList;
                        break;
                    case 10:
                        ShowType = (int)QMS_ErrataListTypes.E_ApprovedList;
                        break;
                    case 11:
                        ShowType = (int)QMS_ErrataListTypes.E_RejectedList;
                        break;
                    case 12:
                        ShowType = (int)QMS_ErrataListTypes.E_Verify;
                        break;
                    case 13:
                        ShowType = (int)QMS_ErrataListTypes.E_Manage;
                        break;
                    case 15:
                        ShowType = (int)QMS_ErrataListTypes.E_Complete;
                        break;
                }
                TempData["QueryE"] = ShowType;
                TempData.Keep("QueryE");
                int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                NotificationUpdate(Notification_ID, EmpIDs);
                TempData["UI"] = ERTABO.EmpID;

                if (ShowType == 1 || ShowType == 2 || ShowType == 8|| ShowType == 15)
                {
                    TempData["RoleID"] = 24;
                }
                if (ShowType == 2 || ShowType ==5 || ShowType == 8)
                {
                    TempData["RoleID"] = 25;
                }
                if (ShowType == 4 || ShowType == 7 || ShowType == 8)
                {
                    TempData["RoleID"] = 26;
                }
                if (ShowType == 6 || ShowType == 8)
                {
                    TempData["RoleID"] = 27;
                }
                //For Charts
                if (ShowType == 9 || ShowType == 10 || ShowType == 11 || ShowType == 12)
                {
                    TempData.Keep("RoleIDFromCharts");
                    TempData.Keep("DeptIDFromDashboard");
                    TempData.Keep("FromDateDashboard");
                    TempData.Keep("ToDateDashboard");
                }
                //default value zero
                else
                {
                    TempData["ToDateDashboard"] = 0;
                    TempData["FromDateDashboard"] = 0;
                    TempData["DeptIDFromDashboard"] = 0;
                    TempData["RoleIDFromCharts"] = 0;
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "ERT_M1:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
            }
            return View();
        }
        public JsonResult GetErrataList()
     {
            try
            {
                int iDisplayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int iDisplayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int iSortCol_0 = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir_0 = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                string Role = Request.Params["Role"];
                int EmployeeID= Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                int RoleIDCharts = Convert.ToInt32(Request.Params["RoleIDCharts"]);
                int deptIdDashboard = Convert.ToInt32(Request.Params["DeptDashboard"]);
                string FromDateDashboard = Request.Params["FromDateDashboard"];
                string ToDateDashboard = Request.Params["ToDateDashboard"];

                //start commented by pradeep 31-01-2020

                //For column wise search implementation
                //var sSearch_EmpCode = HttpContext.Current.Request.Params["sSearch_0"].ToString();
                //string sSearch_Rownumber = Request.Params["sSearch_0"];
                //int sSearch_ER_ID = Convert.ToInt32(Request.Params["sSearch_1"]);
                string sSearch_ErrataFileNo = Request.Params["sSearch_2"];
                string sSearch_DeptName = Request.Params["sSearch_3"];
                string sSearch_DocumentNo = Request.Params["sSearch_4"];
                string sSearch_DocTitle = Request.Params["sSearch_5"];
                string sSearch_SectionNo = Request.Params["sSearch_6"];
                string sSearch_EmpName = Request.Params["sSearch_7"];
                string sSearch_Initiator_SubmitedDate = Request.Params["sSearch_8"];
                string sSearch_Status = Request.Params["sSearch_9"];



                //end commented by pradeep 31-01-2020

                List<ErrataBO> listEmployees = new List<ErrataBO>();
                int filteredCount = 0;
                string FromDate = "";
                string ToDate = "";
                if (FromDateDashboard == "0")
                {
                    FromDate = "";
                }
                else
                {
                    FromDate = FromDateDashboard;
                }
                if (ToDateDashboard == "0")
                {
                    ToDate = "";
                }
                else
                {
                    ToDate = ToDateDashboard;
                }
                DataSet dt = Qms_bal.GetErrataListDalBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, Role, EmployeeID, RoleIDCharts, deptIdDashboard, FromDate, ToDate,  
                     sSearch_DeptName, sSearch_ErrataFileNo, sSearch_DocumentNo, sSearch_DocTitle, sSearch_EmpName, sSearch_Initiator_SubmitedDate, sSearch_SectionNo, sSearch_Status);
                if (dt.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow rdr in dt.Tables[0].Rows)
                    {
                        ErrataBO ERRTBO = new ErrataBO();
                        ERRTBO.Rownumber = Convert.ToInt32(rdr["RowNumber"]);
                        ERRTBO.ER_ID = Convert.ToInt32(rdr["ER_ID"]);
                        ERRTBO.DeptName = ((rdr["Department"]).ToString());
                        ERRTBO.ErrataFileNo = (rdr["ER_file_number"]).ToString();
                        ERRTBO.DocumentNo = (rdr["DocumentNo"].ToString());
                        ERRTBO.DocTitle = (rdr["DocTitle"]).ToString();
                        ERRTBO.EmpName = (rdr["preparedBy"]).ToString();
                        ERRTBO.Initiator_SubmitedDate = (rdr["CreatedDate"].ToString());
                        ERRTBO.SectionNo = Convert.ToString((rdr["Section"].ToString()));
                        ERRTBO.Status = ((rdr["CurrentStatus"])).ToString();
                        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                        listEmployees.Add(ERRTBO);
                    }
                }
                return Json(new
                {
                    hasError = false,
                    iTotalRecords = filteredCount,
                    iTotalDisplayRecords = filteredCount,
                    aaData = listEmployees
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = "Yes";
                return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);
            }
        }
        //Session Validation get Login EmployeeID
        public void EmployeID(ErrataBO ERTABO)
        {
            if (Session["UserDetails"] !=null)
            {
                ERTABO.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            }
        }
        public void NotificationUpdate(int Notification_ID, int[] EmpIDs)
        {
            ViewBag.ERID = 0;
            if (Notification_ID != 0)
            {
                objUMS_BAL = new UMS_BAL();
                objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs); //2 represents NotifyEmp had visited the page through notification link.
                var GetEID = (from E in dbEF.AizantIT_ErrataNotificationLink
                              where E.NotificationID == Notification_ID
                              select new { E.ERID }).ToList();
                foreach (var item in GetEID)
                {
                    ViewBag.ERID = item.ERID;
                }
            }
        }
        #endregion
        #region Errata Create
        public PartialViewResult Errata_Create(int id)
        {
            try
            {
                ErrataBO ERTABO = new ErrataBO();
                //Create
                if (id == 0)
                {
                    DataTable dt = new DataTable();
                    EmployeID(ERTABO);
                    //To get departments and Document type
                    BindDropDownErrataCreate(ERTABO,id);
                }
                //Update
                if (id != 0)
                {
                    ERTABO = RetrieveData(id);
                    //RetriveActionBy(id);
                    //For binding Department Names and Document type
                    BindDropDownErrataCreate(ERTABO,id);
                    var RefNo = (from dv in dbEF.AizantIT_DocVersion
                                 join DM in dbEF.AizantIT_DocumentMaster on dv.DocumentID equals DM.DocumentID
                                 where DM.DocumentTypeID == ERTABO.DocumentType && DM.DeptID == ERTABO.DeptID && ( dv.Status == "A" || dv.Status=="R") && dv.EffectiveDate != null
                                 select new { dv.VersionID, DM.DocumentNumber }).ToList();
                    ViewBag.ReferenceNumber = new SelectList(RefNo.ToList(), "VersionID", "DocumentNumber", ERTABO.versionID);
                    int VersionID = Convert.ToInt32(ERTABO.versionID);
                    if (RefNo.Count > 0)
                    {
                        bool has = RefNo.Any(t => t.VersionID == VersionID);
                        if (has == false)
                        {
                            var DocumentID = (from t1 in dbEF.AizantIT_DocVersion
                                              where t1.VersionID == VersionID
                                              select t1.DocumentID).SingleOrDefault();

                            var GetNewVersionID = (from t1 in dbEF.AizantIT_DocVersion
                                                   orderby t1.VersionID descending
                                                   where t1.DocumentID == DocumentID && (t1.Status == "A" || t1.Status == "R") && t1.EffectiveDate != null
                                                   select t1.VersionID);
                            int NewVersionID = GetNewVersionID.First();
                            ERTABO.DocTitle = (from t1 in dbEF.AizantIT_DocVersionTitle
                                              join t2 in dbEF.AizantIT_DocVersion on t1.DocTitleID equals t2.DocTitleID
                                              where t2.VersionID == NewVersionID
                                              select t1.Title).SingleOrDefault();

                            ViewBag.ReferenceNumber = new SelectList(RefNo.ToList(), "VersionID", "DocumentNumber", NewVersionID);

                        }
                    }
                    //For Binding HOD EmpName on Assignto Dropdown in place of using linq query 12-02-2019
                    objUMS_BAL = new UMS_BAL();
                    List<SelectListItem> objAssignHod = new List<SelectListItem>();
                    DataTable dtemp = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(ERTABO.DeptID), (int)QMS_Roles.HOD, true); //25=hod
                    foreach (DataRow RPItem in dtemp.Rows)
                    {
                        objAssignHod.Add(new SelectListItem()
                        {
                            Text = (RPItem["EmpName"]).ToString(),
                            Value = (RPItem["EmpID"]).ToString()
                        });
                    }
                    ViewBag.HOD = new SelectList(objAssignHod.ToList(), "Value", "Text", ERTABO.HOD_EmpID);
                    Initiator_comments(ERTABO);
                }
                List<int> ErratRatificationNo = new List<int> { 3, 11 };//3 Errata 11 Ratification
                var TypeOfErrata = (from t in dbEF.AizantIT_QualityEvent
                                    where ErratRatificationNo.Contains(t.QualityEvent_TypeID)
                             select new { t.QualityEvent_TypeID, t.QualityEvent_TypeName }).ToList();
                ViewBag.TypeOfCorrection = new SelectList(TypeOfErrata.ToList(), "QualityEvent_TypeID", "QualityEvent_TypeName", ERTABO.TypeofCorrection);
                return PartialView(ERTABO);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "ERT_M2:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }
        [HttpPost]
        public JsonResult Errat_Create(ErrataBO ERTABO, string Status)
        {
            try
            {
                var ErrataNumber = "";
                DataTable dt = new DataTable();
                EmployeID(ERTABO);//Method
                ERTABO.Initiator_EmpID = ERTABO.EmpID;
                if(Status == "Created")
                {
                    dt = Qms_bal.ErrataFileInsertBal(ERTABO, Status);
                    ErrataNumber = dt.Rows[0][0].ToString();
                }
                else
                {
                Boolean Result = ToCheckEmpValidityErrata(ERTABO.ER_ID, 24);
                if(Result == true)
                {
               dt = Qms_bal.ErrataFileInsertBal(ERTABO, Status);
                        ErrataNumber = ERTABO.ErrataFileNo;
                }
                else
                {
                    return Json(new { hasError = true, data = "You are not authorized User to do action." }, JsonRequestBehavior.AllowGet);
                }
                }
                return Json(new { hasError = false, data = ErrataNumber }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "ERT_M3:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        public void BindDropDownErrataCreate(ErrataBO errataBO ,int id)
        {
            CCNBal cCNBal = new CCNBal();
            EmployeID(errataBO);
            errataBO.DocumentType = (id == 0 ? -1 : errataBO.DocumentType);
            //To get Departments and Document type
            DataSet ds = cCNBal.GetddlBindingCCNCreate(errataBO.EmpID);
            //Department binding
            List<SelectListItem> Dep = new List<SelectListItem>();
            Dep.Add(new SelectListItem
            {
                Text = "Select",
                Value = "0",
            });
            foreach (DataRow rdr in ds.Tables[0].Rows)
            {
                Dep.Add(new SelectListItem
                {
                    Text = (rdr["DepartmentName"]).ToString(),
                    Value = (rdr["DeptID"]).ToString(),
                    Selected = (Convert.ToInt32(rdr["DeptID"]) == errataBO.DeptID ? true : false)
                });
            }
            ViewBag.Department = new SelectList(Dep.ToList(), "Value", "Text", errataBO.DeptID);
            //Document Type binding
            List<SelectListItem> DocumentTypeList = new List<SelectListItem>();
            DocumentTypeList.Add(new SelectListItem
            {
                Text = "Select",
                Value = "-1",
            });
            foreach (DataRow rdr in ds.Tables[1].Rows)
            {
                DocumentTypeList.Add(new SelectListItem
                {
                    Text = (rdr["DocumentType"]).ToString(),
                    Value = (rdr["DocumentTypeID"]).ToString(),
                    Selected = (Convert.ToInt32(rdr["DocumentTypeID"]) == errataBO.DocumentType ? true : false)
                });
            }
            DocumentTypeList.Add(new SelectListItem
            {
                Text = "Others",
                Value = "0",
            });
            ViewBag.DocumentType = new SelectList(DocumentTypeList.ToList(), "Value", "Text", errataBO.DocumentType);
        }
        #endregion

        #region AutoGeneratedNumber
        //Bind Errata file number 
        [HttpGet]
        public JsonResult BindErrataNo(int DeptID,int TypeOfCorrection)
        {
            try
            {
                DataTable dt1 = new DataTable();
                //5 means Errata
                dt1 = Qms_bal.BalGetAutoGenaratedNumber(DeptID,TypeOfCorrection);
                var ErrataNumber = dt1.Rows[0][0].ToString();
                return Json(new { hasError = false, data = ErrataNumber }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "ERT_M4:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region HOD_Review
        public ErrataBO BindLatestComments(ErrataBO objErrata)
        {
            int ActionStatus = 0;
            List<int> objErrataStatus = new List<int>() { 1,2,4,5,13,6,15};
            if (objErrataStatus.Contains(objErrata.CurrentStatus))
            {
                ActionStatus=objErrata.CurrentStatus;
            }
            var CommentsLatest = (from QR in dbEF.AizantIT_ErrataHistory
                            where QR.ER_ID == objErrata.ER_ID && QR.ActionStatus == ActionStatus
                            select new { QR.Comments }).ToList();
            foreach (var C1 in CommentsLatest)
            {
                objErrata.LatestCommentsErrata = C1.Comments;
            }
                return objErrata;
        }
        public PartialViewResult Errata_HODReview(int id, string Number, string R)
        {
            try
            {
                TempData.Keep("QueryE");
                ErrataBO ERTABO = new ErrataBO();
                EmployeID(ERTABO);
                //RetriveActionBy(id);
                ERTABO.EmpName = (from E in dbEF.AizantIT_EmpMaster
                           where E.EmpID == ERTABO.EmpID
                           select  (E.FirstName + "" + E.LastName) ).SingleOrDefault();
                
                //ViewBag.EmployeeName = ERTABO.EmpName;
                TempData["EmpID"] = ERTABO.EmpID;
                ERTABO = BindData(id, Number);
                //Bind latest comments
                BindLatestComments(ERTABO);
                _ErrataImpactPoints(ERTABO);
                return PartialView(ERTABO);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "ERT_M6:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }

        public PartialViewResult Errata_QAReview(int id, string Number, string R)
        {
            try
            {
                TempData.Keep("QueryE");
                ErrataBO ERTABO = new ErrataBO();
                EmployeID(ERTABO);
                //RetriveActionBy(id);
                 ERTABO.EmpName = (from E in dbEF.AizantIT_EmpMaster
                           where E.EmpID == ERTABO.EmpID
                           select ( E.FirstName + "" + E.LastName) ).SingleOrDefault();
               
                ViewBag.EmployeeName = ERTABO.EmpName;
                TempData["EmpID"] = ERTABO.EmpID;
                ERTABO = BindData(id, Number);
                //Bind latest comments
                BindLatestComments(ERTABO);
                _ErrataImpactPoints(ERTABO);
                return PartialView(ERTABO);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "ERT_M6:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }
        //get all data 
        public ErrataBO RetrieveData(int id)
        {
            ErrataBO ERTA = new ErrataBO();
            var ERTA1 = (from EM in dbEF.AizantIT_ErrataMaster
                         where EM.ER_ID == id
                         select new
                         {
                             EM.ER_FileNumber,
                             EM.DeptID,
                             EM.ProposedCorrectionDoc_Desc,
                             EM.InitiatorEmpID,
                             EM.InitiatorSubmitedDate,
                             EM.SectionNo,
                             EM.CurrentStatus,
                             EM.ER_ID,
                             EM.HOD_EmpID,
                             EM.TypeofCorrection,
                             EM.JustificationDesc,
                             EM.QA_EmpID,
                             EM.Head_QA_Empid,
                             EM.IsMaster,
                             EM.IsSoftCopy,
                             EM.IsOthers,
                             EM.DocType,
                             EM.OtherDocDescriptions,
                             EM.EvaluationDesc
                         }).ToList();
            foreach (var item in ERTA1)
            {
                ERTA.ErrataFileNo = item.ER_FileNumber.ToString();
                ERTA.ER_ID = item.ER_ID;
                ERTA.DeptID = (int)item.DeptID;
                ERTA.SectionNo = item.SectionNo;
                ERTA.Initiator_EmpID = (int)item.InitiatorEmpID;
                ERTA.Initiator_SubmitedDate = Convert.ToDateTime(item.InitiatorSubmitedDate).ToString("dd MMM yyyy");
                ERTA.DocumentType = (int)item.DocType;
                ERTA.ProposedCorrectionDoc_Desc = item.ProposedCorrectionDoc_Desc;
                ERTA.CurrentStatus = (int)item.CurrentStatus;
                ERTA.HOD_EmpID = (int)item.HOD_EmpID;
                ERTA.TypeofCorrection = (int)item.TypeofCorrection;
                if (ERTA.CurrentStatus == 6 || ERTA.CurrentStatus == 15 || ERTA.CurrentStatus == 8)
                {
                    ERTA.Evaluation_Desc = item.EvaluationDesc;
                    ERTA.QA_EmpID = (int)item.QA_EmpID;
                }
                DocumentDetialsClass objDocument = new DocumentDetialsClass();
                QMSCommonActions CommonDocBo = new QMSCommonActions();
                if (ERTA.DocumentType != 0)
                {
                    objDocument = CommonDocBo.GetDocumentDetails(ERTA.ER_ID, 3);
                        ERTA.DocumentNo = objDocument.DocumentNumber;
                        ERTA.DocTitle = objDocument.DocumentName;
                        ERTA.versionID = objDocument.VersionID.ToString();
                        ERTA.DocumentTypeName = objDocument.DocumentType;
                }
                if (ERTA.DocumentType == 0)
                {
                    objDocument = CommonDocBo.GetOtherDocumentDetails(ERTA.ER_ID, 3);
                        ERTA.DocumentNo = objDocument.DocumentNumber;
                        ERTA.DocTitle1 = objDocument.DocumentName;
                        //For Hod Page
                        ERTA.DocTitle = objDocument.DocumentName;
                        ERTA.DocumentTypeName = "Others";
                }
                if ( ERTA.CurrentStatus == 4 || ERTA.CurrentStatus == 5 || ERTA.CurrentStatus == 6 || ERTA.CurrentStatus == 15 || ERTA.CurrentStatus == 13 || ERTA.CurrentStatus == 12 || ERTA.CurrentStatus == 8)//
                {
                    ERTA.JustificationDesc = item.JustificationDesc;
                }
              
                if (ERTA.CurrentStatus == 5)//
                {
                    ERTA.HeadQA_EmpID = (int)item.Head_QA_Empid;
                }
                if ( ERTA.CurrentStatus == 6 || ERTA.CurrentStatus == 8 || ERTA.CurrentStatus == 12)
                {
                    ERTA.MasterDocument = Convert.ToBoolean(item.IsMaster);
                    ERTA.OtherDocumnets = item.OtherDocDescriptions;
                    ERTA.SoftCopy = Convert.ToBoolean(item.IsSoftCopy);
                    ERTA.Other_DocCheck = Convert.ToBoolean(item.IsOthers);
                }
            }
            return ERTA;
        }
        public ErrataBO BindData(int id, string number)
        {
            ErrataBO ERTA = new ErrataBO();
            ERTA = RetrieveData(id);
            //tempdata
            TempData["ER_ID"] = ERTA.ER_ID;
            //department name 
            ERTA.DeptName = (from dm in dbEF.AizantIT_DepartmentMaster
                            where dm.DeptID == ERTA.DeptID
                            select  dm.DepartmentName ).SingleOrDefault();
           
            //Impact points            
            var impact = (from imp in dbEF.AizantIT_Errata_ImpactOfChangeMaster
                          select new
                          {
                              imp.ImpactChangeName,
                              imp.ImpactChangeID
                          }).ToList();
            int i = 1;
            foreach (var dr in impact)
            {
                if (i == 1)
                {
                    ERTA.Impoints1.ImpactPointID = Convert.ToInt32(dr.ImpactChangeID);
                    ERTA.Impoints1.ImpactName = Convert.ToString(dr.ImpactChangeName.ToString());
                }
                if (i == 2)
                {
                    ERTA.Impoints2.ImpactPointID = Convert.ToInt32(dr.ImpactChangeID);
                    ERTA.Impoints2.ImpactName = Convert.ToString(dr.ImpactChangeName.ToString());
                }
                if (i == 3)
                {
                    ERTA.Impoints3.ImpactPointID = Convert.ToInt32(dr.ImpactChangeID);
                    ERTA.Impoints3.ImpactName = Convert.ToString(dr.ImpactChangeName.ToString());
                }
                if (i == 4)
                {
                    ERTA.Impoints4.ImpactPointID = Convert.ToInt32(dr.ImpactChangeID);
                    ERTA.Impoints4.ImpactName = Convert.ToString(dr.ImpactChangeName.ToString());
                }
                i++;
            }
            //
            EmployeID(ERTA);
            //Initiator ,hod employee id not binded to Assigned to QA Dropdown
            List<int> QAEmpid = new List<int>() { ERTA.Initiator_EmpID, ERTA.HOD_EmpID, ERTA.EmpID };
            //Initiator ,HOD and QA employee id not binded to Assigned to HeadQA Dropdown
            List<int> HQAEmpid = new List<int>() { ERTA.Initiator_EmpID, ERTA.HOD_EmpID,ERTA.EmpID };
           
            //Type of Correction Name display
            ERTA.TypeofChangeName = (from T in dbEF.AizantIT_QualityEvent
                                    where T.QualityEvent_TypeID == ERTA.TypeofCorrection
                                    select  T.QualityEvent_TypeName ).SingleOrDefault();
            /*Assign QA dropdown*/
            List<SelectListItem> objAssignQA = new List<SelectListItem>();
            DataSet dtempqa = Qms_bal.GetEmpNamesbasedonRoleIDBal((int)QMS_Roles.QA, "A"); //26=QA
            objAssignQA.Add(new SelectListItem
            {
                Text = "Select",
                Value = "0",
            });
            foreach (DataRow Item in dtempqa.Tables[0].Rows)
            {
                if (!QAEmpid.Contains(Convert.ToInt32(Item["EmpID"])))
                {
                    objAssignQA.Add(new SelectListItem()
                    {
                        Text = (Item["EmpName"]).ToString(),
                        Value = (Item["EmpID"]).ToString(),
                         Selected = (Convert.ToInt32(Item["EmpID"]) == ERTA.QA_EmpID ? true : false)
                    });
                }
            }
            ERTA.ddl_QAEmpID = objAssignQA;
            /*For Binding Head QA Employee name*/
            List<SelectListItem> objAssignHQA = new List<SelectListItem>();
            DataSet dtemphqa = Qms_bal.GetEmpNamesbasedonRoleIDBal((int)QMS_Roles.HeadQA,"A"); //27=HQA
            objAssignHQA.Add(new SelectListItem
            {
                Text = "Select",
                Value = "0",
            });

            foreach (DataRow Item in dtemphqa.Tables[0].Rows)
            {
                if (!HQAEmpid.Contains(Convert.ToInt32(Item["EmpID"])))
                {
                    objAssignHQA.Add(new SelectListItem()
                    {
                        Text = (Item["EmpName"]).ToString(),
                        Value = (Item["EmpID"]).ToString(),
                        Selected = (Convert.ToInt32(Item["EmpID"]) == ERTA.HeadQA_EmpID ? true : false)
                    });
                }
            }
            ERTA.ddl_HeadQAEmpID = objAssignHQA;
            if (number == "2" || number == "4")
            {
              
                //Bind Revert Dropdown List by roles
                var HodEmpName = (from objErrata in dbEF.AizantIT_ErrataMaster
                                 join objemp in dbEF.AizantIT_EmpMaster on objErrata.HOD_EmpID equals objemp.EmpID
                                 where objErrata.ER_ID == ERTA.ER_ID
                                 select new { fullName = (objemp.FirstName + "" + objemp.LastName) }).Take(1);
                foreach (var EmpItem in HodEmpName)
                {
                    ViewBag.HODEmployeeName = "HOD ( " + EmpItem.fullName + " )";
                }
                List<int> ids1 = new List<int>() { 24, 25 };
                var role = (from R in dbEF.AizantIT_Roles
                            where ids1.Contains(R.RoleID)
                            select new { R.RoleID, R.RoleName }).ToList();
                ViewBag.SelectEmployee = new SelectList(role.ToList(), "RoleID", "RoleName");
            }
            //comments
            Initiator_comments(ERTA);
            return ERTA;
        }
        public void Initiator_comments(ErrataBO ERTA)
        {
            try
            {
                var Comments = (from QR in dbEF.AizantIT_ErrataHistory
                                where QR.ER_ID == ERTA.ER_ID && QR.ActionStatus == ERTA.CurrentStatus
                                select new { QR.Comments }).ToList();

                foreach (var C1 in Comments)
                {
                    if (ERTA.CurrentStatus == 3 || ERTA.CurrentStatus == 4 || ERTA.CurrentStatus == 13 || ERTA.CurrentStatus == 10)
                    {
                        ERTA.HOD_Comments = C1.Comments;
                    }

                    if (ERTA.CurrentStatus == 1 || ERTA.CurrentStatus == 2)
                    {
                        ERTA.initiator_Comments = C1.Comments;
                    }
                    if (ERTA.CurrentStatus == 5)
                    {
                        ERTA.QA_Comments = C1.Comments;
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message + ".";
            }
        }
        #endregion

        #region Json Method Actions
        //Bind Document Name calling from Incident ,Errata,CCN,NTF
        //[HttpGet]
        //public JsonResult BindDocumentName(int versionID, int DocumentTypeID)
        //{
        //    try
        //    {
        //        var HOD = (from DM in dbEF.AizantIT_DocumentMaster
        //                   join dv in dbEF.AizantIT_DocVersion on DM.DocumentID equals dv.DocumentID
        //                   join Vt in dbEF.AizantIT_DocVersionTitle on dv.DocTitleID equals Vt.DocTitleID
        //                   where dv.VersionID == versionID && DM.DocumentTypeID == DocumentTypeID
        //                   select new { Vt.Title, dv.VersionNumber }).Distinct().ToList();
        //        string BindDocname = "";
        //        int BindversionNumber = 0;
        //        foreach (var q1 in HOD)
        //        {
        //            BindDocname = q1.Title;//Document Name
        //            BindversionNumber = q1.VersionNumber;
        //        }
        //        return Json(new { hasError = false, data = BindDocname, BindversionNumber }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
        //        string ErMsg = "ERT_M5:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
        //        Aizant_log.Error(ErMsg);
        //        return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //Retrive  Errata values
        //public void RetriveActionBy(int id)
        //{
        //    int Empid = 0;
        //    var v1 = (from EH in dbEF.AizantIT_ErrataHistory
        //              where EH.ER_ID == id
        //              select new { EH.ActionBy }).ToList();
        //    foreach (var item in v1)
        //    {
        //        Empid = (int)item.ActionBy;
        //    }
        //    var empname = (from E in dbEF.AizantIT_EmpMaster
        //                   where E.EmpID == Empid
        //                   select new { Fullname = (E.FirstName + " " + E.LastName) }).ToList();
        //    foreach (var item in empname)
        //    {
        //        ViewBag.ActionBy = item.Fullname;
        //    }
        //}
        //Hod can Revert and Reject Errata File
        [HttpPost]
        public JsonResult RevertAndRejectErrataFile(ErrataBO ErrataBo)
        {
            try
            {
                int RoleID=0;
                //HOD Revert-- To Check Current Emp ID is equal to DB Employee ID
                if (ErrataBo.Status == "HOD")
                {
                    RoleID = 25;
                }
                else if(ErrataBo.Status == "HQA")
                {
                    RoleID = 27;
                }else if (ErrataBo.Status == "R")
                {
                    RoleID = ErrataBo.RoleId;
                }
                Boolean Result = ToCheckEmpValidityErrata(ErrataBo.ER_ID, RoleID);
                if(Result == true)
                {
                int i = Qms_bal.ErrataRevertBAL(ErrataBo);
                return Json(new { hasError = false, data = ErrataBo }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { hasError = true, data = "You are not authorized User to do action." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "ERT_M7:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult ApproveErrataFile(ErrataBO ErrataBo, string Status, List<ErrataImpactPoints> Impacts)
        {
            try
            {
                //Impact Point Insert Errata
                DataTable dtImpactlst = new DataTable();
                dtImpactlst.Columns.Add("ER_ID");
                dtImpactlst.Columns.Add("ImpactChangeID");
                dtImpactlst.Columns.Add("isImpacted");
                dtImpactlst.Columns.Add("ReferenceNumber");
                dtImpactlst.Columns.Add("Comments");
                DataRow dr;
                if(Impacts!= null)
                {
                    foreach (ErrataImpactPoints item in Impacts)
                    {
                        dr = dtImpactlst.NewRow();
                        dr["ER_ID"] = item.ER_Filenumber;
                        dr["ImpactChangeID"] = item.ImpactPointID;
                        dr["isImpacted"] = item.isyesorNo ;
                        dr["ReferenceNumber"] = item.DocNo;
                        dr["Comments"] = item.Comments;
                        dtImpactlst.Rows.Add(dr);
                    }
                }
                EmployeID(ErrataBo);
                int RoleID = 0;
                if(Status == "Submit" || Status == "Update")
                {
                    RoleID = 25;
                }
                if(Status == "QASubmit")
                {
                    RoleID = 26;
                }
                if(Status == "HQASubmit")
                {
                    RoleID = 27;
                }
                if (Status == "InitiatorComplete")
                {
                    RoleID = 24;
                }
                Boolean Result = ToCheckEmpValidityErrata(ErrataBo.ER_ID, RoleID);
                if (Result == true)
                {
                    int i = Qms_bal.ErrataSubmittedToQABal(ErrataBo, Status, dtImpactlst);
                    return Json(new { hasError = false, data = ErrataBo }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { hasError = true, data = "You are not authorized User to do action." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "ERT_M8:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AssignedToTempDataErrata(int DeptID, int RoleID, string FromDate, string ToDate)
        {
            try
            {
                TempData["RoleIDFromCharts"] = RoleID;
                TempData.Keep("RoleIDFromCharts");
                TempData["DeptIDFromDashboard"] = DeptID;
                TempData.Keep("DeptIDFromDashboard");
                TempData["FromDateDashboard"] = FromDate == "" ? DateTime.Now.AddDays(-365).ToString("dd MMM yyyy") : FromDate;
                TempData.Keep("FromDateDashboard");
                TempData["ToDateDashboard"] = ToDate == "" ? DateTime.Now.ToString("dd MMM yyyy") : ToDate; ;
                TempData.Keep("ToDateDashboard");
                return Json(new { hasError = false, data = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { hasError = true, data = HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Errata Main List 
        public ActionResult ErrataMainListView(int id, String number)
        {
            try
            {
            ErrataBO ERTA = new ErrataBO();
            ERTA = RetrieveData(id);
                ViewBag.DDlYesNo = new List<SelectListItem>{ new SelectListItem{
                 Text="No",
                Value = "0"
            },
            new SelectListItem{

                Text="Yes",
                Value = "1"
            }};
            List<int> QAEmpid = new List<int>() { ERTA.Initiator_EmpID, ERTA.HOD_EmpID };
                //For Binding QA Emp Names based on select Department in Errata MainLilst QA 12-02-2019
                List<SelectListItem> objAssignQA = new List<SelectListItem>();
                DataSet dtempqa = Qms_bal.GetEmpNamesbasedonRoleIDBal((int)QMS_Roles.QA,"A"); //26=QA
                foreach (DataRow Item in dtempqa.Tables[0].Rows)
                {
                    if (!QAEmpid.Contains(Convert.ToInt32(Item["EmpID"])))
                    {
                        objAssignQA.Add(new SelectListItem()
                        {
                            Text = (Item["EmpName"]).ToString(),
                            Value = (Item["EmpID"]).ToString()
                        });
                    }
                }
                ViewBag.QAEmployee = new SelectList(objAssignQA.Distinct().ToList(), "Value", "Text");

            var DeptName = (from dm in dbEF.AizantIT_DepartmentMaster
                            where dm.DeptID == ERTA.DeptID
                            select new { dm.DepartmentName }).ToList();
            foreach (var item in DeptName)
            {
                ERTA.DeptName = item.DepartmentName;
            }

                ERTA.TypeofChangeName = (from T in dbEF.AizantIT_QualityEvent
                                        where T.QualityEvent_TypeID == ERTA.TypeofCorrection
                                        select  T.QualityEvent_TypeName).SingleOrDefault();
                List<int> HQAEmpid = new List<int>() { ERTA.Initiator_EmpID, ERTA.HOD_EmpID };
            
                /*For Binding Head QA Employee name in Errata MainList*/
                List<SelectListItem> objAssignHQA = new List<SelectListItem>();
                DataSet dtemphqa = Qms_bal.GetEmpNamesbasedonRoleIDBal((int)QMS_Roles.HeadQA,"A"); //26=QA
                foreach (DataRow Item in dtemphqa.Tables[0].Rows)
                {
                    if (!HQAEmpid.Contains(Convert.ToInt32(Item["EmpID"])))
                    {
                        objAssignHQA.Add(new SelectListItem()
                        {
                            Text = (Item["EmpName"]).ToString(),
                            Value = (Item["EmpID"]).ToString()
                        });
                    }
                }
                ViewBag.HEQAEmployee = new SelectList(objAssignHQA.Distinct().ToList(), "Value", "Text", ERTA.HeadQA_EmpID);

                _ErrataImpactPoints(ERTA);
                return PartialView(ERTA);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "ERT_M9:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }
        #endregion
        
        #region NotUsingCode
        public ActionResult BindDocumentType(int deptid)
        {
            try
            {
                List<SelectListItem> Department2 = new List<SelectListItem>();
                var D1 = (from tp in dbEF.AizantIT_DocumentType
                          select new { tp.DocumentTypeID, tp.DocumentType }).ToList();

                List<SelectListItem> impListDoctype = new List<SelectListItem>();
                impListDoctype.Add(new SelectListItem { Text = "select ", Value = "0" });
                foreach (var q1 in D1)
                {
                    impListDoctype.Add(new SelectListItem { Text = q1.DocumentType, Value = q1.DocumentTypeID.ToString() });
                }
                impListDoctype.Add(new SelectListItem { Text = "Others ", Value = "-1" });
                return Json(impListDoctype, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "ERT_M11:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult FillVersionNumber(int versoinID)
        {
            try
            {
                var DocName1 = (from dv in dbEF.AizantIT_DocVersion
                                where dv.VersionID == versoinID
                                select new { dv.VersionNumber }).ToList();
                int BindversionNumber = 0;
                foreach (var q1 in DocName1)
                {
                    BindversionNumber = q1.VersionNumber;
                }
                return Json(new { hasError = false, data = BindversionNumber }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "ERT_M12:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        public string GetFullName(int EmpId)
        {
            var EmployeeName = (from E in dbEF.AizantIT_EmpMaster
                                where E.EmpID == EmpId
                                select (E.FirstName + "" + E.LastName)).SingleOrDefault();
            return EmployeeName;
        }
        public PartialViewResult _ErrataAdminManage(int id, string Number, string R)
        {
            TempData["QueryE"] = R;
            ErrataBO ERTABO = new ErrataBO();
            ERTABO = BindData(id, Number);
            //For Binding Initiator EmpName on Initiator Manage  
            objUMS_BAL = new UMS_BAL();
            DataSet dsEmployees = Qms_bal.GetEmployeesDeptandRoleWiseInAdminManageBAL(ERTABO.DeptID, 3);
            List<ErrataBO> ListEmployee = new List<ErrataBO>();
            for(int i=0;i<4;i++)
            {
                if(i == 0)
                {
                    //For getting Initiator Employee Names
                    List<SelectListItem> InitiatorList = new List<SelectListItem>();
                    InitiatorList.Add(new SelectListItem
                    {
                        Text="Select",
                        Value="0",
                    });
                   foreach(DataRow rdr in dsEmployees.Tables[0].Rows)
                    {
                        InitiatorList.Add(new SelectListItem
                        {
                            Text = (rdr["EmpName"]).ToString(),
                            Value = (rdr["EmpID"]).ToString(),
                            Selected = (Convert.ToInt32(rdr["EmpID"]) == ERTABO.Initiator_EmpID ? true : false)
                        });
                    }
                    ListEmployee.Add(new ErrataBO
                    {
                        Sno =1,
                        DDLEmpList = InitiatorList,
                        RoleName = "Initiator",
                        SelectDDLEmp = ERTABO.Initiator_EmpID,
                        SelectedEmployeeName= GetFullName(ERTABO.Initiator_EmpID)
                    });
                }
                if(i == 1)
                {
                    List<SelectListItem> HODList = new List<SelectListItem>();
                    HODList.Add(new SelectListItem
                    {
                        Text = "Select",
                        Value ="0",
                    });
                    foreach(DataRow rdr in dsEmployees.Tables[1].Rows)
                    {
                        HODList.Add(new SelectListItem
                        {
                         Text = (rdr["EmpName"]).ToString(),
                         Value = (rdr["EmpID"]).ToString(),
                         Selected = (Convert.ToInt32(rdr["EmpID"]) == ERTABO.HOD_EmpID ? true : false)
                        });
                    }
                    ListEmployee.Add(new ErrataBO
                    {
                        Sno = 2,
                        DDLEmpList = HODList,
                        RoleName = "HOD",
                        SelectDDLEmp = ERTABO.HOD_EmpID,
                        SelectedEmployeeName = GetFullName(ERTABO.HOD_EmpID)
                    });
                }
                if(i == 2)
                {
                    var QAEmpIDManage = (from ER in dbEF.AizantIT_ErrataMaster
                                         where ER.ER_ID == id
                                         select ER.QA_EmpID).SingleOrDefault();
                    int QAEmpID = Convert.ToInt32(QAEmpIDManage);
                    List<SelectListItem> HeadQAList = new List<SelectListItem>();
                    List<int> HeadQAEmployeeid = new List<int>() { ERTABO.Initiator_EmpID, ERTABO.HOD_EmpID, QAEmpID };
                    var HeadQA = (from ER in dbEF.AizantIT_ErrataMaster
                                  where ER.ER_ID == id
                                  select ER.Head_QA_Empid).SingleOrDefault();
                    int SelectedHeadQA = Convert.ToInt32(HeadQA);
                    HeadQAList.Add(new SelectListItem
                    {
                        Text = "Select",
                        Value = "0",
                    });
                    foreach (DataRow rdr in dsEmployees.Tables[3].Rows)
                    {
                        if (!HeadQAEmployeeid.Contains(Convert.ToInt32(rdr["EmpID"])))
                        {
                            HeadQAList.Add(new SelectListItem
                            {
                                Text = (rdr["EmpName"]).ToString(),
                                Value = (rdr["EmpID"]).ToString(),
                                Selected = (Convert.ToInt32(rdr["EmpID"]) == SelectedHeadQA ? true : false)
                            });
                        }
                    }
                    ListEmployee.Add(new ErrataBO
                    {
                        Sno = 3,
                        DDLEmpList = HeadQAList,
                        RoleName = "HeadQa",
                        SelectDDLEmp = SelectedHeadQA,
                        SelectedEmployeeName = GetFullName(SelectedHeadQA)
                    });
                }
                if(i == 3)
                {

                    List<SelectListItem> QAList = new List<SelectListItem>();
                    List<int> QaEmployeeid = new List<int>() { ERTABO.Initiator_EmpID, ERTABO.HOD_EmpID };
                    var QA = (from ER in dbEF.AizantIT_ErrataMaster
                              where ER.ER_ID == id
                              select ER.QA_EmpID).SingleOrDefault();
                    int SelecteQA = Convert.ToInt32(QA);

                    QAList.Add(new SelectListItem
                    {
                        Text = "Select",
                        Value = "0",
                    });
                    foreach (DataRow rdr in dsEmployees.Tables[2].Rows)
                    {
                        if (!QaEmployeeid.Contains(Convert.ToInt32(rdr["EmpID"])))
                        {
                            QAList.Add(new SelectListItem
                            {
                                Text = (rdr["EmpName"]).ToString(),
                                Value = (rdr["EmpID"]).ToString(),
                                Selected = (Convert.ToInt32(rdr["EmpID"]) == SelecteQA ? true : false)
                            });
                        }
                    }
                    ListEmployee.Add(new ErrataBO
                    {
                        Sno = 4,
                        DDLEmpList = QAList,
                        RoleName = "QA",
                        SelectDDLEmp = SelecteQA,
                        SelectedEmployeeName = GetFullName(SelecteQA)
                    });



                }
                ViewBag.ListEmployees = ListEmployee;
            }
            return PartialView(ERTABO);
        }
        [HttpPost]
        public JsonResult UpdateManageEmp(ErrataBO objERT)
        {
            QMS_BAL Qms_bal = new QMS_BAL();
            try
            {
                objERT.ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                int i = (Qms_bal.ERTAdminManageBal(objERT, out int Result));
                if(Result == 0)
                {
                    return Json(new { hasError = false, data = i }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { hasError = true, data = "Modification of Employee is Restricted, as the Transaction is Completed" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "NTF_M19:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        public bool ToCheckEmpValidityErrata(int ERID, int RoleID)
        {
            int Initiator = 0, HOD = 0, QA = 0, HeadQA = 0;
            Boolean i = true;
            int LoginEmployeeID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            var GetDetails = (from ER in dbEF.AizantIT_ErrataMaster
                              where ER.ER_ID == ERID
                              select new
                              {
                                  ER.InitiatorEmpID,
                                  ER.HOD_EmpID,
                                  ER.QA_EmpID,
                                  ER.Head_QA_Empid
                              }).ToList();

            foreach (var item in GetDetails)
            {
                Initiator = (int)item.InitiatorEmpID;
                HOD = (int)item.HOD_EmpID;
                QA = item.QA_EmpID == null ? 0 : (int)item.QA_EmpID;
                HeadQA = item.Head_QA_Empid == null ? 0 : (int)item.Head_QA_Empid;
            }
            if (RoleID == 24)
            {
                if (LoginEmployeeID == Initiator)
                {
                    i = true;//Success
                }
                else
                {
                    i = false;//fail
                }
            }
            if (RoleID == 25)
            {
                if (LoginEmployeeID == HOD)
                {
                    i = true; //Success
                }
                else
                {
                    i = false;//fail
                }
            }
            if (RoleID == 26)
            {
                if (LoginEmployeeID == QA)
                {
                    i = true;//Success
                }
                else
                {
                    i = false;//fail
                }
            }
            if (RoleID == 27)
            {
                if (LoginEmployeeID == HeadQA)
                {
                    i = true;//Success
                }
                else
                {
                    i = false;//false
                }
            }
            return i;
        }
        public PartialViewResult _ErrataImpactPoints(ErrataBO objErrata)
        {
            TempData.Keep("QueryE");
            List<ErrataAssessImpactPoints> impctlst = new List<ErrataAssessImpactPoints>();
            List<SelectListItem> BindDDLImpactList = new List<SelectListItem>();
            if(objErrata.CurrentStatus == 1 || objErrata.CurrentStatus == 2)
            {
            var GetImpactStatusMasters = (from s in dbEF.AizantIT_Incident_EffectStatusMaster
                                          select new { s.EffectStatusID, s.EffectStatusName }).Distinct().ToList();
            foreach (var item in GetImpactStatusMasters)
            {
                if (3 != item.EffectStatusID)//3 means NA . Not required in Errata 
                {
                    BindDDLImpactList.Add(new SelectListItem
                    {
                        Text = item.EffectStatusName,
                        Value = item.EffectStatusID.ToString(),
                    });
                }
            }
            ViewBag.DDlYesNo = new SelectList(BindDDLImpactList.ToList(), "Value", "Text");
            objErrata.ddlIsYesorNo = BindDDLImpactList;
            var impact = (from imp in dbEF.AizantIT_Errata_ImpactOfChangeMaster
                          select new
                          {
                              imp.ImpactChangeName,
                              imp.ImpactChangeID
                          }).ToList();
            foreach (var dr in impact)
            {
                impctlst.Add(new ErrataAssessImpactPoints
                {
                    ImpactName = dr.ImpactChangeName.ToString(),
                    ImpactPointID = Convert.ToInt32(dr.ImpactChangeID),
                });
            }
            ViewBag.Impact = impctlst.AsEnumerable();
            }
            else
            {
                //start
                var CStatus = "N/A";
              
                if (TempData["QueryE"].ToString() != "8" && TempData["QueryE"].ToString() != "9")
                {
                    if (objErrata.CurrentStatus == 5)
                    {
                        CStatus = "";
                    }
                }
                //end
                var GetImpactPointsData = (from Imp in dbEF.AizantIT_Errata_ImpactChanges
                                    join IC in dbEF.AizantIT_Errata_ImpactOfChangeMaster on Imp.ImpactChangeID equals IC.ImpactChangeID
                                    where Imp.ER_ID == objErrata.ER_ID
                                    select new
                                    {
                                        Imp.ImpactChangeID, Imp.isImpacted, Imp.ReferenceNumber, Imp.Comments, IC.ImpactChangeName
                                    }).ToList();
                foreach (var dr in GetImpactPointsData)
                {
                    impctlst.Add(new ErrataAssessImpactPoints
                    {
                        ImpactName = dr.ImpactChangeName.ToString(),
                        ImpactPointID = Convert.ToInt32(dr.ImpactChangeID),
                        DocNo = dr.ReferenceNumber != null ? dr.ReferenceNumber.ToString() : CStatus,
                        Comments = dr.Comments != null ? dr.Comments.ToString() : CStatus,
                        isyesorNo = Convert.ToInt32(dr.isImpacted).ToString(),
                        _lstImpactStatus = new SelectList(dbEF.AizantIT_Incident_EffectStatusMaster.Where(t1=>t1.EffectStatusID!=3).ToList(), "EffectStatusID", "EffectStatusName", Convert.ToInt32(dr.isImpacted).ToString())
                    });
                }
                ViewBag.Impact = impctlst.AsEnumerable();
            }
            return PartialView();
        }
        public PartialViewResult _ErrataDetailsView(int id, string Number)
        {
            ErrataBO ERTABO = new ErrataBO();
            ERTABO = BindData(id, Number);
            return PartialView(ERTABO);
        }
        #region Singleton Practice
        public ActionResult GetErrataSigletoneList()
        {
            //DataSet dt = QMS_BAL.GetInstance().GetErrataDetailsBAL();
            //ErrataBO obj = ErrataBO.GetErrataInstance();

            List<ErrataBO> getlist = new List<ErrataBO>();
            List<ErrataBO> getlist1 = new List<ErrataBO>();
            #region EntityFrameWork
            //Parallel.Invoke(
            // () => ViewBag.ErrataList= GetAnaylticalDepts(getlist, 3),
            // () => ViewBag.ErrataList1 = GetBODepts(getlist1, 2));

            #endregion
            #region SQL_SP

            //if (dt.Tables[0].Rows.Count > 0)
            //{
            //    foreach (DataRow rdr in dt.Tables[0].Rows)
            //    {
            //        getlist.Add(new ErrataBO
            //        {
            //            ErrataFileNo = rdr["ER_FileNumber"].ToString(),
            //            DeptID = Convert.ToInt32(rdr["DeptID"]),
            //            EmpID = Convert.ToInt32(rdr["InitiatorEmpID"])
            //        });

            //    }
            //}
            #endregion

            //ViewBag.ErrataList = getlist;
            return View();
        }

        //private static List<ErrataBO> GetAnaylticalDepts(List<ErrataBO> getlist1,int DeptID )
        //{
        //    var GetErrataListDB = (from t1 in AizantIT_DevEntities.GetInstanceEF().AizantIT_ErrataMaster
        //                         where t1.DeptID==DeptID select t1).ToList();
        //    foreach (var item in GetErrataListDB)
        //    {
        //        ErrataBO obj = new ErrataBO();
        //        obj.ErrataFileNo = item.ER_FileNumber;
        //        obj.DeptID = Convert.ToInt32(item.DeptID);
        //        obj.ProposedCorrectionDoc_Desc = item.ProposedCorrectionDoc_Desc;
        //        obj.ER_ID = Convert.ToInt32(item.ER_ID);
        //        getlist1.Add(obj);
        //    }
        //  return   getlist1;
        //}
        //private static List<ErrataBO> GetBODepts(List<ErrataBO> getlist2,int DeptID)
        //{
        //    var GetErrataList1 = (from t1 in AizantIT_DevEntities.GetInstanceEF().AizantIT_ErrataMaster
        //                         where t1.DeptID == DeptID
        //                         select t1).ToList();
        //    foreach (var item in GetErrataList1)
        //    {
        //        ErrataBO obj = new ErrataBO();
        //        obj.ErrataFileNo = item.ER_FileNumber;
        //        obj.DeptID = Convert.ToInt32(item.DeptID);
        //        obj.ProposedCorrectionDoc_Desc = item.ProposedCorrectionDoc_Desc;
        //        obj.ER_ID = Convert.ToInt32(item.ER_ID);
        //        getlist2.Add(obj);
        //    }
        //   return getlist2;
        //}
        //public ActionResult Edit(int id)
        //{
        //    AizantIT_ErrataMaster c = (from t1 in AizantIT_DevEntities.GetInstanceEF().AizantIT_ErrataMaster
        //                               where t1.ER_ID == id
        //                               select t1).First();
        //    c.ProposedCorrectionDoc_Desc = "Added  ";
        //    EDMXClass.GetEDMXInstance.SaveChanges();
        //    return RedirectToAction("GetErrataSigletoneList", "Errata", new { area = "QMS" });
        //        //Redirect("/Areas/QMS/Controllers/Errata/Errata/");
        //}
        #endregion
    }
    //public sealed class EDMXClass
    //{
    //    private static readonly Lazy<AizantIT_DevEntities> EdxmEF = new Lazy<AizantIT_DevEntities>(() => new AizantIT_DevEntities());
    //    //private static readonly AizantIT_DevEntities EdxmEF = null;
    //    private EDMXClass()
    //    {

    //    }
    //    public static AizantIT_DevEntities GetEDMXInstance
    //    {
    //        get
    //        {
    //            //if (EdxmEF == null)
    //            //    EdxmEF = new AizantIT_DevEntities();
    //            return EdxmEF.Value;
    //        }
    //    }
    //}

}