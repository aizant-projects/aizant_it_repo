﻿using Aizant_API_Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using AizantIT_PharmaApp.Areas.QMS.Models.QmsModel;

namespace AizantIT_PharmaApp.Areas.QMS.Controllers.Common
{
    public class DocumentViewerController : Controller
    {
        // GET: QMS/DocumentViewer
        //private AizantIT_QMS_EF db = new AizantIT_QMS_EF();
        private AizantIT_DevEntities dbEF = new AizantIT_DevEntities();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RichEditViewer(int ID)
        {
            Models.QmsModel.DocumentViewerBO obj = new Models.QmsModel.DocumentViewerBO();
            var DocFiles = (from a in dbEF.AizantIT_Incident_IncidentAttachments
                            where a.UniquePKID==ID
                            select new { a.IncidentMasterID,a.FileType,a.FileName}).ToList();
            foreach (var item in DocFiles)
            {
                //string strFile = item.IncidentAttachament.ToString();
                //byte[] bytesF = System.IO.File.ReadAllBytes(strFile);
               // obj.fContent = item.IncidentAttachament;
                if(item.FileType==".docx")
                obj.DFormat = DevExpress.XtraRichEdit.DocumentFormat.OpenXml;
                if (item.FileType == ".doc")
                    obj.DFormat = DevExpress.XtraRichEdit.DocumentFormat.Doc;
                if (item.FileType == ".rtf")
                    obj.DFormat = DevExpress.XtraRichEdit.DocumentFormat.Rtf;
                if (item.FileType == ".txt")
                    obj.DFormat = DevExpress.XtraRichEdit.DocumentFormat.PlainText;
               
            }
            return View(obj);
        }

        public ActionResult RichEditViewer2(int? EventID, int ID)
        {
            Models.QmsModel.DocumentViewerBO obj = new Models.QmsModel.DocumentViewerBO();
            if (EventID == 1)
            {
                var DocFiles = (from a in dbEF.AizantIT_Incident_IncidentAttachments
                                where a.UniquePKID == ID
                                select new { a.IncidentMasterID, a.FileType, a.FileName }).ToList();
                foreach (var item in DocFiles)
                {
                    //string strFile = item.IncidentAttachament.ToString();
                    //byte[] bytesF = System.IO.File.ReadAllBytes(strFile);
                   // obj.fContent = item.IncidentAttachament;
                    if (item.FileType == ".docx")
                        obj.DFormat = DevExpress.XtraRichEdit.DocumentFormat.OpenXml;
                    if (item.FileType == ".doc")
                        obj.DFormat = DevExpress.XtraRichEdit.DocumentFormat.Doc;
                    if (item.FileType == ".rtf")
                        obj.DFormat = DevExpress.XtraRichEdit.DocumentFormat.Rtf;
                    if (item.FileType == ".txt")
                        obj.DFormat = DevExpress.XtraRichEdit.DocumentFormat.PlainText;
                    obj.UQID=ID.ToString();
                    obj.UQID = new Guid().ToString();

                }
            }
            return View(obj);
        }
        public ActionResult SpreadSheetViewer(int EventID, int ID)
        {
            Models.QmsModel.DocumentViewerBO obj = new Models.QmsModel.DocumentViewerBO();
            if (EventID == 1)
            {
                var DocFiles = (from a in dbEF.AizantIT_Incident_IncidentAttachments
                                where a.UniquePKID == ID
                                select new { a.IncidentMasterID, a.FileType, a.FileName }).ToList();
                foreach (var item in DocFiles)
                {                   
                    //obj.fContent = item.IncidentAttachament;
                    if (item.FileType == ".xlsx")
                        obj.XLFormat = DevExpress.Spreadsheet.DocumentFormat.Xlsx;
                    if (item.FileType == ".xls")
                        obj.XLFormat = DevExpress.Spreadsheet.DocumentFormat.Xls;
                    obj.UQID = new Guid().ToString();

                }
            }
            return View(obj);
        }
        public ActionResult ImageViewer(int EventID, int ID)
        {

            Models.QmsModel.DocumentViewerBO obj = new Models.QmsModel.DocumentViewerBO();
            if (EventID == 1)
            {
                var DocFiles = (from a in dbEF.AizantIT_Incident_IncidentAttachments
                                where a.UniquePKID == ID
                                select new { a.IncidentMasterID, a.FileType, a.FileName }).ToList();
                foreach (var item in DocFiles)
                {
                    
                    //obj.fContent = item.IncidentAttachament;


                }
            }
            return View(obj);
        }
        public ActionResult PDFViewer(int EventID, int ID)
        {

         
            if (EventID == 1)
            {

                string embed = "<object data=\"{0}\" type=\"application/pdf\" width=\"1000px\" height=\"600px\">";
                //embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
                //embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
                embed += "</object>";
                TempData["Embed"] = string.Format(embed, VirtualPathUtility.ToAbsolute("~/ASPXHandlers/QMSPDFViewHandler.ashx?EventID=1&DOCID=" + ID));
            }
            return View();
        }
    }
}