﻿using AizantIT_PharmaApp.Areas.QMS.Models;
using AizantIT_PharmaApp.Areas.QMS.Models.Audit.DataLayer;
using AizantIT_PharmaApp.Areas.QMS.Models.Audit.ViewModel;
using AizantIT_PharmaApp.Areas.QMS.Models.Common;
//using AizantIT_PharmaApp.Areas.QMS.Models.QmsModel;
using AizantIT_PharmaApp.Common.CustomFilters;
using QMS_BusinessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using static Aizant_Enums.AizantEnums;
using AizantIT_PharmaApp.Common.UMS;
using QMS_BO.QMS_Dashboard_BO;
using Aizant_API_Entities;
using AizantIT_PharmaApp.Common;
using System.Net.Http;

namespace AizantIT_PharmaApp.Areas.QMS.Controllers.Common
{
    [RouteArea]
    [CustAuthentication]
    [CustAuthorization(
        (int)Modules.QMS,
        (int)QMS_Roles.Initiator,
        (int)QMS_Roles.HOD,
        (int)QMS_Roles.QA,
        (int)QMS_Roles.HeadQA,
        (int)QMS_Roles.User,
        (int)QMS_Roles.AdminRole,
        (int)QMS_Roles.Investigator,
        (int)QMS_Audit_Roles.AuditViewer,
        (int)QMS_Audit_Roles.AuditHOD,
        (int)QMS_Audit_Roles.AuditQA,
        (int)QMS_Audit_Roles.AuditHeadQA,
        (int)QMS_Audit_Roles.AuditAdmin)]
    public class DashBoardController : Controller
    {
        // GET: QMS_DashBoard
        QMS_BAL Qms_bal = new QMS_BAL();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private AizantIT_DevEntities dbEF = new AizantIT_DevEntities();

        #region GetUserEmployeeID
        public void GetUserEmployeID(Dashboard_QMS objdashboard)
        {
            if (Session["UserDetails"] != null)
            {
                objdashboard.EMPID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            }
        }
        #endregion

        #region Dashboard ViewCards
        //QMS Dashbord View 
        public ActionResult QMS_Dashboard()
        {
            try
            {
                //throw new Exception("HAI");
                //Maintain Module Roles
                TempData.Keep("EmpModuleRoles");

                bool ShowReviewCard = false;
                bool ShowRevertCard = false;
                bool ShowPendingCard = false;
                bool ShowReminderCard = false;
                bool ShowOverdueSticky = false;
                bool ShowQMSQualityEventChart = false;
                bool ShowQMSCAPAchart = false;
                Dashboard_QMS objCards = new Dashboard_QMS();
                List<SelectListItem> items = new List<SelectListItem>();
                //if (TempData["EmpModuleRoles"] != null)
                if (Session["EmpModuleRoles"] != null)
                {
                    int EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    string[] ModuleRoles = (string[])Session["EmpModuleRoles"];
                    ViewBag.ModuleRoles = (string[])Session["EmpModuleRoles"];

                    #region QMS Audit
                    DAL_AuditReport objAuditDal = new DAL_AuditReport();
                    DataSet dsAuditDashBoardData = objAuditDal.GetAuditDashBoardDetails((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                    if (dsAuditDashBoardData.Tables[0].Rows.Count > 0)
                    {
                        DataTable dtAuditExternal = dsAuditDashBoardData.Tables[0];
                        int countHeadQA_RevertedAudit = Convert.ToInt32(dtAuditExternal.Select("CountFor='HeadQA_RevertedAudit'")[0]["Count"]);
                        int countResponsesReview = Convert.ToInt32(dtAuditExternal.Select("CountFor='ResponsesReview'")[0]["Count"]);
                        int countAuditsPendingOnComplete = Convert.ToInt32(dtAuditExternal.Select("CountFor='AuditsPendingOnComplete'")[0]["Count"]);
                        int countAuditsPendingOnHeadQA_Approval = Convert.ToInt32(dtAuditExternal.Select("CountFor='AuditsPendingOnHeadQA_Approval'")[0]["Count"]);
                        int countObservationAcceptancePending = Convert.ToInt32(dtAuditExternal.Select("CountFor='ObservationAcceptancePending'")[0]["Count"]);
                        int countObservationResponsePending = Convert.ToInt32(dtAuditExternal.Select("CountFor='ObservationResponsePending'")[0]["Count"]);
                        int countRevertedResponses = Convert.ToInt32(dtAuditExternal.Select("CountFor='RevertedResponses'")[0]["Count"]);
                        int countObservationDueDateReminderToQAAndHeadQAForAllRecords = Convert.ToInt32(dtAuditExternal.Select("CountFor='ObservationDueDateReminderToQA'")[0]["Count"]);
                        int CountObservationOverDueDateForAllRecords = Convert.ToInt32(dtAuditExternal.Select("CountFor='ObservationOverDueToQA'")[0]["Count"]);
                        //int countObservationDueDateReminderToAuthorQA = Convert.ToInt32(dtAuditExternal.Select("CountFor='ObservationDueDateReminderToAuthorQA'")[0]["Count"]);
                        int countObservationDueDateReminderToResponsibleHOD = Convert.ToInt32(dtAuditExternal.Select("CountFor='ObservationDueDateReminderToResponsibleHOD'")[0]["Count"]);
                        int countObservationOverDueDateToResponsibleHOD = Convert.ToInt32(dtAuditExternal.Select("CountFor='ObservationOverDueToHOD'")[0]["Count"]);
                        if (ModuleRoles.Contains(((int)Aizant_Enums.AizantEnums.QMS_Audit_Roles.AuditQA).ToString()) || ModuleRoles.Contains(((int)Aizant_Enums.AizantEnums.QMS_Audit_Roles.AuditHeadQA).ToString()))
                        {
                            objCards.Audit_ObservationsOverDueCountToQAandHeadQAForAllRecords = CountObservationOverDueDateForAllRecords;
                            objCards.TotalOverDueCount += CountObservationOverDueDateForAllRecords;
                            ShowOverdueSticky = true;
                            objCards.Audit_observationDueDateReminderToQAForAllRecords = countObservationDueDateReminderToQAAndHeadQAForAllRecords;
                            objCards.ReminderTotalCount += countObservationDueDateReminderToQAAndHeadQAForAllRecords;
                            ShowReminderCard = true;
                        }
                        if (ModuleRoles.Contains(((int)Aizant_Enums.AizantEnums.QMS_Audit_Roles.AuditQA).ToString()))
                        {
                            objCards.Audit_HeadQA_RevertedAudit = countHeadQA_RevertedAudit;
                            objCards.Audit_ResponsesReview = countResponsesReview;
                            objCards.Audit_PendingOnComplete = countAuditsPendingOnComplete;
                            //objCards.Audit_observationDueDateReminderToAuthorQA = countObservationDueDateReminderToAuthorQA;


                            objCards.ReviewTotalCount += countResponsesReview;
                            objCards.RevertTotalCount += countHeadQA_RevertedAudit;
                            objCards.TotalPending += countAuditsPendingOnComplete;
                            //objCards.ReminderTotalCount += countObservationDueDateReminderToAuthorQA;
                            ShowReviewCard = true;
                            ShowRevertCard = true;
                            ShowPendingCard = true;
                            objCards.ShowInProgressAuditChart = true;
                            objCards.ShowCompletedAuditChart = true;
                        }
                        if (ModuleRoles.Contains(((int)Aizant_Enums.AizantEnums.QMS_Audit_Roles.AuditHOD).ToString()))
                        {
                            objCards.Audit_ObservationAcceptancePending = countObservationAcceptancePending;
                            objCards.Audit_ObservationResponsePending = countObservationResponsePending;
                            objCards.Audit_RevertedResponses = countRevertedResponses;
                            objCards.Audit_observationDueDateReminderToResponsibleHOD = countObservationDueDateReminderToResponsibleHOD;
                            objCards.Audit_ObservationsOverDueCountToResponsibleHOD = countObservationOverDueDateToResponsibleHOD;
                            objCards.TotalOverDueCount += countObservationOverDueDateToResponsibleHOD;
                            objCards.TotalPending += countObservationResponsePending + countObservationAcceptancePending;
                            objCards.RevertTotalCount += countRevertedResponses;
                            objCards.ReminderTotalCount += countObservationDueDateReminderToResponsibleHOD;
                            ShowOverdueSticky = true;
                            ShowRevertCard = true;
                            ShowPendingCard = true;
                            ShowReminderCard = true;
                            objCards.ShowCompletedAuditChart = true;
                        }
                        if (ModuleRoles.Contains(((int)Aizant_Enums.AizantEnums.QMS_Audit_Roles.AuditHeadQA).ToString()))
                        {
                            objCards.Audit_AuditsPendingOnHeadQA_Approval = countAuditsPendingOnHeadQA_Approval;
                            objCards.ReviewTotalCount += countAuditsPendingOnHeadQA_Approval;
                            ShowReviewCard = true;
                            objCards.ShowInProgressAuditChart = true;
                            objCards.ShowCompletedAuditChart = true;
                        }
                        if (ModuleRoles.Contains(((int)Aizant_Enums.AizantEnums.QMS_Audit_Roles.AuditAdmin).ToString()))
                        {
                            objCards.ShowInProgressAuditChart = true;
                            objCards.ShowCompletedAuditChart = true;
                        }
                        if (ModuleRoles.Contains(((int)Aizant_Enums.AizantEnums.QMS_Audit_Roles.AuditViewer).ToString()))
                        {
                            objCards.ShowCompletedAuditChart = true;
                        }
                    }
                    #endregion

                    #region QMS
                    //if (ModuleRoles.Contains(((int)Aizant_Enums.AizantEnums.QMS_Roles.AdminRole).ToString()) || ModuleRoles.Contains(((int)Aizant_Enums.AizantEnums.QMS_Roles.HeadQA).ToString())
                    //    || ModuleRoles.Contains(((int)Aizant_Enums.AizantEnums.QMS_Roles.QA).ToString()) || ModuleRoles.Contains(((int)Aizant_Enums.AizantEnums.QMS_Roles.HOD).ToString())
                    //    || ModuleRoles.Contains(((int)Aizant_Enums.AizantEnums.QMS_Roles.Initiator).ToString()) || ModuleRoles.Contains(((int)Aizant_Enums.AizantEnums.QMS_Roles.User).ToString())
                    //    || ModuleRoles.Contains(((int)Aizant_Enums.AizantEnums.QMS_Roles.Investigator).ToString())
                    //    || ModuleRoles.Contains(((int)Aizant_Enums.AizantEnums.QMS_Audit_Roles.AuditHOD).ToString())
                    //    || ModuleRoles.Contains(((int)Aizant_Enums.AizantEnums.QMS_Audit_Roles.AuditQA).ToString())
                    //    || ModuleRoles.Contains(((int)Aizant_Enums.AizantEnums.QMS_Audit_Roles.AuditHeadQA).ToString()))
                    //{

                        //Binding Get All Quality Events 
                        BindQualityEventType();
                        //Bind All Classification
                        BindCCNClssification();


                        #region GetQMSDataforCards
                        DataSet ds = new DataSet();
                        ds = Qms_bal.BalDashboard(Convert.ToInt32(EmpID));
                       
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            DataTable dtQMSDashboard = ds.Tables[0];
                            int countNTFInitiatorRevertedList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='NTFInitiatorRevertedList'")[0]["Count"]);
                            int countNTFHODReviewList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='NTFHODReviewList'")[0]["Count"]);
                            int countNTFHODRevertedList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='NTFHODRevertedList'")[0]["Count"]);
                            int countNTFQAReviewList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='NTFQAReviewList'")[0]["Count"]);
                            int countNTFQARevertedList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='NTFQARevertedList'")[0]["Count"]);
                            int countNTFHQAReviewList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='NTFHQAReviewList'")[0]["Count"]);
                            int countERTInitiatorRevertedList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='ERTInitiatorRevertedList'")[0]["Count"]);
                            int countERTHODReviewList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='ERTHODReviewList'")[0]["Count"]);
                            int countERTHODRevertedList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='ERTHODRevertedList'")[0]["Count"]);
                            int countERTQAReviewList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='ERTQAReviewList'")[0]["Count"]);
                            int countERTInitiatorCompleteList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='ERTInitiatorCompleteList'")[0]["Count"]);
                            int countERTHQAReviewList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='ERTHQAReviewList'")[0]["Count"]);
                            int countCAPAQAReviewList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='CAPAQAReviewList'")[0]["Count"]);
                            int countCAPAHQAReviewList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='CAPAHQAReviewList'")[0]["Count"]);
                            int countCAPAHODRevertedList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='CAPAHODRevertedList'")[0]["Count"]);
                            int countCAPAUserAceptList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='CAPAUserAceptList'")[0]["Count"]);
                            int countCAPAUserCompleteList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='CAPAUserCompleteList'")[0]["Count"]);
                            int countCAPARem1and2HOD = Convert.ToInt32(dtQMSDashboard.Select("CountFor='CAPARem1and2HOD'")[0]["Count"]);
                            int countCAPARem1and2QAApproveList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='CAPARem1and2QAApproveList'")[0]["Count"]);
                            int countCAPAVerificationList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='CAPAVerificationList'")[0]["Count"]);
                            int countCAPARem1and2_HODRevertedList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='CAPARem1and2_HODRevertedList'")[0]["Count"]);
                            int countCAPARem1_CreatedList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='CAPARem1_CreatedList'")[0]["Count"]);
                            int countCAPARem2_CreatedList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='CAPARem2_CreatedList'")[0]["Count"]);
                            int countCapaClose = Convert.ToInt32(dtQMSDashboard.Select("CountFor='CapaClose'")[0]["Count"]);
                            int countCapaVerifHQAApprove = Convert.ToInt32(dtQMSDashboard.Select("CountFor='CapaVerifHQAApprove'")[0]["Count"]);
                            int countIncdInitiatorRevertedList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='IncdInitiatorRevertedList'")[0]["Count"]);
                            int countIncdHodReviewList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='IncdHodReviewList'")[0]["Count"]);
                            int countIncdHodRevertedList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='IncdHodRevertedList'")[0]["Count"]);
                            int countIncdQAReviewList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='IncdQAReviewList'")[0]["Count"]);
                            int countIncdHQAReviewList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='IncdHQAReviewList'")[0]["Count"]);
                            int countIncdInvestReviewList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='IncdInvestReviewList'")[0]["Count"]);
                            int countIncdInvestRevertedList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='IncdInvestRevertedList'")[0]["Count"]);
                            int countIncdHodActionPlanList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='IncdHodActionPlanList'")[0]["Count"]);
                            int countIncdOtherHODInvestList = Convert.ToInt32(dtQMSDashboard.Select("CountFor='IncdOtherHODInvestList'")[0]["Count"]);
                            int countCCNHODApprove = Convert.ToInt32(dtQMSDashboard.Select("CountFor='CCNHODApprove'")[0]["Count"]);
                            int countCCNHODAssessment = Convert.ToInt32(dtQMSDashboard.Select("CountFor='CCNHODAssessment'")[0]["Count"]);
                            int countCCNHQAApprove = Convert.ToInt32(dtQMSDashboard.Select("CountFor='CCNHQAApprove'")[0]["Count"]);
                            int countCCNInitiatorRevert = Convert.ToInt32(dtQMSDashboard.Select("CountFor='CCNInitiatorRevert'")[0]["Count"]);
                            int countCCNQAAssessment = Convert.ToInt32(dtQMSDashboard.Select("CountFor='CCNQAAssessment'")[0]["Count"]);
                            int countCCNQAClose = Convert.ToInt32(dtQMSDashboard.Select("CountFor='CCNQAClose'")[0]["Count"]);
                            int countCCNHODReverted = Convert.ToInt32(dtQMSDashboard.Select("CountFor='CCNHODReverted'")[0]["Count"]);
                            int countCCNQAReverted = Convert.ToInt32(dtQMSDashboard.Select("CountFor='CCNQAReverted'")[0]["Count"]);
                            int countCCNVerification = Convert.ToInt32(dtQMSDashboard.Select("CountFor='IncdOtherHODInvestList'")[0]["Count"]);

                            #region Initiator RoleCards
                            if (ModuleRoles.Contains("24"))
                            {
                                ShowRevertCard = true;
                                ShowReviewCard = true;
                                ShowQMSQualityEventChart = true;
                                // NTF Initiator Revert Cards
                                objCards.RevertTotalCount += countNTFInitiatorRevertedList;
                                objCards.NTFHodRevert = countNTFInitiatorRevertedList;
                                 // Errata Initiator Revert Cards
                                 objCards.RevertTotalCount += countERTInitiatorRevertedList;
                                 objCards.ErrataInitiatorRevert = countERTInitiatorRevertedList;
                                //Errata Complete
                                        objCards.ReviewTotalCount += countERTInitiatorCompleteList;
                                        objCards.ErrataComplete = countERTInitiatorCompleteList;
                                   
                                //CCN Initiator Revert Cards
                                        objCards.RevertTotalCount += countCCNInitiatorRevert;
                                        objCards.CCNInitiatorRevert = countCCNInitiatorRevert;
                               
                                //Incident Initiator Reverted List Count
                                        objCards.RevertTotalCount += countIncdInitiatorRevertedList;
                                        objCards.INI_InitiatorRevert = countIncdInitiatorRevertedList;
                            }
                            #endregion

                            #region HodRoleCards 
                            //Hod Role
                            if (ModuleRoles.Contains("25") || ModuleRoles.Contains("35"))
                            {
                                ShowReviewCard = true;
                                ShowRevertCard = true;
                                ShowReminderCard = true;
                                ShowOverdueSticky = true;
                                if (ModuleRoles.Contains("25"))
                                {
                                    ShowQMSQualityEventChart = true;
                                }
                                ShowQMSCAPAchart = true;

                                // NTF HOD Review
                                        objCards.ReviewTotalCount += countNTFHODReviewList;
                                        objCards.NTFHODReview = countNTFHODReviewList;
                                    
                                // Errata HOD Review list
                                        objCards.ReviewTotalCount += countERTHODReviewList;
                                        objCards.ErrataHODReview = countERTHODReviewList;

                                // CCN HOD 
                                        objCards.ReviewTotalCount += countCCNHODApprove;
                                        objCards.CCNHodApprove = countCCNHODApprove;
                               
                                //CCN HOD Assementes
                                        objCards.ReviewTotalCount += countCCNHODAssessment;
                                        objCards.CCNHODAssessment = countCCNHODAssessment;
                                    
                                // NTF hod revert list        
                                        objCards.RevertTotalCount += countNTFHODRevertedList;
                                        objCards.NTFQARevert = countNTFHODRevertedList;
                                   
                                // Errata HOD Revert list
                                        objCards.RevertTotalCount += countERTHODRevertedList;
                                        objCards.ErrataHODRevert = countERTHODRevertedList;
                                   
                                // Capa HOD Revert List
                                        objCards.RevertTotalCount += countCAPAHODRevertedList;
                                        objCards.CapaHODRevert = countCAPAHODRevertedList;
                                   
                                //CCN HOD Reverted list 
                                        objCards.RevertTotalCount += countCCNHODReverted;
                                        objCards.CCNHoDRevert = countCCNHODReverted;
                                    
                                // Capa reminder 1 2 HOD Complete
                                        objCards.ReminderTotalCount += countCAPARem1and2HOD;
                                        objCards.CapaRemainderHODApprove = countCAPARem1and2HOD;
                                   
                                // Capa reminder QA Revert to HOD 25
                                        objCards.ReminderTotalCount += countCAPARem1and2_HODRevertedList;
                                        objCards.CapaRemainderHodRevert = countCAPARem1and2_HODRevertedList;
                                   
                                // Incident HOD Review List 29
                                        objCards.ReviewTotalCount += countIncdHodReviewList;
                                        objCards.INI_HODReview = countIncdHodReviewList;
                                    
                                // Incident HOD Revert List 30
                                        objCards.RevertTotalCount += countIncdHodRevertedList;
                                        objCards.INI_HODRevert = countIncdHodRevertedList;
                                   
                                // Incident HOD Action plan List 35
                                        objCards.ReviewTotalCount += countIncdHodActionPlanList;
                                        objCards.INI_HODActionPlan = countIncdHodActionPlanList;
                                    
                                // Incident investigation Other dept HOD Review List 36
                                        objCards.ReviewTotalCount += countIncdOtherHODInvestList;
                                        objCards.INI_OtherDeptHodReview = countIncdOtherHODInvestList;
                            }
                            #endregion

                            #region QARoleCards
                            if (ModuleRoles.Contains("26") || ModuleRoles.Contains("36"))
                            {
                                ShowReviewCard = true;
                                ShowRevertCard = true;
                                ShowReminderCard = true;
                                ShowOverdueSticky = true;
                                if (ModuleRoles.Contains("26"))
                                {
                                    ShowQMSQualityEventChart = true;
                                }
                                ShowQMSCAPAchart = true;
                                // Incident QA Review 31
                                        objCards.ReviewTotalCount += countIncdQAReviewList;
                                        objCards.INI_QAReview = countIncdQAReviewList;
                                   
                                // NoteToFile QA review  8   
                                        objCards.ReviewTotalCount += countNTFQAReviewList;
                                        objCards.NTFQA_Review = countNTFQAReviewList;
                                   
                                //Errata QA Review list 16
                               
                                        objCards.ReviewTotalCount += countERTQAReviewList;
                                        objCards.ErrataQAReview = countERTQAReviewList;
                               
                                // Capa QA Review 17
                             
                                        objCards.ReviewTotalCount += countCAPAQAReviewList;
                                        objCards.CapaQAReview = countCAPAQAReviewList;

                                //CCN QA Closer List (CCN 5)
                                        objCards.ReviewTotalCount += countCCNQAClose;
                                        objCards.CCNQACloserList = countCCNQAClose;
                                //CCN QA Assessment (CCN4)
                                objCards.ReviewTotalCount += countCCNQAAssessment;
                                objCards.CCNQAssessment = countCCNQAAssessment;

                                // QA  Reverted list 9
                                        objCards.RevertTotalCount += countNTFQARevertedList;
                                        objCards.NTFHeadQA_Revert = countNTFQARevertedList;
                                    
                                //CCN QA Revert (CCN 7)
                               
                                        objCards.RevertTotalCount += countCCNQAReverted;
                                        objCards.CCNQARevert = countCCNQAReverted;
                                   
                                // Capa Reminder1 and 2 QAApprover 23
                                
                                        objCards.ReminderTotalCount += countCAPARem1and2QAApproveList;
                                        objCards.CapaRemainderQAApprove = countCAPARem1and2QAApproveList;

                                // Capa Verify Complete 24
                                        objCards.ReviewTotalCount += countCAPAVerificationList;
                                        objCards.CapaVerify = countCAPAVerificationList;
                                   
                                //Capa Closure 37
                                        objCards.ReviewTotalCount += countCapaClose;
                                        objCards.CapaClosure = countCapaClose;
                                   
                                // Capa Reminder1 Created count Show For QA 26
                               
                                        objCards.ReminderTotalCount += countCAPARem1_CreatedList;
                                        objCards.CapaReminder1Count = countCAPARem1_CreatedList;
                               
                                // Capa Reminder2 Created count Show For QA 27
                                        objCards.ReminderTotalCount += countCAPARem2_CreatedList;
                                        objCards.CapaReminder2Count = countCAPARem2_CreatedList;
                                   
                            }
                            #endregion

                            #region HeadQA RoleCards
                            if (ModuleRoles.Contains("27") || ModuleRoles.Contains("37"))
                            {
                                ShowReviewCard = true;
                                ShowRevertCard = true;
                                ShowOverdueSticky = true;
                                if (ModuleRoles.Contains("27"))
                                {
                                    ShowQMSQualityEventChart = true;
                                }
                                ShowQMSCAPAchart = true;
                                // Incident HeadQA Review list 32
                             
                                        objCards.ReviewTotalCount += countIncdHQAReviewList;
                                        objCards.INI_HeadQAReview = countIncdHQAReviewList;
                                
                                //NTF HeadQA Review list 10
                                        objCards.ReviewTotalCount += countNTFHQAReviewList;
                                        objCards.NTFHEADQA_Review = countNTFHQAReviewList;
                                   
                                // Errata HEad QA Review list
                                        objCards.ReviewTotalCount += countERTHQAReviewList;
                                        objCards.ErrataHQAReview = countERTHQAReviewList;
                                    
                                // Capa HeadQA Review 18
                                        objCards.ReviewTotalCount += countCAPAHQAReviewList;
                                        objCards.CapaHeadQAReview = countCAPAHQAReviewList;
                               
                                //CCNHead QA Approve CCN 2
                                        objCards.ReviewTotalCount += countCCNHQAApprove;
                                        objCards.CCNHeadQAApprove = countCCNHQAApprove;
                                    
                                //CAPA Verification Approval_HQA 38
                                        objCards.ReviewTotalCount += countCapaVerifHQAApprove;
                                        objCards.CapaHeadQAApprove = countCapaVerifHQAApprove;
                            }
                            #endregion

                            #region User RoleCards
                            if (ModuleRoles.Contains("29"))
                            {
                                ShowPendingCard = true;
                                ShowOverdueSticky = true;
                                ShowQMSCAPAchart = true;
                                // Capa Employee Accept And Complete 20
                                        objCards.TotalPending += countCAPAUserAceptList;
                                        objCards.CapaEmployeeAccept = countCAPAUserAceptList;
                                    
                                // Capa Employee Complete
                                        objCards.TotalPending += countCAPAUserCompleteList;
                                        objCards.CapaEmployeeComplete = countCAPAUserCompleteList;
                            }
                            #endregion
                            if (ModuleRoles.Contains("31"))
                            {
                                ShowQMSCAPAchart = true;
                                ShowQMSQualityEventChart = true;
                            }
                            #region Investigator RoleCards
                            if (ModuleRoles.Contains("33"))
                            {
                                ShowRevertCard = true;
                                ShowReviewCard = true;
                                ShowReviewCard = true;
                                ShowQMSQualityEventChart = true;
                                // Investigator Review List 33
                                        objCards.ReviewTotalCount += countIncdInvestReviewList;
                                        objCards.INI_InvestigatorReview = countIncdInvestReviewList;
                                   
                                // Investigator Reverted List 34
                                        objCards.RevertTotalCount += countIncdInvestRevertedList;
                                        objCards.INI_InvestigatorRevert = countIncdInvestRevertedList;
                            }
                            #endregion
                        }
                    #endregion
                    GetUserEmployeID(objCards);
                       
                        DataTable dtRoles = UserRoles.GetEmpRoles(4);//ModuelID
                        int CharEventRoleID = 0;
                        int CharCAPARoleID = 0;
                        //cAPA
                        if (dtRoles.Select("RoleID=" + (int)QMS_Roles.HOD).Length > 0 || dtRoles.Select("RoleID=" + (int)QMS_Roles.QA).Length > 0
                            || dtRoles.Select("RoleID=" + (int)QMS_Roles.HeadQA).Length > 0 || dtRoles.Select("RoleID=" + (int)QMS_Audit_Roles.AuditHOD).Length > 0
                            || dtRoles.Select("RoleID=" + (int)QMS_Audit_Roles.AuditQA).Length > 0 || dtRoles.Select("RoleID=" + (int)QMS_Audit_Roles.AuditHeadQA).Length > 0
                            || dtRoles.Select("RoleID=" + (int)QMS_Roles.AdminRole).Length > 0 || dtRoles.Select("RoleID=" + (int)QMS_Roles.User).Length > 0)
                        {
                            CharCAPARoleID = GetCAPARolesBinding(objCards);
                        }
                        //Event cahrt
                        if (dtRoles.Select("RoleID=" + (int)QMS_Roles.HOD).Length > 0 || dtRoles.Select("RoleID=" + (int)QMS_Roles.QA).Length > 0
                            || dtRoles.Select("RoleID=" + (int)QMS_Roles.HeadQA).Length > 0 || dtRoles.Select("RoleID=" + (int)QMS_Roles.Initiator).Length > 0
                            || dtRoles.Select("RoleID=" + (int)QMS_Roles.Investigator).Length > 0 || dtRoles.Select("RoleID=" + (int)QMS_Roles.AdminRole).Length > 0)
                        {
                            CharEventRoleID = GetEventRolesBinding(objCards);
                        }
                        Dashboard_QMS objCharts = new Dashboard_QMS();
                        //Getting EventName and Total Records Count to Events Chart
                        objCharts = GetTotalCountToChart(CharEventRoleID, CharCAPARoleID, objCards.EMPID);

                        //Binding Get All Department names on Dashboard Events chart
                        GetAllDepartments(CharEventRoleID,objCards.EMPID);
                        objCards.QulaityEventList = objCharts.QulaityEventList;
                        objCards.CapaQualityList = objCharts.CapaQualityList;
                    }
                    #endregion

                objCards.ShowReviewCard = ShowReviewCard;
                objCards.ShowRevertCard = ShowRevertCard;
                objCards.ShowRemainderCard = ShowReminderCard;
                objCards.ShowPendingCard = ShowPendingCard;
                //objCards.ShowQmsChartsDropdown = ShowQmsChartsDropdown;
                objCards.ShowQMSCAPAChart = ShowQMSCAPAchart;
                objCards.ShowQmsQaulityEventChart = ShowQMSQualityEventChart;
                if (objCards.TotalOverDueCount > 0 && ShowOverdueSticky == true)
                {
                    objCards.ShowOverdueSticky = ShowOverdueSticky;
                }
                //using (var client = new HttpClient())
                //{
                //    int EmpID1 = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                //    client.BaseAddress = new Uri("http://localhost:53973/api/Dashboard/GetDashboardCards?");
                //    //HTTP GET
                //    var responseTask = client.GetAsync("GetDashboardCards?EmpID=" + EmpID1);
                //    responseTask.Wait();


                //    var result = responseTask.Result;
                //    if (result.IsSuccessStatusCode)
                //    {
                      
                //    }
                //    else //web api sent error response 
                //    {
                //        //log response status here..

                //       // students = Enumerable.Empty<StudentViewModel>();

                //        ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                //    }
                //}
                return View(objCards);

            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "DashB_M1:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.SysError = ErMsg;
                
                return View();
            }
        }
        #endregion

        #region BindDropdownForCharts
        //Event Chart Roles Dropdown
        public int GetEventRolesBinding(Dashboard_QMS objCards)
        {
            //Get roleID in Session
            DataTable dtRoles = UserRoles.GetEmpRoles(4);//ModuelID
            List<int> NotBindingRoles = new List<int> { 34, 35, 36, 37, 38, 29 };
            List<SelectListItem> EventRolesList = new List<SelectListItem>();
            if (dtRoles.Rows.Count > 0)
            {
                foreach (DataRow rdrRoles in dtRoles.Rows)
                {
                    int RoleID = Convert.ToInt32(rdrRoles["RoleID"]);
                    var RoleName = (from tblR in dbEF.AizantIT_Roles
                                    where tblR.RoleID == RoleID
                                    select tblR.RoleName).SingleOrDefault();

                    if (!NotBindingRoles.Contains(RoleID))
                    {
                        EventRolesList.Add(new SelectListItem
                        {
                            Text = RoleName,
                            Value = Convert.ToInt32(RoleID).ToString(),
                        });
                    }
                }
            }
            objCards.ddlEventRoles = EventRolesList;
            return Convert.ToInt32(EventRolesList[0].Value);
        }
        public int GetCAPARolesBinding(Dashboard_QMS objCards)
        {
            List<int> NotBindingCAPARoles = new List<int> { 24, 33, 38, 34 };
            List<SelectListItem> EventRolesList = new List<SelectListItem>();
            //CAPA Chart Roles Dropdown
            List<SelectListItem> CAPARolesList = new List<SelectListItem>();

            //Get roleID in Session
            DataTable dtRoles = UserRoles.GetEmpRoles(4);//ModuelID
            if (dtRoles.Rows.Count > 0)
            {
                foreach (DataRow rdrRoles in dtRoles.Rows)
                {
                    int RoleID = Convert.ToInt32(rdrRoles["RoleID"]);
                    var RoleName = (from tblR in dbEF.AizantIT_Roles
                                    where tblR.RoleID == RoleID
                                    select tblR.RoleName).SingleOrDefault();
                    if (!NotBindingCAPARoles.Contains(RoleID))
                    {
                        CAPARolesList.Add(new SelectListItem
                        {
                            Text = RoleName,
                            Value = Convert.ToInt32(RoleID).ToString(),
                        });
                    }
                }
            }

            objCards.ddlCAPARoles = CAPARolesList;
            return Convert.ToInt32(CAPARolesList[0].Value);
        }
        //To Bind Get All Department Names on Dashboard Chart
        public void GetAllDepartments(int EventRoleID,int EmpID)
        {
            //GetUserEmployeID(objCards);
            List<int> GetRoleIdinSession = new List<int>();
            
            //Bind Departments
            List<SelectListItem> DeptNames = new List<SelectListItem>();
            DeptNames.Add(new SelectListItem
            {
                Text = "--Select Department--",
                Value = "0",
            });
            List<int> QA_HQA_Admin = new List<int> { 26, 27, 31 };
           if(EventRoleID==26|| EventRoleID == 27 || EventRoleID == 31)
            {
                var BindAllDeptNames = (from DN in dbEF.AizantIT_DepartmentMaster
                                        select new { DN.DeptID, DN.DepartmentName }).ToList();
                foreach (var item1 in BindAllDeptNames)
                {
                    DeptNames.Add(new SelectListItem
                    {
                        Text = item1.DepartmentName,
                        Value = item1.DeptID.ToString(),
                    });
                }
            }
            else
            {
                DataSet ds = Qms_bal.GetAccessibleDepartmentsBAL(EmpID, EventRoleID,4);
                foreach (DataRow rdr in ds.Tables[0].Rows)
                {
                    DeptNames.Add(new SelectListItem
                        {
                            Text = (rdr["DepartmentName"]).ToString(),
                            Value = (rdr["DeptID"]).ToString(),
                        });
                }
            }
            ViewBag.DeptID = new SelectList(DeptNames.ToList(), "Value", "Text");
        }


        [HttpGet]
        public JsonResult OnchangeRoleDepartmentBinding(int EventsRoleId)
        {
            try
            {
                Dashboard_QMS objCards = new Dashboard_QMS();
                GetUserEmployeID(objCards);
                List<SelectListItem> DeptNames = new List<SelectListItem>();
                DeptNames.Add(new SelectListItem
                {
                    Text = "--Select Department--",
                    Value = "0",
                });
                if (EventsRoleId == 26|| EventsRoleId == 27 || EventsRoleId == 31){
                    var BindAllDeptNames1 = (from DN in dbEF.AizantIT_DepartmentMaster
                                             select new { DN.DeptID, DN.DepartmentName }).ToList();
                    foreach (var item1 in BindAllDeptNames1)
                    {
                        DeptNames.Add(new SelectListItem
                        {
                            Text = item1.DepartmentName,
                            Value = item1.DeptID.ToString(),
                        });
                    }
                }
                else
                {
                    int EmpID= Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    DataSet ds = Qms_bal.GetAccessibleDepartmentsBAL(EmpID, EventsRoleId, 4);
                    foreach (DataRow rdr in ds.Tables[0].Rows)
                    {
                        DeptNames.Add(new SelectListItem
                        {
                            Text = (rdr["DepartmentName"]).ToString(),
                            Value = (rdr["DeptID"]).ToString(),
                        });
                    }
                }
                return Json(DeptNames, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "DashB_M2:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);

            }
        }
        public void BindCCNClssification()
        {
            List<int> NotBinding = new List<int> { 3, 5 };
            List<SelectListItem> CCNClassification = new List<SelectListItem>();
            CCNClassification.Add(new SelectListItem
            {
                Text = "--ALL--",
                Value = "0",
            });
            CCNClassification.Add(new SelectListItem
            {
                Text = "Major",
                Value = "1",
            });
            CCNClassification.Add(new SelectListItem
            {
                Text = "Minor",
                Value = "2",
            });
            CCNClassification.Add(new SelectListItem
            {
                Text = "Permanent",
                Value = "3",
            });
            CCNClassification.Add(new SelectListItem
            {
                Text = "Temporary",
                Value = "4",
            });
            ViewBag.CCNTypes = CCNClassification;
        }
        [HttpGet]
        public JsonResult BindDepartmentsWhenRoleChanged(int RoleID)
        {
            try
            {
                Dashboard_QMS objCards = new Dashboard_QMS();
                GetUserEmployeID(objCards);
                List<SelectListItem> BindDepartments = new List<SelectListItem>();
                BindDepartments.Add(new SelectListItem
                {
                    Text = "--Select Department--",
                    Value = "0",
                });
                if (RoleID == 25 || RoleID == 24)
                {
                    var BindAllDeptNames = (from DN in dbEF.AizantIT_DepartmentMaster
                                            join EM in dbEF.AizantIT_EmpMaster on DN.DeptID equals EM.DefaultDeptID
                                            where EM.EmpID == objCards.EMPID
                                            select new { DN.DeptID, DN.DepartmentName }).ToList();
                    foreach (var item1 in BindAllDeptNames)
                    {
                        BindDepartments.Add(new SelectListItem
                        {
                            Text = item1.DepartmentName,
                            Value = item1.DeptID.ToString(),
                        });
                    }
                }
                else
                {
                    var BindAllDeptNames1 = (from DN in dbEF.AizantIT_DepartmentMaster
                                             select new { DN.DeptID, DN.DepartmentName }).ToList();
                    foreach (var item1 in BindAllDeptNames1)
                    {
                        BindDepartments.Add(new SelectListItem
                        {
                            Text = item1.DepartmentName,
                            Value = item1.DeptID.ToString(),
                        });
                    }
                }
                return Json(BindDepartments, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "DashB_M3:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region ChartsBindingCode
        #region Audit Charts
        [HttpPost]
        public JsonResult GetInProgressAuditChartData(int BaseTypeID, bool ShowOnlyQaAsAuthor)//BaseTypeID represent 1 for Regulatory 2 for Client Audits
        {
            try
            {
                List<AuditCharts> objChartsList = new List<AuditCharts>();
                DAL_AuditReport objAuditDal = new DAL_AuditReport();
                string RoleType = "HeadQAorAdminRole";
                bool _HasOnlyQARolebutNotHeadQAorAdminRole = false;
                if ((!HasThisRole((int)QMS_Audit_Roles.AuditHeadQA) && !HasThisRole((int)QMS_Audit_Roles.AuditAdmin)) && HasThisRole((int)QMS_Audit_Roles.AuditQA))
                {
                    RoleType = "OnlyQA";
                    _HasOnlyQARolebutNotHeadQAorAdminRole = true;
                }
                if (ShowOnlyQaAsAuthor == false)
                {
                    RoleType = "ShowAlltoQA";
                }
                DataSet dsAuditCharts = objAuditDal.GetAuditDashBoardCharts(BaseTypeID, RoleType, Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                DataTable dtInProgressAuditChartData = dsAuditCharts.Tables[0];
                for (int i = 0; i < dtInProgressAuditChartData.Rows.Count; i++)
                {
                    objChartsList.Add(
                       new AuditCharts(dtInProgressAuditChartData.Rows[i]["TextField"].ToString(),
                       Convert.ToInt32(dtInProgressAuditChartData.Rows[i]["ValueCount"]), Convert.ToInt32(dtInProgressAuditChartData.Rows[i]["ValueField"])));
                }
                return Json(new
                {
                    msg = "Loaded Sucessfully",
                    msgType = "success",
                    HasOnlyQARoleButNotHeadQAorAdminRole = _HasOnlyQARolebutNotHeadQAorAdminRole,
                    aaData = objChartsList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message) + ".";
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        // Overdue Sticky
        public ActionResult GetQMSAndAuditOverDueData()
        {
            try
            {
                List<OverdueSticky> objDashboardList = new List<OverdueSticky>();
                CommonDatalayer objAuditDal = new CommonDatalayer();
                int OverDueTotalCount = 0;
                DataSet dsDashboardOverdue = objAuditDal.GetQMSAndAuditOverDueData(Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));

                DataTable dtQMSDashboardOverdue = dsDashboardOverdue.Tables[0];
                for (int i = 0; i < dtQMSDashboardOverdue.Rows.Count; i++)
                {
                    if (Convert.ToInt32(dtQMSDashboardOverdue.Rows[i]["Count"]) > 0 && (
                        dtQMSDashboardOverdue.Rows[i]["CountFor"].ToString() == "CAPAOverDueUserRole" && HasThisRole((int)QMS_Roles.User)
                        ))
                    {
                        OverDueTotalCount += Convert.ToInt32(dtQMSDashboardOverdue.Rows[i]["Count"]);
                        objDashboardList.Add(
                       new OverdueSticky(dtQMSDashboardOverdue.Rows[i]["CountFor"].ToString(),
                       Convert.ToInt32(dtQMSDashboardOverdue.Rows[i]["Count"])));
                    }
                }

                DataTable dtAuditDashboardOverdue = dsDashboardOverdue.Tables[1];
                for (int i = 0; i < dtAuditDashboardOverdue.Rows.Count; i++)
                {
                    if (Convert.ToInt32(dtAuditDashboardOverdue.Rows[i]["Count"]) > 0 && (
                        (dtAuditDashboardOverdue.Rows[i]["CountFor"].ToString() == "ObservationOverDueToEveryQAandHeadQA" && (HasThisRole((int)QMS_Audit_Roles.AuditQA) || HasThisRole((int)QMS_Audit_Roles.AuditHeadQA))) ||
                        (dtAuditDashboardOverdue.Rows[i]["CountFor"].ToString() == "ObservationOverDueToResponsibleHOD" && HasThisRole((int)QMS_Audit_Roles.AuditHOD))
                        ))
                    {
                        OverDueTotalCount += Convert.ToInt32(dtAuditDashboardOverdue.Rows[i]["Count"]);
                        objDashboardList.Add(
                       new OverdueSticky(dtAuditDashboardOverdue.Rows[i]["CountFor"].ToString(),
                       Convert.ToInt32(dtAuditDashboardOverdue.Rows[i]["Count"])));
                    }
                }

                return Json(new
                {
                    msg = "Loaded Successfully",
                    msgType = "success",
                    EmpName = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"].ToString(),
                    TotalOverDueCount = OverDueTotalCount,
                    OverDueData = objDashboardList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message) + ".";
                return Json(new { msg = message.Replace("'", "\'"), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public bool HasThisRole(int RoleID)
        {
            if ((Session["UserDetails"] as DataSet).Tables[1].Select("ModuleID=" + (int)Modules.QMS).Length > 0 &&
                (Session["UserDetails"] as DataSet).Tables[1].Select("ModuleID=" + (int)Modules.QMS).CopyToDataTable().Select("RoleID=" + RoleID).Length > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool HasOnlyThisRole(int RoleID)
        {
            if ((Session["UserDetails"] as DataSet).Tables[1].Select("ModuleID=" + (int)Modules.QMS).Length == 1 &&
                (Session["UserDetails"] as DataSet).Tables[1].Select("ModuleID=" + (int)Modules.QMS).CopyToDataTable().Select("RoleID=" + RoleID).Length > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        [HttpPost]
        public JsonResult GetCompletedAuditChartData(int BaseTypeID)//BaseTypeID represent 1 for Regulatory 2 for Client Audits
        {
            try
            {
                List<AuditCharts> objChartsList = new List<AuditCharts>();
                DAL_AuditReport objAuditDal = new DAL_AuditReport();
                string RoleType = "All";
                if (HasOnlyThisRole((int)QMS_Audit_Roles.AuditViewer))
                {
                    RoleType = "OnlyViewer";
                }
                DataSet dsAuditCharts = objAuditDal.GetAuditDashBoardCharts(BaseTypeID, RoleType, 0);
                DataTable dtCompletedAuditChartData = dsAuditCharts.Tables[1];
                for (int i = 0; i < dtCompletedAuditChartData.Rows.Count; i++)
                {
                    objChartsList.Add(
                       new AuditCharts(dtCompletedAuditChartData.Rows[i]["TextField"].ToString(),
                       Convert.ToInt32(dtCompletedAuditChartData.Rows[i]["ValueCount"]), Convert.ToInt32(dtCompletedAuditChartData.Rows[i]["ValueField"])));
                }
                return Json(new
                {
                    msg = "Loaded Successfully",
                    msgType = "success",
                    aaData = objChartsList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = "Yes";
                return Json("Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) + ".", JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        //This Method Executed when Page Load Get All counts
        public Dashboard_QMS GetTotalCountToChart(int CharEventRoleID, int CharCAPARoleID, int EmpId)
        {
            DataSet ds = new DataSet();
            Dashboard_QMS objbo = new Dashboard_QMS();
            if (CharEventRoleID > 0)
            {
                ds = Qms_bal.GetTotalCountToChartBal(CharEventRoleID, 0, EmpId, "", "");
                List<EventList> QulaityEventList = new List<EventList>();
                if (ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        QulaityEventList.Add(new EventList(dr["EventName"].ToString(), Convert.ToDouble(dr["TOTRecords"])));

                    }
                }
                objbo.QulaityEventList = QulaityEventList;
                ViewBag.QualityEventNoRecoreds = -1;
                if (QulaityEventList[0].TotalRecords == 0 && QulaityEventList[1].TotalRecords == 0 && QulaityEventList[2].TotalRecords == 0 &&
                    QulaityEventList[3].TotalRecords == 0 )
                {
                    ViewBag.QualityEventNoRecoreds = 0;
                }
            }
            //GET CAPA Count
            DataSet ds1 = new DataSet();
            if (CharCAPARoleID > 0)
            {
                ds1 = Qms_bal.GetTotalCountToCAPADeptWiseBal(CharCAPARoleID, EmpId, "", "");
                List<EventCAPAList> CapaEventList = new List<EventCAPAList>();
                if (ds1.Tables.Count > 0)
                {
                    foreach (DataRow dr1 in ds1.Tables[0].Rows)
                    {
                        CapaEventList.Add(new EventCAPAList(dr1["DeptCode"].ToString(), Convert.ToDouble(dr1["CapaCount"])));
                    }
                    objbo.CapaQualityList = CapaEventList;
                    ViewBag.CapaNoRecoreds = -1;
                    if (CapaEventList.Count == 0)
                    {
                        ViewBag.CapaNoRecoreds = 0;
                    }
                }
            }
            return objbo;
        }

        //Bind EventInner chart
        [HttpPost]
        public JsonResult QualityEventInnerChart(string QualityEventType, int ChartRoleID, int DeptID, string FromDate, string ToDate)
        {
            try {
                DataSet ds = new DataSet();
                Dashboard_QMS ObjQE = new Dashboard_QMS();
                //Get EmployeeID
                GetUserEmployeID(ObjQE);
                ds = Qms_bal.GetPendingTotalCountToChartBal(QualityEventType, ChartRoleID, DeptID, ObjQE.EMPID, FromDate, ToDate);
                int TotalCountEventsStatus = 0;
                List<EventInnerList> InnerCharts = new List<EventInnerList>();
                if (ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        InnerCharts.Add(new EventInnerList(dr["EventName"].ToString(), Convert.ToDouble(dr["TOTRecords"])));
                        TotalCountEventsStatus += Convert.ToInt32(dr["TOTRecords"]);
                    }
                }

                ObjQE.TotalEventCounts = TotalCountEventsStatus;
                //Bind Argument
                ObjQE.QualityEventType = QualityEventType;
                ObjQE.QualityInnerList = InnerCharts;
                return Json(new { hasError = false, data = ObjQE }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "DashB_M4:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        //Bind Chart Details When click on Department and FromDate and ToDate
        [HttpPost]
        public JsonResult getDeptWisequalityEvent(int DeptID, int RoleID, string FromDate, string ToDate)
        {
            try
            {
                DataSet ds = new DataSet();
                Dashboard_QMS objbo = new Dashboard_QMS();
                //Get EmployeeID
                GetUserEmployeID(objbo);
                ds = Qms_bal.GetTotalCountToChartBal(RoleID, DeptID, objbo.EMPID, FromDate, ToDate);
                List<String> empList = new List<String>();
                List<EventList> QulaityEventList = new List<EventList>();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        QulaityEventList.Add(new EventList(dr["EventName"].ToString(), Convert.ToDouble(dr["TOTRecords"])));

                    }
                }
                objbo.QulaityEventList = QulaityEventList;
                if (QulaityEventList[0].TotalRecords == 0 && QulaityEventList[1].TotalRecords == 0 && QulaityEventList[2].TotalRecords == 0 &&
                    QulaityEventList[3].TotalRecords == 0 )
                {
                    objbo.QualityEventNoRecoreds = 0;
                }
                else
                {
                    objbo.QualityEventNoRecoreds = QulaityEventList.Count;
                }
                return Json(new { hasError = false, data = objbo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "DashB_M5:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region RefreshCharts
        //Bind Chart Details when click on  RoleWise DropDown and Refersh to Event Chart
        [HttpPost]
        public JsonResult BindRoleWiseChat(int DeptID, int EventRoleID, int CapaRoleID, string FromDate, string ToDate, int EventStatus)
        {
            try
            {
                DataSet ds = new DataSet();
                Dashboard_QMS objbo = new Dashboard_QMS();
                GetUserEmployeID(objbo);
                List<int> NotBindingRoles = new List<int> { 34, 35, 36, 37, 38, 29 };
                if (!NotBindingRoles.Contains(EventRoleID))//Not binding Capa and quality Event Charts
                {
                    //if (EventRoleID != 29)//Event Chart hide when user role Select
                    //{
                    if (EventStatus == 2)
                    {
                        ds = Qms_bal.GetTotalCountToChartBal(EventRoleID, DeptID, objbo.EMPID, FromDate, ToDate);
                        List<String> empList = new List<String>();
                        List<EventList> QulaityEventList = new List<EventList>();
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dr in ds.Tables[0].Rows)
                            {
                                QulaityEventList.Add(new EventList(dr["EventName"].ToString(), Convert.ToDouble(dr["TOTRecords"])));

                            }
                        }
                        objbo.QulaityEventList = QulaityEventList;
                        if (QulaityEventList[0].TotalRecords == 0 && QulaityEventList[1].TotalRecords == 0 && QulaityEventList[2].TotalRecords == 0 &&
                    QulaityEventList[3].TotalRecords == 0)
                        {
                            objbo.QualityEventNoRecoreds = 0;
                        }
                        else
                        {
                            objbo.QualityEventNoRecoreds = QulaityEventList.Count;
                        }
                    }
                }
                //GET CAPA Count
                if (EventStatus == 3)
                {
                    List<int> NotBindingCAPARoles = new List<int> { 24, 33, 38, 34 };
                    if (!NotBindingCAPARoles.Contains(CapaRoleID))//Not binding Capa and quality Event Charts
                    {
                        DataSet ds1 = new DataSet();
                        ds1 = Qms_bal.GetTotalCountToCAPADeptWiseBal(CapaRoleID, objbo.EMPID, FromDate, ToDate);
                        List<EventCAPAList> CapaEventList = new List<EventCAPAList>();
                        if (ds1.Tables.Count > 0)
                        {
                            foreach (DataRow dr1 in ds1.Tables[0].Rows)
                            {
                                CapaEventList.Add(new EventCAPAList(dr1["DeptCode"].ToString(), Convert.ToDouble(dr1["CapaCount"])));
                            }
                        }
                        objbo.CapaQualityList = CapaEventList;
                        if (CapaEventList.Count == 0)
                        {
                            objbo.CapaNoRecoreds = 0;
                        }
                        else
                        {
                            objbo.CapaNoRecoreds = CapaEventList.Count;
                        }
                    }
                }
                return Json(new { hasError = false, data = objbo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "DashB_M6:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region CapaChartActions
        //Capa Filters formDate and ToDate
        [HttpPost]
        public JsonResult GetCAPAChartWithFromDateToDate(int RoleID, string FromDateCapa, string ToDateCapa)
        {
            try
            {
                DataSet ds = new DataSet();
                Dashboard_QMS objbo = new Dashboard_QMS();
                //Get EmployeeID
                GetUserEmployeID(objbo);
                List<String> empList = new List<String>();
                DataSet ds1 = new DataSet();
                ds1 = Qms_bal.GetTotalCountToCAPADeptWiseBal(RoleID, objbo.EMPID, FromDateCapa, ToDateCapa);
                List<EventCAPAList> CapaEventList = new List<EventCAPAList>();
                if (ds1.Tables.Count > 0)
                {
                    foreach (DataRow dr1 in ds1.Tables[0].Rows)
                    {
                        CapaEventList.Add(new EventCAPAList(dr1["DeptCode"].ToString(), Convert.ToDouble(dr1["CapaCount"])));
                    }
                    objbo.CapaQualityList = CapaEventList;
                    if (CapaEventList.Count == 0)
                    {
                        objbo.CapaNoRecoreds = 0;
                    }
                    else
                    {
                        objbo.CapaNoRecoreds = CapaEventList.Count;
                    }
                }
                return Json(new { hasError = false, data = objbo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "DashB_M7:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult CapaStatusInnerChart(string Argument, int ChartRoleID, string FromDate, string ToDate)
        {
            try
            {
                DataSet ds = new DataSet();
                Dashboard_QMS ObjQE = new Dashboard_QMS();
                //Get EmployeeID
                GetUserEmployeID(ObjQE);
                ds = Qms_bal.capaStatusInnerChartBAL(Argument, ChartRoleID, ObjQE.EMPID, FromDate, ToDate, 0);
                int CapaDrillDowntotalCount = 0;
                List<EventCAPAStatusList> InnerCharts = new List<EventCAPAStatusList>();
                if (ds.Tables.Count > 0)
                {
                    int RNum = 0;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        switch (dr["EventStatusName"].ToString().ToUpper())
                        {
                            case "PENDING":
                                RNum = 1;
                                break;
                            case "RM1":
                                RNum = 2;
                                break;
                            case "RM2":
                                RNum = 3;
                                break;
                            case "OVERDUE":
                                RNum = 4;
                                break;
                            case "COMPLETED":
                                RNum = 5;
                                break;
                            case "CLOSED":
                                RNum = 6;
                                break;
                            case "VERIFIED":
                                RNum = 7;
                                break;
                            case "REJECTED":
                                RNum = 8;
                                break;

                        }
                        InnerCharts.Add(new EventCAPAStatusList(dr["EventStatusName"].ToString(), Convert.ToDouble(dr["TotalStatusRecords"]), RNum));
                        CapaDrillDowntotalCount += Convert.ToInt32(dr["TotalStatusRecords"]);
                    }
                }
                //CapaDrilldown Status Total count
                ObjQE.QualityEventTotalCount = CapaDrillDowntotalCount;
                ObjQE.CapaStatusList = InnerCharts.OrderBy(mvt => mvt.RowID).ToList();
                return Json(new { hasError = false, data = ObjQE }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "DashB_M8:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult BindCapaStatusWithQualityEventType(string Argument, int ChartRoleID, string FromDate, string ToDate, int QualityEventType)
        {
            try
            {
                DataSet ds = new DataSet();
                Dashboard_QMS ObjQE = new Dashboard_QMS();
                //Get EmployeeID
                GetUserEmployeID(ObjQE);
                ds = Qms_bal.capaStatusInnerChartBAL(Argument, ChartRoleID, ObjQE.EMPID, FromDate, ToDate, QualityEventType);
                int TotalCountQE = 0;
                List<EventCAPAStatusList> InnerCharts = new List<EventCAPAStatusList>();
                if (ds.Tables.Count > 0)
                {

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {


                        InnerCharts.Add(new EventCAPAStatusList(dr["EventStatusName"].ToString(), Convert.ToDouble(dr["TotalStatusRecords"]), 0));
                        TotalCountQE += Convert.ToInt32(dr["TotalStatusRecords"]);
                    }
                }
                var QualityEvent = (from QE in dbEF.AizantIT_QualityEvent
                                    where QE.QualityEvent_TypeID == QualityEventType
                                    select new { QE.QualityEvent_TypeName }).SingleOrDefault();
                ObjQE.QualityEventName = QualityEvent.QualityEvent_TypeName;
                ObjQE.QualityEventTotalCount = TotalCountQE;
                ObjQE.CapaStatusList = InnerCharts;
                return Json(new { hasError = false, data = ObjQE }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "DashB_M9:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        public void BindQualityEventType()
        {
            //3- Errata 4-CAPA not binding
            List<int> NotBinding = new List<int> { 3, 5,10,11 };
            List<SelectListItem> Events = new List<SelectListItem>();
            var BindEvents = (from Q in dbEF.AizantIT_QualityEvent
                              where Q.CurrentStatus == true && !NotBinding.Contains(Q.QualityEvent_TypeID)
                              select new { Q.QualityEvent_TypeID, Q.QualityEvent_TypeName }).ToList();
            Events.Add(new SelectListItem
            {
                Text = "--ALL--",
                Value = "0",
            });
            foreach (var item in BindEvents)
            {
                Events.Add(new SelectListItem
                {
                    Text = item.QualityEvent_TypeName,
                    Value = item.QualityEvent_TypeID.ToString(),

                });
            }
            ViewBag.QualityEventType = new SelectList(Events.ToList(), "Value", "Text");
        }
        #endregion
        public JsonResult GetRoleName(int RoleID)
        {
            try
            {
                //AizantIT_UmsEntities dbUMs = new AizantIT_UmsEntities();
                var GetRoleName = (from R in dbEF.AizantIT_Roles
                                   where R.RoleID == RoleID
                                   select R.RoleName).SingleOrDefault();
                return Json(new { hasError = false, data = GetRoleName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "DashB_M7:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }


    }
    public class AccessibleDepatOnEmpIDRoleID{

        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string DeptCode { get; set; }
    }

}

