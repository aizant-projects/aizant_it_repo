﻿using Aizant_API_Entities;
using AizantIT_PharmaApp.Areas.QMS.Models.Common;
using AizantIT_PharmaApp.Common;
using QMS_BO.QMS_CAPABO;
using QMS_BO.QMS_NoteTofile_BO;
using QMS_BO.QMSComonBO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.Areas.QMS.Controllers.Common
{
    public class QMSCommonController : Controller
    {
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // BindReferenceNo to Dropdown
        [HttpPost]
        public JsonResult BindReferenceNo(int deptid, int documentTypeid)
        {
            try
            {
                AizantIT_DevEntities dbEF = new AizantIT_DevEntities();

                var ReferrencNumber = (from dv in dbEF.AizantIT_DocVersion
                                       join DM in dbEF.AizantIT_DocumentMaster on dv.DocumentID equals DM.DocumentID
                                       where DM.DocumentTypeID == documentTypeid && DM.DeptID == deptid && (dv.Status == "A" || dv.Status == "R") && dv.EffectiveDate != null
                                       select new { dv.VersionID, DM.DocumentNumber }).ToList();
                List<SelectListItem> impList1 = new List<SelectListItem>();
                impList1.Add(new SelectListItem { Text = "Select ", Value = "0" });
                foreach (var q2 in ReferrencNumber)
                {
                    impList1.Add(new SelectListItem { Text = q2.DocumentNumber, Value = q2.VersionID.ToString() });
                }
                return Json(impList1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "QCommon_M1:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        //Bind Document Name calling from Incident ,Errata,CCN,NTF
        [HttpGet]
        public JsonResult BindDocumentName(int versionID, int DocumentTypeID)
        {
            try
            {
                AizantIT_DevEntities dbEF = new AizantIT_DevEntities();

                var HOD = (from DM in dbEF.AizantIT_DocumentMaster
                           join dv in dbEF.AizantIT_DocVersion on DM.DocumentID equals dv.DocumentID
                           join Vt in dbEF.AizantIT_DocVersionTitle on dv.DocTitleID equals Vt.DocTitleID
                           where dv.VersionID == versionID && DM.DocumentTypeID == DocumentTypeID
                           select new { Vt.Title, dv.VersionNumber }).Distinct().ToList();
                string BindDocname = "";
                int BindversionNumber = 0;
                foreach (var q1 in HOD)
                {
                    BindDocname = q1.Title;//Document Name
                    BindversionNumber = q1.VersionNumber;
                }
                return Json(new { hasError = false, data = BindDocname, BindversionNumber }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "ERT_M5:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        //this method used in CCN,Errata,Incident,NTF
        [HttpPost]
        public JsonResult BindHOD(int deptid)
        {
            try
            {
                //For Binding Hod Emp Names based on select Department 12-02-2019
                NoteFileBO NTFBO = new NoteFileBO();
                UMS_BAL objUMS_BAL = new UMS_BAL();
                List<SelectListItem> objAssignHod = new List<SelectListItem>();
                DataTable dtemp = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(deptid, (int)QMS_Roles.HOD, true); //25=hod
                objAssignHod.Add(new SelectListItem { Text = "Select", Value = "0" });
                foreach (DataRow RPItem in dtemp.Rows)
                {
                    objAssignHod.Add(new SelectListItem()
                    {
                        Text = (RPItem["EmpName"]).ToString(),
                        Value = (RPItem["EmpID"]).ToString()
                    });
                }
                return Json(objAssignHod, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "NTF_M6:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
    }
    public class QMSCommonActions
    {
        int ReturnResult;
        //AizantIT_DevEntities db =new AizantIT_DevEntities(); 
        public int InsertCAPADetails(CapaModel capaModel, AizantIT_DevEntities db)
        {
            string ActionStatus = "";
            if (capaModel.QualityEvent_TypeID == "4")//Note to file
            {
                ActionStatus = "NTFInsert";
            }
            else if (capaModel.QualityEvent_TypeID == "1")//Incident
            {
                ActionStatus = "IncidentCapaInsert";
            }
            else if (capaModel.QualityEvent_TypeID == "2")//CCN
            {
                ActionStatus = "CCNCapaInsert";
            }
            else if (capaModel.QualityEvent_TypeID == "9")//Audit
            {
                ActionStatus = "Audit";
            }
            CommonDatalayer objAuditDal = new CommonDatalayer();
            int capaID = capaModel.CAPAID;
            int Department = capaModel.DeptID;
            int PlanofAction = Convert.ToInt32(capaModel.PlanofAction);
            string TargetDate = Convert.ToDateTime(capaModel.TargetDate).ToString("yyyy/MM/dd");
            int QualityEvent_TypeID = Convert.ToInt32(capaModel.QualityEvent_TypeID);
            int Responisble_Person = capaModel.ResponsibleEmpID;
            string Capa_ActionDesc = capaModel.Capa_ActionDesc;
            string QualityEvent_Number = capaModel.QualityEvent_Number;
            int CreatedBy = Convert.ToInt32(capaModel.CreatedBy);
            string action;
            if (capaModel.CAPAID == 0)
            {
                action = ActionStatus;
            }
            else
            {   //Update CAPA
                action = "CapaUpdate";
            }
            int AssessmentUniqueID = capaModel.AssessementUniqueID;
            int CapaQAEmployee = capaModel.CapaQAEmpId; ;
            int HeadQA_EmpID = capaModel.HeadQa_EmpID;
            int BaseID = capaModel.EventsBaseID;
            if (capaModel.CAPAID == 0)
            {
                db.AizantIT_SP_QualityEventsCapaCreation(capaID, Department, PlanofAction, TargetDate,
                          QualityEvent_TypeID, Responisble_Person, Capa_ActionDesc, QualityEvent_Number, CreatedBy,
                          action, AssessmentUniqueID, CapaQAEmployee, HeadQA_EmpID, BaseID);
            }
            else
            {
                var GetCAPAdetails = (from C in db.AizantIT_CAPAMaster
                                      join A in db.AizantIT_CAPA_ActionPlan
                                      on C.CAPAID equals A.CAPAID
                                      where C.CAPAID == capaModel.CAPAID
                                      select new
                                      {
                                          A.Capa_ActionID,
                                          A.Capa_Action_ImpID,
                                          A.Capa_Action_Imp_DeptID,
                                          A.ActionPlan_TimeLines,
                                          A.Capa_ActionDesc,
                                          C.QA_EmpID,
                                          C.QualityEvent_TypeID,
                                          C.QualityEvent_Number
                                      });
                CapaModel objCapa = new CapaModel();
                foreach (var item in GetCAPAdetails)
                {
                    objCapa.CapaQAEmpId = (int)item.QA_EmpID;
                    objCapa.Capa_ActionDesc = item.Capa_ActionDesc;
                    objCapa.DeptID = Convert.ToInt32(item.Capa_Action_Imp_DeptID);
                    objCapa.ResponsibleEmpID = Convert.ToInt32(item.Capa_Action_ImpID);
                    objCapa.QualityEvent_Number = item.QualityEvent_Number;
                    objCapa.QualityEvent_TypeID = item.QualityEvent_TypeID.ToString();
                    objCapa.PlanofAction = item.Capa_ActionID;
                    objCapa.TargetDate = Convert.ToDateTime(item.ActionPlan_TimeLines).ToString("dd MMM yyyy");
                }
                if (capaModel.CapaQAEmpId == objCapa.CapaQAEmpId && capaModel.Capa_ActionDesc == objCapa.Capa_ActionDesc && capaModel.DeptID == objCapa.DeptID &&
                    capaModel.ResponsibleEmpID == objCapa.ResponsibleEmpID && capaModel.QualityEvent_TypeID == objCapa.QualityEvent_TypeID &&
                    capaModel.QualityEvent_Number == objCapa.QualityEvent_Number && capaModel.TargetDate == objCapa.TargetDate
                    && capaModel.PlanofAction == objCapa.PlanofAction)
                {
                    ReturnResult = 1;
                }
                else
                {

                    db.AizantIT_SP_QualityEventsCapaCreation(capaID, Department, PlanofAction, TargetDate,
                                     QualityEvent_TypeID, Responisble_Person, Capa_ActionDesc, QualityEvent_Number, CreatedBy,
                                     action, AssessmentUniqueID, CapaQAEmployee, HeadQA_EmpID, BaseID);
                    //int dt = objAuditDal.QualityEventsCapaInsertDal(capaModel, "CapaUpdate");
                }
            }
            return ReturnResult;
        }
        public void EventsApprove_SendNtificationToResponsiblePerson(int EventBaseID, int QualityEventID)
        {
            using (AizantIT_DevEntities DB = new AizantIT_DevEntities())
            {
                DB.AizantIT_SP_CAPANotificationToResponsble_AfterHQAApprove(EventBaseID, QualityEventID);
            }
        }
        public void EventCAPANumber_AutoGenerated(int HeadQAEmpID, int EventBaseID, int QualityEventID, int ApprovedBy = 0)
        {
            using (AizantIT_DevEntities DB = new AizantIT_DevEntities())
            {
                DB.AizantIT_SP_CAPANOGenerateONEventApprove(HeadQAEmpID, EventBaseID, QualityEventID, ApprovedBy);
            }
        }
        //for Incident
        public static Boolean GetIsTMSHODRoleExist(int EmployeID = 0)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                if (EmployeID == 0)
                    EmployeID = Convert.ToInt32((HttpContext.Current.Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);

                var IsRoleExists = (from t1 in dbEF.AizantIT_EmpModuleRoles
                                    where t1.EmpID == EmployeID && t1.ModuleID == 3 && t1.RoleID == 3
                                    select t1).Any();
                return IsRoleExists;
            }
        }
        public static Boolean TMSHODAccesibleDeptExist(int incidentID, ref string DepartmentName, int AuthorEmpID = 0, int DepID = 0)
        {
            using (AizantIT_DevEntities dbEF = new AizantIT_DevEntities())
            {
                var getIncident = (from t1 in dbEF.AizantIT_QITTS_Master
                                   where t1.Incident_ID == incidentID
                                   select t1).SingleOrDefault();
                if (AuthorEmpID == 0)
                    AuthorEmpID = getIncident.AuthorBy;

                if (DepID == 0)
                    DepID = getIncident.DeptID;

                UMS_BAL objUMS_BAL = new UMS_BAL();
                int[] RoleIDs = new int[] { (int)TMS_UserRole.HOD_TMS };
                DataTable dt = objUMS_BAL.getRoleWiseDepts(AuthorEmpID, RoleIDs);
                bool exists = dt.Select().ToList().Exists(row => row["DeptID"].ToString() == (DepID).ToString());

                var Department = (from t1 in dbEF.AizantIT_DepartmentMaster
                                  where t1.DeptID == DepID
                                  select t1.DepartmentName).SingleOrDefault();
                DepartmentName = Department;
                return exists;
            }
        }
        public DocumentDetialsClass GetDocumentDetails(int Transcation_ID, int QEventID)
        {
            DocumentDetialsClass objbo = new DocumentDetialsClass();
            AizantIT_DevEntities dbEF = new AizantIT_DevEntities();
            var GetDocDetials = (from T1 in dbEF.AizantIT_Event_DocRef
                                 join T2 in dbEF.AizantIT_DocVersion on T1.DocVersionID equals T2.VersionID
                                 join T3 in dbEF.AizantIT_DocumentMaster on T2.DocumentID equals T3.DocumentID
                                 join T4 in dbEF.AizantIT_DocumentType on T3.DocumentTypeID equals T4.DocumentTypeID
                                 join t5 in dbEF.AizantIT_DocVersionTitle on T2.DocTitleID equals t5.DocTitleID
                                 where T1.QEventTYpeID == QEventID && T1.QTransctionID == Transcation_ID
                                 select new { t5.Title, T3.DocumentNumber, T2.VersionID, T4.DocumentType }).ToList();
            foreach (var item in GetDocDetials)
            {
                objbo.DocumentNumber = item.DocumentNumber;
                objbo.DocumentName = item.Title;
                objbo.DocumentType = item.DocumentType;
                objbo.VersionID = item.VersionID;
            }
            return objbo;
        }
        public DocumentDetialsClass GetOtherDocumentDetails(int Transcation_ID, int QEventID)
        {
            DocumentDetialsClass objbo = new DocumentDetialsClass();
            AizantIT_DevEntities dbEF = new AizantIT_DevEntities();
            var OtherDoc = (from T1 in dbEF.AizantIT_Event_OtherRef
                            where T1.QEventTYpeID == QEventID && T1.QTransctionID == Transcation_ID
                            select new { T1.ReferenceDetails, T1.ReferenceNumber }).Take(1);
            foreach (var item in OtherDoc)
            {
                objbo.DocumentNumber = item.ReferenceNumber;
                objbo.DocumentName = item.ReferenceDetails;
            }
            return objbo;
        }
    }
}