﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Aizant_API_Entities;
using Aizant_Enums;
using AizantIT_PharmaApp.Areas.QMS.XtraReports.CCN;
using AizantIT_PharmaApp.Common;
using AizantIT_PharmaApp.Common.CustomFilters;
using DevExpress.XtraReports.UI;
using QMS_BO.QMS_ChangeControl_BO;
using QMS_BO.QMSComonBO;
using QMS_BusinessLayer;
using UMS_BO;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;
using AizantIT_PharmaApp.Areas.QMS.Models.Common;
using System.Threading.Tasks;
using AizantIT_PharmaApp.Areas.QMS.Controllers.Common;

namespace AizantIT_PharmaApp.Areas.QMS.Controllers.ChangeController
{
    // [SessionExpire]
    [RouteArea]
    [CustAuthentication]
    [CustAuthorization(
        (int)AizantEnums.Modules.QMS,
        (int)QMS_Roles.Initiator,
        (int)QMS_Roles.HOD,
        (int)QMS_Roles.QA,
        (int)QMS_Roles.HeadQA,
        (int)QMS_Roles.AdminRole,
         (int)QMS_Roles.User,
        (int)QMS_Roles.Investigator,
        (int)QMS_Audit_Roles.AuditHOD,
         (int)QMS_Audit_Roles.AuditQA,
         (int)QMS_Audit_Roles.AuditHeadQA)]
    public class ChangeControlController : Controller
    {
        private AizantIT_DevEntities dbEF = new AizantIT_DevEntities();
        QMS_BAL objbal = new QMS_BAL();
        CCNBal Qms_bal = new CCNBal();
        UMS_BAL objUMS_BAL = new UMS_BAL();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region CCNList
        // GET: ChangeControlNote
        public ActionResult CCNList(int ShowType)
        {
            try
            {
                CCNMaster CCNBO = new CCNMaster();
                //Query String
                switch (ShowType)
                {
                    case 1:
                        ShowType = (int)QMS_CCNListTypes.CCNCreatedList;
                        break;
                    case 2:
                        ShowType = (int)QMS_CCNListTypes.Initiator_RevertedList;
                        break;
                    case 3:
                        ShowType = (int)QMS_CCNListTypes.CCNHodReview;
                        break;
                    case 4:
                        ShowType = (int)QMS_CCNListTypes.QA_AssessmentList;
                        break;
                    case 5:
                        ShowType = (int)QMS_CCNListTypes.HODImpactpoints;
                        break;
                    case 6:
                        ShowType = (int)QMS_CCNListTypes.HOD_RevertedImpactpoints;
                        break;
                    case 7:
                        ShowType = (int)QMS_CCNListTypes.HOD_ImpactPointsMultiNotifn;
                        break;
                    case 8:
                        ShowType = (int)QMS_CCNListTypes.HOD_RevertImpactpointMultiRevertNotifn;
                        break;
                    case 9:
                        ShowType = (int)QMS_CCNListTypes.QA_RevertedList;
                        break;
                    case 10:
                        ShowType = (int)QMS_CCNListTypes.HeadQA_Approval;
                        break;
                    case 11:
                        ShowType = (int)QMS_CCNListTypes.CCN_QAClose;
                        break;
                    case 12:
                        ShowType = (int)QMS_CCNListTypes.QA_Verification;
                        break;
                    case 13:
                        ShowType = (int)QMS_CCNListTypes.CCN_Reports;
                        break;
                    case 14:
                        ShowType = (int)QMS_CCNListTypes.CCN_MainList;
                        break;
                    case 15:
                        ShowType = (int)QMS_CCNListTypes.C_Pending;
                        break;
                    case 16:
                        ShowType = (int)QMS_CCNListTypes.C_Rejected;
                        break;
                    case 17:
                        ShowType = (int)QMS_CCNListTypes.C_Approved;
                        break;
                    case 18:
                        ShowType = (int)QMS_CCNListTypes.C_Verify;
                        break;
                    case 19:
                        ShowType = (int)QMS_CCNListTypes.C_Closer;
                        break;
                    case 20:
                        ShowType = (int)QMS_CCNListTypes.C_DueDate;
                        break;
                    case 21:
                        ShowType = (int)QMS_CCNListTypes.filterInitiator;
                        break;
                    case 22:
                        ShowType = (int)QMS_CCNListTypes.CCN_Manage;
                        break;
                }
                TempData["QueryString"] = ShowType;
                TempData.Keep("QueryString");
                //TempData["CCN_ID"] = 0 ;
                //Check Roles
                CheckRoles(ShowType);
                //GetUserEmployeID(CCNBO);

                //int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };

                //NotificationUpdate(Notification_ID, EmpIDs);

                if (ShowType == 15 || ShowType == 16 || ShowType == 17 || ShowType == 18 || ShowType == 19 || ShowType == 20)
                {
                    TempData.Keep("CCNEvent"); TempData.Keep("RoleIDFromCharts"); TempData.Keep("DeptIDFromDashboard");
                    TempData.Keep("FromDateDashboard"); TempData.Keep("ToDateDashboard");
                }
                else
                {
                    TempData["CCNEvent"] = 0;
                    TempData["ToDateDashboard"] = 0;
                    TempData["FromDateDashboard"] = 0;
                    TempData["DeptIDFromDashboard"] = 0;
                    TempData["RoleIDFromCharts"] = 0;
                }

                return View();
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M1:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return View();
            }
        }
        public void CheckRoles(int ShowType)
        {
            if (ShowType == 2)
            {   //Initiator
                TempData["RoleID"] = 24;
            }

            if (ShowType == 3 || ShowType == 5 || ShowType == 7 || ShowType == 6 || ShowType == 8)
            {
                //Hod role
                TempData["RoleID"] = 25;
            }

            if (ShowType == 4 || ShowType == 12 || ShowType == 9 || ShowType == 11)
            {
                //QA Role
                TempData["RoleID"] = 26;
            }

            if (ShowType == 10)
            {
                //Head QA Role
                TempData["RoleID"] = 27;
            }
            if (ShowType == 14 || ShowType == 21 || ShowType == 1)
            {
                //CCN List
                TempData["RoleID"] = 'M';
            }
            if (ShowType == 13)
            {
                //ReportList
                TempData["RoleID"] = "R";
            }
        }

        //public void NotificationUpdate(int Notification_ID, int[] EmpIDs)
        //{
        //    ViewBag.CCNID = 0; ViewBag.UniqueID = 0; ViewBag.BlockID = 0; TempData["CCN_ID"] = 0;
        //    int CCN_ID = 0; //int CCNCurrentStatus = 0;
        //    if (Notification_ID != 0)
        //    {
        //        //Notification Status Update
        //        objUMS_BAL = new UMS_BAL();
        //        objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs); //2 represents NotifyEmp had visited the page through notification link.

        //        var GetCCNID = (from C in dbEF.AizantIT_CCNNotificationLink
        //                        where C.NotificationID == Notification_ID
        //                        select C.CCNID).SingleOrDefault();
        //        ViewBag.CCNID = GetCCNID;
        //        CCN_ID = GetCCNID;
        //        TempData["CCN_ID"] = GetCCNID;
        //        TempData.Keep("CCN_ID");
        //        int EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
        //        var GetAssessmentIDBlcokID = (from T1 in dbEF.AizantIT_ChangeControlCommitte
        //                                      join t2 in dbEF.AizantIT_CCNNotificationLink
        //                                      on T1.Assessment_ID equals t2.Assessment_UniqueID
        //                                      where t2.NotificationID == Notification_ID
        //                                      select T1).SingleOrDefault();
        //        if (GetAssessmentIDBlcokID != null)
        //        {
        //            ViewBag.UniqueID = GetAssessmentIDBlcokID.Assessment_ID;
        //            ViewBag.BlockID = GetAssessmentIDBlcokID.BlockID;
        //        }

        //    }
        //}
        //Notification Status Update
        public void GetNotificationUpdate(int Notification_ID, int[] EmpIDs)
        {
                objUMS_BAL = new UMS_BAL();
                objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs); //2 represents NotifyEmp had visited the page through notification link.
        }
        public AssesmentBlock_IDs GetAssessmentID(int Notification_ID)
        {
            AssesmentBlock_IDs objAss_IDs = new AssesmentBlock_IDs();
            var GetAssessmentIDBlcokID = (from T1 in dbEF.AizantIT_ChangeControlCommitte
                                          join t2 in dbEF.AizantIT_CCNNotificationLink
                                          on T1.Assessment_ID equals t2.Assessment_UniqueID
                                          where t2.NotificationID == Notification_ID
                                          select T1).SingleOrDefault();
            if (GetAssessmentIDBlcokID != null)
            {
                objAss_IDs.AssessmentID = GetAssessmentIDBlcokID.Assessment_ID;
                objAss_IDs.BlockID = GetAssessmentIDBlcokID.BlockID;
            }
            return objAss_IDs;
        }
        [HttpGet]
        public JsonResult GetCCNDataList()
        {
            try
            {
                int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                int EmployeeID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
               // int CCN_ID = Convert.ToInt32(Request.Params["CCN_ID"]);
                int Role = Convert.ToInt32(Request.Params["Role"]);
                int RoleIDCharts = Convert.ToInt32(Request.Params["RoleIDCharts"]);
                int deptIdDashboard = Convert.ToInt32(Request.Params["DeptDashboard"]);
                string FromDateDashboard = (Request.Params["FromDateDashboard"]).ToString();
                string ToDateDashboard = (Request.Params["ToDateDashboard"]).ToString();
                var EventChart = (Request.Params["EventChart"]).ToString();

                string sSearch_CCN_Number = Request.Params["sSearch_2"];
                string sSearch_Department = Request.Params["sSearch_3"];
                string sSearch_Block = Request.Params["sSearch_4"];
                string sSearch_Initiatedby = Request.Params["sSearch_5"];
                string sSearch_Classification = Request.Params["sSearch_6"];
                string sSearch_CCN_Date = Request.Params["sSearch_7"];
                string sSearch_CurrentStatus = Request.Params["sSearch_8"];
                int filteredCount = 0;

                List<CCNList> listEmployees = new List<CCNList>();
                int totalRecordsCount = 0;
                DataSet dt = Qms_bal.GetCCNListBal(displayLength, displayStart, sortCol, sSortDir, sSearch, Role,/* CCN_ID,*/ EventChart, 
                    RoleIDCharts, deptIdDashboard, FromDateDashboard, ToDateDashboard, EmployeeID
                    , sSearch_CCN_Number, sSearch_Department, sSearch_Block, sSearch_Initiatedby
            , sSearch_Classification, sSearch_CCN_Date, sSearch_CurrentStatus);

                if (dt.Tables[0].Rows.Count > 0)
                {
                    totalRecordsCount = Convert.ToInt32(dt.Tables[0].Rows[0]["TotalCount"]);
                    foreach (DataRow rdr in dt.Tables[0].Rows)
                    {
                        CCNList CCNList = new CCNList();
                        CCNList.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                        CCNList.CCNID = Convert.ToInt32(rdr["CCN_ID"].ToString());
                        CCNList.BlockID = Convert.ToInt32(rdr["BlockID"].ToString());
                        CCNList.CCN_Number = (rdr["CCN_No"].ToString());
                        CCNList.Block = (rdr["BlockName"].ToString());
                        CCNList.CCN_Date = (rdr["CCN_Date"].ToString());
                        CCNList.Department = (rdr["Department"]).ToString();
                        CCNList.Initiatedby = rdr["Initiatedby"].ToString();
                        CCNList.CurrentStatus = (rdr["CurrentStatus"]).ToString();
                        //CCNList.Transaction = (rdr["Transction"].ToString());
                        CCNList.AssessmentID = Convert.ToInt32(rdr["AssessmentID"]);
                        CCNList.Classification = (rdr["Classification"]).ToString();
                        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                        listEmployees.Add(CCNList);
                    }
                }
                return Json(new
                {
                    hasError = false,
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = listEmployees
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                //ViewBag.SysError = "Yes";
                return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region GetEmployeeID
        public void GetUserEmployeID(CCNMaster CCNBO)
        {
            if (Session["UserDetails"] != null)
            {
                CCNBO.EMPID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            }
        }
        public int GetCookieEmployeID()
        {
            int GetUserEmpID = 0;
            if (Session["UserDetails"] != null)
            {
                GetUserEmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            }
            return GetUserEmpID;
        }
        #endregion

        #region DropDownBinding
        //Bind Department dropdown for ReportList
        public void BindDeptDropdownForReports()
        {
            //Department binding
            List<SelectListItem> DepRpt = new List<SelectListItem>();
            var DepartmentRept = (from DM in dbEF.AizantIT_DepartmentMaster
                                  orderby DM.DepartmentName
                                  select new { DM.DeptID, DM.DepartmentName }).Distinct().ToList();
            DepRpt.Add(new SelectListItem
            {
                Text = "Select",
                Value = "0",
            });
            foreach (var item in DepartmentRept)
            {
                DepRpt.Add(new SelectListItem
                {
                    Text = item.DepartmentName,
                    Value = item.DeptID.ToString(),
                });
            }
            ViewBag.ddlDepartmentBinding = new SelectList(DepRpt.ToList(), "Value", "Text");
        }


        //QA ,HQA,Block and Department
        public void CCNDropdownbindings(CCNMaster CCNMasterBO)
        {

            List<int> QaEmployeeid = new List<int>() { CCNMasterBO.Initiator_EmpID, CCNMasterBO.HOD_EmpID };

            List<SelectListItem> AssignedQAList = BindQAandHeadQAEmployeeID((int)QMS_Roles.QA, CCNMasterBO.QA_EmpID, QaEmployeeid);
            CCNMasterBO.ddlAssignedQA = AssignedQAList;

            List<int> HeadQAEmployeeid = new List<int>() { CCNMasterBO.Initiator_EmpID, CCNMasterBO.HOD_EmpID, CCNMasterBO.QA_EmpID };

            List<SelectListItem> AssignedHQAList = BindQAandHeadQAEmployeeID((int)QMS_Roles.HeadQA, CCNMasterBO.HeadQA_EmpID, HeadQAEmployeeid);
            CCNMasterBO.ddlAssignedHeadQA = AssignedHQAList;

            //Bind Block ID  to DropDown
            List<SelectListItem> BindBlock = new List<SelectListItem>();

            var D1 = (from tp in dbEF.AizantIT_BlockMaster
                      where tp.Currentstatus == true
                      select new { tp.BlockName, tp.BlockID }).ToList();
            BindBlock.Add(new SelectListItem
            {
                Text = "Select",
                Value = "0",
            });
            foreach (var item in D1)
            {
                BindBlock.Add(new SelectListItem
                {
                    Text = item.BlockName,
                    Value = Convert.ToString(item.BlockID),
                });
            }
            ViewBag.BlockID = new SelectList(BindBlock.ToList(), "Value", "Text");
        }
        #endregion

        #region CCNCreation
        public void BindDropDownCCNCreate(CCNMaster objccn)
        {
            //CCNMaster objccn = new CCNMaster();
            GetUserEmployeID(objccn);
            DataSet ds = Qms_bal.GetddlBindingCCNCreate(objccn.EMPID);

            //Department binding
            List<SelectListItem> Dep = new List<SelectListItem>();
            Dep.Add(new SelectListItem
            {
                Text = "Select",
                Value = "0",
            });
            foreach (DataRow rdr in ds.Tables[0].Rows)
            {
                Dep.Add(new SelectListItem
                {
                    Text = (rdr["DepartmentName"]).ToString(),
                    Value = (rdr["DeptID"]).ToString(),
                    Selected = (Convert.ToInt32(rdr["DeptID"]) == objccn.DeptID ? true : false)
                });
            }
            objccn.ddlDeptList = Dep;
            //Document Type binding
            List<SelectListItem> DocumentTypeList = new List<SelectListItem>();
            DocumentTypeList.Add(new SelectListItem
            {
                Text = "Select",
                Value = "-1",
            });
            foreach (DataRow rdr in ds.Tables[1].Rows)

            {
                DocumentTypeList.Add(new SelectListItem
                {
                    Text = (rdr["DocumentType"]).ToString(),
                    Value = (rdr["DocumentTypeID"]).ToString(),
                    Selected = (Convert.ToInt32(rdr["DocumentTypeID"]) == objccn.DocumentType ? true : false)
                    //objccn.CCN_ID!=0 || (Convert.ToInt32(rdr["DocumentTypeID"]) == objccn.DocumentType )? true: false
                    //

                });
            }
            DocumentTypeList.Add(new SelectListItem
            {
                Text = "Others",
                Value = "0",
            });
            objccn.ddlDocumentTypeList = DocumentTypeList;
            //Market Type binding
            List<SelectListItem> MarketList = new List<SelectListItem>();
            MarketList.Add(new SelectListItem
            {
                Text = "Select",
                Value = "0",
            });
            foreach (DataRow rdr in ds.Tables[2].Rows)
            {
                MarketList.Add(new SelectListItem
                {
                    Text = (rdr["MarketName"]).ToString(),
                    Value = (rdr["MarketID"]).ToString(),
                    Selected = (Convert.ToInt32(rdr["MarketID"]) == objccn.marketID ? true : false)
                });
            }
            objccn.ddlMarketList = MarketList;
            //Project Type binding
            List<SelectListItem> ProjectList = new List<SelectListItem>();
            ProjectList.Add(new SelectListItem
            {
                Text = "Select",
                Value = "0",
            });
            foreach (DataRow rdr in ds.Tables[3].Rows)
            {
                ProjectList.Add(new SelectListItem
                {
                    Text = (rdr["ProjectNumber"]).ToString(),
                    Value = (rdr["ProjectID"]).ToString(),
                    Selected = (Convert.ToInt32(rdr["ProjectID"]) == objccn.ProjectID ? true : false)
                });
            }
            objccn.ddlProjectList = ProjectList;
            //Item Type binding
            List<SelectListItem> ItemList = new List<SelectListItem>();
            ItemList.Add(new SelectListItem
            {
                Text = "Select",
                Value = "0",
            });
            foreach (DataRow rdr in ds.Tables[4].Rows)
            {
                ItemList.Add(new SelectListItem
                {
                    Text = (rdr["ItemCode"]).ToString(),
                    Value = (rdr["ItemID"]).ToString(),
                    Selected = (Convert.ToInt32(rdr["ItemID"]) == objccn.ItemID ? true : false)

                });
            }
            objccn.ddlItemList = ItemList;
            //TypeOfChange  binding
            List<SelectListItem> TypeOfChangeList = new List<SelectListItem>();
            TypeOfChangeList.Add(new SelectListItem
            {
                Text = "Select",
                Value = "0",
            });
            foreach (DataRow rdr in ds.Tables[5].Rows)
            {
                TypeOfChangeList.Add(new SelectListItem
                {
                    Text = (rdr["TypeofChange"]).ToString(),
                    Value = (rdr["TC_ID"]).ToString(),
                    Selected = (Convert.ToInt32(rdr["TC_ID"]) == objccn.TC_ID ? true : false)

                });
            }
            objccn.ddlTypeOfChangeList = TypeOfChangeList;

        }
        public List<SelectListItem> BindHODDropdownAccessibleDepartmentsandRole(int DeptID, int Roleid, int SelectedEmpID, List<int> NotBindingLoginEmpId)
        {
            //Assigned HOD's
            List<SelectListItem> objAssignHod = new List<SelectListItem>();
            var AssignedHOD = dbEF.Database.SqlQuery<AccessibleEmpOnDeptandRole>("exec UMS.AizantIT_SP_ToGetEmployeesByDeptIDandRoleID " +
               +DeptID + "," + Roleid + ",'" + 1 + "'").ToList();
            objAssignHod.Add(new SelectListItem
            {
                Text = "Select",
                Value = "0",
            });
            foreach (var RPItem in AssignedHOD)
            {
                if (!NotBindingLoginEmpId.Contains(RPItem.EmpID))
                {
                    objAssignHod.Add(new SelectListItem()
                    {
                        Text = RPItem.EmpName,
                        Value = RPItem.EmpID.ToString(),
                        Selected = (Convert.ToInt32(RPItem.EmpID) == SelectedEmpID ? true : false)
                    });
                }
            }
            return objAssignHod.ToList();
        }
        public List<SelectListItem> BindQAandHeadQAEmployeeID(int RoleID, int SelectedEmpId, List<int> NotBindingEmpID)
        {

            List<SelectListItem> AssignedQAList = new List<SelectListItem>();

            var AssignedQA = dbEF.Database.SqlQuery<AccessibleEmpOnDeptandRole>("exec [UMS].[AizantIT_SP_GetEmployeesByRoleIDAndStatus] " +
                             RoleID + ",'A'").ToList();
            foreach (var item in AssignedQA)
            {
                if (!NotBindingEmpID.Contains(item.EmpID))
                {
                    AssignedQAList.Add(new SelectListItem()
                    {
                        Text = item.EmpName,
                        Value = item.EmpID.ToString(),
                        Selected = (item.EmpID == SelectedEmpId ? true : false)
                    });
                }
            }
            return AssignedQAList;
        }
        //Create CCN
        public ActionResult CreateCCN(int? id=0, int ShowType=0, int Notification_ID = 0)

        {
            try
            {
                //throw new Exception("HAI");
                TempData.Remove("IsTempFileID");

                CCNMaster CCNBO = new CCNMaster();
                if (id == 0 || id == null)//Create New
                {
                    TempData["QueryString"] = "CCNCreat";
                    BindDropDownCCNCreate(CCNBO);
                }
                else  //Edit 
                {
                    if (Notification_ID != 0)
                    {
                        TempData["QueryString"] = ShowType;
                        TempData.Keep("QueryString");
                        int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                        GetNotificationUpdate(Notification_ID, EmpIDs);
                    }
                    TempData.Keep("QueryString");
                    CCNBO = GetCCNdata(id);
                    RefershCCNDeletedFilesIsTempIsFalse((int)id, "Update");
                    FileUploadsExistorNotInInitator(id);
                    BindDropDownCCNCreate(CCNBO);
                    //Bind Document Reference number with Selected Value
                    var RefNo = (from dv in dbEF.AizantIT_DocVersion
                                 join DM in dbEF.AizantIT_DocumentMaster on dv.DocumentID equals DM.DocumentID
                                 where DM.DocumentTypeID == CCNBO.DocumentType && DM.DeptID == CCNBO.DeptID && (dv.Status == "A" || dv.Status == "R") && dv.EffectiveDate != null
                                 select new { dv.VersionID, DM.DocumentNumber }).ToList();
                    ViewBag.ReferenceNumber = new SelectList(RefNo.ToList(), "VersionID", "DocumentNumber", CCNBO.VersionID);
                    int VersionID = Convert.ToInt32(CCNBO.VersionID);
                    if (RefNo.Count > 0)
                    {
                        bool has = RefNo.Any(t => t.VersionID == VersionID);
                        if (has == false)
                        {
                            var DocumentID = (from t1 in dbEF.AizantIT_DocVersion
                                              where t1.VersionID == VersionID
                                              select t1.DocumentID).SingleOrDefault();

                            var GetNewVersionID = (from t1 in dbEF.AizantIT_DocVersion
                                                   orderby t1.VersionID descending
                                                   where t1.DocumentID == DocumentID && (t1.Status == "A" || t1.Status == "R") && t1.EffectiveDate != null
                                                   select  t1.VersionID);
                            int NewVersionID = GetNewVersionID.First();
                            CCNBO.DocTitle = (from t1 in dbEF.AizantIT_DocVersionTitle
                                              join t2 in dbEF.AizantIT_DocVersion on t1.DocTitleID equals t2.DocTitleID
                                              where t2.VersionID == NewVersionID
                                              select t1.Title).SingleOrDefault();

                            ViewBag.ReferenceNumber = new SelectList(RefNo.ToList(), "VersionID", "DocumentNumber", NewVersionID);

                        }
                    }
                    //Assigned HOD's
                    List<int> NotBinding = new List<int> { };
                    List<SelectListItem> objAssignHod = BindHODDropdownAccessibleDepartmentsandRole(CCNBO.DeptID, (int)QMS_Roles.HOD, CCNBO.HOD_EmpID, NotBinding);

                    ViewBag.AssignedHOD = new SelectList(objAssignHod.ToList(), "Value", "Text", CCNBO.HOD_EmpID);
                    //Revert CCN by Head QA 
                    if (CCNBO.CurrentStatus == 9)
                    {
                        var HOD_EmpName = (from EM in dbEF.AizantIT_EmpMaster
                                           where EM.EmpID == CCNBO.HOD_EmpID
                                           select (EM.FirstName + "" + EM.LastName)
                                           ).SingleOrDefault();
                        CCNBO.HQA_RevertHodEmpName = HOD_EmpName;
                    }
                }
                ALL_Comments(CCNBO);
                return View("CreateCCN", CCNBO);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M2:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return View();
            }
        }

        //Create CCN Submit Code 
        [HttpPost]
        public JsonResult CreateCCN(CCNMaster cCNMaster, string Status)
        {
            try
            {
                TempData.Keep("IsTempFileID");
                int IsTempRefID = Convert.ToInt32(TempData["IsTempFileID"]);
                DataTable dt = new DataTable();
                cCNMaster.Initiator_EmpID = GetCookieEmployeID();
                if (Status == "Created")
                {
                    dt = Qms_bal.CCNInsertBal(cCNMaster, Status, IsTempRefID);
                    var CCN_Number = dt.Rows[0][0].ToString();
                    TempData.Remove("IsTempFileID");
                    return Json(new { hasError = false, data = CCN_Number }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Boolean Result = ToCheckValidEmployeeOrNot(cCNMaster.CCN_ID, 24);
                    if (Result == true)
                    {
                        dt = Qms_bal.CCNInsertBal(cCNMaster, Status, IsTempRefID);
                        var CCN_Number = cCNMaster.CCN_No;
                        TempData.Remove("IsTempFileID");
                        return Json(new { hasError = false, data = CCN_Number }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { hasError = true, data = "You are not authorized Initiator to do action" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M3:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }

        }
        #endregion


        #region GetCCNnumber
        //Get CCN Number
        [HttpGet]
        public JsonResult CCNNumber(int DeptID)
        {
            try
            {
                DataTable dt1 = new DataTable();
                //2 means Change Control
                dt1 = objbal.BalGetAutoGenaratedNumber(DeptID, 2);
                var CCNNumber = dt1.Rows[0][0].ToString();
                return Json(new { hasError = false, data = CCNNumber }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M5:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region CapaActionForCCN

        //Deleted Temporary Capa's
        public void DeleteTempCapas(int AssessmentID)
        {
            Qms_bal.DeleteCapaTemp(AssessmentID, "CCN", 0);
        }

        //Delete Assessment Records
        [HttpPost]
        public JsonResult DeleteAssessmentRecords(int id, string Oper, string ActionFileNo = "")//ActionFileNo=CCNNumber.
        {
            try
            {
                int ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                int i = Qms_bal.DeleteCCN_ActionPlanAndCCCRecordBal(id, Oper, ActionBy, ActionFileNo);
                return Json(new { hasError = false, data = i }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M9:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region AssessmentsCode

        [HttpPost]
        public JsonResult ImpactPoins(List<ImpactPoints> students, string comments, int Status, int AssessmentUniqueID)
        {
            try
            {
                var AssignedDeptID = (from CC in dbEF.AizantIT_ChangeControlCommitte
                                      join DM in dbEF.AizantIT_DepartmentMaster on CC.DeptID equals DM.DeptID
                                      where CC.Assessment_ID == AssessmentUniqueID
                                      select new { DM.DepartmentName }).SingleOrDefault();
                //Assessment Comments added with Assessment Department and Comments
                var Assesscomments = AssignedDeptID.DepartmentName + ":  " + comments.Trim();
                int CCNIDImpact = 0;
                foreach (var item in students)
                {
                    CCNIDImpact = item.CCN_ID;
                }
                //Added Temporary table and this table send to sp
                if (students == null)
                {
                    students = new List<ImpactPoints>();

                }
                int i = 0, k = 0, dept = 0, block = 0;
                if (TempData["ImpcatDeptEmpty"] != null)
                {
                    dept = (int)TempData["ImpcatDeptEmpty"];
                    block = (int)TempData["ImpcatBlockEmpty"];
                }

                DataTable dtImpactlst = new DataTable();
                dtImpactlst.Columns.Add("ImpPoint_ID");
                dtImpactlst.Columns.Add("CCN_ID");
                dtImpactlst.Columns.Add("isYesorNo");
                dtImpactlst.Columns.Add("DocTitleorRefN");
                dtImpactlst.Columns.Add("Remarks");
                dtImpactlst.Columns.Add("AssessmentId");
                DataRow dr;
                foreach (ImpactPoints item in students)
                {
                    dr = dtImpactlst.NewRow();

                    dr["ImpPoint_ID"] = item.ImpactPointID;
                    dr["CCN_ID"] = item.CCN_ID;
                    dr["isYesorNo"] = item.isyesorNo;
                    if (item.DocNo == null)
                    {
                        dr["DocTitleorRefN"] = "N/A";
                    }
                    else
                    {
                        dr["DocTitleorRefN"] = item.DocNo;
                    }
                    if (item.Comments == null)
                    {
                        dr["Remarks"] = "N/A";
                    }
                    else
                    {
                        dr["Remarks"] = item.Comments;
                    }
                    dr["AssessmentId"] = AssessmentUniqueID;
                    dtImpactlst.Rows.Add(dr);
                }
                //Getting Employee id
                int Empid = GetCookieEmployeID();
                Boolean Result = ToAssessmentCheckValidEmployeeOrNot(CCNIDImpact, AssessmentUniqueID, 25);
                if (Result == true)
                {
                    int count = Qms_bal.AddCCN_HODimpactPointInsertBal(Empid, dtImpactlst, CCNIDImpact, Assesscomments, dept, block, Status, AssessmentUniqueID);
                }
                else
                {
                    return Json(new { hasError = true, data = "You are not authorized HOD to do action." }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { hasError = false, data = k }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M10:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        //Assessments View
        public PartialViewResult ViewHODAssessmentDetails(int? UID, int? CCNID, int? DeptID, int? blockID)
        {
            try
            {

                TempData["AssessmentIDFromList"] = UID;
                //TempData["Block"] = blockID;
                ViewBag.AssessmeentID = UID;
                GetHodAssFileUploadCount((int)UID);
                //Get Department Name and Block Name
                var AssignedDeptID = (from CC in dbEF.AizantIT_ChangeControlCommitte
                                      join DM in dbEF.AizantIT_DepartmentMaster on CC.DeptID equals DM.DeptID
                                      join B in dbEF.AizantIT_BlockMaster on CC.BlockID equals B.BlockID
                                      where CC.Assessment_ID == UID && CC.BlockID == blockID
                                      select new
                                      {
                                          DM.DepartmentName,
                                          B.BlockName,
                                      }).ToList();
                foreach (var item in AssignedDeptID)
                {

                    ViewData["AssignedDeptName"] = item.DepartmentName;
                    ViewData["AssigneDBlockName"] = item.BlockName;
                }
                TempData.Keep("QueryString");
                CCNMaster CCNBO = new CCNMaster();
                Aizant_API_Entities.AizantIT_ChangeControlMaster CCNMAster_ = new Aizant_API_Entities.AizantIT_ChangeControlMaster();
                Aizant_API_Entities.AizantIT_ChangeControlCommitte aizantIT_ = new Aizant_API_Entities.AizantIT_ChangeControlCommitte();
                aizantIT_ = dbEF.AizantIT_ChangeControlCommitte.Where(a => a.Assessment_ID == UID).ToList().FirstOrDefault();
                CCNMAster_ = dbEF.AizantIT_ChangeControlMaster.Where(a => a.CCN_ID == CCNID).ToList().FirstOrDefault();
                //Binded CCN Master Status bind to ChangeControlCommitte table
                aizantIT_.CurrentStatus = CCNMAster_.CurrentStatus;
                TempData["CCNNumber"] = CCNMAster_.CCN_No;
                List<ImpactPoints> Emp = new List<ImpactPoints>();
                var GetAssessment = (from od in dbEF.AizantIT_ImpactAssessmentTransaction
                                     join DepLnk in dbEF.AizantIT_ImpactDeptLink on od.ImpPoint_ID equals DepLnk.ImpPoint_ID
                                     join pd in dbEF.AizantIT_ImpactPoints on DepLnk.Imp_Desc_ID equals pd.Imp_Desc_ID
                                     where od.CCN_ID == CCNID && od.AssessmentId == UID/* && DepLnk.Currentstatus == true*/
                                     select new { pd.Impact_Description, od.ImpPoint_ID, od.isYesorNo, od.DocTitleorRefN, od.Remarks, od.Trans_ID }).OrderBy(t2 => t2.Trans_ID)

                                     .Union(from A in dbEF.AizantIT_ImpactAssessmentTransaction
                                            join pd in dbEF.AizantIT_ImpactPoints on A.ImpPoint_ID equals pd.Imp_Desc_ID
                                            where A.CCN_ID == CCNID && A.AssessmentId == UID && A.ImpPoint_ID == 0
                                            select new { pd.Impact_Description, A.ImpPoint_ID, A.isYesorNo, A.DocTitleorRefN, A.Remarks, A.Trans_ID }).OrderBy(t2 => t2.Trans_ID).ToList();

                foreach (var dr in GetAssessment)
                {
                    bool k = dr.isYesorNo.Value;
                    string s1 = "";
                    if (k == true) { s1 = "Yes"; } else { s1 = "No"; }
                    Emp.Add(new QMS_BO.QMS_ChangeControl_BO.ImpactPoints
                    {
                        ImpactName = dr.Impact_Description.ToString(),
                        DocNo = dr.DocTitleorRefN.ToString(),
                        Comments = dr.Remarks.ToString(),
                        isyesorNo = s1,
                        TransctionID = dr.Trans_ID,
                    }
                    );
                }
                ViewBag.Impact = Emp.AsEnumerable();
                return PartialView(aizantIT_);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M11:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }

        }

        //Bind Impact Points in HOD Page
        [HttpPost]
        public JsonResult OnDDLChange(string[] pId, string blockname, int CCNID)
        {

            try
            {
                if (pId != null)
                {
                    List<SelectListItem> impListHOD = new List<SelectListItem>();
                    DataTable dsEmp = new DataTable();
                    string names = string.Empty;
                    foreach (var obj in pId)
                    {
                        names += obj + ",";
                        //your insert query
                    }
                    names = names.TrimEnd(',');
                    dsEmp = Qms_bal.CCNSplitDeptStringBal(names);
                    List<ImpactPoints> Emp = new List<ImpactPoints>();
                    for (int i = 0; i < dsEmp.Rows.Count; i++)
                    {
                        ImpactPoints objEmp = new ImpactPoints();
                        objEmp.ImpactPointID = Convert.ToInt32(dsEmp.Rows[i]["DeptID"]);//Department ID
                        objEmp.ImpactName = dsEmp.Rows[i]["DepartmentName"].ToString();//DeptName
                        objEmp.Comments = blockname.ToString();//Block Name
                        int GetDeptID = Convert.ToInt32(dsEmp.Rows[i]["DeptID"].ToString());

                        List<EmployeesList> empList = new List<EmployeesList>();
                        int LoginEMPID = GetCookieEmployeID();
                        //Login Employee id Not binding and Bind Hod's
                        List<int> NotBindingQAHQA = new List<int>() { LoginEMPID };

                        objUMS_BAL = new UMS_BAL();
                        List<SelectListItem> objAssignHod = new List<SelectListItem>();
                        DataTable dtemp = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(GetDeptID), (int)QMS_Roles.HOD, true); //25=hod

                        foreach (DataRow result in dtemp.Rows)
                        {
                            EmployeesList empObjList = new EmployeesList();
                            if (!NotBindingQAHQA.Contains(Convert.ToInt32(result["EmpID"])))
                            {
                                empObjList.EmpID = Convert.ToInt32(result["EmpID"]);
                                empObjList.EmployeeName = (result["DeptCode"] + " - " + (result["EmpName"])).ToString();
                                empList.Add(empObjList);
                            }
                        }
                        objEmp.Employees = empList;
                        Emp.Add(objEmp);
                    }
                    return Json(Emp, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M2:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult AddDeptAssessment(List<ChangeControlCommitte> students, int blockid)
        {
            try
            {
                if (students == null)
                {
                    students = new List<ChangeControlCommitte>();
                }
                int l = 0;
                string str = string.Empty;
                DataTable dtDeptImpactlst = new DataTable();
                dtDeptImpactlst.Columns.Add("CCN_ID");
                dtDeptImpactlst.Columns.Add("AssigndToID");
                dtDeptImpactlst.Columns.Add("DeptID");
                dtDeptImpactlst.Columns.Add("BlockID");
                dtDeptImpactlst.Columns.Add("CurrentStatus");
                dtDeptImpactlst.Columns.Add("AssigndByID");
                dtDeptImpactlst.Columns.Add("Hod_SubmitDate");
                dtDeptImpactlst.Columns.Add("IsTemp");
                DataRow dr;
                int ISExistCount = 0;
                foreach (ChangeControlCommitte stu in students)
                {
                    Boolean Result = ToCheckValidEmployeeOrNot(stu.CCN_ID, 26);
                    if (Result == true)
                    {
                        if (stu.EmpID != 0)
                        {
                            int AssignedBYID = GetCookieEmployeID();
                            dr = dtDeptImpactlst.NewRow();
                            dr["CCN_ID"] = stu.CCN_ID;
                            dr["AssigndToID"] = stu.EmpID;
                            dr["DeptID"] = stu.DeptID;
                            dr["BlockID"] = blockid;
                            dr["CurrentStatus"] = 1;
                            dr["AssigndByID"] = AssignedBYID;
                            dr["Hod_SubmitDate"] = DateTime.Now.ToString("M/d/yyyy");
                            dr["IsTemp"] = 1;

                            dtDeptImpactlst.Rows.Add(dr);
                            l = l + 1;//Count
                            int DepartmentID = Convert.ToInt32(stu.DeptID);
                            var IsExistDeprtment = (from CC in dbEF.AizantIT_ChangeControlCommitte
                                                    where CC.CCN_ID == stu.CCN_ID && CC.BlockID == blockid && CC.DeptID == DepartmentID
                                                    select CC.Assessment_ID).Count();
                            if (IsExistDeprtment != 0)
                            {
                                ISExistCount += IsExistDeprtment;
                            }
                        }
                        else
                        { //ToGet Which Department HOD employee Not There
                            var departmentName = (from d in dbEF.AizantIT_DepartmentMaster
                                                  where d.DeptID.ToString() == stu.DeptID
                                                  select d.DepartmentName).SingleOrDefault();

                            str += "Select HOD's for This " + departmentName + " Department." + "<br/>";
                        }
                    }
                    else
                    {
                        return Json(new { hasError = true, data = "You are not authorized QA to do action." }, JsonRequestBehavior.AllowGet);
                    }
                }
                if (str != "")
                {
                    return Json(new { hasError = true, data = str }, JsonRequestBehavior.AllowGet);

                }
                else if (ISExistCount == 0)
                {
                    int count = Qms_bal.AddCCN_DepartmentBlockCommiteeBAL(dtDeptImpactlst);
                }
                else
                {
                    string ErrMsg = "Already you added this department and block for this Assessment.";
                    return Json(new { hasError = true, data = ErrMsg }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { hasError = false, data = l }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M12:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        //Edit Assigned Departments
        public PartialViewResult EditCCN_ChangeControlComiitte(int id)
        {
            try
            {
                AizantIT_ChangeControlCommitte objCCC = new AizantIT_ChangeControlCommitte();
                var capa1 = (from pd in dbEF.AizantIT_ChangeControlCommitte
                             where pd.Assessment_ID == id
                             select new
                             {
                                 pd.Assessment_ID,
                                 pd.Hod_SubmitDate,
                                 pd.AssigndByID,
                                 pd.AssigndToID,
                                 pd.DeptID,
                                 pd.CurrentStatus,

                                 pd.CCN_ID,
                                 pd.BlockID
                             }).ToList();
                foreach (var dr in capa1)
                {
                    objCCC.Assessment_ID = Convert.ToInt32(dr.Assessment_ID);
                    objCCC.AssigndByID = Convert.ToInt32(dr.AssigndByID);
                    objCCC.DeptID = Convert.ToInt32(dr.DeptID.ToString());
                    objCCC.BlockID = Convert.ToInt32(dr.BlockID.ToString());
                    objCCC.AssigndToID = Convert.ToInt32(dr.AssigndToID.ToString());
                    objCCC.CurrentStatus = Convert.ToInt32(dr.CurrentStatus.ToString());
                    objCCC.CCN_ID = Convert.ToInt32(dr.CCN_ID.ToString());

                }
                ViewBag.BlockIDEdit = new SelectList(dbEF.AizantIT_BlockMaster, "BlockID", "BlockName", objCCC.BlockID);
                ViewBag.DeptID = new SelectList((from IP in dbEF.AizantIT_ImpactDeptLink
                                                 join DM in dbEF.AizantIT_DepartmentMaster on IP.DeptID equals DM.DeptID
                                                 where IP.BlockID == objCCC.BlockID
                                                 select new
                                                 {
                                                     DeptID = IP.DeptID,
                                                     DeptName = DM.DepartmentName
                                                 }).Distinct(), "DeptID", "DeptName", objCCC.DeptID).ToList();
                int GetLoginEMPID = GetCookieEmployeID();
                //Login Employee id Not binding and Bind Hod's
                List<int> lgnEmployeeid = new List<int>() { GetLoginEMPID };
                objUMS_BAL = new UMS_BAL();
                List<SelectListItem> objAssignHod = new List<SelectListItem>();
                DataTable dtemp = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(objCCC.DeptID), (int)QMS_Roles.HOD, true); //25=hod

                foreach (DataRow result in dtemp.Rows)
                {
                    if (!lgnEmployeeid.Contains(Convert.ToInt32(result["EmpID"])))
                    {
                        objAssignHod.Add(new SelectListItem
                        {
                            Text = (result["DeptCode"] + " - " + (result["EmpName"])).ToString(),
                            Value = Convert.ToInt32(result["EmpID"]).ToString(),
                        });
                    }
                }
                ViewBag.EmployeeName1 = new SelectList(objAssignHod.ToList(), "Value", "Text", objCCC.AssigndToID);
                return PartialView(objCCC);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M13:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }
        //UPdate Assigned Departments
        [HttpPost]
        public JsonResult UpdateCCN_ChangeControlCommitte(AizantIT_ChangeControlCommitte aizant)
        {
            try
            {
                int CCN_ID = Convert.ToInt32(aizant.CCN_ID);
                int DeptID = Convert.ToInt32(aizant.DeptID);
                int BlockID = Convert.ToInt32(aizant.BlockID);
                int UniqueID = Convert.ToInt32(aizant.Assessment_ID);
                int AssignToID = Convert.ToInt32(aizant.AssigndToID);
                var Count = dbEF.AizantIT_ChangeControlCommitte.FirstOrDefault(p => p.BlockID == BlockID && p.DeptID == DeptID && p.CCN_ID == CCN_ID && p.Assessment_ID != UniqueID);
                if (Count == null)
                {
                    //Get User Employee id
                    int Empid = GetCookieEmployeID();
                    DataTable count = Qms_bal.AddCCN_CommitteBal(Empid, Convert.ToInt32(aizant.Assessment_ID), CCN_ID, aizant.BlockID, Convert.ToInt32(aizant.AssigndToID), Convert.ToInt32(aizant.DeptID), "Update", 0);
                    return Json(new { hasError = false, aizant }, JsonRequestBehavior.AllowGet);

                }
                return Json(new { hasError = true, data = "Already you are added Assessment for This Department and This Block" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M14:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        //Bind Assigned Departments
        [HttpGet]
        public JsonResult FillDept(int BlockID)
        {
            try
            {
                var EmpList = dbEF.AizantIT_ImpactDeptLink.Where(c => c.BlockID == BlockID && c.Currentstatus == true).Distinct().ToList();
                var Deptlist = (from s in dbEF.AizantIT_ImpactDeptLink.ToList()
                                where s.Currentstatus == true && s.BlockID == BlockID
                                select new
                                {
                                    DeptID = s.DeptID,

                                }).Distinct();
                int k = 0;
                List<SelectListItem> impList1 = new List<SelectListItem>();
                foreach (var q2 in Deptlist)
                {
                    var impactISExits = (from i in dbEF.AizantIT_ImpactDeptLink
                                         where i.DeptID == q2.DeptID && i.BlockID == BlockID
                                         select new { i.ImpPoint_ID }).ToList();
                    foreach (var item in impactISExits)
                    {
                        k = k + 1;
                    }
                    if (k > 0)
                    {
                        //bind Departments
                        var name = dbEF.AizantIT_DepartmentMaster.Where(c => c.DeptID == q2.DeptID).ToList().FirstOrDefault();
                        impList1.Add(new SelectListItem { Text = name.DepartmentName, Value = q2.DeptID.ToString() });
                    }
                }
                return Json(impList1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M15:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        public void DeleteTempAssignedDepartment(int? CCNid)
        {
            Boolean IsTempVal = Convert.ToBoolean(1);
            dbEF.AizantIT_ChangeControlCommitte.RemoveRange(dbEF.AizantIT_ChangeControlCommitte.Where(c => c.IsTemp == IsTempVal && c.CCN_ID == CCNid));
            dbEF.SaveChanges();
        }

        public ActionResult BindImpactHOD(int DeptID)
        {
            try
            {
                //Assigned HOD's
                objUMS_BAL = new UMS_BAL();
                List<SelectListItem> CCCobjAssignHod = new List<SelectListItem>();
                DataTable dtemp = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(Convert.ToInt32(DeptID), (int)QMS_Roles.HOD, true); //25=hod

                foreach (DataRow result in dtemp.Rows)
                {
                    CCCobjAssignHod.Add(new SelectListItem
                    {
                        Text = (result["DeptCode"] + " - " + (result["EmpName"])).ToString(),
                        Value = Convert.ToInt32(result["EmpID"]).ToString(),
                    });
                }
                return Json(CCCobjAssignHod, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M24:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region CloseCCN
        public PartialViewResult CreateCloseCCN(int id)
        {
            try
            {
                var RoleType = "Close";
                RefershCCNDeletedFilesIsTempIsFalse(id, RoleType);

                CCNClose cCN_objclose = new CCNClose();
                var CCNNO = dbEF.AizantIT_ChangeControlMaster.Find(id);
                cCN_objclose.CCN_Number = CCNNO.CCN_No;
                cCN_objclose.CCNID = id;
                cCN_objclose.ClosureDate =DateTime.Now.ToString("dd MMM yyyy");
                return PartialView(cCN_objclose);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M16:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }
        [HttpPost]
        public JsonResult AddCCNCloseDetails(CCNClose objccnclose)
        {
            try
            {
                AizantIT_CCNCloseFileAttachments objFile = new AizantIT_CCNCloseFileAttachments();
                objFile.CCNID = objccnclose.CCNID;
                //Get User Employee id
                objccnclose.QAEMpID = GetCookieEmployeID();
                Boolean Result = ToCheckValidEmployeeOrNot((int)objFile.CCNID, 26);
                if (Result == true)
                {


                    int dt = Qms_bal.QAAction_CCNCloserBal(objccnclose);
                }
                else
                {
                    return Json(new { msg = "You are not authorized QA to do action.", msgType = "error" }, JsonRequestBehavior.AllowGet);

                }
                return Json(new { msg = "", msgType = "success", }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M18:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { msg = ErMsg, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion




        #region CCNHodQAHQAPage
        //CCN View Page
        public PartialViewResult CCNDetails(int? id)
        {
            try
            {

                //BindActionPlanQEvent();
                CCNMaster objccn = new CCNMaster();
                AizantIT_ChangeControlMaster aizantIT_Change = dbEF.AizantIT_ChangeControlMaster.Find(id);


                objccn.CCN_ID = aizantIT_Change.CCN_ID;
                objccn.CCN_No = aizantIT_Change.CCN_No;
                objccn.DescriptionofProposedChanges = aizantIT_Change.DescriptionofProposedChanges;
                objccn.ReasonforChange = aizantIT_Change.ReasonforChange;
                objccn.JustificationforChange = aizantIT_Change.JustificationforChange;
                objccn.ExistingProcedure = aizantIT_Change.ExistingProcedure;
                objccn.CategoryofChange = Convert.ToInt32(aizantIT_Change.CategoryofChange);

                if (objccn.CategoryofChange == 2)
                {
                    AizanIT_CCNTemp_Applicable objtempfields = dbEF.AizanIT_CCNTemp_Applicable.Where(a => a.CCN_ID == id).SingleOrDefault();

                    objccn.ApplicableBatch = objtempfields.ApplicableBatch;

                    objccn.ApplicableTimeline = (objtempfields.ApplicableTimeline == null ? "N/A" : Convert.ToDateTime(objtempfields.ApplicableTimeline).ToString("dd MMM yyyy"));
                    objccn.ApplicableEquipment = objtempfields.ApplicableEquipment;
                }
                var _Date = (from C in dbEF.AizantIT_ChangeControlMaster
                             where C.CCN_ID == objccn.CCN_ID
                             select new { C.CCN_Date }).ToList();
                foreach (var item in _Date)
                {
                    objccn.CCN_Date = Convert.ToDateTime(item.CCN_Date).ToString("dd MMM yyyy");
                }
                if (aizantIT_Change.Market_ID != 0)
                {
                    objccn.MarketName = (from M in dbEF.AizantIT_Market
                                         where M.MarketID == aizantIT_Change.Market_ID
                                         select M.MarketName).SingleOrDefault();
                }
                else
                {
                    objccn.MarketName = "N/A";
                }
                if (aizantIT_Change.ProjectID != 0)
                {
                    objccn.ProjectNumber = (from p in dbEF.AizantIT_Projects
                                            where p.ProjectID == aizantIT_Change.ProjectID
                                            select p.ProjectNumber).SingleOrDefault();
                }
                else
                {
                    objccn.ProjectNumber = "N/A";
                }
                if (aizantIT_Change.ItemID != 0)
                {
                    objccn.ItemCode = (from I in dbEF.AizantIT_ItemMaster
                                       where I.ItemID == aizantIT_Change.ItemID
                                       select I.ItemCode).SingleOrDefault();
                }
                else
                {
                    objccn.ItemCode = "N/A";
                }
                objccn.TypeChange = (from T in dbEF.AizantIT_TypeChangeMaster
                                     where T.TC_ID == aizantIT_Change.TC_ID
                                     select T.TypeofChange).SingleOrDefault();
                objccn.DepartmentName = (from D in dbEF.AizantIT_DepartmentMaster
                                         where D.DeptID == aizantIT_Change.DeptID
                                         select D.DepartmentName).SingleOrDefault();
                if (aizantIT_Change.CategoryofChange == 1)
                {
                    objccn.CategoryofChangeName = "Permanent";
                }
                else
                {
                    objccn.CategoryofChangeName = "Temporary";
                }
                DocumentDetialsClass objDocument = new DocumentDetialsClass();
                QMSCommonActions CommonDocBo = new QMSCommonActions();
                if (aizantIT_Change.DocumentTypeID != 0 && aizantIT_Change.DocumentTypeID != -1)//Not in others and Select 
                {
                    objDocument = CommonDocBo.GetDocumentDetails(aizantIT_Change.CCN_ID, 2);
                        objccn.DocName = objDocument.DocumentName;
                        objccn.ReferenceNumber = objDocument.DocumentNumber;
                        objccn.DocumentTypeName = objDocument.DocumentType;
                }
                else if (aizantIT_Change.DocumentTypeID == 0)
                {
                    objDocument = CommonDocBo.GetOtherDocumentDetails(aizantIT_Change.CCN_ID, 2);
                        objccn.DocName = objDocument.DocumentName;
                        objccn.ReferenceNumber = objDocument.DocumentNumber;
                        objccn.DocumentTypeName = "Others";
                }

                else
                {
                    objccn.ReferenceNumber = "N/A";
                    objccn.DocName = "N/A";
                    objccn.DocumentTypeName = "N/A";

                }
                return PartialView(objccn);
            }
            catch (Exception ex)
            {
                ViewBag.Exception = HelpClass.SQLEscapeString(ex.Message) + ".";
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }

        //Retrieve the CCN DATA
        public CCNMaster GetCCNdata(int? id)
        {
            CCNMaster CCNMasterBO = new CCNMaster();
            var Items = (from CM in dbEF.AizantIT_ChangeControlMaster
                         join D in dbEF.AizantIT_DepartmentMaster on CM.DeptID equals D.DeptID
                         join TC in dbEF.AizantIT_TypeChangeMaster on CM.TC_ID equals TC.TC_ID
                         join Temp in dbEF.AizanIT_CCNTemp_Applicable on CM.CCN_ID equals Temp.CCN_ID into CMDLeftjoin
                         from Temp in CMDLeftjoin.DefaultIfEmpty()
                         join M in dbEF.AizantIT_Market on CM.Market_ID equals M.MarketID into CMDTCM
                         from M in CMDTCM.DefaultIfEmpty()
                         join I in dbEF.AizantIT_ItemMaster on CM.ItemID equals I.ItemID into CMDTCM1
                         from I in CMDTCM1.DefaultIfEmpty()
                         join P in dbEF.AizantIT_Projects on CM.ProjectID equals P.ProjectID into CMDTCM2
                         from P in CMDTCM2.DefaultIfEmpty()
                         where CM.CCN_ID == id
                         select new
                         {
                             CM.CCN_ID,
                             CM.DeptID,
                             CM.CCN_Date,
                             CM.CCN_No,
                             CM.TC_ID,
                             CM.ItemID,
                             CM.Market_ID,
                             CM.ProjectID,
                             CM.CategoryofChange,
                             CM.DescriptionofProposedChanges,
                             CM.ReasonforChange,
                             CM.JustificationforChange,
                             CM.ExistingProcedure,
                             CM.CurrentStatus,
                             CM.ChangeClassification,
                             CM.Initiator_EmpID,
                             CM.Initiator_SubmitedDate,
                             CM.HOD_EmpID,
                             CM.HeadQA_EmpID,
                             CM.DocumentTypeID,
                             D.DepartmentName,
                             TC.TypeofChange,
                             CM.QA_EmpID,
                             CM.DueDate,
                             ProjectNumber = (P.ProjectNumber == null ? "N/A" : P.ProjectNumber),
                             ItemCode = (I.ItemCode == null ? "N/A" : I.ItemCode),
                             ItemName = (I.ItemName == null ? "N/A" : I.ItemName),
                             MarketName = (M.MarketName == null ? "N/A" : M.MarketName),
                             Temp.ApplicableBatch,
                             Temp.ApplicableEquipment,
                             Temp.ApplicableTimeline,
                         }
                       ).ToList();
            foreach (var item in Items)
            {
                CCNMasterBO.Initiator_EmpID = (int)item.Initiator_EmpID;
                CCNMasterBO.DeptID = (int)item.DeptID;
                CCNMasterBO.CCN_ID = item.CCN_ID;
                CCNMasterBO.CCN_Date = Convert.ToDateTime(item.CCN_Date).ToString("dd MMM yyyy");
                CCNMasterBO.DepartmentName = item.DepartmentName;
                CCNMasterBO.CCN_No = item.CCN_No;
                CCNMasterBO.TypeChange = item.TypeofChange;
                CCNMasterBO.MarketName = item.MarketName;
                CCNMasterBO.ItemID = Convert.ToInt32(item.ItemID);
                CCNMasterBO.ItemCode = item.ItemCode;
                CCNMasterBO.ItemName = item.ItemName;
                CCNMasterBO.TC_ID = (int)item.TC_ID;
                CCNMasterBO.marketID = (int)item.Market_ID;
                CCNMasterBO.ProjectID = (int)item.ProjectID;
                CCNMasterBO.ProjectNumber = item.ProjectNumber;
                CCNMasterBO.CategoryofChange = Convert.ToInt32(item.CategoryofChange);
                CCNMasterBO.ReasonforChange = item.ReasonforChange;
                CCNMasterBO.JustificationforChange = item.JustificationforChange;
                CCNMasterBO.ExistingProcedure = item.ExistingProcedure;
                CCNMasterBO.DescriptionofProposedChanges = item.DescriptionofProposedChanges;
                CCNMasterBO.CurrentStatus = (int)item.CurrentStatus;
                CCNMasterBO.HOD_EmpID = (int)item.HOD_EmpID;
                CCNMasterBO.DueDate = (item.DueDate == null ? "N/A" : Convert.ToDateTime(item.DueDate).ToString("dd MMM yyyy"));
                CCNMasterBO.ApplicableBatch = item.ApplicableBatch;
                CCNMasterBO.ApplicableEquipment = item.ApplicableEquipment;
                CCNMasterBO.ApplicableTimeline = (item.ApplicableTimeline == null ? "N/A" : Convert.ToDateTime(item.ApplicableTimeline).ToString("dd MMM yyyy"));
                if (CCNMasterBO.CurrentStatus != 1)
                {
                    CCNMasterBO.QA_EmpID = item.QA_EmpID == null ? 0 : (int)item.QA_EmpID;
                }
                CCNMasterBO.DocumentType = (int)item.DocumentTypeID;

                if (new[] { 15, 26, 25, 24, 29, 8, 30, 10 }.Contains(CCNMasterBO.CurrentStatus))
                {
                    CCNMasterBO.ChangeClassification = (item.ChangeClassification).ToString();
                }
                //Head_QA Revert to QA
                List<int> HeadQaDropdown = new List<int> { 25, 29, 15, 8, 30, 10, 24 };//10 Headqa rejected
                if (HeadQaDropdown.Contains(CCNMasterBO.CurrentStatus))
                {
                    CCNMasterBO.HeadQA_EmpID = (int)item.HeadQA_EmpID;
                }
                DocumentDetialsClass objDocument = new DocumentDetialsClass();
                QMSCommonActions CommonDocBo = new QMSCommonActions();

                if (CCNMasterBO.DocumentType != 0 && CCNMasterBO.DocumentType != -1)//Not in others and Select 
                {
                    objDocument= CommonDocBo.GetDocumentDetails(CCNMasterBO.CCN_ID, 2);

                    CCNMasterBO.ReferenceNumber = objDocument.DocumentNumber;
                    CCNMasterBO.DocTitle = objDocument.DocumentName;
                    CCNMasterBO.VersionID = objDocument.VersionID.ToString();
                    CCNMasterBO.DocumentTypeName = objDocument.DocumentType;
                }
                else if (CCNMasterBO.DocumentType == 0)//Others
                {
                    objDocument = CommonDocBo.GetOtherDocumentDetails(CCNMasterBO.CCN_ID, 2);
                        CCNMasterBO.ReferenceNumber = objDocument.DocumentNumber;
                        CCNMasterBO.DocTitle = objDocument.DocumentName;
                        CCNMasterBO.DocumentTypeName = "Others";
                }
                else
                {
                    CCNMasterBO.ReferenceNumber = "N/A";
                    CCNMasterBO.DocTitle = "N/A";
                    CCNMasterBO.DocumentTypeName = "N/A";
                }
                //whome to Revert popup binding
                var QaEmpName = (from objccn in dbEF.AizantIT_ChangeControlMaster
                                 join objemp in dbEF.AizantIT_EmpMaster on objccn.QA_EmpID equals objemp.EmpID
                                 where objccn.CCN_ID == CCNMasterBO.CCN_ID
                                 select new { fullName = (objemp.FirstName + "" + objemp.LastName) }).Take(1);
                foreach (var EmpItem in QaEmpName)
                {
                    ViewBag.QaEmployeeName = "QA ( " + EmpItem.fullName + " )";
                }
            }
            return CCNMasterBO;
        }

        public void FileUploadsExistorNotInInitator(int? id)
        {
            ViewBag.CCNInitiatorFileExistorNot = 0;

            ViewBag.CCNInitiatorFileExistorNot = (from CInitator in dbEF.AizantIT_CCNInitiatorFileAttachment
                                                  where CInitator.CCNID == id
                                                  select CInitator).Count();
        }
        //CCN Hod ,QA and HeadQa Pages
        public ActionResult CCNHODApprove(int? id, int? BlockID=0, int? AssessmentID=0, int ShowType = 0, int Notification_ID = 0)
        {
            ViewBag.Exception = null;
            try
            {
                if (Notification_ID != 0)
                {
                    TempData["QueryString"] = ShowType;
                    TempData.Keep("QueryString");
                    int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                    GetNotificationUpdate(Notification_ID, EmpIDs);
                    if (ShowType == 7 || ShowType == 8)
                    {
                        AssesmentBlock_IDs AssesBlockID = new AssesmentBlock_IDs();
                        AssesBlockID = GetAssessmentID(Notification_ID);
                        BlockID = AssesBlockID.BlockID;
                        AssessmentID = AssesBlockID.AssessmentID;
                    }

                }

                FileUploadsExistorNotInInitator(id);

                //Hod Assessment upload file removed when Is temp files is false
                ViewBag.FilesCountHOD = -1; //Default stored
                if (AssessmentID != 0)
                {
                    GetHodAssFileUploadCount((int)AssessmentID);
                    var RoleType = "HOD";
                    RefershCCNDeletedFilesIsTempIsFalse((int)AssessmentID, RoleType);
                }
                //Qa Assigned Assessment to Hod deleted temporary files
                DeleteTempAssignedDepartment(id);

                //Count No of Assessments Completed or not
                AssessmentsCompletedOrNot(id);

                //Deleted Temporary Capa's
                DeleteTempCapas((int)AssessmentID);

                TempData["AssessmentIDFromList"] = AssessmentID;
                TempData.Keep("AssessmentIDFromList");
                //TempData.Keep("GetCCNCount");

                int Role = Convert.ToInt32((TempData["QueryString"]));
                TempData.Keep("QueryString");

                CCNMaster CCNMasterBO = new CCNMaster();
                //Retrieve ccn Details
                CCNMasterBO = GetCCNdata(id);

                CCNMasterBO.AssessmentId = (int)AssessmentID;

                //get Employee id
                GetUserEmployeID(CCNMasterBO);

                //Roles checking
                CheckRoles(Role);

                //if (BlockID == null)
                //{
                //    TempData["Block"] = 0;
                //}
                //else
                //{
                //    //Session["Block"] = BlockID;
                //    TempData["Block"] = BlockID;
                //    TempData.Keep("Block");
                //}
                if (CCNMasterBO.CategoryofChange == 1)
                {
                    CCNMasterBO.CategoryofChangeName = "Permanent";
                }
                else if (CCNMasterBO.CategoryofChange == 2)
                {
                    CCNMasterBO.CategoryofChangeName = "Temporary";
                }
                //Bind Drop downs QA and HeadQA
                CCNDropdownbindings(CCNMasterBO);

                var Assesmentsumary = (from pd in dbEF.AizantIT_CCNHistory where pd.CCNID == id && pd.ActionStatus == 15 || pd.ActionStatus == 24 || pd.ActionStatus == 26 || pd.ActionStatus == 29 select new { pd.Comments }).ToList().LastOrDefault();
                if (Assesmentsumary != null) { ViewBag.QAAssess_SumaryComments = Assesmentsumary.Comments; } else { ViewBag.QAAssess_SumaryComments = ""; }
                var CloseingCCN = (from pd in dbEF.AizantIT_CCNHistory where pd.CCNID == id && pd.ActionStatus == 11 select new { pd.Comments }).ToList().LastOrDefault();
                if (CloseingCCN != null) { ViewBag.CloseingCCN = CloseingCCN.Comments; } else { ViewBag.CloseingCCN = ""; }

                #region ImpactPoints

                if (AssessmentID != 0 && BlockID != 0)//Only Assessment HOD Login
                {
                    List<ImpactPoints> Emp = new List<ImpactPoints>();
                    //Get User Employee id
                    int empid = GetCookieEmployeID();

                    //Get Deptid for NEW Assessments
                    var ImpDeptidNew = (from pd in dbEF.AizantIT_ChangeControlCommitte
                                        where pd.CCN_ID == CCNMasterBO.CCN_ID &&
                    (pd.CurrentStatus == 7) && pd.AssigndToID == empid && pd.BlockID == BlockID && pd.Assessment_ID == AssessmentID
                                        select new { pd.DeptID }).OrderByDescending(c => c.DeptID).FirstOrDefault();

                    if (ImpDeptidNew != null)
                    {
                        TempData["ImpcatDeptEmpty"] = ImpDeptidNew.DeptID;
                    }
                    //Get Deptid forExisting Assessments
                    var ImpDeptidExisting = (from pd in dbEF.AizantIT_ChangeControlCommitte
                                             where pd.CCN_ID == CCNMasterBO.CCN_ID && (pd.CurrentStatus == 3 || pd.CurrentStatus == 9) && pd.AssigndToID == empid && pd.BlockID == BlockID && pd.Assessment_ID == AssessmentID
                                             select new { pd.DeptID }).OrderByDescending(c => c.DeptID).FirstOrDefault();
                    if (ImpDeptidExisting != null)
                    {
                        TempData["ImpcatDeptEmpty"] = ImpDeptidExisting.DeptID;
                    }
                    //Get Deptid for New Block
                    var ImpblockidNew = (from pd in dbEF.AizantIT_ChangeControlCommitte
                                         where pd.CCN_ID == CCNMasterBO.CCN_ID && (pd.CurrentStatus == 7) && pd.AssigndToID == empid && pd.BlockID == BlockID && pd.Assessment_ID == AssessmentID
                                         select new { pd.BlockID }).OrderByDescending(c => c.BlockID).FirstOrDefault();
                    if (ImpblockidNew != null)
                    {
                        TempData["ImpcatBlockEmpty"] = ImpblockidNew.BlockID;
                    }
                    //Get Deptid for Existing Block
                    var ImpblockidExisting = (from pd in dbEF.AizantIT_ChangeControlCommitte
                                              where pd.CCN_ID == CCNMasterBO.CCN_ID && (pd.CurrentStatus == 3 || pd.CurrentStatus == 9) && pd.AssigndToID == empid && pd.BlockID == BlockID && pd.Assessment_ID == AssessmentID
                                              select new { pd.BlockID }).OrderByDescending(c => c.BlockID).FirstOrDefault();
                    if (ImpblockidExisting != null)
                    {
                        TempData["ImpcatBlockEmpty"] = ImpblockidExisting.BlockID;
                    }
                    if (ImpDeptidNew != null && ImpblockidNew != null)
                    {

                        var s = (from pd in dbEF.AizantIT_ImpactPoints
                                 join od in dbEF.AizantIT_ImpactDeptLink on pd.Imp_Desc_ID equals od.Imp_Desc_ID
                                 where od.DeptID == (ImpDeptidNew.DeptID) && od.BlockID == (ImpblockidNew.BlockID) && od.Currentstatus == true
                                 select new { pd.Impact_Description, od.ImpPoint_ID }).OrderBy(c => c.Impact_Description).ToList();
                        foreach (var dr in s)
                        {
                            Emp.Add(new QMS_BO.QMS_ChangeControl_BO.ImpactPoints
                            {
                                ImpactName = dr.Impact_Description.ToString(),
                                ImpactPointID = Convert.ToInt32(dr.ImpPoint_ID.ToString()),
                                ImpactPointStatus = 2,//new impactpoints
                            });
                        }
                        var Other = (from I in dbEF.AizantIT_ImpactPoints
                                     where I.Imp_Desc_ID == 0
                                     select new { I.Impact_Description, I.Imp_Desc_ID, }).ToList();
                        foreach (var item in Other)
                        {
                            Emp.Add(new QMS_BO.QMS_ChangeControl_BO.ImpactPoints
                            {
                                ImpactName = item.Impact_Description.ToString(),
                                ImpactPointID = Convert.ToInt32(item.Imp_Desc_ID.ToString()),
                                ImpactPointStatus = 2,//new impactpoints
                            });

                        }
                        ViewBag.Impact = Emp.AsEnumerable();
                        ViewBag.YESNOBind = new List<SelectListItem>{ new SelectListItem{
                 Text="No",
                Value = "0"
            },
            new SelectListItem{

                Text="Yes",
                Value = "1"
            }};
                    }
                    else if (ImpDeptidExisting != null && ImpblockidExisting != null)
                    {


                        var s = (from od in dbEF.AizantIT_ImpactAssessmentTransaction
                                 join DepLnk in dbEF.AizantIT_ImpactDeptLink on od.ImpPoint_ID equals DepLnk.ImpPoint_ID
                                 join pd in dbEF.AizantIT_ImpactPoints on DepLnk.Imp_Desc_ID equals pd.Imp_Desc_ID
                                 where od.CCN_ID == CCNMasterBO.CCN_ID && od.AssessmentId == AssessmentID && DepLnk.Currentstatus == true
                                 select new { pd.Impact_Description, od.ImpPoint_ID, od.isYesorNo, od.DocTitleorRefN, od.Remarks }).OrderBy(c => c.Impact_Description).ToList();

                        foreach (var dr in s)
                        {
                            Emp.Add(new QMS_BO.QMS_ChangeControl_BO.ImpactPoints
                            {
                                ImpactName = dr.Impact_Description.ToString(),
                                ImpactPointID = Convert.ToInt32(dr.ImpPoint_ID.ToString()),
                                DocNo = dr.DocTitleorRefN.ToString(),
                                Comments = dr.Remarks.ToString(),
                                isyesorNo = dr.isYesorNo.ToString(),
                                ImpactPointStatus = 1,//edit impactpoints
                            }

                            );
                        }
                        var GetOthersValues = (from A in dbEF.AizantIT_ImpactAssessmentTransaction
                                               join pd in dbEF.AizantIT_ImpactPoints on A.ImpPoint_ID equals pd.Imp_Desc_ID
                                               where A.CCN_ID == CCNMasterBO.CCN_ID && A.AssessmentId == AssessmentID && A.ImpPoint_ID == 0
                                               select new { pd.Impact_Description, A.ImpPoint_ID, A.isYesorNo, A.DocTitleorRefN, A.Remarks }).ToList();
                        foreach (var dr in GetOthersValues)
                        {
                            Emp.Add(new QMS_BO.QMS_ChangeControl_BO.ImpactPoints
                            {
                                ImpactName = dr.Impact_Description.ToString(),
                                ImpactPointID = Convert.ToInt32(dr.ImpPoint_ID.ToString()),
                                DocNo = dr.DocTitleorRefN.ToString(),
                                Comments = dr.Remarks.ToString(),
                                isyesorNo = dr.isYesorNo.ToString(),
                                ImpactPointStatus = 1,//edit impactpoints
                            }

                            );
                        }

                        ViewBag.Impact = Emp.AsEnumerable();
                        // Check Impact point Status for this table "AizantIT_ChangeControlCommitte" 
                        var ActionAndImpactStatus = (from S in dbEF.AizantIT_ChangeControlCommitte
                                                     where S.CCN_ID == CCNMasterBO.CCN_ID && S.AssigndToID == CCNMasterBO.EMPID && S.BlockID == ImpblockidExisting.BlockID && S.DeptID == ImpDeptidExisting.DeptID
                                                     select new
                                                     {
                                                         S.CurrentStatus
                                                     }).ToList();
                        foreach (var item in ActionAndImpactStatus)
                        {
                            CCNMasterBO.ActionAndImpactStatus = (int)item.CurrentStatus;
                        }
                    }
                    else
                    {
                        ViewBag.Impact = Enumerable.Empty<string>();

                    }

                    //ViewBag.Actionplan = Enumerable.Empty<string>();
                    //int k = 0;
                    //var AssessmentStatus = (from CM in dbEF.AizantIT_ChangeControlCommitte
                    //                        where CM.CCN_ID == CCNMasterBO.CCN_ID
                    //                        select new { CM.CurrentStatus }).ToList();
                    //foreach (var item in AssessmentStatus)
                    //{
                    //    var Status = (int)item.CurrentStatus;
                    //    if (Status == 1)
                    //    {
                    //        k = k + 1;
                    //    }

                    //}

                    var AssignedDeptID = (from CC in dbEF.AizantIT_ChangeControlCommitte
                                          join DM in dbEF.AizantIT_DepartmentMaster on CC.DeptID equals DM.DeptID
                                          join B in dbEF.AizantIT_BlockMaster on CC.BlockID equals B.BlockID
                                          where CC.Assessment_ID == AssessmentID && CC.BlockID == BlockID
                                          select new
                                          {
                                              DM.DepartmentName,
                                              B.BlockName
                                          }).ToList();
                    foreach (var item in AssignedDeptID)
                    {
                        ViewData["AssignedDeptName"] = item.DepartmentName;
                        ViewData["AssigneDBlockName"] = item.BlockName;

                    }
                }
                else
                {
                    ViewBag.Impact = Enumerable.Empty<string>();
                }

                #endregion

                ALL_Comments(CCNMasterBO);
                return View("CCNHODApprove", CCNMasterBO);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M19:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return View();
            }
        }
        public void ALL_Comments(CCNMaster CCNBO)
        {
            int ActionStatus = 0;
            if (CCNBO.CurrentStatus == 5)
            {
                if (CCNBO.ActionAndImpactStatus == 3)
                {
                    ActionStatus = 19;
                }
                else
                {
                    ActionStatus = 5;
                }
            }
            else
            {
                ActionStatus = CCNBO.CurrentStatus;
            }

            var Comments = (from QR in dbEF.AizantIT_CCNHistory
                            where QR.CCNID == CCNBO.CCN_ID && QR.ActionStatus == ActionStatus
                            select new { QR.Comments }).ToList();

            foreach (var C1 in Comments)
            {

                if (CCNBO.CurrentStatus == 4 || CCNBO.CurrentStatus == 13 || CCNBO.CurrentStatus == 10)
                {
                    CCNBO.HOD_Comments = C1.Comments;
                }
                if (CCNBO.CurrentStatus == 2)
                {
                    if (C1.Comments != null && C1.Comments != "")
                    {
                        CCNBO.HOD_Comments = C1.Comments;
                    }
                    else
                    {
                        CCNBO.HOD_Comments = "N/A";
                    }
                }
                if (CCNBO.CurrentStatus == 25 || CCNBO.CurrentStatus == 30 || CCNBO.CurrentStatus == 8)
                {
                    CCNBO.HEADQA_Comments = C1.Comments;
                }
                //Revert list page HQA and hod reverted to initiator
                if (new[] { 3, 9, 6 }.Contains(CCNBO.CurrentStatus))
                {
                    if (C1.Comments != null)
                    {
                        CCNBO.Comments = C1.Comments;
                    }
                    else
                    {
                        CCNBO.Comments = "N/A";
                    }
                }
                if (new[] { 1, 12 }.Contains(CCNBO.CurrentStatus))
                {
                    if (C1.Comments != null)
                    {
                        CCNBO.Initiator_Comments = C1.Comments;
                    }
                    else
                    {
                        CCNBO.Initiator_Comments = "N/A";
                    }

                }
                if (CCNBO.CurrentStatus == 5)
                {
                    CCNBO.QA_Comments = C1.Comments;
                }
            }
        }

        #endregion

        #region MastersRefresh

        [HttpGet]
        public JsonResult RefreshTypeChange()//for Refresh Dropdown RefreshItem
        {
            try
            {
                var TypeChange1 = (from T in dbEF.AizantIT_TypeChangeMaster
                                   where T.Currentstatus == true
                                   select new { T.TC_ID, T.TypeofChange }).ToList();
                List<SelectListItem> TypeChangeList = new List<SelectListItem>();
                TypeChangeList.Add(new SelectListItem { Text = "select ", Value = "0" });
                foreach (var q1 in TypeChange1)
                {
                    TypeChangeList.Add(new SelectListItem { Text = q1.TypeofChange, Value = q1.TC_ID.ToString() });
                }
                return Json(TypeChangeList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M21:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RefreshItem()//for Refresh Dropdown
        {
            try
            {
                var Items = (from I in dbEF.AizantIT_ItemMaster
                             select new { I.ItemID, I.ItemCode }).ToList();
                List<SelectListItem> ItemList = new List<SelectListItem>();
                ItemList.Add(new SelectListItem { Text = "select ", Value = "0" });
                foreach (var q1 in Items)
                {
                    ItemList.Add(new SelectListItem { Text = q1.ItemCode, Value = q1.ItemID.ToString() });
                }
                return Json(ItemList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M22:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RefreshProject()//for Refresh Dropdown
        {
            try
            {
                var Project1 = (from P in dbEF.AizantIT_Projects
                                select new { P.ProjectID, P.ProjectNumber }).ToList();
                List<SelectListItem> projectList = new List<SelectListItem>();
                projectList.Add(new SelectListItem { Text = "select ", Value = "0" });
                foreach (var q1 in Project1)
                {
                    projectList.Add(new SelectListItem { Text = q1.ProjectNumber.ToString(), Value = q1.ProjectID.ToString() });
                }
                return Json(projectList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M23:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Json methods Data Exist or Not
        [HttpPost]
        public JsonResult CheckAssessmentPendingOrNot(CCNMaster CCNBO)
        {
            try
            {
                //Assessment buttons and comments box count 1= visible true. count =2 visible false
                var CheckCCNExist = (from CC in dbEF.AizantIT_ChangeControlCommitte
                                     where CC.CCN_ID == CCNBO.CCN_ID && CC.CurrentStatus == 1
                                     select CC).Count();
                var Count = 0;
                if (CheckCCNExist != 0)
                {   //Status is Pending
                    Count = 1;//Departments  Available Qa Can approve the assessment 
                }
                else
                {
                    //There is no pending
                    Count = 2;// There is no departments Not available
                }
                //Classification count 0 =visible false. count !=0 Visible true 
                List<int> Status = new List<int> { 4, 10 };
                var IsExistAssessment = (from CM in dbEF.AizantIT_ChangeControlCommitte
                                         where CM.CCN_ID == CCNBO.CCN_ID
                                         select CM).Count();
                //There is no Assessments.

                var CountAssessmentsStaus = "";
                if (IsExistAssessment == 0)
                {
                    //Assessment Not Created Still now
                    CountAssessmentsStaus = "NotCreatedAss";
                }
                else
                {
                    var CountAssessments = (from CM in dbEF.AizantIT_ChangeControlCommitte
                                            where CM.CCN_ID == CCNBO.CCN_ID && !Status.Contains((int)CM.CurrentStatus)
                                            select CM).Count();
                    CountAssessmentsStaus = CountAssessments.ToString();

                }
                return Json(new { hasError = false, Count, CountAssessmentsStaus }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M25:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        //Assessment Completed or not if Completed Classification Tab Displayed
        public int AssessmentsCompletedOrNot(int? CCNId)
        {
            List<int> Status = new List<int> { 4, 10 };
            ViewBag.CountAssessments = (from CM in dbEF.AizantIT_ChangeControlCommitte
                                        where CM.CCN_ID == CCNId && !Status.Contains((int)CM.CurrentStatus)
                                        select CM).Count();
            return ViewBag.CountAssessments;
        }

        public JsonResult IsExistAssessmentCompletedorNotAfterDelete(CCNMaster objccn)
        {
            try
            {
                List<int> Status = new List<int> { 4, 10 };
                var CountAssessmentsAfterDelete = (from CM in dbEF.AizantIT_ChangeControlCommitte
                                                   where CM.CCN_ID == objccn.CCN_ID && !Status.Contains((int)CM.CurrentStatus)
                                                   select CM).Count();

                return Json(new { hasError = false, data = CountAssessmentsAfterDelete }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M26:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult CheckAssessmentsApprovedOrNot(CCNMaster CCNBO)
        {
            try
            {
                DataTable dt1 = new DataTable();
                dt1 = Qms_bal.AddCCN_CommitteBal(0, 0, CCNBO.CCN_ID, 0, 2, 1, "CCN_count", CCNBO.AssessmentStaus);
                int Result = 0;
                if (dt1.Rows.Count > 0)
                {
                    Result = 1; //Not Completed Assessments
                }
                else
                {
                    Result = 2;//Completed Assessments
                }
                return Json(new { hasError = false, data = Result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M26:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }

        }
        //Approve Revert DataBase Actions  
        [HttpPost]
        public JsonResult ApproveRevertActions(CCNMaster CCNBO)
        {
            try
            {
                if (CCNBO.DatabaseSubmitStatus == "QAApprove")
                {
                    //CurrentStatus 6 means Qa Reverted To Initiator 
                    //7 Means Qa Rejected for CCN 
                    if (CCNBO.CurrentStatus == 6 || CCNBO.CurrentStatus == 7)
                    {
                        //Get User Employee id
                        CCNBO.QA_EmpID = GetCookieEmployeID();
                        Boolean Result = ToCheckValidEmployeeOrNot(CCNBO.CCN_ID, 26);
                        if (Result == true)
                        {
                            int dt = Qms_bal.QAActionCCNBal(CCNBO);
                        }
                        else
                        {
                            return Json(new { hasError = true, data = "You are not authorized QA to do action." }, JsonRequestBehavior.AllowGet);

                        }
                    }
                    //Current Status 5 Submit Assign Department
                    else
                    {
                        var CheckCCNExist = (from CC in dbEF.AizantIT_ChangeControlCommitte
                                             where CC.CCN_ID == CCNBO.CCN_ID && CC.CurrentStatus == 1
                                             select CC).Count();
                        if (CheckCCNExist != 0)
                        {
                            Boolean Result = ToCheckValidEmployeeOrNot(CCNBO.CCN_ID, 26);
                            if (Result == true)
                            {
                                //Get User Employee id
                                CCNBO.QA_EmpID = GetCookieEmployeID();
                                int dt = Qms_bal.QAActionCCNBal(CCNBO);
                            }
                            else
                            {
                                return Json(new { hasError = true, data = "You are not authorized QA to do action." }, JsonRequestBehavior.AllowGet);

                            }
                        }
                        else
                        {
                            return Json(new { hasError = true, data = "There is no departments. Add at least one Department." }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else if (CCNBO.DatabaseSubmitStatus == "HODApprove")
                {
                    //Get User Employee id
                    CCNBO.Initiator_EmpID = GetCookieEmployeID();
                    Boolean Result = ToCheckValidEmployeeOrNot(CCNBO.CCN_ID, 25);
                    if (Result == true)
                    {
                        int dt = Qms_bal.HODActionCCNBal(CCNBO);

                    }
                    else
                    {
                        return Json(new { hasError = true, data = "You are not authorized HOD to do action." }, JsonRequestBehavior.AllowGet);
                    }
                    //BindActionPlanQEvent();
                }
                else if (CCNBO.DatabaseSubmitStatus == "Assessment")
                {
                    Boolean Result = ToCheckValidEmployeeOrNot(CCNBO.CCN_ID, 26);
                    if (Result == true)
                    {
                        //All Assessment Complete or Not
                        DataTable dt1 = new DataTable();
                        dt1 = Qms_bal.AddCCN_CommitteBal(0, 0, CCNBO.CCN_ID, 0, 2, 1, "CCN_count", CCNBO.AssessmentStaus);
                        if (dt1.Rows.Count > 0)
                        {
                            return Json(new { hasError = true, data = "Approve All Assessments." }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            //Get User Employee id
                            CCNBO.QA_EmpID = GetCookieEmployeID();

                            int dt = Qms_bal.QAActionCCNBal(CCNBO);
                        }
                    }
                    else
                    {
                        return Json(new { hasError = true, data = "You are not authorized QA to do action." }, JsonRequestBehavior.AllowGet);
                    }
                }
                else if (CCNBO.DatabaseSubmitStatus == "HeadQASubmit")
                {

                    //Get User Employee id
                    CCNBO.HeadQA_EmpID = GetCookieEmployeID();
                    Boolean Result = ToCheckValidEmployeeOrNot(CCNBO.CCN_ID, 27);
                    if (Result == true)
                    {
                        int dt = Qms_bal.HeadQAActionCCNBal(CCNBO);
                    }
                    else
                    {
                        return Json(new { hasError = true, data = "You are not authorized HeadQA to do action." }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { hasError = false, data = CCNBO }, JsonRequestBehavior.AllowGet);
                }
                else if (CCNBO.DatabaseSubmitStatus == "AssessmentRevertHQA")
                {

                    //Get User Employee id
                    CCNBO.HeadQA_EmpID = GetCookieEmployeID();
                    Boolean Result = ToCheckValidEmployeeOrNot(CCNBO.CCN_ID, 27);
                    if (Result == true)
                    {
                        int dt = Qms_bal.AssessmentRevertHeadQABAL(CCNBO);
                    }
                    else
                    {
                        return Json(new { hasError = true, data = "You are not authorized HeadQA to do action." }, JsonRequestBehavior.AllowGet);

                    }
                }

                else if (CCNBO.DatabaseSubmitStatus == "AssessmentApproveQA")
                {
                    //Get User Employee id
                    CCNBO.EMPID = GetCookieEmployeID();
                    int GetStatus = CCNBO.CurrentStatus;
                    int Role = 0; string Text = "";
                    if (GetStatus == 4 || GetStatus == 3)
                    {
                        Role = 26;
                        Text = "QA";
                    }
                    else
                    {
                        Role = 27;
                        Text = "HEad QA";

                    }
                    Boolean Result = ToAssessmentCheckValidEmployeeOrNot(CCNBO.CCN_ID, CCNBO.AssessmentId, Role);
                    if (Result == true)
                    {
                        int dt = Qms_bal.AssessmenT_ActionsCCNBal(CCNBO);
                        var id = CCNBO.CCN_ID;
                        CCNBO.AssessmentCompletedOrNotStatus = AssessmentsCompletedOrNot(id);
                    }
                    else
                    {
                        return Json(new { hasError = true, data = "You are not authorized " + Text + " to do action." }, JsonRequestBehavior.AllowGet);

                    }
                    return Json(new { hasError = false, data = CCNBO }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { hasError = false, data = CCNBO }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M27:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        //Verifying Capa Completed or Not before click on CCN Close button
        [HttpPost]
        public JsonResult VerifiyingCapaBeforClosing(CCNMaster objCCN)
        {
            try
            {
                Boolean Result = ToCheckValidEmployeeOrNot(objCCN.CCN_ID, 26);
                if (Result == true)
                {
                    //21 CAPA Closed, 9 CAPA verified, 22 HeadQA Verified,10 CAPA Deleted
                    List<int> CurrentStatusExist = new List<int>() { 21, 9, 22, 10 };
                    //How Many Capa's are there in CCN
                    var CapaTotalCount = (from CM in dbEF.AizantIT_CAPAMaster
                                          join AP in dbEF.AizantIT_CAPA_ActionPlan on CM.CAPAID equals AP.CAPAID
                                          where CM.QualityEvent_Number == objCCN.CCN_No && AP.BaseID == objCCN.CCN_ID && CM.QualityEvent_TypeID == 2
                                          select new { AP.CAPAID }).Count();
                    var FinishedCount = (from CM in dbEF.AizantIT_CAPAMaster
                                         join AP in dbEF.AizantIT_CAPA_ActionPlan on CM.CAPAID equals AP.CAPAID
                                         where CM.QualityEvent_Number == objCCN.CCN_No
                                         && CurrentStatusExist.Contains((int)AP.CurrentStatus) && AP.BaseID == objCCN.CCN_ID && CM.QualityEvent_TypeID == 2
                                         select new { AP.CAPAID }).Count();
                    //check All Capa's are finished or not
                    var Count = 0;
                    if (CapaTotalCount == FinishedCount)
                    {
                        Count = 1;//Capa Completed
                    }
                    else
                    {
                        Count = 2;//Capa Pending
                    }
                    return Json(new { hasError = false, data = Count }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { hasError = true, data = "You are not authorized QA to do action." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M30:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);

            }
        }
        //Store tempData for Charts
        public JsonResult AssignedToTempDateaCCN(int DeptID, int RoleID, string FromDate, string ToDate, string CCNEvent)
        {
            try
            {
                TempData["CCNEvent"] = CCNEvent;
                TempData.Keep("CCNEvent");
                TempData["RoleIDFromCharts"] = RoleID;
                TempData.Keep("RoleIDFromCharts");
                TempData["DeptIDFromDashboard"] = DeptID;
                TempData.Keep("DeptIDFromDashboard");
                TempData["FromDateDashboard"] = FromDate == "" ? DateTime.Now.AddDays(-365).ToString("dd MMM yyyy") : FromDate;
                TempData.Keep("FromDateDashboard");
                TempData["ToDateDashboard"] = ToDate == "" ? DateTime.Now.ToString("dd MMM yyyy") : ToDate; ;
                TempData.Keep("ToDateDashboard");
                return Json(new { hasError = false, data = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M31:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);

            }
        }

        //HQA Can Refresh If Any  Assessments Reverted  
        [HttpPost]
        public JsonResult RefershAssessmentHQA(CCNMaster objccn)
        {
            try
            {
                // Get User Employee id
                objccn.HeadQA_EmpID = GetCookieEmployeID();
                int dt = Qms_bal.RefershAssessmentHeadQAPAgeBAL(objccn);
                return Json(new { hasError = false, data = objccn }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M34:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region MainList

        public ActionResult MainListCCN(int? id)
        {
            ViewBag.Exception = null;
            try
            {
                FileUploadsExistorNotInInitator(id);

                TempData.Keep("CCN_ID");
                TempData.Keep("QueryString");
                CCNMaster CCNMasterBO = new CCNMaster();
                //Retrieve ccn Details
                CCNMasterBO = GetCCNdata(id);
                //get Employee id
                GetUserEmployeID(CCNMasterBO);

                if (CCNMasterBO.CategoryofChange == 1)
                {
                    CCNMasterBO.CategoryofChangeName = "Permanent";
                }
                else if (CCNMasterBO.CategoryofChange == 2)
                {
                    CCNMasterBO.CategoryofChangeName = "Temporary";
                }

                //Get Headqa Employee Name
                ViewBag.headqaEmployeeName = (from E in dbEF.AizantIT_EmpMaster
                                              where E.EmpID == CCNMasterBO.HeadQA_EmpID
                                              select E.FirstName + "" + E.LastName).SingleOrDefault();

                var Assesmentsumary = (from pd in dbEF.AizantIT_CCNHistory
                                       where pd.CCNID == id && (pd.ActionStatus == 15 || pd.ActionStatus == 24 || pd.ActionStatus == 30 || pd.ActionStatus == 26)
                                       select new { pd.Comments }).ToList().LastOrDefault();
                if (Assesmentsumary != null) { ViewBag.QAAssess_SumaryComments = Assesmentsumary.Comments; } else { ViewBag.QAAssess_SumaryComments = ""; }

                var HEadQAAssesmentsumary = (from pd in dbEF.AizantIT_CCNHistory
                                             where pd.CCNID == id && (pd.ActionStatus == 8 || pd.ActionStatus == 10)
                                             select new { pd.Comments }).ToList().LastOrDefault();
                if (HEadQAAssesmentsumary != null) { CCNMasterBO.HEADQA_Comments = HEadQAAssesmentsumary.Comments; } else { CCNMasterBO.HEADQA_Comments = ""; }


                var ccnCloser = (from CM in dbEF.AizantIT_ChangeControlMaster
                                 join C in dbEF.AizantIT_CCNClose on CM.CCN_ID equals C.CCN_ID
                                 join CH in dbEF.AizantIT_CCNHistory on CM.CCN_ID equals CH.CCNID
                                 where CM.CCN_ID == CCNMasterBO.CCN_ID && CH.ActionStatus == 29
                                 select new
                                 {
                                     C.ClosureDate,
                                     CH.Comments
                                 }).ToList();
                foreach (var item in ccnCloser)
                {
                    ViewBag.CCNCloserEffectiveDate = Convert.ToDateTime(item.ClosureDate).ToString("dd MMM yyyy");
                    ViewBag.CCNClosercomments = item.Comments;
                }
                if (ccnCloser.Count <= 0)
                {
                    ViewBag.CCNCloserEffectiveDate = "N/A";
                    ViewBag.CCNClosercomments = "N/A";
                }

                // ALL_Comments(CCNMasterBO);
                return View("MainListCCN", CCNMasterBO);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M2:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return View();
            }
        }

        #endregion

        #region CCNReports

        #region Generate Report

        //Report Generate
        public void GenerateReport(int CCNID)
        {
            CCNMasterHeaderReport Rept1 = new CCNMasterHeaderReport();
            CCNAttachmentsReport attachFileRpt = new CCNAttachmentsReport();

            //Company Logo
            DataTable _dtCompany = fillCompany();
            if (_dtCompany.Rows.Count != 0)
            {
                if (!string.IsNullOrEmpty(_dtCompany.Rows[0]["CompanyLogo"].ToString()))
                {
                    ((XRPictureBox)(Rept1.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                    ((XRPictureBox)(attachFileRpt.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                }
            }
            else
            {
                throw new Exception("Please Upload Company Logo");
            }
            #region GetCCNDetailsForReports


            Models.QmsModel.CCNMasterReports objReport = new Models.QmsModel.CCNMasterReports();
            var Items = (from CM in dbEF.AizantIT_ChangeControlMaster
                         join D in dbEF.AizantIT_DepartmentMaster on CM.DeptID equals D.DeptID
                         join TC in dbEF.AizantIT_TypeChangeMaster on CM.TC_ID equals TC.TC_ID
                         join Temp in dbEF.AizanIT_CCNTemp_Applicable on CM.CCN_ID equals Temp.CCN_ID into CMDLeftjoin
                         from Temp in CMDLeftjoin.DefaultIfEmpty()
                         join M in dbEF.AizantIT_Market on CM.Market_ID equals M.MarketID into CMDTCM
                         from M in CMDTCM.DefaultIfEmpty()
                         join I in dbEF.AizantIT_ItemMaster on CM.ItemID equals I.ItemID into CMDTCM1
                         from I in CMDTCM1.DefaultIfEmpty()
                         join P in dbEF.AizantIT_Projects on CM.ProjectID equals P.ProjectID into CMDTCM2
                         from P in CMDTCM2.DefaultIfEmpty()
                         where CM.CCN_ID == CCNID
                         select new
                         {
                             CM.CCN_Date,
                             CM.CCN_No,
                             CM.CategoryofChange,
                             CM.DescriptionofProposedChanges,
                             CM.ReasonforChange,
                             CM.JustificationforChange,
                             CM.ExistingProcedure,
                             CM.CurrentStatus,
                             CM.ChangeClassification,
                             CM.DocumentTypeID,
                             D.DepartmentName,
                             TC.TypeofChange,
                             CM.DueDate,
                             ProjectNumber = (P.ProjectNumber == null ? "N/A" : P.ProjectNumber),
                             ItemCode = (I.ItemCode == null ? "N/A" : I.ItemCode),
                             ItemName = (I.ItemName == null ? "N/A" : I.ItemName),
                             MarketName = (M.MarketName == null ? "N/A" : M.MarketName),
                             Temp.ApplicableBatch,
                             Temp.ApplicableEquipment,
                             Temp.ApplicableTimeline,
                         }
                     ).ToList();
            foreach (var item in Items)
            {
                //For Report Header binding
                objReport.CCN_No_Label = ": " + item.CCN_No;
                objReport.Label_DepartmentName = ": " + item.DepartmentName;
                //end
                objReport.CCN_No = item.CCN_No;
                objReport.DepartmentName = item.DepartmentName;
                objReport.TypeOfChange = ": " + item.TypeofChange;
                objReport.CCN_Date = ": " + Convert.ToDateTime(item.CCN_Date).ToString("dd MMM yyyy");
                objReport.DescriptionofProposedChanges = item.DescriptionofProposedChanges;
                objReport.ReasonforChange = item.ReasonforChange;
                objReport.JustificationforChange = item.JustificationforChange;
                objReport.MarketName = ": " + item.MarketName;
                objReport.ExistingProcedure = item.ExistingProcedure;
                objReport.ItemCode = ": " + item.ItemCode;
                objReport.ProjectNumber = ": " + item.ProjectNumber;
                objReport.DocumentType = (int)item.DocumentTypeID;
                objReport.currentStatus = (int)item.CurrentStatus;
                objReport.DueDate = ": " + Convert.ToDateTime(item.DueDate).ToString("dd MMM yyyy");
                objReport.ApplicableBatch = item.ApplicableBatch;
                objReport.ApplicableEquipment = item.ApplicableEquipment;
                objReport.ApplicableTimeLine = ": " + Convert.ToDateTime(item.ApplicableTimeline).ToString("dd MMM yyyy");

                //For Binding Due Date
                if (objReport.DueDate == ": 01 Jan 0001")
                {
                    ((XRLabel)(Rept1.FindControl("xrTableCellDueDateValue", false))).Text = " : --NA--";
                }
                else
                {
                    ((XRLabel)(Rept1.FindControl("xrTableCellDueDateValue", false))).Text = objReport.DueDate;
                }

                if (item.CategoryofChange == 1)
                {
                    objReport.CategoryofChange = ": Permanent";
                }
                if (item.CategoryofChange == 2)
                {
                    objReport.CategoryofChange = ": Temporary";
                }

            }


            if (objReport.CategoryofChange == ": Temporary")
            {
                //For Binding Applicable Time Line
                if (objReport.ApplicableTimeLine == ": 01 Jan 0001")
                {
                    ((XRLabel)(Rept1.FindControl("xrTblCellAppTimeLineTxt", false))).Text = "--NA-- ";
                }
                else
                {
                    ((XRLabel)(Rept1.FindControl("xrTblCellAppTimeLineTxt", false))).Text = objReport.ApplicableTimeLine;
                }

                XRTable tblCCNHeaderReportTable = ((XRTable)(Rept1.FindControl("xrTblCCNHeader", true)));
                tblCCNHeaderReportTable.BeginInit();

                foreach (var item in Items)
                {
                    XRTableRow row1 = new XRTableRow();//row create
                    XRTableCell cell1 = new XRTableCell();
                    cell1.CanGrow = true;
                    cell1.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Bold);
                    cell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell1.Text = "Applicable Batch/es#/Lot/s#/Strength/s# :  ";
                    cell1.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right;
                    row1.Cells.Add(cell1);

                    XRTableRow row2 = new XRTableRow();//row create
                    XRTableCell cell2 = new XRTableCell();
                    cell2.CanGrow = true;
                    cell2.Width = 387;
                    cell2.Font = new System.Drawing.Font("Times New Roman", 10f, FontStyle.Regular);
                    cell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell2.Text = item.ApplicableBatch;
                    cell2.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                    row2.Cells.Add(cell2);

                    XRTableRow row3 = new XRTableRow();//row create
                    XRTableCell cell3 = new XRTableCell();
                    cell3.CanGrow = true;
                    cell3.Width = 380;
                    cell3.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Bold);
                    cell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell3.Text = "Applicable Equipment /Instrument /Area Name & ID :  ";
                    cell3.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right;
                    row3.Cells.Add(cell3);

                    XRTableRow row4 = new XRTableRow();//row create
                    XRTableCell cell4 = new XRTableCell();
                    cell4.CanGrow = true;
                    cell4.Width = 387;
                    cell4.Font = new System.Drawing.Font("Times New Roman", 10f, FontStyle.Regular);
                    cell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell4.Text = item.ApplicableEquipment;
                    cell4.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                    row4.Cells.Add(cell4);
                    //end
                    tblCCNHeaderReportTable.Rows.Add(row1);
                    tblCCNHeaderReportTable.Rows.Add(row2);
                    tblCCNHeaderReportTable.Rows.Add(row3);
                    tblCCNHeaderReportTable.Rows.Add(row4);
                }
                tblCCNHeaderReportTable.EndInit();
            }
            else
            {
                ((XRLabel)(Rept1.FindControl("xrTblCellAppTimeLineLbl", false))).Text = " ";
                ((XRLabel)(Rept1.FindControl("xrTblCellAppTimeLineTxt", false))).Text = " ";
            }
            //end
            DocumentDetialsClass objDocument = new DocumentDetialsClass();
            QMSCommonActions CommonDocBo = new QMSCommonActions();
            if (objReport.DocumentType != 0 && objReport.DocumentType != -1)//Not others and Select 
            {
                objDocument = CommonDocBo.GetDocumentDetails(CCNID, 2);
                objReport.DocumentName = ": " + objDocument.DocumentName;
                objReport.DocumentNumber = ": " + objDocument.DocumentNumber;
                objReport.DocumentTypeName = ": " + objDocument.DocumentType;
            }
            else if (objReport.DocumentType == 0)//Others
            {
                objDocument = CommonDocBo.GetOtherDocumentDetails(CCNID, 2);
                    objReport.DocumentNumber = ": " + objDocument.DocumentNumber;
                    objReport.DocumentName = ": " + objDocument.DocumentName;
                    objReport.DocumentTypeName = ": Others";
            }
            else //Only Select 
            {
                objReport.DocumentNumber = ": -NA-";
                objReport.DocumentName = ": -NA-";
                objReport.DocumentTypeName = ": -NA-";

            }
            // Get Initiator Names
            var InitiatorName = (from CH in dbEF.AizantIT_CCNHistory
                                 join Em in dbEF.AizantIT_EmpMaster on CH.ActionBy equals Em.EmpID
                                 join Desg in dbEF.AizantIT_Designation on Em.CurrentDesignation equals Desg.DesignationID
                                 where CH.ActionStatus == 1 && CH.CCNID == CCNID
                                 select new { CH.ActionDate, Desg.DesignationName, fulName = (Em.FirstName + "" + Em.LastName) }).SingleOrDefault();
            objReport.InitiatorSubmitDate = Convert.ToDateTime(InitiatorName.ActionDate).ToString("dd MMM yyyy HH: mm:ss");
            objReport.InitiatorName = InitiatorName.fulName;
            objReport.InitiatorDesignation = "(" + InitiatorName.DesignationName + ")";

            #region HODCommentsAndE-sign
            List<int> objCurrentStatus = new List<int>() { 1, 12, 3, 4 };
            if (objCurrentStatus.Contains(objReport.currentStatus))
            {
                XRLabel lblhodCommentslabel = ((XRLabel)(Rept1.FindControl("xrlblHodLable", true)));
                XRLabel lblhodCommentsbox = ((XRLabel)(Rept1.FindControl("xrlblHodComments", true)));
                XRTable tblsignaturehod = ((XRTable)(Rept1.FindControl("xrtblHodSign", true)));
                XRLabel lblhoddesignation = ((XRLabel)(Rept1.FindControl("xrlblhoddesignation", true)));
                lblhodCommentslabel.Visible = false;
                lblhodCommentsbox.Visible = false;
                tblsignaturehod.Visible = false;
                lblhoddesignation.Visible = false;
            }
            else
            {
                //Get HOD Names 
                var HODName = (from CH in dbEF.AizantIT_CCNHistory
                               join Em in dbEF.AizantIT_EmpMaster on CH.ActionBy equals Em.EmpID
                               join Desg in dbEF.AizantIT_Designation on Em.CurrentDesignation equals Desg.DesignationID
                               where CH.ActionStatus == 2 && CH.CCNID == CCNID
                               select new { CH.ActionDate, Desg.DesignationName, Em.CurrentDesignation, CH.Comments, fulName = (Em.FirstName + "" + Em.LastName) }).ToList();
                foreach (var item in HODName)
                {
                    objReport.HODSubmitDate = Convert.ToDateTime(item.ActionDate).ToString("dd MMM yyyy HH: mm:ss");
                    objReport.HODName = item.fulName;
                    objReport.HodComments = item.Comments;
                    objReport.HodDesignation = "(" + item.DesignationName + ")";
                }
            }
            #endregion
            #endregion
            Rept1.DataSource = objReport;
            Rept1.CreateDocument();

            #region Report2Assessment

            //Report 2 for Assessment
            //Get Assessment ID 
            DataSet ds = new DataSet();
            ds = Qms_bal.GetCCNAssessmentIdBAL(CCNID);
            if (ds.Tables.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    int AssessmentID = Convert.ToInt32(dr["Assessment_ID"]);
                    CCNAssessmentReport AssReport = new CCNAssessmentReport();
                    #region AssessRptLOGANDCCNno
                    if (_dtCompany.Rows.Count != 0)
                    {
                        if (!string.IsNullOrEmpty(_dtCompany.Rows[0]["CompanyLogo"].ToString()))
                        {
                            ((XRPictureBox)(AssReport.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                        }
                    }
                    else
                    {
                        throw new Exception("Please Upload Company Logo");
                    }
                    ((XRLabel)(AssReport.FindControl("xrlblDept", false))).Text = objReport.DepartmentName;
                    ((XRLabel)(AssReport.FindControl("xrlblccnNumber", false))).Text = objReport.CCN_No;
                    #endregion
                    //create table for CCN Assessment
                    XRTable tblCCnAssessment = ((XRTable)(AssReport.FindControl("xrtblccnAssessment", true)));
                    tblCCnAssessment.BeginInit();
                    AssReport.Visible = false;
                    var GetAssessment = (from od in dbEF.AizantIT_ImpactAssessmentTransaction
                                         join DepLnk in dbEF.AizantIT_ImpactDeptLink on od.ImpPoint_ID equals DepLnk.ImpPoint_ID
                                         join pd in dbEF.AizantIT_ImpactPoints on DepLnk.Imp_Desc_ID equals pd.Imp_Desc_ID
                                         where od.CCN_ID == CCNID && od.AssessmentId == AssessmentID 
                                         select new { pd.Impact_Description, od.ImpPoint_ID, od.isYesorNo, od.DocTitleorRefN, od.Remarks, od.Trans_ID }).OrderBy(t2 => t2.Trans_ID)

                                         .Union(from A in dbEF.AizantIT_ImpactAssessmentTransaction
                                                join pd in dbEF.AizantIT_ImpactPoints on A.ImpPoint_ID equals pd.Imp_Desc_ID
                                                where A.CCN_ID == CCNID && A.AssessmentId == AssessmentID && A.ImpPoint_ID == 0
                                                select new { pd.Impact_Description, A.ImpPoint_ID, A.isYesorNo, A.DocTitleorRefN, A.Remarks, A.Trans_ID }).OrderBy(t2 => t2.Trans_ID).ToList();

                    foreach (var item in GetAssessment)
                    {
                        AssReport.Visible = true;
                        XRTableRow row1 = new XRTableRow();//row create

                        XRTableCell cell1 = new XRTableCell();
                        cell1.CanGrow = true;
                        cell1.Width = Convert.ToInt32(220.91);
                        //cell1.Font = new Font("Times New Roman", 9f, FontStyle.Regular);
                        cell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                        cell1.Text = item.Impact_Description;
                        //cell1.BorderWidth = 1;
                        row1.Cells.Add(cell1);
                        XRTableCell cell2 = new XRTableCell();
                        cell2.CanGrow = true;
                        cell2.Width = Convert.ToInt32(80.14);
                        //cell2.BorderWidth = 1;
                        //cell2.Font = new Font("Times New Roman", 9f, FontStyle.Regular);
                        cell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                        var YesorNo = "";
                        if (item.isYesorNo == Convert.ToBoolean("true"))
                        { YesorNo = "Yes"; }
                        if (item.isYesorNo == Convert.ToBoolean("false"))
                        { YesorNo = "NO"; }
                        cell2.Text = YesorNo;
                        //cell2.BorderWidth = 1;
                        row1.Cells.Add(cell2);
                        XRTableCell cell3 = new XRTableCell();
                        cell3.CanGrow = true;
                        cell3.Width = Convert.ToInt32(234.11);
                        //cell3.Font = new Font("Times New Roman", 9f, FontStyle.Regular);
                        cell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                        cell3.Text = item.DocTitleorRefN;
                        //cell3.BorderWidth = 1;
                        row1.Cells.Add(cell3);
                        XRTableCell cell4 = new XRTableCell();
                        cell4.CanGrow = true;
                        cell4.Width = Convert.ToInt32(253.71);
                        //cell4.Font = new Font("Times New Roman", 9f, FontStyle.Regular);
                        cell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                        cell4.Text = item.Remarks;
                        //cell4.BorderWidth = 1;
                        row1.Cells.Add(cell4);
                        tblCCnAssessment.Rows.Add(row1);
                    }
                    tblCCnAssessment.EndInit();
                    //Get Department Name and block name
                    var AssignedDeptID = (from CC in dbEF.AizantIT_ChangeControlCommitte
                                          join DM in dbEF.AizantIT_DepartmentMaster on CC.DeptID equals DM.DeptID
                                          join B in dbEF.AizantIT_BlockMaster on CC.BlockID equals B.BlockID
                                          where CC.Assessment_ID == AssessmentID
                                          select new
                                          {
                                              CC.AssigndToID,
                                              CC.Hod_SubmitDate,
                                              DM.DepartmentName,
                                              B.BlockName
                                          }).ToList();
                    foreach (var item in AssignedDeptID)
                    {
                        ((XRLabel)(AssReport.FindControl("xrlblDeptName", false))).Text = "Department :" + item.DepartmentName + " ( " + item.BlockName + " )";
                    }
                    //Capa Binding for Report
                    CCNCAPAAssessmentReport(AssessmentID, AssReport, CCNID);
                    AssReport.CreateDocument();
                    // Add all pages of the 2nd report to the end of the 1st report. 
                    Rept1.Pages.AddRange(AssReport.Pages);


                }
            }

            #endregion

            #region Report3Classification


            ClassificationVerifyReport CfverifyReport = new ClassificationVerifyReport();

            #region PageHeaderAndLogo
            if (_dtCompany.Rows.Count != 0)
            {
                if (!string.IsNullOrEmpty(_dtCompany.Rows[0]["CompanyLogo"].ToString()))
                {
                    ((XRPictureBox)(CfverifyReport.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                }
            }
            else
            {
                throw new Exception("Please Upload Company Logo");
            }
               ((XRLabel)(Rept1.FindControl("xrlblDept", false))).Text = objReport.DepartmentName;
            ((XRLabel)(Rept1.FindControl("xrlblccnNumber", false))).Text = objReport.CCN_No;

            ((XRLabel)(CfverifyReport.FindControl("xrlblDept", false))).Text = objReport.DepartmentName;
            ((XRLabel)(CfverifyReport.FindControl("xrlblccnNumber", false))).Text = objReport.CCN_No;

            #endregion

            List<int> objClasificationStatus = new List<int>() { 15, 24, 10, 8, 29, 30 };
            if (objClasificationStatus.Contains(objReport.currentStatus))
            {
                //Get Classification and head qa summary
                Classification(CCNID, CfverifyReport, objReport.currentStatus);
            }
            else
            {
                CfverifyReport.Visible = false;
            }
            CfverifyReport.CreateDocument();
            Rept1.Pages.AddRange(CfverifyReport.Pages);

            #endregion

            //For Setting ChangeControl File Attachment Report 28-02-2019
            #region CCN File Attachment Report
            CCNAttachmentReportPage(CCNID, attachFileRpt, objReport.CCN_No, objReport.DepartmentName);
            attachFileRpt.CreateDocument();
            Rept1.Pages.AddRange(attachFileRpt.Pages);

            #endregion
            //End

            string TempAttachmentDir = Server.MapPath("~\\Areas\\QMS\\Models\\ChangeControl\\Reports");
            if (!Directory.Exists(TempAttachmentDir))
            {
                Directory.CreateDirectory(TempAttachmentDir);
            }
            string fileName = CCNID + DateTime.Now.ToString("yyyMMddHHmmssfff") + ".pdf";
            string strFilePath = TempAttachmentDir + "\\" + fileName;
            Rept1.ExportToPdf(strFilePath);
            string onlyReportPath = "Areas\\QMS\\Models\\ChangeControl\\Reports" + "\\" + fileName;

            var GetAlreadyExistReportPath = (from T1 in dbEF.AizantIT_CCNFinalReports
                                             where T1.CCNID == CCNID
                                             select T1).SingleOrDefault();
            if (GetAlreadyExistReportPath != null)
            {
                if (GetAlreadyExistReportPath.ReportFilePath != null)
                {
                    //Delete old Merged File.
                    QMSAttachments objAttachmentsCommon = new QMSAttachments();
                    objAttachmentsCommon.RemovePhysicalPathFile(Server.MapPath("~\\" + GetAlreadyExistReportPath));
                }
                //Updated Report path 
                AizantIT_CCNFinalReports objCCNAttachmentAsSingle =
                           dbEF.AizantIT_CCNFinalReports.SingleOrDefault(p => p.CCNID == CCNID);
                objCCNAttachmentAsSingle.ReportFilePath = onlyReportPath;
                objCCNAttachmentAsSingle.CurrentStatus = objReport.currentStatus;
                dbEF.SaveChanges();
            }
            else
            {
                //Inserted Report path
                AizantIT_CCNFinalReports objInertReportPath = new AizantIT_CCNFinalReports();
                objInertReportPath.ReportFilePath = onlyReportPath;
                objInertReportPath.CCNID = CCNID;
                objInertReportPath.CreatedDate = DateTime.Now;
                objInertReportPath.CurrentStatus = objReport.currentStatus;
                dbEF.AizantIT_CCNFinalReports.Add(objInertReportPath);
                dbEF.SaveChanges();
            }

            //Report generated
            GenerateFinalCCNReport(onlyReportPath, CCNID);

        }
        #endregion

        DataTable fillCompany()
        {
            CompanyBO objCompanyBO;
            UMS_BAL objUMS_BAL;
            DataTable dtcompany = new DataTable();
            objCompanyBO = new CompanyBO();
            objCompanyBO.Mode = 2;
            objCompanyBO.CompanyID = 0;
            objCompanyBO.CompanyCode = "";
            objCompanyBO.CompanyName = "";
            objCompanyBO.CompanyDescription = "";
            objCompanyBO.CompanyPhoneNo1 = "";
            objCompanyBO.CompanyPhoneNo2 = "";
            objCompanyBO.CompanyFaxNo1 = "";
            objCompanyBO.CompanyFaxNo2 = ""; //txtFaxNo2.Value;
            objCompanyBO.CompanyEmailID = "";
            objCompanyBO.CompanyWebUrl = "";
            objCompanyBO.CompanyAddress = "";
            objCompanyBO.CompanyLocation = "";// txtLocat.Value;
            objCompanyBO.CompanyCity = "";
            objCompanyBO.CompanyState = "";
            objCompanyBO.CompanyCountry = "";
            objCompanyBO.CompanyPinCode = "";
            objCompanyBO.CompanyStartDate = "";
            objCompanyBO.CompanyLogo = new byte[0];

            objUMS_BAL = new UMS_BAL();
            dtcompany = objUMS_BAL.CompanyQuery(objCompanyBO);
            return dtcompany;
        }
        private System.Drawing.Image BindApplicantImage(byte[] applicantImage)
        {
            using (var ms = new MemoryStream(applicantImage))
            {
                return System.Drawing.Image.FromStream(ms);
            }
        }

        public void CCNCAPAAssessmentReport(int AssessmentID, CCNAssessmentReport AssRpt, int CCNID)
        {
            AizantIT_ChangeControlCommitte aizantIT_ = new AizantIT_ChangeControlCommitte();
            aizantIT_ = dbEF.AizantIT_ChangeControlCommitte.Where(a => a.Assessment_ID == AssessmentID).ToList().FirstOrDefault();
            DataSet ds = new DataSet();
            XRTable tblCCNCAPA = ((XRTable)(AssRpt.FindControl("xrltblCapa", true)));
            XRLabel lblheaderCapa = ((XRLabel)(AssRpt.FindControl("xrlblCapaActionHeader", true)));
            tblCCNCAPA.Visible = false;
            lblheaderCapa.Visible = false;
            #region BindTablesCCNCapaDetails
            QMS_BAL QmsBAL = new QMS_BAL();
            DataSet dt = QmsBAL.GetEventsCAPAListBAL(100, 0, 0, "", "", CCNID, 2, AssessmentID);
            if (dt.Tables[0].Rows.Count > 0)
            {
                tblCCNCAPA.Visible = true;
                lblheaderCapa.Visible = true;
                tblCCNCAPA.BeginInit();

                //CAPA Dynamic Table binding 01-08-2019
                for (int i = 0; i < dt.Tables[0].Rows.Count; i++)
                {
                    XRTableRow row1 = new XRTableRow();//row create
                    XRTableCell cell1 = new XRTableCell();
                    cell1.CanGrow = true;
                    cell1.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Bold);
                    cell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell1.Text = "CAPA Number : ";
                    cell1.WidthF = 150.5F;
                    cell1.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    //cell1.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right;
                    row1.Cells.Add(cell1);

                    XRTableCell cell2 = new XRTableCell();
                    cell2.CanGrow = true;
                    cell2.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Regular);
                    cell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell2.WidthF = 254.5F;
                    cell2.Text = dt.Tables[0].Rows[i]["CAPA_Number"].ToString();
                    cell2.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    //cell2.LeftF = 30;
                    cell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0);
                    row1.Cells.Add(cell2);


                    XRTableCell cell3 = new XRTableCell();
                    cell3.CanGrow = true;
                    cell3.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Bold);
                    cell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell3.WidthF = 100.5F;
                    cell3.Text = "Department : ";
                    cell3.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    row1.Cells.Add(cell3);

                    XRTableCell cell4 = new XRTableCell();
                    cell4.CanGrow = true;
                    cell4.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Regular);
                    cell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell4.WidthF = 304.5F;
                    cell4.Text = dt.Tables[0].Rows[i]["Department"].ToString();
                    cell4.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    cell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0);
                    row1.Cells.Add(cell4);

                    XRTableRow row2 = new XRTableRow();//row create
                    XRTableCell cell5 = new XRTableCell();
                    cell5.CanGrow = true;
                    cell5.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Bold);
                    cell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell5.WidthF = 150.5F;
                    cell5.Text = "Type of Action : ";
                    cell5.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    row2.Cells.Add(cell5);

                    XRTableCell cell6 = new XRTableCell();
                    cell6.CanGrow = true;
                    cell6.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Regular);
                    cell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell6.WidthF = 254.5F;
                    cell6.Text = dt.Tables[0].Rows[i]["PlanofAction"].ToString();
                    cell6.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    cell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0);
                    row2.Cells.Add(cell6);

                    XRTableCell cell7 = new XRTableCell();
                    cell7.CanGrow = true;
                    cell7.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Bold);
                    cell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell7.WidthF = 100.5F;
                    cell7.Text = "Target Date : ";
                    cell7.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    row2.Cells.Add(cell7);

                    XRTableCell cell8 = new XRTableCell();
                    cell8.CanGrow = true;
                    cell8.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Regular);
                    cell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell8.WidthF = 304.5F;
                    cell8.Text = Convert.ToDateTime(dt.Tables[0].Rows[i]["TargetDate"]).ToString("dd MMM yyyy");
                    cell8.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    cell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0);
                    row2.Cells.Add(cell8);

                    XRTableRow row3 = new XRTableRow();//row create
                    XRTableCell cell9 = new XRTableCell();
                    cell9.CanGrow = true;
                    cell9.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Bold);
                    cell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell9.WidthF = 150.5F;
                    cell9.Text = "Status : ";
                    cell9.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    row3.Cells.Add(cell9);

                    XRTableCell cell10 = new XRTableCell();
                    cell10.CanGrow = true;
                    cell10.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Regular);
                    cell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell10.WidthF = 659.5F;
                    cell10.Text = dt.Tables[0].Rows[i]["CurrentStatus"].ToString().Trim();
                    cell10.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    cell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0);
                    row3.Cells.Add(cell10);

                    XRTableRow row4 = new XRTableRow();
                    XRTableCell cell11 = new XRTableCell();
                    cell11.CanGrow = true;
                    cell11.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Bold);
                    cell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell11.WidthF = 150.5F;
                    cell11.Text = "Responsible Person : ";
                    cell11.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    row4.Cells.Add(cell11);

                    XRTableCell cell12 = new XRTableCell();
                    cell12.CanGrow = true;
                    cell12.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Regular);
                    cell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell12.WidthF = 659.5F;
                    cell12.Text = dt.Tables[0].Rows[i]["Responsible_Persons"].ToString();
                    cell12.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    cell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0);
                    row4.Cells.Add(cell12);


                    XRTableRow row5 = new XRTableRow();
                    XRTableCell cell13 = new XRTableCell();
                    cell13.CanGrow = true;
                    cell13.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Bold);
                    cell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell13.WidthF = 150.5F;
                    cell13.Text = "Description : ";
                    cell13.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    row5.Cells.Add(cell13);

                    XRTableCell cell14 = new XRTableCell();
                    cell14.CanGrow = true;
                    cell14.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Regular);
                    cell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell14.WidthF = 659.5F;
                    cell14.Text = dt.Tables[0].Rows[i]["CAPA_Discrption"].ToString();
                    cell14.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    cell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0);
                    row5.Cells.Add(cell14);

                    //For giving empty space for row
                    XRTableRow row6 = new XRTableRow();
                    XRTableCell cell15 = new XRTableCell();
                    cell15.CanGrow = true;
                    //cell15.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right;
                    if (dt.Tables[0].Rows.Count != (i + 1))
                        cell15.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                    else
                        cell15.Borders = DevExpress.XtraPrinting.BorderSide.None;

                    cell15.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Regular);
                    cell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell15.WidthF = 202.5F;
                    cell15.Text = "  ";

                    XRTableCell cell16 = new XRTableCell();
                    cell16.CanGrow = true;
                    if (dt.Tables[0].Rows.Count != (i + 1))
                        cell16.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                    else
                        cell16.Borders = DevExpress.XtraPrinting.BorderSide.None;
                    cell16.Font = new System.Drawing.Font("Times New Roman", 11f, FontStyle.Regular);
                    cell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
                    cell16.WidthF = 607.5F;
                    cell16.Text = "  ";
                    row6.Cells.Add(cell16);

                    tblCCNCAPA.Rows.Add(row1);
                    tblCCNCAPA.Rows.Add(row2);
                    tblCCNCAPA.Rows.Add(row3);
                    tblCCNCAPA.Rows.Add(row4);
                    tblCCNCAPA.Rows.Add(row5);
                    tblCCNCAPA.Rows.Add(row6);
                }
                //end
                tblCCNCAPA.EndInit();
            }
            #endregion

            #region AssessmentE-sing&&Comments
             ((XRLabel)(AssRpt.FindControl("xrlblAssessmentComments", false))).Text = aizantIT_.Comments;
            var AssessmentHODNames = (from t1 in dbEF.AizantIT_ChangeControlCommitte
                                       join t2 in dbEF.AizantIT_EmpMaster on t1.AssigndToID equals t2.EmpID
                                       join t3 in dbEF.AizantIT_Designation on t2.CurrentDesignation equals t3.DesignationID
                                       where t1.Assessment_ID == AssessmentID && t1.CCN_ID == CCNID
                                       select new { t1.Hod_SubmitDate, t3.DesignationName, fulName = (t2.FirstName + "" + t2.LastName) }).ToList();
                                     
            foreach (var item in AssessmentHODNames)
            {
                ((XRLabel)(AssRpt.FindControl("xrlblAssessmentHodname", false))).Text = item.fulName;
                ((XRLabel)(AssRpt.FindControl("xrlblasshoddate", false))).Text = Convert.ToDateTime(item.Hod_SubmitDate).ToString("dd MMM yyyy HH: mm:ss");
                ((XRLabel)(AssRpt.FindControl("xrlblDesignation", false))).Text = "(" + item.DesignationName.ToString() + ")";
            }
            #endregion

        }

        public void Classification(int ccnid, ClassificationVerifyReport CfVerifyRept, int CurrentStaus)
        {
            ClassificationBO objCfjbo = new ClassificationBO();
            List<int?> QACommetnsStatus = new List<int?> { 15, 24 };
            var Classification = (from cm in dbEF.AizantIT_ChangeControlMaster
                                  join CH in dbEF.AizantIT_CCNHistory on cm.CCN_ID equals CH.CCNID
                                  join EM in dbEF.AizantIT_EmpMaster on CH.ActionBy equals EM.EmpID
                                  join D in dbEF.AizantIT_Designation on EM.CurrentDesignation equals D.DesignationID
                                  where cm.CCN_ID == ccnid && QACommetnsStatus.Contains(CH.ActionStatus)
                                  select new { cm.ChangeClassification, D.DesignationName, CH.Comments, CH.ActionDate, QAName = (EM.FirstName + "" + EM.LastName) }).ToList();
            foreach (var item in Classification)
            {
                if (item.ChangeClassification == (1))
                {
                    objCfjbo.Classification = "Major";
                }
                if (item.ChangeClassification == (0))
                {
                    objCfjbo.Classification = "Minor";
                }
                objCfjbo.QaName = item.QAName;
                objCfjbo.QASubmittedDate = Convert.ToDateTime(item.ActionDate).ToString("dd MMM yyyy HH: mm:ss");
                objCfjbo.QaSummary = item.Comments;
                objCfjbo.QaDesignation = "(" + item.DesignationName + ")";
            }

            XRTable tblHqaComments = ((XRTable)(CfVerifyRept.FindControl("xrHQACommentsTable", true)));
            XRTable tblHqaEsign = ((XRTable)(CfVerifyRept.FindControl("xrtblEsign", true)));
            XRLabel lblheaderHqa = ((XRLabel)(CfVerifyRept.FindControl("xrlblhqaHeader", true)));
            tblHqaEsign.Visible = false;
            lblheaderHqa.Visible = false;
            tblHqaComments.Visible = false;

            var HeadQASummary = (from CH in dbEF.AizantIT_CCNHistory
                                 join EM in dbEF.AizantIT_EmpMaster on CH.ActionBy equals EM.EmpID
                                 join D in dbEF.AizantIT_Designation on EM.CurrentDesignation equals D.DesignationID
                                 where CH.CCNID == ccnid && CH.ActionStatus == 8
                                 select new { CH.Comments, D.DesignationName, CH.ActionDate, HeadQAname = (EM.FirstName + "" + EM.LastName) }).ToList();
            foreach (var item in HeadQASummary)
            {
                tblHqaEsign.Visible = true;
                lblheaderHqa.Visible = true;
                tblHqaComments.Visible = true;
                objCfjbo.HeadQaName = item.HeadQAname;
                objCfjbo.HeadQASummary = item.Comments;
                objCfjbo.HeadQaSubmittedDate = Convert.ToDateTime(item.ActionDate).ToString("dd MMM yyyy HH: mm:ss");
                objCfjbo.HEadQaDesignation = "(" + item.DesignationName + ")";
            }
            XRTable tblClosingComments = ((XRTable)(CfVerifyRept.FindControl("xrtblCosingComments", true)));
            XRTable tblClosingEsign = ((XRTable)(CfVerifyRept.FindControl("xrtblcolsingEsign", true)));
            XRLabel lblheaderClosing = ((XRLabel)(CfVerifyRept.FindControl("xrlblClosingheader", true)));
            tblClosingComments.Visible = false;
            tblClosingEsign.Visible = false;
            lblheaderClosing.Visible = false;
            var CloseOfCCN = (from cm in dbEF.AizantIT_ChangeControlMaster
                              join CH in dbEF.AizantIT_CCNHistory on cm.CCN_ID equals CH.CCNID
                              join EM in dbEF.AizantIT_EmpMaster on CH.ActionBy equals EM.EmpID
                              join D in dbEF.AizantIT_Designation on EM.CurrentDesignation equals D.DesignationID
                              where cm.CCN_ID == ccnid && CH.ActionStatus == 29
                              select new { D.DesignationName, CH.Comments, CH.ActionDate, CloseQaName = (EM.FirstName + "" + EM.LastName) }).ToList();
            foreach (var itemC in CloseOfCCN)
            {
                tblClosingComments.Visible = true;
                tblClosingEsign.Visible = true;
                lblheaderClosing.Visible = true;
                //objCfjbo.CloseEffectiveDate = Convert.ToDateTime(itemC.EffectiveDate).ToString("dd MMM yyyy");
                objCfjbo.CloseComments = itemC.Comments;
                objCfjbo.CloseQAName = itemC.CloseQaName;
                objCfjbo.CloseQaSubmittedDate = Convert.ToDateTime(itemC.ActionDate).ToString("dd MMM yyyy HH: mm:ss");
                objCfjbo.CloseDesignation = "(" + itemC.DesignationName + ")";
            }

            //For Binding Effective Date
            var CCNEffectiveDate = (from ccnc in dbEF.AizantIT_CCNClose
                                    select new { ccnc.ClosureDate }
                                    ).ToList();
            foreach (var itemEffDate in CCNEffectiveDate)
            {
                objCfjbo.CloseEffectiveDate = Convert.ToDateTime(itemEffDate.ClosureDate).ToString("dd MMM yyyy");
            }
            CfVerifyRept.DataSource = objCfjbo;
        }

        // CCN File Attachment method 28-02-2019
        public void CCNAttachmentReportPage(int ccnid, CCNAttachmentsReport attachFileRpt, string CCN_No, string DepartmentName)
        {
            QMS_BAL qmsBal = new QMS_BAL();
            //For Binding CCN No and Department Name
            ((XRLabel)(attachFileRpt.FindControl("xrlblccnno", false))).Text = CCN_No;
            ((XRLabel)(attachFileRpt.FindControl("xrlbldeptname", false))).Text = DepartmentName;

            DataSet dsGetCCNFileAttch = qmsBal.GetCCNFileNameBal(ccnid);

            if (dsGetCCNFileAttch.Tables[0].Rows.Count == 0 && dsGetCCNFileAttch.Tables[1].Rows.Count == 0
                && dsGetCCNFileAttch.Tables[2].Rows.Count == 0 && dsGetCCNFileAttch.Tables[3].Rows.Count == 0)
            {
                attachFileRpt.Visible = false;
            }
            else
            {
                //For Binding InitatorFile Attachment Table
                XRTable tblInitiatorFileAttachTable = ((XRTable)(attachFileRpt.FindControl("xrTableInitiatorFileAttachments", true)));
                tblInitiatorFileAttachTable.BeginInit();

                DataTable dtInitiatorTable = dsGetCCNFileAttch.Tables[0];

                if (dtInitiatorTable.Rows.Count == 0)
                {
                    attachFileRpt.FindControl("xrLabelInitiatorFileAttachments", true).Visible = false;
                    attachFileRpt.FindControl("xrTableInitiatorFileAttachments", true).Visible = false;
                }
                foreach (DataRow dtRow in dtInitiatorTable.Rows)
                {
                    XRTableRow row1 = new XRTableRow();//row create
                    XRTableCell cell1 = new XRTableCell();
                    cell1.CanGrow = true;
                    cell1.Width = 41;
                    cell1.Text = dtRow["Sno"].ToString();
                    row1.Cells.Add(cell1);

                    XRTableCell cell2 = new XRTableCell();
                    cell2.CanGrow = true;
                    cell2.Width = 172;
                    cell2.Text = dtRow["EmpName"].ToString();
                    row1.Cells.Add(cell2);

                    XRTableCell cell3 = new XRTableCell();
                    cell3.CanGrow = true;
                    cell3.Width = 72;
                    cell3.Text = dtRow["RoleName"].ToString();
                    row1.Cells.Add(cell3);

                    XRTableCell cell4 = new XRTableCell();
                    cell4.CanGrow = true;
                    cell4.Width = 420;
                    cell4.Text = dtRow["FileName"].ToString();
                    row1.Cells.Add(cell4);

                    tblInitiatorFileAttachTable.Rows.Add(row1);
                }
                tblInitiatorFileAttachTable.EndInit();

                //For Binding CCN HOD Assessment File Attachment Table

                XRTable labelAssmntHODFileAttachTable = ((XRTable)(attachFileRpt.FindControl("xrLabelHODAssmtFileAttachments", true)));
                XRTable tblAssmntHODFileAttachTable = ((XRTable)(attachFileRpt.FindControl("xrTableHODAssmtFileAttachments", true)));

                if (dtInitiatorTable.Rows.Count == 0)
                {
                    //labelAssmntHODFileAttachTable.LocationF = new PointF(25.88F, 0F);
                    //tblAssmntHODFileAttachTable.LocationF = new PointF(25.88F, 37.5F);
                    labelAssmntHODFileAttachTable.LocationF = new PointF(0F, 0F);
                    tblAssmntHODFileAttachTable.LocationF = new PointF(0F, 37.5F);
                }

                tblAssmntHODFileAttachTable.BeginInit();

                DataTable dtHODAssmntTable = dsGetCCNFileAttch.Tables[1];

                if (dtHODAssmntTable.Rows.Count == 0)
                {
                    attachFileRpt.FindControl("xrLabelHODAssmtFileAttachments", true).Visible = false;
                    attachFileRpt.FindControl("xrTableHODAssmtFileAttachments", true).Visible = false;
                }

                foreach (DataRow dtRow in dtHODAssmntTable.Rows)
                {
                    XRTableRow row1 = new XRTableRow();//row create
                    XRTableCell cell1 = new XRTableCell();
                    cell1.CanGrow = true;
                    cell1.Width = 41;
                    cell1.Text = dtRow["Sno"].ToString();
                    row1.Cells.Add(cell1);

                    XRTableCell cell2 = new XRTableCell();
                    cell2.CanGrow = true;
                    cell2.Width = 172;
                    cell2.Text = dtRow["EmpName"].ToString();
                    row1.Cells.Add(cell2);

                    XRTableCell cell3 = new XRTableCell();
                    cell3.CanGrow = true;
                    cell3.Width = 72;
                    cell3.Text = dtRow["RoleName"].ToString();
                    row1.Cells.Add(cell3);

                    XRTableCell cell4 = new XRTableCell();
                    cell4.CanGrow = true;
                    cell4.Width = 420;
                    cell4.Text = dtRow["FileName"].ToString();
                    row1.Cells.Add(cell4);

                    tblAssmntHODFileAttachTable.Rows.Add(row1);
                }
                tblAssmntHODFileAttachTable.EndInit();


                //For Binding CCN QA Closure File Attachment Table
                XRTable lblQAClosureFileAttachLable = ((XRTable)(attachFileRpt.FindControl("xrLabelCCNClosureFileAttachments", true)));
                XRTable tblQAClosureFileAttachTable = ((XRTable)(attachFileRpt.FindControl("xrTableCCNClosureFileAttachments", true)));

                tblQAClosureFileAttachTable.BeginInit();

                DataTable dtCCNClosureTable = dsGetCCNFileAttch.Tables[2];

                if (dtCCNClosureTable.Rows.Count == 0)
                {
                    attachFileRpt.FindControl("xrLabelCCNClosureFileAttachments", true).Visible = false;
                    attachFileRpt.FindControl("xrTableCCNClosureFileAttachments", true).Visible = false;
                }

                foreach (DataRow dtRow in dtCCNClosureTable.Rows)
                {
                    XRTableRow row1 = new XRTableRow();//row create
                    XRTableCell cell1 = new XRTableCell();
                    cell1.CanGrow = true;
                    cell1.Width = 41;
                    cell1.Text = dtRow["Sno"].ToString();
                    row1.Cells.Add(cell1);

                    XRTableCell cell2 = new XRTableCell();
                    cell2.CanGrow = true;
                    cell2.Width = 172;
                    cell2.Text = dtRow["EmpName"].ToString();
                    row1.Cells.Add(cell2);

                    XRTableCell cell3 = new XRTableCell();
                    cell3.CanGrow = true;
                    cell3.Width = 71;
                    cell3.Text = dtRow["RoleName"].ToString();
                    row1.Cells.Add(cell3);

                    XRTableCell cell4 = new XRTableCell();
                    cell4.CanGrow = true;
                    cell4.Width = 420;
                    cell4.Text = dtRow["FileName"].ToString();
                    row1.Cells.Add(cell4);

                    tblQAClosureFileAttachTable.Rows.Add(row1);
                }
                tblQAClosureFileAttachTable.EndInit();

                //For Binding CAPA Complete Attachment Files
                XRTable lblCAPACompltAttachmentLable = ((XRTable)(attachFileRpt.FindControl("xrLabelCAPACompltattachment", true)));
                XRTable tblCAPACompletedFileAttachTable = ((XRTable)(attachFileRpt.FindControl("xrTableCAPAAttachment", true)));
                tblCAPACompletedFileAttachTable.BeginInit();

                DataTable dtCAPACompleteAttachmentTable = dsGetCCNFileAttch.Tables[3];

                if (dtCAPACompleteAttachmentTable.Rows.Count == 0)
                {
                    attachFileRpt.FindControl("xrLabelCAPACompltattachment", true).Visible = false;
                    attachFileRpt.FindControl("xrTableCAPAAttachment", true).Visible = false;
                }
                foreach (DataRow dtRow in dtCAPACompleteAttachmentTable.Rows)
                {
                    XRTableRow row1 = new XRTableRow();//row create
                    XRTableCell cell1 = new XRTableCell();
                    cell1.CanGrow = true;
                    cell1.WidthF = 43.14f;
                    cell1.Text = dtRow["Sno"].ToString();
                    row1.Cells.Add(cell1);

                    XRTableCell cell2 = new XRTableCell();
                    cell2.CanGrow = true;
                    cell2.WidthF = 133.17f;
                    cell2.Text = dtRow["CAPA_Number"].ToString();
                    row1.Cells.Add(cell2);

                    XRTableCell cell3 = new XRTableCell();
                    cell3.CanGrow = true;
                    cell3.WidthF = 190.36f;
                    cell3.Text = dtRow["Assessment"].ToString();
                    row1.Cells.Add(cell3);

                    XRTableCell cell4 = new XRTableCell();
                    cell4.CanGrow = true;
                    cell4.WidthF = 364.58f;
                    cell4.Text = dtRow["FileName"].ToString();
                    row1.Cells.Add(cell4);

                    tblCAPACompletedFileAttachTable.Rows.Add(row1);
                }
                tblCAPACompletedFileAttachTable.EndInit();

                if (dtInitiatorTable.Rows.Count == 0)//Initiator files zero
                {

                    labelAssmntHODFileAttachTable.LocationF = new PointF(0F, 0F);
                    tblAssmntHODFileAttachTable.LocationF = new PointF(0F, 37.5F);
                    lblQAClosureFileAttachLable.LocationF = new PointF(0F, 103.12F);
                    tblQAClosureFileAttachTable.LocationF = new PointF(0F, 141.67F);
                    lblCAPACompltAttachmentLable.LocationF = new PointF(0F, 205.21F);
                    tblCAPACompletedFileAttachTable.LocationF = new PointF(0F, 245.83F);
                }

                if (dtHODAssmntTable.Rows.Count == 0)//Assessment files zero
                {
                    //lblQAClosureFileAttachLable.LocationF = new PointF(25.88F, 103.12F);
                    //tblQAClosureFileAttachTable.LocationF = new PointF(25.88F, 141.67F);
                    //lblCAPACompltAttachmentLable.LocationF = new PointF(25.88F, 205.21F);
                    //tblCAPACompletedFileAttachTable.LocationF = new PointF(25.88F, 245.83F);

                    lblQAClosureFileAttachLable.LocationF = new PointF(0F, 103.12F);
                    tblQAClosureFileAttachTable.LocationF = new PointF(0F, 141.67F);
                    lblCAPACompltAttachmentLable.LocationF = new PointF(0F, 205.21F);
                    tblCAPACompletedFileAttachTable.LocationF = new PointF(0F, 245.83F);
                }
                if (dtInitiatorTable.Rows.Count == 0 && dtHODAssmntTable.Rows.Count == 0)//Initiator and Assessment files zero
                {
                    //lblQAClosureFileAttachLable.LocationF = new PointF(25.88F, 0F);
                    //tblQAClosureFileAttachTable.LocationF = new PointF(25.88F, 37.5F);
                    //lblCAPACompltAttachmentLable.LocationF = new PointF(25.88F, 103.12F);
                    //tblCAPACompletedFileAttachTable.LocationF = new PointF(25.88F, 141.67F);

                    lblQAClosureFileAttachLable.LocationF = new PointF(0F, 0F);
                    tblQAClosureFileAttachTable.LocationF = new PointF(0F, 37.5F);
                    lblCAPACompltAttachmentLable.LocationF = new PointF(0F, 103.12F);
                    tblCAPACompletedFileAttachTable.LocationF = new PointF(0F, 141.67F);
                }
                if (dtCCNClosureTable.Rows.Count == 0)////CCN Closer files zero
                {
                    //lblCAPACompltAttachmentLable.LocationF = new PointF(25.88F, 103.12F);
                    //tblCAPACompletedFileAttachTable.LocationF = new PointF(25.88F, 141.67F);

                    lblCAPACompltAttachmentLable.LocationF = new PointF(0F, 205.21F);
                    tblCAPACompletedFileAttachTable.LocationF = new PointF(0F, 245.83F);
                }
                //Initiator ,assessment and closer  files zero
                if (dtInitiatorTable.Rows.Count == 0 && dtHODAssmntTable.Rows.Count == 0 && dtCCNClosureTable.Rows.Count == 0)
                {
                    //lblCAPACompltAttachmentLable.LocationF = new PointF(25.88F, 0F);
                    //tblCAPACompletedFileAttachTable.LocationF = new PointF(25.88F, 37.5F);

                    lblCAPACompltAttachmentLable.LocationF = new PointF(0F, 0F);
                    tblCAPACompletedFileAttachTable.LocationF = new PointF(0F, 37.5F);
                }

            }
        }
        //end
        public ActionResult ReportList(int ShowType)
        {
            try
            {
                //For Report drop down binding
                BindDeptDropdownForReports();
                TempData["QuerySting"] = ShowType;

                return View();
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M33:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                TempData["Exception"] = ErMsg;
                //ViewBag.Exception = ErMsg;
                return View();
            }
        }
        //List Report view
        [HttpGet]
        public JsonResult CCNReportPrintView(int DepartmentID, string RptFromDate, string RptToDate, string RoledIDStatus)
        {
            try
            {
                int EmployeeID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);

                QMS_BAL qmsBal = new QMS_BAL();
                CCNListViewReport rpt_CCNList = new CCNListViewReport();
                //Code for Company Logo
                DataTable _dtCompany = fillCompany();
                if (_dtCompany.Rows.Count != 0)
                {
                    if (!string.IsNullOrEmpty(_dtCompany.Rows[0]["CompanyLogo"].ToString()))
                    {
                        ((XRPictureBox)(rpt_CCNList.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                    }
                }
                else
                {
                    throw new Exception("Please Upload Company Logo");
                }
                DataSet dsCCNListFrmDateToDate = objbal.GetCCNReportListBal(0, 0, 0, "desc", "", RoledIDStatus, DepartmentID, RptFromDate, RptToDate, EmployeeID, 1,"","","","","","","");//
                if (dsCCNListFrmDateToDate.Tables[0].Rows.Count > 0)
                {
                    rpt_CCNList.DataSource = dsCCNListFrmDateToDate.Tables[0];
                    rpt_CCNList.DataMember = dsCCNListFrmDateToDate.Tables[0].TableName;
                    //For Binding Department Name by DepartmentID
                    var DeptName = (from DE in dbEF.AizantIT_DepartmentMaster
                                    where DE.DeptID == DepartmentID
                                    select new { DE.DepartmentName }).ToList();
                    if (DeptName.Count == 0)
                    {
                        rpt_CCNList.FindControl("xrtblDepartment", true).Visible = false;
                    }

                    // Report Header empty space trimming for with department and without department 28-06-2019
                    XRTable tblCCNDept = ((XRTable)(rpt_CCNList.FindControl("xrTableFromToDateCCN", true)));
                    XRLabel lblCCNReport = ((XRLabel)(rpt_CCNList.FindControl("xrLabel2", true)));
                    if (DeptName.Count == 0)
                    {

                        tblCCNDept.LocationF = new PointF(120.61F, 61.88F);
                        //lblCCNReport.LocationF = new PointF(328.39F, 42.37F);
                        lblCCNReport.LocationF = new PointF(259.03F, 20.42F);
                    }
                    //end

                    foreach (var item in DeptName)
                    {
                        ((XRLabel)(rpt_CCNList.FindControl("xrlblCCNDept", false))).Text = item.DepartmentName;
                    }
                ((XRLabel)(rpt_CCNList.FindControl("xrlblCCNFromDate", false))).Text = RptFromDate;
                    ((XRLabel)(rpt_CCNList.FindControl("xrlblCCNToDate", false))).Text = RptToDate;

                    string strFilePath = DynamicFolder.CreateDynamicFolder(11);
                    string fileName = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                    strFilePath = strFilePath + "\\" + fileName;
                    //for Print Path
                    var PrintFilePath = System.Web.VirtualPathUtility.ToAbsolute("/QMSTempReports/" + DateTime.Today.ToString("yyyyMMdd") + "/" + fileName); ;
                    rpt_CCNList.ExportToPdf(strFilePath);
                    return Json(new { hasError = false, data = fileName, PrintFilePath }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { hasError = true, data = "No Records found" }, JsonRequestBehavior.AllowGet);
                }
                //return View();
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M34:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        //CCN Report List View
        [HttpGet]
        public JsonResult GetCCNReportList()
        {
            try
            {
                //For avoid page display size on Incident Report
                int iDisplayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int iDisplayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int iSortCol_0 = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir_0 = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                string Role = Request.Params["Role"];
                int deptIdReport = Convert.ToInt32(Request.Params["DeptRpt"]);
                string FromDateReport = (Request.Params["FromDateRpt"]).ToString();
                string ToDateReport = (Request.Params["ToDateReport"]).ToString();
                int EmployeeID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                int filteredCount = 0;

                string sSearch_CCN_Number = Request.Params["sSearch_2"];
                string sSearch_Department = Request.Params["sSearch_3"];
                string sSearch_Initiatedby = Request.Params["sSearch_4"];
                string sSearch_CCN_Date = Request.Params["sSearch_5"];
                string sSearch_Due_Date = Request.Params["sSearch_6"];
                string sSearch_Classification = Request.Params["sSearch_7"];
                string sSearch_CurrentStatus = Request.Params["sSearch_8"];

                List<CCNList> listEmployeesReport = new List<CCNList>();

                int totalRecordsCount = 0;
                DataSet dt = objbal.GetCCNReportListBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, Role, deptIdReport,
                    FromDateReport, ToDateReport, EmployeeID, 0,
                     sSearch_CCN_Number, sSearch_Department, sSearch_Initiatedby
            , sSearch_Classification, sSearch_CCN_Date, sSearch_Due_Date, sSearch_CurrentStatus);

                if (dt.Tables[0].Rows.Count > 0)
                {
                    totalRecordsCount = Convert.ToInt32(dt.Tables[0].Rows[0]["TotalCount"]);
                    foreach (DataRow rdr in dt.Tables[0].Rows)
                    {
                        CCNList CCNList = new CCNList();
                        CCNList.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                        CCNList.CCNID = Convert.ToInt32(rdr["CCN_ID"].ToString());
                        CCNList.BlockID = Convert.ToInt32(rdr["BlockID"].ToString());
                        CCNList.CCN_Number = (rdr["CCN_No"].ToString());
                        CCNList.Block = (rdr["BlockName"].ToString());
                        CCNList.CCN_Date = (rdr["CCN_Date"].ToString());
                        CCNList.Department = (rdr["Department"]).ToString();
                        CCNList.Initiatedby = rdr["Initiatedby"].ToString();
                        CCNList.CurrentStatus = (rdr["CurrentStatus"]).ToString();
                        CCNList.Transaction = (rdr["Transction"].ToString());
                        CCNList.AssessmentID = Convert.ToInt32(rdr["AssessmentID"]);
                        CCNList.Due_Date = (rdr["DueDate"]).ToString();
                        CCNList.Classification = (rdr["Classification"]).ToString();
                        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                        CCNList.CurrentStatunumber = Convert.ToInt32(rdr["CurrentStatusNumber"]);
                        listEmployeesReport.Add(CCNList);
                    }
                }
                return Json(new
                {
                    hasError = false,
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = filteredCount,
                    aaData = listEmployeesReport
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                //ViewBag.SysError = "Yes";
                return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion


        #region File Uploads

        #region CCNFileUplaod Creation
        public ActionResult InitatorFileUploadPV()
        {
            TempData.Keep("QueryString");
            return PartialView();
        }
        [HttpPost]
        public JsonResult BindFileUploadForCCNInitator()
        {
            try
            {
                if (TempData["FileMsgCreate"] == null)
                {
                    TempData["FileMsgCreate"] = "";
                }
                if (TempData["FileAddedCountCreate"] == null)
                {
                    TempData["FileAddedCountCreate"] = 0;
                }
                if (TempData["FileexistsCountCreate"] == null)
                {
                    TempData["FileexistsCountCreate"] = 0;
                }
                if (TempData["IsTempFileID"] == null)
                {
                    TempData["IsTempFileID"] = 0;
                }
                string msg = "";
                string UploadedFilePath = null;
                using (DbContextTransaction transaction = dbEF.Database.BeginTransaction())
                {
                    try
                    {
                        HttpFileCollectionBase files = Request.Files;
                        AizantIT_CCNInitiatorFileAttachment objFilebo = new AizantIT_CCNInitiatorFileAttachment();
                        objFilebo.CCNID = Convert.ToInt32(Request.Params["CCNID"]);
                        int FileexistsCount = 0;
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFileBase file = files[i];
                            if (file != null)
                            {
                                AizantIT_CCNIsTempFileUpload objtemp = new AizantIT_CCNIsTempFileUpload();
                                objFilebo.FileName = Path.GetFileName(file.FileName);
                                objFilebo.FileType = Path.GetExtension(file.FileName);
                                if (objFilebo.CCNID == 0) //ccn id not exist
                                {
                                    if (Convert.ToInt32(TempData["IsTempFileID"]) == 0)
                                    {
                                        dbEF.AizantIT_CCNIsTempFileUpload.Add(objtemp);
                                        dbEF.SaveChanges();

                                        objFilebo.TemprefID = objtemp.TemprefID;
                                        TempData["IsTempFileID"] = objtemp.TemprefID;
                                        TempData.Keep("IsTempFileID");
                                    }
                                    else
                                    {
                                        objFilebo.TemprefID = Convert.ToInt32(TempData["IsTempFileID"]);
                                        TempData.Keep("IsTempFileID");
                                    }
                                    var Count = dbEF.AizantIT_CCNInitiatorFileAttachment.FirstOrDefault(h => h.FileName == objFilebo.FileName
                                  && h.FileType == objFilebo.FileType && h.TemprefID == objFilebo.TemprefID);
                                    if (Count != null)
                                    {
                                        FileexistsCount++;
                                    }
                                }
                                else //ccn id exist
                                {
                                    var Count1 = dbEF.AizantIT_CCNInitiatorFileAttachment.FirstOrDefault(h => h.FileName == objFilebo.FileName
                           && h.FileType == objFilebo.FileType && h.CCNID == objFilebo.CCNID);
                                    if (Count1 != null)
                                    {
                                        FileexistsCount++;
                                    }
                                }
                            }
                            if (FileexistsCount == 0)
                            {
                                if (ModelState.IsValid)
                                {
                                    if (file != null)
                                    {
                                        objFilebo.Createdby = GetCookieEmployeID();
                                        objFilebo.CreatedDate = DateTime.Now;
                                        QMSAttachments objAttachmentsCommon = new QMSAttachments();
                                        UploadedFilePath = objAttachmentsCommon.SaveUploadedFileinPhysicalPath(file, "Initiator", 2);
                                        objFilebo.FilePath = UploadedFilePath;
                                        objFilebo.FileName = Path.GetFileName(file.FileName);
                                        objFilebo.FileType = Path.GetExtension(file.FileName);
                                        dbEF.AizantIT_CCNInitiatorFileAttachment.Add(objFilebo);
                                        dbEF.SaveChanges();
                                        UploadedFilePath = null;
                                        TempData["FileMsgCreate"] += Path.GetFileName(file.FileName) + " File uploaded successfully." + "<br/>";
                                        TempData["FileAddedCountCreate"] = Convert.ToInt32(TempData["FileAddedCountCreate"]) + 1;
                                    }
                                }
                            }
                            else
                            {
                                TempData["FileMsgCreate"] += Path.GetFileName(file.FileName) + " upload failed as file already exists." + "<br/>";
                                TempData["FileexistsCountCreate"] = Convert.ToInt32(TempData["FileexistsCountCreate"]) + 1;
                            }
                        }
                        TempData.Keep("FileMsgCreate");
                        TempData.Keep("FileexistsCountCreate");
                        TempData.Keep("FileAddedCountCreate");
                        TempData.Keep("IsTempFileID");
                        var MsgType = "";
                        if (Convert.ToInt32(TempData["FileexistsCountCreate"]) > 0 && Convert.ToInt32(TempData["FileAddedCountCreate"]) == 0)
                        {
                            MsgType = "error";
                        }
                        if (Convert.ToInt32(TempData["FileAddedCountCreate"]) > 0 && Convert.ToInt32(TempData["FileexistsCountCreate"]) == 0)
                        {
                            MsgType = "success";
                        }
                        if (Convert.ToInt32(TempData["FileAddedCountCreate"]) > 0 && Convert.ToInt32(TempData["FileexistsCountCreate"]) > 0)
                        {
                            MsgType = "info";
                        }
                        transaction.Commit();
                        return Json(new { hasError = false, msg = TempData["FileMsgCreate"], msgType = MsgType }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        if (UploadedFilePath != null)
                        {
                            QMSAttachments objAttachmentsCommon = new QMSAttachments();
                            objAttachmentsCommon.RemovePhysicalPathFile(Server.MapPath("~\\" + UploadedFilePath));
                        }
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M35:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                return Json(new { hasError = true, msg = ErMsg, msgType = "Exceptionerror" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        //CCN Close file Upload
        public JsonResult BindFileUploadForCCNClose()
        {
            try
            {
                if (TempData["FileMsgClose"] == null)
                {
                    TempData["FileMsgClose"] = "";
                }
                if (TempData["FileAddedCountClose"] == null)
                {
                    TempData["FileAddedCountClose"] = 0;
                }
                if (TempData["FileexistsCountClose"] == null)
                {
                    TempData["FileexistsCountClose"] = 0;
                }
                string msg = "";
                string UploadedFilePath = null;
                using (DbContextTransaction transaction = dbEF.Database.BeginTransaction())
                {
                    try
                    {
                        HttpFileCollectionBase files = Request.Files;
                        AizantIT_CCNCloseFileAttachments objFileboclose = new AizantIT_CCNCloseFileAttachments();
                        objFileboclose.CCNID = Convert.ToInt32(Request.Params["CCNID"]);
                        int FileexistsCount = 0;

                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFileBase file = files[i];
                            if (file != null)
                            {
                                objFileboclose.FileName = Path.GetFileName(file.FileName);
                                objFileboclose.FileType = Path.GetExtension(file.FileName);
                                var Count = dbEF.AizantIT_CCNCloseFileAttachments.FirstOrDefault(h => h.FileName == objFileboclose.FileName
                                 && h.FileType == objFileboclose.FileType && h.CCNID == objFileboclose.CCNID);
                                if (Count != null)
                                {
                                    FileexistsCount++;
                                }
                            }
                            if (FileexistsCount == 0)
                            {
                                if (ModelState.IsValid)
                                {
                                    if (file != null)
                                    {
                                        objFileboclose.FileName = Path.GetFileName(file.FileName);
                                        objFileboclose.FileType = Path.GetExtension(file.FileName);
                                        objFileboclose.Createdby = GetCookieEmployeID();
                                        objFileboclose.CreatedDate = DateTime.Now;
                                        QMSAttachments objAttachmentsCommon = new QMSAttachments();
                                        UploadedFilePath = objAttachmentsCommon.SaveUploadedFileinPhysicalPath(file, "CCN Close", 2);
                                        objFileboclose.FilePath = UploadedFilePath;
                                        dbEF.AizantIT_CCNCloseFileAttachments.Add(objFileboclose);
                                        dbEF.SaveChanges();
                                        UploadedFilePath = null;
                                        TempData["FileMsgClose"] += Path.GetFileName(file.FileName) + " File uploaded successfully." + "<br/>";
                                        TempData["FileAddedCountClose"] = Convert.ToInt32(TempData["FileAddedCountClose"]) + 1;
                                    }
                                }
                            }
                            else
                            {
                                TempData["FileMsgClose"] += Path.GetFileName(file.FileName) + " upload failed as file already exists." + "<br/>";
                                TempData["FileexistsCountClose"] = Convert.ToInt32(TempData["FileexistsCount"]) + 1;
                            }
                        }
                        TempData.Keep("FileMsgClose");
                        TempData.Keep("FileexistsCountClose");
                        TempData.Keep("FileAddedCountClose");
                        var MsgType = "";
                        if (Convert.ToInt32(TempData["FileexistsCountClose"]) > 0 && Convert.ToInt32(TempData["FileAddedCountClose"]) == 0)
                        {
                            MsgType = "error";
                        }
                        if (Convert.ToInt32(TempData["FileAddedCountClose"]) > 0 && Convert.ToInt32(TempData["FileexistsCountClose"]) == 0)
                        {
                            MsgType = "success";
                        }
                        if (Convert.ToInt32(TempData["FileAddedCountClose"]) > 0 && Convert.ToInt32(TempData["FileexistsCountClose"]) > 0)
                        {
                            MsgType = "info";
                        }
                        transaction.Commit();
                        return Json(new { hasError = false, msg = TempData["FileMsgClose"], msgType = MsgType }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        if (UploadedFilePath != null)
                        {
                            QMSAttachments objAttachmentsCommon = new QMSAttachments();
                            objAttachmentsCommon.RemovePhysicalPathFile(Server.MapPath("~\\" + UploadedFilePath));
                        }
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M35:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                return Json(new { hasError = true, msg = ErMsg, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        //Get hod assessment file upload count
        public void GetHodAssFileUploadCount(int AssessmentID)
        {

            ViewBag.FilesCountHOD = (from HF in dbEF.AizantIT_HODAssessmentAttachmentFile
                                     where HF.AssessmentID == AssessmentID
                                     select HF).Count();

        }
        // Hod Assessment fileUpload
        [HttpPost]
        public JsonResult BindFileUploadForHODAssessCCN()
        {
            try
            {
                if (TempData["FileMsgCCN"] == null)
                {
                    TempData["FileMsgCCN"] = "";
                }
                if (TempData["FileAddedCountCCN"] == null)
                {
                    TempData["FileAddedCountCCN"] = 0;
                }
                if (TempData["FileexistsCountCCN"] == null)
                {
                    TempData["FileexistsCountCCN"] = 0;
                }
                var MsgType = "";
                string msg = "";
                string UploadedFilePath = null;
                using (DbContextTransaction transaction = dbEF.Database.BeginTransaction())
                {
                    try
                    {
                        HttpFileCollectionBase files = Request.Files;
                        AizantIT_HODAssessmentAttachmentFile objFileboHod = new AizantIT_HODAssessmentAttachmentFile();
                        objFileboHod.CCNID = Convert.ToInt32(Request.Params["CCNID"]);
                        objFileboHod.AssessmentID = Convert.ToInt32(Request.Params["AssessID"]);
                        //Get Department Name and Block Name

                        var AssignedDeptID = (from CC in dbEF.AizantIT_ChangeControlCommitte
                                              join DM in dbEF.AizantIT_DepartmentMaster on CC.DeptID equals DM.DeptID
                                              join B in dbEF.AizantIT_BlockMaster on CC.BlockID equals B.BlockID
                                              where CC.Assessment_ID == objFileboHod.AssessmentID
                                              select new
                                              {
                                                  DM.DepartmentName,
                                                  B.BlockName,
                                              }).SingleOrDefault();

                        var DepartmentName = AssignedDeptID.DepartmentName;
                        var BlockName = AssignedDeptID.BlockName;

                        Boolean Result = ToAssessmentCheckValidEmployeeOrNot((int)objFileboHod.CCNID, (int)objFileboHod.AssessmentID, 25);
                        if (Result == true)
                        {
                            int FileexistsCount = 0;

                            for (int i = 0; i < files.Count; i++)
                            {
                                HttpPostedFileBase file = files[i];
                                if (file != null)
                                {
                                    objFileboHod.FileName = Path.GetFileName(file.FileName);
                                    objFileboHod.FileType = Path.GetExtension(file.FileName);
                                    var Count = dbEF.AizantIT_HODAssessmentAttachmentFile.FirstOrDefault(h => h.FileName == objFileboHod.FileName
                                     && h.FileType == objFileboHod.FileType && h.CCNID == objFileboHod.CCNID && h.AssessmentID == objFileboHod.AssessmentID);
                                    if (Count != null)
                                    {
                                        FileexistsCount++;
                                    }
                                }
                                if (FileexistsCount == 0)
                                {
                                    if (ModelState.IsValid)
                                    {
                                        if (file != null)
                                        {
                                            objFileboHod.FileName = Path.GetFileName(file.FileName);
                                            objFileboHod.FileType = Path.GetExtension(file.FileName);
                                            objFileboHod.Createdby = GetCookieEmployeID();
                                            objFileboHod.RoleID = 25;
                                            QMSAttachments objAttachmentsCommon = new QMSAttachments();
                                            UploadedFilePath = objAttachmentsCommon.SaveUploadedFileinPhysicalPath(file, DepartmentName + '/' + BlockName, 2);
                                            objFileboHod.FilePath = UploadedFilePath;
                                            objFileboHod.CreatedDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-ddThh:mm"));
                                            dbEF.AizantIT_HODAssessmentAttachmentFile.Add(objFileboHod);
                                            dbEF.SaveChanges();
                                            UploadedFilePath = null;
                                            TempData["FileMsgCCN"] += Path.GetFileName(file.FileName) + " File uploaded successfully." + "<br/>";
                                            TempData["FileAddedCountCCN"] = Convert.ToInt32(TempData["FileAddedCount"]) + 1;
                                        }
                                    }
                                }
                                else
                                {
                                    TempData["FileMsgCCN"] += Path.GetFileName(file.FileName) + " upload failed as file already exists." + "<br/>";
                                    TempData["FileexistsCountCCN"] = Convert.ToInt32(TempData["FileexistsCount"]) + 1;
                                }
                            }
                            TempData.Keep("FileMsgCCN");
                            TempData.Keep("FileexistsCountCCN");
                            TempData.Keep("FileAddedCountCCN");

                            if (Convert.ToInt32(TempData["FileexistsCountCCN"]) > 0 && Convert.ToInt32(TempData["FileAddedCountCCN"]) == 0)
                            {
                                MsgType = "error";
                            }
                            if (Convert.ToInt32(TempData["FileAddedCountCCN"]) > 0 && Convert.ToInt32(TempData["FileexistsCountCCN"]) == 0)
                            {
                                MsgType = "success";
                            }
                            if (Convert.ToInt32(TempData["FileAddedCountCCN"]) > 0 && Convert.ToInt32(TempData["FileexistsCountCCN"]) > 0)
                            {
                                MsgType = "info";
                            }
                        }
                        else
                        {
                            return Json(new { hasError = true, msg = 1, msgType = "error" }, JsonRequestBehavior.AllowGet);
                        }
                        transaction.Commit();
                        return Json(new { hasError = false, msg = TempData["FileMsgCCN"], msgType = MsgType }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        if (UploadedFilePath != null)
                        {
                            QMSAttachments objAttachmentsCommon = new QMSAttachments();
                            objAttachmentsCommon.RemovePhysicalPathFile(Server.MapPath("~\\" + UploadedFilePath));
                        }
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M35:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                return Json(new { hasError = true, msg = ErMsg, msgType = "Exceptionerror" }, JsonRequestBehavior.AllowGet);
            }
        }

        //Remove temp data values
        [HttpGet]
        public JsonResult RemoveTempData(string RoleTypeTemp)
        {
            try
            {
                if (RoleTypeTemp == "verify")
                {
                    TempData.Remove("FileMsg");
                    TempData.Remove("FileexistsCount");
                    TempData.Remove("FileAddedCount");
                }
                if (RoleTypeTemp == "HodAssess")
                {
                    TempData.Remove("FileMsgCCN");
                    TempData.Remove("FileexistsCountCCN");
                    TempData.Remove("FileAddedCountCCN");
                }
                if (RoleTypeTemp == "CloseCCN")
                {
                    TempData.Remove("FileMsgClose");
                    TempData.Remove("FileexistsCountClose");
                    TempData.Remove("FileAddedCountClose");
                }
                if (RoleTypeTemp == "InitateFile")
                {
                    TempData.Remove("FileMsgCreate");
                    TempData.Remove("FileexistsCountCreate");
                    TempData.Remove("FileAddedCountCreate");

                }
                return Json(new { hasError = false }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { hasError = true, data = HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);

            }

        }
        //Delete file upload files
        public ActionResult DeletedCCNFileUploadDocuments(int? AttachmentID, string RoleType)
        {
            try
            {
                if (RoleType == "Initiator")
                {

                    var GetFiles = (from HA in dbEF.AizantIT_CCNInitiatorFileAttachment
                                    where HA.UniquePKID == AttachmentID
                                    select HA).SingleOrDefault();
                    AizantIT_CCNHistory objhistroy = new AizantIT_CCNHistory();

                    objhistroy.CCNID = GetFiles.CCNID;
                    objhistroy.ActionBy = GetCookieEmployeID();
                    objhistroy.ActionStatus = 41;
                    objhistroy.Comments = "Removed File Name:" + GetFiles.FileName;
                    objhistroy.RoleID = 24;
                    objhistroy.ActionDate = DateTime.Now;
                    objhistroy.AssignedTo = 0;
                    objhistroy.AssignedToRoleID = 0;

                    var _ObjDetete = dbEF.AizantIT_CCNInitiatorFileAttachment.Where(a => a.UniquePKID == AttachmentID).ToList();
                    //Removed file path in physical folder
                    QMSAttachments objAttachmentsCommon = new QMSAttachments();
                    objAttachmentsCommon.RemovePhysicalPathFile(Server.MapPath("~\\" + GetFiles.FilePath));
                    dbEF.AizantIT_CCNInitiatorFileAttachment.Remove(_ObjDetete[0]);
                    //History for delete file
                    if (GetFiles.Istemp == true)
                    {
                        dbEF.AizantIT_CCNHistory.Add(objhistroy);
                    }
                    dbEF.SaveChanges();
                }
                if (RoleType == "HOD")
                {
                    var GetFiles = (from HA in dbEF.AizantIT_HODAssessmentAttachmentFile
                                    where HA.UniquePKID == AttachmentID
                                    select HA).SingleOrDefault();
                    AizantIT_CCNHistory objhistroy = new AizantIT_CCNHistory();

                    objhistroy.CCNID = GetFiles.CCNID;
                    objhistroy.ActionBy = GetCookieEmployeID();
                    objhistroy.ActionStatus = 38;
                    objhistroy.Comments = "Removed File Name:" + GetFiles.FileName;
                    objhistroy.RoleID = 25;
                    objhistroy.ActionDate = DateTime.Now;
                    objhistroy.AssignedTo = 0;
                    objhistroy.AssignedToRoleID = 0;

                    var _ObjDetete = dbEF.AizantIT_HODAssessmentAttachmentFile.Where(a => a.UniquePKID == AttachmentID).ToList();
                    QMSAttachments objAttachmentsCommon = new QMSAttachments();
                    objAttachmentsCommon.RemovePhysicalPathFile(Server.MapPath("~\\" + GetFiles.FilePath));

                    dbEF.AizantIT_HODAssessmentAttachmentFile.Remove(_ObjDetete[0]);
                    if (GetFiles.isTemp == true)
                    {
                        //History for delete file
                        dbEF.AizantIT_CCNHistory.Add(objhistroy);
                    }
                    dbEF.SaveChanges();
                }
                if (RoleType == "Close")
                {
                    var GetFiles = (from HA in dbEF.AizantIT_CCNCloseFileAttachments
                                    where HA.UniquePKID == AttachmentID
                                    select HA).SingleOrDefault();
                    AizantIT_CCNHistory objhistroy = new AizantIT_CCNHistory();
                    if (GetFiles != null)
                    {
                        objhistroy.CCNID = GetFiles.CCNID;
                        objhistroy.ActionBy = GetCookieEmployeID();
                        objhistroy.ActionStatus = 39;
                        objhistroy.Comments = "Removed File Name:" + GetFiles.FileName;
                        objhistroy.RoleID = 26;
                        objhistroy.ActionDate = DateTime.Now;
                        objhistroy.AssignedTo = 0;
                        objhistroy.AssignedToRoleID = 0;
                    }

                    var _ObjDetete = dbEF.AizantIT_CCNCloseFileAttachments.Where(a => a.UniquePKID == AttachmentID).ToList();
                    QMSAttachments objAttachmentsCommon = new QMSAttachments();
                    objAttachmentsCommon.RemovePhysicalPathFile(Server.MapPath("~\\" + GetFiles.FilePath));

                    dbEF.AizantIT_CCNCloseFileAttachments.Remove(_ObjDetete[0]);
                    //History for delete file
                    if (GetFiles.isTemp == true)
                    {
                        dbEF.AizantIT_CCNHistory.Add(objhistroy);
                    }
                    dbEF.SaveChanges();
                }
                return Json(new { hasError = false, data = AttachmentID }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M36:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        //Get Hod Assessment Uploaded list 
        [HttpGet]
        public JsonResult GetHodAssUploadList()
        {
            try
            {
                TempData.Keep("IsTempFileID");
                int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                int CCNID = Convert.ToInt16(Request.Params["CCNNumber"]);
                if (CCNID == 0)
                {
                    if (TempData["IsTempFileID"] != "0")
                    {
                        CCNID = Convert.ToInt32(TempData["IsTempFileID"]);
                    }
                }
                int AssessID = Convert.ToInt32(Request.Params["AssessID"]);
                string Status = (Request.Params["Status"]).ToString();
                int filteredCount = 0;
                List<CCNMaster> CCNFilesList = new List<CCNMaster>();
                int totalRecordsCount = 0;
                int EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                DataSet dthodAttach = Qms_bal.GetHodAttachments(displayLength, displayStart, sortCol, sSortDir, sSearch, CCNID, AssessID, Status);
                if (dthodAttach.Tables[0].Rows.Count > 0)
                {
                    totalRecordsCount = Convert.ToInt32(dthodAttach.Tables[0].Rows[0]["TotalCount"]);
                    foreach (DataRow rdr in dthodAttach.Tables[0].Rows)
                    {
                        CCNMaster objccn = new CCNMaster();
                        objccn.RowNumberHodAss = Convert.ToInt32(rdr["RowNumber"]);
                        objccn.CCNHodFileUniqueID = Convert.ToInt32(rdr["UniquePKID"]);
                        objccn.FileNameHod = (rdr["Document"].ToString());
                        objccn.FileTypeHod = (rdr["FileType"].ToString());
                        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                        CCNFilesList.Add(objccn);
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = CCNFilesList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());

                return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);
            }
        }

        //For Getting CCN Department wise Assessment List
        [HttpGet]
        public JsonResult GetCCN_DeptAssementList()
        {
            try
            {
                int iDisplayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int iDisplayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int iSortCol_0 = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir_0 = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];

                int CCN_No = Convert.ToInt16(Request.Params["CCN_No"]);
                List<ChangeControlCommitte> listEmployees = new List<ChangeControlCommitte>();
                int filteredCount = 0;

                CCNBal qMS_BAL = new CCNBal();
                DataSet dt = qMS_BAL.GetCCN_DeptAssBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, CCN_No);

                int totalRecordsCount = 0;
                if (dt.Tables[0].Rows.Count > 0)
                {

                    totalRecordsCount = Convert.ToInt32(dt.Tables[0].Rows[0]["TotalCount"]);
                    foreach (DataRow rdr in dt.Tables[0].Rows)
                    {
                        ChangeControlCommitte objCCC = new ChangeControlCommitte();
                        objCCC.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                        objCCC.UniqueID = Convert.ToInt32(rdr["UniqueID"]);
                        objCCC.BlockID = Convert.ToInt32(rdr["BlockID"].ToString());
                        objCCC.BlockName = (rdr["BlockName"].ToString());
                        objCCC.EmpID = Convert.ToInt32(rdr["EmpID"].ToString());
                        objCCC.EmployeeName = (rdr["EmmployeeName"]).ToString();
                        objCCC.DeptName = (rdr["DeptName"]).ToString();
                        objCCC.DeptID = (rdr["DeptID"].ToString());
                        objCCC.CuurentStatus = (rdr["CurrentStatus"].ToString());
                        objCCC.CCN_ID = Convert.ToInt32(rdr["CCN_ID"].ToString());
                        objCCC.CurrentStatusNumber = (int)rdr["curentStatusNumber"];//Using this display icons in CNHODApprove Page
                        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                        listEmployees.Add(objCCC);
                    }
                }
                return Json(new
                {
                    hasError = false,
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = filteredCount,
                    aaData = listEmployees
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = "Yes";

                return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);
            }
        }
        //Refresh Verification Page Remove Deleted Is Temp Files
        public void RefershCCNDeletedFilesIsTempIsFalse(int ID, string RoleType)
        {
            if (RoleType == "HOD")
            {
                var objHODassessment = (from IFile in dbEF.AizantIT_HODAssessmentAttachmentFile
                                        where IFile.AssessmentID == ID && IFile.isTemp == false
                                        select IFile).ToList();
                //var _lstDel = dbEF.AizantIT_HODAssessmentAttachmentFile.Where(s => s.AssessmentID == ID && s.isTemp == false);
                if (objHODassessment.Count > 0)
                {
                    QMSAttachments objAttachmentsCommon = new QMSAttachments();
                    foreach (var item in objHODassessment)
                    {
                        objAttachmentsCommon.RemovePhysicalPathFile(Server.MapPath("~\\" + item.FilePath));
                    }
                    //dbEF.AizantIT_HODAssessmentAttachmentFile.RemoveRange(_lstDel);
                    dbEF.AizantIT_HODAssessmentAttachmentFile.RemoveRange(objHODassessment);
                    dbEF.SaveChanges();
                }

            }
            if (RoleType == "Close")
            {
                var objClose = (from IFile in dbEF.AizantIT_CCNCloseFileAttachments
                                where IFile.CCNID == ID && IFile.isTemp == false
                                select IFile).ToList();
                if (objClose.Count > 0)
                {
                    foreach (var item in objClose)
                    {
                        QMSAttachments objAttachmentsCommon = new QMSAttachments();
                        objAttachmentsCommon.RemovePhysicalPathFile(Server.MapPath("~\\" + item.FilePath));
                    }
                    dbEF.AizantIT_CCNCloseFileAttachments.RemoveRange(objClose);
                    dbEF.SaveChanges();
                }
            }
            if (RoleType == "Update")
            {
                var initiatorFile = (from IFile in dbEF.AizantIT_CCNInitiatorFileAttachment
                                     where IFile.CCNID == ID && IFile.Istemp == false
                                     select IFile).ToList();
                if (initiatorFile.Count > 0)
                {
                    QMSAttachments objAttachmentsCommon = new QMSAttachments();
                    foreach (var item in initiatorFile)
                    {
                        objAttachmentsCommon.RemovePhysicalPathFile(Server.MapPath("~\\" + item.FilePath));
                        //objAttachmentsCommon.RemovePhysicalPathFile(item.FilePath);
                    }
                    dbEF.AizantIT_CCNInitiatorFileAttachment.RemoveRange(initiatorFile);
                    dbEF.SaveChanges();
                }

            }
        }

        //CCN Verification File upload list
       // [HttpGet]
       // public JsonResult GetCCNFileUploadList(int iDisplayLength, int iDisplayStart, int iSortCol_0,
       //string sSortDir_0, string sSearch, int CCNNumber)
       // {
       //     try
       //     {
       //         List<CCN_Verification> CCNFilesList = new List<CCN_Verification>();
       //         int filteredCount = 0;
       //         QMS_BAL objbal = new QMS_BAL();
       //         DataSet dt = objbal.GetCCNFileUploadListBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, CCNNumber);
       //         if (dt.Tables[0].Rows.Count > 0)
       //         {
       //             foreach (DataRow rdr in dt.Tables[0].Rows)
       //             {
       //                 CCN_Verification objccn = new CCN_Verification();
       //                 objccn.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
       //                 objccn.CCNFileUniqueID = Convert.ToInt32(rdr["UniquePKID"]);
       //                 objccn.FileName = (rdr["Document"].ToString());
       //                 objccn.FileType = (rdr["FileType"].ToString());
       //                 filteredCount = Convert.ToInt32(rdr["TotalCount"]);
       //                 CCNFilesList.Add(objccn);
       //             }
       //         }
       //         return Json(new
       //         {
       //             hassError = false,
       //             iTotalRecords = dt.Tables[0].Rows.Count,
       //             iTotalDisplayRecords = filteredCount,
       //             aaData = CCNFilesList
       //         }, JsonRequestBehavior.AllowGet);
       //     }
       //     catch (Exception ex)
       //     {
       //         int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
       //         return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);
       //     }
       // }

        #endregion

        #region NotUsingCodingCheckandRemove

        public PartialViewResult CCN_Review(int id, string Number, string R)
        {
            CCNMaster CCNMasterBO = new CCNMaster();
            CCNHODApprove(id, null, null);
            return PartialView(CCNMasterBO);
        }

        #region Verification Not Using

        public PartialViewResult CreateVerification(int id)
        {
            try
            {
                var RoleType = "Verify";
                RefershCCNDeletedFilesIsTempIsFalse(id, RoleType);

                CCN_Verification cCN_Verification = new CCN_Verification();
                var CCNNO = dbEF.AizantIT_ChangeControlMaster.Find(id);
                cCN_Verification.CCN_Number = CCNNO.CCN_No;
                cCN_Verification.CCNID = id;
                ViewBag.CCNDate = Convert.ToDateTime(CCNNO.CCN_Date).ToString("dd MMM yyyy");
                return PartialView(cCN_Verification);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M16:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }

        public PartialViewResult ViewVerification(int id)
        {
            try
            {
                CCN_Verification cCN_Verification = new CCN_Verification();
                var verify = dbEF.AizantIT_VerificationOfChangeEffectiveness.Find(id);
                var CCNNO = dbEF.AizantIT_ChangeControlMaster.Where(a => a.CCN_ID == verify.CCN_ID).ToList().FirstOrDefault();
                cCN_Verification.CCN_Number = CCNNO.CCN_No;
                cCN_Verification.Comments = verify.Comments;
                cCN_Verification.Implementation_Date = Convert.ToDateTime(verify.DateOfImplementation).ToString("dd-MMM-yyyy");

                return PartialView(cCN_Verification);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M17:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }
        #endregion
        #endregion

        #region Re-Assign employee
        public PartialViewResult _CCNManege(int CCNID)
        {
            CCNMaster objbo = new CCNMaster();
            //Retrieve ccn Details
            objbo = GetCCNdata(CCNID);
            if (objbo.CategoryofChange == 1)
            {
                objbo.CategoryofChangeName = "Permanent";
            }
            else if (objbo.CategoryofChange == 2)
            {
                objbo.CategoryofChangeName = "Temporary";
            }

            List<CCNMaster> bindddl = new List<CCNMaster>();
            DataSet ds = objbal.GetEmployeesDeptandRoleWiseInAdminManageBAL(objbo.DeptID, 2);
            for (int i = 0; i < 4; i++)
            {
                //Initiator
                if (i == 0)
                {
                    List<SelectListItem> objAssignedInitiator = new List<SelectListItem>();
                    objAssignedInitiator.Add(new SelectListItem
                    {
                        Text = "Select",
                        Value = "0",
                    });
                    foreach (DataRow rdr in ds.Tables[0].Rows)
                    {
                        objAssignedInitiator.Add(new SelectListItem
                        {
                            Text = (rdr["EmpName"]).ToString(),
                            Value = (rdr["EmpID"]).ToString(),
                            Selected = (Convert.ToInt32(rdr["EmpID"]) == objbo.Initiator_EmpID ? true : false)
                        });
                    }
                    //Initiator drop down
                    bindddl.Add(new CCNMaster
                    {
                        ddlAdminManageRoles = objAssignedInitiator.ToList(),
                        AdminMangeRoleName = "Initiator",
                        SelectedAssessmentHOD = objbo.Initiator_EmpID,
                        SelectedEmployee = GetFullName(objbo.Initiator_EmpID)
                    });
                }
                //HOD
                if (i == 1)
                {
                    //HOD- drop down
                    List<SelectListItem> objAssignHod = new List<SelectListItem>();
                    objAssignHod.Add(new SelectListItem
                    {
                        Text = "Select",
                        Value = "0",
                    });
                    foreach (DataRow rdr in ds.Tables[1].Rows)
                    {
                        objAssignHod.Add(new SelectListItem
                        {
                            Text = (rdr["EmpName"]).ToString(),
                            Value = (rdr["EmpID"]).ToString(),
                            Selected = (Convert.ToInt32(rdr["EmpID"]) == objbo.HOD_EmpID ? true : false)
                        });
                    }
                    bindddl.Add(new CCNMaster
                    {
                        ddlAdminManageRoles = objAssignHod.ToList(),
                        AdminMangeRoleName = "HOD",
                        SelectedAssessmentHOD = objbo.HOD_EmpID,
                        SelectedEmployee = GetFullName(objbo.HOD_EmpID)

                    });
                }
                //QA
                if (i == 2)
                {
                    List<int> QaEmployeeid = new List<int>() { objbo.Initiator_EmpID, objbo.HOD_EmpID, objbo.HeadQA_EmpID };
                    List<SelectListItem> AssignedQAList = new List<SelectListItem>();
                    AssignedQAList.Add(new SelectListItem
                    {
                        Text = "Select",
                        Value = "0",
                    });
                    foreach (DataRow rdr in ds.Tables[2].Rows)
                    {
                        if (!QaEmployeeid.Contains((int)(rdr["EmpID"])))
                        {
                            AssignedQAList.Add(new SelectListItem
                            {
                                Text = (rdr["EmpName"]).ToString(),
                                Value = (rdr["EmpID"]).ToString(),
                                Selected = (Convert.ToInt32(rdr["EmpID"]) == objbo.QA_EmpID ? true : false)
                            });
                        }
                    }
                    bindddl.Add(new CCNMaster
                    {
                        ddlAdminManageRoles = AssignedQAList.ToList(),
                        AdminMangeRoleName = "QA",
                        SelectedAssessmentHOD = objbo.QA_EmpID,
                        SelectedEmployee = GetFullName(objbo.QA_EmpID)
                    });
                }
                //HeadQA
                if (i == 3)
                {
                    List<int> HeadQAEmployeeid = new List<int>() { objbo.Initiator_EmpID, objbo.HOD_EmpID, objbo.QA_EmpID };
                    List<SelectListItem> AssignedHQAList = new List<SelectListItem>();
                    AssignedHQAList.Add(new SelectListItem
                    {
                        Text = "Select",
                        Value = "0",
                    });
                    foreach (DataRow rdr in ds.Tables[3].Rows)
                    {
                        if (!HeadQAEmployeeid.Contains((int)(rdr["EmpID"])))
                        {
                            AssignedHQAList.Add(new SelectListItem
                            {
                                Text = (rdr["EmpName"]).ToString(),
                                Value = (rdr["EmpID"]).ToString(),
                                Selected = (Convert.ToInt32(rdr["EmpID"]) == objbo.HeadQA_EmpID ? true : false)
                            });
                        }
                    }
                    bindddl.Add(new CCNMaster
                    {
                        ddlAdminManageRoles = AssignedHQAList.ToList(),
                        AdminMangeRoleName = "HeadQA",
                        SelectedAssessmentHOD = objbo.HeadQA_EmpID,
                        SelectedEmployee = GetFullName(objbo.HeadQA_EmpID)
                    });
                }
            }

            ViewBag.GetManageRolesList = bindddl.AsEnumerable();

            ViewBag.AssessmentExistOrNot = (from CC in dbEF.AizantIT_ChangeControlCommitte
                                            where CC.CCN_ID == CCNID
                                            select CC).Count();
            return PartialView(objbo);
        }
        public string GetFullName(int EmpId)
        {
            var EmployeeName = (from E in dbEF.AizantIT_EmpMaster
                                where E.EmpID == EmpId
                                select (E.FirstName + "" + E.LastName)).SingleOrDefault();
            return EmployeeName;
        }
        [HttpGet]
        public JsonResult GetAssementForCCNManaging()
        {
            try
            {
                int iDisplayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int iDisplayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int iSortCol_0 = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir_0 = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];

                int CCN_No = Convert.ToInt16(Request.Params["CCN_No"]);
                List<ChangeControlCommitte> listEmployees = new List<ChangeControlCommitte>();
                int filteredCount = 0;
                //QA Not Binded in Assigned HOD's
                int NotBindQA = -1;
                var NotbindingQAHQA = (from C in dbEF.AizantIT_ChangeControlMaster
                                       where C.CCN_ID == CCN_No
                                       select new { C.QA_EmpID, C.HeadQA_EmpID }).ToList();
                foreach (var item in NotbindingQAHQA)
                {
                    NotBindQA = Convert.ToInt32(item.QA_EmpID);
                    //NotBindHQA = Convert.ToInt32(item.HeadQA_EmpID);
                }
                List<int> NotBindingLoginEmpId = new List<int> { NotBindQA /*, NotBindHQA*/ };
                CCNBal qMS_BAL = new CCNBal();
                DataSet dt = qMS_BAL.GetCCN_DeptAssBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, CCN_No);

                int totalRecordsCount = 0;
                if (dt.Tables[0].Rows.Count > 0)
                {

                    totalRecordsCount = Convert.ToInt32(dt.Tables[0].Rows[0]["TotalCount"]);
                    foreach (DataRow rdr in dt.Tables[0].Rows)
                    {
                        ChangeControlCommitte objCCC = new ChangeControlCommitte();
                        objCCC.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                        objCCC.UniqueID = Convert.ToInt32(rdr["UniqueID"]);
                        objCCC.BlockID = Convert.ToInt32(rdr["BlockID"].ToString());
                        objCCC.BlockName = (rdr["BlockName"].ToString());
                        objCCC.EmpID = Convert.ToInt32(rdr["EmpID"].ToString());
                        objCCC.EmployeeName = (rdr["EmmployeeName"]).ToString();
                        objCCC.DeptName = (rdr["DeptName"]).ToString();
                        objCCC.DeptID = (rdr["DeptID"].ToString());
                        objCCC.CuurentStatus = (rdr["CurrentStatus"].ToString());
                        objCCC.CCN_ID = Convert.ToInt32(rdr["CCN_ID"].ToString());
                        objCCC.CurrentStatusNumber = (int)rdr["curentStatusNumber"];//Using this display icons in CNHODApprove Page
                        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                        //HOD Drop down
                        int DepartmentID = Convert.ToInt32(objCCC.DeptID);

                        List<SelectListItem> objAssignAssessmentHod = BindHODDropdownAccessibleDepartmentsandRole(DepartmentID, (int)QMS_Roles.HOD, objCCC.EmpID, NotBindingLoginEmpId);
                        objCCC.ddlAssessmentHODs = objAssignAssessmentHod.ToList();
                        listEmployees.Add(objCCC);
                    }
                }
                return Json(new
                {
                    hasError = false,
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = filteredCount,
                    aaData = listEmployees
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                //ViewBag.SysError = "Yes";
                return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult AssessmentModifyAdmin(int EmployeeID, int AssessmentID, int CCNID, string AssessmentComments)
        {
            try
            {
                int ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                int i = Qms_bal.UpdateAssessmentHODsBal(EmployeeID, AssessmentID, CCNID, ActionBy, AssessmentComments, out int Result);

                if (Result == 0)
                {
                    return Json(new { hasError = false, msgType = "success" }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    return Json(new { hasError = true, msgType = "warning", data = "Modification of Employee is Restricted, as the Transaction is Completed." }, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { hasError = true, msgType = "error", data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public JsonResult ModifyEmployeesManageRoles(int EmployeeID, int CCNID, int Status, string ReassignComments)
        {
            try
            {
                int ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                int i = Qms_bal.ModifiedAssignedEmployeeInAdminBAL(EmployeeID, CCNID, Status, ActionBy, ReassignComments, out int Result);
                if (Result == 0)
                {
                    return Json(new { hasError = false, msgType = "Success" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { hasError = true, msgType = "warning", data = "Modification of Employee is Restricted, as the Transaction is Completed." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { hasError = true, msgType = "error", data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);
            }

        }
        public bool ToCheckValidEmployeeOrNot(int ccnid, int RoleID)
        {
            int Initiator = 0, HOD = 0, QA = 0, HeadQA = 0;
            Boolean i = true;
            int LoginEmployeeID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            var GetDetails = (from CM in dbEF.AizantIT_ChangeControlMaster
                              where CM.CCN_ID == ccnid
                              select new
                              {
                                  CM.Initiator_EmpID,
                                  CM.HOD_EmpID,
                                  CM.QA_EmpID,
                                  CM.HeadQA_EmpID

                              }).ToList();
            foreach (var item in GetDetails)
            {
                Initiator = (int)item.Initiator_EmpID;
                HOD = (int)item.HOD_EmpID;
                QA = item.QA_EmpID == null ? 0 : (int)item.QA_EmpID;
                HeadQA = item.HeadQA_EmpID == null ? 0 : (int)item.HeadQA_EmpID;

            }
            if (RoleID == 24)
            {
                if (LoginEmployeeID == Initiator)
                {
                    i = true; //Success
                }
                else
                {
                    i = false;//Fail
                }
            }
            else if (RoleID == 25)
            {
                if (LoginEmployeeID == HOD)
                {
                    i = true; //Success
                }
                else
                {
                    i = false;//Fail
                }
            }
            else if (RoleID == 26)
            {
                if (LoginEmployeeID == QA)
                {
                    i = true; //Success
                }
                else
                {
                    i = false;//Fail
                }
            }
            else if (RoleID == 27)
            {
                if (LoginEmployeeID == HeadQA)
                {
                    i = true; //Success
                }
                else
                {
                    i = false;//Fail
                }
            }
            return i;
        }
        public bool ToAssessmentCheckValidEmployeeOrNot(int ccnid, int AssessmentID, int RoleId)
        {
            int HOD = 0, QA = 0, HEadQA = 0; ;
            Boolean i = true;
            int LoginEmployeeID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            var GetDetails = (from Ccc in dbEF.AizantIT_ChangeControlCommitte
                              join CM in dbEF.AizantIT_ChangeControlMaster on Ccc.CCN_ID equals CM.CCN_ID
                              where Ccc.CCN_ID == ccnid && Ccc.Assessment_ID == AssessmentID
                              select new
                              {
                                  Ccc.AssigndToID,
                                  CM.QA_EmpID,
                                  CM.HeadQA_EmpID,
                              }).ToList();
            foreach (var item in GetDetails)
            {
                HOD = (int)item.AssigndToID;
                QA = (int)item.QA_EmpID;
                HEadQA = item.HeadQA_EmpID == null ? 0 : (int)item.HeadQA_EmpID; ;
            }
            if (RoleId == 25)
            {
                if (LoginEmployeeID == HOD)
                {
                    i = true; //Success
                }
                else
                {
                    i = false;//Fail
                }
            }
            if (RoleId == 26)
            {
                if (LoginEmployeeID == QA)
                {
                    i = true; //Success
                }
                else
                {
                    i = false;//Fail
                }
            }
            if (RoleId == 27)
            {
                if (LoginEmployeeID == HEadQA)
                {
                    i = true; //Success
                }
                else
                {
                    i = false;//Fail
                }
            }
            return i;
        }
        #endregion

        #region ReportClass
        //For Reports
        public class ClassificationBO
        {
            public string Classification { get; set; }
            public string QaSummary { get; set; }
            public string QaName { get; set; }
            public string QASubmittedDate { get; set; }
            public string HeadQASummary { get; set; }
            public string HeadQaName { get; set; }
            public string HeadQaSubmittedDate { get; set; }
            public string CloseEffectiveDate { get; set; }
            public string CloseComments { get; set; }
            public string CloseQAName { get; set; }
            public string CloseQaSubmittedDate { get; set; }
            public string VerifyImplementationDate { get; set; }
            public string VerifyComments { get; set; }
            public string VerifyQAName { get; set; }
            public string VerifyQaSubmittedDate { get; set; }
            public string VerifyDesignation { get; set; }
            public string CloseDesignation { get; set; }
            public string HEadQaDesignation { get; set; }
            public string QaDesignation { get; set; }
        }
        #endregion

        [HttpGet]
        public JsonResult GetCommentsData(int HistoryID)
        {
            try
            {

                var Comments = (from CH in dbEF.AizantIT_CCNHistory
                                where CH.CCNHistoryID == HistoryID
                                select CH.Comments).SingleOrDefault();
                return Json(new { hasError = false, data = Comments }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M5:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetFileUploadList()
        {
            try
            {
                TempData.Keep("IsTempFileID");
                int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                int CCNID = Convert.ToInt16(Request.Params["EventUniqueID"]);
                if (CCNID == 0)
                {
                    if (TempData["IsTempFileID"] != "0")
                    {
                        CCNID = Convert.ToInt32(TempData["IsTempFileID"]);
                    }
                }
                int AssessID = Convert.ToInt32(Request.Params["AssessID"]);
                string Status = (Request.Params["Status"]).ToString();
                int filteredCount = 0;
                List<CCNMaster> CCNFilesList = new List<CCNMaster>();
                int totalRecordsCount = 0;
                int EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                DataSet dthodAttach = Qms_bal.GetHodAttachments(displayLength, displayStart, sortCol, sSortDir, sSearch, CCNID, AssessID, Status);
                if (dthodAttach.Tables[0].Rows.Count > 0)
                {
                    totalRecordsCount = Convert.ToInt32(dthodAttach.Tables[0].Rows[0]["TotalCount"]);
                    foreach (DataRow rdr in dthodAttach.Tables[0].Rows)
                    {
                        CCNMaster objccn = new CCNMaster();
                        objccn.RowNumberHodAss = Convert.ToInt32(rdr["RowNumber"]);
                        objccn.CCNHodFileUniqueID = Convert.ToInt32(rdr["UniquePKID"]);
                        objccn.FileNameHod = (rdr["Document"].ToString());
                        objccn.FileTypeHod = (rdr["FileType"].ToString());
                        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                        CCNFilesList.Add(objccn);
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = CCNFilesList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());

                return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);
            }
        }

        #region All attachments are make as Single
        public void MakeCCNAttachmentsToSingleFile(int CCNID)
        {
            List<string> objCCNAttachmentFilePaths = new List<string> { };
            var InitiatorAttachmentFiles = (from t1 in dbEF.AizantIT_ChangeControlMaster
                                            join t2 in dbEF.AizantIT_CCNInitiatorFileAttachment on t1.CCN_ID equals t2.CCNID
                                            where t1.CCN_ID == CCNID
                                            select t2).ToList();
            foreach (var item in InitiatorAttachmentFiles)
            {
                objCCNAttachmentFilePaths.Add(item.FilePath);
            }
            var AssessmentAttachmentFiles = (from t1 in dbEF.AizantIT_ChangeControlMaster
                                             join t3 in dbEF.AizantIT_HODAssessmentAttachmentFile on t1.CCN_ID equals t3.CCNID
                                             where t1.CCN_ID == CCNID
                                             select t3).ToList();
            foreach (var item in AssessmentAttachmentFiles)
            {
                objCCNAttachmentFilePaths.Add(item.FilePath);
            }
            var CCNCapaCompleteAttachmentFiles = (from t1 in dbEF.AizantIT_ChangeControlCommitte
                                                  join t2 in dbEF.AizantIT_AssessmentCAPA on t1.Assessment_ID equals t2.AssessmentId
                                                  join t3 in dbEF.AizantIT_CapaCompltFileAttachments on t2.CAPAID equals t3.CapaID
                                                  where t1.CCN_ID == CCNID
                                                  select t3).ToList();
            foreach (var item in CCNCapaCompleteAttachmentFiles)
            {
                objCCNAttachmentFilePaths.Add(item.FilePath);
            }
            var CCNCloseAttachmentFiles = (from t1 in dbEF.AizantIT_ChangeControlMaster
                                           join t4 in dbEF.AizantIT_CCNCloseFileAttachments on t1.CCN_ID equals t4.CCNID
                                           where t1.CCN_ID == CCNID
                                           select t4).ToList();
            foreach (var item in CCNCloseAttachmentFiles)
            {
                objCCNAttachmentFilePaths.Add(item.FilePath);
            }


            var objAlreadyMergedCCNAttachments = (from item in dbEF.AizantIT_CCNFinalReports
                                                  where item.CCNID == CCNID
                                                  select item).SingleOrDefault();
            if (objAlreadyMergedCCNAttachments != null) //There's already a Merged Attachments File
            {
                if (objAlreadyMergedCCNAttachments.AttachmentFilePath != null)
                {
                    //Delete old Merged File.
                    QMSAttachments objAttachmentsCommon = new QMSAttachments();
                    objAttachmentsCommon.RemovePhysicalPathFile(Server.MapPath("~\\" + objAlreadyMergedCCNAttachments.AttachmentFilePath));
                }

                //Update Newly Merged File
                if (objCCNAttachmentFilePaths.Count > 0)
                {
                    AizantIT_CCNFinalReports objCCNAttachmentAsSingle =
                           dbEF.AizantIT_CCNFinalReports.SingleOrDefault(p => p.CCNID == CCNID);
                    QMSAttachments objAttachmentsCommon = new QMSAttachments();
                    objCCNAttachmentAsSingle.AttachmentFilePath = objAttachmentsCommon.MergeAllTheFilesAsSingleFile(objCCNAttachmentFilePaths, "Areas\\QMS\\Models\\ChangeControl\\CCNAttachments\\MergedAttachments", CCNID + DateTime.Now.ToString("yyyMMddHHmmssfff") + ".pdf");
                }
            }
            else //Create new Merge File
            {
                if (objCCNAttachmentFilePaths.Count > 0)
                {
                    AizantIT_CCNFinalReports objCCNAttachmentAsSingle = new AizantIT_CCNFinalReports();
                    objCCNAttachmentAsSingle.CCNID = CCNID;
                    objCCNAttachmentAsSingle.CreatedDate = DateTime.Now;
                    QMSAttachments objAttachmentsCommon = new QMSAttachments();
                    objCCNAttachmentAsSingle.AttachmentFilePath = objAttachmentsCommon.MergeAllTheFilesAsSingleFile(objCCNAttachmentFilePaths, "Areas\\QMS\\Models\\ChangeControl\\CCNAttachments\\MergedAttachments", CCNID + DateTime.Now.ToString("yyyMMddHHmmssfff") + ".pdf");
                    dbEF.AizantIT_CCNFinalReports.Add(objCCNAttachmentAsSingle);
                }
            }
            dbEF.SaveChanges();
        }
        #endregion



        #region Final report and Attachments Merged
        public void GenerateFinalCCNReport(string onlyReportPath, int CCNID)
        {

            List<string> ObjCCNFilesToMerge = new List<string>();
            ObjCCNFilesToMerge.Insert(0, onlyReportPath);
            var objCCNListAttachment = (from t1 in dbEF.AizantIT_CCNFinalReports
                                        where t1.CCNID == CCNID
                                        select t1).ToList();
            if (objCCNListAttachment != null)
            {
                foreach (var item in objCCNListAttachment)
                {
                    ObjCCNFilesToMerge.Insert(1, item.AttachmentFilePath);
                }
            }
            var objFinalReportAlredayExistsorNot = (from t1 in dbEF.AizantIT_CCNFinalReports
                                                    where t1.CCNID == CCNID
                                                    select t1).SingleOrDefault();
            if (objFinalReportAlredayExistsorNot.FinalReport != null)
            {
                //Delete old Merged File.
                QMSAttachments objFinalReport = new QMSAttachments();
                objFinalReport.RemovePhysicalPathFile(Server.MapPath("~\\" + objFinalReportAlredayExistsorNot.FinalReport));
            }

            // MergeAllTheFilesAsSingleFile,Update the DB with File path.
            AizantIT_CCNFinalReports objAizantITReport = new AizantIT_CCNFinalReports();
            objAizantITReport = dbEF.AizantIT_CCNFinalReports.SingleOrDefault(x => x.CCNID == CCNID);
            QMSAttachments objAttachmentsCommon = new QMSAttachments();
            objAizantITReport.FinalReport = objAttachmentsCommon.MergeAllTheFilesAsSingleFile(ObjCCNFilesToMerge, "Areas\\QMS\\Models\\ChangeControl\\Reports\\FinalReport", CCNID + DateTime.Now.ToString("yyyMMddHHmmssfff") + ".pdf");
            dbEF.SaveChanges();
        }
        #endregion
        #region JS Pdf
        public JsonResult CCNGetFinalReportPDFPath(int CCNID, int CurrentStatus)
        {
            string Msg = "", MsgType = "";
            try
            {
                var GetReportGenerateCCN = (from t1 in dbEF.AizantIT_CCNFinalReports
                                            where t1.CCNID == CCNID
                                            select t1).SingleOrDefault();
                if (GetReportGenerateCCN != null)
                {
                    if (GetReportGenerateCCN.CurrentStatus != CurrentStatus)
                    {
                        MakeCCNAttachmentsToSingleFile(CCNID);
                        GenerateReport(CCNID);
                    }
                }
                else
                {
                    MakeCCNAttachmentsToSingleFile(CCNID);
                    GenerateReport(CCNID);
                }
                var GetFilePath = (from T1 in dbEF.AizantIT_CCNFinalReports
                                   where T1.CCNID == CCNID
                                   select T1.FinalReport).SingleOrDefault();
                string filePath = "";
                if (GetFilePath != null)
                {
                    string FullfilePath = Server.MapPath("~\\" + GetFilePath);
                    filePath = GetFilePath;

                    if (System.IO.File.Exists(FullfilePath))
                    {
                        Msg = filePath;
                        MsgType = "success";
                    }
                    else
                    {
                        Msg = "File does not exist.";
                        MsgType = "error";
                    }
                }
                else
                {
                    Msg = "File does not exist.";
                    MsgType = "error";
                }

                return Json(new { msgType = MsgType, data = Msg }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Msg = "PDF_Viewer:" + strline + "  " + strMsg;
                MsgType = "error";
                return Json(new { msgType = MsgType, data = Msg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Download Report
        public async Task<ActionResult> DownloadChangeControlReport(int CCNID)
        {
            try
            {
                var GetCCNNumber = (from t1 in dbEF.AizantIT_ChangeControlMaster
                                    where t1.CCN_ID == CCNID
                                    select t1.CCN_No).SingleOrDefault();
                string Downloadfilename = "CCN_Report" + GetCCNNumber + ".pdf";
                string filePath = (from objCCNeport in dbEF.AizantIT_CCNFinalReports
                                   where objCCNeport.CCNID == CCNID
                                   select objCCNeport.FinalReport).SingleOrDefault();
                byte[] fileBytes;

                fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~\\" + filePath));
                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, Downloadfilename);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M1:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                TempData["Exception"] = ErMsg;
                return RedirectToAction("ReportList", new { ShowType = 13 });
            }
        }

        #endregion

        #region SaveDraft
        public JsonResult SaveAssessmentQA(CCNMaster CCNBO)
        {
            try
            {
                        var CheckCCNExist = (from CC in dbEF.AizantIT_ChangeControlCommitte
                                             where CC.CCN_ID == CCNBO.CCN_ID && CC.CurrentStatus == 1
                                             select CC).Count();
                if (CheckCCNExist != 0)
                {
                    Boolean Result = ToCheckValidEmployeeOrNot(CCNBO.CCN_ID, 26);
                    if (Result == true)
                    {
                        //Get User Employee id
                        CCNBO.QA_EmpID = GetCookieEmployeID();
                        int dt = Qms_bal.QASaveDraftBAL(CCNBO);
                    }
                    else
                    {
                        return Json(new { hasError = true, data = "You are not authorized QA to do action." }, JsonRequestBehavior.AllowGet);

                    }
                }
                else
                {
                    return Json(new { hasError = true, data = "There is no departments. Add at least one Department." }, JsonRequestBehavior.AllowGet);
                }
                
                
                return Json(new { hasError = false, data = CCNBO }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M27:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

    }
}