﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Aizant_Enums;
using AizantIT_PharmaApp;

using AizantIT_PharmaApp.Areas.QMS.WebServices;
using AizantIT_PharmaApp.Common.CustomFilters;
using static Aizant_Enums.AizantEnums;
using System.Diagnostics;
using Aizant_API_Entities;
using QMS_BO.QMS_ChangeControl_BO;
using QMS_BusinessLayer;
using AizantIT_PharmaApp.Common;

namespace AizantIT_PharmaApp.Areas.QMS.Controllers.ChangeController
{
    //[SessionExpire]
    [RouteArea]
    [CustAuthentication]
    [CustAuthorization(
       (int)AizantEnums.Modules.QMS,
        (int)QMS_Roles.Initiator,
        (int)QMS_Roles.AdminRole)]
    public class BlockMasterController : Controller
    {
        private AizantIT_DevEntities dbEF = new AizantIT_DevEntities();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // GET: QMS_BlockMaster
        public ActionResult BlockList()
        {
            try
            {
                return View(dbEF.AizantIT_BlockMaster.ToList());
            }
            catch(Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "BLOCKM_M1:" + LineNo + "  " + ex.Message + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return View();
            }
        }
        public JsonResult GetBlockMasterList(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch)
        {

            List<BlockList> listEmployees = new List<BlockList>();
            int filteredCount = 0;
            QMS_BAL objbal = new QMS_BAL();
            DataSet dt = objbal.GetBlockMasterLISTBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch);
            if (dt.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Tables[0].Rows)
                {
                    BlockList BlockList = new BlockList();
                    BlockList.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    BlockList.BlockID = Convert.ToInt32(rdr["BlockID"].ToString());
                    BlockList.BlockName = (rdr["BlockName"]).ToString();
                    BlockList.CurrentStatus = (rdr["CurrentStatus"].ToString());

                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listEmployees.Add(BlockList);
                }
            }

          return Json (new 
            {
              hasError = false,
              iTotalRecords = dt.Tables[1].Rows.Count,
                iTotalDisplayRecords = filteredCount,
                aaData = listEmployees
            },JsonRequestBehavior.AllowGet);
        }

        // GET: QMS_BlockMaster/Create
        public PartialViewResult Create()
        {
            try
            {
                return PartialView();
            }
            catch(Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "BLOCKM_M2:" + LineNo + "  " + ex.Message + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }

        // POST: QMS_BlockMaster/Create
        [HttpPost]       
        public JsonResult Create( AizantIT_BlockMaster aizantIT_BlockMaster)
        {
            try
            {
            string SubmitData = "";
            string Status = "";
            AizantIT_BlockMaster objbl = new AizantIT_BlockMaster();
            objbl.BlockName = aizantIT_BlockMaster.BlockName.Trim();
            objbl.BlockID = aizantIT_BlockMaster.BlockID;
            objbl.Currentstatus = aizantIT_BlockMaster.Currentstatus;
            objbl.CreatedBy= Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
            objbl.CreatedDate= DateTime.Now;
                if(objbl.Currentstatus == true)
                {
                    Status = "Active";
                }
                else
                {
                    Status = "Inactive";
                }
                SubmitData = "Block Name : " + objbl.BlockName + ", Current Status : " + Status;
                //string desc = aizantIT_BlockMaster.BlockName;
                AizantIT_BlockMasterHistory objblH = new AizantIT_BlockMasterHistory();
                objblH.ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                objblH.RoleID = 31;//Admin Role
                objblH.ActionDate = DateTime.Now;
                objblH.ActionStatus = 1;
                objblH.SubmittedData = SubmitData;
                objblH.Comments = null;
                var Count = dbEF.AizantIT_BlockMaster.FirstOrDefault(p => p.BlockName == objbl.BlockName);
            
            if (Count == null)
            {
                if (ModelState.IsValid)
                {
                    dbEF.AizantIT_BlockMaster.Add(objbl);
                    dbEF.SaveChanges();
                        objblH.BlockID = objbl.BlockID;
                        dbEF.AizantIT_BlockMasterHistory.Add(objblH);
                        dbEF.SaveChanges();
                        return Json(objbl, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
            return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "BLOCKM_M3:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: QMS_BlockMaster/Edit/5
        public PartialViewResult Edit(int? id)
        {
            try
            {
            if (id == null)
            {
                return PartialView("_FriendlyError");
            }
            AizantIT_BlockMaster aizantIT_BlockMaster = dbEF.AizantIT_BlockMaster.Find(id);
            if (aizantIT_BlockMaster == null)
            {
                return PartialView("_FriendlyError");
            }
            return PartialView(aizantIT_BlockMaster);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "BLOCKM_M4:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }

        // POST: QMS_BlockMaster/Edit/5
        [HttpPost]      
        public JsonResult Edit(AizantIT_BlockMaster aizantIT_BlockMaster,string Comments)
        {
            try
            {
                string SubmitData = "";
                string Status = "";
                AizantIT_BlockMaster objbl = new AizantIT_BlockMaster();
            objbl.BlockName = aizantIT_BlockMaster.BlockName.Trim();
            objbl.BlockID = aizantIT_BlockMaster.BlockID;
            objbl.Currentstatus = aizantIT_BlockMaster.Currentstatus;
            objbl.ModifiedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
            objbl.ModifiedDate = DateTime.Now;
                if (objbl.Currentstatus == true)
                {
                    Status = "Active";
                }
                else
                {
                    Status = "Inactive";
                }
                SubmitData = "Block Name : " + objbl.BlockName + ", Current Status : " + Status;

                //string desc = aizantIT_BlockMaster.BlockName;
                AizantIT_BlockMasterHistory objblH = new AizantIT_BlockMasterHistory();
                objblH.BlockID = aizantIT_BlockMaster.BlockID;
                objblH.ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                objblH.RoleID = 31;//Admin Role
                objblH.ActionDate = DateTime.Now;
                objblH.ActionStatus = 2;
                objblH.SubmittedData = SubmitData;
                objblH.Comments = Comments;
                int id = Convert.ToInt32(aizantIT_BlockMaster.BlockID);
            var exists = dbEF.AizantIT_BlockMaster.FirstOrDefault(p => p.BlockName == objbl.BlockName && p.Currentstatus==objbl.Currentstatus);
            aizantIT_BlockMaster.ModifiedDate = System.DateTime.Now;
            if (exists == null)
            {
                if (ModelState.IsValid)
                {
                    dbEF.Entry(objbl).State = EntityState.Modified;
                        dbEF.AizantIT_BlockMasterHistory.Add(objblH);
                        dbEF.SaveChanges();
                    return Json(objbl, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
            return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "BLOCKM_M5:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        dbEF.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
