﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Aizant_Enums;
using AizantIT_PharmaApp;

using AizantIT_PharmaApp.Common.CustomFilters;
using QMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;
using System.Diagnostics;
using Aizant_API_Entities;
using QMS_BO.QMS_Masters_BO;
using System.Threading;
using AizantIT_PharmaApp.Common;

namespace AizantIT_PharmaApp.Areas.QMS.Controllers.ChangeController
{
    [RouteArea]
    [CustAuthentication]
    [CustAuthorization(
        (int)AizantEnums.Modules.QMS,
        (int)QMS_Roles.Initiator,
        (int)QMS_Roles.AdminRole)]

    public class ImpactDeptLinkController : Controller
    {
        private AizantIT_DevEntities dbEF = new AizantIT_DevEntities();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        // GET: QMS_ImpactDeptLink/Index
        public ActionResult Index()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "IMPDL_M1:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
               
                return View();
            }
        }

        public JsonResult GetImpactPointAndDeptLinkList(int iDisplayLength, int iDisplayStart, int iSortCol_0,
      string sSortDir_0, string sSearch)
        {
            List<ImpactPointAndDeptLinkList> listEmployees = new List<ImpactPointAndDeptLinkList>();
            int filteredCount = 0;
            QMS_BAL objbal = new QMS_BAL();
            DataSet dt = objbal.GetImpactPointAndDeptLinkBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch);
            if (dt.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Tables[0].Rows)
                {
                    ImpactPointAndDeptLinkList typeChange = new ImpactPointAndDeptLinkList();
                    typeChange.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    typeChange.ImpPoint_ID = Convert.ToInt32(rdr["ImpPoint_ID"].ToString());
                    typeChange.Impact_Description = (rdr["Impact_Description"].ToString());
                    typeChange.Department = (rdr["Department"]).ToString();
                    typeChange.BlockName = (rdr["BlockName"]).ToString();
                    typeChange.CurrentStatus = (rdr["CurrentStatus"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listEmployees.Add(typeChange);
                }
            }
            return Json(new
            {
                hasError = false,
                iTotalRecords = filteredCount,
                iTotalDisplayRecords = filteredCount,
                aaData = listEmployees
            }, JsonRequestBehavior.AllowGet);

        }
        public PartialViewResult Create()
        {
            try
            {
                List<SelectListItem> impactListBlock = new List<SelectListItem>();
                var Block = (from B in dbEF.AizantIT_BlockMaster
                             where B.Currentstatus == true
                             select new { B.BlockID, B.BlockName }).ToList();
                impactListBlock.Add(new SelectListItem
                {
                    Text = "Select",
                    Value = "0",
                });
                foreach (var item in Block)
                {
                    impactListBlock.Add(new SelectListItem
                    {
                        Text = item.BlockName,
                        Value = item.BlockID.ToString(),

                    });
            }
                ViewBag.BlockID = new SelectList(impactListBlock.ToList(), "Value", "Text");
                //Imp_Desc_ID != 0 && Currentstatus == true This Two Fields not Binding to Dropdown 0 Means Others True means Active
                List<SelectListItem> impactList = new List<SelectListItem>();
                var s = (from I in dbEF.AizantIT_ImpactPoints
                         where I.Imp_Desc_ID != 0 && I.Currentstatus==true
                         select I);
                foreach (var dr in s)
                {
                    impactList.Add(new SelectListItem
                    {
                        Text = dr.Impact_Description.ToString(),
                        Value = dr.Imp_Desc_ID.ToString(),
                    });
                }
                ViewBag.Imp_Desc_ID = new SelectList(impactList.ToList(), "Value", "Text");
                List<SelectListItem> impactListDept = new List<SelectListItem>();
                var Dept = dbEF.AizantIT_DepartmentMaster.ToList();
                impactListDept.Add(new SelectListItem
                {
                    Text = "Select",
                    Value = "0",
                });
                foreach (var dr in Dept)
                {
                    impactListDept.Add(new SelectListItem
                    {
                        Text = dr.DepartmentName.ToString(),
                        Value =dr.DeptID.ToString(),
                    });
                }
                ViewBag.DeptID = new SelectList(impactListDept.ToList(), "Value", "Text");
                
                return PartialView();
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "IMPDL_M2:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
                
            }
        }
        [HttpPost]       
        public JsonResult Create(AizantIT_ImpactDeptLink aizantIT_ImpactDeptLink,List<int> ImpactPoints)
        {
            try
            {
                int Deptid = Convert.ToInt32(aizantIT_ImpactDeptLink.DeptID);
                int blockID = Convert.ToInt32(aizantIT_ImpactDeptLink.BlockID);
                string SubmitData = "";
                var deptname = (from t1 in dbEF.AizantIT_DepartmentMaster
                                where t1.DeptID == Deptid
                                select t1.DepartmentName).SingleOrDefault();
                SubmitData = " Department: " + deptname;
                var Blockname = (from t1 in dbEF.AizantIT_BlockMaster
                                 where t1.BlockID == blockID
                                 select t1.BlockName).SingleOrDefault();
                SubmitData = SubmitData + " , Block Name: " + Blockname;

                string SubmitImpact = "";
                foreach (int item in ImpactPoints)
                {
                    int ImpID = Convert.ToInt32(item);
                    aizantIT_ImpactDeptLink.Imp_Desc_ID = ImpID;
                    var Impactname = (from t1 in dbEF.AizantIT_ImpactPoints
                                      where t1.Imp_Desc_ID == ImpID
                                      select t1.Impact_Description).SingleOrDefault();
                    SubmitImpact = SubmitData + " , ImpactPoint: " + Impactname;
                    SubmitImpact = SubmitImpact + " , Current Status: Active";

                    AizantIT_ImpactDeptLinkHistory objIDH = new AizantIT_ImpactDeptLinkHistory();
                    objIDH.ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                    objIDH.RoleID = 31;//Admin Role
                    objIDH.ActionDate = DateTime.Now;
                    objIDH.ActionStatus = 1;
                    objIDH.Comments = null;
                    objIDH.SubmittedData = SubmitImpact;
                    SubmitImpact = "";
                    var Count = dbEF.AizantIT_ImpactDeptLink.FirstOrDefault(p => p.BlockID == blockID && p.DeptID == Deptid && p.Imp_Desc_ID == ImpID);
                    if (Count == null)
                    {
                        if (ModelState.IsValid)
                        {
                            dbEF.AizantIT_ImpactDeptLink.Add(aizantIT_ImpactDeptLink);
                            dbEF.SaveChanges();
                            objIDH.ImpPoint_ID = aizantIT_ImpactDeptLink.ImpPoint_ID;
                            dbEF.AizantIT_ImpactDeptLinkHistory.Add(objIDH);
                            dbEF.SaveChanges();
                        }
                    }
                    else
                    {
                        return Json(0, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(blockID, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "IMPDL_M3:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }           
        }
        // GET: QMS_ImpactDeptLink/Edit/5
        public PartialViewResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return PartialView();
                }
                AizantIT_ImpactDeptLink aizantIT_ImpactDeptLink = dbEF.AizantIT_ImpactDeptLink.Find(id);
                if (aizantIT_ImpactDeptLink == null)
                {
                    return PartialView();
                }
                List<SelectListItem> impactListBlock = new List<SelectListItem>();
                var Block = (from B in dbEF.AizantIT_BlockMaster
                             where B.Currentstatus == true
                             select new { B.BlockID, B.BlockName }).ToList();
                impactListBlock.Add(new SelectListItem
                {
                    Text = "Select",
                    Value = "0",
                });
                foreach (var item in Block)
                {
                    impactListBlock.Add(new SelectListItem
                    {
                        Text = item.BlockName,
                        Value = item.BlockID.ToString(),

                    });
                    ViewBag.BlockID = new SelectList(impactListBlock.ToList(), "Value", "Text", aizantIT_ImpactDeptLink.BlockID);

                }
                List<SelectListItem> impactList = new List<SelectListItem>();
                var s = (from I in dbEF.AizantIT_ImpactPoints
                         where I.Imp_Desc_ID != 0
                         select I);
                impactList.Add(new SelectListItem
                {
                    Text = "Select",
                    Value = "0",
                });
                foreach (var dr in s)
                {
                    impactList.Add(new SelectListItem
                    {
                        Text = dr.Impact_Description.ToString(),
                        Value = dr.Imp_Desc_ID.ToString(),
                    });
                }
                ViewBag.Imp_Desc_ID = new SelectList(impactList.ToList(), "Value", "Text", aizantIT_ImpactDeptLink.Imp_Desc_ID);
                List<SelectListItem> impactListDept = new List<SelectListItem>();
                var Dept = dbEF.AizantIT_DepartmentMaster.ToList();

                impactListDept.Add(new SelectListItem
                {
                    Text = "Select",
                    Value = "0",
                });
                foreach (var dr in Dept)
                {
                    impactListDept.Add(new SelectListItem
                    {
                        Text = dr.DepartmentName.ToString(),
                        Value = dr.DeptID.ToString(),
                    });
                }
                ViewBag.DeptID = new SelectList(impactListDept.ToList(), "Value", "Text", aizantIT_ImpactDeptLink.DeptID);
                return PartialView(aizantIT_ImpactDeptLink);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "IMPDL_M4:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
                
            }

        }
        [HttpPost]       
        public JsonResult Edit( AizantIT_ImpactDeptLink aizantIT_ImpactDeptLink,string Comments,int DepatIDCheck,int blockCheck,
           int ImpactCheck,string CurrentStatusCheck)
        {
            try
            {
                //int ImpointID = Convert.ToInt32(aizantIT_ImpactDeptLink.ImpPoint_ID);
                //int ImpDEscID = Convert.ToInt32(aizantIT_ImpactDeptLink.Imp_Desc_ID);
                //int Deptid = Convert.ToInt32(aizantIT_ImpactDeptLink.DeptID);
                //int blockID = Convert.ToInt32(aizantIT_ImpactDeptLink.BlockID);
                var SubmitData = "";
                if (DepatIDCheck != 0)
                {
                    var deptname = (from t1 in dbEF.AizantIT_DepartmentMaster
                                    where t1.DeptID == DepatIDCheck
                                    select t1.DepartmentName).SingleOrDefault();
                    SubmitData = " Department: " + deptname +',';
                }
                if (blockCheck != 0)
                {
                    var Blockname = (from t1 in dbEF.AizantIT_BlockMaster
                                    where t1.BlockID == blockCheck
                                     select t1.BlockName).SingleOrDefault();
                    SubmitData = SubmitData+ " Block Name: " + Blockname +',';

                }
                if (ImpactCheck != 0)
                {
                    var Impactname = (from t1 in dbEF.AizantIT_ImpactPoints
                                     where t1.Imp_Desc_ID == ImpactCheck
                                     select t1.Impact_Description).SingleOrDefault();
                    SubmitData = SubmitData + " ImpactPoint: " + Impactname+',';

                }
                if (CurrentStatusCheck != "0")
                { string Status = "";
                    if (CurrentStatusCheck == "true")
                    {
                        Status = "Active";
                    }
                    else { Status = "In Active"; }
                    SubmitData = SubmitData + " Current Status: " + Status;

                }
                AizantIT_ImpactDeptLinkHistory objIDH = new AizantIT_ImpactDeptLinkHistory();
                objIDH.ImpPoint_ID = aizantIT_ImpactDeptLink.ImpPoint_ID;
                objIDH.ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                objIDH.RoleID = 31;//Admin Role
                objIDH.ActionDate = DateTime.Now;
                objIDH.ActionStatus = 2;
                objIDH.Comments = Comments;
                objIDH.SubmittedData= SubmitData;
                var Count = dbEF.AizantIT_ImpactDeptLink.FirstOrDefault(p => p.BlockID == aizantIT_ImpactDeptLink.BlockID && p.DeptID == aizantIT_ImpactDeptLink.DeptID 
                && p.Imp_Desc_ID == aizantIT_ImpactDeptLink.Imp_Desc_ID && p.Currentstatus==aizantIT_ImpactDeptLink.Currentstatus);
                if (Count == null)
                {
                    if (ModelState.IsValid)
                    {
                        dbEF.Entry(aizantIT_ImpactDeptLink).State = EntityState.Modified;
                        dbEF.AizantIT_ImpactDeptLinkHistory.Add(objIDH);
                        dbEF.SaveChanges();
                        return Json(aizantIT_ImpactDeptLink.BlockID, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
                return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "IMPDL_M5:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
               
            }           
        }
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        dbEF.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
