﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Aizant_Enums;
using AizantIT_PharmaApp;
using AizantIT_PharmaApp.Areas.QMS.WebServices;
using AizantIT_PharmaApp.Common.CustomFilters;
using static Aizant_Enums.AizantEnums;
using System.Diagnostics;
using Aizant_API_Entities;
using QMS_BO.QMS_Masters_BO;
using QMS_BusinessLayer;
using AizantIT_PharmaApp.Common;

namespace AizantIT_PharmaApp.Areas.QMS.Controllers.ChangeController
{
    //[SessionExpire]
    [RouteArea]
    [CustAuthentication]
    [CustAuthorization(
        (int)AizantEnums.Modules.QMS,
        (int)QMS_Roles.Initiator,
         (int)QMS_Roles.AdminRole)]
    public class ImpactPointsController : Controller
    {
        private AizantIT_DevEntities dbEF = new AizantIT_DevEntities();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET: QMS_ImpactPoints
        public ActionResult Index()
        {
            try
            {
                return View(dbEF.AizantIT_ImpactPoints.ToList());
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "IMPP_M1:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
               
                return View();
            }
        }
        public JsonResult GetImpactPontsList(int iDisplayLength, int iDisplayStart, int iSortCol_0,
       string sSortDir_0, string sSearch)
        {
            List<ImpactPointList> listEmployees = new List<ImpactPointList>();
            int filteredCount = 0;
            QMS_BAL objbal = new QMS_BAL();
            DataSet dt = objbal.GetImpactPointLISTBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch);
            if (dt.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Tables[0].Rows)
                {
                    ImpactPointList impact = new ImpactPointList();
                    impact.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    impact.Imp_Desc_ID = Convert.ToInt32(rdr["Imp_Desc_ID"].ToString());
                    impact.Impact_Description = (rdr["Impact_Description"]).ToString();
                    impact.CurrentStatus = (rdr["CurrentStatus"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listEmployees.Add(impact);
                }
            }
            return Json(new
            {
                hasError = false,
                iTotalRecords = filteredCount,
                iTotalDisplayRecords = filteredCount,
                aaData = listEmployees
            }, JsonRequestBehavior.AllowGet);
        }
        // GET: QMS_ImpactPoints/Create
        public PartialViewResult Create()
        {
            try
            {
                return PartialView();
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "IMPP_M2:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }

        // POST: QMS_ImpactPoints/Create
        [HttpPost]      
        public JsonResult Create( AizantIT_ImpactPoints aizantIT_ImpactPoints)
        {
            using (DbContextTransaction transaction = dbEF.Database.BeginTransaction())
            {
                try
                {
                    string SubmitData = "";
                    string Status = "";
                    AizantIT_ImpactPoints objImp = new AizantIT_ImpactPoints();
                    objImp.Impact_Description = aizantIT_ImpactPoints.Impact_Description.Trim();
                    objImp.Imp_Desc_ID = aizantIT_ImpactPoints.Imp_Desc_ID;
                    objImp.ModifiedBy = aizantIT_ImpactPoints.ModifiedBy;
                    objImp.ModifiedDate = aizantIT_ImpactPoints.ModifiedDate;
                    objImp.CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                    objImp.CreatedDate = DateTime.Now;
                    objImp.Currentstatus = aizantIT_ImpactPoints.Currentstatus;
                    if(objImp.Currentstatus == true)
                    {
                        Status = "Active";
                    }
                    else
                    {
                        Status = "InActive";
                    }
                    SubmitData = "Impact Point : " + objImp.Impact_Description + ", Current Status : " + Status;
                    AizantIT_ImpactPointsHistory objImpH = new AizantIT_ImpactPointsHistory();
                    objImpH.ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                    objImpH.RoleID = 31;//Admin Role
                    objImpH.ActionDate = DateTime.Now;
                    objImpH.ActionStatus = 1;
                    objImpH.SubmittedData = SubmitData;
                    objImpH.Comments = null;
                    var Count = dbEF.AizantIT_ImpactPoints.FirstOrDefault(p => p.Impact_Description == objImp.Impact_Description);
                    if (Count == null)
                    {
                        if (ModelState.IsValid)
                        {
                            dbEF.AizantIT_ImpactPoints.Add(objImp);
                            dbEF.SaveChanges();
                            objImpH.Imp_Desc_ID = objImp.Imp_Desc_ID;
                            dbEF.AizantIT_ImpactPointsHistory.Add(objImpH);
                            dbEF.SaveChanges();
                            transaction.Commit();
                            return Json(objImp, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(0, JsonRequestBehavior.AllowGet);
                    }
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "IMPP_M3:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    transaction.Rollback();
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        public PartialViewResult Edit(int? id)
        {
            try
            {
            if (id == null)
            {
                return PartialView("_FriendlyError");
            }
            AizantIT_ImpactPoints aizantIT_Impact = dbEF.AizantIT_ImpactPoints.Find(id);
            if (aizantIT_Impact == null)
            {
                return PartialView("_FriendlyError");
            }
            return PartialView(aizantIT_Impact);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "IMPP_M4:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }

        // POST: QMS_ImpactPoints/Edit/5
        [HttpPost]       
        public JsonResult Edit( AizantIT_ImpactPoints aizantIT_ImpactPoints,string Comments)
        {
            using (DbContextTransaction transaction = dbEF.Database.BeginTransaction())
            {
                try
                {
                    string SubmitData = "";
                    string Status = "";
                    AizantIT_ImpactPoints objImp = new AizantIT_ImpactPoints();
                    objImp.Impact_Description = aizantIT_ImpactPoints.Impact_Description.Trim();
                    objImp.Imp_Desc_ID = aizantIT_ImpactPoints.Imp_Desc_ID;
                    objImp.ModifiedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                    objImp.ModifiedDate = DateTime.Now;
                    objImp.CreatedBy = aizantIT_ImpactPoints.CreatedBy;
                    objImp.Currentstatus = aizantIT_ImpactPoints.Currentstatus;
                    if(objImp.Currentstatus == true)
                    {
                        Status = "Active";
                    }
                    else
                    {
                        Status = "InActive";
                    }
                    SubmitData = "Impact Point : " + objImp.Impact_Description + ", Current Status : " + Status;
                    AizantIT_ImpactPointsHistory objImpH = new AizantIT_ImpactPointsHistory();
                    objImpH.Imp_Desc_ID = aizantIT_ImpactPoints.Imp_Desc_ID;
                    objImpH.ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                    objImpH.RoleID = 31;//Admin Role
                    objImpH.ActionDate = DateTime.Now;
                    objImpH.ActionStatus = 2;
                    objImpH.SubmittedData = SubmitData;
                    objImpH.Comments = Comments;
                    int id = Convert.ToInt32(aizantIT_ImpactPoints.Imp_Desc_ID);
                    var exists = dbEF.AizantIT_ImpactPoints.FirstOrDefault(p => p.Impact_Description == objImp.Impact_Description && p.Currentstatus==objImp.Currentstatus);
                    aizantIT_ImpactPoints.ModifiedDate = System.DateTime.Now;
                    if (exists == null)
                    {
                        if (ModelState.IsValid)
                        {
                            dbEF.Entry(objImp).State = EntityState.Modified;
                            dbEF.AizantIT_ImpactPointsHistory.Add(objImpH);
                            dbEF.SaveChanges();
                            transaction.Commit();
                            return Json(objImp, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(0, JsonRequestBehavior.AllowGet);
                    }
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "IMPP_M5:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    transaction.Rollback();
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        dbEF.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
