﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Aizant_Enums;
using AizantIT_PharmaApp;

using AizantIT_PharmaApp.Areas.QMS.WebServices;
using AizantIT_PharmaApp.Common.CustomFilters;
using QMS_BO;
using static Aizant_Enums.AizantEnums;
using System.Diagnostics;
using Aizant_API_Entities;
using QMS_BO.QMS_Masters_BO;
using QMS_BusinessLayer;
using AizantIT_PharmaApp.Common;

namespace AizantIT_PharmaApp.Areas.QMS.Controllers.ChangeController
{
    //[SessionExpire]
    [RouteArea]
    [CustAuthentication]
    [CustAuthorization(
        (int)AizantEnums.Modules.QMS,
         (int)QMS_Roles.AdminRole,
        (int)QMS_Roles.Initiator      
        )]
    public class TypeChangeMasterController : Controller
    {
        private AizantIT_DevEntities dbEF = new AizantIT_DevEntities();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET: QMS_TypeChangeMaster hjhj
        public ActionResult Index()
        {
            try
            {
                return View(dbEF.AizantIT_TypeChangeMaster.ToList());
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "ACTP_M5:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
               
                return View();
            }
        }
        public JsonResult GetTypeChangeMasterList(int iDisplayLength, int iDisplayStart, int iSortCol_0,
       string sSortDir_0, string sSearch)
        {
            List<TypeChangeMasterList> listEmployees = new List<TypeChangeMasterList>();
            int filteredCount = 0;
            QMS_BAL objbal = new QMS_BAL();
            DataSet dt = objbal.GetTypeChangeMasterLISTBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch);
            if (dt.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Tables[0].Rows)
                {
                    TypeChangeMasterList typeChange = new TypeChangeMasterList();
                    typeChange.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    typeChange.TC_ID = Convert.ToInt32(rdr["TC_ID"].ToString());
                    typeChange.TypeofChange = (rdr["TypeofChange"]).ToString();
                    typeChange.CurrentStatus = (rdr["CurrentStatus"].ToString());
                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listEmployees.Add(typeChange);
                }
            }
            return Json(new
            {
                hasError = false,
                iTotalRecords = dt.Tables[1].Rows.Count,
                iTotalDisplayRecords = filteredCount,
                aaData = listEmployees
            }, JsonRequestBehavior.AllowGet);
        }

        // GET: QMS_TypeChangeMaster/Create
        public PartialViewResult Create()
        {
            try
            {
                return PartialView();
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "TypeCM_M1:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }

        // POST: QMS_TypeChangeMaster/Create
        [HttpPost]      
        public JsonResult Create(AizantIT_TypeChangeMaster aizantIT_TypeChangeMaster)
        {
            using (DbContextTransaction transaction = dbEF.Database.BeginTransaction())
            {
                try
                {
                    string TypeOfChange = "";
                    string CurrentStatus = "";
                    AizantIT_TypeChangeMaster objTC = new AizantIT_TypeChangeMaster();
                    objTC.TC_ID = aizantIT_TypeChangeMaster.TC_ID;
                    objTC.TypeofChange = aizantIT_TypeChangeMaster.TypeofChange.Trim();
                    objTC.Currentstatus = aizantIT_TypeChangeMaster.Currentstatus;
                    objTC.CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                    objTC.CreatedDate = DateTime.Now;
                    objTC.ModifiedDate = aizantIT_TypeChangeMaster.ModifiedDate;
                    objTC.ModifiedBy = aizantIT_TypeChangeMaster.ModifiedBy;
                    AizantIT_TypeChangeMasteHistory objTCH = new AizantIT_TypeChangeMasteHistory();
                    objTCH.ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                    objTCH.RoleID = 31;//Admin Role
                    objTCH.ActionDate = DateTime.Now;
                    objTCH.ActionStatus = 1;
                    objTCH.Comments = null;
                    TypeOfChange = objTC.TypeofChange;
                    if (objTC.Currentstatus == true)
                    {
                        CurrentStatus = "Active";
                    }
                    else
                    {
                        CurrentStatus = "In-Active";
                    }
                     objTCH.SubmittedData = "Type Of Change : " + TypeOfChange +", "+"Current Status : " + CurrentStatus;
                    var Count = dbEF.AizantIT_TypeChangeMaster.FirstOrDefault(p => p.TypeofChange == objTC.TypeofChange);
                    if (Count == null)
                    {
                        if (ModelState.IsValid)
                        {
                            dbEF.AizantIT_TypeChangeMaster.Add(objTC);
                            dbEF.SaveChanges();
                            objTCH.TC_ID = objTC.TC_ID;
                            dbEF.AizantIT_TypeChangeMasteHistory.Add(objTCH);
                            dbEF.SaveChanges();
                            transaction.Commit();
                            return Json(objTC, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(0, JsonRequestBehavior.AllowGet);
                    }
                    
                    return Json(0, JsonRequestBehavior.AllowGet);                    
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "TypeCM_M2:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    transaction.Rollback();
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        // GET: QMS_TypeChangeMaster/Edit/5
        public PartialViewResult Edit(int? id)
        {
            try
            {
            if (id == null)
            {
                return PartialView("_FriendlyError");
            }
            AizantIT_TypeChangeMaster aizantIT_TypeChange = dbEF.AizantIT_TypeChangeMaster.Find(id);
            if (aizantIT_TypeChange == null)
            {
                return PartialView("_FriendlyError");
            }
            return PartialView(aizantIT_TypeChange);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "TypeCM_M3:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }

        }

        // POST: QMS_TypeChangeMaster/Edit/5
        [HttpPost]
        public JsonResult Edit(AizantIT_TypeChangeMaster aizantIT_TypeChangeMaster,string Comments)
        {
            using (DbContextTransaction transaction = dbEF.Database.BeginTransaction())
            {
                try
                {
                    string TypeOfChange = "";
                    string CurrentStatus = "";
                    AizantIT_TypeChangeMaster objTC = new AizantIT_TypeChangeMaster();
                    objTC.TC_ID = aizantIT_TypeChangeMaster.TC_ID;
                    objTC.TypeofChange = aizantIT_TypeChangeMaster.TypeofChange.Trim();
                    objTC.Currentstatus = aizantIT_TypeChangeMaster.Currentstatus;
                    objTC.CreatedBy = aizantIT_TypeChangeMaster.CreatedBy;
                    objTC.CreatedDate = aizantIT_TypeChangeMaster.CreatedDate;
                    objTC.ModifiedDate = DateTime.Now;
                    objTC.ModifiedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                    AizantIT_TypeChangeMasteHistory objTCH = new AizantIT_TypeChangeMasteHistory();
                    objTCH.TC_ID = aizantIT_TypeChangeMaster.TC_ID;
                    objTCH.ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                    objTCH.RoleID = 31;//Admin Role
                    objTCH.ActionDate = DateTime.Now;
                    objTCH.ActionStatus = 2;
                    objTCH.Comments = Comments;
                    TypeOfChange = objTC.TypeofChange;
                    if (objTC.Currentstatus == true)
                    {
                        CurrentStatus = "Active";
                    }
                    else
                    {
                        CurrentStatus = "In-Active";
                    }
                    objTCH.SubmittedData = "Type Of Change : " + TypeOfChange + ", " + "Current Status : " + CurrentStatus;
                    int id = Convert.ToInt32(aizantIT_TypeChangeMaster.TC_ID);
                    var exists = dbEF.AizantIT_TypeChangeMaster.FirstOrDefault(p => p.TypeofChange == objTC.TypeofChange && p.Currentstatus==objTC.Currentstatus);
                    aizantIT_TypeChangeMaster.ModifiedDate = System.DateTime.Now;
                    if (exists == null)
                    {
                        if (ModelState.IsValid)
                        {
                            dbEF.Entry(objTC).State = EntityState.Modified;
                            dbEF.AizantIT_TypeChangeMasteHistory.Add(objTCH);
                            dbEF.SaveChanges();
                            transaction.Commit();
                            return Json(objTC, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(0, JsonRequestBehavior.AllowGet);
                    }
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    string ErMsg = "TypeCM_M3:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                    Aizant_log.Error(ErMsg);
                    transaction.Rollback();
                    return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        dbEF.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
        public PartialViewResult ItemName()
        {
            try
            {
                return PartialView();

            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "TypeCM_M3:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }
        [HttpPost]
        public JsonResult ItemMasterCreate(AizantIT_ItemMaster ItemMasterBO)
        {
            try
            {
                AizantIT_ItemMaster objItm = new AizantIT_ItemMaster();
                objItm.ItemCode = ItemMasterBO.ItemCode.Trim();
                objItm.ItemDescription = ItemMasterBO.ItemDescription==null? "":ItemMasterBO.ItemDescription.Trim();
                objItm.ItemName = ItemMasterBO.ItemName.Trim();
                objItm.ItemStatus = true;
                string desc = ItemMasterBO.ItemDescription;
                var Count = dbEF.AizantIT_ItemMaster.FirstOrDefault(p => p.ItemCode == objItm.ItemCode);
                var namecount = dbEF.AizantIT_ItemMaster.FirstOrDefault(p => p.ItemName == objItm.ItemName);
                if (Count == null && namecount == null)
                {
                    if (ModelState.IsValid)
                    {
                        dbEF.AizantIT_ItemMaster.Add(objItm);
                        dbEF.SaveChanges();
                        return Json(new { hasError = false, data = objItm }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { hasError = true, data = "Already Exists" }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { hasError = false, data = objItm }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "TypeCM_M4:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
             
            }
        }
        public PartialViewResult ProjectName()
        {
            try
            {
                return PartialView();
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "TypeCM_M3:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }
        [HttpPost]
        public JsonResult ProjectMasterCreate(AizantIT_Projects ProjectBO)
        {
            try
            {
                AizantIT_Projects objproj = new AizantIT_Projects();
                objproj.ProjectDescription = ProjectBO.ProjectDescription==null? "":ProjectBO.ProjectDescription.Trim();
                objproj.ProjectName = ProjectBO.ProjectName.Trim();
                objproj.ProjectNumber= ProjectBO.ProjectNumber.Trim();
                objproj.ProjectStatus = true;
                
                string desc = ProjectBO.ProjectDescription;
                var Count = dbEF.AizantIT_Projects.FirstOrDefault(p => p.ProjectNumber == objproj.ProjectNumber);
                var namecount = dbEF.AizantIT_Projects.FirstOrDefault(p => p.ProjectName == objproj.ProjectName);
                if (Count == null && namecount == null)
                {
                    if (ModelState.IsValid)
                    {
                        dbEF.AizantIT_Projects.Add(objproj);
                        dbEF.SaveChanges();
                        return Json(new { hasError = false, data = objproj }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { hasError = true, data = "Already Exists" }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { hasError = false, data = objproj }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "TypeCM_M4:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
      
    }
}
