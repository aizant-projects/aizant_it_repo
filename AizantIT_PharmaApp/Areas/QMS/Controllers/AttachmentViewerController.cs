﻿using AizantIT_PharmaApp.Common.CustomFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AizantIT_PharmaApp.Areas.QMS.Controllers
{   
    [Route("QMS/AttachmentViewerController/{action}")]
    [RouteArea]
    [CustAuthentication]
    public class AttachmentViewerController : Controller
    {
        // GET: QMS/AttachmentViewer
        public ActionResult RichEditViewer()
        {
            return View();
        }

        public ActionResult RichEditView()
        {
            return View();
        }
    }
}