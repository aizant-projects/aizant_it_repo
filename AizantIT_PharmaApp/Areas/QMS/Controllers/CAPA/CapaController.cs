﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QMS_BO;
using System.Data;
using QMS_BusinessLayer;
using AizantIT_PharmaApp.Common.CustomFilters;
using Aizant_Enums;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;
using System.IO;
using System.Diagnostics;
using System.Collections;
using DevExpress.XtraReports.UI;
using AizantIT_PharmaApp.Common;
using AizantIT_PharmaApp.Areas.QMS.XtraReports.CAPA;
using UMS_BO;
using QMS_BO.QMSComonBO;
using QMS_BO.QMS_CAPABO;
using Aizant_API_Entities;
using QMS_BO.QMS_CAPA_BO;
using AizantIT_PharmaApp.Areas.QMS.Controllers.Common;
using System.Data.Entity;
using AizantIT_PharmaApp.Areas.QMS.Models.Common;
using System.Drawing;
using System.Threading.Tasks;
using TMS_BusinessLayer;

namespace AizantIT_PharmaApp.Areas.QMS.Controllers
{
    //[SessionExpire]
    [RouteArea]
    [CustAuthentication]
    [CustAuthorization(
        (int)AizantEnums.Modules.QMS,
        (int)AizantEnums.QMS_Roles.Initiator,
        (int)QMS_Roles.HOD,
        (int)QMS_Roles.QA,
        (int)QMS_Roles.HeadQA,
        (int)QMS_Roles.User,
        (int)QMS_Roles.Investigator,
        (int)QMS_Roles.AdminRole,
        (int)QMS_Audit_Roles.AuditHOD,
        (int)QMS_Audit_Roles.AuditQA,
        (int)QMS_Audit_Roles.AuditHeadQA
        )]

    public class CapaController : Controller
    {
        // GET: QMS_Capa
        QMS_BAL Qms_bal = new QMS_BAL();
        UMS_BAL objUMS_BAL = new UMS_BAL();
        private AizantIT_DevEntities db = new AizantIT_DevEntities();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region CapaList
        public ActionResult CapaList(string R, int Notification_ID = 0)
        {
            try
            {
                //CapaModel CapaBO = new CapaModel();
                //Query String
                TempData["R"] = R;
                TempData.Keep("R");
                CheckRoles(R);
                //GetUserEmpID(CapaBO);
                ViewBag.CAPAID = 0; ViewBag.RemainderStatus = 0; ViewBag.Remainder1 = 0; ViewBag.Remainder2 = 0;
                ViewBag.DeptID = 0;
                if (Notification_ID != 0)
                {
                    int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
                    NotificationUpdate(Notification_ID, EmpIDs);
                }
                BindDeptDropdownForReports();
                return View();
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M1:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                TempData["Exception"] = ErMsg;
                return View();
            }
        }

        // CAPA List
        [HttpGet]
        public JsonResult GetCAPAList()
        {
            try
            {
                int iDisplayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int iDisplayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int iSortCol_0 = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir_0 = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                string Role = Request.Params["Role"];
                int RptDeptID = Convert.ToInt32(Request.Params["DeptRpt"]);
                int RptQualityEvent = Convert.ToInt32(Request.Params["QualityEventRpt"]);
                string RptFrmDate = Request.Params["FromDateRpt"];
                string RptToDate = Request.Params["ToDateReport"];

                string sSearch_CAPA_Number = Request.Params["sSearch_1"];
                string sSearch_Department = Request.Params["sSearch_3"];
                string sSearch_QualityEventName = Request.Params["sSearch_4"];
                string sSearch_QualityEvent_Number = Request.Params["sSearch_5"];
                string sSearch_EmployeeName = Request.Params["sSearch_6"];
                string sSearch_CAPA_Date = Request.Params["sSearch_7"];
                string sSearch_TargetDate = Request.Params["sSearch_8"];
                string sSearch_CurrentStatus = Request.Params["sSearch_9"];

                int EmployeeID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                List<CapaModel> listEmployees = new List<CapaModel>();
                int filteredCount = 0;
                var RoleIDCharts = 0;
                var deptIdDashboard = 0;
                var FromDateDashboard = "";
                var ToDateDashboard = "";
                var CapaFileterEvent = 0;
                //For avoid page display size on Incident Report
                if (Session["ChartsCapaFilterData"] != null)
                {
                    Hashtable ht = Session["ChartsCapaFilterData"] as Hashtable;
                    RoleIDCharts = Convert.ToInt32(ht["RoleIDCapaChart"].ToString());
                    deptIdDashboard = Convert.ToInt32(ht["DeptIDCapaChart"].ToString());
                    FromDateDashboard = ht["FromDateCapachart"].ToString();
                    ToDateDashboard = ht["ToDateCapaChart"].ToString();
                    CapaFileterEvent = Convert.ToInt32(ht["CapaFileterEvent"].ToString());
                }
                var OverDueCapaRoleID = 0;
                if (Session["OverDueRoleID"] != null)
                {
                    OverDueCapaRoleID = Convert.ToInt32(Session["OverDueRoleID"]);
                }
                DataSet dt = Qms_bal.GetCAPAListBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, Role,
                    RoleIDCharts, deptIdDashboard, FromDateDashboard, ToDateDashboard, CapaFileterEvent, OverDueCapaRoleID,
                    RptDeptID, RptFrmDate, RptToDate, EmployeeID, RptQualityEvent, 0,
                    sSearch_CAPA_Number, sSearch_Department, sSearch_QualityEventName, sSearch_QualityEvent_Number, sSearch_EmployeeName
                    , sSearch_CAPA_Date, sSearch_TargetDate, sSearch_CurrentStatus);
                int totalRecordsCount = 0;
                if (dt.Tables[0].Rows.Count > 0)
                {
                    totalRecordsCount = Convert.ToInt32(dt.Tables[0].Rows[0]["TotalCount"]);
                    foreach (DataRow rdr in dt.Tables[0].Rows)
                    {
                        CapaModel capa = new CapaModel();
                        capa.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                        capa.CAPAID = Convert.ToInt32(rdr["CAPAID"].ToString());
                        if (rdr["CAPA_Number"].ToString() == "0")
                        {
                            capa.CAPA_Number = "N/A";
                        }
                        else
                        {
                            capa.CAPA_Number = (rdr["CAPA_Number"].ToString());
                        }
                        capa.CAPA_Date = (rdr["CAPA_Date"].ToString()).ToString();
                        capa.Department = (rdr["Department"]).ToString();
                        capa.EmployeeName = (rdr["EmployeeName"]).ToString();
                        capa.CurrentStatus = (rdr["CurrentStatus"].ToString());
                        capa.QualityEvent_Number = (rdr["QualityEvent_Number"].ToString());
                        capa.QualityEvent_TypeID = (rdr["QualityEvent_TypeID"].ToString());
                        capa.RM1 = (rdr["RM1"].ToString());
                        capa.RM2 = (rdr["RM2"].ToString());
                        //capa.Trasaction = (rdr["Trasaction"].ToString());
                        capa.Verified_YesOrNO = (rdr["Verified_YesOrNO"].ToString());
                        capa.TargetDate = (rdr["TargetDate"]).ToString();
                        capa.EmpID = Convert.ToInt32(rdr["EmpID"].ToString());
                        capa.DeptID = Convert.ToInt32(rdr["DeptID"].ToString());
                        //if (rdr["EmpAcceptedDate"].ToString() == "")
                        //{
                        //    capa.EmpAcceptedDate = null;
                        //}
                        //else
                        //{
                        //    capa.EmpAcceptedDate = (rdr["EmpAcceptedDate"].ToString());
                        //}
                        capa.CurrentStatusNumber = (int)rdr["CurrentStatusNumber"];
                        capa.OverDue = Convert.ToInt32(rdr["OverDue"].ToString());
                        capa.QualityEventName = rdr["QualityEventName"].ToString();
                        capa.EventsBaseID = Convert.ToInt32(rdr["EventBaseID"]);
                        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                        listEmployees.Add(capa);
                    }
                }
                return Json(new
                {
                    hasError = false,
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = filteredCount,
                    aaData = listEmployees
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);
            }
        }

        //Bind Department drop down for ReportList
        public void BindDeptDropdownForReports()
        {
            //Department binding
            List<SelectListItem> DepRpt = new List<SelectListItem>();
            var DepartmentRept = (from DM in db.AizantIT_DepartmentMaster
                                  orderby DM.DepartmentName
                                  select new { DM.DeptID, DM.DepartmentName }).Distinct().ToList();
            DepRpt.Add(new SelectListItem
            {
                Text = "Select",
                Value = "0",
            });
            foreach (var item in DepartmentRept)
            {
                DepRpt.Add(new SelectListItem
                {
                    Text = item.DepartmentName,
                    Value = item.DeptID.ToString(),
                });
            }
            ViewBag.ddlCapaDepartmentBinding = new SelectList(DepRpt.ToList(), "Value", "Text");
            List<int> notbindingEvents = new List<int> { 10, 11 };
            List<SelectListItem> QualityEventListRpt = new List<SelectListItem>();
            var Quality_Event = (from QE in db.AizantIT_QualityEvent
                                 where QE.QualityEvent_TypeID != 3 && QE.QualityEvent_TypeID != 5
                                 select new { QE.QualityEvent_TypeID, QE.QualityEvent_TypeName }).ToList();
            QualityEventListRpt.Add(new SelectListItem
            {
                Text = "Select",
                Value = "0",
            });
            foreach (var item in Quality_Event)
            {
                if (!notbindingEvents.Contains(item.QualityEvent_TypeID))
                {
                    QualityEventListRpt.Add(new SelectListItem
                    {
                        Text = item.QualityEvent_TypeName,
                        Value = item.QualityEvent_TypeID.ToString(),
                    });
                }
            }
            ViewBag.ddlQualityEventsReports = new SelectList(QualityEventListRpt.ToList(), "Value", "Text");
        }
        //Roles Checking
        public void CheckRoles(string R)
        {
            //TempData["QueryString"] = R;
            if (R == "EMP_List" || R == "C_EmpComplete" || R == "C_EmpAccept" || R == "CapaMainList")
            {
                TempData["RoleID"] = "EMP_List";
            }
            //Initiator
            if (R == "Create" || R == "Hod_List" || R == "HOD_RT" || R == "Hod_RevertedList" || R == "CapaMainList")
            {
                TempData["RoleID"] = 25;
            }
            //QA Role
            if (R == "QA_List" || R == "QA_R" || R == "QA_verify" || R == "QARmdrAppLsit"
                || R == "QARmdrTransLsit" || R == "QARmdr2TransLsit" || R == "MainList" || R == "CapaMainList" || R == "CapaReportList")
            {
                TempData["RoleID"] = 26;
            }
            //HeadQA Role
            if (R == "HQA_R" || R == "CapaMainList" || R == "CapaReportList")
            {
                TempData["RoleID"] = 27;
            }
            TempData.Keep("RoleID");
        }

        public void GetUserEmpID(CapaModel objCapaBO)
        {
            if (Session["UserDetails"] != null)
            {
                objCapaBO.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            }
        }
        public void NotificationUpdate(int Notification_ID, int[] EmpIDs)
        {
            int Capa_ID = 0; 
                objUMS_BAL = new UMS_BAL();
                objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs); //2 represents NotifyEmp had visited the page through notification link.

                var GetCAPAID = (from C in db.AizantIT_CapaNotifictionLink
                                 where C.NotificationID == Notification_ID
                                 select new { C.CAPAID }).ToList();
                foreach (var item in GetCAPAID)
                {
                    ViewBag.CAPAID = item.CAPAID;
                    Capa_ID = item.CAPAID;
                }
                var GetRemainderStatus = (from R in db.AizantIT_Capa_Remainder
                                          join CA in db.AizantIT_CAPA_ActionPlan on R.CAPAID equals CA.CAPAID
                                          where R.CAPAID == Capa_ID
                                          select new { R.RemainderStatus, CA.Capa_Action_Imp_DeptID }).ToList();
                foreach (var item in GetRemainderStatus)
                {
                    ViewBag.RemainderStatus = item.RemainderStatus;
                    ViewBag.DeptID = item.Capa_Action_Imp_DeptID;

                }
                int EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                DataSet dt = Qms_bal.GetRemainderExitsorNot(EmpID, Capa_ID);
                if (dt.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Tables[0].Rows)
                    {
                        ViewBag.Remainder1 = dr["RM1"].ToString();
                        ViewBag.Remainder2 = dr["RM2"].ToString();
                    }
                }
        }
        public void UpdateNotificationLink(int Notification_ID){
            objUMS_BAL = new UMS_BAL();
            int[] EmpIDs = new int[] { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) };
            objUMS_BAL.submitNotifications(Request.QueryString["Notification_ID"].ToString(), 2, EmpIDs); //2 represents NotifyEmp had visited the page through notification link.

        }
        #endregion

        #region CAPA Create
        public void BindDropDownCAPACreate(CapaModel objbo)
        {
            GetUserEmpID(objbo);
            DataSet ds = new DataSet();
            ds = Qms_bal.GetddlBindingCAPACreateBAL(objbo.EmpID);

            //Department binding
            List<SelectListItem> Dep = new List<SelectListItem>();
            Dep.Add(new SelectListItem
            {
                Text = "Select",
                Value = "0",
            });
            foreach (DataRow rdr in ds.Tables[0].Rows)
            {
                Dep.Add(new SelectListItem
                {
                    Text = (rdr["DepartmentName"]).ToString(),
                    Value = (rdr["DeptID"]).ToString(),
                    Selected = (Convert.ToInt32(rdr["DeptID"]) == objbo.DeptID ? true : false)
                });
            }
            objbo.ddlDeptList = Dep;
            List<int> NotBindingQuality;
            //  //Quality Events binding except CCN,CAPA,Errata,Audit,NoteToFile,Others,Ratification
            if (objbo.CurrentStatus == null)
            {
                NotBindingQuality = new List<int> { 1, 2, 3, 4, 5, 9, 10, 11};

            }
            else if (objbo.CurrentStatus == "17" || objbo.CurrentStatus == "18")
            {
                if (objbo.QualityEvent_TypeID == "9")
                {
                    NotBindingQuality = new List<int> { 1, 2, 3, 4, 5, 10, 11 };

                }
                else
                {
                    NotBindingQuality = new List<int> { 1, 2, 3, 4, 5, 9, 10, 11 };

                }
            }
            else
            {
                NotBindingQuality = new List<int> { };

            }

            List<SelectListItem> QualityEventList = new List<SelectListItem>();
            foreach (DataRow rdr in ds.Tables[1].Rows)
            {
                if (!NotBindingQuality.Contains(Convert.ToInt32(rdr["QualityEvent_TypeID"])))
                {
                    QualityEventList.Add(new SelectListItem
                    {
                        Text = (rdr["QualityEvent_TypeName"]).ToString(),
                        Value = (rdr["QualityEvent_TypeID"]).ToString(),
                        Selected = (Convert.ToInt32(rdr["QualityEvent_TypeID"]) == Convert.ToInt32(objbo.QualityEvent_TypeID) ? true : false)
                    });
                }
            }
            objbo.ddlqualityEventList = QualityEventList;

            //Action Plan binding
            List<SelectListItem> ActionPlanList = new List<SelectListItem>();
            ActionPlanList.Add(new SelectListItem
            {
                Text = "Select",
                Value = "0",
            });
            foreach (DataRow rdr in ds.Tables[2].Rows)
            {
                ActionPlanList.Add(new SelectListItem
                {
                    Text = (rdr["Capa_ActionName"]).ToString(),
                    Value = (rdr["Capa_ActionID"]).ToString(),
                    Selected = (Convert.ToInt32(rdr["Capa_ActionID"]) == Convert.ToInt32(objbo.PlanofAction) ? true : false)
                });
            }
            objbo.ddlActionplan = ActionPlanList;
        }

        //Capa Create Page
        public PartialViewResult Create(int CAPAID = 0)
        {
            try
            {
                CapaModel CapaBO = new CapaModel();
                GetUserEmpID(CapaBO);
                if (CAPAID == 0)
                {
                    BindDropDownCAPACreate(CapaBO);
                }
                else
                {
                    CapaBO = BindData(CAPAID);
                    BindDropDownCAPACreate(CapaBO);

                    //Code for Responsible Employees DDL
                    List<SelectListItem> ObjResponsiblePersonsList = new List<SelectListItem>();
                    var ResponsibleItemsList = db.Database.SqlQuery<AccessibleEmpOnDeptandRole>("exec UMS.AizantIT_SP_ToGetEmployeesByDeptIDandRoleID " +
                                +CapaBO.DeptID + "," + (int)QMS_Roles.User + ",1").ToList();
                    ObjResponsiblePersonsList.Add(new SelectListItem
                    {
                        Text = "-- Select Responsible Person --",
                        Value = "0",
                    });
                    foreach (var RPItem in ResponsibleItemsList)
                    {
                        ObjResponsiblePersonsList.Add(new SelectListItem()
                        {
                            Text = RPItem.EmpName,
                            Value = RPItem.EmpID.ToString(),
                            Selected = (RPItem.EmpID == (Convert.ToInt32(CapaBO.EmployeeName)) ? true : false)
                        });
                    }
                    CapaBO.ddlResponsiblePerson = ObjResponsiblePersonsList;

                    //Bind Assigned QA Employee with Selected Value

                    List<int> QaEmployeeid = new List<int>() { CapaBO.EmpID, Convert.ToInt32(CapaBO.EmployeeName) };
                    List<SelectListItem> objAssignedQAList = new List<SelectListItem>();
                    int QARoleID;
                    if (CapaBO.QualityEvent_TypeID == "9")
                    {
                        QARoleID = 36;//Audit HeadQA
                    }
                    else
                    {
                        QARoleID = 26;//QMS HEadQA
                    }
                    var GetAssignedQA = db.Database.SqlQuery<AccessibleEmpOnDeptandRole>("exec [UMS].[AizantIT_SP_GetEmployeesByRoleIDAndStatus] " +
                                 QARoleID + ",'A'").ToList();
                    foreach (var QAItem in GetAssignedQA)
                    {
                        if (!QaEmployeeid.Contains(QAItem.EmpID))
                        {
                            objAssignedQAList.Add(new SelectListItem()
                            {
                                Text = QAItem.EmpName,
                                Value = QAItem.EmpID.ToString(),
                                Selected = (QAItem.EmpID == CapaBO.QA_EmpId ? true : false)
                            });
                        }
                    }
                    CapaBO.ddlQAEmployeeID = objAssignedQAList;
                }
                return PartialView(CapaBO);

            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M2:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }
        [HttpPost]
        public JsonResult Create(CapaModel capaModel)
        {
            try
            {
                DataTable dt = new DataTable();
                if (Session["UserDetails"] != null)
                {
                    //HodEmployee id
                    capaModel.CreatedBy = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                }
                dt = Qms_bal.CapaInsertBal(capaModel);
                var CAPANumber = dt.Rows[0][0].ToString();
                return Json(new { hasError = false, data = CAPANumber }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M8:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult Update(CapaModel capaModel)
        {
            try
            {
                DataTable dt = new DataTable();
                if (Session["UserDetails"] != null)
                {
                    capaModel.Modified = (Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString();
                }
                dt = Qms_bal.CapaUpdateBal(capaModel, out int ResultStatus);
                if (ResultStatus == 0)
                {
                    return Json(new { hasError = false, data = capaModel }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { hasError = true, data = "You are not authorized HOD to do action." }, JsonRequestBehavior.AllowGet);
                {

                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M9:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion



        #region Bind Drop downs

        //Bind QA drop down when select Responsible person (only used in CAPA reverted to HOD)
        [HttpGet]
        public JsonResult BindAssignqaOnchangeResponsiblePerson(int QualityEventID, int ResponsibleID)
        {
            try
            {
                int QARoleID;
                if (QualityEventID == 9)
                {
                    QARoleID = 36;
                }
                else
                {
                    QARoleID = 26;
                }
                int LoginEmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                List<int> QaEmployeeid = new List<int>() { LoginEmpID, ResponsibleID };

                List<SelectListItem> objAssignQA = new List<SelectListItem>();
                var AssignedHOD = db.Database.SqlQuery<AccessibleEmpOnDeptandRole>("exec UMS.AizantIT_SP_GetEmployeesByRoleIDAndStatus " + QARoleID + ",'A'").ToList();
                objAssignQA.Add(new SelectListItem { Text = "Select", Value = "0" });

                foreach (var QAItem in AssignedHOD)
                {
                    //Not binding Login and responsible id
                    if (!QaEmployeeid.Contains(QAItem.EmpID))
                    {
                        objAssignQA.Add(new SelectListItem()
                        {
                            Text = QAItem.EmpName,
                            Value = QAItem.EmpID.ToString()
                        });
                    }
                }
                return Json(objAssignQA, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M29:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }

        }

        //Hod's Bind in Capa Reminder 
        public void BindHod(int? id, int? DeptID)
        {
            //not binded QA Employee id to Hod Employee DropDown
            var EmpId = (from CM in db.AizantIT_CAPAMaster
                         where CM.CAPAID == id
                         select new { CM.QA_EmpID, CM.HeadQA_EmpID, CM.QualityEvent_TypeID }).ToList();
            int QAEmployeeid = 0;
            int QaulityEventTypeID = 0;
            int HQAempId = 0;
            foreach (var q1 in EmpId)
            {
                QAEmployeeid = Convert.ToInt32(q1.QA_EmpID);
                HQAempId = Convert.ToInt32(q1.HeadQA_EmpID);
                QaulityEventTypeID = Convert.ToInt32(q1.QualityEvent_TypeID);

            }
            List<int> NotBindingRemHodEmpID = new List<int> { QAEmployeeid, HQAempId };
            //Bind Responsible Hod's without Selected item 
            var CAPAReslut = db.AizantIT_CAPAMaster.Find(id);
            int RoldID = 0;
            if (QaulityEventTypeID == 9)
            {
                RoldID = Convert.ToInt32(QMS_Audit_Roles.AuditHOD);
            }
            else
            {
                RoldID = Convert.ToInt32(QMS_Roles.HOD);
            }
            //Assigned HOD's
            List<SelectListItem> objAssignResponsiblePerson = new List<SelectListItem>();
            var EmployeeName = db.Database.SqlQuery<AccessibleEmpOnDeptandRole>("exec UMS.AizantIT_SP_ToGetEmployeesByDeptIDandRoleID " +
               +DeptID + "," + RoldID + ",'" + 1 + "'").ToList();
            foreach (var RPItem in EmployeeName)
            {
                if (!NotBindingRemHodEmpID.Contains(RPItem.EmpID))
                {
                    objAssignResponsiblePerson.Add(new SelectListItem()
                    {
                        Text = RPItem.EmpName,
                        Value = RPItem.EmpID.ToString()
                    });
                }
            }
            ViewBag.HODEmployeeName = new SelectList(objAssignResponsiblePerson.ToList(), "Value", "Text", CAPAReslut.CreatedBy);
        }

        #region Fill Responsible person
        //Bind User role(29) employee Only to Responsible Person Dropdown 
        [HttpGet]
        public JsonResult FillEmployee(int DeptID/*,int HODEmpID*/)
        {
            try
            {
                List<SelectListItem> ObjResponsiblePersonsList = new List<SelectListItem>();
                var ResponsibleItemsList = db.Database.SqlQuery<AccessibleEmpOnDeptandRole>("exec UMS.AizantIT_SP_ToGetEmployeesByDeptIDandRoleID " +
                            +DeptID + "," + (int)QMS_Roles.User + ",1").ToList();
                ObjResponsiblePersonsList.Add(new SelectListItem
                {
                    Text = "-- Select Responsible Person --",
                    Value = "0",
                });
                foreach (var RPItem in ResponsibleItemsList)
                {
                    if (Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]) != RPItem.EmpID)
                    {
                        ObjResponsiblePersonsList.Add(new SelectListItem()
                        {
                            Text = RPItem.EmpName,
                            Value = RPItem.EmpID.ToString()
                        });
                    }
                }
                return Json(ObjResponsiblePersonsList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M14:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #endregion

        #region Reminders
        [HttpGet]
        public JsonResult CAPACompleteOrNot(int CAPAID)
        {
            try
            {
                var ExistCAPAorNot = (from CM in db.AizantIT_CAPA_ActionPlan
                                      where CM.CAPAID == CAPAID
                                      select CM.CurrentStatus).SingleOrDefault();
                return Json(new { hasError = false, data = ExistCAPAorNot }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M9:" + LineNo + "  " + ex.Message + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);

            }
        }

        public PartialViewResult EditRM1(int? id, int? DeptID)
        {
            AizantIT_Capa_Remainder capaReminder = new AizantIT_Capa_Remainder();
            try
            {
                string Role = Convert.ToString(TempData["R"]);
                TempData.Keep("R");
                //Get CAPA Number
                var capanumber = db.AizantIT_CAPAMaster.Find(id);
                ViewBag.CapaNumber = capanumber.CAPA_Number;
                //Check roles
                CheckRoles(Role);
                if (id == null)
                {
                    return PartialView();
                }
                DataSet ds = new DataSet();
                ds = Qms_bal.CapaTranaction(Convert.ToInt32(id), "Reminder1");

                if (ds.Tables[0].Rows.Count > 0)
                {
                    capaReminder.HOD_Justify_Notes = ds.Tables[0].Rows[0]["HOD_Justify_Notes"].ToString();
                    capaReminder.QA_Remainder_Notes = ds.Tables[0].Rows[0]["QA_Remainder_Notes"].ToString();
                    string dat = Convert.ToDateTime(ds.Tables[0].Rows[0]["RevisedTargetDate"]).ToString("dd MMM yyyy");
                    ViewBag.RevisedTargetDate = dat;
                    capaReminder.RevisedTargetDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["RevisedTargetDate"]);
                    capaReminder.RemainderStatus = Convert.ToInt32(ds.Tables[0].Rows[0]["RemainderStatus"].ToString());
                    capaReminder.CAPAID = Convert.ToInt32(ds.Tables[0].Rows[0]["CAPAID"].ToString());
                    capaReminder.Hod_EmpID = Convert.ToInt32(ds.Tables[0].Rows[0]["Hod_EmpID"].ToString());
                    if (capaReminder.RemainderStatus != 5)
                    {
                        capaReminder.IsRevisedDate = Convert.ToBoolean(ds.Tables[0].Rows[0]["IsRevisedDate"].ToString());
                    }
                    //not binded Qa Employee and HeadQA  to Hod Employee DropDown
                    var EmpIds = (from CM1 in db.AizantIT_CAPAMaster
                                  where CM1.CAPAID == id
                                  select new { CM1.QA_EmpID, CM1.HeadQA_EmpID, CM1.QualityEvent_TypeID }).SingleOrDefault();

                    int QAEmployeeidRM1 = Convert.ToInt32(EmpIds.QA_EmpID);
                    int HQAEmpIDRM1 = Convert.ToInt32(EmpIds.HeadQA_EmpID);
                    int QualityEventTypeID = Convert.ToInt32(EmpIds.QualityEvent_TypeID);

                    List<int> NotBinidngQAHQAInRemHodddlRM1 = new List<int> { QAEmployeeidRM1, HQAEmpIDRM1 };
                    int RoleID = 0;
                    if (QualityEventTypeID == 9)//Audit
                    {
                        RoleID = Convert.ToInt32(QMS_Audit_Roles.AuditHOD);
                    }
                    else
                    {
                        RoleID = Convert.ToInt32(QMS_Roles.HOD);
                    }
                    //Assigned HOD's in Reminder
                    List<SelectListItem> objAssignReminderHOds = new List<SelectListItem>();
                    var AssigneHODReminder = db.Database.SqlQuery<AccessibleEmpOnDeptandRole>("exec UMS.AizantIT_SP_ToGetEmployeesByDeptIDandRoleID " +
                       +DeptID + "," + RoleID + ",'" + 1 + "'").ToList();
                    foreach (var RPItem in AssigneHODReminder)
                    {
                        if (!NotBinidngQAHQAInRemHodddlRM1.Contains(RPItem.EmpID))
                        {
                            objAssignReminderHOds.Add(new SelectListItem()
                            {
                                Text = RPItem.EmpName,
                                Value = RPItem.EmpID.ToString()
                            });
                        }
                    }
                    ViewBag.HODEmployeeName = new SelectList(objAssignReminderHOds.ToList(), "Value", "Text", capaReminder.Hod_EmpID);

                    //Bind Reminder Status
                    ViewBag.ReminderStatusName = (from S in db.AizantIT_ObjectStatus
                                                  where S.StatusID == capaReminder.RemainderStatus && S.ObjID == 7
                                                  select S.StatusName).Take(1);
                }
                else
                {
                    //Bind Responsible  Hod's without Selected item
                    BindHod(id, DeptID);
                }
                var aizantIT_Capa_Remainder = db.AizantIT_Capa_Remainder.Where(a => a.CAPAID == (id)).ToList().FirstOrDefault();
                AizantIT_CAPA_ActionPlan CAP = db.AizantIT_CAPA_ActionPlan.Where(c => c.CAPAID == id).FirstOrDefault();
                AizantIT_CAPAMaster CpMbo = db.AizantIT_CAPAMaster.Find(id);
                ViewBag.CAPANO = CpMbo.CAPA_Number;
                if (ds.Tables[0].Rows.Count == 0)
                {
                    if (aizantIT_Capa_Remainder == null)
                    {
                        ViewBag.ProposedDate = Convert.ToDateTime(CAP.ActionPlan_TimeLines).ToString("dd MMM yyyy");
                        AizantIT_Capa_Remainder aizantIT_Capa_Remainder1 = new AizantIT_Capa_Remainder();
                        aizantIT_Capa_Remainder1.RemainderStatus = 0;
                        aizantIT_Capa_Remainder1.CAPAID = Convert.ToInt32(id);
                        return PartialView(aizantIT_Capa_Remainder1);
                    }
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    ViewBag.ProposedDate = Convert.ToDateTime(CAP.ActionPlan_TimeLines).ToString("dd MMM yyyy");
                }
                else
                {
                    ViewBag.ProposedDate = Convert.ToDateTime(CAP.ActionPlan_TimeLines).ToString("dd MMM yyyy");
                }
                //Get  Hod Employee Name
                if (capaReminder.Hod_EmpID != 0)
                {
                    ViewBag.RM1HODEmpName = (from E in db.AizantIT_EmpMaster
                                             where E.EmpID == capaReminder.Hod_EmpID
                                             select (E.FirstName + "" + E.LastName)).SingleOrDefault();
                }
                return PartialView(capaReminder);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M5:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }
        public PartialViewResult EditRM2(int? id, int? DeptID)
        {
            try
            {
                string Role = Convert.ToString(TempData["R"]);
                TempData.Keep("R");
                CheckRoles(Role);
                if (id == null)
                {
                    return PartialView();
                }
                DataSet ds = new DataSet();
                ds = Qms_bal.CapaTranaction(Convert.ToInt32(id), "Reminder2");
                AizantIT_Capa_Remainder capaReminder = new AizantIT_Capa_Remainder();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    capaReminder.HOD_Justify_Notes = ds.Tables[0].Rows[0]["HOD_Justify_Notes"].ToString();
                    capaReminder.QA_Remainder_Notes = ds.Tables[0].Rows[0]["QA_Remainder_Notes"].ToString();
                    string dat = Convert.ToDateTime(ds.Tables[0].Rows[0]["RevisedTargetDate"]).ToString("dd MMM yyyy");
                    ViewBag.RevisedTargetDateRM2 = dat;
                    capaReminder.RevisedTargetDate = Convert.ToDateTime(dat);
                    capaReminder.RemainderStatus = Convert.ToInt32(ds.Tables[0].Rows[0]["RemainderStatus"].ToString());
                    capaReminder.CAPAID = Convert.ToInt32(ds.Tables[0].Rows[0]["CAPAID"].ToString());
                    capaReminder.Hod_EmpID = Convert.ToInt32(ds.Tables[0].Rows[0]["Hod_EmpID"].ToString());

                    if (capaReminder.RemainderStatus != 12)
                    {
                        capaReminder.IsRevisedDate = Convert.ToBoolean(ds.Tables[0].Rows[0]["IsRevisedDate"].ToString());

                    }
                    //not binded Qa and HQA Employee id to Hod Employee DropDown
                    var EmpIds = (from CM in db.AizantIT_CAPAMaster
                                  where CM.CAPAID == id
                                  select new { CM.QA_EmpID, CM.HeadQA_EmpID, CM.QualityEvent_TypeID }).SingleOrDefault();

                    int QAEmployeeidRM2 = Convert.ToInt32(EmpIds.QA_EmpID);
                    int HqaEmpIdRM2 = Convert.ToInt32(EmpIds.HeadQA_EmpID);
                    int QualityEventTypeID = Convert.ToInt32(EmpIds.QualityEvent_TypeID);

                    List<int> NotBindingQAandHqainHodddlRm2 = new List<int> { QAEmployeeidRM2, HqaEmpIdRM2 };

                    int RoleIDRM2 = 0;
                    if (QualityEventTypeID == 9)
                    {
                        RoleIDRM2 = Convert.ToInt32(QMS_Audit_Roles.AuditHOD);
                    }
                    else
                    {
                        RoleIDRM2 = Convert.ToInt32(QMS_Roles.HOD);
                    }

                    //Bind Responsible HOS's With Selected item
                    //Assigned HOD's in Reminder2
                    List<SelectListItem> objAssignReminder2HOds = new List<SelectListItem>();
                    var AssigneHODReminder2 = db.Database.SqlQuery<AccessibleEmpOnDeptandRole>("exec UMS.AizantIT_SP_ToGetEmployeesByDeptIDandRoleID " +
                       +DeptID + "," + RoleIDRM2 + ",'" + 1 + "'").ToList();
                    foreach (var RPItem in AssigneHODReminder2)
                    {
                        if (!NotBindingQAandHqainHodddlRm2.Contains(RPItem.EmpID))
                        {
                            objAssignReminder2HOds.Add(new SelectListItem()
                            {
                                Text = RPItem.EmpName,
                                Value = RPItem.EmpID.ToString()
                            });
                        }
                    }
                    ViewBag.HODEmployeeName = new SelectList(objAssignReminder2HOds.ToList(), "Value", "Text", capaReminder.Hod_EmpID);

                    //Bind Reminder Status
                    ViewBag.ReminderStatusName = (from S in db.AizantIT_ObjectStatus
                                                  where S.StatusID == capaReminder.RemainderStatus && S.ObjID == 7
                                                  select S.StatusName).Take(1);

                }
                else
                {
                    //Bind Responsible  Hod's without Selected item
                    BindHod(id, DeptID);
                }
                //var aizantIT_Capa_Remainder = db.AizantIT_Capa_Remainder.Where(a => a.CAPAID == (id)).ToList().FirstOrDefault();
                AizantIT_CAPA_ActionPlan CAP = db.AizantIT_CAPA_ActionPlan.Where(c => c.CAPAID == id).FirstOrDefault();
                AizantIT_CAPAMaster cM = db.AizantIT_CAPAMaster.Find(id);
                ViewBag.CAPANO = cM.CAPA_Number;
                if (ds.Tables[1].Rows.Count > 0)//get RevisedDate after Reminder Approve
                {
                    ViewBag.ProposedDate = Convert.ToDateTime(ds.Tables[1].Rows[0]["RevisedTargetDate"]).ToString("dd MMM yyyy");
                    capaReminder.CAPAID = Convert.ToInt32(ds.Tables[1].Rows[0]["CAPAID"].ToString());
                    if (capaReminder.RemainderStatus == null)
                    {
                        capaReminder.RemainderStatus = 0;
                    }
                }
                else
                {
                    ViewBag.ProposedDate = Convert.ToDateTime(CAP.ActionPlan_TimeLines).ToString("dd MMM yyyy");
                }
                if (capaReminder.Hod_EmpID != 0)
                {
                    ViewBag.RM2HODEmpName = (from E in db.AizantIT_EmpMaster
                                             where E.EmpID == capaReminder.Hod_EmpID
                                             select (E.FirstName + "" + E.LastName)).SingleOrDefault();
                }
                return PartialView(capaReminder);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M6:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }

        //Create Reminder At QA Role
        [HttpPost]
        public JsonResult RMQAA(AizantIT_Capa_Remainder capaModel1)
        {
            try
            {
                string Role = Convert.ToString(TempData["R"]);
                TempData.Keep("R");
                CheckRoles(Role);
                DataTable dt = new DataTable();
                CapaReminder capaModel = new CapaReminder();
                capaModel.CAPAID = capaModel1.CAPAID;
                if (capaModel1.RevisedTargetDate != null)
                {
                    string dat = (capaModel1.RevisedTargetDate).ToString();
                    capaModel.RevisedTargetDate = Convert.ToDateTime(dat).ToString("yyyy/MM/dd");
                }
                else
                {
                    capaModel.RevisedTargetDate = "";
                }
                capaModel.HOD_Comments = capaModel1.HOD_Justify_Notes;
                capaModel.QA_Comments = capaModel1.QA_Remainder_Notes;
                capaModel.RemainderStatus = capaModel1.RemainderStatus.ToString();
                capaModel.EmployeeName = capaModel1.Hod_EmpID.ToString();
                if (capaModel.RemainderStatus != "5" && capaModel.RemainderStatus != "12")
                {
                    capaModel.IsRevisedDate = (bool)capaModel1.IsRevisedDate;
                }
                if (Session["UserDetails"] != null)
                {
                    capaModel.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                }

                string role = "";
                int RoleID = Convert.ToInt32(TempData["RoleID"]);
                if (RoleID == 26)
                {
                    role = "QA";
                    capaModel.HOD_Comments = "";
                }
                else if (RoleID == 25)
                {
                    role = "HOD";
                    capaModel.QA_Comments = "";
                }
                Boolean IsTrue;
                List<int?> StatusContians = new List<int?> { 13, 8 };
                if (capaModel.RemainderStatus == "6")
                {
                    IsTrue = (from T1 in db.AizantIT_Capa_Remainder
                              where T1.CAPAID == capaModel.CAPAID && T1.RemainderStatus != 6
                              select T1).Any();
                }
                else if (capaModel.RemainderStatus == "13")
                {
                    IsTrue = (from T1 in db.AizantIT_Capa_Remainder
                              where T1.CAPAID == capaModel.CAPAID && !StatusContians.Contains(T1.RemainderStatus)
                              select T1).Any();
                }
                else
                {
                    IsTrue = true;
                }
                if (IsTrue == true)
                {
                    dt = Qms_bal.CapaReminderBAL1(capaModel, role, out int Result);
                    if (Result == 0)
                    {
                        return Json(new { hasError = "success", data = capaModel }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { hasError = "warning", data = "You are not authorized employee to do action." }, JsonRequestBehavior.AllowGet);

                    }
                }
                else
                {
                    return Json(new { hasError = "warning", data = "Reminder Revision Is already Submitted." }, JsonRequestBehavior.AllowGet);

                }

            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M7:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = "error", data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region UserAccept Complete

        [HttpPost]
        public JsonResult EmpAcceptStatus(CapaModel capaModel)
        {
            try
            {
                DataTable dt = new DataTable();
                capaModel.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                dt = Qms_bal.CapaEmpStatusBal(capaModel, out int ResultStatus);
                if (ResultStatus == 0)
                {
                    return Json(new { hasError = false, data = capaModel }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { hasError = true, data = "You are not authorized User to do action." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M10:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region CapaClose
        //Not using
        public PartialViewResult ActionEmployee(int id)
        {
            try
            {
                var Status = "C_Complt";
                RefershDeletedFilesIsTempIsFalse(id, Status);
                string Role = Convert.ToString(TempData["R"]);
                TempData.Keep("R");
                //Check roles
                CheckRoles(Role);
                CapaModel capa = new CapaModel();
                capa = BindData(id);
                CapaModel getlatestDate = new CapaModel();
                getlatestDate = BindLatestTargetDate(id);
                if (getlatestDate.TargetDate != null)
                {
                    capa.TargetDate = getlatestDate.TargetDate;
                }
                return PartialView(capa);
            }
            catch (Exception ex)
            {

                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M16:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }

        }

        [HttpPost]
        public JsonResult SubmitCAPAClose(MainListCapaBO objcapa)
        {
            try
            {
                CapaModel objmodel = new CapaModel();
                //Get employee ID
                GetUserEmpID(objmodel);
                objcapa.EMPId = objmodel.EmpID;

                Qms_bal.GetCAPACloseBAL(objcapa, out int Result, out int AllCpapCompleted);
                if (Result == 0)
                {
                    return Json(new { hasError = false, data = AllCpapCompleted }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { hasError = true, data = "You are not authorized QA to do action." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M11:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Verification

        //QA Verify the Capa or Drop the Capa
        [HttpPost]
        public JsonResult VerifiedCAPA(MainListCapaBO objcapaModel)
        {
            try
            {
                string GUIDVerification;
                if (objcapaModel.VerificationID == 0)
                {
                    TempData.Keep("GUIDVerification");
                    if (TempData["GUIDVerification"] != null)
                        objcapaModel.GUID = TempData["GUIDVerification"].ToString();
                }
                //Get employee ID
                CapaModel objbo = new CapaModel();
                GetUserEmpID(objbo);
                objcapaModel.EMPId = objbo.EmpID;
                if (objcapaModel.ISCheckedFinalorNot == false)//Not implemented
                {
                    objcapaModel.ImplementorDateVerificationCAPA = "";
                }
                Qms_bal.GetCAPAVerificationBAL(objcapaModel, out int Result);
                if (Result == 0)
                {
                    return Json(new { msg = "", msgType = "success", }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { msg = "You are not authorized QA to do action.", msgType = "error", }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M11:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { msg = ErMsg, msgType = "error", }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Capa FileUploads
        //Refresh Verification Page Remove Deleted Is Temp Files
        public void RefershDeletedFilesIsTempIsFalse(int Capaid, string Status)
        {
            if (Status == "Verify")
            {
                var objC_verificationAttachment = (from objCmpt in db.AizantIT_CAPAVerificationFilesAttachements
                                                   where objCmpt.CapaID == Capaid && objCmpt.isTemp == false
                                                   select objCmpt).ToList();
                //var _lstDel = dbEF.AizantIT_HODAssessmentAttachmentFile.Where(s => s.AssessmentID == ID && s.isTemp == false);
                if (objC_verificationAttachment.Count > 0)
                {
                    QMSAttachments objAttachmentsCommon = new QMSAttachments();
                    foreach (var item in objC_verificationAttachment)
                    {
                        objAttachmentsCommon.RemovePhysicalPathFile(Server.MapPath("~\\" + item.FilePath));
                    }
                    db.AizantIT_CAPAVerificationFilesAttachements.RemoveRange(objC_verificationAttachment);
                    db.SaveChanges();
                }
            }
            if (Status == "C_Complt")
            {
                var objC_CompltAttachment = (from objCmpt in db.AizantIT_CapaCompltFileAttachments
                                             where objCmpt.CapaID == Capaid && objCmpt.isTemp == false
                                             select objCmpt).ToList();
                //var _lstDel = dbEF.AizantIT_HODAssessmentAttachmentFile.Where(s => s.AssessmentID == ID && s.isTemp == false);
                if (objC_CompltAttachment.Count > 0)
                {
                    QMSAttachments objAttachmentsCommon = new QMSAttachments();
                    foreach (var item in objC_CompltAttachment)
                    {
                        objAttachmentsCommon.RemovePhysicalPathFile(Server.MapPath("~\\" + item.FilePath));
                    }
                    db.AizantIT_CapaCompltFileAttachments.RemoveRange(objC_CompltAttachment);
                    db.SaveChanges();
                }
            }
        }
        //Delete Incident File Upload Documents Details
        [HttpPost]
        public JsonResult DeleteCapaFileUploadDocuments(int? UniquePKID, string RoleType)
        {
            try
            {
                if (RoleType == "C_Verify")
                {
                    var objC_verificationAttachment = (from objCmpt in db.AizantIT_CAPAVerificationFilesAttachements
                                                       where objCmpt.UniquePKID == UniquePKID
                                                       select objCmpt).ToList();
                    //var _lstDel = dbEF.AizantIT_HODAssessmentAttachmentFile.Where(s => s.AssessmentID == ID && s.isTemp == false);
                    if (objC_verificationAttachment.Count > 0)
                    {
                        QMSAttachments objAttachmentsCommon = new QMSAttachments();
                        foreach (var item in objC_verificationAttachment)
                        {
                            objAttachmentsCommon.RemovePhysicalPathFile(Server.MapPath("~\\" + item.FilePath));
                        }
                        db.AizantIT_CAPAVerificationFilesAttachements.RemoveRange(objC_verificationAttachment);
                        db.SaveChanges();
                    }
                    //    var _ObjDetete = db.AizantIT_CAPAVerificationFilesAttachements.Where(a => a.UniquePKID == UniquePKID).ToList();
                    //db.AizantIT_CAPAVerificationFilesAttachements.Remove(_ObjDetete[0]);
                    //db.SaveChanges();
                }
                if (RoleType == "C_Complt")
                {
                    var objC_CompltAttachment = (from objCmpt in db.AizantIT_CapaCompltFileAttachments
                                                 where objCmpt.UniquePKID == UniquePKID
                                                 select objCmpt).ToList();
                    if (objC_CompltAttachment.Count > 0)
                    {
                        QMSAttachments objAttachmentsCommon = new QMSAttachments();
                        foreach (var item in objC_CompltAttachment)
                        {
                            objAttachmentsCommon.RemovePhysicalPathFile(Server.MapPath("~\\" + item.FilePath));
                        }
                        db.AizantIT_CapaCompltFileAttachments.RemoveRange(objC_CompltAttachment);
                        db.SaveChanges();
                    }
                    //var _ObjDetete = db.AizantIT_CapaCompltFileAttachments.Where(a => a.UniquePKID == UniquePKID).ToList();
                    //db.AizantIT_CapaCompltFileAttachments.Remove(_ObjDetete[0]);
                    //db.SaveChanges();

                }

                return Json(new { hasError = false, data = UniquePKID }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M12:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        //When Files Upload Added to List
        [HttpPost]
        public JsonResult BindCapaFilesToList(CapaModel capaModel)
        {
            try
            {
                AizantIT_CAPAVerificationFilesAttachements objFilebo = new AizantIT_CAPAVerificationFilesAttachements();
                objFilebo.CapaID = capaModel.CAPAID;
                int FileexistsCount = 0;
                foreach (HttpPostedFileBase file in capaModel.CapaFileUpload)
                {
                    if (file != null)
                    {
                        objFilebo.FileName = Path.GetFileName(file.FileName);
                        objFilebo.FileType = Path.GetExtension(file.FileName);
                        var Count = db.AizantIT_CAPAVerificationFilesAttachements.FirstOrDefault(h => h.FileName == objFilebo.FileName
                && h.FileType == objFilebo.FileType && h.CapaID == objFilebo.CapaID);
                        if (Count != null)
                        {
                            FileexistsCount++;
                        }
                    }
                }
                if (FileexistsCount == 0)
                {
                    if (ModelState.IsValid)
                    {
                        if (capaModel.CapaFileUpload[0] != null)
                        {
                            foreach (HttpPostedFileBase file in capaModel.CapaFileUpload)
                            {
                                byte[] bytes;
                                using (BinaryReader br = new BinaryReader(file.InputStream))
                                {
                                    bytes = br.ReadBytes(file.ContentLength);
                                    //objFilebo.CapaAttachament = bytes;
                                }
                                if (file != null)
                                {
                                    objFilebo.FileName = Path.GetFileName(file.FileName);
                                    objFilebo.FileType = Path.GetExtension(file.FileName);
                                    db.AizantIT_CAPAVerificationFilesAttachements.Add(objFilebo);
                                    db.SaveChanges();
                                }
                            }
                        }
                    }
                }
                else
                {
                    return Json(new { msg = "File already exists", msgType = "error", }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { msg = "", msgType = "success", }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message) + ".";
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult BindFileUploadForCAPAComplet()
        {
            try
            {

                if (TempData["FileMsg"] == null)
                {
                    TempData["FileMsg"] = "";
                }
                if (TempData["FileAddedCount"] == null)
                {
                    TempData["FileAddedCount"] = 0;
                }
                if (TempData["FileexistsCount"] == null)
                {
                    TempData["FileexistsCount"] = 0;
                }
                string msg = "";
                var MsgType = "";
                string UploadedFilePath = null;
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        HttpFileCollectionBase files = Request.Files;
                        AizantIT_CapaCompltFileAttachments objFilebo = new AizantIT_CapaCompltFileAttachments();
                        objFilebo.CapaID = Convert.ToInt32(Request.Params["CAPAID"]);

                        var GetCAPADetails = (from objCM in db.AizantIT_CAPAMaster
                                              where objCM.CAPAID == objFilebo.CapaID
                                              select objCM).SingleOrDefault();
                        var CAPANumber = GetCAPADetails.CAPA_Number;
                        var QualityEventNumber = GetCAPADetails.QualityEvent_Number;
                        int FileexistsCount = 0;
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFileBase file = files[i];
                            if (file != null)
                            {
                                objFilebo.FileName = Path.GetFileName(file.FileName);
                                objFilebo.FileType = Path.GetExtension(file.FileName);
                                var Count = db.AizantIT_CapaCompltFileAttachments.FirstOrDefault(h => h.FileName == objFilebo.FileName
                        && h.FileType == objFilebo.FileType && h.CapaID == objFilebo.CapaID);
                                if (Count != null)
                                {
                                    FileexistsCount++;
                                }
                            }
                            if (FileexistsCount == 0)
                            {
                                if (ModelState.IsValid)
                                {
                                    if (file != null)
                                    {
                                        objFilebo.FileName = Path.GetFileName(file.FileName);
                                        objFilebo.FileType = Path.GetExtension(file.FileName);
                                        objFilebo.CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                        objFilebo.CreatedDate = Convert.ToDateTime(DateTime.Now.ToString("dd MMM yyyy HH: mm:ss"));
                                        objFilebo.RoleID = 29;
                                        QMSAttachments objAttachmentsCommon = new QMSAttachments();
                                        UploadedFilePath = objAttachmentsCommon.SaveUploadedFileinPhysicalPath(file, QualityEventNumber + ' ' + '-' + ' ' + CAPANumber, 5);
                                        objFilebo.FilePath = UploadedFilePath;
                                        db.AizantIT_CapaCompltFileAttachments.Add(objFilebo);
                                        db.SaveChanges();
                                        UploadedFilePath = null;
                                        TempData["FileMsg"] += Path.GetFileName(file.FileName) + " File uploaded successfully" + "<br/>";
                                        TempData["FileAddedCount"] = Convert.ToInt32(TempData["FileAddedCount"]) + 1;
                                    }

                                }
                            }
                            else
                            {
                                TempData["FileMsg"] += Path.GetFileName(file.FileName) + " upload failed as file already exists" + "<br/>";
                                TempData["FileexistsCount"] = Convert.ToInt32(TempData["FileexistsCount"]) + 1;
                            }
                        }
                        TempData.Keep("FileMsg");
                        TempData.Keep("FileexistsCount");
                        TempData.Keep("FileAddedCount");

                        if (Convert.ToInt32(TempData["FileexistsCount"]) > 0 && Convert.ToInt32(TempData["FileAddedCount"]) == 0)
                        {
                            MsgType = "error";
                        }
                        if (Convert.ToInt32(TempData["FileAddedCount"]) > 0 && Convert.ToInt32(TempData["FileexistsCount"]) == 0)
                        {
                            MsgType = "success";
                        }
                        if (Convert.ToInt32(TempData["FileAddedCount"]) > 0 && Convert.ToInt32(TempData["FileexistsCount"]) > 0)
                        {
                            MsgType = "info";
                        }
                        transaction.Commit();
                        return Json(new { hasError = false, msg = TempData["FileMsg"], msgType = MsgType }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        if (UploadedFilePath != null)
                        {
                            QMSAttachments objAttachmentsCommon = new QMSAttachments();
                            objAttachmentsCommon.RemovePhysicalPathFile(Server.MapPath("~\\" + UploadedFilePath));
                        }
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {

                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message) + ".";
                return Json(new { msg = message, msgType = "Exceptionerror" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult BindCapaVerificationFilesToList()
        {
            try
            {
                if (TempData["FileMsg"] == null)
                {
                    TempData["FileMsg"] = "";
                }
                if (TempData["FileAddedCount"] == null)
                {
                    TempData["FileAddedCount"] = 0;
                }
                if (TempData["FileexistsCount"] == null)
                {
                    TempData["FileexistsCount"] = 0;
                }
                string msg = "";
                string UploadedFilePath = null;
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        HttpFileCollectionBase files = Request.Files;
                        AizantIT_CAPAVerificationFilesAttachements objFilebo = new AizantIT_CAPAVerificationFilesAttachements();
                        objFilebo.CapaID = Convert.ToInt32(Request.Params["CAPAID"]);
                        objFilebo.EFVerificationID = (Request.Params["VerficationID"]);
                        var GUID = Request.Params["GUID"];
                        if (objFilebo.EFVerificationID == "0")
                        {
                            objFilebo.EFVerificationID = GUID.ToString();
                            TempData["GUIDVerification"] = GUID;
                        }
                        var GetCAPADetails = (from objCM in db.AizantIT_CAPAMaster
                                              where objCM.CAPAID == objFilebo.CapaID
                                              select objCM).SingleOrDefault();
                        var CAPANumber = GetCAPADetails.CAPA_Number;
                        var QualityEventNumber = GetCAPADetails.QualityEvent_Number;

                        int FileexistsCount = 0;
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFileBase file = files[i];
                            if (file != null)
                            {
                                objFilebo.FileName = Path.GetFileName(file.FileName);
                                objFilebo.FileType = Path.GetExtension(file.FileName);
                                var Count = db.AizantIT_CAPAVerificationFilesAttachements.FirstOrDefault(h => h.FileName == objFilebo.FileName
                        && h.FileType == objFilebo.FileType && h.CapaID == objFilebo.CapaID && h.EFVerificationID == objFilebo.EFVerificationID);
                                if (Count != null)
                                {
                                    FileexistsCount++;
                                }
                            }
                            if (FileexistsCount == 0)
                            {
                                if (ModelState.IsValid)
                                {
                                    if (file != null)
                                    {
                                        objFilebo.CreatedBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                                        objFilebo.CreatedDate = Convert.ToDateTime(DateTime.Now.ToString("dd MMM yyyy HH: mm:ss"));
                                        objFilebo.FileName = Path.GetFileName(file.FileName);
                                        objFilebo.FileType = Path.GetExtension(file.FileName);
                                        objFilebo.RoleID = 26;
                                        QMSAttachments objAttachmentsCommon = new QMSAttachments();
                                        UploadedFilePath = objAttachmentsCommon.SaveUploadedFileinPhysicalPath(file, QualityEventNumber + ' ' + '-' + ' ' + CAPANumber, 5);
                                        objFilebo.FilePath = UploadedFilePath;
                                        db.AizantIT_CAPAVerificationFilesAttachements.Add(objFilebo);
                                        db.SaveChanges();
                                        UploadedFilePath = null;
                                        TempData["FileMsg"] += Path.GetFileName(file.FileName) + " File uploaded successfully" + "<br/>";
                                        TempData["FileAddedCount"] = Convert.ToInt32(TempData["FileAddedCount"]) + 1;
                                    }

                                }
                            }
                            else
                            {
                                TempData["FileMsg"] += Path.GetFileName(file.FileName) + " upload failed as file already exists" + "<br/>";
                                TempData["FileexistsCount"] = Convert.ToInt32(TempData["FileexistsCount"]) + 1;
                            }
                        }
                        TempData.Keep("FileMsg");
                        TempData.Keep("FileexistsCount");
                        TempData.Keep("FileAddedCount");
                        var MsgType = "";
                        if (Convert.ToInt32(TempData["FileexistsCount"]) > 0 && Convert.ToInt32(TempData["FileAddedCount"]) == 0)
                        {
                            MsgType = "error";
                        }
                        if (Convert.ToInt32(TempData["FileAddedCount"]) > 0 && Convert.ToInt32(TempData["FileexistsCount"]) == 0)
                        {
                            MsgType = "success";
                        }
                        if (Convert.ToInt32(TempData["FileAddedCount"]) > 0 && Convert.ToInt32(TempData["FileexistsCount"]) > 0)
                        {
                            MsgType = "info";
                        }
                        transaction.Commit();
                        return Json(new { hasError = false, msg = TempData["FileMsg"], msgType = MsgType }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        if (UploadedFilePath != null)
                        {
                            QMSAttachments objAttachmentsCommon = new QMSAttachments();
                            objAttachmentsCommon.RemovePhysicalPathFile(Server.MapPath("~\\" + UploadedFilePath));
                        }
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message) + ".";
                return Json(new { msg = message, msgType = "Exceptionerror" }, JsonRequestBehavior.AllowGet);
            }
        }

        //Get Hod Assessment Uploaded list 
        [HttpGet]
        public JsonResult GetFileUploadList()
        {
            try
            {
                int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                int CAPAID = Convert.ToInt16(Request.Params["CapaID"]);
                string Status = (Request.Params["Status"]).ToString();
                string VerificationID = (Request.Params["VerificationID"]);
                if (VerificationID == "0")
                {
                    TempData.Keep("GUIDVerification");
                    if (TempData["GUIDVerification"] != null)
                        VerificationID = TempData["GUIDVerification"].ToString();
                }
                int filteredCount = 0;
                List<CapaModel> CapaFilesList = new List<CapaModel>();
                int totalRecordsCount = 0;
                int EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                DataSet dthodAttach = Qms_bal.GetCapaCompltFileuploadBAL(displayLength, displayStart, sortCol, sSortDir, sSearch, CAPAID, Status, VerificationID);
                if (dthodAttach.Tables[0].Rows.Count > 0)
                {
                    totalRecordsCount = Convert.ToInt32(dthodAttach.Tables[0].Rows[0]["TotalCount"]);
                    foreach (DataRow rdr in dthodAttach.Tables[0].Rows)
                    {
                        CapaModel objcapabo = new CapaModel();
                        objcapabo.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                        objcapabo.CapaFileUniqueID = Convert.ToInt32(rdr["UniquePKID"]);
                        objcapabo.FileNameComplet = (rdr["Document"].ToString());
                        objcapabo.FileTypeComplet = (rdr["FileType"].ToString());
                        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                        CapaFilesList.Add(objcapabo);
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = CapaFilesList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                // ViewBag.SysError = "Yes";
                return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult RemoveTempData()
        {
            try
            {
                TempData.Remove("FileMsg");
                TempData.Remove("FileexistsCount");
                TempData.Remove("FileAddedCount");
                return Json(new { hasError = false }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { hasError = true, data = HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);

            }

        }

        public ActionResult _AttachmentsCapaComplete()
        {
            TempData.Keep("CAPAID");
            CapaModel objbo = new CapaModel();
            objbo.CAPAID = Convert.ToInt32(TempData["CAPAID"]);
            return PartialView(objbo);
        }

        #endregion

        #region HQAApproval
        [HttpPost]
        public JsonResult SubmitCAPAHQAApproval(MainListCapaBO objcapaModel)
        {
            try
            {
                //Get employee ID
                CapaModel objbo = new CapaModel();
                GetUserEmpID(objbo);
                objcapaModel.EMPId = objbo.EmpID;
                Qms_bal.GetCAPAHQAApprovalBAL(objcapaModel, out int Result);
                if (Result == 0)
                {
                    return Json(new { msg = "", msgType = "success", }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { msg = "You are not authorized HeadQA to do action.", msgType = "error", }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M11:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { msg = ErMsg, msgType = "error", }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region AutoGenerated CAPANumber
        //Capa Number Binding
        [HttpGet]
        public JsonResult CapaNODB(int DeptID)
        {
            try
            {
                DataTable dt1 = new DataTable();
                //1 Means CAPA
                //dt1 = Qms_bal.BalGetAutoGenaratedNumber(DeptID, 1);
                Qms_bal.BalCAPAAutoGenaratedNumber(DeptID, out string CAPANumber, out int CAPA_MaxNo);
                //var CAPANumber = dt1.Rows[0][0].ToString();

                return Json(new { hasError = false, data = CAPANumber, CAPA_MaxNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M13:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Retrieve CAPA Data 
        //Retrieve capa Data
        public CapaModel BindData(int id)
        {

            CapaModel capa = new CapaModel();
            if (Session["UserDetails"] != null)
            {
                capa.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            }

            var capa1 = (from pd in db.AizantIT_CAPAMaster
                         join od in db.AizantIT_CAPA_ActionPlan on pd.CAPAID equals od.CAPAID
                         join Dep in db.AizantIT_DepartmentMaster on od.Capa_Action_Imp_DeptID equals Dep.DeptID
                         join AD in db.AizantIT_ActionPlanDescription on od.Capa_ActionID equals AD.Capa_ActionID
                         join Q in db.AizantIT_QualityEvent on pd.QualityEvent_TypeID equals Q.QualityEvent_TypeID
                         join E in db.AizantIT_EmpMaster on od.Capa_Action_ImpID equals E.EmpID
                         where pd.CAPAID == id
                         select new
                         {
                             od.CAPAID,
                             pd.CAPA_Number,
                             pd.CAPA_Date,
                             pd.QualityEvent_TypeID,
                             od.Capa_Action_Imp_DeptID,
                             od.Capa_Action_ImpID,
                             od.CurrentStatus,
                             pd.Verified_YesOrNO,
                             pd.QualityEvent_Number,
                             od.ActionPlan_TimeLines,
                             od.Capa_ActionID,
                             od.Capa_ActionDesc,
                             pd.QA_EmpID,
                             pd.HeadQA_EmpID,
                             pd.CreatedBy,
                             Dep.DepartmentName,
                             AD.Capa_ActionName,
                             Q.QualityEvent_TypeName,
                             Responsibleperson = (E.FirstName + "" + E.LastName)
                         }).ToList();

            foreach (var dr in capa1)
            {
                capa.CAPAID = Convert.ToInt32(dr.CAPAID);
                capa.CAPA_Number = dr.CAPA_Number.ToString();
                capa.CAPA_Date = Convert.ToDateTime(dr.CAPA_Date).ToString("dd MMM yyyy");
                capa.Department = dr.DepartmentName.ToString();
                capa.DeptID = Convert.ToInt32(dr.Capa_Action_Imp_DeptID);
                capa.EmployeeName = dr.Capa_Action_ImpID.ToString();
                capa.TargetDate = Convert.ToDateTime(dr.ActionPlan_TimeLines).ToString("dd MMM yyyy");
                capa.QualityEvent_TypeID = dr.QualityEvent_TypeID.ToString();
                capa.QualityEvent_Number = dr.QualityEvent_Number.ToString();
                capa.Verified_YesOrNO = dr.Verified_YesOrNO.ToString();
                capa.CurrentStatus = dr.CurrentStatus.ToString();
                capa.PlanofAction = dr.Capa_ActionID;
                capa.ActionPlanName = dr.Capa_ActionName;
                capa.Capa_ActionDesc = dr.Capa_ActionDesc.ToString();
                capa.QA_EmpId = Convert.ToInt32(dr.QA_EmpID);
                capa.CreatedBy = dr.CreatedBy.ToString();
                capa.HeadQa_EmpID = Convert.ToInt32(dr.HeadQA_EmpID);
                capa.QualityEventName = dr.QualityEvent_TypeName;
                capa.ResponsiblePersonName = dr.Responsibleperson;

            }

            //Binding Comments
            int ActionStatus = Convert.ToInt32(capa.CurrentStatus);

            var capacomments = (from cm in db.AizantIT_CAPAMaster
                                join crt in db.AizantIT_Capa_History on cm.CAPAID equals crt.CapaID
                                where crt.CapaID == id && crt.Status == ActionStatus
                                select new
                                {
                                    crt.Comments,
                                }).ToList();
            foreach (var dr1 in capacomments)
            {
                capa.Revertedcomments = dr1.Comments.ToString();
            }

            int CapaCreatedBy = Convert.ToInt32(capa.CreatedBy);

            #region BindDropDowns
            //Bind Department and ActionPlan and qaulityEvent Drop downs
            //BindDropDownCAPACreate(capa);
           
            //Not binding CreatedBy and QA EmployeeId
            List<int> HeadQAList = new List<int>() { CapaCreatedBy, capa.QA_EmpId, Convert.ToInt32(capa.EmployeeName) };


            List<SelectListItem> objAssignHeadQAList = new List<SelectListItem>();
            int HeadQARoleID;
            if (capa.QualityEvent_TypeID == "9")
            {
                HeadQARoleID = 37;/*Audit HeadQA*/
            }
            else
            { HeadQARoleID = 27;/*QMS HEadQA*/    }
            //Bind HeadQA Drop down
            var GetAssignedHeadQA = db.Database.SqlQuery<AccessibleEmpOnDeptandRole>("exec [UMS].[AizantIT_SP_GetEmployeesByRoleIDAndStatus] " +
                         HeadQARoleID + ",'A'").ToList();
            objAssignHeadQAList.Add(new SelectListItem
            {
                Text = "-- Select HeadQA --",
                Value = "0",
            });
            foreach (var HQAItem in GetAssignedHeadQA)
            {
                if (!HeadQAList.Contains(HQAItem.EmpID))
                {
                    objAssignHeadQAList.Add(new SelectListItem()
                    {
                        Text = HQAItem.EmpName,
                        Value = HQAItem.EmpID.ToString(),
                        Selected = (HQAItem.EmpID == capa.HeadQa_EmpID ? true : false)
                    });
                }
            }
            capa.ddlHeadqaEmpId = objAssignHeadQAList;
            
            #endregion

            TempData["EmpID"] = capa.EmpID;
            return capa;
        }

        public MainListCapaBO GetCAPA_Details(int CAPAID)
        {
            MainListCapaBO objCapa = new MainListCapaBO();
            var CapaDetails = (from pd in db.AizantIT_CAPAMaster
                               join od in db.AizantIT_CAPA_ActionPlan on pd.CAPAID equals od.CAPAID
                               join Dp in db.AizantIT_DepartmentMaster on od.Capa_Action_Imp_DeptID equals Dp.DeptID
                               join Emp in db.AizantIT_EmpMaster on od.Capa_Action_ImpID equals Emp.EmpID
                               join AD in db.AizantIT_ActionPlanDescription on od.Capa_ActionID equals AD.Capa_ActionID
                               where pd.CAPAID == CAPAID
                               select new
                               {
                                   od.CAPAID,
                                   pd.CAPA_Number,
                                   pd.CAPA_Date,
                                   pd.QualityEvent_TypeID,
                                   od.Capa_Action_Imp_DeptID,
                                   od.Capa_Action_ImpID,
                                   od.CurrentStatus,
                                   pd.Verified_YesOrNO,
                                   pd.QualityEvent_Number,
                                   od.ActionPlan_TimeLines,
                                   od.Capa_ActionID,
                                   od.Capa_ActionDesc,
                                   Dp.DepartmentName,
                                   Responsiblename = (Emp.FirstName + "" + Emp.LastName),
                                   pd.QA_EmpID,
                                   od.Closuere_Date,
                                   pd.HeadQA_EmpID,
                                   AD.Capa_ActionName,
                                   od.BaseID,
                                   pd.CreatedBy
                               }).ToList();

            foreach (var dr in CapaDetails)
            {
                objCapa.CAPAID = Convert.ToInt32(dr.CAPAID);
                objCapa.CAPA_Number = dr.CAPA_Number.ToString();
                objCapa.CAPA_Date = Convert.ToDateTime(dr.CAPA_Date).ToString("dd MMM yyyy");
                objCapa.Department = dr.DepartmentName;
                objCapa.EmployeeName = dr.Responsiblename;
                objCapa.TargetDate = Convert.ToDateTime(dr.ActionPlan_TimeLines).ToString("dd MMM yyyy");
                objCapa.QualityEvent_TypeID = dr.QualityEvent_TypeID.ToString();
                objCapa.QualityEvent_Number = dr.QualityEvent_Number.ToString();
                objCapa.HeadQAEmpID = Convert.ToInt32(dr.HeadQA_EmpID);
                objCapa.PlanofAction = dr.Capa_ActionName;
                objCapa.EventBaseID = Convert.ToInt32(dr.BaseID);
                objCapa.CAPACreatedBy = Convert.ToInt32(dr.CreatedBy);
                objCapa.QAEmpID = Convert.ToInt32(dr.QA_EmpID);
                objCapa.ResponsibleEmpID = Convert.ToInt32(dr.Capa_Action_ImpID);
                if (dr.Verified_YesOrNO.ToString() == "True")
                {
                    objCapa.Verified_YesOrNO = "Yes";
                }
                else
                {
                    objCapa.Verified_YesOrNO = "No";
                }
                objCapa.CurrentStatus = (int)dr.CurrentStatus;
                objCapa.Capa_ActionDesc = dr.Capa_ActionDesc.ToString();
                if (dr.QA_EmpID != 0)
                {
                    objCapa.Assigned_QA = GetUserFullName((int)dr.QA_EmpID);
                }
                if (dr.CurrentStatus == 4)
                {
                    objCapa.ClosureEffectiveDate = DateTime.Now.ToString("dd MMM yyyy");
                }
            }
            int QualityID = Convert.ToInt32(objCapa.QualityEvent_TypeID);
            objCapa.QualityEventName = (from Q in db.AizantIT_QualityEvent
                                        where Q.QualityEvent_TypeID == QualityID
                                        select Q.QualityEvent_TypeName).SingleOrDefault();
            objCapa.CurrentStatusName = (from S in db.AizantIT_ObjectStatus
                                         where S.ObjID == 7 && S.StatusID == objCapa.CurrentStatus
                                         select S.StatusName).SingleOrDefault();
            return objCapa;
        }
        #endregion

        #region CAPA Details View
        //Capa Details page at QA and Head QA
        public PartialViewResult Details(int id)
        {
            try
            {
                //string Role = Convert.ToString(TempData["R"]);
                TempData.Keep("R");
                //Check roles
                // CheckRoles(Role);
                CapaModel capa = new CapaModel();
                capa = BindData(id);

                if (capa.CurrentStatus == "3")//after capa Accept to check this method
                {
                    RefershDeletedFilesIsTempIsFalse(id, "C_Complt");
                }
                CapaModel getlatestDate = new CapaModel();
                getlatestDate = BindLatestTargetDate(id);
                if (getlatestDate.TargetDate != null)
                {
                    capa.TargetDate = getlatestDate.TargetDate;

                }
                return PartialView(capa);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M15:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }

        #endregion

        #region Get Latest Target Date
        public CapaModel BindLatestTargetDate(int CapaID)
        {
            CapaModel objRm1 = new CapaModel();
            DataSet ds = new DataSet();
            var RM1Count = (from R in db.AizantIT_Capa_Remainder
                            where R.CAPAID == CapaID && R.RemainderStatus == 8
                            select R).Count();
            if (RM1Count != 0)
            {
                ds = Qms_bal.CapaTranaction(Convert.ToInt32(CapaID), "Reminder1");

                if (ds.Tables[0].Rows.Count > 0)
                {
                    objRm1.TargetDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["RevisedTargetDate"]).ToString("dd MMM yyyy");
                }
            }
            var RM2Count = (from R in db.AizantIT_Capa_Remainder
                            where R.CAPAID == CapaID && R.RemainderStatus == 15
                            select R).Count();
            if (RM2Count != 0)
            {
                DataSet ds1 = new DataSet();
                ds1 = Qms_bal.CapaTranaction(Convert.ToInt32(CapaID), "Reminder2");

                if (ds1.Tables[0].Rows.Count > 0)
                {
                    objRm1.TargetDate = Convert.ToDateTime(ds1.Tables[0].Rows[0]["RevisedTargetDate"]).ToString("dd MMM yyyy");
                }
            }
            return objRm1;
        }

        #endregion

        #region Json Database Action
        //Bind Reminder Data and Submitted To Database
        public AizantIT_Capa_Remainder BindCapaData(AizantIT_Capa_Remainder CApaModel1)
        {
            DataTable dt1 = new DataTable();
            CapaReminder capaModel = new CapaReminder();
            capaModel.CAPAID = CApaModel1.CAPAID;
            if (CApaModel1.RevisedTargetDate != null)
            {
                string dat = (CApaModel1.RevisedTargetDate).ToString();
                capaModel.RevisedTargetDate = Convert.ToDateTime(dat).ToString("yyyy/MM/dd");
            }
            else
            {
                capaModel.RevisedTargetDate = "";
            }
            capaModel.HOD_Comments = CApaModel1.HOD_Justify_Notes;
            capaModel.QA_Comments = CApaModel1.QA_Remainder_Notes;
            capaModel.RemainderStatus = CApaModel1.RemainderStatus.ToString();
            capaModel.EmployeeName = CApaModel1.Hod_EmpID.ToString();

            if (Session["UserDetails"] != null)
            {
                capaModel.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            }
            string role = "";
            TempData.Keep("RoleID");
            int RoleID = Convert.ToInt32(TempData["RoleID"]);
            if (RoleID == 26)
            {
                role = "QA";
                capaModel.HOD_Comments = "";
            }
            else if (RoleID == 25)
            {
                role = "HOD";
                capaModel.QA_Comments = "";
            }
            dt1 = Qms_bal.CapaReminderBAL1(capaModel, role, out int Result);
            //Return OutputParmeter 
            CApaModel1.RM_ID = Result;
            return CApaModel1;
        }

        //QA and HeadQa Approve And Revert Action 
        [HttpPost]
        public JsonResult QaSubmittedToDataBase(CapaModel CapaBo)
        {
            try
            {
                DataSet dt = new DataSet();
                dt = Qms_bal.CapaTranactionInsertUpdateBAL(CapaBo, out int ResultOputparm);
                if (ResultOputparm == 0)
                {
                    return Json(new { hasError = false, data = CapaBo }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    return Json(new { hasError = true, data = "You are not authorized Employee to do action." }, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M17:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);

            }
        }

        //Reminder 1 and 2 Submit To DataBase
        [HttpPost]
        public JsonResult ReminderApproveAndRevert(AizantIT_Capa_Remainder CapaReminderBO)
        {
            try
            {
                DataTable dt1 = new DataTable();


                Boolean IsTrue = true;
                if (CapaReminderBO.RemainderStatus == 7 || CapaReminderBO.RemainderStatus == 8)
                {
                    IsTrue = (from T1 in db.AizantIT_Capa_Remainder
                              where T1.RemainderStatus == 6 && T1.CAPAID == CapaReminderBO.CAPAID
                              select T1).Any();
                }
                else if (CapaReminderBO.RemainderStatus == 14 || CapaReminderBO.RemainderStatus == 15)
                {
                    IsTrue = (from T1 in db.AizantIT_Capa_Remainder
                              where T1.RemainderStatus == 13 && T1.CAPAID == CapaReminderBO.CAPAID
                              select T1).Any();
                }
                if (IsTrue == true)
                {
                    AizantIT_Capa_Remainder CapaReminderobj = BindCapaData(CapaReminderBO);
                    if (CapaReminderobj.RM_ID == 0)
                    {
                        return Json(new { hasError = "success", data = CapaReminderobj }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { hasError = "warning", data = "You are not authorized Employee to do action." }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { hasError = "warning", data = "Reminder Approve/Revert action is already submitted." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M18:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = "error", data = ErMsg }, JsonRequestBehavior.AllowGet);

            }

        }

        #endregion

        #region CAPA Main List


        public ActionResult CapaMainList(int CapaID, string R=null, int Notification_ID = 0)
        {
            try
            {
                if (Notification_ID != 0)
                {
                    TempData["R"] = R;
                    TempData.Keep("R");
                    UpdateNotificationLink(Notification_ID);
                }
                TempData.Keep("R");
                TempData["CAPAID"] = CapaID;
                TempData.Keep("CAPAID");
                //ViewBag.VerificationFileExist =0;
                //ViewBag.VerificationFileExist = (from VF in db.AizantIT_CAPAVerificationFilesAttachements
                //                                 where VF.CapaID == CapaID
                //                                 select VF).Count();
                MainListCapaBO objCapa = new MainListCapaBO();
                objCapa = GetCAPA_Details(CapaID);

                #region Deleted IsTemp false  files

                if (objCapa.CurrentStatus == 21)//After capa closed this method checked
                {
                    RefershDeletedFilesIsTempIsFalse(CapaID, "Verify");
                }
                #endregion

                #region CAPAFileUpload at Complete

                List<int> objCAPAFilComplt = new List<int> { 4, 21, 9, 22 };
                ViewBag.CAPAFileUpload = 0;
                if (objCAPAFilComplt.Contains(objCapa.CurrentStatus))
                {

                    ViewBag.CAPAFileUpload = (from CCF in db.AizantIT_CapaCompltFileAttachments
                                              where CCF.CapaID == objCapa.CAPAID
                                              select CCF).Count();
                }

                #endregion

                #region RM1

                ViewBag.RM1Count = (from Rem1 in db.AizantIT_Capa_Remainder
                                    where Rem1.CAPAID == CapaID && Rem1.RemainderStatus == 8
                                    select R).Count();
                if (ViewBag.RM1Count != 0)
                {


                    DataSet ds = new DataSet();
                    ds = Qms_bal.CapaTranaction(Convert.ToInt32(CapaID), "Reminder1");

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        objCapa.HOD_Justify_Notes = ds.Tables[0].Rows[0]["HOD_Justify_Notes"].ToString();
                        objCapa.QA_Remainder_Notes = ds.Tables[0].Rows[0]["QA_Remainder_Notes"].ToString();
                        string dat = Convert.ToDateTime(ds.Tables[0].Rows[0]["RevisedTargetDate"]).ToString("dd MMM yyyy");
                        ViewBag.RevisedTargetDate = dat;
                        objCapa.RevisedTargetDate = Convert.ToDateTime(dat).ToString("dd MMM yyyy");
                        //IF Revised Date Exists replace the Revised Date instead of proposed Date
                        objCapa.TargetDate = objCapa.RevisedTargetDate;
                        objCapa.RemainderStatus = Convert.ToInt32(ds.Tables[0].Rows[0]["RemainderStatus"].ToString());
                        objCapa.CAPAID = Convert.ToInt32(ds.Tables[0].Rows[0]["CAPAID"].ToString());
                        objCapa.HodEmpIDRM1 = Convert.ToInt32(ds.Tables[0].Rows[0]["Hod_EmpID"].ToString());
                        if (objCapa.RemainderStatus != 5)
                        {
                            if (ds.Tables[0].Rows[0]["IsRevisedDate"] == "False")
                            {
                                objCapa.IsRevisedDate = Convert.ToBoolean("false");
                            }
                            else
                            {
                                objCapa.IsRevisedDate = Convert.ToBoolean("True");

                            }
                        }
                        //Get  Hod Employee Name
                        if (objCapa.HodEmpIDRM1 != 0)
                        {
                            objCapa.AssignHODNameRM1 = (from E in db.AizantIT_EmpMaster
                                                        where E.EmpID == objCapa.HodEmpIDRM1
                                                        select (E.FirstName + "" + E.LastName)).SingleOrDefault();
                        }
                        //Bind Reminder Status
                        objCapa.ReminderStatusName = (from S in db.AizantIT_ObjectStatus
                                                      where S.StatusID == objCapa.RemainderStatus && S.ObjID == 7
                                                      select S.StatusName).SingleOrDefault();
                    }
                    var aizantIT_Capa_Remainder = db.AizantIT_Capa_Remainder.Where(a => a.CAPAID == (CapaID)).ToList().FirstOrDefault();
                    AizantIT_CAPA_ActionPlan cAPAMaster = db.AizantIT_CAPA_ActionPlan.Where(c => c.CAPAID == CapaID).FirstOrDefault();
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        ViewBag.ProposedDate = Convert.ToDateTime(cAPAMaster.ActionPlan_TimeLines).ToString("dd MMM yyyy");
                    }
                    else
                    {
                        ViewBag.ProposedDate = Convert.ToDateTime(cAPAMaster.ActionPlan_TimeLines).ToString("dd MMM yyyy");
                    }
                }
                #endregion
                #region RM2
                ViewBag.RM2Count = (from Rem2 in db.AizantIT_Capa_Remainder
                                    where Rem2.CAPAID == CapaID && Rem2.RemainderStatus == 15
                                    select Rem2).Count();
                if (ViewBag.RM2Count != 0)
                {
                    DataSet ds1 = new DataSet();
                    ds1 = Qms_bal.CapaTranaction(Convert.ToInt32(CapaID), "Reminder2");

                    if (ds1.Tables[0].Rows.Count > 0)
                    {
                        objCapa.HOD_Justify_NotesRM2 = ds1.Tables[0].Rows[0]["HOD_Justify_Notes"].ToString();
                        objCapa.QA_Remainder_NotesRM2 = ds1.Tables[0].Rows[0]["QA_Remainder_Notes"].ToString();
                        string dat = Convert.ToDateTime(ds1.Tables[0].Rows[0]["RevisedTargetDate"]).ToString("dd MMM yyyy");
                        objCapa.RevisedTargetDateRM2 = Convert.ToDateTime(dat).ToString("dd MMM yyyy");
                        //IF Revised Date Exists replace the Revised Date instead of proposed Date
                        objCapa.TargetDate = objCapa.RevisedTargetDateRM2;
                        objCapa.RemainderStatusRM2 = Convert.ToInt32(ds1.Tables[0].Rows[0]["RemainderStatus"].ToString());
                        objCapa.CAPAID = Convert.ToInt32(ds1.Tables[0].Rows[0]["CAPAID"].ToString());
                        objCapa.HodEmpIDRM2 = Convert.ToInt32(ds1.Tables[0].Rows[0]["Hod_EmpID"].ToString());

                        if (objCapa.RemainderStatus != 12)
                        {
                            if (ds1.Tables[0].Rows[0]["IsRevisedDate"] == "False")
                            {
                                objCapa.IsRevisedDateRM2 = Convert.ToBoolean("false");
                            }
                            else
                            {
                                objCapa.IsRevisedDateRM2 = Convert.ToBoolean("True");

                            }
                        }
                        if (objCapa.HodEmpIDRM2 != 0)
                        {
                            objCapa.AssignHODNameRM2 = (from E in db.AizantIT_EmpMaster
                                                        where E.EmpID == objCapa.HodEmpIDRM2
                                                        select (E.FirstName + "" + E.LastName)).SingleOrDefault();
                        }
                        //Bind Reminder Status
                        objCapa.ReminderStatusNameRM2 = (from S2 in db.AizantIT_ObjectStatus
                                                         where S2.StatusID == objCapa.RemainderStatusRM2 && S2.ObjID == 7
                                                         select S2.StatusName).SingleOrDefault();
                    }
                    var aizantIT_Capa_Remainder1 = db.AizantIT_Capa_Remainder.Where(a => a.CAPAID == (CapaID)).ToList().FirstOrDefault();
                    AizantIT_CAPA_ActionPlan cAPAMaster12 = db.AizantIT_CAPA_ActionPlan.Where(c => c.CAPAID == CapaID).FirstOrDefault();
                    AizantIT_CAPAMaster objCapaTable = db.AizantIT_CAPAMaster.Find(CapaID);
                    ViewBag.CAPANO = objCapaTable.CAPA_Number;
                    if (ds1.Tables[1].Rows.Count > 0)
                    {
                        ViewBag.ProposedDateRM2 = Convert.ToDateTime(ds1.Tables[1].Rows[0]["RevisedTargetDate"]).ToString("dd MMM yyyy");
                        objCapa.CAPAID = Convert.ToInt32(ds1.Tables[1].Rows[0]["CAPAID"].ToString());
                        if (objCapa.RemainderStatus == null)
                        {
                            objCapa.RemainderStatusRM2 = 0;
                        }
                    }
                    else
                    {
                        ViewBag.ProposedDateRM2 = Convert.ToDateTime(cAPAMaster12.ActionPlan_TimeLines).ToString("dd MMM yyyy");
                    }
                }
                #endregion
                #region CAPA Close
                //CAPA CLosed then call the This Linq query
                List<int> getCloseStatus = new List<int> { 21, 22, 9 };
                ViewBag.CloseCount = 0;
                if (getCloseStatus.Contains(objCapa.CurrentStatus))
                {
                    ViewBag.CloseCount = 1;
                    var CloseCAPA = (from CA in db.AizantIT_CAPA_ActionPlan
                                     join H in db.AizantIT_Capa_History on CA.CAPAID equals H.CapaID
                                     where CA.CAPAID == CapaID && H.Status == 23
                                     select new { CA.Closuere_Date, H.Comments, H.SignDate }).ToList();
                    foreach (var item in CloseCAPA)
                    {
                        objCapa.ClosureEffectiveDate = Convert.ToDateTime(item.Closuere_Date).ToString("dd MMM yyyy");
                        objCapa.ClosureComments = item.Comments;
                        objCapa.VerificationDueDate = Convert.ToDateTime(item.SignDate).AddDays(90).ToString("dd MMM yyyy");
                    }
                }
                #endregion

                #region CAPA verification


                if (objCapa.CurrentStatus == 21)
                {
                    ViewBag.IsPendingVerification = IsPendingVerification(objCapa.CAPAID);

                }
                List<int> getVerificationStatus = new List<int> { 9, 22, 21 };
                ViewBag.VerificationCount = 0;
                if (getVerificationStatus.Contains(objCapa.CurrentStatus))
                {
                    ViewBag.VerificationCount = (from t1 in db.AizantIT_CAPAEffectivenessOfVerification
                                                 where t1.CAPAID == objCapa.CAPAID
                                                 select t1).Count();

                    //    ViewBag.VerificationCount = 1;
                    //    var Verification = (from CA in db.AizantIT_CAPA_ActionPlan
                    //                        join H in db.AizantIT_Capa_History on CA.CAPAID equals H.CapaID
                    //                        where CA.CAPAID == CapaID && H.Status == 9
                    //                        select new { CA.ImplementerDate, H.Comments,H.SignDate }).ToList();
                    //    foreach (var item in Verification)
                    //    {
                    //        objCapa.ImplementorDateVerificationCAPA = Convert.ToDateTime(item.ImplementerDate).ToString("dd MMM yyyy");
                    //        objCapa.VerificationComments = item.Comments;
                    //        objCapa.VerifiedDate =Convert.ToDateTime(item.SignDate).ToString("dd MMM yyyy");
                    //    }
                }
                #endregion

                #region ddlDropdown
                CapaModel objcapabo = new CapaModel();
                GetUserEmpID(objcapabo);

                //Not binding CreatedBy and QA EmployeeId
                List<int> NotBndgHeadQAList = new List<int>() { objcapabo.EmpID };
                List<SelectListItem> objAssignHeadQAList = new List<SelectListItem>();
                int HeadQARoleID;
                if (objCapa.QualityEvent_TypeID == "9")
                {
                    HeadQARoleID = 37;/*Audit HeadQA*/
                }
                else
                {
                    HeadQARoleID = 27;/*QMS HEadQA*/
                }
                //Bind HeadQA Dropdown
                var GetAssignedHeadQA = db.Database.SqlQuery<AccessibleEmpOnDeptandRole>("exec [UMS].[AizantIT_SP_GetEmployeesByRoleIDAndStatus] " +
                             HeadQARoleID + ",'A'").ToList();
                objAssignHeadQAList.Add(new SelectListItem
                {
                    Text = "-- Select HeadQA --",
                    Value = "0",
                });
                foreach (var HQAItem in GetAssignedHeadQA)
                {
                    if (!NotBndgHeadQAList.Contains(HQAItem.EmpID))
                    {
                        objAssignHeadQAList.Add(new SelectListItem()
                        {
                            Text = HQAItem.EmpName,
                            Value = HQAItem.EmpID.ToString(),
                            Selected = (HQAItem.EmpID == objCapa.HeadQAEmpID ? true : false)
                        });
                    }
                }
                objCapa.ddlAssigneToHQA = objAssignHeadQAList;
                #endregion

                #region HQA Approval
                if (objCapa.CurrentStatus == 22)
                {
                    objCapa.HQAApprovalComments = (from H in db.AizantIT_Capa_History
                                                   where H.CapaID == objCapa.CAPAID && H.Status == 24
                                                   select H.Comments).SingleOrDefault();
                }
                #endregion

                return View(objCapa);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M21:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return View();
            }
        }
        #endregion

        [HttpPost]
        public JsonResult AssignedToTempDateaCAPA(int CapaEvent, string DeptCode, int RoleID, string FromDate, string ToDate)
        {
            try
            {
                string FormDateChart = FromDate == "" ? DateTime.Now.AddDays(-365).ToString("dd MMM yyyy") : FromDate;
                string ToDateChart = ToDate == "" ? DateTime.Now.ToString("dd MMM yyyy") : ToDate; ;
                //Get Dept id with DepartmentCode
                var DeptID = (from DM in db.AizantIT_DepartmentMaster
                              where DM.DeptCode == DeptCode
                              select DM.DeptID).SingleOrDefault();
                Hashtable htCharts = new Hashtable();
                htCharts.Add("RoleIDCapaChart", RoleID);
                htCharts.Add("DeptIDCapaChart", DeptID);
                htCharts.Add("FromDateCapachart", FormDateChart);
                htCharts.Add("ToDateCapaChart", ToDateChart);
                htCharts.Add("CapaFileterEvent", CapaEvent);
                Session["ChartsCapaFilterData"] = htCharts;

                return Json(new { hasError = false, data = "" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M22:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult AssignedRoleIdInSession(int RoleID)
        {
            try
            {
                Session["OverDueRoleID"] = RoleID;
                return Json(new { hasError = false, data = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { hasError = true, data = HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);

            }
        }
        [HttpPost]


        #region Report Print


        //For Displaying Image Logo
        //For Binding Company Logo on Incident Report
        DataTable fillCompany()
        {
            CompanyBO objCompanyBO;
            UMS_BAL objUMS_BAL;
            DataTable dtcompany = new DataTable();
            objCompanyBO = new CompanyBO();
            objCompanyBO.Mode = 2;
            objCompanyBO.CompanyID = 0;
            objCompanyBO.CompanyCode = "";
            objCompanyBO.CompanyName = "";
            objCompanyBO.CompanyDescription = "";
            objCompanyBO.CompanyPhoneNo1 = "";
            objCompanyBO.CompanyPhoneNo2 = "";
            objCompanyBO.CompanyFaxNo1 = "";
            objCompanyBO.CompanyFaxNo2 = ""; //txtFaxNo2.Value;
            objCompanyBO.CompanyEmailID = "";
            objCompanyBO.CompanyWebUrl = "";
            objCompanyBO.CompanyAddress = "";
            objCompanyBO.CompanyLocation = "";// txtLocat.Value;
            objCompanyBO.CompanyCity = "";
            objCompanyBO.CompanyState = "";
            objCompanyBO.CompanyCountry = "";
            objCompanyBO.CompanyPinCode = "";
            objCompanyBO.CompanyStartDate = "";
            objCompanyBO.CompanyLogo = new byte[0];

            objUMS_BAL = new UMS_BAL();
            dtcompany = objUMS_BAL.CompanyQuery(objCompanyBO);
            return dtcompany;
        }
        //For Binding Image Logo on Reports
        private System.Drawing.Image BindApplicantImage(byte[] applicantImage)
        {
            using (var ms = new MemoryStream(applicantImage))
            {
                return System.Drawing.Image.FromStream(ms);
            }
        }
        [HttpGet]
        public JsonResult CapaReportView(int DepartmentID, int EventID, string RptFromDate, string RptToDate)
        {
            try
            {
                string Role = "CapaReportList";
                int EmployeeID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                var OverDueCapaRoleID = 0;
                if (Session["OverDueRoleID"] != null)
                {
                    OverDueCapaRoleID = Convert.ToInt32(Session["OverDueRoleID"]);
                }

                QMS_BAL qmsbal = new QMS_BAL();
                CapaModel capa1 = new CapaModel();
                CAPAListViewReport rpt_CAPAList = new CAPAListViewReport();
                //Code for Company Logo
                DataTable _dtCompany = fillCompany();
                if (_dtCompany.Rows.Count != 0)
                {
                    if (!string.IsNullOrEmpty(_dtCompany.Rows[0]["CompanyLogo"].ToString()))
                    {
                        ((XRPictureBox)(rpt_CAPAList.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                    }
                }
                else
                {
                    throw new Exception("Please Upload Company Logo");
                }
                DataSet dsCAPAListFrmDateToDate = Qms_bal.GetCAPAListBal(0, 0, 0, "desc", "", Role, 0, DepartmentID, RptFromDate, RptToDate, 0, OverDueCapaRoleID,
                    DepartmentID, RptFromDate, RptToDate, EmployeeID, EventID, 1,"","","","","","","","");
                if (dsCAPAListFrmDateToDate.Tables[0].Rows.Count > 0)
                {
                    // rpt_CAPAList.CreateDocument();
                    rpt_CAPAList.DataSource = dsCAPAListFrmDateToDate.Tables[0];
                    rpt_CAPAList.DataMember = dsCAPAListFrmDateToDate.Tables[0].TableName;

                    //For Binding Department Name by DepartmentID
                    var DeptName = (from DE in db.AizantIT_DepartmentMaster
                                    where DE.DeptID == DepartmentID
                                    select new { DE.DepartmentName }).ToList();
                    if (DeptName.Count == 0)
                    {
                        rpt_CAPAList.FindControl("xrtblCAPADepartment", true).Visible = false;
                    }
                    foreach (var item in DeptName)
                    {
                        ((XRLabel)(rpt_CAPAList.FindControl("xrlblDept", false))).Text = item.DepartmentName;
                    }

                    ((XRLabel)(rpt_CAPAList.FindControl("xrlblFromDate", false))).Text = RptFromDate;
                    ((XRLabel)(rpt_CAPAList.FindControl("xrlblToDate", false))).Text = RptToDate;

                    //For Binding Quality Event Name by QualityEvent ID
                    var QEventName = (from QE in db.AizantIT_QualityEvent
                                      where QE.QualityEvent_TypeID == EventID
                                      select new { QE.QualityEvent_TypeName }).ToList();
                    if (QEventName.Count == 0)
                    {
                        rpt_CAPAList.FindControl("xrTableEventName", true).Visible = false;
                    }
                    foreach (var item in QEventName)
                    {
                        ((XRLabel)(rpt_CAPAList.FindControl("xrlblEventName", false))).Text = item.QualityEvent_TypeName;
                    }

                    XRTable tblCapaDeptAndEventName = ((XRTable)(rpt_CAPAList.FindControl("xrTableFrmDateToDate", true)));
                    XRLabel lblCapaReport = ((XRLabel)(rpt_CAPAList.FindControl("xrLabel1", true)));

                    if (DeptName.Count == 0 && QEventName.Count == 0)
                    {
                        tblCapaDeptAndEventName.LocationF = new System.Drawing.PointF(518.71F, 83F);
                        lblCapaReport.LocationF = new System.Drawing.PointF(684.98F, 46.54F);
                    }

                    string strFilePath = DynamicFolder.CreateDynamicFolder(11);
                    string fileName = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                    strFilePath = strFilePath + "\\" + fileName;
                    //for Print Path
                    var PrintFilePath = System.Web.VirtualPathUtility.ToAbsolute("/QMSTempReports/" + DateTime.Today.ToString("yyyyMMdd") + "/" + fileName); ;
                    rpt_CAPAList.ExportToPdf(strFilePath);
                    return Json(new { hasError = false, data = fileName, PrintFilePath }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { hasError = false, data = "No Records found" }, JsonRequestBehavior.AllowGet);
                }
                //return View("CapaReportView", rpt_CAPAList);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M21:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region QMS Events History and List

        //This all History Action Method ( NTF,CCN,CAPA,Errata,Incident)
        public PartialViewResult TransactionHistory(int? id, int EventType)
        {
            try
            {
                DataSet dt = new DataSet();
                CapaModel CapaBO = new CapaModel();
                if (EventType == 5)
                {
                    ViewBag.HistoryTitleCapa = (from CAPA in db.AizantIT_CAPAMaster
                                                where CAPA.CAPAID == id
                                                select CAPA.CAPA_Number).SingleOrDefault();
                    ViewBag.TitleName = "CAPA";
                }
                if (EventType == 2)
                {
                    ViewBag.HistoryTitleCapa = (from CCN in db.AizantIT_ChangeControlMaster
                                                where CCN.CCN_ID == id
                                                select CCN.CCN_No).SingleOrDefault();
                    ViewBag.TitleName = "Change Control Note";
                }
                if (EventType == 4)
                {
                    ViewBag.HistoryTitleCapa = (from NTF in db.AizantIT_NoteToFileMaster
                                                where NTF.NTF_ID == id
                                                select NTF.NoteFile_Number).SingleOrDefault();
                    ViewBag.TitleName = "Note to file";
                }
                if (EventType == 3)
                {
                    ViewBag.HistoryTitleCapa = (from E in db.AizantIT_ErrataMaster
                                                where E.ER_ID == id
                                                select E.ER_FileNumber).SingleOrDefault();
                    ViewBag.TitleName = "Errata";
                }
                if (EventType == 1)
                {
                    ViewBag.HistoryTitleCapa = (from IN in db.AizantIT_IncidentMaster
                                                where IN.IncidentMasterID == id
                                                select IN.IRNumber).SingleOrDefault();
                    ViewBag.TitleName = "Incident";
                }
                if (EventType == 501)
                {
                    ViewBag.HistoryTitleCapa = (from IN in db.AizantIT_TypeChangeMaster
                                                where IN.TC_ID == id
                                                select IN.TypeofChange).SingleOrDefault();
                    ViewBag.TitleName = "Type of Change";
                }
                if (EventType == 502)
                {
                    ViewBag.HistoryTitleCapa = (from IN in db.AizantIT_ImpactPoints
                                                where IN.Imp_Desc_ID == id
                                                select IN.Impact_Description).SingleOrDefault();
                    ViewBag.TitleName = "Impact Points";
                }
                if (EventType == 503)
                {
                    ViewBag.HistoryTitleCapa = (from IN in db.AizantIT_BlockMaster
                                                where IN.BlockID == id
                                                select IN.BlockName).SingleOrDefault();
                    ViewBag.TitleName = "Block Master";
                }
                if (EventType == 504)
                {
                    ViewBag.HistoryTitleCapa = (from IN in db.AizantIT_ActionPlanDescription
                                                where IN.Capa_ActionID == id
                                                select IN.Capa_ActionName).SingleOrDefault();
                    ViewBag.TitleName = "Action Plan";
                }
                if (EventType == 505)
                {
                    ViewBag.HistoryTitleCapa = (from t1 in db.AizantIT_ImpactDeptLink
                                                join t2 in db.AizantIT_ImpactPoints on t1.Imp_Desc_ID equals t2.Imp_Desc_ID
                                                join t3 in db.AizantIT_DepartmentMaster on t1.DeptID equals t3.DeptID
                                                join t4 in db.AizantIT_BlockMaster on t1.BlockID equals t4.BlockID
                                                where t1.ImpPoint_ID == id
                                                select t2.Impact_Description + "/" + t3.DeptCode + "/" + t4.BlockName).SingleOrDefault();
                    ViewBag.TitleName = "Impact Dept Link";
                }
                if (EventType == 506)//For Incident TTS History
                {
                    var GetSubRefID = (from t1 in db.AizantIT_TTS_GeneralSub
                                       where t1.TTS_ID == id
                                       select t1.SubRef_ID).SingleOrDefault();

                    if (GetSubRefID != 0)
                    {
                        ViewBag.HistoryTitleCapa = (from T1 in db.AizantIT_IncidentMaster
                                                    where T1.IncidentMasterID == GetSubRefID
                                                    select T1.IRNumber).SingleOrDefault();
                    }
                    else
                    {
                        ViewBag.HistoryTitleCapa = (from T1 in db.AizantIT_IncidentMaster
                                                    join T2 in db.AizantIT_QITTS_Master
                                                    on T1.IncidentMasterID equals T2.Incident_ID
                                                    where T2.TTS_ID == id
                                                    select T1.IRNumber).SingleOrDefault();
                    }
                    ViewBag.TitleName = "Training Schedule History";
                }

                CapaReminder objcapabo = new CapaReminder();
                return PartialView("/Areas/QMS/Views/Shared/QMS/_TransactionHistory.cshtml");
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M4:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }

        [HttpGet]
        //CCN,Notetofile,Errata,Incident,CAPA History Data
        public JsonResult GetQMSEventsHistoryList()
        {
            try
            {
                //
                int iDisplayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int iDisplayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int iSortCol_0 = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir_0 = Request.Params["sSortDir_0"];
                var sSearch =Convert.ToString(Request.Params["sSearch"]);
                int EventID = Convert.ToInt32(Request.Params["EventID"]);
                int EventTypeID = Convert.ToInt32(Request.Params["EventTypeID"]);
                var IncidentStatusTTSID = Request.Params["TMS_TTSID"];
                int totalRecordsCount = 0;
                List<HistoryBo> historylst = new List<HistoryBo>();
                if (IncidentStatusTTSID == "16")//TTS History HQA Approved 
                {
                        TMS_BAL objTMS_Bal = new TMS_BAL();
                        DataTable dt = objTMS_Bal.GetTTS_ActionHistory(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, "",Convert.ToString(EventID));
                    DataView dv = dt.DefaultView;
                    dv.Sort = "ActionDate desc";
                    DataTable sortedDT = dv.ToTable();
                    if (sortedDT.Rows.Count > 0)
                        {
                            totalRecordsCount = Convert.ToInt32(sortedDT.Rows[0]["TotalCount"]);
                            for (int i =0; i < sortedDT.Rows.Count; i++)
                            {
                                historylst.Add(
                            new HistoryBo(Convert.ToInt32(sortedDT.Rows[i]["RowNumber"]), 0, sortedDT.Rows[i]["ActionRole"].ToString(), sortedDT.Rows[i]["ActionStatus"].ToString(), sortedDT.Rows[i]["ActionBy"].ToString(), sortedDT.Rows[i]["ActionDate"].ToString(),
                            sortedDT.Rows[i]["Remarks"].ToString(), "", 0, "", "", "",""));
                            }
                        }
                    }
                else
                {
                    var GetHistoryDetails = db.AizantIT_SP_GetHistoryTransactions(
                        iDisplayLength,
                        iDisplayStart,
                        iSortCol_0,
                        sSortDir_0,
                        sSearch,
                        EventID,
                        EventTypeID
                        );
                    foreach (var item in GetHistoryDetails)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        historylst.Add(new HistoryBo(Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.ActionHistoryID), item.ActionRole, item.ActionStatus
                            , item.ActionBy, item.ActionDate, item.Comments == "" ? "N/A" : item.Comments,Convert.ToDateTime(item.TargetDate).ToString("dd MMM yyyy") == "01 Jan 1900" ? "N/A" : Convert.ToDateTime(item.TargetDate).ToString("dd MMM yyyy"),
                            Convert.ToInt32(item.CommLength), item.offComments, item.AssigneTo == "N/A" ? "N/A" : item.AssigneTo + " (" + item.AssignedToRoleID + ")", item.AssignedToRoleID, item.SubmitData));
                    }
                }
             
                return Json(new
                {
                    hasError = false,
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = historylst
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region AuditDetails View

        public ActionResult _GetAuditView(int ResponseID)
        {
            try
            {
                CapaAuditView objbo = new CapaAuditView();
                DataSet ds = Qms_bal.GetCapaAuditDetailsBAL(ResponseID);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        objbo.AuditNumber = dr["AuditNumber"].ToString();
                        objbo.CategoryName = dr["CategoryName"].ToString();
                        objbo.ObservationNumber = dr["ObservationNumber"].ToString();
                        objbo.CategoryName = dr["CategoryName"].ToString();
                        objbo.ObservationCurrentStatus = dr["ObservationCurrentStatus"].ToString();
                        objbo.AuditCurrentStatus = dr["AuditCurrentStatus"].ToString();
                        objbo.ObservationPriority = dr["ObservationPriority"].ToString();
                        objbo.ObservationDescription = dr["ObservationDescription"].ToString();
                        objbo.SiteName = dr["SiteName"].ToString();
                        objbo.DepartmentName = dr["DepartmentName"].ToString();
                        objbo.ResponseDueDate = dr["ResponseDueDate"].ToString();
                    }

                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    List<ObservationSystemCAPA> objSysList = new List<ObservationSystemCAPA>();
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        objSysList.Add(new ObservationSystemCAPA()
                        {
                            FDA_SystemName = dr["FDA_SystemName"].ToString(),
                            AreaName = dr["AreaName"].ToString(),
                            SubAreaName = dr["SubAreaName"].ToString(),
                            Remarks = dr["Remarks"].ToString(),

                        });

                    }
                    objbo.ObservationSystems = objSysList;
                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[2].Rows)
                    {
                        objbo.ResponseDescription = dr["ResponseDescription"].ToString();
                        objbo.ResponseDate = dr["ResponseDate"].ToString();
                        objbo.ResponseExpectedOrCloserDate = dr["ExpectedOrCloserDate"].ToString();
                    }
                }
                return PartialView(objbo);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M:" + LineNo + "  " + ex.Message + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }
        #endregion

        #region CAPA Re-AssignEmployee
        public PartialViewResult _CAPAManage(int CAPAID)
        {
            CapaModel objcapabo = new CapaModel();
            objcapabo = BindData(CAPAID);
            int ResponsibleEmpID = Convert.ToInt32(objcapabo.EmployeeName);
            int VerificationHQAID = 0; int VerifiCurrentStatus = 0;
            var GetEffectiveness = (from t1 in db.AizantIT_CAPAEffectivenessOfVerification
                                    where t1.CAPAID == CAPAID && (t1.CurrentStatus == 1 || t1.CurrentStatus == 4)
                                    select t1).ToList();
            foreach (var item in GetEffectiveness)
            {
                VerificationHQAID = (int)item.HeadQAEmpID;
                VerifiCurrentStatus = (int)item.CurrentStatus;
            }
            ViewBag.GetVerificationStatus = db.AizantIT_CAPAEffectivenessOfVerification
                             .Where(x
                                 => x.CAPAID == CAPAID
                                )
                             .OrderByDescending(x => x.EFV_ID)
                             .Take(1)
                             .Select(x => x.CurrentStatus)
                             .ToList()
                             .FirstOrDefault();
            if (ViewBag.GetVerificationStatus == null)
                ViewBag.GetVerificationStatus = 0;
            int GetReminde1rHODEmpid = 0;
            int GetReminde2rHODEmpid = 0;
            List<string> notbinding = new List<string> { "9", "22" };
            //if (!notbinding.Contains( objcapabo.CurrentStatus))// || objcapabo.CurrentStatus != "22")
            //{
            List<int?> Reminderstatus1 = new List<int?> { 5, 6, 7, 8 };
            var GetReminde1rDetails = (from R in db.AizantIT_Capa_Remainder
                                       where R.CAPAID == CAPAID && Reminderstatus1.Contains(R.RemainderStatus)
                                       select new { R.Hod_EmpID, R.RemainderStatus }).ToList();
            foreach (var item in GetReminde1rDetails)
            {
                objcapabo.ReminderStatus1 = (int)item.RemainderStatus;
                GetReminde1rHODEmpid = (int)item.Hod_EmpID;
            }
            List<int?> Reminderstatus2 = new List<int?> { 12, 13, 14, 15 };
            var GetReminde2rDetails = (from R in db.AizantIT_Capa_Remainder
                                       where R.CAPAID == CAPAID && Reminderstatus2.Contains(R.RemainderStatus)
                                       select new { R.Hod_EmpID, R.RemainderStatus }).ToList();
            foreach (var item in GetReminde2rDetails)
            {
                objcapabo.ReminderStatus2 = (int)item.RemainderStatus;
                GetReminde2rHODEmpid = (int)item.Hod_EmpID;
            }
            //}
            //objcapabo.ReminderStatus =;
            int QualityID = Convert.ToInt32(objcapabo.QualityEvent_TypeID);
            objcapabo.QualityEventName = (from Q in db.AizantIT_QualityEvent
                                          where Q.QualityEvent_TypeID == QualityID
                                          select Q.QualityEvent_TypeName).SingleOrDefault();
            if (objcapabo.Verified_YesOrNO.ToString() == "True")
            {
                objcapabo.Verified_YesOrNO = "Yes";
            }
            else
            {
                objcapabo.Verified_YesOrNO = "No";
            }
            if (objcapabo.QA_EmpId != 0)
            {
                objcapabo.Assigned_QA = (from E in db.AizantIT_EmpMaster
                                         where E.EmpID == objcapabo.QA_EmpId
                                         select (E.FirstName + "" + E.LastName)).SingleOrDefault();
            }
            List<ManageAdmin> bindddl = new List<ManageAdmin>();
            int QualityEventNumber;
            if (QualityID == 9)
            {
                QualityEventNumber = 9;
            }
            else
            {
                QualityEventNumber = 5;
            }
            DataSet ds = Qms_bal.GetEmployeesDeptandRoleWiseInAdminManageBAL(objcapabo.DeptID, QualityEventNumber);

            for (int i = 0; i < 6; i++)
            {
                //HOD- dropdown
                if (i == 0)
                {
                    List<SelectListItem> objAssignHod = new List<SelectListItem>();
                    objAssignHod.Add(new SelectListItem
                    {
                        Text = "Select",
                        Value = "0",
                    });
                    int CreatedBy = Convert.ToInt32(objcapabo.CreatedBy);
                    foreach (DataRow rdr in ds.Tables[1].Rows)
                    {
                        objAssignHod.Add(new SelectListItem
                        {
                            Text = (rdr["EmpName"]).ToString(),
                            Value = (rdr["EmpID"]).ToString(),
                            Selected = (Convert.ToInt32(rdr["EmpID"]) == CreatedBy ? true : false)
                        });
                    }
                    bindddl.Add(new ManageAdmin
                    {
                        ddlManageList = objAssignHod.ToList(),
                        ManageRoleName = "HOD",
                        ManageSelectedEmp = CreatedBy,
                        SelectedEmployeeName = GetUserFullName(CreatedBy)

                    });
                }
                //QA dropdown
                if (i == 1)
                {

                    List<int> QaEmployeeid = new List<int>() { Convert.ToInt16(objcapabo.CreatedBy), ResponsibleEmpID, objcapabo.HeadQa_EmpID };
                    List<SelectListItem> AssignedQAList = new List<SelectListItem>();
                    AssignedQAList.Add(new SelectListItem
                    {
                        Text = "Select",
                        Value = "0",
                    });
                    foreach (DataRow rdr in ds.Tables[2].Rows)
                    {
                        if (!QaEmployeeid.Contains((int)(rdr["EmpID"])))
                        {
                            AssignedQAList.Add(new SelectListItem
                            {
                                Text = (rdr["EmpName"]).ToString(),
                                Value = (rdr["EmpID"]).ToString(),
                                Selected = (Convert.ToInt32(rdr["EmpID"]) == objcapabo.QA_EmpId ? true : false)
                            });
                        }
                    }
                    bindddl.Add(new ManageAdmin
                    {
                        ddlManageList = AssignedQAList.ToList(),
                        ManageRoleName = "QA",
                        ManageSelectedEmp = objcapabo.QA_EmpId,
                        SelectedEmployeeName = GetUserFullName(objcapabo.QA_EmpId)
                    });
                }
                //HeadQA
                if (i == 2)
                {
                    int HQAEMPID = 0;
                    if (VerifiCurrentStatus == 1 || VerifiCurrentStatus == 4)
                    {
                        HQAEMPID = VerificationHQAID;
                    }
                    else
                    {
                        HQAEMPID = objcapabo.HeadQa_EmpID;
                    }
                    List<int> HeadQAEmployeeid = new List<int>() { Convert.ToInt16(objcapabo.CreatedBy), objcapabo.QA_EmpId, ResponsibleEmpID };
                    List<SelectListItem> AssignedHQAList = new List<SelectListItem>();
                    AssignedHQAList.Add(new SelectListItem
                    {
                        Text = "Select",
                        Value = "0",
                    });
                    foreach (DataRow rdr in ds.Tables[3].Rows)
                    {
                        if (!HeadQAEmployeeid.Contains((int)(rdr["EmpID"])))
                        {
                            AssignedHQAList.Add(new SelectListItem
                            {
                                Text = (rdr["EmpName"]).ToString(),
                                Value = (rdr["EmpID"]).ToString(),
                                Selected = (Convert.ToInt32(rdr["EmpID"]) == HQAEMPID ? true : false)
                            });
                        }
                    }
                    bindddl.Add(new ManageAdmin
                    {
                        ddlManageList = AssignedHQAList.ToList(),
                        ManageRoleName = "HeadQA",
                        ManageSelectedEmp = HQAEMPID,
                        SelectedEmployeeName = GetUserFullName(HQAEMPID)
                    });
                }
                //User
                if (i == 3)
                {
                    List<SelectListItem> objAssignedResponsible = new List<SelectListItem>();
                    List<int> notbindingEmpID = new List<int> { objcapabo.QA_EmpId, objcapabo.HeadQa_EmpID };
                    objAssignedResponsible.Add(new SelectListItem
                    {
                        Text = "Select",
                        Value = "0",
                    });
                    foreach (DataRow rdr in ds.Tables[4].Rows)
                    {
                        if (!notbindingEmpID.Contains(Convert.ToInt32(rdr["EmpID"])))
                        {
                            objAssignedResponsible.Add(new SelectListItem
                            {
                                Text = (rdr["EmpName"]).ToString(),
                                Value = (rdr["EmpID"]).ToString(),
                                Selected = (Convert.ToInt32(rdr["EmpID"]) == ResponsibleEmpID ? true : false)
                            });
                        }
                    }
                    //Initiator dropdown
                    bindddl.Add(new ManageAdmin
                    {
                        ddlManageList = objAssignedResponsible.ToList(),
                        ManageRoleName = "User",
                        ManageSelectedEmp = ResponsibleEmpID,
                        SelectedEmployeeName = GetUserFullName(ResponsibleEmpID)
                    });
                }
                //Reminder1
                if (i == 4)
                {
                    #region Reminders

                    if (GetReminde1rHODEmpid != 0)
                    {

                        List<SelectListItem> objReminder1HOD = new List<SelectListItem>();
                        objReminder1HOD.Add(new SelectListItem
                        {
                            Text = "Select",
                            Value = "0",
                        });
                        foreach (DataRow rdr in ds.Tables[1].Rows)
                        {
                            if (objcapabo.QA_EmpId != Convert.ToInt32(rdr["EmpID"]))
                            {
                                objReminder1HOD.Add(new SelectListItem
                                {
                                    Text = (rdr["EmpName"]).ToString(),
                                    Value = (rdr["EmpID"]).ToString(),
                                    Selected = (Convert.ToInt32(rdr["EmpID"]) == GetReminde1rHODEmpid ? true : false)
                                });
                            }
                        }
                        //Initiator dropdown
                        bindddl.Add(new ManageAdmin
                        {
                            ddlManageList = objReminder1HOD.ToList(),
                            ManageRoleName = "Reminder1",
                            ManageSelectedEmp = (int)GetReminde1rHODEmpid,
                            SelectedEmployeeName = GetUserFullName(GetReminde1rHODEmpid)
                        });
                    }
                    #endregion
                }
                //Reminder 2
                if (i == 5)
                {
                    #region Reminders2

                    if (GetReminde2rHODEmpid != 0)
                    {
                        List<SelectListItem> objReminder1HOD = new List<SelectListItem>();
                        objReminder1HOD.Add(new SelectListItem
                        {
                            Text = "Select",
                            Value = "0",
                        });
                        foreach (DataRow rdr in ds.Tables[1].Rows)
                        {
                            if (objcapabo.QA_EmpId != Convert.ToInt32(rdr["EmpID"]))
                            {
                                objReminder1HOD.Add(new SelectListItem
                                {
                                    Text = (rdr["EmpName"]).ToString(),
                                    Value = (rdr["EmpID"]).ToString(),
                                    Selected = (Convert.ToInt32(rdr["EmpID"]) == GetReminde2rHODEmpid ? true : false)
                                });
                            }
                        }
                        //Initiator dropdown
                        bindddl.Add(new ManageAdmin
                        {
                            ddlManageList = objReminder1HOD.ToList(),
                            ManageRoleName = "Reminder2",
                            ManageSelectedEmp = (int)GetReminde2rHODEmpid,
                            SelectedEmployeeName = GetUserFullName(GetReminde2rHODEmpid)
                        });
                    }
                    #endregion
                }

            }

            ViewBag.GetDetailsAdminMange = bindddl.AsEnumerable();

            objcapabo.EmployeeName = (from EM in db.AizantIT_EmpMaster
                                      where EM.EmpID == ResponsibleEmpID
                                      select EM.FirstName + "" + EM.LastName
                                       ).SingleOrDefault();
            //objcapabo.EmployeeName = ResponsiblePersonName;

            return PartialView(objcapabo);
        }
        [HttpPost]
        public JsonResult ModifiedEmployeesInAdminMange(int EmployeeID, int CapaID, int Status, string GetReAssignComments)
        {

            try
            {
                CCNBal objbl = new CCNBal();
                int ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                int i = objbl.ModifiedCAPA_EmployeesInAdminManageBAL(EmployeeID, CapaID, Status, ActionBy, GetReAssignComments, out int Result);
                if (Result == 0)
                {
                    return Json(new { hasError = false, msgType = "success" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { hasError = true, msgType = "warning", data = "Modification of Employee is Restricted, as the Transaction is Completed" }, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { hasError = true, msgType = "error", data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);
            }

        }
        #endregion


        #region Events CAPA Creation NTF CCN Incident and Audit


        public ActionResult _CAPACreation(int QaulityEventTypeID, int CAPAID, string QualityEventNumber = "0", int EventDepTID = 0, int TranstID = 0, int AssessmentID = 0, int Status = 0)
        {
            CapaModel capa = new CapaModel();
            capa.AssessementUniqueID = AssessmentID;

            var capa1 = (from pd in db.AizantIT_CAPAMaster
                         join od in db.AizantIT_CAPA_ActionPlan on pd.CAPAID equals od.CAPAID
                         where pd.CAPAID == CAPAID
                         select new
                         {
                             od.CAPAID,
                             pd.CAPA_Date,
                             pd.QualityEvent_TypeID,
                             od.Capa_Action_Imp_DeptID,
                             od.Capa_Action_ImpID,
                             od.CurrentStatus,
                             pd.QualityEvent_Number,
                             od.ActionPlan_TimeLines,
                             od.Capa_ActionID,
                             od.Capa_ActionDesc,
                             pd.QA_EmpID,
                             od.BaseID,
                             pd.CreatedBy,
                         }).ToList();

            foreach (var dr in capa1)
            {
                capa.CAPAID = Convert.ToInt32(dr.CAPAID);
                capa.CAPA_Date = Convert.ToDateTime(dr.CAPA_Date).ToString("dd MMM yyyy");
                capa.DeptID = Convert.ToInt32(dr.Capa_Action_Imp_DeptID);
                capa.EmployeeName = dr.Capa_Action_ImpID.ToString();
                capa.TargetDate = Convert.ToDateTime(dr.ActionPlan_TimeLines).ToString("dd MMM yyyy");
                capa.QualityEvent_TypeID = dr.QualityEvent_TypeID.ToString();
                capa.QualityEvent_Number = dr.QualityEvent_Number.ToString();
                capa.CurrentStatus = dr.CurrentStatus.ToString();
                capa.PlanofAction = dr.Capa_ActionID;
                capa.Capa_ActionDesc = dr.Capa_ActionDesc.ToString();
                capa.CapaQAEmpId = Convert.ToInt32(dr.QA_EmpID);
                capa.EventsBaseID = Convert.ToInt32(dr.BaseID);
                capa.CreatedBy = (dr.CreatedBy).ToString();
            }
            if (QaulityEventTypeID != 0)
            {
                capa.QualityEvent_TypeID = Convert.ToString(QaulityEventTypeID);
            }
            if (QualityEventNumber != "0")
            {
                capa.QualityEvent_Number = QualityEventNumber;
            }
            if (TranstID != 0)
            {
                capa.EventsBaseID = TranstID;
            }

            if (CAPAID == 0)
            {
                capa.CAPAID = 0;
                capa.DeptID = EventDepTID;
                capa.CreatedBy = ((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]).ToString();
            }
            List<SelectListItem> BindDepartList = new List<SelectListItem>();
            BindDepartList.Add(new SelectListItem
            {
                Text = "Select",
                Value = "0",
            });
            var GetAllDeprtment = (from D in db.AizantIT_DepartmentMaster
                                   select new { D.DepartmentName, D.DeptID }).ToList();
            foreach (var item in GetAllDeprtment)
            {
                BindDepartList.Add(new SelectListItem
                {
                    Text = item.DepartmentName,
                    Value = item.DeptID.ToString(),
                    Selected = (Convert.ToInt32(item.DeptID) == capa.DeptID ? true : false)
                });
            }
            capa.ddlDeptList = BindDepartList;
            List<SelectListItem> BindQualityEvnets = new List<SelectListItem>();
            BindQualityEvnets.Add(new SelectListItem
            {
                Text = "Select",
                Value = "0",
            });
            var GetQualityEvents = (from Q in db.AizantIT_QualityEvent
                                    select new { Q.QualityEvent_TypeName, Q.QualityEvent_TypeID }).ToList();
            foreach (var item in GetQualityEvents)
            {
                BindQualityEvnets.Add(new SelectListItem
                {
                    Text = item.QualityEvent_TypeName,
                    Value = item.QualityEvent_TypeID.ToString(),
                    Selected = (Convert.ToInt32(item.QualityEvent_TypeID) == QaulityEventTypeID ? true : false)
                });
            }
            capa.ddlqualityEventList = BindQualityEvnets;

            List<SelectListItem> Actionplanlst = new List<SelectListItem>();
            var ActionPlan = (from AD in db.AizantIT_ActionPlanDescription
                              where AD.CurrentStatus == true
                              select new { AD.Capa_ActionID, AD.Capa_ActionName }).ToList();
            Actionplanlst.Add(new SelectListItem
            {
                Text = "Select",
                Value = "0",
            });
            foreach (var item in ActionPlan)
            {
                Actionplanlst.Add(new SelectListItem
                {
                    Text = item.Capa_ActionName,
                    Value = item.Capa_ActionID.ToString(),
                    Selected = (item.Capa_ActionID == capa.PlanofAction ? true : false)
                });
            }
            capa.ddlActionplan = Actionplanlst;

            //Bind Responsible Person drop down only User Role Employees are binded 
            List<SelectListItem> ObjResponsiblePersonsList = new List<SelectListItem>();
            var ResponsibleItemsList = db.Database.SqlQuery<AccessibleEmpOnDeptandRole>("exec UMS.AizantIT_SP_ToGetEmployeesByDeptIDandRoleID " +
                        +capa.DeptID + "," + (int)QMS_Roles.User + ",1").ToList();
            ObjResponsiblePersonsList.Add(new SelectListItem
            {
                Text = "-- Select Responsible Person --",
                Value = "0",
            });
            foreach (var RPItem in ResponsibleItemsList)
            {
                if (capa.CreatedBy != RPItem.EmpID.ToString())
                {
                    ObjResponsiblePersonsList.Add(new SelectListItem()
                    {
                        Text = RPItem.EmpName,
                        Value = RPItem.EmpID.ToString(),
                        Selected = (RPItem.EmpID.ToString() == capa.EmployeeName ? true : false)
                    });
                }
            }
            capa.ddlResponsiblePerson = ObjResponsiblePersonsList;
            int QARoleID;
            int HODRoleID;
            if (capa.QualityEvent_TypeID == "9")
            {
                QARoleID = (int)QMS_Audit_Roles.AuditQA;
                HODRoleID = (int)QMS_Audit_Roles.AuditHOD;
            }
            else
            {
                QARoleID = (int)QMS_Roles.QA;
                HODRoleID = (int)QMS_Roles.HOD;
            }

            //Capa QA Employee id binded
            int LoginEmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            List<int> CapaQaEmployeeid = new List<int>() { LoginEmpID, Convert.ToInt32(capa.CreatedBy), Convert.ToInt32(capa.EmployeeName) };
            List<SelectListItem> objAssignQA = new List<SelectListItem>();


            var GetQAEmployees = db.Database.SqlQuery<AccessibleEmpOnDeptandRole>("exec [UMS].[AizantIT_SP_GetEmployeesByRoleIDAndStatus] " +
                         QARoleID + ",'A'").ToList();

            objAssignQA.Add(new SelectListItem
            {
                Text = "Select QA",
                Value = "0",
            });
            foreach (var item in GetQAEmployees)
            {
                if (Status == 1)
                {
                    objAssignQA.Add(new SelectListItem()
                    {
                        Text = item.EmpName,
                        Value = item.EmpID.ToString(),
                        Selected = (Convert.ToInt32(item.EmpID) == capa.CapaQAEmpId ? true : false)
                    });
                }
                else
                {
                    if (!CapaQaEmployeeid.Contains(item.EmpID))
                    {
                        objAssignQA.Add(new SelectListItem()
                        {
                            Text = item.EmpName,
                            Value = item.EmpID.ToString(),
                            Selected = (Convert.ToInt32(item.EmpID) == capa.CapaQAEmpId ? true : false)
                        });
                    }
                }
            }

            capa.ddlQAEmployeeID = objAssignQA;
            //Bind HOD's
            List<SelectListItem> objAssignHod = new List<SelectListItem>();
            DataTable dtemp = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(capa.DeptID, HODRoleID, true); //25=hod
            objAssignHod.Add(new SelectListItem { Text = "Select", Value = "0" });
            foreach (DataRow RPItem in dtemp.Rows)
            {
                objAssignHod.Add(new SelectListItem()
                {
                    Text = (RPItem["EmpName"]).ToString(),
                    Value = (RPItem["EmpID"]).ToString(),
                    Selected = ((RPItem["EmpID"]).ToString() == (capa.CreatedBy).ToString() ? true : false)

                });
            }
            capa.ddlCAPAHODList = objAssignHod;

            return PartialView("/Areas/QMS/Views/Shared/QMS/_CAPACreation.cshtml", capa);
        }
        [HttpGet]
        public JsonResult BindddlAssignedToQAInCAPA(int ResponsibleID, int QualityEventTypeId = 0)
        {
            try
            {
                int RoleID;
                if (QualityEventTypeId == 9)
                {
                    RoleID = (int)QMS_Audit_Roles.AuditQA;
                }
                else
                {
                    RoleID = (int)QMS_Roles.QA;
                }
                int LoginEmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                List<int> QaEmployeeid = new List<int>() { LoginEmpID, ResponsibleID };

                List<SelectListItem> objAssignQA = new List<SelectListItem>();
                var AssignedHOD = db.Database.SqlQuery<AccessibleEmpOnDeptandRole>("exec UMS.AizantIT_SP_GetEmployeesByRoleIDAndStatus " + RoleID + ",'A'").ToList();
                objAssignQA.Add(new SelectListItem { Text = "Select", Value = "0" });

                foreach (var QAItem in AssignedHOD)
                {
                    //Not binding Login and responsible id
                    if (!QaEmployeeid.Contains(QAItem.EmpID))
                    {
                        objAssignQA.Add(new SelectListItem()
                        {
                            Text = QAItem.EmpName,
                            Value = QAItem.EmpID.ToString()
                        });
                    }
                }
                return Json(objAssignQA, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M29:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }

        }
        //public ActionResult GetCapaData(int DPManufacturingID)
        //{
        //    AizantIT_DevEntities objDEV_Entities = new AizantIT_DevEntities();
        //    var objDPManufacturing = objDEV_Entities.AizantIT_DP_ManufacturingSite.Where(dp => dp.DP_ManufacturingSiteID == DPManufacturingID).Select(dpf => new { dpf.DP_ManufacturingSiteID, dpf.DP_ManufacturingSiteName, dpf.Description, dpf.IsActive }).SingleOrDefault();
        //    return Json(objDPManufacturing, JsonRequestBehavior.AllowGet);
        //}
        [HttpPost]
        public JsonResult AssignedHodDeptwise(int deptid, int QualityEventTypeID)
        {
            try
            {
                int HodRoleId;
                if (QualityEventTypeID == 9)
                {
                    HodRoleId = (int)QMS_Audit_Roles.AuditHOD;
                }
                else
                {
                    HodRoleId = (int)QMS_Roles.HOD;
                }
                //For Binding Hod Emp Names based on select Department 12-02-2019
                objUMS_BAL = new UMS_BAL();
                List<SelectListItem> objAssignHod = new List<SelectListItem>();
                DataTable dtemp = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(deptid, HodRoleId, true); //25=hod
                objAssignHod.Add(new SelectListItem { Text = "Select", Value = "0" });
                foreach (DataRow RPItem in dtemp.Rows)
                {
                    objAssignHod.Add(new SelectListItem()
                    {
                        Text = (RPItem["EmpName"]).ToString(),
                        Value = (RPItem["EmpID"]).ToString()
                    });
                }
                return Json(objAssignHod, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "NTF_M6:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public JsonResult CAPA_Submit(CapaModel capaModel)
        {
            try
            {
                if (capaModel.EventsBaseID != 0)
                {
                    using (DbContextTransaction transaction = db.Database.BeginTransaction())
                    {
                        try
                        {
                            QMSCommonActions CAPAObj = new QMSCommonActions();
                            int Result = CAPAObj.InsertCAPADetails(capaModel, db);
                            if (capaModel.QualityEvent_TypeID == "9")
                            {
                                UpdateObservationResponseIsCAPARequiredforInProgressAudit(capaModel.EventsBaseID, true);
                            }
                            if (Result == 1)
                            {
                                return Json(new { hasError = true, data = "There are no Changes to Update." }, JsonRequestBehavior.AllowGet);
                            }
                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw ex;
                        }
                    }
                }
                // BindDropdown();
                ModelState.Clear();
                return Json(new { hasError = false }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M11:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        public void UpdateObservationResponseIsCAPARequiredforInProgressAudit(int ResponseID, bool IsCAPARequired)
        {
            AizantIT_ObservationResponse dtAizantIT_ObservationResponse =
                               db.AizantIT_ObservationResponse.SingleOrDefault(p => p.ResponseID == ResponseID);
            dtAizantIT_ObservationResponse.IsCapaRequired = IsCAPARequired == true ? "Y" : "N";
            db.SaveChanges();
        }
        [HttpGet]
        public JsonResult QualityEvents_CapaList()
        {
            try
            {
                int NoOfCAPACount = 0;
                int iDisplayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int iDisplayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int iSortCol_0 = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir_0 = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                int EventsBaseID = Convert.ToInt32(Request.Params["EventsBaseID"]);
                string QualityEventTypeID = Request.Params["QualityEventTypeID"];
                int AssessmentID = Convert.ToInt32(Request.Params["AssessmentID"]);

                List<CapaModel> listEmployees = new List<CapaModel>();
                int filteredCount = 0;
                //CCNBal qMS_BAL = new CCNBal();

                DataSet dt = Qms_bal.GetEventsCAPAListBAL(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, EventsBaseID, Convert.ToInt32(QualityEventTypeID), AssessmentID);
                NoOfCAPACount = dt.Tables[0].Rows.Count;
                if (dt.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow rdr in dt.Tables[0].Rows)
                    {
                        CapaModel objcapabo = new CapaModel();
                        objcapabo.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                        objcapabo.CAPA_Number = (rdr["CAPA_Number"].ToString());
                        objcapabo.ActionPlanName = (rdr["PlanofAction"].ToString());
                        objcapabo.Department = (rdr["Department"]).ToString();
                        objcapabo.EmployeeName = (rdr["Responsible_Persons"]).ToString();
                        objcapabo.TargetDate = (rdr["TargetDate"]).ToString();
                        objcapabo.CurrentStatus = (rdr["CurrentStatus"].ToString());
                        objcapabo.CAPAID = Convert.ToInt32(rdr["CAPAID"].ToString());
                        objcapabo.Assigned_QA = rdr["AssignedQA"].ToString();
                        objcapabo.QualityEvent_TypeID = (rdr["QualityTypeID"]).ToString();
                        objcapabo.QualityEvent_Number = (rdr["QualityEventNumber"]).ToString();
                        objcapabo.EventsBaseID = Convert.ToInt32(rdr["BaseID"]);
                        filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                        listEmployees.Add(objcapabo);
                    }
                }
                return Json(new
                {
                    hasError = false,
                    iTotalRecords = dt.Tables[0].Rows.Count,
                    iTotalDisplayRecords = filteredCount,
                    aaData = listEmployees,
                    NoOfCAPACount
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = "Yes";
                return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CAPAEventsDetails(int CapaID)
        {
            try
            {
                CapaModel objModelcapa = new CapaModel();

                var capaDetails = (from CM in db.AizantIT_CAPAMaster
                                   join CA in db.AizantIT_CAPA_ActionPlan on CM.CAPAID equals CA.CAPAID
                                   join D in db.AizantIT_DepartmentMaster on CA.Capa_Action_Imp_DeptID equals D.DeptID
                                   join AD in db.AizantIT_ActionPlanDescription on CA.Capa_ActionID equals AD.Capa_ActionID
                                   join E1 in db.AizantIT_EmpMaster on CA.Capa_Action_ImpID equals E1.EmpID
                                   join E2 in db.AizantIT_EmpMaster on CM.QA_EmpID equals E2.EmpID
                                   where CM.CAPAID == CapaID
                                   select new
                                   {
                                       D.DepartmentName,
                                       AD.Capa_ActionName,
                                       CM.QualityEvent_Number,
                                       Responsiblename = (E1.FirstName + "" + E1.LastName),
                                       QAReviewerName = (E2.FirstName + "" + E2.LastName),
                                       CA.Capa_ActionDesc,
                                       CA.ActionPlan_TimeLines
                                   }).SingleOrDefault();

                objModelcapa.TargetDate = Convert.ToDateTime(capaDetails.ActionPlan_TimeLines).ToString("dd MMM yyyy"); ;
                objModelcapa.Department = capaDetails.DepartmentName;
                objModelcapa.Capa_ActionDesc = capaDetails.Capa_ActionDesc;
                objModelcapa.ActionPlanName = capaDetails.Capa_ActionName;
                objModelcapa.QualityEvent_Number = capaDetails.QualityEvent_Number;
                objModelcapa.QAReviewerName = capaDetails.QAReviewerName;
                objModelcapa.ResponsiblePersonName = capaDetails.Responsiblename;
                CapaModel getlatestDate = new CapaModel();
                getlatestDate = BindLatestTargetDate(CapaID);
                if (getlatestDate.TargetDate != null)
                {
                    objModelcapa.TargetDate = getlatestDate.TargetDate;
                }
                return Json(new { hasError = false, data = objModelcapa }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + ex.Message;
                return Json(new { hasError = true, data = message.Replace("'", "\'") }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult DeleteCAPARecordsActionPlan(int id, string QualityEventID, int BaseID, string ActionFileNo = "")//ActionFileNo=CCNNumber.
        {
            try
            {
                int ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                CCNBal Qms_bal = new CCNBal();
                int i = Qms_bal.DeleteCCN_ActionPlanAndCCCRecordBal(id, QualityEventID, ActionBy, ActionFileNo);
                //For Audit 
                if (QualityEventID == "9" && (from AP in db.AizantIT_CAPA_ActionPlan
                                              join CM in db.AizantIT_CAPAMaster
                                               on AP.CAPAID equals CM.CAPAID
                                              where AP.BaseID == BaseID && CM.QualityEvent_TypeID == 9 && AP.CurrentStatus != 10
                                              select AP).Count() == 0)
                {
                    UpdateObservationResponseIsCAPARequiredforInProgressAudit(BaseID, false);
                }
                return Json(new { hasError = false, data = i }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CCN_M9:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region CAPA Report Generate


        public void GetUserCompletedDetailsReport(MainListCapaBO objCAPAbo, CAPAApprovalReport3 rpt_CAPA3)
        {
            var GetUserCompleteDetails = (from t1 in db.AizantIT_CAPA_ActionPlan
                                          join t2 in db.AizantIT_Capa_History on t1.CAPAID equals t2.CapaID
                                          where t1.CAPAID == objCAPAbo.CAPAID && t2.Status == 4
                                          orderby t1.UniqueID descending
                                          select t2).Take(1).SingleOrDefault();

            ((XRLabel)(rpt_CAPA3.FindControl("xrtblCompletedComments", false))).Text = GetUserCompleteDetails.Comments;
            ((XRLabel)(rpt_CAPA3.FindControl("xrtblCompletedSignDate", false))).Text =  Convert.ToDateTime(GetUserCompleteDetails.SignDate).ToString("dd MMM yyyy HH:mm:ss");
            ((XRLabel)(rpt_CAPA3.FindControl("xrlblCompletdDesignation", false))).Text = "(" + GetDesignationName(objCAPAbo.ResponsibleEmpID) + ")";
        }
        public MainListCapaBO GetCAPACloseDetailsReport(MainListCapaBO objCAPAbo, CAPAApprovalReport3 rpt_CAPA3)
        {
            //CAPA CLosed then call the This Linq query
            List<int> getCloseStatus = new List<int> { 21, 22, 9 };
            ViewBag.CloseCount = 0;
            if (getCloseStatus.Contains(objCAPAbo.CurrentStatus))
            {
                ViewBag.CloseCount = 1;
                var CloseCAPA = (from CA in db.AizantIT_CAPA_ActionPlan
                                 join H in db.AizantIT_Capa_History on CA.CAPAID equals H.CapaID
                                 where CA.CAPAID == objCAPAbo.CAPAID && H.Status == 23
                                 select new { CA.Closuere_Date, H.Comments, H.SignDate }).ToList();
                foreach (var item in CloseCAPA)
                {
                    objCAPAbo.ClosureEffectiveDate = Convert.ToDateTime(item.Closuere_Date).ToString("dd MMM yyyy");
                    objCAPAbo.ClosureComments = item.Comments;
                    objCAPAbo.VerificationDueDate = Convert.ToDateTime(item.SignDate).AddDays(90).ToString("dd MMM yyyy");
                    ((XRLabel)(rpt_CAPA3.FindControl("xrtblCloseSignDate", false))).Text = Convert.ToDateTime(item.SignDate).ToString("dd MMM yyyy HH:mm:ss");
                    ((XRLabel)(rpt_CAPA3.FindControl("xrlblCloseDesignation", false))).Text = "(" + GetDesignationName(objCAPAbo.QAEmpID) + ")";
                }
            }
            return objCAPAbo;
        }
        public MainListCapaBO GetCAPAVerification(MainListCapaBO objCAPAbo, CAPAApprovalReport3 rpt_CAPA3)
        {
            XRTable tblVerificationCAPATable = ((XRTable)(rpt_CAPA3.FindControl("xrtblCapaVerification", true)));
            tblVerificationCAPATable.BeginInit();
            int SNo = 1;
            var Verification = (from CA in db.AizantIT_CAPAEffectivenessOfVerification
                                where CA.CAPAID == objCAPAbo.CAPAID
                                select CA).ToList();
            foreach (var item in Verification)
            {
                XRTableRow row1 = new XRTableRow();//row create
                XRTableCell cell1 = new XRTableCell();
                cell1.CanGrow = true;
                cell1.Width = Convert.ToInt32(39.69);
                cell1.Text = SNo.ToString();
                row1.Cells.Add(cell1);

                XRTableCell cell2 = new XRTableCell();
                cell2.CanGrow = true;
                cell2.Width = Convert.ToInt32(109.85);
                cell2.Text = (item.IsFinal == true ? "Implemented" : "Not Implemented").ToString();
                row1.Cells.Add(cell2);

                XRTableCell cell3 = new XRTableCell();
                cell3.CanGrow = true;
                cell3.Width = Convert.ToInt32(119.03);

                cell3.Text = Convert.ToDateTime(item.Implementation_Date).ToString("dd MMM yyyy") == "01 Jan 1900" ? "N/A" : Convert.ToDateTime(item.Implementation_Date).ToString("dd MMM yyyy");
                row1.Cells.Add(cell3);
                XRTableCell cell4 = new XRTableCell();
                cell4.CanGrow = true;
                cell4.Width = Convert.ToInt32(222.14);
                cell4.Text = GetUserFullName((int)item.HeadQAEmpID);
                row1.Cells.Add(cell4);

                XRTableCell cell5 = new XRTableCell();
                cell5.CanGrow = true;
                cell5.Width = Convert.ToInt32(131.52);
                cell5.Text = Convert.ToDateTime(item.VerificationDate).ToString("dd MMM yyyy HH:mm:ss") == null ? "N/A" : Convert.ToDateTime(item.VerificationDate).ToString("dd MMM yyyy HH:mm:ss");
                row1.Cells.Add(cell5);
                XRTableCell cell6 = new XRTableCell();
                cell6.CanGrow = true;
                cell6.Width = Convert.ToInt32(137.77);
                cell6.Text = item.HQAApproveDate == null ? "N/A" : Convert.ToDateTime(item.HQAApproveDate).ToString("dd MMM yyyy HH:mm:ss");
                row1.Cells.Add(cell6);

                tblVerificationCAPATable.Rows.Add(row1);
                SNo++;
            }
            tblVerificationCAPATable.EndInit();


            return objCAPAbo;
        }
        public MainListCapaBO GetHQAVerificationApproval(MainListCapaBO objCAPAbo, CAPAApprovalReport3 rpt_CAPA3)
        {
            var GetHQAVerificationApproval = (from H in db.AizantIT_Capa_History
                                              where H.CapaID == objCAPAbo.CAPAID && H.Status == 24
                                              select H).SingleOrDefault();
            objCAPAbo.HQAApprovalComments = GetHQAVerificationApproval.Comments;
            ((XRLabel)(rpt_CAPA3.FindControl("xrtblHQAVerifySignDate", false))).Text = Convert.ToDateTime(GetHQAVerificationApproval.SignDate).ToString(("dd MMM yyyy HH:mm:ss"));
            ((XRLabel)(rpt_CAPA3.FindControl("xrtblHQAVerifyByName", false))).Text = GetUserFullName(objCAPAbo.HeadQAEmpID) + ")";
            ((XRLabel)(rpt_CAPA3.FindControl("xrlblHQAVerifyDesignation", false))).Text = "(" + GetDesignationName(objCAPAbo.HeadQAEmpID) + ")";
            return objCAPAbo;
        }
        public string GetUserFullName(int EmpId)
        {
            var EmployeeName = (from E in db.AizantIT_EmpMaster
                                where E.EmpID == EmpId
                                select (E.FirstName + "" + E.LastName)).SingleOrDefault();
            return EmployeeName;
        }
        public string GetSignedDate(int CAPAID, int ActionStatus)
        {
            var GetSignDate = (from t1 in db.AizantIT_Capa_History
                               where t1.CapaID == CAPAID && t1.Status == ActionStatus
                               orderby t1.UniqueID descending
                               select t1.SignDate).First();
            return Convert.ToDateTime(GetSignDate).ToString("dd MMM yyyy HH:mm:ss");
        }
        public string GetHistoryComments(int CAPAID, int ActionStatus)
        {
            var GetComments = (from t1 in db.AizantIT_Capa_History
                               where t1.CapaID == CAPAID && t1.Status == ActionStatus
                               select t1.Comments).SingleOrDefault();
            return Convert.ToString(GetComments);
        }
        public string GetQASignedDate(int BaseID, int QualityEventID, int CAPAID)
        {

            if (QualityEventID == 4)//Note to file
            {
                var GetSignDate = (from t1 in db.AizantIT_NoteToFileHistory
                                   where t1.NTF_ID == BaseID && t1.ActionStatus == 9
                                   orderby t1.NTF_HistoryID descending
                                   select t1.ActionDate).Take(1).SingleOrDefault();
                return Convert.ToDateTime(GetSignDate).ToString("dd MMM yyyy HH:mm:ss"); ;
            }
            else if (QualityEventID == 2)//CCN
            {
                List<int> ActionStatu = new List<int> { 15, 24 };
                var GetSignDate = (from t1 in db.AizantIT_CCNHistory
                                   where t1.CCNID == BaseID && ActionStatu.Contains((int)t1.ActionStatus)
                                   orderby t1.CCNHistoryID descending
                                   select t1.ActionDate).Take(1).SingleOrDefault();
                return Convert.ToDateTime(GetSignDate).ToString("dd MMM yyyy HH:mm:ss"); ;
            }
            else if (QualityEventID == 1)//Incident
            {
                var GetSignDate = (from t1 in db.AizantIT_IncidentHistory
                                   where t1.IncidentMasterID == BaseID && t1.ActionStatus == 13
                                   orderby t1.IncidentMasterID descending
                                   select t1.ActionDate).Take(1).SingleOrDefault();
                return Convert.ToDateTime(GetSignDate).ToString("dd MMM yyyy HH:mm:ss"); ;
            }
            else if (QualityEventID == 9)//Audit
            {
                return Convert.ToDateTime(GetSignedDate(CAPAID, 2)).ToString("dd MMM yyyy HH:mm:ss");
            }
            else
            {
                var GetSignDate = (from t1 in db.AizantIT_Capa_History
                                   where t1.CapaID == CAPAID && t1.Status == 16
                                   orderby t1.UniqueID descending
                                   select t1.SignDate).Take(1).SingleOrDefault();
                return Convert.ToDateTime(GetSignDate).ToString("dd MMM yyyy HH:mm:ss");
            }
        }
        public string GetDesignationName(int EmpID)
        {
            var GetDesignation = (from em in db.AizantIT_EmpMaster
                                  join de in db.AizantIT_Designation on em.CurrentDesignation equals de.DesignationID
                                  where em.EmpID == EmpID
                                  select de.DesignationName).SingleOrDefault();
            return GetDesignation;
        }

        public void GenerateCAPAReport(int CAPAID)
        {
            //try
            //{
            MainListCapaBO objCAPAbo = new MainListCapaBO();
            CAPADetailsReport rpt_CAPA1 = new CAPADetailsReport();
            RemindersReport rpt_CAPAReminders = new RemindersReport();
            CAPAApprovalReport3 rpt_CAPA3 = new CAPAApprovalReport3();
            CAPAAttachmentReport rpt_CAPA4 = new CAPAAttachmentReport();
            //Code for Company Logo
            DataTable _dtCompany = fillCompany();
            if (_dtCompany.Rows.Count != 0)
            {
                if (!string.IsNullOrEmpty(_dtCompany.Rows[0]["CompanyLogo"].ToString()))
                {
                    ((XRPictureBox)(rpt_CAPA1.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                    ((XRPictureBox)(rpt_CAPAReminders.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                    ((XRPictureBox)(rpt_CAPA3.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                    ((XRPictureBox)(rpt_CAPA4.FindControl("pbxCompanyLogo", false))).Image = BindApplicantImage((byte[])_dtCompany.Rows[0]["CompanyLogo"]);
                }
            }
            else
            {
                throw new Exception("Please Upload Company Logo");
            }
            objCAPAbo = GetCAPA_Details(CAPAID);
            #region Signature


            objCAPAbo.CAPACreatedByRpt = GetUserFullName(objCAPAbo.CAPACreatedBy);
            objCAPAbo.CAPACreatedSignDateRpt = GetSignedDate(objCAPAbo.CAPAID, 1);
            ((XRLabel)(rpt_CAPA1.FindControl("xrlableCreatedByDesignation", false))).Text = '(' + GetDesignationName(objCAPAbo.CAPACreatedBy) + ')';
            objCAPAbo.QAApprovedByRpt = objCAPAbo.Assigned_QA;

            objCAPAbo.QAApprovedSignDateRpt = GetQASignedDate(objCAPAbo.EventBaseID, Convert.ToInt32(objCAPAbo.QualityEvent_TypeID), objCAPAbo.CAPAID);
            ((XRLabel)(rpt_CAPA1.FindControl("xrlableQAApprovedInitialdesgnation", false))).Text = '(' + GetDesignationName(objCAPAbo.QAEmpID) + ')';
            if (objCAPAbo.QualityEvent_TypeID != "9")
            {
                objCAPAbo.HQAApprovedByRpt = GetUserFullName(objCAPAbo.HeadQAEmpID);
                objCAPAbo.HQAApprovedSignDateRpt = GetSignedDate(objCAPAbo.CAPAID, 2);
                ((XRLabel)(rpt_CAPA1.FindControl("xrlableHqaInitialdesgnation", false))).Text = '(' + GetDesignationName(objCAPAbo.HeadQAEmpID) + ')';
            }
            else
            {
                //for Audit hide Head QA 
                rpt_CAPA1.FindControl("xrtableHqaAppInitial", true).Visible = false;
                rpt_CAPA1.FindControl("xrlableHqaInitialdesgnation", true).Visible = false;
            }
            var GetUSerAcceptDetails = (from t1 in db.AizantIT_Capa_History
                                         join  t2 in db.AizantIT_EmpMaster on t1.EmpID equals t2.EmpID
                               where t1.CapaID == CAPAID  && t1.Status == 3
                               orderby t1.UniqueID descending
                               select new { UserAcceptName=(t2.FirstName + "" + t2.LastName), t1.SignDate,t1.EmpID }).SingleOrDefault();
            // Convert.ToDateTime(GetSignDate).ToString("dd MMM yyyy HH:mm:ss");
            objCAPAbo.UserAcceptByRpt = GetUSerAcceptDetails.UserAcceptName;
            objCAPAbo.UserSignDateRpt = Convert.ToDateTime(GetUSerAcceptDetails.SignDate).ToString("dd MMM yyyy HH:mm:ss");
            ((XRLabel)(rpt_CAPA1.FindControl("xrlbluserDesgnation", false))).Text = '(' + GetDesignationName(Convert.ToInt16(GetUSerAcceptDetails.EmpID)) + ')'; 
            #endregion

            rpt_CAPA1.DataSource = objCAPAbo;
            rpt_CAPA1.CreateDocument();

            #region Reminders

            var RM1Count = (from R in db.AizantIT_Capa_Remainder
                            where R.CAPAID == CAPAID && R.RemainderStatus == 8
                            select R).Count();
            if (RM1Count != 0)//RM1 Exists 
            {
                objCAPAbo = GetReminder1Details(objCAPAbo, rpt_CAPAReminders);

                var RM2Count = (from R in db.AizantIT_Capa_Remainder
                                where R.CAPAID == objCAPAbo.CAPAID && R.RemainderStatus == 15
                                select R).Count();
                if (RM2Count != 0)//RM2 Exists 
                {
                    objCAPAbo = GetReminder2Details(objCAPAbo, rpt_CAPAReminders);
                }
                else
                {
                    // there is Reminder 2 Hide 
                    rpt_CAPAReminders.FindControl("xrtblReminder2table", true).Visible = false;
                    // rpt_CAPAReminders.FindControl("xrtblRM2Initiation", true).Visible = false;
                    rpt_CAPAReminders.FindControl("xrTableCreatedByQARm2", true).Visible = false;
                    rpt_CAPAReminders.FindControl("xrlblDesignationQARM2", true).Visible = false;
                    rpt_CAPAReminders.FindControl("xrtblRM2Submition", true).Visible = false;
                    rpt_CAPAReminders.FindControl("xrtblSubmittedbyHodEsign", true).Visible = false;
                    rpt_CAPAReminders.FindControl("xrlbDesignationHODRM2", true).Visible = false;
                    rpt_CAPAReminders.FindControl("xrtblRM2Approved", true).Visible = false;
                    rpt_CAPAReminders.FindControl("xrtblApprovedbyQAEsign", true).Visible = false;
                    rpt_CAPAReminders.FindControl("xrlblDesignationacceptRm2", true).Visible = false;
                }
                rpt_CAPA1.DataSource = objCAPAbo;
                rpt_CAPA1.CreateDocument();

                rpt_CAPAReminders.DataSource = objCAPAbo;
                rpt_CAPAReminders.CreateDocument();
                rpt_CAPA1.Pages.AddRange(rpt_CAPAReminders.Pages);
            }
            else
            {
                rpt_CAPA1.DataSource = objCAPAbo;
                rpt_CAPA1.CreateDocument();
            }
            #endregion

            #region Report3

            GetUserCompletedDetailsReport(objCAPAbo, rpt_CAPA3);//USer Completed
            objCAPAbo = GetCAPACloseDetailsReport(objCAPAbo, rpt_CAPA3);//Close Details
            var GetVerificationcount = (from t1 in db.AizantIT_CAPAEffectivenessOfVerification
                                        where t1.CAPAID == objCAPAbo.CAPAID
                                        select t1).Count();
            if (GetVerificationcount != 0)
            {
                objCAPAbo = GetCAPAVerification(objCAPAbo, rpt_CAPA3);
                //List<int> getVerificationStatus = new List<int> { 9, 22 };
                //if (getVerificationStatus.Contains(objCAPAbo.CurrentStatus))//Verification
                //{
                ((XRLabel)(rpt_CAPA3.FindControl("xrlblVerficationDesignation", false))).Text = "(" + GetDesignationName(objCAPAbo.QAEmpID) + ")";
                //}
                //else
                //{
                //    rpt_CAPA3.FindControl("xrtblVerifyEsign", true).Visible = false;
                //    rpt_CAPA3.FindControl("xrlblVerficationDesignation", true).Visible = false;
                //}
            }
            else
            {
                rpt_CAPA3.FindControl("xrlblverificationLabelName", true).Visible = false;
                rpt_CAPA3.FindControl("xrtblCapaVerification", true).Visible = false;
                rpt_CAPA3.FindControl("xrtblVerifyEsign", true).Visible = false;
                rpt_CAPA3.FindControl("xrlblVerficationDesignation", true).Visible = false;
            }
            //if (objCAPAbo.CurrentStatus == 22)//HQA Verification Approval
            //{
            //    ((XRLabel)(rpt_CAPA3.FindControl("xrtblHQAVerifyByName", false))).Text = GetUserFullName(objCAPAbo.HeadQAEmpID) + ")";
            //((XRLabel)(rpt_CAPA3.FindControl("xrlblHQAVerifyDesignation", false))).Text = "(" + GetDesignationName(objCAPAbo.HeadQAEmpID) + ")";
            //    objCAPAbo = GetHQAVerificationApproval(objCAPAbo, rpt_CAPA3);
            //}
            //else
            //{
            //    rpt_CAPA3.FindControl("xrtblHQAverifyEsign", true).Visible = false;
            //    rpt_CAPA3.FindControl("xrlblHQAVerifyDesignation", true).Visible = false;
            //}

            rpt_CAPA3.DataSource = objCAPAbo;
            rpt_CAPA3.CreateDocument();
            rpt_CAPA1.Pages.AddRange(rpt_CAPA3.Pages);
            #endregion

            #region Report 4


            #region Completed  Attachment
            XRTable tblCompeltedFileAttachTable = ((XRTable)(rpt_CAPA4.FindControl("xrtblCompletedAttachment", true)));
            tblCompeltedFileAttachTable.BeginInit();
            int SNo = 1;
            var CompeltedAttachmentDetils = (from t1 in db.AizantIT_CapaCompltFileAttachments
                                             join t2 in db.AizantIT_EmpMaster on t1.CreatedBy equals t2.EmpID
                                             join t3 in db.AizantIT_Roles on t1.RoleID equals t3.RoleID
                                             where t1.CapaID == objCAPAbo.CAPAID
                                             select new
                                             {
                                                 t1.FileName,
                                                 FullName = (t2.FirstName + " " + t2.LastName),
                                                 t3.RoleName
                                             }).ToList();
            if (CompeltedAttachmentDetils.Count == 0)
            {
                rpt_CAPA4.FindControl("xrtblCompletAttLableName", true).Visible = false;
                rpt_CAPA4.FindControl("xrtblCompletedAttachment", true).Visible = false;
            }
            foreach (var item in CompeltedAttachmentDetils)
            {
                XRTableRow row1 = new XRTableRow();//row create
                XRTableCell cell1 = new XRTableCell();
                cell1.CanGrow = true;
                cell1.Width = 41;
                cell1.Text = SNo.ToString();
                row1.Cells.Add(cell1);

                XRTableCell cell2 = new XRTableCell();
                cell2.CanGrow = true;
                cell2.Width = 172;
                cell2.Text = item.FullName;
                row1.Cells.Add(cell2);

                XRTableCell cell3 = new XRTableCell();
                cell3.CanGrow = true;
                cell3.Width = 72;
                cell3.Text = item.RoleName;
                row1.Cells.Add(cell3);

                XRTableCell cell4 = new XRTableCell();
                cell4.CanGrow = true;
                cell4.Width = 420;
                cell4.Text = item.FileName;
                row1.Cells.Add(cell4);

                tblCompeltedFileAttachTable.Rows.Add(row1);
                SNo++;
            }
            tblCompeltedFileAttachTable.EndInit();
            #endregion

            #region Verification  Attachment
            XRTable tblVerificationFileAttachLableName = ((XRTable)(rpt_CAPA4.FindControl("xrtblVerificationLabelName", true)));
            XRTable tblVerificationFileAttachTable = ((XRTable)(rpt_CAPA4.FindControl("xrtblVerificationFiels", true)));
            tblVerificationFileAttachTable.BeginInit();
            int SNoV = 1;
            var VerificationAttachmentDetils = (from t1 in db.AizantIT_CAPAVerificationFilesAttachements
                                                join t2 in db.AizantIT_EmpMaster on t1.CreatedBy equals t2.EmpID
                                                join t3 in db.AizantIT_Roles on t1.RoleID equals t3.RoleID
                                                where t1.CapaID == objCAPAbo.CAPAID
                                                select new
                                                {
                                                    t1.FileName,
                                                    FullName = (t2.FirstName + " " + t2.LastName),
                                                    t3.RoleName
                                                }).ToList();
            if (VerificationAttachmentDetils.Count == 0)
            {
                rpt_CAPA4.FindControl("xrtblVerificationLabelName", true).Visible = false;
                rpt_CAPA4.FindControl("xrtblVerificationFiels", true).Visible = false;
            }
            foreach (var item in VerificationAttachmentDetils)
            {
                XRTableRow row1 = new XRTableRow();//row create
                XRTableCell cell1 = new XRTableCell();
                cell1.CanGrow = true;
                cell1.Width = 41;
                cell1.Text = SNoV.ToString();
                row1.Cells.Add(cell1);

                XRTableCell cell2 = new XRTableCell();
                cell2.CanGrow = true;
                cell2.Width = 172;
                cell2.Text = item.FullName;
                row1.Cells.Add(cell2);

                XRTableCell cell3 = new XRTableCell();
                cell3.CanGrow = true;
                cell3.Width = 72;
                cell3.Text = item.RoleName;
                row1.Cells.Add(cell3);

                XRTableCell cell4 = new XRTableCell();
                cell4.CanGrow = true;
                cell4.Width = 420;
                cell4.Text = item.FileName;
                row1.Cells.Add(cell4);

                tblVerificationFileAttachTable.Rows.Add(row1);
                SNoV++;
            }
            tblVerificationFileAttachTable.EndInit();
            #endregion
            if (CompeltedAttachmentDetils.Count == 0 && VerificationAttachmentDetils.Count != 0)
            {
                tblVerificationFileAttachLableName.LocationF = new PointF(0F, 10F);
                tblVerificationFileAttachTable.LocationF = new PointF(0F, 35F);
            }
            if (CompeltedAttachmentDetils.Count != 0 || VerificationAttachmentDetils.Count != 0)
            {
                rpt_CAPA4.DataSource = objCAPAbo;
                rpt_CAPA4.CreateDocument();
                rpt_CAPA1.Pages.AddRange(rpt_CAPA4.Pages);
            }

            #endregion

            string TempAttachmentDir = Server.MapPath("~\\Areas\\QMS\\Models\\CAPA\\Reports");
            if (!Directory.Exists(TempAttachmentDir))
            {
                Directory.CreateDirectory(TempAttachmentDir);
            }
            string fileName = objCAPAbo.CAPAID + DateTime.Now.ToString("yyyMMddHHmmssfff") + ".pdf";
            string strFilePath = TempAttachmentDir + "\\" + fileName;
            rpt_CAPA1.ExportToPdf(strFilePath);
            string onlyReportPath = "Areas\\QMS\\Models\\CAPA\\Reports" + "\\" + fileName;

            var GetAlreadyExistReportPath = (from T1 in db.AizantIT_CAPAFinalReport
                                             where T1.CAPAID == objCAPAbo.CAPAID
                                             select T1).SingleOrDefault();
            if (GetAlreadyExistReportPath != null)
            {
                if (GetAlreadyExistReportPath.ReportFilePath != null)
                {
                    //Delete old Merged File.
                    QMSAttachments objAttachmentsCommon = new QMSAttachments();
                    objAttachmentsCommon.RemovePhysicalPathFile(Server.MapPath("~\\" + GetAlreadyExistReportPath));
                }
                //Updated Report path 
                AizantIT_CAPAFinalReport objCAPAAttachmentAsSingle =
                           db.AizantIT_CAPAFinalReport.SingleOrDefault(p => p.CAPAID == objCAPAbo.CAPAID);
                objCAPAAttachmentAsSingle.ReportFilePath = onlyReportPath;
                objCAPAAttachmentAsSingle.CurrentStatus = objCAPAbo.CurrentStatus;
                db.SaveChanges();
            }
            else
            {
                //Inserted Report path
                AizantIT_CAPAFinalReport objInnerReportPath = new AizantIT_CAPAFinalReport();
                objInnerReportPath.ReportFilePath = onlyReportPath;
                objInnerReportPath.CAPAID = objCAPAbo.CAPAID;
                objInnerReportPath.CreatedDate = DateTime.Now;
                objInnerReportPath.CurrentStatus = objCAPAbo.CurrentStatus;
                db.AizantIT_CAPAFinalReport.Add(objInnerReportPath);
                db.SaveChanges();
            }

            //Report generated
            GenerateFinalCAPAReport(onlyReportPath, objCAPAbo.CAPAID);
        }

        #region Final report and Attachments Merged
        public void GenerateFinalCAPAReport(string onlyReportPath, int CAPAID)
        {

            List<string> ObjCAPAFilesToMerge = new List<string>();
            ObjCAPAFilesToMerge.Insert(0, onlyReportPath);
            var objCAPAListAttachment = (from t1 in db.AizantIT_CAPAFinalReport
                                         where t1.CAPAID == CAPAID
                                         select t1).ToList();
            if (objCAPAListAttachment != null)
            {
                foreach (var item in objCAPAListAttachment)
                {
                    ObjCAPAFilesToMerge.Insert(1, item.AttachmentFilePath);
                }
            }
            var objFinalReportAlredayExistsorNot = (from t1 in db.AizantIT_CAPAFinalReport
                                                    where t1.CAPAID == CAPAID
                                                    select t1).SingleOrDefault();
            if (objFinalReportAlredayExistsorNot.FinalReportPath != null)
            {
                //Delete old Merged File.
                QMSAttachments objFinalReport = new QMSAttachments();
                objFinalReport.RemovePhysicalPathFile(Server.MapPath("~\\" + objFinalReportAlredayExistsorNot.FinalReportPath));
            }

            // MergeAllTheFilesAsSingleFile,Update the DB with File path.
            AizantIT_CAPAFinalReport objAizantITReport = new AizantIT_CAPAFinalReport();
            objAizantITReport = db.AizantIT_CAPAFinalReport.SingleOrDefault(x => x.CAPAID == CAPAID);
            QMSAttachments objAttachmentsCommon = new QMSAttachments();
            objAizantITReport.FinalReportPath = objAttachmentsCommon.MergeAllTheFilesAsSingleFile(ObjCAPAFilesToMerge, "Areas\\QMS\\Models\\CAPA\\Reports\\FinalReport", CAPAID + DateTime.Now.ToString("yyyMMddHHmmssfff") + ".pdf");
            db.SaveChanges();
        }
        #endregion

        public MainListCapaBO GetReminder1Details(MainListCapaBO objCAPAbo, RemindersReport rpt_CAPAReminders)
        {
            DataSet ds = new DataSet();
            ds = Qms_bal.CapaTranaction(Convert.ToInt32(objCAPAbo.CAPAID), "Reminder1");
            if (ds.Tables[0].Rows.Count > 0)
            {
                ((XRLabel)(rpt_CAPAReminders.FindControl("xrlblIsRevisedDate", false))).Text = ((ds.Tables[0].Rows[0]["IsRevisedDate"]) == "False" ? "NO" : "YES");
                objCAPAbo.PraposedTargetDate1 = objCAPAbo.TargetDate;
                objCAPAbo.RevisedTargetDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["RevisedTargetDate"]).ToString("dd MMM yyyy");
                objCAPAbo.TargetDate = objCAPAbo.RevisedTargetDate;
                objCAPAbo.AssignHODNameRM1 = GetUserFullName(Convert.ToInt32(ds.Tables[0].Rows[0]["Hod_EmpID"].ToString()));
                //Bind Reminder Status
                objCAPAbo.RemainderStatus = Convert.ToInt32(ds.Tables[0].Rows[0]["RemainderStatus"].ToString());
                objCAPAbo.ReminderStatusName = (from S in db.AizantIT_ObjectStatus
                                                where S.StatusID == objCAPAbo.RemainderStatus && S.ObjID == 7
                                                select S.StatusName).SingleOrDefault();

                objCAPAbo.QA_Remainder_Notes = ds.Tables[0].Rows[0]["QA_Remainder_Notes"].ToString();
                objCAPAbo.HOD_Justify_Notes = ds.Tables[0].Rows[0]["HOD_Justify_Notes"].ToString();


                ((XRLabel)(rpt_CAPAReminders.FindControl("xrtblRM1InitiationComments", false))).Text = GetHistoryComments(objCAPAbo.CAPAID, 5);
                ((XRLabel)(rpt_CAPAReminders.FindControl("xrtblRM1QACreatedDate", false))).Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["ReminderCreatedDate"]).ToString("dd MMM yyyy HH: mm:ss");
                ((XRLabel)(rpt_CAPAReminders.FindControl("xrlblDesignationQA", false))).Text = '(' + GetDesignationName(objCAPAbo.QAEmpID) + ')';

                ((XRLabel)(rpt_CAPAReminders.FindControl("xrtblRM1HODCreatedDate", false))).Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["Hod_SubmitDate"]).ToString("dd MMM yyyy HH: mm:ss");
                ((XRLabel)(rpt_CAPAReminders.FindControl("xrlbDesignationHOD", false))).Text = '(' + GetDesignationName(Convert.ToInt32(ds.Tables[0].Rows[0]["Hod_EmpID"].ToString())) + ')';

                ((XRLabel)(rpt_CAPAReminders.FindControl("xrtblRM1AcceptDate", false))).Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["QA_Accepted_SubmitedDate"]).ToString("dd MMM yyyy HH: mm:ss");
                ((XRLabel)(rpt_CAPAReminders.FindControl("xrlblDesignationaccept", false))).Text = '(' + GetDesignationName(objCAPAbo.QAEmpID) + ')';
            }
            return objCAPAbo;
        }
        public MainListCapaBO GetReminder2Details(MainListCapaBO objCAPAbo, RemindersReport rpt_CAPAReminders)
        {

            DataSet ds = new DataSet();
            ds = Qms_bal.CapaTranaction(Convert.ToInt32(objCAPAbo.CAPAID), "Reminder2");
            if (ds.Tables[0].Rows.Count > 0)
            {
                ((XRLabel)(rpt_CAPAReminders.FindControl("xrlblIsRevisedDateRM2", false))).Text = ((ds.Tables[0].Rows[0]["IsRevisedDate"]) == "False" ? "NO" : "YES");
                objCAPAbo.PraposedTargetDate2 = objCAPAbo.RevisedTargetDate;
                objCAPAbo.RevisedTargetDateRM2 = Convert.ToDateTime(ds.Tables[0].Rows[0]["RevisedTargetDate"]).ToString("dd MMM yyyy");
                objCAPAbo.TargetDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["RevisedTargetDate"]).ToString("dd MMM yyyy");
                objCAPAbo.AssignHODNameRM2 = GetUserFullName(Convert.ToInt32(ds.Tables[0].Rows[0]["Hod_EmpID"].ToString()));
                //Bind Reminder Status
                objCAPAbo.RemainderStatusRM2 = Convert.ToInt32(ds.Tables[0].Rows[0]["RemainderStatus"].ToString());
                objCAPAbo.ReminderStatusNameRM2 = (from S in db.AizantIT_ObjectStatus
                                                   where S.StatusID == objCAPAbo.RemainderStatusRM2 && S.ObjID == 7
                                                   select S.StatusName).SingleOrDefault();

                objCAPAbo.QA_Remainder_NotesRM2 = ds.Tables[0].Rows[0]["QA_Remainder_Notes"].ToString();
                objCAPAbo.HOD_Justify_NotesRM2 = ds.Tables[0].Rows[0]["HOD_Justify_Notes"].ToString();

                ((XRLabel)(rpt_CAPAReminders.FindControl("xrtblRM2InitiationComments", false))).Text = GetHistoryComments(objCAPAbo.CAPAID, 12);

                ((XRLabel)(rpt_CAPAReminders.FindControl("xrtblRM2QACreatedDate", false))).Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["ReminderCreatedDate"]).ToString("dd MMM yyyy HH: mm:ss");
                ((XRLabel)(rpt_CAPAReminders.FindControl("xrlblDesignationQARM2", false))).Text = '(' + GetDesignationName(objCAPAbo.QAEmpID) + ')';

                ((XRLabel)(rpt_CAPAReminders.FindControl("xrtblRM2HODCreatedDate", false))).Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["Hod_SubmitDate"]).ToString("dd MMM yyyy HH: mm:ss");
                ((XRLabel)(rpt_CAPAReminders.FindControl("xrlbDesignationHODRM2", false))).Text = '(' + GetDesignationName(Convert.ToInt32(ds.Tables[0].Rows[0]["Hod_EmpID"].ToString())) + ')';

                ((XRLabel)(rpt_CAPAReminders.FindControl("xrtblRM2AcceptDate", false))).Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["QA_Accepted_SubmitedDate"]).ToString("dd MMM yyyy HH: mm:ss");
                ((XRLabel)(rpt_CAPAReminders.FindControl("xrlblDesignationacceptRm2", false))).Text = '(' + GetDesignationName(objCAPAbo.QAEmpID) + ')';
            }
            return objCAPAbo;
        }
        #endregion

        public void MakeCAPA_AttachmentsToSingleFile(int CAPAID)
        {
            List<string> objCAPAAttachmentFilePaths = new List<string> { };
            var CompletedAttachmentFiles = (from t1 in db.AizantIT_CAPA_ActionPlan
                                            join t2 in db.AizantIT_CapaCompltFileAttachments on t1.CAPAID equals t2.CapaID
                                            where t1.CAPAID == CAPAID
                                            select t2).ToList();
            foreach (var item in CompletedAttachmentFiles)
            {
                objCAPAAttachmentFilePaths.Add(item.FilePath);
            }
            var VerificationAttachmentFiles = (from t1 in db.AizantIT_CAPA_ActionPlan
                                               join t3 in db.AizantIT_CAPAVerificationFilesAttachements on t1.CAPAID equals t3.CapaID
                                               where t1.CAPAID == CAPAID
                                               select t3).ToList();
            foreach (var item in VerificationAttachmentFiles)
            {
                objCAPAAttachmentFilePaths.Add(item.FilePath);
            }

            var objAlreadyMergedCAPAAttachments = (from item in db.AizantIT_CAPAFinalReport
                                                   where item.CAPAID == CAPAID
                                                   select item).SingleOrDefault();
            if (objAlreadyMergedCAPAAttachments != null) //There's already a Merged Attachments File
            {
                if (objAlreadyMergedCAPAAttachments.AttachmentFilePath != null)
                {
                    //Delete old Merged File.
                    QMSAttachments objAttachmentsCommon = new QMSAttachments();
                    objAttachmentsCommon.RemovePhysicalPathFile(Server.MapPath("~\\" + objAlreadyMergedCAPAAttachments.AttachmentFilePath));
                }

                //Update Newly Merged File
                if (objCAPAAttachmentFilePaths.Count > 0)
                {
                    AizantIT_CAPAFinalReport objCAPAAttachmentAsSingle =
                           db.AizantIT_CAPAFinalReport.SingleOrDefault(p => p.CAPAID == CAPAID);
                    QMSAttachments objAttachmentsCommon = new QMSAttachments();
                    objCAPAAttachmentAsSingle.AttachmentFilePath = objAttachmentsCommon.MergeAllTheFilesAsSingleFile(objCAPAAttachmentFilePaths, "Areas\\QMS\\Models\\CAPA\\CAPAAttachments\\MergedAttachments", CAPAID + DateTime.Now.ToString("yyyMMddHHmmssfff") + ".pdf");
                }
            }
            else //Create new Merge File
            {
                if (objCAPAAttachmentFilePaths.Count > 0)
                {
                    AizantIT_CAPAFinalReport objCAPAttachmentAsSingle = new AizantIT_CAPAFinalReport();
                    objCAPAttachmentAsSingle.CAPAID = CAPAID;
                    objCAPAttachmentAsSingle.CreatedDate = DateTime.Now;
                    QMSAttachments objAttachmentsCommon = new QMSAttachments();
                    objCAPAttachmentAsSingle.AttachmentFilePath = objAttachmentsCommon.MergeAllTheFilesAsSingleFile(objCAPAAttachmentFilePaths, "Areas\\QMS\\Models\\CAPA\\CAPAAttachments\\MergedAttachments", CAPAID + DateTime.Now.ToString("yyyMMddHHmmssfff") + ".pdf");
                    db.AizantIT_CAPAFinalReport.Add(objCAPAttachmentAsSingle);
                }
            }
            db.SaveChanges();
        }

        #region JS Pdf
        //public JsonResult CAPAGetFinalReportPDFPath(int CAPAID, int CurrentStatus)
        //{
        //    string Msg = "", MsgType = "";
        //    try
        //    {
        //        var GetReportGenerate = (from t1 in db.AizantIT_CAPAFinalReport
        //                                 where t1.CAPAID == CAPAID
        //                                 select t1).SingleOrDefault();
        //        if (GetReportGenerate != null)
        //        {
        //                if (GetReportGenerate.CurrentStatus != CurrentStatus)
        //                {
        //                    MakeCAPA_AttachmentsToSingleFile(CAPAID);

        //                GenerateCAPAReport(CAPAID);
        //            }
        //        }
        //        else
        //        {
        //            MakeCAPA_AttachmentsToSingleFile(CAPAID);

        //            GenerateCAPAReport(CAPAID);
        //        }


        //        var GetFilePath = (from T1 in db.AizantIT_CAPAFinalReport
        //                           where T1.CAPAID == CAPAID
        //                           select T1.FinalReportPath).SingleOrDefault();
        //        if (GetFilePath != null)
        //        {
        //            string filePath = Server.MapPath("~\\" + GetFilePath);
        //            string[] filenames = GetFilePath.Split('\\');

        //            if (System.IO.File.Exists(filePath))
        //            {
        //                string Path = "<iframe src=\"" + System.Web.VirtualPathUtility.ToAbsolute("~/Areas\\QMS\\Models\\CAPA\\Reports\\FinalReport\\web\\viewer.html?file=" + filenames[filenames.Length - 1]) + "#toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" frameborder=\"0\" height=\"700px\" ></iframe>";
        //                Msg = Path;
        //                MsgType = "success";
        //            }
        //            else
        //            {
        //                Msg = "File does not exist.";
        //                MsgType = "error";
        //            }
        //        }
        //        else
        //        {
        //            Msg = "File does not exist.";
        //            MsgType = "error";
        //        }

        //        return Json(new { msgType = MsgType, data = Msg }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        string strline = HelpClass.LineNo(ex);
        //        string strMsg = HelpClass.SQLEscapeString(ex.Message);
        //        Msg = "PDF_Viewer:" + strline + "  " + strMsg;
        //        MsgType = "error";
        //        return Json(new { msgType = MsgType, data = Msg }, JsonRequestBehavior.AllowGet);
        //    }
        //}
        public JsonResult CAPAGetFinalReportPDFPath(int CAPAID, int CurrentStatus)
        {
            string Msg = "", MsgType = "";
            try
            {
                var GetReportGenerate = (from t1 in db.AizantIT_CAPAFinalReport
                                         where t1.CAPAID == CAPAID
                                         select t1).SingleOrDefault();
                if (GetReportGenerate != null)
                {
                    if (GetReportGenerate.CurrentStatus != CurrentStatus)
                    {
                        MakeCAPA_AttachmentsToSingleFile(CAPAID);

                        GenerateCAPAReport(CAPAID);
                    }
                }
                else
                {
                    MakeCAPA_AttachmentsToSingleFile(CAPAID);

                    GenerateCAPAReport(CAPAID);
                }


                var GetFilePath = (from T1 in db.AizantIT_CAPAFinalReport
                                   where T1.CAPAID == CAPAID
                                   select T1.FinalReportPath).SingleOrDefault();
                string filePath = "";
                if (GetFilePath != null)
                {
                    filePath = (GetFilePath);
                    string FullfilePath = Server.MapPath("~\\" + GetFilePath);
                    if (System.IO.File.Exists(FullfilePath))
                    {
                        Msg = filePath;
                        MsgType = "success";
                    }
                    else
                    {
                        Msg = "File does not exist.";
                        MsgType = "error";
                    }
                }
                else
                {
                    Msg = "File does not exist.";
                    MsgType = "error";
                }

                return Json(new { msgType = MsgType, data = Msg }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                Msg = "PDF_Viewer:" + strline + "  " + strMsg;
                MsgType = "error";
                return Json(new { msgType = MsgType, data = Msg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Download Report
        public async Task<ActionResult> DownloadCAPAReport(int CAPAID)
        {
            try
            {
                var GetCAPANumber = (from t1 in db.AizantIT_CAPAMaster
                                     where t1.CAPAID == CAPAID
                                     select t1.CAPA_Number).SingleOrDefault();
                string Downloadfilename = "CAPA_Report" + GetCAPANumber + ".pdf";
                string filePath = (from t1 in db.AizantIT_CAPAFinalReport
                                   where t1.CAPAID == CAPAID
                                   select t1.FinalReportPath).SingleOrDefault();
                byte[] fileBytes;

                fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~\\" + filePath));
                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, Downloadfilename);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                TempData["Exception"] = HelpClass.SQLEscapeString(ex.Message);
                return RedirectToAction("CapaList");
            }
        }

        #endregion

        public JsonResult GetCAPAVerificationEffectiveness()
        {
            try
            {
                int iDisplayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int iDisplayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int iSortCol_0 = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir_0 = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                int CAPAID = Convert.ToInt32(Request.Params["CapaID"]);
                int totalRecordsCount = 0;

                List<VerificationBO> VerificationboList = new List<VerificationBO>();
                var GetCAPAVerificationDetails = db.AizantIT_SP_GetCAPAEffectivenessVerification(
                    iDisplayLength,
                    iDisplayStart,
                    iSortCol_0,
                    sSortDir_0,
                    sSearch,
                    CAPAID
                    );
                foreach (var item in GetCAPAVerificationDetails)
                {
                    if (totalRecordsCount == 0)
                    {
                        totalRecordsCount = Convert.ToInt32(item.TotalCount);
                    }
                    VerificationboList.Add(new VerificationBO(Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.UniquePKID), (item.Implementor_Date), item.Description, Convert.ToInt32(item.CAPAID),
                        Convert.ToString(item.CurrentStatus), item.IsImplemented, item.HeadQAEmpName, Convert.ToInt32(item.CurrentStatusNumber)));
                }
                return Json(new
                {
                    hasError = false,
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = VerificationboList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { hasError = true, data = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message) + "." }, JsonRequestBehavior.AllowGet);
            }
        }
        public PartialViewResult _CAPAVerificationAddAttachments(int CAPAID, int EVID = 0)
        {
            TempData.Keep("R");
            MainListCapaBO objCapa = new MainListCapaBO();
            var GetQualityEventID = (from t1 in db.AizantIT_CAPAMaster
                                     where t1.CAPAID == CAPAID
                                     select t1).SingleOrDefault();
            if (GetQualityEventID != null) { 
                objCapa.CAPA_Date = Convert.ToDateTime(GetQualityEventID.CAPA_Date).ToString("dd MMM yyyy");
                objCapa.QualityEvent_TypeID = GetQualityEventID.QualityEvent_TypeID.ToString();
                objCapa.QAEmpID = (int)GetQualityEventID.QA_EmpID;
                objCapa.CAPA_Number = GetQualityEventID.CAPA_Number;
                objCapa.HeadQAEmpID = (int)GetQualityEventID.HeadQA_EmpID;
            }
            objCapa.CAPAID = CAPAID;
            objCapa.EMPId = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            #region VerificationID Exists

            if (EVID != 0)//Edit
            {
                var GetVerifiation = (from t1 in db.AizantIT_CAPAEffectivenessOfVerification
                                      where t1.EFV_ID == EVID
                                      select t1).SingleOrDefault();

                if(GetVerifiation!=null)
                {
                    objCapa.HeadQAEmpID = (int)GetVerifiation.HeadQAEmpID;
                    objCapa.ImplementorDateVerificationCAPA = Convert.ToDateTime(GetVerifiation.Implementation_Date).ToString("dd MMM yyyy");
                    objCapa.ISCheckedFinalorNot = Convert.ToBoolean(GetVerifiation.IsFinal);
                    objCapa.VerificationID = GetVerifiation.EFV_ID;
                }
                string EFVerificationID = EVID.ToString();
                ViewBag.AttachmentsExists = (from t2 in db.AizantIT_CAPAVerificationFilesAttachements
                                             where t2.EFVerificationID == EFVerificationID
                                             select t2).Count();
                ViewBag.Everification1stTime = "No";
            }

            #endregion
            #region VerificationID Not Exists
            
            else if (EVID == 0)
            {
                objCapa.VerificationID = 0;
                ViewBag.AttachmentsExists = 0;
                ViewBag.Everification1stTime = "YES";
                objCapa.GUID = Guid.NewGuid().ToString();
            }
            #endregion
            #region HQADropDown


            //Not binding LoginID  QA EmployeeId
            List<int> NotBndgHeadQAList = new List<int>() { objCapa.QAEmpID };
            List<SelectListItem> objAssignHeadQAList = new List<SelectListItem>();
            int HeadQARoleID;
            if (objCapa.QualityEvent_TypeID == "9")
            {
                HeadQARoleID = 37;/*Audit HeadQA*/
            }
            else
            {
                HeadQARoleID = 27;/*QMS HEadQA*/
            }
            //Bind HeadQA Dropdown
            var GetAssignedHeadQA = db.Database.SqlQuery<AccessibleEmpOnDeptandRole>("exec [UMS].[AizantIT_SP_GetEmployeesByRoleIDAndStatus] " +
                         HeadQARoleID + ",'A'").ToList();
            objAssignHeadQAList.Add(new SelectListItem
            {
                Text = "-- Select HeadQA --",
                Value = "0",
            });
            foreach (var HQAItem in GetAssignedHeadQA)
            {
                if (!NotBndgHeadQAList.Contains(HQAItem.EmpID))
                {
                    objAssignHeadQAList.Add(new SelectListItem()
                    {
                        Text = HQAItem.EmpName,
                        Value = HQAItem.EmpID.ToString(),
                        Selected = (HQAItem.EmpID == objCapa.HeadQAEmpID ? true : false)
                    });
                }
            }
            objCapa.ddlAssigneToHQA = objAssignHeadQAList;
            #endregion
            return PartialView(objCapa);
        }

        public JsonResult VerificationApproveRevert(MainListCapaBO objcapaModel)
        {
            try
            {
                //Get employee ID
                CapaModel objbo = new CapaModel();
                GetUserEmpID(objbo);
                objcapaModel.EMPId = objbo.EmpID;
                Qms_bal.CAPAVerificationApproveRevertBAL(objcapaModel, out int Result);
                if (Result == 0)
                {
                    return Json(new { msg = "", msgType = "success", }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { msg = "You are not authorized HQA to do action.", msgType = "error", }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "CAPA_M11:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { msg = ErMsg, msgType = "error", }, JsonRequestBehavior.AllowGet);
            }
        }
        public Boolean IsPendingVerification(int CAPAID)
        {
            List<int?> Status = new List<int?> { 1, 2 };
            var result = (from t1 in db.AizantIT_CAPAEffectivenessOfVerification
                          where t1.CAPAID == CAPAID && Status.Contains(t1.CurrentStatus)
                          select t1).Any();

            return result;
        }
    }

}

