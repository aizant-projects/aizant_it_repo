﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Aizant_Enums;
using AizantIT_PharmaApp.Common.CustomFilters;
using static Aizant_Enums.AizantEnums;
using System.Diagnostics;
using Aizant_API_Entities;
using QMS_BO.QMS_Masters_BO;
using QMS_BusinessLayer;
using AizantIT_PharmaApp.Common;

namespace AizantIT_PharmaApp.Areas.QMS.Controllers.CAPA
{
    //[SessionExpire]
    [RouteArea]
    [CustAuthentication]
    [CustAuthorization(
       (int)AizantEnums.Modules.QMS,
        (int)QMS_Roles.Initiator,
        (int)QMS_Roles.AdminRole)]

    public class ActionPlanController : Controller
    {
        
        private AizantIT_DevEntities dbEF = new AizantIT_DevEntities();
        private readonly log4net.ILog Aizant_log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // GET: QMS_ActionPlan
        public ActionResult Index()
        {
            try
            {
            return View(dbEF.AizantIT_ActionPlanDescription.ToList());
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "ACTP_M5:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return View();
            }
        }

        public JsonResult GetActionPlanList(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch)
        {
            List<ActionPlanList> listEmployees = new List<ActionPlanList>();
            int filteredCount = 0;
            string Operation = "Employee";
            QMS_BAL objbal = new QMS_BAL();
            DataSet dt = objbal.GetActionPlanLISTBal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch);
            if (dt.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow rdr in dt.Tables[0].Rows)
                {
                    ActionPlanList actionPlan = new ActionPlanList();
                    actionPlan.RowNumber = Convert.ToInt32(rdr["RowNumber"]);
                    actionPlan.Capa_ActionID = Convert.ToInt32(rdr["Capa_ActionID"].ToString());
                    actionPlan.Capa_ActionName = (rdr["Capa_ActionName"]).ToString();
                    actionPlan.CurrentStatus = (rdr["currentstatus"].ToString());

                    filteredCount = Convert.ToInt32(rdr["TotalCount"]);
                    listEmployees.Add(actionPlan);
                }
            }

            return Json(new
            {
                hassError = false,
                iTotalRecords = dt.Tables[1].Rows.Count,
                iTotalDisplayRecords = filteredCount,
                aaData = listEmployees
            },JsonRequestBehavior.AllowGet);

        }
        // GET: QMS_ActionPlan/Create
        public PartialViewResult Create()
        {
            try
            {
            return PartialView();
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "ACTP_M1:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }
        [HttpPost]       
        public JsonResult Create( AizantIT_ActionPlanDescription aizantIT_ActionPlanDescription)
        {
            try
            {
                string TypeOfAction= "";
                string CurrentStatus = "";
                AizantIT_ActionPlanDescription objAPD = new AizantIT_ActionPlanDescription();
            objAPD.Capa_ActionID = aizantIT_ActionPlanDescription.Capa_ActionID;
            objAPD.Capa_ActionName = aizantIT_ActionPlanDescription.Capa_ActionName.Trim();
            objAPD.CurrentStatus = aizantIT_ActionPlanDescription.CurrentStatus;
                AizantIT_ActionPlanDescriptionHistory objAPDH = new AizantIT_ActionPlanDescriptionHistory();
                objAPDH.ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                objAPDH.RoleID = 31;//Admin Role
                objAPDH.ActionDate = DateTime.Now;
                objAPDH.ActionStatus = 1;
                objAPDH.Comments = null;
                TypeOfAction = objAPD.Capa_ActionName;
                if (objAPD.CurrentStatus == true)
                {
                    CurrentStatus = "Active";
                }
                else
                {
                    CurrentStatus = "In-Active";
                }
                objAPDH.SubmittedData = "Type Of Action : " + TypeOfAction + ", " + "Current Status : " + CurrentStatus;
                string desc = aizantIT_ActionPlanDescription.Capa_ActionName.Trim();
                var Count = dbEF.AizantIT_ActionPlanDescription.FirstOrDefault(p => p.Capa_ActionName == objAPD.Capa_ActionName);
                if (Count == null)
                {
                    if (ModelState.IsValid)
                    {
                        dbEF.AizantIT_ActionPlanDescription.Add(objAPD);
                        dbEF.SaveChanges();
                        objAPDH.Capa_ActionID = objAPD.Capa_ActionID;
                        dbEF.AizantIT_ActionPlanDescriptionHistory.Add(objAPDH);
                        dbEF.SaveChanges();
                        return Json(objAPD, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
                return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "ACTP_M2:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }

        }

        // GET: QMS_ActionPlan/Edit/5
        public PartialViewResult Edit(int? id)
        {
            try
            {
            if (id == null)
            {
                return PartialView("_FriendlyError");
            }
            AizantIT_ActionPlanDescription aizantIT_ActionPlanDescription = dbEF.AizantIT_ActionPlanDescription.Find(id);
            if (aizantIT_ActionPlanDescription == null)
            {
                return PartialView("_FriendlyError");
            }
            return PartialView(aizantIT_ActionPlanDescription);

            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "ACTP_M3:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                ViewBag.Exception = ErMsg;
                return PartialView("ExecptionError", ViewBag.Exception);
            }
        }

        [HttpPost]       
        public JsonResult Edit( AizantIT_ActionPlanDescription aizantIT_ActionPlanDescription,string Comments)
        {
            try
            {
                string TypeOfAction = "";
                string CurrentStatus = "";
                AizantIT_ActionPlanDescription objAPD = new AizantIT_ActionPlanDescription();
            objAPD.Capa_ActionID = aizantIT_ActionPlanDescription.Capa_ActionID;
            objAPD.Capa_ActionName = aizantIT_ActionPlanDescription.Capa_ActionName.Trim();
            objAPD.CurrentStatus = aizantIT_ActionPlanDescription.CurrentStatus;
                AizantIT_ActionPlanDescriptionHistory objAPDH = new AizantIT_ActionPlanDescriptionHistory();
                objAPDH.Capa_ActionID = aizantIT_ActionPlanDescription.Capa_ActionID;
                objAPDH.ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString());
                objAPDH.RoleID = 31;//Admin Role
                objAPDH.ActionDate = DateTime.Now;
                objAPDH.ActionStatus = 2;
                objAPDH.Comments = Comments;
                TypeOfAction = objAPD.Capa_ActionName;
                if (objAPD.CurrentStatus == true)
                {
                    CurrentStatus = "Active";
                }
                else
                {
                    CurrentStatus = "In-Active";
                }
                objAPDH.SubmittedData = "Type Of Action : " + TypeOfAction + ", " + "Current Status : " + CurrentStatus;
                string desc = aizantIT_ActionPlanDescription.Capa_ActionName.Trim();
            int id = Convert.ToInt32(aizantIT_ActionPlanDescription.Capa_ActionID);
            var exists = dbEF.AizantIT_ActionPlanDescription.FirstOrDefault(p => p.Capa_ActionName == objAPD.Capa_ActionName && p.CurrentStatus==objAPD.CurrentStatus);
            if (exists == null)
            {
                if (ModelState.IsValid)
                {
                    dbEF.Entry(objAPD).State = EntityState.Modified;
                        dbEF.AizantIT_ActionPlanDescriptionHistory.Add(objAPDH);
                        dbEF.SaveChanges();
                        
                        return Json(objAPD, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
            return Json(0, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string ErMsg = "ACTP_M4:" + LineNo + "  " + HelpClass.SQLEscapeString(ex.Message) + ".";
                Aizant_log.Error(ErMsg);
                return Json(new { hasError = true, data = ErMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbEF.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
