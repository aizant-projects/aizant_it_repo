﻿using System.Web.Mvc;

namespace AizantIT_PharmaApp.Areas.Regulatory
{
    public class RegulatoryAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Regulatory";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Regulatory_default",
                "Regulatory/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}