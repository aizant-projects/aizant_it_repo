﻿using Aizant_API_Entities;
using AizantIT_PharmaApp.Areas.Regulatory.Controllers.RegulatoryLinq;
using AizantIT_PharmaApp.Areas.Regulatory.Models;
using AizantIT_PharmaApp.Areas.Regulatory.Models.RegulatoryViewModel;
using AizantIT_PharmaApp.Areas.Regulatory.Models.Reports;
using AizantIT_PharmaApp.Areas.Regulatory.Models.Reports.DataSource;
using AizantIT_PharmaApp.Common;
using AizantIT_PharmaApp.Common.CustomFilters;
using AizantIT_PharmaApp.Controllers.Aizant_BL;
using AizantIT_PharmaApp.Models.Common;
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.Areas.Regulatory.Controllers
{
    [Route("Regulatory/RegulatoryMainController/{action}")]
    [RouteArea]
    [CustAuthentication]
    public class RegulatoryMainController : Controller
    {
        // GET: Regulatory/RegulatoryDashBoard
        [CustAuthorization((int)Modules.RA,
            (int)RA_UserRole.RA_Admin,
            (int)RA_UserRole.RA_Lead,
            (int)RA_UserRole.RA_Approver,
            (int)RA_UserRole.RA_HOD)]
        public ActionResult RegulatoryDashBoard()
        {
            bool PendingCard = false;
            bool RevertedCard = false;
            RegulatoryDashBoard objregulatoryDashBoard = new RegulatoryDashBoard();
            var EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            string[] ModuleRoles = (string[])Session["EmpModuleRoles"];
            var dbContext = new AizantIT_DevEntities();
            List<PendingListItems> Pendinglist = new List<PendingListItems>();
            List<RevertListItems> Revertlist = new List<RevertListItems>();

            if (ModuleRoles.Contains(((int)Aizant_Enums.AizantEnums.RA_UserRole.RA_Approver).ToString()))
            {
                PendingCard = true;
                int DossierPendingCount = dbContext.AizantIT_ProductDossier.Where(a => a.CurrentStatus == 2 && a.ApproverEmpID == EmpID).Count();
                int DeficiencyPendingCount = dbContext.AizantIT_DeficiencyMaster.Where(a => a.CurrentStatus == 2 && a.ApproverID == EmpID).Count();

                if (DossierPendingCount > 0)//Dossier Pending
                {
                    Pendinglist.Add(new PendingListItems(DossierPendingCount, "/Regulatory/RegulatoryMain/DossierMainList?ShowType=4", "Product Details for Approval"));
                    objregulatoryDashBoard.PendingTotalCount += DossierPendingCount;
                }
                if (DeficiencyPendingCount > 0)//Deficiency Pending
                {

                    Pendinglist.Add(new PendingListItems(DeficiencyPendingCount, "/Regulatory/RegulatoryMain/DeficiencyMainList?ShowType=14", "Deficiency"));
                    objregulatoryDashBoard.PendingTotalCount += DeficiencyPendingCount;
                }
            }
            if (ModuleRoles.Contains(((int)Aizant_Enums.AizantEnums.RA_UserRole.RA_Lead).ToString()))
            {
                RevertedCard = true;
                PendingCard = true;
                int DossierRevertCount = dbContext.AizantIT_ProductDossier.Where(a => a.CurrentStatus == 4 && a.AuthorEmpID == EmpID).Count();//Current Status==4(Revert)
                int DossierPendingCount = dbContext.AizantIT_ProductDossier.Where(a => new int[] { 5, 6, 7 }.Contains(a.CurrentStatus) && a.AuthorEmpID == EmpID).Count();
                int DeficiencyRevertCount = dbContext.AizantIT_DeficiencyMaster.Where(a => a.CurrentStatus == 4 && a.AuthorID == EmpID).Count();//Current Status ==4(Revert)

                if (DossierRevertCount > 0)//Product Revert
                {
                    Revertlist.Add(new RevertListItems(DossierRevertCount, "/Regulatory/RegulatoryMain/DossierMainList?ShowType=2", "Product Details"));
                    objregulatoryDashBoard.RevertTotalCount += DossierRevertCount;
                }
                if (DossierPendingCount > 0)//Product Pending
                {
                    Pendinglist.Add(new PendingListItems(DossierPendingCount, "/Regulatory/RegulatoryMain/DossierMainList?ShowType=9", "Product Completion Pending"));
                    objregulatoryDashBoard.PendingTotalCount += DossierPendingCount;
                }
                if (DeficiencyRevertCount > 0)//Deficiency Revert
                {
                    Revertlist.Add(new RevertListItems(DeficiencyRevertCount, "/Regulatory/RegulatoryMain/DeficiencyMainList?ShowType=13", "Deficiency"));
                    objregulatoryDashBoard.RevertTotalCount += DeficiencyRevertCount;
                }
                objregulatoryDashBoard.RaLead = Convert.ToInt32(RA_UserRole.RA_Lead);
            }
            if (ModuleRoles.Contains(((int)Aizant_Enums.AizantEnums.RA_UserRole.RA_HOD).ToString()))
            {
                PendingCard = true;
                int IsObservationPendingCount = (from DF in dbContext.AizantIT_DF_Observation
                                                 join DEF in dbContext.AizantIT_DeficiencyMaster on DF.DeficiencyID equals DEF.DeficiencyID
                                                 where DF.CurrentStatus == 1 && DF.ResponsiblePersonEmpID == EmpID && DF.Status == 1 && DEF.CurrentStatus == 5
                                                 select DF).Count();

                int IsObservationCompleteCount = (from DF in dbContext.AizantIT_DF_Observation
                                                  join DEF in dbContext.AizantIT_DeficiencyMaster on DF.DeficiencyID equals DEF.DeficiencyID
                                                  where DF.CurrentStatus == 2 && DF.ResponsiblePersonEmpID == EmpID && DF.Status == 1 && DEF.CurrentStatus == 5
                                                  select DF).Count();

                if (IsObservationPendingCount > 0)//Observations to Accept
                {
                    Pendinglist.Add(new PendingListItems(IsObservationPendingCount, "/Regulatory/RegulatoryMain/ObservationMainList?ShowType=18", "Observation to Accept"));
                    objregulatoryDashBoard.PendingTotalCount += IsObservationPendingCount;
                }
                if (IsObservationCompleteCount > 0)//Observations to Complete
                {
                    Pendinglist.Add(new PendingListItems(IsObservationCompleteCount, "/Regulatory/RegulatoryMain/ObservationMainList?ShowType=19", "Observation to Close"));
                    objregulatoryDashBoard.PendingTotalCount += IsObservationCompleteCount;
                }
                objregulatoryDashBoard.RaHod = Convert.ToInt32(RA_UserRole.RA_HOD);
            }
            objregulatoryDashBoard.pendingListItems = Pendinglist;
            objregulatoryDashBoard.revertListItems = Revertlist;

            objregulatoryDashBoard.PendingCard = PendingCard;
            objregulatoryDashBoard.RevertedCard = RevertedCard;
            return View(objregulatoryDashBoard);
        }

        #region Dossier
        private DateTime? getDateValue(string strDate)
        {
            if (strDate == "" || strDate == null)
            {
                return null;
            }
            else
            {
                return Convert.ToDateTime(strDate);
            }
        }
        public PartialViewResult _ViewDossierDetails(int DossierID)
        {
            var EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            //Notification Status Updating
            int Notification_ID = GetDossierNotificationID(DossierID);
            if (Notification_ID != 0)
            {
                UpdateRA_NotificationStatus(Notification_ID, 2);
            }
            ViewDossierModel objViewDossierModel = new ViewDossierModel();
            Aizant_Bl objAizant_Bl = new Aizant_Bl();
            var dbRegulatory = new AizantIT_DevEntities();
            List<SelectListItem> objDossierList = new List<SelectListItem>();
            var DossierDetails = (from PD in dbRegulatory.AizantIT_ProductDossier
                                  join EM in dbRegulatory.AizantIT_EmpMaster on PD.AuthorEmpID equals EM.EmpID
                                  join EM1 in dbRegulatory.AizantIT_EmpMaster on PD.ApproverEmpID equals EM1.EmpID
                                  join PM in dbRegulatory.AizantIT_ProductMaster on PD.ProductID equals PM.ProductID
                                  join C in dbRegulatory.AizantIT_Client on PD.ClientID equals C.ClientID
                                  join C1 in dbRegulatory.AizantIT_Client on PD.ApplicantID equals C1.ClientID
                                  join DF in dbRegulatory.AizantIT_DosageForm on PD.DosageFormID equals DF.DosageFormID
                                  join M in dbRegulatory.AizantIT_Market on PD.MarketID equals M.MarketID
                                  join DPM in dbRegulatory.AizantIT_DP_ManufacturingSite on PD.DP_ManufacturingSiteID equals DPM.DP_ManufacturingSiteID
                                  join CT in dbRegulatory.AizantIT_Countries on PD.CountryID equals CT.CountryID
                                  join PS in dbRegulatory.AizantIT_ProdStrength on PD.ProductDossierID equals PS.ProductDossierID
                                  join UOM in dbRegulatory.AizantIT_UOMMaster on PS.UOMID equals UOM.UOMID
                                  where PD.ProductDossierID == DossierID
                                  select new
                                  {
                                      PD.ProductDossierID,
                                      PD.CurrentStatus,
                                      DossierStatus = (PD.DossierStatus == "U" ? "Under Review" : "Approved"),
                                      PD.ApplicationNum,
                                      PM.ProductName,
                                      C.ClientName,
                                      ApplicantName = C1.ClientName,
                                      DF.DosageFormName,
                                      M.MarketName,
                                      CT.CountryName,
                                      PD.MarketingStatusID,
                                      PD.DP_StorageCondition,
                                      PD.ShelfLife,
                                      DPM.DP_ManufacturingSiteName,
                                      Author = (EM.FirstName + " " + EM.LastName),
                                      PD.SubmissionDate,
                                      PD.ReviewAcceptedDate,
                                      PD.GoalDate,
                                      PD.ApprovedDate,
                                      PD.ApproverEmpID,
                                      PD.AuthorEmpID,
                                      PD.PackDetails
                                  }).ToList();

            foreach (var item in DossierDetails)
            {
                objViewDossierModel.ProductDossierID = item.ProductDossierID;
                objViewDossierModel.DossierCurrentStatus = ProductDossierCurrentStatus(item.CurrentStatus);
                objViewDossierModel.CurrentStatus = item.CurrentStatus;
                objViewDossierModel.DossierStatus = item.DossierStatus;
                objViewDossierModel.ApplicationNumber = item.ApplicationNum;
                objViewDossierModel.ProductName = item.ProductName;
                objViewDossierModel.ClinetName = item.ClientName;
                objViewDossierModel.ApplicantName = item.ApplicantName;
                objViewDossierModel.DosageForm = item.DosageFormName;
                objViewDossierModel.MarketName = item.MarketName;
                objViewDossierModel.CountryName = item.CountryName;
                objViewDossierModel.MarketingStatus = GetMarketingStatus(item.MarketingStatusID);
                objViewDossierModel.DP_StorageCondition = item.DP_StorageCondition;
                objViewDossierModel.ShelfLifeID = item.ShelfLife;
                objViewDossierModel.DPManufacturingSiteName = item.DP_ManufacturingSiteName;
                objViewDossierModel.AuthorName = item.Author;
                objViewDossierModel.SubmissionDate = item.SubmissionDate == null ? "NA" : Convert.ToDateTime(item.SubmissionDate).ToString(HelpClass.dateFormat);
                objViewDossierModel.ReviewAcceptedDate = item.ReviewAcceptedDate == null ? "NA" : Convert.ToDateTime(item.ReviewAcceptedDate).ToString(HelpClass.dateFormat);
                objViewDossierModel.GoalDate = item.GoalDate == null ? "NA" : Convert.ToDateTime(item.GoalDate).ToString(HelpClass.dateFormat);
                objViewDossierModel.ApprovedDate = item.ApprovedDate == null ? "NA" : Convert.ToDateTime(item.ApprovedDate).ToString(HelpClass.dateFormat);
                objViewDossierModel.ApproverID = item.ApproverEmpID;
                objViewDossierModel.AuthorID = item.AuthorEmpID;
                objViewDossierModel.PackDetails = item.PackDetails;
            }

            objViewDossierModel.DeficiencyExists = dbRegulatory.AizantIT_DeficiencyMaster.Where(D => D.ProductDossierID == DossierID).Any();

            // Strength
            StringBuilder strngth = new StringBuilder();
            var Strength = (from PD in dbRegulatory.AizantIT_ProductDossier
                            join PS in dbRegulatory.AizantIT_ProdStrength on PD.ProductDossierID equals PS.ProductDossierID
                            join UOM in dbRegulatory.AizantIT_UOMMaster on PS.UOMID equals UOM.UOMID
                            where PD.ProductDossierID == DossierID
                            select new
                            {
                                Strengthvalue = (PS.StrengthValue + "" + UOM.UOMName)
                            });

            foreach (var item in Strength)
            {
                strngth.Append(item.Strengthvalue + ", " + " ");
            }
            objViewDossierModel.Strength = strngth.ToString().Remove(strngth.ToString().Length - 3);

            //DMF Holder
            var ProductDMFHolder = (from PD in dbRegulatory.AizantIT_ProductDossier
                                    join DMF in dbRegulatory.AizantIT_ProdDMFHolder on PD.ProductDossierID equals DMF.ProductDossierID
                                    join DMF_H in dbRegulatory.AizantIT_DMF_Holder on DMF.DMFHolderID equals DMF_H.DMF_HolderID
                                    join Drug in dbRegulatory.AizantIT_DrugSubstance on DMF.DrugSubstanceID equals Drug.DrugSubstanceID
                                    join DSMS in dbRegulatory.AizantIT_DS_ManufacturingSite on DMF.DS_ManufacturingSiteID equals DSMS.DS_ManufacturingSiteID
                                    where PD.ProductDossierID == DossierID
                                    select new
                                    {
                                        DMF_H.DMF_HolderName,
                                        Drug.DrugSubstanceName,
                                        DSMS.DS_ManufacturingSiteName,
                                        DMF.DMFHolderNum
                                    }).ToList();
            List<Product_DMFHolder> objProductDMFHolderList = new List<Product_DMFHolder>();
            foreach (var item in ProductDMFHolder)
            {
                Product_DMFHolder objProductDMFHolder = new Product_DMFHolder();
                objProductDMFHolder.DMF_HolderName = item.DMF_HolderName;
                objProductDMFHolder.DrugSubstance_Name = item.DrugSubstanceName;
                objProductDMFHolder.DSManufacturing_Name = item.DS_ManufacturingSiteName;
                objProductDMFHolder.DMFNumber = item.DMFHolderNum;
                objProductDMFHolderList.Add(objProductDMFHolder);
            }
            objViewDossierModel.Product_DMFHolder = objProductDMFHolderList;

            //Contract Testing Lab
            var ContractTestingLaboratory = (from PD in dbRegulatory.AizantIT_ProductDossier
                                             join CTL in dbRegulatory.AizantIT_ProdContractTestingLab on PD.ProductDossierID equals CTL.ProductDossierID
                                             join CTLab in dbRegulatory.AizantIT_ContractTestingLaboratory on CTL.ContractTestingLabID equals CTLab.ContractTestingLabID
                                             where PD.ProductDossierID == DossierID
                                             select new
                                             {
                                                 CTLab.ContractTestingLabName,
                                                 CTL.FunctionType
                                             }).ToList();

            List<ContractTestingLaboratory> objContractTestingLaboratorytList = new List<ContractTestingLaboratory>();
            foreach (var item in ContractTestingLaboratory)
            {
                ContractTestingLaboratory objContractTestingLaboratory = new ContractTestingLaboratory();
                objContractTestingLaboratory.CTL_Name = item.ContractTestingLabName;
                objContractTestingLaboratory.FunctionalType = item.FunctionType;
                objContractTestingLaboratorytList.Add(objContractTestingLaboratory);
            }
            objViewDossierModel.ContractTestingLabName = objContractTestingLaboratorytList;

            objViewDossierModel.DeficiencyCount = DossierWiseDeficiencyChecking(dbRegulatory, DossierID);
            objViewDossierModel.AuthenticationEmpID = EmpID;
            return PartialView(objViewDossierModel);
        }
        public PartialViewResult _DeficiencyListView()
        {
            return PartialView();
        }
        public DossierViewModel BindAllDropDownList(DossierViewModel objDossierDDList)
        {
            try
            {
                using (AizantIT_DevEntities objDEVEntitie = new AizantIT_DevEntities())
                {
                    //Product Master List
                    List<SelectListItem> objProductList = new List<SelectListItem>();

                    var ProductDetails = (from Product in objDEVEntitie.AizantIT_ProductMaster
                                          select new
                                          {
                                              Product.ProductName,
                                              Product.ProductID,
                                              Product.ProductCode

                                          }).ToList().OrderBy(a => a.ProductName);
                    objProductList.Add(new SelectListItem
                    {
                        Text = "--Select--",
                        Value = "0",
                    });
                    foreach (var Product in ProductDetails)
                    {
                        objProductList.Add(
                            new SelectListItem
                            {
                                Text = Product.ProductName + "-" + Product.ProductCode,
                                Value = Product.ProductID.ToString(),
                                Selected = (Convert.ToInt32(Product.ProductID) == objDossierDDList.ProductID ? true : false)
                            });
                    }
                    objDossierDDList.ddlProductList = objProductList;

                    //Applicant List
                    List<SelectListItem> objApplicantList = new List<SelectListItem>();

                    var ApplicantDetails = (from Applicant in objDEVEntitie.AizantIT_Client
                                            where Applicant.IsActive == true
                                            select new
                                            {
                                                Applicant.ClientName,
                                                Applicant.ClientID

                                            }).ToList().OrderBy(a => a.ClientName);
                    objApplicantList.Add(new SelectListItem
                    {
                        Text = "--Select--",
                        Value = "0",
                    });
                    foreach (var Appilicant in ApplicantDetails)
                    {
                        objApplicantList.Add(
                            new SelectListItem
                            {
                                Text = Appilicant.ClientName,
                                Value = Appilicant.ClientID.ToString(),
                                Selected = (Convert.ToInt32(Appilicant.ClientID) == objDossierDDList.ApplicantID ? true : false)
                            });
                    }
                    objDossierDDList.ddlApplicantList = objApplicantList;

                    //DosageForm
                    List<SelectListItem> objDosageFormList = new List<SelectListItem>();

                    var DosageFormDetails = (from Dosage in objDEVEntitie.AizantIT_DosageForm
                                             where Dosage.isActive == true
                                             select new
                                             {
                                                 Dosage.DosageFormName,
                                                 Dosage.DosageFormID
                                             }).ToList().OrderBy(p => p.DosageFormName);
                    objDosageFormList.Add(new SelectListItem
                    {
                        Text = "--Select--",
                        Value = "0",
                    });
                    foreach (var DosageForm in DosageFormDetails)
                    {
                        objDosageFormList.Add(new SelectListItem
                        {
                            Text = DosageForm.DosageFormName,
                            Value = DosageForm.DosageFormID.ToString(),
                            Selected = (Convert.ToInt32(DosageForm.DosageFormID) == objDossierDDList.DosageFormID ? true : false)
                        });
                    }
                    objDossierDDList.ddlDosageFormList = objDosageFormList;

                    //Market List
                    List<SelectListItem> objMarketList = new List<SelectListItem>();

                    var MarketDetails = (from Market in objDEVEntitie.AizantIT_Market
                                         where Market.isActive == true
                                         select new
                                         {
                                             Market.MarketName,
                                             Market.MarketID
                                         }).ToList().OrderBy(p => p.MarketName);
                    objMarketList.Add(new SelectListItem
                    {
                        Text = "--Select--",
                        Value = "0",
                    });
                    foreach (var market in MarketDetails)
                    {
                        objMarketList.Add(new SelectListItem
                        {
                            Text = market.MarketName,
                            Value = market.MarketID.ToString(),
                            Selected = (Convert.ToInt32(market.MarketID) == objDossierDDList.MarketID ? true : false)
                        });
                    }
                    objDossierDDList.ddlMarketList = objMarketList;

                    //Countries List
                    List<SelectListItem> objCountryList = new List<SelectListItem>();

                    var CountriesDetails = (from Countries in objDEVEntitie.AizantIT_Countries
                                            select new
                                            {
                                                Countries.CountryName,
                                                Countries.CountryID
                                            }).ToList().OrderBy(p => p.CountryName);
                    objCountryList.Add(new SelectListItem
                    {
                        Text = "--Select--",
                        Value = "0",
                    });
                    foreach (var Countries in CountriesDetails)
                    {
                        objCountryList.Add(new SelectListItem
                        {
                            Text = Countries.CountryName,
                            Value = Countries.CountryID.ToString(),
                            Selected = (Convert.ToInt32(Countries.CountryID) == objDossierDDList.CountryID ? true : false)
                        });
                    }
                    objDossierDDList.ddlCountryList = objCountryList;

                    //DrugSubstance List
                    List<SelectListItem> objDrugSubstanceList = new List<SelectListItem>();
                    var DrugSubstanceDetails = (from DrugSubstance in objDEVEntitie.AizantIT_DrugSubstance
                                                where DrugSubstance.IsActive == true
                                                select new
                                                {
                                                    DrugSubstance.DrugSubstanceName,
                                                    DrugSubstance.DrugSubstanceID
                                                }).ToList().OrderBy(p => p.DrugSubstanceName);
                    objDrugSubstanceList.Add(new SelectListItem
                    {
                        Text = "--Select--",
                        Value = "0",
                    });
                    foreach (var DrugSubstance in DrugSubstanceDetails)
                    {
                        objDrugSubstanceList.Add(new SelectListItem
                        {
                            Text = DrugSubstance.DrugSubstanceName,
                            Value = DrugSubstance.DrugSubstanceID.ToString(),
                        });
                    }
                    objDossierDDList.ddlDrugSubstanceList = objDrugSubstanceList;

                    //Client List
                    List<SelectListItem> objClientList = new List<SelectListItem>();

                    var ClientDetails = (from Client in objDEVEntitie.AizantIT_Client
                                         where Client.IsActive == true
                                         select new
                                         {
                                             Client.ClientName,
                                             Client.ClientID

                                         }).ToList().OrderBy(a => a.ClientName);
                    objClientList.Add(new SelectListItem
                    {
                        Text = "--Select--",
                        Value = "0",
                    });
                    foreach (var Client in ClientDetails)
                    {
                        objClientList.Add(
                            new SelectListItem
                            {
                                Text = Client.ClientName,
                                Value = Client.ClientID.ToString(),
                                Selected = (Convert.ToInt32(Client.ClientID) == objDossierDDList.ClientID ? true : false)
                            });
                    }
                    objDossierDDList.ddlClientList = objClientList;

                    //DP_ManufacturingSite List
                    List<SelectListItem> objDPManufacturingSiteList = new List<SelectListItem>();

                    var DP_ManufacturingSiteDetails = (from DP_ManufacturingSite in objDEVEntitie.AizantIT_DP_ManufacturingSite
                                                       where DP_ManufacturingSite.IsActive == true
                                                       select new
                                                       {
                                                           DP_ManufacturingSite.DP_ManufacturingSiteName,
                                                           DP_ManufacturingSite.DP_ManufacturingSiteID

                                                       }).ToList().OrderBy(p => p.DP_ManufacturingSiteName);
                    objDPManufacturingSiteList.Add(new SelectListItem
                    {
                        Text = "--Select--",
                        Value = "0",
                    });
                    foreach (var DP_ManufacturingSite in DP_ManufacturingSiteDetails)
                    {
                        objDPManufacturingSiteList.Add(new SelectListItem
                        {
                            Text = DP_ManufacturingSite.DP_ManufacturingSiteName,
                            Value = DP_ManufacturingSite.DP_ManufacturingSiteID.ToString(),
                            Selected = (Convert.ToInt32(DP_ManufacturingSite.DP_ManufacturingSiteID) == objDossierDDList.DPManufacturingSiteID ? true : false)
                        });
                    }
                    objDossierDDList.ddlDPManufacturingSiteList = objDPManufacturingSiteList;

                    //UOMMaster List
                    List<SelectListItem> objUOMMasterList = new List<SelectListItem>();

                    var UOMMasterDetails = (from UOMMaster in objDEVEntitie.AizantIT_UOMMaster
                                            select new
                                            {
                                                UOMMaster.UOMName,
                                                UOMMaster.UOMID

                                            }).ToList().OrderBy(p => p.UOMName);
                    objUOMMasterList.Add(new SelectListItem
                    {
                        Text = "--Select--",
                        Value = "0",
                    });
                    foreach (var UOMMaster in UOMMasterDetails)
                    {
                        objUOMMasterList.Add(new SelectListItem
                        {
                            Text = UOMMaster.UOMName,
                            Value = UOMMaster.UOMID.ToString(),
                            Selected = (Convert.ToInt32(UOMMaster.UOMID) == objDossierDDList.UOMID ? true : false)
                        });
                    }
                    objDossierDDList.ddlUOMMasterList = objUOMMasterList;

                    //ContractTestingLaboratory List
                    List<SelectListItem> objContractTestingLaboratoryList = new List<SelectListItem>();

                    var ContractTestingLaboratoryDetails = (from Ctl in objDEVEntitie.AizantIT_ContractTestingLaboratory
                                                            where Ctl.IsActive == true
                                                            select new
                                                            {
                                                                Ctl.ContractTestingLabName,
                                                                Ctl.ContractTestingLabID

                                                            }).ToList().OrderBy(p => p.ContractTestingLabName);
                    objContractTestingLaboratoryList.Add(new SelectListItem
                    {
                        Text = "--Select--",
                        Value = "0",
                    });
                    foreach (var CTL in ContractTestingLaboratoryDetails)
                    {
                        objContractTestingLaboratoryList.Add(new SelectListItem
                        {
                            Text = CTL.ContractTestingLabName.ToString(),
                            Value = CTL.ContractTestingLabID.ToString(),
                            Selected = (Convert.ToInt32(CTL.ContractTestingLabID) == objDossierDDList.ContractTestingLabID ? true : false)
                        });
                    }
                    objDossierDDList.ddlContractTestingLaboratoryList = objContractTestingLaboratoryList;

                    //DMF_HolderList
                    List<SelectListItem> objDMFHolderList = new List<SelectListItem>();

                    var DMF_HolderDetails = (from DMF in objDEVEntitie.AizantIT_DMF_Holder
                                             where DMF.IsActive == true
                                             select new
                                             {
                                                 DMF.DMF_HolderName,
                                                 DMF.DMF_HolderID

                                             }).ToList().OrderBy(p => p.DMF_HolderID);
                    objDMFHolderList.Add(new SelectListItem
                    {
                        Text = "--Select--",
                        Value = "0",
                    });
                    foreach (var DMF_Holder in DMF_HolderDetails)
                    {
                        objDMFHolderList.Add(new SelectListItem
                        {
                            Text = DMF_Holder.DMF_HolderName,
                            Value = DMF_Holder.DMF_HolderID.ToString(),
                            Selected = (Convert.ToInt32(DMF_Holder.DMF_HolderID) == objDossierDDList.DMFHolderID ? true : false)
                        });
                    }
                    objDossierDDList.ddlDMFHolderList = objDMFHolderList;

                    //DS Manufacturing Site
                    List<SelectListItem> objDSManufacturingSiteList = new List<SelectListItem>();

                    var DS_ManufacturingSiteDetails = (from DS_ManufacturingSite in objDEVEntitie.AizantIT_DS_ManufacturingSite
                                                       where DS_ManufacturingSite.IsActive == true
                                                       select new
                                                       {
                                                           DS_ManufacturingSite.DS_ManufacturingSiteName,
                                                           DS_ManufacturingSite.DS_ManufacturingSiteID

                                                       }).ToList().OrderBy(p => p.DS_ManufacturingSiteName);
                    objDSManufacturingSiteList.Add(new SelectListItem
                    {
                        Text = "--Select--",
                        Value = "0",
                    });
                    foreach (var DS_ManufacturingSite in DS_ManufacturingSiteDetails)
                    {
                        objDSManufacturingSiteList.Add(new SelectListItem
                        {
                            Text = DS_ManufacturingSite.DS_ManufacturingSiteName,
                            Value = DS_ManufacturingSite.DS_ManufacturingSiteID.ToString(),
                            Selected = (Convert.ToInt32(DS_ManufacturingSite.DS_ManufacturingSiteID) == objDossierDDList.DS_ManufacturingSiteID ? true : false)
                        });
                    }
                    objDossierDDList.ddlDSManufacturingList = objDSManufacturingSiteList;
                    return objDossierDDList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region ProductDossier
        [CustAuthorization((int)Modules.RA, (int)RA_UserRole.RA_Lead, (int)RA_UserRole.RA_Approver)]
        public ActionResult CreateDossierMain(int ProductDossierID = 0) // where there's a Dossier ID it's Update else it's for Create.
        {
            try
            {
                DossierViewModel dossierViewModel = new DossierViewModel();
                Aizant_Bl objAizant_Bl = new Aizant_Bl();
                if (ProductDossierID != 0)
                {
                    var dbDevEntities = new AizantIT_DevEntities();

                    var ProductDetails = dbDevEntities.AizantIT_ProductDossier.Where(a => a.ProductDossierID == ProductDossierID).Select(a => new { a.CurrentStatus, a.AuthorEmpID }).SingleOrDefault();
                    //if not in state of Edit Redirect to View DossierDetails Action Method calling  in Same Controller 
                    if ((ProductDetails.CurrentStatus != 1 || ProductDetails.CurrentStatus != 4) && ProductDetails.AuthorEmpID != Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]))
                    {
                        return RedirectToAction("viewDossierDetails", new { DossierID = ProductDossierID, ListType = 3 });//3:Product Details Main List
                    }
                    else
                    {
                        int Notification_ID = GetDossierNotificationID(ProductDossierID);
                        if (Notification_ID != 0)
                        {
                            UpdateRA_NotificationStatus(Notification_ID, 2);
                        }
                        using (var dbContext = new AizantIT_DevEntities())
                        {
                            var ProductDossierData = dbContext.AizantIT_ProductDossier.Where(a => a.ProductDossierID == ProductDossierID).Select(a => new
                            {
                                a.DossierStatus,
                                a.ProductDossierID,
                                a.ApplicationNum,
                                a.ProductID,
                                a.ClientID,
                                a.ApplicantID,
                                a.DosageFormID,
                                a.DP_ManufacturingSiteID,
                                a.ShelfLife,
                                a.DP_StorageCondition,
                                a.CurrentStatus,
                                a.MarketID,
                                a.CountryID,
                                a.AuthorEmpID,
                                a.MarketingStatusID,
                                a.SubmissionDate,
                                a.ReviewAcceptedDate,
                                a.GoalDate,
                                a.ApprovedDate,
                                a.ApproverEmpID,
                                a.PackDetails
                            }).SingleOrDefault();

                            dossierViewModel.DossierStatus = ProductDossierData.DossierStatus;
                            dossierViewModel.ProductDossierID = ProductDossierData.ProductDossierID;
                            dossierViewModel.ApplicationNumber = ProductDossierData.ApplicationNum;
                            dossierViewModel.ProductID = ProductDossierData.ProductID;
                            dossierViewModel.ClientID = ProductDossierData.ClientID;
                            dossierViewModel.ApplicantID = ProductDossierData.ApplicantID;
                            dossierViewModel.DosageFormID = ProductDossierData.DosageFormID;
                            dossierViewModel.DP_StorageCondition = ProductDossierData.DP_StorageCondition;
                            dossierViewModel.ShelfLifeID = ProductDossierData.ShelfLife;
                            dossierViewModel.DPManufacturingSiteID = ProductDossierData.DP_ManufacturingSiteID;
                            dossierViewModel.AuthorName = dbContext.AizantIT_EmpMaster.Where(a => a.EmpID == ProductDossierData.AuthorEmpID).Select(a => a.FirstName + " " + a.LastName).SingleOrDefault();
                            dossierViewModel.ProductCurrentStatus = ProductDossierCurrentStatus(ProductDossierData.CurrentStatus);
                            dossierViewModel.CurrentStatusID = ProductDossierData.CurrentStatus;
                            dossierViewModel.MarketID = ProductDossierData.MarketID;
                            dossierViewModel.CountryID = ProductDossierData.CountryID;
                            dossierViewModel.AuthorID = ProductDossierData.AuthorEmpID;
                            dossierViewModel.MarketingStatusID = ProductDossierData.MarketingStatusID;
                            dossierViewModel.SubmissionDate = Convert.ToDateTime(ProductDossierData.SubmissionDate).ToString(HelpClass.dateFormat);
                            dossierViewModel.ReviewAcceptedDate = ProductDossierData.ReviewAcceptedDate == null ? "" : Convert.ToDateTime(ProductDossierData.ReviewAcceptedDate).ToString(HelpClass.dateFormat);
                            dossierViewModel.GoalDate = ProductDossierData.GoalDate == null ? "" : Convert.ToDateTime(ProductDossierData.GoalDate).ToString(HelpClass.dateFormat);
                            dossierViewModel.ApprovedDate = ProductDossierData.ApprovedDate == null ? "" : Convert.ToDateTime(ProductDossierData.ApprovedDate).ToString(HelpClass.dateFormat);
                            dossierViewModel.PackDetails = ProductDossierData.PackDetails;
                            int[] AuthorId = { ProductDossierData.AuthorEmpID };
                            dossierViewModel.ApproverList = objAizant_Bl.GetListofEmpByRole(AuthorId, Convert.ToInt32(RA_UserRole.RA_Approver), "", ProductDossierData.ApproverEmpID.ToString());

                            var Strength = (from PD in dbContext.AizantIT_ProductDossier
                                            join PS in dbContext.AizantIT_ProdStrength on PD.ProductDossierID equals PS.ProductDossierID
                                            join UOM in dbContext.AizantIT_UOMMaster on PS.UOMID equals UOM.UOMID
                                            where PD.ProductDossierID == ProductDossierID
                                            select new
                                            {
                                                PS.ProdStrengthID,
                                                PS.StrengthValue,
                                                UOM.UOMID,
                                                UOM.UOMName
                                            }).ToList();
                            List<Strength> objStrengthList = new List<Strength>();
                            foreach (var item in Strength)
                            {
                                Strength objStrength = new Strength();
                                objStrength.ProdStrengthID = item.ProdStrengthID;
                                objStrength.StrengthName = item.StrengthValue;
                                objStrength.UOMID = item.UOMID;
                                objStrength.UOMNAME = item.UOMName;
                                objStrengthList.Add(objStrength);
                            }

                            dossierViewModel.Strength = objStrengthList;
                            var ContractTestingLaboratory = (from PD in dbContext.AizantIT_ProductDossier
                                                             join CTL in dbContext.AizantIT_ProdContractTestingLab on PD.ProductDossierID equals CTL.ProductDossierID
                                                             join CTLab in dbContext.AizantIT_ContractTestingLaboratory on CTL.ContractTestingLabID equals CTLab.ContractTestingLabID
                                                             where PD.ProductDossierID == ProductDossierID
                                                             select new
                                                             {
                                                                 CTL.ProdContractTestingLabID,
                                                                 CTLab.ContractTestingLabID,
                                                                 CTLab.ContractTestingLabName,
                                                                 CTL.FunctionType
                                                             }).ToList();

                            List<ContractTestingLaboratory> objContractTestingLaboratorytList = new List<ContractTestingLaboratory>();
                            foreach (var item in ContractTestingLaboratory)
                            {
                                ContractTestingLaboratory objContractTestingLaboratory = new ContractTestingLaboratory();
                                objContractTestingLaboratory.ProdContractTestingLabID = item.ProdContractTestingLabID;
                                objContractTestingLaboratory.CTL_ID = item.ContractTestingLabID;
                                objContractTestingLaboratory.CTL_Name = item.ContractTestingLabName;
                                objContractTestingLaboratory.FunctionalType = item.FunctionType;
                                objContractTestingLaboratorytList.Add(objContractTestingLaboratory);
                            }

                            dossierViewModel.ContractTestingLaboratory = objContractTestingLaboratorytList;
                            //Product DMF Holder
                            var ProductDMFHolder = (from PD in dbContext.AizantIT_ProductDossier
                                                    join DMF in dbContext.AizantIT_ProdDMFHolder on PD.ProductDossierID equals DMF.ProductDossierID
                                                    join DMF_H in dbContext.AizantIT_DMF_Holder on DMF.DMFHolderID equals DMF_H.DMF_HolderID
                                                    join Drug in dbContext.AizantIT_DrugSubstance on DMF.DrugSubstanceID equals Drug.DrugSubstanceID
                                                    join DSMS in dbContext.AizantIT_DS_ManufacturingSite on DMF.DS_ManufacturingSiteID equals DSMS.DS_ManufacturingSiteID
                                                    where DMF.ProductDossierID == ProductDossierID
                                                    select new
                                                    {
                                                        DMF_H.DMF_HolderName,
                                                        Drug.DrugSubstanceName,
                                                        DSMS.DS_ManufacturingSiteName,
                                                        DMF.DMFHolderNum,
                                                        DMF.DMFHolderID,
                                                        DMF.DrugSubstanceID,
                                                        DMF.DS_ManufacturingSiteID,
                                                        DMF.ProdDMFHolderID
                                                    }).ToList();


                            List<Product_DMFHolder> objProdDMFHolderList = new List<Product_DMFHolder>();
                            foreach (var item in ProductDMFHolder)
                            {
                                Product_DMFHolder obProdDMFHolder = new Product_DMFHolder();
                                obProdDMFHolder.ProdDMFHolderID = item.ProdDMFHolderID;
                                obProdDMFHolder.DMF_HolderID = item.DMFHolderID;
                                obProdDMFHolder.DrugSubstance_ID = item.DrugSubstanceID;
                                obProdDMFHolder.DSManufacturing_ID = item.DS_ManufacturingSiteID;
                                obProdDMFHolder.DMFNumber = item.DMFHolderNum;
                                obProdDMFHolder.DMF_HolderName = item.DMF_HolderName;
                                obProdDMFHolder.DrugSubstance_Name = item.DrugSubstanceName;
                                obProdDMFHolder.DSManufacturing_Name = item.DS_ManufacturingSiteName;
                                objProdDMFHolderList.Add(obProdDMFHolder);
                            }
                            dossierViewModel.Product_DMFHolder = objProdDMFHolderList;

                            DossierViewModel objbindddlList = BindAllDropDownList(dossierViewModel);
                            if (ProductDossierData.CurrentStatus == 1)
                            {
                                dossierViewModel.ProductDossierType = "SubmitForApproval";
                            }
                            else if (ProductDossierData.CurrentStatus == 4)
                            {
                                dossierViewModel.ProductDossierType = "Update";
                            }
                            else
                            {
                                dossierViewModel.ProductDossierType = "NoAction";
                            }
                            return View(dossierViewModel);
                        }
                    }
                }
                else
                {
                    string[] ModuleRoles = (string[])Session["EmpModuleRoles"];
                    if (ModuleRoles.Contains(((int)Aizant_Enums.AizantEnums.RA_UserRole.RA_Lead).ToString()))
                    {
                        dossierViewModel.AuthorName = ((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpName"]).ToString();
                        var EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                        int[] AuthorId = { EmpID };
                        dossierViewModel.ApproverList = objAizant_Bl.GetListofEmpByRole(AuthorId, Convert.ToInt32(RA_UserRole.RA_Approver));
                        dossierViewModel.ProductDossierType = "Create";
                        DossierViewModel objbindddlList = BindAllDropDownList(dossierViewModel);
                        dossierViewModel.ProductCurrentStatus = ProductDossierCurrentStatus(0);// 0:ProductCurrentStatus "NA"
                        return View(dossierViewModel);
                    }
                    else
                    {
                        return View("_RegulatoryUnAuthorizedView");
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult AddProductDossier(CreateProductDossier objCreateProductDossier)
        {
            try
            {
                using (var dbContext = new AizantIT_DevEntities())
                {
                    var ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);

                    bool CheckingValue = ProductDossierApplicationNumberChecking(objCreateProductDossier.ApplicationNumber);
                    if (CheckingValue)
                    {
                        return Json(new { msgType = "warning" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                        {
                            try
                            {
                                AizantIT_ProductDossier objProductDossier = new AizantIT_ProductDossier();
                                objProductDossier.ProductID = objCreateProductDossier.ProductID;
                                objProductDossier.ApplicationNum = objCreateProductDossier.ApplicationNumber;
                                objProductDossier.DosageFormID = objCreateProductDossier.DosageFormID;
                                objProductDossier.MarketID = objCreateProductDossier.MarketID;
                                objProductDossier.CountryID = objCreateProductDossier.CountryID;
                                objProductDossier.ApplicantID = objCreateProductDossier.ApplicantNameID;
                                objProductDossier.MarketingStatusID = objCreateProductDossier.MarketingStatusID;
                                objProductDossier.ClientID = objCreateProductDossier.ClientID;
                                objProductDossier.DP_StorageCondition = objCreateProductDossier.DrugProductStorageCondition;
                                objProductDossier.ShelfLife = objCreateProductDossier.ShelfLifeID;
                                objProductDossier.DP_ManufacturingSiteID = objCreateProductDossier.DrugProductManufacturingSiteID;
                                if (objCreateProductDossier.CreationType == "Create")
                                {
                                    objProductDossier.CurrentStatus = Convert.ToInt32(RA_DossierHistoryStatus.Product_Created);
                                }
                                else if (objCreateProductDossier.CreationType == "CreateAndSubmit")
                                {
                                    objProductDossier.CurrentStatus = Convert.ToInt32(RA_DossierHistoryStatus.Submitted_for_Approval);
                                }
                                objProductDossier.SubmissionDate = objCreateProductDossier.SubmissionDate;
                                objProductDossier.ReviewAcceptedDate = getDateValue(objCreateProductDossier.ReviewAcceptedDate);
                                objProductDossier.GoalDate = getDateValue(objCreateProductDossier.GoalDate);
                                objProductDossier.ApprovedDate = getDateValue(objCreateProductDossier.ApprovedDate);
                                objProductDossier.ApproverEmpID = objCreateProductDossier.ApproverID;
                                objProductDossier.AuthorEmpID = ActionBy;
                                objProductDossier.DossierStatus = objCreateProductDossier.DossierStatus;
                                objProductDossier.CreatedAs = objCreateProductDossier.DossierStatus;
                                objProductDossier.PackDetails = objCreateProductDossier.PackDetails;
                                dbContext.AizantIT_ProductDossier.Add(objProductDossier);
                                dbContext.SaveChanges();

                                //Strength
                                List<AizantIT_ProdStrength> objProdStrengthList = new List<AizantIT_ProdStrength>();
                                if (objCreateProductDossier.Strength.Count > 0)
                                {

                                    foreach (var item in objCreateProductDossier.Strength)
                                    {
                                        AizantIT_ProdStrength objProdStrength = new AizantIT_ProdStrength();
                                        objProdStrength.ProductDossierID = objProductDossier.ProductDossierID;
                                        objProdStrength.StrengthValue = item.StrengthName;
                                        objProdStrength.UOMID = item.UOMID;
                                        objProdStrengthList.Add(objProdStrength);
                                    }
                                    dbContext.AizantIT_ProdStrength.AddRange(objProdStrengthList);
                                }
                                //Contract Testing Laboratory
                                List<AizantIT_ProdContractTestingLab> objProdContractTestingLabList = new List<AizantIT_ProdContractTestingLab>();
                                if (objCreateProductDossier.ContractTestingLaboratory.Count > 0)
                                {
                                    foreach (var item in objCreateProductDossier.ContractTestingLaboratory)
                                    {
                                        AizantIT_ProdContractTestingLab objPCT = new AizantIT_ProdContractTestingLab();
                                        objPCT.ProductDossierID = objProductDossier.ProductDossierID;
                                        objPCT.FunctionType = item.FunctionalType;
                                        objPCT.ContractTestingLabID = item.CTL_ID;
                                        objProdContractTestingLabList.Add(objPCT);
                                    }
                                    dbContext.AizantIT_ProdContractTestingLab.AddRange(objProdContractTestingLabList);
                                }
                                //DMF Holder
                                List<Product_DMFHolderList> Prod_DMFHolderlist = new List<Product_DMFHolderList>();
                                if (objCreateProductDossier.Product_DMFHolder.Count > 0)
                                {
                                    foreach (var item in objCreateProductDossier.Product_DMFHolder)
                                    {
                                        Prod_DMFHolderlist.Add(new Product_DMFHolderList(objProductDossier.ProductDossierID, item.DMF_HolderID, item.DrugSubstance_ID, item.DSManufacturing_ID, item.DMFNumber));
                                    }
                                    ProdDMF_HolderInsert(dbContext, Prod_DMFHolderlist);
                                }

                                dbContext.SaveChanges();

                                if (objCreateProductDossier.CreationType == "Create")
                                {
                                    ProductDossierHistoryInsert(dbContext, objProductDossier.ProductDossierID, ActionBy, Convert.ToInt32(RA_UserRole.RA_Lead), Convert.ToInt32(RA_DossierHistoryStatus.Product_Created), objCreateProductDossier.Comments);
                                }
                                else
                                {
                                    ProductDossierHistoryInsert(dbContext, objProductDossier.ProductDossierID, ActionBy, Convert.ToInt32(RA_UserRole.RA_Lead), Convert.ToInt32(RA_DossierHistoryStatus.Product_Created), objCreateProductDossier.Comments);
                                    ProductDossierHistoryInsert(dbContext, objProductDossier.ProductDossierID, ActionBy, Convert.ToInt32(RA_UserRole.RA_Lead), Convert.ToInt32(RA_DossierHistoryStatus.Submitted_for_Approval), objCreateProductDossier.Comments);

                                    OnDossierCreateAddNotification(dbContext, objProductDossier.ApplicationNum, objProductDossier.ApproverEmpID, objProductDossier.ProductDossierID, objProductDossier.DossierStatus);
                                }
                                transaction.Commit();
                                return Json(new { msgType = "success", _DossierID = objProductDossier.ProductDossierID, _CurrentStatus = objProductDossier.CurrentStatus }, JsonRequestBehavior.AllowGet);
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                throw ex;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        private static void OnDossierCreateAddNotification(AizantIT_DevEntities dbContext, string ApplicationNum, int ApproverEmpID, int ProductDossierID, string DossierStatus)
        {
            NotificationActionByLinq objNotifications = new NotificationActionByLinq();
            NotificationBO objNotificationsBO = new NotificationBO();
            objNotificationsBO.Description = string.Format("Product has been Created on Product Application No: '{0}' of Dossier Status '{1}'", ApplicationNum, (DossierStatus == "U" ? "Under Review" : "Approved"));
            objNotificationsBO.ModuleID = Convert.ToInt32(Modules.RA);
            objNotificationsBO.NotificationLink = "/Regulatory/RegulatoryMain/ViewDossierDetails?DossierID=" + ProductDossierID + "&ListType=1";
            List<NotificationRoleIDandDeptID> NotificationRoleIDSandDeptIDs = new List<NotificationRoleIDandDeptID>();
            NotificationRoleIDSandDeptIDs.Add(new NotificationRoleIDandDeptID
            {
                RoleID = Convert.ToInt32(RA_UserRole.RA_Approver),
                DeptID = 0
            });
            objNotificationsBO.NotificationRoleIDSandDeptIDs = NotificationRoleIDSandDeptIDs;
            List<int> objNotificationEmpIDs = new List<int>();
            objNotificationEmpIDs.Add(ApproverEmpID);
            objNotificationsBO.NotificationEmpIDS = objNotificationEmpIDs;
            int GetNotificationID_No = objNotifications.InsertNotifications(objNotificationsBO);

            AizantIT_DossierNotifyLink objDossierNotifyLink = new AizantIT_DossierNotifyLink();
            objDossierNotifyLink.ProductDossierID = ProductDossierID;
            objDossierNotifyLink.NotificationID = GetNotificationID_No;
            dbContext.AizantIT_DossierNotifyLink.Add(objDossierNotifyLink);
            dbContext.SaveChanges();
        }
        public List<string> ValidateDossierDeficiency(AizantIT_DevEntities dbContext, CreateProductDossier objCreateProductDossier)
        {
            List<string> lstError = new List<string>();
            var ProductDossierID = dbContext.AizantIT_ProductDossier.Where(a => a.ProductDossierID == objCreateProductDossier.ProductDossierID).Select(a => a.ProductDossierID).SingleOrDefault();

            var DeficiencyObservation =
                        from T1 in dbContext.AizantIT_DeficiencyMaster
                        join T2 in dbContext.AizantIT_DF_Observation on T1.DeficiencyID equals T2.DeficiencyID into leftJoinObj
                        from T3 in leftJoinObj.DefaultIfEmpty()
                        where T1.ProductDossierID == ProductDossierID
                        select new
                        {
                            _DeficiencyID = T1.DeficiencyID,
                            _ObservationID = (int?)T3.DF_ObservationID
                        };

            var DeficiencyWithNoObservation = DeficiencyObservation.Where(p => p._ObservationID == null).Any();

            if (DeficiencyWithNoObservation)
            {
                lstError.Add(RegulatoryValidationMessages.RA_ErrorMsg2);
            }
            DateTime _ProductApprovedDate = Convert.ToDateTime(objCreateProductDossier.ApprovedDate);
            bool IsReceiptDateExceed = false;
            bool IsDueDateExceed = false;
            var DossierRecords = dbContext.AizantIT_DeficiencyMaster.Where(a => a.ProductDossierID == objCreateProductDossier.ProductDossierID
                 && !(a.DeficiencyReceiptDate >= objCreateProductDossier.SubmissionDate && a.DeficiencyReceiptDate <= _ProductApprovedDate)).Any();
            if (DossierRecords)
            {
                IsReceiptDateExceed = true;
            }
            var DossierRecordsForDueDate = dbContext.AizantIT_DeficiencyMaster.Where(a => a.ProductDossierID == objCreateProductDossier.ProductDossierID
                 && !(a.DueDate >= objCreateProductDossier.SubmissionDate && a.DueDate <= _ProductApprovedDate)).Any();
            if (DossierRecordsForDueDate)
            {
                IsDueDateExceed = true;
            }
            if (IsDueDateExceed || IsReceiptDateExceed)
            {
                lstError.Add("Deficiency " + ((IsDueDateExceed && IsReceiptDateExceed) ? "Receipt Date & Due Date" : IsDueDateExceed ? "Due Date" : "Receipt Date") + " Should be within Product Submission Date & Approved Date.");
            }
            return lstError;
        }
        [HttpPost]
        public ActionResult UpdateProductDossier(CreateProductDossier objCreateProductDossier)
        {
            try
            {
                var EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                using (var dbContext = new AizantIT_DevEntities())
                {
                    if (dbContext.AizantIT_ProductDossier.Where(a => a.ProductDossierID == objCreateProductDossier.ProductDossierID && a.AuthorEmpID == EmpID).Any())
                    {
                        var HasDeficiency = dbContext.AizantIT_DeficiencyMaster.Select(p => p.ProductDossierID == objCreateProductDossier.ProductDossierID).Any();
                        List<string> lstValidateDossierDeficiency;
                        if (HasDeficiency)
                        {
                            lstValidateDossierDeficiency = ValidateDossierDeficiency(dbContext, objCreateProductDossier);
                            if (lstValidateDossierDeficiency.Count == 0)
                            {
                                return SubmitUpdatedDossier(dbContext, objCreateProductDossier);
                            }
                            else
                            {
                                return Json(new { msg = lstValidateDossierDeficiency, msgType = "error" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return SubmitUpdatedDossier(dbContext, objCreateProductDossier);
                        }
                    }
                    else
                    {
                        return Json(new { msgType = "sysError", msg = RegulatoryValidationMessages.RA_ErrorMsg1 }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "sysError" }, JsonRequestBehavior.AllowGet);
            }
        }
        private JsonResult SubmitUpdatedDossier(AizantIT_DevEntities dbContext, CreateProductDossier objCreateProductDossier)
        {

            var ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
            {
                try
                {
                    //implementing Common Update Dossier Function
                    ProductDossierUpdate(dbContext, objCreateProductDossier);
                    //History 
                    ProductDossierHistoryInsert(dbContext, objCreateProductDossier.ProductDossierID, ActionBy, Convert.ToInt32(RA_UserRole.RA_Lead), Convert.ToInt32(RA_DossierHistoryStatus.Modified_Product), objCreateProductDossier.Comments);
                    ProductDossierHistoryInsert(dbContext, objCreateProductDossier.ProductDossierID, ActionBy, Convert.ToInt32(RA_UserRole.RA_Lead), Convert.ToInt32(RA_DossierHistoryStatus.Submitted_for_Approval), objCreateProductDossier.Comments);
                    //Notification Update
                    NotificationActionByLinq objNotifications = new NotificationActionByLinq();
                    NotificationBO objNotificationsBO = new NotificationBO();
                    objNotificationsBO.NotificationID = GetDossierNotificationID(objCreateProductDossier.ProductDossierID);
                    objNotificationsBO.Description = string.Format("Product has been Modified on Product Application No: '{0}' of Dossier Status '{1}'", objCreateProductDossier.ApplicationNumber, (objCreateProductDossier.DossierStatus == "U" ? "Under Review" : "Approved"));
                    objNotificationsBO.ModuleID = Convert.ToInt32(Modules.RA);
                    objNotificationsBO.NotificationLink = "/Regulatory/RegulatoryMain/ViewDossierDetails?DossierID=" + objCreateProductDossier.ProductDossierID + "&ListType=1";
                    List<NotificationRoleIDandDeptID> NotificationRoleIDSandDeptIDs = new List<NotificationRoleIDandDeptID>();
                    NotificationRoleIDSandDeptIDs.Add(new NotificationRoleIDandDeptID
                    {
                        RoleID = Convert.ToInt32(RA_UserRole.RA_Approver),
                        DeptID = 0
                    });
                    objNotificationsBO.NotificationRoleIDSandDeptIDs = NotificationRoleIDSandDeptIDs;
                    List<int> objNotificationEmpIDs = new List<int>();
                    objNotificationEmpIDs.Add(objCreateProductDossier.ApproverID);
                    objNotificationsBO.NotificationEmpIDS = objNotificationEmpIDs;
                    //objNotificationsBO.NotificationStatus = 1;
                    objNotifications.NotificationToNextEmp(objNotificationsBO);
                    transaction.Commit();
                    return Json(new { msgType = "success" }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }
        public void ProductDossierUpdate(AizantIT_DevEntities dbContext, CreateProductDossier objCreateProductDossier)
        {
            try
            {
                var ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                var objProductDossier = dbContext.AizantIT_ProductDossier.Where(a => a.ProductDossierID == objCreateProductDossier.ProductDossierID).SingleOrDefault();
                objProductDossier.DosageFormID = objCreateProductDossier.DosageFormID;
                objProductDossier.MarketID = objCreateProductDossier.MarketID;
                objProductDossier.CountryID = objCreateProductDossier.CountryID;
                objProductDossier.ApplicantID = objCreateProductDossier.ApplicantNameID;
                objProductDossier.MarketingStatusID = objCreateProductDossier.MarketingStatusID;
                objProductDossier.ClientID = objCreateProductDossier.ClientID;
                objProductDossier.DP_StorageCondition = objCreateProductDossier.DrugProductStorageCondition;
                objProductDossier.ShelfLife = objCreateProductDossier.ShelfLifeID;
                objProductDossier.DP_ManufacturingSiteID = objCreateProductDossier.DrugProductManufacturingSiteID;
                objProductDossier.CurrentStatus = Convert.ToInt32(RA_DossierHistoryStatus.Submitted_for_Approval);
                objProductDossier.SubmissionDate = objCreateProductDossier.SubmissionDate;
                objProductDossier.ReviewAcceptedDate = getDateValue(objCreateProductDossier.ReviewAcceptedDate);
                objProductDossier.GoalDate = getDateValue(objCreateProductDossier.GoalDate);
                objProductDossier.ApprovedDate = getDateValue(objCreateProductDossier.ApprovedDate);
                objProductDossier.ApproverEmpID = objCreateProductDossier.ApproverID;
                objProductDossier.AuthorEmpID = ActionBy;
                objProductDossier.DossierStatus = objCreateProductDossier.DossierStatus;
                objProductDossier.CreatedAs = objCreateProductDossier.DossierStatus;
                objProductDossier.PackDetails = objCreateProductDossier.PackDetails;
                dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private ActionResult SubmitForApprovalAfterCreate(AizantIT_DevEntities dbContext, CreateProductDossier objProductDossierApproval)
        {
            var ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
            {
                try
                {
                    var DossierDetails = dbContext.AizantIT_ProductDossier.Where(a => a.ProductDossierID == objProductDossierApproval.ProductDossierID).Select(a => new
                    {
                        a.ProductDossierID,
                        a.DossierStatus,
                        a.ApproverEmpID,
                        a.ApplicationNum,
                    }).SingleOrDefault();
                    ProductDossierUpdate(dbContext, objProductDossierApproval);
                    //Dossier History
                    ProductDossierHistoryInsert(dbContext, DossierDetails.ProductDossierID, ActionBy, Convert.ToInt32(RA_UserRole.RA_Lead), Convert.ToInt32(RA_DossierHistoryStatus.Submitted_for_Approval), objProductDossierApproval.Comments);

                    OnDossierCreateAddNotification(dbContext, DossierDetails.ApplicationNum, DossierDetails.ApproverEmpID, DossierDetails.ProductDossierID, DossierDetails.DossierStatus);
                    transaction.Commit();
                    return Json(new { msgType = "success", _DossierID = objProductDossierApproval.ProductDossierID }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }
        public ActionResult ProductDossierSubmitForApproval(CreateProductDossier objProductDossierApproval)
        {
            try
            {
                using (var dbContext = new AizantIT_DevEntities())
                {
                    var HasDeficiency = dbContext.AizantIT_DeficiencyMaster.Select(p => p.ProductDossierID == objProductDossierApproval.ProductDossierID).Any();
                    List<string> lstValidateDossierDeficiency;
                    if (HasDeficiency)
                    {
                        lstValidateDossierDeficiency = ValidateDossierDeficiency(dbContext, objProductDossierApproval);
                        if (lstValidateDossierDeficiency.Count == 0)
                        {
                            return SubmitForApprovalAfterCreate(dbContext, objProductDossierApproval);
                        }
                        else
                        {
                            return Json(new { msg = lstValidateDossierDeficiency, msgType = "error", _DossierID = objProductDossierApproval.ProductDossierID }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return SubmitForApprovalAfterCreate(dbContext, objProductDossierApproval);
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "sysError" }, JsonRequestBehavior.AllowGet);
            }
        }
        public bool ProductDossierApplicationNumberChecking(string ApplicationNumber)
        {
            try
            {
                using (var dbContext = new AizantIT_DevEntities())
                {
                    var ApplicationNumberChecking = dbContext.AizantIT_ProductDossier.Any(a => a.ApplicationNum == ApplicationNumber);
                    if (ApplicationNumberChecking)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CustAuthorization((int)Modules.RA, (int)RA_UserRole.RA_Lead, (int)RA_UserRole.RA_Approver)]
        public ActionResult ViewDossierDetails(int DossierID, int ListType = 0)
        {
            var EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            DossierMainDetails objDossierMainDetailsModel = new DossierMainDetails();
            Aizant_Bl objAizant_Bl = new Aizant_Bl();
            var dbRegulatory = new AizantIT_DevEntities();
            List<SelectListItem> objDossierList = new List<SelectListItem>();

            var DossierMainDetails = (from PD in dbRegulatory.AizantIT_ProductDossier
                                      join EM in dbRegulatory.AizantIT_EmpMaster on PD.AuthorEmpID equals EM.EmpID
                                      join EM1 in dbRegulatory.AizantIT_EmpMaster on PD.ApproverEmpID equals EM1.EmpID
                                      where PD.ProductDossierID == DossierID
                                      select new
                                      {
                                          PD.AuthorEmpID,
                                          PD.ApproverEmpID,
                                          PD.CurrentStatus,
                                          PD.DossierStatus,
                                          PD.ReviewAcceptedDate,
                                          PD.GoalDate,
                                          Approver = (EM1.FirstName + " " + EM1.LastName),
                                          PD.PackDetails
                                      }).SingleOrDefault();

            objDossierMainDetailsModel.DossierAuthorID = DossierMainDetails.AuthorEmpID;
            objDossierMainDetailsModel.DossierApproverID = DossierMainDetails.ApproverEmpID;
            objDossierMainDetailsModel.CurrentStatus = DossierMainDetails.CurrentStatus;
            objDossierMainDetailsModel.DossierStatus = DossierMainDetails.DossierStatus;
            objDossierMainDetailsModel.ReviewAcceptedDate = DossierMainDetails.ReviewAcceptedDate == null ? "NA" : Convert.ToDateTime(DossierMainDetails.ReviewAcceptedDate).ToString(HelpClass.dateFormat);
            objDossierMainDetailsModel.GoalDate = DossierMainDetails.GoalDate == null ? "NA" : Convert.ToDateTime(DossierMainDetails.GoalDate).ToString(HelpClass.dateFormat);
            objDossierMainDetailsModel.ProductDossierID = DossierID;
            objDossierMainDetailsModel.ListType = ListType;
            objDossierMainDetailsModel.PackDetails = DossierMainDetails.PackDetails;
            objDossierMainDetailsModel.DossierApproverName = DossierMainDetails.Approver;
            objDossierMainDetailsModel.DeficiencyExists = dbRegulatory.AizantIT_DeficiencyMaster.Where(D => D.ProductDossierID == DossierID).Any();

            objDossierMainDetailsModel.DeficiencyCount = DossierWiseDeficiencyChecking(dbRegulatory, DossierID);
            objDossierMainDetailsModel.AuthenticationEmpID = EmpID;
            return View(objDossierMainDetailsModel);
        }
        public ActionResult _UploadAttachments()
        {
            return PartialView();
        }
        public ActionResult DossierPendingList()
        {
            return View();
        }
        public void UpdateRA_NotificationStatus(int NotificationID, int NotificationStatus)
        {
            if (NotificationID != 0)
            {
                NotificationActionByLinq objNotifications = new NotificationActionByLinq();
                int[] EmpIDs = { Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"].ToString()) };
                objNotifications.UpdateNotificationStatus(NotificationID, EmpIDs, NotificationStatus);//2 represents NotifyEmp had visited the page through notification link.
            }
        }
        [CustAuthorization((int)Modules.RA, (int)RA_UserRole.RA_Lead,
        (int)RA_UserRole.RA_Approver,
        (int)RA_UserRole.RA_HOD)]
        public ActionResult DossierMainList(string ShowType)
        {
            Session["ProductDetailsList"] = null;
            ViewBag.ShowType = ShowType;
            return View();
        }
        public ActionResult DossierReviewList(int Notification_ID = 0)
        {
            return View();
        }
        #region Deficiency Attachments
        public ActionResult DeficiencyAttachments()
        {
            try
            {
                var dbRegulatory = new AizantIT_DevEntities();
                string Msg = "";
                string MsgType = "";
                int filecount = Request.Files.Count;
                string DeficiencyID = Request.Params["DeficiencyID"];
                HttpFileCollectionBase files = Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    AizantIT_DeficiencyAttachment objDeficiencyAttachment = new AizantIT_DeficiencyAttachment();
                    HttpPostedFileBase file = files[i];
                    string fileName = GetActualFileName(file.FileName);
                    objDeficiencyAttachment.FileName = fileName;
                    objDeficiencyAttachment.FileExtention = GetOnlyFileExtension(file.FileName).ToLower();
                    byte[] bytes;
                    using (BinaryReader br = new BinaryReader(file.InputStream))
                    {
                        bytes = br.ReadBytes(file.ContentLength);
                        objDeficiencyAttachment.Attachment = bytes;
                    }
                    objDeficiencyAttachment.AttachmentDate = DateTime.Now;
                    objDeficiencyAttachment.DeficiencyID = Convert.ToInt32(DeficiencyID);
                    dbRegulatory.AizantIT_DeficiencyAttachment.Add(objDeficiencyAttachment);
                    dbRegulatory.SaveChanges();
                }
                Msg = RegulatoryValidationMessages.RA_ErrorMsg3;
                MsgType = "success";
                return Json(new { msg = Msg, msgType = MsgType }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetDeficiencyAttachmentList()
        {
            try
            {
                int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];
                string DeficiencyID = Request.Params["DeficiencyID"];

                List<DeficiencyAttachmentList> objDeficiencyAttachmentList = new List<DeficiencyAttachmentList>();
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbRegulatory = new AizantIT_DevEntities())
                {
                    var objDefResultList = dbRegulatory.AizantIT_SP_DeficiencyAttachmentList(
                               displayLength,
                               displayStart,
                               sortCol,
                               sSortDir,
                               sSearch != null ? sSearch.Trim() : "",
                               Convert.ToInt32(DeficiencyID));

                    foreach (var item in objDefResultList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objDeficiencyAttachmentList.Add(new DeficiencyAttachmentList(Convert.ToInt32(item.AttachmentID), item.FileName,
                             item.FileExtention, item.AttachmentDate));
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objDeficiencyAttachmentList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        private string GetOnlyFileExtension(string fileName)
        {
            return Path.GetExtension(fileName).Remove(0, 1);
        }
        private string GetActualFileName(string fileName)
        {
            return Path.GetFileName(fileName).Replace(Path.GetExtension(fileName), "");
        }
        public JsonResult DeleteDefAttachment(int AttachmentID)
        {
            try
            {
                var dbRegulatory = new AizantIT_DevEntities();
                using (DbContextTransaction objtransaction = dbRegulatory.Database.BeginTransaction())
                {
                    try
                    {
                        AizantIT_DeficiencyAttachment objDefAttachment = (from item in dbRegulatory.AizantIT_DeficiencyAttachment
                                                                          where item.DeficiencyAttachmentID == AttachmentID
                                                                          select item).Single();

                        int DeficiencyID = objDefAttachment.DeficiencyID;
                        string fileName = objDefAttachment.FileName;

                        dbRegulatory.AizantIT_DeficiencyAttachment.Remove(objDefAttachment);
                        dbRegulatory.SaveChanges();
                        objtransaction.Commit();
                        string message = fileName;
                        return Json(new { msg = message + " Removed", msgType = "success" }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        objtransaction.Rollback();
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = LineNo + " :" + HelpClass.SQLEscapeString(ex.Message);
                return Json("error: " + HelpClass.SQLEscapeString(ex.Message), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        public ActionResult GetProductDetailsList(int ListType, string IsJqureyDatatable)
        {
            if (IsJqureyDatatable == "N")
            {
                return DownloadProductListReport(ListType, IsJqureyDatatable);
            }
            else
            {
                ProductDetailsMainListResult objProductDetailsMainListResult = new ProductDetailsMainListResult();

                objProductDetailsMainListResult.displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                objProductDetailsMainListResult.displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                objProductDetailsMainListResult.sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
                objProductDetailsMainListResult.sSortDir = Request.Params["sSortDir_0"];
                objProductDetailsMainListResult.sSearch = Request.Params["sSearch"];

                objProductDetailsMainListResult.sSearch_ProductName = Request.Params["sSearch_2"];
                objProductDetailsMainListResult.sSearch_ApplicationNumber = Request.Params["sSearch_3"];
                objProductDetailsMainListResult.sSearch_SubmissionDate = Request.Params["sSearch_4"];
                objProductDetailsMainListResult.sSearch_ReviewAcceptedDate = Request.Params["sSearch_5"];
                objProductDetailsMainListResult.sSearch_ApplicantName = Request.Params["sSearch_6"];
                objProductDetailsMainListResult.sSearch_ClientName = Request.Params["sSearch_7"];
                objProductDetailsMainListResult.sSearch_CurrentStatus = Request.Params["sSearch_8"];
                objProductDetailsMainListResult.sSearch_GoalDate = Request.Params["sSearch_9"];
                objProductDetailsMainListResult.sSearch_DossierStatus = Request.Params["sSearch_10"];
                objProductDetailsMainListResult.sSearch_ApprovedDate = Request.Params["sSearch_11"];

                objProductDetailsMainListResult.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                Session["ProductDetailsList"] = objProductDetailsMainListResult;

                List<ProductDetailsList> objDossierList = new List<ProductDetailsList>();
                try
                {
                    int totalRecordsCount = 0;
                    using (AizantIT_DevEntities dbRegulatory = new AizantIT_DevEntities())
                    {
                        var DossierList = GetProductDetailsMainList(ListType, IsJqureyDatatable);
                        foreach (var item in DossierList)
                        {
                            if (totalRecordsCount == 0)
                            {
                                totalRecordsCount = Convert.ToInt32(item.TotalCount);
                            }
                            objDossierList.Add(new ProductDetailsList(Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.DossierID), item.ProductName,
                                               item.ApplicationNumber, item.SubmissionDate.ToString(), item.ReviewAcceptedDate.ToString(),
                                               item.ApplicantName, item.ClientName, item.CurrentStatus, item.DossierStatus, item.GoalDate.ToString(),
                                               item.ApprovedDate.ToString(), Convert.ToInt32(item.DossierCurrentStatusID),
                                               Convert.ToInt32(item.AthorEmpID), Convert.ToInt32(item.ApproverEmpID)));
                        }
                    }
                    return Json(new
                    {
                        iTotalRecords = totalRecordsCount,
                        iTotalDisplayRecords = totalRecordsCount,
                        aaData = objDossierList
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ObjectResult<AizantIT_SP_GetProductDetailsList_Result> GetProductDetailsMainList(int ListType, string IsJqureyDatatable)
        {
            try
            {
                ProductDetailsMainListResult objProductDetailsMainListResult = (ProductDetailsMainListResult)Session["ProductDetailsList"];
                AizantIT_DevEntities dbRegulatory = new AizantIT_DevEntities();

                var DossierList = dbRegulatory.AizantIT_SP_GetProductDetailsList(
                                  objProductDetailsMainListResult.displayLength,
                                  objProductDetailsMainListResult.displayStart,
                                  objProductDetailsMainListResult.sortCol,
                                  objProductDetailsMainListResult.sSortDir,
                                  objProductDetailsMainListResult.sSearch != null ? objProductDetailsMainListResult.sSearch.Trim() : "",
                                  objProductDetailsMainListResult.sSearch_ProductName,
                                  objProductDetailsMainListResult.sSearch_ApplicationNumber,
                                  objProductDetailsMainListResult.sSearch_SubmissionDate,
                                  objProductDetailsMainListResult.sSearch_ReviewAcceptedDate,
                                  objProductDetailsMainListResult.sSearch_ApplicantName,
                                  objProductDetailsMainListResult.sSearch_ClientName,
                                  objProductDetailsMainListResult.sSearch_CurrentStatus,
                                  objProductDetailsMainListResult.sSearch_DossierStatus,
                                  objProductDetailsMainListResult.sSearch_GoalDate,
                                  objProductDetailsMainListResult.sSearch_ApprovedDate,
                                  objProductDetailsMainListResult.EmpID,
                                  ListType,
                                  IsJqureyDatatable
                                  );
                return DossierList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region Deficiency
        public ActionResult DeficiencyList(int DossierID = 0)
        {
            try
            {
                return PartialView("_DeficiencyList");
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetDeficiencyList(int ShowType, int ProductDossierId = 0)
        {
            int EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            List<DeficiencyList> objDeficiencyList = new List<DeficiencyList>();
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbRegulatory = new AizantIT_DevEntities())
                {
                    var DeficiencyList = dbRegulatory.AizantIT_SP_GetDeficiencyList(
                               displayLength,
                               displayStart,
                               sortCol,
                               sSortDir,
                               sSearch != null ? sSearch.Trim() : "", ProductDossierId, ShowType, EmpID
                               );
                    foreach (var item in DeficiencyList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objDeficiencyList.Add(new DeficiencyList(Convert.ToInt32(item.RowNumber),
                            Convert.ToInt32(item.DeficiencyID), item.TypeofDeficiency, item.DeficiencyCode,
                            item.DeficiencyReceiptDate, item.DueDate.ToString(), item.DeficiencyStatus,
                            item.DeficiencyCurrentStatus, Convert.ToInt32(item.ProductDossierID), Convert.ToInt32(item.CurrentStatusID), Convert.ToInt32(item.AuthorID), Convert.ToInt32(item.DossierCurrentStatusID),
                            Convert.ToChar(item.DossierCreatedAs), item.ApplicationNumber, item.ProductName, Convert.ToInt32(item.ApproverID)));
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objDeficiencyList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetObservationList(int ShowType, int ObservationID = 0, int DeficiencyID = 0, int ProductDossierID = 0, string IsJqueryDataTable = "Y")
        {
            if (IsJqueryDataTable == "N")
            {
                return DownloadObservationReport(ShowType, ObservationID, DeficiencyID, ProductDossierID, IsJqueryDataTable);
            }
            else
            {
                ObservationMainListResult objObservationMainListResult = new ObservationMainListResult();
                objObservationMainListResult.EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                objObservationMainListResult.displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                objObservationMainListResult.displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                objObservationMainListResult.sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
                objObservationMainListResult.sSortDir = Request.Params["sSortDir_0"];
                objObservationMainListResult.sSearch = Request.Params["sSearch"];

                objObservationMainListResult.sSearch_ApplicationNumber = Request.Params["sSearch_2"];
                objObservationMainListResult.sSearch_ProductName = Request.Params["sSearch_3"];
                objObservationMainListResult.sSearch_FunctionalArea = Request.Params["sSearch_4"];
                objObservationMainListResult.sSearch_DepartmentName = Request.Params["sSearch_5"];
                objObservationMainListResult.sSearch_ResponsiblePerson = Request.Params["sSearch_6"];
                objObservationMainListResult.sSearch_DueDate = Request.Params["sSearch_7"];
                objObservationMainListResult.sSearch_Market = Request.Params["sSearch_8"];
                objObservationMainListResult.sSearch_Client = Request.Params["sSearch_9"];
                objObservationMainListResult.sSearch_Applicant = Request.Params["sSearch_10"];
                objObservationMainListResult.sSearch_ObservationStatus = Request.Params["sSearch_11"];
                objObservationMainListResult.sSearch_ObservationCurrentStatus = Request.Params["sSearch_12"];

                Session["RegulatoryObservationList"] = objObservationMainListResult;

                List<ObservationList> objObservationList = new List<ObservationList>();
                try
                {
                    int totalRecordsCount = 0;

                    var ObservationMainList = GetObservationMainListData(ShowType, ObservationID, DeficiencyID, ProductDossierID, IsJqueryDataTable);

                    foreach (var item in ObservationMainList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objObservationList.Add(new ObservationList(Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.ObservationID), item.FunctionalArea,
                        item.DepartmentName, item.ResponsiblePerson, item.DueDate, Convert.ToInt32(item.ProductDossierID), Convert.ToInt32(item.DeficiencyID), Convert.ToInt32(item.ObservationCurrentStatusID), Convert.ToInt32(item.ResponsiblePersonEmpID),
                        Convert.ToInt32(item.DeficiencyCurrentStatusID), Convert.ToChar(item.DossierCreatedAs), Convert.ToInt32(item.DossierCurrentStatusID),
                        item.Market, item.Client, item.Applicant, item.ObservationStatus, item.ObservationCurrentStatus, item.ApplicationNumber, item.ProductName));
                    }
                    return Json(new
                    {
                        iTotalRecords = totalRecordsCount,
                        iTotalDisplayRecords = totalRecordsCount,
                        aaData = objObservationList
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ObjectResult<AizantIT_SP_GetObservationList_Result> GetObservationMainListData(int ShowType, int ObservationID = 0, int DeficiencyID = 0, int ProductDossierID = 0, string IsJqueryDataTable = "Y")
        {
            try
            {
                ObservationMainListResult objObservationMainListResult = (ObservationMainListResult)Session["RegulatoryObservationList"];
                AizantIT_DevEntities dbRegulatory = new AizantIT_DevEntities();

                var ObservationMainList = dbRegulatory.AizantIT_SP_GetObservationList(
                                objObservationMainListResult.displayLength,
                                objObservationMainListResult.displayStart,
                                objObservationMainListResult.sortCol,
                                objObservationMainListResult.sSortDir,
                                objObservationMainListResult.sSearch != null ? objObservationMainListResult.sSearch.Trim() : "", ObservationID, ProductDossierID, DeficiencyID, ShowType, objObservationMainListResult.EmpID, IsJqueryDataTable,
                                objObservationMainListResult.sSearch_FunctionalArea, objObservationMainListResult.sSearch_DepartmentName,
                                objObservationMainListResult.sSearch_ResponsiblePerson,
                                objObservationMainListResult.sSearch_ObservationStatus != null ? objObservationMainListResult.sSearch_ObservationStatus : "",
                                objObservationMainListResult.sSearch_ObservationCurrentStatus != null ? objObservationMainListResult.sSearch_ObservationCurrentStatus : "",
                                objObservationMainListResult.sSearch_DueDate,
                                objObservationMainListResult.sSearch_Market, objObservationMainListResult.sSearch_Client,
                                objObservationMainListResult.sSearch_Applicant, objObservationMainListResult.sSearch_ApplicationNumber,
                                objObservationMainListResult.sSearch_ProductName
                                );
                return ObservationMainList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult CreateDeficiency(CreateDeficiency objCreateDeficiency)
        {
            try
            {
                using (var dbContext = new AizantIT_DevEntities())
                {
                    var ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);

                    if (dbContext.AizantIT_ProductDossier.Where(a => a.ProductDossierID == objCreateDeficiency.ProductDossierID && a.AuthorEmpID == ActionBy).Any())
                    {
                        return CreateDef(objCreateDeficiency, dbContext, ActionBy);
                    }
                    else if (dbContext.AizantIT_ProductDossier.Where(a => a.ProductDossierID == objCreateDeficiency.ProductDossierID && a.DossierStatus == "A" && a.CurrentStatus == 8).Any())
                    {
                        return CreateDef(objCreateDeficiency, dbContext, ActionBy);
                    }
                    else
                    {
                        return Json(new { msgType = "sysError", msg = RegulatoryValidationMessages.RA_ErrorMsg1 }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        private JsonResult CreateDef(CreateDeficiency objCreateDeficiency, AizantIT_DevEntities dbContext, int ActionBy)
        {
            var DeficiencyCodeExist = dbContext.AizantIT_DeficiencyMaster.Where(a => a.ProductDossierID == objCreateDeficiency.ProductDossierID && a.DeficiencyCode == objCreateDeficiency.DeficiencyCode).Any();

            if (!DeficiencyCodeExist)
            {
                using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                {
                    try
                    {
                        AizantIT_DeficiencyMaster objDeficiencyMaster = new AizantIT_DeficiencyMaster();
                        objDeficiencyMaster.ProductDossierID = objCreateDeficiency.ProductDossierID;
                        objDeficiencyMaster.DeficiencyCode = objCreateDeficiency.DeficiencyCode;
                        objDeficiencyMaster.TypeofDeficiencyID = objCreateDeficiency.TypeOfDeficiencyID;
                        objDeficiencyMaster.DeficiencyReceiptDate = objCreateDeficiency.DeficiencyReceiptDate;
                        objDeficiencyMaster.DueDate = objCreateDeficiency.DueDate;
                        objDeficiencyMaster.DeficiencyStatus = objCreateDeficiency.DeficiencyStatus;
                        objDeficiencyMaster.ClosedDate = objCreateDeficiency.ClosedDate;
                        objDeficiencyMaster.ApproverID = objCreateDeficiency.ApproverID;
                        objDeficiencyMaster.Remarks = objCreateDeficiency.Remarks;
                        objDeficiencyMaster.CurrentStatus = Convert.ToInt32(RA_DeficiencyHistoryStatus.Deficiency_Created);
                        objDeficiencyMaster.AuthorID = ActionBy;
                        dbContext.AizantIT_DeficiencyMaster.Add(objDeficiencyMaster);
                        dbContext.SaveChanges();
                        int DeficiencyID = objDeficiencyMaster.DeficiencyID;

                        DeficiencyHistoryInsert(dbContext, DeficiencyID, ActionBy, Convert.ToInt32(RA_UserRole.RA_Lead), Convert.ToInt32(RA_DeficiencyHistoryStatus.Deficiency_Created), objCreateDeficiency.Remarks);
                        transaction.Commit();
                        return Json(new { msgType = "success", _DeficiencyID = DeficiencyID }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
            else
            {
                return Json(new { msgType = "error", msg = RegulatoryValidationMessages.RA_ErrorMsg4 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdateDeficiency(CreateDeficiency objUpdateDeficiency)
        {
            try
            {
                var ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);

                if (new AizantIT_DevEntities().AizantIT_DeficiencyMaster.Where(a => a.DeficiencyID == objUpdateDeficiency.DeficiencyID
                                    && a.AuthorID == ActionBy).Any())
                {
                    if (!ApproverModificationinDeficiency(objUpdateDeficiency.DeficiencyID, objUpdateDeficiency.ApproverID))
                    {
                        int ShowType = 12;
                        using (var dbContext = new AizantIT_DevEntities())
                        {
                            var ProductDetails = (from prod in dbContext.AizantIT_ProductDossier
                                                  where prod.ProductDossierID == objUpdateDeficiency.ProductDossierID
                                                  select new { prod.ApplicationNum }).SingleOrDefault();

                            var DeficiencyCodeExist = dbContext.AizantIT_DeficiencyMaster.Where(a => a.ProductDossierID == objUpdateDeficiency.ProductDossierID && a.DeficiencyCode == objUpdateDeficiency.DeficiencyCode && a.DeficiencyID != objUpdateDeficiency.DeficiencyID).Any();
                            if (!DeficiencyCodeExist)
                            {
                                // Check whether Observation Due date are within Deficiency Due date & Receipt Date
                                //var NotInDeficiencyDates = false;
                                //dbContext.AizantIT_DF_Observation.Where(p => ((p.DueDate > objUpdateDeficiency.DueDate)
                                //    || (p.DueDate < objUpdateDeficiency.DeficiencyReceiptDate)) && p.DeficiencyID == objUpdateDeficiency.DeficiencyID).Any();
                                //if (!NotInDeficiencyDates)
                                //{
                                using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                                {
                                    try
                                    {
                                        var DeficiencyMaster = dbContext.AizantIT_DeficiencyMaster.Where(a => a.DeficiencyID == objUpdateDeficiency.DeficiencyID).SingleOrDefault();
                                        DeficiencyMaster.DeficiencyCode = objUpdateDeficiency.DeficiencyCode;
                                        DeficiencyMaster.TypeofDeficiencyID = objUpdateDeficiency.TypeOfDeficiencyID;
                                        DeficiencyMaster.DeficiencyReceiptDate = objUpdateDeficiency.DeficiencyReceiptDate;
                                        DeficiencyMaster.DueDate = objUpdateDeficiency.DueDate;
                                        DeficiencyMaster.DeficiencyStatus = objUpdateDeficiency.DeficiencyStatus;//Open
                                        DeficiencyMaster.ClosedDate = getDateValue(objUpdateDeficiency.ClosedDate.ToString());
                                        DeficiencyMaster.ApproverID = objUpdateDeficiency.ApproverID;
                                        DeficiencyMaster.Remarks = objUpdateDeficiency.Remarks;
                                        if (objUpdateDeficiency.DossierStatus == "U" || (objUpdateDeficiency.DossierStatus == "A" && objUpdateDeficiency.DeficiencyStatus == 1))
                                        {
                                            DeficiencyMaster.CurrentStatus = Convert.ToInt32(RA_DeficiencyHistoryStatus.Submitted_for_Approval);
                                        }
                                        else
                                        {
                                            DeficiencyMaster.CurrentStatus = Convert.ToInt32(RA_DeficiencyHistoryStatus.Deficiency_Modified);
                                        }
                                        DeficiencyMaster.AuthorID = ActionBy;
                                        dbContext.SaveChanges();

                                        if (objUpdateDeficiency.DossierStatus == "U" || (objUpdateDeficiency.DossierStatus == "A" && objUpdateDeficiency.DeficiencyStatus == 1))
                                        {
                                            //Notification
                                            NotificationActionByLinq objNotifications = new NotificationActionByLinq();
                                            NotificationBO objNotificationsBO = new NotificationBO();
                                            objNotificationsBO.NotificationID = GetDeficiencyNotificationID(objUpdateDeficiency.DeficiencyID);
                                            objNotificationsBO.Description = string.Format("Deficiency has been Modified with Deficiency Code No: '{0}' Product Application No :'{1}'", objUpdateDeficiency.DeficiencyCode, ProductDetails.ApplicationNum);
                                            objNotificationsBO.ModuleID = Convert.ToInt32(Modules.RA);
                                            objNotificationsBO.NotificationLink = "/Regulatory/RegulatoryMain/DeficiencyMainList?ShowType=" + ShowType + "&DeficiencyID=" + DeficiencyMaster.DeficiencyID + "&ProductDossierID=" + DeficiencyMaster.ProductDossierID;
                                            List<NotificationRoleIDandDeptID> NotificationRoleIDSandDeptIDs = new List<NotificationRoleIDandDeptID>();

                                            NotificationRoleIDSandDeptIDs.Add(new NotificationRoleIDandDeptID
                                            {
                                                RoleID = Convert.ToInt32(RA_UserRole.RA_Approver),
                                                DeptID = 0
                                            });
                                            objNotificationsBO.NotificationRoleIDSandDeptIDs = NotificationRoleIDSandDeptIDs;
                                            List<int> objNotificationEmpIDs = new List<int>();
                                            objNotificationEmpIDs.Add(objUpdateDeficiency.ApproverID);
                                            objNotificationsBO.NotificationEmpIDS = objNotificationEmpIDs;
                                            objNotifications.NotificationToNextEmp(objNotificationsBO);

                                            DeficiencyHistoryInsert(dbContext, objUpdateDeficiency.DeficiencyID, ActionBy, Convert.ToInt32(RA_UserRole.RA_Lead), Convert.ToInt32(RA_DeficiencyHistoryStatus.Deficiency_Modified), objUpdateDeficiency.Remarks);
                                            DeficiencyHistoryInsert(dbContext, objUpdateDeficiency.DeficiencyID, ActionBy, Convert.ToInt32(RA_UserRole.RA_Lead), Convert.ToInt32(RA_DeficiencyHistoryStatus.Submitted_for_Approval), "");
                                        }
                                        else
                                        {
                                            DeficiencyHistoryInsert(dbContext, objUpdateDeficiency.DeficiencyID, ActionBy, Convert.ToInt32(RA_UserRole.RA_Lead), Convert.ToInt32(RA_DeficiencyHistoryStatus.Deficiency_Modified), objUpdateDeficiency.Remarks);
                                        }
                                        transaction.Commit();

                                        return Json(new { msgType = "success" }, JsonRequestBehavior.AllowGet);
                                    }
                                    catch (Exception ex)
                                    {
                                        transaction.Rollback();
                                        throw ex;
                                    }
                                }
                            }
                            else
                            {
                                return Json(new { msgType = "error", msg = RegulatoryValidationMessages.RA_ErrorMsg5 }, JsonRequestBehavior.AllowGet);
                            }
                            //}
                            //else
                            //{
                            //    return Json(new { msgType = "error", msg = "Deficiency Code Already Exists." }, JsonRequestBehavior.AllowGet);
                            //}
                        }
                    }
                    else
                    {
                        return Json(new { msgType = "error", msg = RegulatoryValidationMessages.RA_ErrorMsg6 }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { msgType = "sysError", msg = RegulatoryValidationMessages.RA_ErrorMsg1 }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "sysError" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult CreateObservation(CreateObservation objCreateObservation)
        {
            try
            {
                using (var dbContext = new AizantIT_DevEntities())
                {
                    var ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    if (dbContext.AizantIT_DeficiencyMaster.Where(a => a.DeficiencyID == objCreateObservation.DeficiencyID && a.AuthorID == ActionBy).Any())
                    {
                        using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                        {
                            try
                            {
                                AizantIT_DF_Observation objObservation = new AizantIT_DF_Observation();
                                objObservation.DeficiencyID = objCreateObservation.DeficiencyID;
                                objObservation.FunctionalAreaID = objCreateObservation.FunctionalAreaID;
                                objObservation.DepartmentID = objCreateObservation.DepartmentID;
                                objObservation.ObservationDescription = objCreateObservation.ObservationDescription.Trim();
                                objObservation.ResponsiblePersonEmpID = objCreateObservation.ResponsiblePersonEmpID;
                                objObservation.Status = objCreateObservation.Status;
                                // objObservation.DueDate = objCreateObservation.DueDate;
                                objObservation.CurrentStatus = Convert.ToInt32(RA_ObservationHistoryStatus.Observation_Created);
                                dbContext.AizantIT_DF_Observation.Add(objObservation);
                                dbContext.SaveChanges();

                                ObservationHistoryInsert(dbContext, objObservation.DF_ObservationID, ActionBy, Convert.ToInt32(RA_UserRole.RA_Lead), Convert.ToInt32(RA_ObservationHistoryStatus.Observation_Created), objCreateObservation.Comments);
                                transaction.Commit();

                                var ObservationCount = DeficiencyWiseObservationChecking(dbContext, objCreateObservation.DeficiencyID);
                                return Json(new { msgType = "success", _ObservationCount = ObservationCount }, JsonRequestBehavior.AllowGet);
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                throw ex;
                            }
                        }
                    }
                    else
                    {
                        return Json(new { msg = RegulatoryValidationMessages.RA_ErrorMsg1, msgType = "error" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult UpdateObservation(CreateObservation objUpdateObservation)
        {
            try
            {
                using (var dbContext = new AizantIT_DevEntities())
                {
                    var ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);

                    if (dbContext.AizantIT_DeficiencyMaster.Where(a => a.DeficiencyID == objUpdateObservation.DeficiencyID && a.AuthorID == ActionBy).Any())
                    {
                        using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                        {
                            try
                            {
                                var objObservation = dbContext.AizantIT_DF_Observation.Where(a => a.DF_ObservationID == objUpdateObservation.DF_ObservationID).SingleOrDefault();
                                objObservation.DF_ObservationID = objUpdateObservation.DF_ObservationID;
                                objObservation.FunctionalAreaID = objUpdateObservation.FunctionalAreaID;
                                objObservation.DepartmentID = objUpdateObservation.DepartmentID;
                                objObservation.ObservationDescription = objUpdateObservation.ObservationDescription.Trim();
                                objObservation.ResponsiblePersonEmpID = objUpdateObservation.ResponsiblePersonEmpID;
                                objObservation.Status = objUpdateObservation.Status;
                                // objObservation.DueDate = objUpdateObservation.DueDate;
                                dbContext.SaveChanges();
                                if (objUpdateObservation.DeficiencyCurrentStatus == 4)
                                {
                                    ObservationHistoryInsert(dbContext, objObservation.DF_ObservationID, ActionBy, Convert.ToInt32(RA_UserRole.RA_Lead), Convert.ToInt32(RA_ObservationHistoryStatus.Observation_Modified), objUpdateObservation.Comments);
                                }
                                transaction.Commit();

                                return Json(new { msgType = "success", }, JsonRequestBehavior.AllowGet);
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                throw ex;
                            }
                        }
                    }
                    else
                    {
                        return Json(new { msg = RegulatoryValidationMessages.RA_ErrorMsg1, msgType = "error" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult DeleteObservation(int ObservationID, int DeficiencyID)
        {
            try
            {
                var ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                using (var dbContext = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction objtransction = dbContext.Database.BeginTransaction())
                    {
                        try
                        {
                            var DeficiencyObserCount = dbContext.AizantIT_DF_Observation.Where(D => D.DeficiencyID == DeficiencyID).Count();
                            if (DeficiencyObserCount == 1)
                            {
                                return Json(new { msgType = "warning" }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                var ObservationHistoryRecord = dbContext.AizantIT_DF_ObservationHistory.Where(c => c.BaseID == ObservationID).ToList();
                                dbContext.AizantIT_DF_ObservationHistory.RemoveRange(ObservationHistoryRecord);
                                dbContext.SaveChanges();

                                var ObservationRecord = dbContext.AizantIT_DF_Observation.Where(c => c.DF_ObservationID == ObservationID).SingleOrDefault();
                                dbContext.AizantIT_DF_Observation.Remove(ObservationRecord);
                                dbContext.SaveChanges();
                                objtransction.Commit();
                                return Json(new { msgType = "success" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            objtransction.Rollback();
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        //DropDown List's
        public List<SelectListItem> TypeofDeficiency()
        {
            var dbContext = new AizantIT_DevEntities();
            List<SelectListItem> TypeofDeficiencyLists = new List<SelectListItem>();
            var Details = dbContext.AizantIT_TypeOfDeficiency.Where(a => a.IsActive == true).ToList();
            TypeofDeficiencyLists.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var Item in Details)
            {
                TypeofDeficiencyLists.Add(new SelectListItem
                {
                    Text = Item.TypeOfDeficiencyName,
                    Value = Item.TypeOfDeficiencyID.ToString(),
                });
            }
            return TypeofDeficiencyLists;
        }
        public List<SelectListItem> FunctionalAreaList()
        {
            var dbContext = new AizantIT_DevEntities();
            List<SelectListItem> FunctionalAreaList = new List<SelectListItem>();
            var Details = dbContext.AizantIT_FunctionalArea.Where(a => a.IsActive == true).ToList();
            FunctionalAreaList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var Item in Details)
            {
                FunctionalAreaList.Add(new SelectListItem
                {
                    Text = Item.FunctionalAreaName,
                    Value = Item.FunctionalAreaID.ToString(),
                });
            }
            return FunctionalAreaList;
        }
        public ActionResult GetDossierHistoryList(int BaseID = 0, int TableID = 0)
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            List<RegulatoryMasterHistoryList> objProductDossierHistoryList = new List<RegulatoryMasterHistoryList>();
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbRegulatory = new AizantIT_DevEntities())
                {
                    var TypeOfDeficiencyList = dbRegulatory.AizantIT_SP_RegulatoryHistory(
                               displayLength,
                               displayStart,
                               sortCol,
                               sSortDir,
                               sSearch != null ? sSearch.Trim() : "", TableID, BaseID
                               );
                    foreach (var item in TypeOfDeficiencyList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objProductDossierHistoryList.Add(new RegulatoryMasterHistoryList(Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.ActionHistoryID), item.ActionBy, item.ActionRole, item.ActionDate, item.ActionStatus, item.Remarks));
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objProductDossierHistoryList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public void ProductDossierHistoryInsert(AizantIT_DevEntities dbContext, int ProductDossierId, int ActionBy, int ActionRole, int ActionStatus, string Comments = "")
        {
            AizantIT_DossierMasterHistory objDossierMasterHistory = new AizantIT_DossierMasterHistory();
            objDossierMasterHistory.BaseID = ProductDossierId;
            objDossierMasterHistory.ActionBy = ActionBy;
            objDossierMasterHistory.RoleID = ActionRole;
            objDossierMasterHistory.ActionDate = DateTime.Now;
            objDossierMasterHistory.ActionStatus = ActionStatus;
            objDossierMasterHistory.Comments = Comments;
            dbContext.AizantIT_DossierMasterHistory.Add(objDossierMasterHistory);
            dbContext.SaveChanges();
        }
        public void DeficiencyHistoryInsert(AizantIT_DevEntities dbContext, int DeficiencyID,
            int? ActionBy, int? ActionRole, int ActionStatus, string Comments = "")
        {
            AizantIT_DeficiencyHistory objDeficiencyHistory = new AizantIT_DeficiencyHistory();
            objDeficiencyHistory.BaseID = DeficiencyID;
            objDeficiencyHistory.ActionBy = ActionBy;
            objDeficiencyHistory.RoleID = ActionRole;
            objDeficiencyHistory.ActionDate = DateTime.Now;
            objDeficiencyHistory.ActionStatus = ActionStatus;
            objDeficiencyHistory.Comments = Comments;
            dbContext.AizantIT_DeficiencyHistory.Add(objDeficiencyHistory);
            dbContext.SaveChanges();
        }
        public void ObservationHistoryInsert(AizantIT_DevEntities dbContext, int ObservationID,
            int? ActionBy, int? ActionRole, int ActionStatus, string Comments = "", bool IsActionDateNull = false)
        {
            AizantIT_DF_ObservationHistory objObservationHistory = new AizantIT_DF_ObservationHistory();
            objObservationHistory.BaseID = ObservationID;
            objObservationHistory.ActionBy = ActionBy;
            if (IsActionDateNull)
            {
                objObservationHistory.ActionDate = null;
            }
            else
            {
                objObservationHistory.ActionDate = DateTime.Now;
            }
            objObservationHistory.ActionStatus = ActionStatus;//Observation Initiated
            objObservationHistory.RoleID = ActionRole;
            objObservationHistory.Comments = Comments;
            dbContext.AizantIT_DF_ObservationHistory.Add(objObservationHistory);
            dbContext.SaveChanges();
        }
        public ActionResult DossierApproval(ViewDossierModel objDossier)
        {
            int EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            using (AizantIT_DevEntities dbRegulatory = new AizantIT_DevEntities())
            {
                using (DbContextTransaction transaction = dbRegulatory.Database.BeginTransaction())
                {
                    try
                    {
                        AizantIT_ProductDossier objAizantIT_productDossier = new AizantIT_ProductDossier();
                        objAizantIT_productDossier = dbRegulatory.AizantIT_ProductDossier.SingleOrDefault(x => x.ProductDossierID == objDossier.ProductDossierID);

                        if (objAizantIT_productDossier.ApproverEmpID == EmpID)
                        {
                            if (objDossier.CurrentStatus == 4)
                            {
                                NotificationActionByLinq objNotifications = new NotificationActionByLinq();
                                NotificationBO objNotificationsBO = new NotificationBO();
                                objNotificationsBO.NotificationID = GetDossierNotificationID(objAizantIT_productDossier.ProductDossierID);
                                objNotificationsBO.Description = string.Format("Product has been Reverted with Application No: '{0}' of Dossier Status '{1}'", objAizantIT_productDossier.ApplicationNum, (objAizantIT_productDossier.DossierStatus == "U" ? "Under Review" : "Approved"));
                                objNotificationsBO.ModuleID = Convert.ToInt32(Modules.RA);
                                objNotificationsBO.NotificationLink = "/Regulatory/RegulatoryMain/CreateDossierMain?ProductDossierID=" + objAizantIT_productDossier.ProductDossierID;
                                List<NotificationRoleIDandDeptID> NotificationRoleIDSandDeptIDs = new List<NotificationRoleIDandDeptID>();
                                NotificationRoleIDSandDeptIDs.Add(new NotificationRoleIDandDeptID
                                {
                                    RoleID = Convert.ToInt32(RA_UserRole.RA_Lead),
                                    DeptID = 0
                                });
                                objNotificationsBO.NotificationRoleIDSandDeptIDs = NotificationRoleIDSandDeptIDs;
                                List<int> objNotificationEmpIDs = new List<int>();
                                objNotificationEmpIDs.Add(objAizantIT_productDossier.AuthorEmpID);
                                objNotificationsBO.NotificationEmpIDS = objNotificationEmpIDs;
                                objNotifications.NotificationToNextEmp(objNotificationsBO);

                                objAizantIT_productDossier.CurrentStatus = Convert.ToInt32(RA_DossierHistoryStatus.Reverted_Product);
                                dbRegulatory.SaveChanges();
                                ProductDossierHistoryInsert(dbRegulatory, objDossier.ProductDossierID, EmpID, Convert.ToInt32(RA_UserRole.RA_Approver), Convert.ToInt32(RA_DossierHistoryStatus.Reverted_Product), objDossier.Comments);
                                transaction.Commit();
                            }
                            else
                            {
                                if (objAizantIT_productDossier.DossierStatus == "U")
                                {
                                    int ListType = 9;
                                    NotificationActionByLinq objNotifications = new NotificationActionByLinq();
                                    NotificationBO objNotificationsBO = new NotificationBO();
                                    objNotificationsBO.NotificationID = GetDossierNotificationID(objAizantIT_productDossier.ProductDossierID);
                                    objNotificationsBO.Description = string.Format("Product has been Approved By RA Approver with Application No: '{0}' of Dossier Status '{1}'", objAizantIT_productDossier.ApplicationNum, (objAizantIT_productDossier.DossierStatus == "U" ? "Under Review" : "Approved"));
                                    objNotificationsBO.ModuleID = Convert.ToInt32(Modules.RA);
                                    objNotificationsBO.NotificationLink = "/Regulatory/RegulatoryMain/ViewDossierDetails?DossierID=" + objAizantIT_productDossier.ProductDossierID + "&ListType=" + ListType;
                                    List<NotificationRoleIDandDeptID> NotificationRoleIDSandDeptIDs = new List<NotificationRoleIDandDeptID>();
                                    NotificationRoleIDSandDeptIDs.Add(new NotificationRoleIDandDeptID
                                    {
                                        RoleID = Convert.ToInt32(RA_UserRole.RA_Lead),
                                        DeptID = 0
                                    });
                                    objNotificationsBO.NotificationRoleIDSandDeptIDs = NotificationRoleIDSandDeptIDs;
                                    List<int> objNotificationEmpIDs = new List<int>();
                                    objNotificationEmpIDs.Add(objAizantIT_productDossier.AuthorEmpID);
                                    objNotificationsBO.NotificationEmpIDS = objNotificationEmpIDs;
                                    objNotifications.NotificationToNextEmp(objNotificationsBO);

                                    objAizantIT_productDossier.CurrentStatus = Convert.ToInt32(RA_DossierHistoryStatus.Product_Approved_by_RA_Approver);//Current Status Inserted
                                    dbRegulatory.SaveChanges();

                                    ProductDossierHistoryInsert(dbRegulatory, objDossier.ProductDossierID, EmpID, Convert.ToInt32(RA_UserRole.RA_Approver), Convert.ToInt32(RA_DossierHistoryStatus.Product_Approved_by_RA_Approver), objDossier.Comments);
                                    transaction.Commit();
                                }
                                else
                                {
                                    int Get_NotificationID = GetDossierNotificationID(objAizantIT_productDossier.ProductDossierID);
                                    int[] NotificationIDArray = { Get_NotificationID };
                                    var objDossierNotifyLink = dbRegulatory.AizantIT_DossierNotifyLink.Where(p => p.NotificationID == Get_NotificationID).SingleOrDefault();
                                    dbRegulatory.AizantIT_DossierNotifyLink.Remove(objDossierNotifyLink);
                                    NotificationActionByLinq objNotifications = new NotificationActionByLinq();

                                    objAizantIT_productDossier.CurrentStatus = Convert.ToInt32(RA_DossierHistoryStatus.Dossier_Completed);
                                    ProductDossierHistoryInsert(dbRegulatory, objDossier.ProductDossierID, EmpID, Convert.ToInt32(RA_UserRole.RA_Approver), Convert.ToInt32(RA_DossierHistoryStatus.Product_Approved_by_RA_Approver), objDossier.Comments);
                                    ProductDossierHistoryInsert(dbRegulatory, objDossier.ProductDossierID, EmpID, Convert.ToInt32(RA_UserRole.RA_Approver), Convert.ToInt32(RA_DossierHistoryStatus.Dossier_Completed));
                                    dbRegulatory.SaveChanges();
                                    var DeficiencyDetails = dbRegulatory.AizantIT_DeficiencyMaster.Where(a => a.ProductDossierID == objAizantIT_productDossier.ProductDossierID).ToList();

                                    foreach (var item in DeficiencyDetails)
                                    {
                                        var DeficiencyData = dbRegulatory.AizantIT_DeficiencyMaster.Where(a => a.DeficiencyID == item.DeficiencyID).SingleOrDefault();
                                        DeficiencyData.CurrentStatus = Convert.ToInt32(RA_DeficiencyHistoryStatus.Deficiency_Completed);
                                        DateTime? ClosedDate = DeficiencyData.ClosedDate;
                                        //Deficiency History
                                        DeficiencyHistoryInsert(dbRegulatory, DeficiencyData.DeficiencyID, null, null, Convert.ToInt32(RA_DeficiencyHistoryStatus.Deficiency_Completed), ClosedDate != null ? "Actual Closed Date of Deficiency on " + (Convert.ToDateTime(ClosedDate)).ToString("dd MMM yyyy") : "N/A");
                                        //Observation 
                                        var ObservationDetails = dbRegulatory.AizantIT_DF_Observation.Where(a => a.DeficiencyID == item.DeficiencyID).ToList();
                                        foreach (var Observation in ObservationDetails)
                                        {
                                            var ObservationData = dbRegulatory.AizantIT_DF_Observation.Where(a => a.DF_ObservationID == Observation.DF_ObservationID).SingleOrDefault();
                                            ObservationData.CurrentStatus = Convert.ToInt32(RA_ObservationHistoryStatus.ObservationCompletionSubmitted);
                                            var responsiblePerson = ObservationData.ResponsiblePersonEmpID;
                                            dbRegulatory.SaveChanges();
                                            //Observation History 
                                            ObservationHistoryInsert(dbRegulatory, Observation.DF_ObservationID, responsiblePerson, Convert.ToInt32(RA_UserRole.RA_HOD),
                                                Convert.ToInt32(RA_ObservationHistoryStatus.ObservationCompletionSubmitted), "", true);
                                        }
                                        dbRegulatory.SaveChanges();
                                    }
                                    transaction.Commit();
                                    objNotifications.DeleteNotification(NotificationIDArray);
                                }
                            }
                        }
                        else
                        {
                            return Json(new { msg = RegulatoryValidationMessages.RA_ErrorMsg1, msgType = "error" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
            return Json(new { msg = RegulatoryValidationMessages.RA_ErrorMsg7, msgType = "success" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetFunctionalAreaDepartmentsList(int FunctionAreaID)
        {
            var dbContext = new AizantIT_DevEntities();
            List<SelectListItem> DepertmentLists = new List<SelectListItem>();
            var DepartmentsList = (from pd in dbContext.AizantIT_DepartmentMaster
                                   join od in dbContext.AizantIT_FunctionalAreaDept on pd.DeptID equals od.DeptID
                                   orderby od.DeptID
                                   where od.FunctionalAreaID == FunctionAreaID
                                   select new
                                   {
                                       pd.DeptID,
                                       pd.DepartmentName
                                   }).ToList();

            DepertmentLists.Add(new SelectListItem
            {
                Text = "--Select--",
                Value = "0"

            });
            foreach (var Item in DepartmentsList)
            {
                DepertmentLists.Add(new SelectListItem
                {
                    Text = Item.DepartmentName,
                    Value = Item.DeptID.ToString(),

                });
            }
            return Json(DepertmentLists, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDepartmentWiseEmployeeList(int DeptID)
        {
            try
            {
                Aizant_Bl objAizant_Bl = new Aizant_Bl();
                var EmployeeList = objAizant_Bl.GetListofEmpByDeptIDRoleIDAndStatus(null, DeptID, Convert.ToInt32(RA_UserRole.RA_HOD), "A", null, "--select--");
                return Json(new { EmployeeList, msgType = "success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult Deficiencydropdownlist()
        {
            var dbContext = new AizantIT_DevEntities();
            Aizant_Bl objAizant_Bl = new Aizant_Bl();
            var TypeOFDeficiency = TypeofDeficiency();
            var Approver = objAizant_Bl.GetListofEmpByRole(null, Convert.ToInt32(RA_UserRole.RA_Approver));
            return Json(new { _TypeOFDeficiency = TypeOFDeficiency, _Approver = Approver }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetFunctionalAreaList()
        {
            var FunctionalArea = FunctionalAreaList();
            return Json(new { _FunctionalArea = FunctionalArea, }, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult DossierHistory(int BaseID, int TableID)
        {
            ViewBag.BaseID = BaseID;
            ViewBag.TableID = TableID;
            return PartialView("_DossierHistory");
        }
        public PartialViewResult _CreateDeficiency(int DeficiencyID = 0)
        {
            ViewDeficiencyModel objEditDeficiencyModel = new ViewDeficiencyModel();
            if (DeficiencyID > 0)
            {
                Aizant_Bl objAizant_Bl = new Aizant_Bl();
                var dbRegulatory = new AizantIT_DevEntities();
                int Notification_ID = GetDeficiencyNotificationID(DeficiencyID);
                if (Notification_ID != 0)
                {
                    UpdateRA_NotificationStatus(Notification_ID, 2);
                }
                var DeficiencyData = (from T1 in dbRegulatory.AizantIT_DeficiencyMaster
                                      join
                                      T2 in dbRegulatory.AizantIT_ProductDossier on T1.ProductDossierID equals T2.ProductDossierID
                                      join T3 in dbRegulatory.AizantIT_TypeOfDeficiency on T1.TypeofDeficiencyID equals T3.TypeOfDeficiencyID
                                      join T4 in dbRegulatory.AizantIT_EmpMaster on T1.ApproverID equals T4.EmpID
                                      where T1.DeficiencyID == DeficiencyID
                                      select new
                                      {
                                          T1.DeficiencyID,
                                          T1.DeficiencyCode,
                                          T1.TypeofDeficiencyID,
                                          T1.DeficiencyReceiptDate,
                                          T1.DueDate,
                                          T1.DeficiencyStatus,
                                          T1.ClosedDate,
                                          T1.ApproverID,
                                          T2.SubmissionDate,
                                          T2.ReviewAcceptedDate,
                                          T2.DossierStatus,
                                          T2.AuthorEmpID,
                                          T1.CurrentStatus,
                                          T2.ApprovedDate,
                                          T3.TypeOfDeficiencyName,
                                          ApproverName = (T4.FirstName + "" + T4.LastName)
                                      }).SingleOrDefault();

                objEditDeficiencyModel.DeficiencyCode = DeficiencyData.DeficiencyCode;
                objEditDeficiencyModel.TypeOfDeficiencyID = DeficiencyData.TypeofDeficiencyID;
                objEditDeficiencyModel.DeficiencyReceiptDate = Convert.ToDateTime(DeficiencyData.DeficiencyReceiptDate).ToString(HelpClass.dateFormat);
                objEditDeficiencyModel.DueDate = Convert.ToDateTime(DeficiencyData.DueDate).ToString(HelpClass.dateFormat);
                objEditDeficiencyModel.DeficiencyStatus = DeficiencyData.DeficiencyStatus == 1 ? "Open" : "Close";
                objEditDeficiencyModel.ClosedDate = DeficiencyData.ClosedDate == null ? "NA" : Convert.ToDateTime(DeficiencyData.ClosedDate).ToString(HelpClass.dateFormat);
                objEditDeficiencyModel.ApproverID = DeficiencyData.ApproverID;
                objEditDeficiencyModel.DeficiencyID = DeficiencyID;
                objEditDeficiencyModel.SubmissionDate = Convert.ToDateTime(DeficiencyData.SubmissionDate).ToString(HelpClass.dateFormat);
                objEditDeficiencyModel.ReviewAcceptedDate = Convert.ToDateTime(DeficiencyData.ReviewAcceptedDate).ToString(HelpClass.dateFormat);
                objEditDeficiencyModel.DossierStatus = DeficiencyData.DossierStatus;
                objEditDeficiencyModel.DeficiencyCurrentStatus = DeficiencyData.CurrentStatus;
                var DeficiencyAttachmentID = dbRegulatory.AizantIT_DeficiencyAttachment.Where(D => D.DeficiencyID == DeficiencyID).Count();
                objEditDeficiencyModel.DeficiencyAttachmentID = DeficiencyAttachmentID;
                objEditDeficiencyModel.AuthorID = DeficiencyData.AuthorEmpID;
                objEditDeficiencyModel.AuthenticationEmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                objEditDeficiencyModel.TypeOfDeficiencyName = DeficiencyData.TypeOfDeficiencyName;
                objEditDeficiencyModel.ApproverName = DeficiencyData.ApproverName;
                objEditDeficiencyModel.ApproveDate = DeficiencyData.ApprovedDate == null ? null : Convert.ToDateTime(DeficiencyData.ApprovedDate).ToString(HelpClass.dateFormat);

                objEditDeficiencyModel.ObservationCount = DeficiencyWiseObservationChecking(dbRegulatory, DeficiencyID);
            }
            return PartialView(objEditDeficiencyModel);
        }
        public PartialViewResult _CreateObservation(int ObservationID)
        {
            ViewObservationModel objEditDeficiencyObserModel = new ViewObservationModel();
            if (ObservationID > 0)
            {
                Aizant_Bl objAizant_Bl = new Aizant_Bl();
                var dbRegulatory = new AizantIT_DevEntities();
                var DeficiencyObservData = dbRegulatory.AizantIT_DF_Observation.Where(a => a.DF_ObservationID == ObservationID).Select(a => new
                {
                    a.DF_ObservationID,
                    a.FunctionalAreaID,
                    a.DepartmentID,
                    a.ResponsiblePersonEmpID,
                    a.ObservationDescription,
                    // a.DueDate,
                    a.Status
                }
                ).SingleOrDefault();
                objEditDeficiencyObserModel.ObservationID = DeficiencyObservData.DF_ObservationID;
                objEditDeficiencyObserModel.FunctionalAreaID = DeficiencyObservData.FunctionalAreaID;
                objEditDeficiencyObserModel.DepartmentID = DeficiencyObservData.DepartmentID;
                objEditDeficiencyObserModel.ResponsiblePersonID = DeficiencyObservData.ResponsiblePersonEmpID;
                // objEditDeficiencyObserModel.DueDate = Convert.ToDateTime(DeficiencyObservData.DueDate).ToString(HelpClass.dateFormat);
                objEditDeficiencyObserModel.ObservationDescription = DeficiencyObservData.ObservationDescription;
                objEditDeficiencyObserModel.ObservationStatusID = DeficiencyObservData.Status;
            }
            return PartialView(objEditDeficiencyObserModel);
        }
        public PartialViewResult _ViewDeficiency(int DeficiencyID)
        {
            int EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);

            int Notification_ID = GetDeficiencyNotificationID(DeficiencyID);
            if (Notification_ID != 0)
            {
                UpdateRA_NotificationStatus(Notification_ID, 2);
            }
            ViewDeficiencyModel objViewDeficiencyModel = new ViewDeficiencyModel();
            Aizant_Bl objAizant_Bl = new Aizant_Bl();
            var dbRegulatory = new AizantIT_DevEntities();
            List<SelectListItem> objViewDeficiencyList = new List<SelectListItem>();

            var DefCode = (from PD in dbRegulatory.AizantIT_ProductDossier
                           join DefM in dbRegulatory.AizantIT_DeficiencyMaster on PD.ProductDossierID equals DefM.ProductDossierID
                           join TD in dbRegulatory.AizantIT_TypeOfDeficiency on DefM.TypeofDeficiencyID equals TD.TypeOfDeficiencyID
                           join EMP in dbRegulatory.AizantIT_EmpMaster on DefM.ApproverID equals EMP.EmpID
                           where DefM.DeficiencyID == DeficiencyID
                           select new
                           {
                               DefM.DeficiencyCode,
                               TD.TypeOfDeficiencyName,
                               DefM.DeficiencyReceiptDate,
                               DefM.DueDate,
                               DeficiencyStatus = (DefM.DeficiencyStatus == 1 ? "Open" : "Close"),
                               PD.DossierStatus,
                               DefM.ClosedDate,
                               PD.AuthorEmpID,
                               DefM.ApproverID,
                               ApproverName = (EMP.FirstName + " " + EMP.LastName),
                               DefM.CurrentStatus,
                               DossierCurrentStatus = PD.CurrentStatus
                           }).SingleOrDefault();

            objViewDeficiencyModel.DeficiencyCode = DefCode.DeficiencyCode;
            objViewDeficiencyModel.TypeOfDeficiencyName = DefCode.TypeOfDeficiencyName;
            objViewDeficiencyModel.DeficiencyReceiptDate = Convert.ToDateTime(DefCode.DeficiencyReceiptDate).ToString(HelpClass.dateFormat);
            objViewDeficiencyModel.DueDate = Convert.ToDateTime(DefCode.DueDate).ToString(HelpClass.dateFormat);
            objViewDeficiencyModel.DeficiencyStatus = DefCode.DeficiencyStatus;
            objViewDeficiencyModel.DeficiencyCurrentStatus = DefCode.CurrentStatus;
            objViewDeficiencyModel.DossierStatus = DefCode.DossierStatus;
            objViewDeficiencyModel.AuthorID = DefCode.AuthorEmpID;
            objViewDeficiencyModel.ApproverID = DefCode.ApproverID;
            objViewDeficiencyModel.AuthenticationEmpID = EmpID;
            objViewDeficiencyModel.ApproverName = DefCode.ApproverName;
            objViewDeficiencyModel.ClosedDate = DefCode.ClosedDate == null ? "NA" : Convert.ToDateTime(DefCode.ClosedDate).ToString(HelpClass.dateFormat);
            objViewDeficiencyModel.Remarks = RegulatoryComments(dbRegulatory, 2, DeficiencyID);

            var DeficiencyAttachmentID = dbRegulatory.AizantIT_DeficiencyAttachment.Where(D => D.DeficiencyID == DeficiencyID).Count();
            objViewDeficiencyModel.DeficiencyID = DeficiencyID;
            objViewDeficiencyModel.DeficiencyAttachmentID = DeficiencyAttachmentID;
            objViewDeficiencyModel.DossierCurrentStatus = DefCode.DossierCurrentStatus;
            return PartialView(objViewDeficiencyModel);
        }
        public PartialViewResult _ViewObservation(int ObservationID)
        {
            int Notification_ID = GetObservationNotificationID(ObservationID);
            if (Notification_ID != 0)
            {
                UpdateRA_NotificationStatus(Notification_ID, 2);
            }
            ViewObservationModel objViewObservationModel = new ViewObservationModel();
            Aizant_Bl objAizant_Bl = new Aizant_Bl();
            var dbRegulatory = new AizantIT_DevEntities();
            List<SelectListItem> objViewObservationList = new List<SelectListItem>();
            int EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);

            var DefFunctList = (from FA in dbRegulatory.AizantIT_FunctionalArea
                                join DefOb in dbRegulatory.AizantIT_DF_Observation on FA.FunctionalAreaID equals DefOb.FunctionalAreaID
                                join defM in dbRegulatory.AizantIT_DeficiencyMaster on DefOb.DeficiencyID equals defM.DeficiencyID
                                join DM in dbRegulatory.AizantIT_DepartmentMaster on DefOb.DepartmentID equals DM.DeptID
                                join EMP in dbRegulatory.AizantIT_EmpMaster on DefOb.ResponsiblePersonEmpID equals EMP.EmpID
                                where DefOb.DF_ObservationID == ObservationID
                                select new
                                {
                                    FA.FunctionalAreaName,
                                    DM.DepartmentName,
                                    DefOb.ObservationDescription,
                                    ResponsiblePerson = (EMP.FirstName + " " + EMP.LastName),
                                    ObservationStatus = (DefOb.Status == 1 ? "Open" : "Close"),
                                    DefOb.ResponsiblePersonEmpID,
                                    DefOb.CurrentStatus,
                                    defM.DueDate
                                    //  DefOb.DueDate

                                }).SingleOrDefault();

            objViewObservationModel.FunctionalAreaName = DefFunctList.FunctionalAreaName;
            objViewObservationModel.DepartmentName = DefFunctList.DepartmentName;
            objViewObservationModel.ObservationDescription = DefFunctList.ObservationDescription;
            objViewObservationModel.ResponsiblePerson = DefFunctList.ResponsiblePerson;
            objViewObservationModel.ObservationStatus = DefFunctList.ObservationStatus;
            objViewObservationModel.Remarks = RegulatoryComments(dbRegulatory, 3, ObservationID);//3:Observation Table
            objViewObservationModel.ObservationID = ObservationID;
            objViewObservationModel.ResponsiblePersonID = DefFunctList.ResponsiblePersonEmpID;
            objViewObservationModel.ObservationCurrentStatus = DefFunctList.CurrentStatus;
            objViewObservationModel.DueDate = Convert.ToDateTime(DefFunctList.DueDate).ToString(HelpClass.dateFormat);
            objViewObservationModel.AuthenticationEmpID = EmpID;
            return PartialView(objViewObservationModel);
        }
        public PartialViewResult _AddAttachment()
        {
            return PartialView();
        }
        public string ProductDossierCurrentStatus(int CurrentStatus)
        {
            string Name = "";
            switch (CurrentStatus)
            {
                case 1:
                    Name = "Product Created";
                    break;
                case 2:
                    Name = "Submitted for Approval";
                    break;
                case 3:
                    Name = "Product Modified";
                    break;
                case 4:
                    Name = "Product Reverted";
                    break;
                case 5:
                    Name = "Review Accepted Date Submitted";
                    break;
                case 6:
                    Name = "Goal Date Submitted";
                    break;
                case 7:
                    Name = "Product Approved by RA Approver";
                    break;
                case 8:
                    Name = "Dossier Completed";
                    break;
                default:
                    Name = "NA";
                    break;
            }
            return Name;
        }
        //Edit Mode Product Dossier
        public JsonResult AddProductDossierStrength(int ProductDossierID, int Strength, int UOM_ID)
        {
            try
            {
                using (var dbContext = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                    {
                        try
                        {
                            AizantIT_ProdStrength objStrength = new AizantIT_ProdStrength();
                            objStrength.ProductDossierID = ProductDossierID;
                            objStrength.StrengthValue = Strength;
                            objStrength.UOMID = UOM_ID;
                            dbContext.AizantIT_ProdStrength.Add(objStrength);
                            dbContext.SaveChanges();
                            int _ProdUOM_ID = objStrength.ProdStrengthID;
                            transaction.Commit();
                            return Json(new { msgType = "success", ProdUOM_ID = _ProdUOM_ID }, JsonRequestBehavior.AllowGet);
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult UpdateProductDossierStrength(int ID, int ProductDossierID, int Strength, int UOM_ID)
        {
            try
            {
                using (var dbContext = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                    {
                        try
                        {
                            var objStrength = dbContext.AizantIT_ProdStrength.Where(a => a.ProdStrengthID == ID).SingleOrDefault();
                            objStrength.StrengthValue = Strength;
                            objStrength.UOMID = UOM_ID;
                            dbContext.SaveChanges();

                            transaction.Commit();
                            return Json(new { msgType = "success" }, JsonRequestBehavior.AllowGet);
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult DeleteProductDossierStrength(int StrengthID)
        {
            try
            {
                using (var dbContext = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                    {
                        try
                        {
                            var StrengthRecord = dbContext.AizantIT_ProdStrength.Where(a => a.ProdStrengthID == StrengthID).SingleOrDefault();
                            dbContext.AizantIT_ProdStrength.Remove(StrengthRecord);
                            dbContext.SaveChanges();
                            transaction.Commit();
                            return Json(new { msgType = "success" }, JsonRequestBehavior.AllowGet);
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AddCTL(CTL_CreationVM CTL_Data)
        {
            try
            {
                using (var dbContext = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                    {
                        try
                        {
                            AizantIT_ProdContractTestingLab objCTL = new AizantIT_ProdContractTestingLab();
                            objCTL.ProductDossierID = CTL_Data.ProductDossierID;
                            objCTL.FunctionType = CTL_Data.Functionalities;
                            objCTL.ContractTestingLabID = CTL_Data.CTL_ID;
                            dbContext.AizantIT_ProdContractTestingLab.Add(objCTL);
                            dbContext.SaveChanges();
                            int _ProdContractTestingLabID = objCTL.ProdContractTestingLabID;
                            transaction.Commit();
                            return Json(new { msgType = "success", ProdCTL_ID = _ProdContractTestingLabID }, JsonRequestBehavior.AllowGet);
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult UpdateCTL(int CTL_ID, string Functionalities, int TblCTL_ID)
        {
            try
            {
                using (var dbContext = new AizantIT_DevEntities())
                {
                    var objStrength = dbContext.AizantIT_ProdContractTestingLab.Where(a => a.ProdContractTestingLabID == TblCTL_ID).SingleOrDefault();
                    objStrength.FunctionType = Functionalities;
                    objStrength.ContractTestingLabID = CTL_ID;
                    dbContext.SaveChanges();
                    return Json(new { msgType = "success" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult DeleteCTL(int CTLId)
        {
            try
            {
                using (var dbContext = new AizantIT_DevEntities())
                {
                    var CTLRecord = dbContext.AizantIT_ProdContractTestingLab.Where(c => c.ProdContractTestingLabID == CTLId).SingleOrDefault();
                    dbContext.AizantIT_ProdContractTestingLab.Remove(CTLRecord);
                    dbContext.SaveChanges();
                    return Json(new { msgType = "success" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public int GetDossierNotificationID(int ProductDossierID)
        {
            var dbContext = new AizantIT_DevEntities();
            var NotificationID = dbContext.AizantIT_DossierNotifyLink.Where(a => a.ProductDossierID == ProductDossierID).Select(a => a.NotificationID).SingleOrDefault();
            return Convert.ToInt32(NotificationID);
        }
        public int GetDeficiencyNotificationID(int DeficiencyID)
        {
            var dbContext = new AizantIT_DevEntities();
            var NotificationID = dbContext.AizantIT_DeficiencyNotifyLink.Where(a => a.DeficiencyID == DeficiencyID).Select(a => a.NotificationID).SingleOrDefault();
            return Convert.ToInt32(NotificationID);
        }
        public int GetObservationNotificationID(int ObservationID)
        {
            var dbContext = new AizantIT_DevEntities();
            var NotificationID = dbContext.AizantIT_DF_ObservationNotifyLink.Where(o => o.ObservationID == ObservationID).Select(o => o.NotificationID).SingleOrDefault();
            return Convert.ToInt32(NotificationID);
        }
        #region Dossier Under_review Process
        public ActionResult DossierDatesSubmission(int ProductDossierID, string ReviewAcceptedDate, string GoalDate, string Comments = "")
        {
            try
            {
                var ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                using (AizantIT_DevEntities dbContext = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                    {
                        try
                        {
                            var GoalDataResult = 0;
                            var ReviewAcceptedDateResult = 0;
                            var ProductDossierDetails = dbContext.AizantIT_ProductDossier.Where(a => a.ProductDossierID == ProductDossierID).SingleOrDefault();

                            if (ProductDossierDetails.AuthorEmpID == ActionBy)
                            {
                                if (ProductDossierDetails.ReviewAcceptedDate == null)
                                {
                                    ProductDossierDetails.ReviewAcceptedDate = getDateValue(ReviewAcceptedDate);
                                    ProductDossierDetails.CurrentStatus = Convert.ToInt32(RA_DossierHistoryStatus.Review_Accepted_Date_Submitted);
                                    ReviewAcceptedDateResult = 1;
                                }
                                if (GoalDate != "")
                                {
                                    if (ProductDossierDetails.GoalDate == null)
                                    {
                                        ProductDossierDetails.GoalDate = getDateValue(GoalDate);
                                        ProductDossierDetails.CurrentStatus = Convert.ToInt32(RA_DossierHistoryStatus.Goal_Date_Submitted);
                                        GoalDataResult = 1;
                                    }
                                }
                                dbContext.SaveChanges();
                                //History
                                string Description = "";
                                int ResultType = 0;
                                int DeficiencyCount = 0;

                                if ((GoalDataResult == 1) && (ReviewAcceptedDateResult == 1))
                                {
                                    ProductDossierHistoryInsert(dbContext, ProductDossierID, ActionBy, Convert.ToInt32(RA_UserRole.RA_Lead), Convert.ToInt32(RA_DossierHistoryStatus.Review_Accepted_Date_Submitted), Comments);
                                    ProductDossierHistoryInsert(dbContext, ProductDossierID, ActionBy, Convert.ToInt32(RA_UserRole.RA_Lead), Convert.ToInt32(RA_DossierHistoryStatus.Goal_Date_Submitted), Comments);
                                    Description = string.Format("Product Review Accepted Date And Goal Date Added on Product Application No: '{0}' of Dossier Status '{1}'", ProductDossierDetails.ApplicationNum, (ProductDossierDetails.DossierStatus == "U" ? "Under Review" : "Approved"));
                                    ResultType = 3;//Goal Date and Review Accepted Date
                                    DeficiencyCount = DossierWiseDeficiencyChecking(dbContext, ProductDossierID);//Dossier Wise Deficiency Count ==0 then Dossier status Enable False
                                }
                                else
                                {
                                    if (ReviewAcceptedDateResult == 1)
                                    {
                                        ProductDossierHistoryInsert(dbContext, ProductDossierID, ActionBy, Convert.ToInt32(RA_UserRole.RA_Lead), Convert.ToInt32(RA_DossierHistoryStatus.Review_Accepted_Date_Submitted), Comments);
                                        Description = string.Format("Product Review Accepted Date Added on Product Application No: '{0}' of Dossier Status '{1}'", ProductDossierDetails.ApplicationNum, (ProductDossierDetails.DossierStatus == "U" ? "Under Review" : "Approved"));
                                        ResultType = 1;//Review Accepted Data 

                                    }
                                    if (GoalDataResult == 1)
                                    {
                                        ProductDossierHistoryInsert(dbContext, ProductDossierID, ActionBy, Convert.ToInt32(RA_UserRole.RA_Lead), Convert.ToInt32(RA_DossierHistoryStatus.Goal_Date_Submitted), Comments);
                                        Description = string.Format("Product Goal Date Added on Product Application No: '{0}' of Dossier Status '{1}'", ProductDossierDetails.ApplicationNum, (ProductDossierDetails.DossierStatus == "U" ? "Under Review" : "Approved"));
                                        ResultType = 2;//Goal Date 
                                        DeficiencyCount = DossierWiseDeficiencyChecking(dbContext, ProductDossierID);//Dossier Wise Deficiency Count ==0 then Dossier status Enable False
                                    }
                                }

                                //Notification
                                NotificationActionByLinq objNotifications = new NotificationActionByLinq();
                                NotificationBO objNotificationsBO = new NotificationBO();
                                objNotificationsBO.NotificationID = GetDossierNotificationID(ProductDossierID);
                                objNotificationsBO.Description = Description;
                                objNotificationsBO.ModuleID = Convert.ToInt32(Modules.RA);
                                objNotificationsBO.NotificationLink = "/Regulatory/RegulatoryMain/ViewDossierDetails?DossierID=" + ProductDossierDetails.ProductDossierID + "&ListType=1";
                                List<NotificationRoleIDandDeptID> NotificationRoleIDSandDeptIDs = new List<NotificationRoleIDandDeptID>();
                                NotificationRoleIDSandDeptIDs.Add(new NotificationRoleIDandDeptID
                                {
                                    RoleID = Convert.ToInt32(RA_UserRole.RA_Approver),
                                    DeptID = 0
                                });
                                objNotificationsBO.NotificationRoleIDSandDeptIDs = NotificationRoleIDSandDeptIDs;
                                List<int> objNotificationEmpIDs = new List<int>();
                                objNotificationEmpIDs.Add(ProductDossierDetails.ApproverEmpID);
                                objNotificationsBO.NotificationEmpIDS = objNotificationEmpIDs;
                                objNotifications.NotificationToNextEmp(objNotificationsBO);
                                transaction.Commit();
                                return Json(new { _ResultType = ResultType, _DeficiencyCount = DeficiencyCount }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { _ResultType = 4, msg = RegulatoryValidationMessages.RA_ErrorMsg1 }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), _ResultType = 4 }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ProductDossierComplete(int ProductDossierID, string DossierStatus, string ApprovedDate, int MarketStatus, string Comments = "")
        {
            try
            {
                var ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                using (AizantIT_DevEntities dbContext = new AizantIT_DevEntities())
                {
                    if (dbContext.AizantIT_ProductDossier.Where(a => a.ProductDossierID == ProductDossierID && a.AuthorEmpID == ActionBy).Any())
                    {
                        var DeficiencyCount = DossierWiseDeficiencyChecking(dbContext, ProductDossierID);
                        if (DeficiencyCount == 0)
                        {
                            using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                            {
                                try
                                {
                                    var ProductDossier = dbContext.AizantIT_ProductDossier.Where(a => a.ProductDossierID == ProductDossierID).SingleOrDefault();
                                    ProductDossier.DossierStatus = DossierStatus;
                                    ProductDossier.MarketingStatusID = MarketStatus;
                                    ProductDossier.ApprovedDate = getDateValue(ApprovedDate);
                                    ProductDossier.CurrentStatus = Convert.ToInt32(RA_DossierHistoryStatus.Dossier_Completed);
                                    dbContext.SaveChanges();

                                    ProductDossierHistoryInsert(dbContext, ProductDossierID, ActionBy, Convert.ToInt32(RA_UserRole.RA_Lead), Convert.ToInt32(RA_DossierHistoryStatus.Dossier_Completed), Comments);
                                    int Notification_ID = GetDossierNotificationID(ProductDossierID);

                                    int[] Array_Notification_ID = { Notification_ID };
                                    dbContext.AizantIT_DossierNotifyLink.RemoveRange(
                                    dbContext.AizantIT_DossierNotifyLink.Where(p => Array_Notification_ID.Contains((int)p.NotificationID)));
                                    dbContext.SaveChanges();
                                    NotificationActionByLinq obNotificationLinq = new NotificationActionByLinq();
                                    transaction.Commit();
                                    obNotificationLinq.DeleteNotification(Array_Notification_ID);
                                    return Json(new { msgType = "success" }, JsonRequestBehavior.AllowGet);
                                }
                                catch (Exception ex)
                                {
                                    transaction.Rollback();
                                    throw ex;
                                }
                            }
                        }
                        else
                        {
                            return Json(new { msgType = "warning" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { msgType = "error", msg = RegulatoryValidationMessages.RA_ErrorMsg1 }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        [CustAuthorization((int)Modules.RA, (int)RA_UserRole.RA_Lead,
                                       (int)RA_UserRole.RA_Approver)]
        public ActionResult DeficiencyMainList(int ShowType, int DeficiencyID = 0, int ProductDossierID = 0)
        {
            ViewBag.ShowType = ShowType;
            ViewBag.DeficiencyID = DeficiencyID;
            ViewBag.ProductDossierID = ProductDossierID;
            return View();
        }
        //Deficiency Under review Process
        public ActionResult DeficiencySubmitForApproval(int DeficiencyID, string Remarks = "")
        {
            try
            {
                var ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                using (AizantIT_DevEntities dbContext = new AizantIT_DevEntities())
                {
                    if (dbContext.AizantIT_DeficiencyMaster.Where(a => a.DeficiencyID == DeficiencyID && a.AuthorID == ActionBy).Any())
                    {
                        using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                        {
                            try
                            {
                                var DeficiencyData = dbContext.AizantIT_DeficiencyMaster.Where(a => a.DeficiencyID == DeficiencyID && a.CurrentStatus == 1).SingleOrDefault();

                                DeficiencyData.CurrentStatus = Convert.ToInt32(RA_DeficiencyHistoryStatus.Submitted_for_Approval);
                                DeficiencyData.AuthorID = ActionBy;
                                DeficiencyData.Remarks = Remarks;
                                dbContext.SaveChanges();

                                OnDeficiencyCreateAddNotification(dbContext, DeficiencyData.ApproverID, DeficiencyData.DeficiencyCode, DeficiencyData.DeficiencyID, (int)DeficiencyData.ProductDossierID);
                                DeficiencyHistoryInsert(dbContext, DeficiencyData.DeficiencyID, ActionBy, Convert.ToInt32(RA_UserRole.RA_Lead), Convert.ToInt32(RA_DeficiencyHistoryStatus.Submitted_for_Approval), Remarks);
                                transaction.Commit();
                                return Json(new { msgType = "success" }, JsonRequestBehavior.AllowGet);
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                throw ex;
                            }
                        }
                    }
                    else
                    {
                        return Json(new { msgType = "error", msg = RegulatoryValidationMessages.RA_ErrorMsg1 }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult DeficiencyApproval(int DeficiencyID, string DeficiencyComment, int DeficiencyStatus)
        {
            int EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            try
            {
                using (AizantIT_DevEntities dbContext = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                    {
                        try
                        {
                            var DeficiencyDetails = dbContext.AizantIT_DeficiencyMaster.Where(a => a.DeficiencyID == DeficiencyID).SingleOrDefault();

                            if (DeficiencyDetails.ApproverID == EmpID)
                            {
                                var ProductDetails = (from prod in dbContext.AizantIT_ProductDossier
                                                      where prod.ProductDossierID == DeficiencyDetails.ProductDossierID
                                                      select new { prod.ApplicationNum }).SingleOrDefault();
                                int ShowType;
                                if (DeficiencyStatus == 4)
                                {
                                    DeficiencyDetails.CurrentStatus = Convert.ToInt32(RA_DeficiencyHistoryStatus.Deficiency_Reverted);
                                    DeficiencyDetails.ApproverID = EmpID;
                                    dbContext.SaveChanges();
                                    ShowType = 13;//Revert List type
                                    NotificationActionByLinq objNotifications = new NotificationActionByLinq();
                                    NotificationBO objNotificationsBO = new NotificationBO();
                                    objNotificationsBO.NotificationID = GetDeficiencyNotificationID(DeficiencyID);
                                    objNotificationsBO.Description = string.Format("Deficiency has been Reverted with Deficiency Code : '{0}' on Application No.:'{1}'", DeficiencyDetails.DeficiencyCode, ProductDetails.ApplicationNum);
                                    objNotificationsBO.ModuleID = Convert.ToInt32(Modules.RA);
                                    objNotificationsBO.NotificationLink = "/Regulatory/RegulatoryMain/DeficiencyMainList?ShowType=" + ShowType + "&DeficiencyID=" + DeficiencyID + "&ProductDossierID=" + DeficiencyDetails.ProductDossierID;
                                    List<NotificationRoleIDandDeptID> NotificationRoleIDSandDeptIDs = new List<NotificationRoleIDandDeptID>();

                                    NotificationRoleIDSandDeptIDs.Add(new NotificationRoleIDandDeptID
                                    {
                                        RoleID = Convert.ToInt32(RA_UserRole.RA_Lead),
                                        DeptID = 0
                                    });
                                    objNotificationsBO.NotificationRoleIDSandDeptIDs = NotificationRoleIDSandDeptIDs;
                                    List<int> objNotificationEmpIDs = new List<int>();
                                    objNotificationEmpIDs.Add(DeficiencyDetails.AuthorID);
                                    objNotificationsBO.NotificationEmpIDS = objNotificationEmpIDs;
                                    objNotifications.NotificationToNextEmp(objNotificationsBO);

                                    DeficiencyHistoryInsert(dbContext, DeficiencyID, EmpID, Convert.ToInt32(RA_UserRole.RA_Approver), Convert.ToInt32(RA_DeficiencyHistoryStatus.Deficiency_Reverted), DeficiencyComment);
                                    transaction.Commit();
                                    return Json(new { msgType = "success" }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    DeficiencyDetails.CurrentStatus = Convert.ToInt32(RA_DeficiencyHistoryStatus.Deficiency_Approved);
                                    DeficiencyDetails.ApproverID = EmpID;
                                    dbContext.SaveChanges();
                                    int Notification_ID = GetDeficiencyNotificationID(DeficiencyID);
                                    if (Notification_ID != 0)
                                    {
                                        UpdateRA_NotificationStatus(Notification_ID, 3);
                                    }
                                    var ObservationDetails = dbContext.AizantIT_DF_Observation.Where(a => a.DeficiencyID == DeficiencyID).ToList();

                                    foreach (var item in ObservationDetails)
                                    {
                                        var observationData = dbContext.AizantIT_DF_Observation.Where(a => a.DF_ObservationID == item.DF_ObservationID).SingleOrDefault();
                                        observationData.CurrentStatus = Convert.ToInt16(RA_ObservationHistoryStatus.ObservationInitiated);
                                        dbContext.SaveChanges();
                                        ObservationHistoryInsert(dbContext, item.DF_ObservationID, EmpID, Convert.ToInt32(RA_UserRole.RA_Approver), Convert.ToInt16(RA_ObservationHistoryStatus.ObservationInitiated));

                                        OnObservationCreateAddNotification(dbContext, item.ResponsiblePersonEmpID, DeficiencyDetails.DeficiencyCode, item.DF_ObservationID, item.DepartmentID, ProductDetails.ApplicationNum);
                                    }
                                    DeficiencyHistoryInsert(dbContext, DeficiencyID, EmpID, Convert.ToInt32(RA_UserRole.RA_Approver), Convert.ToInt32(RA_DeficiencyHistoryStatus.Deficiency_Approved), DeficiencyComment);
                                    transaction.Commit();

                                    return Json(new { msgType = "success" }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json(new { msg = RegulatoryValidationMessages.RA_ErrorMsg1, msgType = "error" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        private static void OnDeficiencyCreateAddNotification(AizantIT_DevEntities dbContext, int ApproverID, string DeficiencyCode, int DeficiencyID, int ProductDossierID)
        {
            var ApplicationNumber = dbContext.AizantIT_ProductDossier.Where(a => a.ProductDossierID == ProductDossierID).Select(a => a.ApplicationNum).SingleOrDefault();
            int ShowType = 12;
            NotificationActionByLinq objNotifications = new NotificationActionByLinq();
            NotificationBO objNotificationsBO = new NotificationBO();
            objNotificationsBO.Description = string.Format("Deficiency has been Created with Deficiency Code: '{0}' on Application No. :'{1}'", DeficiencyCode, ApplicationNumber);
            objNotificationsBO.ModuleID = Convert.ToInt32(Modules.RA);
            objNotificationsBO.NotificationLink = "/Regulatory/RegulatoryMain/DeficiencyMainList?ShowType=" + ShowType + "&DeficiencyID=" + DeficiencyID + "&ProductDossierID=" + ProductDossierID;
            List<NotificationRoleIDandDeptID> NotificationRoleIDSandDeptIDs = new List<NotificationRoleIDandDeptID>();
            NotificationRoleIDSandDeptIDs.Add(new NotificationRoleIDandDeptID
            {
                RoleID = Convert.ToInt32(RA_UserRole.RA_Approver),
                DeptID = 0
            });
            objNotificationsBO.NotificationRoleIDSandDeptIDs = NotificationRoleIDSandDeptIDs;
            List<int> objNotificationEmpIDs = new List<int>();
            objNotificationEmpIDs.Add(ApproverID);
            objNotificationsBO.NotificationEmpIDS = objNotificationEmpIDs;
            int GetNotificationID_No = objNotifications.InsertNotifications(objNotificationsBO);

            AizantIT_DeficiencyNotifyLink objDeficiencyNotifyLink = new AizantIT_DeficiencyNotifyLink();
            objDeficiencyNotifyLink.DeficiencyID = DeficiencyID;
            objDeficiencyNotifyLink.NotificationID = GetNotificationID_No;
            dbContext.AizantIT_DeficiencyNotifyLink.Add(objDeficiencyNotifyLink);
            dbContext.SaveChanges();
        }
        private static void OnObservationCreateAddNotification(AizantIT_DevEntities dbContext, int ResponsiblePerson, string DeficiencyCode, int ObservationID, int DeptID, string AppilicationNumber)
        {
            int ShowType = 18;
            NotificationActionByLinq objNotifications = new NotificationActionByLinq();
            NotificationBO objNotificationsBO = new NotificationBO();
            objNotificationsBO.Description = string.Format("Observation has been Created with Deficiency Code : '{0}' on Application No.: '{1}'", DeficiencyCode, AppilicationNumber);
            objNotificationsBO.ModuleID = Convert.ToInt32(Modules.RA);
            objNotificationsBO.NotificationLink = "/Regulatory/RegulatoryMain/ObservationMainList?ShowType=" + ShowType + "&ObservationID=" + ObservationID;
            List<NotificationRoleIDandDeptID> NotificationRoleIDSandDeptIDs = new List<NotificationRoleIDandDeptID>();
            NotificationRoleIDSandDeptIDs.Add(new NotificationRoleIDandDeptID
            {
                RoleID = Convert.ToInt32(RA_UserRole.RA_HOD),
                DeptID = DeptID
            });
            objNotificationsBO.NotificationRoleIDSandDeptIDs = NotificationRoleIDSandDeptIDs;
            List<int> objNotificationEmpIDs = new List<int>();
            objNotificationEmpIDs.Add(ResponsiblePerson);
            objNotificationsBO.NotificationEmpIDS = objNotificationEmpIDs;
            int GetNotificationID_No = objNotifications.InsertNotifications(objNotificationsBO);

            AizantIT_DF_ObservationNotifyLink objObservationNotifyLink = new AizantIT_DF_ObservationNotifyLink();
            objObservationNotifyLink.ObservationID = ObservationID;
            objObservationNotifyLink.NotificationID = GetNotificationID_No;
            dbContext.AizantIT_DF_ObservationNotifyLink.Add(objObservationNotifyLink);
            dbContext.SaveChanges();
        }
        [CustAuthorization((int)Modules.RA, (int)RA_UserRole.RA_Lead,
       (int)RA_UserRole.RA_Approver,
       (int)RA_UserRole.RA_HOD)]
        public ActionResult ObservationMainList(int ShowType, int ObservationID = 0)
        {
            Session["RegulatoryObservationList"] = null;
            ViewBag.ShowTypes = ShowType;
            ViewBag.ObservationID = ObservationID;
            string[] ModuleRoles = (string[])Session["EmpModuleRoles"];
            if (ModuleRoles.Contains(((int)Aizant_Enums.AizantEnums.RA_UserRole.RA_Lead).ToString())
                || ModuleRoles.Contains(((int)Aizant_Enums.AizantEnums.RA_UserRole.RA_Approver).ToString()))
            {
                ViewBag.ShowExportPDF = "Y";
            }
            return View();
        }
        //Observation Complete or Accept
        public ActionResult ObservationAcceptOrComplete(int ObservationID, int ObservationStatus = 0, int ResponceType = 0, string Comments = "")
        {
            try
            {
                string MsgText = "";
                var ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                using (AizantIT_DevEntities dbContext = new AizantIT_DevEntities())
                {
                    using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                    {
                        try
                        {
                            var ObservationDetails = dbContext.AizantIT_DF_Observation.Where(a => a.DF_ObservationID == ObservationID).SingleOrDefault();
                            if (ObservationDetails.ResponsiblePersonEmpID == ActionBy)
                            {
                                if (ResponceType == Convert.ToInt32(RA_ObservationHistoryStatus.ObservationAccepted))//2:Observation Accept
                                {
                                    ObservationDetails.CurrentStatus = Convert.ToInt32(RA_ObservationHistoryStatus.ObservationAccepted);
                                    dbContext.SaveChanges();
                                    ObservationHistoryInsert(dbContext, ObservationID, ActionBy, Convert.ToInt32(RA_UserRole.RA_HOD), Convert.ToInt32(RA_ObservationHistoryStatus.ObservationAccepted));
                                    MsgText = RegulatoryValidationMessages.RA_ErrorMsg9;
                                    int Notification_ID = GetObservationNotificationID(ObservationID);
                                    if (Notification_ID != 0)
                                    {
                                        UpdateRA_NotificationStatus(Notification_ID, 3);
                                    }
                                    transaction.Commit();
                                }
                                else
                                {
                                    ObservationDetails.Status = ObservationStatus;
                                    ObservationDetails.CurrentStatus = Convert.ToInt32(RA_ObservationHistoryStatus.ObservationCompletionSubmitted);
                                    dbContext.SaveChanges();
                                    //Observation History
                                    ObservationHistoryInsert(dbContext, ObservationID, ActionBy, Convert.ToInt32(RA_UserRole.RA_HOD), Convert.ToInt32(RA_ObservationHistoryStatus.ObservationCompletionSubmitted), Comments);

                                    var ObservationOpenStatusCount = dbContext.AizantIT_DF_Observation.Where(a => a.DeficiencyID == ObservationDetails.DeficiencyID && a.Status == 1).Count();

                                    if (ObservationOpenStatusCount == 0)//Based on DeficiencyID all the Observation status 'Close' then Deficiency Status Closed And History inserted 'Deficiency Completed'
                                    {
                                        var DeficiencyDetails = dbContext.AizantIT_DeficiencyMaster.Where(a => a.DeficiencyID == ObservationDetails.DeficiencyID).SingleOrDefault();
                                        DeficiencyDetails.DeficiencyStatus = 2;//2:Deficiency Status Close
                                        DeficiencyDetails.ClosedDate = DateTime.Now;
                                        DeficiencyDetails.CurrentStatus = Convert.ToInt32(RA_DeficiencyHistoryStatus.Deficiency_Completed);
                                        dbContext.SaveChanges();
                                        DeficiencyHistoryInsert(dbContext, Convert.ToInt32(ObservationDetails.DeficiencyID), null, null, Convert.ToInt32(RA_DeficiencyHistoryStatus.Deficiency_Completed));

                                        //Observation Notification Deletion
                                        int Obs_Notification_ID = GetObservationNotificationID(ObservationID);
                                        dbContext.AizantIT_DF_ObservationNotifyLink.RemoveRange(
                                        dbContext.AizantIT_DF_ObservationNotifyLink.Where(p => p.NotificationID == Obs_Notification_ID));

                                        //Deficiency Notification  Deletion
                                        int Def_Notification_ID = GetDeficiencyNotificationID(Convert.ToInt32(ObservationDetails.DeficiencyID));
                                        dbContext.AizantIT_DeficiencyNotifyLink.RemoveRange(
                                        dbContext.AizantIT_DeficiencyNotifyLink.Where(p => p.NotificationID == Def_Notification_ID));

                                        int[] Array_Notification_ID = { Obs_Notification_ID, Def_Notification_ID };
                                        dbContext.SaveChanges();
                                        NotificationActionByLinq obNotificationLinq = new NotificationActionByLinq();

                                        transaction.Commit();
                                        obNotificationLinq.DeleteNotification(Array_Notification_ID);
                                    }
                                    else
                                    {
                                        transaction.Commit();
                                    }
                                    MsgText = RegulatoryValidationMessages.RA_ErrorMsg8;
                                }
                                return Json(new { msgType = "success", msg = MsgText }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { msg = RegulatoryValidationMessages.RA_ErrorMsg1, msgType = "error" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        //Deficiency Wise Observation Count 
        public int DeficiencyWiseObservationChecking(AizantIT_DevEntities dbContext, int DeficiencyID = 0)
        {
            return dbContext.AizantIT_DF_Observation.Where(a => a.DeficiencyID == DeficiencyID && a.Status == 1).Count();
        }
        public int DossierWiseDeficiencyChecking(AizantIT_DevEntities dbContext, int ProductDossierID)
        {
            return dbContext.AizantIT_DeficiencyMaster.Where(a => a.ProductDossierID == ProductDossierID && a.DeficiencyStatus == 1).Count();
        }
        //Get Regulatory Comments Based on Table_ID & Pk_ID 
        public string RegulatoryComments(AizantIT_DevEntities dbContext, int TableID, int BasedID)
        {
            var Comments = "";
            if (TableID == 1)//Dossier Table 
            {
                Comments = (from RH in dbContext.AizantIT_DossierMasterHistory where RH.BaseID == BasedID orderby RH.HistoryID descending select RH.Comments).FirstOrDefault();
            }
            else if (TableID == 2)//Deficiency Table 
            {
                Comments = (from RH in dbContext.AizantIT_DeficiencyHistory where RH.BaseID == BasedID orderby RH.HistoryID descending select RH.Comments).FirstOrDefault();
            }
            else if (TableID == 3)//Observation Table 
            {
                Comments = (from RH in dbContext.AizantIT_DF_ObservationHistory where RH.BaseID == BasedID orderby RH.HistoryID descending select RH.Comments).FirstOrDefault();
            }
            return Comments is null ? "N/A" : Comments;
        }
        #region Regulatory Chart's
        public ActionResult DossierChart()
        {
            try
            {
                List<RaDossierChart> objDossierChartsList = new List<RaDossierChart>();
                RegulatoryLinqQuery objDossierLinq = new RegulatoryLinqQuery();
                DataSet dsDossierCharts = objDossierLinq.GetDossierDashBoardCharts(Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                DataTable dtDossierChartData = dsDossierCharts.Tables[0];
                for (int i = 0; i < dtDossierChartData.Rows.Count; i++)
                {
                    objDossierChartsList.Add(
                       new RaDossierChart(dtDossierChartData.Rows[i]["EventName"].ToString(),
                       Convert.ToInt32(dtDossierChartData.Rows[i]["Pending"]), Convert.ToInt32(dtDossierChartData.Rows[i]["Completed"])));
                }
                return Json(new
                {
                    msg = RegulatoryValidationMessages.RA_ErrorMsg10,
                    msgType = "success",
                    aaData = objDossierChartsList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message) + ".";
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ObservationChart()
        {
            try
            {
                List<RaObservationChart> objObservationChartsList = new List<RaObservationChart>();
                RegulatoryLinqQuery objDossierLinq = new RegulatoryLinqQuery();
                DataSet dsObservationCharts = objDossierLinq.GetObservationDashBoardCharts(Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
                DataTable dtObservationChartData = dsObservationCharts.Tables[0];
                for (int i = 0; i < dtObservationChartData.Rows.Count; i++)
                {
                    objObservationChartsList.Add(
                       new RaObservationChart(dtObservationChartData.Rows[i]["EventName"].ToString(),
                       Convert.ToInt32(dtObservationChartData.Rows[i]["Pending"])));
                }
                return Json(new
                {
                    msg = RegulatoryValidationMessages.RA_ErrorMsg10,
                    msgType = "success",
                    aaData = objObservationChartsList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                string message = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message) + ".";
                return Json(new { msg = message, msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult DeficiencyChartList()
        {
            double DeficinecyCount = 0;
            var EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            RegulatoryLinqQuery obRegulatoryLinqQuery = new RegulatoryLinqQuery();
            DataSet ds = obRegulatoryLinqQuery.GetDeficiencyChartList(EmpID);
            List<DeficiencyChartList> objchartList = new List<DeficiencyChartList>();
            if (ds.Tables.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    objchartList.Add(new DeficiencyChartList(dr["EventName"].ToString(), Convert.ToDouble(dr["TotalCount"])));
                    DeficinecyCount += Convert.ToDouble(dr["TotalCount"]);
                }
            }
            return Json(new { DeficiencychartList = objchartList, _DeficinecyCount = DeficinecyCount }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        public string GetMarketingStatus(int MarketStatusID)
        {
            string MarketStatus = "";
            switch (MarketStatusID)
            {
                case 1:
                    MarketStatus = "Yes";
                    break;
                case 2:
                    MarketStatus = "No";
                    break;
                default:
                    MarketStatus = "NA";
                    break;
            }
            return MarketStatus;
        }
        #region Product DMFHolder
        public JsonResult AddProduct_DMFHolder(int DMFHolderID, int DrugSubstanceID, int DSManufacturing_ID, string DMFHolderNum, int ProductDossierID)
        {
            try
            {
                using (var dbContext = new AizantIT_DevEntities())
                {
                    Product_DMFHolderList Prod_DMFHolderlist = new Product_DMFHolderList(ProductDossierID, DMFHolderID, DrugSubstanceID, DSManufacturing_ID, DMFHolderNum);
                    AizantIT_ProdDMFHolder objAizantIT_ProdDMFHolder = AddProdDMF_Object(Prod_DMFHolderlist);
                    dbContext.AizantIT_ProdDMFHolder.Add(objAizantIT_ProdDMFHolder);
                    dbContext.SaveChanges();
                    var _ProdDMFHolderID = objAizantIT_ProdDMFHolder.ProdDMFHolderID;
                    return Json(new { msgType = "success", ProdDMFHolderID = _ProdDMFHolderID }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult UpdateProduct_DMFHolder(int ProdDMFHolderID, int DMFHolderID, int DrugSubstanceID, int DS_ManufacturingSiteID, string DMFHolderNum)
        {
            try
            {
                using (var dbContext = new AizantIT_DevEntities())
                {
                    var Product_DMFHolderDetails = dbContext.AizantIT_ProdDMFHolder.Where(a => a.ProdDMFHolderID == ProdDMFHolderID).SingleOrDefault();
                    Product_DMFHolderDetails.DMFHolderID = DMFHolderID;
                    Product_DMFHolderDetails.DrugSubstanceID = DrugSubstanceID;
                    Product_DMFHolderDetails.DS_ManufacturingSiteID = DS_ManufacturingSiteID;
                    Product_DMFHolderDetails.DMFHolderNum = DMFHolderNum;
                    dbContext.SaveChanges();
                    return Json(new { msgType = "success" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult DeleteProduct_DMFHolder(int ProdDMFHolderID)
        {
            try
            {
                using (var dbContext = new AizantIT_DevEntities())
                {
                    var ProdDMFHolder = dbContext.AizantIT_ProdDMFHolder.Where(c => c.ProdDMFHolderID == ProdDMFHolderID).SingleOrDefault();
                    dbContext.AizantIT_ProdDMFHolder.Remove(ProdDMFHolder);
                    dbContext.SaveChanges();
                    return Json(new { msgType = "success" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public void ProdDMF_HolderInsert(AizantIT_DevEntities dbContext, List<Product_DMFHolderList> ProdDMFList)
        {
            List<AizantIT_ProdDMFHolder> objAizantIT_ProdDMFHolderList = new List<AizantIT_ProdDMFHolder>();
            if (ProdDMFList.Count > 0)
            {
                foreach (var item in ProdDMFList)
                {
                    AizantIT_ProdDMFHolder objAizantIT_ProdDMFHolder = AddProdDMF_Object(item);
                    objAizantIT_ProdDMFHolderList.Add(objAizantIT_ProdDMFHolder);
                }
                dbContext.AizantIT_ProdDMFHolder.AddRange(objAizantIT_ProdDMFHolderList);
            }
        }
        private static AizantIT_ProdDMFHolder AddProdDMF_Object(Product_DMFHolderList item)
        {
            AizantIT_ProdDMFHolder objAizantIT_ProdDMFHolder = new AizantIT_ProdDMFHolder();
            objAizantIT_ProdDMFHolder.ProductDossierID = item.ProductDossierID;
            objAizantIT_ProdDMFHolder.DMFHolderID = item.DMF_HolderID;
            objAizantIT_ProdDMFHolder.DrugSubstanceID = item.DrugSubstance_ID;
            objAizantIT_ProdDMFHolder.DS_ManufacturingSiteID = item.DSManufacturing_ID;
            objAizantIT_ProdDMFHolder.DMFHolderNum = Convert.ToString(item.DMFNumber);
            return objAizantIT_ProdDMFHolder;
        }
        #endregion

        #region RegulatoryCrystalReport
        //Observation Report
        public FileResult DownloadObservationReport(int ShowType, int ObservationID = 0, int DeficiencyID = 0, int ProductDossierID = 0, string IsJqueryDataTable = "N")
        {
            try
            {
                string Downloadfilename = "ProductObservationList.pdf";
                byte[] fileBytes;
                ReportDocument objCrDoc = new ReportDocument();
                string CrPath = Server.MapPath("~/Areas/Regulatory/Models/Reports/CR_ObservationMainList.rpt");
                objCrDoc.Load(CrPath);
                ObservationListReport objObservationReport = new ObservationListReport();
                var ObservationMainList = GetObservationMainListData(ShowType, ObservationID, DeficiencyID, ProductDossierID, IsJqueryDataTable);
                foreach (var item in ObservationMainList)
                {
                    objObservationReport.ObservationMainList.AddObservationMainListRow(Convert.ToInt32(item.ObservationID), item.FunctionalArea, item.DepartmentName, item.ResponsiblePerson, item.ObservationStatus, item.ObservationCurrentStatus, Convert.ToString(item.DueDate), Convert.ToInt32(item.ProductDossierID), Convert.ToInt32(item.DeficiencyID), Convert.ToInt32(item.ObservationCurrentStatusID), Convert.ToInt32(item.ResponsiblePersonEmpID), Convert.ToInt32(item.DeficiencyCurrentStatusID), item.DossierCreatedAs, Convert.ToInt32(item.DossierCurrentStatusID), item.Market, item.Client, item.Applicant, item.ApplicationNumber, item.ProductName);
                }
                objCrDoc.SetDataSource(objObservationReport);
                string ObservationReportFileDirectory = Server.MapPath("~\\Areas\\Regulatory\\Models\\Reports\\ObservationTemp");
                if (!Directory.Exists(ObservationReportFileDirectory))
                {
                    Directory.CreateDirectory(ObservationReportFileDirectory);
                }
                string ObservationReportFilePath = ObservationReportFileDirectory + "\\Observation_List_Report_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                objCrDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, ObservationReportFilePath);
                fileBytes = System.IO.File.ReadAllBytes(ObservationReportFilePath);
                System.IO.File.Delete(ObservationReportFilePath);
                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, Downloadfilename);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~/Areas/Regulatory/Models/Error.txt"));
                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, "InternalError.txt");
            }
        }
        public FileResult DownloadProductListReport(int ListType, string IsJqureyDatatable)
        {
            try
            {
                string Downloadfilename = "ProductMainList.pdf";
                byte[] fileBytes;
                ReportDocument objCrDoc = new ReportDocument();
                string CrPath = Server.MapPath("~/Areas/Regulatory/Models/Reports/CR_ProductDetailsMainList.rpt");
                objCrDoc.Load(CrPath);
                ProductDetailsListReport objProductDetailsReport = new ProductDetailsListReport();
                var ProductDetailsMainList = GetProductDetailsMainList(ListType, IsJqureyDatatable);

                foreach (var item in ProductDetailsMainList)
                {
                    objProductDetailsReport.ProductDetailsList.AddProductDetailsListRow(Convert.ToInt32(item.DossierID), item.ProductName, item.ApplicationNumber, Convert.ToString(item.SubmissionDate), Convert.ToString(item.ReviewAcceptedDate), item.ApplicantName, item.ClientName, item.CurrentStatus, item.DossierStatus, Convert.ToString(item.GoalDate), Convert.ToString(item.ApprovedDate), Convert.ToInt32(item.DossierCurrentStatusID));
                }
                objCrDoc.SetDataSource(objProductDetailsReport);

                string ProductDetailsReportFileName = "ProductDetails_List_Report_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                string ProductDetailsReportFileDirectory = Server.MapPath("~\\Areas\\Regulatory\\Models\\Reports\\ProductDetailsListTemp");
                if (!Directory.Exists(ProductDetailsReportFileDirectory))
                {
                    Directory.CreateDirectory(ProductDetailsReportFileDirectory);
                }
                string ProductDetailsReportFilePath = ProductDetailsReportFileDirectory + "\\" + ProductDetailsReportFileName;
                objCrDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, ProductDetailsReportFilePath);
                fileBytes = System.IO.File.ReadAllBytes(ProductDetailsReportFilePath);
                System.IO.File.Delete(ProductDetailsReportFilePath);
                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, Downloadfilename);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~/Areas/Regulatory/Models/Error.txt"));
                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, "InternalError.txt");
            }
        }
        #endregion

        #region Re-Assign Employees
        [CustAuthorization((int)Modules.RA, (int)RA_UserRole.RA_Admin)]
        public ActionResult ReAssignProductList()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = LineNo + " :" + HelpClass.SQLEscapeString(ex.Message);
                return View();
            }
        }
        public JsonResult ReAssignProductListData()
        {
            try
            {
                int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
                int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
                int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
                string sSortDir = Request.Params["sSortDir_0"];
                string sSearch = Request.Params["sSearch"];

                List<ProductDetailsList> objProductDetailsList = new List<ProductDetailsList>();
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbRegulatory = new AizantIT_DevEntities())
                {
                    var objProductDetailsListData = dbRegulatory.AizantIT_SP_ProductReAssignList(
                               displayLength,
                               displayStart,
                               sortCol,
                               sSortDir,
                               sSearch != null ? sSearch.Trim() : "");

                    foreach (var item in objProductDetailsListData)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objProductDetailsList.Add(new ProductDetailsList(Convert.ToInt32(item.RowNumber),
                            Convert.ToInt32(item.DossierID), Convert.ToInt32(item.DossierCurrentStatusID),
                            item.ProductName, item.ApplicationNumber,
                            item.ApplicantName, item.ClientName, item.CurrentStatus, item.DossierStatus, item.Author,
                                               item.Approver));
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objProductDetailsList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [CustAuthorization((int)Modules.RA, (int)RA_UserRole.RA_Admin)]
        public ActionResult ReassignRegulatoryEmployees(int ProductID)
        {
            DossierViewModel objViewDossierDetails = new DossierViewModel();
            try
            {
                using (AizantIT_DevEntities dbContext = new AizantIT_DevEntities())
                {
                    var ProductDetails = (from T1 in dbContext.AizantIT_ProductDossier
                                          join T2 in dbContext.AizantIT_ProductMaster on T1.ProductID equals T2.ProductID
                                          join T3 in dbContext.AizantIT_Client on T1.ClientID equals T3.ClientID
                                          join T4 in dbContext.AizantIT_EmpMaster on T1.AuthorEmpID equals T4.EmpID
                                          join T5 in dbContext.AizantIT_EmpMaster on T1.ApproverEmpID equals T5.EmpID
                                          where T1.ProductDossierID == ProductID
                                          select new
                                          {
                                              T2.ProductName,
                                              T1.ApplicationNum,
                                              T1.ApplicantID,
                                              T3.ClientName,
                                              T1.AuthorEmpID,
                                              T1.ApproverEmpID,
                                              CurrentStatus = (
                                                          T1.CurrentStatus == 1 ? "Product Details Created" :
                                                          T1.CurrentStatus == 2 ? "Submitted For Approval" :
                                                          T1.CurrentStatus == 3 ? "Modified" :
                                                          T1.CurrentStatus == 4 ? "Reverted" :
                                                          T1.CurrentStatus == 5 ? "Review Accepted Date Submitted" :
                                                          T1.CurrentStatus == 6 ? "Goal Date Submitted" :
                                                          T1.CurrentStatus == 7 ? "Product Details Approved by RA Approver" :
                                                          T1.CurrentStatus == 8 ? "Dossier Completed" :
                                                          T1.CurrentStatus == 9 ? "Re - Assigned Author" :
                                                          T1.CurrentStatus == 10 ? "Re - Assigned Approver" :
                                                          "N/A"
                                                          ),
                                              T1.DossierStatus,
                                              DossierAuthorName = T4.FirstName + " " + T4.LastName,
                                              DosierApproverName = T5.FirstName + " " + T5.LastName
                                          }
                                          ).SingleOrDefault();

                    objViewDossierDetails.ProductName = ProductDetails.ProductName;
                    objViewDossierDetails.ApplicationNumber = ProductDetails.ApplicationNum;
                    objViewDossierDetails.ApplicantName = ProductDetails.ApplicantID.ToString();
                    objViewDossierDetails.ClinetName = ProductDetails.ClientName;
                    objViewDossierDetails.CurrentStatus = ProductDetails.CurrentStatus;
                    objViewDossierDetails.AuthorID = ProductDetails.AuthorEmpID;
                    objViewDossierDetails.ApproverID = ProductDetails.ApproverEmpID;
                    objViewDossierDetails.DossierStatus = ProductDetails.DossierStatus;
                    objViewDossierDetails.AuthorName = ProductDetails.DossierAuthorName;
                    objViewDossierDetails.ApproverName = ProductDetails.DosierApproverName;

                    objViewDossierDetails.DossierID = ProductID;
                    objViewDossierDetails.RA_LeadList = new Aizant_Bl().GetListofEmpByRole(null, Convert.ToInt32(RA_UserRole.RA_Lead), "A", ProductDetails.AuthorEmpID.ToString());
                    objViewDossierDetails.ApproverList = new Aizant_Bl().GetListofEmpByRole(null, Convert.ToInt32(RA_UserRole.RA_Approver), "A", ProductDetails.ApproverEmpID.ToString());
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = LineNo + " :" + HelpClass.SQLEscapeString(ex.Message);
            }
            return View(objViewDossierDetails);
        }
        [HttpPost]
        public JsonResult ReAssignEmps(RA_ReAssignEmps ReAssignEmps)
        {
            string _Msg = RegulatoryValidationMessages.RA_ErrorMsg11, _MsgType = "error";
            int _UpdatedLeadEmpID = 0, _UpdatedApproverEmpID = 0;
            if (EmployeeActiveStatusChecking(ReAssignEmps.RA_Lead_EmpID, ReAssignEmps.RA_Approver_EmpID, out int statusResult))
            {
                try
                {
                    var ProductDossierData = new AizantIT_DevEntities().AizantIT_ProductDossier.Where(a => a.ProductDossierID == ReAssignEmps.ProductID).Select(a => new
                    {
                        a.DossierStatus,
                        a.CurrentStatus,
                        a.AuthorEmpID,
                        a.ApproverEmpID,
                        a.CreatedAs,
                    }).SingleOrDefault();

                    bool IsApproverUpdatedSuccessfully = false;
                    bool IsRA_LeadUpdatedSuccessfully = false;

                    //To Modify RA Approver
                    if (ReAssignEmps.IsA_ApproverChanged == true)
                    {
                        if (new int[] { 1, 2, 3, 4 }.Contains(ProductDossierData.CurrentStatus))
                        {
                            if (ReAssignEmps.RA_Approver_EmpID != ProductDossierData.ApproverEmpID)
                            {
                                using (AizantIT_DevEntities dbContext = new AizantIT_DevEntities())
                                {
                                    var ObjUpdateProductDossier = dbContext.AizantIT_ProductDossier.Where(a => a.ProductDossierID == ReAssignEmps.ProductID).SingleOrDefault();
                                    ObjUpdateProductDossier.ApproverEmpID = _UpdatedApproverEmpID = ReAssignEmps.RA_Approver_EmpID;
                                    ProductDossierHistoryInsert(dbContext, ReAssignEmps.ProductID, Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]), Convert.ToInt32(RA_UserRole.RA_Admin), Convert.ToInt32(RA_DossierHistoryStatus.Re_Assigned_Approver), ReAssignEmps.Remarks);
                                    IsApproverUpdatedSuccessfully = true;
                                    //Notification Updating
                                    if (new int[] { 2, 5, 6 }.Contains(ProductDossierData.CurrentStatus))
                                    {
                                        int NotitificationID = GetDossierNotificationID(ReAssignEmps.ProductID);
                                        NotificationActionByLinq notificationActionByLinq = new NotificationActionByLinq();
                                        int[] ApproverEmpID = { ReAssignEmps.RA_Approver_EmpID };
                                        notificationActionByLinq.UpdateNotificationEmps(NotitificationID, ApproverEmpID, 1);
                                    }
                                }
                            }
                        }
                    }
                    //To Modify RA Lead
                    if (ReAssignEmps.IsRA_LeadChanged == true)
                    {
                        if (ProductDossierData.CurrentStatus < 8 || (ProductDossierData.CurrentStatus == 8 && ProductDossierData.DossierStatus == "A"))
                        {
                            if (ReAssignEmps.RA_Lead_EmpID != ProductDossierData.AuthorEmpID)
                            {
                                using (AizantIT_DevEntities dbContext = new AizantIT_DevEntities())
                                {
                                    var ObjUpdateProductDossier = dbContext.AizantIT_ProductDossier.Where(a => a.ProductDossierID == ReAssignEmps.ProductID).SingleOrDefault();
                                    ObjUpdateProductDossier.AuthorEmpID = _UpdatedLeadEmpID = ReAssignEmps.RA_Lead_EmpID;

                                    var DF_NotificationIDs = (from DM in dbContext.AizantIT_DeficiencyMaster
                                                              join DL in dbContext.AizantIT_DeficiencyNotifyLink on DM.DeficiencyID equals DL.DeficiencyID
                                                              where DM.ProductDossierID == ReAssignEmps.ProductID && DM.CurrentStatus == 4
                                                              && DM.AuthorID != ReAssignEmps.RA_Lead_EmpID
                                                              select new { DL.NotificationID }).ToList();
                                    //Notification Shifting 
                                    if (DF_NotificationIDs.Count != 0)
                                    {
                                        List<AizantIT_UserNotifications> objNotifications = new List<AizantIT_UserNotifications>();
                                        foreach (var item in DF_NotificationIDs)
                                        {
                                            AizantIT_UserNotifications objAizantIT_UserNotifications = new AizantIT_UserNotifications();
                                            objAizantIT_UserNotifications.NotificationID = Convert.ToInt32(item.NotificationID);
                                            objAizantIT_UserNotifications.EmpID = ReAssignEmps.RA_Lead_EmpID;
                                            objAizantIT_UserNotifications.Status = 1;
                                            objNotifications.Add(objAizantIT_UserNotifications);
                                        }

                                        NotificationActionByLinq objNotificationActionByLinq = new NotificationActionByLinq();
                                        objNotificationActionByLinq.UpdateNotificationEmps(objNotifications);
                                    }
                                    //For the Task ID : 6271
                                    dbContext.AizantIT_DeficiencyMaster
                                    .Where(b => b.ProductDossierID == ReAssignEmps.ProductID && b.DeficiencyStatus == 1).ToList().ForEach(c =>
                                    {
                                        c.AuthorID = ReAssignEmps.RA_Lead_EmpID;
                                    });

                                    ProductDossierHistoryInsert(dbContext, ReAssignEmps.ProductID, Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]), Convert.ToInt32(RA_UserRole.RA_Admin), Convert.ToInt32(RA_DossierHistoryStatus.Re_Assigned_Author), ReAssignEmps.Remarks);
                                    IsRA_LeadUpdatedSuccessfully = true;

                                    if (new int[] { 4, 7 }.Contains(ProductDossierData.CurrentStatus))
                                    {
                                        //Notification Updating
                                        int NotitificationID = GetDossierNotificationID(ReAssignEmps.ProductID);
                                        NotificationActionByLinq notificationActionByLinq = new NotificationActionByLinq();
                                        int[] AuthorEmpID = { ReAssignEmps.RA_Lead_EmpID };
                                        notificationActionByLinq.UpdateNotificationEmps(NotitificationID, AuthorEmpID, 1);
                                    }
                                }
                            }
                        }
                    }

                    if (ReAssignEmps.IsRA_LeadChanged == true && ReAssignEmps.IsA_ApproverChanged == true)
                    {
                        _Msg = RegulatoryValidationMessages.RA_ErrorMsg12;
                        _MsgType = "error";
                        if (IsRA_LeadUpdatedSuccessfully == true && IsApproverUpdatedSuccessfully == true)
                        {
                            _Msg = RegulatoryValidationMessages.RA_ErrorMsg13;
                            _MsgType = "success";
                        }
                        else if (IsRA_LeadUpdatedSuccessfully == true && IsApproverUpdatedSuccessfully == false)
                        {
                            _Msg = RegulatoryValidationMessages.RA_ErrorMsg12;
                            _MsgType = "info";
                        }
                        else if (IsRA_LeadUpdatedSuccessfully == false && IsApproverUpdatedSuccessfully == true)
                        {
                            _Msg = RegulatoryValidationMessages.RA_ErrorMsg14;
                            _MsgType = "info";
                        }
                    }
                    else if (ReAssignEmps.IsRA_LeadChanged == false && ReAssignEmps.IsA_ApproverChanged == true)
                    {
                        _Msg = RegulatoryValidationMessages.RA_ErrorMsg15;
                        _MsgType = "info";
                        if (IsRA_LeadUpdatedSuccessfully == false && IsApproverUpdatedSuccessfully == true)
                        {
                            _Msg = RegulatoryValidationMessages.RA_ErrorMsg16;
                            _MsgType = "success";
                        }
                    }
                    else if (ReAssignEmps.IsRA_LeadChanged == true && ReAssignEmps.IsA_ApproverChanged == false)
                    {
                        _Msg = RegulatoryValidationMessages.RA_ErrorMsg17;
                        _MsgType = "info";
                        if (IsRA_LeadUpdatedSuccessfully == true && IsApproverUpdatedSuccessfully == false)
                        {
                            _Msg = RegulatoryValidationMessages.RA_ErrorMsg18;
                            _MsgType = "success";
                        }
                    }
                }
                catch (Exception ex)
                {
                    int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                    _Msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message); _MsgType = "error";
                }
            }
            else
            {
                _Msg = statusResult == 1 ? RegulatoryValidationMessages.RA_ErrorMsg29 : (statusResult == 2 ? RegulatoryValidationMessages.RA_ErrorMsg30 : RegulatoryValidationMessages.RA_ErrorMsg31);
                _MsgType = "sysError";
            }
            return Json(new { Msg = _Msg, MsgType = _MsgType, UpdatedLeadEmpID = _UpdatedLeadEmpID, UpdatedApproverEmpID = _UpdatedApproverEmpID }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ReAssignObservationHOD(RA_ReAssignEmps ReAssignEmps)
        {
            string Msg = RegulatoryValidationMessages.RA_ErrorMsg11, MsgType = "error";
            try
            {
                using (AizantIT_DevEntities dbContext = new AizantIT_DevEntities())
                {
                    var DeficiencyObservData = dbContext.AizantIT_DF_Observation.Where(a => a.DF_ObservationID == ReAssignEmps.ObservationID).Select(a => new
                    {
                        a.ResponsiblePersonEmpID,
                        a.CurrentStatus
                    }
                   ).SingleOrDefault();
                    if (DeficiencyObservData.CurrentStatus < 3)
                    {
                        if (DeficiencyObservData.ResponsiblePersonEmpID != ReAssignEmps.RA_HOD_EmpID)
                        {
                            var ObjUpdateObservation = dbContext.AizantIT_DF_Observation.Where(a => a.DF_ObservationID == ReAssignEmps.ObservationID).SingleOrDefault();
                            ObjUpdateObservation.ResponsiblePersonEmpID = ReAssignEmps.RA_HOD_EmpID;
                            ObservationHistoryInsert(dbContext, ReAssignEmps.ObservationID, Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]), Convert.ToInt32(RA_UserRole.RA_Admin), Convert.ToInt32(RA_ObservationHistoryStatus.Re_Assigned_ResponsiblePerson), ReAssignEmps.Remarks);
                            //Notification Updating 
                            if (DeficiencyObservData.CurrentStatus == 1)
                            {
                                int NotitificationID = GetObservationNotificationID(ReAssignEmps.ObservationID);
                                NotificationActionByLinq notificationActionByLinq = new NotificationActionByLinq();
                                int[] ResponsiblePersonEmpID = { ReAssignEmps.RA_HOD_EmpID };
                                notificationActionByLinq.UpdateNotificationEmps(NotitificationID, ResponsiblePersonEmpID, 1);
                            }
                            Msg = RegulatoryValidationMessages.RA_ErrorMsg19; MsgType = "success";
                        }
                        else
                        {
                            Msg = RegulatoryValidationMessages.RA_ErrorMsg20; MsgType = "info";
                        }
                    }
                    else
                    {
                        Msg = RegulatoryValidationMessages.RA_ErrorMsg21; MsgType = "info";
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                Msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message); MsgType = "error";
            }
            return Json(new { msg = Msg, msgType = MsgType }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ReassignDeficiencyApprover(int DeficiencyID, int DeficiencyApproverEmpID, int DeficiencyLeadEmpID, string Remarks)
        {
            string Msg = RegulatoryValidationMessages.RA_ErrorMsg11, MsgType = "error";
            int _DeficiencyApproverID = 0; int _DeficiencyLeadEmpID = 0;
            try
            {
                using (AizantIT_DevEntities dbContext = new AizantIT_DevEntities())
                {
                    if (EmployeeActiveStatusChecking(DeficiencyLeadEmpID, DeficiencyApproverEmpID, out int statusResult))
                    {
                        var DeficiencyData = dbContext.AizantIT_DeficiencyMaster.Where(a => a.DeficiencyID == DeficiencyID).Select(a => new
                        {
                            a.ApproverID,
                            a.AuthorID,
                            a.CurrentStatus
                        }
                      ).SingleOrDefault();
                        if (DeficiencyData.CurrentStatus < 5)
                        {
                            if (DeficiencyData.ApproverID != DeficiencyApproverEmpID)
                            {
                                var ObjUpdateDeficiencyMaster = dbContext.AizantIT_DeficiencyMaster.Where(a => a.DeficiencyID == DeficiencyID).SingleOrDefault();
                                ObjUpdateDeficiencyMaster.ApproverID = DeficiencyApproverEmpID;
                                DeficiencyHistoryInsert(dbContext, DeficiencyID, Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]), Convert.ToInt32(RA_UserRole.RA_Admin), Convert.ToInt32(RA_DeficiencyHistoryStatus.Re_Assigned_Approver), Remarks);
                                //Notification Updating
                                if (DeficiencyData.CurrentStatus == 2)
                                {
                                    int NotitificationID = GetDeficiencyNotificationID(DeficiencyID);
                                    NotificationActionByLinq notificationActionByLinq = new NotificationActionByLinq();
                                    int[] ResponsiblePersonEmpID = { DeficiencyApproverEmpID };
                                    notificationActionByLinq.UpdateNotificationEmps(NotitificationID, ResponsiblePersonEmpID, 1);
                                }
                                //Msg = RegulatoryValidationMessages.RA_ErrorMsg22; MsgType = "success";
                                _DeficiencyApproverID = DeficiencyApproverEmpID;
                            }


                            if (DeficiencyData.AuthorID != DeficiencyLeadEmpID)
                            {
                                var ObjUpdateDeficiencyMaster = dbContext.AizantIT_DeficiencyMaster.Where(a => a.DeficiencyID == DeficiencyID).SingleOrDefault();
                                ObjUpdateDeficiencyMaster.AuthorID = DeficiencyLeadEmpID;
                                DeficiencyHistoryInsert(dbContext, DeficiencyID, Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]), Convert.ToInt32(RA_UserRole.RA_Admin), Convert.ToInt32(RA_DeficiencyHistoryStatus.Re_Assigned_Author), Remarks);
                                //Notification Updating
                                if (DeficiencyData.CurrentStatus == 4)
                                {
                                    int NotitificationID = GetDeficiencyNotificationID(DeficiencyID);
                                    NotificationActionByLinq notificationActionByLinq = new NotificationActionByLinq();
                                    int[] ResponsiblePersonEmpID = { DeficiencyLeadEmpID };
                                    notificationActionByLinq.UpdateNotificationEmps(NotitificationID, ResponsiblePersonEmpID, 1);
                                }
                                //Msg = RegulatoryValidationMessages.RA_ErrorMsg25; MsgType = "success";
                                _DeficiencyLeadEmpID = DeficiencyLeadEmpID;
                            }
                            if (_DeficiencyLeadEmpID != 0 && _DeficiencyApproverID != 0)
                            {
                                Msg = RegulatoryValidationMessages.RA_ErrorMsg13; MsgType = "success";
                            }
                            else if (_DeficiencyLeadEmpID != 0 && _DeficiencyApproverID == 0)
                            {
                                Msg = RegulatoryValidationMessages.RA_ErrorMsg25; MsgType = "success";
                            }
                            else if (_DeficiencyLeadEmpID == 0 && _DeficiencyApproverID != 0)
                            {
                                Msg = RegulatoryValidationMessages.RA_ErrorMsg22; MsgType = "success";
                            }
                            else
                            {
                                Msg = RegulatoryValidationMessages.RA_ErrorMsg26; MsgType = "info";
                            }
                        }
                        else
                        {
                            Msg = RegulatoryValidationMessages.RA_ErrorMsg24; MsgType = "error";
                        }
                    }
                    else
                    {
                        Msg = statusResult == 1 ? RegulatoryValidationMessages.RA_ErrorMsg28 : (statusResult == 2 ? RegulatoryValidationMessages.RA_ErrorMsg30 : RegulatoryValidationMessages.RA_ErrorMsg31);
                        MsgType = "Custerror";

                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                Msg = "Error " + LineNo + ":" + HelpClass.SQLEscapeString(ex.Message); MsgType = "error";
            }
            return Json(new { msg = Msg, msgType = MsgType, DeficiencyApproverID = _DeficiencyApproverID, DeficiencyLeadID = _DeficiencyLeadEmpID }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ReAssignDeficiencyList(int ProductDossierId)
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            List<DeficiencyList> objDeficiencyList = new List<DeficiencyList>();
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbRegulatory = new AizantIT_DevEntities())
                {
                    var DeficiencyList = dbRegulatory.AizantIT_SP_DeficiencyListForReAssignEmps(
                               displayLength,
                               displayStart,
                               sortCol,
                               sSortDir,
                               sSearch != null ? sSearch.Trim() : "", ProductDossierId
                               );
                    foreach (var item in DeficiencyList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objDeficiencyList.Add(new DeficiencyList(Convert.ToInt32(item.RowNumber),
                            Convert.ToInt32(item.DeficiencyID), item.TypeofDeficiency, item.DeficiencyCode, item.DeficiencyStatus,
                            item.DeficiencyCurrentStatus, Convert.ToInt32(item.CurrentStatusID), Convert.ToInt32(item.DeficiencyApproverID),
                            Convert.ToInt32(item.DeficiencyLeadID), item.DeficiencyAuthorName, item.DeficiencyApproverName));
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objDeficiencyList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ReAssignObservationList(int DeficiencyID)
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            List<ObservationList> objReAssignObservationList = new List<ObservationList>();
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbRegulatory = new AizantIT_DevEntities())
                {
                    var ReAssignObservationList = dbRegulatory.AizantIT_SP_ObservationListForReAssignEmps(
                               displayLength,
                               displayStart,
                               sortCol,
                               sSortDir,
                               sSearch != null ? sSearch.Trim() : "", DeficiencyID
                               );
                    foreach (var item in ReAssignObservationList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objReAssignObservationList.Add(new ObservationList(Convert.ToInt32(item.RowNumber),
                            Convert.ToInt32(item.ObservationID), item.FunctionalArea, item.DepartmentName, item.ResponsiblePerson,
                            item.ObservationStatus, item.ObservationCurrentStatus, Convert.ToInt32(item.ObservationCurrentStatusID), Convert.ToInt32(item.ResponsiblePersonEmpID)));
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objReAssignObservationList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        //Responsible Person   bind On Observation List in-Line DropdownList 
        public JsonResult GetObservationResponsePersonList(int ObservationID)
        {
            Aizant_Bl objAizant_Bl = new Aizant_Bl();
            var dbContext = new AizantIT_DevEntities();
            var ObservationDetails = dbContext.AizantIT_DF_Observation.Where(a => a.DF_ObservationID == ObservationID).Select(a => new { a.DepartmentID, a.ResponsiblePersonEmpID }).SingleOrDefault();
            var ResponsiblePersonList = objAizant_Bl.GetListofEmpByDeptIDRoleIDAndStatus(null, ObservationDetails.DepartmentID, Convert.ToInt32(RA_UserRole.RA_HOD), "A", ObservationDetails.ResponsiblePersonEmpID.ToString());
            return Json(ResponsiblePersonList, JsonRequestBehavior.AllowGet);
        }
        #endregion
        //for THis Deficiency  Approver  if Exist in  Observation Table  or not  if Exist  Return The True 
        public bool ApproverModificationinDeficiency(int DeficiencyID, int ApproverEmpID)
        {
            using (var dbContext = new AizantIT_DevEntities())
            {
                return dbContext.AizantIT_DF_Observation.Where(a => a.DeficiencyID == DeficiencyID && a.Status == 1).Any(a => a.ResponsiblePersonEmpID == ApproverEmpID);
            }
        }

        public bool EmployeeActiveStatusChecking(int Lead_EmpID, int Approver_EmpID, out int Status)
        {
            bool Result = true;
            Status = 0;
            using (var dbContext = new AizantIT_DevEntities())
            {
                var ralead = dbContext.AizantIT_EmpMaster.Any(a => a.EmpID == Lead_EmpID && a.Status == "A");
                var raApprover = dbContext.AizantIT_EmpMaster.Any(a => a.EmpID == Approver_EmpID && a.Status == "A");
                if (!ralead && !raApprover)
                {
                    Result = false;
                    Status = 1;
                }
                else if (!ralead)
                {
                    Result = false;
                    Status = 2;
                }
                else if (!raApprover)
                {
                    Result = false;
                    Status = 3;
                }
            }
            return Result;
        }
    }

}