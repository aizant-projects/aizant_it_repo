﻿using Aizant_API_Entities;
using AizantIT_PharmaApp.Areas.Regulatory.Controllers.RegulatoryLinq;
using AizantIT_PharmaApp.Areas.Regulatory.Models;
using AizantIT_PharmaApp.Areas.Regulatory.Models.RegulatoryViewModel;
using AizantIT_PharmaApp.Common;
using AizantIT_PharmaApp.Common.CustomFilters;
using AizantIT_PharmaApp.Controllers.Aizant_BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.Areas.Regulatory.Controllers
{
    [Route("Regulatory/RegulatoryMasterController/{action}")]
    [RouteArea]
    [CustAuthentication]
    public class RegulatoryMasterController : Controller
    {
        // GET: Regulatory/RegulatoryMaster
        [CustAuthorization((int)Modules.RA,
        (int)RA_UserRole.RA_Admin)]
        public ActionResult RegulatoryMasterList()
        {
            return View();
        }
        #region Type of Deficiency
        public PartialViewResult GetTypeOfDeficiency()
        {
            return PartialView();
        }
        public ActionResult GetTypeOfDeficiencyList()
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            List<TypeOfDeficiencyList> objTypeOfDeficiencyList = new List<TypeOfDeficiencyList>();
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbRegulatory = new AizantIT_DevEntities())
                {
                    var TypeOfDeficiencyList = dbRegulatory.AizantIT_SP_GetTypeOfDeficiencyList(
                               displayLength,
                               displayStart,
                               sortCol,
                               sSortDir,
                               sSearch != null ? sSearch.Trim() : ""
                               );
                    foreach (var item in TypeOfDeficiencyList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objTypeOfDeficiencyList.Add(new TypeOfDeficiencyList(Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.TypeOfDeficiencyID), item.TypeOfDeficiencyName, Convert.ToBoolean(item.IsActive), item.Status));
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objTypeOfDeficiencyList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Create_TOD(string DeficiencyName, string Remarks)
        {
            try
            {
                using (var dbRegulatory = new AizantIT_DevEntities())
                {
                    int ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    ObjectParameter objParm = new ObjectParameter("ActionStatus", typeof(int));
                    dbRegulatory.AizantIT_SP_TypeOfDeficiencyInsert(DeficiencyName, Remarks, ActionBy, objParm);
                    int result = Convert.ToInt32(objParm.Value);
                    return Json(new { _result = result }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult UpdateDeficiency(int DeficiencyID, string DeficiencyName, string Remarks, bool Status)
        {
            try
            {
                using (var dbRegulatory = new AizantIT_DevEntities())
                {
                    int ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                    ObjectParameter objParm = new ObjectParameter("ActionStatus", typeof(int));
                    dbRegulatory.AizantIT_SP_TypeOfDeficiencyUpdated(DeficiencyID, DeficiencyName.Trim(), Remarks, Status, ActionBy, objParm);
                    int result = Convert.ToInt32(objParm.Value);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetTypeOfDeficiencyHistoryList(int DeficiencyID)
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir =Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            List<RegulatoryMasterHistoryList> objTypeOfDeficiencyHistoryList = new List<RegulatoryMasterHistoryList>();
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbRegulatory = new AizantIT_DevEntities())
                {
                    var TypeOfDeficiencyList = dbRegulatory.AizantIT_SP_TypeOfDeficiencyHistory(
                               displayLength,
                               displayStart,
                               sortCol,
                               sSortDir,
                               sSearch != null ? sSearch.Trim() : "", DeficiencyID
                               );
                    foreach (var item in TypeOfDeficiencyList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objTypeOfDeficiencyHistoryList.Add(new RegulatoryMasterHistoryList(Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.TOD_History_ID), item.ActionBy, item.ActionRole, item.ActionDate, item.ActionStatus, item.Comments));
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objTypeOfDeficiencyHistoryList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult BindTypeOfDeficiency(int DeficiencyID=0)
        {
            using (AizantIT_DevEntities objaizantIT_DevEntities= new AizantIT_DevEntities())
            {
                var GetDeficiency_Data = (from A in objaizantIT_DevEntities.AizantIT_TypeOfDeficiency
                                          where A.TypeOfDeficiencyID == DeficiencyID
                                          select new
                                          {
                                              TypeOfDeficiencyID = A.TypeOfDeficiencyID,
                                              TypeOfDeficiencyName = A.TypeOfDeficiencyName,
                                              IsActive = A.IsActive
                                          }).SingleOrDefault();
                return Json(new { hasError = false,data = GetDeficiency_Data},JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Functional Area
        public PartialViewResult GetFunctionalArea()
        {
            return PartialView();
        }
        public ActionResult GetFunctionAreaList()
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            List<FunctionalAreaList> objFunctionalAreaList = new List<FunctionalAreaList>();
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbRegulatory = new AizantIT_DevEntities())
                {
                    var TypeOfDeficiencyList = dbRegulatory.AizantIT_SP_GetFunctionalAreaList(
                               displayLength,
                               displayStart,
                               sortCol,
                               sSortDir,
                               sSearch != null ? sSearch.Trim() : ""
                               );

                    foreach (var item in TypeOfDeficiencyList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objFunctionalAreaList.Add(new FunctionalAreaList(Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.FunctionalAreaID), item.FunctionalAreaName, Convert.ToBoolean(item.IsActive), item.DeptCode, item.DeptID, item.Status));
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objFunctionalAreaList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult CreateFunctionalArea(string FunctionalAreaName, string Remarks, int [] SelectedDetIDs)
        {
            try
            {
                DataTable _dtDeptIDs = AddDeptIDsCreate(SelectedDetIDs);
                int ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                RegulatoryLinqQuery obRegulatoryLinqQuery = new RegulatoryLinqQuery();
                obRegulatoryLinqQuery.CreateFunctionalArea(FunctionalAreaName.Trim(), Remarks, ActionBy, _dtDeptIDs, out int ActionStatus);
                int result = ActionStatus;
                return Json(result , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult UpdateFunctionalArea(int FunctionalAreaID, string FunctionalAreaName, string Remarks, bool Status, int[] SelectedDetIDs)
        {
            try
            {
                DataTable _dtDeptIDs = AddDeptIDsCreate(SelectedDetIDs);
                int ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                RegulatoryLinqQuery obRegulatoryLinqQuery = new RegulatoryLinqQuery();
                obRegulatoryLinqQuery.UpdateFunctionalArea(FunctionalAreaID, FunctionalAreaName.Trim(), Remarks, Status, ActionBy, _dtDeptIDs, out int ActionStatus);
                int result = ActionStatus;
                return Json(result , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        private DataTable AddDeptIDsCreate(int[] DeptIDs)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("DeptID");
            foreach (var DeptID in DeptIDs)
            {
                dt.Rows.Add(DeptID);
            }
            return dt;
        }
        public ActionResult GetFunctionalAreaHistoryList(int FunctionalAreaID)
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            List<RegulatoryMasterHistoryList> objFunctionalAreaHistoryList = new List<RegulatoryMasterHistoryList>();

            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbRegulatory = new AizantIT_DevEntities())
                {
                    var TypeOfDeficiencyList = dbRegulatory.AizantIT_SP_FunctionalAreaHistory(
                               displayLength,
                               displayStart,
                               sortCol,
                               sSortDir,
                               sSearch != null ? sSearch.Trim() : "", FunctionalAreaID
                               );
                    foreach (var item in TypeOfDeficiencyList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objFunctionalAreaHistoryList.Add(new RegulatoryMasterHistoryList(Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.FA_HistoryID), item.ActionBy, item.ActionRole, item.ActionDate, item.ActionStatus, item.Comments==""?"N/A":item.Comments));
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objFunctionalAreaHistoryList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                //ViewBag.SysError = LineNo + " :" + ex.Message.Replace("'", "\'");
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult BindFunctionalArea(int FunctionalAreaID = 0)
        {
            using (AizantIT_DevEntities objaizantIT_DevEntities = new AizantIT_DevEntities())
            {
                var GetFunctionalArea_Data = (from F in objaizantIT_DevEntities.AizantIT_FunctionalArea
                                              join DF in objaizantIT_DevEntities.AizantIT_FunctionalAreaDept
                                              on F.FunctionalAreaID equals DF.FunctionalAreaID 
                                              where F.FunctionalAreaID == FunctionalAreaID
                                              select new
                                              {
                                                  FunctionalAreaID = F.FunctionalAreaID,
                                                  FunctionalAreaName = F.FunctionalAreaName,
                                                  IsActive = F.IsActive,
                                                  DeptIds = DF.DeptID
                                              }).ToList();
                return Json(new { hasError = false, data = GetFunctionalArea_Data}, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region DrugSubstance
        public PartialViewResult GetDrugSubstanceMainList()
        {
            return PartialView("_GetDrugSubstanceMainList");
        }
        public JsonResult GetDrugSubstanceList()
        {
            int sDisplayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int sDisplayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sSortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            List<DrugSubstanceList> objDrugSubstanceList = new List<DrugSubstanceList>();
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities objPMS_Entitie = new AizantIT_DevEntities())
                {
                    var DrugSubstanceDetails = objPMS_Entitie.AizantIT_SP_GetDrugSubstanceList(
                        sDisplayLength,
                        sDisplayStart,
                        sSortCol,
                        sSortDir,
                          sSearch != null ? sSearch.Trim() : "");
                    foreach (var item in DrugSubstanceDetails)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objDrugSubstanceList.Add(new DrugSubstanceList(Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.DrugSubstanceID),
                            item.DrugSubstanceName, item.DrugSubstanceDescription, Convert.ToBoolean(item.isActive), Convert.ToInt32(item.DescriptionLength), item.Status));
                    }
                }
                return Json(
                    new
                    {
                        iTotalRecords = totalRecordsCount,
                        iTotalDisplayRecords = totalRecordsCount,
                        aaData = objDrugSubstanceList
                    }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult AddDrugSubstance(string DrugSubstanceName, string DrugSubstanceDescription)
        {
            try
            {
                using (AizantIT_DevEntities objDEV_Entities = new AizantIT_DevEntities())
                {
                    bool drugName = objDEV_Entities.AizantIT_DrugSubstance.Any(x => x.DrugSubstanceName.ToLower().Trim() == DrugSubstanceName.ToLower().Trim());
                    if (drugName)
                    {
                        return Json(new { hasError = "Warning" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        using (DbContextTransaction transaction = objDEV_Entities.Database.BeginTransaction())
                        {
                            try
                            {
                                AizantIT_DrugSubstance objDrugSubSatnce = new AizantIT_DrugSubstance();
                                objDrugSubSatnce.DrugSubstanceName = DrugSubstanceName;
                                objDrugSubSatnce.Description = DrugSubstanceDescription;
                                objDrugSubSatnce.IsActive = true;
                                objDEV_Entities.AizantIT_DrugSubstance.Add(objDrugSubSatnce);
                                objDEV_Entities.SaveChanges();
                                //History Insert
                                DrugSubstanceHistoryInsert(objDEV_Entities, objDrugSubSatnce.DrugSubstanceID, 1);
                                transaction.Commit();
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                throw ex;
                            }
                        }
                    }
                    return Json(new { hasError = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetDrugSubstanceData(int DrugSubstanceID)
        {
            AizantIT_DevEntities objDEV_Entities = new AizantIT_DevEntities();
            var DrugSubstance = objDEV_Entities.AizantIT_DrugSubstance.Where(s => s.DrugSubstanceID == DrugSubstanceID).Select(p=>new {p.DrugSubstanceID,p.DrugSubstanceName,p.Description,p.IsActive}).SingleOrDefault();
            return Json(DrugSubstance , JsonRequestBehavior.AllowGet);
        }
        public ActionResult UpdateDrugSubstance(string DrugSubstanceName, string DurgSubstanceDescription, bool IsActive, string Comment, int DrugSubsatanceID)
        {
            try
            {
                using (AizantIT_DevEntities objDEV_Entities = new AizantIT_DevEntities())
                {
                    bool drugName = objDEV_Entities.AizantIT_DrugSubstance.Any(x => x.DrugSubstanceName.ToLower().Trim() == DrugSubstanceName.ToLower().Trim() && x.DrugSubstanceID != DrugSubsatanceID);
                    if (drugName)
                    {
                        return Json(new { hasError = "Warning" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        using (DbContextTransaction transaction = objDEV_Entities.Database.BeginTransaction())
                        {
                            try
                            {
                                AizantIT_DrugSubstance objDrugSubstance = objDEV_Entities.AizantIT_DrugSubstance.SingleOrDefault(id => id.DrugSubstanceID == DrugSubsatanceID);
                                objDrugSubstance.DrugSubstanceName = DrugSubstanceName;
                                objDrugSubstance.Description = DurgSubstanceDescription;
                                objDrugSubstance.IsActive = IsActive;
                                objDEV_Entities.SaveChanges();
                                //History Insert
                                DrugSubstanceHistoryInsert(objDEV_Entities, objDrugSubstance.DrugSubstanceID, 2, Comment);
                                transaction.Commit();
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                throw ex;
                            }
                        }
                    }
                    return Json(new { hasError = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetDrugSubstanceHistoryList(int DrugSubstanceID)
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir =Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            List<RegulatoryMasterHistoryList> objDrugSubstanceHistoryList = new List<RegulatoryMasterHistoryList>();
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbRegulatory = new AizantIT_DevEntities())
                {
                    var DrugSubstanceList = dbRegulatory.AizantIT_SP_DrugSubStanceHistory(
                               displayLength,
                               displayStart,
                               sortCol,
                               sSortDir,
                               sSearch != null ? sSearch.Trim() : "", DrugSubstanceID
                               );
                    foreach (var item in DrugSubstanceList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objDrugSubstanceHistoryList.Add(new RegulatoryMasterHistoryList(Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.History_ID), item.ActionBy, item.ActionRole, item.ActionDate, item.ActionStatus, item.Comments));
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objDrugSubstanceHistoryList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region DP_Manufacturing
        public PartialViewResult GetDPManufacturingSite()
        {
            return PartialView();
        }
        public ActionResult AddDPManufacturing(DPManufacturingViewModel objCreateDPManufacturingSite)
        {
            try
            {
                using (AizantIT_DevEntities objDEV_Entities = new AizantIT_DevEntities())
                {
                    int Result = DPManufacturing_NameAndDPManufacturing_FEINumberChecking(objDEV_Entities, objCreateDPManufacturingSite);
                    if (Result == 0)
                    {
                        using (DbContextTransaction transaction = objDEV_Entities.Database.BeginTransaction())
                        {
                            try
                            {
                                AizantIT_DP_ManufacturingSite objDPManufacturing = new AizantIT_DP_ManufacturingSite();
                                objDPManufacturing.DP_ManufacturingSiteName = objCreateDPManufacturingSite.DPManufactringName;
                                objDPManufacturing.DP_FEINumber = objCreateDPManufacturingSite.FEINumber;
                                objDPManufacturing.Description = objCreateDPManufacturingSite.DPManufactringDescription;
                                objDPManufacturing.IsActive = true;
                                objDEV_Entities.AizantIT_DP_ManufacturingSite.Add(objDPManufacturing);
                                objDEV_Entities.SaveChanges();
                                //History Insert
                                DP_ManufacturingSiteHistoryInsert(objDEV_Entities, objDPManufacturing.DP_ManufacturingSiteID, 1);
                                transaction.Commit();
                                return Json(new { hasError = true,_ResultType= Result }, JsonRequestBehavior.AllowGet);
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                throw ex;
                            }
                        }
                    }              
                    else
                    {
                        return Json(new { hasError = true, _ResultType = Result}, JsonRequestBehavior.AllowGet);
                    }                    
                }
            }
            catch (Exception ex)
            {
                return Json(new { hasError = false, data = HelpClass.SQLEscapeString(ex.Message) }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult GetDPManufacturingData(int DPManufacturingID)
        {
            AizantIT_DevEntities objDEV_Entities = new AizantIT_DevEntities();
            var objDPManufacturing = objDEV_Entities.AizantIT_DP_ManufacturingSite.Where(dp => dp.DP_ManufacturingSiteID == DPManufacturingID).Select(dpf=>new { dpf.DP_ManufacturingSiteID,dpf.DP_ManufacturingSiteName,dpf.DP_FEINumber,dpf.Description,dpf.IsActive}).SingleOrDefault();
            return Json(objDPManufacturing, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UpdateDPManufacturing(DPManufacturingViewModel objUpdateDPManufacturingSite)//string DPManufactringName, string DPManufactringDescription, bool IsActive, string Comment, int DPManufacturingID
        {
            try
            {
                using (AizantIT_DevEntities objDEV_Entities = new AizantIT_DevEntities())
                {
                    int Result = DPManufacturing_NameAndDPManufacturing_FEINumberChecking(objDEV_Entities, objUpdateDPManufacturingSite);
                    if (Result!=0)
                    {
                        return Json(new { hasError = true,_ResultType = Result }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        using (DbContextTransaction transaction = objDEV_Entities.Database.BeginTransaction())
                        {
                            try
                            {
                                AizantIT_DP_ManufacturingSite objDPManufacturing = objDEV_Entities.AizantIT_DP_ManufacturingSite.SingleOrDefault(p => p.DP_ManufacturingSiteID == objUpdateDPManufacturingSite.DPManufacturingID);
                                objDPManufacturing.DP_ManufacturingSiteName = objUpdateDPManufacturingSite.DPManufactringName;
                                objDPManufacturing.DP_FEINumber = objUpdateDPManufacturingSite.FEINumber;
                                objDPManufacturing.Description = objUpdateDPManufacturingSite.DPManufactringDescription;
                                objDPManufacturing.IsActive = objUpdateDPManufacturingSite.IsActive;
                                objDEV_Entities.SaveChanges();
                                //History Insert
                                DP_ManufacturingSiteHistoryInsert(objDEV_Entities, objDPManufacturing.DP_ManufacturingSiteID, 2, objUpdateDPManufacturingSite.Comments);
                                transaction.Commit();
                                return Json(new { hasError = true, _ResultType = Result }, JsonRequestBehavior.AllowGet);
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                throw ex;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { hasError = false, data = HelpClass.SQLEscapeString(ex.Message) }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetDPManufacturingList()
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            List<DPManufacturSIteList> objDPManufacturSiteList = new List<DPManufacturSIteList>();
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbRegulatory = new AizantIT_DevEntities())
                {
                    var DPManufacturList = dbRegulatory.AizantIT_SP_GetDPManufacturingSiteList(
                               displayLength,
                               displayStart,
                               sortCol,
                               sSortDir,
                               sSearch != null ? sSearch.Trim() : ""
                               );
                    foreach (var item in DPManufacturList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objDPManufacturSiteList.Add(new DPManufacturSIteList(Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.DP_ManufacturingSiteID), item.DP_ManufacturingSiteName,item.DP_FEINumber,item.Description, Convert.ToBoolean(item.isActive), Convert.ToInt32(item.DescriptionLength), item.Status));
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objDPManufacturSiteList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetDPManufacturingHistoryList(int DPManufacturingID)
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir =Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            List<RegulatoryMasterHistoryList> objDrugSubstanceHistoryList = new List<RegulatoryMasterHistoryList>();
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbRegulatory = new AizantIT_DevEntities())
                {
                    var DPManufacturList = dbRegulatory.AizantIT_SP_DPManufacturingSiteHistory(
                                           displayLength,
                                           displayStart,
                                           sortCol,
                                           sSortDir,
                                           sSearch != null ? sSearch.Trim() : "", DPManufacturingID
                                           );

                    foreach (var item in DPManufacturList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objDrugSubstanceHistoryList.Add(new RegulatoryMasterHistoryList(Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.HistoryID), item.ActionBy, item.ActionRole, item.ActionDate, item.ActionStatus, item.Comments));
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objDrugSubstanceHistoryList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public int DPManufacturing_NameAndDPManufacturing_FEINumberChecking(AizantIT_DevEntities objDev_Entitie, DPManufacturingViewModel objDPManufacturingModel)
        {
            int _Result = 0;
            bool dpManufacturingName = objDev_Entitie.AizantIT_DP_ManufacturingSite.Any(x => x.DP_ManufacturingSiteName.ToLower().Trim() == objDPManufacturingModel.DPManufactringName.ToLower().Trim() && x.DP_ManufacturingSiteID != objDPManufacturingModel.DPManufacturingID);
            bool dpManufacturingFEINumber = objDev_Entitie.AizantIT_DP_ManufacturingSite.Any(x => x.DP_FEINumber.ToLower().Trim() == objDPManufacturingModel.FEINumber.ToLower().Trim() && x.DP_ManufacturingSiteID != objDPManufacturingModel.DPManufacturingID);
            if (dpManufacturingName)
            {
                _Result = 1;//dpManufactuirngName Exist
            }
            if (dpManufacturingFEINumber)
            {
                _Result = 2;//dpManufacturingFEINumber Exist
            }
            if (dpManufacturingName && dpManufacturingFEINumber)
            {
                _Result = 3; //dpManufactuirngName and  dpManufacturingFEINumber Exit
            }
            return _Result;
        }
        #endregion

        #region DMF_Holder
        public PartialViewResult GetDMFHolder()
        {
            return PartialView();
        }
        public JsonResult GetDMFHolderMainList()
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];

            List<DMFHolderList> objDMFHolderList = new List<DMFHolderList>();

            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities objDevEntities = new AizantIT_DevEntities())
                {

                    var DMFHolderList = objDevEntities.AizantIT_SP_GetDMFHolderList(
                               displayLength,
                               displayStart,
                               sortCol,
                               sSortDir,
                               sSearch != null ? sSearch.Trim() : ""
                               ); 
                    foreach (var item in DMFHolderList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objDMFHolderList.Add(new DMFHolderList(
                            Convert.ToInt32(item.RowNumber),
                            Convert.ToInt32(item.DMFID),
                            item.DMFCode,
                            item.DMFHolderName,
                            item.CountryName,
                            item.StateName,
                            item.CityName,
                            item.PinCode,
                            item.Phone,
                            item.EmailID,
                            item.Addres,
                            Convert.ToBoolean(item.isActive), item.Status
                            ));
                    }
                    return Json(new
                    {
                        iTotalRecords = totalRecordsCount,
                        iTotalDisplayRecords = totalRecordsCount,
                        aaData = objDMFHolderList
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = "Yes";
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult AddDMFHolderDetails(DMFHolderViewModel objDMFHolder)
        {
            try
            {
                var data = ValidationsDMFHolder(objDMFHolder);
                using (AizantIT_DevEntities objDEVEntitie = new AizantIT_DevEntities())
                {
                    if (data!="")
                    {
                        return Json(new { hasError = "Warning", _data = data }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        using (DbContextTransaction transaction = objDEVEntitie.Database.BeginTransaction())
                        {
                            try
                            {
                                AizantIT_DMF_Holder objAizantDMFHolder = new AizantIT_DMF_Holder();
                                objAizantDMFHolder.DMF_HolderName = objDMFHolder.DMFHolderName;
                                objAizantDMFHolder.DMF_Code = objDMFHolder.DMFCode;
                                objAizantDMFHolder.CountryID = objDMFHolder.CountryID == 0 ? null : objDMFHolder.CountryID;
                                objAizantDMFHolder.StateID = objDMFHolder.StateID == 0 ? null : objDMFHolder.StateID;
                                objAizantDMFHolder.CityID = objDMFHolder.CityID == 0 ? null : objDMFHolder.CityID;
                                objAizantDMFHolder.PinCode = objDMFHolder.PinCode;
                                objAizantDMFHolder.Phone = objDMFHolder.Phone;
                                objAizantDMFHolder.LandlineAreaCode = objDMFHolder.landLineAreaCode;
                                objAizantDMFHolder.LandlineNum = objDMFHolder.landLineNumber;
                                objAizantDMFHolder.E_mail = objDMFHolder.DMFEmail;
                                objAizantDMFHolder.Address = objDMFHolder.Address;
                                objAizantDMFHolder.IsActive = true;
                                objDEVEntitie.AizantIT_DMF_Holder.Add(objAizantDMFHolder);
                                objDEVEntitie.SaveChanges();
                                //History
                                DMFHolderHistoryInsert(objDEVEntitie, objAizantDMFHolder.DMF_HolderID, 1, objDMFHolder.Comment);
                                transaction.Commit();
                                return Json(new { hasError = false }, JsonRequestBehavior.AllowGet);
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                throw ex;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { hasError = true, msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult BindDataDMFHolder(int DMF_HolderID = 0)
        {
            Aizant_Bl objAizant_Bl = new Aizant_Bl();
            try
            {
                using (AizantIT_DevEntities objDEVEntities = new AizantIT_DevEntities())
                {
                    var _DMF_Holder_Data = (from D in objDEVEntities.AizantIT_DMF_Holder
                                            where D.DMF_HolderID == DMF_HolderID
                                            select new
                                            {
                                                DMFID = D.DMF_HolderID,
                                                DMFHolderName = D.DMF_HolderName,
                                                DMFCode = D.DMF_Code,
                                                CountryID = D.CountryID == null ? 0 : D.CountryID,
                                                StateID = D.StateID ==null ? 0 : D.StateID,
                                                CityID = D.CityID,
                                                PinCode = D.PinCode,
                                                Phone = D.Phone,
                                                landLineAreaCode = D.LandlineAreaCode,
                                                landLineNumber = D.LandlineNum,
                                                DMFEmail = D.E_mail,
                                                Address = D.Address,
                                                isActive = D.IsActive
                                            }).SingleOrDefault();
                    return Json(new { hasError = false, data = _DMF_Holder_Data,
                        CountryList = objAizant_Bl.BindCountries(_DMF_Holder_Data.CountryID.ToString()),
                        StateList = objAizant_Bl.BindCountryWiseStatesList(_DMF_Holder_Data.StateID.ToString(), _DMF_Holder_Data.CountryID.ToString()),
                       CityList = objAizant_Bl.BindStateWiseCityList(_DMF_Holder_Data.CityID.ToString(), _DMF_Holder_Data.StateID.ToString())

                }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult UpdateDMFHolder(DMFHolderViewModel objDMFHolder)
        {
            try
            {
               var data = ValidationsDMFHolder(objDMFHolder);
                using (AizantIT_DevEntities objDEVEntitie = new AizantIT_DevEntities())
                {
                    if(data!="")
                    {
                           return Json(new { hasError = "Warning", _data = data }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        using (DbContextTransaction transaction = objDEVEntitie.Database.BeginTransaction())
                        {

                            try
                            {
                                var objAizantDMFHolder = objDEVEntitie.AizantIT_DMF_Holder.Where(id => id.DMF_HolderID == objDMFHolder.DMFID).SingleOrDefault();
                                objAizantDMFHolder.DMF_HolderName = objDMFHolder.DMFHolderName;
                                objAizantDMFHolder.DMF_Code = objDMFHolder.DMFCode;
                                objAizantDMFHolder.CountryID = objDMFHolder.CountryID == 0 ? null : objDMFHolder.CountryID; 
                                objAizantDMFHolder.StateID = objDMFHolder.StateID == 0 ? null : objDMFHolder.StateID;
                                objAizantDMFHolder.CityID = objDMFHolder.CityID==0 ? null : objDMFHolder.CityID;
                                objAizantDMFHolder.PinCode = objDMFHolder.PinCode;
                                objAizantDMFHolder.LandlineAreaCode = objDMFHolder.landLineAreaCode;
                                objAizantDMFHolder.LandlineNum = objDMFHolder.landLineNumber;
                                objAizantDMFHolder.Phone = objDMFHolder.Phone;
                                objAizantDMFHolder.E_mail = objDMFHolder.DMFEmail;
                                objAizantDMFHolder.Address = objDMFHolder.Address;
                                objAizantDMFHolder.IsActive = objDMFHolder.isActive;
                                objDEVEntitie.SaveChanges();
                                //History Insert
                                DMFHolderHistoryInsert(objDEVEntitie, objAizantDMFHolder.DMF_HolderID, 2, objDMFHolder.Comment);
                                transaction.Commit();
                                return Json(new { hasError = false }, JsonRequestBehavior.AllowGet);
                            }
                            catch (Exception)
                            {
                                transaction.Rollback();
                                throw;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { hasError = true, msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public string ValidationsDMFHolder(DMFHolderViewModel objDMFHolder)
        {
            string Result = "";
            AizantIT_DevEntities objDEVEntitie = new AizantIT_DevEntities();
            bool LandlineCode = false, LandlineNumber = false, MobileNumber = false, Email = false;
            string _DMFCode = "DMF_Code", _DMFName = "DMF_Name", _DMFCodeandName = "DMF_CodeName", _LandlineNumber = "LN_Number", _MobileNumber = "MobileNO", _Email = "Email";
            bool DMFCode = objDEVEntitie.AizantIT_DMF_Holder.Any(y => y.DMF_Code.Trim() == objDMFHolder.DMFCode.Trim() && y.DMF_HolderID != objDMFHolder.DMFID);
            bool DMFName = objDEVEntitie.AizantIT_DMF_Holder.Any(x => x.DMF_HolderName.Trim() == objDMFHolder.DMFHolderName.Trim() && x.DMF_HolderID != objDMFHolder.DMFID);

            if (objDMFHolder.landLineNumber!=null)
            {
                LandlineNumber = objDEVEntitie.AizantIT_DMF_Holder.Any(x => x.LandlineNum.Trim() == objDMFHolder.landLineNumber.Trim() && x.DMF_HolderID != objDMFHolder.DMFID);
            }
            if(objDMFHolder.landLineAreaCode!=null)
            {
                LandlineCode = objDEVEntitie.AizantIT_DMF_Holder.Any(x => x.LandlineAreaCode.Trim() == objDMFHolder.landLineAreaCode.Trim() && x.DMF_HolderID != objDMFHolder.DMFID);
            }
            if(objDMFHolder.Phone!=null)
            {
                 MobileNumber = objDEVEntitie.AizantIT_DMF_Holder.Any(x => x.Phone.Trim() == objDMFHolder.Phone.Trim() && x.DMF_HolderID != objDMFHolder.DMFID);
            }
            if(objDMFHolder.DMFEmail!=null)
            {
                Email = objDEVEntitie.AizantIT_DMF_Holder.Any(x => x.E_mail.Trim() == objDMFHolder.DMFEmail.Trim() && x.DMF_HolderID != objDMFHolder.DMFID);
            }

            if (DMFCode || DMFName || (LandlineCode && LandlineNumber) || MobileNumber || Email)//DMF Code Already Exists if Condition is true
            {
                if (LandlineCode == true && LandlineNumber == true)
                {
                    Result= _LandlineNumber;
                }
                if (MobileNumber == true)
                {
                    Result = _MobileNumber;
                }
                if (Email == true)
                {
                    Result= _Email;
                }
                if (DMFCode == true && DMFName == false)
                {
                    Result= _DMFCode;
                }
                else if (DMFCode == false && DMFName == true)
                {
                    Result= _DMFName;
                }
                else if(DMFCode == true && DMFName == true)
                {
                    Result= _DMFCodeandName;
                }
            }
            return Result;
        }
        public ActionResult GetDMFHolderHistoryList(int DMF_ID)
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir =Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            List<RegulatoryMasterHistoryList> objDMF_HolderHistoryList = new List<RegulatoryMasterHistoryList>();
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbRegulatory = new AizantIT_DevEntities())
                {
                    var DMF_HolderList = dbRegulatory.AizantIT_SP_DMFHolderHistory(
                                           displayLength,
                                           displayStart,
                                           sortCol,
                                           sSortDir,
                                           sSearch != null ? sSearch.Trim() : "", DMF_ID
                                           );

                    foreach (var item in DMF_HolderList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objDMF_HolderHistoryList.Add(new RegulatoryMasterHistoryList(Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.HistoryID), item.ActionBy, item.ActionRole, item.ActionDate, item.ActionStatus, item.Comments));
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objDMF_HolderHistoryList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Contract Testing Laboratory
        public PartialViewResult GetContractTestingLaboratory()
        {
            return PartialView();
        }
        public JsonResult GetContractTestingLaboratoryList()
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];

            List<ContractTestingLaboratoryList> objContractTestingLaboratoryList = new List<ContractTestingLaboratoryList>();

            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities objDevEntities = new AizantIT_DevEntities())
                {

                    var ContractTestingLaboratoryList = objDevEntities.AizantIT_SP_GetContractTestingLaboratoryList(
                               displayLength,
                               displayStart,
                               sortCol,
                               sSortDir,
                               sSearch != null ? sSearch.Trim() : ""
                               );
                    foreach (var item in ContractTestingLaboratoryList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objContractTestingLaboratoryList.Add(new ContractTestingLaboratoryList(
                            Convert.ToInt32(item.RowNumber),
                            Convert.ToInt32(item.ContractTestingLabID),
                            item.ContractTestingLabName,
                            item.FEINumber,
                            item.CountryName,
                            item.StateName,
                            item.CityName,
                            item.PinCode,
                            item.Phone,
                            item.EmailID,
                            item.Fax,
                            item.Address,
                            Convert.ToBoolean(item.isActive),
                            item.Status));
                    }
                    return Json(new
                    {
                        iTotalRecords = totalRecordsCount,
                        iTotalDisplayRecords = totalRecordsCount,
                        aaData = objContractTestingLaboratoryList
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                ViewBag.SysError = "Yes";
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult AddContractTestingLaboratory(ContractTestingLaboratoryViewModel objContractTestingLaboratoryModel)
        {
            try
            {
                
                using (AizantIT_DevEntities objDev_Entitie = new AizantIT_DevEntities())
                {
                    int CTL_Result = CTL_NameAndCTL_FEINumberChecking(objDev_Entitie, objContractTestingLaboratoryModel);

                    if (CTL_Result == 0)
                    {
                        using (DbContextTransaction transaction = objDev_Entitie.Database.BeginTransaction())
                        {
                            try
                            {
                                AizantIT_ContractTestingLaboratory objContractTestingLab = new AizantIT_ContractTestingLaboratory();
                                objContractTestingLab.ContractTestingLabName = objContractTestingLaboratoryModel.ContractTestingLabName;
                                objContractTestingLab.FEINumber = objContractTestingLaboratoryModel.FEINumber;
                                objContractTestingLab.CountryID = objContractTestingLaboratoryModel.CountryID;
                                objContractTestingLab.StateID = objContractTestingLaboratoryModel.StateID;
                                objContractTestingLab.CityID = objContractTestingLaboratoryModel.CityID == 0 ? null : objContractTestingLaboratoryModel.CityID;
                                objContractTestingLab.PinCode = objContractTestingLaboratoryModel.PinCode;
                                objContractTestingLab.Phone = objContractTestingLaboratoryModel.Phone;
                                objContractTestingLab.LandlineAreaCode = objContractTestingLaboratoryModel.landLineAreaCode;
                                objContractTestingLab.LandlineNum = objContractTestingLaboratoryModel.landLineNumber;
                                objContractTestingLab.E_mail = objContractTestingLaboratoryModel.E_mail;
                                objContractTestingLab.Fax = objContractTestingLaboratoryModel.Fax;
                                objContractTestingLab.Address = objContractTestingLaboratoryModel.Address;
                                objContractTestingLab.IsActive = true;
                                objDev_Entitie.AizantIT_ContractTestingLaboratory.Add(objContractTestingLab);
                                objDev_Entitie.SaveChanges();
                                //History Insert
                                ContractTestingLaboratoryHistoryInsert(objDev_Entitie, objContractTestingLab.ContractTestingLabID,1,objContractTestingLaboratoryModel.Comments);
                                transaction.Commit();
                                return Json(new { hasError = true, _ResultType = CTL_Result }, JsonRequestBehavior.AllowGet);
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                throw ex;
                            }
                        }
                    }
                    else
                    {
                        return Json(new { hasError = true, _ResultType = CTL_Result }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);

            }
        }
        public int CTL_NameAndCTL_FEINumberChecking(AizantIT_DevEntities objDev_Entitie,ContractTestingLaboratoryViewModel objContractTestingLaboratoryModel)
        {
            int _Result = 0;
            bool MobileNo=false,Email = false, landLineNumber= false, landLineCode=false;
            bool contractTestingSiteName = objDev_Entitie.AizantIT_ContractTestingLaboratory.Any(x => x.ContractTestingLabName.ToLower().Trim() == objContractTestingLaboratoryModel.ContractTestingLabName.ToLower().Trim() && x.ContractTestingLabID!= objContractTestingLaboratoryModel.ContractTestingLabID);
            bool contractFEINumber = objDev_Entitie.AizantIT_ContractTestingLaboratory.Any(x => x.FEINumber.ToLower().Trim() == objContractTestingLaboratoryModel.FEINumber.ToLower().Trim() && x.ContractTestingLabID != objContractTestingLaboratoryModel.ContractTestingLabID);
            if (objContractTestingLaboratoryModel.landLineNumber != null)
            {
                landLineNumber = objDev_Entitie.AizantIT_ContractTestingLaboratory.Any(x => x.LandlineNum.ToLower().Trim() == objContractTestingLaboratoryModel.landLineNumber.ToLower().Trim() && x.ContractTestingLabID != objContractTestingLaboratoryModel.ContractTestingLabID);
            }
            if (objContractTestingLaboratoryModel.landLineAreaCode != null)
            {
                landLineCode = objDev_Entitie.AizantIT_ContractTestingLaboratory.Any(x => x.LandlineAreaCode.ToLower().Trim() == objContractTestingLaboratoryModel.landLineAreaCode.ToLower().Trim() && x.ContractTestingLabID != objContractTestingLaboratoryModel.ContractTestingLabID);
            }
            if (objContractTestingLaboratoryModel.Phone != null)
            {
               MobileNo = objDev_Entitie.AizantIT_ContractTestingLaboratory.Any(x => x.Phone.ToLower().Trim() == objContractTestingLaboratoryModel.Phone.ToLower().Trim() && x.ContractTestingLabID != objContractTestingLaboratoryModel.ContractTestingLabID);
            }
            if(objContractTestingLaboratoryModel.E_mail != null)
            {
                Email = objDev_Entitie.AizantIT_ContractTestingLaboratory.Any(x => x.E_mail.ToLower().Trim() == objContractTestingLaboratoryModel.E_mail.ToLower().Trim() && x.ContractTestingLabID != objContractTestingLaboratoryModel.ContractTestingLabID);
            }

            if (contractTestingSiteName)
            {
                _Result = 1;//contractTestingSiteName Exist
            }
            if (contractFEINumber)
            {
                _Result = 2;//contractFEINumber Exist
            }
            if(landLineNumber && landLineCode)
            {
                _Result = 4;//landLineNumber Exist
            }
            if (contractTestingSiteName && contractFEINumber)
            {
                _Result = 3; //contractTestingSiteName and  contractFEINumber Exit
            }
            if(MobileNo)
            {
                _Result = 5;
            }
            if(Email)
            {
                _Result = 6;
            }
            return _Result;
        }
        public JsonResult BindDataContractTestingLaboratory(int ContractTestingLaboratoryID = 0)
        {
            Aizant_Bl objAizant_Bl = new Aizant_Bl();
            using (AizantIT_DevEntities objDEV_Entities = new AizantIT_DevEntities())
            {
                var _ContractTestingDetailsData = (from p in objDEV_Entities.AizantIT_ContractTestingLaboratory
                                                  where p.ContractTestingLabID == ContractTestingLaboratoryID
                                                  select new 
                                                  {
                                                      ContractTestingLabID = p.ContractTestingLabID,
                                                      ContractTestingLabName = p.ContractTestingLabName,
                                                      FEINumber = p.FEINumber,
                                                      CountryID = p.CountryID,
                                                      StateID = p.StateID,
                                                      CityID = p.CityID,
                                                      PinCode = p.PinCode,
                                                      Phone = p.Phone,
                                                      landLineAreaCode = p.LandlineAreaCode,
                                                      landLineNum = p.LandlineNum,
                                                      E_mail = p.E_mail,
                                                      Fax = p.Fax,
                                                      Address = p.Address,
                                                      IsActive = p.IsActive,
                                                  }).SingleOrDefault();
                return Json(new { hasError = false, data = _ContractTestingDetailsData,
                    CountryList = objAizant_Bl.BindCountries(_ContractTestingDetailsData.CountryID.ToString()),
                    StateList = objAizant_Bl.BindCountryWiseStatesList(_ContractTestingDetailsData.StateID.ToString(), _ContractTestingDetailsData.CountryID.ToString()),
                    CityList = objAizant_Bl.BindStateWiseCityList(_ContractTestingDetailsData.CityID.ToString(), _ContractTestingDetailsData.StateID.ToString())
                }, JsonRequestBehavior.AllowGet);
            }
           
        }
        public ActionResult UpdateContractTestingLaboratory(ContractTestingLaboratoryViewModel objContractTestingLaboratoryModel)
        {
            
            try
            {
                using (AizantIT_DevEntities objPMS_Entitie = new AizantIT_DevEntities())
                {
                    int CTL_Result = CTL_NameAndCTL_FEINumberChecking(objPMS_Entitie, objContractTestingLaboratoryModel);
                    if (CTL_Result != 0)
                    {
                        return Json(new { _ResultType= CTL_Result }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        using (DbContextTransaction transcation = objPMS_Entitie.Database.BeginTransaction())
                        {
                            try
                            {
                                var objContract = objPMS_Entitie.AizantIT_ContractTestingLaboratory.Where(id => id.ContractTestingLabID == objContractTestingLaboratoryModel.ContractTestingLabID).SingleOrDefault();
                                objContract.ContractTestingLabName = objContractTestingLaboratoryModel.ContractTestingLabName;
                                objContract.FEINumber = objContractTestingLaboratoryModel.FEINumber;
                                objContract.CountryID = objContractTestingLaboratoryModel.CountryID;
                                objContract.StateID = objContractTestingLaboratoryModel.StateID;
                                objContract.CityID = objContractTestingLaboratoryModel.CityID == 0 ? null : objContractTestingLaboratoryModel.CityID;
                                objContract.PinCode = objContractTestingLaboratoryModel.PinCode;
                                objContract.Phone = objContractTestingLaboratoryModel.Phone;
                                objContract.LandlineAreaCode = objContractTestingLaboratoryModel.landLineAreaCode;
                                objContract.LandlineNum = objContractTestingLaboratoryModel.landLineNumber;
                                objContract.E_mail = objContractTestingLaboratoryModel.E_mail;
                                objContract.Fax = objContractTestingLaboratoryModel.Fax;
                                objContract.Address = objContractTestingLaboratoryModel.Address;
                                objContract.IsActive = objContractTestingLaboratoryModel.IsActive;
                                objPMS_Entitie.SaveChanges();
                                //History Insert
                                ContractTestingLaboratoryHistoryInsert(objPMS_Entitie, objContract.ContractTestingLabID, 2, objContractTestingLaboratoryModel.Comments);
                                transcation.Commit();
                                return Json(new { _ResultType = CTL_Result }, JsonRequestBehavior.AllowGet);
                            }
                            catch (Exception ex)
                            {
                                transcation.Rollback();
                                throw ex;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetContractTestingLaboratoryHistoryList(int ContractTestingLaboratoryID)
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            List<RegulatoryMasterHistoryList> objContractTestingLaboratoryHistoryList = new List<RegulatoryMasterHistoryList>();
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbRegulatory = new AizantIT_DevEntities())
                {
                    var ContractTestingLaboratoryHistoryList = dbRegulatory.AizantIT_SP_ContractTestingLabHistory(
                                           displayLength,
                                           displayStart,
                                           sortCol,
                                           sSortDir,
                                           sSearch != null ? sSearch.Trim() : "", ContractTestingLaboratoryID
                                           );

                    foreach (var item in ContractTestingLaboratoryHistoryList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objContractTestingLaboratoryHistoryList.Add(new RegulatoryMasterHistoryList(Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.HistoryID), item.ActionBy, item.ActionRole, item.ActionDate, item.ActionStatus, item.Comments));
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objContractTestingLaboratoryHistoryList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        //Regulatory Master History 
        #region Regulatory Master History 
        public void ContractTestingLaboratoryHistoryInsert(AizantIT_DevEntities objPMS_Entitie, int ContractTestingLabID, int ActionStatus, string Comments = "")
        {

            AizantIT_ContractTestingLaboratoryHistory objContratAudit = new AizantIT_ContractTestingLaboratoryHistory();
            objContratAudit.ContractTestingLabID = ContractTestingLabID;
            objContratAudit.ActionBy = Convert.ToInt32(((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
            objContratAudit.RoleID = Convert.ToInt32(RA_UserRole.RA_Admin);
            objContratAudit.ActionDate = DateTime.Now;
            objContratAudit.ActionStatus = ActionStatus;
            objContratAudit.Comments = Comments;
            objPMS_Entitie.AizantIT_ContractTestingLaboratoryHistory.Add(objContratAudit);
            objPMS_Entitie.SaveChanges();

        }
        public void DMFHolderHistoryInsert(AizantIT_DevEntities objDEVEntitie, int DMFID, int ActionStatus, string Comments = "")
        {

            AizantIT_DMF_HolderHistory objDMF_Holder = new AizantIT_DMF_HolderHistory();
            objDMF_Holder.DMF_HolderID = DMFID;
            objDMF_Holder.ActionBy = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
            objDMF_Holder.RoleID = Convert.ToInt32(RA_UserRole.RA_Admin);
            objDMF_Holder.ActionDate = DateTime.Now;
            objDMF_Holder.ActionStatus = ActionStatus;//DMF Holder Updated
            objDMF_Holder.Comments = Comments;
            objDEVEntitie.AizantIT_DMF_HolderHistory.Add(objDMF_Holder);
            objDEVEntitie.SaveChanges();

        }
        public void DP_ManufacturingSiteHistoryInsert(AizantIT_DevEntities objDEVEntitie, int DP_ManufacturingSiteID, int ActionStatus, string Comments = "")
        {
            AizantIT_DP_ManufacturingSiteHistory objDPManfacturingAudit = new AizantIT_DP_ManufacturingSiteHistory();
            objDPManfacturingAudit.DP_ManufacturingSiteID = DP_ManufacturingSiteID;
            objDPManfacturingAudit.ActionBy = Convert.ToInt32(((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
            objDPManfacturingAudit.RoleID = Convert.ToInt32(RA_UserRole.RA_Admin);
            objDPManfacturingAudit.ActionDate = DateTime.Now;
            objDPManfacturingAudit.ActionStatus = ActionStatus;
            objDPManfacturingAudit.Comments = Comments;
            objDEVEntitie.AizantIT_DP_ManufacturingSiteHistory.Add(objDPManfacturingAudit);
            objDEVEntitie.SaveChanges();
        }
        public void DrugSubstanceHistoryInsert(AizantIT_DevEntities objDEVEntitie, int DrugSubstanceID, int ActionStatus, string Comments = "")
        {
            AizantIT_DrugSubstanceHistory objDurgSubstanceAudit = new AizantIT_DrugSubstanceHistory();
            objDurgSubstanceAudit.DrugSubstanceID = DrugSubstanceID;
            objDurgSubstanceAudit.ActionBy = Convert.ToInt32(((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
            objDurgSubstanceAudit.RoleID = Convert.ToInt32(RA_UserRole.RA_Admin);
            objDurgSubstanceAudit.ActionDate = DateTime.Now;
            objDurgSubstanceAudit.ActionStatus = ActionStatus;
            objDurgSubstanceAudit.Comments = Comments;
            objDEVEntitie.AizantIT_DrugSubstanceHistory.Add(objDurgSubstanceAudit);
            objDEVEntitie.SaveChanges();
        }
        public void DSManufacturingSiteHistoryInsert(AizantIT_DevEntities objDEVEntitie,int DS_ManufacturingSiteID,int ActionStatus,string Comments="")
        {
            AizantIT_DS_ManufacturingSiteHistory objDSManufacturingSiteHistory = new AizantIT_DS_ManufacturingSiteHistory();
            objDSManufacturingSiteHistory.DS_ManufacturingSiteID = DS_ManufacturingSiteID;
            objDSManufacturingSiteHistory.ActionBy = Convert.ToInt32(((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]));
            objDSManufacturingSiteHistory.RoleID = Convert.ToInt32(RA_UserRole.RA_Admin);
            objDSManufacturingSiteHistory.ActionDate = DateTime.Now;
            objDSManufacturingSiteHistory.ActionStatus = ActionStatus;
            objDSManufacturingSiteHistory.Comments = Comments;
            objDEVEntitie.AizantIT_DS_ManufacturingSiteHistory.Add(objDSManufacturingSiteHistory);
            objDEVEntitie.SaveChanges();
        }
        #endregion

        #region DS_ManufacturingSite
        public PartialViewResult GetDSManufacturingSite()
        {
            return PartialView();
        }
        public ActionResult AddDSManufacturingSiteData(DSManufacturingViewModel objCreateDSManufacturingSite)
        {
            try
            {
                using (AizantIT_DevEntities objDevEntity = new AizantIT_DevEntities())
                {
                   int _Result= DSManufacturingNameANDFEINumber(objDevEntity, objCreateDSManufacturingSite);
                    if (_Result == 0)
                    {
                        using (DbContextTransaction objDBTransction = objDevEntity.Database.BeginTransaction())
                        {
                            try
                            {
                                AizantIT_DS_ManufacturingSite objDSManufacturing = new AizantIT_DS_ManufacturingSite();
                                objDSManufacturing.DS_ManufacturingSiteID = objCreateDSManufacturingSite.DSManufacturingID;
                                objDSManufacturing.DS_ManufacturingSiteName = objCreateDSManufacturingSite.DSManufacturingName;
                                objDSManufacturing.FEI_Number = objCreateDSManufacturingSite.FEINumber;
                                objDSManufacturing.IsActive = true;
                                objDevEntity.AizantIT_DS_ManufacturingSite.Add(objDSManufacturing);
                                objDevEntity.SaveChanges();
                                //history Insert
                                DSManufacturingSiteHistoryInsert(objDevEntity, objDSManufacturing.DS_ManufacturingSiteID, 1);
                                objDBTransction.Commit();
                                return Json(new { hasError = true, _ResultType = _Result }, JsonRequestBehavior.AllowGet);
                            }
                            catch (Exception ex)
                            {
                                objDBTransction.Rollback();
                                throw ex;
                            }
                        }
                    }
                    else
                    {
                        return Json(new { hasError = false,_ResultType = _Result }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult UpdateDSManufacturingSiteData(DSManufacturingViewModel objUpdateDSManufacturingSite)
        {
            try
            {
                using (AizantIT_DevEntities objDevEntity = new AizantIT_DevEntities())
                {
                    int _Result = DSManufacturingNameANDFEINumber(objDevEntity, objUpdateDSManufacturingSite);
                    if (_Result != 0)
                    {
                        return Json(new {hasError = true ,_ResultType = _Result },JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        using (DbContextTransaction objDBTransction = objDevEntity.Database.BeginTransaction())
                        {
                            try
                            {
                                AizantIT_DS_ManufacturingSite objDSManufacturingSite = objDevEntity.AizantIT_DS_ManufacturingSite.SingleOrDefault(x=>x.DS_ManufacturingSiteID==objUpdateDSManufacturingSite.DSManufacturingID);
                                objDSManufacturingSite.DS_ManufacturingSiteName = objUpdateDSManufacturingSite.DSManufacturingName;
                                objDSManufacturingSite.FEI_Number = objUpdateDSManufacturingSite.FEINumber;
                                objDSManufacturingSite.IsActive = objUpdateDSManufacturingSite.IsActive;
                                objDevEntity.SaveChanges();
                                //History insert 
                                DSManufacturingSiteHistoryInsert(objDevEntity, objDSManufacturingSite.DS_ManufacturingSiteID, 2, objUpdateDSManufacturingSite.Comments);
                                objDBTransction.Commit();
                                return Json(new { hasError = false, _ResultType = _Result }, JsonRequestBehavior.AllowGet);
                            }
                            catch (Exception ex)
                            {
                                objDBTransction.Rollback();
                                throw ex;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult GetDSManufacturingData(int DS_ManufacturingID)
        {
            AizantIT_DevEntities objDEV_Entities = new AizantIT_DevEntities();
            var objDSManufacturing = objDEV_Entities.AizantIT_DS_ManufacturingSite.Where(ds => ds.DS_ManufacturingSiteID == DS_ManufacturingID).Select(dsf => new {dsf.DS_ManufacturingSiteID , dsf.DS_ManufacturingSiteName, dsf.FEI_Number, dsf.IsActive }).SingleOrDefault();
            return Json(objDSManufacturing, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetDSManufacturingSiteDataList()
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];

            List<DSManufacturingSiteList> objDSManufacturingSiteList = new List<DSManufacturingSiteList>();
            try
            {
                 int totalRecordsCount = 0;
                using (AizantIT_DevEntities objDev_Entity = new AizantIT_DevEntities())
                {
                    var DSManufacturingDataList = objDev_Entity.AizantIT_SP_GetDSManufacturingSiteList(
                                                   displayLength,
                                                   displayStart,
                                                   sortCol,
                                                   sSortDir,
                                                   sSearch != null ? sSearch.Trim() : "" );
                    foreach (var item in DSManufacturingDataList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objDSManufacturingSiteList.Add(new DSManufacturingSiteList(Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.DS_ManufacturingSiteID),item.DS_ManufacturingSiteName,item.FEI_Number,Convert.ToBoolean(item.IsActive),item.Status));
                    }
                }
                return Json(new {
                                    iTotalRecords = totalRecordsCount,
                                    iTotalDisplayRecords = totalRecordsCount,
                                    aaData = objDSManufacturingSiteList
                                },JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetDSManufacturingHistoryList(int DSManufacturingID)
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            List<RegulatoryMasterHistoryList> objDSManufacturingHistoryList = new List<RegulatoryMasterHistoryList>();
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities dbRegulatory = new AizantIT_DevEntities())
                {
                    var DSManufacturList = dbRegulatory.AizantIT_SP_DSManufacturingSiteHistory(
                                           displayLength,
                                           displayStart,
                                           sortCol,
                                           sSortDir,
                                           sSearch != null ? sSearch.Trim() : "", DSManufacturingID
                                           );

                    foreach (var item in DSManufacturList)
                    {
                        if (totalRecordsCount == 0)
                        {
                            totalRecordsCount = Convert.ToInt32(item.TotalCount);
                        }
                        objDSManufacturingHistoryList.Add(new RegulatoryMasterHistoryList(Convert.ToInt32(item.RowNumber), Convert.ToInt32(item.HistoryID), item.ActionBy, item.ActionRole, item.ActionDate, item.ActionStatus, item.Comments));
                    }
                }
                return Json(new
                {
                    iTotalRecords = totalRecordsCount,
                    iTotalDisplayRecords = totalRecordsCount,
                    aaData = objDSManufacturingHistoryList
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + HelpClass.SQLEscapeString(ex.Message), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public int DSManufacturingNameANDFEINumber(AizantIT_DevEntities objDevEntity, DSManufacturingViewModel objDSManufacturingModel)
        {
            int Result = 0;
            bool dsManufacturing_Name = objDevEntity.AizantIT_DS_ManufacturingSite.Any(x => x.DS_ManufacturingSiteName.ToLower().Trim() == objDSManufacturingModel.DSManufacturingName.ToLower().Trim() && x.DS_ManufacturingSiteID != objDSManufacturingModel.DSManufacturingID);
            bool FEI_Number = objDevEntity.AizantIT_DS_ManufacturingSite.Any(y => y.FEI_Number.Trim() == objDSManufacturingModel.FEINumber.Trim() && y.DS_ManufacturingSiteID != objDSManufacturingModel.DSManufacturingID);
            if(dsManufacturing_Name)
            {
                Result = 1;//dsManufacturing_Name already Exists
            }
            if(FEI_Number)
            {
                Result = 2;//FEINumber already Exists
            }
            if(dsManufacturing_Name && FEI_Number)
            {
                Result = 3;//dsManufacturing_Name && FEINumber already Exists
            }
            return Result;
        }
        #endregion
    }
}