﻿using Aizant_API_Entities;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace AizantIT_PharmaApp.Areas.Regulatory.Controllers.RegulatoryLinq
{
    public class RegulatoryLinqQuery
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());

        public void CreateFunctionalArea(string FunctionAreaName,string Remarks,int ActionBy,DataTable _dtDeptIDs,out int ActionStatus)
        {
            AizantIT_DevEntities dbRegulatory = new AizantIT_DevEntities();
            SqlParameter ParamTypeOfDeficiencyName = new SqlParameter("@FunctionalAreaName",FunctionAreaName);
            SqlParameter ParamRemarks = new SqlParameter("@Remarks", Remarks);
            SqlParameter ParamActionBy = new SqlParameter("@ActionBy", ActionBy);
            SqlParameter ParamDeptIDsTB = new SqlParameter("@DeptList", _dtDeptIDs);
            SqlParameter ParamActionStatus = new SqlParameter("@ActionStatus", SqlDbType.Int);
            ParamActionStatus.Direction = ParameterDirection.Output;
            ParamDeptIDsTB.TypeName = "[dbo].[AizantIT_TVP_DeptIDs]";
            dbRegulatory.Database.ExecuteSqlCommand("[RA].[AizantIT_SP_FunctionalAreaInsert] @FunctionalAreaName,@Remarks,@ActionBy,@DeptList,@ActionStatus out",
            ParamTypeOfDeficiencyName, ParamRemarks, ParamActionBy, ParamDeptIDsTB, ParamActionStatus);
            ActionStatus = Convert.ToInt32(ParamActionStatus.Value);
        }
        public void UpdateFunctionalArea(int FunctionAreaId,string FunctionAreaName, string Remarks,bool Status,int ActionBy, DataTable _dtDeptIDs, out int ActionStatus)
        {

            AizantIT_DevEntities dbRegulatory = new AizantIT_DevEntities();
            SqlParameter ParamFunctionalAreaId = new SqlParameter("@FunctionalAreaID", FunctionAreaId);
            SqlParameter ParamFunctionalAreaName = new SqlParameter("@FunctionalAreaName", FunctionAreaName);
            SqlParameter ParamRemarks = new SqlParameter("@Remarks", Remarks);
            SqlParameter ParamStatus = new SqlParameter("@IsActive", Status);
            SqlParameter ParamActionBy = new SqlParameter("@ActionBy", ActionBy);
            SqlParameter ParamDeptIDsTB = new SqlParameter("@DeptList", _dtDeptIDs);
            SqlParameter ParamActionStatus = new SqlParameter("@ActionStatus", SqlDbType.Int);
            ParamActionStatus.Direction = ParameterDirection.Output;
            ParamDeptIDsTB.TypeName = "[dbo].[AizantIT_TVP_DeptIDs]";
            dbRegulatory.Database.ExecuteSqlCommand("[RA].[AizantIT_SP_FunctionalAreaUpdate] @FunctionalAreaID,@FunctionalAreaName,@Remarks,@IsActive,@ActionBy,@DeptList,@ActionStatus out",
            ParamFunctionalAreaId, ParamFunctionalAreaName,ParamRemarks, ParamStatus,ParamActionBy, ParamDeptIDsTB, ParamActionStatus);
            ActionStatus = Convert.ToInt32(ParamActionStatus.Value);

        }
       
        public DataSet GetDossierDashBoardCharts(int EmpID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[RA].[AizantIT_SP_GetCompletedAndPendingCountForDossier]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter ParamEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(ParamEmpID).Value = EmpID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetObservationDashBoardCharts(int EmpID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("[RA].[AizantIT_SP_GetCompletedAndPendingCountForObservation]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter ParamEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(ParamEmpID).Value = EmpID;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetDeficiencyChartList(int Employeeid)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AizantIT_ConStr"].ToString());
                string query = "RA.AizantIT_SP_GetDeficiencyChartList";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter GetEmpID = new SqlParameter("@EmpID", SqlDbType.Int);
                cmd.Parameters.Add(GetEmpID).Value = (Employeeid);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }

}