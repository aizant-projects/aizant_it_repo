﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.Areas.Regulatory
{
    public class RegulatoryValidationMessages
    {
        //A const OBJECT is always Static
        public const string RA_ErrorMsg1 = "You are not Authorized User";
        public const string RA_ErrorMsg2 = "One Or More Deficiency Exists Without Observation(s)";
        public const string RA_ErrorMsg3 = "Deficiency Attachment(s) Uploaded";
        public const string RA_ErrorMsg4 = "Deficiency Code Already Exists";
        public const string RA_ErrorMsg5 = "Observation(s) Due Date are not in Deficiency Receipt Date & Due Date";
        public const string RA_ErrorMsg6 = "You're not Allowed to Select this user as Deficiency Approver, Due to This User is Hod for Some Observation(s)";
        public const string RA_ErrorMsg7 = "Updated Successfully";
        public const string RA_ErrorMsg8 = "Observation closed";
        public const string RA_ErrorMsg9 = "Observation Accepted";
        public const string RA_ErrorMsg10 = "Loaded Successfully";
        public const string RA_ErrorMsg11  = "Failed to Update";
        public const string RA_ErrorMsg12  = "Re-Assigned RA Lead, RA Approver not in the Modified State to Re-Assign";
        public const string RA_ErrorMsg13  = "Re-Assigned RA Approver and RA Lead";
        public const string RA_ErrorMsg14  = "Re-Assigned RA Approver, RA Lead not in the Modified State to Re-Assign";
        public const string RA_ErrorMsg15  = "RA Approver not in the Modified State to Re-Assign";
        public const string RA_ErrorMsg16  = "Re-Assigned RA Approver";
        public const string RA_ErrorMsg17  = "RA Lead not in the Modified State to Re-Assign";
        public const string RA_ErrorMsg18 = "Re-assigned RA Lead";
        public const string RA_ErrorMsg19 = "Observation Responsible Person Updated Successfully";
        public const string RA_ErrorMsg20 = "Observation Responsible Person is already Updated or No changes to Update";
        public const string RA_ErrorMsg21 = "Responsible Person Not in the modified state to Re-assign";
        public const string RA_ErrorMsg22 = "Deficiency Approver Updated Successfully";
        public const string RA_ErrorMsg23 = "Deficiency Approver is already Updated or No changes to Update";
        public const string RA_ErrorMsg24 = "Deficiency Lead & Approver Not in the modified state to Re-assign";
        public const string RA_ErrorMsg25 = "Deficiency RA Lead Updated Successfully";
        public const string RA_ErrorMsg26 = "Deficiency RA Lead is already Updated or No changes to Update";
        public const string RA_ErrorMsg27 = "Deficiency RA Lead Not in the modified state to Re-assign";
        public const string RA_ErrorMsg28 = "Deficiency RA Lead  or RA Approver Not in Active state";
        public const string RA_ErrorMsg29 = "Dossier RA Lead  and  RA Approver Not in Active state";
        public const string RA_ErrorMsg30 = "RA Lead  Not in Active state";
        public const string RA_ErrorMsg31 = "RA Approver Not in Active state";
    }
}