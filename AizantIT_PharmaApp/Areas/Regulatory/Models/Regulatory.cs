﻿namespace AizantIT_PharmaApp.Areas.Regulatory.Models
{
    public class TypeOfDeficiencyList
    {
        public int TypeOfDeficiencyID { get; set; }
        public string TypeOfDeficiencyName { get; set; }
        public string Remarks { get; set; }
        public bool? IsActive { get; set; }
        public int RowNumber { get; set; }
        public string Status { get; set; }

        public TypeOfDeficiencyList(int _RowNumber,int _TypeOfDeficiencyID,string _TypeOfDeficiencyName,bool _IsActive, string _Status)
        {
            RowNumber = _RowNumber;
            TypeOfDeficiencyID = _TypeOfDeficiencyID;
            TypeOfDeficiencyName = _TypeOfDeficiencyName;
            IsActive = _IsActive;
            Status = _Status;
        }
    }
    public class RegulatoryMasterHistoryList
    {
        public int  RowNumber { get; set; }
        public int History_ID { get; set; }
        public string ActionBy { get; set; }
        public string ActionRole { get; set; }
        public string ActionDate { get; set; }
        public string ActionStatus { get; set; }
        public string Comments { get; set; }
        public RegulatoryMasterHistoryList(int _RowNumber,int _History_ID, string _ActionBy,string _ActionRole,string _ActionDate,string _ActionStatus, string _Comments)
        {
            RowNumber = _RowNumber;
            History_ID = _History_ID;
            ActionBy = _ActionBy;
            ActionRole = _ActionRole;
            ActionDate = _ActionDate;
            ActionStatus = _ActionStatus;
            Comments = _Comments;
        }
    }
    public class FunctionalAreaList
    {
        public int FunctionalAreaID { get; set; }
        public string FunctionalAreaName { get; set; }
        public string Remarks { get; set; }
        public bool IsActive { get; set; }
        public string DeptCode { get; set; }
        public string DeptIDs { get; set; }
        public int RowNumber { get; set; }
        public string Status { get; set; }

        public FunctionalAreaList(int _RowNumber, int _FunctionalAreaID, string _FunctionalAreaName, bool _IsActive,string _DeptCode,string _DeptIDs, string _Status)
        {
            RowNumber = _RowNumber;
            FunctionalAreaID = _FunctionalAreaID;
            FunctionalAreaName = _FunctionalAreaName;
            DeptCode = _DeptCode;
            DeptIDs = _DeptIDs;
            IsActive = _IsActive;
            Status = _Status;
        }
    }
    public class ProductDetailsList
    {
        public int RowNumber { get; set; }
        public int DossierID { get; set; }
        public int DossierCurrentStatusID { get; set; }
        public string ProductName { get; set; }
        public string ApplicationNumber { get; set; }
        public string SubmissionDate { get; set; }
        public string ReviewAcceptedDate { get; set; }
        public string ApplicantName { get; set; }
        public string ClientName { get; set; }
        public string CurrentStatus { get; set; }
        public string DossierStatus { get; set; }
        public string GoalDate { get; set; }
        public string ApprovedDate { get; set; }
        public string Author{ get; set; }
        public string Approver { get; set; }
        public int AthorEmpID { get; set; }
        public int ApproverEmpID { get; set; }
 
        //For Product Main
        public ProductDetailsList(int _RowNumber, int _DossierID, string _ProductName, 
            string _ApplicationNumber, string _SubmissionDate, string _ReviewAcceptedDate,  
            string _ApplicantName, string _ClientName, string _CurrentStatus, 
            string _DossierStatus, string _GoalDate, string _ApprovedDate, int _DossierCurrentStatusID,int _AuthorEmpID,int _ApproverEmpID)
        {
            RowNumber = _RowNumber;
            DossierID = _DossierID;
            DossierCurrentStatusID = _DossierCurrentStatusID;
            ProductName = _ProductName;
            ApplicationNumber = _ApplicationNumber;
            SubmissionDate = _SubmissionDate;
            ReviewAcceptedDate = _ReviewAcceptedDate;
            ApplicantName = _ApplicantName;
            ClientName = _ClientName;
            CurrentStatus = _CurrentStatus;
            DossierStatus = _DossierStatus;
            GoalDate = _GoalDate;
            ApprovedDate = _ApprovedDate;
            AthorEmpID = _AuthorEmpID;
            ApproverEmpID = _ApproverEmpID;
        }

        //For Product Re-Assign Emps
        public ProductDetailsList(int _RowNumber, int _DossierID, int _DossierCurrentStatusID,
            string _ProductName, string _ApplicationNumber, string _ApplicantName, string _ClientName, 
            string _CurrentStatus, string _DossierStatus,string _Author, string _Approver)
        {
            RowNumber = _RowNumber;
            DossierID = _DossierID;
            DossierCurrentStatusID = _DossierCurrentStatusID;
            ProductName = _ProductName;
            ApplicationNumber = _ApplicationNumber;
            ApplicantName = _ApplicantName;
            ClientName = _ClientName;
            CurrentStatus = _CurrentStatus;
            DossierStatus = _DossierStatus;
            Author = _Author;
            Approver = _Approver;
        }
    }
    public class DrugSubstanceList
    {
        public int DrugSubstanceID { get; set; }
        public string DrugSubstanceName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public int RowNumber { get; set; }
        public int Count { get; set; }
        public string Comment { get; set; }
        public int DescriptionLength { set; get; }
        public string Status { get; set; }
        public DrugSubstanceList(int _RowNumber,int _DrugSubstanceID,string _DrugSubstanceName,string _Description, bool _IsActive,int _DescriptionLength, string _Status)
        {
            RowNumber = _RowNumber;
            DrugSubstanceID = _DrugSubstanceID;
            DrugSubstanceName = _DrugSubstanceName;
            Description = _Description;
            IsActive = _IsActive;
            DescriptionLength = _DescriptionLength;
            Status = _Status;
        }

    }
    public class DPManufacturSIteList
    {
        public int DP_ManufacturingSiteID { get; set; }
        public string DP_ManufacturingSiteName { get; set; }
        public string DP_FEINumber { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public int RowNumber { get; set; }
        public int Count { get; set; }
        public string Comment { get; set; }
        public int DescriptionLength { set; get; }
        public string Status { get; set; }

        public DPManufacturSIteList(int _RowNumber, int _DP_ManufacturingSiteID, string _DP_ManufacturingSiteName, string _DP_FEINumber,string _Description, bool _IsActive, int _DescriptionLength, string _Status)
        {
            RowNumber = _RowNumber;
            DP_ManufacturingSiteID = _DP_ManufacturingSiteID;
            DP_ManufacturingSiteName = _DP_ManufacturingSiteName;
            DP_FEINumber = _DP_FEINumber;
            Description = _Description;
            IsActive = _IsActive;
            DescriptionLength = _DescriptionLength;
            Status = _Status;

        }

    }
    public class DMFHolderList
    {
        public int RowNumber { get; set; }
        public int DMFID { get; set; }
        public string DMFHolderName { get; set; }
        public string DMFCode { get; set; }
        public string Phone { get; set; }
        public string DMFEmail { get; set; }
        public string Address { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
        public string PinCode { get; set; }
        public bool isActive { get; set; }
        public string Status { get; set; }
        public DMFHolderList(
            int _RowNumber,int _DMFID,
            string _DMFCode, string _DMFHolderName, string _CountryName,string _StateName,
            string _CityName,string _PinCode,
            string _Phone,string _EmailID,
            string _Addres,bool _isActive, string _Status)
        {
            RowNumber = _RowNumber;
            DMFID = _DMFID;
            DMFCode = _DMFCode;
            DMFHolderName = _DMFHolderName;
            CountryName = _CountryName;
            StateName = _StateName;
            CityName = _CityName;
            PinCode = _PinCode;
            Phone = _Phone;
            DMFEmail = _EmailID;
            Address = _Addres;
            isActive = _isActive;
            Status = _Status;
        }
    }
    public class ContractTestingLaboratoryList
    {
        public int RowNumber { get; set; }
        public int ContractTestingLabID { get; set; }
        public string ContractTestingLabName { get; set; }
        public string FEINumber { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
        public string PinCode { get; set; }
        public string Phone { get; set; }
        public string EmailID { get; set; }
        public string Fax { get; set; }
        public string Address { get; set; }
        public bool IsActive { get; set; }
        public string Status { get; set; }
        public ContractTestingLaboratoryList(
            int _RowNumber, int _ContractTestingLabID, string _ContractTestingLabName,
            string _FEINumber, string _CountryName, string _StateName,
            string _CityName, string _PinCode,
            string _Phone, string _EmailID,string _Fax,
            string _Addres, bool _isActive, string _Status)
        {
            RowNumber = _RowNumber;
            ContractTestingLabID = _ContractTestingLabID;
            ContractTestingLabName = _ContractTestingLabName;
            FEINumber = _FEINumber;
            CountryName = _CountryName;
            StateName = _StateName;
            CityName = _CityName;
            PinCode = _PinCode;
            Phone = _Phone;
            EmailID = _EmailID;
            Fax = _Fax;
            Address = _Addres;
            IsActive = _isActive;
            Status = _Status;
        }
    }
    public class DeficiencyList
    {
    
        public int DeficiencyID { get; set; }
        public string TypeofDeficiency { get; set; }
        public string DeficiencyCode { get; set; }
        public string DeficiencyReceiptDate { get; set; }
        public string DueDate { get; set; }
        public string DeficiencyStatus { get; set; }
        public string DeficiencyCurrentStatus { get; set; }
        public int ProductDossierID { get; set; }
        public int CurrentStatusID { get; set; }
        public int AuthorID { get; set; }
        public int DossierCurrentStatusID { get; set; }
        public int RowNumber { get; set; }
        public char DossierCreatedAs { get; set; }
        public string ApplicationNumber { get; set; }
        public string ProductName { get; set; }
        public int AprroverID { get; set; }
        public int DeficiencyAprroverID { get; set; }
        public int DeficiencyLeadID { get; set; }
        public string DeficiencyAuthorName { get; set; }
        public string DeficiencyApproverName { get; set; }
        public DeficiencyList( int _RowNumber,int _DeficiencyID,string _TypeofDeficiency, string _DeficiencyCode, string _DeficiencyReceiptDate,string _DueDate, 
            string _DeficiencyStatus, string _DeficiencyCurrentStatus, int _ProductDossierID, int _CurrentStatusID, int _AuthorID, int _DossierCurrentStatusID,
            char _DossierCreatedAs, string _ApplicationNumber, string _ProductName,int _DeficiencyAprroverID )
        {
            RowNumber = _RowNumber;
            DeficiencyID = _DeficiencyID;
            TypeofDeficiency = _TypeofDeficiency;
            DeficiencyCode = _DeficiencyCode;
            DeficiencyReceiptDate = _DeficiencyReceiptDate;
            DueDate = _DueDate;
            DeficiencyStatus = _DeficiencyStatus;
            DeficiencyCurrentStatus = _DeficiencyCurrentStatus;
            ProductDossierID = _ProductDossierID;
            CurrentStatusID = _CurrentStatusID;
            AuthorID = _AuthorID;
            DossierCurrentStatusID = _DossierCurrentStatusID;
            DossierCreatedAs = _DossierCreatedAs;
            ApplicationNumber = _ApplicationNumber;
            ProductName = _ProductName;
            AprroverID = _DeficiencyAprroverID;

        }
        public DeficiencyList(
            int _RowNumber,
            int _DeficiencyID,
            string _TypeofDeficiency,
            string _DeficiencyCode,
            string _DeficiencyStatus,
            string _DeficiencyCurrentStatus,
            int _CurrentStatusID,
            int _DeficiencyAprroverID,
            int _DeficiencyLeadID,
            string _DeficiencyAuthorName,
            string _DeficiencyApproverName
            )
        {
            RowNumber = _RowNumber;
            DeficiencyID = _DeficiencyID;
            TypeofDeficiency = _TypeofDeficiency;
            DeficiencyCode = _DeficiencyCode;
            DeficiencyStatus = _DeficiencyStatus;
            DeficiencyCurrentStatus = _DeficiencyCurrentStatus;
            CurrentStatusID = _CurrentStatusID;
            DeficiencyAprroverID = _DeficiencyAprroverID;
            DeficiencyLeadID = _DeficiencyLeadID;
            DeficiencyAuthorName = _DeficiencyAuthorName;
            DeficiencyApproverName = _DeficiencyApproverName;
        }
    }
    public class ObservationList
    {
        public int ObservationID { get; set; }
        public string FunctionalArea { get; set; }
        public string DepartmentName { get; set; }
        public string ResponsiblePerson { get; set; }
        public string ObservationStatus { get; set; }
        public string ObservationCurrentStatus { get; set; }
        public int RowNumber { get; set; }
        public string DueDate { get; set; }
        public int ProductDossierID { get; set; }
        public int DeficiencyID { get; set; }
        public int ObservationCurrentStatusID { get; set; }
        public int ResponsiblePersonEmpID { get; set; }
        public int DeficiencyCurrentStatusID { get; set; }
        public char DossierCreatedAs { get; set; }
        public int DossierCurrentStatusID { get; set; }
        public string Market { get; set; }
        public string Client { get; set; }
        public string Applicant { get; set; }
        public string ApplicationNumber { get; set; }
        public string ProductName { get; set; }
        //For Observation Main List
        public ObservationList(int _RowNumber, int _ObservationID, string _FunctionalArea, string _DepartmentName, string _ResponsiblePerson,
            string _DueDate, int _ProductDossierID,int _DeficiencyID,int _ObservationCurrentStatusID, int _ResponsiblePersonEmpID,
            int _DeficiencyCurrentStatusID,char _DossierCreatedAs,int _DossierCurrentStatusID,string _Market,string _Client,string _Applicant, string _ObservationStatus, string _ObservationCurrentStatus,string _ApplicationNumber,string _ProductName)
        {
            RowNumber = _RowNumber;
            ObservationID = _ObservationID;
            FunctionalArea = _FunctionalArea;
            DepartmentName = _DepartmentName;
            ResponsiblePerson = _ResponsiblePerson;
            ObservationStatus = _ObservationStatus;
            ObservationCurrentStatus = _ObservationCurrentStatus;
            DueDate = _DueDate;
            ProductDossierID = _ProductDossierID;
            DeficiencyID = _DeficiencyID;
            ObservationCurrentStatusID = _ObservationCurrentStatusID;
            ResponsiblePersonEmpID = _ResponsiblePersonEmpID;
            DeficiencyCurrentStatusID = _DeficiencyCurrentStatusID;
            DossierCreatedAs = _DossierCreatedAs;
            DossierCurrentStatusID = _DossierCurrentStatusID;
            Market = _Market;
            Client = _Client;
            Applicant = _Applicant;
            ApplicationNumber = _ApplicationNumber;
            ProductName = _ProductName;
        }
        //For Re-assign Observation List
        public ObservationList(int _RowNumber, int _ObservationID, string _FunctionalArea, string _DepartmentName, string _ResponsiblePerson, string _ObservationStatus, string _ObservationCurrentStatus,
         int _ObservationCurrentStatusID, int _ResponsiblePersonEmpID
      )
        {
            RowNumber = _RowNumber;
            ObservationID = _ObservationID;
            FunctionalArea = _FunctionalArea;
            DepartmentName = _DepartmentName;
            ResponsiblePerson = _ResponsiblePerson;
            ObservationStatus = _ObservationStatus;
            ObservationCurrentStatus = _ObservationCurrentStatus;
            ObservationCurrentStatusID = _ObservationCurrentStatusID;
            ResponsiblePersonEmpID = _ResponsiblePersonEmpID;

        }
    }

    public class DSManufacturingSiteList
    {
        public int DS_ManufacturingSiteID { get; set; }
        public string DS_ManufacturingSiteName { get; set; }
        public string FEI_Number { get; set; }
        public bool IsActive { get; set; }
        public int RowNumber { get; set; }
        public int Count { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
        public DSManufacturingSiteList(int _RowNumber, int _DS_ManufacturingSiteID, string _DS_ManufacturingSiteName, string _FEI_Number, bool _IsActive,string _Status)
        {
            RowNumber = _RowNumber;
            DS_ManufacturingSiteID = _DS_ManufacturingSiteID;
            DS_ManufacturingSiteName = _DS_ManufacturingSiteName;
            FEI_Number = _FEI_Number;
            IsActive = _IsActive;
            Status = _Status;
        }
    }

    public class RA_ReAssignEmps
    {
        public int ProductID { get; set; }
        public int RA_Lead_EmpID { get; set; }
        public bool IsRA_LeadChanged { get; set; }
        public int RA_Approver_EmpID { get; set; }
        public bool IsA_ApproverChanged { get; set; }
        public int ObservationID { get; set; }
        public int RA_HOD_EmpID { get; set; }
        public string Remarks { get; set; }
    }

}