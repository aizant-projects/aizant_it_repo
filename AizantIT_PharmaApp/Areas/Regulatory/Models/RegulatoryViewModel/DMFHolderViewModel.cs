﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace AizantIT_PharmaApp.Areas.Regulatory.Models.RegulatoryViewModel
{
    public class DMFHolderViewModel
    {
        public int RowNumber { get; set; }
        public int DMFID { get; set; }
        public string DMFHolderName { get; set; }
        public string DMFCode { get; set; }
        public int DMFNumber { get; set; }
        public string Phone { get; set; }
        public string landLineAreaCode { get; set; }
        public string landLineNumber { get; set; }
        public string Fax { get; set; }
        public string DMFEmail { get; set; }
        public string Address { get; set; }
        public int? CityID { get; set; }
        public int? StateID { get; set; }
        public int? CountryID { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
        public string PinCode { get; set; }
        public bool isActive { get; set; }
        public string Comment { get; set; }
        public List<SelectListItem> CountryList { get; set; }
        public List<SelectListItem> StateList { get; set; }
        public List<SelectListItem> CityList { get; set; }
    }
}