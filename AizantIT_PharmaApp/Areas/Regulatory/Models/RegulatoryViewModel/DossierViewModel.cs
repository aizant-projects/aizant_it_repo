﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace AizantIT_PharmaApp.Areas.Regulatory.Models.RegulatoryViewModel
{
    public class DossierViewModel
    {
        public int ProductDossierID { get; set; }
        public string DossierStatus { get; set; }
        public int DossierID { get; set; }
        public string Product { get; set; }
        public string DosageForm { get; set; }
        public string Market { get; set; }
        public string ApplicantName { get; set; }
        public string Client { get; set; }
        public string SubmissionDate { get; set; }
        public string ReviewAcceptedDate { get; set; }
        public string GoalDate { get; set; }
        public string ApprovedDate { get; set; }
        public string AuthorName { get; set; }
        public string ApproverName { get; set; }
        public string ApplicationNumber { get; set; }
        public string Comments { get; set; }
        public string DP_StorageCondition { get; set; }
        public string CurrentStatus { get; set; }
        public int CurrentStatusID { get; set; }
        public string DrugSubStanceIDs { get; set; }
        public int MarketingStatusID { get; set; }
        public string ProductDossierType { get; set; }
        public string PackDetails { get; set; }
        public List<SelectListItem> ApplicationNumberList { get; set; }
        public List<SelectListItem> ApproverList { get; set; }
        public List<SelectListItem> RA_LeadList { get; set; }

        public List<SelectListItem> DossierStatusList { get; set; }
        public string ProductCurrentStatus { get; set; }
        public decimal? StrengthValue { get; set; }
        public int ProductID { get; set; }
        public string ProductName { get; set; }

        public int ApplicantID { get; set; }


        public int DosageFormID { get; set; }
        public string DosageName { get; set; }



        public int MarketID { get; set; }
        public string MarketName { get; set; }

        public int? CountryID { get; set; }
        public string CountryName { get; set; }

        public int DrugSubstanceID { get; set; }
        public string DrugSubstanceName { get; set; }

        public int DS_ManufacturingSiteID { get; set; }
        public string DS_ManufacturingSiteName { get; set; }

        public int ClientID { get; set; }
        public string ClinetName { get; set; }
        public int AuthorID { get; set; }
        public int ApproverID { get; set; }

        public int ShelfLifeID { get; set; }
        public string ShelfLifeName { get; set; }

        public int DPManufacturingSiteID { get; set; }
        public string DPManufacturingSiteName { get; set; }

        public int UOMID { get; set; }
        public string UOMName { get; set; }
        

        public int ContractTestingLabID { get; set; }
        public string ContractTestingLabName { get; set; }

        public int DMFHolderID { get; set; }
        public string DMFHolderName { get; set; }

        public List<SelectListItem> ddlProductList { get; set; }
        public List<SelectListItem> ddlApplicantList { get; set; }
        public List<SelectListItem> ddlDosageFormList { get; set; }
        public List<SelectListItem> ddlMarketList { get; set; }
        public List<SelectListItem> ddlCountryList { get; set; }
        public List<SelectListItem> ddlDrugSubstanceList { get; set; }
        public List<SelectListItem> ddlDSManufacturingList { get; set; }
        public List<SelectListItem> ddlClientList { get; set; }
        public List<SelectListItem> ddlShelfLifeList { get; set; }
        public List<SelectListItem> ddlDPManufacturingSiteList { get; set; }
        public List<SelectListItem> ddlUOMMasterList { get; set; }
        public List<SelectListItem> ddlContractTestingLaboratoryList { get; set; }
        public List<SelectListItem> ddlDMFHolderList { get; set; }

        public List<Strength> Strength { get; set; }
        public List<Product_DMFHolder> Product_DMFHolder { get; set; }
        public List<ContractTestingLaboratory> ContractTestingLaboratory { get; set; }
        public int DeficiencyID { get; set; }
        public string DeficiencyRecieptDate { get; set; }
        public string DeficiencyDueDate { get; set; }

    }
    public class ViewDossierModel
    {
        public int ProductDossierID { get; set; }
        public string DossierStatus { get; set; }
        public string ApplicationNumber { get; set; }
        public string ProductName { get; set; }
        public string ClinetName { get; set; }
        public string ApplicantName { get; set; }
        public string DosageForm { get; set; }
        public bool DeficiencyExists { get; set; }
        public string MarketName { get; set; }
        public string CountryName { get; set; }
        public string MarketingStatus { get; set; }
        public string DP_StorageCondition { get; set; }
        public int ShelfLifeID { get; set; }
        public string AuthorName { get; set; }
        public string SubmissionDate { get; set; }
        public string ReviewAcceptedDate { get; set; }
        public string GoalDate { get; set; }
        public string ApprovedDate { get; set; }
        public string Strength { get; set; }
        public int CurrentStatus { get; set; }
        public int ApproverID { get; set; }
        public int AuthorID { get; set; }
        public int ListType { get; set; }
        public string DossierCurrentStatus { get; set; }
        public string PackDetails { get; set; }
        //public string ContractTestingLabName { get; set; }

        public string DMFHolderName { get; set; }
        public string DrugSubstanceName { set; get; }
        public string DS_ManufacturingSiteName { get; set; }
        public string DPManufacturingSiteName { get; set; }
        public int DMFHolderNum { get; set; }
        public string FunctionType { get; set; }

        public string Comments { get; set; }
        public int DeficiencyCount { get; set; }
        public int AuthenticationEmpID { get; set; }
        public List<ContractTestingLaboratory> ContractTestingLabName { get; set; }
        public List<Product_DMFHolder> Product_DMFHolder { get; set; }

    }
    public  class DossierMainDetails
    {
        public int DossierAuthorID { get; set; }
        public int DossierApproverID { get; set; }
        public string DossierApproverName { get; set; }
        public int CurrentStatus { get; set; }
        public string DossierStatus { get; set; }
        public int ProductDossierID { get; set; }
        public int ListType { get; set; }

        public int DeficiencyCount { get; set; }
        public int AuthenticationEmpID { get; set; }
        public bool DeficiencyExists { get; set; }
        public string ReviewAcceptedDate { get; set; }
        public string GoalDate { get; set; }
        public string PackDetails { get; set; }
    }
    public class DeficiencyAttachmentList
    {
        public int AttachmentID { get; set; }
        public string FileName { get; set; }
        public string FileExtention { get; set; }
        public string AttachmentDate { get; set; }

        public DeficiencyAttachmentList(int _AttachmentID, string _FileName,
            string _FileExtention, string _AttachmentDate)
        {
            AttachmentID = _AttachmentID;
            FileName = _FileName;
            FileExtention = _FileExtention;
            AttachmentDate = _AttachmentDate;
        }
    }

    public class ViewDeficiencyModel
    {
        public string DeficiencyCode { get; set; }
        public string TypeOfDeficiencyName { get; set; }
        public string DeficiencyReceiptDate { get; set; }
        public string DueDate { get; set; }
        public string DeficiencyStatus { get; set; }
        public int DossierCurrentStatus { get; set; }

        public int DeficiencyAttachmentID { get; set; }

        public int DeficiencyID { get; set; }
        public int ProductDossierID { get; set; }
        public int TypeOfDeficiencyID { get; set; }
        public string ClosedDate { get; set; }
        public int ApproverID { get; set; }
        public string Remarks { get; set; }
        public string Status { get; set; }
        public string SubmissionDate { get; set; }
        public string ApproveDate{get;set;}
        public string ReviewAcceptedDate { get; set; }
        public string DossierStatus { get; set; }
        public int DeficiencyCurrentStatus { get; set; }
        public int AuthenticationEmpID { get; set; }
        public int AuthorID { get; set; }
        public string ApproverName { get; set; }

        public int ObservationCount { get; set; }
    }

    public class ViewObservationModel
    {
        public int ObservationID { get; set; }
        public int FunctionalAreaID { get; set; }
        public int DepartmentID { get; set; }
        public string DueDate { get; set; }
        public string Remarks { get; set; }
        public int ResponsiblePersonID { get; set; }
        public string FunctionalAreaName { get; set; }
        public string DepartmentName { get; set; }
        public string ObservationDescription { get; set; }
        public string ResponsiblePerson { get; set; }
        public string ObservationStatus { get; set; }
        public int ObservationStatusID { get; set; }
        public int ObservationCurrentStatus { get; set; }
        public int AuthenticationEmpID { get; set; }
        public string DossierStatus { get; set; }
    }


    public class CreateProductDossier
    {
        public int ProductDossierID { get; set; }
        public string DossierStatus { get; set; }
        public int ProductID { get; set; }
        public int DosageFormID { get; set; }
        public int MarketID { get; set; }
        public int ApplicantNameID { get; set; }
        public int CountryID { get; set; }
        public string ApplicationNumber { get; set; }
        public int ApprovalStatusID { get; set; }
        public int[] DrugSubstanceID { get; set; }
        public int[] DSManufacturingID { get; set; }
        public int MarketingStatusID { get; set; }
        public int ClientID { get; set; }
        public string  DMFSiteID { get; set; }
        public string DrugProductStorageCondition { get; set; }
        public int ShelfLifeID { get; set; }
        public int DrugProductManufacturingSiteID { get; set; }
        public DateTime SubmissionDate { get; set; }
        public string ReviewAcceptedDate { get; set; }
        public string GoalDate { get; set; }
        public string ApprovedDate { get; set; }
        public string AuthorID { get; set; }
        public int ApproverID { get; set; }
        public string CreationType { get; set; }
        public string Comments { get; set; }
        public string PackDetails { get; set; }
        public List<Strength> Strength { get; set; }
        public List<Product_DMFHolder> Product_DMFHolder { get; set; }
        public List<ContractTestingLaboratory> ContractTestingLaboratory { get; set; }
    }
   

    public class Strength
    {
        public int ProdStrengthID { get; set; }
        public decimal StrengthName { get; set; }
        public int UOMID { get; set; }
        public string UOMNAME { get; set; }


    }
    public class ContractTestingLaboratory
    {
        public int ProdContractTestingLabID { get; set; }
        public int CTL_ID { get; set; }
        public string CTL_Name { get; set; }
        public string FunctionalType { get; set; }
       
    }
    public class Product_DMFHolder
    {
        public int ProductDossierID { get; set; }
        public int ProdDMFHolderID { get; set; }
        public int DMF_HolderID { get; set; }
        public string DMF_HolderName { get; set; }
        public int DrugSubstance_ID { get; set; }
        public string DrugSubstance_Name { get; set; }
        public int DSManufacturing_ID { get; set; }
        public string DSManufacturing_Name { get; set; }
        public string DMFNumber { get; set; }
    }

    public class Product_DMFHolderList
    {
        public int ProductDossierID { get; set; }
        public int DMF_HolderID { get; set; }
        public int DrugSubstance_ID { get; set; }
        public int DSManufacturing_ID { get; set; }
        public string DMFNumber { get; set; }


        public Product_DMFHolderList(int _ProductDossierID, int _DMF_HolderID, int _DrugSubstance_ID,int _DSManufacturing_ID,string _DMFNumber)
        {
            ProductDossierID = _ProductDossierID;
            DMF_HolderID = _DMF_HolderID;
            DrugSubstance_ID = _DrugSubstance_ID;
            DSManufacturing_ID = _DSManufacturing_ID;
            DMFNumber = _DMFNumber;

        }
    }

}