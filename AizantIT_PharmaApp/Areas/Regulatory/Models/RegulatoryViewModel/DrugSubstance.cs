﻿using System;

namespace AizantIT_PharmaApp.Areas.Regulatory.Models.RegulatoryViewModel
{
    public class DrugSubstance
    {
        public Nullable<int> TotalCount { get; set; }
        public int DrugSubstanceID { get; set; }
        public string DrugSubstanceName { get; set; }
        public string Description { get; set; }
        public bool isActive { get; set; }
        public int RowNumber { get; set; }
        public int Count { get; set; }
        public string Comment { get; set; }
        public int DescriptionLength { set; get; }
    }
}