﻿using System.Collections.Generic;

namespace AizantIT_PharmaApp.Areas.Regulatory.Models.RegulatoryViewModel
{
    public class RegulatoryDashBoard
    {     
        public bool ApproverCard { get; set; }
        public bool PendingCard { get; set; }
        public bool RevertedCard { get; set; }
        public int ApproveTotalCount { get; set; }
        public int RevertTotalCount { get; set; }
        public int PendingTotalCount { get; set; }

        public List<PendingListItems> pendingListItems { get; set; }

        public List<RevertListItems> revertListItems { get; set; }

        public List<DeficiencyChartList> DeficiencyChartList { get; set; }

        public int RaLead { get; set; }
        public int RaHod { get; set; }

    }

    public class PendingListItems
    {
        public string NavigationLink { get; set; }
        public string PendingText { get; set; }
        public int PendingCount { get; set; }

        public PendingListItems(int _PendingCount,string _NavigationLink,string _PendingText)
        {
            NavigationLink = _NavigationLink;
            PendingText = _PendingText;
            PendingCount = _PendingCount;
        }
    }

    public class RevertListItems
    {
        public string NavigationLink { get; set; }
        public string RevertText { get; set; }
        public int RevertCount { get; set; }

        public RevertListItems(int _RevertCount,string _NavigationLink, string _RevertText)
        {
            NavigationLink = _NavigationLink;
            RevertText = _RevertText;
            RevertCount = _RevertCount;
        }
    }

    public class RaDossierChart
    {
        public string EventName { get; set; }
        public int Pending { get; set; }
        public int Completed { get; set; }
        public RaDossierChart(string _EventName, int _Pending, int _Completed)
        {
            EventName = _EventName;
            Pending = _Pending;
            Completed = _Completed;
        }
    }
    public class RaObservationChart
    {
        public string EventName { get; set; }
        public int Pending { get; set; }
        public RaObservationChart(string _EventName, int _Pending)
        {
            EventName = _EventName;
            Pending = _Pending;
        }
    }


    public class DeficiencyChartList
    {
        public DeficiencyChartList(string EventName1, double TotalRecords1)
        {
            EventName = EventName1; TotalRecords = TotalRecords1;
        }
        private string _EventName;
        public string EventName
        {
            get
            {
                return _EventName;
            }
            set
            {
                _EventName = value;
            }
        }
        private double _TotalRecords;
        public double TotalRecords
        {
            get
            {
                return _TotalRecords;
            }
            set
            {
                _TotalRecords = value;
            }
        }
    }
}