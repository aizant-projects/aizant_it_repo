﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace AizantIT_PharmaApp.Areas.Regulatory.Models.RegulatoryViewModel
{
    public class ContractTestingLaboratoryViewModel
    {
        public int ContractTestingLabID { get; set; }
        public string ContractTestingLabName { get; set; }
        public string FEINumber { get; set; }
        public string Phone { get; set; }
        public string landLineAreaCode { get; set; }
        public string landLineNumber { get; set; }
        public string Fax { get; set; }
        public string E_mail { get; set; }
        public string Address { get; set; }
        public string otherAddress { get; set; }
        public int? CityID { get; set; }
        public int StateID { get; set; }
        public int CountryID { get; set; }
        public string PinCode { get; set; }
        public bool IsActive { get; set; }
        public string Comments { get; set; }
       

        public List<SelectListItem> CountryList { get; set; }
        public List<SelectListItem> StateList { get; set; }
        public List<SelectListItem> CityList { get; set; }

    }

    public class DPManufacturingViewModel
    {
        public int DPManufacturingID { get; set; }
        public string DPManufactringName { get; set; }
        public string FEINumber { get; set; }
        public string DPManufactringDescription { get; set; }
        public bool IsActive { get; set; }
        public string Comments { get; set; }
    }
    public class DSManufacturingViewModel
    {
        public int DSManufacturingID { get; set; }
        public string DSManufacturingName { get; set; }
        public string FEINumber { get; set; }
        public bool IsActive { get; set; }
        public string Comments { get; set; }
    }
}