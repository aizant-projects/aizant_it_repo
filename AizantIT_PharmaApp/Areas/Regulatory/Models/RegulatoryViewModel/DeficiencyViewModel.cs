﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace AizantIT_PharmaApp.Areas.Regulatory.Models.RegulatoryViewModel
{
    public class DeficiencyViewModel
    {

    }

    public class CreateDeficiency
    {

        public int DeficiencyID { get; set; }
        public string DeficiencyCode { get; set; }
        public int ProductDossierID { get; set; }
        public int TypeOfDeficiencyID { get; set; }
        public DateTime DeficiencyReceiptDate { get; set; }
        public DateTime DueDate { get; set; }
        public int DeficiencyStatus { get; set; }
        public DateTime? ClosedDate { get; set; }
        public int ApproverID { get; set; }
        public string Remarks { get; set; }
        public string Status { get; set; }
        public string  DossierStatus { get; set; }

        public List<SelectListItem> TypeOfDeficiencyList { get; set; }
        public List<SelectListItem>  ApproverList { get; set; }
        public List<SelectListItem> FunctionalAreaList { get; set; }
        public List<SelectListItem> DepartmetList { get; set; }

        public List<SelectListItem> ResponsiblePersonList { get; set; }

    }
    public class CreateObservation
    {
        public int DF_ObservationID { get; set; }
        public int DeficiencyID { get; set; }
        public int FunctionalAreaID { get; set; }
        public int DepartmentID { get; set; }
        public string ObservationDescription { get; set; }
        public int ResponsiblePersonEmpID { get; set; }
        public int Status { get; set; }
        public string Comments { get; set; }
        public DateTime DueDate { get; set; }
        public int DeficiencyCurrentStatus { get; set; }
    }
}