﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AizantIT_PharmaApp.Areas.Regulatory.Models.RegulatoryViewModel
{
    public class CTL_CreationVM
    {
        public int CTL_ID { get; set; }
        public string Functionalities { get; set; }
        public int ProductDossierID { get; set; }
    }
}