﻿namespace AizantIT_PharmaApp.Areas.Regulatory.Models.Reports
{
    public class ObservationMainListResult
    {
        public int EmpID { get; set; }
        public int displayLength { get; set; }
        public int displayStart { get; set; }
        public int sortCol { get; set; }
        public string sSortDir { get; set; }
        public string sSearch { get; set; }

        public string sSearch_ApplicationNumber { get; set; }
        public string sSearch_ProductName { get; set; }
        public string sSearch_FunctionalArea { get; set; }
        public string sSearch_DepartmentName { get; set; }
        public string sSearch_ResponsiblePerson { get; set; }
        public string sSearch_DueDate { get; set; }
        public string sSearch_Market { get; set; }
        public string sSearch_Client { get; set; }
        public string sSearch_Applicant { get; set; }
        public string sSearch_ObservationStatus { get; set; }
        public string sSearch_ObservationCurrentStatus { get; set; }        
    }
    public class ProductDetailsMainListResult
    {
        public int EmpID { get; set; }
        public int displayLength { get; set; }
        public int displayStart { get; set; }
        public int sortCol { get; set; }
        public string sSortDir { get; set; }
        public string sSearch { get; set; }

        public string sSearch_ProductName { get;set;}
        public string sSearch_ApplicationNumber { get;set;}
        public string sSearch_SubmissionDate { get;set;}
        public string sSearch_ReviewAcceptedDate { get;set;}
        public string sSearch_ApplicantName { get;set;}
        public string sSearch_ClientName { get;set;}
        public string sSearch_CurrentStatus { get;set;}
        public string sSearch_DossierStatus { get;set;}
        public string sSearch_GoalDate { get;set;}
        public string sSearch_ApprovedDate { get;set;}

    }

}