﻿using Aizant_API_Entities;
using Aizant_Enums;
using AizantIT_PharmaApp.Areas.PRODUCT.Models.DataLayer;
using AizantIT_PharmaApp.Areas.PRODUCT.Models.ViewModels;
using AizantIT_PharmaApp.Common.CustomFilters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Web.Mvc;

namespace AizantIT_PharmaApp.Areas.PRODUCT.Controllers
{
    [Route("PRODUCT/MasterController/{action}")]
    [RouteArea]
    [CustAuthentication]
    [CustAuthorization(
        (int)AizantEnums.Modules.Product,
        (int)AizantEnums.Product_Roles.Product_Admin
    )]
    public class MasterController : Controller
    {
        // GET: PRODUCT/Master
        public int SeletedValue = 0;
        public ActionResult Index()
        {
            return View();
        }
        //DDL selected action method 
        public ActionResult ProductMasterList()
        {
            return View();
        }

        #region---History
        public ActionResult _HistoryDetails(int id = 0)
        {
            ViewBag.DosageList = id;
            return PartialView();

        }
        public ActionResult EachHistoryDetails(int ID)
        {
            int displayLength = Convert.ToInt32(Request.Params["iDisplayLength"]);
            int displayStart = Convert.ToInt32(Request.Params["iDisplayStart"]);
            int sortCol = Convert.ToInt32(Request.Params["iSortCol_0"]);
            string sSortDir = Request.Params["sSortDir_0"];
            string sSearch = Request.Params["sSearch"];
            try
            {
                int totalRecordsCount = 0;
                using (AizantIT_DevEntities objPMS_Entitie = new AizantIT_DevEntities())
                {
                    DAL_Dosage objDAL = new DAL_Dosage();
                    DataTable dt = objDAL.GetHistoryListDAL(displayLength, displayStart, sortCol, sSortDir, sSearch, ID);
                    List<HistoryView> objDosageAuditList = new List<HistoryView>();

                    if (dt.Rows.Count > 0)
                    {
                        totalRecordsCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                        foreach (DataRow dr in dt.Rows)
                        {
                            HistoryView objDosagFomAudit = new HistoryView();
                            objDosagFomAudit.PKID = Convert.ToInt32(dr["Product_HistoryID"]);
                            objDosagFomAudit.RowNumber = Convert.ToInt32(dr["RowNumber"]);
                            objDosagFomAudit.FirstName = Convert.ToString(dr["EmployeeName"]);
                            objDosagFomAudit.RoleName = Convert.ToString(dr["RoleName"]);
                            objDosagFomAudit.ActionDate = (dr["ActionDate"]).ToString();
                            objDosagFomAudit.AuditActionName = Convert.ToString(dr["AuditActionName"]);
                            objDosagFomAudit.Comments = Convert.ToString(dr["Comments"]);
                            objDosagFomAudit.CommentLength = Convert.ToInt32(dr["CommLength"]);
                            objDosageAuditList.Add(objDosagFomAudit);
                        }
                    }
                    return Json(
                        new
                        {
                            iTotalRecords = totalRecordsCount,
                            iTotalDisplayRecords = totalRecordsCount,
                            aaData = objDosageAuditList
                        }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                int LineNo = (new StackTrace(ex, true).GetFrame((new StackTrace(ex, true)).FrameCount - 1).GetFileLineNumber());
                return Json(new { msg = "Error " + LineNo + " :" + ex.Message.Replace("'", "\'"), msgType = "error" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

    }
}