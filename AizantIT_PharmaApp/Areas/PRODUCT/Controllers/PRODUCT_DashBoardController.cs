﻿using Aizant_Enums;
using AizantIT_PharmaApp.Common.CustomFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AizantIT_PharmaApp.Areas.PRODUCT.Controllers
{
    [Route("PRODUCT/PRODUCT_DashBoardController/{action}")]
    [RouteArea]
    [CustAuthentication]
    [CustAuthorization(
        (int)AizantEnums.Modules.AdminSetup,
        (int)AizantEnums.Product_Roles.Product_Admin,
        (int)AizantEnums.Product_Roles.Product_Approver,
        (int)AizantEnums.Product_Roles.Product_User
    )]
    public class PRODUCT_DashBoardController : Controller
    {
        // GET: PRODUCT/PRODUCT_DashBoard
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult PRODUCT_Dashboard()
        {
            return View();
        }
    }
}