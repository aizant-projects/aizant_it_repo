﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AizantIT_PharmaApp.Areas.PRODUCT.Models.ModelProductList
{
    public class ProductList
    {
        public int RowNumber { get; set; }
        public int CurrentStatus { get; set; }
        public int SNO { get; set; }

        public int PKID { get; set; }
        public int ProductID { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public decimal? StrengthValue { get; set; }

        public int RALeadID { get; set; }
        public string RALeadName { get; set; }


       
    }

    public class HistoryView
    {
        public int PKID { get; set; }
        public int RowNumber { get; set; }
        public int DosageFormID { get; set; }
        public string FirstName { get; set; }
        public string RoleName { get; set; }
        public string AuditActionName { get; set; }
        public string SubmittedData { get; set; }
        public string Comments { get; set; }
        public int CommentLength { get; set; }
        public string ActionDate { get; set; }
        public int AssignToId { get; set; }
        public string AssignToName { get; set; }
        public int AssignToRoleId { get; set; }
        public string AssignToRoleName { get; set; }
    }
}