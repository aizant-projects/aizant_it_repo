﻿using System.Web.Mvc;

namespace AizantIT_PharmaApp.Areas.PRODUCT
{
    public class PRODUCTAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "PRODUCT";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "PRODUCT_default",
                "PRODUCT/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}