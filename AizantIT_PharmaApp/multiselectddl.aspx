﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="multiselectddl.aspx.cs" Inherits="AizantIT_PharmaApp.multiselectddl" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <link href="<%=ResolveUrl("~/Content/bootstrap.css")%>" rel="stylesheet" />
    <script src="<%=ResolveUrl("~/Scripts/jquery-3.2.1.min.js")%>"></script>
    <script src="<%=ResolveUrl("~/Scripts/bootstrap.min.js")%>"></script>
    <script src="<%=ResolveUrl("~/Scripts/moment.min.js")%>"></script>
   
    <link href="<%=ResolveUrl("~/Scripts/select2-4.0.3/dist/css/select2.min.css")%>" rel="stylesheet" />
    <script src="<%=ResolveUrl("~/Scripts/select2-4.0.3/dist/js/select2.min.js")%>"></script>
    <script src="<%=ResolveUrl("~/AppCSS/src/jquery.richtext.js")%>"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.12.1.min.js")%>"></script>

    <link href="<%=ResolveUrl("~/AppCSS/AppStyles.css")%>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/AppCSS/LeftNaviStyle.css")%>" rel="stylesheet" />
    <script src="<%=ResolveUrl("~/AppScripts/GlobalScripts.js")%>"></script>
    <link href="<%=ResolveUrl("~/AppCSS/TMS/tms_style.css")%>" rel="stylesheet" />
     <script src="../../Scripts/select2-4.0.3/bootstrap-select.js"></script>
    <link href="../../Scripts/select2-4.0.3/bootstrap-select.css" rel="stylesheet" />

     <script type="text/javascript">
         $(function () {
             $('[id*=lstFruits]').multiselect({
                 includeSelectAllOption: true
             });
             $("#Button1").click(function () {
                 alert($(".multiselect-selected-text").html());
             });
         });
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
           <%-- <asp:UpdatePanel ID="upDDl" UpdateMode="Conditional" runat="server">
                            <ContentTemplate>--%>
                               <%-- <asp:DropDownList ID="DropDownList1" onchange="SetSelectedText(this)" CssClass="col-lg-12 selectpicker padding-none" multiple data-live-search="true" data-live-search-placeholder="Search" runat="server">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                    <asp:ListItem Text="SOP1" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="SOP2" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="SOP3" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="SOP4" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="SOP5" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="SOP6" Value="6"></asp:ListItem>
                                    <asp:ListItem Text="SOP7" Value="7"></asp:ListItem>
                                    <asp:ListItem Text="SOP8" Value="8"></asp:ListItem>
                                </asp:DropDownList>--%>

             <asp:ListBox ID="lstFruits" runat="server" CssClass="col-lg-12 selectpicker padding-none" multiple data-live-search="true" data-live-search-placeholder="Search" SelectionMode="Multiple">
                <asp:ListItem Text="Mango" Value="1" />
                <asp:ListItem Text="Apple" Value="2" />
                <asp:ListItem Text="Banana" Value="3" />
                <asp:ListItem Text="Guava" Value="4" />
                <asp:ListItem Text="Orange" Value="5" />
            </asp:ListBox>

            <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />

                       <%--     </ContentTemplate>
                        </asp:UpdatePanel>--%>
<input type="hidden" id ="hfSopName" name="SopName" />
<script type = "text/javascript">
    //function SetSelectedText(DropDownList1) {
    //    var selectedText = DropDownList1.options[DropDownList1.selectedIndex].innerHTML;
    //    document.getElementById("hfSopName").value = document.getElementById("hfSopName").value+','+ selectedText;
    //}
</script>
<%--<asp:Button Text="Submit" runat="server" OnClick="Submit" />--%>
        </div>
    </form>
</body>
</html>
