﻿var docWarnMsg = "Please Close The Dev Tools Window.";
var callBack_OnPDF_Success;
function PdfViewerContentLoading(PdfViewerUrl, TypeID, DocOrVersionID, OfEmpID, OfRoleID, loadOn = "#divPDF_Viewer", PdfCallBackFunction = '') {
    PdfViewerUrl = PdfViewerUrl || '/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath';
    TypeID = TypeID || "0";
    DocOrVersionID = DocOrVersionID || "0";
    OfEmpID = OfEmpID || "0";
    OfRoleID = OfRoleID || "0";
    callBack_OnPDF_Success = PdfCallBackFunction;
    $(loadOn).html("Loading...");
    //For Checking the Console is in Open or not
    var element = new Image;
    var devtoolsOpen = false;
    element.__defineGetter__("id", function () {
        devtoolsOpen = true; // This only executes when devtools is open.       
    });
    var SetTimerforConsole;
    SetTimerforConsole = setInterval(function () {
        devtoolsOpen = false;
        console.log(element);
        if (devtoolsOpen) {
            $(loadOn).html(docWarnMsg);
            //clearInterval(SetTimerforConsole);
            alert(docWarnMsg);
        }
        else
        {
            clearInterval(SetTimerforConsole);
            //For PDF Viewer Loading
            $(loadOn).html("Loading...");
            var objInputstr = { "TypeID": TypeID, "DocOrVersionID": DocOrVersionID, "OfEmpID": OfEmpID, "OfRoleID": OfRoleID };
            var jsonData = JSON.stringify(objInputstr);
            $.ajax({
                type: "POST",
                url: PdfViewerUrl,
                // url: '<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>',
                // url: '@Url.Content("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")',
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var result = JSON.parse(response.d);
                    if (result.msgType == "success") {
                        $(loadOn).html(result.msg);
                        onPDF_LoadingSuccess();
                    }
                    else {
                        custAlertMsg(result.msg, "error");
                        $(loadOn).html(result.msg.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}));
                    }
                },
                failure: function (response) {
                    // alert(response.d);
                },
                error: function (xhr, textStatus, error) {
                    if (typeof console == "object") {
                        console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        // var msg = textStatus;
                        var msg = "";
                        if (xhr.status === 0) {
                            msg = 'No Server Connection, Verify Network.';
                        } else if (xhr.status == 404) {
                            msg = 'Requested Page Not Found. [404]';
                        } else if (xhr.status == 500) {
                            msg = 'Internal Server Error [500].';
                        }
                        //else if (textStatus === 'parsererror') {
                        //    msg = 'Requested JSON parse failed.';
                        //} else if (textStatus === 'timeout') {
                        //    msg = 'Time out error.';
                        //} else if (textStatus === 'abort') {
                        //    msg = 'Ajax request aborted.';
                        //} else {
                        //    msg = 'Uncaught Error.\n' + xhr.responseText;
                        //}
                        if (msg != "") {
                            custAlertMsg(msg, "error");
                        }
                    }
                }
            });
        }
    }, 1000);
}
function onPDF_LoadingSuccess() {
    this[callBack_OnPDF_Success]();
}
function PdfFormViewerContentLoading(PdfViewerUrl, FileName, loadOnFormDiv = "#divPDF_Viewer") {
    $(loadOnFormDiv).html("Loading...");
    //For Checking the Console is in Open or not
    var elementForm = new Image;
    var devtoolsOpenForm = false;
    elementForm.__defineGetter__("id", function () {
        devtoolsOpenForm = true; // This only executes when devtools is open.       
    });
    var SetTimerforConsoleForm;
    SetTimerforConsoleForm = setInterval(function () {
        devtoolsOpenForm = false;
        console.log(elementForm);
        if (devtoolsOpenForm) {
            $(loadOnFormDiv).html(docWarnMsg);
            //clearInterval(SetTimerforConsole);
            alert(docWarnMsg);
        }
        else {
            clearInterval(SetTimerforConsoleForm);
            //For PDF Viewer Loading
            $(loadOnFormDiv).html("Loading...");
            var objInputstr = { "FileName": FileName };
            var jsonData = JSON.stringify(objInputstr);
            $.ajax({
                type: "POST",
                url: PdfViewerUrl,
                // url: '<%=ResolveUrl("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")%>',
                // url: '@Url.Content("~/Common/WebServices/PDF_Viewer.asmx/GetPDF_FilePath")',
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var result = JSON.parse(response.d);
                    if (result.msgType == "success") {
                        $(loadOnFormDiv).html(result.msg);
                    }
                    else {
                        custAlertMsg(result.msg, "error");
                    }
                },
                failure: function (response) {
                    // alert(response.d);
                },
                error: function (xhr, textStatus, error) {
                    if (typeof console == "object") {
                        console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                        // var msg = textStatus;
                        var msg = "";
                        if (xhr.status === 0) {
                            msg = 'No server Connection, Verify Network.';
                        } else if (xhr.status == 404) {
                            msg = 'Requested page not found. [404]';
                        } else if (xhr.status == 500) {
                            msg = 'Internal Server Error [500].';
                        }
                        //else if (textStatus === 'parsererror') {
                        //    msg = 'Requested JSON parse failed.';
                        //} else if (textStatus === 'timeout') {
                        //    msg = 'Time out error.';
                        //} else if (textStatus === 'abort') {
                        //    msg = 'Ajax request aborted.';
                        //} else {
                        //    msg = 'Uncaught Error.\n' + xhr.responseText;
                        //}
                        if (msg != "") {
                            custAlertMsg(msg, "error");
                        }
                    }
                }
            });
        }
    }, 1000);
}

$(document).on('shown.bs.modal', '#IdleTO_Logout_popup', function (event) {
    if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
    }
});