﻿
//JS Function for Prepopulate MyProfile Label using onclick function
function PrepopulateUserProfileBind(DefinedUrl) {
    $.ajax({
        type: "POST",
        url: DefinedUrl,
        //url: '/UserManagement/WebServicesList.asmx/MyProfileGetDataToMyProfilePrepoulateLabel',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            var MyProfileObj = response.d;
            var result = $.parseJSON(MyProfileObj);
            var FirstName = result['FirstName'];
            var LastName = result['LastName'];
            var Name = FirstName +' '+ LastName;
            var Department = result['Department'];
            var Designation = result['Designation'];
            var Email = result['Email'];
            var Image1 = result['Image'];

            
            $('#lblFullNameMVC').text(Name);
            $('#lblDepartmentMVC').text(Department);
            $('#lblDesignationMVC').text(Designation);
            $('#lblEmailMVC').text(Email);
            //$('#MyProfImage').attr('src', Image1);
            $('#MyImage').attr('src', Image1);
            //$('#MyImage1').attr('src', Image1);
            //alert($('#MyImage1').attr('src'));
        },
        failure: function (response) {
            // alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    })
}
