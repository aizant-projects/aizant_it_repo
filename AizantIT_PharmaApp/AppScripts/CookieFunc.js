﻿//Reading the Cookie by its name and returning  the value.
function readCookie(CookieName) {  
    var docCookieArray = document.cookie.split(';');   
    var cookieVal = null;
    for (var i = 0; i < docCookieArray.length; i++) {
        var cookieItem = docCookieArray[i];
        cookieItem = cookieItem.trim();
        
        if (cookieItem.indexOf(CookieName) == 0) {
            return cookieItem.substring(CookieName.length+1, cookieItem.length);
        }
    }
    return cookieVal;
}

//Deleting the Cookies by its name.
function deleteCookies(cookieObj) {   
    for (var i = 0; i < cookieObj.length; i++) {
        DeleteCookie(cookieObj[i]);
    }   
}

function DeleteCookie(CookieName) {
    var expireDate = new Date();
    expireDate.setDate(expireDate.getDate() - 1);
    var strDelCookie = CookieName + '=; Path=/; Expires=' + expireDate;
    document.cookie = strDelCookie;   
}

// Checking the cookie and Redirecting to Login page if There is no cookie.
function CheckWetherCK_EmpIDandCK_SessionTO() {
    var LoginEmpIDCheck = readCookie("CookieUserIn");
    var SessionTimeOut = readCookie("CookieExpire");
    if (LoginEmpIDCheck == null || LoginEmpIDCheck == '' || SessionTimeOut == null || SessionTimeOut=='') {        
        deleteCookies(["CookieExpire", "CookieUserIn"]);
        window.open("/UserLogin.aspx", "_self");
    }
}