﻿//validate email ID
function IsValidEmail(email) {
    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return expr.test(email);
};

function ValidatePhoneNo(evt) {
    if ((evt.keyCode > 47 && evt.keyCode < 58))
        return evt.returnValue;
    return evt.returnValue = '';
};
function ValidatePhoneNoOnPaste() {
    var ArraypastedData = window.event.clipboardData.getData('text').split('');
    var AllowedRegularexpfilter = /^[0-9]*$/;
    for (var i = 0; i < ArraypastedData.length; i++) {
        if (!AllowedRegularexpfilter.test(ArraypastedData[i])) {
            custAlertMsg('Can\'t paste! Since there\'s a content other than <b>Numerics</b> in your copied text.', "warning");
            return false;
        }
    }
    return true;
}
function ValidateFreeText(evt) {
    if ((evt.keyCode !== 34 && evt.keyCode !== 39
        && evt.keyCode !== 123 && evt.keyCode !== 125
        && evt.keyCode !== 35 && evt.keyCode !== 36))
        return true;
    return false;
};
function ValidateFreeTextOnPaste() {
    var ArraypastedData = window.event.clipboardData.getData('text').split('');
    if (ArraypastedData.includes("$") ||
        ArraypastedData.includes("\'") ||
        ArraypastedData.includes("\"") ||
        ArraypastedData.includes("#")) {
        custAlertMsg('Can\'t paste! Since there\'s a Special character(s) like <b>$</b>, <b>\'</b>, <b>"</b> and <b>#</b>  in your copied text.', "warning");
        return false;
    }
    return true;
}
//count for Security Question(No of Characters allowed)
function characterCounter(controlId, countControlId, maxCharlimit) {

    if (controlId.value.length > maxCharlimit)
        controlId.value = controlId.value.substring(0, maxCharlimit);
    else
        countControlId.value = maxCharlimit - controlId.value.length;
}

//Only for Alphabets and Special characters along with Space
function ValidateAlphaNumericWithSpecialChars(evt) {
    // var Regularexpfilter = /^[a-zA-Z!”$%&’()*\=+, \/';:\"[\\\]\^_`{|}~@#<>.?-]+$/;
    var AllowedRegularexpfilter = /^[0-9a-zA-Z’/ '_-]*$/; // For TaskID :4598.
    //var Regularexpfilter = "^[^<>{}\"/|;:.,~!?@#$%^=&*\\]\\\\()\\*$";
    //var Regularexpfilter = /^[A-Z@~`!@#$%^&*()_=+{}\\\\';:\"\\/?>.<,-]*$/i;
    var IsValidText_bool = AllowedRegularexpfilter.test(evt.key);
    if (IsValidText_bool === false) {
        return false;
    }
    return true;
}

function ValidateAlphaWithSpecialChars(evt) {
    // var Regularexpfilter = /^[a-zA-Z!”$%&’()*\=+, \/';:\"[\\\]\^_`{|}~@#<>.?-]+$/;
    var AllowedRegularexpfilter = /^[a-zA-Z’/ '_-]/; // For TaskID :4598.
    //var Regularexpfilter = "^[^<>{}\"/|;:.,~!?@#$%^=&*\\]\\\\()\\*$";
    //var Regularexpfilter = /^[A-Z@~`!@#$%^&*()_=+{}\\\\';:\"\\/?>.<,-]*$/i;
    var IsValidText_bool = AllowedRegularexpfilter.test(evt.key);
    if (IsValidText_bool === false) {
        return false;
    }
    return true;
}
function ValidateAlphaWithSpecialCharsOnPaste() {
    var ArraypastedData = window.event.clipboardData.getData('text').split('');
    var AllowedRegularexpfilter = /^[a-zA-Z’/ '_-]*$/;
    for (var i = 0; i < ArraypastedData.length; i++) {
        if (!AllowedRegularexpfilter.test(ArraypastedData[i])) {
            custAlertMsg('Copied Text Should be Allowed Only <b>Alphabet </b> and <b>_</b>, <b>-</b>, <b>\'</b>, <b>/</b> This Special Characters.', "warning");
            return false;
        }
    }
    return true;
}
//Only for Alphabets and Special characters along with Space
//function ValidateAlphaNumericWithSpecialChars(evt) {
//     var Regularexpfilter = /^[a-zA-Z!”$%&’()*\=+, \/';:\"[\\\]\^_`{|}~@#<>.?-]+$/;
//    var AllowedRegularexpfilter = /^[0-9a-zA-Z’/ '_-]*$/; // For TaskID :4598.
//    var Regularexpfilter = "^[^<>{}\"/|;:.,~!?@#$%^=&*\\]\\\\()\\*$";
//    var Regularexpfilter = /^[A-Z@~`!@#$%^&*()_=+{}\\\\';:\"\\/?>.<,-]*$/i;
//    var IsValidText_bool = AllowedRegularexpfilter.test(evt.key);
//    if (IsValidText_bool == false) {
//        return false;
//    }
//    return true;
//}
function bindOnPaste() {
    $(".ValidateAlphaNumericWithSpecialChars").bind("paste", function (e) {
        var pastedData = e.originalEvent.clipboardData.getData('text');
        var pastedAry = pastedData.split('');
        var AllowedRegularexpfilter = /^[0-9a-zA-Z’/ '_-]*$/;
        for (var i = 0; i < pastedAry.length; i++) {
            if (!AllowedRegularexpfilter.test(pastedAry[i])) {
                e.preventDefault();
                custAlertMsg('Copied Text Should Be Allowed Only <b>AlphaNumeric</b>  and  <b>_</b>, <b>-</b>, <b>\'</b>, <b>/</b>  This Special Characters.', "warning");
                return false;
            }
        }
        return true;
    });
    $(".ValidateAlphaWithSpecialChars").bind("paste", function (e) {
        var pastedData = e.originalEvent.clipboardData.getData('text');
        var pastedAry = pastedData.split('');
        var AllowedRegularexpfilter = /^[a-zA-Z’/ '_-]*$/;
        for (var i = 0; i < pastedAry.length; i++) {
            if (!AllowedRegularexpfilter.test(pastedAry[i])) {
                e.preventDefault();
                custAlertMsg('Copied Text Should be Allowed Only <b>Alphabet </b> and <b>_</b>, <b>-</b>, <b>\'</b>, <b>/</b> This Special Characters.', "warning");
                return false;
            }
        }
        return true;
    });
    $(".ValidatePhoneNo").bind("paste", function (e) {
        var pastedData = e.originalEvent.clipboardData.getData('text');
        var pastedAry = pastedData.split('');
        var AllowedRegularexpfilter = /^[0-9]*$/;
        for (var i = 0; i < pastedAry.length; i++) {
            if (!AllowedRegularexpfilter.test(pastedAry[i])) {
                e.preventDefault();
                custAlertMsg('Copied Text Should be Allowed Only <b>Numbers </b>', "warning");
                return false;
            }
        }
        return true;
    });
    $(".ValidateAlpha").bind("paste", function (e) {
        var pastedData = e.originalEvent.clipboardData.getData('text');
        var pastedAry = pastedData.split('');
        var AllowedRegularexpfilter = /^[a-zA-Z ]*$/;
        for (var i = 0; i < pastedAry.length; i++) {
            if (!AllowedRegularexpfilter.test(pastedAry[i])) {
                e.preventDefault();
                custAlertMsg('Can\'t paste! Other than Alphabet', "warning");
                return false;
            }
        }
        return true;
    });
    $(".ValidateAlphawithDot").bind("paste", function (e) {
        var pastedData = e.originalEvent.clipboardData.getData('text');
        var pastedAry = pastedData.split('');
        var AllowedRegularexpfilter = /^[a-zA-Z/. ]*$/;
        for (var i = 0; i < pastedAry.length; i++) {
            if (!AllowedRegularexpfilter.test(pastedAry[i])) {
                e.preventDefault();
                custAlertMsg('Can\'t paste! Other than Alphabet & .', "warning");
                return false;
            }
        }
        return true;
    });
    $(".ValidateAlphaNumericSpace").bind("paste", function (e) {
        var pastedData = e.originalEvent.clipboardData.getData('text');
        var pastedAry = pastedData.split('');
        var AllowedRegularexpfilter = /^[0-9a-zA-Z’ ]*$/;
        for (var i = 0; i < pastedAry.length; i++) {
            if (!AllowedRegularexpfilter.test(pastedAry[i])) {
                e.preventDefault();
                custAlertMsg('Can\'t paste! Other than AlphaNumeric', "warning");
                return false;
            }
        }
        return true;
    });
    $(".ValidateFreeText").bind("paste", function (e) {
        var pastedData = e.originalEvent.clipboardData.getData('text');
        if (pastedData.includes("$") ||
            pastedData.includes("\'") ||
            pastedData.includes("\"") ||
            pastedData.includes("#")) {
            e.preventDefault();
            custAlertMsg('Can\'t paste! Since there\'s a Special character(s) like <b>$</b>, <b>\'</b>, <b>"</b> and <b>#</b>  in your copied text.', "warning");
        }
    });
}

//for only alphabets
function ValidateAlpha(evt) {
    var keyCode = (evt.which) ? evt.which : evt.keyCode;
    if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && keyCode !== 32) {
        return false;
    }
    return true;
}
function ValidateAlphawithDot(evt) {
    var keyCode = (evt.which) ? evt.which : evt.keyCode;
    if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && keyCode !== 32 && keyCode !==46) {
        return false;
    }
    return true;
}
function ValidateAlphaOnPaste() {
    var ArraypastedData = window.event.clipboardData.getData('text').split('');
    var AllowedRegularexpfilter = /^[a-zA-Z ]*$/;
    for (var i = 0; i < ArraypastedData.length; i++) {
        if (!AllowedRegularexpfilter.test(ArraypastedData[i])) {
            custAlertMsg('Can\'t paste! Since there\'s a content other than <b>Alphabet(s)</b> in your copied text.', "warning");
            return false;
        }
    }
    return true;
}
function ValidateAlphaUnderscoreandMinusandBrackets(evt) {
    var keyCode = (evt.which) ? evt.which : evt.keyCode;
    if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && keyCode !== 32 && keyCode !== 45 && keyCode !== 95 && keyCode !== 40 && keyCode !== 41) {
        return false;
    }
    return true;
}
function ValidateAlphaUnderscoreandMinusandBracketsOnPaste() {
    var ArraypastedData = window.event.clipboardData.getData('text').split('');
    var AllowedRegularexpfilter = /^[a-zA-Z() _-]*$/;
    for (var i = 0; i < ArraypastedData.length; i++) {
        if (!AllowedRegularexpfilter.test(ArraypastedData[i])) {
            custAlertMsg('Can\'t paste! Since there\'s a content other than <b>Alphabet(s)</b> and <b>_</b>, <b>-</b>, <b>()</b> in your copied text.', "warning");
            return false;
        }
    }
    return true;
}
// Alphanumeric without space [No space allowed]
function ValidateAlphaNumeric(evt) {
    var keyCode = (evt.which) ? evt.which : evt.keyCode;
    //if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && (keyCode < 48 || keyCode > 57) && keyCode == 32) {
    if ((keyCode > 47 && keyCode < 58) || (keyCode > 64 && keyCode < 91) || (keyCode > 96 && keyCode < 123)) {
        return true;
    }
    return false;
}
function ValidateAlphaNumericOnPaste() {
    var ArraypastedData = window.event.clipboardData.getData('text').split('');
    var AllowedRegularexpfilter = /^[0-9a-zA-Z’]*$/;
    for (var i = 0; i < ArraypastedData.length; i++) {
        if (!AllowedRegularexpfilter.test(ArraypastedData[i])) {
            custAlertMsg('Copied Text Should Be Allowed Only <b>AlphaNumeric</b>.', "warning");
            return false;
        }
    }
    return true;
}

// Alphanumeric with space
function ValidateAlphaNumericSpace(evt) {
    var keyCode = (evt.which) ? evt.which : evt.keyCode;
    //if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && (keyCode < 48 || keyCode > 57) && keyCode == 32) {
    if ((keyCode > 47 && keyCode < 58) || (keyCode > 64 && keyCode < 91) || (keyCode > 96 && keyCode < 123) || keyCode === 32) {
        return true;
    }
    return false;
}

// Allow Only Non Decimal Numbers 
function AllowNonDecimalNumbers(evt) {
    if ((evt.keyCode > 47 && evt.keyCode < 58))
        return true;
    return false;
}

// Allow Only Decimal Numbers 
function AllowDecimalNumbers(evt) {
    if ((evt.keyCode > 47 && evt.keyCode < 58 && evt.keyCode === 190))
        return true;
    return false;
}

////for only numbers
//function OnlyNumber(control) {
//    evt = (this.evt) ? this.evt : window.event;
//    var charCode = (evt.which) ? evt.which : evt.keyCode;
//    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
//        return false;
//    }
//    return true;
//}

// to restrict an enter key
function RestrictEnterKey(event) {
    if (event.which === '13') {
        event.preventDefault();
    }
}

//Nospace in Login and SignUp
function nospaces(evt) {
    if (evt.keyCode === 32) {
        return false;
    }
    else {
        return true;
    }
}



function onlyDotsAndNumbers(txt, event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode === 46) {
        if (txt.value.indexOf(".") < 0)
            return true;
        else
            return false;
    }

    if (txt.value.indexOf(".") > 0) {
        var txtlen = txt.value.length;
        var dotpos = txt.value.indexOf(".");
        if ((txtlen - dotpos) > 3)
            return false;
    }

    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function onlyDotHyphenCommaAndNumbers(txt, event) {

    var charCode = (event.which) ? event.which : event.keyCode
    if (charCode === 44 || charCode === 45 || charCode === 46) {
        if (txt.value.indexOf(".") > 0 || txt.value.indexOf(",") < 0 || txt.value.indexOf("-") < 0)
            return true;
        else
            return false;
    }
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function IsValidURL(str) {
    var pattern = new RegExp('^(https?:\/\/)?' + // protocol
        '((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}|' + // domain name
        '((\d{1,3}\.){3}\d{1,3}))' + // OR ip (v4) address
        '(\:\d+)?(\/[-a-z\d%_.~+]*)*' + // port and path
        '(\?[;&a-z\d%_.~+=-]*)?' + // query string
        '(\#[-a-z\d_]*)?$', 'i'); // fragment locater
    if (!pattern.test(str)) {
        alert("Please enter a valid URL.");
        return false;
    } else {
        return true;
    }
}
//For making the Sentence words to caps.
function ConvertCustAlertMsg(Custmsg) {
    var words = Custmsg.split(" ");
    for (i = 0; i < words.length; i++) {
        if (words[i].length > 1 || i === 0) {
            words[i] = words[i].substr(0, 1).toUpperCase() + words[i].substr(1);
        }
    }
    return words.join(" ");
}
//For restricting percentage and square brackets
function RestrictPercentageSquareBraces(evt) {
    var AllowedRegularexpfilter = /^[%[\\\]]*$/;
    var IsValidText_bool = AllowedRegularexpfilter.test(evt.key);
    if (IsValidText_bool === true) {
        return false;
    }
    return true;
}

function GetUniqueArray(anArray) {
    var result = [];
    $.each(anArray, function (i, v) {
        if ($.inArray(v, result) === -1)
            result.push(v);
    });
    return result;
}
//For restricting Special Characters *,\,[,],%,&,",?,;:|. for Document Number
function RestrictDirectoryCharacters(evt) {
    var AllowedRegularexpfilter = /^[*&%"?';:|^,.<>[\\\]]*$/;
    var IsValidText_bool = AllowedRegularexpfilter.test(evt.key);
    if (IsValidText_bool === true) {
        return false;
    }
    return true;
}
//Restricting Copy Paste Special Characters *,\,[,],%,&,",?,;:|. for Document Number
function RestrictDirectoryCharactersOnPaste() {
    var ArraypastedData = window.event.clipboardData.getData('text').split('');
    var AllowedRegularexpfilter = /^[*&%"?';:|^,.<>[\\\]]*$/;
    for (var i = 0; i < ArraypastedData.length; i++) {
        if (AllowedRegularexpfilter.test(ArraypastedData[i])) {
            custAlertMsg('Copied Text can Not be Allowed <b>Alphabet </b> <b>*</b>, <b>\\\</b>,<b>[</b>,<b>]</b>,<b>&</b>,<b>%</b>,<b>"</b>,<b>?</b>,<b>\'</b> This Special Characters.', "warning");
            return false;
        }
    }
    return true;
}
function RestrictStarFrontSlashSquareBraces(evt) {
    var AllowedRegularexpfilter = /^[*[\\\]]*$/;
    var IsValidText_bool = AllowedRegularexpfilter.test(evt.key);
    if (IsValidText_bool === true) {
        return false;
    }
    return true;
}
//Restricting Copy Paste Special Characters *,[,],\ for Doument Name
function ValidateStarFrontSlashSquareBracesOnPaste() {
    var ArraypastedData = window.event.clipboardData.getData('text').split('');
    var AllowedRegularexpfilter = /^[*[\\\]]*$/;
    for (var i = 0; i < ArraypastedData.length; i++) {
        if (AllowedRegularexpfilter.test(ArraypastedData[i])) {
            custAlertMsg('Copied Text can Not be Allowed <b>Alphabet </b> <b>*</b>, <b>\\\</b>,<b>[</b>,<b>]</b>, This Special Characters.', "warning");
            return false;
        }
    }
    return true;
}
function Allowcommadashnumbers(evt) {
    var AllowedRegularexpfilter = /^[1-9\,\-]*$/;
    var IsValidText_bool = AllowedRegularexpfilter.test(evt.key);
    if (IsValidText_bool === true) {
        return true;
    }
    return false;
}
function AllowcommadashnumbersOnPaste() {
    var ArraypastedData = window.event.clipboardData.getData('text').split('');
    var AllowedRegularexpfilter = /^[1-9\,\-]*$/;
    for (var i = 0; i < ArraypastedData.length; i++) {
        if (AllowedRegularexpfilter.test(ArraypastedData[i])) {
            custAlertMsg('Copied Text can  Allow <b>Numbers </b>1-9 <b>, </b>- <b> This Special Characters.', "warning");
            return true;
        }
    }
    return false;
}
