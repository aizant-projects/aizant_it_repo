﻿//JS Function for getting binding Roles on Dept
function MyRolesGettingDrop(DefinedUrl, ModuleUrl) {
    var ModuleDDL = $('#ddlModlules');
    var AssignedRolesDDL = $('#ddlAssignRoles');
    var AccessibleDeptDDL = $('#ddlAccessibleDept');

    var NewModuleUrl = ModuleUrl;
    $.ajax({
        type: "POST",
        url: DefinedUrl,
        //url: '/UserManagement/WebServicesList.asmx/MyRolesDropDownBindPrePopulateLoadModules',
        contentType: "application/json; charset=utf-8",
        dataType: "json",

        success: function (data) {
            var MyProfileObj = data.d;
            var result = $.parseJSON(MyProfileObj);
            var ModuleData = result['records'];
            //alert(ModuleData);
            ModuleDDL.append($('<option/>', { value: 0, text: 'Select Module' }));

            //if (ModuleDDL.selectable = "0") {
            //    document.getElementById("divassignRoles").style.display = "none";
            //} else {
            //    document.getElementById("divassignRoles").style.display = "inline-block";
            //}

            for (var i = 0; i < ModuleData.length; i++) {
                ModuleDDL.append($('<option/>', { value: ModuleData[i].ModuleID, text: ModuleData[i].ModuleName }));
            }
        },
        failure: function (response) {
            // alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
    ModuleDDL.change(function () {
        var SelectedModuleID = $(this).val();
        var objStr = { "SelectedModuleID": SelectedModuleID };
        var jsonData = JSON.stringify(objStr);

        if ($(this).val() == "0") {
            //document.getElementById("divassignRoles").style.display = "none";
            AssignedRolesDDL.empty();
            AccessibleDeptDDL.empty();
        }
        else {
            //OnClickAssignedRolesAndDeptsUrl();
            $.ajax({
                //url: DefinedUrl,
                type: "POST",
                //data: { SelectedModuleID: $(this).val()},
                url: NewModuleUrl,
               // url: '/UserManagement/WebServicesList.asmx/MyRolesGettingRolesAndDepartment',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: jsonData,
                //contentType: "application/json; charset=utf-8",
                //data: {},
                success: function (data) {
                    var MyRoleAndDeptObj = data.d;
                    var result = $.parseJSON(MyRoleAndDeptObj);
                    var ModuleRoles = result['records1'];
                    var ModuleDept = result['records2'];
                    AssignedRolesDDL.empty();
                    AccessibleDeptDDL.empty();
                    for (var i = 0; i < ModuleRoles.length; i++) {
                        AssignedRolesDDL.append($('<li/>', { value: ModuleRoles[i].RoleID, text: ModuleRoles[i].RoleName }));
                    }
                    for (var i = 0; i < ModuleDept.length; i++) {
                        AccessibleDeptDDL.append($('<li/>', { value: ModuleDept[i].DeptID, text: ModuleDept[i].DepartmentName }));
                    }
                },
                failure: function (response) {
                    // alert(response.d);
                },
                error: function (response) {
                    alert(response.d);
                }
            });
        }
    });
}

//JS Function for Binding Dept roles
function MyRolesGettingDropDown(DefinedUrl) {
    var ModuleDDL = $('#ddlModules');
    var AssignedRolesDDL = $('#ddlAssignRoles');
    var AccessibleDeptDDL = $('#ddlAccessibleDept');

    //var NewModuleUrl = ModuleUrl;
    $.ajax({
        type: "POST",
        url: DefinedUrl,
        ////url: '/UserManagement/WebServicesList.asmx/MyRolesDropDownBindPrePopulateLoadModules',
        contentType: "application/json; charset=utf-8",
        dataType: "json",

        success: function (data) {
            var MyProfileObj = data.d;
            var result = $.parseJSON(MyProfileObj);
            var ModuleData = result['records'];
            ////alert(ModuleData);
            ModuleDDL.append($('<option/>', { value: 0, text: 'Select Module' }));

            $("#ddlModules").empty();
            $("#ddlModules").append("<option value='0'>  -- Select Module -- </option>");
            for (var i = 0; i < ModuleData.length; i++) {
                var OptModule = "<option value='" + ModuleData[i].ModuleID + "'>" + ModuleData[i].ModuleName + "</option>";
                $("#ddlModules").append(OptModule);
            }
        },
        failure: function (response) {
            //// alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}

//JS Function for getting Roles and Dept based on selected module
function MyRolesOnSelect(DefinedUrl, SelectedModuleID) {
    var objStr = { "SelectedModuleID": SelectedModuleID };
    var jsonData = JSON.stringify(objStr);
    var ModuleDDL = $('#ddlModlules');
    var AssignedRolesDDL = $('#ddlAssignRoles');
    var AccessibleDeptDDL = $('#ddlAccessibleDept');
    if (SelectedModuleID == "0") {
        AssignedRolesDDL.empty();
        AccessibleDeptDDL.empty();
    }
    else {
        $.ajax({
            type: "POST",
            url: DefinedUrl,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: jsonData,
            success: function (data) {
                var MyRoleAndDeptObj = data.d;
                var result = $.parseJSON(MyRoleAndDeptObj);

                var ModuleRoles = result['records1'];
                var ModuleDept = result['records2'];
              
                AssignedRolesDDL.empty();
                AccessibleDeptDDL.empty();
                for (var i = 0; i < ModuleRoles.length; i++) {
                    AssignedRolesDDL.append($('<li/>', { value: ModuleRoles[i].RoleID, text: ModuleRoles[i].RoleName }));
                }
                for (var i = 0; i < ModuleDept.length; i++) {
                    AccessibleDeptDDL.append($('<li/>', { value: ModuleDept[i].DeptID, text: ModuleDept[i].DepartmentName }));
                }
            },
            failure: function (response) {
                //// alert(response.d);
            },
            error: function (response) {
                alert(response.d);
            }
        });
    }
}