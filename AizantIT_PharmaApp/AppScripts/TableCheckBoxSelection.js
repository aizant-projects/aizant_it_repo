﻿// For Header Checkbox

function chkHeaderSelect(ctrl) {
    
    var grid = $(ctrl).closest("table");
    $("input[type=checkbox]", grid).each(function () {
        if ($(ctrl).is(":checked")) {
            $(this).prop('checked', true);
        }
        else
        {
            $(this).prop('checked', false);
        }
    });
}

// For Checkbox Row
function chkRowSelect(ctrl) {    
    var grid = $(ctrl).closest("table");
    var chkHeader = $("[class='chkHeaderElement']", grid).children();
    if (!$(ctrl).is(":checked")) {
        $(chkHeader).prop('checked', false);
    }
    else {
        var totalRowsElements = $("[class=chkRowElement]", grid).children();
        var SelectedCount = 0;
        $(totalRowsElements).each(function (i, Element) {            
            if ($(Element).is(":checked")) {
                SelectedCount++;
            }
        });
        if (SelectedCount == totalRowsElements.length) {
            $(chkHeader).prop('checked', true);
        }
    }
}