﻿function getEveryoneComments(DefinedUrl, DocVersionID) {
    $.ajax({
        type: "GET",
        url: DefinedUrl,
        data: { "DocVID": DocVersionID },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            bindComments(response.d);
        },
        failure: function (response) {
            // alert(response.d);
        },
        error: function (response) {
            // alert(response.d);
        }
    });
}

function GetFormComments(DefinedUrl, FormVersionID) {
    $.ajax({
        type: "GET",
        url: DefinedUrl,
        data: { "FormVersionID": FormVersionID },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(response) {
            bindComments(response.d);
        },
        failure: function (response) {
            // alert(response.d);
        },
        error: function (response) {
            // alert(response.d);
        }
    });
}
function getReviewComments(DefinedUrl, DocVersionID,ProcessID) {
    $.ajax({
        type: "GET",
        url: DefinedUrl,
        data: { "DocVID": DocVersionID, "ProcessID": ProcessID},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            bindComments(response.d);
        },
        failure: function (response) {
            // alert(response.d);
        },
        error: function (response) {
            // alert(response.d);
        }
    });
}
function bindComments(CommentResult) {
    $("#divgvcomments").empty();    
    $(CommentResult).each(function () {
        var divComments = "<div class='col-lg-12 padding-none top bottom float-left'><div class='col-lg-6 top float-left'>"
            + "<label for='inputEmail3' class='padding-none profile_label col-xl-3 col-lg-3 col-sm-3 float-left'>UserName</label>"
            + "<div class='col-xl-9 col-lg-9 col-sm-9 profile_defined float-left'>"
            + "<span class='form-control login_input_sign_up'>" + this.EmpName + "</span>"
            + "</div></div><div class='col-lg-6 top float-left'><label for='inputEmail3' class='padding-none profile_label col-xl-3 col-lg-3 col-sm-3 float-left'>Role</label>"
            + "<div class='col-xl-9 col-lg-9 col-sm-9  profile_defined'>"
            + "<span class='form-control login_input_sign_up'>" + this.Role + "</span>"
            + "</div></div></div><div class='col-lg-12 commentStyle float-left'><label for='inputEmail3' class='col-sm-12 '>Comments</label>"
            + "<div class='col-sm-12 profile_defined float-left padding-left_di'>"
            + "<textarea Rows='3' readonly='readonly' class='form-control bottom col-12'>" + this.Comments + "</textarea>"
            + "</div></div>";
        $("#divgvcomments").append(divComments);
    });
}