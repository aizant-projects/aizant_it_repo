﻿var ShowLess = 'Yes';
var _NotifyURL = '';
var _ClearNotifyURL = '';
var _EmpID = 0;

//for Notification UI Script Start
$('#notificationsBody').click(function () {
    return false;
});
function ShowNotifyView() {
    $('#notificationsBody').toggleClass("ShowLess");
    $('#notificationsBody').toggleClass("ShowMore");
    $('#notificationFooterShowLess').toggle();
    $('#notificationFooterShowMore').toggle();
    loadNotifications(ShowLess, _NotifyURL, _ClearNotifyURL, _EmpID);
}
$('#notificationFooterShowLess').click(function () {
    ShowLess = 'Yes';
    ShowNotifyView();
    return false;
});
$('#notificationFooterShowMore').click(function () {
    ShowLess = 'No';
    ShowNotifyView();
    return false;
});

var selector = '.landing_links ul li a';

$(selector).on('click', function () {
    $(selector).removeClass('active');
    $(this).addClass('active');
});

$("#notificationLink").click(function () {
    $(".menu_list").hide();
    $(".notification_linkdiv").fadeToggle(300);
    $("#notificationContainer").fadeToggle(300);
    $('#notificationFooterShowMore').show();
    $('#notificationFooterShowLess').hide();
    ShowLess = 'Yes';
    loadNotifications(ShowLess, _NotifyURL, _ClearNotifyURL, _EmpID);
    return false;
});

$(".user_details_info").click(function () {
    $(".menu_list").toggle();
    $("#notificationContainer").hide();
    $(".test_time").hide();
    $(".notification_linkdiv").hide();
});
var NotificationContainerClick = false;
$("#notificationContainer").click(function () {
    NotificationContainerClick = true;
});
$(document).click(function () {
    $(".menu_list").hide();
    $(".test_time").hide();
    if (NotificationContainerClick == false) {
        $(".notification_linkdiv").hide();
        $("#notificationContainer").hide();
    }
    NotificationContainerClick = false;
    $('#notificationsBody').removeClass("ShowMore");
    $('#notificationsBody').addClass("ShowLess");
});
//for Notification UI Script End
var startTimer;
//Notification Functional Script Start
function Init_Notification(NotifyURL, ClearNotifyURL, EmpID) {
    _NotifyURL = NotifyURL;
    _ClearNotifyURL = ClearNotifyURL;
    _EmpID = EmpID;
    loadNotifications(ShowLess, _NotifyURL, _ClearNotifyURL, _EmpID);
    StartNotifyTimer();       
}

function StartNotifyTimer() {    
    startTimer = setInterval(function () {
        var sec = 0;
        var seconds = sec + 1;
        if (seconds >= 10) {
            seconds = 0;
            loadNotifications(ShowLess, _NotifyURL, _ClearNotifyURL, _EmpID);
        }
        sec = seconds;
    }, 1000);
}

function loadNotifications(ShowLess, NotifyURL, ClearNotifyURL, EmpID) {
    var objStr = { "Emp_ID": EmpID, "ShowLess": ShowLess };
    var jsonData = JSON.stringify(objStr);
    clearInterval(startTimer);
    $.ajax({
        type: "POST",
        url: NotifyURL,
        data: jsonData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            OnNotifySucceed(response, NotifyURL, ClearNotifyURL, EmpID, ShowLess);
            StartNotifyTimer();
        },
        failure: function (response) {
            // alert(response.d);
        },
        error: function (response) {
            // alert(response.d);
        }
        //error: function (xhr, textStatus, error) {
        //    if (typeof console == "object") {
        //        console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
        //        // var msg = textStatus;
        //        var msg = "";
        //        if (xhr.status === 0) {
        //            msg = 'No server Connection, Verify Network.';
        //        } else if (xhr.status == 404) {
        //            msg = 'Requested page not found. [404]';
        //        } else if (xhr.status == 500) {
        //            msg = 'Internal Server Error [500].';
        //        }
        //        //else if (textStatus === 'parsererror') {
        //        //    msg = 'Requested JSON parse failed.';
        //        //} else if (textStatus === 'timeout') {
        //        //    msg = 'Time out error.';
        //        //} else if (textStatus === 'abort') {
        //        //    msg = 'Ajax request aborted.';
        //        //} else {
        //        //    msg = 'Uncaught Error.\n' + xhr.responseText;
        //        //}
        //        if (msg != "") {
        //            custAlertMsg(msg, "error");
        //        }
        //    }
        //}
    });
}

function OnNotifySucceed(response, NotifyURL, ClearNotifyURL, EmpID, ShowLess) {
    var div = $("#notificationsBody div").eq(0).clone(true);
    var notificationsBody = response.d;
    $("#notificationsBody div").eq(0).remove();
    $("#notificationsBody").empty();
    $("#notification_count").removeAttr('style');
    document.getElementById("notification_count").innerHTML = 0;
    $(notificationsBody).each(function () {
        var n = this.ReferenceLink.includes("#");
        var TolltipTitle = 'Click to Visit The Page.';
        if (n == true) {
            TolltipTitle = 'Click On Close Button To Remove the Notification.';
        }
        var ClearNotifyOnClick = 'ClearNotify(' + this.NotificationID + ',"' + ClearNotifyURL + '", "' + NotifyURL + '",' + EmpID + ',"' + ShowLess + '")';
        var divNotification = "<div title='" + TolltipTitle + "' class='panel_list_notificaton_" + this.Status
            + "' id='" + this.NotificationID + "'>"
            + "<a href='" + this.ReferenceLink + "' onclick='RedirectTo(this);' class='notification_name col-lg-11 float-left' >" + this.Description
            + "</a><span onclick='" + ClearNotifyOnClick + "'"
            + "class=' col-lg-1  float-left CloseNotification' title='Click to Clear'>x</span>"
            + "<div class='col-lg-12  float-left'><div class='float-left moudle_label_" + this.Module + "' >"
            + this.Module + "</div><div class='float-right moudle_time'>" + this.Date + "</div></div></div>";
        $("#notificationsBody").append(divNotification);
        div = $("#notificationsBody div").eq(0).clone(true);
        $("#notification_count").html(this.UnseenNotificationCount);
        document.getElementById("notification_count").innerHTML = this.UnseenNotificationCount;
        document.getElementById("hdnNotifyActualCount").value = this.TotalNotifyCount;
    });
    if (document.getElementById("notification_count").innerHTML == 0) {
        document.getElementById("notification_count").style.display = 'none';
    }
    if (document.getElementById("hdnNotifyActualCount").value > 4) {
        if (ShowLess == "Yes") {
            $('#notificationFooterShowMore').show();
        }
    }
    else {
        $('#notificationFooterShowMore').hide();
        $('#notificationFooterShowLess').hide();
    }
    if (document.getElementById("hdnNotifyActualCount").value == "0" || response.d.length == 0) {
        $("#notificationsBody").prepend("<div id='ntyEmpty' style='text-align:center;font-weight: bold;'>No Notifications To Show</div>");
    }
}

function ClearNotify(NotifyID, ClearNotifyURL, GetNotifyURL, EmpID, ShowLess) {
    var objStr = { "Notification_ID": NotifyID, "Emp_ID": EmpID };
    var tempClearNotifyURL = ClearNotifyURL;
    var jsonData = JSON.stringify(objStr);
    $.ajax({
        type: "POST",
        url: tempClearNotifyURL,
        data: jsonData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function () {
            loadNotifications(ShowLess, GetNotifyURL, ClearNotifyURL, EmpID);
            if (document.getElementById("hdnNotifyActualCount").value = 0) {
                document.getElementById("notification_count").style.display = 'none';
                $("#notificationContainer").hide();
            }
        },
        failure: function (response) {
            // alert(response.d);
        },
        error: function (response) {
            // alert(response.d);
        }
    });
}

function RedirectTo(anchor) {
    var RedirectUrl = anchor.href;
    window.open(RedirectUrl, '_self');
}
//Notification Functional Script Start