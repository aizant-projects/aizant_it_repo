﻿$(function () {
    $("#btnRelatedObservations").on("click", function () {
        $("#draggablelist").show();
        $("#draggablelist").draggable();
    });
    $("#relatedobsclose").on("click", function () {
        $("#draggablelist").hide();
    });
});

$("#draggablelist").show();

function GetRelatedObservationsForDocument(DocID) {
    UserSessionCheck();
   // var objStr = { "DocID": DocID };
    //var jsonData = JSON.stringify(objStr);
    $.ajax({
        type: "GET",
        url: AizantIT_WebAPI_URL + "/api/TMS_ML/GetObservationsListonDocID?DocID="+DocID,
        contentType: "application/json; charset=utf-8",
        //beforeSend: function (request) {
        //    request.setRequestHeader("ActiveUserSession", 'dm0rucf55w3bpwpqqxq5wnkg');
        //},
        headers: WebAPIRequestHeaderJson,// localStorage.getItem('token') },
        dataType: "json",
        success: function (response) {
            $(".related_draglist").empty();
            if (response.length > 0) {
                for (var i = 0; i < response.length; i++) {
                    var liObservations;
                    if (response[i].ObservationNum != "") {
                        liObservations = "<li class='internal_osb_ml'>" + response[i].ObservationDescription + "</li>";
                    }
                    else {
                        liObservations = "<li>" + response[i].ObservationDescription + "</li>";
                    }
                    $(".related_draglist").append(liObservations);
                }
                $(".relatedobs_count").html(response.length);
                $("#btnRelatedObservations").show();
            }
            else {
                $(".related_draglist").append("No Related Observations");
            }
        },
        failure: function (response) {
            custAlertMsg("Failure : unable to fetch the Related Observations", "error");
        },
        error: function (response) {
            custAlertMsg(response.statusText, "error");
        }
    });
}
function makeRelatedObservationsForDocumentToDefault() {
    $("#btnRelatedObservations").hide(); 
    $("#draggablelist").css({
        //'display': 'block',
        'left': '10px',
        'top': '40px'
    });
    $("#draggablelist").hide();
    $(".related_draglist").empty();
    $(".related_draglist").append("Loading...");
   
}