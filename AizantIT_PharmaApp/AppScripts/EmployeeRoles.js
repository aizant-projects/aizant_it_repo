
//get employee Module Role Wise Departments using Ajax call
function GetEmployeeRoles(empID, moduleID, roleId, visible, divEmployeeAssignRole) {
    var jsonSend = { 'EmpID': parseInt(empID), 'ModuleID': parseInt(moduleID), 'RoleID': parseInt(roleId) };
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        data: jsonSend,
        dataType: "json",
        url: "/UserManagement/WebServicesList.asmx/GetRolewiseDepartments",
        success: function (result) {
            var data = JSON.parse(result.d);
            empRoleList = data.RolesList;//json RoleList result store in EmproleList array
            empDepartmentList = data.DepartmentsList;//json DepartmentList result store in DepartmentsList array
            empModuleList = data.ModuleList;
            var isAccessible = data.IsAccessDept;//this is used to check particular role isAccessible True/false
            if (isAccessible == false) {
                    $('#divDeptsList').hide();
                    IsAccessibleDepartments = 1;
                }
            else {
                    $('#divDeptsList').show();
                    IsAccessibleDepartments = 0;
                }
                var roleBasedDepartments = [];//RoleWIse Departments Id's store
                if (empRoleList != 0) {
                    RoleWiseDepartmentsLoading(empModuleList, empRoleList, empDepartmentList, visible,divEmployeeAssignRole);//bind the role Wise accesble Department in Div Tag
                }
                else {
                    if (empID == 0) {
                        $(divEmployeeAssignRole).empty();//if Employee Id is zero Clear the Div content
                    }
                    else {
                        if (roleId != 0) {
                            if (empDepartmentList == 0) {//if Employee Department List empty Display the Notification in div tag
                                $(divEmployeeAssignRole).append("<div class='text-center accesibleDepartment'> Roles Doesn't Exists</div>");
                            }
                        } else {//if Employee roleList empty Display the Notification in div tag
                            $(divEmployeeAssignRole).append("<div class='text-center accesibleDepartment'> No roles exists on this module</div>");
                        }
                    }
                }
                if (roleId != 0) {//IF Role Exist Change button Name 
                    if (visible == true) {
                        if (empRoleList.length > 0) {
                            $("#btnSubmit").text('Update');
                        }
                        else {
                            $("#btnSubmit").text('Add');
                        }
                    } 
                    
                    //Select Departments id's add to Emps array
                    $.each(empDepartmentList, function (i, item) {
                        roleBasedDepartments.push(item.DeptID);
                    });
                    AccessibleDepartmentRefresh(roleBasedDepartments);
                }
                else {
                    AccessibleDepartmentRefresh(0);
                }
        },
        error: function (jqXHR, exception) {
            alert(exception);
        }
    });
}

//Employee Module Role Wise Department Load on Div tag
function RoleWiseDepartmentsLoading(empModuleList, empRoleList, empDepartmentList, visible, divEmployeeAssignRole) {
    var displayClass = "";//Cross Icon Remove
    if (visible == false) {
        displayClass = "role_hide_visible";
    }
    $.each(empModuleList, function (i, itemModule) {
        var CurrentModuleElements = "";
        moduleName = itemModule.ModuleName;
        var moduleMainId = 'divModule' + itemModule.ModuleID;
        CurrentModuleElements = CurrentModuleElements + "<div id='" + moduleMainId + "' class='col-lg-12 padding-none float-left  bottom'>"
            + "<div class='col-lg-12 float-left padding-none bottom'>"
            + "<div class='col-lg-12 float-left role_grid_header bottom'>"
            + "<span  class='col-lg-10 float-left roleHeader'>Module :<b style='color: #126571;'>" + itemModule.ModuleName + "</b></span>"
            + "</div>" //role_grid_header div ending
            + "<div class='col-lg-12 float-left padding-none bottom Modalpopup-Scroll1'>";
        var CurrentRoleElements = "";
        $.each(empRoleList, function (i, itemRole) {
            if (itemModule.ModuleID == itemRole.ModuleID) {
                var roleDeptMainID = 'ulModule' + itemModule.ModuleID + 'ulRoleDepts' + itemRole.RoleID;
                var roleHeaderMainID = 'moduleHeader' + itemModule.ModuleID + 'roleHeader' + itemRole.RoleID;
                CurrentRoleElements = CurrentRoleElements +
                    "<div class='col-lg-12 Modalpopup-Scroll'>"
                    + "<div class='col-lg-12 float-left  bottom padding-none' style='border: 1px solid #07889a;' "
                    + "id=" + roleHeaderMainID + " > <div class='col-lg-12 role_inner_grid_header'>"
                    + "<span class='çol-lg-10 float-left'><span class=' innerroleHeader'>"
                    + " Role : </span>"
                    + "<span id='roleValue' class='value_output' style='color: #126571;font-size:11pt;'>" + itemRole.RoleName + "</span>"
                    + "</span> <div class='' col-lg-2 float-right'>"
                    + "<div class=' float-right " + displayClass + "' title='Delete' "
                    + "onclick='RoleDelete(" + itemRole.RoleID + "," + itemRole.ModuleID + ");' title='Remove Role'>"
                    + "<span style='color:red'><i class='fa fa-close'></i></span></div></div></div>" //this line ends divs
                    //department div start
                    + "<div class='col-lg-12 float-left' style='margin-bottom:5px;'>"
                    + "<div class='col-12'>"
                    + " <ul class='roles_list_nav col-lg-12' id='" + roleDeptMainID + "'>";
                var CurrentRoleDeptElements = "";
                $.each(empDepartmentList, function (i, results) {
                    var deptElementsID = 'liModule' + itemRole.ModuleID + 'liRole' + itemRole.RoleID + 'liDept' + results.DeptID;
                    if (results.RoleId == itemRole.RoleID && results.ModuleID == itemModule.ModuleID) {

                        if (results.DepartmentName == null) {
                            CurrentRoleDeptElements = CurrentRoleDeptElements
                                + "<div class='text-center accesibleDepartment_content'>Accessible Department Not Applicable</div>";
                        }
                        else {
                            CurrentRoleDeptElements = CurrentRoleDeptElements
                                + "<li class='top' value=" + results.DeptID + " id=" + deptElementsID + ">"
                                + "<span class='btn-signup_popup' >"
                                + results.DepartmentName + "  " + "<span class='" + displayClass + "' "
                                + "style='color:Red;padding-left: 6px;' title='Delete' "
                                + "onclick='DeleteRoleDepartments( " + results.DeptID + "," + itemRole.RoleID + "," + itemRole.ModuleID + "); '>"
                                + "   <span style='color:red;border-radius: 5px; padding: 2px 4px;'><i class='fa fa-close'></i></span></span></span>";
                        }
                    }
                });
                CurrentRoleDeptElements = CurrentRoleDeptElements + "</div></ul>";//ul above div ends
                CurrentRoleElements = CurrentRoleElements + CurrentRoleDeptElements + "</div></div></div>"; 
            }
        });
        + "</div></div>";
        CurrentModuleElements = CurrentModuleElements + CurrentRoleElements;
        if (empModuleList.length==1) {
            $(divEmployeeAssignRole).empty();
        }
        $(divEmployeeAssignRole).append(CurrentModuleElements);
    });
}
//Accessible Department DropdownList Refresh
function AccessibleDepartmentRefresh(roleDeptList) {
    $("#lstAccessDepartment").selectpicker('refresh');
    $("#lstAccessDepartment").selectpicker('val', roleDeptList);
}

