﻿$(document).ready(function () {
    //Event Chart Filters
    $(".filter_one").on("click", function () {
        $("#filter_dropdownEvent").toggle();
    });
    $(".filter_InProgressAudit").on("click", function () {
        $("#divInprogressAuditpopup").toggle();
    });
    $(".filter_CompletedAudit").on("click", function () {
        $("#divCompletedAuditpopup").toggle();
    });

    //Cancle for Event chart
    $("#dashboard_btnEvent").on("click", function () {
        $("#filter_dropdownEvent").hide();
    });
    //Capa Chart Filters
    $(".filter_twoCapa").on("click", function () {
        $("#filter_dropdownCapa").toggle();
    });
    //Cancel for Capa chart
    $("#dashboard_btnCapa").on("click", function () {
        $("#filter_dropdownCapa").hide();
    });

});
//var ReviewCount =Model.ReviewTotalCount;
//if (ReviewCount == 0) {
//    $("#divReviewNoDetails").show();
//    $("#divReviewViewDetails").hide();
//}
//else {

//    $("#divReviewNoDetails").hide();
//}