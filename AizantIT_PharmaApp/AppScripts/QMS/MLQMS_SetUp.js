﻿var List;
function GetDescriptionForML(IncidentID, IncNumber) {
    $.ajax({
        type: "GET",
        url: AGA_ML_URL + "/incident?id=" + IncidentID,
        data: "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var IRNumber = response.IR_Number;
            var IRDescription = response.Similar_Incidents;
            $("#idDescCount").html(IRNumber.length - 1);//Count
            $('#tblIncidentDesc_List tbody').empty();
            if (IRNumber.length - 1 != 0) {
                $("#divMLDescription").show();
                for (var i = 1; i < IRNumber.length; i++) {
                    if (IncNumber != IRNumber[i]) {
                        $('#tblIncidentDesc_List tbody').append("<tr><td class='text-left'>" + IRNumber[i] + "</td>" +
                            "<td class='text-left'>" + IRDescription[i] + "</td></tr>");
                    }
                }
            } else {
                $("#divNoMLDescription").show();
                var value = "No Records";
                $('#tblIncidentDesc_List tbody').append("<tr><td class='tableBorder'></td><td class='text-left tableBorder'>" + value + "</td></tr>");
            }
        },
        failure: function (response) {
            custAlertMsg("Failure : unable to fetch the Related Observations", "error");
        },
        error: function (response) {
            custAlertMsg(response.statusText, "error");
        }
    });
}
