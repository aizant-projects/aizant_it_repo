﻿//JS Function for Prepopulate SecurityQuesAnsw using onclick function
function PrepopulateSecurityQuesAnsw(DefinedUrl) {
    $.ajax({
        type: "POST",
        url: DefinedUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            var SecurityObj = response.d;
            var result2 = $.parseJSON(SecurityObj);
            var securityQuestion = result2['securityQuestion'];
            var SecurityAnswer = result2['SecurityAnswer'];
            $('#txtSecQues').val(securityQuestion);
            $('#txtSecAns').val(SecurityAnswer);

            var txtSecQuesReadOnly = document.getElementById("txtSecQues");
            txtSecQuesReadOnly.setAttribute("readOnly", "true");

            var txtSecAnsReadOnly = document.getElementById("txtSecAns");
            txtSecAnsReadOnly.setAttribute("readOnly", "true");

            document.getElementById("btnSecQuesEdit").style.display = "inline-block";
            document.getElementById("btnSecQuesUpdate").style.display = "none";
            document.getElementById("btnSecQuesCancel").style.display = "none";

        },
        failure: function (response) {
            // alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    })
}


//JS Function to Disable Edit button and appear Update and cancel btn and txt boxes in read and write mode by clicking Edit btn
function EditSecurityQueAns() {
    document.getElementById("btnSecQuesUpdate").style.display = "inline-block";
    document.getElementById("btnSecQuesCancel").style.display = "inline-block";
    document.getElementById("btnSecQuesEdit").style.display = "none";

    $("#txtSecQues").prop("readonly", false);
    $("#txtSecAns").prop("readonly", false);
}

function CancelSecurityQueAns() {
    //PrepopulateSecurityQuesAnsw();
    PrepopulateSecurityQuesAnswURL();
    document.getElementById("btnSecQuesEdit").style.display = "inline-block";
    document.getElementById("btnSecQuesUpdate").style.display = "none";
    document.getElementById("btnSecQuesCancel").style.display = "none";
}


//JS Function Ajax for ValidateSecurityQuesAnsw
function ValidateSecurityQandAns() {
    var SecurityQuestion = document.getElementById('txtSecQues').value;
    var SecurityAnswer = document.getElementById('txtSecAns').value;
    re = /[`~!@#$%^&*()_|+\-=;:'",.<>\{\}\[\]\\\/]/gi;
    errors = [];
    if (SecurityQuestion == '') {
        errors.push("Enter Security Question.");
    }
    if (SecurityQuestion != '') {
        if (SecurityQuestion.length <= 3) {
            errors.push("Security Question Should Be Atleast 4 Characters.");
        }
    }
    if (SecurityAnswer == '') {
        errors.push("Enter Security Answer.");
    }
    if (SecurityAnswer != '') {
        if (SecurityAnswer.length <= 1) {
            errors.push("SecurityAnswer Should Be Atleast 2 Characters.");
        }
    }
    if (errors.length > 0) {
        custAlertMsg(errors.join("<br/>"), "error");
    }
    // Client side validation pass
    else {
        UpdateSecurityQueAnsURL();
    }
}

//JS Function to Update SecurityQues and SecurityAns by clicking on Update btn
function UpdateSecurityQueAns(DefinedUrl) {

    var SecurityQuestion = document.getElementById('txtSecQues').value;
    var SecurityAnswer = document.getElementById('txtSecAns').value;
    var objStr = { "SecurityQuestion": SecurityQuestion, "SecurityAnswer": SecurityAnswer };
    var jsonData = JSON.stringify(objStr);

    $.ajax({
        type: "POST",
        url: DefinedUrl,
        //url: "/UserManagement/WebServicesList.asmx/UpdateSecurityQuesAnsw",
        data: jsonData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var strResult = $.parseJSON(response.d);

            errors = [];
            if (strResult == "1") {
                errors.push("Updated Successfully.");
            }

            if (errors.length > 0) {
                if (strResult == "1") {
                    //document.getElementById('msgSuccess').innerHTML = errors.join("<br/>");
                    custAlertMsg(errors.join("<br/>"), "success");
                    $('#ModalSuccess').modal();
                    $("#btnSuccessOk").focus();
                    $('#ModalSuccess').click(function () {
                        $("#btnSecQuesUpdate").hide();
                        $("#btnSecQuesCancel").hide();
                        $("#btnSecQuesEdit").show();
                    });

                    $("#txtSecQues").prop("readonly", true);
                    $("#txtSecAns").prop("readonly", true);
                }
            }
        },
        failure: function () {
            //custAlertMsg("Invalid Password.", "error");
        }
    });
}


function SecurityPasswordClear() {
    document.getElementById('txtCurrentPwd').value = "";
    document.getElementById('txtNewPassword').value = "";
    document.getElementById('txtConfirmPassword').value = "";
}
function ChangeEmpPassword()
{
    var txtCurrentPwd = document.getElementById('txtCurrentPwd').value;
    var txtNewPwd = document.getElementById('txtNewPassword').value;
    var txtConfirmPwd = document.getElementById('txtConfirmPassword').value;
    var undefChar = /[\ _]/;
    var undefChars = /[\ _]/;
    errors = [];
    if (txtCurrentPwd == '')
    {
        errors.push("Enter Old Password.");
    }
    if (txtCurrentPwd != "")
    {
        if (txtCurrentPwd.length < 8)
        {
            errors.push("Password must contain Minimum 8 characters.");
        }
    }
    if (txtNewPwd != "") {

        if (txtNewPwd.length < 8) {
            errors.push("Password must contain Minimum 8 characters.");
        }
        if (txtNewPwd.length > 20)
        {
            errors.push("Password must contain Maximum 20 characters.");
        }
        if (txtNewPwd.search(/[a-z]/) < 0) {
            errors.push("Your Password must contain atleast one Lower Case.");
        }
        if (txtNewPwd.search(/[A-Z]/) < 0) {
            errors.push("Your Password must contain atleast one Upper Case.");
        }
        if (txtNewPwd.search(/[0-9]/) < 0) {
            errors.push("Your Password must contain atleast one Number.");
        }
        if (txtNewPwd.search(/[@#&*]/) < 0) {
            errors.push("Password must contain atleast one @,#,&,* character.");
        }
        if (txtCurrentPwd == txtNewPwd) {
            errors.push("New Password should not be same as old password.")
        }
    }
    if (txtNewPwd == '')
    {
        errors.push("Enter New Password.");
    }
    if (txtConfirmPwd != "")
    {
        if (txtNewPwd != txtConfirmPwd)
        {
            errors.push("New Password and Confirm Password should match.");
        }
    }
    if (txtConfirmPwd == '') {
        errors.push("Enter Confirm Password.");
    }
    // Client side validation fail
    if (errors.length > 0)
    {
        custAlertMsg(errors.join("<br/>"), "error");
       // document.getElementById('msgError').innerHTML = errors.join("<br/>");
        $('#ModalError').modal();
        $("#btnErrorOk").focus();
    }
    // Client side validation pass
    else
    {
        SaveEmpPasswordURL();
    }
}


function SaveEmpPassword(DefinedUrl) {
    var CurrentPassword = document.getElementById('txtCurrentPwd').value;
    var NewPassword = document.getElementById('txtNewPassword').value;

    var objStr = { "CurrentPassword": CurrentPassword, "NewPassword": NewPassword };
    var jsonData = JSON.stringify(objStr);

    $.ajax({
        type: "POST",
        url: DefinedUrl,
        data: jsonData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var strResult = $.parseJSON(response.d);
            errors = [];
            if (strResult == "1") {
                errors.push("New Password Should not be Previous 3 Passwords.");
            }
            else if (strResult == "2")
            {
                errors.push("Password Changed Successfully.");
            }
            else if (strResult == "3") {
                errors.push("Password Updation Failed.");
            }
            else if (strResult == "4") {
                errors.push("Invalid Password.");
            }
            else if (strResult == "5") {
                errors.push("Please submit Employee Details.");
            }
            if (errors.length > 0)
            {

                if (strResult == "2")
                {
                       //document.getElementById('msgSuccess').innerHTML = errors.join("<br/>");
                    custAlertMsg(errors.join("<br/>"), "success","ProfielHide();");
                    $('#ModalSuccess').modal();
                    $("#btnSuccessOk").focus();

                    $('#ModalSuccess').click(function ()
                    {
                        document.getElementById('txtCurrentPwd').value = "";
                        document.getElementById('txtNewPassword').value = "";
                        document.getElementById('txtConfirmPassword').value = "";
                    });
                }
                else {
                    //document.getElementById('msgError').innerHTML = errors.join("<br/>");
                    custAlertMsg(errors.join("<br/>"), "error");
                    $('#ModalError').modal();
                    $("#btnErrorOk").focus();
                }
            }
        },
        failure: function () {
            //custAlertMsg("Invalid Password.", "error");
        }
    });
}
function ProfielHide() {
    $('#AjaxCallSecurityProf').modal('hide');
}

//Validation to not to allow special characters in SecurityQuestion TextBox except ?
$(function () {
    $('#txtSecQues').keyup(function () {
        var yourInput = $(this).val();
        re = /[`~!@#$%^&*()_|+\-=;:'",.<>\{\}\[\]\\\/]/gi;
        var isSplChar = re.test(yourInput);
        if (isSplChar) {
            var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=;:'",.<>\{\}\[\]\\\/]/gi, '');
            $(this).val(no_spl_char);
        }
    });
});

