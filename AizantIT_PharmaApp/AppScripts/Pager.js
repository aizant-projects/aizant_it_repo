﻿function InitPagination(totalMaxPageIndex) {
    $("#divPager").empty();
    if (totalMaxPageIndex > 1) {
        var pageStr = "";
        if (totalMaxPageIndex > 5) {
            pageStr = "<ul class='pagination float-right'><li class='disabled'><a href='#'>&laquo;</a></li><li><a href='#' onclick='getPageSelectValue(this);'>1</a></li>";
        }
        else {
            pageStr = "<ul class='pagination float-right'></li><li><a href='#' onclick='getPageSelectValue(this);'>1</a></li>";
        }
        for (var i = 2; i <= totalMaxPageIndex; i++) {
            pageStr = pageStr + "<li><a href='#' onclick='getPageSelectValue(this);'>" + i + "</a></li>";
            if (i == 5) {
                break;
            }
        }
        if (totalMaxPageIndex > 5) {
            pageStr = pageStr + "<li><a href='#' onclick='OnGetNextPageIndexes(5," + totalMaxPageIndex + ");'>...</a></li><li onclick='gotoLastPageView(" + totalMaxPageIndex + ");'><a href='#'>&raquo;</a></li></ul>";
        }
        else {
            pageStr = pageStr + "</ul>";
        }
        $("#divPager").append(pageStr);
    }
}

function OnGetNextPageIndexes(PreviousMaxIndex, totalMaxPageIndex) {
    $("#divPager").empty();
    var pageStr = "<ul class='pagination float-right'><li onclick='gotoFirstPageView(" + totalMaxPageIndex + ");'><a href='#'>&laquo;</a></li></li><li onclick='OnGetPreviousPageIndexes(" + (PreviousMaxIndex + 1) + "," + totalMaxPageIndex + ");'><a href='#'>...</a></li>";
    var showPageIndex = 0;
    var limitCount = 5;
    var IndexRemains = "N";
    var CurrentMaxIndex = 0;
    for (var i = PreviousMaxIndex + 1; i <= totalMaxPageIndex; i++) {
        if (limitCount == 0) {
            IndexRemains = "Y";
            CurrentMaxIndex = i - 1;
            break;
        }
        else {
            pageStr = pageStr + "<li><a href='#' onclick='getPageSelectValue(this);'>" + i + "</a></li>";
        }
        limitCount--;
    }
    if (IndexRemains == "Y") {
        pageStr = pageStr + "<li onclick='OnGetNextPageIndexes(" + CurrentMaxIndex + "," + totalMaxPageIndex + ");'><a href='#'>...</a></li><li onclick='gotoLastPageView(" + totalMaxPageIndex + ");'><a href='#'>&raquo;</a></li></ul>";
    }
    else {
        pageStr = pageStr + "<li class='disabled'><a href='#'>&raquo;</a></li></ul>";
    }
    $("#divPager").append(pageStr);
}

function OnGetPreviousPageIndexes(CurrentMinIndex, totalMaxPageIndex) {
    $("#divPager").empty();
    var pageStr = "<ul class='pagination float-right'>";
    var IndexRemains = "N";
    var minPageIndex = 0;
    var limitCount = 5;
    var pageNums = "";
    for (var i = CurrentMinIndex - 1; i >= 1; i--) {
        pageNums = "<li><a href='#' onclick='getPageSelectValue(this);'>" + i + "</a></li>" + pageNums;
        if (limitCount == 1) {
            IndexRemains = "Y";
            minPageIndex = i;
            break;
        }
        limitCount--;
    }
    if (CurrentMinIndex != 6) {
        pageStr = pageStr + "<li onclick='gotoFirstPageView(" + totalMaxPageIndex + ");'><a href='#'>&laquo;</a></li><li onclick='OnGetPreviousPageIndexes(" + minPageIndex + "," + totalMaxPageIndex + ");'><a href='#'>...</a></li>" + pageNums
            + "<li onclick='OnGetNextPageIndexes(" + (CurrentMinIndex - 1) + "," + totalMaxPageIndex + ");'><a href='#'>...</a></li>";;
    }
    else {
        pageStr = pageStr + "<li class='disabled'><a href='#'>&laquo;</a></li>" + pageNums + "<li onclick='OnGetNextPageIndexes(" + (CurrentMinIndex - 1) + "," + totalMaxPageIndex + ");'><a href='#'>...</a></li>";
    }
    pageStr = pageStr + "<li onclick='gotoLastPageView(" + totalMaxPageIndex + ");'><a href='#'>&raquo;</a></li></ul>";
    $("#divPager").append(pageStr);
}

function gotoFirstPageView(totalMaxPageIndex) {
    InitPagination(totalMaxPageIndex);
}

function gotoLastPageView(totalMaxPageIndex) {
    var getMinIndex = 5;
    while (totalMaxPageIndex > getMinIndex) {
        getMinIndex = getMinIndex + 5
    }
    getMinIndex = getMinIndex - 5;

    OnGetNextPageIndexes(getMinIndex, totalMaxPageIndex);
}

function highlightCurrentPage(currentPage) {
    $("#divPager").find("a").each(function () {
        var aText = $(this).text();
        if (aText == currentPage) {
            $(this).css({ 'backgroundColor': '#0769ad', 'color': '#fff' });
        }
        else {
            $(this).css({ 'backgroundColor': '#fff', 'color': '#337ab7' });
        }
    });
}
