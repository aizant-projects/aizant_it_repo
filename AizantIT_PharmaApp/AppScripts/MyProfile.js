﻿
//JS Function for Prepopulate MyProfile Label using onclick function
function PrepopulateMyProfileLabel(DefinedUrl) {
    //alert(DefinedUrl)
    $.ajax({
        type: "GET",
        url: DefinedUrl,
        //url: '/UserManagement/WebServicesList.asmx/MyProfileGetDataToMyProfilePrepoulateLabel',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var MyProfileObj = response.d;
            var result = $.parseJSON(MyProfileObj);
            var EmployeeCode = result['EmployeeCode'];
            var FirstName = result['FirstName'];
            var LastName = result['LastName'];
            var Dob = result['Dob'];
            var Gender = result['Gender'];
            var Department = result['Department'];
            var Designation = result['Designation'];
            var BloodGroup = result['BloodGroup'];
            var StartDate = result['StartDate'];
            var Email = result['Email'];
            var Address = result['Address'];
            var Mobile = result['Mobile'];
            var Country = result['Country'];
            var State = result['State'];
            var Image1 = result['Image'];

            $('#lblEmployeeCode').text(EmployeeCode);
            //$('#lblEmployeeCode').val(EmployeeCode);
            $('#lblFirstName').text(FirstName);
            $('#lblLastName').text(LastName);
            $('#lblDob').text(Dob);
            $('#lblGender').text(Gender);
            $('#lblDepartment').text(Department);
            $('#lblDesignationn').text(Designation);
            $('#lblBloodGroup').text(BloodGroup);
            $('#lblStartDate').text(StartDate);
            $('#lblEmaile').text(Email);
            $('#lblAddress').text(Address);
            $('#lblMobile').text(Mobile);
            $('#lblCountry').text(Country);
            $('#lblState').text(State);
            $('#MyImage1').attr('src', Image1);
            //alert($('#MyImage1').attr('src'));
        },
        failure: function (response) {
            // alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    })
}

