﻿//Left Side Navi Active by Page
$(function () {
    var path = window.location.pathname + window.location.search;
    var href_value = "";
    $(".nav_list ul li").each(function () {
        href_value = $(this).children("a").attr("href");
        if (path == href_value) {
         
            $(this).children("a").addClass("active");
            $(this).parents(".collapse").addClass("show").prev().removeClass("collapsed");
        }
    });
    $(".card-header").click(function () {
     
        $(this).nextAll(".card-header").addClass("collapsed");
        $(this).prevAll(".card-header").addClass("collapsed");
    });
    $(".panel_ums").click(function () {
        $(this).prevAll().children(".collapse").removeClass("show in");
        $(this).nextAll().children(".collapse").removeClass("show in");
    });
    $(".panel-heading").click(function () {
        $(this).next(".collapse").removeClass("show");
    });
    $(".panel-title").each(function () {
        var href_value = $(this).children("a").attr("href");
        if (path == href_value) {
            $(this).parents(".panelList").addClass("active");
        }
    });
    $(".card-title").each(function () {  
        var href_value = $(this).attr("href");
        if (path == href_value) {
            
            $(this).parents(".headerList").addClass("active");
        }
    });
    $(".headerList a").each(function () {
        var href_value = $(this).attr("href");
        if (path == href_value) {
            $(this).parents(".headerList").addClass("active").parents(".collapse").addClass("show").prev().removeClass("collapsed");
        }
    });
    $('.productmenu_list ul li').each(function () {
        href_value = $(this).children("a").attr("href") + window.location.search;
        if (path == href_value) {
            $(this).children('a').addClass("active");
        }
    });
    $('.reg_menu_list  ul li').each(function () {
        href_value = $(this).children("a").attr("href") + window.location.search;
        if (path == href_value) {
            
            $(this).children('a').addClass("active");
        }
    });
});
