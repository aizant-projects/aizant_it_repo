﻿//for Notification UI Script Start
$('#notificationsBody').click(function () {
    return false;
});

//maintaing a flag for Top5Notification with out any action  on notifications then re call  'loadNotifications()' function  
var onLoadTop5Notification = false;
$('#notificationFooterShowAll').click(function () {
    $("#div_notification_link,#notificationContainer").hide();
    $('#ModalNotification').modal('show');
    onLoadTop5Notification = false;
});

//var selector = '.landing_links ul li a';
//$(selector).on('click', function () {
//    $(selector).removeClass('active');
//    $(this).addClass('active');
//});

//Notification On Click
$("#notificationLink").click(function () {
   $(".menu_list").hide();
   $('#notificationFooterShowAll').hide();
   $("#div_notification_link,#notificationContainer").fadeToggle(300);
    loadNotifications();
    return false;
});

//User Profile Related
$(".user_details_info").click(function () {
    $(".menu_list").toggle();
    $("#notificationContainer,#div_notification_link").hide();
    //$(".test_time").hide();
});

var NotificationContainerClick = false;

$("#notificationContainer").click(function () {
    NotificationContainerClick = true;
});

$(document).click(function () {
    $(".menu_list").hide();
    //$(".test_time").hide();
    if (NotificationContainerClick == false) {
        $("#div_notification_link,#notificationContainer").hide();
        onLoadTop5Notification = false;
    }
    NotificationContainerClick = false;
});

var startTimer;
// initial notification
function Init_Notification() {
    LoadNotificationCount();
    StartNotifyTimer();
}

//Load NotificationCount 
function LoadNotificationCount() {
    $.ajax({
        type: 'Get',
        url: AizantIT_WebAPI_URL + "/api/Notification/GetNotificationCount?EmpID=" + _NotifyEmpID,
        dataType: 'JSON',
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            if (response != 0) {
                $('#notification_count').html(response);
            } else {
                $('#notification_count').hide();
            }
        },
        error: function (jqXHR) {
        }
    });
}

//Timer 
function StartNotifyTimer() {
    var sec = 0;
    var seconds = 0;
    startTimer = setInterval(function () {
        seconds = sec + 1;
        if (seconds >= 10) {
            seconds = 0;
            LoadNotificationCount();
            if (onLoadTop5Notification == true) {
                loadNotifications();
            }
        }
        sec = seconds;
    },3000);
}

//Notification Loading  Function 
function loadNotifications() {
    clearInterval(startTimer);
    $("#notificationsBody").empty();
    $.ajax({
        type: "GET",
        url: AizantIT_WebAPI_URL + "/api/Notification/GetNotificationData?EmpID=" + _NotifyEmpID,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            OnNotifySucceed(response);
            StartNotifyTimer();
        },
        failure: function (response) {
            console.log(response);
        },
        error: function (response) {
            console.log(response);
        }
    });
}

//On Notification Succeed Loading Data in Div
function OnNotifySucceed(response) {
    onLoadTop5Notification = true;
    $("#notificationsBody").html(response.UserTop5Notifications);
    if (response.UserTotalNotifications>5) {
        $('#notificationFooterShowAll').show(); 
        $('#divNotificationList').show();
        $('#divNotificationList').empty();
        $('#divNotificationList').load("/Common/Notification/NotificationList.html");
    }
    else {
        $('#notificationFooterShowAll').hide();
    }
}

function ClearNotify(NotifyID, EmpID, cntrl) {
    $.ajax({
        type: "POST",
        url: AizantIT_WebAPI_URL + "/api/Notification/SubmitNotifications?Notification_ID=" + NotifyID + "&Emp_ID=" + EmpID,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function () {
            LoadNotificationCount();
            loadNotifications();
        },
        failure: function (response) {
            console.log(response);
        },
        error: function (response) {
            console.log(response);
        }
    });
}

function RedirectTo(cntrl) {
    window.open(cntrl.href, '_self');
}
