﻿var docWarnMsg = "Please Close The Dev Tools Window.";
var callBack_OnPDF_Success;
function PdfViewerContentLoading(FilePathAndName, loadOn = "#divPDF_Viewer", PdfCallBackFunction = '') {
    var FilePathAndNameWithIFrame = "<iframe src=\"" + window.location.origin + "/Scripts/pdf.js/web/viewer.html?file=" + window.location.origin + "/" + FilePathAndName + "#toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" frameborder=\"0\" height=\"700px\" ></iframe>";
    callBack_OnPDF_Success = PdfCallBackFunction;
    $(loadOn).html("Loading...");
    //For Checking the Console is in Open or not
    var element = new Image;
    var devtoolsOpen = false;
    element.__defineGetter__("id", function () {
        devtoolsOpen = true; // This only executes when devtools is open.       
    });
    var SetTimerforConsole;
    SetTimerforConsole = setInterval(function () {
        devtoolsOpen = false;
        console.log(element);
        if (devtoolsOpen) {
            $(loadOn).html(docWarnMsg);
            //clearInterval(SetTimerforConsole);
            alert(docWarnMsg);
        }
        else {
            clearInterval(SetTimerforConsole);
            //For PDF Viewer Loading
            $(loadOn).html("Loading...");
            $(loadOn).html(FilePathAndNameWithIFrame);
            if (PdfCallBackFunction!='') {
                onPDF_LoadingSuccess();
            }
        }
    }, 1000);
}
function onPDF_LoadingSuccess() {
    this[callBack_OnPDF_Success]();
}
$(document).on('shown.bs.modal', '#IdleTO_Logout_popup', function (event) {
   //// $("iframe").contents().find("#viewerContainer").removeClass("pdfPresentationMode");
   // if ($("iframe").contents().find("#viewerContainer").hasClass("pdfPresentationMode")) {
   //     //alert("Session is about to expire within 2 minutes,click ok/Esc to exit full screen.");
   //     document.webkitExitFullscreen();//Chrome, Safari and Opera
   //     //document.msExitFullscreen();//IE, Edge
   //     //document.mozCancelFullScreen();//Firefox, mozilla
   // }
 if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
    }
});