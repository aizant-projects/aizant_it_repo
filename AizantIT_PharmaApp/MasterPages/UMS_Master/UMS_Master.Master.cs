﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AizantIT_PharmaApp.MasterPages.UMS_Master
{
    public partial class UMS_Master : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.DataBind();
            if (Cache.Get("CacheEmpInfo") != null && Request.Cookies.Get("CookieEmpID")!=null)
            {

                int namelength = ((Session["UserSignIn"] as DataSet).Tables[0].Rows[0]["Name"].ToString()).Length;
                if (namelength > 17)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "length", "UsernameLength('" + (Session["UserSignIn"] as DataSet).Tables[0].Rows[0]["Name"].ToString() + "');", true);
                    lnkUser.Attributes.Add("title", "" + (Session["UserSignIn"] as DataSet).Tables[0].Rows[0]["Name"].ToString() + "");
                }
                else
                {
                    lnkUser.Text = (Session["UserSignIn"] as DataSet).Tables[0].Rows[0]["Name"].ToString();
                    lnkUser.Attributes.Add("title", "User Name");
                }
            }
        }
        protected void lnkLogOut_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("~/UserLogin.aspx");
        }
    }
}