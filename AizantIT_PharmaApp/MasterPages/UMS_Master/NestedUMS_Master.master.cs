﻿using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.MasterPages.UMS_Master
{
    public partial class NestedUMS_Master : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    //Page.Header.DataBind();
                    DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                    DataTable dtTemp = new DataTable();
                    DataRow[] drModule = dt.Select("ModuleID=" + (int)Modules.AdminSetup);
                    if (drModule.Length > 0)
                    {
                        dtTemp = drModule.CopyToDataTable();
                        if (dtTemp.Rows.Count > 0)
                        {
                            if (dtTemp.Select("RoleID=" + (int)AdminSetup_UserRole.UserAdmin_UMS).Length > 0)
                            {
                                divUserAdminPages.Visible = true;
                            }
                            if ((dtTemp.Select("RoleID=" + (int)AdminSetup_UserRole.GlobalAdmin_UMS).Length > 0) 
                                || (dtTemp.Select("RoleID=" + (int)AdminSetup_UserRole.ProductAdmin).Length > 0))
                            {
                                divGlobalAdminPages.Visible = true;
                                if (dtTemp.Select("RoleID=" + (int)AdminSetup_UserRole.GlobalAdmin_UMS).Length > 0)
                                {
                                    liGlobalSetup.Visible = true;
                                    liSite.Visible = true;
                                    liMarket.Visible = true;
                                    liClient.Visible = true;
                                    liDosage.Visible = true;
                                    liUnitofMeasurement.Visible = true;
                                    liProductId.Visible = true;
                                }
                                //if (dtTemp.Select("RoleID=" + (int)AdminSetup_UserRole.ProductAdmin).Length > 0)
                                //{
                                //     liProductId.Visible = true;
                                //}
                            }                                                        
                        }
                    }
                }
            }
        }       
    }
}