﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VMS_BO;
using VMS_BAL;
using System.Data;
using System.Text;
using System.Configuration;
using UMS_BusinessLayer;
using AizantIT_PharmaApp.Common;

namespace AizantIT_PharmaApp.MasterPages.VMS_Master
{
    public partial class NestedVMS_Master : System.Web.UI.MasterPage
    {
        VMSRolePrivilegesBAL objVMSRolePrivilegesBAL = new VMSRolePrivilegesBAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            bindmenu();
        }
        void bindmenu()
        {
            if (HelpClass.IsUserAuthenticated())
            {              
                
              
                    RolePrivilegesBO objMenuRolePrivilegesBO = new RolePrivilegesBO();
                    objMenuRolePrivilegesBO.RoleID = Convert.ToInt32(Session["sesGMSRoleID"].ToString());                    
                    DataTable dtMenu = objVMSRolePrivilegesBAL.GetMenuItems(objMenuRolePrivilegesBO);
                    foreach (DataRow dr in dtMenu.Rows)
                    {
                        if (dr["PageID"].ToString().Trim() == "1")
                        {
                            VisitorRegister.Visible = true;
                        }
                        if (dr["PageID"].ToString().Trim() == "2")
                        {
                            VehicleRegster.Visible = true;
                        }
                        if (dr["PageID"].ToString().Trim() == "3")
                        {
                            MaterialRegister.Visible = true;
                        }
                        if (dr["PageID"].ToString().Trim() == "4")
                        {
                            VmsSettings.Visible = true;
                        }
                        if (dr["PageID"].ToString().Trim() == "5")
                        {
                            FacilitiesRequest.Visible = true;
                        }
                        if (dr["PageID"].ToString().Trim() == "6")
                        {
                            VisitorRegister.Visible = true;
                        }
                        if (dr["PageID"].ToString().Trim() == "7")
                        {
                            Div_vmsReport.Visible = true;
                        }
                    }
                    if (Session["sesGMSRoleID"].ToString() == "8")
                    {
                        VmsSettings.Visible = true;
                        Div_vmsReport.Visible = true;
                    }
               
                
                
            }
        }
       
    }
}

