﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VMS_BO;
using VMS_BAL;
using System.Data;
using System.Text;
using System.Configuration;
using UMS_BusinessLayer;
using AizantIT_PharmaApp.Common;

namespace AizantIT_PharmaApp.MasterPages.VMS_Master
{
    public partial class VMS_Master : System.Web.UI.MasterPage
    {
        VMSRolePrivilegesBAL objVMSRolePrivilegesBAL = new VMSRolePrivilegesBAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
           //bindmenu();
        }
        void bindmenu()
        {
            if (HelpClass.IsUserAuthenticated())
            {
                RolePrivilegesBO objRolePrivilegesBO = new RolePrivilegesBO();
                objRolePrivilegesBO.RoleID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                DataTable dtRole = objVMSRolePrivilegesBAL.GetEmpRoleID(objRolePrivilegesBO);
                RolePrivilegesBO objMenuRolePrivilegesBO = new RolePrivilegesBO();
                objMenuRolePrivilegesBO.RoleID = Convert.ToInt32(dtRole.Rows[0]["RoleID"].ToString());
                if (Convert.ToInt32(dtRole.Rows[0]["RoleID"].ToString()) == 22)//To Hide MyProfile To Security.
                {
                    btnMyprofile.Visible = false;
                }
                Session["sesGMSRoleID"]= dtRole.Rows[0]["RoleID"].ToString();
                DataTable dtMenu = objVMSRolePrivilegesBAL.GetMenuItems(objMenuRolePrivilegesBO);
                StringBuilder sbMenu = new StringBuilder();
                bool isHeaderMenu = false;
                string stripadd = "http://" + Convert.ToString(ConfigurationManager.AppSettings["hostip"]);
                foreach (DataRow dr in dtMenu.Rows)
                {
                    if (dr["PageURL"].ToString().Trim() == "#")
                    {
                        //fa fa-id-card-o Visitor Register  MenuIco
                        //fa fa-truck Material Register
                        //fa fa-handshake-o Service Register
                        //fa fa-bus Vechile Register
                        //fa fa-hand-o-right Supply Register
                        // fa fa-user-circle Employee Register
                        // fa fa-user-circle Accessible Roles
                        if (isHeaderMenu == true)
                        {
                            sbMenu.Append("</ul></ li > ");                            
                            sbMenu.Append("<li><a href = '#'  ><i class='"+ dr["MenuIco"].ToString() + "'></i>");
                            sbMenu.Append(dr["PageTitle"].ToString());
                            sbMenu.Append("<i class='fa fa-caret-down'></i></a><ul>");
                        }
                        if (isHeaderMenu == false)
                        {
                            isHeaderMenu = true;
                            //< li >< a href = "#" id = "NavLnkVisitorRegistration" >< i class="fa fa-id-card-o"></i>Visitor Register<i class="fa fa-caret-down"></i></a>
                            //<ul>
                            sbMenu.Append("<li><a href = '#'  ><i class='" + dr["MenuIco"].ToString() + "'></i>");
                            sbMenu.Append(dr["PageTitle"].ToString());
                            sbMenu.Append("<i class='fa fa-caret-down'></i></a><ul>");
                        }
                    }
                    else
                    {
                        //<li><a href="<%=ResolveUrl("~/VMS/VisitorRegister/VisitorRegistration.aspx")%>" id="VisitorRegistration">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Check In</a></li>
                        sbMenu.Append("<li><a href=" + stripadd);
                        sbMenu.Append(dr["PageURL"].ToString());
                        sbMenu.Append(">");
                        sbMenu.Append(dr["PageTitle"].ToString());
                        sbMenu.Append("</a></li>");
                    }
                }
                sbMenu.Append("</ul></ li > ");
                uladmMenu.InnerHtml = sbMenu.ToString();
            }
        }
        protected void lnkLogOut_Click(object sender, EventArgs e)
        {
            Session["UserDetails"] = null;
            Session.Abandon();
            Response.Redirect("~/UserLogin.aspx");
        }
    }
}