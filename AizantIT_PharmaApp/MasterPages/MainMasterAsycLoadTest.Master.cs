﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.Configuration;
using TMS_BusinessLayer;
using System.Web.Services;
using AizantIT_PharmaApp.Common;
using UMS_BusinessLayer;
using System.Data.SqlClient;

namespace AizantIT_PharmaApp.MasterPages
{
    public partial class MainMasterAsycLoadTest : System.Web.UI.MasterPage
    {
        public int resulttime { get; set; }
        TMS_BAL objBAL_TMS;
        static int EmpID = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Cache.Get("CacheEmpInfo") != null && Request.Cookies.Get("CookieEmpID")!=null)
            {
                EmpID = Convert.ToInt32((Session["UserSignIn"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                //Modified by pradeep
                Session["EmpID"] = EmpID;
                ViewState["RoleID"] = Convert.ToInt32((Session["UserSignIn"] as DataSet).Tables[1].Rows[0]["RoleID"]);
                hdnEmpID.Value = EmpID.ToString();
                if (ViewState["RoleID"].ToString() == "22")//To hide My Profile and Notification Icon to Security User 22-security RoleID
                {
                    //Commented by pradeep
                    //div_MyProfile.Visible = false;
                    notificationLink.Visible = false;
                }
                int namelength = ((Session["UserSignIn"] as DataSet).Tables[0].Rows[0]["Name"].ToString()).Length;
                if (namelength > 7)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "length", "UsernameLength('" + (Session["UserSignIn"] as DataSet).Tables[0].Rows[0]["Name"].ToString() + "');", true);

                    lnkUser.Attributes.Add("title", "" + (Session["UserSignIn"] as DataSet).Tables[0].Rows[0]["Name"].ToString() + "");
                }
                else
                {
                    lnkUser.Text = (Session["UserSignIn"] as DataSet).Tables[0].Rows[0]["Name"].ToString();
                    
                    lnkUser.Attributes.Add("title", "" + (Session["UserSignIn"] as DataSet).Tables[0].Rows[0]["Name"].ToString() + "");
                }
                InitializeThePage();
                //for getting the notifications
                //objBAL_TMS = new TMS_BAL();
                //DataTable dtNotification = objBAL_TMS.getDatatoNotifications(EmpID);
                //lblNotificationCount.Text = dtNotification.Rows.Count.ToString();
            }
        }
        //[WebMethod]
        //public static List<NotificationData> GetNotifications()
        //{
        //    List<NotificationData> Notification = new List<NotificationData>();

        //    TMS_BAL objTMS_Bal = new TMS_BAL();

        //    DataTable dtNotification = objTMS_Bal.getDatatoNotifications(EmpID);
        //    // lblNotificationCount.Text = dtNotification.Rows.Count.ToString();

        //    foreach (DataRow drresult in dtNotification.Rows)
        //    {
        //        Notification.Add(new NotificationData
        //        {
        //            NotifiCationID = drresult["NotificationID"].ToString(),
        //            Description = drresult["Description"].ToString(),
        //            Module = drresult["Module"].ToString(),
        //            Status = drresult["Notificationstatus"].ToString(),
        //            ReferenceLink = drresult["NotificationLink"].ToString(),
        //            Date = drresult["GeneratedDate"].ToString(),
        //        });
        //    }

        //    return Notification;
        //}

        protected void lnkLogOut_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("~/UserLogin.aspx");
        }

        //Modified by pradeep for Security Profile

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("~/UserLogin.aspx");
        }

        protected void btnEditSec_Click(object sender, ImageClickEventArgs e)
        {
            //Session.Abandon();
            Response.Redirect("~/UserManagement/MyProfile.aspx");
        }
        public void InitializeThePage()
        {
            try
            {
                if (Cache.Get("CacheEmpInfo") != null && Request.Cookies.Get("CookieEmpID")!=null)
                {
                    EmpID = Convert.ToInt32((Session["UserSignIn"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                }
                HFMyProEmpID.Value = EmpID.ToString();
                GetDataToMyProfile();
                //LoadModules();
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "MyPro2" + strline + "  " + strMsg, "error");
            }
        }

        private void GetDataToMyProfile()
        {
            try
            {
                UMS_BAL objUMS_BAL = new UMS_BAL();
                EmpID = Convert.ToInt32(HFMyProEmpID.Value);
                DataSet dt = objUMS_BAL.GetDataToMyProfile(EmpID);
                ViewState["DT"] = dt;
                if (dt.Tables[0].Rows.Count > 0)
                {
                    //lblEmployeeCode.Text = dt.Tables[0].Rows[0]["EmpCode"].ToString();
                    lblFullName.Text = dt.Tables[0].Rows[0]["FirstName"].ToString() + " " + dt.Tables[0].Rows[0]["LastName"].ToString();
                    
                    abc.InnerText = dt.Tables[0].Rows[0]["FirstName"].ToString() + " " + dt.Tables[0].Rows[0]["LastName"].ToString();
                    //lblDob.Text = dt.Tables[0].Rows[0]["Dateofbirth"].ToString();
                    //lblGender.Text = dt.Tables[0].Rows[0]["Gender"].ToString();
                    lblDeparment.Text = dt.Tables[0].Rows[0]["DepartmentName"].ToString();
                    lblDesignation.Text = dt.Tables[0].Rows[0]["DesignationName"].ToString();
                    //lblBloodGroup.Text = dt.Tables[0].Rows[0]["BloodGroup"].ToString();
                    //if (!string.IsNullOrEmpty(dt.Tables[0].Rows[0]["StartDate"].ToString()))
                    //{
                    //    lblStartDate.Text = dt.Tables[0].Rows[0]["StartDate"].ToString();
                    //}
                    //else
                    //{
                    //    lblStartDate.Text = "- NA -";
                    //}
                    lblEmail.Text = dt.Tables[0].Rows[0]["OfficialEmailID"].ToString();
                    //txtAddress.Text = dt.Tables[0].Rows[0]["Address"].ToString();
                    //lblMobile.Text = dt.Tables[0].Rows[0]["MobileNo"].ToString();
                    //lblCountry.Text = dt.Tables[0].Rows[0]["CountryName"].ToString();
                    //lblState.Text = dt.Tables[0].Rows[0]["StateName"].ToString();
                    //txtSecQuestion.Text = dt.Tables[0].Rows[0]["Question"].ToString();
                    //hdnQuestion.Value = dt.Tables[0].Rows[0]["Question"].ToString();
                    //txtSecAnswer.Text = dt.Tables[0].Rows[0]["Answer"].ToString();
                    //hdnAnswer.Value = dt.Tables[0].Rows[0]["Answer"].ToString();


                    if (!string.IsNullOrEmpty(dt.Tables[0].Rows[0]["Photo"].ToString()))
                    {
                        MyImage.ImageUrl = objUMS_BAL.BindApplicantImage((byte[])dt.Tables[0].Rows[0]["Photo"]);
                    }
                    else
                    {
                        MyImage.ImageUrl = "~/Images/UserLogin/DefaultImage.jpg";
                    }
                    //Modified by pradeep
                    if (!string.IsNullOrEmpty(dt.Tables[0].Rows[0]["Photo"].ToString()))
                    {
                        MyProfImage.ImageUrl = objUMS_BAL.BindApplicantImage((byte[])dt.Tables[0].Rows[0]["Photo"]);
                    }
                    else
                    {
                        MyProfImage.ImageUrl = "~/Images/UserLogin/DefaultImage.jpg";
                    }


                }
            }
            catch (SqlException sqlEx)
            {
                string strmsgF = HelpClass.SQLEscapeString(sqlEx.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "MyPro3" + strmsgF, "error");
            }
            catch (Exception ex)
            {
                string strline = HelpClass.LineNo(ex);
                string strMsg = HelpClass.SQLEscapeString(ex.Message);
                HelpClass.custAlertMsg(this, this.GetType(), "MyPro3" + strline + "  " + strMsg, "error");
            }
        }

        //protected void SecurityMyProfilebtn_Click(object sender, EventArgs e)
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_SecurityMyProf').modal({ show: 'true', backdrop: 'static', keyboard: false });", true);
        //    dvDocViewer1.InnerHtml = "&nbsp";
        //    span_VisitorTitle1.InnerText = "";
        //    dvDocViewer1.InnerHtml = "<iframe  src=\"" + ResolveUrl("~/UserManagement/MyProfile1.aspx") + "#toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" height=\"672px\"></iframe>";
        //    UpViewDL1.Update();
        //    UpHeading1.Update();
        //}

        //protected void SecurityChangePwd_Click(object sender, EventArgs e)
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pop1", "$('#myModal_SecurityPwdChange').modal({ show: 'true', backdrop: 'static', keyboard: false });", true);
        //    dvDocViewer2.InnerHtml = "&nbsp";
        //    span_VisitorTitle2.InnerText = "";
        //    dvDocViewer2.InnerHtml = "<iframe  src=\"" + ResolveUrl("~/UserManagement/SecuritySettings1.aspx") + "#toolbar=0&navpanes=0&scrollbar=0\" width=\"100%\" height=\"270px\" frameBorder=\"0\"></iframe>";
        //    UpViewDL2.Update();
        //    UpHeading2.Update();
        //}
    }
}