﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.MasterPages.TMS_Master
{
    public partial class TMS_Master : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserSignIn"]!= null)
                {
                    Page.Header.DataBind();
                    DataTable dt = (Session["UserSignIn"] as DataSet).Tables[1];
                    DataTable dtTemp = new DataTable();
                    DataRow[] drUMS = dt.Select("ModuleID="+(int)Modules.TMS);
                    if (drUMS.Length > 0)
                    {
                        dtTemp = drUMS.CopyToDataTable();
                        if (dtTemp.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtTemp.Rows.Count; i++)
                            {
                                if (Convert.ToInt32(dtTemp.Rows[i]["RoleID"]) == 3) // HOD
                                {
                                    liAdmintration.Visible = true;
                                    liJR_Template.Visible = true;

                                    liJR_Main.Visible = true;
                                    liAssignJR.Visible = true;
                                    liDeclinedJR.Visible = true;

                                    liTS_Main.Visible = true;
                                    liAssignTS.Visible = true;

                                    liApproveQuestionnarieMain.Visible = true;                                 
                                    liApproveQuestionnaire.Visible = true;

                                    liTS_Main.Visible = true;
                                    liAssignTS.Visible = true;
                                    liQA_DeclinedTS.Visible = true;
                                    
                                    liTrainingMain.Visible = true;
                                    liHOD_Evaluation.Visible = true;                                                                                                                                  
                                }
                                if (Convert.ToInt32(dtTemp.Rows[i]["RoleID"]) == 7) // Trainer
                                {
                                    liAdmintration.Visible = true;
                                    liJR_Template.Visible = true;
                                    liQuestionnariesMaster.Visible = true;

                                    //liJR_Main.Visible = true;
                                    //liAssignJR.Visible = true;
                                   
                                    liQA_DeclinedTS.Visible = true;

                                    liTrainingMain.Visible = true;
                                    liTrainerEvaluation.Visible = true;                                                                                                  
                                }
                                if (Convert.ToInt32(dtTemp.Rows[i]["RoleID"]) == 5) //QA
                                {
                                    liJR_Main.Visible = true;
                                    liJR_Approval.Visible = true;

                                    liTS_Main.Visible = true;                                   
                                    liTS_Approval.Visible = true;
                                  
                                    liTrainingMain.Visible = true;
                                    //liQA_Evaluation.Visible = true;                                                            
                                }
                                if (Convert.ToInt32(dtTemp.Rows[i]["RoleID"]) == 6) // Trainee
                                {
                                    liJR_Main.Visible = true;
                                    liAcceptMyJR.Visible = true;

                                    liTrainingMain.Visible = true;
                                    liJrTraining.Visible = true;                                      
                                }                                
                            }
                        }
                    }
                }
            }
        }
        protected void lnkLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UserLogin.aspx", false);
            Session.Abandon();
        }
    }
}