﻿using AizantIT_PharmaApp.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_BusinessLayer;
using static Aizant_Enums.AizantEnums;

namespace AizantIT_PharmaApp.MasterPages.TMS_Master
{
    public partial class TMS_NestedMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HelpClass.IsUserAuthenticated())
                {
                    //Page.Header.DataBind();
                    DataTable dt = (Session["UserDetails"] as DataSet).Tables[1];
                    DataTable dtTemp = new DataTable();
                    DataRow[] drModule = dt.Select("ModuleID="+(int)Modules.TMS);
                    if (drModule.Length > 0)
                    {
                        dtTemp = drModule.CopyToDataTable();
                        if (dtTemp.Rows.Count > 0)
                        {
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.Admin_TMS).Length > 0 && dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0 && dtTemp.Select("RoleID=" + (int)TMS_UserRole.QA_TMS).Length > 0 && dtTemp.Select("RoleID="+(int)TMS_UserRole.Trainee_TMS).Length == 0 && dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainer_TMS).Length > 0)
                            {
                                //divTmsDashboard.Visible = false;
                            }
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.Dept_Head).Length > 0) // Department Head
                            {
                                divTmsDashboard.Visible = true;
                                liReports_Main.Visible = true;
                                liScheduledJRAndTT_PendingList.Visible = true;
                            }
                                if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.HOD_TMS).Length > 0) // HOD
                            {
                                divTmsDashboard.Visible = true;
                                //Administration
                                liAdmintration.Visible = true;
                                liQuestionnaireMain.Visible = true;
                                liApproveQuestionnaire.Visible = true;
                                liTrainingCalendarMain.Visible = true;
                                liReviewTrainingCalendar_HOD.Visible = true;
                                liInitializeATC.Visible = true;
                                liFeedbackmain.Visible = true;
                                liFeedback.Visible = true;

                                //Job Responsibilities(JR)
                                liJR_Main.Visible = true;
                                liJR_Template_Main.Visible = true;
                                liJR_Template.Visible = true;
                                liJR_Templatelist.Visible = true;
                                liAssignJR.Visible = true;
                                liRevertedJR.Visible = true;

                                //Training Schedule(TS)
                                liTS_Main.Visible = true;
                                liTS_JR_Main.Visible = true;
                                // liAssignTS.Visible = true;
                                liQA_DeclinedTS.Visible = true;
                                liTS_TT_Main.Visible = true;
                                liCreateTT.Visible = true;
                                liPendingTT.Visible = true;

                                //Training
                                divHODorTrainee.Visible = true;
                                liTrainingMain.Visible = true;
                                liHOD_Evaluation.Visible = true;

                                //Reports
                                liReports_Main.Visible = true;
                                //liReports_Audit_SubMain.Visible = true;
                                //liJR_AuditTrail.Visible = true;
                                //liTT_AuditTrail.Visible = true;
                                liDocReports.Visible = true;
                                liEmpJRList.Visible = true;
                                liTrainingCalendarList.Visible = true;
                                liEmployeeTrainingRecords.Visible = true;
                                liTTList.Visible = true;
                            }
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.Admin_TMS).Length > 0) // Admin
                            {
                                divModifyActionEmps.Visible = true;
                                divTmsDashboard.Visible = false;
                            }
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.QA_TMS).Length > 0) // QA
                            {
                                divTmsDashboard.Visible = true;
                                //Administration
                                liAdmintration.Visible = true;
                                liTrainingCalendarMain.Visible = true;
                                liApproveTrainingCalendar_QA.Visible = true;

                                //Job Responsibilities(JR)
                                liJR_Main.Visible = true;
                                liJR_Approval.Visible = true;

                                //Training Schedule(TS)
                                liTS_Main.Visible = true;
                                liTS_JR_Main.Visible = true;
                                liTS_Approval.Visible = true;
                                liTS_TT_Main.Visible = true;
                                liApproveTT.Visible = true;

                                //Reports
                                liReports_Main.Visible = true;
                                //liReports_Audit_SubMain.Visible = true;
                                //liJR_AuditTrail.Visible = true;
                                // liTT_AuditTrail.Visible = true;
                                liDocReports.Visible = true;
                                liEmpJRList.Visible = true;
                                liTrainingCalendarList.Visible = true;
                                liEmployeeTrainingRecords.Visible = true;
                                liTTList.Visible = true;
                            }
                            if (dtTemp.Select("RoleID="+(int)TMS_UserRole.Trainee_TMS).Length > 0) // Trainee
                            {
                                divTmsDashboard.Visible = true;

                                liJR_Main.Visible = true;
                                liAcceptMyJR.Visible = true;

                                //Training
                                divHODorTrainee.Visible = true;
                                liTrainingMain.Visible = true;
                                liJrTraining.Visible = true;
                                liTargetTraining.Visible = true;

                                liTrainerAnswerOnDoc.Visible = true;

                                //Reports
                                liReports_Main.Visible = true;
                                liMyTrainingRecords.Visible = true;
                            }
                            if (dtTemp.Select("RoleID=" + (int)TMS_UserRole.Trainer_TMS).Length > 0) // Trainer
                            {
                                divTmsDashboard.Visible = true;
                                //Administration
                                liAdmintration.Visible = true;
                                liQuestionnaireMain.Visible = true;
                                liQuestionnarie.Visible = true;
                                liDocumentFAQs.Visible = true;
                                liTrainingCalendarMain.Visible = true;
                                liCreateTrainingCalendar.Visible = true;
                                liRevertedTrainingCalendar.Visible = true;

                                liTraineeQuestionOnDoc.Visible = true;

                                //Training Schedule
                                liTS_Main.Visible = true;
                                //for Target TS
                                liTS_TT_Main.Visible = true;
                                liQA_ApprovedList.Visible = true;

                                //Training
                                liTrainingMain.Visible = true;
                                liTrainerEvaluationMain.Visible = true;
                                liTrainerEvaluation.Visible = true;
                                liTargetTrainingEvaluation.Visible = true;

                                //Reports
                                liReports_Main.Visible = true;
                                //liReports_Audit_SubMain.Visible = true;
                                //liJR_AuditTrail.Visible = true;
                                //liTT_AuditTrail.Visible = true; 
                                liDocReports.Visible = true;
                                liEmpJRList.Visible = true;
                                liTrainingCalendarList.Visible = true;
                                liEmployeeTrainingRecords.Visible = true;
                                liTTList.Visible = true;
                            }                            
                        }
                    }
                }
            }
        }
    }
}