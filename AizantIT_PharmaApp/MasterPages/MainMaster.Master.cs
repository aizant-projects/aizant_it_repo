﻿using AizantIT_PharmaApp.Common;
using System;
using System.Data;
using TMS_BusinessLayer;

namespace AizantIT_PharmaApp.MasterPages
{
    public partial class MainMaster : System.Web.UI.MasterPage
    {
        public int resulttime { get; set; }
        TMS_BAL objBAL_TMS;
        static int EmpID = 0;

        protected void Page_Load(object sender, EventArgs e)
        {            
            if (Session["UserDetails"]!=null)
            {
                EmpID = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[0].Rows[0]["EmpID"]);
                ViewState["RoleID"] = Convert.ToInt32((Session["UserDetails"] as DataSet).Tables[1].Rows[0]["RoleID"]);
                hdnEmpID.Value = EmpID.ToString();
                if (ViewState["RoleID"].ToString() == "22")//To hide My Profile and Notification Icon to Security User 22-security RoleID
                {
                    //div_MyProfile.Visible = false;
                    notificationLink.Visible = false;
                }
                HFMyProEmpID.Value = EmpID.ToString();
                /*
                int namelength = ((Session["UserSignIn"] as DataSet).Tables[0].Rows[0]["Name"].ToString()).Length;
                if (namelength > 7)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "length", "UsernameLength('" + (Session["UserSignIn"] as DataSet).Tables[0].Rows[0]["Name"].ToString() + "');", true);
                    lnkUser.Attributes.Add("title", "" + (Session["UserSignIn"] as DataSet).Tables[0].Rows[0]["Name"].ToString() + "");
                }
                else
                {
                    lnkUser.Text = (Session["UserSignIn"] as DataSet).Tables[0].Rows[0]["Name"].ToString();
                    lnkUser.Attributes.Add("title", "" + (Session["UserSignIn"] as DataSet).Tables[0].Rows[0]["Name"].ToString() + "");
                }
                */
                
                //for getting the notifications
                //objBAL_TMS = new TMS_BAL();
                //DataTable dtNotification = objBAL_TMS.getDatatoNotifications(EmpID);
                //lblNotificationCount.Text = dtNotification.Rows.Count.ToString();
            }
        }
     
        //protected void btnLogout_Click(object sender, EventArgs e)
        //{
        //    if (HelpClass.IsUserAuthenticated())
        //    {
        //        Response.Cookies["CookieExpire"].Expires =
        //        Response.Cookies["CookieUserIn"].Expires = DateTime.Now.AddDays(-1);
        //        DataTable dt =(Session["UserDetails"] as DataSet).Tables["UserDetails"];
        //        //RemoveSessionFromDB(dt.Rows[0]["ActiveUserSession"].ToString());
        //        Session.RemoveAll();                            
        //    }
        //    Response.Redirect("~/UserLogin.aspx");
        //}
        
    }
}
