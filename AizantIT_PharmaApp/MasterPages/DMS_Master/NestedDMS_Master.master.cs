﻿using AizantIT_DMSBAL;
using AizantIT_PharmaApp.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_BusinessLayer;

namespace AizantIT_PharmaApp.MasterPages.DMS_Master
{
    public partial class NestedDMS_Master : System.Web.UI.MasterPage
    {
        DocumentCreationBAL DMS_bal = new DocumentCreationBAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext.Current.Response.Cache.SetAllowResponseInBrowserHistory(false);
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            if (!IsPostBack)
            {
                if (Session["UserDetails"]!=null)
                {
                    DataTable dtx = (Session["UserDetails"] as DataSet).Tables[1];
                    DataTable dtTemp = new DataTable();
                    DataRow[] drUMS = dtx.Select("ModuleID=2");//2-DMS
                    if (drUMS.Length > 0)
                    {
                        dtTemp = drUMS.CopyToDataTable();
                        if (dtTemp.Select("RoleID=2").Length > 0)//admin role
                        {
                            Admin.Visible = true;
                            docmainList.Visible = true;
                            ObsoleteDocumentsList.Visible = true;
                            formmainList.Visible = true;
                        }
                        if (dtTemp.Select("RoleID=9").Length > 0)//approver role
                        {
                            Approver.Visible = true;
                            docmainList.Visible = true;
                            //ObsoleteDocumentsList.Visible = true;
                        }
                        if (dtTemp.Select("RoleID=10").Length > 0)//reviewer role
                        {
                            Reviewer.Visible = true;
                            docmainList.Visible = true;
                            //ObsoleteDocumentsList.Visible = true;
                        }
                       
                        if (dtTemp.Select("RoleID=12").Length > 0)//initiator role
                        {
                            Initiator.Visible = true;
                            docmainList.Visible = true;
                            //ObsoleteDocumentsList.Visible = true;
                        }
                        if (dtTemp.Select("RoleID=13").Length > 0)//authorizer role
                        {
                            Author.Visible = true;
                            docmainList.Visible = true;
                            //ObsoleteDocumentsList.Visible = true;
                        }
                        if (dtTemp.Select("RoleID=11").Length > 0)//author role
                        {
                            Creator.Visible = true;
                            docmainList.Visible = true;
                            //ObsoleteDocumentsList.Visible = true;
                        }
                        if (dtTemp.Select("RoleID=23").Length > 0)//reader role
                        {
                            ReaderList.Visible = true;
                            printreqmainLists.Visible = true;
                        }                      
                        if (dtTemp.Select("RoleID=30").Length > 0)//controller role
                        {
                            Controller.Visible = true;
                            docmainList.Visible = true;
                            //ObsoleteDocumentsList.Visible = true;
                            formmainList.Visible = true;
                            printreqmainLists.Visible = true;
                            FormReturnReport.Visible = true;
                        }
                        if (dtTemp.Select("RoleID=32").Length > 0)//printreviewer role
                        {
                            printreviewer.Visible = true;
                            printreqmainLists.Visible = true;
                        }
                    }
                }
            }
        }
    }
}