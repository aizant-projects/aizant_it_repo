﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AizantIT_DMSBO
{
    public class TempExternalFileBO
    {
        private int _CrudType;

        public int CrudType
        {
            get { return _CrudType; }
            set { _CrudType = value; }
        }

        private int _Ref_FileID;

        public int Ref_FileID
        {
            get { return _Ref_FileID; }
            set { _Ref_FileID = value; }
        }

        private string _FileName;

        public string FileName
        {
            get { return _FileName; }
            set { _FileName = value; }
        }

        private string _FileExtension;

        public string FileExtension
        {
            get { return _FileExtension; }
            set { _FileExtension = value; }
        }

        private byte[] _FileContent;

        public byte[] FileContent
        {
            get { return _FileContent; }
            set { _FileContent = value; }
        }

        private int _DocExternal_VersionID;

        public int DocExternal_VersionID
        {
            get { return _DocExternal_VersionID; }
            set { _DocExternal_VersionID = value; }
        }
    }
}
