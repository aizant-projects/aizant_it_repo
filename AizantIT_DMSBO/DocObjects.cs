﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace AizantIT_DMSBO
{

    public class DocObjects
    {
        private int _pkid;

        public int pkid
        {
            get { return _pkid; }
            set { _pkid = value; }
        }
        private int _Status;

        public int Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        private int _Version;

        public int Version
        {
            get { return _Version; }
            set { _Version = value; }
        }


        private string _Filename;

        public string Filename
        {
            get { return _Filename; }
            set { _Filename = value; }
        }


        private byte[] _Content;

        public byte[] Content
        {
            get { return _Content; }
            set { _Content = value; }
        }

        private ListBox _oldrev;

        public ListBox oldrev
        {
            get { return _oldrev; }
            set { _oldrev = value; }
        }

        private ListBox _oldapr;

        public ListBox oldapr
        {
            get { return _oldapr; }
            set { _oldapr = value; }
        }


        private int _AutoDocName;

        public int AutoDocName
        {
            get { return _AutoDocName; }
            set { _AutoDocName = value; }
        }

        private string _RType;

        public string RType
        {
            get { return _RType; }
            set { _RType = value; }
        }

        private string _DocumentID;

        public string DocumentID
        {
            get { return _DocumentID; }
            set { _DocumentID = value; }
        }
        private string _UserRole;

        public string UserRole
        {
            get { return _UserRole; }
            set { _UserRole = value; }
        }

        private string _dept;

        public string dept
        {
            get { return _dept; }
            set { _dept = value; }
        }

        private string _DocumentType;

        public string DocumentType
        {
            get { return _DocumentType; }
            set { _DocumentType = value; }
        }

        private string _ProcessType;

        public string ProcessType
        {
            get { return _ProcessType; }
            set { _ProcessType = value; }
        }
        private string _ReviewYear;
        public string ReviewYear
        {
            get { return _ReviewYear; }
            set { _ReviewYear = value; }
        }
        private string _TypeID;

        public string TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        private string _DMS_DocumentName;

        public string DMS_DocumentName
        {
            get { return _DMS_DocumentName; }
            set { _DMS_DocumentName = value; }
        }

        private int _DMS_DocumentType;

        public int DMS_DocumentType
        {
            get { return _DMS_DocumentType; }
            set { _DMS_DocumentType = value; }
        }
        private int _DMS_IntiatedBy;

        public int DMS_IntiatedBy
        {
            get { return _DMS_IntiatedBy; }
            set { _DMS_IntiatedBy = value; }
        }
        private int _DMS_ModifiedBy;

        public int DMS_ModifiedBy
        {
            get { return _DMS_ModifiedBy; }
            set { _DMS_ModifiedBy = value; }
        }
        private int _DMS_CreatedBy;

        public int DMS_CreatedBy
        {
            get { return _DMS_CreatedBy; }
            set { _DMS_CreatedBy = value; }
        }
        private int _DMS_Deparment;

        public int DMS_Deparment
        {
            get { return _DMS_Deparment; }
            set { _DMS_Deparment = value; }
        }

        private ListBox _DMS_ReviewedBy;

        public ListBox DMS_ReviewedBy
        {
            get { return _DMS_ReviewedBy; }
            set { _DMS_ReviewedBy = value; }
        }

        private ListBox _DMS_ApprovedBy;

        public ListBox DMS_ApprovedBy
        {
            get { return _DMS_ApprovedBy; }
            set { _DMS_ApprovedBy = value; }
        }

        private int _RequestType;

        public int RequestType
        {
            get { return _RequestType; }
            set { _RequestType = value; }
        }

        private string _FormRequestType;

        public string FormRequestType
        {
            get { return _FormRequestType; }
            set { _FormRequestType = value; }
        }
        private int _authorBy;

        public int authorBy
        {
            get { return _authorBy; }
            set { _authorBy = value; }
        }

        private string _documentNUmber;

        public string documentNUmber
        {
            get { return _documentNUmber; }
            set { _documentNUmber = value; }
        }

        private DateTime _effective;

        public DateTime effective
        {
            get { return _effective; }
            set { _effective = value; }
        }

        public DateTime? expiration { get; set; }
        private string _Purpose;

        public string Purpose
        {
            get { return _Purpose; }
            set { _Purpose = value; }
        }
        public int Validity { get; set; }
        public string PrintReqComments { get; set; }

        private string _PreparedBY;

        public string PreparedBY
        {
            get { return _PreparedBY; }
            set { _PreparedBY = value; }
        }

        private string _oldReviewedBy;

        public string oldReviewedBy
        {
            get { return _oldReviewedBy; }
            set { _oldReviewedBy = value; }
        }

        private string _oldApprovedBY;

        public string oldApprovedBY
        {
            get { return _oldApprovedBY; }
            set { _oldApprovedBY = value; }
        }

        private int _RequestTypeID;

        public int RequestTypeID
        {
            get { return _RequestTypeID; }
            set { _RequestTypeID = value; }
        }

        private DateTime _PreparedDate;

        public DateTime PreparedDate
        {
            get { return _PreparedDate; }
            set { _PreparedDate = value; }
        }

        private DateTime _ReviewedDate;

        public DateTime ReviewedDate
        {
            get { return _ReviewedDate; }
            set { _ReviewedDate = value; }
        }

        private DateTime _ApprovedDate;

        public DateTime ApprovedDate
        {
            get { return _ApprovedDate; }
            set { _ApprovedDate = value; }
        }

        private List<Int_EmployeeBy> _lstApproveBy;

        public List<Int_EmployeeBy> lstApproveBy
        {
            get { return _lstApproveBy; }
            set { _lstApproveBy = value; }
        }

        private List<Int_EmployeeBy> _lstQA;

        public List<Int_EmployeeBy> lstQA
        {
            get { return _lstQA; }
            set { _lstQA = value; }
        }

        private List<Int_EmployeeBy> _lstReviewBy;

        public List<Int_EmployeeBy> lstReviewBy
        {
            get { return _lstReviewBy; }
            set { _lstReviewBy = value; }
        }

        private int _action;

        public int action
        {
            get { return _action; }
            set { _action = value; }
        }

        private DateTime _FromDate;

        public DateTime FromDate
        {
            get { return _FromDate; }
            set { _FromDate = value; }
        }

        private DateTime _ToDate;

        public DateTime ToDate
        {
            get { return _ToDate; }
            set { _ToDate = value; }
        }

        private int _Mode = 0;

        public int Mode
        {
            get { return _Mode; }
            set { _Mode = value; }
        }

        private string _DocStatus;

        public string DocStatus
        {
            get { return _DocStatus; }
            set { _DocStatus = value; }
        }

        private int _isManage;

        public int isManage
        {
            get { return _isManage; }
            set { _isManage = value; }
        }

        private string _Remarks;

        public string Remarks
        {
            get { return _Remarks; }
            set { _Remarks = value; }
        }

        private string _Pageno;

        public string Pageno
        {
            get { return _Pageno; }
            set { _Pageno = value; }
        }

        private int _DMSRequestByID;

        public int DMSRequestByID
        {
            get { return _DMSRequestByID; }
            set { _DMSRequestByID = value; }
        }

        private int _DMSReviewedByID;

        public int DMSReviewedByID
        {
            get { return _DMSReviewedByID; }
            set { _DMSReviewedByID = value; }
        }

        private int _DMSDocControllerID;

        public int DMSDocControllerID
        {
            get { return _DMSDocControllerID; }
            set { _DMSDocControllerID = value; }
        }

        private bool _IsSoftCopy;

        public bool IsSoftCopy
        {
            get { return _IsSoftCopy; }
            set { _IsSoftCopy = value; }
        }

        private int _EmpID;

        public int EmpID
        {
            get { return _EmpID; }
            set { _EmpID = value; }
        }

        private string _FormNumber;

        public string FormNumber
        {
            get { return _FormNumber; }
            set { _FormNumber = value; }
        }

        private string _FormName;

        public string FormName
        {
            get { return _FormName; }
            set { _FormName = value; }
        }

        private int _RowNumber;

        public int RowNumber
        {
            get { return _RowNumber; }
            set { _RowNumber = value; }
        }

        private int _FormVersionID;

        public int FormVersionID
        {
            get { return _FormVersionID; }
            set { _FormVersionID = value; }
        }

        private string _CreatedBy;

        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }

        private string _ActionName;

        public string ActionName
        {
            get { return _ActionName; }
            set { _ActionName = value; }
        }

        private int _NoofCopies;

        public int NoofCopies
        {
            get { return _NoofCopies; }
            set { _NoofCopies = value; }
        }

        private string _ProjectNumber;

        public string ProjectNumber
        {
            get { return _ProjectNumber; }
            set { _ProjectNumber = value; }
        }

        private int _FormReqID;

        public int FormReqID
        {
            get { return _FormReqID; }
            set { _FormReqID = value; }
        }

        private string _FilePath;

        public string FilePath
        {
            get { return _FilePath; }
            set { _FilePath = value; }
        }

        private int _RoleID;

        public int RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }

        private string _CopyNumbers;

        public string CopyNumbers
        {
            get { return _CopyNumbers; }
            set { _CopyNumbers = value; }
        }

        private int _DocRefID;

        public int DocRefID
        {
            get { return _DocRefID; }
            set { _DocRefID = value; }
        }
        private string _VersionNumber;

        public string VersionNumber
        {
            get { return _VersionNumber; }
            set { _VersionNumber = value; }
        }
        private int _PendingFormRequestCount;

        public int PendingFormRequestCount
        {
            get { return _PendingFormRequestCount; }
            set { _PendingFormRequestCount = value; }
        }
        private int _PendingFormRevisionCount;

        public int PendingFormRevisionCount
        {
            get { return _PendingFormRevisionCount; }
            set { _PendingFormRevisionCount = value; }
        }
        private int _isTemp;

        public int isTemp
        {
            get { return _isTemp; }
            set { _isTemp = value; }
        }

        private string _FilePath1;

        public string FilePath1
        {
            get { return _FilePath1; }
            set { _FilePath1 = value; }
        }

        private string _FilePath2;

        public string FilePath2
        {
            get { return _FilePath2; }
            set { _FilePath2 = value; }
        }

        private string _FileType1;

        public string FileType1
        {
            get { return _FileType1; }
            set { _FileType1 = value; }
        }

        private string _FileType2;

        public string FileType2
        {
            get { return _FileType2; }
            set { _FileType2 = value; }
        }

        private string _FileName1;

        public string FileName1
        {
            get { return _FileName1; }
            set { _FileName1 = value; }
        }

        private string _FileName2;

        public string FileName2
        {
            get { return _FileName2; }
            set { _FileName2 = value; }
        }

        private string _Time;

        public string Time
        {
            get { return _Time; }
            set { _Time = value; }
        }

        private int _CommentType;

        public int CommentType
        {
            get { return _CommentType; }
            set { _CommentType = value; }
        }

        private int _AssignedTo;

        public int AssignedTo
        {
            get { return _AssignedTo; }
            set { _AssignedTo = value; }
        }

        private int _AssignedRole;

        public int AssignedRole
        {
            get { return _AssignedRole; }
            set { _AssignedRole = value; }
        }

        private int _CopyType;

        public int CopyType
        {
            get { return _CopyType; }
            set { _CopyType = value; }
        }
    }
    public class ReferralBO
    {
        private int _Doc_VersionID = 0;

        public int Doc_VersionID
        {
            get { return _Doc_VersionID; }
            set { _Doc_VersionID = value; }
        }
        private int _Doc_Ref_VersionID = 0;

        public int Doc_Ref_VersionID
        {
            get { return _Doc_Ref_VersionID; }
            set { _Doc_Ref_VersionID = value; }
        }
        private string _Doc_RefContent = string.Empty;

        public string Doc_RefContent
        {
            get { return _Doc_RefContent; }
            set { _Doc_RefContent = value; }
        }
        private string _Doc_FileName = string.Empty;

        public string Doc_FileName
        {
            get { return _Doc_FileName; }
            set { _Doc_FileName = value; }
        }
        private string _Doc_FileExtension = string.Empty;

        public string Doc_FileExtension
        {
            get { return _Doc_FileExtension; }
            set { _Doc_FileExtension = value; }
        }
        private byte[] _Doc_FileContent = null;

        public byte[] Doc_FileContent
        {
            get { return _Doc_FileContent; }
            set { _Doc_FileContent = value; }
        }
    }
    public class JQDataTableBO
    {
        private int _iDisplayLength = 0;
        public string IsJQueyList { get; set; }
        public string ReportTitle { get; set; }
        public int iDisplayLength
        {
            get { return _iDisplayLength; }
            set { _iDisplayLength = value; }
        }
        private int _iDisplayStart = 0;

        public int iDisplayStart
        {
            get { return _iDisplayStart; }
            set { _iDisplayStart = value; }
        }
        private int _iSortCol = 0;

        public int iSortCol
        {
            get { return _iSortCol; }
            set { _iSortCol = value; }
        }
        private string _sSortDir;

        public string sSortDir
        {
            get { return _sSortDir; }
            set { _sSortDir = value; }
        }
        private string _sSearch;

        public string sSearch
        {
            get { return _sSearch; }
            set { _sSearch = value; }
        }
        private string _sSearchDocumentNumber;

        public string sSearchDocumentNumber
        {
            get { return _sSearchDocumentNumber; }
            set { _sSearchDocumentNumber = value; }
        }
        private string _sSearchDocumentName;

        public string sSearchDocumentName
        {
            get { return _sSearchDocumentName; }
            set { _sSearchDocumentName = value; }
        }
        private string _sSearchVersionNumber;

        public string sSearchVersionNumber
        {
            get { return _sSearchVersionNumber; }
            set { _sSearchVersionNumber = value; }
        }
        private string _sSearchDocumentType;

        public string sSearchDocumentType
        {
            get { return _sSearchDocumentType; }
            set { _sSearchDocumentType = value; }
        }
        private string _sSearchDepartment;

        public string sSearchDepartment
        {
            get { return _sSearchDepartment; }
            set { _sSearchDepartment = value; }
        }
        private string _sSearchRequestType;

        public string sSearchRequestType
        {
            get { return _sSearchRequestType; }
            set { _sSearchRequestType = value; }
        }
        private string _sSearchEffectiveDate;

        public string sSearchEffectiveDate
        {
            get { return _sSearchEffectiveDate; }
            set { _sSearchEffectiveDate = value; }
        }
        private string _sSearchExpirationDate;

        public string sSearchExpirationDate
        {
            get { return _sSearchExpirationDate; }
            set { _sSearchExpirationDate = value; }
        }
        private string _sSearchStatus;
        public string sSearchStatus
        {
            get { return _sSearchStatus; }
            set { _sSearchStatus = value; }
        }
        private string _sSearchFormNumber;

        public string sSearchFormNumber
        {
            get { return _sSearchFormNumber; }
            set { _sSearchFormNumber = value; }
        }
        private string _sSearchFormName;

        public string sSearchFormName
        {
            get { return _sSearchFormName; }
            set { _sSearchFormName = value; }
        }
        private string _sSearchPurpose;

        public string sSearchPurpose
        {
            get { return _sSearchPurpose; }
            set { _sSearchPurpose = value; }
        }
        private string _sSearchCreatedBy;

        public string sSearchCreatedBy
        {
            get { return _sSearchCreatedBy; }
            set { _sSearchCreatedBy = value; }
        }
        private string _sSearchApprovedBy;

        public string sSearchApprovedBy
        {
            get { return _sSearchApprovedBy; }
            set { _sSearchApprovedBy = value; }
        }
        private string _sSearchActionName;

        public string sSearchActionName
        {
            get { return _sSearchActionName; }
            set { _sSearchActionName = value; }
        }
        private string _sSearchInitiatedBy;

        public string sSearchInitiatedBy
        {
            get { return _sSearchInitiatedBy; }
            set { _sSearchInitiatedBy = value; }
        }
        private string _sSearchRequestNumber;

        public string sSearchRequestNumber
        {
            get { return _sSearchRequestNumber; }
            set { _sSearchRequestNumber = value; }
        }
        private string _sSearchPageNumbers;

        public string sSearchPageNumbers
        {
            get { return _sSearchPageNumbers; }
            set { _sSearchPageNumbers = value; }
        }
        private string _sSearchNoofCopies;

        public string sSearchNoofCopies
        {
            get { return _sSearchNoofCopies; }
            set { _sSearchNoofCopies = value; }
        }
        private string _sSearchProcessType;

        public string sSearchProcessType
        {
            get { return _sSearchProcessType; }
            set { _sSearchProcessType = value; }
        }
        private string _sSearchIsTraining;

        public string sSearchIsTraining
        {
            get { return _sSearchIsTraining; }
            set { _sSearchIsTraining = value; }
        }
        private string _sSearch_ReviewPeriod;
            public string sSearch_ReviewPeriodP
        {
            get { return _sSearch_ReviewPeriod; }
            set { _sSearch_ReviewPeriod = value; }
        }
        private string _OPeration;

        public string OPeration
        {
            get { return _OPeration; }
            set { _OPeration = value; }
        }
        private int _iMode = 0;

        public int iMode
        {
            get { return _iMode; }
            set { _iMode = value; }
        }
        private int _pkid = 0;

        public int pkid
        {
            get { return _pkid; }
            set { _pkid = value; }
        }
        private int _EmpID;

        public int EmpID
        {
            get { return _EmpID; }
            set { _EmpID = value; }
        }
        private string _DocStatus;

        public string DocStatus
        {
            get { return _DocStatus; }
            set { _DocStatus = value; }
        }
        private int _ChartStatus;

        public int ChartStatus
        {
            get { return _ChartStatus; }
            set { _ChartStatus = value; }
        }
        private int _ChartDocTypeID;

        public int ChartDocTypeID
        {
            get { return _ChartDocTypeID; }
            set { _ChartDocTypeID = value; }
        }
        private int _ChartDeptID;

        public int ChartDeptID
        {
            get { return _ChartDeptID; }
            set { _ChartDeptID = value; }
        }
        private DateTime _ChartFromDate;

        public DateTime ChartFromDate
        {
            get { return _ChartFromDate; }
            set { _ChartFromDate = value; }
        }
        private DateTime _ChartToDate;

        public DateTime ChartToDate
        {
            get { return _ChartToDate; }
            set { _ChartToDate = value; }
        }
        private string _ChartDeptCode;

        public string ChartDeptCode
        {
            get { return _ChartDeptCode; }
            set { _ChartDeptCode = value; }
        }
        private string _ChartDocTypeName;

        public string ChartDocTypeName
        {
            get { return _ChartDocTypeName; }
            set { _ChartDocTypeName = value; }
        }
        private int _DeptID;

        public int DeptID
        {
            get { return _DeptID; }
            set { _DeptID = value; }
        }
        private int _RType;

        public int RType
        {
            get { return _RType; }
            set { _RType = value; }
        }
        private int _Verify;

        public int Verify
        {
            get { return _Verify; }
            set { _Verify = value; }
        }
        private int _ReqTypeID;

        public int ReqTypeID
        {
            get { return _ReqTypeID; }
            set { _ReqTypeID = value; }
        }
        private string _sSearchRequestBy;

        public string sSearchRequestBy
        {
            get { return _sSearchRequestBy; }
            set { _sSearchRequestBy = value; }
        }
        private string _sSearchRequestedDate;

        public string sSearchRequestedDate
        {
            get { return _sSearchRequestedDate; }
            set { _sSearchRequestedDate = value; }
        }
    }
    public class ReportTitlePath
    {
        public string ReportTitle { get; set; }
        public string ReportPath { get; set; }
    }
    public class ExistingDocBO
    {
        private int _RowNumber;

        public int RowNumber
        {
            get { return _RowNumber; }
            set { _RowNumber = value; }
        }

        private int _VersionID;

        public int VersionID
        {
            get { return _VersionID; }
            set { _VersionID = value; }
        }
        private string _Department;

        public string Department
        {
            get { return _Department; }
            set { _Department = value; }
        }

        private string _DocumentName;

        public string DocumentName
        {
            get { return _DocumentName; }
            set { _DocumentName = value; }
        }
        private int _Version;

        public int Version
        {
            get { return _Version; }
            set { _Version = value; }
        }
        private string _DocumentType;

        public string DocumentType
        {
            get { return _DocumentType; }
            set { _DocumentType = value; }
        }

        private string _DocumentNumber;

        public string DocumentNumber
        {
            get { return _DocumentNumber; }
            set { _DocumentNumber = value; }
        }
        private string _EffectiveDate;

        public string EffectiveDate
        {
            get { return _EffectiveDate; }
            set { _EffectiveDate = value; }
        }
        private string _ExpirationDate;

        public string ExpirationDate
        {
            get { return _ExpirationDate; }
            set { _ExpirationDate = value; }
        }
        private int _expStatus;

        public int expStatus
        {
            get { return _expStatus; }
            set { _expStatus = value; }
        }
        private string _VersionNumber;

        public string VersionNumber
        {
            get { return _VersionNumber; }
            set { _VersionNumber = value; }
        }
    }
    public class DocumentTypeBO
    {
        private int _RowNumber;

        public int RowNumber
        {
            get { return _RowNumber; }
            set { _RowNumber = value; }
        }
        private int _DocumentTypeID;

        public int DocumentTypeID
        {
            get { return _DocumentTypeID; }
            set { _DocumentTypeID = value; }
        }
        private string _DocumentType;

        public string DocumentType
        {
            get { return _DocumentType; }
            set { _DocumentType = value; }
        }
        private int _ProcessTypeID;

        public int ProcessTypeID
        {
            get { return _ProcessTypeID; }
            set { _ProcessTypeID = value; }
        }
        private string _ProcessType;

        public string ProcessType
        {
            get { return _ProcessType; }
            set { _ProcessType = value; }
        }
        private string _ReviewPeriod;
        public string ReviewPeriod
        {
            get { return _ReviewPeriod; }
            set { _ReviewPeriod = value; }
        } 
        private string _IsTraining;

        public string IsTraining
        {
            get { return _IsTraining; }
            set { _IsTraining = value; }
        }
    }
    public class AddTemplateBO
    {
        private int _RowNumber;

        public int RowNumber
        {
            get { return _RowNumber; }
            set { _RowNumber = value; }
        }
        private int _TemplateID;

        public int TemplateID
        {
            get { return _TemplateID; }
            set { _TemplateID = value; }
        }
        private int _CreatedBy;

        public int CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        private string _TemplateName;

        public string TemplateName
        {
            get { return _TemplateName; }
            set { _TemplateName = value; }
        }

    }
    public class DocListBO
    {
        private int _RowNumber;

        public int RowNumber
        {
            get { return _RowNumber; }
            set { _RowNumber = value; }
        }

        private int _VersionID;

        public int VersionID
        {
            get { return _VersionID; }
            set { _VersionID = value; }
        }
        private string _Department;

        public string Department
        {
            get { return _Department; }
            set { _Department = value; }
        }

        private string _DocumentName;

        public string DocumentName
        {
            get { return _DocumentName; }
            set { _DocumentName = value; }
        }

        private int _Version;

        public int Version
        {
            get { return _Version; }
            set { _Version = value; }
        }

        private string _DocumentNumber;

        public string DocumentNumber
        {
            get { return _DocumentNumber; }
            set { _DocumentNumber = value; }
        }
        private string _DocumentType;

        public string DocumentType
        {
            get { return _DocumentType; }
            set { _DocumentType = value; }
        }
        private string _EffectiveDate;

        public string EffectiveDate
        {
            get { return _EffectiveDate; }
            set { _EffectiveDate = value; }
        }
        private string _ExpirationDate;

        public string ExpirationDate
        {
            get { return _ExpirationDate; }
            set { _ExpirationDate = value; }
        }
        private int _expStatus;

        public int expStatus
        {
            get { return _expStatus; }
            set { _expStatus = value; }
        }

        private int _ReferralCount;

        public int ReferralCount
        {
            get { return _ReferralCount; }
            set { _ReferralCount = value; }
        }
        private string _IsPublic;

        public string IsPublic
        {
            get { return _IsPublic; }
            set { _IsPublic = value; }
        }
        private string _VersionNumber;

        public string VersionNumber
        {
            get { return _VersionNumber; }
            set { _VersionNumber = value; }
        }
    }
    public class DatesBO
    {
        private int _RowNumber;

        public int RowNumber
        {
            get { return _RowNumber; }
            set { _RowNumber = value; }
        }

        private int _VersionID;

        public int VersionID
        {
            get { return _VersionID; }
            set { _VersionID = value; }
        }
        private int _vDocumentID;

        public int vDocumentID
        {
            get { return _vDocumentID; }
            set { _vDocumentID = value; }
        }


        private string _Department;

        public string Department
        {
            get { return _Department; }
            set { _Department = value; }
        }

        private string _DocumentName;

        public string DocumentName
        {
            get { return _DocumentName; }
            set { _DocumentName = value; }
        }

        private string _DocumentNumber;

        public string DocumentNumber
        {
            get { return _DocumentNumber; }
            set { _DocumentNumber = value; }
        }
        private string _EffectiveDate;

        public string EffectiveDate
        {
            get { return _EffectiveDate; }
            set { _EffectiveDate = value; }
        }
        private string _ExpirationDate;

        public string ExpirationDate
        {
            get { return _ExpirationDate; }
            set { _ExpirationDate = value; }
        }
        private bool _TrainingCount;

        public bool TrainingCount
        {
            get { return _TrainingCount; }
            set { _TrainingCount = value; }
        }
        private string _DocumentType;

        public string DocumentType
        {
            get { return _DocumentType; }
            set { _DocumentType = value; }
        }
        private int _Version;

        public int Version
        {
            get { return _Version; }
            set { _Version = value; }
        }
        private bool _IsTraining;

        public bool IsTraining
        {
            get { return _IsTraining; }
            set { _IsTraining = value; }
        }
        private string _VersionNumber;

        public string VersionNumber
        {
            get { return _VersionNumber; }
            set { _VersionNumber = value; }
        }
        private int _PendingDocRequestCount;

        public int PendingDocRequestCount
        {
            get { return _PendingDocRequestCount; }
            set { _PendingDocRequestCount = value; }
        }
        private int _RequestTypeID;

        public int RequestTypeID
        {
            get { return _RequestTypeID; }
            set { _RequestTypeID = value; }
        }
        private String _ReviewYear;
        public String ReviewYear
        {
            get { return _ReviewYear; }
            set { _ReviewYear = value; }
        }
        public int ReviewYearID { get; set; }
    }
    public class ReviewDocBO
    {
        private int _RowNumber;

        public int RowNumber
        {
            get { return _RowNumber; }
            set { _RowNumber = value; }
        }

        private int _VersionID;

        public int VersionID
        {
            get { return _VersionID; }
            set { _VersionID = value; }
        }

        private int _Id;

        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _Department;

        public string Department
        {
            get { return _Department; }
            set { _Department = value; }
        }

        private string _DocumentName;

        public string DocumentName
        {
            get { return _DocumentName; }
            set { _DocumentName = value; }
        }

        private string _DocumentNumber;

        public string DocumentNumber
        {
            get { return _DocumentNumber; }
            set { _DocumentNumber = value; }
        }

        private string _DocumentType;

        public string DocumentType
        {
            get { return _DocumentType; }
            set { _DocumentType = value; }
        }
        private string _Creator;

        public string Creator
        {
            get { return _Creator; }
            set { _Creator = value; }
        }
        private string _Initiator;

        public string Initiator
        {
            get { return _Initiator; }
            set { _Initiator = value; }
        }
        private String _RequestType;

        public String RequestType
        {
            get { return _RequestType; }
            set { _RequestType = value; }
        }
    }
    public class RejectedDocBO
    {
        private int _RowNumber;

        public int RowNumber
        {
            get { return _RowNumber; }
            set { _RowNumber = value; }
        }

        private int _VersionID;

        public int VersionID
        {
            get { return _VersionID; }
            set { _VersionID = value; }
        }

        private string _Department;

        public string Department
        {
            get { return _Department; }
            set { _Department = value; }
        }

        private string _DocumentName;

        public string DocumentName
        {
            get { return _DocumentName; }
            set { _DocumentName = value; }
        }

        private string _DocumentNumber;

        public string DocumentNumber
        {
            get { return _DocumentNumber; }
            set { _DocumentNumber = value; }
        }

        private string _DocumentType;

        public string DocumentType
        {
            get { return _DocumentType; }
            set { _DocumentType = value; }
        }
        private string _InitiatedBy;

        public string InitiatedBy
        {
            get { return _InitiatedBy; }
            set { _InitiatedBy = value; }
        }
        private string _RecordStatus;

        public string RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        private string _RequestType;

        public string RequestType
        {
            get { return _RequestType; }
            set { _RequestType = value; }
        }
    }
    public class ManageDocBO
    {
        private int _RowNumber;

        public int RowNumber
        {
            get { return _RowNumber; }
            set { _RowNumber = value; }
        }

        private int _VersionID;

        public int VersionID
        {
            get { return _VersionID; }
            set { _VersionID = value; }
        }

        private string _Department;

        public string Department
        {
            get { return _Department; }
            set { _Department = value; }
        }

        private string _DocumentName;

        public string DocumentName
        {
            get { return _DocumentName; }
            set { _DocumentName = value; }
        }

        private string _DocumentNumber;

        public string DocumentNumber
        {
            get { return _DocumentNumber; }
            set { _DocumentNumber = value; }
        }

        private string _DocumentType;

        public string DocumentType
        {
            get { return _DocumentType; }
            set { _DocumentType = value; }
        }
        private string _InitiatedBy;

        public string InitiatedBy
        {
            get { return _InitiatedBy; }
            set { _InitiatedBy = value; }
        }
        private string _RecordStatus;

        public string RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        private string _RequestType;

        public string RequestType
        {
            get { return _RequestType; }
            set { _RequestType = value; }
        }
    }
    public class GetQADocBO
    {
        private int _RowNumber;

        public int RowNumber
        {
            get { return _RowNumber; }
            set { _RowNumber = value; }
        }

        private int _VersionID;

        public int VersionID
        {
            get { return _VersionID; }
            set { _VersionID = value; }
        }

        private string _Department;

        public string Department
        {
            get { return _Department; }
            set { _Department = value; }
        }

        private string _DocumentName;

        public string DocumentName
        {
            get { return _DocumentName; }
            set { _DocumentName = value; }
        }

        private string _DocumentNumber;

        public string DocumentNumber
        {
            get { return _DocumentNumber; }
            set { _DocumentNumber = value; }
        }

        private string _DocumentType;

        public string DocumentType
        {
            get { return _DocumentType; }
            set { _DocumentType = value; }
        }
        private string _InitiatedBy;

        public string InitiatedBy
        {
            get { return _InitiatedBy; }
            set { _InitiatedBy = value; }
        }
        private string _RecordStatus;

        public string RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        private string _RequestType;

        public string RequestType
        {
            get { return _RequestType; }
            set { _RequestType = value; }
        }

        private int _RequestTypeID;

        public int RequestTypeID
        {
            get { return _RequestTypeID; }
            set { _RequestTypeID = value; }
        }
        private string _EffectiveDate;

        public string EffectiveDate
        {
            get { return _EffectiveDate; }
            set { _EffectiveDate = value; }
        }
        private string _ExpirationDate;

        public string ExpirationDate
        {
            get { return _ExpirationDate; }
            set { _ExpirationDate = value; }
        }
        private string _Status;

        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        private int _vDocumentID;

        public int vDocumentID
        {
            get { return _vDocumentID; }
            set { _vDocumentID = value; }
        }

        private bool _TrainingCount;

        public bool TrainingCount
        {
            get { return _TrainingCount; }
            set { _TrainingCount = value; }
        }
        private bool _IsTraining;

        public bool IsTraining
        {
            get { return _IsTraining; }
            set { _IsTraining = value; }
        }
        private int _PendingDocRequestCount;

        public int PendingDocRequestCount
        {
            get { return _PendingDocRequestCount; }
            set { _PendingDocRequestCount = value; }
        }
    }
    public class ApproveDocBO
    {
        private int _RowNumber;

        public int RowNumber
        {
            get { return _RowNumber; }
            set { _RowNumber = value; }
        }

        private int _VersionID;

        public int VersionID
        {
            get { return _VersionID; }
            set { _VersionID = value; }
        }

        private int _Id;

        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _Department;

        public string Department
        {
            get { return _Department; }
            set { _Department = value; }
        }

        private string _DocumentName;

        public string DocumentName
        {
            get { return _DocumentName; }
            set { _DocumentName = value; }
        }

        private string _DocumentNumber;

        public string DocumentNumber
        {
            get { return _DocumentNumber; }
            set { _DocumentNumber = value; }
        }

        private string _DocumentType;

        public string DocumentType
        {
            get { return _DocumentType; }
            set { _DocumentType = value; }
        }
        private string _Creator;

        public string Creator
        {
            get { return _Creator; }
            set { _Creator = value; }
        }
        private string _Initiator;

        public string Initiator
        {
            get { return _Initiator; }
            set { _Initiator = value; }
        }
        private string _RequestType;

        public string RequestType
        {
            get { return _RequestType; }
            set { _RequestType = value; }
        }

        private bool _TrainingCount;

        public bool TrainingCount
        {
            get { return _TrainingCount; }
            set { _TrainingCount = value; }
        }

        private int _authorCount;

        public int authorCount
        {
            get { return _authorCount; }
            set { _authorCount = value; }
        }
        private int _vDocumentID;

        public int vDocumentID
        {
            get { return _vDocumentID; }
            set { _vDocumentID = value; }
        }
        private bool _IsTraining;

        public bool IsTraining
        {
            get { return _IsTraining; }
            set { _IsTraining = value; }
        }
    }
    public class AuthorizeDocBO
    {
        private int _RowNumber;

        public int RowNumber
        {
            get { return _RowNumber; }
            set { _RowNumber = value; }
        }

        private int _VersionID;

        public int VersionID
        {
            get { return _VersionID; }
            set { _VersionID = value; }
        }

        private int _Id;

        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _Department;

        public string Department
        {
            get { return _Department; }
            set { _Department = value; }
        }

        private string _DocumentName;

        public string DocumentName
        {
            get { return _DocumentName; }
            set { _DocumentName = value; }
        }

        private string _DocumentNumber;

        public string DocumentNumber
        {
            get { return _DocumentNumber; }
            set { _DocumentNumber = value; }
        }

        private string _DocumentType;

        public string DocumentType
        {
            get { return _DocumentType; }
            set { _DocumentType = value; }
        }
        private string _Creator;

        public string Creator
        {
            get { return _Creator; }
            set { _Creator = value; }
        }
        private string _Initiator;

        public string Initiator
        {
            get { return _Initiator; }
            set { _Initiator = value; }
        }
        private string _RequestType;

        public string RequestType
        {
            get { return _RequestType; }
            set { _RequestType = value; }
        }

        private bool _TrainingCount;

        public bool TrainingCount
        {
            get { return _TrainingCount; }
            set { _TrainingCount = value; }
        }
        private int _vDocumentID;

        public int vDocumentID
        {
            get { return _vDocumentID; }
            set { _vDocumentID = value; }
        }
        private bool _IsTraining;

        public bool IsTraining
        {
            get { return _IsTraining; }
            set { _IsTraining = value; }
        }
    }
    public class RenewDocBO
    {
        private int _RowNumber;

        public int RowNumber
        {
            get { return _RowNumber; }
            set { _RowNumber = value; }
        }

        private int _VersionID;

        public int VersionID
        {
            get { return _VersionID; }
            set { _VersionID = value; }
        }

        private string _Department;

        public string Department
        {
            get { return _Department; }
            set { _Department = value; }
        }

        private string _DocumentName;

        public string DocumentName
        {
            get { return _DocumentName; }
            set { _DocumentName = value; }
        }

        private string _DocumentNumber;

        public string DocumentNumber
        {
            get { return _DocumentNumber; }
            set { _DocumentNumber = value; }
        }

        private string _ExpirationDate;

        public string ExpirationDate
        {
            get { return _ExpirationDate; }
            set { _ExpirationDate = value; }
        }
        private string _RequestType;

        public string RequestType
        {
            get { return _RequestType; }
            set { _RequestType = value; }
        }
        private int _PendingDocRequestCount;

        public int PendingDocRequestCount
        {
            get { return _PendingDocRequestCount; }
            set { _PendingDocRequestCount = value; }
        }
    }
    public class NewDocCreatorBO
    {
        private int _RowNumber;

        public int RowNumber
        {
            get { return _RowNumber; }
            set { _RowNumber = value; }
        }

        private int _VersionID;

        public int VersionID
        {
            get { return _VersionID; }
            set { _VersionID = value; }
        }

        private int _Id;

        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _Department;

        public string Department
        {
            get { return _Department; }
            set { _Department = value; }
        }

        private string _DocumentName;

        public string DocumentName
        {
            get { return _DocumentName; }
            set { _DocumentName = value; }
        }

        private string _DocumentNumber;

        public string DocumentNumber
        {
            get { return _DocumentNumber; }
            set { _DocumentNumber = value; }
        }

        private string _DocumentType;

        public string DocumentType
        {
            get { return _DocumentType; }
            set { _DocumentType = value; }
        }
        private string _Initiator;

        public string Initiator
        {
            get { return _Initiator; }
            set { _Initiator = value; }
        }
        private string _RequestType;

        public string RequestType
        {
            get { return _RequestType; }
            set { _RequestType = value; }
        }
        private string _Create;

        public string Create
        {
            get { return _Create; }
            set { _Create = value; }
        }
        private int _ProcessTypeID;

        public int ProcessTypeID
        {
            get { return _ProcessTypeID; }
            set { _ProcessTypeID = value; }
        }
    }
    public class PendingDocCreatorBO
    {
        private int _RowNumber;

        public int RowNumber
        {
            get { return _RowNumber; }
            set { _RowNumber = value; }
        }

        private int _VersionID;

        public int VersionID
        {
            get { return _VersionID; }
            set { _VersionID = value; }
        }

        private int _Id;

        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _Department;

        public string Department
        {
            get { return _Department; }
            set { _Department = value; }
        }

        private string _DocumentName;

        public string DocumentName
        {
            get { return _DocumentName; }
            set { _DocumentName = value; }
        }

        private string _DocumentNumber;

        public string DocumentNumber
        {
            get { return _DocumentNumber; }
            set { _DocumentNumber = value; }
        }

        private string _DocumentType;

        public string DocumentType
        {
            get { return _DocumentType; }
            set { _DocumentType = value; }
        }
        private string _Initiator;

        public string Initiator
        {
            get { return _Initiator; }
            set { _Initiator = value; }
        }
        private string _RequestType;

        public string RequestType
        {
            get { return _RequestType; }
            set { _RequestType = value; }
        }
        private int _ProcessTypeID;

        public int ProcessTypeID
        {
            get { return _ProcessTypeID; }
            set { _ProcessTypeID = value; }
        }
    }
    public class Int_EmployeeBy
    {
        private int _EmpID;

        public int EmpID
        {
            get { return _EmpID; }
            set { _EmpID = value; }
        }

        private int _DeptID;

        public int DeptID
        {
            get { return _DeptID; }
            set { _DeptID = value; }
        }

    }
    public class AdminDocListBO
    {
        private int _RowNumber;
        public string DocTitleLablName { get; set; }
        public int VersionCount { get; set; }
        public int RowNumber
        {
            get { return _RowNumber; }
            set { _RowNumber = value; }
        }

        private int _VersionID;

        public int VersionID
        {
            get { return _VersionID; }
            set { _VersionID = value; }
        }
        private int _vDocumentID;

        public int vDocumentID
        {
            get { return _vDocumentID; }
            set { _vDocumentID = value; }
        }
        private string _Department;

        public string Department
        {
            get { return _Department; }
            set { _Department = value; }
        }

        private string _DocumentName;

        public string DocumentName
        {
            get { return _DocumentName; }
            set { _DocumentName = value; }
        }

        private int _Version;

        public int Version
        {
            get { return _Version; }
            set { _Version = value; }
        }

        private string _DocumentNumber;

        public string DocumentNumber
        {
            get { return _DocumentNumber; }
            set { _DocumentNumber = value; }
        }
        private string _DocumentType;

        public string DocumentType
        {
            get { return _DocumentType; }
            set { _DocumentType = value; }
        }
        private String _RequestType;

        public String RequestType
        {
            get { return _RequestType; }
            set { _RequestType = value; }
        }
        private string _EffectiveDate;

        public string EffectiveDate
        {
            get { return _EffectiveDate; }
            set { _EffectiveDate = value; }
        }
        private string _ExpirationDate;

        public string ExpirationDate
        {
            get { return _ExpirationDate; }
            set { _ExpirationDate = value; }
        }
        private string _statusMsg;

        public string statusMsg
        {
            get { return _statusMsg; }
            set { _statusMsg = value; }
        }
        private int _expStatus;

        public int expStatus
        {
            get { return _expStatus; }
            set { _expStatus = value; }
        }
        private string _VersionNumber;

        public string VersionNumber
        {
            get { return _VersionNumber; }
            set { _VersionNumber = value; }
        }

        private string _DocLocationCount;

        public string DocLocationCount
        {
            get { return _DocLocationCount; }
            set { _DocLocationCount = value; }
        }

    }
}
