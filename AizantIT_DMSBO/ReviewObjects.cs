﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AizantIT_DMSBO
{
    public class ReviewObjects
    {
        private string _DocStatus;

        public string DocStatus
        {
            get { return _DocStatus; }
            set { _DocStatus = value; }
        }

        private int _Status;

        public int Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        private string _Process;

        public string Process
        {
            get { return _Process; }
            set { _Process = value; }
        }

        private string _Comments;

        public string Comments
        {
            get { return _Comments; }
            set { _Comments = value; }
        }

        private int _EmpID;

        public int EmpID
        {
            get { return _EmpID; }
            set { _EmpID = value; }
        }

        private string _TemplateName;

        public string TemplateName
        {
            get { return _TemplateName; }
            set { _TemplateName = value; }
        }

        private string _Ext;

        public string Ext
        {
            get { return _Ext; }
            set { _Ext = value; }
        }

        private byte[] _Content;

        public byte[] Content
        {
            get { return _Content; }
            set { _Content = value; }
        }

        private int _Creator;

        public int Creator
        {
            get { return _Creator; }
            set { _Creator = value; }
        }

        private int _Id;

        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _ReviewedTime;

        public string ReviewedTime
        {
            get { return _ReviewedTime; }
            set { _ReviewedTime = value; }
        }
    }
    public class DocPrintRequest
    {
        private int _RowNumber;

        public int RowNumber
        {
            get { return _RowNumber; }
            set { _RowNumber = value; }
        }

        private int _DocReqID;

        public int DocReqID
        {
            get { return _DocReqID; }
            set { _DocReqID = value; }
        }

        private string _RequestNumber;

        public string RequestNumber
        {
            get { return _RequestNumber; }
            set { _RequestNumber = value; }
        }

        private string _DocumentName;

        public string DocumentName
        {
            get { return _DocumentName; }
            set { _DocumentName = value; }
        }
        private string _DocumentVersionNumber;

        public string DocumentVersionNumber
        {
            get { return _DocumentVersionNumber; }
            set { _DocumentVersionNumber = value; }
        }

        private string _PrintPurpose;

        public string PrintPurpose
        {
            get { return _PrintPurpose; }
            set { _PrintPurpose = value; }
        }

        private string _PageNumbers;

        public string PageNumbers
        {
            get { return _PageNumbers; }
            set { _PageNumbers = value; }
        }

        private int _ReviewedByID;

        public int ReviewedByID
        {
            get { return _ReviewedByID; }
            set { _ReviewedByID = value; }
        }

        private int _DocControllerByID;

        public int DocControllerByID
        {
            get { return _DocControllerByID; }
            set { _DocControllerByID = value; }
        }
        private int _ManagedByID;

        public int ManagedByID
        {
            get { return _ManagedByID; }
            set { _ManagedByID = value; }
        }
        private string _Comments;

        public string Comments
        {
            get { return _Comments; }
            set { _Comments = value; }
        }

        private int _Validity;

        public int Validity
        {
            get { return _Validity; }
            set { _Validity = value; }
        }

        private int _ActionStatusID;

        public int ActionStatusID
        {
            get { return _ActionStatusID; }
            set { _ActionStatusID = value; }
        }
        private string _ActionName;

        public string ActionName
        {
            get { return _ActionName; }
            set { _ActionName = value; }
        }

        private int _DocVersionID;

        public int DocVersionID
        {
            get { return _DocVersionID; }
            set { _DocVersionID = value; }
        }

        private bool _IsSoftCopy;

        public bool IsSoftCopy
        {
            get { return _IsSoftCopy; }
            set { _IsSoftCopy = value; }
        }

        private int _RequestedByID;

        public int RequestedByID
        {
            get { return _RequestedByID; }
            set { _RequestedByID = value; }
        }
        private string _DocumentNumber;

        public string DocumentNumber
        {
            get { return _DocumentNumber; }
            set { _DocumentNumber = value; }
        }

        private string _DeptName;

        public string DeptName
        {
            get { return _DeptName; }
            set { _DeptName = value; }
        }
        private string _RequestBy;

        public string RequestBy
        {
            get { return _RequestBy; }
            set { _RequestBy = value; }
        }
        private string _ApprovedBy;

        public string ApprovedBy
        {
            get { return _ApprovedBy; }
            set { _ApprovedBy = value; }
        }
        private string _RequestedDate;

        public string RequestedDate
        {
            get { return _RequestedDate; }
            set { _RequestedDate = value; }
        }
        private int _Mode;

        public int Mode
        {
            get { return _Mode; }
            set { _Mode = value; }
        }
    }
    public class FormPrintRequest
    {
        private int _RowNumber;

        public int RowNumber
        {
            get { return _RowNumber; }
            set { _RowNumber = value; }
        }

        private int _FormReqID;

        public int FormReqID
        {
            get { return _FormReqID; }
            set { _FormReqID = value; }
        }

        private string _RequestNumber;

        public string RequestNumber
        {
            get { return _RequestNumber; }
            set { _RequestNumber = value; }
        }

        private string _FormName;

        public string FormName
        {
            get { return _FormName; }
            set { _FormName = value; }
        }

        private string _RequestPurpose;

        public string RequestPurpose
        {
            get { return _RequestPurpose; }
            set { _RequestPurpose = value; }
        }

        private int _NoofCopies;

        public int NoofCopies
        {
            get { return _NoofCopies; }
            set { _NoofCopies = value; }
        }

        private int _ReviewedByID;

        public int ReviewedByID
        {
            get { return _ReviewedByID; }
            set { _ReviewedByID = value; }
        }

        private string _Comments;

        public string Comments
        {
            get { return _Comments; }
            set { _Comments = value; }
        }

        private int _Validity;

        public int Validity
        {
            get { return _Validity; }
            set { _Validity = value; }
        }

        private int _ActionStatusID;

        public int ActionStatusID
        {
            get { return _ActionStatusID; }
            set { _ActionStatusID = value; }
        }

        private string _ActionName;

        public string ActionName
        {
            get { return _ActionName; }
            set { _ActionName = value; }
        }

        private int _FormVersionID;

        public int FormVersionID
        {
            get { return _FormVersionID; }
            set { _FormVersionID = value; }
        }
        private string _FormVersionNumber;

        public string FormVersionNumber
        {
            get { return _FormVersionNumber; }
            set { _FormVersionNumber = value; }
        }

        private DateTime? _FromDate;

        public DateTime? FromDate
        {
            get { return _FromDate; }
            set { _FromDate = value; }
        }

        private DateTime? _ToDate;

        public DateTime? ToDate
        {
            get { return _ToDate; }
            set { _ToDate = value; }
        }

        private string _FormNumber;

        public string FormNumber
        {
            get { return _FormNumber; }
            set { _FormNumber = value; }
        }

        private string _DeptName;

        public string DeptName
        {
            get { return _DeptName; }
            set { _DeptName = value; }
        }

        private string _CopyNumbers;

        public string CopyNumbers
        {
            get { return _CopyNumbers; }
            set { _CopyNumbers = value; }
        }
        private string _DocControllerID;

        public string DocControllerID
        {
            get { return _DocControllerID; }
            set { _DocControllerID = value; }
        }
        private string _RequestBy;

        public string RequestBy
        {
            get { return _RequestBy; }
            set { _RequestBy = value; }
        }
        private string _ApprovedBy;

        public string ApprovedBy
        {
            get { return _ApprovedBy; }
            set { _ApprovedBy = value; }
        }
        private string _RequestedDate;

        public string RequestedDate
        {
            get { return _RequestedDate; }
            set { _RequestedDate = value; }
        }
    }
    public class Printer_Names
    {
        public int printerID { get; set; }
        public string PrinterAreanName { get; set; }
        public string PrinterName { get; set; }
    }
}
