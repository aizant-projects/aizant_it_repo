﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_ML_BusinessObjects
{
    public class AizantIT_AuditObservation
    {
        public int ObservationID { get; set; }
        public string ObservationNum { get; set; }
        public string ObservationDescription { get; set; }
        public AizantIT_AuditObservation()
        {

        }
        //public AizantIT_AuditObservation(int _ObservationID,string _ObservationNum,string _ObservationDescription)
        //{
        //    ObservationID = _ObservationID;
        //    ObservationNum = _ObservationNum;
        //    ObservationDescription = _ObservationDescription;
        //}
    }
}
