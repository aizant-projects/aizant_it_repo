﻿using System;
using System.Collections.Generic;
using System.Data;
using TMS_BusinessObjects;
using TMS_DataLayer;
using UMS_BusinessLayer;
using Aizant_API_Entities;
using static Aizant_Enums.AizantEnums;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using API_BussinessObjects;
using TMS_BusinessObjects.jQueryParamsObject;
using System.Web;
using System.Globalization;
using TMS_BusinessObjects.ML_Objects;

namespace TMS_BusinessLayer
{
    public class TMS_BAL
    {
        TMS_DAL objDal;
        UMS_BAL objUMS_BAL = new UMS_BAL();

        //JR and Target Dashboard Chart
        public DataTable GetCompletedandPendingCountBal(int Year, int EmpID, int DeptID = 0)
        {
            objDal = new TMS_DAL();
            return objDal.GetCompletedandPendingCountDal(Year, EmpID, DeptID);
        }
        //ATC Dashboard chart
        public DataTable GetCompletedandPendingCountForATCBal(int Year, int EmpID, int DeptID = 0)
        {
            objDal = new TMS_DAL();
            return objDal.GetCompletedandPendingCountForATCDal(Year, EmpID, DeptID);
        }

        public DataTable GetOverDueCountBal(int EmpID, int DeptID = 0)
        {
            objDal = new TMS_DAL();
            return objDal.GetOverDueCountBalDal(EmpID, DeptID);
        }

        public DataTable GetTraineeOverDueCountBal(int EmpID, int DeptID)
        {
            objDal = new TMS_DAL();
            return objDal.GetTraineeOverDueCountDal(EmpID, DeptID);
        }
        public DataTable GetTraineePendingCountBal(int EmpID, int Year, int DeptID)
        {
            objDal = new TMS_DAL();
            return objDal.GetTraineePendingCountDal(EmpID, Year, DeptID);
        }
        public DataTable GetClassroomTrainingCountBal(int EmpID, string TargetType)
        {
            objDal = new TMS_DAL();
            return objDal.GetClassroomTrainingCountDal(EmpID, TargetType);
        }

        #region Jquery DataTable
        public DataTable GetJR_LIST(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, string sSearch_TraineeName, string sSearch_JR_Name, string sSearch_JR_Version,
          string sSearch_HOD_Name, string sSearch_DepartmentName, string sSearch_DesignationName, string sSearch_StatusName,
          int Year, string EmpID, int DeptID, int FilterType, string Status, out int ToTalCount)
        {
            objDal = new TMS_DAL();
            int IEmpID = Convert.ToInt32(EmpID);
            return objDal.GetJR_LISTDb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, sSearch_TraineeName, sSearch_JR_Name,
                sSearch_JR_Version, sSearch_HOD_Name, sSearch_DepartmentName, sSearch_DesignationName, sSearch_StatusName,
                Year, IEmpID, DeptID, FilterType, Status, out ToTalCount);
        }

        //JR Training List
        public DataTable GetJR_TrainingList(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, string EmpID, int FilterType, int NotificationID)
        {
            objDal = new TMS_DAL();
            int IEmpID = Convert.ToInt32(EmpID);
            return objDal.GetJR_TrainingListDb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IEmpID, FilterType, NotificationID);
        }

        //JR Training Regular
        public DataTable GetJR_TrainingRegular(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, string JR_ID)
        {
            objDal = new TMS_DAL();
            int IJR_ID = Convert.ToInt32(JR_ID);
            return objDal.GetJR_TrainingRegularDb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IJR_ID);
        }

        //JR Training Retrospective
        public DataTable GetJR_TrainingRetrospective(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, string JR_ID)
        {
            objDal = new TMS_DAL();
            int IJR_ID = Convert.ToInt32(JR_ID);
            return objDal.GetJR_TrainingRetrospectiveDb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IJR_ID);
        }

        public DataTable GetTQ_Docs(int displayLength, int displayStart, int sortCol, string sSortDir, string sSearch, int trainerEmpID)
        {
            objDal = new TMS_DAL();
            return objDal.GetTQ_DocsDb(displayLength, displayStart, sortCol, sSortDir, sSearch, trainerEmpID);
        }

        // To get Trainer Answers 
        public DataTable GetTA_Docs(int displayLength, int displayStart, int sortCol, string sSortDir, string sSearch, int traineeEmpID)
        {
            objDal = new TMS_DAL();
            return objDal.GetTA_DocsDb(displayLength, displayStart, sortCol, sSortDir, sSearch, traineeEmpID);
        }

        public DataTable GetHodJrEvaluationDetails(int displayLength, int displayStart, int sortCol, string sSortDir, string sSearch, string sSearch_JR_Name, string sSearch_JR_Version, string sSearch_TraineeName, string sSearch_TraineeDeptName, string sSearch_JR_AssignedBy, string sSearch_JR_AssignedDate, string hodEmpID, string NotificationID)
        {
            objDal = new TMS_DAL();
            int iHodEmpID = Convert.ToInt32(hodEmpID);
            int iNotificationID = Convert.ToInt32(NotificationID);
            return objDal.GetHodJrEvaluationDetailsDb(displayLength, displayStart, sortCol, sSortDir, sSearch, sSearch_JR_Name, sSearch_JR_Version, sSearch_TraineeName, sSearch_TraineeDeptName, sSearch_JR_AssignedBy, sSearch_JR_AssignedDate, iHodEmpID, iNotificationID);
        }

        //JR Training Doc Details
        public DataTable GetJR_TrainingDocDetails(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, string JR_ID, string Dept_ID, string TrainingOn)
        {
            objDal = new TMS_DAL();
            int IJR_ID = Convert.ToInt32(JR_ID);
            int IDept_ID = Convert.ToInt32(Dept_ID);

            return objDal.GetJR_TrainingDocDetailsDb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IJR_ID, IDept_ID, TrainingOn);
        }
        //JR Dept wise TrainerDetails
        public DataTable GetJR_DeptTrainerDetails(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, string JR_ID, string DeptID)
        {
            objDal = new TMS_DAL();
            int IJR_ID = Convert.ToInt32(JR_ID);
            int IDeptID = Convert.ToInt32(DeptID);

            return objDal.GetJR_DeptTrainerDetailsDb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IJR_ID, IDeptID);
        }

        //JR Pending List
        public DataTable GetJR_PendingList(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, string EmpID, int FilterType, int NotificationID)
        {
            objDal = new TMS_DAL();
            int IEmpID = Convert.ToInt32(EmpID);
            return objDal.GetJR_PendingListdb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IEmpID, FilterType, NotificationID);
        }

        public void EvalutedJrByHod(int jrID, int hodEmpID, out int evaluateResult)
        {
            objDal = new TMS_DAL();
            objDal.EvalutedJrByHodDb(jrID, hodEmpID, out evaluateResult);
        }
        //JR Review List
        public DataTable GetJR_ReviewList(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, string EmpID, int FilterType, int NotificationID)
        {
            objDal = new TMS_DAL();
            int IEmpID = Convert.ToInt32(EmpID);
            return objDal.GetJR_ReviewListdb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IEmpID, FilterType, NotificationID);
        }


        //JR TS Review List
        public DataTable GetJrTS_ReviewList(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, string EmpID, int FilterType, int NotificationID)
        {
            objDal = new TMS_DAL();
            int IEmpID = Convert.ToInt32(EmpID);
            return objDal.GetJrTS_ReviewListdb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IEmpID, FilterType, NotificationID);
        }


        //JR TS Pending List
        public DataTable GetJrTS_PendingList(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, string EmpID, int FilterType, int NotificationID)
        {
            objDal = new TMS_DAL();
            int IEmpID = Convert.ToInt32(EmpID);
            return objDal.GetJrTS_PendingListdb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IEmpID, FilterType, NotificationID);
        }

        public DataTable GetReadDurationBal(string tTS_ID, string empID)
        {
            try
            {
                objDal = new TMS_DAL();
                int ItTS_ID = Convert.ToInt32(tTS_ID);
                int IempID = Convert.ToInt32(empID);

                return objDal.GetReadDurationDal(ItTS_ID, IempID);
            }
            catch (Exception)
            {

                throw;
            }
        }
        //To get incident TTS_ID in General
        public DataTable GetIncidentInGeneralBal(int TTS_ID)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.GetIncidentInGeneralDal(TTS_ID);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public DataTable GetTrainingFilePath(int jRID)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.GetTrainingFileDAL(jRID);
            }
            catch (Exception)
            {

                throw;
            }
        }

        //JR Trainer evaluation
        public DataTable GetJR_TrainerEvaluationDetails(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, string sSearch_JR_Name, string sSearch_JR_Version, string sSearch_TraineeName, string sSearch_TraineeDeptName, string sSearch_JR_AssignedBy, string sSearch_JR_AssignedDate, string TrainerID)
        {
            objDal = new TMS_DAL();
            int ITrainerID = Convert.ToInt32(TrainerID);
            return objDal.GetJR_TrainerEvaluationDetailsdb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, sSearch_JR_Name, sSearch_JR_Version, sSearch_TraineeName, sSearch_TraineeDeptName, sSearch_JR_AssignedBy, sSearch_JR_AssignedDate, ITrainerID);
        }

        //JR Trained Docs
        public DataTable GetJR_TrainedDocs(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, string JR_ID, string TrainerID)
        {
            objDal = new TMS_DAL();
            int IJR_ID = Convert.ToInt32(JR_ID);
            int ITrainerID = Convert.ToInt32(TrainerID);
            return objDal.GetJR_TrainedDocsdb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IJR_ID, ITrainerID);
        }
        //JR Emp Trained Docs
        public DataTable GetJR_EmpTrainedDocs(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, string JR_ID)
        {
            objDal = new TMS_DAL();
            int IJR_ID = Convert.ToInt32(JR_ID);
            return objDal.GetJR_EmpTrainedDocsdb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IJR_ID);
        }

        //JR TrainerDetails
        public DataTable GetJR_TrainerDetails(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, string JR_ID, out int IsRetrospective)
        {
            objDal = new TMS_DAL();
            int IJR_ID = Convert.ToInt32(JR_ID);
            return objDal.GetJR_TrainerDetailsdb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IJR_ID, out IsRetrospective);
        }

        //JR TrainerDetails
        public DataTable GetJR_RetrospectiveDetails(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, string JR_ID)
        {
            objDal = new TMS_DAL();
            int IJR_ID = Convert.ToInt32(JR_ID);
            return objDal.GetJR_RetrospectiveDetailsdb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IJR_ID);
        }
        //JR Action History
        public DataTable GetJR_ActionHistory(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, string JR_ID)
        {
            objDal = new TMS_DAL();
            int IJR_ID = Convert.ToInt32(JR_ID);
            return objDal.GetJR_ActionHitorydb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IJR_ID);
        }


        //JR_TS Action History
        public DataTable GetJrTS_ActionHistory(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, string TS_ID)
        {
            objDal = new TMS_DAL();
            int ITS_ID = Convert.ToInt32(TS_ID);
            return objDal.GetJrTS_ActionHistorydb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, ITS_ID);
        }

        public void EvalutedJrByTrainer(int JR_ID, int DocID, int Trainer_EmpID, string Explained_Status, out int evaluateResult)
        {
            objDal = new TMS_DAL();
            objDal.SubmitTrainerEvaluationForJRDb(JR_ID, DocID, Trainer_EmpID, Explained_Status, out evaluateResult);
        }

        //TTS Action History
        public DataTable GetTTS_ActionHistory(int iDisplayLength, int iDisplayStart, int iSortCol_0,
         string sSortDir_0, string sSearch, string TTS_ID)
        {
            objDal = new TMS_DAL();
            int ITTS_ID = Convert.ToInt32(TTS_ID);
            return objDal.GetTTS_ActionHistorydb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, ITTS_ID);
        }

        public int InsertFeedbackForm(int EmpID, int BaseID, int BasedOn, DataTable dtFeedback, string FeedbackComments, out int Result)
        {
            objDal = new TMS_DAL();
            return objDal.InsertFeedbackFormdb(EmpID, BaseID, BasedOn, dtFeedback, FeedbackComments, out Result);
        }

        //JR Template List
        public DataTable GetJR_TemplateList(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, string EmpID)
        {
            objDal = new TMS_DAL();
            int IEmpID = Convert.ToInt32(EmpID);
            return objDal.GetJR_TemplateListdb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IEmpID);
        }

        public List<JR_TemplateListObjects> Get_JRTemplateList(BasicJQ_DT_BO objBasicJQ_DT_BO)
        {
            try
            {
                objDal = new TMS_DAL();
                var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
                List<JR_TemplateListObjects> list_JR_TemplateListObjects = new List<JR_TemplateListObjects>();
                DataTable dt = objDal.GetJR_TemplateListdb(objBasicJQ_DT_BO.DisplayLength,
                    objBasicJQ_DT_BO.DisplayStart, objBasicJQ_DT_BO.SortCol,
                    objBasicJQ_DT_BO.SortDir, objBasicJQ_DT_BO.Search,
                    Convert.ToInt32(EmpID));
                if (dt.Rows.Count > 0)
                {
                    objBasicJQ_DT_BO.TotalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    list_JR_TemplateListObjects.Add(
                       new JR_TemplateListObjects(Convert.ToInt32(dt.Rows[i]["JR_TemplateID"]), dt.Rows[i]["JR_TemplateName"].ToString(), Convert.ToInt32(dt.Rows[i]["DeptID"]),
                       dt.Rows[i]["DepartmentName"].ToString(), dt.Rows[i]["DeptCode"].ToString(), dt.Rows[i]["CreatedByName"].ToString(), dt.Rows[i]["CreatedDate"].ToString(),
                       dt.Rows[i]["ModifiedByName"].ToString(), dt.Rows[i]["ModifiedDate"].ToString()));
                }
                return list_JR_TemplateListObjects;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //Dynamic List for TMS
        public DataTable GetTMS_ActionHistory(int iDisplayLength, int iDisplayStart, int iSortCol_0,
         string sSortDir_0, string sSearch, string TableID, string BaseID)
        {
            objDal = new TMS_DAL();
            int ITable_ID = Convert.ToInt32(TableID);
            int IBaseID = Convert.ToInt32(BaseID);
            return objDal.GetTMS_ActionHistorydb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, ITable_ID, IBaseID);
        }

        //JR PendingList with WebApi
        public List<JR_PendingListObjects> Get_JRPendingList(BasicJQ_DT_BO objBasicJQ_DT_BO)
        {
            try
            {
                int FilterType;
                objDal = new TMS_DAL();
                List<JR_PendingListObjects> list_JR_PendingList = new List<JR_PendingListObjects>();

                var EmpID = HttpContext.Current.Request.Params["EmpID"].ToString(CultureInfo.CurrentCulture);
                var NotificationID = int.Parse(HttpContext.Current.Request.Params["NotificationID"]);
                if (NotificationID == 0)
                {
                    FilterType = int.Parse(HttpContext.Current.Request.Params["FilterType"]);
                }
                else
                {
                    FilterType = 1;
                }
                DataTable dt = objDal.GetJR_PendingListdb(objBasicJQ_DT_BO.DisplayLength,
                                objBasicJQ_DT_BO.DisplayStart, objBasicJQ_DT_BO.SortCol,
                                objBasicJQ_DT_BO.SortDir, objBasicJQ_DT_BO.Search,
                                Convert.ToInt32(EmpID), FilterType, NotificationID);
                if (dt.Rows.Count > 0)
                {
                    objBasicJQ_DT_BO.TotalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    list_JR_PendingList.Add(
                       new JR_PendingListObjects(Convert.ToInt32(dt.Rows[i]["JR_ID"]), dt.Rows[i]["JR_Name"].ToString(), dt.Rows[i]["JR_Version"].ToString(), dt.Rows[i]["EmpCode"].ToString(), dt.Rows[i]["EmpName"].ToString(), dt.Rows[i]["DeptCode"].ToString(), dt.Rows[i]["DesignationName"].ToString(),
                        dt.Rows[i]["HOD_AssignedDate"].ToString(), dt.Rows[i]["TraineeAcceptedDate"].ToString(),
                        dt.Rows[i]["DeclinedBy"].ToString(), dt.Rows[i]["QARevertedDate"].ToString(),
                        dt.Rows[i]["Reviewer"].ToString(), dt.Rows[i]["StatusName"].ToString(), Convert.ToInt32(dt.Rows[i]["StatusID"])));
                }
                return list_JR_PendingList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        public void PostTQ_ToFAQs(DataTable dtTqIDs, int TrainerEmpID, out int resultStatus)
        {
            objDal = new TMS_DAL();
            objDal.PostTQ_ToFAQsDb(dtTqIDs, TrainerEmpID, out resultStatus);
        }
        #region Admin Role
        public DataTable GetUnderQaApproveJRs(int displayLength, int displayStart, int sortCol, string sSortDir, string sSearch)
        {
            objDal = new TMS_DAL();
            return objDal.GetUnderQaApproveJRsDb(displayLength, displayStart, sortCol, sSortDir, sSearch);
        }
        public DataTable GetUnderQaApproveJrTSList(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch)
        {
            objDal = new TMS_DAL();
            return objDal.GetUnderQaApproveJrTSListDb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch);
        }
        public DataTable GetUnderQaApproveTargetTS_List(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch)
        {
            objDal = new TMS_DAL();
            return objDal.GetUnderQaApproveTargetTS_ListDb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch);
        }
        public void MarkCommentAsAnswer(string markedCommentID, string empID, string tQ_ID, out int resultStatus)
        {
            try
            {
                objDal = new TMS_DAL();
                objDal.MarkCommentAsAnswerDb(markedCommentID, empID, tQ_ID, out resultStatus);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable GetJR_Evaluator(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch)
        {
            objDal = new TMS_DAL();
            return objDal.GetJR_EvaluatorDb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch);
        }
        public int submitModifiedHODorQAforJR(int JR_ID, int ModifiedHOD, int ModifiedQA, int ModifiedBy, string Remarks, out int ResultStatus)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.submitModifiedHODorQAforJrDb(JR_ID, ModifiedHOD, ModifiedQA, ModifiedBy, Remarks, out ResultStatus);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int submitModifyHodEvaluator(int JR_ID, int ModifiedHOD, int ModifiedBy, string Remarks, out int ResultStatus)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.submitModifyHodEvaluatorDb(JR_ID, ModifiedHOD, ModifiedBy, Remarks, out ResultStatus);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void submitModifiedHODorQAforJRTS(string TS_ID, string AuthorID, string ReviewerID, int ModifiedBy, string Remarks, out int result)
        {
            try
            {
                objDal = new TMS_DAL();
                int iTS_ID = Convert.ToInt32(TS_ID);
                int iAuthorID = Convert.ToInt32(AuthorID);
                int iReviewerID = Convert.ToInt32(ReviewerID);
                objDal.submitModifiedHODorQAforJRTSDb(iTS_ID, iAuthorID, iReviewerID, ModifiedBy, Remarks, out result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void submitModifiedHODorQAforTargetTS(int TargetTS_ID, string AuthorID, string ReviewerID, int ModifiedBy, string Remarks, out int result)
        {
            try
            {
                objDal = new TMS_DAL();
                int iAuthorID = Convert.ToInt32(AuthorID);
                int iReviewerID = Convert.ToInt32(ReviewerID);
                objDal.submitModifiedHODorQAforTargetTSDb(TargetTS_ID, iAuthorID, iReviewerID, ModifiedBy, Remarks, out result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region FAQ Questions
        public void SubmitAnswertoQuestionbyTrainer(string QuestionID, string empID, string Answer, out int result)
        {
            try
            {
                objDal = new TMS_DAL();
                int iTrainerID = Convert.ToInt32(empID);
                int iQuestionID = Convert.ToInt32(QuestionID);
                objDal.SubmitAnswertoQuestionbyTrainerDal(iTrainerID, iQuestionID, Answer, out result);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public void SubmitCommentToTQ(string TQ_ID, string empID, string roleID, string Comment, out int result)
        {
            try
            {
                objDal = new TMS_DAL();
                int iTQ_ID = Convert.ToInt32(TQ_ID);
                int iEmpID = Convert.ToInt32(empID);
                int iRoleID = Convert.ToInt32(roleID);
                objDal.SubmitCommentToTQDb(iTQ_ID, iEmpID, iRoleID, Comment, out result);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public DataSet getTraineeRaisedQuestionstoTrainerBal(string TrainerID, string docID)
        {
            try
            {
                objDal = new TMS_DAL();
                int iTrainerID = Convert.ToInt32(TrainerID);
                int idocID = Convert.ToInt32(docID);
                return objDal.getTraineeRaisedQuestionstoTrainerDal(iTrainerID, idocID);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public DataSet getTraineeRaisedQuestionstoTraineeBal(string baseType, string baseID, string docID, string traineeID)
        {
            try
            {
                objDal = new TMS_DAL();
                int ibaseType = Convert.ToInt32(baseType);
                int ibaseID = Convert.ToInt32(baseID);
                int idocID = Convert.ToInt32(docID);
                int itraineeID = Convert.ToInt32(traineeID);
                return objDal.getTraineeRaisedQuestionstoTraineeDal(ibaseType, ibaseID, idocID, itraineeID);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        //To get Trainee Asked Questions to Trainee 
        public DataSet GetTraineeaskedQuestionstoTraineeBal(string DocID, string TraineeID)
        {
            try
            {
                objDal = new TMS_DAL();
                int idocID = Convert.ToInt32(DocID);
                int itraineeID = Convert.ToInt32(TraineeID);
                return objDal.GetTraineeaskedQuestionstoTraineeBalDal(idocID, itraineeID);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public DataTable getLatestAnswertoTQ(string questionID)
        {
            try
            {
                objDal = new TMS_DAL();
                int iquestionID = Convert.ToInt32(questionID);
                return objDal.getLatestAnswertoTQDb(iquestionID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable getDocFAQsAnswer(string questionID)
        {
            try
            {
                objDal = new TMS_DAL();
                int iquestionID = Convert.ToInt32(questionID);
                return objDal.getDocFAQsAnswerDb(iquestionID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable getDocFAQs(string PageIndex, string PageSize, string DocID, String sSearch, out int RecordCount)
        {
            try
            {
                objDal = new TMS_DAL();
                int iPageIndex = Convert.ToInt32(PageIndex);
                int iPageSize = Convert.ToInt32(PageSize);
                int iDocID = Convert.ToInt32(DocID);
                return objDal.getDocFAQsDb(iPageIndex, iPageSize, iDocID, sSearch, out RecordCount);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable GetDocFAQs_List(int displayLength, int displayStart, int sortCol, string sSortDir, string sSearch, int docID)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.GetDocFAQs_ListDb(displayLength, displayStart, sortCol, sSortDir, sSearch, docID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void ModifyDOCFAQ(string DocFAQQ_ID, string Question, string Answer, string EmpID, string questionStatus, string modifiedReason, out int result)
        {
            try
            {
                objDal = new TMS_DAL();
                int iDocFAQ_ID = Convert.ToInt32(DocFAQQ_ID);
                int iEmpID = Convert.ToInt32(EmpID);
                int iQuestionStatus = Convert.ToInt32(questionStatus);
                objDal.ModifyDOCFAQDb(iDocFAQ_ID, Question, Answer, iEmpID, iQuestionStatus, modifiedReason, out result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void CreateDocFAQ(string Doc_ID, string Question, string Answer, string EmpID, out int result)
        {
            try
            {
                objDal = new TMS_DAL();
                int iDoc_ID = Convert.ToInt32(Doc_ID);
                int iEmpID = Convert.ToInt32(EmpID);
                objDal.CreteDocFAQDb(iDoc_ID, Question, Answer, iEmpID, out result);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable GetDocFaqQuestionDetails(string DocQueID)
        {
            try
            {
                objDal = new TMS_DAL();
                int iDocQueID = Convert.ToInt32(DocQueID);
                return objDal.GetDocFaqQuestionDetailsDb(iDocQueID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable GetDocFAQ_History_List(int displayLength, int displayStart, int sortCol, string sSortDir, string sSearch, int docFaqID
            // , out int totalRecordCount
            )
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.GetDocFAQ_History_ListDb(displayLength, displayStart, sortCol, sSortDir, sSearch, docFaqID
                    // , out totalRecordCount
                    );
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        #endregion

        public DataTable TTS_CreationListBal(int displayLength, int displayStart, int sortCol, string sSortDir, string sSearch,
           int EmpID, int FilterType)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.TTS_CreationListBalDB(displayLength, displayStart, sortCol, sSortDir, sSearch,
                    EmpID, FilterType);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetTraineesOnEvaluation(int displayLength, int displayStart, int sortCol, string sSortDir, string sSearch,
            int tTS_TrainingSessionID, int evaluateOn)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.GetTraineesOnEvaluationDB(displayLength, displayStart, sortCol, sSortDir, sSearch,
                    tTS_TrainingSessionID, evaluateOn);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //WebApi Function for GetTraineesOnEvaluation
        public List<TrainingSessionEvaluation> GetTargetTrainerEvaluationList(TrainerEvaluationListObject objTMS_DT_BO)
        {
            try
            {
                objDal = new TMS_DAL();
                List<TrainingSessionEvaluation> objTraineeList = new List<TrainingSessionEvaluation>();
                DataTable dt = objDal.GetTraineesOnEvaluationDB(objTMS_DT_BO.DisplayLength, objTMS_DT_BO.DisplayStart, objTMS_DT_BO.SortCol, objTMS_DT_BO.SortDir, objTMS_DT_BO.Search, objTMS_DT_BO.TTS_TrainingSessionID, objTMS_DT_BO.EvaluateOn);
                if (dt.Rows.Count > 0)
                {
                    objTMS_DT_BO.TotalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objTraineeList.Add(
                       new TrainingSessionEvaluation(Convert.ToInt32(dt.Rows[i]["TTS_TrainingSessionID"]),
                       Convert.ToInt32(dt.Rows[i]["TraineeID"]), Convert.ToInt32(dt.Rows[i]["EvaluationStatus"]),
                       Convert.ToInt32(dt.Rows[i]["TargetExamID"]), dt.Rows[i]["EmpCode"].ToString(),
                       dt.Rows[i]["Trainee"].ToString(), dt.Rows[i]["DepartmentName"].ToString(), Convert.ToInt32(dt.Rows[i]["Attempts"])));
                }
                return objTraineeList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #region Training Performance
        public List<TrainingPerformanceObjects> GetTrainingPerformanceBal(TrainingPerformanceListObjects objTMS_DT_BO)
        {
            try
            {
                objDal = new TMS_DAL();
                List<TrainingPerformanceObjects> objTrainingPerformanceList = new List<TrainingPerformanceObjects>();
                DataTable dt = objDal.GetTrainingPerformanceDal(objTMS_DT_BO.DisplayLength, objTMS_DT_BO.DisplayStart, objTMS_DT_BO.SortCol, objTMS_DT_BO.SortDir, objTMS_DT_BO.Search, objTMS_DT_BO.EmpID,objTMS_DT_BO.DeptID);
                if (dt.Rows.Count > 0)
                {
                    objTMS_DT_BO.TotalRecordCount = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objTrainingPerformanceList.Add(
                       new TrainingPerformanceObjects(dt.Rows[i]["EmpName"].ToString(),
                       dt.Rows[i]["Avg_Duration"].ToString(), Convert.ToInt32(dt.Rows[i]["Avg_Marks"]),
                       Convert.ToInt32(dt.Rows[i]["No_of_times_Failed"]), Convert.ToInt32(dt.Rows[i]["No_of_Documents"]),
                       Convert.ToInt32(dt.Rows[i]["Overall_Performance"].ToString())));
                }
                return objTrainingPerformanceList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<TraineePerformanceObjects> GetTraineePerformanceBal(int EmpID)
        {
            try
            {
                objDal = new TMS_DAL();
                List<TraineePerformanceObjects> objTraineePerformanceList = new List<TraineePerformanceObjects>();
                DataTable dt = objDal.GetTraineePerformanceDal(EmpID);
                
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objTraineePerformanceList.Add(
                       new TraineePerformanceObjects(dt.Rows[i]["EmpName"].ToString(),
                       dt.Rows[i]["Avg_Duration"].ToString(), Convert.ToInt32(dt.Rows[i]["Avg_Marks"]),
                       Convert.ToInt32(dt.Rows[i]["No_of_times_Failed"]), Convert.ToInt32(dt.Rows[i]["No_of_Documents"]),
                       Convert.ToInt32(dt.Rows[i]["Overall_Performance"].ToString())));
                }
                return objTraineePerformanceList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion


        public DataTable GetTrainingSessionDetails(int displayLength, int displayStart, int sortCol, string sSortDir, string sSearch,
           int tTS_TrainingSessionID, string ExamType, int TTS_ID)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.GetTrainingSessionDetailsDB(displayLength, displayStart, sortCol, sSortDir, sSearch,
                    tTS_TrainingSessionID, ExamType, TTS_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable getJR_AssignedSops(string deptID, string JRID)
        {
            try
            {
                objDal = new TMS_DAL();
                int ideptID = Convert.ToInt32(deptID);
                int iJRID = Convert.ToInt32(JRID);
                return objDal.getJR_AssignedSopsDb(ideptID, iJRID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable getTS_AuditDetails(int JR_ID)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.getTS_AuditDetailsDb(JR_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region DropDownsbinding
        public DataTable getEmpbyDeptIdandRoleID(string deptID, string RoleID)
        {
            try
            {
                objDal = new TMS_DAL();
                int ideptID = Convert.ToInt32(deptID);
                int iRoleID = Convert.ToInt32(RoleID);
                return objDal.getEmpbyDeptIdandRoleIDDb(ideptID, iRoleID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet getAllEmpbyDeptIdandRoleID(int deptID, int empBindStatus)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.getAllEmpbyDeptIdandRoleIDdb(deptID, empBindStatus);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable BindOverallDepartsDeptBAL(int EmpId)
        {
            try
            {
                objUMS_BAL = new UMS_BAL();
                int[] RoleIDs = new int[] { (int)TMS_UserRole.HOD_TMS, (int)TMS_UserRole.Trainer_TMS, (int)TMS_UserRole.QA_TMS };
                return objUMS_BAL.getRoleWiseDepts(EmpId, RoleIDs);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        // To Update JR Training Status
        public void UpdateTrainingDeptStatus(int DocID, int DocVersionID, int JrID)
        {
            try
            {
                objDal = new TMS_DAL();
                objDal.UpdateTrainingDeptStatusDAL(DocID, DocVersionID, JrID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //public void UpdateTrainingStatus1(int DocID, int DocVersionID, int JrID)
        //{
        //    try
        //    {
        //        AizantIT_DevEntities dbAizantIT_TMS = new AizantIT_DevEntities();
        //        var DeptId = (from item in dbAizantIT_TMS.AizantIT_DocumentMaster
        //                                                     where item.DocumentID == DocID
        //                                                     select new { item.DeptID}).SingleOrDefault();

        //        var Status = (from item in dbAizantIT_TMS.AizantIT_JR_DeptDuration
        //                      where item.DeptID == DeptId && item.JR_ID== JrID
        //                      select new { item.Status }).SingleOrDefault();
        //        if (Status == 1)
        //        {
        //            (from p in dbAizantIT_TMS.AizantIT_JR_DeptDuration
        //             where p.JR_ID == JrID && p.DeptID == DeptId
        //             select p).ToList().ForEach(x => x.status = 2);

        //            dbAizantIT_TMS.SaveChanges();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}

        // For Modifying all Employees

        #endregion

        public void EvaluateTargetTrainee(int trainingSessionID, int traineeID, int trainerID, out int ActionStatus,
            int Opinion = 1, int ExplanationStatus = 0, string Remarks = "")
        {
            try
            {
                objDal = new TMS_DAL();
                objDal.EvaluateTargetTraineeDB(trainingSessionID, traineeID, trainerID, out ActionStatus, Opinion, ExplanationStatus, Remarks);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void EvaluateJrTrainee(int DocID, int JR_ID, int TrainerID, out int EvaluateResult,
            string Opinion, string Remarks = "")
        {
            try
            {
                objDal = new TMS_DAL();
                objDal.EvaluateJrTraineeDal(DocID, JR_ID, TrainerID, out EvaluateResult, Opinion, Remarks);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetTQ_PreviousAnswers(int TQ_ID)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.GetTQ_PreviousAnswersDB(TQ_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetTQ_Conversation(string TQ_ID)
        {
            try
            {
                objDal = new TMS_DAL();
                int ITQ_ID = Convert.ToInt32(TQ_ID);
                return objDal.GetTQ_ConversationDb(ITQ_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        #region Crystal Report
        public DataSet GetDatatoCR(string jRID)
        {
            try
            {
                objDal = new TMS_DAL();
                int iJR_ID = Convert.ToInt32(jRID);
                return objDal.GetDatatoCRDb(iJR_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetAllDocuments(string deptID, string documentType)
        {
            try
            {
                objDal = new TMS_DAL();
                int ideptID = Convert.ToInt32(deptID);
                int idocumentType = Convert.ToInt32(documentType);
                return objDal.GetAllDocumentsDb(ideptID, idocumentType);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public DataSet getAccessibleAndToDoActionDepts(string empID, string Type)
        {
            try
            {
                objDal = new TMS_DAL();
                int iempID = Convert.ToInt32(empID);
                return objDal.getAccessibleAndToDoActionDeptsDb(iempID, Type);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        #endregion

        #region JR Training
        public DataTable getTrainingFileDataToView(string jR_ID)
        {
            try
            {
                objDal = new TMS_DAL();
                int iJR_ID = Convert.ToInt32(jR_ID);
                return objDal.getTrainingFileDataToViewDb(iJR_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public DataTable getEmployeeCvOrAc(int EmpID, int docType)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.getEmployeeCvOrAcDb(EmpID, docType);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet GetJrTrainingDetails(int jRID)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.GetJrTrainingDetailsDb(jRID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Employee JR
        public DataSet getHOD_ListAndQA_ListAndJR_ListByDeptID(string accessibleDeptID)
        {
            try
            {
                objDal = new TMS_DAL();
                int iDeptID = Convert.ToInt32(accessibleDeptID);
                return objDal.getHOD_ListAndQA_ListAndJR_ListByDeptIDdb(iDeptID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable getJrAssignedEmployeeList(string DeptID)
        {
            try
            {
                objDal = new TMS_DAL();
                int iDeptID = Convert.ToInt32(DeptID);
                return objDal.getJrAssignedEmployees(iDeptID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void JR_TemplateOperations(string TemplateName, string Description, string DeptID,
           string CreateOrModifiedBy, string Operation, out int Status, string JrID = "")
        {
            try
            {
                objDal = new TMS_DAL();
                int iDeptID = Convert.ToInt32(DeptID);
                int iCreateOrModifiedBy = Convert.ToInt32(CreateOrModifiedBy);
                if (Operation == "Insert")
                {
                    objDal.JR_TemplateOperationsDb(TemplateName, Description, iDeptID, iCreateOrModifiedBy, Operation, out Status);
                }
                else
                {
                    int iJrID = Convert.ToInt32(JrID);
                    objDal.JR_TemplateOperationsDb(TemplateName, Description, iDeptID, iCreateOrModifiedBy, Operation, out Status, iJrID);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable JR_TemplateList(string DeptID, string EmpID = "")
        {
            try
            {
                objDal = new TMS_DAL();
                int iDeptID = Convert.ToInt32(DeptID);
                if (EmpID != "")
                {
                    int iEmpID = Convert.ToInt32(EmpID);
                    return objDal.getJR_TemplateListDb(iDeptID, iEmpID);
                }
                else
                {
                    return objDal.getJR_TemplateListDb(iDeptID);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet GetEmployeeJR(string JrID)
        {
            try
            {
                objDal = new TMS_DAL();
                int iJrID = Convert.ToInt32(JrID);
                return objDal.GetEmployeeJrDb(iJrID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string getJR_Description(string JrID)
        {
            try
            {
                objDal = new TMS_DAL();
                int iJrID = Convert.ToInt32(JrID);
                return objDal.getJR_DescriptionDb(iJrID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //public DataTable getSOPsForRefresher(int deptID, int refresherMonth,int CalendarYear)
        //{
        //    try
        //    {
        //        objDal = new TMS_DAL();
        //        return objDal.getSOPsForRefresherDB(deptID, refresherMonth, CalendarYear);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}

        public DataSet getTsForModification(int TS_ID)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.getTsForModificationDb(TS_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void EmpJR_Operations(string Operation, EmpJrObjects objEmpJr, out int ResultStatus, string ModifiedBy = "")
        {
            try
            {
                objDal = new TMS_DAL();
                objDal.EmpJR_OperationsDb(Operation, objEmpJr, out ResultStatus, ModifiedBy);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable GetJRsToAccept(int empID, int status)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.GetJRsToAcceptDb(empID, status);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int AcceptMyJR(int JRID, int EmpID, int Status, out int ResultStatus)
        {
            try
            {
                objDal = new TMS_DAL();
                int count = objDal.AcceptMyJRDb(JRID, EmpID, Status, out ResultStatus);
                return count;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Time
        public int ConfirmOralExamSopRead(string JR_ID, string SopID, string examTypeConfirm, out int Result)
        {
            try
            {
                objDal = new TMS_DAL();
                int iJR_ID = Convert.ToInt32(JR_ID);
                int isopID = Convert.ToInt32(SopID);
                int iexamTypeConfirm = Convert.ToInt32(examTypeConfirm);
                return objDal.ConfirmOralExamSopReadDb(iJR_ID, isopID, iexamTypeConfirm, out Result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet getExamResult(int BaseID, int BaseType, int DocID = 0)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.getExamResultDataDb(BaseID, BaseType, DocID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SubmitExamResult(string BaseID, bool passedOutStatus, int marks, DataTable dtTraineeAnswerSheet,
            string ExplainedStatus, string BasedOn, out int Result, out int Attempts, string DocID = "0", string TraineeID = "0")
        {
            try
            {
                Result = 0;
                Attempts = 1;
                objDal = new TMS_DAL();
                if (BasedOn == "1") // JR Training
                {
                    int iJR_ID = Convert.ToInt32(BaseID);
                    int iDocID = Convert.ToInt32(DocID);
                    objDal.SubmitJRExamResultDb(iJR_ID, iDocID, passedOutStatus, marks, dtTraineeAnswerSheet, ExplainedStatus, out Result, out Attempts);
                }
                else if (BasedOn == "2") // Target Training
                {
                    // Target Exam Submission here.
                    int iTTS_SessionID = Convert.ToInt32(BaseID);
                    int iTraineeID = Convert.ToInt32(TraineeID);
                    int iExplainedStatus = 0;
                    if (ExplainedStatus == "Y")
                        iExplainedStatus = 1;
                    else if (ExplainedStatus == "N")
                        iExplainedStatus = 2;
                    else
                        iExplainedStatus = 0;
                    objDal.SubmitTargetExamResultDb(iTTS_SessionID, iTraineeID, passedOutStatus, marks, dtTraineeAnswerSheet, iExplainedStatus, out Result, out Attempts);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet getSopQuestionnaireForExam(string sopID, out int ExamType)
        {
            try
            {
                objDal = new TMS_DAL();
                int isopID = Convert.ToInt32(sopID);
                return objDal.getSopQuestionnaireForExamDb(isopID, out ExamType);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void GetTTS_StatusClose(string BaseID, out int Result)
        {
            try
            {
                objDal = new TMS_DAL();
                objDal.GetTTS_StatusCloseDAL(BaseID, out Result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void ToGetTTS_StatusClose(string TTSID, out int Result)
        {
            try
            {
                objDal = new TMS_DAL();
                objDal.ToGetTTS_StatusCloseDAL(TTSID, out Result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable getFeedbackQuestionListBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int FeedbackID)
        {
            objDal = new TMS_DAL();
            return objDal.getFeedbackQuestionListDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, FeedbackID);
        }
        public DataTable getFeedbackHistoryListBal(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int FeedbackID)
        {
            objDal = new TMS_DAL();
            return objDal.getFeedbackHistoryListDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, FeedbackID);
        }
        public DataTable FeedbackForCreation(int FeedbackType, out int FeedbackStatus)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.FeedbackForCreationDb(FeedbackType, out FeedbackStatus);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet ViewEmployeeFeedback(int EmpFeedbackID, int FeedbackType)
        {
            try
            {
                Dictionary<int, string> ratingDictionary = new Dictionary<int, string>();
                ratingDictionary.Add(1, "<span class='fa fa-star checked'></span><span class='fa fa-star'></span><span class='fa fa-star'></span><span class='fa fa-star'></span><span class='fa fa-star'></span>");
                ratingDictionary.Add(2, "<span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star'></span><span class='fa fa-star'></span><span class='fa fa-star'></span>");
                ratingDictionary.Add(3, "<span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star'></span><span class='fa fa-star'></span>");
                ratingDictionary.Add(4, "<span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star'></span>");
                ratingDictionary.Add(5, "<span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star checked'></span>");

                objDal = new TMS_DAL();
                DataSet ds = new DataSet();
                ds = objDal.ViewEmployeeFeedbackDb(EmpFeedbackID, FeedbackType);
                DataTable dt = ds.Tables[1];
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["Rating"].ToString() == "1")
                    {
                        dr["Rating"] = ratingDictionary[1];
                    }
                    if (dr["Rating"].ToString() == "2")
                    {
                        dr["Rating"] = ratingDictionary[2];
                    }
                    if (dr["Rating"].ToString() == "3")
                    {
                        dr["Rating"] = ratingDictionary[3];
                    }
                    if (dr["Rating"].ToString() == "4")
                    {
                        dr["Rating"] = ratingDictionary[4];
                    }
                    if (dr["Rating"].ToString() == "5")
                    {
                        dr["Rating"] = ratingDictionary[5];
                    }
                }
                dt.AcceptChanges();
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ViewRemarks(int SessionID)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.ViewRemarksdb(SessionID);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public DataSet getDocInfo(string jR_ID, int docID, int docVersionID, out int result)
        {
            try
            {
                objDal = new TMS_DAL();
                int ijR_ID = Convert.ToInt32(jR_ID);
                return objDal.getDocInfoDb(ijR_ID, docID, docVersionID, out result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void UpdateDocDuration(string JR_ID, string sopVersion, string SopID, string Hours, string Minutes, string Seconds)
        {
            try
            {
                objDal = new TMS_DAL();
                int isopVersion = Convert.ToInt32(sopVersion);
                int iJR_ID = Convert.ToInt32(JR_ID);
                int iSopID = Convert.ToInt32(SopID);
                int iHours = Convert.ToInt32(Hours);
                int iMinutes = Convert.ToInt32(Minutes);
                int iSeconds = Convert.ToInt32(Seconds);
                objDal.UpdateDocDurationDb(iJR_ID, isopVersion, iSopID, iHours, iMinutes, iSeconds);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //Update Trainee Doc Read Duration
        public void UpdateTraineeDocDuration(string TTS_ID, string TraineeIDs, string Hours, string Minutes, string Seconds)
        {
            try
            {
                objDal = new TMS_DAL();
                int iTTS_ID = Convert.ToInt32(TTS_ID);
                int iTraineeIDs = Convert.ToInt32(TraineeIDs);
                int iHours = Convert.ToInt32(Hours);
                int iMinutes = Convert.ToInt32(Minutes);
                int iSeconds = Convert.ToInt32(Seconds);
                objDal.UpdateTraineeDocDurationDb(iTTS_ID, iTraineeIDs, iHours, iMinutes, iSeconds);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable getDocDataToView(string DocID, string DocVersion, string BasedOn, string BaseID)
        {
            try
            {
                objDal = new TMS_DAL();
                int iDocID = Convert.ToInt32(DocID);
                int iDocVersion = Convert.ToInt32(DocVersion);
                int iBasedOn = Convert.ToInt32(BasedOn);
                int iBaseID = Convert.ToInt32(BaseID);
                return objDal.getDocDataToViewDb(iDocID, iDocVersion, iBasedOn, iBaseID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ActiveDocumentContent(string DocID)
        {
            try
            {
                objDal = new TMS_DAL();
                int iDocID = Convert.ToInt32(DocID);
                return objDal.ActiveDocumentContentDb(iDocID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Training Schedule
        public DataSet getViewTS(int jR_ID)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.getViewTDb(jR_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SubmitTS_Approval(int empid, int jR_ID, string status, string comments, out int result)
        {
            try
            {
                objDal = new TMS_DAL();
                int iStatus = Convert.ToInt32(status);
                objDal.SubmitTS_ApprovalDb(empid, jR_ID, iStatus, comments, out result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void CreateTS(DataSet ds, int JR_ID, int EmpID, string Reviewer, string HOD_Remarks, out int result)
        {
            try
            {
                objDal = new TMS_DAL();
                int iReviewer = Convert.ToInt32(Reviewer);
                objDal.CreateTsDb(ds, JR_ID, EmpID, iReviewer, HOD_Remarks, out result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void ModifyTS(DataSet ds, int JR_ID, int EmpID, string HOD_Remarks, out int result)
        {
            try
            {
                objDal = new TMS_DAL();
                objDal.ModifyTsDb(ds, JR_ID, EmpID, HOD_Remarks, out result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetDeptWiseSOPs(string DeptID, DataTable dtExists = null)
        {
            try
            {
                objDal = new TMS_DAL();
                int iDeptID = Convert.ToInt32(DeptID);
                DataTable dtTemp = objDal.GetDeptWiseSOPsDb(iDeptID);
                DataColumn dcSopStatus = new DataColumn("Status");
                dtTemp.Columns.Add(dcSopStatus);
                foreach (DataRow dr in dtTemp.Rows)
                {
                    dr["Status"] = "Y";
                }
                if (dtExists != null)
                {
                    DataView dvDeptSOPs = new DataView(dtExists);
                    dvDeptSOPs.RowFilter = "DeptID=" + DeptID;
                    DataTable dtSOPsDeptWise = dvDeptSOPs.ToTable();
                    if (dtSOPsDeptWise.Rows.Count > 0)
                    {
                        foreach (DataRow drFromDb in dtTemp.Rows)
                        {
                            foreach (DataRow drLocal in dtSOPsDeptWise.Rows)
                            {
                                if (Convert.ToInt32(drFromDb["DocumentID"]) == Convert.ToInt32(drLocal["DocumentID"]))
                                {
                                    drFromDb["Status"] = drLocal["Status"];
                                }
                            }
                        }
                        for (int i = dtExists.Rows.Count; i > 0; i--)
                        {
                            DataRow drLocal = dtExists.Rows[i - 1];
                            if (drLocal["DeptID"].ToString() == DeptID)
                            {
                                dtExists.Rows.Remove(drLocal);
                                dtExists.AcceptChanges();
                            }
                        }
                    }
                    dtExists.Merge(dtTemp);
                    return dtExists;
                }
                else
                {
                    return dtTemp;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //public DataTable GetInitialize_ATC(int atcID, string department, string calendarYear, string author, string reviewer, string approver)
        //{
        //    objDal = new TMS_DAL();
        //    return objDal.GetInitialize_ATC(atcID, department, calendarYear, author, reviewer, approver);
        //}

        public DataTable GetDeptTrainers(string DeptID, DataTable dtExists = null)
        {
            try
            {
                objDal = new TMS_DAL();
                int iDeptID = Convert.ToInt32(DeptID);
                //DataTable dtTemp = objDal.GetDeptTrainersDb(iDeptID);
                DataTable dtTemp = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(iDeptID, 7, true);
                DataColumn dcStatus = new DataColumn("Status");
                dtTemp.Columns.Add(dcStatus);
                foreach (DataRow dr in dtTemp.Rows)
                {
                    dr["Status"] = "Y";
                }
                if (dtExists != null)
                {
                    DataView dvDeptTrainers = new DataView(dtExists);
                    dvDeptTrainers.RowFilter = "DeptID=" + DeptID;
                    DataTable dtTrainersDeptWise = dvDeptTrainers.ToTable();
                    if (dtTrainersDeptWise.Rows.Count > 0)
                    {
                        foreach (DataRow drFromDb in dtTemp.Rows)
                        {
                            foreach (DataRow drLocal in dtTrainersDeptWise.Rows)
                            {
                                if (Convert.ToInt32(drFromDb["EmpID"]) == Convert.ToInt32(drLocal["EmpID"]))
                                {
                                    drFromDb["Status"] = drLocal["Status"];
                                }
                            }
                        }
                        for (int i = dtExists.Rows.Count; i > 0; i--)
                        {
                            DataRow drLocal = dtExists.Rows[i - 1];
                            if (drLocal["DeptID"].ToString() == DeptID)
                            {
                                dtExists.Rows.Remove(drLocal);
                                dtExists.AcceptChanges();
                            }
                        }
                    }
                    dtExists.Merge(dtTemp);
                    return dtExists;
                }
                else
                {
                    return dtTemp;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        #endregion

        #region TMS_DashBoard
        public DataTable GetATC_DocumentDetails(string deptID, string month)
        {
            try
            {
                objDal = new TMS_DAL();
                int ideptID = Convert.ToInt32(deptID);
                int iMonth = Convert.ToInt32(month);
                return objDal.GetATC_DocumentDetailsDb(ideptID, iMonth);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetDocCountandMonth(string deptID)
        {
            try
            {
                objDal = new TMS_DAL();
                int ideptID = Convert.ToInt32(deptID);
                return objDal.GetDocCountandMonthDb(ideptID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet GetDataTODashBoard(int empID)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.GetDataTODashBoardDb(empID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region JR_Template
        public DataTable getTemplateDetails(string tempID)
        {
            try
            {
                objDal = new TMS_DAL();
                int itempID = Convert.ToInt32(tempID);
                return objDal.getTemplateDetailsDb(itempID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Questionnaire
        public DataTable GetActiveDocs(string DepID, string DocType)
        {
            try
            {
                objDal = new TMS_DAL();
                int iDepID = Convert.ToInt32(DepID);
                int iDoctype = Convert.ToInt32(DocType);
                return objDal.GetActiveDocsDb(iDepID, iDoctype);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void Questionnaire(QuestionnaireObjects objQUE, DataTable dtOptions, out int Result)
        {
            try
            {
                objDal = new TMS_DAL();
                objDal.QuestionnaireDb(objQUE, dtOptions, out Result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetDeptWiseDocsForQuestionnaireApprovalOrRevert(string deptID, string EmpID, String Type, string DocType)
        {
            try
            {
                objDal = new TMS_DAL();
                int ideptID = Convert.ToInt32(deptID);
                int iEmpID = Convert.ToInt32(EmpID);
                int iDocType = Convert.ToInt32(DocType);
                return objDal.GetDeptWiseDocsForQuestionnaireApprovalOrRevertDb(ideptID, iEmpID, Type, iDocType);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet gvDocQuestionnaire(string DocumentID, int EmpID, out int OnTraining)
        {
            try
            {
                objDal = new TMS_DAL();
                int iDocumentID = Convert.ToInt32(DocumentID);
                return objDal.gvDocQuestionnaireDb(iDocumentID, EmpID, out OnTraining);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet gvDocQuestionnaireApproval(string SopID, string Status, int EmpID)
        {
            try
            {
                objDal = new TMS_DAL();
                int iSopID = Convert.ToInt32(SopID);
                int iStatus = Convert.ToInt32(Status);
                return objDal.gvSopQuestionnaireApprovalDb(iSopID, iStatus, EmpID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet GetQuestionnairetoEdit(int queID)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.GetQuestionnairetoEditDb(queID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void DeleteQuestionnaire(int queID, out int DelStatus)
        {
            try
            {
                objDal = new TMS_DAL();
                objDal.DeleteQuestionnaireDb(queID, out DelStatus);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int InActiveQuestionnaire(string Sopid, int queID, int Status, int EmpID, out int Result)
        {
            try
            {
                objDal = new TMS_DAL();
                //int iStatus = Convert.ToInt32(Status);
                int SopID = Convert.ToInt32(Sopid);
                return objDal.InActiveQuestionnaireDb(SopID, queID, Status, EmpID, out Result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int RevertQuestionnaire(int queID, string RevertReason, int Empid, int Status, out int Result)
        {
            try
            {
                objDal = new TMS_DAL();
                //int iQueid = Convert.ToInt32(queID);
                //int iStatus = Convert.ToInt32(Status);
                return objDal.RevertQuestionnaireDb(queID, RevertReason, Empid, Status, out Result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int AcceptQuestionnaire(string SopID, int empid, string Status, DataTable dtQuestionIDs)
        {
            try
            {
                objDal = new TMS_DAL();
                int iSopid = Convert.ToInt32(SopID);
                int iStatus = Convert.ToInt32(Status);
                return objDal.AcceptQuestionnaireDb(iSopid, empid, iStatus, dtQuestionIDs);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet getModuleWiseDeptsAndDocTypes(int empID)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.getModuleWiseDeptsAndDocTypesDb(empID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        public DataTable DocumentType()
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.DocTypedb();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int RetJustificationNote_Insert(EmpJrObjects objEmpJrObjects, out int Status)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.RetJustificationNote_Insert(objEmpJrObjects, out Status);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetRetrospectiveNoteData(int jR_ID)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.GetRetrospectiveNoteData(jR_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region Target TS       
        public DataTable getEffectedDocsByTargetType(int deptID, int targetType, int DocumentType, int TTS_ID = 0)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.getEffectedDocsByTargetTypeDB(deptID, targetType, DocumentType, TTS_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable getDocsForRefresher(int deptID, int refresherMonth, int CalendarYear, int DocumentType)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.getDocsForRefresherDB(deptID, refresherMonth, CalendarYear, DocumentType);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable getEffectedDocumentsForGeneral(int DeptID, int DocType)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.getEffectedDocumentsForGeneralDB(DeptID, DocType);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable GetCreationYearForJRandTargetBAL()
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.GetCreationYearForJRandTargetDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable BindAssignedDeptBAL(int EmpID)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.BindAssignedDeptDAL(EmpID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetSelfCompleteTTSIDBAL(int TTSID)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.GetSelfCompleteTTSID_DAL(TTSID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet GetCreationYearForATCBAL()
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.GetCreationYearForATCDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetPreviousTraineesForRevisionDocs(string DeptID, string DocID)
        {
            try
            {
                objDal = new TMS_DAL();
                int iDeptID = Convert.ToInt32(DeptID);
                int iDocID = Convert.ToInt32(DocID);
                return objDal.GetPreviousTraineesForRevisionDocDb(iDeptID, iDocID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetDeptTrainees(string DeptID, DataTable dtExists = null)
        {
            try
            {
                objDal = new TMS_DAL();
                int iDeptID = Convert.ToInt32(DeptID);
                DataTable dtTemp = objUMS_BAL.GetEmployeesByDeptIDandRoleID_BAL(iDeptID, 6, true);
                //DataTable dtTemp = objDal.getEmpbyDefaultDeptIDandRoleIDdb(Convert.ToInt32(DeptID), 6);
                DataColumn dcStatus = new DataColumn("Status");
                dtTemp.Columns.Add(dcStatus);
                foreach (DataRow dr in dtTemp.Rows)
                {
                    dr["Status"] = "Y";
                }
                if (dtExists != null)
                {
                    DataView dvDeptTrainees = new DataView(dtExists);
                    if (DeptID != "0")
                    {
                        dvDeptTrainees.RowFilter = "DeptID=" + DeptID;
                    }
                    DataTable dtTraineesDeptWise = dvDeptTrainees.ToTable();
                    if (dtTraineesDeptWise.Rows.Count > 0)
                    {
                        foreach (DataRow drFromDb in dtTemp.Rows)
                        {
                            foreach (DataRow drLocal in dtTraineesDeptWise.Rows)
                            {
                                if (Convert.ToInt32(drFromDb["EmpID"]) == Convert.ToInt32(drLocal["EmpID"]))
                                {
                                    drFromDb["Status"] = drLocal["Status"];
                                }
                            }
                        }
                        for (int i = dtExists.Rows.Count; i > 0; i--)
                        {
                            DataRow drLocal = dtExists.Rows[i - 1];
                            if (drLocal["DeptID"].ToString() == DeptID)
                            {
                                dtExists.Rows.Remove(drLocal);
                                dtExists.AcceptChanges();
                            }
                        }
                    }
                    if (DeptID != "0")
                    {
                        dtExists.Merge(dtTemp);
                    }
                    return dtExists;
                }
                else
                {
                    return dtTemp;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetDeptTraineesForGeneral(string DeptID, string DocID, DataTable dtExists = null, int IsIncident = 0)
        {
            try
            {
                objDal = new TMS_DAL();
                int iDeptID = Convert.ToInt32(DeptID);
                int iDocID = Convert.ToInt32(DocID);
                DataTable dtTemp = objDal.getEmpbyDefaultDeptIDandRoleIDForGeneraldb(Convert.ToInt32(DeptID), iDocID, 6, 0, IsIncident);
                DataColumn dcStatus = new DataColumn("Status");
                dtTemp.Columns.Add(dcStatus);
                foreach (DataRow dr in dtTemp.Rows)
                {
                    dr["Status"] = "Y";
                }
                if (dtExists != null)
                {
                    DataView dvDeptTrainees = new DataView(dtExists);
                    if (DeptID != "0")
                    {
                        dvDeptTrainees.RowFilter = "DeptID=" + DeptID;
                    }
                    DataTable dtTraineesDeptWise = dvDeptTrainees.ToTable();
                    if (dtTraineesDeptWise.Rows.Count > 0)
                    {
                        foreach (DataRow drFromDb in dtTemp.Rows)
                        {
                            foreach (DataRow drLocal in dtTraineesDeptWise.Rows)
                            {
                                if (Convert.ToInt32(drFromDb["EmpID"]) == Convert.ToInt32(drLocal["EmpID"]))
                                {
                                    drFromDb["Status"] = drLocal["Status"];
                                }
                            }
                        }
                        for (int i = dtExists.Rows.Count; i > 0; i--)
                        {
                            DataRow drLocal = dtExists.Rows[i - 1];
                            if (drLocal["DeptID"].ToString() == DeptID)
                            {
                                dtExists.Rows.Remove(drLocal);
                                dtExists.AcceptChanges();
                            }
                        }
                    }
                    if (DeptID != "0")
                    {
                        dtExists.Merge(dtTemp);
                    }
                    return dtExists;
                }
                else
                {
                    return dtTemp;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable EditTraineesForGeneralTraining(string DeptID, string DocID, string TTSID, DataTable dtExists = null, int IncidentSP = 0)
        {
            try
            {
                objDal = new TMS_DAL();
                int iDeptID = Convert.ToInt32(DeptID);
                int iDocID = Convert.ToInt32(DocID);
                int iTTSID = Convert.ToInt32(TTSID);
                DataTable dtTemp = objDal.EditTraineesForGeneralTrainingdb(Convert.ToInt32(DeptID), iDocID, iTTSID, 6, 0, IncidentSP);
                DataColumn dcStatus = new DataColumn("Status");
                dtTemp.Columns.Add(dcStatus);
                foreach (DataRow dr in dtTemp.Rows)
                {
                    dr["Status"] = "Y";
                }
                if (dtExists != null)
                {
                    DataView dvDeptTrainees = new DataView(dtExists);
                    if (DeptID != "0")
                    {
                        dvDeptTrainees.RowFilter = "DeptID=" + DeptID;
                    }
                    DataTable dtTraineesDeptWise = dvDeptTrainees.ToTable();
                    if (dtTraineesDeptWise.Rows.Count > 0)
                    {
                        foreach (DataRow drFromDb in dtTemp.Rows)
                        {
                            foreach (DataRow drLocal in dtTraineesDeptWise.Rows)
                            {
                                if (Convert.ToInt32(drFromDb["EmpID"]) == Convert.ToInt32(drLocal["EmpID"]))
                                {
                                    drFromDb["Status"] = drLocal["Status"];
                                }
                            }
                        }
                        for (int i = dtExists.Rows.Count; i > 0; i--)
                        {
                            DataRow drLocal = dtExists.Rows[i - 1];
                            if (drLocal["DeptID"].ToString() == DeptID)
                            {
                                dtExists.Rows.Remove(drLocal);
                                dtExists.AcceptChanges();
                            }
                        }
                    }
                    if (DeptID != "0")
                    {
                        dtExists.Merge(dtTemp);
                    }
                    return dtExists;
                }
                else
                {
                    return dtTemp;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void UpdateSelfCompleteStatusBal(string TTS_ID, string TraineeID, int status)
        {
            try
            {
                objDal = new TMS_DAL();
                int iTTS_ID = Convert.ToInt32(TTS_ID);
                int iTraineeID = Convert.ToInt32(TraineeID);
                objDal.UpdateSelfCompleteStatusBalDal(iTTS_ID, iTraineeID, status);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void CreateTargetTraining(TTS_Objects objTTS, out int result)
        {
            try
            {
                objDal = new TMS_DAL();
                objDal.CreateTargetTrainingDb(objTTS, out result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void UpdateTargetTraining(TTS_Objects objTTS, out int result)
        {
            try
            {
                objDal = new TMS_DAL();
                objDal.UpdateTargetTrainingDb(objTTS, out result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet GetTargetTrainingDetails(string TTS_ID, int EmpID, out int CurrentHistoryStatus, out int TargetCurrentStatus)
        {
            try
            {
                objDal = new TMS_DAL();
                int iTTS_ID = Convert.ToInt32(TTS_ID);
                return objDal.GetTargetTrainingDetailsDb(iTTS_ID, EmpID, out CurrentHistoryStatus, out TargetCurrentStatus);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //for TTS QA Approval
        public void SubmitTTS_QA_ActionSubmit(string TTS_ID, string ApproverEmpID, string ActionStatus, out int ResultStatus, string Remarks = "")
        {
            try
            {
                objDal = new TMS_DAL();
                int iTTS_ID = Convert.ToInt32(TTS_ID);
                int iApproverEmpID = Convert.ToInt32(ApproverEmpID);
                int iActionStatus = Convert.ToInt32(ActionStatus);
                objDal.SubmitTTS_QA_ApprovalDb(iTTS_ID, iApproverEmpID, iActionStatus, out ResultStatus, Remarks);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //Create Target Sessions Suresh
        public void CreateTargetSessions(int TTSID, int TrainerID, string TrainingDate, string FromTime, string ToTime, string Remarks, out int Result)
        {
            try
            {
                objDal = new TMS_DAL();
                objDal.CreateTargetSessions(TTSID, TrainerID, TrainingDate, FromTime, ToTime, Remarks, out Result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable CreateOnlineTraining(int tTSID, string EmpID, string DueDate, string Remarks, char IsSelfCompleteAllowed, out int result)
        {
            try
            {
                objDal = new TMS_DAL();
                int Empid = Convert.ToInt32(EmpID);
                return objDal.CreateOnlineTrainingDb(tTSID, Empid, DueDate, Remarks, IsSelfCompleteAllowed, out result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable getTTS_OnlineSessionDetails(string tTS_ID)
        {
            try
            {
                objDal = new TMS_DAL();
                int itTS_ID = Convert.ToInt32(tTS_ID);
                return objDal.getTTS_OnlineSessionDetailsDb(itTS_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //ReSchedule Target Sessions Suresh
        public void ReScheduleTargetSessions(string TTSID, string TrainingDate, string FromTime, string ToTime, string Remarks, string TrainerID, out int Result)
        {
            try
            {
                objDal = new TMS_DAL();
                int iTTS_ID = Convert.ToInt32(TTSID);
                int TrainerId = Convert.ToInt32(TrainerID);
                objDal.ReScheduleTargetSessions(iTTS_ID, TrainingDate, FromTime, ToTime, Remarks, TrainerId, out Result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetTTS_TraingSessionList(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int TTSID, out int TotalCount)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.GetTTS_TraingSessionList(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, TTSID, out TotalCount);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //To Get Traning Session Details
        public DataSet GetTrainingSessionDetailsBal(string TTS_ID, out int SessionsCount)
        {
            try
            {
                objDal = new TMS_DAL();
                int iTTS_ID = Convert.ToInt32(TTS_ID);
                return objDal.GetTrainingSessionDetails(iTTS_ID, out SessionsCount);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //To get Active Docs
        public DataTable GetActiveDocsBal(string DocID, out int Result)
        {
            try
            {
                objDal = new TMS_DAL();
                int iDocID = Convert.ToInt32(DocID);
                return objDal.GetActiveDocsDal(iDocID, out Result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //To Update  Jr Doc for Oral Evaluation
        public DataTable JRDocToSubmitOral(string DocID, string JR_ID, out int Result)
        {
            try
            {
                objDal = new TMS_DAL();
                int iDocID = Convert.ToInt32(DocID);
                int iJR_ID = Convert.ToInt32(JR_ID);
                return objDal.JRDocToSubmitOralDal(iDocID, iJR_ID, out Result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //To get Trainer Opinion
        public DataTable GetTrainerOpinionBal(string DocID, string JR_ID)
        {
            try
            {
                objDal = new TMS_DAL();
                int iDocID = Convert.ToInt32(DocID);
                int iJR_ID = Convert.ToInt32(JR_ID);
                return objDal.GetTrainerOpinionDal(iDocID, iJR_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        // Take Attandence to Trainees in TTS insert
        public void InsertTraineesAttandence(int TTS_TrainingSessionsID, string Trainee_ID, DataTable dtTrainees, char ExamType, out int Result)
        {
            try
            {
                objDal = new TMS_DAL();
                int TraineeID = Convert.ToInt32(Trainee_ID);
                objDal.InsertTraineesAttandence(TTS_TrainingSessionsID, TraineeID, dtTrainees, ExamType, out Result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion
        #region ATC
        public void SubmitInitializeATC(string DeptID, string Year, string InitiatedBy, string Remarks, string AtcAuthor_ID, string HOD_Approval_ID, string QA_Approval_ID, string Notification_Link, out int creationStatus)
        {
            try
            {
                objDal = new TMS_DAL();
                int iDeptID = Convert.ToInt32(DeptID);
                int iYear = Convert.ToInt32(Year);
                int iInitiatedBy = Convert.ToInt32(InitiatedBy);
                int iAtcAuthor_ID = Convert.ToInt32(AtcAuthor_ID);
                int iHOD_Approval_ID = Convert.ToInt32(HOD_Approval_ID);
                int iQA_Approval_ID = Convert.ToInt32(QA_Approval_ID);
                objDal.SubmitInitializeATCDb(iDeptID, iYear, iInitiatedBy, Remarks, iAtcAuthor_ID, iHOD_Approval_ID, iQA_Approval_ID, Notification_Link, out creationStatus);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void UpdateInitializedATC(string ATC_ID, string ModifiedBy_ID, string Remarks, string ATC_Author_ID, string ATC_HOD_Approval_ID, string ATC_QA_Approval_ID, out int creationStatus)
        {
            try
            {
                objDal = new TMS_DAL();
                int iATC_ID = Convert.ToInt32(ATC_ID);
                int iModifiedBy_ID = Convert.ToInt32(ModifiedBy_ID);
                int iATC_Author_ID = Convert.ToInt32(ATC_Author_ID);
                int iATC_HOD_Approval_ID = Convert.ToInt32(ATC_HOD_Approval_ID);
                int iATC_QA_Approval_ID = Convert.ToInt32(ATC_QA_Approval_ID);
                objDal.UpdateInitializedATCDb(iATC_ID, iModifiedBy_ID, Remarks, iATC_Author_ID, iATC_HOD_Approval_ID, iATC_QA_Approval_ID, out creationStatus);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        public DataSet GetRoleWiseEmployees(int DeptID, int IsEmpActive = 0)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.GetRoleWiseEmployees(DeptID, IsEmpActive);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetEmployeeJrList_View(int JR_ID)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.GetEmployeeJrList_View(JR_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void TraineeAskQuestion(int TraineeID, string Question, int DocID, int BaseID, int BasedOn, out int Result)
        {
            try
            {
                objDal = new TMS_DAL();
                objDal.TraineeAskQuestiondb(TraineeID, Question, DocID, BaseID, BasedOn, out Result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //JR Training List
        public DataSet GetJR_Content(int JR_ID)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.GetJrTrainingList_Viewdb(JR_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //To get JR Regular Records
        public DataTable GetJR_RegularRecordsBal(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, string JR_ID, string TrainerID, out int IsRetrospective)
        {
            objDal = new TMS_DAL();
            int iJR_ID = Convert.ToInt32(JR_ID);
            int iTrainerID = Convert.ToInt32(TrainerID);
            return objDal.GetJR_RegularRecordsDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, iJR_ID, iTrainerID, out IsRetrospective);
        }

        //To get JR Retrospective Records
        public DataTable GetJR_RetrospectiveRecordsBal(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, string JR_ID, string TrainerID)
        {
            objDal = new TMS_DAL();
            int iJR_ID = Convert.ToInt32(JR_ID);
            int iTrainerID = Convert.ToInt32(TrainerID);
            return objDal.GetJR_RetrospectiveRecordsDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, iJR_ID, iTrainerID);
        }
        public int InseretJR_PrintLog(string JR_ID, string Emp_ID, string selectedValue)
        {
            try
            {
                objDal = new TMS_DAL();
                int iJR_ID = Convert.ToInt32(JR_ID);
                int iEmpID = Convert.ToInt32(Emp_ID);
                int iTF_Type = Convert.ToInt32(selectedValue);
                return objDal.InseretJR_PrintLogDb(iJR_ID, iEmpID, iTF_Type);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet getATC_DataforModification(int ATC_ID)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.getATC_DataforModificationDb(ATC_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //Get Initialise ATC details
        public DataSet GetInitializeATCDetails(int ATCID)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.GetInitializeATCDetails(ATCID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet GetEffectedDocsforAtcCreation(int DocumentTypeID, int DeptID)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.GetEffectedDocsforAtcCreationdb(DocumentTypeID, DeptID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int InsertTrainingCalender(int DeptID, int Year, int EmpID, string Remarks, DataTable dtResult, int InitializeATC_ID, out int CreationStatus)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.InsertTrainingCalender(DeptID, Year, EmpID, Remarks, dtResult, InitializeATC_ID, out CreationStatus);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        // Annual Training Calender View
        public DataSet GetAnnualTrainingCalenderView(int AtcID)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.GetAnnualTrainingCalenderView(AtcID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //Getting Trainees Based On Attendence
        public DataTable GetTraineesAttendence(int iDisplayLength, int iDisplayStart, int iSortCol_0,
        string sSortDir_0, string sSearch, int TTS_ID, int AttendenceStatus, out int ToTalCount)
        {
            objDal = new TMS_DAL();
            return objDal.GetTraineesAttendence(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, TTS_ID, AttendenceStatus, out ToTalCount);
        }

        // for getting ATC list data to JQuery datatable
        public DataTable GetATC_List(int iDisplayLength, int iDisplayStart, int iSortCol_0,
         string sSortDir_0, string sSearch, int EmpID, int NotificationID, out int ToTalCount)
        {
            objDal = new TMS_DAL();
            return objDal.GetATC_List(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, EmpID, NotificationID, out ToTalCount);
        }

        //for getting TTS PendingList data to Jquery Datatable
        public DataTable GetPendingTTS_List(int iDisplayLength, int iDisplayStart, int iSortCol_0,
         string sSortDir_0, string sSearch, int EmpID, int FilterType, int NotificationID, out int ToTalCount)
        {
            objDal = new TMS_DAL();
            return objDal.GetPendingTTS_List(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, EmpID, FilterType, NotificationID, out ToTalCount);
        }
        //for getting Initialized ATC data to Jquery Datatable
        public DataTable GetInitialize_ATC(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, int EmpID)
        {
            objDal = new TMS_DAL();
            return objDal.GetInitialize_ATC(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, EmpID);
        }
        //For getting the History of ATC. by ATC ID
        public DataTable get_ATC_HistoryList(int iDisplayLength, int displayStart, int sortCol, string sSortDir, string sSearch, int aTC_ID)
        {
            objDal = new TMS_DAL();
            return objDal.get_ATC_HistoryListdB(iDisplayLength, displayStart, sortCol, sSortDir, sSearch, aTC_ID);
        }
        //for getting ATC Pending List Data to Jquery DataTable
        public DataTable GetATC_PendingList(int iDisplayLength, int iDisplayStart, int iSortCol_0,
        string sSortDir_0, string sSearch, int EmpID, int FilterType, int NotificationID, out int ToTalCount)
        {
            objDal = new TMS_DAL();
            return objDal.GetATC_PendingList(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, EmpID, FilterType, NotificationID, out ToTalCount);
        }

        //for getting ATC Training Coordinator Approval Data to Jquery DataTable
        public DataTable GetATC_Hod_Approval(int iDisplayLength, int iDisplayStart, int iSortCol_0,
       string sSortDir_0, string sSearch, int EmpID, int FilterType, int NotificationID, out int ToTalCount)
        {
            objDal = new TMS_DAL();
            return objDal.GetATC_Hod_Approval(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, EmpID, FilterType, NotificationID, out ToTalCount);
        }

        //for getting ATC QA Approval Data to Jquery DataTable
        public DataTable GetATC_QA_Approval(int iDisplayLength, int iDisplayStart, int iSortCol_0,
       string sSortDir_0, string sSearch, int EmpID, int FilterType, int NotificationID, out int ToTalCount)
        {
            objDal = new TMS_DAL();
            return objDal.GetATC_QA_Approval(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, EmpID, FilterType, NotificationID, out ToTalCount);
        }
        //for getting TTS_Exam Details
        public DataTable GetTTS_ExamDetails(int iDisplayLength, int iDisplayStart, int iSortCol_0,
        string sSortDir_0, string sSearch, int TTS_ID)
        {
            objDal = new TMS_DAL();
            return objDal.GetTTS_ExamDetailsdb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, TTS_ID);
        }

        //for getting TTS ApproveList data to Jquery Datatable
        public DataTable GetApproveTTS_List(int iDisplayLength, int iDisplayStart, int iSortCol_0,
         string sSortDir_0, string sSearch, int EmpID, int FilterType, int NotificationID, out int ToTalCount)
        {
            objDal = new TMS_DAL();
            return objDal.GetApproveTTS_List(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, EmpID, FilterType, NotificationID, out ToTalCount);
        }

        //for getting TTS ApprovedList data to Trainers Jquery Datatable
        public DataTable GetQA_ApprovedTTS_List(int displayLength, int displayStart, int sortCol, string sSortDir, string sSearch, int trainerID, int NotificationID, out int totalRecordCount)
        {
            objDal = new TMS_DAL();
            return objDal.GetQA_ApprovedTTS_ListDb(displayLength, displayStart, sortCol, sSortDir, sSearch, trainerID, NotificationID, out totalRecordCount);
        }

        //for getting TTS Trainee Session List data to  Jquery Datatable
        public DataTable GetTraineeSession_List(int displayLength, int displayStart, int sortCol, string sSortDir, string sSearch, int traineeID, int DeptID, int FilterType, string TargetType, int Year, string Status, out int totalRecordCount)
        {
            objDal = new TMS_DAL();
            return objDal.GetTraineeSession_ListDb(displayLength, displayStart, sortCol, sSortDir, sSearch, traineeID, DeptID, FilterType, TargetType, Year, Status, out totalRecordCount);
        }
        //to get trainee Online Session list to JQ Datatable in target training page
        public DataTable GetTraineeOnlineSession_List(int displayLength, int displayStart, int sortCol, string sSortDir, string sSearch, int traineeID, int DeptID, int FilterType, string TargetType, int Year, string Status, out int totalRecordCount)
        {
            objDal = new TMS_DAL();
            return objDal.GetTraineeOnlineSession_ListDb(displayLength, displayStart, sortCol, sSortDir, sSearch, traineeID, DeptID, FilterType, TargetType, Year, Status, out totalRecordCount);
        }
        //for getting TTS Trainer Evaluation data to  Jquery Datatable
        public DataTable GetTrainerEvaluationTTS_List(int displayLength, int displayStart, int sortCol, string sSortDir, string sSearch,string sSearch_Document, string sSearch_DeptCode, string sSearch_TrainingDate, string sSearch_StartTime,
            string sSearch_EndTime, string sSearch_Trainer, string sSearch_TypeOfTraining, string sSearch_ExamType, int trainerID,string Evaluated, out int totalRecordCount)
        {
            objDal = new TMS_DAL();
            return objDal.GetTrainerEvaluationTTS_ListDb(displayLength, displayStart, sortCol, sSortDir, sSearch, sSearch_Document, sSearch_DeptCode, sSearch_TrainingDate, sSearch_StartTime,
                 sSearch_EndTime, sSearch_Trainer, sSearch_TypeOfTraining, sSearch_ExamType, trainerID, Evaluated, out totalRecordCount);
        }
        //for getting TTS ApprovedList data to reports page for HOD and QA Jquery Datatable
        public DataTable GetQA_ApprovedTTS_ListForHodAndQa(int displayLength, int displayStart, int sortCol, string sSortDir, string sSearch,
            string sSearch_DocumentName, string sSearch_DeptCode, string sSearch_TypeOfTraining, string sSearch_TargetType, string sSearch_Author, string sSearch_Reviewer, string sSearch_ApproveDate, string sSearch_StatusName,
            int EMP_ID, int Year, int FilterType, int DeptID, int Month, string TargetType, string Status, int SubRefID, out int totalRecordCount)
        {
            objDal = new TMS_DAL();
            return objDal.GetQA_ApprovedTTS_ListForHodAndQaDb(displayLength, displayStart, sortCol, sSortDir, sSearch, sSearch_DocumentName, sSearch_DeptCode, sSearch_TypeOfTraining, sSearch_TargetType, sSearch_Author, sSearch_Reviewer, sSearch_ApproveDate, sSearch_StatusName, EMP_ID, Year, FilterType, DeptID, Month, TargetType, Status, SubRefID, out totalRecordCount);
        }
        //for getting TTS Trainee List data to Jquery Datatable
        public DataTable GetTTS_TraineeAssignedList(int displayLength, int displayStart, int sortCol, string sSortDir, string sSearch, int TTS_ID, int TTSSession_ID, out int totalRecordCount)
        {
            objDal = new TMS_DAL();
            return objDal.GetTTS_TraineeAssignedListDb(displayLength, displayStart, sortCol, sSortDir, sSearch, TTS_ID, TTSSession_ID, out totalRecordCount);
        }
        //for getting  Assigned ATC data to Jquery Datatable
        public DataTable GetAssigned_ATC_List(int iDisplayLength, int iDisplayStart, int iSortCol_0,
         string sSortDir_0, string sSearch, int EmpID, int NotificationID, out int ToTalCount)
        {
            objDal = new TMS_DAL();
            return objDal.GetAssigned_ATC_List(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, EmpID, NotificationID, out ToTalCount);
        }

        // AnnualTraining Calender Update by Trainer
        public void UpdateTrainingCalenderTrainer(int AtcID, int ActionBy, string Remarks, DataTable dtResult, int ActionActor, int ActionState, out int CreationStatus)
        {
            try
            {
                objDal = new TMS_DAL();
                objDal.UpdateTrainingCalenderTrainer(AtcID, ActionBy, Remarks, dtResult, ActionActor, ActionState, out CreationStatus);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        // AnnualTraining Calender Update By Hod And QA
        public void UpdateTrainingCalender(int AtcID, int ActionBy, string Remarks, int ActionActor, int ActionState, out int CreationStatus)
        {
            try
            {
                objDal = new TMS_DAL();
                objDal.UpdateTrainingCalender(AtcID, ActionBy, Remarks, ActionActor, ActionState, out CreationStatus);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region FeedbackForm
        //Feedback Form  Creation
        public void CreateFeedbackForm(string Title, int FeedbackType, int EmpID, string Remarks, int Status, int FeedbackID, out int FeedbackResult)
        {
            try
            {
                objDal = new TMS_DAL();
                objDal.UpdateFeedbackForm(Title, FeedbackType, EmpID, Remarks, Status, FeedbackID, out FeedbackResult);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //Feedback Form  Update
        public void UpdateFeedbackForm(string Title, int FeedbackType, int EmpID, string Remarks, int Status, int FeedbackID, out int FeedbackResult)
        {
            try
            {
                objDal = new TMS_DAL();
                objDal.UpdateFeedbackForm(Title, FeedbackType, EmpID, Remarks, Status, FeedbackID, out FeedbackResult);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //Insert Feedback Questions
        public int CreateFeedbackQuestions(int FeedbackID, string Question, string Remarks, int EmpID, string Title, int FeedbackType, out int FeedbackResultID, out int Result)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.CreateFeedbackQuestions(FeedbackID, Question, Remarks, EmpID, Title, FeedbackType, out FeedbackResultID, out Result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //Feedback Form Questions Update
        public int UpdateFeedbackFormQuestions(int EmpID, string Question, string Remarks, int Status, string QuestionID, int FeedbackID, int FeedbackType, out int Result)
        {
            try
            {
                objDal = new TMS_DAL();
                int iQuestionID = Convert.ToInt32(QuestionID);
                return objDal.UpdateFeedbackFormQuestions(EmpID, Question, Remarks, Status, iQuestionID, FeedbackID, FeedbackType, out Result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //To Get FeedBackMaster Details.
        public DataTable GetFeedbackMasterDetails(string FeedbackType)
        {
            try
            {
                objDal = new TMS_DAL();
                int iFeedbackType = Convert.ToInt32(FeedbackType);
                return objDal.GetFeedbackMasterDetailsDb(iFeedbackType);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        //To Get Employee Training Records
        public DataTable GetEmpTrainingRecords(int iDisplayLength, int iDisplayStart, int iSortCol_0,
         string sSortDir_0, string sSearch, int EmpID, string sSearch_DocumentName, string sSearch_DocRefNo, string sSearch_DeptName, string sSearch_ModeOfTraining, string sSearch_Opinion,
                  string sSearch_EvaluatedBy,string sSearch_EvaluatedDate, string sSearch_TrainingType, string sSearch_ReadDuration, out string TotalDuration)
        {
            objDal = new TMS_DAL();
            return objDal.GetEmpTrainingRecords(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, 
                sSearch, EmpID, sSearch_DocumentName, sSearch_DocRefNo, sSearch_DeptName, sSearch_ModeOfTraining, sSearch_Opinion,
                    sSearch_EvaluatedBy, sSearch_EvaluatedDate, sSearch_TrainingType, sSearch_ReadDuration, out TotalDuration);
        }
        public DataTable GetMyTrainingRecords(int iDisplayLength, int iDisplayStart, int iSortCol_0,
         string sSortDir_0, string sSearch, int EmpID, int FilterType, int Year, int DeptID, string TargetType)
        {
            objDal = new TMS_DAL();
            return objDal.GetMyTrainingRecordsDal(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0,
                sSearch, EmpID, FilterType, Year, DeptID, TargetType);
        }

        // To Get Document Wise Training Records ListGetDocWiseOnGoingTrainingRecords
        public DataTable GetDocWiseTrainingRecords(int iDisplayLength, int iDisplayStart, int iSortCol_0,
         string sSortDir_0, string sSearch, int DocumentID, string FromDate, string ToDate, string sSearch_Document, string sSearch_VersionNumber,
         string sSearch_TraineeEmpCode, string sSearch_TraineeName, string sSearch_TrainingDate, string sSearch_TrainerName, string sSearch_Type)
        {
            objDal = new TMS_DAL();
            return objDal.GetDocWiseTrainingRecordsDb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, DocumentID, FromDate, ToDate, sSearch_Document,
                  sSearch_VersionNumber, sSearch_TraineeEmpCode, sSearch_TraineeName, sSearch_TrainingDate, sSearch_TrainerName, sSearch_Type);
        }
        // To Get Document Wise OnGoing Training Records List
        public DataTable GetDocWiseOnGoingTrainingRecords(int iDisplayLength, int iDisplayStart, int iSortCol_0, string sSortDir_0, string sSearch, int DocumentID, string sSearch_Document, string sSearch_VersionNumber,
         string sSearch_TraineeEmpCode, string sSearch_TraineeName, string sSearch_TrainingStatus, string sSearch_TrainerName, string sSearch_Type)
        {
            objDal = new TMS_DAL();
            return objDal.GetDocWiseOnGoingTrainingRecordsDb(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, DocumentID,
                sSearch_Document, sSearch_VersionNumber, sSearch_TraineeEmpCode, sSearch_TraineeName, sSearch_TrainingStatus, sSearch_TrainerName, sSearch_Type);
        }

        //insert jr training file 
        public void InsertJrTrainingFile(int jrid, DataTable TrainingFiles)
        {
            try
            {
                objDal = new TMS_DAL();
                objDal.InsertJrTrainingFileIntoDB(jrid, TrainingFiles);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public DataTable GetPrintLogTrainingFileDetails(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, string JR_ID)
        {
            objDal = new TMS_DAL();
            int IJR_ID = Convert.ToInt32(JR_ID);
            return objDal.GetPrintLogTrainingFileDB(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, IJR_ID);
        }

        public DataTable GetATC_MRTList(int iDisplayLength, int iDisplayStart, int iSortCol_0,
          string sSortDir_0, string sSearch, int ATC_ID)
        {
            objDal = new TMS_DAL();
            return objDal.GetATC_MRTList(iDisplayLength, iDisplayStart, iSortCol_0, sSortDir_0, sSearch, ATC_ID);
        }

        #region TMS Modification by 27 Nov 2019

        public int CloseTheTargetTraining(int TTS_ID, int EmpID, string Comments)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.CloseTheTargetTraining(TTS_ID, EmpID, Comments);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public DataTable CancelledTraineesList(int TTS_ID)
        {
            try
            {
                objDal = new TMS_DAL();
                return objDal.CancelledTraineesList(TTS_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        //for getting ATC MRT  Jquery DataTable

        #region
        //public DataTable GetTraineesInActiveStatus(DataTable dtViewStateTrainees)
        //{
        //    objDal = new TMS_DAL();
        //    return objDal.GetInActiveTraineesStatus(dtViewStateTrainees);
        //}
        #endregion

        //public int GetDocumentRecommendedCount(int traineeID)
        //{
        //    objDal = new TMS_DAL();
        //    return objDal.GetDocumentRecommendedCountDb(traineeID);
        //}
    }
}
