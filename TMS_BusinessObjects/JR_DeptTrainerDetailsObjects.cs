﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class JR_DeptTrainerDetailsObjects
    {
        public string EmpCode { get; set; }
        public string TrainerName { get; set; }

        public JR_DeptTrainerDetailsObjects(string _EmpCode, string _TrainerName)
        {
            EmpCode = _EmpCode;
            TrainerName = _TrainerName;
        }
    }
}
