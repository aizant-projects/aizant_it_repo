﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
    public class FeedbackMasterHistoryObjects
    {
        public int ActionID { get; set; }
        public string ActionBy { get; set; }
        public string ActionDate { get; set; }
        public string StatusName { get; set; }
        public string Remarks { get; set; }
        
        public FeedbackMasterHistoryObjects(int _ActionID,string _ActionBy, string _ActionDate,string _StatusName,string _Remarks)
        {
            ActionID = _ActionID;
            ActionBy = _ActionBy;
            ActionDate = _ActionDate;
            StatusName = _StatusName;
            Remarks = _Remarks;
        }
    }
}
