﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
    public class JrTsRetropectiveDetailsObject
    {
        public string DeptName { get; set; }
        public string DocumentNumber { get; set; }
        public string DeptCode { get; set; }
       
        public JrTsRetropectiveDetailsObject(string _DeptName, string _DocumentNumber, string _DeptCode)
        {
            DeptName = _DeptName;
            DocumentNumber = _DocumentNumber;
            DeptCode = _DeptCode;
        }
    }
}
