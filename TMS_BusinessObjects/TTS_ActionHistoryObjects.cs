﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
    public class TTS_ActionHistoryObjects
    {
        public string ActionHistoryID { get; set; }
        public string ActionStatus { get; set; }
        public string ActionBy { get; set; }
        public string ActionRole { get; set; }
        public string ActionDate { get; set; }
        public string Remarks { get; set; }

        public TTS_ActionHistoryObjects(string _ActionStatus,string _ActionBy, string _ActionRole,string _ActionDate, string _Remarks)
        {
            ActionStatus = _ActionStatus;
            ActionBy = _ActionBy;
            ActionRole = _ActionRole;
            ActionDate = _ActionDate;
            Remarks = _Remarks;
        }
        public TTS_ActionHistoryObjects(string _ActionHistoryID, string _ActionStatus, string _ActionBy, string _ActionRole, string _ActionDate, string _Remarks)
        {
            ActionHistoryID = _ActionHistoryID;
            ActionStatus = _ActionStatus;
            ActionBy = _ActionBy;
            ActionRole = _ActionRole;
            ActionDate = _ActionDate;
            Remarks = _Remarks;
        }
    }
}
