﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class FeedbackMasterObjects
    {
        public int FeedbackQuestionID { get; set; }
        public string QuestionTitle { get; set; }
        public string StatusName { get; set; }
        public int EmpFeedbackonQuestion { get; set; }
        public FeedbackMasterObjects(int _FeedbackQuestionID, string _QuestionTitle, string _StatusName,int _EmpFeedbackonQuestion)
        {
            FeedbackQuestionID = _FeedbackQuestionID;
            QuestionTitle = _QuestionTitle;
            StatusName = _StatusName;
            EmpFeedbackonQuestion = _EmpFeedbackonQuestion;
        }
    }
}
