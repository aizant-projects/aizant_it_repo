﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
    public class EmpJrListObjects
    {
        public int JR_ID { get; set; }
        public int TS_ID { get; set; }
        public string TraineeName { get; set; }
        public string JR_Name { get; set; }
        public int JR_Version { get; set; }
        public string HOD_Name { get; set; }
        public string StatusName { get; set; }
        public string DesignationName { get; set; }
        public string DepartmentName { get; set; }
        public string CurrentStatus { get; set; }
        public int EmpFeedbackID { get; set; }

        public EmpJrListObjects(int JR_ID,int _TS_ID, string TraineeName, string JR_Name, int JR_Version, string HOD_Name,
          string StatusName, string DesignationName, string DepartmentName, string CurrentStatus,int EmpFeedbackID)
        {
            this.JR_ID = JR_ID;
            this.TS_ID = _TS_ID;
            this.TraineeName = TraineeName;
            this.JR_Name = JR_Name;
            this.JR_Version = JR_Version;
            this.HOD_Name = HOD_Name;
            this.StatusName = StatusName;
            this.DesignationName = DesignationName;
            this.DepartmentName = DepartmentName;
            this.CurrentStatus = CurrentStatus;
            this.EmpFeedbackID = EmpFeedbackID;
        }
    }
}
