﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class JR_TemplateListObjects
    {
        public int JR_TemplateID { get; set; }
        public string JR_TemplateName { get; set; }
        public int DeptID { get; set; }
        public string DepartmentName { get; set; }
        public string DeptCode { get; set; }
        public string CreatedByName { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedByName { get; set; }
        public string ModifiedDate { get; set; }

        public JR_TemplateListObjects(int _JR_TemplateID, string _JR_TemplateName, int _DeptID, string _DepartmentName, string _DeptCode, string _CreatedByName, string _CreatedDate, string _ModifiedByName, string _ModifiedDate)
        {
            JR_TemplateID = _JR_TemplateID;
            JR_TemplateName = _JR_TemplateName;
            DeptID = _DeptID;
            DepartmentName = _DepartmentName;
            DeptCode = _DeptCode;
            CreatedByName = _CreatedByName;
            CreatedDate = _CreatedDate;
            ModifiedByName = _ModifiedByName;
            ModifiedDate = _ModifiedDate;
        }
    }
}
