﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class JR_RegularObjects
    {
        public int DocumentID { get; set; }
        public string Document { get; set; }
        public string DocumentType { get; set; }
        public int VersionNumber { get; set; }
        public string DepartmentName { get; set; }
        public string ExamType { get; set; }
        public int Attempts { get; set; }
        public string StatusName { get; set; }
        public string ReadDuration { get; set; }

        public JR_RegularObjects(int DocumentID, string Document, string DocumentType, int VersionNumber, string DepartmentName, string ExamType, int Attempts, string StatusName, string ReadDuration)
        {
            this.DocumentID = DocumentID;
            this.Document = Document;
            this.DocumentType = DocumentType;
            this.VersionNumber = VersionNumber;
            this.DepartmentName = DepartmentName;
            this.ExamType = ExamType;
            this.Attempts = Attempts;
            this.StatusName = StatusName;
            this.ReadDuration = ReadDuration;
        }
    }
}
