﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
    public class JR_EmpTrainedDocsObjects
    {
        public int JrID { get; set; }
        public int DocID { get; set; }
        public string Document { get; set; }
        public int DocVersion { get; set; }
        public string Department { get; set; }
        public string ReadDuration { get; set; }
        public int Attempts { get; set; }
        public string QualifiedDate { get; set; }
        public int MarksObtained { get; set; }
        public string EvaluatedTrainer { get; set; }

        public JR_EmpTrainedDocsObjects(int _JrID, int _DocID, string _Document, int _DocVersion, string _Department, string _ReadDuration, int _Attempts, string _QualifiedDate, int _MarksObtained, string _EvaluatedTrainer)
        {
            JrID = _JrID;
            DocID = _DocID;
            Document = _Document;
            DocVersion = _DocVersion;
            Department = _Department;
            ReadDuration = _ReadDuration;
            Attempts = _Attempts;
            QualifiedDate = _QualifiedDate;
            MarksObtained = _MarksObtained;
            EvaluatedTrainer = _EvaluatedTrainer;
        }
    }
}