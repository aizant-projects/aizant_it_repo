﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class TraineesAttendenceObject
    {
        public string EmpCode { get; set; }
        public string EmpName { get; set; }
        public string DepartmentName { get; set; }
        public bool IsTraineeCancelled { get; set; }
        public TraineesAttendenceObject(string EmpCode, string EmpName, string DepartmentName,bool IsTraineeCancelled)
        {
            this.EmpCode = EmpCode;
            this.EmpName = EmpName;
            this.DepartmentName = DepartmentName;
            this.IsTraineeCancelled = IsTraineeCancelled;
        }
    }
}
