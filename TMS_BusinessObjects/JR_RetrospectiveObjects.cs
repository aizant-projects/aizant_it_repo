﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class JR_RetrospectiveObjects
    {
        public string Document { get; set; }
        public string DocumentType { get; set; }
        public int VersionNumber { get; set; }
        public string DepartmentName { get; set; }

        public JR_RetrospectiveObjects(string Document, string DocumentType, int VersionNumber, string DepartmentName)
        {
            this.Document = Document;
            this.DocumentType = DocumentType;
            this.VersionNumber = VersionNumber;
            this.DepartmentName = DepartmentName;
        }
    }
}

