﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class TraineePendingCount_Objects
    {
        public string EventName { get; set; }
        public int Pending { get; set; }
        public int Completed { get; set; }
        

        public TraineePendingCount_Objects(string EventName, int Pending,int Completed)
        {
            this.EventName = EventName;
            this.Pending = Pending;
            this.Completed = Completed;
        }
    }
}
