﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
    public class TrainingSessionEvaluation
    {
        public int TTS_TrainingSessionID { get; set; }
        public int TraineeID { get; set; }
        public int EvaluationStatus { get; set; }
        public int TargetExamID { get; set; }
        public string EmpCode { get; set; }
        public string Trainee { get; set; }
        public string DepartmentName { get; set; }
        public int Attempts { get; set; }

        public TrainingSessionEvaluation(int TTS_TrainingSessionID, int TraineeID, int EvaluationStatus,int TargetExamID, string EmpCode,
          string Trainee, string DepartmentName,int Attempts)
        {
            this.TTS_TrainingSessionID = TTS_TrainingSessionID;
            this.TraineeID = TraineeID;
            this.EvaluationStatus = EvaluationStatus;
            this.TargetExamID = TargetExamID;
            this.EmpCode = EmpCode;
            this.Trainee = Trainee;
            this.DepartmentName = DepartmentName;
            this.Attempts = Attempts;
        }
    }
}
