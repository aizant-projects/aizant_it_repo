﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
    public class DocTrainingReportsObjects
    {
        public string Document { get; set; }
        public int VersionNumber { get; set; }
        public string TraineeEmpCode { get; set; }
        public string TraineeName { get; set; }
        public string TrainingDate { get; set; }
        public string TrainingStatus { get; set; }
        public string TrainerNames { get; set; }
        public string TrainerName { get; set; }
        public string Type { get; set; }

        public DocTrainingReportsObjects(string Document, int VersionNumber, string TraineeEmpCode, string TraineeName, string TrainingDate, string TrainerName, string Type)
        {
            this.Document = Document;
            this.VersionNumber = VersionNumber;
            this.TraineeEmpCode = TraineeEmpCode;
            this.TraineeName = TraineeName;
            this.TrainingDate = TrainingDate;
            this.TrainerName = TrainerName;
            this.Type = Type;
        }
        public DocTrainingReportsObjects( int VersionNumber, string Document, string TraineeEmpCode, string TraineeName, string TrainingStatus, string TrainerNames, string Type)
        {
            this.Document = Document;
            this.VersionNumber = VersionNumber;
            this.TraineeEmpCode = TraineeEmpCode;
            this.TraineeName = TraineeName;
            this.TrainingStatus = TrainingStatus;
            this.TrainerNames = TrainerNames;
            this.Type = Type;
        }
    }
}