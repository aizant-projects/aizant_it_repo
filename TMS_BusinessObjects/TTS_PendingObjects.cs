﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
    public class TTS_PendingObjects
    {
        public int TTS_ID { get; set; }
        public string DocumentName { get; set; }
        public string DepartmentName { get; set; }
        public string TypeOfTraining { get; set; }
        public string Reviewer { get; set; }
        public string CurrentStatus { get; set; }
        public int StatusID { get; set; }

        public TTS_PendingObjects(int TTS_ID, string DocumentName, string DepartmentName, string TypeOfTraining,
          string Reviewer, string CurrentStatus, int StatusID)
        {
            this.TTS_ID = TTS_ID;
            this.DocumentName = DocumentName;
            this.DepartmentName = DepartmentName;
            this.TypeOfTraining = TypeOfTraining;
            this.Reviewer = Reviewer;
            this.CurrentStatus = CurrentStatus;
            this.StatusID = StatusID;
        }
    }
}
