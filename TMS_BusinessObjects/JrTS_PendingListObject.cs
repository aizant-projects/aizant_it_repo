﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class JrTS_PendingListObject
    {
        public int TS_ID { get; set; }
        public string JR_Name { get; set; }
        public string JR_Version { get; set; }
        public string TraineeName { get; set; }
        public string EmpCode { get; set; }
        public string DeptName { get; set; }
        public string DeptCode { get; set; }
        public string DesignationName { get; set; }
        public string TS_Reviewer { get; set; }
        public string StatusName { get; set; }
        public int StatusID { get; set; }

        public JrTS_PendingListObject(int _TS_ID, string _JR_Name, string _JR_Version, string _TraineeName, string _EmpCode, string _DeptName, string _DeptCode, string _DesignationName, string _TS_Reviewer, string _StatusName, int _StatusID)
        {
            TS_ID = _TS_ID;
            JR_Name = _JR_Name;
            JR_Version = _JR_Version;
            TraineeName = _TraineeName;
            EmpCode = _EmpCode;
            DeptName = _DeptName;
            DeptCode = _DeptCode;
            DesignationName = _DesignationName;
            TS_Reviewer = _TS_Reviewer;
            StatusName = _StatusName;
            StatusID = _StatusID;
        }
    }
}
