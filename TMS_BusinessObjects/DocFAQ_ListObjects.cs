﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class DocFAQ_ListObjects
    {
        public int DocQuestionID { get; set; }
        public string DocQuestion { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string CurrentStatusName { get; set; }

        public DocFAQ_ListObjects(int DocQuestionID,  string DocQuestion, string CreatedBy, string CreatedDate,string CurrentStatusName)
        {
            this.DocQuestionID = DocQuestionID;
            this.DocQuestion = DocQuestion;
            this.CreatedBy = CreatedBy;
            this.CreatedDate = CreatedDate;
            this.CurrentStatusName = CurrentStatusName;
        }
        
    }
}
