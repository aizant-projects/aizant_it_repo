﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class JR_TrainerEvaluationDetailsObjects
    {
        public int JR_ID { get; set; }
        public string JR_Name { get; set; }
        public string JR_Version { get; set; }
        public string TraineeName { get; set; }
        public string TraineeDeptName { get; set; }
        public string JR_AssignedBy { get; set; }
        public string JR_AssignedDate { get; set; }

        public JR_TrainerEvaluationDetailsObjects(int _JR_ID, string _JR_Name, string _JR_Version, string _TraineeName, string _TraineeDeptName, string _JR_AssignedBy, string _JR_AssignedDate)
        {
            JR_ID = _JR_ID;
            JR_Name = _JR_Name;
            JR_Version = _JR_Version;
            TraineeName = _TraineeName;
            TraineeDeptName = _TraineeDeptName;
            JR_AssignedBy = _JR_AssignedBy;
            JR_AssignedDate = _JR_AssignedDate;
        }
    }
}