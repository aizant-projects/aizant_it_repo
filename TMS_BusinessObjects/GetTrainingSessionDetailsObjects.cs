﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class GetTrainingSessionDetailsObjects
    {
        public int TTS_TrainingSessionID { get; set; }
        public int TraineeID { get; set; }
        public int EvaluationStatus { get; set; }
        public int TargetExamID { get; set; }
        public string EmpCode { get; set; }
        public string Trainee { get; set; }
        public string DepartmentName { get; set; }
        public int Attempts { get; set; }
        public string StatusText { get; set; }
        public int EmpFeedbackID { get; set; }
        public int TrainerSatisfactory { get; set; }
        public string EvaluationRemarks { get; set; }

        public GetTrainingSessionDetailsObjects(int TTS_TrainingSessionID, int TraineeID, int EvaluationStatus, int TargetExamID, string EmpCode,
          string Trainee, string DepartmentName, int Attempts,string StatusText,int EmpFeedbackID,int TrainerSatisfactory,string EvaluationRemarks)
        {
            this.TTS_TrainingSessionID = TTS_TrainingSessionID;
            this.TraineeID = TraineeID;
            this.EvaluationStatus = EvaluationStatus;
            this.TargetExamID = TargetExamID;
            this.EmpCode = EmpCode;
            this.Trainee = Trainee;
            this.DepartmentName = DepartmentName;
            this.Attempts = Attempts;
            this.StatusText = StatusText;
            this.EmpFeedbackID = EmpFeedbackID;
            this.TrainerSatisfactory = TrainerSatisfactory;
            this.EvaluationRemarks = EvaluationRemarks;
        }
    }
}
