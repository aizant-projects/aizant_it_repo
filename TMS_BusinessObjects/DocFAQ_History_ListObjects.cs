﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class DocFAQ_History_ListObjects
    {
        public int DocFAQ_ID { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string ModifiedReason { get; set; }

        public DocFAQ_History_ListObjects(int DocFAQ_ID, string ModifiedBy, string ModifiedDate, string ModifiedReason)
        {
            this.DocFAQ_ID = DocFAQ_ID;
            this.ModifiedBy = ModifiedBy;
            this.ModifiedDate = ModifiedDate;
            this.ModifiedReason = ModifiedReason;
        }
    }
}
