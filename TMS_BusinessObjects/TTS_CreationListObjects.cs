﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class TTS_CreationListObjects
    {
        public int DeptID { get; set; }
        public string DepartmentName { get; set; }
        public int DocumentID { get; set; }
        public string DocumentName { get; set; }
        public int DocumentTypeID { get; set; }
        public string DocumentType { get; set; }
        public string TargetType { get; set; }
        public int TargetTypeID { get; set; }

        public TTS_CreationListObjects(int DeptID, string DepartmentName, int DocumentID, string DocumentName, int DocumentTypeID,string DocumentType, string TargetType, int TargetTypeID)
        {
            this.DeptID = DeptID;
            this.DepartmentName = DepartmentName;
            this.DocumentID = DocumentID;
            this.DocumentName = DocumentName;
            this.DocumentTypeID = DocumentTypeID;
            this.DocumentType = DocumentType;
            this.TargetType = TargetType;
            this.TargetTypeID = TargetTypeID;

        }
    }
}