﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
    public  class TTS_ApprovalListObjects
    {
        public int TTS_ID { get; set; }
        public string DocumentName { get; set; }
        public string DepartmentName { get; set; }
        public string TypeOfTraining { get; set; }
        public string Author { get; set; }
        public string CurrentStatus { get; set; }
        public int StatusID { get; set; }

        public TTS_ApprovalListObjects(int TTS_ID, string DocumentName, string DepartmentName, string TypeOfTraining,
          string Author, string CurrentStatus, int StatusID)
        {
            this.TTS_ID = TTS_ID;
            this.DocumentName = DocumentName;
            this.DepartmentName = DepartmentName;
            this.TypeOfTraining = TypeOfTraining;
            this.Author = Author;
            this.CurrentStatus = CurrentStatus;
            this.StatusID = StatusID;
        }
    }
}
