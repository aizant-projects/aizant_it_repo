﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public  class JR_TrainingDocDetailsObjects
    {
        public int DocumentID { get; set; }
        public int DocumentVesrsionID { get; set; }
      
        public string Document { get; set; }
        public string DocumentType { get; set; }
        public string TrainingStatus { get; set; }
        public JR_TrainingDocDetailsObjects(int _DocumentID, int _DocumentVesrsionID, string _Document, string _DocumentType, string _TrainingStatus)
        {
            DocumentID = _DocumentID;
            DocumentVesrsionID = _DocumentVesrsionID;
            Document = _Document;
            DocumentType = _DocumentType;
            TrainingStatus = _TrainingStatus;
        }
    }
}
