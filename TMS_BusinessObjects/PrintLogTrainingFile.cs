﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class PrintLogTrainingFile
    {
        public int Log_ID { get; set; }
        public string PrintBy { get; set; }
        public string PrintDate { get; set; }
        public string TF_Type { get; set; }
        public PrintLogTrainingFile(int _LogID,string _PrintBy, string _PrintDate, string _TF_Type)
        {
            Log_ID = _LogID;
            PrintBy = _PrintBy;
            PrintDate = _PrintDate;
            TF_Type = _TF_Type;
        }
    }
}
