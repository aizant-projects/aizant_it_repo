﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
    public class TTS_Trainer_EvaluationListObjects
    {
        public int TTS_ID { get; set; }
        public int TTS_TrainingSessionID { get; set; }
        public int DocumentID { get; set; }
        public string Document { get; set; }
        public string DeptCode { get; set; }
        public string TrainingDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Trainer { get; set; }
        public string TypeOfTraining { get; set; }
        public string ExamType { get; set; }


        public TTS_Trainer_EvaluationListObjects(int TTS_ID, int TTS_TrainingSessionID, int DocumentID, string Document, string DeptCode, string TrainingDate,
           string StartTime, string EndTime, string Trainer, string TypeOfTraining, string ExamType)
        {
            this.TTS_ID = TTS_ID;
            this.TTS_TrainingSessionID = TTS_TrainingSessionID;
            this.DocumentID = DocumentID;
            this.Document = Document;
            this.DeptCode = DeptCode;
            this.TrainingDate = TrainingDate;
            this.StartTime = StartTime;
            this.EndTime = EndTime;
            this.Trainer = Trainer;
            this.TypeOfTraining = TypeOfTraining;
            this.ExamType = ExamType;
        }
    }
}
