﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
    public  class TTS_TraineeAsignedListObjects
    {
        public int Trainee_ID { get; set; }
        public string Trainee_Code { get; set; }
        public string Trainee_Name { get; set; }
        public string Dept_Code { get; set; }
        public string ExamType { get; set; }
      
        public TTS_TraineeAsignedListObjects(int Trainee_ID, string Trainee_Code, string Trainee_Name, string Dept_Code,string ExamType)
        {
            this.Trainee_ID = Trainee_ID;
            this.Trainee_Code = Trainee_Code;
            this.Trainee_Name = Trainee_Name;
            this.Dept_Code = Dept_Code;
            this.ExamType = ExamType;           
        }
    }
}
