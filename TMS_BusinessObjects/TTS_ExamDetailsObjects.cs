﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class TTS_ExamDetailsObjects
    {
        public int TargetExamID { get; set; }
        public int EmpID { get; set; }
        public string EmpCode { get; set; }
        public string EmpName { get; set; }
        public int Attempts { get; set; }
        public string Status { get; set; }
        public int StatusID { get; set; }
        public TTS_ExamDetailsObjects(int TargetExamID, int EmpID,string EmpCode, string EmpName, int Attempts,
        string Status,int StatusID)
        {
            this.TargetExamID = TargetExamID;
            this.EmpID = EmpID;
            this.EmpCode = EmpCode;
            this.EmpName = EmpName;
            this.Attempts = Attempts;
            this.Status = Status;
            this.StatusID = StatusID;
        }
    }
}
