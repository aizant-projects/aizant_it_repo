﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
    public class JR_TrainingRegularObjects
    {
        public int Dept_ID { get; set; }
        public string DepartmentName { get; set; }
        public int DocumentCount { get; set; }
        public int TrainerCount { get; set; }
        public int AssignedDays { get; set; }
        public int RemainingDays { get; set; }
        public string StatusName { get; set; }

        public JR_TrainingRegularObjects(int _Dept_ID, string _DepartmentName, int _DocumentCount,int _TrainerCount, int _AssignedDays, int _RemainingDays, string _StatusName)
        {
            Dept_ID = _Dept_ID;
            DepartmentName = _DepartmentName;
            DocumentCount = _DocumentCount;
            TrainerCount = _TrainerCount;
            AssignedDays = _AssignedDays;
            RemainingDays = _RemainingDays;
            StatusName = _StatusName;
        }
    }
}
