﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects.TMS_Dashboard
{
    public class ATC_DocumentDeatils
    {
        public string Document { get; set; }
        public string DocumentType { get; set; }

        public ATC_DocumentDeatils(string Document, string DocumentType)
        {
            this.Document = Document;
            this.DocumentType = DocumentType;
        }
    }
}
