﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
    public class JrActionEmps
    {         
        public int JR_ID { get; set; }
        public int DeptID { get; set; }
        public int StatusID { get; set; }
        public int TraineeID { get; set; }
        public int AuthorID { get; set; }
        public int ReviewerID { get; set; }
        public string JR_Name { get; set; }
        public string Version { get; set; }
        public string Department { get; set; }
        public string Trainee { get; set; }
        public string Author { get; set; }
        public string Reviewer { get; set; }
        public string Status { get; set; }

        public JrActionEmps(int _JR_ID, int _DeptID, int _StatusID,int _TraineeID, int _AuthorID,int _ReviewerID,string _JR_Name, string _Version, string _Department,
          string _Trainee, string _Author, string _Reviewer, string _Status)
        {
            JR_ID = _JR_ID;
            DeptID = _DeptID;
            StatusID = _StatusID;
            TraineeID = _TraineeID;
            AuthorID = _AuthorID;
            ReviewerID = _ReviewerID;
            Author = _Author;
            JR_Name = _JR_Name;
            Version = _Version;
            Department = _Department;
            Trainee = _Trainee;
            Author = _Author;
            Reviewer = _Reviewer;
            Status = _Status;
        }
    }
}
