﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class JrTS_Objects
    {
        public string ActionStatus { get; set; }
        public string ActionBy { get; set; }
        public string ActionRole { get; set; }
        public string ActionDate { get; set; }
        public string Remarks { get; set; }

        public JrTS_Objects(string _ActionStatus, string _ActionBy, string _ActionRole, string _ActionDate, string _Remarks)
        {
            ActionStatus = _ActionStatus;
            ActionBy = _ActionBy;
            ActionRole = _ActionRole;
            ActionDate = _ActionDate;
            Remarks = _Remarks;
        }
    }
}
