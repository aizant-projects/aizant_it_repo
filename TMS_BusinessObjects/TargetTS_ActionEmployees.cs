﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
    public class TargetTS_ActionEmployees
    {
       public int TTS_ID { get; set; } 
        public int DeptID { get; set; }
        public int StatusID { get; set; }
        public int AuthorID { get; set; }
        public int ReviewerID { get; set; }
        public string Document { get; set; }
        public string Department { get; set; }
        public string TargetType { get; set; }
        public string Author { get; set; }
        public string Reviewer { get; set; }
        public string Status { get; set; }

        public TargetTS_ActionEmployees(int _TTS_ID, int _DeptID, int _StatusID, int _AuthorID, int _ReviewerID, string _Document,  string _Department,
          string _TargetType, string _Author, string _Reviewer, string _Status)
        {
            TTS_ID = _TTS_ID;
            DeptID = _DeptID;
            StatusID = _StatusID;
            AuthorID = _AuthorID;
            ReviewerID = _ReviewerID;
            Document = _Document;
            Department = _Department;
            TargetType = _TargetType;
            Author = _Author;
            Reviewer = _Reviewer;
            Status = _Status;
        }
    }
}
