﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
    public class TTS_QA_ApprovedListObjects
    {
        public int TTS_ID { get; set; }
        public string DocumentName { get; set; }
        public string DepartmentName { get; set; }
        public string TypeOfTraining { get; set; }
        public string Author { get; set; }
        public string Reviewer { get; set; }
        public string ApprovedDate { get; set; }
        public string StatusName { get; set; }

        public TTS_QA_ApprovedListObjects(int TTS_ID, string DocumentName, string DepartmentName, string TypeOfTraining,
          string Author, string Reviewer, string ApprovedDate,string StatusName)
        {
            this.TTS_ID = TTS_ID;
            this.DocumentName = DocumentName;
            this.DepartmentName = DepartmentName;
            this.TypeOfTraining = TypeOfTraining;
            this.Author = Author;
            this.Reviewer = Reviewer;
            this.ApprovedDate = ApprovedDate;
            this.StatusName = StatusName;
        }
    }
}
