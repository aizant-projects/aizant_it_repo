﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class TQ_ConversationListObjects
    {
        //This is the Business Objects for the Trainee Asked Questions Conversation used in "TraineeQuestionsOnDoc.asmx.cs"
        public string CommentID { get; set; }
        public string Comment { get; set; }
        public string Commentor { get; set; }
        public string CommentorRole { get; set; }
        public string MarkedAsAnswer { get; set; }
        public TQ_ConversationListObjects(string CommentID, string Comment, string Commentor, string CommentorRole, string MarkedAsAnswer)
        {
            this.CommentID = CommentID;
            this.Comment = Comment;
            this.Commentor = Commentor;
            this.CommentorRole = CommentorRole;
            this.MarkedAsAnswer = MarkedAsAnswer;
        }
    }
}
