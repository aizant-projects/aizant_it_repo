﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
    public class TTS_TargetSessionsObjects
    {
        public int TTS_TrainingSessionsID { get; set; }
        public string TrainingDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string ExamType { get; set; }
        public string Trainer { get; set; }
        public string StatusName { get; set; }
        public int StatusID { get; set; }

        public TTS_TargetSessionsObjects(int TTS_TrainingSessionsID, string TrainingDate, string StartTime, string EndTime,string ExamType,
          string Trainer, string StatusName, int StatusID)
        {
            this.TTS_TrainingSessionsID = TTS_TrainingSessionsID;
            this.TrainingDate = TrainingDate;
            this.StartTime = StartTime;
            this.EndTime = EndTime;
            this.ExamType = ExamType;
            this.Trainer = Trainer;
            this.StatusName = StatusName;
            this.StatusID = StatusID;
        }
    }
}
