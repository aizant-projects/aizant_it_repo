﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class JR_TrainedDocsObject
    {
        public int DocumentID { get; set; }
        public int EvaluatedStatusID { get; set; }
        public string Document { get; set; }
        public int DocVersion { get; set; }
        public string DepartmentName { get; set; }
        public int Attempts { get; set; }
        public string QualifiedDate { get; set; }
        public int Marks { get; set; }
        public string ReadDuration { get; set; }
        public string EvaluatedStatus { get; set; }

        public JR_TrainedDocsObject(int _DocumentID, int _EvaluatedStatusID, string _Document,int _DocVersion, string _DepartmentName, int _Attempts, string _QualifiedDate, int _Marks, string _ReadDuration, string _EvaluatedStatus)
        {
            DocumentID = _DocumentID;
            EvaluatedStatusID = _EvaluatedStatusID;
            Document = _Document;
            DocVersion = _DocVersion;
            DepartmentName = _DepartmentName;
            Attempts = _Attempts;
            QualifiedDate = _QualifiedDate;
            Marks = _Marks;
            ReadDuration = _ReadDuration;
            EvaluatedStatus = _EvaluatedStatus;
        }
    }
}
