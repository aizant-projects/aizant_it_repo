﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class ATC_PendingListObjects
    {
        public int ATC_ID { get; set; }
        public string DepartmentName { get; set; }
        public int CalendarYear { get; set; }
        public string Reviewer { get; set; }
        public string Approver { get; set; }
        public string CurrentStatus { get; set; }
        public int StatusID { get; set; }

        public ATC_PendingListObjects(int ATC_ID, string DepartmentName, int CalendarYear,
          string Reviewer, string Approver, string CurrentStatus, int StatusID)
        {
            this.ATC_ID = ATC_ID;
            this.DepartmentName = DepartmentName;
            this.CalendarYear = CalendarYear;
            this.Reviewer = Reviewer;
            this.Approver = Approver;
            this.CurrentStatus = CurrentStatus;
            this.StatusID = StatusID;
        }
    }
}
