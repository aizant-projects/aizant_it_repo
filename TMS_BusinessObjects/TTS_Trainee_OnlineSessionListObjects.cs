﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
    public class TTS_Trainee_OnlineSessionListObjects
    {
        public int TTS_ID { get; set; }
        public int TTS_OnlineTrainingSessionID { get; set; }
        public int TargetExamID { get; set; }
        public int DocumentID { get; set; }
        public int DocumentVersionID { get; set; }
        public string Document { get; set; }
        public string DeptCode { get; set; }
        public string DueDate { get; set; }
        public string Trainer { get; set; }
        public string Remarks { get; set; }
        public Boolean SelfComplete { get; set; }
        public string StatusName { get; set; }
        public int Status { get; set; }
        public TTS_Trainee_OnlineSessionListObjects(int TTS_ID, int TTS_OnlineTrainingSessionID,int TargetExamID, int DocumentID, int DocumentVersionID,
            string Document,string DeptCode, string DueDate,string Trainer,string Remarks,Boolean SelfComplete,string StatusName,int Status)
        {
            this.TTS_ID = TTS_ID;
            this.TTS_OnlineTrainingSessionID = TTS_OnlineTrainingSessionID;
            this.TargetExamID = TargetExamID;
            this.DocumentID = DocumentID;
            this.DocumentVersionID = DocumentVersionID;
            this.Document = Document;
            this.DeptCode = DeptCode;
            this.DueDate = DueDate;
            this.Trainer = Trainer;
            this.Remarks = Remarks;
            this.SelfComplete = SelfComplete;
            this.StatusName = StatusName;
            this.Status = Status;
        }
    }
}
