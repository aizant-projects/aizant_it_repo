﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class TTS_ListObjects
    {
        public int RowNumber { get; set; }
        public int TTS_ID { get; set; }
        public string DocumentName { get; set; }
        public string DeptCode { get; set; }
        public string TypeOfTraining { get; set; }
        public string TargetType { get; set; }
        public string Author { get; set; }
        public string Reviewer { get; set; }
        public string ApprovedDate { get; set; }
        public string StatusName { get; set; }

        public TTS_ListObjects(int RowNumber,int TTS_ID, string DocumentName, string DeptCode, string TypeOfTraining,string TargetType,
          string Author, string Reviewer, string ApprovedDate,string StatusName)
        {
            this.RowNumber = RowNumber;
            this.TTS_ID = TTS_ID;
            this.DocumentName = DocumentName;
            this.DeptCode = DeptCode;
            this.TypeOfTraining = TypeOfTraining;
            this.TargetType = TargetType;
            this.Author = Author;
            this.Reviewer = Reviewer;
            this.ApprovedDate = ApprovedDate;
            this.StatusName = StatusName;
        }
    }
}
