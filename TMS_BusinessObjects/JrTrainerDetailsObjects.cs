﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
    public class JrTrainerDetailsObjects
    {
       
        public string Department { get; set; }
        public string Document { get; set; }
        public string Trainer { get; set; }
        public int Days { get; set; }
        public JrTrainerDetailsObjects(string _Department, string _Document, string _Trainer,int _Days)
        {
            Department = _Department;
            Document = _Document;
            Trainer = _Trainer;
            Days = _Days;
        }
    }
}
