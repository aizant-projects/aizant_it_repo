﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class JR_TrainingRetrospectiveObjects
    {
        public int Dept_ID { get; set; }
        public string DepartmentName { get; set; }
        public int Document { get; set; }
        public JR_TrainingRetrospectiveObjects(int _Dept_ID, string _DepartmentName, int _Document)
        {
            Dept_ID = _Dept_ID;
            DepartmentName = _DepartmentName;
            Document = _Document;
        }
    }
}
