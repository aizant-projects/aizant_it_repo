﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects.TQ_OnDocs
{
    public class TQ_IDs
    {
        public int TQ_ID { get; set; }

        public TQ_IDs(int _TQ_ID)
        {
            TQ_ID = _TQ_ID;
        }
    }
}
