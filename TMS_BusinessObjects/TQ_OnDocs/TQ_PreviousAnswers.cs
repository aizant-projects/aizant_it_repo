﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects.TQ_OnDocs
{
    public class TQ_PreviousAnswers
    {
        public string Employee { get; set; }
        public string MarkedOn { get; set; }
        public string Answer { get; set; }
      
        public TQ_PreviousAnswers(string Employee, string MarkedOn, string Answer)
        {           
            this.Employee = Employee;
            this.MarkedOn = MarkedOn;
            this.Answer = Answer;
        }        
    }
}
