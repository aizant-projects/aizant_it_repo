﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects.TQ_OnDocs
{
    public class DocsWithTraineeQuestions
    {
        public int DocID { get; set; }
        public string Document { get; set; }
        public string Department { get; set; }
        public string TraineeQuestions { get; set; }
     
        public DocsWithTraineeQuestions(int DocID, string Document, string Department, string TraineeQuestions)
        {
            this.DocID = DocID;
            this.Document = Document;
            this.Department = Department;          
            this.TraineeQuestions = TraineeQuestions;
        }
    }
}
