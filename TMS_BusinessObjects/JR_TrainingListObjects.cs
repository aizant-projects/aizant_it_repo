﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
    public class JR_TrainingListObjects
    {
        public int JR_ID { get; set; }
        public string JR_Name { get; set; }
        public string JR_Version { get; set; }
        public string AssignedBy { get; set; }
        public string StatusName { get; set; }
        public int StatusID { get; set; }
        
        public JR_TrainingListObjects(int _JR_ID, string _JR_Name, string _JR_Version, string _AssignedBy, string _StatusName, int _StatusID)
        {
            JR_ID = _JR_ID;
            JR_Name = _JR_Name;
            JR_Version = _JR_Version;
            AssignedBy = _AssignedBy;
            StatusName = _StatusName;
            StatusID = _StatusID;            
        }
    }
}

