﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class AssignTraineeDept_Objects
    {
        public int DeptID { get; set; }
        public string DepartmentName { get; set; }
      
        public AssignTraineeDept_Objects(int DeptID, string DepartmentName)
        {
            this.DeptID = DeptID;
            this.DepartmentName = DepartmentName;
        }
    }
}
