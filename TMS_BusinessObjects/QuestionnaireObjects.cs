﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
    public class QuestionnaireObjects
    {
        public int createdOrModifiedBy { get; set; }
        public int QueID { get; set; }
        public string Operation { get; set; }
        public int DocID { get; set; }
        public string CurrentStatus { get; set; }
        public string Question_Descriptipon { get; set; }
        public string Question_Answer { get; set; }
    }
}
