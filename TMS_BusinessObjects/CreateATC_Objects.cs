﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
    public class CreateATC_Objects
    {
        public int InitializeATC_ID { get; set; }
        public int DeptID { get; set; }
        public string DepartmentName { get; set; }
        public int CalendarYear { get; set; }
        public string InitiatedBy { get; set; }
        public string InitiatedDate { get; set; }
       
        

        public CreateATC_Objects(int InitializeATC_ID,int DeptID, string DepartmentName, int CalendarYear,
          string InitiatedBy, string InitiatedDate)
        {
            this.InitializeATC_ID = InitializeATC_ID;
            this.DeptID = DeptID;
            this.DepartmentName = DepartmentName;
            this.CalendarYear = CalendarYear;
            this.InitiatedBy = InitiatedBy;
            this.InitiatedDate = InitiatedDate;
        }
    }
}
