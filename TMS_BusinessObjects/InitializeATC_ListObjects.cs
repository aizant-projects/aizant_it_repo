﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
     public class InitializeATC_ListObjects
    {
        public int ATC_ID { get; set; }
        public int DeptID { get; set; }
        public string DeptCode { get; set; }
        public string DeptName { get; set; }
        public int CalendarYear { get; set; }
        public String InitiatedBy { get; set; }
        public string InitiatedDate { get; set; }
        public int ATC_Author_ID { get; set; }
        public string Author { get; set; }
        public int ATC_HOD_Approval_ID { get; set; }
        public string Reviewer { get; set; }
        public int ATC_QA_Approval_ID { get; set; }
        public string Approver { get; set; }
        public int CurrentStatus { get; set; }

        public InitializeATC_ListObjects(int ATC_ID, int DeptID, string DeptCode, string DeptName, int CalendarYear, string InitiatedBy, string InitiatedDate,
          int ATC_Author_ID, string Author, int ATC_HOD_Approval_ID, string Reviewer, int ATC_QA_Approval_ID, string Approver,int CurrentStatus)
        {
            this.ATC_ID = ATC_ID;
            this.DeptID = DeptID;
            this.DeptCode = DeptCode;
            this.DeptName = DeptName;
            this.CalendarYear = CalendarYear;
            this.InitiatedBy = InitiatedBy;
            this.InitiatedDate = InitiatedDate;
            this.ATC_Author_ID = ATC_Author_ID;
            this.Author = Author;
            this.ATC_HOD_Approval_ID = ATC_HOD_Approval_ID;
            this.Reviewer = Reviewer;
            this.ATC_QA_Approval_ID = ATC_QA_Approval_ID;
            this.Approver = Approver;
            this.CurrentStatus = CurrentStatus;
        }
    }
}
