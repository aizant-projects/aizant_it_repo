﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class MyTrainingList_Objects
    {
        public int DocumentID { get; set; }
        public string DocumentName { get; set; }
        public string DocRefNo { get; set; }
        public string DeptName { get; set; }
        public string ModeOfTraining { get; set; }
        public string Opinion { get; set; }
        public string EvaluatedBy { get; set; }
        public string EvaluatedDate { get; set; }
        public string TrainingType { get; set; }
        public int DeptID { get; set; }
        public string ReadDuration { get; set; }
        public int EmpFeedbackID { get; set; }
        public int CreatedYear { get; set; }

        public MyTrainingList_Objects(int DocumentID, string DocumentName, string DocRefNo, string DeptName, string ModeOfTraining, string Opinion, string EvaluatedBy,
          string EvaluatedDate, string TrainingType, int DeptID, string ReadDuration, int EmpFeedbackID,int CreatedYear)
        {
            this.DocumentID = DocumentID;
            this.DocumentName = DocumentName;
            this.DocRefNo = DocRefNo;
            this.DeptName = DeptName;
            this.ModeOfTraining = ModeOfTraining;
            this.Opinion = Opinion;
            this.EvaluatedBy = EvaluatedBy;
            this.EvaluatedDate = EvaluatedDate;
            this.TrainingType = TrainingType;
            this.DeptID = DeptID;
            this.ReadDuration = ReadDuration;
            this.EmpFeedbackID = EmpFeedbackID;
            this.CreatedYear = CreatedYear;
        }
    }
}
