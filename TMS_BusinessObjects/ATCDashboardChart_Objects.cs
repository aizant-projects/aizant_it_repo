﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class ATCDashboardChart_Objects
    {
        public string MonthName { get; set; }
        public int Pending { get; set; }
        public int Completed { get; set; }

        public ATCDashboardChart_Objects(string MonthName, int Pending, int Completed)
        {
            this.MonthName = MonthName;
            this.Pending = Pending;
            this.Completed = Completed;
        }
    }
}
