﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
    public  class JR_ReviewListObjects
    {
        public int JR_ID { get; set; }
        public string JR_Name { get; set; }
        public string JR_Version { get; set; }
        public string EmpCode { get; set; }
        public string EmpName { get; set; }
        public string DeptCode { get; set; }
        public string DesignationName { get; set; }
        public string HOD_AssignedDate { get; set; }
        public string TraineeAcceptedDate { get; set; }
        public string DeclinedBy { get; set; }
        public string QARevertedDate { get; set; }
        public string Approver { get; set; }
        public string StatusName { get; set; }
        public int StatusID { get; set; }


        public JR_ReviewListObjects(int _JR_ID, string _JR_Name, string _JR_Version, string _EmpCode, string _EmpName, string _DeptCode, string _DesignationName, string _HOD_AssignedDate, string _TraineeAcceptedDate, string _DeclinedBy, string _QARevertedDate, string _Approver, string _StatusName, int _StatusID)
        {
            JR_ID = _JR_ID;
            JR_Name = _JR_Name;
            JR_Version = _JR_Version;
            EmpCode = _EmpCode;
            EmpName = _EmpName;
            DeptCode = _DeptCode;
            DesignationName = _DesignationName;
            HOD_AssignedDate = _HOD_AssignedDate;
            TraineeAcceptedDate = _TraineeAcceptedDate;
            DeclinedBy = _DeclinedBy;
            QARevertedDate = _QARevertedDate;
            Approver = _Approver;
            StatusName = _StatusName;
            StatusID = _StatusID;
        }
    }
}
