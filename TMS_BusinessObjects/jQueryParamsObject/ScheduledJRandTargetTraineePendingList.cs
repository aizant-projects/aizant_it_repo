﻿using API_BussinessObjects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace TMS_BusinessObjects.jQueryParamsObject
{
   public class ScheduledJRandTargetTraineePendingList: BasicJQ_DT_BO
    {
        public int HeadEmpID { get; set; }
        public ScheduledJRandTargetTraineePendingList()
        {
            HeadEmpID = int.Parse(HttpContext.Current.Request.Params["HeadEmpID"]);
        }
        public int TraineeID { get; set; }
        public string Trainee { get; set; }
        public string Department { get; set; }
        public int TrainingPendingCount { get; set; }
        public ScheduledJRandTargetTraineePendingList(int _TraineeID, string _Trainee,string _Department, int _TrainingPendingCount)
        {
            TraineeID = _TraineeID;
            Trainee = _Trainee;
            Department = _Department;
            TrainingPendingCount = _TrainingPendingCount;
        }
    }
}
