﻿using API_BussinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects.jQueryParamsObject
{
    public class TrainerEvaluationListObject : BasicJQ_DT_BO
    {
        //For Disposing The Objects
        //private bool disposed = false;
        //protected override void Dispose(bool disposing)
        //{
        //    if (!disposed)
        //    {
        //        if (disposing)
        //        {
        //            TTS_TrainingSessionID = 0;
        //            EvaluateOn = 0;
        //        }
        //        disposed = true;
        //    }
        //    base.Dispose(disposing);
        //}

        public int TTS_TrainingSessionID { get; set; }
        public int EvaluateOn { get; set; }
        //For SingleTon DP
        public TrainerEvaluationListObject()
        {
            TTS_TrainingSessionID = int.Parse(System.Web.HttpContext.Current.Request.Params["TTS_TrainingSessionID"]);
            EvaluateOn = int.Parse(System.Web.HttpContext.Current.Request.Params["EvaluateOn"]);
        }
        //public static TrainerEvaluationListObject ClassObjTrainerEvaluationListObject;
        //public static TrainerEvaluationListObject GetTrainerEvaluationListObjectInstance()
        //{
        //    // if (ClassObjTrainerEvaluationListObject == null)
        //    ClassObjTrainerEvaluationListObject = null;
        //            ClassObjTrainerEvaluationListObject = new TrainerEvaluationListObject();
        //    return ClassObjTrainerEvaluationListObject;
        //}
    }
}
