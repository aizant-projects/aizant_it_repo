﻿using API_BussinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects.jQueryParamsObject
{
   public class TrainingPerformanceListObjects : BasicJQ_DT_BO
    {
        public int EmpID { get; set; }
        public int DeptID { get; set; }
        public TrainingPerformanceListObjects()
        {
            EmpID = int.Parse(System.Web.HttpContext.Current.Request.Params["EmpID"]);
            DeptID = int.Parse(System.Web.HttpContext.Current.Request.Params["DeptID"]);
        }
    }
}
