﻿using API_BussinessObjects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace TMS_BusinessObjects.jQueryParamsObject
{
   public class ScheduledJRandTT_PendingListObjects: BasicJQ_DT_BO
    {
        public int TraineeID { get; set; }
        public string SearchTrainee { get; set; }
        public string SearchDocument { get; set; }
        public string SearchDocVersion { get; set; }
        public string SearchDepartment { get; set; }
        public string SarchTypeOfTraining { get; set; }
        public ScheduledJRandTT_PendingListObjects()
        {
            TraineeID = int.Parse(HttpContext.Current.Request.Params["TraineeID"]);
            SearchTrainee = HttpContext.Current.Request.Params["sSearch_0"].ToString(CultureInfo.CurrentCulture);
            SearchDocument = HttpContext.Current.Request.Params["sSearch_1"].ToString(CultureInfo.CurrentCulture);
            SearchDocVersion = HttpContext.Current.Request.Params["sSearch_2"].ToString(CultureInfo.CurrentCulture);
            SearchDepartment = HttpContext.Current.Request.Params["sSearch_3"].ToString(CultureInfo.CurrentCulture);
            SarchTypeOfTraining = HttpContext.Current.Request.Params["sSearch_4"].ToString(CultureInfo.CurrentCulture);
        }
        public string Trainee { get; set; }
        public string Document { get; set; }
        public string DocumentVersion { get; set; }
        public string DocumentDepartment { get; set; }
        public string TrainingType { get; set; }
        public ScheduledJRandTT_PendingListObjects(string _Trainee, string _Document, string _DocumentVersion, string _DocumentDepartment, string _TrainingType)
        {
            Trainee = _Trainee;
            Document = _Document;
            DocumentVersion = _DocumentVersion;
            DocumentDepartment = _DocumentDepartment;
            TrainingType = _TrainingType;
        }
    }
}
