﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
     public class TTS_Trainee_SessionListObjects
     {
        public int TTS_ID { get; set; }
        public int TTS_ClassRoomTrainingSessionID { get; set; }
        public int TargetExamID { get; set; }
        public int DocumentID { get; set; }
        public int DocumentVersionID { get; set; }
        public string Document { get; set; }
        public string DeptCode { get; set; }
        public string TrainingDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Trainer { get; set; }
        public string Examtype { get; set; }
        public string Remarks { get; set; }
        public string TypeOfTraining { get; set; }
        public string StatusName { get; set; }
        public int EmpFeedbackID { get; set; }
        public TTS_Trainee_SessionListObjects( int TTS_ID,int TTS_ClassRoomTrainingSessionID,int TargetExamID, 
            int DocumentID,int DocumentVersionID, string Document, string DeptCode, string TrainingDate,
            string StartTime, string EndTime, string Trainer, string Examtype, string Remarks, 
            string TypeOfTraining, string StatusName,int EmpFeedbackID)
        {
            this.TTS_ID = TTS_ID;
            this.TTS_ClassRoomTrainingSessionID = TTS_ClassRoomTrainingSessionID;
            this.TargetExamID = TargetExamID;
            this.DocumentID = DocumentID;
            this.DocumentVersionID = DocumentVersionID;
            this.Document = Document;
            this.DeptCode = DeptCode;
            this.TrainingDate = TrainingDate;
            this.StartTime = StartTime;
            this.EndTime = EndTime;
            this.Trainer = Trainer;
            this.Examtype = Examtype;
            this.Remarks = Remarks;
            this.TypeOfTraining = TypeOfTraining;
            this.StatusName = StatusName;
            this.EmpFeedbackID = EmpFeedbackID;
        }
    }
}
