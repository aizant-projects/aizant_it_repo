﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class DocTraineeRaisedQuestions
    {
        public string QuestionID { get; set; }
        public string QuestionDescription { get; set; }
        public string QuestionRaisedBy { get; set; }
        public string QuestionRaisedDate { get; set; }
        public string QuestionAnsweredBy { get; set; }
        public string QuestionAnsweredDate { get; set; }
        public string IsQuestionMarkedAsFAQ { get; set; }
        public  string IsPreviousQuestionsShow { get; set; }

    }
}
