﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
    public class EmpJrObjects
    {       
        public int EmpID { get; set; }
        public int DeptID { get; set; }
        public int JrID { get; set; }
        public int JR_TemplateID { get; set; }
        public int CurrentStatus { get; set; }
        public string Operation { get; set; }
        public string Jr_Name { get; set; }
        public string JrDescription { get; set; }
        public int AssignedBy { get; set; }
        public int ApprovedOrDeclinedBy { get; set; }
        public string Remarks { get; set; }
        public string DeclinedReason { get; set; }

        public string RQ_JustificationNote { get; set; }

        public string CommentDate { get; set; }

    }   
}
