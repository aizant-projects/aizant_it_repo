﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects.ML_Objects
{
    public class TrainingPerformanceObjects
    {
        public string EmpName { get; set; }
        public string Avg_Duration { get; set; }
        public int Avg_Marks { get; set; }
        public int No_of_times_Failed { get; set; }
        public int No_of_Documents { get; set; }
        public int Overall_Performance { get; set; }
      
        public TrainingPerformanceObjects(string EmpName, string Avg_Duration, int Avg_Marks, int No_of_times_Failed, int No_of_Documents,
        int Overall_Performance)
        {
            this.EmpName = EmpName;
            this.Avg_Duration = Avg_Duration;
            this.Avg_Marks = Avg_Marks;
            this.No_of_times_Failed = No_of_times_Failed;
            this.No_of_Documents = No_of_Documents;
            this.Overall_Performance = Overall_Performance;
        }
    }
}
