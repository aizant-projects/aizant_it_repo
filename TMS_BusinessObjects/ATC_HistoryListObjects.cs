﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
    public class ATC_HistoryListObjects
    {
        public int ATC_HistoryID { get; set; }
        public string Action { get; set; }
        public string Action_By { get; set; }
        public string Role { get; set; }
        public string ActionDate { get; set; }
        public string Remarks { get; set; }
       
        public ATC_HistoryListObjects(int ATC_HistoryID, string Action, string Action_By, string Role, string ActionDate,string Remarks)
        {
            this.ATC_HistoryID = ATC_HistoryID;
            this.Action = Action;
            this.Action_By = Action_By;
            this.Role = Role;
            this.ActionDate = ActionDate;
            this.Remarks = Remarks;
        }
    }
}
