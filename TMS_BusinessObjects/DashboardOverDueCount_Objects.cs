﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class DashboardOverDueCount_Objects
    {
        public string EventName { get; set; }
        public int OverDue { get; set; }

        public DashboardOverDueCount_Objects(string EventName, int OverDue)
        {
            this.EventName = EventName;
            this.OverDue = OverDue;
           
        }
    }
}
