﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
   public class JR_EvaluatorObject
    {
        public int JR_ID { get; set; }
        public int DeptID { get; set; }
        public int EvaluatroHodEmpID { get; set; }
        public string JR_Name { get; set; }
        public string JR_Version { get; set; }
        public string Department { get; set; }
        public string TraineeName { get; set; }
        public string EvaluatorHodName { get; set; }

        public JR_EvaluatorObject(int _JR_ID, int _DeptID, int _EvaluatroHodEmpID, string _JR_Name, string _JR_Version, string _Department,
          string _TraineeName, string _EvaluatorHodName)
        {
            JR_ID = _JR_ID;
            DeptID = _DeptID;
            EvaluatroHodEmpID = _EvaluatroHodEmpID;
            JR_Name = _JR_Name;
            JR_Version = _JR_Version;
            Department = _Department;
            TraineeName = _TraineeName;
            EvaluatorHodName = _EvaluatorHodName;
        }
    }
}
