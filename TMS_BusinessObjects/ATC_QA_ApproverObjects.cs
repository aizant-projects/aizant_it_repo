﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_BusinessObjects
{
     public class ATC_QA_ApproverObjects
    {
        public int ATC_ID { get; set; }
        public string DepartmentName { get; set; }
        public int CalendarYear { get; set; }
        public string Author { get; set; }
        public string Reviewer { get; set; }
        public string CurrentStatus { get; set; }
        public int StatusID { get; set; }

        public ATC_QA_ApproverObjects(int ATC_ID, string DepartmentName, int CalendarYear,
          string Author, string Reviewer, string CurrentStatus, int StatusID)
        {
            this.ATC_ID = ATC_ID;
            this.DepartmentName = DepartmentName;
            this.CalendarYear = CalendarYear;
            this.Author = Author;
            this.Reviewer = Reviewer;
            this.CurrentStatus = CurrentStatus;
            this.StatusID = StatusID;
        }
    }
}
