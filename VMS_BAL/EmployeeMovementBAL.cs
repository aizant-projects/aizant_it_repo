﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using VMS_BO;
using VMS_DAL;

namespace VMS_BAL
{
    public class EmployeeMovementBAL
    {
        EmployeeMovementDAL objEmployeeMovementDAL = new EmployeeMovementDAL();

        //Staff Movement BAL

        public int StaffMovement_Insert(SupplyRegisterBO objSupplyRegisterBO)
        {
            return objEmployeeMovementDAL.StaffMovement_Insert(objSupplyRegisterBO);
        }

        public DataTable SearchFilterStaff(SupplyRegisterBO objSupplyRegisterBO)
        {
            return objEmployeeMovementDAL.SearchFilterStaff(objSupplyRegisterBO);
        }

        public int Update_StaffRegisterEdit(SupplyRegisterBO objSupplyRegisterBO)
        {
            return objEmployeeMovementDAL.Update_StaffRegisterEdit(objSupplyRegisterBO);
        }

        public DataTable BindDataStaffMovementRegister()
        {
            return objEmployeeMovementDAL.BindDataStaffMovementRegister();
        }

        public DataTable StaffRegisterlinkbuttonFilldata(SupplyRegisterBO objSupplyRegisterBO)
        {
            return objEmployeeMovementDAL.StaffRegisterlinkbuttonFilldata(objSupplyRegisterBO);
        }

        public int Checkout(SupplyRegisterBO objSupplyRegisterBO)
        {
            return objEmployeeMovementDAL.Checkout(objSupplyRegisterBO);
        }


        //Holiday Register BAL

        public int HolidayRegister_Insert(SupplyRegisterBO objSupplyRegisterBO)
        {
            return objEmployeeMovementDAL.HolidayRegister_Insert(objSupplyRegisterBO);
        }

        public DataTable SearchFilterHoliday(SupplyRegisterBO objSupplyRegisterBO)
        {
            return objEmployeeMovementDAL.SearchFilterHoliday(objSupplyRegisterBO);
        }

        public int Update_HolidayRegisterEdit(SupplyRegisterBO objSupplyRegisterBO)
        {
            return objEmployeeMovementDAL.Update_HolidayRegisterEdit(objSupplyRegisterBO);
        }

        public DataTable BindDataHolidayRegister()
        {
            return objEmployeeMovementDAL.BindDataHolidayRegister();
        }

        public DataTable HolidayRegisterlinkbuttonFilldata(SupplyRegisterBO objSupplyRegisterBO)
        {
            return objEmployeeMovementDAL.HolidayRegisterlinkbuttonFilldata(objSupplyRegisterBO);
        }

        public int CheckoutHoliday(SupplyRegisterBO objSupplyRegisterBO)
        {
            return objEmployeeMovementDAL.CheckoutHoliday(objSupplyRegisterBO);
        }

        public DataTable BindEmpMovementType()
        {
            return objEmployeeMovementDAL.BindEmpMovementType();
        }
    }
}
