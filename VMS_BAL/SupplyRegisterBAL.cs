﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using VMS_BO;
using VMS_DAL;

namespace VMS_BAL
{
    public class SupplyRegisterBAL
    {
        SupplyRegisterDAL objSupplyRegisterDAL = new SupplyRegisterDAL();

        //Snacks Supply BAL

        public int Snacks_Insert(SupplyRegisterBO objSupplyRegisterBO)
        {
            return objSupplyRegisterDAL.Snacks_Insert(objSupplyRegisterBO);
        }

        public DataTable SearchFilterSnacksSupply(SupplyRegisterBO objSupplyRegisterBO)
        {
            return objSupplyRegisterDAL.SearchFilterSnacksSupply(objSupplyRegisterBO);
        }

        public int Update_SnacksSupplyRegisterEdit(SupplyRegisterBO objSupplyRegisterBO)
        {
            return objSupplyRegisterDAL.Update_SnacksSupplyRegisterEdit(objSupplyRegisterBO);
        }

        public DataTable BindDataSnacksSupplyRegister()
        {
            return objSupplyRegisterDAL.BindDataSnacksSupplyRegister();
        }

        public DataTable SnacksSupplyRegisterlinkbuttonFilldata(SupplyRegisterBO objSupplyRegisterBO)
        {
            return objSupplyRegisterDAL.SnacksSupplyRegisterlinkbuttonFilldata(objSupplyRegisterBO);
        }

        public DataTable BindSupplyType()
        {
            return objSupplyRegisterDAL.BindSupplyType();
        }



        //Key Supply BAL

        public int Key_Insert(SupplyRegisterBO objSupplyRegisterBO)
        {
            return objSupplyRegisterDAL.Key_Insert(objSupplyRegisterBO);
        }

        public DataTable SearchFilterKeySupply(SupplyRegisterBO objSupplyRegisterBO)
        {
            return objSupplyRegisterDAL.SearchFilterKeySupply(objSupplyRegisterBO);
        }

        public int Update_KeySupplyRegisterEdit(SupplyRegisterBO objSupplyRegisterBO)
        {
            return objSupplyRegisterDAL.Update_KeySupplyRegisterEdit(objSupplyRegisterBO);
        }

        public DataTable BindDataKeySupplyRegister()
        {
            return objSupplyRegisterDAL.BindDataKeySupplyRegister();
        }

        public DataTable KeySupplyRegisterlinkbuttonFilldata(SupplyRegisterBO objSupplyRegisterBO)
        {
            return objSupplyRegisterDAL.KeySupplyRegisterlinkbuttonFilldata(objSupplyRegisterBO);
        }

        public int CheckoutOutwardKey(SupplyRegisterBO objSupplyRegisterBO)
        {
            return objSupplyRegisterDAL.CheckoutOutwardKey(objSupplyRegisterBO);
        }


        //AccessCard Supply BAL

        public int Access_Insert(SupplyRegisterBO objSupplyRegisterBO)
        {
            return objSupplyRegisterDAL.Access_Insert(objSupplyRegisterBO);
        }

        public DataTable SearchFilterAccessCardSupply(SupplyRegisterBO objSupplyRegisterBO)
        {
            return objSupplyRegisterDAL.SearchFilterAccessCardSupply(objSupplyRegisterBO);
        }

        public int Update_AccessCardSupplyRegisterEdit(SupplyRegisterBO objSupplyRegisterBO)
        {
            return objSupplyRegisterDAL.Update_AccessCardSupplyRegisterEdit(objSupplyRegisterBO);
        }

        public DataTable BindDataAccessCardSupplyRegister()
        {
            return objSupplyRegisterDAL.BindDataAccessCardSupplyRegister();
        }

        public DataTable AccessCardSupplyRegisterlinkbuttonFilldata(SupplyRegisterBO objSupplyRegisterBO)
        {
            return objSupplyRegisterDAL.AccessCardSupplyRegisterlinkbuttonFilldata(objSupplyRegisterBO);
        }

        public int CheckoutOutwardAccess(SupplyRegisterBO objSupplyRegisterBO)
        {
            return objSupplyRegisterDAL.CheckoutOutwardAccess(objSupplyRegisterBO);
        }
    }
}
