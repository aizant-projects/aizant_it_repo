﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using VMS_BO;
using VMS_DAL;


namespace VMS_BAL
{
    public class VMSRolePrivilegesBAL
    {
        VMSRolePrivilegesDAL objVMSRolePrivilegesDAL = new VMSRolePrivilegesDAL();
        public DataTable GetPrivileIDAndNames()
        {
            return objVMSRolePrivilegesDAL.GetPrivileIDAndNames();
        }

        public DataTable GetRoles()
        {
            return objVMSRolePrivilegesDAL.GetRoles();
        }

        public int InsertRolePrivileges(List<RolePrivilegesBO> lstRolePrivilegesBO)
        {
            return objVMSRolePrivilegesDAL.InsertRolePrivileges(lstRolePrivilegesBO);
        }

        public DataTable GetRolePrivileges(RolePrivilegesBO objRolePrivilegesBO)
        {
            return objVMSRolePrivilegesDAL.GetRolePrivileges(objRolePrivilegesBO);
        }
        public DataTable GetMenuItems(RolePrivilegesBO objRolePrivilegesBO)
        {
            return objVMSRolePrivilegesDAL.GetMenuItems(objRolePrivilegesBO);
        }
        public DataTable GetEmpRoleID(RolePrivilegesBO objRolePrivilegesBO)
        {
            return objVMSRolePrivilegesDAL.GetEmpRoleID(objRolePrivilegesBO);
        }
        }
}
