﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using VMS_BO;
using VMS_DAL;


namespace VMS_BAL
{
    public class ServiceRegisterBAL
    {
        ServiceRegisterDAL objServiceRegisterDAL = new ServiceRegisterDAL();

        public int ServiceRegister_Insert(ServiceAndVechileBO objServiceAndVechileBO)
        {
            return objServiceRegisterDAL.ServiceRegister_Insert(objServiceAndVechileBO);
        }

        public DataTable SearchFilterService(ServiceAndVechileBO objServiceAndVechileBO)
        {
            return objServiceRegisterDAL.SearchFilterService(objServiceAndVechileBO);
        }

        public int Update_ServiceRegisterEdit(ServiceAndVechileBO objServiceAndVechileBO)
        {
            return objServiceRegisterDAL.Update_ServiceRegisterEdit(objServiceAndVechileBO);
        }

        public DataTable BindDataServiceRegister()
        {
            return objServiceRegisterDAL.BindDataServiceRegister();
        }

        public DataTable ServiceRegisterlinkbuttonFilldata(ServiceAndVechileBO objServiceAndVechileBO)
        {
            return objServiceRegisterDAL.ServiceRegisterlinkbuttonFilldata(objServiceAndVechileBO);
        }

        public DataTable BindDataServiceType()
        {
            return objServiceRegisterDAL.BindDataServiceType();
        }

        public int ServiceTypeMasterInsert(ServiceAndVechileBO objServiceAndVechileBO)
        {
            return objServiceRegisterDAL.ServiceTypeMasterInsert(objServiceAndVechileBO);
        }

        public int CheckoutService(ServiceAndVechileBO objServiceAndVechileBO)
        {
            return objServiceRegisterDAL.CheckoutService(objServiceAndVechileBO);
        }

        public int ServiceTypeifExistsorNot(string ServiceType)
        {
            return objServiceRegisterDAL.ServiceTypeifExistsorNot(ServiceType);
        }
    }
}
