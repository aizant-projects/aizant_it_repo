﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using VMS_BO;
using VMS_DAL;
using System.Data.SqlClient;

namespace VMS_BAL
{
    public class VisitorRegisterBAL
    {
        VisitorRegisterDAL objVisitorRegisterDAL = new VisitorRegisterDAL();

        public int Visitor_Insert(VisitorRegisterBO objvisitor)
        {
            return objVisitorRegisterDAL.Visitor_Insert(objvisitor);
        }

        public DataTable SearchFilterVisitor(VisitorRegisterBO objvisitor)
        {
            return objVisitorRegisterDAL.SearchFilterVisitor(objvisitor);
        }

        public int Update_Visitor(VisitorRegisterBO objvisitor)
        {
            return objVisitorRegisterDAL.Update_Visitor(objvisitor);
        }

        public DataTable BindDataVisitor()
        {
            return objVisitorRegisterDAL.BindDataVisitor();
        }

        //public DataTable VisitorlinkbuttonFilldata(VisitorRegisterBO objvisitor)
        //{
        //    return objVisitorRegisterDAL.loLoad(objvisitor);
        //}

        public int PurposeOfVisitUpdate(VisitorRegisterBO objvisitor)
        {
            return objVisitorRegisterDAL.PurposeOfVisitUpdate(objvisitor);
        }

        public int BookAppointment(VisitorAppointmentBookingsBO objAppointment)
        {
            return objVisitorRegisterDAL.BookAppointment(objAppointment);
        }

        public int IDProofTypeUpdate(VisitorRegisterBO objvisitor)
        {
            return objVisitorRegisterDAL.IDProofTypeUpdate(objvisitor);
        }

        public DataTable LoadddlIDProofType()
        {
            return objVisitorRegisterDAL.LoadddlIDProofType();
        }

        public DataTable LoadDepartment()
        {
            return objVisitorRegisterDAL.LoadDepartment();
        }

        public DataTable LoadPurposeofVisit()
        {
            return objVisitorRegisterDAL.LoadPurposeofVisit();
        }

        public DataTable GetAppointmentData(VisitorAppointmentBookingsBO objVisitorAppointmentBookingBO)
        {
            return objVisitorRegisterDAL.GetAppointmentData(objVisitorAppointmentBookingBO);
        }

        public DataTable GetAppointmentDataBind(VisitorAppointmentBookingsBO objVisitorAppointmentBookingBO)
        {
            return objVisitorRegisterDAL.GetAppointmentDataBind(objVisitorAppointmentBookingBO);
        }

        public string VisitorAutoGenerateID()
        {
            return objVisitorRegisterDAL.VisitorAutoGenerateID();
        }

        public string AppointmentVisitorAutoGenerateID()
        {
            return objVisitorRegisterDAL.AppointmentVisitorAutoGenerateID();
        }

        public int CheckoutVisitor(VisitorRegisterBO objvisitorRegisterBO)
        {
            return objVisitorRegisterDAL.CheckoutVisitor(objvisitorRegisterBO);
        }

        public DataTable BindCountriesBAL()
        {
            return objVisitorRegisterDAL.BindCountriesDAL();
        }

        public DataTable BindStatesBAL(int CountryID)
        {
            return objVisitorRegisterDAL.BindStatesDAL(CountryID);
        }

        public DataTable BindCities(int StateID)
        {
            return objVisitorRegisterDAL.BindCities(StateID);
        }

        public DataTable GetEmpData(int DeptID)
        {
            return objVisitorRegisterDAL.GetEmpData(DeptID);
        }

        public DataSet VisitorReport(string VistorID)
        {
            return objVisitorRegisterDAL.VisitorReports(VistorID);
        }

        public int IDCardTypeifExistsorNot(string IDCardType)
        {
            return objVisitorRegisterDAL.IDCardTypeifExistsorNot(IDCardType);
        }

        public int PurposeofVisitifExistsorNot(string Purpose)
        {
            return objVisitorRegisterDAL.PurposeofVisitifExistsorNot(Purpose);
        }

        public DataTable SearchFilterAppointment(VisitorAppointmentBookingsBO objVisitorAppointmentBookingBO)
        {
            return objVisitorRegisterDAL.SearchFilterAppointment(objVisitorAppointmentBookingBO);
        }

        public int Update_Appointment(VisitorAppointmentBookingsBO objVisitorAppointmentBookingBO)
        {
            return objVisitorRegisterDAL.Update_Appointment(objVisitorAppointmentBookingBO);
        }

        //public DataTable AppointmentlinkbuttonFilldata(VisitorAppointmentBookingsBO objVisitorAppointmentBookingBO)
        //{
        //    return objVisitorRegisterDAL.AppointmentlinkbuttonFilldata(objVisitorAppointmentBookingBO);
        //}

        public DataTable AutoComplete(string Name)
        {
            return objVisitorRegisterDAL.AutoComplete(Name);
        }

        public int AppointmentCancel(VisitorAppointmentBookingsBO objVisitorAppointmentBookingBO)
        {
            return objVisitorRegisterDAL.AppointmentCancel(objVisitorAppointmentBookingBO);
        }
        #region Charts and Export
        public DataTable LoadChartNoofVisitors(VisitorRegisterBO objVisitorRegisterBO)
        {
            return objVisitorRegisterDAL.LoadChartNoofVisitors(objVisitorRegisterBO);
        }

        public DataTable LoadChartPurposeofVisit(VisitorRegisterBO objVisitorRegisterBO)
        {
            return objVisitorRegisterDAL.LoadChartPurposeofVisit(objVisitorRegisterBO);
        }
        public DataTable LoadChartVehicle(VisitorRegisterBO objVisitorRegisterBO)
        {
            return objVisitorRegisterDAL.LoadChartVehicle(objVisitorRegisterBO);
        }
        public DataTable GetExportData(VisitorRegisterBO objVisitorRegisterBO)
        {
            return objVisitorRegisterDAL.GetExportData(objVisitorRegisterBO);
        }
        #endregion Charts and Export
        public DataTable TestMethodBal()
        {
            return objVisitorRegisterDAL.TestMethod();
        }

        public DataTable GetVisitorDetails_List(JQDatatableBO _objJQDataTableBO)
        {
            return objVisitorRegisterDAL.GetVisitorDetails_ListDb(_objJQDataTableBO);
        }

        public DataTable GetVisitorAppointmentDetails_List(JQDatatableBO _objJQDataTableBO)
        {
            return objVisitorRegisterDAL.GetVisitorAppointmentDetails_ListDb(_objJQDataTableBO);
        }

        public DataTable GetVisitorChartDetails_ListDb(JQDatatableBO _objJQDataTableBO, string FromDate, string ToDate, int Mode, int FromYear, int ToYear, int FromMonth, int ToMonth, string purposeofvisit, string TimePeriod)
        {
            return objVisitorRegisterDAL.GetVisitorChartDetails_ListDb(_objJQDataTableBO,FromDate, ToDate, Mode, FromYear, ToYear, FromMonth, ToMonth, purposeofvisit, TimePeriod);
        }
    }
}
