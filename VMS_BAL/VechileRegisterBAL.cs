﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using VMS_BO;
using VMS_DAL;


namespace VMS_BAL
{
    public class VechileRegisterBAL
    {
        VechileRegisterDAL objVechileRegisterDAL = new VechileRegisterDAL();

        public int VechileReading_Insert(ServiceAndVechileBO objServiceAndVechileBO)
        {
            return objVechileRegisterDAL.VechileReading_Insert(objServiceAndVechileBO);
        }

        public int Update_VechileRegister(ServiceAndVechileBO objServiceAndVechileBO)
        {
            return objVechileRegisterDAL.Update_VechileRegister(objServiceAndVechileBO);
        }

        public DataTable SearchFilterVechileReading(ServiceAndVechileBO objServiceAndVechileBO)
        {
            return objVechileRegisterDAL.SearchFilterVechileReading(objServiceAndVechileBO);
        }

        //New List in VechicleRegisterList
        public DataTable SearchFilterVechileReadingDetails(ServiceAndVechileBO objServiceAndVechileBO)
        {
            return objVechileRegisterDAL.SearchFilterVechileReadingDetails(objServiceAndVechileBO);
        }

        public DataTable BindDataVechile()
        {
            return objVechileRegisterDAL.BindDataVechile();
        }

        public DataTable VechilelinkbuttonFilldata(ServiceAndVechileBO objServiceAndVechileBO)
        {
            return objVechileRegisterDAL.VechilelinkbuttonFilldata(objServiceAndVechileBO);
        }

        public DataTable LoadVechileNo()
        {
            return objVechileRegisterDAL.LoadVechileNo();
        }

        public int CheckoutVechile(ServiceAndVechileBO objServiceAndVechileBO)
        {
            return objVechileRegisterDAL.CheckoutVechile(objServiceAndVechileBO);
        }

        public DataTable GetDriverNames(ServiceAndVechileBO objServiceAndVechileBO)
        {
            return objVechileRegisterDAL.GetDriverNames(objServiceAndVechileBO);
        }

        public int AddVechlieNoMaster(ServiceAndVechileBO objServiceAndVechileBO)
        {
            return objVechileRegisterDAL.AddVechlieNoMaster(objServiceAndVechileBO);
        }

        public int VechileNoifExistsorNot(string VechileNo)
        {
            return objVechileRegisterDAL.VechileNoifExistsorNot(VechileNo);
        }

        public DataTable GetVechile_Details(JQDatatableBO _objJQDataTableBO)
        {
            return objVechileRegisterDAL.GetVechileDetails_ListDb(_objJQDataTableBO);
        }
    }
}
