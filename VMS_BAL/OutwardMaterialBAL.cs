﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using VMS_BO;
using VMS_DAL;


namespace VMS_BAL
{
    public class OutwardMaterialBAL
    {
        OutwardMaterialDAL objOutwardMaterialDAL = new OutwardMaterialDAL();

        public int InsertMaterial(MaterialBO objMaterialBO)
        {
            return objOutwardMaterialDAL.InsertMaterial(objMaterialBO);
        }

        public DataTable SearchFilter(MaterialBO objMaterialBO)
        {
            return objOutwardMaterialDAL.SearchFilter(objMaterialBO);
        }

        public int Update_OutwardMaterialEdit(MaterialBO objMaterialBO)
        {
            return objOutwardMaterialDAL.Update_OutwardMaterialEdit(objMaterialBO);
        }

        public DataTable OutwardMateriallinkbuttonFilldata(MaterialBO objMaterialBO)
        {
            return objOutwardMaterialDAL.OutwardMateriallinkbuttonFilldata(objMaterialBO);
        }

        public int CheckoutOutwardMaterial(MaterialBO objMaterialBO)
        {
            return objOutwardMaterialDAL.CheckoutOutwardMaterial(objMaterialBO);
        }

        public DataTable BindUnits()
        {
            return objOutwardMaterialDAL.BindUnits();
        }

        public DataTable BindMaterialType()
        {
            return objOutwardMaterialDAL.BindMaterialType();
        }

        public int MaterialRejectOrCancel(MaterialBO objMaterialBO)
        {
            return objOutwardMaterialDAL.MaterialRejectOrCancel(objMaterialBO);
        }

        public int MaterialApproval(MaterialBO objMaterialBO)
        {
            return objOutwardMaterialDAL.MaterialApproval(objMaterialBO);
        }

        public int MaterialStoreRejectOrCancel(MaterialBO objMaterialBO)
        {
            return objOutwardMaterialDAL.MaterialStoreRejectOrCancel(objMaterialBO);
        }

        public int MaterialStoreApproval(MaterialBO objMaterialBO)
        {
            return objOutwardMaterialDAL.MaterialStoreApproval(objMaterialBO);
        }

        public DataTable GetCardCount(MaterialBO objMaterialBO)
        {
            return objOutwardMaterialDAL.GetCardCount(objMaterialBO);
        }

        public string AutoGenerateGP_DC_NO(MaterialBO objMaterialBO)
        {
            return objOutwardMaterialDAL.AutoGenerateGP_DC_NO(objMaterialBO);
        }

        public DataTable GetOutwardMaterialList(JQDatatableBO _objJQDataTableBO)
        {
            return objOutwardMaterialDAL.GetOutwardMaterialListDb(_objJQDataTableBO);
        }

        public DataTable GetManagerOutwardMaterialList(JQDatatableBO objJQDataTable_BO)
        {
            return objOutwardMaterialDAL.GetManagerOutwardMaterialListDb(objJQDataTable_BO);
        }

        public DataTable GetRevertedOutwardMaterialListforUser(JQDatatableBO objJQDataTable_BO)
        {
            return objOutwardMaterialDAL.GetRevertedOutwardMaterialListforUserDb(objJQDataTable_BO);
        }

        public int MaterialClientNameInsert(MaterialBO objMaterialBO)
        {
            return objOutwardMaterialDAL.OutwardMaterialNameInsert(objMaterialBO);
        }

        public DataTable LoadOutwardMaterialNameMaster()
        {
            return objOutwardMaterialDAL.LoadOutwardMaterialNameMaster();
        }

        public DataTable GetOutwardMaterialGridHistory(MaterialBO objMaterialBO)
        {
            return objOutwardMaterialDAL.GetOutwardMaterialGridHistory(objMaterialBO);
        }

        public DataTable GetOutwardMaterialReturnQuantityHistory(MaterialBO objMaterialBO)
        {
            return objOutwardMaterialDAL.GetOutwardMaterialReturnQuantityHistory(objMaterialBO);
        }

        public int MaterialNameifExistsorNot(string MaterialName)
        {
            return objOutwardMaterialDAL.MaterialNameifExistsorNot(MaterialName);
        }

        public DataTable GetOutMaterialIDFromNotification(MaterialBO objMaterialBO)
        {
            return objOutwardMaterialDAL.GetOutMaterialIDFromNotification(objMaterialBO);
        }
        public DataTable getRoleWiseDepartments(int EmpID, int RoleID)
        {
            try
            {
                DataTable dtResult = objOutwardMaterialDAL.GetRoleWiseDepartments(EmpID, RoleID);
                return dtResult;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}