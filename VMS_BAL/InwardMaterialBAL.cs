﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using VMS_BO;
using VMS_DAL;


namespace VMS_BAL
{
    public class InwardMaterialBAL
    {
        InwardMaterialDAL objInwardMaterialDAL = new InwardMaterialDAL();

        public int InsertInwardMaterial(MaterialBO objMaterialBO)
        {
            return objInwardMaterialDAL.InsertInwardMaterial(objMaterialBO);
        }
        public DataTable SearchFilterInwardMaterial(MaterialBO objMaterialBO)
        {
            return objInwardMaterialDAL.SearchFilterInwardMaterial(objMaterialBO);
        }
        public int Update_InwardMaterialEdit(MaterialBO objMaterialBO)
        {
            return objInwardMaterialDAL.Update_InwardMaterialEdit(objMaterialBO);
        }
        public DataTable BindDataInwardMaterials()
        {
            return objInwardMaterialDAL.BindDataInwardMaterials();
        }
        public DataTable InwardMateriallinkbuttonFilldata(MaterialBO objMaterialBO)
        {
            return objInwardMaterialDAL.InwardMateriallinkbuttonFilldata(objMaterialBO);
        }

        public DataTable BindMaterialType()
        {
            return objInwardMaterialDAL.BindMaterialType();
        }

        public DataTable GetInMaterialList(JQDatatableBO _objJQDataTableBO)
        {
            return objInwardMaterialDAL.GetInMaterialListDb(_objJQDataTableBO);
        }
    }
}