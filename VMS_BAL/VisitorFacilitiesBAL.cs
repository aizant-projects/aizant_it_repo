﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using VMS_BO;
using VMS_DAL;
using System.Data.SqlClient;

namespace VMS_BAL
{
    public class VisitorFacilitiesBAL
    {
        VisitorFacilitiesDAL objVisitorFacilitiesDAL = new VisitorFacilitiesDAL();
        public int VisitorFacilities_Insert(VisitorFacilitiesBO objVisitorFacilitiesBO)
        {

            return objVisitorFacilitiesDAL.VisitorFacilities_Insert(objVisitorFacilitiesBO);
        }
        public DataSet GetEmpDataToVisitorFacilities(VisitorFacilitiesBO objVisitorFacilitiesBO)
        {
            return objVisitorFacilitiesDAL.GetEmpDataToVisitorFacilities(objVisitorFacilitiesBO);
        }

        public DataTable SearchFilterVisitorFacilities(VisitorFacilitiesBO objVisitorFacilitiesBO)
        {
            return objVisitorFacilitiesDAL.SearchFilterVisitorFacilities(objVisitorFacilitiesBO);
        }

        public int Update_VisitorFacilites(VisitorFacilitiesBO objVisitorFacilitiesBO)
        {
            return objVisitorFacilitiesDAL.Update_VisitorFacilites(objVisitorFacilitiesBO);
        }

        public int VisitorFacilitiesRevert(VisitorFacilitiesBO objVisitorFacilitiesBO)
        {
            return objVisitorFacilitiesDAL.VisitorFacilitiesRevert(objVisitorFacilitiesBO);
        }

        public int VisitorFacilitiesApproval(VisitorFacilitiesBO objVisitorFacilitiesBO)
        {
            return objVisitorFacilitiesDAL.VisitorFacilitiesApproval(objVisitorFacilitiesBO);
        }
        public int VisitorFacilitiesRejectOrCancel(VisitorFacilitiesBO objVisitorFacilitiesBO)
        {
            return objVisitorFacilitiesDAL.VisitorFacilitiesRejectOrCancel(objVisitorFacilitiesBO);
        }

        public DataTable GetVisitorFacilitiesDetails_List(JQDatatableBO _objJQDataTableBO)
        {
            return objVisitorFacilitiesDAL.GetVisitorFacilitiesDetails_ListDb(_objJQDataTableBO);
        }

        public int MasterClientNameInsert(VisitorFacilitiesBO objVisitorFacilitiesBO)
        {
            return objVisitorFacilitiesDAL.MasterClientNameInsert(objVisitorFacilitiesBO);
        }

        public DataTable LoadVisitorClientNameMaster()
        {
            return objVisitorFacilitiesDAL.LoadVisitorClientNameMaster();
        }

        public DataTable GetVisitorFacilitiesGridHistory(VisitorFacilitiesBO objFacilitiesBO)
        {
            return objVisitorFacilitiesDAL.GetVisitorFacilitiesGridHistory(objFacilitiesBO);
        }

        public int VisitorClientNameifExistsorNot(string ClientName)
        {
            return objVisitorFacilitiesDAL.VisitorClientNameifExistsorNot(ClientName);
        }

        public DataTable GetFacilityIDFromNotification(VisitorFacilitiesBO objFacilitiesBO)
        {
            return objVisitorFacilitiesDAL.GetFacilityIDFromNotification(objFacilitiesBO);
        }
    }
}
